#
# This ProGuard configuration file illustrates how to process applications.

# Specify the input jars, output jars, and library jars.

-injars  ../smart-sql-updater/target/updater-1.0.jar
-outjars updater.jar

-libraryjars  <java.home>/lib/rt.jar

# Save the obfuscation mapping to a file, so you can de-obfuscate any stack
# traces later on. Keep a fixed source file attribute and all line number
# tables to get line numbers in the stack traces.
# You can comment this out if you're not interested in stack traces.

#-printmapping out.map
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

-forceprocessing 
-optimizationpasses 3

# Preserve all annotations.

-keepattributes *Annotation*


# Preserve all native method names and the names of their classes.

-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

# Preserve the special static methods that are required in all enumeration
# classes.

-keepclassmembers,allowoptimization enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Preserve all public applications.

-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
    public void start(javafx.stage.Stage);
}

-dontwarn np.com.ngopal.smart.sql.updater.**
-dontwarn javafx.**

-keep class javafx.** { *; }
-keep class java.** { *; }
-keep class np.com.ngopal.smart.sql.updater.Updater.** { *; }
-keep class np.com.ngopal.smart.sql.updater.ChecksAndUpdater.** { *; }
