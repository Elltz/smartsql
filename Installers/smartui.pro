#
# This ProGuard configuration file illustrates how to process applications.
# Usage:
#     java -jar proguard.jar @applications.pro
#

# Specify the input jars, output jars, and library jars.

-injars  ../smart-sql-ui/target/smart-sql-ui-1.0.jar
-outjars smart-sql-ui.jar

-libraryjars  <java.home>/lib/rt.jar
#-libraryjars ../smart-sql-ui/target/lib/


# Save the obfuscation mapping to a file, so you can de-obfuscate any stack
# traces later on. Keep a fixed source file attribute and all line number
# tables to get line numbers in the stack traces.
# You can comment this out if you're not interested in stack traces.

-printmapping smartui-out.map
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

# Preserve all annotations.

-keepattributes *Annotation*

# You can print out the seeds that are matching the keep options below.

#-printseeds smartui-out.seeds

# Preserve all public applications.

-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
    public void start(javafx.stage.Stage);
    public void stop();
}

# Preserve all native method names and the names of their classes.

-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

# Preserve the special static methods that are required in all enumeration
# classes.

-keepclassmembers,allowoptimization enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);

}

-dontwarn np.com.ngopal.smart.sql.**
-dontwarn np.com.ngopal.smart.sql.ui.MainUI.**
-dontwarn javafx.**

-keep class javafx.** { *; }
#-keep class java.** { *; }
-keep class np.com.ngopal.smart.sql.** { *; }
-keep class np.com.ngopal.smart.sql.ui.MainUI.** { *; }
-keep class org.h2.Driver.** { *; }


