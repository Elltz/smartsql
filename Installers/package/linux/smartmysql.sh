#!/bin/bash

#echo "$(whoami)"

#[ "$UID" -eq 0 ] || exec sudo "$0" "$@"

HOMEDIR=~
DIRECTORY=$HOMEDIR/SmartMySQL
APP_DIRECTORY=$DIRECTORY/app
RUNTIME=$DIRECTORY/runtime
VERSION=#auto_import
ARGUMENTS=#auto_import

copyUpdateableContent(){
	if [ -d $APP_DIRECTORY ]; then
		rm -rf $APP_DIRECTORY 
	fi
	cp -rf app $APP_DIRECTORY
	cp -f smartuninstall.sh $DIRECTORY/smartuninstall.sh
	chmod +x $DIRECTORY/smartuninstall.sh
	printf "#!/bin/sh
cd $APP_DIRECTORY/
$RUNTIME/jre/bin/java -jar $APP_DIRECTORY/main_app.jar $ARGUMENTS" > $DIRECTORY/execs.sh
	chmod +x $DIRECTORY/execs.sh
}

copyRuntime(){
	cp -rf runtime $RUNTIME
	cp -f SmartMysql.png $DIRECTORY/SmartMysql.png
}

addDesktops(){
	printf "[Desktop Entry]
Name=SmartMySQL
Version=$VERSION
Comment=SmartMySQL - smarter Mysql Server
Exec=$DIRECTORY/execs.sh
Icon=$DIRECTORY/SmartMysql.png
Terminal=false
Type=Application
Categories=Application " > $DIRECTORY/SmartMySQL.desktop

chmod +x $DIRECTORY/SmartMySQL.desktop
}

#addScripts(){}


updating(){
	echo "STARTING UPDATING SMARTMYSQL"

	if([ -f $HOMEDIR/.SmartMySQL/ef20ad25f2f859b5cf674158a06b4590/dumpFile.pidx ]) then { 
		 echo "stopping application"
		 pid=$(cat $HOMEDIR/.SmartMySQL/ef20ad25f2f859b5cf674158a06b4590/dumpFile.pidx)
		 kill -9 $pid 
	} else {
		 echo "No running instances"
	}
	fi

	copyUpdateableContent

	echo "UPDATE COMPLETE"
}

freshInstallation(){
	mkdir -p -v $DIRECTORY
	echo "STARTING INSTALLATION OF SMARTMYSQL"

	copyUpdateableContent
	copyRuntime
	addDesktops

	echo "adding shortcut to desktop"
	xdg-desktop-icon install --novendor $DIRECTORY/SmartMySQL.desktop
	#xdg-desktop-icon install --novendor $directory/Uninstaller.desktop

	echo "Adding shortcut to the menu"
	xdg-desktop-menu install --novendor $DIRECTORY/SmartMySQL.desktop

}

#javaLocation="$(which java)"
#echo $javaLocation

#if [ ! -z "$java_home" ]; then
#  echo "YOU DO NOT HAVE JAVA ON YOUR SYSTEM.. PLEASE INSTALL IT BEFORE TRYING"
#  echo "PRESS ANY KEY TO EXIT"
#  read $input
#  exit
#else 

echo " SmartMySQL SOFTWARE LICENSE AGREEMENT
------------------------------------------------------------------------------------
This is a legal agreement between you (either an individual or an entity) and SmartMySQL ('SmartSoft'). By installing the enclosed software, you are agreeing to be bound by the terms of this Agreement. If
you do not agree to the terms of this Agreement, promptly return the software and the accompanying items to the place you obtained them for a full refund. If you need to return the software, you must prepay shipping and either insure the package or assume all risk of loss or damage in transit.

SmartMySQL FTP Library LICENSE
1. GRANT OF LICENSE TO USE. The SmartMySQL product that accompanies this license is referred to herein as 'SOFTWARE'. Ngopal Ltd. ('Ngopal') grants to you as an individual, a personal, non-exclusive license to make and use the SOFTWARE for the sole purpose of designing, developing,
and testing your software product(s). Ngopal grants to you the limited right to use only one copy of the Software on a single computer in the manner set forth in this agreement. If you are an entity,
Ngopal grants you the right to designate one individual within your organization to have the right to use the SOFTWARE in the manner provided above. Ngopal reserves all rights not expressly granted.

2. UPDATES. Upon receipt of future updates of the SOFTWARE (including without limitation the Redistributable Code)(an 'UPDATE'), you may use or transfer the UPDATE only in conjunction with your
then-existing SOFTWARE. The SOFTWARE and all UPDATES (including bug fixes and error corrections) shall be provided by Ngopal to you and are licensed as a single product, and the UPDATES may not be separated from the SOFTWARE for use by more than one user at any time.

3. COPYRIGHT. The SOFTWARE is owned by Ngopal or its suppliers and is protected by copyright laws and international treaty provisions.Therefore, you must treat the SOFTWARE like any other copyrighted material (e.g., a book or musical recording). You may not use or copy the SOFTWARE
or any accompanying written materials for any purposes other than what is described in this Agreement.
Ngopal warrants that Ngopal is the sole owner of all patents, copyrights or other applicable intellectual property rights in and to the SOFTWARE unless otherwise indicated in the documentation
for the SOFTWARE. Ngopal shall defend, indemnify, and hold Licensee harmless from any third party claims, including reasonable attorneys' fees, alleging that Software (including without limitation Sample Code) licensed hereunder infringes or misappropriates third party intellectual property rights.

4. OTHER RESTRICTIONS. You may not reverse-engineer, decompile, or disassemble the SOFTWARE except to the extent such foregoing restriction is expressly prohibited by applicable law.

5. REDISTRIBUTABLE CODE. Portions of the SOFTWARE (specifically the run time modules in binary form) are designated as 'Redistributable Code', subject to the Distribution Requirements described below.

6. SAMPLE CODE. Ngopal grants you the right to use and modify the source code version of the included Sample Code for the sole purpose of designing, developing, testing and supporting your software products. You may also reproduce and distribute the Sample Code in object code form along with any modifications you make to the Sample Code, provided that you comply with the Distribution Requirements described below. For purposes of this section, 'modifications' shall mean enhancements to the functionality of the Sample Code.

7. SOURCE CODE. If you have purchased the SOFTWARE, you may not re-distribute it, nor may you copy it into your own projects. Ngopal retains the copyright to the SOFTWARE source code. You have no right to change or use source code for 3rd party components or applications. This agreement allows you to obtain access to fix and update the software's source code under special circumstances, such as to provide support to your end user customers to whom you have distributed Redistributable Code in conformance with Section 8 below, or if the Ngopal goes out of business.

8. DISTRIBUTION REQUIREMENTS. Notwithstanding section 4 above, you are authorized to redistribute the Sample Code and/or Redistributable Code, (collectively 'REDISTRIBUTABLE COMPONENTS') as described in Sections 5 and 6, only if you (a) distribute them in conjunction with and as part of your software product that adds primary and significant functionality to the REDISTRIBUTABLE COMPONENTS ; (b) do not permit further redistribution of the REDISTRIBUTABLE COMPONENTS by your end-user customers ; (c) do not use Ngopal's name, logo, or trademarks to market your software application product ; (d) include a valid copyright notice on your software product ; and (e) agree to indemnify, hold harmless, and defend Ngopal from and against any third party claims or lawsuits, including reasonable attorney's fees, to the extent arising or resulting from your material breach of your obligations under this agreement ; (f) hold a valid license for each computer the REDISTRIBUTABLE COMPONENTS are distributed to ; (g) do not make any modifications to any of the REDISTRIBUTABLE COMPONENTS. 
Ngopal reserves all rights not expressly granted. Contact Ngopal for the applicable royalties due and other licensing terms for all other uses and/or distribution of the REDISTRIBUTABLE COMPONENTS.

LIMITED WARRANTY

NO WARRANTIES. Ngopal expressly disclaims any warranty for the SOFTWARE. The SOFTWARE and any related documentation is provided 'as is' without warranty of any kind, either express or implied, including, without limitation, the implied warranties or merchantability or fitness for a particular purpose. The entire risk arising out of use or performance of the SOFTWARE remains with you.

CUSTOMER REMEDIES. Each party's entire liability under this license agreement
shall not exceed the price paid for the SOFTWARE.
NO LIABILITY FOR CONSEQUENTIAL DAMAGES. In no event shall Ngopal, its suppliers or you be liable for any damages whatsoever (including, without limitation, damages for loss of business profits, business interruptions, loss of business information, or any other pecuniary loss) arising out of
the use or inability to use this Ngopal product, even if such party has been advised of the possibility of such damages. The limitations and disclaimers set forth in this section do not apply to
[a] either party's obligations of indemnity stated herein or
[b] to your material breach of your obligations under this license agreement.

TRIAL. The trial versions of our products are intended for evaluation purposes only. You may not use the trial version for more than its applicable time.

This agreement is protected by copyright laws and international treaty provisions. If you do not agree to the terms of the license agreement, you are not allowed to use this product or any part of it. Should you have any questions concerning this product, contact Ngopal.


PRESS ANY KEY TO CONTINUE
"

read t

if [ -d "$DIRECTORY" ]; then
   updating
 else 
 freshInstallation
 fi
echo "press any key to exit"
read a
exit
#fi
