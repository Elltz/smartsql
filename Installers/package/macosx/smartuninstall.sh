#!/bin/sh

echo Removing shortcut

xdg-desktop-menu uninstall --novendor ~/SmartMySQL/SmartMySQL.desktop
xdg-desktop-icon uninstall --novendor ~/SmartMySQL/SmartMySQL.desktop

if([ -f ~/.SmartMySQL/ef20ad25f2f859b5cf674158a06b4590/dumpFile.pidx ]) then { 
 echo stopping application
 pid=$(cat ~/.SmartMySQL/ef20ad25f2f859b5cf674158a06b4590/dumpFile.pidx)
 kill -9 $pid 
} else {
 echo "No running instances"
}
fi

rm -rf ~/SmartMySQL
rm -f ~/Desktop/SmartMySQL.desktop

exit 0
