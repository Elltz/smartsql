;This file will be executed next to the application bundle image
;I.e. current directory will contain folder SmartMySQL with application files
[Setup]
AppId={{np.com.ngopal.smart.sql.updater.Updater}}
AppName=<app_name>
AppVersion=1.0
AppVerName=<app_name> 1.0
AppPublisher=TopDBS
AppComments='<app_name>'
AppCopyright=Copyright (C) 2016
VersionInfoVersion=1.0.0.0
;AppPublisherURL=http://java.com/
;AppSupportURL=http://java.com/
;AppUpdatesURL=http://java.com/
DefaultDirName={pf}\<app_name>
DisableStartupPrompt=No
DisableDirPage=No
DisableProgramGroupPage=No
DisableReadyPage=No
DisableFinishedPage=No
DisableWelcomePage=No
DefaultGroupName=SmartMySQL
;Optional License
LicenseFile=
;WinXP or above
MinVersion=0,5.1 
OutputBaseFilename=<app_name>
Compression=lzma
SolidCompression=yes
;PrivilegesRequired=poweruser
SetupIconFile=<app_name>\<app_name>.ico
UninstallDisplayIcon={app}\<app_name>.ico
UninstallDisplayName=<app_name>
WizardImageStretch=Yes
WizardSmallImageFile=<app_name>-setup-icon.bmp   
ArchitecturesInstallIn64BitMode=x64 ia64
ChangesAssociations=Yes
WindowVisible=No
BackColor=$3c71b3
BackSolid=No
SetupLogging=Yes
InfoBeforeFile=
ChangesEnvironment=Yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Dirs]
Name: "{app}\app"; Attribs: hidden; Permissions: users-modify;

[Files]
Source: "<app_name>\<app_name>.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "<app_name>\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
;Source: "<app_name>\runtime\bin\*.dll"; DestDir: "{app}"; Flags: onlyifdoesntexist

[Registry]
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType: expandsz; ValueName: "Path"; ValueData: "{olddata};{app}\runtime\bin"; Check: NeedsAddPath( ExpandConstant('{app}\runtime\bin') );
Root: HKLM; Subkey: "SOFTWARE\Classes\.sql\OpenWithProgIds"; ValueType: none; ValueName: "SmartMySQL.AssocFile.SQL"; ValueData: ""; Flags: uninsdeletevalue
Root: HKLM; Subkey: "SOFTWARE\Classes\SmartMySQL.AssocFile.SQL"; ValueType: string; ValueName: ""; ValueData: "SQL File"; Flags: uninsdeletekey
Root: HKLM; Subkey: "SOFTWARE\Classes\SmartMySQL.AssocFile.SQL\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\<app_name>.exe,0"
Root: HKLM; Subkey: "SOFTWARE\Classes\SmartMySQL.AssocFile.SQL\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\<app_name>.exe"" ""%1"""

[Icons]
Name: "{group}\<app_name>"; Filename: "{app}\<app_name>.exe"; IconFilename: "{app}\<app_name>.ico"; Check: returnTrue()
Name: "{commondesktop}\<app_name>"; Filename: "{app}\<app_name>.exe";  IconFilename: "{app}\<app_name>.ico"; Check: returnTrue()  

[Run]
Filename: "{app}\<app_name>.exe"; Parameters: "-Xappcds:generatecache"; Check: returnFalse()
;Filename: "{app}\<app_name>.exe"; Description: "{cm:LaunchProgram,<app_name>}"; Flags: nowait postinstall skipifsilent; Check: returnTrue()
Filename: "{app}\<app_name>.exe"; Parameters: "-install -svcName ""<app_name>"" -svcDesc ""Build and Manage your Mysql Server"" -mainExe ""<app_name>.exe""  "; Check: returnFalse()

[UninstallRun]
Filename: "{app}\<app_name>.exe "; Parameters: "-uninstall -svcName <app_name> -stopOnUninstall"; Check: returnFalse()

[UninstallDelete]
Type: filesandordirs; Name: "{app}\app\*"
Type: files; Name: "{commondesktop}\<app_name>.lnk"

[InstallDelete]
Type: filesandordirs; Name: "{app}\*"

[Code]
function returnTrue(): Boolean;
begin
  Result := True;
end;

function returnFalse(): Boolean;
begin
  Result := False;
end;

function NeedsAddPath(Param: string): boolean;
var
  OrigPath: string;
begin
  Log(OrigPath);
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE,
    'SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
    'Path', OrigPath)
  then begin
    Result := True;
    exit;
  end;
  { look for the path with leading and trailing semicolon }
  { Pos() returns 0 if not found }
  Result := Pos(';' + Param + ';', ';' + OrigPath + ';') = 0;
end;

const
  EnvironmentKey = 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment';

procedure RemovePath(Path: string);
var
  Paths: string;
  P: Integer;
begin
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE, EnvironmentKey, 'Path', Paths) then
  begin
    Log('PATH not found');
  end
    else
  begin
    Log(Format('PATH is [%s]', [Paths]));

    P := Pos(';' + Uppercase(Path) + ';', ';' + Uppercase(Paths) + ';');
    if P = 0 then
    begin
      Log(Format('Path [%s] not found in PATH', [Path]));
    end
      else
    begin
      if P > 1 then P := P - 1;
      Delete(Paths, P, Length(Path) + 1);
      Log(Format('Path [%s] removed from PATH => [%s]', [Path, Paths]));

      if RegWriteStringValue(HKEY_LOCAL_MACHINE, EnvironmentKey, 'Path', Paths) then
      begin
        Log('PATH written');
      end
        else
      begin
        Log('Error writing PATH');
      end;
    end;
  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then
  begin
    RemovePath( ExpandConstant('{app}\runtime\bin') );
  end;
end;

#define MinJRE "1.7"
#define WebJRE "http://www.oracle.com/technetwork/java/javase/downloads"

function IsJREInstalled: Boolean;
var
  JREVersion: string;
begin
  // read JRE version
  Result := RegQueryStringValue(HKLM32, 'Software\JavaSoft\Java Runtime Environment',
    'CurrentVersion', JREVersion);
  // if the previous reading failed and we're on 64-bit Windows, try to read 
  // the JRE version from WOW node
  if not Result and IsWin64 then
    Result := RegQueryStringValue(HKLM64, 'Software\JavaSoft\Java Runtime Environment',
      'CurrentVersion', JREVersion);
  // if the JRE version was read, check if it's at least the minimum one
  if Result then
    Result := CompareStr(JREVersion, '{#MinJRE}') >= 0;
end;

function GetUninstallString(): String;
var
  sUnInstPath: String;
  sUnInstallString: String;
begin
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{#emit SetupSetting("AppId")}_is1');
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
end; 

function IsUpgrade(): Boolean;
begin
  Result := (GetUninstallString() <> '');
end;

function UnInstallOldVersion(): Integer;
var
  sUnInstallString: String;
  iResultCode: Integer;
begin
// Return Values:
// 1 - uninstall string is empty
// 2 - error executing the UnInstallString
// 3 - successfully executed the UnInstallString

  // default return value
  Result := 0;

  // get the uninstall string of the old app
  sUnInstallString := GetUninstallString();
  if sUnInstallString <> '' then begin
    sUnInstallString := RemoveQuotes(sUnInstallString);
    Exec(sUnInstallString, '/VERYSILENT /NORESTART /SUPPRESSMSGBOXES','', SW_HIDE, ewWaitUntilTerminated, iResultCode)
  end;
end;

function InitializeSetup: Boolean;
var
  ErrorCode: Integer;
  // check if JRE is installed; if not, then...
 begin
  Result := True;
  //UnInstallOldVersion();
  if not IsJREInstalled then
  begin
    // show a message box and let user to choose if they want to download JRE;
    // if so, go to its download site and exit setup; continue otherwise
    if MsgBox('Java version {#MinJRE} is required. Do you want to download it now ?',
      mbConfirmation, MB_YESNO) = IDYES then
    begin
      Result := False;
      ShellExec('', '{#WebJRE}', '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode);
    end;
  end;  
end;
