;This file will be executed next to the application bundle image
;I.e. current directory will contain folder SmartMySQL with application files
[Setup]
AppId={{np.com.ngopal.smart.sql.updater.Updater}}
AppName=<app_name>
AppVersion=1.0
AppVerName=<app_name> 1.0
AppPublisher=TopDBS
AppComments='<app_name>'
AppCopyright=Copyright (C) 2016
VersionInfoVersion=1.0.0.0
;AppPublisherURL=http://java.com/
;AppSupportURL=http://java.com/
;AppUpdatesURL=http://java.com/
DefaultDirName={pf}\<app_name>
DisableStartupPrompt=No
DisableDirPage=No
DisableProgramGroupPage=No
DisableReadyPage=No
DisableFinishedPage=No
DisableWelcomePage=No
DefaultGroupName=SmartMySQL
;Optional License
LicenseFile=
;WinXP or above
MinVersion=0,5.1 
OutputBaseFilename=<app_name>
Compression=lzma
SolidCompression=yes
;PrivilegesRequired=poweruser
SetupIconFile=<app_name>\<app_name>.ico
UninstallDisplayIcon={app}\<app_name>.ico
UninstallDisplayName=<app_name>
WizardImageStretch=Yes
WizardSmallImageFile=<app_name>-setup-icon.bmp   
ArchitecturesInstallIn64BitMode=x64 ia64
ChangesAssociations=Yes
WindowVisible=No
BackColor=$3c71b3
BackSolid=No
SetupLogging=Yes
InfoBeforeFile=
ChangesEnvironment=Yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Dirs]
Name: "{app}\app"; Attribs: hidden; Permissions: users-modify;

[Files]
Source: "<app_name>\<app_name>.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "<app_name>\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
;Source: "<app_name>\runtime\bin\*.dll"; DestDir: "{app}"; Flags: onlyifdoesntexist

[Registry]
Root: HKCU; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.sql\UserChoice"; ValueType: none; ValueName: ""; ValueData: ""; Flags: deletekey; Tasks: sqlmakedefault;
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType: expandsz; ValueName: "Path"; ValueData: "{olddata};{app}\runtime\bin"; Check: NeedsAddPath( ExpandConstant('{app}\runtime\bin') );
Root: HKLM; Subkey: "SOFTWARE\Classes\SmartMySQL.AssocFile.SQL"; ValueType: string; ValueName: ""; ValueData: "SQL Text File"; Flags: uninsdeletekey;
Root: HKLM; Subkey: "SOFTWARE\Classes\SmartMySQL.AssocFile.SQL\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\<app_name>.exe,0";
Root: HKLM; Subkey: "SOFTWARE\Classes\SmartMySQL.AssocFile.SQL\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\<app_name>.exe"" ""%1""" ;
Root: HKLM; Subkey: "SOFTWARE\Classes\.sql"; ValueType: string; ValueName: ""; ValueData: "SmartMySQL.AssocFile.SQL"; Flags: uninsdeletevalue; Tasks: sqlmakedefault;
Root: HKLM; Subkey: "SOFTWARE\Classes\.sql\OpenWithProgIds"; ValueType: string; ValueName: "SmartMySQL.AssocFile.SQL"; ValueData: ""; Flags: uninsdeletevalue;

[Icons]
Name: "{group}\<app_name>"; Filename: "{app}\<app_name>.exe"; IconFilename: "{app}\<app_name>.ico"; Check: returnTrue()
Name: "{commondesktop}\<app_name>"; Filename: "{app}\<app_name>.exe";  IconFilename: "{app}\<app_name>.ico"; Check: returnTrue()  

[Run]
Filename: "{app}\<app_name>.exe"; Parameters: "-Xappcds:generatecache"; Check: returnFalse()
;Filename: "{app}\<app_name>.exe"; Description: "{cm:LaunchProgram,<app_name>}"; Flags: nowait postinstall skipifsilent; Check: returnTrue()
Filename: "{app}\<app_name>.exe"; Parameters: "-install -svcName ""<app_name>"" -svcDesc ""Build and Manage your Mysql Server"" -mainExe ""<app_name>.exe""  "; Check: returnFalse()

[UninstallRun]
Filename: "{app}\<app_name>.exe "; Parameters: "-uninstall -svcName <app_name> -stopOnUninstall"; Check: returnFalse()

[UninstallDelete]
Type: filesandordirs; Name: "{app}\app\*"
Type: files; Name: "{commondesktop}\<app_name>.lnk"

[InstallDelete]
Type: filesandordirs; Name: "{app}\*"

[Tasks]
Name: sqlmakedefault; Description: "Make <app_name> your default sql handler";

[Code]
function returnTrue(): Boolean;
begin
  Result := True;
end;

function returnFalse(): Boolean;
begin
  Result := False;
end;

function hasUserSQLUserChoice(): Boolean;
begin
  Result := false;
  if RegKeyExists(HKEY_CURRENT_USER, 'Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.sql\UserChoice' ) then
  begin
    Result := True;
  end;
end;

function NeedsAddPath(Param: string): boolean;
var
  OrigPath: string;
begin
  Log(OrigPath);
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE,
    'SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
    'Path', OrigPath)
  then begin
    Result := True;
    exit;
  end;
  { look for the path with leading and trailing semicolon }
  { Pos() returns 0 if not found }
  Result := Pos(';' + Param + ';', ';' + OrigPath + ';') = 0;
end;

const
  EnvironmentKey = 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment';

procedure RemovePath(Path: string);
var
  Paths: string;
  P: Integer;
begin
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE, EnvironmentKey, 'Path', Paths) then
  begin
    Log('PATH not found');
  end
    else
  begin
    Log(Format('PATH is [%s]', [Paths]));

    P := Pos(';' + Uppercase(Path) + ';', ';' + Uppercase(Paths) + ';');
    if P = 0 then
    begin
      Log(Format('Path [%s] not found in PATH', [Path]));
    end
      else
    begin
      if P > 1 then P := P - 1;
      Delete(Paths, P, Length(Path) + 1);
      Log(Format('Path [%s] removed from PATH => [%s]', [Path, Paths]));

      if RegWriteStringValue(HKEY_LOCAL_MACHINE, EnvironmentKey, 'Path', Paths) then
      begin
        Log('PATH written');
      end
        else
      begin
        Log('Error writing PATH');
      end;
    end;
  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then
  begin
    RemovePath( ExpandConstant('{app}\runtime\bin') );
  end;
end;

function InitializeSetup: Boolean;
var
  ErrorCode: Integer;
 begin
  Result := True;
end;