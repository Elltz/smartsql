/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxSharedLink;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class BoxBuildConnectionEntity extends BuildConnectionEntity {
    
    private BoxAPIConnection boxAPIConnection;
    private final String api_key = "xm9tq8rej3aprjqcgfsoqveh0hmp2axo";
    private final String account_id = "2793585808";//32215696
    
    private final String enterprise_id = "32215696";
    private final String developerToken = "KQKUHN2KNvYnog9aGex2vAkFDFEScQ8e";
    private final String cliendId = "av2wcnz6sm2xfk82uod9c0woy0mcc7iu";
    private final String clientSecret = "lXhBTpxZtfmFskz5EDCu3yJNwMAQcNia";
    
    private final String primary_access_token = "W3R8dKxq0NHER4ML51BuOSljXp7xftxM";
    
    private BoxFolder rootFolder;
        
    private BoxFolder.Info info_config;
    private BoxFolder.Info info_depend;
    private BoxFolder.Info info_execut;
    private static final byte BoxBuildType = 0;

    public BoxBuildConnectionEntity(RuntimeDocumentWrapper runtimeDocumentWrapper, BuildVars buildVars, BuildStatus buildStatus) {
        super(runtimeDocumentWrapper, buildVars, buildStatus);
        runtimeDocumentWrapper.setBuilttype(BoxBuildType);
    }
      

    @Override
    public void initialize() { 
        boxAPIConnection = new BoxAPIConnection(primary_access_token);
//        if(boxAPIConnection.canRefresh()){
//            System.out.println("CAN REFRESH");
//            boxAPIConnection.refresh();
//        }
        System.out.println("Access Token " + boxAPIConnection.getAccessToken());
        System.out.println("BASE URL " + boxAPIConnection.getBaseURL());
        rootFolder = BoxFolder.getRootFolder(boxAPIConnection);
        boolean needsCreation = true;
        
        for (BoxItem.Info itemInfo : rootFolder.getChildren()) {  
            System.out.println(itemInfo.getName());
            if (itemInfo instanceof BoxFolder.Info) {
                BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
                if(folderInfo.getName().equalsIgnoreCase(folder_config)){
                    needsCreation = false; info_config = folderInfo;
                }else if(folderInfo.getName().equalsIgnoreCase(folder_depend)){
                    needsCreation = false; info_depend = folderInfo;
                }else if(folderInfo.getName().equalsIgnoreCase(folder_execut)){
                    needsCreation = false; info_execut = folderInfo;
                }
            }
        }
        
        if(needsCreation){
            //here we create all folders in the server
            BoxFolder.Info childFolderInfo = rootFolder.createFolder(folder_config);
            System.out.println(childFolderInfo.toString());
            info_config = childFolderInfo;
            childFolderInfo = rootFolder.createFolder(folder_depend);
            System.out.println(childFolderInfo.toString());
            info_depend  = childFolderInfo;
            childFolderInfo = rootFolder.createFolder(folder_execut);
            System.out.println(childFolderInfo.toString());
            info_execut = childFolderInfo;
            BoxFolder executeFolder = childFolderInfo.getResource();
            childFolderInfo = executeFolder.createFolder(folder_execut_linux);
            System.out.println(childFolderInfo.toString());
            childFolderInfo = executeFolder.createFolder(folder_execut_mac);
            System.out.println(childFolderInfo.toString());
            childFolderInfo = executeFolder.createFolder(folder_execut_window);
            System.out.println(childFolderInfo.toString());
        }        
                 
    }
    
    private void checkAndCreateLinkForFile(BoxFile.Info a){
        if(true) {return;} //for now no shared links
        if(a.getSharedLink() == null){
            BoxSharedLink.Permissions perm = new BoxSharedLink.Permissions();
            perm.setCanDownload(true); //perm.setCanPreview(true);
            BoxSharedLink boxSharedLink = a.getResource().createSharedLink(
                    BoxSharedLink.Access.OPEN, null, perm);
            System.out.println(boxSharedLink.getDownloadURL());
            a.setSharedLink(boxSharedLink);
        }
    }

    @Override
    public void openConnection() { }

    @Override
    public boolean isConnectionCreationSuccessfull() { return rootFolder != null;}
    
    @Override
    public void uploadJars() { 
        deleteFilesInServerFolder(info_config.getResource(), info_config.getName());
        
        try {
            Thread.sleep(100);
            innerConfiUploadTier(buildVars.getSmart_sql_updater_jar());
        } catch (Exception e) {
            e.printStackTrace();
        }
         
        try {
            Thread.sleep(100);
            innerConfiUploadTier(buildVars.getSmart_sql_ssj_jar());
        } catch (Exception e) {
            e.printStackTrace();
        }
         
        try {
            Thread.sleep(100);
            innerConfiUploadTier(buildVars.getSmart_sql_ui_jar());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateVersion() {
    }
    
    

    @Override
    public void uploadConfig() {
        try {
            Thread.sleep(100);
            BuildEntry.saveRuntimeDocument();
            innerConfiUploadTier(buildVars.getSmart_sql_info_config_file());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    private void innerConfiUploadTier(File f) throws Exception {
        onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "UPLOADING CONFIG JAR " + f.getName());
        FileInputStream fis = new FileInputStream(f);
        BoxFile.Info newFileInfo = info_config.getResource().uploadFile(fis, f.getName());
        fis.close();
        checkAndCreateLinkForFile(newFileInfo);
        publicFileLinks.put(f.getName(), newFileInfo.getID());
        onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "UPLOAD SUCCESSFULL FOR CONFIG JAR " + f.getName());
    }

    @Override
    public void uploadBinaries() {
        onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "");

        boolean hasFolder = false;
        BoxFolder.Info parentFolderInfo = null;

        for (BoxItem.Info itemInfo : info_execut.getResource()) {
            if (itemInfo instanceof BoxFolder.Info) {
                BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
                // Do something with the folder.
                if ( (buildVars.isIs32bit() && folderInfo.getName().equalsIgnoreCase(_32BitVersion)) ||
                       (!buildVars.isIs32bit() && folderInfo.getName().equalsIgnoreCase(_64BitVersion)) ){
                    hasFolder = true;
                    parentFolderInfo = folderInfo;
                    break;
                }
            }
        }
        
        if(hasFolder){
            deleteFilesInServerFolder(parentFolderInfo.getResource(), parentFolderInfo.getName());
        }else{
            onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "CREATING MISSING BINARY FOLDER ");
            parentFolderInfo = info_execut.getResource().createFolder(buildVars.isIs32bit()? _32BitVersion : _64BitVersion);            
        }

        for (File f : buildVars.getOutputDirectory().listFiles()) {
             onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "UPLOADINIG BINARY FILE " + f.getName());
            try {
                FileInputStream fis = new FileInputStream(f);
                BoxFile.Info newFileInfo = parentFolderInfo.getResource().uploadFile(fis, f.getName());
                fis.close();
                checkAndCreateLinkForFile(newFileInfo);
                onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "UPLOAD COMPLETED FOR " + f.getName());
                //onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "DOWNLOAD URL FOR " + newFileInfo.getSharedLink().getDownloadURL());
                publicFileLinks.put(f.getName(), newFileInfo.getID());
            } catch (Exception e) {
                e.printStackTrace();
                onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "FAILED UPLOAD " + e.getMessage());
            }
        }

    }

    @Override
    public void uploadLibs() {
        onOperationNotification(BuildEntry.BUILD_VARAGS_UPDATE, "");
        HashMap<String,String> onlineDependencyLinks = new HashMap();
        ArrayList<String> errorKeyList = new ArrayList(runtimeDocumentWrapper.getDependencyDirectory().size());
        
        deleteFilesInServerFolder(info_depend.getResource(), info_depend.getName());
        
        for (Map.Entry<String, String> en : runtimeDocumentWrapper.getDependencyDirectory().entrySet()) {
            onOperationNotification(BuildEntry.BUILD_VARAGS_UPDATE, "UPLOADING LIBRARY " + en.getKey());
            try {
                System.out.println("En.getValue() " + en.getValue());
                File theFile = new File(new URI(en.getValue()).getPath());
                
                if(!theFile.exists()){
                    System.out.println(theFile.getPath() + " does not exist");
                    continue;
                }
                
                FileInputStream fis = new FileInputStream(theFile);
                BoxFile.Info newFileInfo = info_depend.getResource().uploadFile(fis, en.getKey());
                fis.close();
                checkAndCreateLinkForFile(newFileInfo);
                //System.out.println("DOWNLOAD URL FOR " + newFileInfo.getSharedLink().getDownloadURL());
                onlineDependencyLinks.put(en.getKey(), newFileInfo.getID());
            } catch (Exception e) {
                e.printStackTrace();
                onOperationNotification(BuildEntry.BUILD_VARAGS_UPDATE, "FAILED UPLOAD " + e.getMessage());
                errorKeyList.add(en.getKey());
            }
        }
        
        System.out.println(errorKeyList.size() + " LIBRARIES FAILED TO UPLOAD");
        
        if(errorKeyList.size() > 0){
            for(String key : errorKeyList){
                onlineDependencyLinks.put(key, runtimeDocumentWrapper.getDependencyDirectory().get(key));
            }
        }
        
        runtimeDocumentWrapper.setDependencyDirectory(onlineDependencyLinks);
        runtimeDocumentWrapper.forceNextConversion();
    }
    
    private void deleteFilesInServerFolder(BoxFolder boxFolder, String name) {
        for (BoxItem.Info itemInfo : boxFolder.getChildren()) {
            if (itemInfo instanceof BoxFile.Info) {
                System.out.println("ABOUT TO DELETE FILE ("
                        + itemInfo.getName() + ") FROM FOLDER " + name);
                ((BoxFile.Info) itemInfo).getResource().delete();
            }
        }
    }
}
