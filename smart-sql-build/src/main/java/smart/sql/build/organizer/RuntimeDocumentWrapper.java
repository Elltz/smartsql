/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.google.gson.Gson;
import java.util.Map;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * BUILD CODE BASE
 */
public class RuntimeDocumentWrapper {
    
    private transient Gson gson;
    private transient String documentWrapperString;
    private boolean forceConvert;
    private byte builttype;
    
    private String newUpdateNotice, version;
    private String bannerText;
    private String youtubeText;
    private String onlineServer;
    private String appEncryptionKey;
    
    private long appSize;
    private String appHash;
    private Map<String,String> dependencyHashes;
    private Map<String,String> dependencyDirectory;    
    
    public RuntimeDocumentWrapper(Gson gson) { this.gson = gson; }
    
    public static RuntimeDocumentWrapper getRuntimeDocumentWrapperInstance(Gson gson , String documentWrap){
        RuntimeDocumentWrapper runtimeDocument = gson.fromJson(documentWrap, RuntimeDocumentWrapper.class);
        runtimeDocument.setGson(gson);
        return runtimeDocument;
    }
    
    @Override
    public String toString() {
        if(forceConvert || documentWrapperString == null || documentWrapperString.isEmpty()){
            documentWrapperString = gson.toJson(RuntimeDocumentWrapper.this, RuntimeDocumentWrapper.class);
            forceConvert = false;
        }
        return documentWrapperString;
    }

    public String getNewUpdateNotice() {
        return newUpdateNotice;
    }

    public long getAppSize() {
        return appSize;
    }

    public String getAppHash() {
        return appHash;
    }

    public Map<String, String> getDependencyHashes() {
        return dependencyHashes;
    }

    public String getVersion() {
        return version;
    }

    public void setNewUpdateNotice(String newUpdateNotice) {
        this.newUpdateNotice = newUpdateNotice;
    }

    public void setBannerText(String text) {
        this.bannerText = text;
    }
    public String getBannerText() {
        return bannerText;
    }
    
    public void setYoutubeText(String text) {
        this.youtubeText = text;
    }
    public String getYoutubeText() {
        return youtubeText;
    }
    
    public void setOnlineServer(String text) {
        this.onlineServer = text;
    }
    public String getOnlineServer() {
        return onlineServer;
    }

    public String getAppEncryptionKey() {
        return appEncryptionKey;
    }

    public void setAppEncryptionKey(String appEncryptionKey) {
        this.appEncryptionKey = appEncryptionKey;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }

    public void setAppSize(long appSize) {
        this.appSize = appSize;
    }

    public void setAppHash(String appHash) {
        this.appHash = appHash;
    }

    public void setDependencyHashes(Map<String, String> dependencyHashes) {
        this.dependencyHashes = dependencyHashes;
    } 

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public void setDependencyDirectory(Map<String, String> dependencyDirectory) {
        this.dependencyDirectory = dependencyDirectory;
    }

    public Map<String, String> getDependencyDirectory() {
        return dependencyDirectory;
    }
    
    public void forceNextConversion(){
        forceConvert = true;
    }    

    public byte getBuilttype() {
        return builttype;
    }

    public void setBuilttype(byte builttype) {
        this.builttype = builttype;
    }
    
}
