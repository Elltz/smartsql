/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.sun.glass.ui.Screen;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * 
 */
public class Main extends Application {
    
    private BuildUIHandler controller;
    
    public static void main(String[] args) {
         launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("BuildUI.fxml"));
        loader.load();
        
        controller = loader.getController();        
        stage.setScene(new Scene(controller.getParent()));
        
        stage.initStyle(StageStyle.DECORATED);
        stage.setTitle("(C) TOPDBS BUILD SYSTEM UI");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("images/icon.png")));
        stage.setResizable(false);
        stage.setY(1.0);
        stage.show();    
        double screenHeight = Screen.getMainScreen().getHeight();
        if(stage.getHeight() > screenHeight){
            stage.setHeight(screenHeight - 100);
            stage.setResizable(true);
        }
        controller.setParentStage(stage);
    }    

    @Override
    public void stop() throws Exception {
        super.stop();
        System.gc();System.gc();System.gc();System.gc();System.gc();System.gc();
        System.exit(0);
    }
}
