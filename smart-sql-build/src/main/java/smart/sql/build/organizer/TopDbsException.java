/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class TopDbsException extends RuntimeException {

    public TopDbsException(String message) {
        super(message);
    }

    public TopDbsException() {
    }

    public TopDbsException(String message, Throwable cause) {
        super(message, cause);
    }

    public TopDbsException(Throwable cause) {
        super(cause);
    }

    public TopDbsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
