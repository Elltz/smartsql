/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.WriteChannel;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.CopyWriter;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class GoogleCloudBuildConnectionEntity extends BuildConnectionEntity {

    private ServiceAccountCredentials serviceAccountCredentials;

    private Bucket smartMysql_storage_bucket;
    private Bucket smartMysql_version_bucket;
    private Bucket smartMysql_executable_bucket;
    private Storage storage;

    private String projectBucketName;
    public static final String PROJECT_ID = "smartmysql";
    public static final String AUTHEN_ID = "keys_updator.json";

    private final byte BoxBuildType = 1;

    private ArrayList<String> libsErrorKeyList;
    private ArrayList<Acl> defaultAcls;

    public static String versionQA_ID = "version_qa.json";
    public static String versionPROD_ID = "version_production.json";

    public GoogleCloudBuildConnectionEntity(RuntimeDocumentWrapper runtimeDocumentWrapper,
            BuildVars buildVars, BuildStatus buildStatus) {
        super(runtimeDocumentWrapper, buildVars, buildStatus);
        if (runtimeDocumentWrapper != null) {
            this.runtimeDocumentWrapper.setBuilttype(BoxBuildType);
        }
        defaultAcls = new ArrayList(2);
        defaultAcls.add(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER));
    }

    public GoogleCloudBuildConnectionEntity(BuildVars bv) {
        this(null, bv, null);
    }

    @Override
    public void initialize() {
        try {
            if (storage == null) {
                File f = new File(buildVars.getInputDirectoryLocation(), AUTHEN_ID);
                serviceAccountCredentials = ServiceAccountCredentials.fromStream(new FileInputStream(f));
                storage = StorageOptions.newBuilder()
                        .setProjectId(PROJECT_ID)
                        .setCredentials(serviceAccountCredentials)
                        .build().getService();
            }
            smartMysql_executable_bucket = null;
            smartMysql_storage_bucket = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
//        
//        if(buildVars.getModule_project() != null && !buildVars.getModule_project().isEmpty()){
//            setProjectName(buildVars.getModule_project());
//        }
    }

    @Override
    public void openConnection() {
        // NO NEED OLD
        //here we download old runtime file if it exist        
//        try {
//            
//            Blob serverBlobFile = smartMysql_storage_bucket.get(buildVars.getSmart_sql_info_config_file().getName());
//            if (serverBlobFile != null) {
//                File tempfile = Files.createTempFile("config", "config").toFile();
//                serverBlobFile.downloadTo(tempfile.toPath());
//
//                String protocolJsonString = new String(Files.readAllBytes(tempfile.toPath()), Charset.forName("UTF-8"));
//                System.out.println(protocolJsonString);
//                oldRuntimeDocumentWrapper = RuntimeDocumentWrapper.getRuntimeDocumentWrapperInstance(
//                        new Gson(), protocolJsonString.trim());
//                protocolJsonString = null;
//                tempfile.delete();
//                serverBlobFile = null;
//                
//                System.gc();
//                Runtime.getRuntime().gc();
//            }
//                        
//        } catch (Exception e) { e.printStackTrace(); }
    }

    @Override
    public boolean isConnectionCreationSuccessfull() {
        return storage != null;
    }

    @Override
    public void uploadLibs() {
        onOperationNotification(BuildEntry.BUILD_VARAGS_UPDATE, "");
        libsErrorKeyList = new ArrayList(50);

        for (Map.Entry<String, String> en : runtimeDocumentWrapper.getDependencyDirectory().entrySet()) {
            onOperationNotification(BuildEntry.BUILD_VARAGS_UPDATE, "UPLOADING LIBRARY " + en.getKey());
            try {
                System.out.println("En.getValue() " + en.getValue());
                File theFile = new File(new URI(en.getValue()).getPath());

                if (!theFile.exists()) {
                    System.out.println(theFile.getPath() + " does not exist");
                    continue;
                }
                
                createORupdate(getStorageBucket(), true, null, theFile, null, publicFileLinks);

            } catch (Exception e) {
                e.printStackTrace();
                onOperationNotification(BuildEntry.BUILD_VARAGS_UPDATE, "FAILED UPLOAD " + e.getMessage());
                libsErrorKeyList.add(en.getKey());
            }
        }

        System.out.println(libsErrorKeyList.size() + " LIBRARIES FAILED TO UPLOAD");
    }

    @Override
    public void uploadBinaries() {

        onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "");

        for (File f : buildVars.getOutputDirectory().listFiles()) {
            onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "UPLOADINIG BINARY FILE " + f.getName());
            try {

                String sufix = (buildVars.isStandalone() ? "_standalone" : "")/* + (buildVars.getBuild_type() == 1 ? "" : "_QA")*/;
//                if (PlatformUtil.isMac()) {
//                    sufix += "_MAC";
//                } else if (PlatformUtil.isUnix()) {
//                    sufix += "_linux";
//                } else if (PlatformUtil.isWindows()) {
//                    sufix += "_win";
//                }

                Blob blob = createORupdate(getStorageBucket(), getExecutableFolderName(), f, sufix, publicFileLinks);
                
                if(buildVars.getBuild_type() == 1){
                    makeFileAccesibleOnPublicExecutableBucket(blob);
                }
            } catch (Exception e) {
                log.error("Error", e);
                onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "FAILED UPLOAD " + e.getMessage());
            }
        }
    }

    @Override
    public void uploadJars() {

        try {
            mindHalt();
            createORupdate(getStorageBucket(), null, buildVars.getSmart_sql_updater_jar(), null, publicFileLinks);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mindHalt();
            createORupdate(getStorageBucket(), null, buildVars.getSmart_sql_ssj_jar(), null, publicFileLinks);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mindHalt();
            createORupdate(getStorageBucket(), null, buildVars.getSmart_sql_ui_jar(), null, publicFileLinks);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateVersion() {
        //updateVersion(buildVars.getVersionNumber(), buildVars.getBuild_type());
        //throw new RuntimeException("Update without parameters not supported");
    }

    public void updateVersion(String versionContent, byte buildType) {
        String versionName = buildType == 1 ? versionPROD_ID : versionQA_ID;
        Blob versionBlob = getVersionBucket().get(versionName);
        try {
            byte[] content = versionContent.getBytes("UTF-8");
            if (versionBlob == null) {
                BlobInfo serverBlobInfoFile = BlobInfo.newBuilder(BlobId.of(getVersionBucket().getName(), versionName))
                        .setCacheControl("no-cache")
                        .setAcl(defaultAcls)
                        .build();
                versionBlob = getVersionBucket().getStorage().create(serverBlobInfoFile);
            }

            try (WriteChannel channel = versionBlob.writer()) {
                channel.write(ByteBuffer.wrap(content));
            } catch (Throwable th) {
                log.error("Error", th);
            }
        } catch (UnsupportedEncodingException ex) {
            log.error("Error", ex);
        }
    }

    @Override
    public void uploadConfig() {
        try {
            onPreUploadConfig();
            mindHalt();
            createORupdate(getStorageBucket(), null, buildVars.getSmart_sql_info_config_file(), null, publicFileLinks);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mindHalt() throws Exception {
        System.gc();
        Thread.sleep(1000);
    }

    private Blob createORupdate(Bucket bucket, String parentFolder, File f, String sufix, Map<String, String>... detailsLoggers) throws Exception {
        return createORupdate(bucket, false, parentFolder, f, sufix, detailsLoggers);
    }

    private Blob createORupdate(Bucket bucket, boolean stripDashFromName, String parentFolder, File f, String sufix, Map<String, String>... detailsLoggers) throws Exception {
        String name = f.getName();

        if (sufix != null) {
            String[] strs = name.split("\\.");
            name = strs[0] + sufix + "." + strs[1];
        }
        
        if(parentFolder != null){
            if(!parentFolder.endsWith("/")){
                parentFolder = parentFolder + "/";
            }
            name = parentFolder + name;
        }

        Blob serverBlobFile = bucket.get(name);
        FileInputStream fis = new FileInputStream(f);

        if (serverBlobFile == null) {

            System.out.println("-------- CREATING FILE " + name + " -----------");
            BlobInfo serverBlobInfoFile = BlobInfo.newBuilder(BlobId.of(bucket.getName(), name))
                    .setCacheControl("no-cache")
                    .setAcl(defaultAcls)
                    .build();
            serverBlobFile = bucket.getStorage().create(serverBlobInfoFile);

        }

        WriteChannel channel = serverBlobFile.writer();
        byte[] array = new byte[1024];
        int numBytesRead = 0;
        while ((numBytesRead = fis.read(array)) != -1) {
            channel.write(ByteBuffer.wrap(array, 0, numBytesRead));
        }
        array = null;
        channel.close();
        fis.close();
        System.gc();

        String derivedLink = deriveLinkFromBucket_file(name);

        name = BuildEntry.getNameFromFile(f, stripDashFromName);

        for (Map<String, String> detailsLogger : detailsLoggers) {
            detailsLogger.put(name, derivedLink);
        }
        
        return serverBlobFile;
    }

    private String deriveLinkFromBucket_file(String filename) {
        return deriveLinkFromBucket_file(filename, buildVars.getBuild_type(), buildVars.getVersionNumber());
    }

    private String deriveLinkFromBucket_file(String filename, int buildType, String versionNumber) {
        //http://storage.googleapis.com/[BUCKET_NAME]/[OBJECT_NAME]
        return "http://storage.googleapis.com/"
                + getProjectBucketName() + (buildType == 1 ? "_production" : "_qa") + "_" + versionNumber.replaceAll("\\.", "_")
                + "/"
                + filename;
    }

    private void onPreUploadConfig() throws Exception {
        runtimeDocumentWrapper.setDependencyDirectory(publicFileLinks);
        runtimeDocumentWrapper.forceNextConversion();
        BuildEntry.saveRuntimeDocument();
        System.out.println(runtimeDocumentWrapper.toString());
    }

    public String getExecutableBucketName() {
        return "smartmysql_executable";
    }

    public String getExecutableFolderName() {
        return getProjectBucketName() + "_executable/";
    }

    private String getVersionBucketName() {
        return getProjectBucketName() + "_versions";
    }

    public String getProjectBucketName() {
        if (projectBucketName == null || projectBucketName.isEmpty()) {
            throw new TopDbsException("Project name(bucket for project) is empty");
        }
        return projectBucketName;
    }

    public void setProjectName(String projectName) {
        this.projectBucketName = projectName;
    }

    public Storage getStorage() {
        return storage;
    }

    public ArrayList<Acl> getDefaultAcls() {
        return defaultAcls;
    }

    public Bucket getStorageBucket() {
        if (smartMysql_storage_bucket == null) {
            String name = getProjectBucketName() + (buildVars.getBuild_type() == 1 ? "_production" : "_qa") + "_" + buildVars.getVersionNumber().replaceAll("\\.", "_");
            smartMysql_storage_bucket = storage.get(name, Storage.BucketGetOption.fields(Storage.BucketField.NAME));
            if (smartMysql_storage_bucket == null || !smartMysql_storage_bucket.exists()) {
                smartMysql_storage_bucket = storage.create(BucketInfo.of(name));
            }
        }

        return smartMysql_storage_bucket;
    }

    public Bucket getVersionBucket() {
        if (smartMysql_version_bucket == null) {
            smartMysql_version_bucket = storage.get(getVersionBucketName(), Storage.BucketGetOption.fields(Storage.BucketField.NAME));
            if (smartMysql_version_bucket == null || !smartMysql_version_bucket.exists()) {
                smartMysql_version_bucket = storage.create(BucketInfo.of(getVersionBucketName()));
            }
        }
        return smartMysql_version_bucket;
    }

    public Bucket getExecutableBucket() {
        if (smartMysql_executable_bucket == null) {
                smartMysql_executable_bucket = storage.get(getExecutableBucketName(), Storage.BucketGetOption.fields(Storage.BucketField.NAME));
                if (smartMysql_executable_bucket == null || !smartMysql_executable_bucket.exists()) {
                    smartMysql_executable_bucket = storage.create(BucketInfo.of(getExecutableBucketName()));
                }
            }
        return smartMysql_executable_bucket;
    }
    
    public void makeFileAccesibleOnPublicExecutableBucket(Blob blob) {
        //HERE WE COPY FILE TO PUBLIC EXECUTABLE BUCKET
        String destname = blob.getName();
        destname = destname.substring(destname.lastIndexOf('/') + 1);
//        destname = destname.replace("_QA", "");
        //here we have "getExecutableBucket().getName()" to make sure the bucket is created if not exist
        CopyWriter cw = blob.copyTo(getExecutableBucket().getName(), destname);
        getStorage().createAcl(cw.getResult().getBlobId(),
                getDefaultAcls().get(0));
    }
}
