package smart.sql.build.organizer;

import java.io.File;
import java.math.BigDecimal;
import java.util.Random;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class TestBuild {

    public static void main(String[] args) {
        
        System.exit(0);
    }
    
    private static void callTestModuleVersionHelper() {
        try {
            GoogleCloudBuildConnectionEntity connectionEntity = 
                    new GoogleCloudBuildConnectionEntity(new BuildVars("", "smart-sql-ui"));
            ModuleVersionHelper mvh = new ModuleVersionHelper((byte) 1, connectionEntity);
            connectionEntity.setProjectName("smart-sql-ui");
            // here we create dummy versions
            Random ran = new Random();
            for (int i = 0; i < 10; i++) {
                BigDecimal bd = BigDecimal.valueOf(ran.nextDouble()).setScale(2, BigDecimal.ROUND_HALF_UP);
                mvh.addNewVersion(bd.toString());
            }
            mvh.setCurrentVersion(String.valueOf(BigDecimal.valueOf(ran.nextDouble()).setScale(2, BigDecimal.ROUND_HALF_UP)));
            mvh.connectToMysqlDb();
            mvh.updateDatabaseCurrentVersion();
            mvh.updateDatabaseSwState();
            mvh.disConnectionFromMysqlDb();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    private static void callTestBuild(){
        System.setProperty(BuildVars.EXECUTE_BUILD_FLAG, "true");
        try {
            BuildEntry.main(new String[]{
                "9.0",
                new File("").getAbsoluteFile().getParentFile().getPath(),
                "smart-sql-ui",
                "SmartMySQL",
                "exe",
                "2",
                "a,b",
                "-BjvmOptions=-XX:MinHeapFreeRatio=5 -BjvmOptions=-XX:MaxHeapFreeRatio=50 -BjvmOptions=-Xms100m -BjvmOptions=-XX:NewRatio=2",
                "1 2 1 1",
                "-1",
                "test message",
                "2",
                "-1"
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
