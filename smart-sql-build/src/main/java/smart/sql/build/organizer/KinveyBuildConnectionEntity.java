/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import java.io.File;
import java.util.Map;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public abstract class KinveyBuildConnectionEntity /*extends BuildConnectionEntity {
    
    private RuntimeDocumentWrapper runtimeDocumentWrapper;
    private boolean connectionSuccess;
    
    @Override
    public void initialize() { 
        
    }

    @Override
    public void openConnection() { 
        
    }

    @Override
    public boolean isConnectionCreationSuccessfull() {  return connectionSuccess; }

    @Override
    public void beginUploadFiles(String[] filetypeToUpload) {         
        if(runtimeDocumentWrapper == null){ throw new TopDbsException("Runtime Document is null"); }
        
        for (String s : filetypeToUpload) {
            if (s.equals(BuildEntry.BUILD_VARAGS_ALL) || s.equals(BuildEntry.BUILD_VARAGS_LIBS)) {
                uploadLIBS();
                break;
            }
        }

        for (String s : filetypeToUpload) {
            if (s.equals(BuildEntry.BUILD_VARAGS_ALL) || s.equals(BuildEntry.BUILD_VARAGS_BINARIES)) {
                uploadBINARIES();
                break;
            }
        }

        for (String s : filetypeToUpload) {
            if (s.equals(BuildEntry.BUILD_VARAGS_ALL) || s.equals(BuildEntry.BUILD_VARAGS_JAR__CONFIG)) {
                uploadJARS__CONFIG();
                break;
            }
        }
        
    }
    
    @Override
    public void setRuntimeDocument(RuntimeDocumentWrapper runtimeDocumentWrapper) { this.runtimeDocumentWrapper = runtimeDocumentWrapper; }

    @Override
    public RuntimeDocumentWrapper getFinalizedRuntime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void uploadJARS__CONFIG() { 
        onOperationNotification(BuildEntry.BUILD_VARAGS_JAR__CONFIG, "");
    }

    private void uploadBINARIES() { 
        onOperationNotification(BuildEntry.BUILD_VARAGS_BINARIES, "");
    }

    private void uploadLIBS() { 
        onOperationNotification(BuildEntry.BUILD_VARAGS_LIBS, "");
        
    }*/
    
{}
