/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.google.gson.Gson;
import com.sun.javafx.PlatformUtil;
import java.awt.Desktop;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class BuildEntry {

    private static RuntimeDocumentWrapper runtimeDocumentWrapper;
    private static BuildStatus buildStatus;
    private static BuildVars buildVars;

    public static final String BUILD_VARAGS_ALL = "ALL";
    public static final String BUILD_VARAGS_BINARIES = "BINARIES";
    public static final String BUILD_VARAGS_UPDATE = "LIBS";
    public static final String moduleName = Main.class.getPackage().getName();
    private static File build_io_file;

    /**
     * This type of binary version allows update and runs update jar and scripts
     * to check for update
     */
    public final static byte BINARY_PACKAGE_UPDATABLE_TYPE = 2;

    /**
     * This type of binary versions contains all stabile packages and jars with
     * dependencies needed for application to run. it does not check for
     * updates.
     */
    public final static byte BINARY_PACKAGE_BUNDLED_TYPE = 1;

    private static void copy(File source, File target) throws Exception {
        System.out.println("source = " + source.getAbsolutePath());
        System.out.println("target = " + target.getAbsolutePath());
        System.out.println();
        if (source.isDirectory()) {
            File dir;
            if (source.getName().equals(target.getName())) {
                dir = target;
            } else {
                dir = new File(target, source.getName());
            }
            dir.mkdirs();
            for (File f : source.listFiles()) {
                copy(f, new File(dir, f.getName()));
            }
        } else {
            Files.copy(source.toPath(), target.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private static void delDirectory(File file, boolean delOrig) throws Exception {
        while (file.listFiles() != null && file.listFiles().length != 0) {
            File temp = file.listFiles()[0];
            if (temp.isDirectory()) {
                delDirectory(temp, true);
                continue;
            }
            temp.delete();
        }
        if (delOrig && file.exists()) {
            if (!file.delete()) {
                throw new TopDbsException(file.getName() + " was unable to delete in clean phase of build");
            }
        }
    }

    private static File[] locateTempFiles() {
        System.out.println();
        File parent = null;
        if (parent.exists()) {

            return new File[]{};
        } else {
            parent = new File(System.getProperty("java.io.tmpdir"));
            return parent.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return dir.isDirectory() && name.startsWith(moduleName);
                }
            });
        }
    }

    private static File getBuil_io_tmpDirectory() {
        if (build_io_file == null) {
            File userDir = new File(System.getProperty("user.dir"));
            System.out.println(userDir.getPath());
            if (!userDir.getName().equalsIgnoreCase("smart-sql-build")) {
                userDir = new File(userDir, "smart-sql-build");
            }
            build_io_file = new File(userDir, "build_io");
            System.out.println("==========================================");
            System.out.println(build_io_file.getPath());
            System.out.println("==========================================");
        }

        if (!build_io_file.exists()) {
            build_io_file.mkdir();
        }

        return build_io_file;
    }

    /**
     * This method checks if we have cached buildVars and buildStatus and
     * restores it from the disk. This method helps when build abruptly
     * shutsdown.
     */
    private static void restorePropertiesFromCache(String[] args) {
        getBuil_io_tmpDirectory();
        if (build_io_file.exists() && build_io_file.isDirectory() && build_io_file.listFiles() != null
                && build_io_file.listFiles().length > 1) {
            readStatus_Vars(build_io_file.listFiles()[0], args);
            readStatus_Vars(build_io_file.listFiles()[1], args);
        }
    }

    private static void readStatus_Vars(File f, String[] args) {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(f))) {
            boolean isStatus = f.getName().equalsIgnoreCase(BuildStatus.class.getName());
            if (isStatus) {
                buildStatus = (BuildStatus) inputStream.readObject();
            } else {
                buildVars = (BuildVars) inputStream.readObject();
                if (buildVars != null && args != null) {
                    buildVars.updateDynamicContent(args);
                }
            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.gc();
    }

    private static void writeStatus_Vars() {
        try {
            System.out.println("ABOUT TO WRITE BUILD STATUS");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(
                    new File(getBuil_io_tmpDirectory(), BuildStatus.class.getName())));
            objectOutputStream.writeObject(buildStatus);
            objectOutputStream.close();

            System.out.println("ABOUT TO WRITE BUILD VARAGS");

            objectOutputStream = new ObjectOutputStream(new FileOutputStream(
                    new File(getBuil_io_tmpDirectory(), BuildVars.class.getName())));
            objectOutputStream.writeObject(buildVars);
            objectOutputStream.close();

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static FileFilter getRequiredFilesFinder(String a) {
        return new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                String name = pathname.getName();
                return name.charAt(0) == a.charAt(0)
                        && name.startsWith(a) && name.endsWith("jar");
            }
        };
    }

    private static File getRequiredFileFromDir(File dir, String a) {
        return dir.listFiles(getRequiredFilesFinder(a))[0];
    }

    private static void platformCopy() throws Exception {
        if (!buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_PLATFORM_COPY)) { return; }
        //for debug
//        try { Desktop.getDesktop().open(buildVars.getOperatingDirectoryLocation()); }
//        catch (IOException ex) {}

        File target = new File(new File(new File(buildVars.getHomeBase()),
                buildVars.getModule_project().replace('_', '-')), "target");
        File lib = new File(target, "lib");
        
        File target2 = new File(new File(new File(buildVars.getHomeBase()),
                "smart-sql-schedule-job"), "target");
        File target3 = new File(new File(new File(buildVars.getHomeBase()),
                "smart-sql-dependency-updater"), "target");

        File operatingPackageDir = new File(buildVars.getOperatingDirectoryLocation(), "package/");
        //copy packaging
        copy(buildVars.getOsDirectoryLocation(), operatingPackageDir);
        //rename packaging files to suit project level build
        for (File packfiles : new File(operatingPackageDir, buildVars.getOsDirectoryLocation().getName()).listFiles()) {
            packfiles.renameTo(new File(packfiles.getParentFile(), packfiles.getName().replace("SmartMysql", buildVars.getAppName(false))));
        }
        //copy libs
        copy(lib, buildVars.getSmart_sql_libs_folder().getParentFile());//because of how the copy method works
        copy(getRequiredFileFromDir(target, "main_app"), buildVars.getSmart_sql_ui_jar());
        copy(getRequiredFileFromDir(target2, "ssj"), buildVars.getSmart_sql_ssj_jar());
        
        if(!buildVars.isStandalone()){
            copy(getRequiredFileFromDir(target3, "update_utility"), buildVars.getSmart_sql_updater_jar());
            if (PlatformUtil.isWindows()){
                copy(new File(buildVars.getInputDirectoryLocation(), "smart-console.lnk"),
                        buildVars.getWindowSmartConsolePath());
            }            
        }

        if (PlatformUtil.isUnix()) {
            copy(new File(buildVars.getJavaHome()), buildVars.getJre_runtime());

            copy(new File(buildVars.getOsDirectoryLocation(), "smartuninstall.sh"), new File(buildVars.getAppBundleLocation(),
                    "smartuninstall.sh"));
            copy(new File(buildVars.getOsDirectoryLocation(), "smartmysql.sh"), new File(buildVars.getAppBundleLocation(),
                    "smartmysql.sh"));
            copy(new File(buildVars.getOsDirectoryLocation(), "SmartMysql-setup-icon.png"), new File(buildVars.getAppBundleLocation(),
                    "SmartMysql.png"));
            copy(new File(buildVars.getOsDirectoryLocation(), "makeself.sh"), new File(buildVars.getOperatingDirectoryLocation(),
                    "makeself.sh"));
            copy(new File(buildVars.getOsDirectoryLocation(), "makeself-header.sh"), new File(buildVars.getOperatingDirectoryLocation(),
                    "makeself-header.sh"));
        }
    }

    private static void executeScript(String... script) throws Exception {
        executeScript(false, script);
    }
    private static void executeScript(boolean streamAfterFinish,String... script) throws Exception {
        Process proc;

        if (script.length > 1) {
            proc = Runtime.getRuntime().exec(script, null, buildVars.getOperatingDirectoryLocation());
        } else {
            proc = Runtime.getRuntime().exec(script[0], null, buildVars.getOperatingDirectoryLocation());
        }
        
        if(streamAfterFinish){
            proc.waitFor();
        }else{
            proc.waitFor(3, TimeUnit.SECONDS);
        }

        int read = -1;
        while (proc.getInputStream().available() > 0) {
            System.out.write(proc.getInputStream().read());
        }
        while (proc.getErrorStream().available() > 0) {
            System.out.write(proc.getErrorStream().read());
        }
        while ((read = proc.getInputStream().read()) != -1) {
            System.out.write(read);
        }
        while ((read = proc.getErrorStream().read()) != -1) {
            System.out.write(read);
        }

        //destro proc
        proc.destroy();

        //rest 1 sec
        Thread.sleep(1000);

        //kill it agiain - using bazooka gun :)
        if (proc.isAlive()) {
            proc.destroyForcibly();
        }

        //rest 1 sec
        System.gc();
        Thread.sleep(1000);
    }

    /**
     * ONLY FOR UNIX SYSTEMS
     *
     * @param listInitialLines
     * @param listFinalLines
     * @throws Exception
     */
    private static void readAndEditCommandQueryOutlineOnUnix(boolean listInitialLines, boolean listFinalLines) throws Exception {

        File smartsql_script = new File(buildVars.getAppBundleLocation(), "smartmysql.sh");
        ArrayList<String> commandQueryLines = new ArrayList(Files.readAllLines(smartsql_script.toPath(),
                Charset.forName("UTF-8")));
        int commandQueryLength = commandQueryLines.size();

        if (listInitialLines) {
            for (String s : commandQueryLines) {
                System.out.println(s);
            }
        }

        for (int pos = 0; pos < commandQueryLength; pos++) {
            String s = commandQueryLines.get(pos).trim();
            int equalPos = s.indexOf('=');

            if (equalPos > -1 && s.charAt(equalPos + 1) == '#' && s.substring(equalPos + 1).equals("#auto_import")) {
                commandQueryLines.remove(pos);
                String argChecker = s.substring(0, equalPos);
                if (argChecker.equals("VERSION")) {
                    commandQueryLines.add(pos, "VERSION=\"" + buildVars.getVersionNumber() + "\"");
                } else if (argChecker.equals("ARGUMENTS")) {
                    commandQueryLines.add(pos, "ARGUMENTS=\"" + buildVars.getAppArgs(true) + "\"");
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        for (String s : commandQueryLines) {
            sb.append(s);
            sb.append(System.lineSeparator());
        }
        commandQueryLines.clear();

        Files.write(smartsql_script.toPath(), sb.toString().getBytes("UTF-8"),
                StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

        if (listFinalLines) {
            System.out.println();
            System.out.println(sb.toString());
        }
        sb.delete(0, sb.length());
        sb = null;
        commandQueryLines = null;
        System.gc();
    }

    /**
     * ONLY FOR WINDOWS
     *
     * @param listLines whether to print the command query to the console
     *
     * @param lineNumbers The line number starting from top, that you want to
     * edit its line The numbering/enumeration of the lines are counting numbers
     * not whole numbers eg: 42 OR [40 - (minus the two lines of comment on
     * top)]
     *
     * @param replaceMents The replacement query in full to use in the line
     * number you specified eg: InfoBeforeFile=notice.txt
     *
     * @throws Exception
     */
    private static void readAndEditCommandQueryOutlineOnWindows(boolean listInitialLines,
            boolean listFinalLines, String[] lineNumbers, String[] replaceMents)
            throws Exception {
        File iss = new File(buildVars.getOperatingDirectoryLocation(), "package/windows/" + buildVars.getAppName(false)+".iss");

        ArrayList<String> commandQueryLines = new ArrayList(Files.readAllLines(iss.toPath(),
                Charset.forName("UTF-8")));
        if (listInitialLines) {
            int i = 0;
            for (String s : commandQueryLines) {
                System.out.println(String.valueOf(i) + ": " + s);
                i++;
            }
        }
        int minusNumber = 1;
        if (lineNumbers != null || replaceMents != null) {
            System.out.println();
            if (lineNumbers != null) {
                for (int pos = 0; pos < lineNumbers.length; pos++) {
                    Integer i = Integer.valueOf(lineNumbers[pos]) - minusNumber;
                    if (i < 0) {
                        continue;
                    }
                    System.out.println("REMOVED LINE : "
                            + commandQueryLines.remove(i.intValue()));
                    try {
                        commandQueryLines.add(i, replaceMents[pos]);
                    } catch (IndexOutOfBoundsException | NullPointerException ibe) {
                        minusNumber++;
                    }
                }

            } else if (replaceMents != null) {
                List<String> temp = new ArrayList(commandQueryLines);
                commandQueryLines.clear();
                for (String s : replaceMents) {
                    commandQueryLines.add(s);
                }

                for (int i = commandQueryLines.size() - 1; i < temp.size(); i++) {
                    commandQueryLines.add(temp.get(i));
                }
                temp.clear();
                temp = null;
            }

            StringBuilder sb = new StringBuilder();
            String innoScriptAppNamePlaceHolderText = "<app_name>";
            for (String s : commandQueryLines) {
                sb.append(s.replace(innoScriptAppNamePlaceHolderText, buildVars.getAppName(false)));
                sb.append(System.lineSeparator());
            }
            commandQueryLines.clear();

            Files.write(iss.toPath(), sb.toString().getBytes("UTF-8"),
                    StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

            if (listFinalLines) {
                System.out.println(sb.toString());
            }
            sb.delete(0, sb.length());
            sb = null;
            commandQueryLines = null;
            System.gc();
        }
        System.out.println();
    }

    private static void buildPlatformExecutable() throws Exception {
        if (PlatformUtil.isMac()) {
            buildMacExecutable();
        } else if (PlatformUtil.isUnix()) {
            buildUnixExecutable();
        } else if (PlatformUtil.isWindows()) {
            buildWindowsExecutable();
        }
    }

    /**
     * ORDER stripMSVCP120 stripMSVCR100 stripMSVCR120
     *
     * @return
     */
    private static boolean[] getStripableUndetectedDLLFilesLineFromISS() {
        boolean stripMSVCP120 = true,
                stripMSVCR120 = true,
                stripMSVCR100 = true;

        File javaJREBINDIR = new File(buildVars.getJavaHome(), "bin");
        String[] childFiles = javaJREBINDIR.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".dll");
            }
        });

        for (String s : childFiles) {
            if (s.charAt(0) == 'm' && s.startsWith("msvcp120")) {
                stripMSVCP120 = false;
            } else if (s.charAt(0) == 'm' && s.startsWith("msvcr120")) {
                stripMSVCR120 = false;
            } else if (s.charAt(0) == 'm' && s.startsWith("msvcr100")) {
                stripMSVCR100 = false;
            }
        }

        return new boolean[]{stripMSVCP120, stripMSVCR100, stripMSVCR120};
    }
    
    private static void prepareInnoScriptWithVariants() throws Exception {
        readAndEditCommandQueryOutlineOnWindows(false, true, new String[]{"4", "6", "7", "24", "36", "42" /*"22",*/},
                new String[]{
                    "AppId={{" + buildVars.getSmartSqlPackageName() + "}}",
                    "AppVersion=" + buildVars.getVersionNumber(),
                    "AppVerName=" + buildVars.getAppName(false) + " " + buildVars.getVersionNumber(),
                    //"AppComments='" + buildVars.getAppName(false) + "'",
                    //"DefaultGroupName=" + buildVars.getAppName(false),
                    "LicenseFile=" + buildVars.getLicenseText(),
                    buildVars.isIs32bit() ? "ArchitecturesInstallIn64BitMode=" : "ArchitecturesInstallIn64BitMode=x64 ia64",
                    "InfoBeforeFile="/* + buildVars.getNotice_file()*/
                });
        
        
//            readAndEditCommandQueryOutlineOnWindows(false, false, new String[]{"4", "5", "6", "7", "9", "15", /*"22",*/ "31", "32", "33", "35",
//                "42", "24", "36", "27", "52", "53", "54", "60", "61", "64", "65", "66", "69","73", 
//                /* we are removing lines 218 to 228 */
//                "218", "219", "220", "221", "222", "223", "224", "225", "226", "227", "228"},
//                    new String[]{
//                        "AppId={{" + buildVars.getSmartSqlPackageName() + "}}",
//                        "AppName=" + buildVars.getAppName(false),
//                        "AppVersion=" + buildVars.getVersionNumber(),
//                        "AppVerName=" + buildVars.getAppName(false) + " " + buildVars.getVersionNumber(),
//                        "AppComments='" + buildVars.getAppName(false) + "'",
//                        "DefaultDirName={pf}\\" + buildVars.getAppName(false),
//                        //"DefaultGroupName=" + buildVars.getAppName(false),
//                        "SetupIconFile=" + buildVars.getAppName(false) +"\\" + buildVars.getAppName(false) +".ico",
//                        "UninstallDisplayIcon={app}\\" + buildVars.getAppName(false) +".ico",
//                        "UninstallDisplayName=" + buildVars.getAppName(false),
//                        "WizardSmallImageFile=" + buildVars.getAppName(false)+"-setup-icon.bmp",
//                        "InfoBeforeFile="/* + buildVars.getNotice_file()*/,
//                        "LicenseFile=" + buildVars.getLicenseText(),
//                        buildVars.isIs32bit() ? "ArchitecturesInstallIn64BitMode=" : "ArchitecturesInstallIn64BitMode=x64 ia64",
//                        "OutputBaseFilename=" + buildVars.getAppName(false),
//                        "Source: \"" + buildVars.getAppName(false) + "\\" + buildVars.getAppName(true)+ "\"; DestDir: \"{app}\"; Flags: ignoreversion",
//                        "Source: \"" + buildVars.getAppName(false) + "\\*\"; DestDir: \"{app}\"; Flags: ignoreversion recursesubdirs createallsubdirs",
//                        ";Source: \"" + buildVars.getAppName(false) + "\\runtime\\bin\\*.dll\"; DestDir: \"{app}\"; Flags: onlyifdoesntexist",
//                        "Name: \"{group}\\" + buildVars.getAppName(false) + "\"; Filename: \"{app}\\" + buildVars.getAppName(true) + "\"; IconFilename: \"{app}\\" + buildVars.getAppName(false) + ".ico\"; Check: returnTrue()",
//                        "Name: \"{commondesktop}\\" + buildVars.getAppName(false) + "\"; Filename: \"{app}\\" + buildVars.getAppName(true) + "\";  IconFilename: \"{app}\\" + buildVars.getAppName(false) + ".ico\"; Check: returnTrue()",
//                        "Filename: \"{app}\\" + buildVars.getAppName(true) + "\"; Parameters: \"-Xappcds:generatecache\"; Check: returnFalse()",
//                        "Filename: \"{app}\\" + buildVars.getAppName(true) + "\"; Description: \"{cm:LaunchProgram," + buildVars.getAppName(false) + "}\"; Flags: nowait postinstall skipifsilent; Check: returnTrue()",
//                        "Filename: \"{app}\\" + buildVars.getAppName(true) + "\"; Parameters: \"-install -svcName \"\"" + buildVars.getAppName(false) + "\"\" -svcDesc \"\"Build and Manage your Mysql Server\"\" -mainExe \"\"" + buildVars.getAppName(true) + "\"\"  \"; Check: returnFalse()",
//                        "Filename: \"{app}\\" + buildVars.getAppName(true) + " \"; Parameters: \"-uninstall -svcName " + buildVars.getAppName(false) + " -stopOnUninstall\"; Check: returnFalse()",
//                        "Type: files; Name: \"{commondesktop}\\" + buildVars.getAppName(true) + ".lnk\""
//                    });

//            if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_WINDOWS_NON_JRE_VERSION)) {
//                String commandWithoutJRE = buildVars.getJavaPackagerStringDirectory()
//                        + " -deploy -native "+ buildVars.getExtension() +" -outdir "
//                        + buildVars.getOutputDirectory().getPath()
//                        + " -outfile "
//                        + buildVars.getAppName(false)
//                        + " -srcdir appBundle"
//                        + " -appclass "
//                        + buildVars.getSmartSqlPackageName()
//                        + " -name SmartMySQL -title 'Smart-Mysql'"
//                        + " -description \"SmartMySQL - smarter Mysql server operations\""
//                        + " -BappVersion="
//                        + buildVars.getVersionNumber()
//                        + " -Bruntime="
//                        + " -BshortcutHint=true -BsystemWide=true"
//                        + " -Bicon=windows/SmartMysql.ico"
//                        + " -BjvmProperties=name=SmartMySQL"
//                        + " -Bidentifier="
//                        + buildVars.getSmartSqlPackageName()
//                        + " -BmainJar=" + buildVars.getSmart_sql_installer_jar().getName()
//                        + buildVars.getAppArgs(false)
//                        + " "
//                        + ((buildVars.getJvmOptions() == null) ? "" : buildVars.getJvmOptions())
//                        + " -v";
//
//                System.out.println();
//                System.out.println("------------------------------------");
//                System.out.println("BUNDLING NON-JRE VERSION");
//                System.out.println("------------------------------------");
//                System.out.println();
//                //we no longer build non jre version
//                //System.out.println(commandWithoutJRE);
//                //executeScript(commandWithoutJRE);
//            }
            //boolean[] bools = getStripableUndetectedDLLFilesLineFromISS();
//            readAndEditCommandQueryOutlineOnWindows(true, true, new String[]{"27", "42",
//                //bools[0] ? "51" : "-1" ,
//                //bools[1] ? "52" : "-1" ,
//                //bools[2] ? "53" : "-1",
//                "218", "219", "220", "221", "222", "223", "224", "225", "226", "227", "228"},
//                    new String[]{
//                        "OutputBaseFilename=" + buildVars.getAppName(false) /*+ "_jre"*/,
//                        "InfoBeforeFile="});
        
    }

    private static void buildWindowsExecutable() throws Exception {

        if (buildVars.getExtension().equalsIgnoreCase("exe")) {
            prepareInnoScriptWithVariants();
            if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_WINDOWS_JRE_VERSION)) {
                String commandWithJRE = buildVars.getJavaPackagerStringDirectory()
                        + " -deploy -native " + buildVars.getExtension() 
                        + " -outdir " + buildVars.getOutputDirectory().getPath()
                        + " -outfile " + buildVars.getAppName(false) /*+ "_jre"*/
                        + " -srcdir appBundle"
                        + " -appclass " + buildVars.getSmartSqlPackageName()
                        + " -name " + buildVars.getAppName(false)                        
                        + " -Bruntime=\"" + buildVars.getJavaHome()
                        + "\" -BshortcutHint=true -BsystemWide=true"
                        + " -BjvmProperties=name=" +buildVars.getAppName(false)
                        + " -Bidentifier=" + buildVars.getSmartSqlPackageName()
                        + " -BmainJar=" + buildVars.getSmart_sql_ui_jar().getName()
                        + buildVars.getAppArgs(false)
                        + " "
                        + buildVars.getJvmOptions()
                        + " -v";

                System.out.println();
                System.out.println("------------------------------------");
                System.out.println("BUNDLING JRE VERSION");
                System.out.println("------------------------------------");
                System.out.println(commandWithJRE);
                executeScript(commandWithJRE);
                System.out.println();
            }
        }
    }

    private static void buildMacExecutable() throws Exception {
        /**
         * With mac we do not care about extension.
         */
        if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_MAC_PKG_DMG_VERSION)) {
            System.out.println();
            System.out.println("------------------------------------");
            System.out.println("BUILDING PKG_DMG MAC VERSION");
            System.out.println("------------------------------------");
// /Library/Java/JavaVirtualMachines/jdk1.8.0_201.jdk/Contents/Home/bin/javapackager
//            String[] commandWithJRE = new String[]{

            List<String> srcFiles = new ArrayList<>();
            collectSrcFiles(buildVars.getAppBundleLocation().getAbsolutePath(), "", srcFiles);
            
            List<String> l = new ArrayList<>();
            l.add(buildVars.getJavaPackagerStringDirectory());
            l.add("-deploy");
//            l.add("-allpermissions");
//            l.add("-nosign");
            l.add("-native");
            l.add(buildVars.getExtension());
            l.add("-outdir");
            l.add(buildVars.getOutputDirectory().getPath());
            l.add("-outfile");
            l.add(buildVars.getAppName(false));
            l.add("-srcdir");
            l.add(buildVars.getAppBundleLocation().getAbsolutePath());
            l.addAll(srcFiles);
            l.add("-appclass");
            l.add(buildVars.getSmartSqlPackageName());
            l.add("-name");
            l.add(buildVars.getAppName(false));
            l.add("-title");
            l.add(buildVars.getAppName(false));
            l.add("-description");
            l.add(buildVars.getAppName(false) + " - smarter Mysql server operations");
            l.add("-BappVersion=" + buildVars.getVersionNumber());
            l.add("-Bruntime=/Library/Java/JavaVirtualMachines/jdk1.8.0_201.jdk/");
            l.add("-BshortcutHint=true");
            l.add("-BsystemWide=true");
            l.add("-Bicon=mac/SmartMysql-volume.icns");
            l.add("-BjvmProperties=name=" + buildVars.getAppName(false));
            l.add("-Bidentifier=" + buildVars.getSmartSqlPackageName());
            l.add("-BmainJar=" + buildVars.getSmart_sql_ui_jar().getName());
            l.add("-Bmac.category=Productivity");
            l.add("-Bmac.CFBundleName=SmartMySQL");
            l.add("-v");
            
            String[] commandWithJRE = l.toArray(new String[0]);
            
            String[] params = (buildVars.getJvmOptions() != null) ? buildVars.getJvmOptions().split(" ") : null;
            String[] args = buildVars.getAppArgs(false).trim().split(" ");

            if (params != null) {
                String[] a = new String[commandWithJRE.length + params.length + args.length];
                int i = 0;
                for (; i < commandWithJRE.length; i++) {
                    a[i] = commandWithJRE[i];
                    if (i + 1 == commandWithJRE.length) {
                        break;
                    }
                }

                //for arguments
                for (String s : args) {
                    a[i] = s;
                    i++;
                }

                //for system args
                for (String s : params) {
                    a[i] = s;
                    i++;
                }

                a[i] = commandWithJRE[commandWithJRE.length - 1];

                for (String s : a) {
                    System.out.println(s);
                }

                commandWithJRE = a;
            }

            System.out.println();
            System.out.println("------------------------------------");
            System.out.println("BUNDLING NON-JRE VERSION");
            System.out.println("------------------------------------");
            System.out.println();
            executeScript(commandWithJRE);
            System.out.println();

        }
    }
    
    private static void collectSrcFiles(String dir, String parent, List<String> list) {        
        
        for (File f : new File(dir).listFiles()) {
            if (f.isDirectory()) {
                collectSrcFiles(f.getAbsolutePath(), parent + f.getName() + File.separatorChar, list);
            } else {
                list.add("-srcfiles");
                list.add(parent + f.getName());
            }
        }
    }

    private static void buildUnixExecutable() throws Exception {
        if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_LINUX_SH_VERSION)) {

            if (buildVars.getExtension().equalsIgnoreCase("sh")) {
                System.out.println();
                System.out.println("------------------------------------");
                System.out.println("BUILDING SHEBANG LINUX VERSION");
                System.out.println("------------------------------------");

                readAndEditCommandQueryOutlineOnUnix(true, true);
                //create & run script
                executeScript("./makeself.sh",
                        "appBundle",
                        new File(buildVars.getOutputDirectory(), buildVars.getAppName(true)).getPath(),//"directory/"SmartMySQL.sh"
                        "Mysql Database Manager & Profiler",
                        "./smartmysql.sh",
                        buildVars.getVersionNumber());
            }
        }

    }

    private static void afterMathBundling() throws Exception {
        if (PlatformUtil.isWindows() || PlatformUtil.isMac()) {
            File bundleLocation = new File(buildVars.getOutputDirectory(), "bundles");
            if (bundleLocation.exists()) {
                for (File ex : bundleLocation.listFiles()) {
                    copy(ex, new File(buildVars.getOutputDirectory(), ex.getName()));
                    ex.delete();
                }
            }
        } else if (PlatformUtil.isUnix()) {
//            //copy the final .sh file to our out dir
//            copy(new File(operatingDirectory, buildVars.getAppName(true)),
//                new File(outputDirectory, finalAppName));
        }

        for (File f : buildVars.getOutputDirectory().listFiles()) {
            boolean extensionCheck = fileExtensionChecker(f);
            if (!extensionCheck) {
                delDirectory(f, true);
            } else if (Math.abs(System.currentTimeMillis() - Files.getLastModifiedTime(f.toPath()).toMillis())
                    > 5 * 60000) { // or 5min
                delDirectory(f, true);
            } else if (extensionCheck) {
                String name = f.getName();
                int post = name.indexOf(PlatformUtil.isMac() ? '-' : '.');
                if (post > -1) {
                    name = name.substring(0, post);
                }
                name = name + (buildVars.isIs32bit() ? "_32" : "_64") + "." + buildVars.getExtension();
                f.renameTo(new File(f.getParentFile(), name));
            }
        }

    }

    private static boolean fileExtensionChecker(File f) {
        String name = f.getName();

        //or it can be done this way
        //int p = name.lastIndexOf('.');
        //return p > -1 && name.substring(p+1).equalsIgnoreCase(buildVars.getExtension());
        ////////// any way is good ///
        return name.endsWith(buildVars.getExtension());
    }

    private static String getJarHash(File f)
            throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        if (f == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        try (FileInputStream smart_ui_stream = new FileInputStream(f);) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] dataBytes = new byte[1024];

            int nread;
            while ((nread = smart_ui_stream.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }

            byte[] mdbytes = md.digest();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff)
                        + 0x100, 16).substring(1));
            }
        }
        return sb.toString();
    }

    private static void createRuntimeDocument() throws Exception {
        //here we create RuntimeDocumentWrapper
        runtimeDocumentWrapper = getOrRetrieveRuntimeDocument();
        runtimeDocumentWrapper.setAppHash(getJarHash(buildVars.getSmart_sql_ui_jar()));
        runtimeDocumentWrapper.setAppSize(buildVars.getSmart_sql_ui_jar().length());
        runtimeDocumentWrapper.setVersion(buildVars.getVersionNumber());
        runtimeDocumentWrapper.setNewUpdateNotice(buildVars.getUpdateNotice_information());
        runtimeDocumentWrapper.setBannerText(buildVars.getBannerText());
        runtimeDocumentWrapper.setYoutubeText(buildVars.getYoutubeText());
        runtimeDocumentWrapper.setOnlineServer(buildVars.getOnlineServer());
        runtimeDocumentWrapper.setAppEncryptionKey(buildVars.getAppEncryptionKey());
        Map<String, String> dependencyHashes = null;//runtimeDocumentWrapper.getDependencyHashes(),
        Map<String, String> dependencyDirectory = new HashMap(60);
        if (dependencyHashes == null) {
            dependencyHashes = new HashMap(60);
            runtimeDocumentWrapper.setDependencyHashes(dependencyHashes);
        }
        runtimeDocumentWrapper.setDependencyDirectory(dependencyDirectory);
        for (File depen : buildVars.getSmart_sql_libs_folder().listFiles()) {
            String dependName = getNameFromFile(depen, true);
            dependencyHashes.put(dependName, getJarHash(depen));
            dependencyDirectory.put(dependName, depen.toURI().toURL().toString()); // for local paths
        }

        //put ssj.jar into dependency map
        dependencyHashes.put(getNameFromFile(buildVars.getSmart_sql_ssj_jar(), false),
                getJarHash(buildVars.getSmart_sql_ssj_jar()));

        //put updater.jar into dependency map
        dependencyHashes.put(getNameFromFile(buildVars.getSmart_sql_updater_jar(), false),
                getJarHash(buildVars.getSmart_sql_updater_jar()));

        runtimeDocumentWrapper.forceNextConversion();
        saveRuntimeDocument();
    }

    public static void saveRuntimeDocument() throws Exception {
        Files.write(buildVars.getSmart_sql_info_config_file().toPath(),
                getOrRetrieveRuntimeDocument().toString().getBytes("UTF-8"),
                StandardOpenOption.WRITE, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static RuntimeDocumentWrapper getOrRetrieveRuntimeDocument() {
        try {
            if (runtimeDocumentWrapper == null) {
                if (buildVars == null) {
                    restorePropertiesFromCache(null);
                }
                runtimeDocumentWrapper = RuntimeDocumentWrapper.getRuntimeDocumentWrapperInstance(new Gson(), new String(Files.readAllBytes(
                        buildVars.getSmart_sql_info_config_file().toPath()), "UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            runtimeDocumentWrapper = new RuntimeDocumentWrapper(new Gson());
        }

        return runtimeDocumentWrapper;
    }

    public static final String getNameFromFile(File depen, boolean deduce) {
        //replace jar extensions
        String name = depen.getName().replace(".jar", "");

        return deduce ? deduceNameEntity(name) : name;
    }

    public static final String deduceNameEntity(String name) {
        //strip the last dash to get rid of versioning
        int index = name.lastIndexOf('-');
        if (index > 0) {
            name = name.substring(0, index);
        }

        return name;
    }

    public static void main(String[] args) throws Exception {
        try {
            //we try restoring
            restorePropertiesFromCache(args);

            if (buildVars == null || buildVars.isFreshBuild()) {

                buildVars = new BuildVars(args);
                buildStatus = new BuildStatus();
            }

            if (buildVars.isForcingLevel()) {
                buildStatus.setCurrentLevel(buildVars.getForcedLevel());
            }

            if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_PLATFORM_COPY)) {
                platformCopy();
                writeStatus_Vars();
            }

            //here we try building the executable
            if (buildVars.isExecutebuild() && buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_BUILD_EXECUTION_BRANCH)) {

                if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_CREATE_BINARY_EXECUTABLES)) {
                    buildPlatformExecutable();
                }

                if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_AFTERMATH_BUNDLING)) {
                    afterMathBundling();
                }
            }

            createRuntimeDocument();
            writeStatus_Vars();

            if (buildVars.isExecutebuild() && buildVars.shouldSignExecutableWithCertificate()) {
                if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_SIGN_WINDOWS_EXECUTABLE)) {
                    digitallySignBinaryFile();
                }

                if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_VERIFY_WINDOWS_EXECUTABLE_STATE)) {
                    checkBinaryValidityState();
                }
            }
            
            // here we execute the update build
            if (buildVars.isExecuteUpdate() && buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_SERVER_UPLOAD_BRANCH)) {
                //update code here
                constructAndUpload();
                //we save again since there are [might be] changes
                saveRuntimeDocument();
                writeStatus_Vars();
            }
            
            if (buildVars.isExecutebuild() && buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_FINAL_PATH_DISPLAYorOPENER)) {
                Desktop.getDesktop().open(buildVars.getOutputDirectory());
            }

            //doing little clean up
            System.out.println("-----------------------------------");
            System.out.println("JUNK CLEAN UP PROCESSS");
            System.out.println("-----------------------------------");

            final String fxBundlerString = "fxbundler";
            for (File tempFiles : buildVars.getOperatingDirectoryLocation().getParentFile().listFiles()) {
                String fileName = tempFiles.getName();

                if ((fileName.charAt(0) == 'f' && fileName.startsWith(fxBundlerString))
                        || (fileName.charAt(0) == 's' && fileName.startsWith(BuildEntry.class.getPackage().getName())
                        && !Files.isSameFile(buildVars.getOperatingDirectoryLocation().toPath(), tempFiles.toPath()))) {
                    try {
                        delDirectory(tempFiles, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            buildStatus.finalizeOperationStatus();

        } finally {
            //persist or serialize or requreables
            writeStatus_Vars();
            System.out.println("CURRENT LEVEL IS " + String.valueOf(buildStatus.getCurrentLevel()));

            if (/*!buildVars.isForcingLevel() && */buildStatus.getCurrentLevel() == BuildStatus.METHOD_INVOCATION_POINTER_COMPLETION) {
            }
            System.gc();
            System.gc();
            System.gc();
        }
    }
    
    private static void digitallySignBinaryFile() {
        if (PlatformUtil.isWindows()) {
            if (buildVars.getSigntool_pathfile() == null || !new File(buildVars.getSigntool_pathfile()).exists()) {
                new RuntimeException("CODE SIGNING ERROR. \n To able to sign binaries on this system you need to have the signtool package, from microsoft. "
                        + "\n You can get this from the Microsoft developer SDK. ").printStackTrace();
            } else {
                boolean signProcessSuc = false;
                int maxFailTries = 3;
                while (!signProcessSuc && maxFailTries > 0) {
                    try {
                        Thread.sleep(1000);
                        executeScript(true, buildVars.getSigningCodeScript());
                        signProcessSuc = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        --maxFailTries;
                    }
                }
            }
        }
    }
    
    private static void checkBinaryValidityState() {
        try {
            if (PlatformUtil.isWindows()) {
                
            }
        } catch (Exception e) { e.printStackTrace(); }
    }

    private static BuildConnectionEntity getCurrentBuildConnectionEntity() {
        GoogleCloudBuildConnectionEntity connectionEntity = new GoogleCloudBuildConnectionEntity(getOrRetrieveRuntimeDocument(), buildVars, buildStatus);
        connectionEntity.setProjectName(buildVars.getModule_project());
        return connectionEntity;
    }

    private static void constructAndUpload() {
        if (!buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_SERVER_UPLOAD_CONSTRUCT_UPLOAD)) {
            return;
        }
        System.out.println();
        System.out.println("-----------------------------------");
        System.out.println("UPLOAD SESSIONS INITIATING");
        System.out.println("-----------------------------------");

        BuildConnectionEntity buildConnectionEntity = getCurrentBuildConnectionEntity();
        buildConnectionEntity.initialize();
        buildConnectionEntity.openConnection();

        if (buildConnectionEntity.isConnectionCreationSuccessfull()) {
            System.out.println("CONNECTION CREATED");

            if (buildVars.getArgVarags() != null) {
                for (String v : buildVars.getArgVarags()) {
                    if (v.equals(BuildEntry.BUILD_VARAGS_UPDATE)) {
                        buildConnectionEntity.beginUploadFiles();

                    } else if (v.equals(BuildEntry.BUILD_VARAGS_BINARIES)) {
                        buildConnectionEntity.beginUploadBinaries();
                    }
                }
            }
        }
    }

    public static boolean hasLastBuildFinishedSuccessfully() {
        return buildStatus.hasFinished();
    }

}
