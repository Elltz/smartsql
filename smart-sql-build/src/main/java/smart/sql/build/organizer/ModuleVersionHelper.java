/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Executors;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ModuleVersionHelper {

    private ArrayList<String> versions_array;
    private String current_version;
    private byte buildtype;
    
    private transient GoogleCloudBuildConnectionEntity gogBuildConnectionEntity;
    private transient Connection onlineConnection;

    public ModuleVersionHelper(byte buildtype, ArrayList<String> versions_array, String current_version,
            GoogleCloudBuildConnectionEntity gogBuildConnectionEntity) {
        this.versions_array = versions_array;
        this.current_version = current_version;
        this.buildtype = buildtype;
        this.gogBuildConnectionEntity = gogBuildConnectionEntity;
    }

    public ModuleVersionHelper(byte buildtype, GoogleCloudBuildConnectionEntity gogBuildConnectionEntity) {
        this(buildtype, new ArrayList<>(5), "1.0", gogBuildConnectionEntity);
    }

    public String convertToJsonString(Gson gson) {
        return gson.toJson(this);
    }
    
    private int compareVersions(String version0, String version1) {
        String[] v0 = version0.split("\\.");
        String[] v1 = version1.split("\\.");
        
        int maj0 = Integer.valueOf(v0[0]);
        int min0 = Integer.valueOf(v0[1]);
        
        int maj1 = Integer.valueOf(v1[0]);
        int min1 = Integer.valueOf(v1[1]);
        
        if (maj0 != maj1) {
            return maj0 - maj1;
        } else {
            return min0 - min1;
        }
    }

    public final String getHighestVersionAvailable() {
        String highestVersion = current_version;
        for (int i = 0; i < versions_array.size(); i++) {
            if (compareVersions(versions_array.get(i), highestVersion) > 0) {
                highestVersion = versions_array.get(i);
            }
        }

        //if nothing happens returns the current;
        return highestVersion;
    }

    public void addNewVersion(String version) {
        boolean alreadyExist = false;
        for (int i = 0; i < versions_array.size(); i++) {
            if (compareVersions(versions_array.get(i), version) == 0) {
                alreadyExist = true;
                break;
            }
        }

        if (!alreadyExist) {
            versions_array.add(version);
        }
    }
    
    public void removeVersion(double version){
        String vers = String.valueOf(version);
        removeVersion(vers);
    }
    
    public void removeVersion(String vers){
        versions_array.remove(vers);        
        if(current_version.equals(vers)){
            current_version = getHighestVersionAvailable();
        }
    }

    public void setCurrentVersion(String current_version) {
        this.current_version = current_version;
        addNewVersion(current_version);
    }

    /**
     * This makes sure that the Google cloud data aligns with the database server data
     */
    public void syncCurrentState() {
        try {
            connectToMysqlDb();
            updateDatabaseSwState();
            updateCurrentVersion();
            disConnectionFromMysqlDb();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * This updates the server data with the local data, making the server data
     * have the same data as the local data
     *
     * @throws Exception
     */
    public boolean updateCurrentState() {
        try {
            updateGoogleCloud();
            updateDatabaseServer();
            return true;
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    private void updateGoogleCloud() {
        gogBuildConnectionEntity.updateVersion(convertToJsonString(new Gson()), buildtype);
    }

    private void updateDatabaseServer() throws Throwable {
        connectToMysqlDb();
        updateDatabaseCurrentVersion();
        disConnectionFromMysqlDb();
    }
    
    public void updateDatabaseCurrentVersion() throws SQLException {
        updateDatabaseVersionState(current_version);
    }
    /*
    public void updateDatabaseVersionState(String version, String downloadLink, char active, String releaseNotes) throws SQLException {
        
    }
    */
    
    /**
     * BUILDTYPE
     * SOFTWARE
     * @return 
     */
    private String getSQLConstWhereClause(){
        StringBuilder sb = new StringBuilder("`software` = '")
                .append(gogBuildConnectionEntity.getProjectBucketName())
                .append("' AND `buildtype` = '");
        
        if(buildtype == (byte)1){
            sb.append("PRODUCTION'");
        }else{
            sb.append("QA'");
        }
        
        return sb.toString();
    }
    
    public void updateDatabaseVersionState(String version) throws SQLException {
        Statement state = onlineConnection.createStatement();
        int updateres = state.executeUpdate("UPDATE `sw_version` SET `active` = 'N' WHERE "
                + getSQLConstWhereClause()
                + ";");
        
        ResultSet rs = state.executeQuery("SELECT EXISTS(SELECT * FROM `sw_version` WHERE `VNo` = '" 
                + version + "' AND "
                + getSQLConstWhereClause()
                + ") AS 'exists';");
        boolean exists = (rs != null && rs.next() && rs.getInt("exists") == 1);
        
        StringBuilder sb;
        if (exists) {
            sb = new StringBuilder("UPDATE `sw_version` SET `active` = 'Y' WHERE `VNo` = '")
                    .append(version)
                    .append("' AND ")
                    .append(getSQLConstWhereClause())
                    .append(";");
        } else {
            sb = new StringBuilder("INSERT INTO `sw_version` ")
                    .append("(`VNo`,`active`,`software`,`buildtype`) VALUES ('")
                    .append(version).append("','")
                    .append("Y").append("','")
                    .append(gogBuildConnectionEntity.getProjectBucketName()).append("','");
            if (buildtype == (byte) 1) {
                sb.append("PRODUCTION'");
            } else {
                sb.append("QA'");
            }
            
            sb.append(");");
        }
        
        updateres = state.executeUpdate(sb.toString());
        System.out.println(updateres);
        state.close();    
        System.gc();System.gc();
    }
    
    public void updateDatabaseSwState() throws SQLException {
        StringBuilder sb = new StringBuilder("SELECT * FROM `sw_version` WHERE ")
                .append(getSQLConstWhereClause()).append(";");
        Statement state = onlineConnection.createStatement();        
        //retrieve all rows for this build module & build type
        ResultSet rs = state.executeQuery(sb.toString());
        
        ArrayList<String> versionsToDelete = new ArrayList(20);
        ArrayList<String> clonedVersionsArray = new ArrayList(versions_array);
        
        while(rs.next()){
            String rsVers = rs.getString("VNo");
            if(!clonedVersionsArray.remove(rsVers.trim())){
                versionsToDelete.add(rsVers);
            }
        }
        
        state.close();
        
        if(versionsToDelete.size() > 0) {
            clearRedundantRows(versionsToDelete);
        }
    }
    
    public void updateCurrentVersion() throws SQLException {
        
        Statement state = onlineConnection.createStatement();        
        ResultSet rs = state.executeQuery("SELECT  concat( max(CONVERT(SUBSTRING_INDEX(VNo,'.',1),UNSIGNED INTEGER)),'.', max(CONVERT(SUBSTRING_INDEX(VNo,'.',-1),UNSIGNED INTEGER))) Vno FROM SmartMySQL.sw_version;");
        
        if(rs != null && rs.next()) {
            setCurrentVersion(rs.getString(1));
        }
        
        state.close();
    }
    
    private void clearRedundantRows(ArrayList<String> versionsToDelete) throws SQLException {
        Statement state = onlineConnection.createStatement();
        StringBuilder sb = new StringBuilder("DELETE FROM `sw_version` WHERE `VNo` IN (");
        for (String s : versionsToDelete) {
            sb.append("'").append(s).append("',");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append(") AND ")
                .append(getSQLConstWhereClause())
                .append(";");
        state.execute(sb.toString());
        state.close();
    }

    public void connectToMysqlDb() throws Throwable {
        if (onlineConnection != null && !onlineConnection.isClosed()
                && onlineConnection.isValid(100)) {
            return;
        }

        Properties props = new Properties();
//        props.put("user", "root");
//        props.put("password", "root");
        props.put("user", "topdbs_customer");
        props.put("password", "top@123");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        onlineConnection = DriverManager.getConnection(getDatabaseConnUrl(), props);
        Statement state = onlineConnection.createStatement();
//        state.execute("use `test`;"
//                + "set wait_timeout=100;");
        state.execute("use SmartMySQL;"
                + "SET wait_timeout = 100;");
        state.close();
    }
    
    private String getDatabaseConnUrl(){
        //return "jdbc:mysql://localhost:3306?allowMultiQueries=true";
        return "jdbc:mysql://www.cloversql.com:3306?allowMultiQueries=true";
    }

    public void disConnectionFromMysqlDb() {
        try {
            if (onlineConnection != null && !onlineConnection.isClosed()
                    && onlineConnection.isValid(100)) {
                if (!onlineConnection.getAutoCommit()) {
                    onlineConnection.commit();
                }
                onlineConnection.close();
                onlineConnection.abort(Executors.newFixedThreadPool(1));
            }
            onlineConnection = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setGogBuildConnectionEntity(GoogleCloudBuildConnectionEntity gogBuildConnectionEntity) {
        this.gogBuildConnectionEntity = gogBuildConnectionEntity;
    }

    public void setBuildtype(byte buildtype) {
        this.buildtype = buildtype;
    }    

    public ArrayList<String> getVersions_array() {
        return versions_array;
    }

    public String getCurrent_version() {
        return current_version;
    }
}
