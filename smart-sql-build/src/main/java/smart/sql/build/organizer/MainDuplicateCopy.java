/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import java.awt.Desktop;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * 
 * No longer used, but reserved for reference
 */
public class MainDuplicateCopy {

    private static String versionNumber;
    private static File outputDirectory; //where the executables will be stored at
    private static File inputDirectory; //the location of where to locate all important directory
    private static File operatingDirectory;//temporary directory created to create files
    private static File updateVersionInfoFile;
    private static File osDirectory;
    private static File java_path;
    private static String licenseText;//the license text
    private static String homeBase;
    private static String executableDetails;
    private static String finalAppName;
    private static String jvmOptions;
    public static final String moduleName = MainDuplicateCopy.class.getPackage().getName();
    private static boolean uploadToServer = false;
    private static byte osNum = -1;
    private static boolean is32bit = false;
    private static boolean oneTimeInstaller = false;
    private static int maxExceptionCheckStat = 0;
    private static String[] buildVarags;
    private static final String BUILD_VARAGS_ALL = "ALL";
    private static final String BUILD_VARAGS_BINARIES = "BINARIES";
    private static final String BUILD_VARAGS_CONFIG = "CONFIG";
    private static final String BUILD_VARAGS_LIBS = "LIBS";
    private static final String BUILD_VARAGS_JAR__CONFIG = "JAR&CONFIG";

    /**
     * ArrayList that i will use in upload positions 0 = libs /dir 1 =
     * smart-sql-ui.jar 2 = ssj.jar 3 = updater.jar 4 = app_info.config 5 =
     * executables /dir
     */
    private static ArrayList<File> requieredFilesToBeUploaded;

    public static void main(String[] args) {

        versionNumber = args[0];
        homeBase = args[1];        
        is32bit = !System.getProperty("os.arch").contains("64");
        buildVarags = args[4].split(",");
        
        String[] name_in_twos = args[3].split("\\.");
        System.out.println(name_in_twos[0]);
        System.out.println(name_in_twos[1]);
        finalAppName = name_in_twos[0] + (is32bit ? "-32" : "-64") + "." + name_in_twos[1];
        System.out.println(finalAppName);
        jvmOptions = args[5];
        
        try {
            inputDirectory = new File(args[2]);
            if (!inputDirectory.exists()) {
                throw new RuntimeException("Dont worry about Exception");
            }
        } catch (Exception ne) {
            inputDirectory = new File(new File(homeBase).getParent(), "Installers");
        }
        updateVersionInfoFile = new File(inputDirectory,"new_update_version_tip.txt");
        detailsWriter((byte) 0, "");

        validateOutputDirectory();
        
        requieredFilesToBeUploaded = new ArrayList(6) {
                @Override
                public void add(int index, Object element) {
                    if ((size() - 1) >= index && get(index) == null) {
                        remove(index);
                    }
                    super.add(index, element);
                }

            };
            for (int i = 0; i < 6; i++) {
                if (i == 5) {
                    requieredFilesToBeUploaded.add(outputDirectory);
                } else {
                    requieredFilesToBeUploaded.add(null);
                }
            }
        
        if (Boolean.valueOf(System.getProperty("buid.execute"))) {

            try {
                if (outputDirectory.exists()) {
                    delDirectory(outputDirectory);
                } else {
                    outputDirectory.mkdir();
                }
            } catch (Exception ne) {
                ne.printStackTrace();
                throw new TopDbsException("Can not create Output Directory location");
            }

            try {
                buildExcutable();
                detailsWriter((byte) 1, executableDetails);
            } catch (Exception ex) {
                throw new TopDbsException(ex);
            }
            
            try { bundleAndWriteNeccessity(); } catch (Exception ex) {}
        }

        if (Boolean.valueOf(System.getProperty("build.production"))) {
            try {                
                preUploadChecks();
                uploadToFTPServer();
                detailsWriter((byte) 2, executableDetails);
            } catch (Exception ex) {
                throw new TopDbsException(ex);
            } finally {
                disconnectFTP(true);
            }
        }

        if (requieredFilesToBeUploaded != null) {
            System.out.println("-------------------------");
            System.out.println("JUST SOME LISTINGS");
            System.out.println("-------------------------");
            for (File f : requieredFilesToBeUploaded) {
                System.out.println(f);
            }
        }
        System.out.println(executableDetails);

        if (outputDirectory != null) {
            System.out.println("----out put file will open shortly---");

            try {
                Desktop.getDesktop().open(outputDirectory);
            } catch (Exception ex) {
                Logger.getLogger(MainDuplicateCopy.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println();
                System.out.println("Try openning the directory yourself "
                        + outputDirectory.getAbsolutePath());
            }
        }
    }
    
    private static void validateOutputDirectory() {
        if (outputDirectory == null) {
            outputDirectory = new File(System.getProperty("user.home") + File.separator
                    + "Desktop/finalBuild");
        }
    }

    private static void detailsWriter(byte who, String what) {
        StringBuilder sb = new StringBuilder(what);
        if (who == 1) {//general 
            sb.append("EXECUTABLE DETAILS FOR ");
            sb.append(System.getProperty("os.name").toUpperCase());
            sb.append(System.getProperty("line.separator"));
            sb.append("----------------------------------------------------------");
            sb.append(System.getProperty("line.separator"));

        } else if (who == 2) {//execution

            sb.append("-executable build type = ");
            sb.append(System.getProperty("os.arch"));
            sb.append(System.getProperty("line.separator"));
            sb.append("-executable contains jre = ");
            sb.append(System.getProperty("user.home") != null);
            sb.append(System.getProperty("line.separator"));

            sb.append(System.getProperty("line.separator"));
            sb.append(System.getProperty("line.separator"));
            sb.append(System.getProperty("line.separator"));

        } else if (who == 3) {//production

        }
        executableDetails = sb.toString();
    }

    private static byte getOs() {
        if (osNum == (byte) -1) {
            String os_name = System.getProperty("os.name").toLowerCase();
            if (os_name.startsWith("win")) {
                osNum = 0;
            } else if (os_name.startsWith("mac")) {
                osNum = 1;
            } else {
                osNum = 2;
            }
        }
        return osNum;
    }

    private static void delDirectory(File file) {
        while (file.listFiles() != null && file.listFiles().length != 0) {
            File temp = file.listFiles()[0];
            if (temp.isDirectory()) {
                delDirectory(temp);
                continue;
            }
            temp.delete();
        }
        if (file.exists()) {
            file.delete();
        }
    }

    private static void copy(File source, File target) throws Exception {
        System.out.println("source = " + source.getAbsolutePath());
        System.out.println("target = " + target.getAbsolutePath());
        System.out.println();
        if (source.isDirectory()) {
            File dir = new File(target, source.getName());
            dir.mkdirs();
            for (File f : source.listFiles()) {
                copy(f, new File(dir, f.getName()));
            }
        } else {
            Files.copy(source.toPath(), target.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private static String getJarHash(String parent, String fileName)
            throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        return getJarHash(new File(parent, fileName));
    }

    private static String getJarHash(File f)
            throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        StringBuilder sb = new StringBuilder();
        try (FileInputStream smart_ui_stream = new FileInputStream(f);) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] dataBytes = new byte[1024];

            int nread = 0;
            while ((nread = smart_ui_stream.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            };

            byte[] mdbytes = md.digest();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff)
                        + 0x100, 16).substring(1));
            }
        }
        return sb.toString();
    }

    private static void saveAppInfo(File filePath, String text) throws IOException {
        if (filePath.exists()) {
            filePath.delete();
        }
        Files.write(filePath.toPath(), text.getBytes("UTF-8"));

    }

    private static void executeScript(String... script) throws Exception {
        Process proc = null;
        if (script.length > 1) {
            proc = Runtime.getRuntime().exec(script, null,
                    operatingDirectory);
        } else {
            proc = Runtime.getRuntime().exec(script[0], null,
                    operatingDirectory);
        }
        proc.waitFor(2, TimeUnit.SECONDS);

        int read = -1;
        while (proc.getInputStream().available() > 0) {
            System.out.write(proc.getInputStream().read());
        }
        while (proc.getErrorStream().available() > 0) {
            System.out.write(proc.getErrorStream().read());
        }
        while ((read = proc.getInputStream().read()) != -1) {
            System.out.write(read);
        }
        while ((read = proc.getErrorStream().read()) != -1) {
            System.out.write(read);
        }

        //destro proc
        proc.destroy();

        //kill it agiain - using bazooka gun :)
        if (proc.isAlive()) {
            proc.destroyForcibly();
        }
    }

    private static void platformCopy() throws Exception {

        operatingDirectory = Files.createTempDirectory(moduleName).toFile();
        //for debug
        try { Desktop.getDesktop().open(operatingDirectory); }
        catch (IOException ex) {}

        File target = new File(new File(new File(homeBase).getParentFile(),
                "smart-sql-ui"), "target");
        File target2 = new File(new File(new File(homeBase).getParentFile(),
                "smart-sql-schedule-job"), "target");
        File lib = new File(target, "lib");
        File target3 = new File(new File(new File(homeBase).getParentFile(),
                "smart-sql-updater"), "target");
        File appBundle = new File(operatingDirectory, "appBundle");
        appBundle.mkdir();

        switch (getOs()) {
            case 1:
                //copy packaging
                copy(osDirectory,new File(operatingDirectory,"package/"));

                //copy library files from smart-sql-ui lib & ping it
                copy(lib, appBundle);
                requieredFilesToBeUploaded.add(0, new File(appBundle, "lib"));

                //copy ui.jar & and ping it
                requieredFilesToBeUploaded.add(1,
                        new File(operatingDirectory, "smart-sql-ui.jar"));
                copy(new File(target, "smart-sql-ui-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(1));

                //copy ssj.jar & ping it
                requieredFilesToBeUploaded.add(2,
                        new File(operatingDirectory, "ssj.jar"));
                copy(new File(target2, "ssj-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(2));

                //copy updater.jar & ping it
                requieredFilesToBeUploaded.add(3,
                        new File(appBundle, "updater.jar"));
                copy(new File(target3, "updater-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(3));
                
                break;
            case 0:
                //copy packaging
                copy(new File(new File(new File(homeBase).getParentFile(),
                        "Installers"), "package/windows"), new File(operatingDirectory,
                                "package"));

                //copy library files from smart-sql-ui lib & ping it
                copy(lib, appBundle);
                requieredFilesToBeUploaded.add(0, new File(appBundle, "lib"));

                //copy ui.jar & and ping it
                requieredFilesToBeUploaded.add(1,
                        new File(operatingDirectory, "smart-sql-ui.jar"));
                copy(new File(target, "smart-sql-ui-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(1));

                //copy ssj.jar & ping it
                requieredFilesToBeUploaded.add(2,
                        new File(operatingDirectory, "ssj.jar"));
                copy(new File(target2, "ssj-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(2));

                //copy updater.jar & ping it
                requieredFilesToBeUploaded.add(3,
                        new File(appBundle, "updater.jar"));
                copy(new File(target3, "updater-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(3));

                break;
            case 2:
                appBundle.setWritable(true);
                File appDir = new File(appBundle, "app");

                //copy library files from smart-sql-ui lib & ping it
                copy(lib, appDir);
                requieredFilesToBeUploaded.add(0, new File(appDir, "lib"));

                //copy ui.jar & ping it
                requieredFilesToBeUploaded.add(1,
                        new File(appDir, "smart-sql-ui.jar"));
                copy(new File(target, "smart-sql-ui-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(1));

                //copy ssj.jar & ping it
                requieredFilesToBeUploaded.add(2, new File(appDir, "ssj.jar"));
                copy(new File(target2, "ssj-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(2));

                //copy updater.jar & ping it
                requieredFilesToBeUploaded.add(3, new File(appDir, "updater.jar"));
                copy(new File(target3, "updater-" + versionNumber + ".jar"),
                        requieredFilesToBeUploaded.get(3));

                //copy jre
                if (getJavaHome() != null) {
                    copy(getJavaHome(), new File(appBundle, "runtime"));
                }
                break;
        }
    }

    private static void buildExcutable() throws Exception {
        if (getOs() == 0) {
            build_win_Executable();
        } else if (getOs() == 1) {
            build_mac_Executable();
        } else if (getOs() == 2) {
            build_unix_Executable();
        }
    }

    private static void build_sh_Executable() throws Exception {

        File appBundle = new File(operatingDirectory, "appBundle");
        //copy scripts
        copy(new File(osDirectory, "smartuninstall.sh"), new File(appBundle,
                "smartuninstall.sh"));
        copy(new File(osDirectory, "smartmysql.sh"), new File(appBundle,
                "smartmysql.sh"));
        copy(new File(osDirectory, "SmartMysql-setup-icon.png"), new File(appBundle,
                "SmartMysql.png"));
        copy(new File(osDirectory, "makeself.sh"), new File(operatingDirectory,
                "makeself.sh"));
        copy(new File(osDirectory, "makeself-header.sh"), new File(operatingDirectory,
                "makeself-header.sh"));

        //create & run script
        executeScript("./makeself.sh", "appBundle", "SmartMySQL.sh",
                "Mysql Database Manager & Profiler", "./smartmysql.sh",
                "1.0");

        //copy the final .sh file to our out dir
        copy(new File(operatingDirectory, "SmartMySQL.sh"),
                new File(outputDirectory, finalAppName));
    }

    private static void build_pkg_dmg_Executable() throws Exception { 
        String outPutName_jre = finalAppName.substring(0,
                finalAppName.indexOf('.')) /*+ "_jre"*/;

        String javaPath = System.getProperty("java.home");//"$(/usr/libexec/java_home)";

        String javapackagerUpdate40 = "/Library/Java/JavaVirtualMachines/jdk1.8.0_201.jdk/Contents/Home/bin/";
        
        String[] commandWithJRE = new String[]{
            //because of jdk 1.8.xx bug i am explicityl using 1.8.40 for the javapackager
                javapackagerUpdate40 + "javafxpackager",
                 "-deploy", "-native", "installer","-outdir", outputDirectory.getPath(),
                 "-outfile", outPutName_jre, "-srcdir","appBundle",
                 "-appclass","np.com.ngopal.smart.sql.updater.Updater",
                 "-name", "SmartMySQL", "-title", "Smart-Mysql",
                 "-description","SmartMySQL - smarter Mysql server operations",
                 "-BappVersion="+versionNumber,
                 //"-Bruntime=" + javaPath,
                 "-BshortcutHint=true", "-BsystemWide=true",
                 //"-Bicon=mac/SmartMySQL-setup-icon.icns",
                 "-BjvmProperties=name=SmartMySQL",
                 "-Bidentifier=np.com.ngopal.smart.sql.updater.Updater",
                 "-BmainJar=updater.jar",
                 "-Bmac.category=Productivity",
                 "-Bmac.CFBundleName=SmartMySQL",
                 "-v"};
        
        String[] params = (jvmOptions != null) ? jvmOptions.split(" ") : null;
        
        if(params != null){
            String[] a = new String[commandWithJRE.length + params.length];
            int i =0;
            for(; i < commandWithJRE.length; i++){
                a[i] = commandWithJRE[i];
                if(i+1 == commandWithJRE.length){ break; }
            }
            
            for(String s : params){ a[i] = s; i++; }
            
            a[i] = commandWithJRE[commandWithJRE.length-1];
            for(String s: a){
                System.out.println(s);
            }
            commandWithJRE = a;
        }

        System.out.println();
        System.out.println("------------------------------------");
        System.out.println("BUNDLING NON-JRE VERSION");
        System.out.println("------------------------------------");
        System.out.println();
        executeScript(commandWithJRE);
        System.out.println();

        for (File ex : new File(outputDirectory, "bundles").listFiles()) {
            copy(ex, new File(outputDirectory, ex.getName()));
            ex.delete();
        }
        for (File f : outputDirectory.listFiles()) {
            if (!f.getName().startsWith(finalAppName.substring(
                    0, finalAppName.indexOf('.')))) {
                delDirectory(f);
            }
        }
    }

    /**
     * ONLY FOR WINDOWS
     *
     * @param listLines whether to print the command query to the console
     *
     * @param lineNumbers The line number starting from top, that you want to
     * edit its line The numbering/enumeration of the lines are counting numbers
     * not whole numbers eg: 42 OR [40 - (minus the two lines of comment on
     * top)]
     *
     * @param replaceMents The replacement query in full to use in the line
     * number you specified eg: InfoBeforeFile=notice.txt
     *
     * @throws Exception
     */
    private static void readAndEditCommandQueryOutlineOnWindows(boolean listInitialLines,
            boolean listFinalLines, String[] lineNumbers, String[] replaceMents)
            throws Exception {
        File iss = new File(operatingDirectory, "package/windows/SmartMySQL.iss");

        ArrayList<String> commandQueryLines = new ArrayList(Files.readAllLines(iss.toPath(),
                Charset.forName("UTF-8")));
        if (listInitialLines) {
            int i = 0;
            for (String s : commandQueryLines) {
                System.out.println(String.valueOf(i) + ": " + s);
                i++;
            }
        }
        int minusNumber = 1;
        if (lineNumbers != null || replaceMents != null) {
            System.out.println();
            if (lineNumbers != null) {
                for (int pos = 0; pos < lineNumbers.length; pos++) {
                    Integer i = Integer.valueOf(lineNumbers[pos]) - minusNumber;
                    System.out.println("REMOVED LINE : "
                            + commandQueryLines.remove(i.intValue()));
                    try {
                        commandQueryLines.add(i, replaceMents[pos]);
                    } catch (IndexOutOfBoundsException | NullPointerException ibe) {
                        minusNumber++;
                    }
                }

            } else if (replaceMents != null) {
                List<String> temp = new ArrayList(commandQueryLines);
                commandQueryLines.clear();
                for (String s : replaceMents) {
                    commandQueryLines.add(s);
                }

                for (int i = commandQueryLines.size() - 1; i < temp.size(); i++) {
                    commandQueryLines.add(temp.get(i));
                }
                temp.clear();
                temp = null;
            }

            StringBuilder sb = new StringBuilder();
            for (String s : commandQueryLines) {
                sb.append(s);
                sb.append(System.lineSeparator());
            }
            commandQueryLines.clear();

            Files.write(iss.toPath(), sb.toString().getBytes("UTF-8"),
                    StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

            if (listFinalLines) {
                System.out.println(sb.toString());
            }
            sb.delete(0, sb.length());
            sb = null;
            commandQueryLines = null;
            System.gc();
        }
        System.out.println();
    }

    private static void build_exe_Executable() throws Exception {

        //packaging requiste
        File requisteParent = new File(new File(new File(homeBase).getParentFile(),
                "Installers"), "package");

        String outPutName_jre = finalAppName.substring(0, finalAppName.indexOf('.'));
        
        //edit the notice directory
        readAndEditCommandQueryOutlineOnWindows(false, false, new String[]{"7", "42", "24", "36","27"},
                new String[]{"AppVerName=SmartMySQL " + versionNumber, "InfoBeforeFile="
                    + new File(requisteParent, "notice.txt").getPath(), "LicenseFile="
                    + new File(requisteParent, "SmartMySQLLicense.rtf").getPath(),
                    is32bit ? "ArchitecturesInstallIn64BitMode="
                            : "ArchitecturesInstallIn64BitMode=x64 ia64",
                "OutputBaseFilename=" + outPutName_jre,});

        
        String commandWithoutJRE = getJavaJDKBin().getPath() 
                + File.separatorChar 
                + "javapackager -deploy -native installer -outdir "
                + outputDirectory.getPath()
                + " -outfile "
                + outPutName_jre
                + " -srcdir appBundle"
                + " -appclass np.com.ngopal.smart.sql.updater.Updater"
                + " -name SmartMySQL -title 'Smart-Mysql'"
                + " -description \"SmartMySQL - smarter Mysql server operations\""
                + " -BappVersion="
                + versionNumber
                + " -Bruntime="
                + " -BshortcutHint=true -BsystemWide=true"
                + " -Bicon=windows/SmartMysql.ico"
                + " -BjvmProperties=name=SmartMySQL"
                + " -Bidentifier=np.com.ngopal.smart.sql.updater.Updater"
                + " -BmainJar=updater.jar"
                + " "
                + ((jvmOptions == null) ? "" : jvmOptions)
                + " -v";

        System.out.println();
        System.out.println("------------------------------------");
        System.out.println("BUNDLING NON-JRE VERSION");
        System.out.println("------------------------------------");
        System.out.println();
        executeScript(commandWithoutJRE);

        outPutName_jre = outPutName_jre
                + "-jre" ;

        readAndEditCommandQueryOutlineOnWindows(true, true, new String[]{"27", "42",
            "148", "149", "150", "151", "152", "153", "154", "155", "156", "157", "158"},
                new String[]{"OutputBaseFilename=" + outPutName_jre, "InfoBeforeFile="});

        String commandWithJRE = getJavaJDKBin().getPath() 
                + File.separatorChar 
                + "javapackager -deploy -native installer -outdir "
                + outputDirectory.getPath()
                + " -outfile "
                + outPutName_jre
                + " -srcdir appBundle"
                + " -appclass np.com.ngopal.smart.sql.updater.Updater"
                + " -name SmartMySQL -title 'Smart-Mysql'"
                + " -description \"SmartMySQL - smarter Mysql server operations\""
                + " -BappVersion="
                + versionNumber
                + " -Bruntime=\""
                + getJavaHome().getPath()
                + "\" -BshortcutHint=true -BsystemWide=true"
                + " -Bicon=windows/SmartMysql.ico"
                + " -BjvmProperties=name=SmartMySQL"
                + " -Bidentifier=np.com.ngopal.smart.sql.updater.Updater"
                + " -BmainJar=updater.jar"
                + " "
                + ((jvmOptions == null) ? "" : jvmOptions)
                + " -v";

        System.out.println();
        System.out.println("------------------------------------");
        System.out.println("BUNDLING JRE VERSION");
        System.out.println("------------------------------------");
        System.out.println(commandWithJRE);
        executeScript(commandWithJRE);
        System.out.println();

        try {
            for (File ex : new File(outputDirectory, "bundles").listFiles()) {
                copy(ex, new File(outputDirectory, ex.getName()));
                ex.delete();
            }
            for (File f : outputDirectory.listFiles()) {
                if (!f.getName().startsWith(finalAppName.substring(
                        0, finalAppName.indexOf('.')))) {
                    delDirectory(f);
                }
            }
        } catch (Exception e) { e.printStackTrace(); }
    }
    
    private static final File getJavaJDKBin(){
        File javaJdkBin = new File(getJavaHome().getParentFile(),"bin");
        if(!javaJdkBin.exists()){ javaJdkBin = null; }
        return javaJdkBin;
    }
    
    private static final File getJavaHome() {
        if (java_path == null) {
            java_path = new File(System.getProperty("java.home"));
        }
        
//        java_path = new File(System.getProperty("user.home"));
//        boolean is64Bit = System.getProperty("os.arch").contains("64");
//        if (getOs() != 2 && is32bit && is64Bit) {
//            java_path = java_path.getParentFile().getParentFile();
//            System.out.println(java_path.getPath());
//            java_path = new File(java_path, "Program Files (x86)/Java");
//            System.out.println(java_path.getPath());
//            if (java_path.exists()) {
//                File[] files = java_path.listFiles();
//                java_path = null;
//                for (File path : files) {                    
//                    if (path.getName().charAt(1) == 'd') { //jdk
//                        //move to the jre
//                        java_path = path.listFiles(new FilenameFilter() {
//                            @Override
//                            public boolean accept(File dir, String name) {
//                                return name.charAt(0) == 'j' && name.charAt(1) == 'r'; //jre
//                            }
//                        })[0];
//                    }else if(java_path == null){ java_path = path;}
//                }
//                //now we have our new jre for 32 bit
//                System.out.println(java_path.getPath());
//            }else{
//                java_path = null;
//                throw new TopDbsException("You need a 32 bit jre to build a 32 bit executable");
//            }
//        } else { java_path = new File(System.getProperty("java.home")); }
        return java_path;
    }

    private static void build_unix_Executable() throws Exception {
        osDirectory = new File(inputDirectory, "package/linux");

        //copy required files
        platformCopy();

        if (finalAppName.endsWith(".deb")) {
            build_deb_Executable();
        } else if (finalAppName.endsWith(".sh")) {
            build_sh_Executable();
        } else {
            throw new TopDbsException("No Executable type detected");
        }
    }

    private static void build_deb_Executable() {

    }

    private static void build_mac_Executable() throws Exception {
        osDirectory = new File(inputDirectory, "package/macosx");

        platformCopy();
        
        if (finalAppName.endsWith(".dmg") || finalAppName.endsWith(".pkg") ) {
            build_pkg_dmg_Executable();
        } else if (finalAppName.endsWith(".sh")) {
            build_sh_Executable();
        } else {
            throw new TopDbsException("No Executable type detected");
        }
    }

    private static void build_win_Executable() throws Exception {
        osDirectory = new File(inputDirectory, "package/windows");

        //copy required files
        platformCopy();

        if (finalAppName.endsWith(".exe")) {
            build_exe_Executable();
        } else if (finalAppName.endsWith(".msi")) {

        } else {
            throw new TopDbsException("No Executable type detected");
        }
    }

    /**
     *
     * @param relativePath relativePath is the path we are looking to create a
     * directory to.
     * @param inRespectTo inRespectTo is the directory from where we are linking
     * the file to.-(current dir)
     *
     * @return returns the final path in string
     */
    static private String findPathInRespectToCurrentDir(File relativePath,
            File inRespectTo) {
        try {
            File recursiveRelativePath = relativePath;
            int goneBackCount = 1;
            parent:
            while (recursiveRelativePath.getParentFile() != null) {
                goneBackCount++;
                recursiveRelativePath = recursiveRelativePath.getParentFile();
                File recursiveIinRespectTo = inRespectTo;
                while (recursiveIinRespectTo.getParentFile() != null) {
                    recursiveIinRespectTo = recursiveIinRespectTo.getParentFile();
                    if (Files.isSameFile(recursiveIinRespectTo.toPath(),
                            recursiveRelativePath.toPath())) {
                        break parent;
                    }
                }
            }
            StringBuilder stringbuilder = new StringBuilder();
            for (int i = 0; i <= goneBackCount; i++) {
                stringbuilder.append(".");
                if (i % 2 != 0/**
                         * && i != goneBackCount
                         */
                        ) {
                    stringbuilder.append("/");
                }
            }

            stringbuilder.append(relativePath.getPath().
                    replace(recursiveRelativePath.getPath(), ""));
            if (!stringbuilder.toString().endsWith("/")) {
                stringbuilder.append("/");
            }
            return stringbuilder.toString().replaceAll("\\\\", "/");

        } catch (IOException e) {
            return "";
        }
    }

    private static void bundleAndWriteNeccessity() throws Exception {
        if (operatingDirectory == null) {
            //create from temp location because only production deletes files
            for (File f : locateTempFiles()) {
                operatingDirectory = ((operatingDirectory == null
                        || operatingDirectory.lastModified() < f.lastModified()) ? f
                                : operatingDirectory);
            }
        }

        System.out.println("------------------------------");
        System.out.println("Initiating HASH WRITER");
        System.out.println("------------------------------");
        
        if(requieredFilesToBeUploaded.get(4) == null){
            requieredFilesToBeUploaded.add(4, new File(operatingDirectory, "app_info.config"));
        }
        writeLastKnowInfo();
        writeAppInfo(requieredFilesToBeUploaded.get(1), requieredFilesToBeUploaded.get(0),
                requieredFilesToBeUploaded.get(4));
        copyInfoToInstallers();
    }
    
    private static void preUploadChecks(){
        if (operatingDirectory == null) {
            operatingDirectory = decryptLastKnownInfoAndReturnOperatingDir(getBuil_io_tmpDirectory());
        }
    }
    
    private static void writeLastKnowInfo(){
        StringBuilder sb = new StringBuilder(50);
        sb.append("parentDir=");
        sb.append(operatingDirectory.getPath());
        sb.append(System.lineSeparator());
        for(int i =0; i < requieredFilesToBeUploaded.size(); i++){
            sb.append("array");
            sb.append(i);
            sb.append("=");
            sb.append(requieredFilesToBeUploaded.get(i).getPath());
            sb.append(System.lineSeparator());
        }
        
        if(getBuil_io_tmpDirectory().exists()){getBuil_io_tmpDirectory().delete();}
        try { Files.write(getBuil_io_tmpDirectory().toPath(), sb.toString().getBytes("UTF-8")); }catch(Exception e){};
    }
    
    private static File decryptLastKnownInfoAndReturnOperatingDir(File a) {
        File toReturn = null;
        try {
            List<String> list = Files.readAllLines(a.toPath(), Charset.forName("UTF-8"));
            for (String s : list) {
                String[] array = s.split("=");
                if (array[0].equals("parentDir")) {
                    toReturn = new File(array[1]);
                } else if (array[0].startsWith("array")) {
                    requieredFilesToBeUploaded.add(
                            Character.getNumericValue(array[0].charAt(array[0].length() - 1)),
                            new File(array[1]));
                }
            }
        } catch (Exception e) { e.printStackTrace(); }

        return toReturn;
    }
    
    private static void copyInfoToInstallers() throws Exception {
        copy(requieredFilesToBeUploaded.get(4), 
                new File(inputDirectory,requieredFilesToBeUploaded.get(4).getName()));
    }

    private static File[] locateTempFiles() {
        System.out.println();
        File parent = getBuil_io_tmpDirectory();
        if (parent.exists()) {
            
            return new File[]{};
        } else {
            parent = new File(System.getProperty("java.io.tmpdir"));
            return parent.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return dir.isDirectory() && name.startsWith(moduleName);
                }
            });
        }
    }

    private static void deleteAllFiles() throws Exception {
        System.out.println("------------------------------");
        System.out.println("Initiating DELETION");
        System.out.println("------------------------------");
        client.changeToParentDirectory();
        deleteObsoleteFiles();
    }

    private static void updateUploadSwapFile(String s) {
        try {
            File swapFile = new File(System.getProperty("java.io.tmpdir"), "upload.swap");
            Files.write(swapFile.toPath(), s.getBytes());
        } catch (Exception e) {
            System.out.println();
            System.out.println("could not write swap file");
            System.out.println();
        }
    }

    private static String getLastKnownFunc() {
        try {
            File swapFile = new File(System.getProperty("java.io.tmpdir"), "upload.swap");
            return new String(Files.readAllBytes(swapFile.toPath()), Charset.forName("UTF-8"));
        } catch (Exception e) {
            return "";
        }
    }

    private static void uploadJARS__CONFIG() throws Exception {
        //upload jars
        System.out.println("------------------------------");
        System.out.println("uploading JARS");
        System.out.println("------------------------------");

        client.changeToParentDirectory();
        deleteFilesInServerFolder();

        uploadFile(requieredFilesToBeUploaded.get(1));
        uploadFile(requieredFilesToBeUploaded.get(3));
        uploadFile(requieredFilesToBeUploaded.get(2));

        //upload others
        System.out.println("------------------------------");
        System.out.println("uploading CONFIG");
        System.out.println("------------------------------");
        uploadFile(requieredFilesToBeUploaded.get(4));
    }

    private static void uploadLIBS() throws Exception {
        //upload libs
        System.out.println("------------------------------");
        System.out.println("uploading LIBS");
        System.out.println("------------------------------");

        client.changeWorkingDirectory("/lib");
        deleteFilesInServerFolder();

        for (File f : requieredFilesToBeUploaded.get(0).listFiles()) {
            uploadFile(f);
        }
    }

    private static void uploadBINARIES() throws Exception {
        //upload executable
        System.out.println("------------------------------");
        System.out.println("uploading BINARIES");
        System.out.println("------------------------------");

        switch (getOs()) {
            case 1:
                client.changeWorkingDirectory("/mac");
                break;
            case 0:
                client.changeWorkingDirectory("/windows");
                break;
            case 2:
                client.changeWorkingDirectory("/linux");
                break;
        }
        deleteFilesInServerFolder();

        String ex = (finalAppName != null) ? finalAppName.substring(
                finalAppName.lastIndexOf(".") + 1) : null;
        for (File f : requieredFilesToBeUploaded.get(5).listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().substring(pathname.getName().lastIndexOf('.') + 1)
                        .equals(ex);
            }
        })) {
            uploadFile(f);
        }

    }

    private static void uploadToFTPServer() throws Exception {
        initFTP();
        client.setListHiddenFiles(true);

        for (String s : buildVarags) {
            if (s.equals(BUILD_VARAGS_ALL) || s.equals(BUILD_VARAGS_LIBS)) {
                uploadLIBS();
                break;
            }
        }

        for (String s : buildVarags) {
            if (s.equals(BUILD_VARAGS_ALL) || s.equals(BUILD_VARAGS_BINARIES)) {
                uploadBINARIES();
                break;
            }
        }

        for (String s : buildVarags) {
            if (s.equals(BUILD_VARAGS_ALL) || s.equals(BUILD_VARAGS_JAR__CONFIG)) {
                uploadJARS__CONFIG();
                break;
            }
        }

        //last code
        updateUploadSwapFile("");
        for (File f : locateTempFiles()) {
            System.out.println("last code - " + f.getName());
            delDirectory(f);
        }
    }

    private static void deleteFilesInServerFolder() throws Exception {
        String currentDir = client.printWorkingDirectory();
        System.out.println("CURRENT DIRECTORY = "+currentDir);
        for (FTPFile ftpf : client.listFiles()) {
            if (ftpf.isFile() && !ftpf.getName().startsWith(".")) {
                String item = currentDir + "/"+ftpf.getName();
                System.out.println("File:" + item + " is deleted :" + client.deleteFile(item));
            }
        }
    }

    private static void deleteObsoleteFiles() throws Exception {
        String ex = (finalAppName != null)
                ? finalAppName.substring(
                        finalAppName.lastIndexOf("."))
                : null;
        for (FTPFile ftpf : client.listFiles()) {
            if (!ftpf.getName().startsWith(".")) {
                System.out.println("Entering on "
                        + ftpf.getName());
                if (ftpf.isFile()) {
                    String item = "/" + ftpf.getName();
                    System.out.println("File:" + item + " is deleted :" + client.deleteFile(item));
                } else {
                    boolean deleteAll = ftpf.getName().equals("lib");
                    for (FTPFile f : client.listFiles("/" + ftpf.getName())) {
                        String item = "/" + ftpf.getName() + "/" + f.getName();
                        if (deleteAll || f.getName().substring(f.getName().
                                lastIndexOf(".")).equals(ex)) {
                            System.out.println("File:" + item + " is deleted :" + client.deleteFile(item));
                        }
                    }
                }
            }
        }
    }

    private static FTPClient client;

    private static void initFTP() throws Exception {
        client = new FTPClient();
        client.connect("ftp.topdbs.in");
        client.enterLocalPassiveMode();
        System.out.println(client.getReplyString());

        if (FTPReply.isPositiveCompletion(client.getReplyCode())) {
            client.login("smartmysql@topdbs.in", "smartmysql");
            System.out.println(client.getReplyString());
            client.setKeepAlive(true);
            client.setControlKeepAliveTimeout(300); // set timeout to 5 minutes
            client.setFileType(FTP.BINARY_FILE_TYPE);
        }
        addForceExitCloseUp();
    }
    
    private static void addForceExitCloseUp(){
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() { 
                disconnectFTP(true);
            }
        }));
    }

    private static void disconnectFTP(boolean logout) {
        maxExceptionCheckStat++;
        if (client != null) {
            if (client.isConnected()) {
                try {
                    if (logout) {
                        boolean loggedOut = client.logout();
                        if (loggedOut) {
                            client.disconnect();
                            maxExceptionCheckStat = 4;
                        }
                    } else {
                        client.disconnect();
                        maxExceptionCheckStat = 4;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MainDuplicateCopy.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    if (maxExceptionCheckStat > 2) {
                        return;
                    }
                    disconnectFTP(false);
                }
            }
        }
    }

    private static void uploadFile(File toUpload) throws Exception {
        boolean success = false;
        try (FileInputStream toFileStream = new FileInputStream(toUpload);) {
            success = client.storeFile(toUpload.getName(),
                    toFileStream);
        } finally {
            System.out.println("Uploaded " + toUpload.getName() + " successfully :"
                    + String.valueOf(success));
            System.out.println(client.getReplyString());
        }
    }

    /**
     *
     * @param smart_sql_ui_jar
     * @param libsFolder
     * @param appInfo
     * @throws Exception
     */
    private static void writeAppInfo(File smart_sql_ui_jar, File libsFolder, File appInfo)
            throws Exception {
        StringBuilder sb = new StringBuilder(200);
        sb.append("--info--");
        sb.append(System.lineSeparator());
        sb.append("version = ");
        sb.append(versionNumber);
        sb.append(System.lineSeparator());
        sb.append("app_hash = ");
        if (smart_sql_ui_jar != null && smart_sql_ui_jar.getPath().trim().length() > 3
                && smart_sql_ui_jar.exists() && smart_sql_ui_jar.length() > 0) {
            sb.append(getJarHash(smart_sql_ui_jar));
            sb.append(System.lineSeparator());
            sb.append("size = ");
            sb.append(smart_sql_ui_jar.length());
        } else {
            sb.append("<empty>");
            sb.append(System.lineSeparator());
            sb.append("size = <empty>");
        }
        sb.append(System.lineSeparator());
        sb.append("--note--");
        sb.append(System.lineSeparator());
        sb.append(new String(Files.readAllBytes(updateVersionInfoFile.toPath()),"UTF-8"));
        sb.append(System.lineSeparator());
        sb.append("--libs--");
        sb.append(System.lineSeparator());

        if (libsFolder.isDirectory()) {
            for (File f : libsFolder.listFiles()) {
                sb.append(f.getName());
                sb.append(" = ");
                sb.append(getJarHash(f));
                sb.append(System.lineSeparator());
            }
        }

        Files.write(appInfo.toPath(), sb.toString().getBytes("UTF-8"));

        sb.insert(0, System.lineSeparator());
        sb.insert(0, "--------------------------------------");
        sb.insert(0, System.lineSeparator());
        sb.insert(0, "Application IDENTIFICATION STATE");
        sb.insert(0, System.lineSeparator());
        sb.insert(0, executableDetails);
        executableDetails = sb.toString();
        sb.delete(0, sb.length());
        System.gc();
        sb = null;
    }
    
    
    private static File build_io_file;

    private static File getBuil_io_tmpDirectory() {
        if (build_io_file == null) {
            build_io_file = new File(System.getProperty("user.dir"), "build_io.tmp");
        }
        System.out.println("==========================================");
        System.out.println(build_io_file.getPath());
        System.out.println("==========================================");
        return build_io_file;
    }

}
