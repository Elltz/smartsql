/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.sun.javafx.PlatformUtil;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 *
 * This contains all important fields for the current build
 */
public class BuildVars implements Serializable {

    private String versionNumber;
    private String outputDirectoryLocation; //where the executables will be stored at
    private String inputDirectoryLocation; //the location of where to locate all important directory
    private String operatingDirectoryLocation;//temporary directory created to create files
    private String osDirectoryLocation;
    private String java_pathLocation;
    private String signtool_pathfile;
    private String licenseText;//the license text
    private String homeBase;
    private String executableDetails;
    private String appName;
    private String jvmOptions;
    private String[] argVarags;
    private String smart_sql_ui_jar;
    private String Smart_sql_ssj_jar;
    private String smart_sql_updater_jar;
    private String smart_sql_libs_folder;
    private String smart_sql_info_config_file;
    private String notice_file;
    private String jre_runtime;
    private String app_bundle_loc;
    private String module_project;
    private int maxExceptionCheckStat = 0;
    private boolean is32bit = false, executebuild, executeUpdate;
    //private boolean standalone;
    private byte binary_package_type;
    private byte build_type;
    private String extension;
    private String arguments;
    private byte forcedLevel;
    private String updateNotice_information;
    private byte certifcateSigningStatus;
    
    private String bannerText;
    private String youtubeText;
    private String onlineServer;
    private String appEncryptionKey;
    
    private File windowSmartConsole;
    
    public static final String APP_INFO_CFG = "app_info.config";
    
    public static final String EXECUTE_BUILD_FLAG = "build.execute";
    public static final String PRODUCTION_BUILD_FLAG = "build.production";

    public BuildVars(String smartsqlProjectDir, String moduleProject) throws Exception {
        this(new String[]{null, smartsqlProjectDir,moduleProject,null,null,null,null,null,null,null,null,null,null});
    }
    
    public BuildVars(String[] args) throws Exception {
        versionNumber = args[0];
        homeBase = args[1];
        module_project = args[2];
        appName = args[3];
        
        if(args[4] != null){
            extension = args[4].trim().toLowerCase();
        }
        
        if(args[5] != null){
            binary_package_type = Byte.valueOf(args[5]);
        }
        
        if(args[6] != null){
            argVarags = args[6].split(",");
        }
        
        jvmOptions = args[7];
        arguments = args[8];
        
        if(args[9] != null){
            forcedLevel = Byte.valueOf(args[9]);
        }
        
        updateNotice_information = args[10];
        
        if(args[11] != null){
            build_type = Byte.valueOf(args[11]);
        }
        
        if(args[12] != null){
            certifcateSigningStatus = Byte.valueOf(args[12]);
        }

        is32bit = !System.getProperty("os.arch").contains("64");
        inputDirectoryLocation = new File(new File(homeBase), "Installers").getPath();
        smart_sql_info_config_file = new File(inputDirectoryLocation, APP_INFO_CFG).getPath();

        executebuild = Boolean.valueOf(System.getProperty(EXECUTE_BUILD_FLAG));
        executeUpdate = Boolean.valueOf(System.getProperty(PRODUCTION_BUILD_FLAG));
        
        if (executebuild) {
            operatingDirectoryLocation = Files.createTempDirectory(BuildEntry.moduleName).toString();
            File appBundle = new File(operatingDirectoryLocation, "appBundle");
            appBundle.mkdir();
            app_bundle_loc = appBundle.getPath();

            if (PlatformUtil.isUnix()) {
                osDirectoryLocation = new File(inputDirectoryLocation, "package/linux").getPath();
                //since Unix structuring is different
                File appDir = new File(appBundle, "app");
                if (!isStandalone()) {
                    smart_sql_updater_jar = new File(appDir, "update_utility.jar").getPath();
                }
                smart_sql_libs_folder = new File(appDir, "lib").getPath();
                Smart_sql_ssj_jar = new File(appDir, "ssj.jar").getPath();
                smart_sql_ui_jar = new File(appDir, "main_app.jar").getPath();
                jre_runtime = new File(appBundle, "runtime").getPath();
            } else if (PlatformUtil.isWindows() || PlatformUtil.isMac()) {
                osDirectoryLocation = new File(inputDirectoryLocation, PlatformUtil.isWindows()
                        ? "package/windows" : "package/macosx").getPath();
                if (!isStandalone()) {
                    smart_sql_updater_jar = new File(appBundle, "update_utility.jar").getPath();
                    windowSmartConsole = new File(appBundle, "smart-console.lnk");
                }
                smart_sql_libs_folder = new File(appBundle, "lib").getPath();
                Smart_sql_ssj_jar = new File(appBundle, "ssj.jar").getPath();
                smart_sql_ui_jar = new File(appBundle, "main_app.jar").getPath();
            }

            notice_file = new File(getOsDirectoryLocation().getParentFile(), "notice.txt").getPath();
            licenseText = new File(getOsDirectoryLocation().getParentFile(), "SmartMySQLLicense.rtf").getPath();

        }
        
        if (args.length > 13) {
            bannerText = args[13];
        }
        
        if (args.length > 14) {
            youtubeText = args[14];
        }
        
        if (args.length > 15) {
            onlineServer = args[15];
        }
        
        if(args.length > 16){
            appEncryptionKey = args[16];
        }
        
        locateSigntoolPathFile();
    }
    
    public File getAppBundleLocation(){
        return new File(app_bundle_loc);
    }

    public File getOutputDirectory() {
        File f;
        if (outputDirectoryLocation == null) {
            f = new File(System.getProperty("user.home") + File.separator
                    + "Desktop/finalBuild");
            outputDirectoryLocation = f.getPath();
        } else {
            f = new File(outputDirectoryLocation);
        }

        if(!f.exists()){ f.mkdir(); }
        
        return f;
    }

    public File getInputDirectoryLocation() {
        return new File(inputDirectoryLocation);
    }

    public File getOperatingDirectoryLocation() {
        return new File(operatingDirectoryLocation);
    }

    public File getJava_pathLocation() {
        return new File(java_pathLocation);
    }

    public String getHomeBase() {
        return homeBase;
    }

    public boolean isIs32bit() {
        return is32bit;
    }

    public boolean isExecutebuild() {
        return executebuild;
    }

    public boolean isExecuteUpdate() {
        return executeUpdate;
    }
    
    public String getBannerText() {
        return bannerText;
    }
    
    public String getYoutubeText() {
        return youtubeText;
    }
    
    public String getOnlineServer() {
        return onlineServer;
    }

    public String getAppEncryptionKey() {
        return appEncryptionKey;
    }

    public final boolean isStandalone() {
        return binary_package_type == BuildEntry.BINARY_PACKAGE_BUNDLED_TYPE;
    }

    public String getAppName(boolean addExtentions) {
        if (addExtentions) {
            return new StringBuffer().append(appName).append('.').append(extension).toString();
        }
        return appName;
    }
    
    public final String getJavaPackagerStringDirectory(){
        File jdkBin = getJavaJDKBin(false);
        /**
         * here we check if javapackager exist, if yes, use it
         * if not do not use it & resort to the old static javafxpackager.
         * its always & will be there, but it is currently deprecated and might
         * be remove in the future
         */
        
        File temp = new File(jdkBin, "javapackager");
        if(temp.exists()){
            return temp.getPath();
        }else{
            return new File(jdkBin,"javafxpackager").getPath();
        }
    }
    
    public final File getJavaJDKBin(boolean useLowerVersionAvailable) {        
        /**
         * This code below might need checking because if it is a development
         * platform it need have jdk, the jdk had jre, hence javaHome will point
         * to the jre in the Jdk. so there no need unneccesarily check. but the
         * check is there. need refractor
         *
         * for lowerversion tracking since we want our application run on all
         * java version starting from 1.8.0_00
         */
        File javaJdkBin = new File(getJavaHome()).getParentFile();

        if (PlatformUtil.isMac()) {
            /**
             * Because of jdk 1.8.xx bug we need explicitly use 1.8.40 for the javapackager
             * but this bug solved in recent jdk versions, but we not sure who.
             * so we check if 1.8.40 jdk version available, IF YES,
             * we use it, IF NO, we do normal & hope it not fail
             */
            String javapackagerUpdate40 = "/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/bin/";
            File macJavaPath = new File(javapackagerUpdate40);
            if (macJavaPath.exists()) {
                return macJavaPath;
            }
        } else {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.charAt(1) == 'd';
                }
            };
            if (useLowerVersionAvailable) {
                File parent = javaJdkBin.getParentFile();// we are in the java folder
                javaJdkBin = null;
                int lowestUpdate = 1000; //we use 10^3 to create a high maximum number
                int lowestVersion = 1000; //same here
                for (File path : parent.listFiles(filenameFilter)) {
                    String name = path.getName();
                    int index = name.lastIndexOf('_');
                    if (index > -1) {
                        int k = Integer.valueOf(name.substring(name.indexOf('.') + 1, name.lastIndexOf('.')));
                        int cur = Integer.valueOf(name.substring(index + 1));
                        if (lowestUpdate >= cur && lowestVersion >= k) {
                            lowestUpdate = cur;
                            lowestVersion = k;
                            javaJdkBin = path;
                        }
                    }
                }
            }
        }  
        
        javaJdkBin = new File(javaJdkBin, "bin");
        System.gc();

        return javaJdkBin;
    }
    
    public final String getJavaHome() {
        return System.getProperty("java.home");
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public String[] getArgVarags() {
        return argVarags;
    }

    public byte getBinary_package_type() {
        return binary_package_type;
    }
    
    public byte getBuild_type() {
        return build_type;
    }

    public File getSmart_sql_ui_jar() {
        return new File(smart_sql_ui_jar);
    }

    public File getSmart_sql_ssj_jar() {
        return new File(Smart_sql_ssj_jar);
    }

    public File getSmart_sql_updater_jar() {
        return new File(smart_sql_updater_jar);
    }
    
    public String getSmartSqlPackageName() {
        try {
            JarFile jar = new JarFile(getSmart_sql_ui_jar());
            return jar.getManifest().getMainAttributes().getValue(Attributes.Name.MAIN_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public File getSmart_sql_libs_folder() {
        return new File(smart_sql_libs_folder);
    }

    public final File getOsDirectoryLocation() {
        return new File(osDirectoryLocation);
    }

    public File getJre_runtime() {
        return new File(jre_runtime);
    }
    
    public File getSmart_sql_info_config_file() {
        return new File(smart_sql_info_config_file);
    }

    public String getLicenseText() {
        return licenseText;
    }

    public String getExtension() {
        return extension;
    }

    public String getJvmOptions() {
        if(jvmOptions == null || jvmOptions.isEmpty()){
            return "";
        }else{
            return jvmOptions;
        }
    }
    
    public String getAppArgs(boolean raw){
        if(arguments == null || arguments.isEmpty()){
            return "";
        }else if(raw){
            return arguments;
        }else{
            StringBuilder sb = new StringBuilder(50);
            for(String s : arguments.split(" ")){
                sb.append(" ");
                sb.append("-argument ");
                sb.append(s);
            }
            System.out.println(sb);
            return sb.toString();
        }
    }

    public String getNotice_file() {
        return notice_file;
    }
    
    public boolean isForcingLevel(){
        return forcedLevel > 0;
    }
    
    public boolean isFreshBuild(){
        return forcedLevel == -1;
    }
    
    public byte getForcedLevel(){
        return forcedLevel;
    }

    public String getUpdateNotice_information() {
        return updateNotice_information;
    }
    
    public void updateDynamicContent(String[] args){
        forcedLevel = Byte.valueOf(args[9]);
    }

    public String getSigntool_pathfile() {
        return signtool_pathfile;
    }

    public String getModule_project() {
        return module_project;
    }
    
    private File searchSigntoolFolderFromWindows(){
        try{
            File programFile = new File("/").listFiles(new FileFilter() {

                @Override
                public boolean accept(File arg0) {
                    return arg0.getName().charAt(0) == 'P' && arg0.getName().contains(" ") && (is32bit || arg0.getName().contains("x86"));
                }
            })[0];

            programFile = programFile.listFiles(new FileFilter() {

                @Override
                public boolean accept(File arg0) {
                    return arg0.getName().charAt(0) == 'M' && arg0.getName().endsWith("SDKs");
                }
            })[0];

            programFile = programFile.listFiles(new FileFilter() {

                @Override
                public boolean accept(File arg0) {
                    return (arg0.getName().charAt(0) == 'C' && arg0.getName().equalsIgnoreCase("clickonce")); // add more conditions with or statement
                }
            })[0];

            programFile = programFile.listFiles(new FileFilter() {

                @Override
                public boolean accept(File arg0) {
                    return (arg0.getName().charAt(0) == 'S' && arg0.getName().equalsIgnoreCase("signtool"));
                }
            })[0];
            
            return programFile.listFiles()[0];
        }catch(Exception e){
            return null;
        }
    }
    
    private File searchSigntoolFolderFromInstallers(){
         try{
             /**              
              * the listFiles returns the original path to the signtool.exe executable
              */
             return new File(getInputDirectoryLocation(), "signtool").listFiles()[0];
        }catch(Exception e){
            return null;
        }
    }
    
    private void locateSigntoolPathFile() {
        try {
            //searching from windows sdk is required
            File programFile = searchSigntoolFolderFromWindows();
            if (programFile == null){
                //our installer version is a fail safe fall back
                programFile = searchSigntoolFolderFromInstallers();
            }
            
            if (programFile != null && programFile.exists() && programFile.isFile()) {
                signtool_pathfile = programFile.getPath();
            }
        } catch (Exception e) {}
    }
    
    public String getSigningCodeScript() {

        StringBuilder sb = new StringBuilder(200)
                .append(signtool_pathfile)
                .append(" sign /v /f ")
                .append(new File(inputDirectoryLocation, "/certificates").listFiles()[0].getPath())
                .append(" /p $SmartMysqlWInSignKey$ ");

        if (shouldAppendTimestamp()) {
            sb.append("/t http://timestamp.comodoca.com/authenticode ");
        }

        sb.append(getOutputDirectory().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".exe");
            }
        })[0].getPath());

        return sb.toString();
    }
    
    private boolean shouldAppendTimestamp(){
        return certifcateSigningStatus == 1;
    }
    
    public boolean shouldSignExecutableWithCertificate(){
        return certifcateSigningStatus > -1;
    }

    public File getWindowSmartConsolePath() {
        return windowSmartConsole;
    }
    
}
