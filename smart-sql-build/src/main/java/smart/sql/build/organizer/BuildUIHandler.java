/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import com.google.api.gax.paging.Page;
import com.google.cloud.ReadChannel;
import com.google.cloud.WriteChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Bucket.BucketSourceOption;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.CopyWriter;
import com.google.cloud.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.sun.javafx.PlatformUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class BuildUIHandler implements Initializable {

    @FXML
    private TextArea bannerTextArea;
    
    @FXML
    private TextField youtubeTextArea;
    
    @FXML
    private TextField onlineServerHost;
    @FXML
    private TextField onlineServerPort;
    @FXML
    private TextField onlineServerDatabase;
    @FXML
    private TextField onlineServerUser;
    @FXML
    private TextField onlineServerPassword;
    
    @FXML
    private VBox busyPane;
    
    private Stage dailogStage;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Label informationLabel;

    @FXML
    private Circle informationStatusIcon;

    @FXML
    private Button buildButton;

    @FXML
    private Label projectVersionLabel;

    @FXML
    private ImageView help_project_version;

    @FXML
    private ImageView help_level_swap;

    @FXML
    private ImageView help_app_mode;

    @FXML
    private ImageView help_updatable_mode;

    @FXML
    private ImageView help_binary_name;

    @FXML
    private ImageView help_executable_type;

    @FXML
    private ImageView help_app_sys_props;

    @FXML
    private ImageView help_prod_mode;

    @FXML
    private ImageView help_cloud_service;

    @FXML
    private ImageView help_latest_build_info;
    
    @FXML ImageView help_check_update_at_start;

    @FXML
    private TextField level_swap_textfield;

    @FXML
    private RadioButton level_swap_radiobutton;

    @FXML
    private ChoiceBox<String> app_mode_box;

    @FXML
    private TextField binary_name_textfield;

    @FXML
    private ChoiceBox<String> executable_type_box;
    
    @FXML
    private RadioButton executable_option_NO;
    @FXML
    private RadioButton executable_option_YES;
    
    @FXML
    private TextArea app_sys_props_area;
    
    @FXML
    private CheckBox uploadUpdateCheckBox;
    @FXML
    private CheckBox uploadExecutableCheckBox;
    
    @FXML
    private HBox uploadHBox;
    
    @FXML
    private RadioButton build_prod_option_NO;
    
    @FXML
    private RadioButton build_prod_option_YES;
    
    @FXML
    private ChoiceBox app_build_box;
    
    @FXML
    private ChoiceBox cloud_service_box;
    
    @FXML
    private TextArea latest_build_info_area;
    
    @FXML
    private Button backButton;
    
    @FXML
    private Label busyLabel;
    
    @FXML
    private Button moveToProductionButton;
        
    @FXML
    private VBox code_front;
    
    @FXML
    private Label stream_text;
    
    @FXML
    private ScrollPane ui_container;
    
    @FXML
    private TextField encrytionkeyField;
    
    @FXML
    private Button cancelbuildButton;
    
    @FXML
    private ScrollPane log_container;
    
    @FXML
    private ChoiceBox check_update_at_start_box;
    
    @FXML
    private VBox frontalPage;
    
    @FXML
    private Label smartLabel;
    
    @FXML
    private ListView<String[]> libs_listview;
        
    @FXML
    private Tab linux_tab;
    
    @FXML
    private Tab mac_tab;
    
    @FXML
    private Tab windows_tab;
    
    @FXML
    private VBox view_info_container;
    
    @FXML
    private Button initiate_build_button;
    
    @FXML
    private Button view_build_info_button;
        
    @FXML
    private HBox running_build_footer;
    
    @FXML
    private TableView<VersionInfo> versionsQaTable;
    @FXML
    private TableView<VersionInfo> versionsProductionTable;
    
    @FXML
    private TabPane versionsTab;
    
    @FXML
    private Tab qaTab;
    
    @FXML
    private Tab productionTab;
    
    @FXML
    private Button deleteQAButton;
    
    @FXML
    private Button deleteProductionButton;
    
    @FXML
    private Button uploadExecutableButton;
    
    @FXML
    private Button increaseMajorButton;
    
    @FXML
    private HBox cert_activateCert_container;
    
    @FXML
    private HBox cert_timestamp_container;    
    
    @FXML
    private RadioButton cert_activateCert_NO;
    
    @FXML
    private RadioButton cert_activateCert_YES;
    
    @FXML
    private RadioButton cert_timestamp_NO;
    
    @FXML
    private RadioButton cert_timestamp_YES;
    
    @FXML
    private CheckBox disableBanner;
    
    @FXML
    private ChoiceBox<String> modulesSelectorChoiceBox;
    
    @FXML
    private Button prod_makeCurrentVersionButton;
    
    @FXML
    private Button qa_makeCurrentVersionButton;
    
    private Stage parentStage;
    
    private final String PROJECT_NAME = "smartsql";
    private final String[] macExecutableTypes = new String[]{"PKG", "DMG" , "ALL"};
    private final String[] linuxExecutableTypes = new String[]{"SH","DEB","ALL"};
    private final String[] windowsExecutableTypes = new String[]{"EXE" , "MSI", "ALL"};
    
    private final String CONNECTION_TESTER_URL = "http://www.google.com";
    
    private ArrayList<String> projectDirLocs= new ArrayList(5);
    
    private volatile boolean buildRunning = false;
    
    private volatile Thread backgroundThread;
        
//    private int minorVersion = 1;
//    private int majorVersion = 2;
    
    private ModuleVersionHelper productionModuleVersionHelper;
    private ModuleVersionHelper qaModuleVersionHelper;
    private Gson gson = new Gson();
    private GoogleCloudBuildConnectionEntity gogBuildConnectionEntity;
    private BuildVars buildVars;
    
    public BuildUIHandler() { }
    
    public void stop() { }    
    
    public Parent getParent() {
        return mainUI;
    }
    
    public void makeBusy(boolean busy) {
        makeBusy(busy, null);
    }
    
    public void makeBusy(boolean busy, String msg) {
        Platform.runLater(() -> {
            busyPane.toFront();
            busyPane.setVisible(busy); 
            
            busyLabel.setVisible(msg != null);
            busyLabel.setText(msg != null ? msg : "");
            
            if (busy) {
                busyPane.requestFocus();
            }
        });
    }
    
    public boolean hasConnectivity() {
        try {
            URLConnection conn = new URL(CONNECTION_TESTER_URL).openConnection();
            conn.setReadTimeout(10000);
            conn.getInputStream();
            return true;
        } catch (Exception e) {
            if(!(e instanceof java.net.UnknownHostException)){ // i do not know why
                e.printStackTrace();
            }
            return false;
        }
    }
        
    public void init() {
        Thread th = new Thread(() -> {
            try {
                
                try {
                    File bannerFile = new File(getModuleBuildName() + "_banner");
                    if (bannerFile.exists()) {
                        try {
                            byte[] data = Files.readAllBytes(bannerFile.toPath());
                            bannerTextArea.setText(new String(data, "UTF-8"));
                        } catch (IOException ex) { log.error("Error", ex); }
                    }
                    
                    File youtubeFile = new File(getModuleBuildName() + "_youtube");
                    if (youtubeFile.exists()) {
                        try {
                            byte[] data = Files.readAllBytes(youtubeFile.toPath());
                            youtubeTextArea.setText(new String(data, "UTF-8"));
                        } catch (IOException ex) { log.error("Error", ex); }
                    }
                    
                    String[] res = getOnlineServerDetails();
                    onlineServerHost.setText(res[0]);
                    onlineServerPort.setText(res[1]);
                    onlineServerUser.setText(res[2]);
                    onlineServerPassword.setText(res[3]);
                    onlineServerDatabase.setText(res[4]);
                } catch (Throwable ex) { log.error("Error", ex); }
                
                makeBusy(true, "Read cloud info");
                
                if (hasConnectivity()) {
                    forceEnableCloudMechanics();
                    getGogBuildConnectionEntity().initialize();
                    Page<Blob> blobs = getGogBuildConnectionEntity().getVersionBucket().list();
                    Iterator<Blob> blobIterator = blobs.iterateAll().iterator();
                    while (blobIterator.hasNext()) {
                        Blob blob = blobIterator.next();
                        if (blob.getName().equalsIgnoreCase(GoogleCloudBuildConnectionEntity.versionPROD_ID)) {
                            productionModuleVersionHelper = gson.fromJson(new String(blob.getContent()), ModuleVersionHelper.class);
                            productionModuleVersionHelper.setBuildtype((byte)1);
                            productionModuleVersionHelper.setGogBuildConnectionEntity(getGogBuildConnectionEntity());
                            productionModuleVersionHelper.syncCurrentState();
                        } else if (blob.getName().equalsIgnoreCase(GoogleCloudBuildConnectionEntity.versionQA_ID)) {
                            qaModuleVersionHelper = gson.fromJson(new String(blob.getContent()), ModuleVersionHelper.class);
                            qaModuleVersionHelper.setBuildtype((byte)2);
                            qaModuleVersionHelper.setGogBuildConnectionEntity(getGogBuildConnectionEntity());
                            qaModuleVersionHelper.syncCurrentState();
                        }
                    }
                } else {
                    forceDisableCloudMechanics();
                    Platform.runLater(() -> popUpErrorDailog("CHECK CONNECTIVITY"));
                }
                
                makeBusy(false);
            } catch (Throwable ex) {
                log.error("Error", ex);
                makeBusy(true, "Init error...");
            }
        });
        th.setDaemon(true);
        th.start();
    }
    
    private ModuleVersionHelper getCurrentModuleVersionHelper(){
        return getModuleVersionHelperFromBuildType((byte) (getSelectionFromBox(app_build_box).charAt(0) == 'P' ? 1 : 2));
    }
    private ModuleVersionHelper getModuleVersionHelperFromBuildType(byte buildType){
        if(buildType == 1){
            if(productionModuleVersionHelper == null){
                productionModuleVersionHelper = new ModuleVersionHelper(buildType, getGogBuildConnectionEntity());
            }
            return productionModuleVersionHelper;
        }else {
            if(qaModuleVersionHelper == null){
                qaModuleVersionHelper = new ModuleVersionHelper(buildType, getGogBuildConnectionEntity());
            }
            return qaModuleVersionHelper;
        }
    }
    
    private void initBuilderBuilds(){        
        if(getCurrentModuleVersionHelper().getVersions_array().isEmpty()){
            projectVersionLabel.setText(getCurrentModuleVersionHelper().getCurrent_version());
        }else{
            String[] vers = getCurrentModuleVersionHelper().getHighestVersionAvailable().split("\\.");            
            projectVersionLabel.setText(vers[0]+ "." + (Integer.valueOf(vers[1]) + 1));
        }
        binary_name_textfield.setText(getTemplateName());
    }
    
    private void initManagerialBuilds() {
        makeBusy(true, "Retrieving project details..");
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    versionsQaTable.getItems().clear();
                    versionsProductionTable.getItems().clear();
                });

                // load list of already existed QA versions
                ObservableList<VersionInfo> qaList = FXCollections.observableArrayList();
                String qa_prefix = getModuleBuildName() + "_qa_";
                Page<Bucket> buckets = getGogBuildConnectionEntity().getStorage().list(Storage.BucketListOption.prefix(qa_prefix));
                Iterator<Bucket> bucketIterator = buckets.iterateAll().iterator();
                while (bucketIterator.hasNext()) {
                    try {
                        Bucket bucket = bucketIterator.next();
                        VersionInfo vi = new VersionInfo();
                        vi.bucket = bucket;
                        vi.type = (byte) 2;
                        String[] version = bucket.getName().substring(qa_prefix.length()).split("_");
                        if (version.length == 2) {
                            vi.major = Integer.valueOf(version[0]);
                            vi.minor = Integer.valueOf(version[1]);
                        }

                        qaList.add(vi);

                    } catch (Throwable ex) { }
                }

                ObservableList<VersionInfo> productionList = FXCollections.observableArrayList();
                String production_prefix = getModuleBuildName() + "_production_";
                buckets = getGogBuildConnectionEntity().getStorage().list(Storage.BucketListOption.prefix(production_prefix));
                bucketIterator = buckets.iterateAll().iterator();
                while (bucketIterator.hasNext()) {
                    try {
                        Bucket bucket = bucketIterator.next();
                        VersionInfo vi = new VersionInfo();
                        vi.bucket = bucket;
                        vi.type = (byte) 1;
                        String[] version = bucket.getName().substring(production_prefix.length()).split("_");
                        if (version.length == 2) {
                            vi.major = Integer.valueOf(version[0]);
                            vi.minor = Integer.valueOf(version[1]);
                        }
                        productionList.add(vi);

                    } catch (Throwable ex) { }
                }

                Platform.runLater(() -> {

                    versionsProductionTable.setItems(productionList);
                    versionsQaTable.setItems(qaList);

                    Comparator<VersionInfo> comparator = (VersionInfo o1, VersionInfo o2) -> {
                        int result = o2.major - o1.major;
                        if (result == 0) {
                            return o2.minor - o1.minor;
                        }

                        return result;
                    };

                    versionsQaTable.getItems().sort(comparator);
                    versionsProductionTable.getItems().sort(comparator);

                    ((ListView<String[]>) mac_tab.getContent()).getItems().clear();
                    ((ListView<String[]>) windows_tab.getContent()).getItems().clear();
                    ((ListView<String[]>) linux_tab.getContent()).getItems().clear();
                });                
                makeBusy(false);
            }
        });
        th.setDaemon(true);
        th.start();
    }
    
    private String deriveDirectLinkFromCloudBucket(String bucketName, String filename){
        //http://storage.googleapis.com/[BUCKET_NAME]/[OBJECT_NAME]
        return 
                "http://storage.googleapis.com/" 
                + bucketName
                + "/"
                + filename
                ;
    }
    
    private Connection getConnection() {
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
            
            Map<String, String> m = new Gson().fromJson(new JsonReader(new FileReader(".." + File.separatorChar + "Installers" + File.separatorChar + "public_server_credentials.json")), HashMap.class);
            
            return DriverManager.getConnection("jdbc:mysql://" + m.get("host") + ":3306", m.get("user"), m.get("password"));
            
        } catch (Throwable ex) {
            log.error("Error", ex);
        }
        
        return null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        bannerTextArea.disableProperty().bind(disableBanner.selectedProperty());
        
        uploadExecutableCheckBox.disableProperty().bind(executable_option_NO.selectedProperty());
        
        initiate_build_button.disableProperty().bind(view_build_info_button.disableProperty());
        
        versionsTab.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue == qaTab) {
                if (!versionsQaTable.getItems().isEmpty()) {
                    versionsQaTable.getSelectionModel().select(0);
                }
            } else if (newValue == productionTab) {
                if (!versionsProductionTable.getItems().isEmpty()) {
                    versionsProductionTable.getSelectionModel().select(0);
                }
            }
        });
        
        TableColumn queryQAVersionMajorColumn = createColumn("QA Version major", "major");
        versionsQaTable.getColumns().add(queryQAVersionMajorColumn);        
        TableColumn queryQAVersionMinorColumn = createColumn("QA Version minor", "minor");
        versionsQaTable.getColumns().add(queryQAVersionMinorColumn);        
        versionsQaTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends VersionInfo> observable, VersionInfo oldValue, VersionInfo newValue) -> {
            versionSelected(newValue);
            if (newValue != null) {
                Platform.runLater(() -> {
                    String vers = newValue.major + "." + newValue.minor;
                    qa_makeCurrentVersionButton.setDisable(vers.equalsIgnoreCase(getModuleVersionHelperFromBuildType((byte) 2).getCurrent_version()));
                });
            }
        });
        
        TableColumn queryProductionMajorVersionColumn = createColumn("Production Version major", "major");
        versionsProductionTable.getColumns().add(queryProductionMajorVersionColumn);
        TableColumn queryProductionMinorVersionColumn = createColumn("Production Version minor", "minor");
        versionsProductionTable.getColumns().add(queryProductionMinorVersionColumn);
        versionsProductionTable.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends VersionInfo> observable, VersionInfo oldValue, VersionInfo newValue) -> {
            versionSelected(newValue);
            if (newValue != null) {
                Platform.runLater(() -> {
                    String vers = newValue.major + "." + newValue.minor;
                    prod_makeCurrentVersionButton.setDisable(vers.equalsIgnoreCase(getModuleVersionHelperFromBuildType((byte) 1).getCurrent_version()));
                });
            }
        });
        
        EventHandler<ActionEvent> introHandlers = (ActionEvent t) -> {
            if (t.getSource() == view_build_info_button) {
                initManagerialBuilds();                
                fillContainer();                
                view_info_container.toFront();
                showFooter(false);
                
            } else if (t.getSource() == initiate_build_button) {
                selectAllDefaults();
                Platform.runLater(() -> {
                    ui_container.toFront();
                    showFooter(true);
                    initBuilderBuilds();
                });
                
            } else if (t.getSource() == backButton) {
                frontalPage.toFront();
                
            } else if (t.getSource() == moveToProductionButton) {
                moveToProduction();
                
            } else if (t.getSource() == deleteQAButton) {
                deleteVersion(versionsQaTable.getSelectionModel().getSelectedItem(), (byte)2);
                versionsQaTable.getItems().remove(versionsQaTable.getSelectionModel().getSelectedItem());
                
            } else if (t.getSource() == deleteProductionButton) {
                deleteVersion(versionsProductionTable.getSelectionModel().getSelectedItem(), (byte)1);
                versionsProductionTable.getItems().remove(versionsProductionTable.getSelectionModel().getSelectedItem());
                
            } else if (t.getSource() == increaseMajorButton) {
                increaseMajorVersion();
            }
        };
        
        view_build_info_button.setOnAction(introHandlers);
        initiate_build_button.setOnAction(introHandlers);
        view_build_info_button.setDisable(true);
        
//        return_from_info_page_to_main_button.setOnAction(introHandlers);
        backButton.setOnAction(introHandlers);
        
        moveToProductionButton.setOnAction(introHandlers);        
        moveToProductionButton.disableProperty().bind(versionsQaTable.getSelectionModel().selectedItemProperty().isNull());
        
        deleteQAButton.setOnAction(introHandlers);        
        deleteQAButton.disableProperty().bind(versionsQaTable.getSelectionModel().selectedItemProperty().isNull());
        
        deleteProductionButton.setOnAction(introHandlers);        
        deleteProductionButton.disableProperty().bind(versionsProductionTable.getSelectionModel().selectedItemProperty().isNull());
        
        increaseMajorButton.setOnAction(introHandlers);
        
        binary_name_textfield.setDisable(true);
        
        populateApplicationModeBox();
        populateApplicationBuildServiceBox();
        populateCloudServiceBox();
        populateExecutableTypeBox();
        populateModuleBuildsBox();
        productionLineInitiation();
        executableLineInitiation();
        certificateActivationLineInitiation();
        certificateTimestampLineInitiation();
//        populateUpdateCheckAtStart();        
        
        cancelbuildButton.visibleProperty().bind(buildButton.disabledProperty());
        
        buildButton.setOnAction((ActionEvent t) -> {
            try{
                if(binary_name_textfield.getText().isEmpty()){ 
                    popUpErrorDailog("YOU NEED TYPE THE FINAL NAME OF APPLICATION");
                    return;
                }
                
                if(build_prod_option_YES.isSelected() && (!uploadUpdateCheckBox.isSelected() &&
                        !uploadExecutableCheckBox.isSelected())){
                    popUpErrorDailog("YOU NEED SELECT AT LEAST 1 TYPE OF UPLOAD");
                    return;
                }
                
                buildButton.setDisable(true);
                backButton.setDisable(true);
                
                //set the system properties
                System.setProperty(BuildVars.EXECUTE_BUILD_FLAG, String.valueOf(executable_option_YES.isSelected()));
                System.setProperty(BuildVars.PRODUCTION_BUILD_FLAG, String.valueOf(build_prod_option_YES.isSelected()));
                
                updateInformationLabel_Circle("Starting build.", Color.YELLOW);
                animateCircle();
                informationLabel.setVisible(false);
                code_front.toFront();
                running_build_footer.toFront();
                
                streamers();
                
                callBuildSystem();
            }catch(Exception e){ e.printStackTrace(); }
            finally{
                //some finalization clause
            }
        });
        
        cancelbuildButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) { 
                if(backgroundThread == null)
                    return;
                backgroundThread.interrupt();
                backgroundThread = null;
                updateInformationLabel_Circle("Cancelling build process......", Color.TOMATO);
            }
        });   
        
        prod_makeCurrentVersionButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) { 
                setProductionVersionAsCurrent();
            }
        });
        
        qa_makeCurrentVersionButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) { 
                setQaVersionAsCurrent();
            }
        });
    }
    
    private void increaseMajorVersion() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Do realy want to increase major version?");
        
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.YES || result.get() == ButtonType.OK) {
            String[] vers = projectVersionLabel.getText().split("\\.");
            projectVersionLabel.setText((Integer.valueOf(vers[0])+1) + ".1");
        }
    }
    
    private void deleteVersion(VersionInfo vi, byte buildType) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Do realy want to delete selected version?");
        
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.YES || result.get() == ButtonType.OK) {
            
            makeBusy(true, "Deleting " + vi.bucket.getName());
            
            Thread th = new Thread(() -> {
                Page<Blob> blobs = vi.bucket.list();
                Iterator<Blob> blobIterator = blobs.iterateAll().iterator();
                
                // read count of blobs
                int count = 0;
                while (blobIterator.hasNext()) {
                    blobIterator.next();
                    count++;                    
                }

                int i = 1;
                blobIterator = blobs.iterateAll().iterator();
                while (blobIterator.hasNext()) {
                    makeBusy(true, "Deleting version " + vi.major + "." + vi.minor + " - " + i + " of " + count);
                    i++;
                    
                    blobIterator.next().delete(Blob.BlobSourceOption.metagenerationMatch());
                }
                
                vi.bucket.delete(BucketSourceOption.metagenerationMatch());
                
                ModuleVersionHelper mvh = getModuleVersionHelperFromBuildType(buildType);
                String vers = vi.major + "." + vi.minor;
                mvh.removeVersion(vers);
                mvh.updateCurrentState();                
                makeBusy(false);
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    /**
     * NOT USED FOR NOW
     * NEED CHANGE LOGIC
     */
    private void autoDeleteVersion() {
        makeBusy(true);
        
        List<VersionInfo> list = new ArrayList<>();
        int qaSize = versionsQaTable.getItems().size();
        if (qaSize > 3) {
            for (int i = 3; i < qaSize; i++) {
                list.add(versionsQaTable.getItems().get(i));
            }
        }

        int productionSize = versionsProductionTable.getItems().size();
        if (productionSize > 3) {
            for (int i = 3; i < productionSize; i++) {
                list.add(versionsProductionTable.getItems().get(i));
            }
        }
        
        makeBusy(false);
        
        Thread th = new Thread(() -> {
            for (VersionInfo vi: list) {
                Page<Blob> blobs = vi.bucket.list();
                Iterator<Blob> blobIterator = blobs.iterateAll().iterator();

                // read count of blobs
                int count = 0;
                while (blobIterator.hasNext()) {
                    blobIterator.next();
                    count++;                    
                }

                int i = 1;
                blobIterator = blobs.iterateAll().iterator();
                while (blobIterator.hasNext()) {
                    makeBusy(true, "Auto deleting old " + (vi.type == (byte)2 ? "QA" : "Production") + " version " + vi.major + "." + vi.minor + " - " + i + " of " + count);
                    i++;
                    
                    blobIterator.next().delete(Blob.BlobSourceOption.metagenerationMatch());
                }
                
                vi.bucket.delete(BucketSourceOption.metagenerationMatch());
            }
            
        });
        th.setDaemon(true);
        th.start();
    }
    
    private void moveToProduction() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Do realy want move QA version to Production?");
        
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.YES || result.get() == ButtonType.OK) {
            
            // check if selected QA version already in PRODUCTION
            VersionInfo vi = versionsQaTable.getSelectionModel().getSelectedItem();
            if (vi != null) {
                makeBusy(true, "Moving to production");
                Thread th = new Thread(() -> {
                    String qaBucketName = vi.bucket.getName();
                    String productionBucketName = qaBucketName.replaceAll("qa", "production");

                    Bucket productionBucket = getGogBuildConnectionEntity().getStorage().get(productionBucketName, Storage.BucketGetOption.fields(Storage.BucketField.NAME));
                    if (productionBucket == null) {                    
                        productionBucket = getGogBuildConnectionEntity().getStorage().create(BucketInfo.of(productionBucketName));                    
                    } 

                    Page<Blob> blobs = vi.bucket.list();
                    Iterator<Blob> blobIterator = blobs.iterateAll().iterator();

                    // read count of blobs
                    int count = 0;
                    while (blobIterator.hasNext()) {
                        blobIterator.next();
                        count++;                    
                    }

                    int i = 1;
                    blobIterator = blobs.iterateAll().iterator();
                    while (blobIterator.hasNext()) {

                        makeBusy(true, "Moving to production " + i + " of " + count);
                        i++;

                        Blob blob = blobIterator.next();
                        if (blob.getName().equals(BuildVars.APP_INFO_CFG)) {

                            try {
                                StringBuilder sb = new StringBuilder();
                                try (ReadChannel reader = blob.reader()) {
                                    ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
                                    while (reader.read(bytes) > 0) {
                                        bytes.flip();
                                        sb.append(new String(bytes.array(), "UTF-8"));
                                        bytes.clear();
                                    }
                                }

                                // replace all qa links to production
                                String content = sb.toString().trim();
                                content = content.replaceAll(/*"http://storage.googleapis.com/" + getModuleBuildName() + */"_qa",
                                        /*"http://storage.googleapis.com/" + getModuleBuildName() + */"_production");

                                Blob serverBlobFile = productionBucket.get(BuildVars.APP_INFO_CFG);
                                if (serverBlobFile == null) {
                                    
                                    BlobInfo serverBlobInfoFile = BlobInfo.newBuilder(BlobId.of(productionBucket.getName(), BuildVars.APP_INFO_CFG))
                                            .setCacheControl("no-cache")
                                            .setAcl(getGogBuildConnectionEntity().getDefaultAcls())
                                            .build();
                                    serverBlobFile = getGogBuildConnectionEntity().getStorage().create(serverBlobInfoFile);
                                } 

                                try (WriteChannel channel = serverBlobFile.writer()) {
                                    channel.write(ByteBuffer.wrap(content.getBytes("UTF-8")));
                                }

                            } catch (Throwable ex) {
                                log.error("Error", ex);
                            }
                        }else {
                            CopyWriter cw = blob.copyTo(productionBucketName);
                            getGogBuildConnectionEntity().getStorage().createAcl(cw.getResult().getBlobId(),
                                    getGogBuildConnectionEntity().getDefaultAcls().get(0));
//                            if (blob.getName().contains("_executable/")) {
//                                getGogBuildConnectionEntity().makeFileAccesibleOnPublicExecutableBucket(blob);
//                            }
                        }
                    }
                    makeBusy(true, "Updating version information");
                    String vers = vi.major + "." + vi.minor;
                    ModuleVersionHelper versionHelper = getModuleVersionHelperFromBuildType((byte)1);
                    versionHelper.setCurrentVersion(vers);
                    versionHelper.updateCurrentState();
                    makeBusy(false);
                });
                th.setDaemon(true);
                th.start();
            }
        }
    }
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        column.setSortable(false);
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }
    
    private Callback<ListView<String[]>, ListCell<String[]>> getAllListViewCallBackInView(){
        return new Callback<ListView<String[]>, ListCell<String[]>>() {
                @Override
                public ListCell<String[]> call(ListView<String[]> p) {
                    return new ListCell<String[]>(){
                        
                        HBox parent;
                        Label text;
                        Button copyButton;
                        
                        {
                            parent = new HBox(10);
                            parent.setAlignment(Pos.CENTER);
                            text = new Label();
                            text.setStyle("-fx-text-fill: -fx-dark-blue-color");
                            copyButton = new Button("COPY DIRECT LINK");
                            copyButton.setBackground(Background.EMPTY);
                            copyButton.setStyle("-fx-border-color: transparent transparent transparent -fx-dark-blue-color;"
                                    + "-fx-text-fill: -fx-dark-blue-color");
                            copyButton.setMinWidth(80.0);
                            HBox.setHgrow(text, Priority.ALWAYS);
                            HBox.setHgrow(copyButton, Priority.NEVER);
                            text.setMaxWidth(Double.MAX_VALUE);
                            parent.setMaxHeight(30);
                            parent.setPrefHeight(30);
                            setGraphic(parent);
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            copyButton.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent t) {
                                    ClipboardContent cc = new ClipboardContent();
                                    cc.putString(getItem()[1]);
                                    Clipboard.getSystemClipboard().setContent(cc);
                                    Platform.runLater(()->{
                                        popUpDailog("URL COPIED TO CLIPBOARD!!!");                                        
                                    });
                                }
                            });
                            
                            parent.getChildren().addAll(text,copyButton);
                        }
                        
                        @Override
                        public void updateSelected(boolean bln) {
                            super.updateSelected(bln);
                            if(bln){
                                
                            }else{
                                
                            }
                        }

                        @Override
                        protected void updateItem(String[] t, boolean bln) {
                            super.updateItem(t, bln); //To change body of generated methods, choose Tools | Templates.
                            if(bln){
                                copyButton.setVisible(false);
                                text.setText("");
                            }else{
                                copyButton.setVisible(true);
                                text.setText(t[0]);
                            }
                        }
                        
                    };
                }
            };
    }
    
    private void versionSelected(VersionInfo vi) {
        makeBusy(true);
        //reset
        libs_listview.getItems().clear();
        ((ListView<String[]>) mac_tab.getContent()).getItems().clear();
        ((ListView<String[]>) windows_tab.getContent()).getItems().clear();
        ((ListView<String[]>) linux_tab.getContent()).getItems().clear();
        Thread th = new Thread(() -> {
            if (vi != null) {
                Page<Blob> blobs = vi.bucket.list();
                Iterator<Blob> blobIterator = blobs.iterateAll().iterator();
                while (blobIterator.hasNext()) {
                    Blob blob = blobIterator.next();
                    if (blob.getName().equals(BuildVars.APP_INFO_CFG)){
                        //do nothing
                    }else {
                        String url = deriveDirectLinkFromCloudBucket(vi.bucket.getName(), blob.getName());
                        boolean jar = blob.getName().endsWith(".jar");
                        int indexOfSlash = blob.getName().indexOf('/');
                        String name;
                        if(indexOfSlash > -1){
                            name = blob.getName().substring(indexOfSlash + 1);
                        }else{
                            name = blob.getName();
                        }

                        if (jar) {
                            Platform.runLater(() -> libs_listview.getItems().add(new String[]{name, url}));
                        } else if(vi.type == (byte)2){
                            insertExectuableBlobOnUI(name, url);
                        }
                    }
                }
                
                if (vi.type == (byte) 1) {
                    blobs = getGogBuildConnectionEntity().getExecutableBucket().list();
                    blobIterator = blobs.iterateAll().iterator();
                    while (blobIterator.hasNext()) {
                        Blob blob = blobIterator.next();
                        String url = deriveDirectLinkFromCloudBucket(getGogBuildConnectionEntity().getExecutableBucket().getName(),
                                blob.getName());
                        
                        int indexOfSlash = blob.getName().indexOf('/');
                        String name;
                        if(indexOfSlash > -1){
                            name = blob.getName().substring(indexOfSlash + 1);
                        }else{
                            name = blob.getName();
                        }
                        insertExectuableBlobOnUI(name, url);
                    }
                }
            }            
            makeBusy(false);
        });
        th.setDaemon(true);
        th.start();
    }
    
    
    private void fillContainer(){
        if(view_info_container.getUserData() == null){
            view_info_container.setUserData(new Object());
            
            libs_listview.setCellFactory(getAllListViewCallBackInView());
            ((ListView<String[]>)linux_tab.getContent()).setCellFactory(getAllListViewCallBackInView());
            ((ListView<String[]>)mac_tab.getContent()).setCellFactory(getAllListViewCallBackInView());
            ((ListView<String[]>)windows_tab.getContent()).setCellFactory(getAllListViewCallBackInView());
        }
        
        //reset
        libs_listview.getItems().clear();       
    }
    
    public void showFooter(boolean withBuild)  {
        running_build_footer.toFront();
        informationStatusIcon.setVisible(withBuild);
        informationLabel.setVisible(withBuild);
        buildButton.setVisible(withBuild);
        backButton.setDisable(false);
    }
    
    private void callBuildSystem() {
        String levelSwap = level_swap_radiobutton.isSelected() ? level_swap_textfield.getText()
                            : "-1";
        if (levelSwap.trim().isEmpty()) {
            levelSwap = "-1";
        } else {
            try {
                Byte.valueOf(levelSwap);
            } catch (NumberFormatException ex) {
                levelSwap = "-1";
            }
        }
        
        String staticLevelSwap = levelSwap;
        backgroundThread = new Thread(() -> {
            try {
                String name = binary_name_textfield.getText().trim();
                updateInformationLabel_Circle("Building " + name + "...", Color.YELLOW);
                
                boolean production = getSelectionFromBox(app_build_box).charAt(0) == 'P';
                
                String encryptedString = null;
                
                try (FileOutputStream fs = new FileOutputStream("info")) {                      
                    Cipher cipher = Cipher.getInstance("DESede");
                    DESedeKeySpec ks = new DESedeKeySpec(encrytionkeyField.getText().getBytes("UTF8"));
                    SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
                    cipher.init(Cipher.ENCRYPT_MODE, skf.generateSecret(ks));
                    
                    String text = onlineServerHost.getText() + "$$" +
                        onlineServerPort.getText() + "$$" +
                        onlineServerUser.getText() + "$$" +
                        onlineServerPassword.getText() + "$$" +
                        onlineServerDatabase.getText();

                    byte[] plainText = text.getBytes("UTF-8");
                    byte[] encryptedText = cipher.doFinal(plainText);

                    encryptedString = new String(Base64.encodeBase64(encryptedText));

                    fs.write(encryptedString.getBytes("UTF-8"));
                }
                
                BuildEntry.main(new String[]{
                    projectVersionLabel.getText(),
                    getProjectDirectory(),
                    getModuleBuildName(),
                    name,
                    getSelectionFromBox(executable_type_box),
                    getSelectionFromBox(app_mode_box).charAt(0) == 'A' ? "1" : "2",
                    constructBuildEntities(),
                    constructAppSystemProps(),
                    generateArguments(),
                    staticLevelSwap,
                    latest_build_info_area.getText(),
                    production ? "1" : "2",
                    getExecutableCertificateState(),
                    disableBanner.isSelected() ? "" : bannerTextArea.getText(),
                    youtubeTextArea.getText(),
                    encryptedString,
                    encrytionkeyField.getText()
                });
                
                File bannerFile = new File(getModuleBuildName() + "_banner");
                try {
                    if (bannerFile.exists()) {
                        Files.delete(bannerFile.toPath());
                    }
                    Files.write(bannerFile.toPath(), bannerTextArea.getText().getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                } catch (Throwable th) {
                    log.error("Error", th);
                }
                
                File youtubeFile = new File(getModuleBuildName() + "_youtube");
                try {
                    if (youtubeFile.exists()) {
                        Files.delete(youtubeFile.toPath());
                    }
                    Files.write(youtubeFile.toPath(), youtubeTextArea.getText().getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                } catch (Throwable th) {
                    log.error("Error", th);
                }
                
                if (uploadUpdateCheckBox.isSelected() && build_prod_option_YES.isSelected()) {
//                    ModuleVersionHelper versionHelper = getCurrentModuleVersionHelper();
                    qaModuleVersionHelper.setCurrentVersion(projectVersionLabel.getText().trim());
                    qaModuleVersionHelper.updateCurrentState();
                    
                    productionModuleVersionHelper.setCurrentVersion(projectVersionLabel.getText().trim());
                    productionModuleVersionHelper.updateCurrentState();
                }
            } catch(Exception e) {
                if (e instanceof IOException) {
                    Platform.runLater(() -> {
                        popUpErrorDailog("PLEASE RUN A FULL BUILD ON SMARTSQL PARENT MODULE");
                    });
                }
                e.printStackTrace();
            } finally {
                Platform.runLater(() -> {
                    buildRunning = false;
                    buildButton.setDisable(false);
                    backButton.setDisable(false);
                    ui_container.toFront();
                    updateInformationLabel_Circle("", Color.TRANSPARENT);
                    if (informationStatusIcon.getUserData() != null) {
                        ((Transition) informationStatusIcon.getUserData()).stop();
                    }
                    if(BuildEntry.hasLastBuildFinishedSuccessfully()){
                        fillContainer();
                    }else{
                        frontalPage.toFront();
                    }
                });
                System.gc(); System.gc(); System.gc(); System.gc();
            }
        });

        backgroundThread.setDaemon(true);
        backgroundThread.start();
    }
    
    private String generateArguments() {        
        String text = app_sys_props_area.getText();
        return text;
    }

    private String constructAppSystemProps() {
        //BINARY_INSTANCE_TYPE (This flag is static)1  BINARY_PACKAGE_TYPE 2  CHECK UPDATE AT APP START true
        StringBuilder sb = new StringBuilder(
                "-BjvmOptions=-XX:MinHeapFreeRatio=5 -BjvmOptions=-XX:MaxHeapFreeRatio=50 -BjvmOptions=-Xms100m -BjvmOptions=-XX:NewRatio=2");
        
        sb.append(" -BjvmProperties=project_name=").append(getModuleBuildName());
        sb.append(" -BjvmProperties=version=").append(projectVersionLabel.getText());
        sb.append(" -BjvmProperties=pack_type=").append(getSelectionFromBox(app_mode_box).charAt(0) == 'A' ? "1" : "2");
        sb.append(" -BjvmProperties=buildType=").append(getSelectionFromBox(app_build_box).charAt(0) == 'P' ? "1" : "2"); // PRODUCTION or QA app
                
        return sb.toString();
    }
    
    private String constructBuildEntities(){
        StringBuilder sb = new StringBuilder(200);
        if(uploadUpdateCheckBox.isSelected()){
            sb.append(BuildEntry.BUILD_VARAGS_UPDATE);
        }
        
        if(uploadExecutableCheckBox.isSelected()){
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append(BuildEntry.BUILD_VARAGS_BINARIES);
        }
        
        return sb.toString();
    }
    
    private String getProjectDirectory() {
       File projectPath = new File("").getAbsoluteFile().getParentFile();
       if(projectPath == null || !projectPath.getName().equalsIgnoreCase(PROJECT_NAME)){
           SimpleStringProperty sp = new SimpleStringProperty();
           popUpOptionsDailog(sp);
           return sp.getValue();
       }
       return projectPath.getAbsolutePath();
    }
    
    private void populateModuleBuildsBox(){
        modulesSelectorChoiceBox.getItems().clear();
                
        modulesSelectorChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                Platform.runLater(()-> {
                    view_build_info_button.setDisable(newValue == null || newValue.isEmpty());
                    getGogBuildConnectionEntity().setProjectName(getModuleBuildName());
                    init();
                });
            }
        });
        
        makeBusy(true, "RETRIEVING EXECUTABLE MODULES");
        
        Platform.runLater(()->{
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    FileFilter executableModulesFolderRetriever = new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            boolean isExeModuleFolder = false;
                            
                            if(pathname.isDirectory() && pathname.getName().charAt(0) == 's' 
                                    && pathname.getName().startsWith("smart")){
                                File target = new File(pathname, "target");
                                isExeModuleFolder = new File(target, "main_app.jar").exists();
                            }
                            
                            return isExeModuleFolder;
                        }
                    };                    
                    
                    for (File f : new File(getProjectDirectory()).listFiles(executableModulesFolderRetriever)) {
                        Platform.runLater(()-> modulesSelectorChoiceBox.getItems().add(f.getName()) );
                        System.gc();
                    }
                    
                    Platform.runLater(()-> {
                        //if modules selector choice box is empty it means we have not executable module to build
                        // that means developer not MAKE FULL CLEAN BUILD ON PROJECT
                        makeBusy(modulesSelectorChoiceBox.getItems().isEmpty(), "ERROR FINDING EXECUTABLES \n Make a full clean build");
                    });
                    
                }
            });
            t.setDaemon(true);
            t.start();
        });
    }
    
    private String getModuleBuildName(){
        return getSelectionFromBox(modulesSelectorChoiceBox).replace('-', '_');
    }
    
    private String getSelectionFromBox(ChoiceBox<String> c){
        return c.getSelectionModel().getSelectedItem();
    }
    
    private void popUpOptionsDailog(StringProperty sp) {
        CountDownLatch mutex = null;
        if (!Platform.isFxApplicationThread()) {
            mutex = new CountDownLatch(1);
            
            CountDownLatch staticMutex = mutex;
            Platform.runLater(() -> {
                popUpOptionsDailogCheckingThread(staticMutex, sp);
            });
            
        } else {
            popUpOptionsDailogCheckingThread(mutex, sp);
        }
        
        
        if (mutex != null) {
            try {
                mutex.await();
            } catch (InterruptedException ex) {
                log.error("Error", ex);
            }
        }
        
    }
    
    private void popUpOptionsDailogCheckingThread(CountDownLatch mutex, StringProperty sp) {
         
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle("Select your project directory:");
        dc.setInitialDirectory(new File("").getAbsoluteFile().getParentFile());
        
        File projectDir = dc.showDialog(null);
        
        if (projectDir != null) {
            sp.set(projectDir.getAbsolutePath());
        }
        if (mutex != null) {
            mutex.countDown();
        }
    }

    private void popUpErrorDailog(String text) {
        popUpDailog(text);
        dailogStage.close();
        dailogStage.getScene().setFill(Color.TOMATO);
        ((Label)dailogStage.getScene().getRoot().getChildrenUnmodifiable().get(0))
                .setTextFill(Color.WHITE);
        dailogStage.showAndWait();
    }
    
    private void popUpDailog(String text) {
        if (dailogStage == null) {
            try {
                dailogStage = new Stage(StageStyle.UTILITY);
                dailogStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("Dialog.fxml"))));
                dailogStage.initModality(Modality.WINDOW_MODAL);
                dailogStage.initOwner(parentStage);

                EventHandler<MouseEvent> dailogStageCloserHandler = new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        dailogStage.close();
                    }
                };
                EventHandler<WindowEvent> open_close_eventhandler = new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent t) {
                        if (t.getEventType().equals(WindowEvent.WINDOW_HIDDEN)) {
                            parentStage.removeEventFilter(MouseEvent.MOUSE_CLICKED, dailogStageCloserHandler);
                        } else if (t.getEventType().equals(WindowEvent.WINDOW_SHOWN)) {
                            parentStage.addEventFilter(MouseEvent.MOUSE_CLICKED, dailogStageCloserHandler);
                        }
                    }
                };

                dailogStage.getScene().setUserData(dailogStageCloserHandler);
                dailogStage.setOnHidden(open_close_eventhandler);
                dailogStage.setOnShown(open_close_eventhandler);

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        dailogStage.getScene().setFill(Color.WHITESMOKE);
        Label l = (Label) dailogStage.getScene().getRoot().getChildrenUnmodifiable().get(0);
        l.setText(text);
        l.setTextFill(Color.BLACK);
        Font f = l.getFont();
        l.setFont(Font.font(f.getName(), FontWeight.THIN, f.getSize()));
        dailogStage.setWidth(500);
        dailogStage.setHeight(200);
        dailogStage.setX(parentStage.getX() + 15);
        dailogStage.setY(parentStage.getHeight() / 3);
        dailogStage.show();
    }
    
    private void populateApplicationModeBox(){
        app_mode_box.getItems().clear();
        app_mode_box.getItems().addAll("ALL-IN-ONE STANDALONE APPLICATION", "SELF-PACKAGEABLE APPLICATION"); 
//        app_mode_box.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
//                if(t1 != null && t1.charAt(0) == 'S'){
//                    check_update_at_start_box.setDisable(false);
//                }else{
//                    check_update_at_start_box.setDisable(true);
//                    check_update_at_start_box.getSelectionModel().selectLast();
//    }
//            }
//        });
    }
        
    private void populateExecutableTypeBox(){
        executable_type_box.getItems().clear();
        if(PlatformUtil.isUnix()){
            executable_type_box.getItems().addAll(linuxExecutableTypes);
        }else if(PlatformUtil.isMac()){
            executable_type_box.getItems().addAll(macExecutableTypes);
        }else if(PlatformUtil.isWindows()){
            executable_type_box.getItems().addAll(windowsExecutableTypes);
        }
        
        executable_type_box.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov,
                    Number t, Number t1) {
                if(t1.intValue() == 2){
                    popUpDailog("Currently 'ALL' is not supported. \n Build will resort to default type");
                     executable_type_box.getSelectionModel().selectFirst();
                }
            }
        });
    }
    
//    private void populateUpdateCheckAtStart() {
//        check_update_at_start_box.getItems().clear();
//        check_update_at_start_box.getItems().addAll("TRUE", "FALSE");
//    }
    
    private void populateApplicationBuildServiceBox(){
        app_build_box.getItems().clear();
        app_build_box.getItems().addAll("PRODUCTION", "QA");
        
        app_build_box.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(newValue != null){
                    initBuilderBuilds();
                }
            }
        });
    }
    
    private void populateCloudServiceBox(){
        cloud_service_box.getItems().clear();
        cloud_service_box.getItems().addAll("GOOGLE CLOUD STORAGE", "FTP FILE SERVER");
    }

    private void certificateTimestampLineInitiation() {
        if (PlatformUtil.isWindows()) {
            ToggleGroup certificate_timestamp_option_toggleGroup = new ToggleGroup();
            cert_timestamp_NO.setToggleGroup(certificate_timestamp_option_toggleGroup);
            cert_timestamp_YES.setToggleGroup(certificate_timestamp_option_toggleGroup);
            
            cert_timestamp_container.disabledProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        certificate_timestamp_option_toggleGroup.selectToggle(cert_timestamp_NO);
                    }
                }
            });
        }
    }
    
    private void certificateActivationLineInitiation() {
        if (PlatformUtil.isWindows()) {
            ToggleGroup certificate_activation_option_toggleGroup = new ToggleGroup();
            cert_activateCert_NO.setToggleGroup(certificate_activation_option_toggleGroup);
            cert_activateCert_YES.setToggleGroup(certificate_activation_option_toggleGroup);
//            cert_activateCert_container.disabledProperty().addListener(new ChangeListener<Boolean>() {
//                @Override
//                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) { 
//                    cert_activateCert_NO.setSelected(true);
//                }
//            });
            cert_timestamp_container.disableProperty().bind(cert_activateCert_NO.selectedProperty().or(cert_activateCert_container.disabledProperty()));
        }
    }
    
    private void productionLineInitiation() {
        ToggleGroup build_prod_option_toggleGroup = new ToggleGroup();
        build_prod_option_NO.setToggleGroup(build_prod_option_toggleGroup);
        build_prod_option_YES.setToggleGroup(build_prod_option_toggleGroup);
        build_prod_option_toggleGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) -> {
            boolean enable = t1 == build_prod_option_YES;
            cloud_service_box.setDisable(!enable);
            latest_build_info_area.setDisable(!enable);
            uploadHBox.setDisable(!enable);
        });
    }
    
    private void executableLineInitiation() {
        ToggleGroup executable_option_toggleGroup = new ToggleGroup();
        executable_option_NO.setToggleGroup(executable_option_toggleGroup);
        executable_option_YES.setToggleGroup(executable_option_toggleGroup);
        executable_option_toggleGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) -> {
            boolean enable = t1 == executable_option_YES;
            //binary_name_textfield.setDisable(!enable);
            executable_type_box.setDisable(!enable);
            app_sys_props_area.setDisable(!enable);
            cert_activateCert_container.setDisable(!enable);
            
            if (!enable) {
                uploadExecutableCheckBox.setSelected(false);
            }
        });
    }
    
    private void selectAllDefaults() {
        Platform.runLater(() -> {
            cloud_service_box.getSelectionModel().selectFirst();
            app_build_box.getSelectionModel().selectLast();
            executable_type_box.getSelectionModel().selectFirst();
            app_mode_box.getSelectionModel().selectLast();
            executable_option_YES.setSelected(true);
            cert_activateCert_YES.setSelected(true);
            boolean cloudEnabled = !build_prod_option_NO.getParent().isDisabled();
            cert_timestamp_YES.setSelected(cloudEnabled);
            uploadUpdateCheckBox.setSelected(cloudEnabled);
            uploadExecutableCheckBox.setSelected(cloudEnabled);
            build_prod_option_YES.setSelected(cloudEnabled);
        });
    }
    
    private void animateCircle() {
        FillTransition ft = new FillTransition(Duration.millis(3000), informationStatusIcon, Color.YELLOW, Color.GREEN);
        ft.setCycleCount(1000000);
        ft.setAutoReverse(true);
        
        FadeTransition ft1 = new FadeTransition(Duration.millis(3000), informationStatusIcon);
        ft1.setFromValue(1.0);
        ft1.setToValue(0.3);
        ft.setCycleCount(100000);
        ft.setAutoReverse(true);
        
        ParallelTransition pt = new ParallelTransition(ft,ft1);
        
        informationStatusIcon.setUserData(pt);
    }
    
    private void updateInformationLabel_Circle(String string, Color c) {
        Platform.runLater(() -> {
            if (string != null) {
                informationLabel.setText(string);
                informationLabel.setVisible(true);
            }
            if (c != null) {
                informationStatusIcon.setFill(c);
            }
        });
    }
    
    private void streamers() throws Exception {
        buildRunning = true;
        System.setOut(new PrintStream(new OutputStream() {
            StringBuilder sb = new StringBuilder("      STARTING BUILD..... \n\n");
            OutputStream out = System.out;
            int i = 0;

            @Override
            public void write(int b) throws IOException {
                out.write(b);
                sb.append((char) b);
                if (i > 20) {
                    i = 0;
                    Platform.runLater(() -> {
                        stream_text.setText(sb.toString());
                        log_container.setVvalue(Double.MAX_VALUE);
                    });
                }
                i++;
            }
        }));
    }

    private void forceDisableCloudMechanics() {
        build_prod_option_NO.getParent().setDisable(true);
        build_prod_option_NO.setSelected(true);
        view_info_container.setDisable(true);
        cert_timestamp_NO.setSelected(true);
        
        view_build_info_button.setDisable(true);
    }
    
    private void forceEnableCloudMechanics(){
        build_prod_option_NO.getParent().setDisable(false);      
        view_info_container.setDisable(false);
        cert_timestamp_YES.setSelected(true);
        
        view_build_info_button.setDisable(false);
    }

    /**
     * @return the gogBuildConnectionEntity
     */
    public GoogleCloudBuildConnectionEntity getGogBuildConnectionEntity() {
        if (gogBuildConnectionEntity == null) {
            try{
                buildVars = new BuildVars(getProjectDirectory(), getModuleBuildName());
                gogBuildConnectionEntity = new GoogleCloudBuildConnectionEntity(buildVars);
                //fail safe
                gogBuildConnectionEntity.initialize();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return gogBuildConnectionEntity;
    }
    
    public class VersionInfo {
        int major;
        int minor;
        
        Bucket bucket;
        
        byte type;
        
        public int getMajor() {
            return major;
        }        
        
        public int getMinor() {
            return minor;
        }        

        public Map<String, String> readLibs() {
            Blob appConfigBlob = bucket.get(BuildVars.APP_INFO_CFG);
            try (ReadChannel reader = appConfigBlob.reader()) {
                StringBuilder sb = new StringBuilder();
                ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
                try {
                    while (reader.read(bytes) > 0) {
                        bytes.flip();
                        sb.append(new String(bytes.array(), "UTF-8"));
                        bytes.clear();
                    }
                } catch (IOException ex) {
                    log.error("Error", ex);
                }
                
                if (sb.length() > 0) {
                    Map<String, Object> map = new Gson().fromJson(sb.toString().trim(), HashMap.class);
                    if (map != null && map.containsKey("dependencyDirectory")) {
                        return (Map<String, String>) map.get("dependencyDirectory");
                    }
                }
            }
            
            return null;
        }
        
    }
    
    /**
     * This method deduce the state of the certificate signing on the final exectuable on WINDOWS ONLY
     * 
     * -1 means it should not sign executable with certificate
     * 0 means it should sign executable with certificate but not timestamp it
     * 1 means it should sign and timestamp exectable with certficate
     * 
     * @return 
     * the final state on certifcate signing; ranges from -1 to 1 (all inclusive)
     */
    private String getExecutableCertificateState(){
        byte state = -1;
        state = cert_activateCert_YES.isSelected() ? ++state : state;
        state = cert_timestamp_YES.isSelected() ? ++state : state;
        return String.valueOf(state);
    }

    public void setParentStage(Stage a) {
        parentStage = a;
    }   
    
    private String[] getOnlineServerDetails() {
        try {
            String result = new BufferedReader(new FileReader("info")).lines().collect(Collectors.joining("\n"));
            Cipher cipher = Cipher.getInstance("DESede");
            DESedeKeySpec ks = new DESedeKeySpec(encrytionkeyField.getText().getBytes("UTF-8"));
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");

            cipher.init(Cipher.DECRYPT_MODE, skf.generateSecret(ks));
            byte[] encryptedText = Base64.decodeBase64(result);
            byte[] plainText = cipher.doFinal(encryptedText);

            String[] res = new String(plainText, "UTF-8").split("\\$\\$");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private void setProductionVersionAsCurrent(){
        setVersionAsCurrent((byte)1);
    }
    
    private void setQaVersionAsCurrent(){
        setVersionAsCurrent((byte)2);
    }
        
    private void setVersionAsCurrent(byte buildType) {
        makeBusy(true);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                VersionInfo vi = null;
                if (buildType == (byte) 1) {
                    vi = versionsProductionTable.getSelectionModel().getSelectedItem();
                } else {
                    vi = versionsQaTable.getSelectionModel().getSelectedItem();
                }
                
                ModuleVersionHelper mvh = getModuleVersionHelperFromBuildType(buildType);
                String vers = vi.major + "." + vi.minor;
                makeBusy(true, "Setting current version as " + vers);
                mvh.setCurrentVersion(vers);
                mvh.updateCurrentState();
                makeBusy(false);
            }
        });
        t.setDaemon(true);
        t.start();
    }
   
    private String getTemplateName() {
        String templateAppName = getModuleBuildName();
        templateAppName = templateAppName.substring(templateAppName.lastIndexOf('_') + 1).toUpperCase();
        
        if (templateAppName.equalsIgnoreCase("UI")) {
            templateAppName = "QB";
        }
        
        if (getSelectionFromBox(app_build_box).charAt(0) != 'P') {
            templateAppName = templateAppName + "_QA";
        }
        templateAppName = "SmartMySQL" + templateAppName;
        
        return templateAppName;
    }
    
    
    private void insertExectuableBlobOnUI(String blobName, String url) {
        
        for (String s : macExecutableTypes) {
            if (blobName.endsWith(s.toLowerCase())) {
                Platform.runLater(() -> {
                    ((ListView<String[]>) mac_tab.getContent()).getItems()
                            .add(new String[]{blobName, url});
                });
                return;
            }
        }

        for (String s : windowsExecutableTypes) {
            if (blobName.endsWith(s.toLowerCase())) {
                Platform.runLater(() -> {
                    ((ListView<String[]>) windows_tab.getContent()).getItems()
                            .add(new String[]{blobName, url});
                });
                return;
            }
        }

        for (String s : linuxExecutableTypes) {
            if (blobName.endsWith(s.toLowerCase())) {
                Platform.runLater(() -> {
                    ((ListView<String[]>) linux_tab.getContent()).getItems()
                            .add(new String[]{blobName, url});
                });
                return;
            }
        }
    }
}
