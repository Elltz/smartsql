/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public abstract class BuildConnectionEntity {
        
    RuntimeDocumentWrapper runtimeDocumentWrapper;
    BuildVars buildVars;
    BuildStatus buildStatus;
    final String folder_config = "configurables";
    final String folder_depend = "dependencies";
    final String folder_execut = "executables";
    final String folder_execut_linux = "linux";
    final String folder_execut_mac = "mac";
    final String folder_execut_window = "windows";
    
    final String _32BitVersion = "32";
    final String _64BitVersion = "64";
    
    final HashMap<String,String> publicFileLinks = new HashMap(100);

    public BuildConnectionEntity(RuntimeDocumentWrapper runtimeDocumentWrapper, BuildVars buildVars, BuildStatus buildStatus) {
        this.runtimeDocumentWrapper = runtimeDocumentWrapper;
        this.buildVars = buildVars;
        this.buildStatus = buildStatus;
    }
    
    
    public abstract void initialize();

    public abstract void openConnection();
    
    public abstract boolean isConnectionCreationSuccessfull();
    
    public void beginUploadBinaries() {
        if(runtimeDocumentWrapper == null) { 
            throw new TopDbsException("Runtime Document is null"); 
        }
        
        uploadBinaries();
    }

    public void beginUploadFiles() {
        if(runtimeDocumentWrapper == null) { 
            throw new TopDbsException("Runtime Document is null"); 
        }
        
        uploadLibs();
        uploadJars();
        uploadConfig();
        
        updateVersion();
        
        System.out.println("");
        System.out.println("------------------------------------------------");
        System.out.println("DONE UPLOADING");
        System.out.println("-------------------------------------------------");
        
        if (buildStatus.isMethodValid(BuildStatus.METHOD_INVOCATION_POINTER_PRINT_OUT_SECTION)) {
            
            System.out.println("--------------------------------------------");
            System.out.println("PRINTING PUBLIC URLS");
            System.out.println("---------------------------------------------");

            for (Map.Entry<String, String> en : publicFileLinks.entrySet()) {
                System.out.println();
                System.out.println("PUBLIC URL FOR " + en.getKey() + " IS " + en.getValue());
            }
            
            System.out.println("--------------------------------------------");
            System.out.println("CONFIGURATION FILE LISTINGS");
            System.out.println("---------------------------------------------");
            
            System.out.println("APP HASH "+runtimeDocumentWrapper.getAppHash());
            System.out.println("JAR SIZE "+runtimeDocumentWrapper.getAppSize());
            System.out.println("UPDATE NOTICE "+runtimeDocumentWrapper.getNewUpdateNotice());
            System.out.println("RUNTIME VERSION "+runtimeDocumentWrapper.getVersion());
            
            System.out.println("LIBS COUNT "+runtimeDocumentWrapper.getDependencyHashes().size());
            
            for(Map.Entry<String, String> en : runtimeDocumentWrapper.getDependencyHashes().entrySet()){
                 System.out.println(en.getKey().toUpperCase()+ " " +en.getValue());
            }           
            
            System.out.println("");
            System.out.println("------------------------------------------------");
            System.out.println("BUILD COMPLETED");
            System.out.println("-------------------------------------------------");
        }
        
    }

    public void onOperationNotification(String fileTypeInPlay, String response) {
        System.out.println();
        if (response == null || response.isEmpty()) {
            System.out.println("BEGINING UPLOAD SESSION " + fileTypeInPlay.toUpperCase());
            System.out.println("-----------------------------------------------------------------------");
        } else {
            System.out.println(response);
        }
    }
    
    public String getPublicFileLink(String key) {
        return publicFileLinks.get(key);
    }
    
    public RuntimeDocumentWrapper getFinalizedRuntime() {
        return runtimeDocumentWrapper;
    }

    public abstract void uploadLibs();
    public abstract void uploadBinaries();
    public abstract void uploadJars();
    public abstract void uploadConfig();
    
    public abstract void updateVersion();
}
