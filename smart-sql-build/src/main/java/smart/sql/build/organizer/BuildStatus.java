/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smart.sql.build.organizer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * 
 * This contains the status of the build level, and current position, and last function executed
 */
public class BuildStatus implements Serializable{
 
    private byte currentLevel;
    private Map<String,String> uploadFiles; //rhis represents the files uploaded, or already done if it was in upload mode  
    
    
    public BuildStatus() {
        uploadFiles = new HashMap<>(80);
        //for debug;
        //currentLevel = METHOD_INVOCATION_POINTER_COMPLETION;
    }
    
    public boolean hasFinished(){
        return currentLevel == METHOD_INVOCATION_POINTER_COMPLETION;
    }
    
    public boolean shouldStartAFreshBuild(){
        boolean bool = hasFinished();
        if(bool)
            setCurrentLevel((byte)-1);
        return bool;
    }
    
    public void finalizeOperationStatus(){
        //it resets everything
        uploadFiles.clear();
        setCurrentLevel(METHOD_INVOCATION_POINTER_COMPLETION);
        System.gc();
    }

    public byte getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(byte currentLevel) {
        this.currentLevel = currentLevel;
        System.out.println("NEW OPERATION LEVEL SET " + this.currentLevel);
    }

    public Map<String, String> getUploadFiles() {
        return uploadFiles;
    }
    
    public boolean isMethodValid(byte flag){
        return !Thread.currentThread().isInterrupted() && shouldStartAFreshBuild() || shouldExecuteMethod(flag);
    }
    
    private boolean shouldExecuteMethod(byte flag){
        boolean returnFlag = flag >= currentLevel;        
        if(returnFlag){ setCurrentLevel(flag); }        
        return returnFlag;
    }
    
    
    public static final byte METHOD_INVOCATION_POINTER_NONE = -1;
    public static final byte METHOD_INVOCATION_POINTER_PLATFORM_COPY = 0;
    public static final byte METHOD_INVOCATION_POINTER_BUILD_EXECUTION_BRANCH = 1;
    public static final byte METHOD_INVOCATION_POINTER_CREATE_BINARY_EXECUTABLES = 2;
    
    public static final byte METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_WINDOWS_NON_JRE_VERSION = 3;
    public static final byte METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_WINDOWS_JRE_VERSION = 4;
    //linux
    public static final byte METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_LINUX_SH_VERSION = 3;
    //mac
    public static final byte METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_MAC_PKG_DMG_VERSION = 3;
    
    public static final byte METHOD_INVOCATION_POINTER_BINARY_EXECUTABLES_AFTERMATH_BUNDLING = 5;
    public static final byte METHOD_INVOCATION_POINTER_RUNTIME_DOCUMENT_CREATION = 6;
    public static final byte METHOD_INVOCATION_POINTER_SIGN_WINDOWS_EXECUTABLE = 7;
    public static final byte METHOD_INVOCATION_POINTER_VERIFY_WINDOWS_EXECUTABLE_STATE = 8;
    public static final byte METHOD_INVOCATION_POINTER_SERVER_UPLOAD_BRANCH = 9;
    public static final byte METHOD_INVOCATION_POINTER_SERVER_UPLOAD_CONSTRUCT_UPLOAD = 10;
    public static final byte METHOD_INVOCATION_POINTER_PRINT_OUT_SECTION = 11;
    public static final byte METHOD_INVOCATION_POINTER_FINAL_PATH_DISPLAYorOPENER = 12;
    public static final byte METHOD_INVOCATION_POINTER_COMPLETION = 13;
}
