/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fxmisc.richtext;

import javafx.scene.control.Label;

/**
 *
 * @author NGM
 */
public interface LineNumberChangeListener {

    void update(Label node, String lineContent, LineNumberFactory factory);

}
