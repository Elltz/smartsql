package org.fxmisc.richtext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.function.IntFunction;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;

import org.reactfx.collection.LiveList;
import org.reactfx.value.Val;

/**
 * Graphic factory that produces labels containing line numbers. To customize
 * appearance, use {@code .lineno} style class in CSS stylesheets.
 */
public class LineNumberFactory implements IntFunction<Node> {

    private static LineNumberChangeListener call;

    private String old = null;
    
    private final Image minimizeOpen = new Image(LineNumberFactory.class.getResource("minimize_open.png").toExternalForm());
    private final Image minimizeMiddle = new Image(LineNumberFactory.class.getResource("minimize_middle.png").toExternalForm());
    private final Image minimizeClose = new Image(LineNumberFactory.class.getResource("minimize_close.png").toExternalForm());
    private final Image minimizeEmpty = new Image(LineNumberFactory.class.getResource("minimize_empty.png").toExternalForm());
    private final Image maximize = new Image(LineNumberFactory.class.getResource("maximize.png").toExternalForm());
    
    
    private final Image error = new Image(LineNumberFactory.class.getResource("error.png").toExternalForm());
    private final Image warning = new Image(LineNumberFactory.class.getResource("warning.png").toExternalForm());
    
    private final Image empty = new Image(LineNumberFactory.class.getResource("empty.png").toExternalForm());

    private static final Insets DEFAULT_INSETS = new Insets(0.0, 5.0, 0.0, 5.0);

    private static final Paint DEFAULT_TEXT_FILL = Color.web("#666");

    private static final Font DEFAULT_FONT
            = Font.font("monospace", FontPosture.ITALIC, 13);

    private static final Background DEFAULT_BACKGROUND
            = new Background(new BackgroundFill(Color.WHITE, null, null));

    public static IntFunction<Node> get(StyledTextArea<?> area, LineNumberChangeListener l) {
        call = l;
        //return get(area, digits -> "%0" + digits + "d");
        return get(area, digits -> "%d");
    }

    public static IntFunction<Node> get(
            StyledTextArea<?> area,
            IntFunction<String> format) {
        return new LineNumberFactory(area, format);
    }

    private final Val<Integer> nParagraphs;

    private final IntFunction<String> format;

    private StyledTextArea<?> area;

    
    private final Map<Paragraph, Label> labels = new HashMap<>();
    private final Map<Integer, Label> numberLabels = new HashMap<>();
    private final Map<Paragraph, MinimizeData> minimizedData = new HashMap<>();
    
    // map of block with opened paragraph as key and list as parapgraphs in block
    private final Map<ParagraphData, List<Paragraph<?>>> blockParagraphs = new HashMap<>();
    // list of middle paragrapsh
    private final List<Paragraph<?>> middles = new ArrayList<>();
    // list of closed paragrapsh
    private final List<Paragraph<?>> closed = new ArrayList<>();
    
    private volatile boolean dataChanged = true;
    
    private static String copyString(String str, int count) {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += str;
        }
        
        return result;
    }
    
    
    private LineNumberFactory(
            StyledTextArea<?> area,
            IntFunction<String> format) {
        this.area = area;
        nParagraphs = LiveList.sizeOf(area.getParagraphs());
        this.format = format;
        
        area.getParagraphs().addListener((ListChangeListener.Change<? extends Paragraph<?>> c) -> {
            while (c.next()) {
                if (c.wasPermutated()) {
                    // do nothing
                } else if (c.wasUpdated()) {
                    // do nothing
                } else {
                    
                    // This is a hack
                    // When we minimize rows, highlighter will removing paragraph
                    // for highlight ")" and this is destroy minimize functionality
                    // we catching when paragraph will change for the same
                    // and reiniting all need data
                    if (c.getRemovedSize() == 1 && c.getAddedSize() == 1) {
                        Paragraph removed = c.getRemoved().get(0);
                        Paragraph added = c.getAddedSubList().get(0);
                        
                        if (isMinimized(removed) && removed.toString().equals(added.toString())) {
                            Label l = labels.remove(removed);
                            labels.put(added, l);
                            added.disableProperty().set(true);
                            
                            MinimizeData md = minimizedData.remove(removed);
                            minimizedData.put(added, md);
                            md.paragraph = added;
                        }
                    }
                }
            }
        });
    }
    
    public void dataWasChanged() {
        dataChanged = true;
    }

    public void showWarning(Label lineNo) {
        lineNo.setGraphic(new ImageView(warning));
    }
    
    public void showError(Label lineNo) {
        lineNo.setGraphic(new ImageView(error));
    }

    public void clear(Label lineNo) {
        lineNo.setGraphic(new ImageView(empty));
        lineNo.setTooltip(null);
        lineNo.setOnMouseClicked(null);
    }
    
    public void clearAll() {
        for (Label l: numberLabels.values()) {
            clear(l);
        }
    }
    
    public void clear(int from, int to){
        for(Map.Entry<Integer,Label> en : numberLabels.entrySet()){
            if(en.getKey() > to){
                //hashmap does not insure order, so this may be wrong. but keeping it for now
                break; 
            }
            if(en.getKey() >= from){
                clear(en.getValue());
            }
        }
    }
    
    public boolean isMinimized(Paragraph p) {
        MinimizeData md = minimizedData.get(p);
        if (md != null) {
            return md.minimized;
        }
        return false;
    }
    
    
    public void checkMinimize() {
        blockParagraphs.clear();
        middles.clear();
        closed.clear();
        
        // find blocks '(' ')'
        checkMinimize(blockParagraphs, middles, closed, "(", ")");
        // find blocks '{' '}'
        checkMinimize(blockParagraphs, middles, closed, "{", "}");

        // update labels images
        invalidateLabels(blockParagraphs, middles, closed);
    }
    
    @Override
    public Node apply(int idx) {
        if (idx >= 0)
        {  
            // minimize label
            Label minimizeLabel = new Label();
            Paragraph currentParagraph = area.getParagraphs().get(idx);
            labels.put(currentParagraph, minimizeLabel);
            
            Val<String> formatted = nParagraphs.map(n -> format(idx + 1 + calcRowShift(idx), n + calcHidenRowShift()));

            Label lineNo = lookupNumberLabel(idx);
            if (lineNo == null) {
                lineNo = new Label();
                lineNo.setAlignment(Pos.CENTER_LEFT);
                lineNo.setFont(DEFAULT_FONT);
                lineNo.setBackground(DEFAULT_BACKGROUND);
                lineNo.setTextFill(DEFAULT_TEXT_FILL);
                lineNo.setPadding(DEFAULT_INSETS);
                lineNo.getStyleClass().add("lineno");
                
                clear(lineNo);
            
                numberLabels.put(idx, lineNo);
            }
            
            if (dataChanged) {
                checkMinimize();
                dataChanged = false;
            } else {
                invalidateLabels(blockParagraphs, middles, closed);
            }
            
            // bind label's text to a Val that stops observing area's paragraphs
            // when lineNo is removed from scene
            lineNo.textProperty().unbind();
            lineNo.textProperty().bind(formatted.conditionOnShowing(lineNo));
            old = area.getParagraph(idx).fullText();
            call.update(lineNo, currentParagraph.fullText(), this);
            return new HBox(lineNo, minimizeLabel);
        }
        
        return null;
    }
    
    
    private String format(int x, int max) {        
        int digits = (int) Math.floor(Math.log10(max)) + 1;
        int currentDigits = (int) Math.floor(Math.log10(x)) + 1;
        
        return copyString(" ", digits - currentDigits) + String.format(format.apply(digits), x);
    }
    
    public Label lookupNumberLabel(int row) {
        return numberLabels.get(row);
    }
    
    
    private void checkMinimize(Map<ParagraphData, List<Paragraph<?>>> blockParagraphs, List<Paragraph<?>> middles, List<Paragraph<?>> closed, String openSeparator, String closeSeparator)
    {
        // put all opened paragraphs in statck
        Stack<ParagraphData> stack = new Stack<>();
        
        for (int i = 0; i < area.getParagraphs().size(); i++)
        {
            Paragraph<?> p = area.getParagraphs().get(i);
            String paragraphText = p.toString();

            // get index of open separator
            ParagraphData data = processParagraphData(p, openSeparator, closeSeparator);
            if (data != null && data.separatorCount > 0)
            {
                stack.push(data);
            }

            // if found paragraph with last separator and not start with it and stack not empty - this is end of minimize block
            if ((paragraphText.trim().endsWith(closeSeparator) || paragraphText.trim().endsWith(closeSeparator + ";")) && (data == null || data.separatorCount < 0) && !stack.isEmpty())
            {
                ParagraphData startParagraphData = stack.pop();

                int startIndex = area.getParagraphs().indexOf(startParagraphData.paragraph);
                List<Paragraph<?>> subList = (List<Paragraph<?>>) area.getParagraphs().subList(startIndex + 1, i + 1);
                
                if (subList.size() > 1)
                {
                    middles.addAll(subList.subList(0, subList.size() - 1));
                }

                if (!subList.isEmpty())
                {
                    closed.add(subList.get(subList.size() - 1));
                }
                                
                blockParagraphs.put(startParagraphData, subList);
            }
        }
    }
    
    // find start index
    private ParagraphData processParagraphData(Paragraph paragraph, String openSeparator, String closeSeparator)
    {
        int count = 0;
        int firstStartIndex = -1;
        for (int i = 0; i < paragraph.fullLength(); i++)
        {
            String symbol = String.valueOf(paragraph.fullText().charAt(i));
            if (symbol.equals(openSeparator))
            {
                if (firstStartIndex < 0)
                {
                    firstStartIndex = i;
                }
                count++;
            }
            else if (symbol.equals(closeSeparator))
            {
                count--;
            }
        }
        
        if (count >= 0)
        {
            ParagraphData data = new ParagraphData();
            data.paragraph = paragraph;
            data.startIndex = firstStartIndex;
            data.separatorCount = count;
            
            return data;
        }
        else
        {
            return null;
        }
    }
    
    private void invalidateLabels(Map<ParagraphData, List<Paragraph<?>>> blockParagraphs, List<Paragraph<?>> middles, List<Paragraph<?>> closed) {
        
        Set<ParagraphData> opened = blockParagraphs.keySet();
        
        // clear not using (removed) paragraphs
        for (Paragraph p: labels.keySet().toArray(new Paragraph[0]))
        {
            if (!area.getParagraphs().contains(p))
            {
                labels.remove(p);
            }
        }
        
        // for each opened paragraph create MinimizeData object
        // set for paragraph labels current image
        for (Paragraph<?> pb: labels.keySet())
        {
            Label l = labels.get(pb);
            ImageView view = new ImageView(minimizeEmpty);
            
            MinimizeData md = minimizedData.get(pb);
            // if paragraph already has MinimizeData object and it minimized - need set maximize image
            // and add mouse listener to label for expanding
            if (md != null && md.minimized)
            {
                view.setImage(maximize);
                l.setCursor(Cursor.DEFAULT);
                l.setOnMouseClicked(md);
            }
            else
            {
                // set current image for paragraph
                if (middles.contains(pb))
                {
                    view.setImage(minimizeMiddle);
                }

                ParagraphData data = null;
                for (ParagraphData pd: opened)
                {
                    if (pd.paragraph.equals(pb))
                    {
                        data = pd;
                        break;
                    }
                }
                
                if (data != null)
                {
                    view.setImage(minimizeOpen);
                    
                    // create if need MinimizeData for opened paragraph
                    if (md == null)
                    {
                        md = new MinimizeData();
                        md.paragraph = data.paragraph;
                        md.startIndex = data.startIndex;
                        
                        minimizedData.put(data.paragraph, md);
                    }

                    md.listParagraphs = blockParagraphs.get(data);
                    l.setOnMouseClicked(md);
                    l.setCursor(Cursor.DEFAULT);
                }

                if (closed.contains(pb))
                {
                    view.setImage(minimizeClose);
                }
            }
            
            l.setGraphic(view);
        }
    }
    
    private class ParagraphData
    {
        private Paragraph<?> paragraph;
        private int startIndex;
        private int separatorCount;
    }

    private class MinimizeData implements EventHandler<MouseEvent>
    {
        private boolean minimized = false;
        private int lines = 0;
        
        private int startIndex = 0;
        
        private Paragraph<?> paragraph;
        private String fullText;
            
        private List<Paragraph<?>> listParagraphs;
        
        public MinimizeData()
        {
        }

        @Override
        public void handle(MouseEvent event) {
            minimized = !minimized;
            
            // check what need to do
            // find indexes for manipulation with textarea
            
            int paragraphIndex = area.getParagraphs().indexOf(paragraph);
            int start = 0;
            
            // find start index
            for (int i = 0; i < paragraphIndex; i++)
            {
                start += area.getParagraph(i).fullLength();
            }
            start += startIndex;

            int end = start + paragraph.fullLength() - startIndex;
                
            if (minimized)
            {
                fullText = paragraph.fullText().substring(startIndex);
                
                Paragraph<?> lastParagraph = null;
                int additionalSize = 0;
                //find end index
                for (Paragraph<?> p: listParagraphs)
                {                    
                    end += p.fullLength();
                    lastParagraph = p;
                    
                    MinimizeData md = minimizedData.get(p);
                    
                    if (md != null)
                    {
                        minimizedData.remove(p);
                    }                    
                    
                    if (md != null && md.minimized)
                    {
                        additionalSize += md.lines - 1;
                        fullText += md.fullText;
                    }
                    else
                    {
                        fullText += p.fullText();
                    }                    
                    
                    // remove from labels each paragraph
                    labels.remove(p);
                }
                
                // if last symbol enter
                if (lastParagraph != null)
                {
                    end -= lastParagraph.fullLength() - lastParagraph.toString().length();
                    
                    if (lastParagraph.toString().trim().endsWith(";")) {
                        end -= lastParagraph.toString().length() - lastParagraph.toString().lastIndexOf(";");
                    }
                }
                
                lines = additionalSize + listParagraphs.size() + 1;
                
                area.replaceText(start + 1, end - 1, "..." + lines + " lines ");
                
                // disable for editing minimized paragraph
                Paragraph minimizedParagraph = area.getParagraph(paragraphIndex);
                minimizedParagraph.disableProperty().set(true);
                
                minimizedData.put(minimizedParagraph, this);
                
                minimizedData.remove(paragraph);
                paragraph = minimizedParagraph;
            }
            else
            {
                area.replaceText(start, end, fullText);
                
                minimizedData.remove(paragraph);
                fullText = null;
                lines = 0;
            }
        }
        
    }
    
    private int calcRowShift(int index)
    {
        int shift = 0;
        int size = area.getParagraphs().size();
        for (int i = 0; i < index; i++)
        {
            if (i < size)
            {
                Paragraph<?> p = area.getParagraphs().get(i);
                MinimizeData md = minimizedData.get(p);
                if (md != null && md.minimized)
                {
                    shift += md.lines - 1;
                }
            }
        }
        
        return shift;
    }
    
    private int calcHidenRowShift()
    {
        int rows = 0;
        for (Paragraph<?> p: minimizedData.keySet())
        {           
            MinimizeData md = minimizedData.get(p);
            if (md != null && md.minimized)
            {
                rows += md.lines - 1;
            }
        }
        
        return rows;
    }    
}
