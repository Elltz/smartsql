package tools.java.pats.nodes;

import java.io.Serializable;
import java.security.InvalidParameterException;

import static java.lang.String.format;
import static tools.java.pats.constants.ProjectStaticConstants.TWO_INDENTS;
import tools.java.pats.formatters.EmbeddedSelects.EmbeddedSelectsFormatter;
import static tools.java.pats.formatters.EmbeddedSelects.Factory.EmbeddedSelectsFormatterFactory.getFormatter;
import tools.java.pats.string.utils.FindIndexesForStringWithinParens;
import tools.java.pats.string.utils.StringIndexes;

/**
 *
 * Created by IntelliJ IDEA.
 * User: Pat Keeler
 * Date: 10/23/11
 * Time: 8:14 AM
 * To change this template use File | Settings | File Templates.
 */

public class Union extends Node implements Query, Serializable {

    private static final long serialVersionUID = 1951L;

    private final String cmd;

    private final String data;

    private String firstData;
    
    /**
     * Final Argument Constructor.
     *
     * @param cmd - sql command name
     * @param data - sql arguments for command
     * @param recursionTab - number of user indents
     * @param userIndentAmount - length of user supplied indents
     * @param selectedStyle - block or expanded
     */
    public Union(final String cmd,
                 final String data,
                 final String recursionTab,
                 final String userIndentAmount,
                 final String selectedStyle) {

      super(recursionTab, userIndentAmount, selectedStyle);

        this.cmd = cmd;
        this.data = data;

        if (cmd.isEmpty()) {
            throw new InvalidParameterException(INVALID_UNION_CMD);
        }
     }

    public void setFirstData(String firstData) {
        this.firstData = firstData;
    }

    public String getFirstData() {
        return firstData;
    }

    
    
    /**
     * This method adds the union command surrounded by blank lines.
     *
     * @param node - Class type
     * @return formatted sql string
     */
    @Override
    public String processLine(Query node) {

        StringBuffer sb = new StringBuffer();
        
        if (firstData != null && !firstData.trim().isEmpty()) {
            processSelect(sb, firstData);
        }
        
        sb.append(format("\n\n%s%s\n", tab, cmd.trim()));
        
        if (data != null && !data.trim().isEmpty()) {
            processSelect(sb, data);
        }
        
        return sb.toString();
    }

    private void processSelect(StringBuffer sb, String s) {
        FindIndexesForStringWithinParens findIndexes = new FindIndexesForStringWithinParens();
        StringIndexes ind = findIndexes.getIndexesForSqlWithinParens(s);

        int start = ind.getStart();
        int end = ind.getEnd();
        
        String newSql = s.substring(start, end);
        
        while (newSql.trim().startsWith("(") && newSql.trim().endsWith(")")) {
            ind = findIndexes.getIndexesForSqlWithinParens(newSql);
            newSql = newSql.substring(ind.getStart(), ind.getEnd());
            
            end = ind.getEnd() + start;
            start += ind.getStart();            
        }
        
        ind.setStart(start);
        ind.setEnd(end);
        
        if (!newSql.isEmpty()) {
            EmbeddedSelectsFormatter esf =
                    getFormatter(TWO_INDENTS, tab, stringIndentAmount, selectedStyle);
            sb.append(esf.formatEmbeddedSelect(s, ind));
        }
    }
}
