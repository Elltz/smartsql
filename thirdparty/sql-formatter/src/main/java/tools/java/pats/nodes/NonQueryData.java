package tools.java.pats.nodes;

import java.io.Serializable;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class NonQueryData extends Node implements Query, Serializable {

    private static final long serialVersionUID = 1951L;

    private final String data;


    /**
     * Final Argument Constructor.
     *
     * @param data - sql arguments for command
     * @param recursionTab - number of user indents
     * @param userIndentAmount - length of user supplied indents
     * @param selectedStyle - block or expanded
     */
    public NonQueryData(
                  final String data,
                  final String recursionTab,
                  final String userIndentAmount,
                  final String selectedStyle) {

        super(recursionTab, userIndentAmount, selectedStyle);

        this.data = data;
    }

    public String getData() {
        return data;
    }
    
    


    /**
     * This class will have the table name as the data resulting in
     * a one line statement.
     *
     * @return - formatted sql string
     */
    @Override
    public String processLine(Query node) {

        StringBuilder sb = new StringBuilder();

        sb.append(data.trim());

        return sb.toString();
    }

}