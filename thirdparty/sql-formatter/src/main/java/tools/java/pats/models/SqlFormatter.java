package tools.java.pats.models;

import tools.java.pats.nodes.Query;
import tools.java.pats.string.utils.StringCleaner;
import tools.java.pats.string.utils.sql.SqlKeywordsToUpperCase;
import tools.java.pats.string.utils.sql.SqlNodeParser;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.List;
import tools.java.pats.nodes.NonQueryData;
import tools.java.pats.nodes.Union;


/**
 * SQL statement formatter.
 * <p>
 * This class will format sql commands.
 * <p>
 * Feel free to contact me with comments, questions or suggestions for
 * improvements.
 * 
 * @author Pat Keeler - keelerpl@gmail.com
 */
public class SqlFormatter implements Serializable {

    private static final long serialVersionUID = 1951L;



	/**
     * Default constructor
     */
	public SqlFormatter() {

		super();
	}

    /**
     * The main SqlFormatter driver.
     *
     * @param sql - un-formatted sql
     * @param tab - user indent amount
     * @param stringIndentAmount - string of tab length
     * @return formatted sql string
     */
	public String formatSql(String sql, String tab,
                            String stringIndentAmount, String selectedStyle) {

        if (sql.isEmpty()) {
            throw new InvalidParameterException(
                    "SQL string can not be blank: String = " + sql);
        }

        //Get leading comment buffer
        StringBuffer comments = new StringBuffer();

        //TODO move comment handler here.

        // StringCleaner.java - Cleans up
        //    form feeds,
        //    line breaks,
        //    excess spaces,
        //    and removes embedded comments.
        StringCleaner cleaner = new StringCleaner();
        sql = cleaner.cleanString(sql, comments);

        // Log the one line sql string.

        // Upper case all commands.
        SqlKeywordsToUpperCase upper = new SqlKeywordsToUpperCase();
        sql = upper.upperCaseKeywords(sql);

        /* Get List of Nodes in script. */
        SqlNodeParser nodeParser = new SqlNodeParser(sql, tab, stringIndentAmount, selectedStyle);
        List<Query> nodeList = nodeParser.parseSql();

        StringBuffer sb = new StringBuffer();

        // Remove last newline from comments.
        if (comments.length() > 0 && 
                        comments.charAt(comments.length() - 1) == '\n') {
                comments.deleteCharAt(comments.length() - 1);
        }
        //Load leading comments (if any) to StringBuffer.
        sb.append(comments.toString());

        // Format each node.
        int i = 0;
        for (Query node: nodeList) {
            try {	
                
                if (node instanceof NonQueryData && nodeList.size() > i + 1) {
                    Query q = nodeList.get(i + 1);
                    if (q instanceof Union) {
                        String data0 = ((NonQueryData)node).getData();
                        String data1 = ((Union)q).getFirstData();
                        if (data0.equals(data1)) {
                            // need ignore
                            continue;
                        }
                    }
                }
                
            	sb.append(node.processLine(node));
            } catch (Exception e) {
                throw new InvalidParameterException(node.getClass().getCanonicalName());
            }
            i++;
        }

        // remove first newline
        if (sb.charAt(0) == '\n') {
                sb.deleteCharAt(0);
        }

        return sb.toString().replace(";",";\n");
    }
}
