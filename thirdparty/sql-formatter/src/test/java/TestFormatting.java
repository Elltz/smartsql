
import org.junit.Assert;
import org.junit.Test;
import tools.java.pats.models.SqlFormatter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class TestFormatting {

    @Test
    public void formatQuery() {
        
        String query = "SELECT * FROM (((SELECT information_schema.`COLUMNS`.table_schema `db_name`, information_schema.`COLUMNS`.table_name `table_name` ,information_schema.TABLES.ENGINE ENGINE , CONCAT(        column_name,        '$$$$',        column_type ,                 '$$$$' , if(information_schema.COLUMNS.IS_NULLABLE,'no', 'NOT NULL'), '$$$$', IF(information_schema.COLUMNS.`EXTRA`='auto_increment', 'auto_increment', ''), '$$$$' , IFNULL(information_schema.COLUMNS.COLUMN_DEFAULT,'')  ,        '$$$$' , IFNULL(information_schema.COLUMNS.CHARACTER_SET_NAME,'') ,        '$$$$' , IFNULL(information_schema.COLUMNS.COLLATION_NAME,'')                ) `column_name`, 1 `type`     FROM      information_schema.`COLUMNS`     JOIN information_schema.`TABLES`       ON information_schema.`COLUMNS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME`         AND information_schema.`COLUMNS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA`   WHERE information_schema.`TABLES`.`TABLE_TYPE` !='VIEW' ORDER BY information_schema.`COLUMNS`.`ORDINAL_POSITION`)) UNION ALL(SELECT information_schema.`STATISTICS`.table_schema `db_name`, information_schema.`STATISTICS`.table_name `table_name`, '' ENGINE, CONCAT(INDEX_TYPE, ' ', index_name, '(', GROUP_CONCAT(column_name ORDER BY seq_in_index ), ')' ) `column_name`, 2 `type` FROM information_schema.`STATISTICS` JOIN information_schema.`TABLES` ON information_schema.`STATISTICS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME` AND information_schema.`STATISTICS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA` WHERE information_schema.`TABLES`.`TABLE_TYPE` !='VIEW' GROUP BY information_schema.`STATISTICS`.table_schema, information_schema.`STATISTICS`.table_name, index_name) UNION ALL (SELECT  table_schema `db_name`, table_name `table_name` , '' ENGINE, CONCAT('CREATE\n" +
"VIEW ', table_name, '\n" +
"AS\n" +
"', `VIEW_DEFINITION`) `column_name`, 3 `type` FROM  information_schema.`VIEWS`) UNION ALL (SELECT  routine_schema `db_name`, routine_name `table_name` , '' ENGINE, '' `column_name`, IF(    Routine_type='PROCEDURE',    4,    5  ) `type` FROM  information_schema.`ROUTINES`) UNION ALL (SELECT  trigger_schema `db_name`, trigger_name `table_name`,'' ENGINE,CONCAT('CREATE TRIGGER ', trigger_name, '\n" +
"  ', action_timing, ' ', event_manipulation, '\n" +
"  ON ', event_object_table, '\n" +
"  FOR EACH ROW\n" +
"', action_statement) `column_name`, 6 `type` FROM  information_schema.`TRIGGERS`) UNION ALL(SELECT  event_schema `db_name`, event_name `table_name`,'' ENGINE,CONCAT('CREATE EVENT ', `EVENT_NAME`, '\n" +
"  ON SCHEDULE ', IF(`EVENT_TYPE` = 'ONE TIME', CONCAT('AT ''', `EXECUTE_AT`, '''\n" +
"  '), CONCAT('EVERY ''', `INTERVAL_VALUE`, ''' ', `INTERVAL_FIELD`, '\n" +
"  STARTS ''', `STARTS`, '''\n" +
"  ', IF (`ENDS` is not null, CONCAT('ENDS ''', `ENDS`, '''\n" +
"  '), ''))), 'DO\n" +
"', `EVENT_DEFINITION`) `column_name`, 7 `type` FROM  information_schema.`EVENTS`) UNION ALL(SELECT  information_schema.`SCHEMATA`.`SCHEMA_NAME` `db_name`,'' `table_name`,'' ENGINE,'' `column_name`, 0 TYPE FROM  information_schema.`SCHEMATA`) UNION ALL (SELECT information_schema.`KEY_COLUMN_USAGE`.CONSTRAINT_SCHEMA db_name, information_schema.`KEY_COLUMN_USAGE`.TABLE_NAME table_name, '' ENGINE, CONCAT('CONSTRAINT `', information_schema.KEY_COLUMN_USAGE.CONSTRAINT_NAME, '` FOREIGN KEY (`', GROUP_CONCAT(information_schema.KEY_COLUMN_USAGE.COLUMN_NAME ORDER BY information_schema.`KEY_COLUMN_USAGE`.`ORDINAL_POSITION` ), '`) REFERENCES `', information_schema.KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME, '`(`', information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME, '`)' , CONCAT( IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='CASCADE',' ON UPDATE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='RESTRICT',' ON UPDATE RESTRICT', '')), IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='CASCADE',' ON DELETE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='RESTRICT',' ON DELETE RESTRICT', '')))) column_name, 8 TYPE FROM information_schema.KEY_COLUMN_USAGE LEFT JOIN information_schema.`REFERENTIAL_CONSTRAINTS` ON information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_NAME` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_NAME` AND information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_SCHEMA` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_SCHEMA` WHERE information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME IS NOT NULL GROUP BY information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_SCHEMA`, information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`) UNION ALL (SELECT T.table_schema `db_name`, T.table_name `table_name`, '' ENGINE, CONCAT( IFNULL(T.engine,''), '$$$$', IFNULL(CCSA.CHARACTER_SET_NAME,''), '$$$$', IFNULL(T.TABLE_COLLATION,''), '$$$$', IFNULL(T.`AUTO_INCREMENT`, ''), '$$$$', IFNULL(T.`AVG_ROW_LENGTH`,'')) `column_name`, 9 TYPE FROM information_schema.`TABLES` T JOIN information_schema.COLLATION_CHARACTER_SET_APPLICABILITY AS CCSA  ON (T.TABLE_COLLATION = CCSA.COLLATION_NAME) )) a ORDER BY 1, 2";
        
//        String query = "(SELECT * FROM a) UNION ALL (SELECT * FROM b) UNION ALL (SELECT * FROM c)";
        
        SqlFormatter sf = new SqlFormatter();
        String result = sf.formatSql(query, "", "2", "block");
        
        String expect = "SELECT\n" +
"  *\n" +
"FROM\n" +
"  (\n" +
"      ((\n" +
"        SELECT\n" +
"          information_schema.`COLUMNS`.table_schema `db_name`,\n" +
"          information_schema.`COLUMNS`.table_name `table_name`,\n" +
"          information_schema.TABLES.ENGINE ENGINE,\n" +
"          CONCAT( column_name,          '$$$$',          column_type ,          '$$$$' ,          IF(information_schema.COLUMNS.IS_NULLABLE,          'no',          'NOT NULL'),          '$$$$',          IF(information_schema.COLUMNS.`EXTRA`='auto_increment',          'auto_increment',          ''),          '$$$$' ,          IFNULL(information_schema.COLUMNS.COLUMN_DEFAULT,          '') ,          '$$$$' ,          IFNULL(information_schema.COLUMNS.CHARACTER_SET_NAME,          '') ,          '$$$$' ,          IFNULL(information_schema.COLUMNS.COLLATION_NAME,          '') ) `column_name`,\n" +
"          1 `type`\n" +
"        FROM\n" +
"          information_schema.`COLUMNS`\n" +
"          JOIN information_schema.`TABLES`\n" +
"            ON information_schema.`COLUMNS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME`\n" +
"              AND information_schema.`COLUMNS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA`\n" +
"        WHERE\n" +
"          information_schema.`TABLES`.`TABLE_TYPE` !='VIEW'\n" +
"        ORDER BY\n" +
"          information_schema.`COLUMNS`.`ORDINAL_POSITION`\n" +
"      )) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          information_schema.`STATISTICS`.table_schema `db_name`,\n" +
"          information_schema.`STATISTICS`.table_name `table_name`,\n" +
"          '' ENGINE,\n" +
"          CONCAT(INDEX_TYPE,          ' ',          index_name,          '(',          GROUP_CONCAT(column_name ORDER BY seq_in_index ),          ')' ) `column_name`,\n" +
"          2 `type`\n" +
"        FROM\n" +
"          information_schema.`STATISTICS`\n" +
"          JOIN information_schema.`TABLES`\n" +
"            ON information_schema.`STATISTICS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME`\n" +
"              AND information_schema.`STATISTICS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA`\n" +
"        WHERE\n" +
"          information_schema.`TABLES`.`TABLE_TYPE` !='VIEW'\n" +
"        GROUP BY\n" +
"          information_schema.`STATISTICS`.table_schema,\n" +
"          information_schema.`STATISTICS`.table_name,\n" +
"          index_name\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          table_schema `db_name`,\n" +
"          table_name `table_name`,\n" +
"          '' ENGINE,\n" +
"          CONCAT('CREATE VIEW ',          table_name,          ' AS ',          `VIEW_DEFINITION`) `column_name`,\n" +
"          3 `type`\n" +
"        FROM\n" +
"          information_schema.`VIEWS`\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          routine_schema `db_name`,\n" +
"          routine_name `table_name`,\n" +
"          '' ENGINE,\n" +
"          '' `column_name`,\n" +
"          IF ( Routine_type='PROCEDURE',\n" +
"            4,\n" +
"            5 ) `type`\n" +
"        FROM\n" +
"          information_schema.`ROUTINES`\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          trigger_schema `db_name`,\n" +
"          trigger_name `table_name`,\n" +
"          '' ENGINE,\n" +
"          CONCAT('CREATE TRIGGER ',          trigger_name,          ' ',          action_timing,          ' ',          event_manipulation,          ' ON ',          event_object_table,          ' FOR EACH ROW ',          action_statement) `column_name`,\n" +
"          6 `type`\n" +
"        FROM\n" +
"          information_schema.`TRIGGERS`\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          event_schema `db_name`,\n" +
"          event_name `table_name`,\n" +
"          '' ENGINE,\n" +
"          CONCAT('CREATE EVENT ',          `EVENT_NAME`,          ' ON SCHEDULE ',          IF(`EVENT_TYPE` = 'ONE TIME',          CONCAT('AT ''',          `EXECUTE_AT`,          ''' '),          CONCAT('EVERY ''',          `INTERVAL_VALUE`,          ''' ',          `INTERVAL_FIELD`,          ' STARTS ''',          `STARTS`,          ''' ',          IF (`ENDS` IS NOT null,          CONCAT('ENDS ''',          `ENDS`,          ''' '),          ''))),          'DO ',          `EVENT_DEFINITION`) `column_name`,\n" +
"          7 `type`\n" +
"        FROM\n" +
"          information_schema.`EVENTS`\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          information_schema.`SCHEMATA`.`SCHEMA_NAME` `db_name`,\n" +
"          '' `table_name`,\n" +
"          '' ENGINE,\n" +
"          '' `column_name`,\n" +
"          0 TYPE\n" +
"        FROM\n" +
"          information_schema.`SCHEMATA`\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          information_schema.`KEY_COLUMN_USAGE`.CONSTRAINT_SCHEMA db_name,\n" +
"          information_schema.`KEY_COLUMN_USAGE`.TABLE_NAME table_name,\n" +
"          '' ENGINE,\n" +
"          CONCAT('CONSTRAINT `',          information_schema.KEY_COLUMN_USAGE.CONSTRAINT_NAME,          '` FOREIGN KEY (`',          GROUP_CONCAT(information_schema.KEY_COLUMN_USAGE.COLUMN_NAME ORDER BY information_schema.`KEY_COLUMN_USAGE`.`ORDINAL_POSITION` ),          '`) REFERENCES `',          information_schema.KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME,          '`(`',          information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME,          '`)' ,          CONCAT( IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='CASCADE',          ' ON UPDATE CASCADE',          IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='RESTRICT',          ' ON UPDATE RESTRICT',          '')),          IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='CASCADE',          ' ON DELETE CASCADE',          IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='RESTRICT',          ' ON DELETE RESTRICT',          '')))) column_name,\n" +
"          8 TYPE\n" +
"        FROM\n" +
"          information_schema.KEY_COLUMN_USAGE\n" +
"          LEFT JOIN information_schema.`REFERENTIAL_CONSTRAINTS`\n" +
"            ON information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_NAME` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_NAME`\n" +
"              AND information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_SCHEMA` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_SCHEMA`\n" +
"        WHERE\n" +
"          information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME IS NOT NULL\n" +
"        GROUP BY\n" +
"          information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_SCHEMA`,\n" +
"          information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`\n" +
"      ) \n" +
"\n" +
"    UNION ALL\n" +
"\n" +
"      (\n" +
"        SELECT\n" +
"          T.table_schema `db_name`,\n" +
"          T.table_name `table_name`,\n" +
"          '' ENGINE,\n" +
"          CONCAT( IFNULL(T.engine,          ''),          '$$$$',          IFNULL(CCSA.CHARACTER_SET_NAME,          ''),          '$$$$',          IFNULL(T.TABLE_COLLATION,          ''),          '$$$$',          IFNULL(T.`AUTO_INCREMENT`,          ''),          '$$$$',          IFNULL(T.`AVG_ROW_LENGTH`,          '')) `column_name`,\n" +
"          9 TYPE\n" +
"        FROM\n" +
"          information_schema.`TABLES` T\n" +
"          JOIN information_schema.COLLATION_CHARACTER_SET_APPLICABILITY AS CCSA\n" +
"            ON (T.TABLE_COLLATION = CCSA.COLLATION_NAME)\n" +
"      ) \n" +
"  ) a\n" +
"ORDER BY\n" +
"  1,\n" +
"  2";
        
        Assert.assertEquals(expect, result);
    }
    
    @Test
    public void testHaving() {
        String query = "select `s2`.`avg_us` AS `avg_us`,ifnull((sum(`s1`.`cnt`) / nullif((select count(0) from `performance_schema`.`events_statements_summary_by_digest`),0)),0) AS `percentile` from (`sys`.`x$ps_digest_avg_latency_distribution` `s1` join `sys`.`x$ps_digest_avg_latency_distribution` `s2` on((`s1`.`avg_us` <= `s2`.`avg_us`))) group by `s2`.`avg_us` having (ifnull((sum(`s1`.`cnt`) / nullif((select count(0) from `performance_schema`.`events_statements_summary_by_digest`),0)),0) > 0.95) order by `percentile` limit 1";
        
        SqlFormatter sf = new SqlFormatter();
        String result = sf.formatSql(query, "", "2", "block");
        
        String expect = "SELECT\n" +
"  `s2`.`avg_us`                                                                                                               AS `avg_us`,\n" +
"  ifnull((SUM(`s1`.`cnt`) / nullif((SELECT COUNT(0) FROM `performance_schema`.`events_statements_summary_by_digest`), 0)), 0) AS `percentile`\n" +
"FROM\n" +
"  (`sys`.`x$ps_digest_avg_latency_distribution` `s1` JOIN `sys`.`x$ps_digest_avg_latency_distribution` `s2` ON((`s1`.`avg_us` <= `s2`.`avg_us`)))\n" +
"GROUP BY\n" +
"  `s2`.`avg_us`\n" +
"HAVING\n" +
"  (ifnull((SUM(`s1`.`cnt`) / nullif(\n" +
"      (\n" +
"        SELECT\n" +
"          COUNT(0)\n" +
"        FROM\n" +
"          `performance_schema`.`events_statements_summary_by_digest`\n" +
"      ) ,0)),0) > 0.95)\n" +
"ORDER BY\n" +
"  `percentile` limit 1";
        
        Assert.assertEquals(expect, result);
    }
}
