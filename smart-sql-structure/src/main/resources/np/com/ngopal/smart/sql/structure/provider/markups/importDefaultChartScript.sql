
INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    1,
    'MySQL Connections',
    0,
'   Max Connections is the maximum permitted number 
of simultaneous client connections. This is the 
value of the max_connections variable.

   Max Used Connections is the maximum number of 
connections that have been in use simultaneously 
since the server was started.

   Connections is the number of connection attempts 
(successful or not) to the MySQL server.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Connections',
    0,
    1,
    0,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (1, 1, 'max_connections', 'Max Connections', '#6ed0e0', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (2, 1, 'Max_used_connections', 'Max Used Connections', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (3, 1, 'Connections', 'Connections', '#7eb26d', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    2,
    'MySQL Client thread Activities',
    0,
'   Peak Threads Connected is number of peak number 
of connections MySQL had in the period of time

   Peak Threads Running is the peak number of threads 
considered "Running" by MySQL, not Sleeping or 
Waiting on Table level locks which can be indicative 
for workload spikes

   Avg Threads Running is the average number of Threads 
running over interval which can be indicative of 
sustained load', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Connections',
    0,
    1,
    1,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (4, 2, 'Connections', 'Peak Threads Connected', '#1f78c1', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (5, 2, 'Threads_running', 'Peak Threads Running', '#e24d42', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (6, 2, 'AVG Threads_running', 'Avg Threads Running', '#eab839', 0, 0);
 
INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    3,
    'MySQL Questions',
    0,
'   The number of queries sent to the server by clients, 
excluding those executed within stored programs.

   This variable does not count the following commands:
- COM_PING
- COM_STATISTICS
- COM_STMT_PREPARE
- COM_STMT_CLOSE
- COM_STMT_RESET', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Table Locks',
    0,
    1,
    2,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (7, 3, 'Questions', 'Questions', '#7eb26d', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    4,
    'MySQL Thread Cache',
    0,
'   Thread Cache Size is the value of the thread_cache_size 
system variable that defines how many threads the server 
should cache for reuse. When a client disconnects, the 
client''s threads are put in the cache (unless it is full). 
As of MySQL 5.6.8, default thread cache size depends on the 
value of max_connections and is capped at 100 threads. Requests 
for threads are satisfied by reusing threads taken from the 
cache if possible, and only when the cache is empty is a new 
thread created.

   Threads Cached is the number of threads in the thread cache.

   Threads Created is the number of threads created to handle 
connections.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Table Locks',
    0,
    1,
    3,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (8, 4, 'thread_cache_size', 'Thread cache size', '#eab839', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (9, 4, 'Threads_created', 'Thread created', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (10, 4, 'Threads_cached', 'Thread Cached', '#6ed0e0', 0, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    5,
    'MySQL Temporary Objects',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Temporary Objects',
    0,
    1,
    4,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (11, 5, 'Created_tmp_tables', 'Created Tmp Tables', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (12, 5, 'Created_tmp_disk_tables', 'Created Tmp Disk Tables', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (13, 5, 'Created_tmp_files', 'Created Tmp Files', '#6ed0e0', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    6,
    'MySQL Select Type',
    0,
'   MySQL Select Types

   As with most relational databases, selecting based on indexes 
is more efficient than scanning an entire table''s data. Here 
we see the counters for selects not done with indexes.

- Select Scan is how many queries caused full table scans, in 
which all the data in the table had to be read and either 
discarded or returned.

- Select Range is how many queries used a range scan, which 
means MySQL scanned all rows in a given range.

- Select Full Join is the number of joins that are not joined 
on an index, this is usually a huge performance hit.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Temporary Objects',
    0,
    1,
    5,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (14, 6, 'Select_scan', 'Select Scan', '#e24d42', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (15, 6, 'Select_range', 'Select Range', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (16, 6, 'Select_full_join', 'Select Full Join', '#7eb26d', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    7,
    'MySQL Sorts',
    0,
'   Sort Rows shows how many rows had to be sorted to return them 
in the correct order.

   For sorts that could not use indexes, it is important to note 
the following:

- Sort Scan shows how many sorts caused a full table scan.

- Sort Range shows how many sorts used a range scan', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Sorts',
    0,
    1,
    6,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (17, 7, 'Sort_scan', 'Sort Scan', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (18, 7, 'Sort_rows', 'Sort Rows', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (19, 7, 'Sort_range', 'Sort Range', '#eab839', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    8,
    'MySQL Slow Queries',
    0,
'   Shows how many slow queries were executed by the server. 
A query is considered slow if it takes more time than the value 
of the long_query_time system variable.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Sorts',
    0,
    1,
    7,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (20, 8, 'Slow_queries', 'Slow Queries', '#7eb26d', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    9,
    'MySQL Aborted connections',
    0,
'   Aborted Clients shows how many connections were aborted 
(the client disconnected improperly or was terminated).

   Aborted Connects shows how many attempts to connect to 
the MySQL server failed. If the number of attempts from a
host reaches the value of the max_connect_errorssystem 
variable, the server blocks that host from further connections.
To unblock the host, flush the host cache using FLUSH HOSTS.

- Communication Errors and Aborted Connections', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Aborted',
    0,
    1,
    8,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (21, 9, 'Aborted_clients', 'Aborted Clients (attemps)', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (22, 9, 'Aborted_connects', 'Aborted Connects (timeout)', '#eab839', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    10,
    'MySQL table locks',
    0,
'   Shows how many table locks MySQL server requested from 
the storage engine.

   Table Locks Immediate shows the number of times that a request 
for a table lock could be granted immediately.

   Table Locks Waited shows the number of times that a request 
for a table lock could not be granted immediately. If this metric 
is rising, you need to deal with lock contention:

- Optimize queries that require table locks.

- Split tables that require frequent locking or replicate them.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Aborted',
    0,
    1,
    9,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (23, 10, 'Table_locks_immediate', 'Table Locks Immediate', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (24, 10, 'Table_locks_waited', 'Table Locks Waited', '#eab839', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    11,
    'MySQL Network Traffic',
    0,
'   Shows how much network traffic is generated by MySQL.

   Outbound is traffic sent from the server.

   Inbound is traffic received by the server.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Network',
    0,
    1,
    10,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (25, 11, 'Bytes_received', 'Inbound', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (26, 11, 'Bytes_sent', 'Outbound', '#eab839', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    12,
    'MySQL Network Hourly',
    0,
'   Shows how much data was sent from the MySQL server and 
how much data was received by the server every hour in the 
last 24 hours.', 
    1,
    'TABLE_BOTTOM',
    'BAR',
    'Network',
    1,
    1,
    11,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (27, 12, 'Bytes_sent', 'Sent', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (28, 12, 'Bytes_received', 'Received', '#eab839', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    13,
    'MySQL internal Memory Overview',
    0,
'   Shows various uses of memory within MySQL.

   System Memory is the total memory for the system.

   InnoDB Buffer Pool Data is the size of the InnoDB buffer 
pool for caching data and indexes in memory.

   TokuDB Cache Size is the size of the TokuDB cache table. 
For more information, see Considerations to Run TokuDB in 
Production

   Key Buffer Size is the size of the buffer used for index 
blocks in MyISAM tables. The key buffer is also known as the 
key cache.

   Adaptive Hash Index Size is the size of the adaptive hash 
index, which enables InnoDB to perform more like an in-memory 
database. When InnoDB notices that some index values are being 
accessed very frequently, it builds a hash index for them in 
memory on top of B-Tree indexes. This allows for very fast 
hashed lookups.

   Query Cache Size is the size of the MySQL query cache, 
which stores the text of a SELECT statement together with the 
corresponding result that was sent to the client. The query 
cache has huge scalability problems, because only one thread 
can do an operation in the query cache at a time. This 
serialization is true for SELECT, INSERT, UPDATE, and DELETE 
queries. The larger the query cache size is set to, the slower 
those operations become.

   InnoDB Dictionary Size is the size of the InnoDB data 
dictionary, which is the internal catalog of tables. InnoDB 
stores the data dictionary on disk, and loads entries into 
memory while the server is running. This is somewhat analogous 
to MySQL‘s table cache, but instead of operating at the server 
level, it is internal to the InnoDB storage engine.

   InnoDB Log Buffer Size the size of the InnoDB redo log 
buffer. It allows transactions to run without having to write 
the log to disk until the transaction commits.', 
    1,
    'TABLE_RIGHT',
    'AREA',
    'Memory',
    0,
    2,
    12,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (29, 13, 'innodb_buffer_pool_size', 'InnoDB Buffer Pool Data', '#eab839', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (30, 13, 'query_cache_size', 'Query Cache Size', '#1f78c1', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (31, 13, 'key_buffer_size', 'Key Buffer Size', '#e24d42', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (32, 13, 'innodb_additional_mem_pool_size', 'InnoDB Additional Memory Pool Size', '#ef843c', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (33, 13, 'innodb_log_buffer_size', 'InnoDB Log Buffer Size', '#6ed0e0', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (34, 13, 'system_memory', 'System Memory', '#7eb26d', 0, 1);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    14,
    'Top Commands Counters',
    0,
'   The Com_{{xxx}} statement counter variables indicate the 
number of times each xxx statement has been executed. There 
is one status variable for each type of statement. For example,
Com_delete and Com_update count DELETE and UPDATE statements, 
respectively. Com_delete_multi and Com_update_multi are similar 
but apply to DELETE and UPDATE statements that use multiple-table 
syntax.', 
    1,
    'TABLE_RIGHT',
    'AREA',
    'Command, Handlers, Processes',
    0,
    2,
    13,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (35, 14, 'Com_select', 'Com_select', '#e24d42', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (36, 14, 'Com_update', 'Com_update', '#cca300', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (37, 14, 'Com_stmt_execute', 'Com_stmt_execute', '#508642', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (38, 14, 'Com_insert', 'Com_insert', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (39, 14, 'Com_commit', 'Com_commit', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (40, 14, 'Com_delete', 'Com_delete', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (41, 14, 'Com_begin', 'Com_begin', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (42, 14, 'Com_set_option', 'Com_set_option', '#1f78c1', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (44, 14, 'Com_show_status', 'Com_show_status', '#705da0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (45, 14, 'Com_show_slave_status', 'Com_show_slave_status', '#ba43a9', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    15,
    'Top Commands Counters Hourly',
    0,
'   The Com_{{xxx}} statement counter variables indicate the number
of times each xxx statement has been executed. There is one status 
variable for each type of statement. For example, Com_delete and 
Com_update count DELETE and UPDATE statements, respectively. 
Com_delete_multi and Com_update_multi are similar but apply to 
DELETE and UPDATE statements that use multiple-table syntax.', 
    1,
    'TABLE_RIGHT',
    'BAR',
    'Command, Handlers, Processes',
    1,
    2,
    14,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (46, 15, 'Com_select', 'Com_select', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (47, 15, 'Com_update', 'Com_update', '#e24d42', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (48, 15, 'Com_stmt_execute', 'Com_stmt_execute', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (49, 15, 'Com_delete', 'Com_delete', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (50, 15, 'Com_begin', 'Com_begin', '#7eb26d', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    16,
    'MySQl Handlers',
    0,
'   Handler statistics are internal statistics on how MySQL
is selecting, updating, inserting, and modifying rows, tables, 
and indexes.

   This is in fact the layer between the Storage Engine and MySQL.

- read_rnd_next is incremented when the server performs a full
table scan and this is a counter you don''t really want to see 
with a high value.

- read_key is incremented when a read is done with an index.

- read_next is incremented when the storage engine is asked 
to ''read the next index entry''. A high value means a lot of 
index scans are being done.', 
    1,
    'TABLE_RIGHT',
    'AREA',
    'Command, Handlers, Processes',
    0,
    2,
    15,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (51, 16, 'Handler_read_rnd_next', 'read_rnd_next', '#b7dbab', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (52, 16, 'Handler_read_next', 'read_next', '#c15c17', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (53, 16, 'Handler_tmp_write', 'tmp_write', '#70dbed', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (54, 16, 'Handler_read_rnd', 'read_rnd', '#6d1f62', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (55, 16, 'Handler_icp_match', 'icp_match', '#e24d42', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (56, 16, 'Handler_icp_attempts', 'icp_attempts', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (57, 16, 'Handler_read_key', 'read_key', '#cca300', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (58, 16, 'Handler_update', 'update', '#f9ba8f', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (59, 16, 'Handler_delete', 'delete', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (60, 16, 'Handler_write', 'write', '#f29191', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (61, 16, 'Handler_tmp_update', 'tmp_update', '#f4d598', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (62, 16, 'Handler_read_rnd_delete', 'read_rnd_delete', '#584477', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    17,
    'MySQL Transaction Handlers',
    0,
    '', 
    1,
    'TABLE_RIGHT',
    'AREA',
    'Command, Handlers, Processes',
    0,
    2,
    16,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (63, 17, 'Handler_commit', 'commit', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (64, 17, 'Handler_prepare', 'prepare', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (65, 17, 'Handler_rollback', 'rollback', '#6ed0e0', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    18,
    'MySQL Query Cache Memory',
    0,
'   The query cache has huge scalability problems in that
only one thread can do an operation in the query cache at
the same time. This serialization is true not only for
SELECTs, but also for INSERT/UPDATE/DELETE.

    This also means that the larger the query_cache_size
is set to, the slower those operations become. In concurrent
environments, the MySQL Query Cache quickly becomes a
contention point, decreasing performance. MariaDB and AWS
Aurora have done work to try and eliminate the query cache
contention in their flavors of MySQL, while MySQL 8.0 has
eliminated the query cache feature.

    The recommended settings for most environments is to
set: query_cache_type=0 query_cache_size=0

    Note that while you can dynamically change these values,
to completely remove the contention point you have to restart
the database.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Query Cache',
    0,
    1,
    19,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (66, 18, 'query_cache_size', 'Query Cache Size', '#eab839', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (67, 18, 'Qcache_free_memory', 'Free Memory', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    19,
    'MySQL Query cache Activity',
    0,
'   The query cache has huge scalability problems in that
only one thread can do an operation in the query cache at
the same time. This serialization is true not only for
SELECTs, but also for INSERT/UPDATE/DELETE.

    This also means that the larger the query_cache_size
is set to, the slower those operations become. In concurrent
environments, the MySQL Query Cache quickly becomes a
contention point, decreasing performance. MariaDB and AWS
Aurora have done work to try and eliminate the query cache
contention in their flavors of MySQL, while MySQL 8.0 has
eliminated the query cache feature.

    The recommended settings for most environments is to
set: query_cache_type=0 query_cache_size=0

    Note that while you can dynamically change these values,
to completely remove the contention point you have to restart
the database.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Query Cache',
    0,
    1,
    20,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (68, 19, 'Qcache_not_cached', 'Not Cached', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (69, 19, 'Qcache_queries_in_cache', 'Queries in Cache', '#e24d42', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (70, 19, 'Qcache_lowmem_prunes', 'Prunes', '#dd7c3a', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (71, 19, 'Qcache_inserts', 'Inserts', '#eab839', 1, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    20,
    'MySQL File Openings',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Files and Tables',
    0,
    1,
    21,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (72, 20, 'DIVIDE Opened_files Uptime 3600', 'Openings per hour', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (73, 20, 'DIVIDE Opened_files Uptime 60', 'Openings per min', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (74, 20, 'DIVIDE Opened_files Uptime', 'Openings per sec', '#68c3d3', 0, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    21,
    'MySQL Open Files',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Files and Tables',
    0,
    1,
    22,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (75, 21, 'Open_files', 'Open Files', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (76, 21, 'Opened_files', 'Opened Files', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (77, 21, 'open_files_limit', 'Open Files Limit', '#6ed0e0', 0, 1);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    22,
    'MySQL Table Open Cache Status',
    0,
'   The recommendation is to set the table_open_cache_instances
to a loose correlation to virtual CPUs, keeping in mind that
more instances means the cache is split more times. If you have
a cache set to 500 but it has 10 instances, each cache will only
have 50 cached.

    The table_definition_cache and table_open_cache can be left
as default as they are auto-sized MySQL 5.6 and above (ie: do
not set them to any value).', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Table Openings',
    0,
    1,
    23,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (78, 22, 'Table_open_cache_hits', 'Table Open Cache Hits', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (79, 22, 'DIVIDE Open_tables Opened_tables', 'Hits', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (80, 22, 'DIVIDE table_open_cache Opened_tables 100', 'Table Open Cache Hits Rate', '#e24d42', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (81, 22, 'Table_open_cache_misses', 'Misses', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (82, 22, 'Opened_files', 'Openings', '#7eb26d', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    23,
    'MySQL Open Table',
    0,
'   The recommendation is to set the table_open_cache_instances to
a loose correlation to virtual CPUs, keeping in mind that more
instances means the cache is split more times. If you have a
cache set to 500 but it has 10 instances, each cache will only
have 50 cached.

    The table_definition_cache and table_open_cache can be left as
default as they are auto-sized MySQL 5.6 and above (ie: do not set
them to any value).', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Table Openings',
    0,
    1,
    24,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (83, 23, 'Table_open_cache', 'Table Open Cache', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (84, 23, 'Open_tables', 'Open Tables', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    24,
    'MySQL Table Definition Cache',
    0,
'   The recommendation is to set the table_open_cache_instances to
a loose correlation to virtual CPUs, keeping in mind that more
instances means the cache is split more times. If you have a
cache set to 500 but it has 10 instances, each cache will only
have 50 cached.

    The table_definition_cache and table_open_cache can be left as
default as they are auto-sized MySQL 5.6 and above (ie: do not set
them to any value).', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'MySQL Table Definition Cache',
    0,
    1,
    25,
    'Profiling'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (85, 24, 'Table_definition_cache', 'Table Definitions Cache Size', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (86, 24, 'Open_table_definitions', 'Open Table Definitions', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (87, 24, 'Opened_table_definitions', 'Opened Table Definitions', '#6ed0e0', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    25,
    'MySQL Replication Delay',
    0,
    '', 
    1,
    'TABLE_RIGHT',
    'AREA',
    '',
    0,
    2,
    1,
    'Replication'
);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (88, 25, 'Seconds_Behind_Master', 'Lag', '#c34239', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    26,
    'Replication Sync Status',
    0,
    '', 
    1,
    'TABLE_RIGHT',
    'AREA',
    '',
    0,
    2,
    2,
    'Replication'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (89, 26, 'Exec_Master_Log_Pos', 'Exec Master Log Pos', '#246d21', 0, 0);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (90, 26, 'Read_Master_Log_Pos', 'Read Master Log Pos', '#c34239', 0, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    27,
    'Relay Log Space',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    '',
    0,
    1,
    3,
    'Replication'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (91, 27, 'Relay_Log_Space', 'Relay Log Space', '#246d21', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    28,
    'Relay Log Writen Hourly',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'BAR',
    '',
    1,
    1,
    4,
    'Replication'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (92, 28, 'Relay_Log_Space', 'Relay Log Space', '#246d21', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    29,
    'InnoDB Transactions',
    0,
'   InnoDB is an MVCC storage engine, which means you can start a
 transaction and continue to see a consistent snapshot even as the
 data changes. This is implemented by keeping old versions of rows
 as they are modified.
    The InnoDB History List is the undo logs which are used to 
store these modifications. They are a fundamental part of InnoDB’s 
transactional architecture.
    If history length is rising regularly, do not let open 
connections linger for a long period as this can affect the 
performance of InnoDB considerably. It is also a good idea to look 
for long running queries in Query Analytics.
', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Checkpoint',
    0,
    2,
    1,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (93, 29, 'TRANSACTIONS__HISTORY_LENGTH', 'History length', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    30,
    'InnoDB Row Operations',
    0,
'   This graph allows you to see which operations occur and the 
number of rows affected per operation. A graph like Queries Per
Second will give you an idea of queries, but one query could 
effect millions of rows.
', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Row Operations',
    0,
    1,
    2,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (94, 30, 'ROW_OPERATIONS__READ', 'Rows read', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (95, 30, 'ROW_OPERATIONS__UPDATED', 'Rows updated', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (96, 30, 'ROW_OPERATIONS__DELETED', 'Rows deleted', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (97, 30, 'ROW_OPERATIONS__INSERTED', 'Rows inserted', '#eab839', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    31,
    'InnoDB Row Lock Time',
    0,
'   When data is locked, then that means that another session 
can NOT update that data until the lock is released (which 
unlocks the data and allows other users to update that data). 
Locks are usually released by either a ROLLBACK or COMMIT SQL 
statement.

    InnoDB implements standard row-level locking where there 
are two types of locks, shared (S) locks and exclusive (X) 
locks.

- A shared (S) lock permits the transaction that holds the 
lock to read a row.
- An exclusive (X) lock permits the transaction that holds 
the lock to update or delete a row.

    Average Row Lock Wait Time is the row lock wait time 
divided by the number of row locks.
    Row Lock Waits are how many times a transaction waited 
on a row lock per second. Row Lock Wait Load is a rolling 
5 minute average of Row Lock Waits.
', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Row Operations',
    0,
    1,
    3,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (98, 31, 'TRANSACTIONS__AVG__LOCK_WAIT', 'Avg Row Lock Wait Time', '#bf1b00', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (99, 31, 'TRANSACTIONS__LOCK_WAIT', 'Row Lock Waits', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    32,
    'InnoDB I/O',
    0,
'   Data Writes - The total number of InnoDB data writes.

    Data Reads - The total number of InnoDB data reads (OS file 
reads).
    Log Writes - The number of physical writes to the InnoDB 
redo log file. 

    Data Fsyncs - The number of fsync() operations. The frequency 
of fsync() calls is influenced by the setting of the 
innodb_flush_method configuration option.
', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'I/O',
    0,
    1,
    4,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (100, 32, 'FILE_IO__WRITES', 'Data Writes', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (101, 32, 'FILE_IO__READS', 'Data Reads', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (102, 32, 'FILE_IO__LOG', 'Log Writes', '#ef843c', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (103, 32, 'FILE_IO__FSYNCS', 'Data Fsyncs', '#6ed0e0', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    33,
    'InnoDB Log File Usage Hourly',
    0,
'   Along with the buffer pool size, innodb_log_file_size is the 
most important setting when we are working with InnoDB. This 
graph shows how much data was written to InnoDB''s redo logs over 
each hour. When the InnoDB log files are full, InnoDB needs to 
flush the modified pages from memory to disk.

    The rules of the thumb is to keep one hour of traffic in 
those logs and let the checkpointing perform its work as smooth 
as possible. If you don''t do this, InnoDB will do synchronous 
flushing at the worst possible time, ie when you are busiest.

    This graph can help guide you in setting the correct 
innodb_log_file_size.
', 
    1,
    'TABLE_BOTTOM',
    'BAR',
    'I/O',
    1,
    1,
    5,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (104, 33, 'LOG__HOURLY__PAGES_FLUSHED', 'Data Written', '#e0752d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    34,
    'Innodb Logging Performance',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'I/O',
    0,
    1,
    6,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (105, 34, 'LOG__INNODB_LOG_FILE_SIZE__TIME', 'Time to Use Redo Log Space', '#447ebc', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (106, 34, 'LOG__INNODB_LOG_BUFFER_SIZE__TIME', 'Time to Use In-Memory Log Buffer', '#6ed0e0', 0, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    35,
    'InnoDB Deadlocks',
    0,
'   Data Writes - The total number of InnoDB data writes.

    Data Reads - The total number of InnoDB data reads (OS file 
reads).
    Log Writes - The number of physical writes to the InnoDB 
redo log file. 

    Data Fsyncs - The number of fsync() operations. The frequency 
of fsync() calls is influenced by the setting of the 
innodb_flush_method configuration option.
', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Deadlocks and ICP',
    0,
    2,
    7,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (107, 35, 'DEADLOCKS__DEADLOCKS', 'Deadlocks', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    36,
    'InnoDB Buffer Pool Content',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Buffer Pool',
    0,
    1,
    8,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (108, 36, 'BUFFER_POOL__DATA_TOTAL', 'Data Total', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (109, 36, 'BUFFER_POOL__DATA_DIRTY', 'Data Dirty', '#eab839', 0, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    37,
    'InnoDB Buffer Pool Pages',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Buffer Pool',
    0,
    1,
    9,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (110, 37, 'Innodb_buffer_pool_pages_data', 'data', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (111, 37, 'Innodb_buffer_pool_pages_misc', 'misc', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (112, 37, 'Innodb_buffer_pool_pages_free', 'free', '#6ed0e0', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    38,
    'InnoDB Buffer Pool I/O',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Buffer Pool I/O',
    0,
    1,
    10,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (113, 38, 'BUFFER_POOL_AND_MEMORY__READ', 'Pages Created', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (114, 38, 'BUFFER_POOL_AND_MEMORY__CREATED', 'Pages Read', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (115, 38, 'BUFFER_POOL_AND_MEMORY__WRITTEN', 'Pages Written', '#6ed0e0', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    39,
    'InnoDB Buffer Pool Requests',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Buffer Pool I/O',
    0,
    1,
    11,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (116, 39, 'Innodb_buffer_pool_read_requests', 'Pages Created', '#7eb26d', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (117, 39, 'Innodb_buffer_pool_write_requests', 'Pages Read', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (118, 39, 'Innodb_buffer_pool_reads', 'Pages Written', '#6ed0e0', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    40,
    'Innodb Buffer Pool Hit Ratio',
    0,
'   The InnoDB Buffer Pool hit ratio is a indicator how often
 your pages are retrieved from memory instead of disk:

- Innodb_buffer_pool_read_requests indicates the the number 
of logical read requests (read from memory) InnoDB has done.

- Innodb_buffer_pool_reads indicates the number of logical 
reads that InnoDB could not satisfy from the buffer pool, 
and had to read directly from the disk (physical reads).
    
    A InnoDB Buffer Pool hit ratio below 99.9% is a weak indicator 
that your InnoDB Buffer Pool could be increased.
', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Buffer Pool I/O',
    0,
    1,
    12,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (119, 40, 'BUFFER_POOL_IO__HIT_RATIO', 'Hit Ratio', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    41,
    'InnoDB Change Buffer Activity',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Change/Insert Buffer',
    0,
    1,
    13,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (120, 41, 'INSERT_BUFFER__MERGED_DELETE_MARK', 'Merged Delete Marks', '#dd7c3a', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (121, 41, 'INSERT_BUFFER__MERGED_DELETE', 'Merged Deletes', '#6ed0e0', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (122, 41, 'INSERT_BUFFER__MERGED_INSERT', 'Merged Inserts', '#eab839', 1, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (123, 41, 'INSERT_BUFFER__MERGES', 'Merges', '#78a868', 1, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    42,
    'InnoDB Connection - OS Waits',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Semaphores',
    0,
    1,
    14,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (124, 42, 'SEMAPHORES__RW_LOCKS_S_WAITS', 'RW locks S OS Waits', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (125, 42, 'SEMAPHORES__RW_LOCKS_X_WAITS', 'RW locks X OS Waits', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (126, 42, 'SEMAPHORES__RW_LOCKS_SX_WAITS', 'RW locks SX OS Waits', '#6ed0e0', 0, 0);

INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    43,
    'InnoDB Connection - Spin Rounds',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'Semaphores',
    0,
    1,
    15,
    'InnodbMatrix'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (127, 43, 'SEMAPHORES__RW_LOCKS_S_ROUNDS', 'RW locks S Spin Rounds', '#7eb26d', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (128, 43, 'SEMAPHORES__RW_LOCKS_X_ROUNDS', 'RW locks X Spin Rounds', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (129, 43, 'SEMAPHORES__RW_LOCKS_SX_ROUNDS', 'RW locks SX Spin Rounds', '#6ed0e0', 0, 0);

 
INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    44,
    'MyISAM Indexes',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'MyISAM Metrics',
    0,
    2,
    16,
    'MyISAMMetrics'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (130, 44, 'Key_write_requests', 'Key Write Requests', '#ef843c', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (131, 44, 'Key_writes', 'Key Writes', '#6ed0e0', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (132, 44, 'Key_read_request', 'Key Read Requests', '#eab839', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (133, 44, 'Key_reads', 'Key Reads', '#7eb26d', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    45,
    'MyISAM Key Buffer Performance',
    0,
'   The Key Read Ratio (Key_reads/Key_read_requests) ratio should 
normally be less than 0.01.

    The Key Write Ratio (Key_writes/Key_write_requests) ratio is 
usually near 1 if you are using mostly updates and deletes, but 
might be much smaller if you tend to do updates that affect many 
rows at the same time or if you are using the DELAY_KEY_WRITE 
table option.', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'MyISAM Metrics',
    0,
    1,
    17,
    'MyISAMMetrics'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (134, 45, 'DIVIDE Key_reads Key_read_requests', 'Key Write Ratio', '#ef843c', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (135, 45, 'DIVIDE Key_writes Key_write_requests', 'Key Writes', '#6ed0e0', 0, 0);


INSERT INTO GS_CHART (
    ID,
    CHART_NAME, 
    CHARTTYPE, 
    DESCRIPTION,
    STATUS,
    UITYPE,
    CHARTSTYLE,
    GROUP_NAME,
    HOURLY,
    GROUP_WIDTH,
    CHART_ORDER,
    TAB,
    ) VALUES (
    46,
    'MyISAM Key Cache',
    0,
    '', 
    1,
    'TABLE_BOTTOM',
    'AREA',
    'MyISAM Metrics',
    0,
    1,
    18,
    'MyISAMMetrics'
);

INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (136, 46, 'key_buffer_size', 'Key Buffer Size', '#ef843c', 0, 1);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (137, 46, 'Key_blocks_used', 'Key Blocks Used', '#6ed0e0', 0, 0);
INSERT INTO GS_CHART_VARIABLE (ID, CHART_ID, GS_NAME, DISPLAY_NAME, DISPLAYCOLOR, "TYPE", "VARIABLE") 
    VALUES (138, 46, 'Key_blocks_not_flushed', 'Key Blocks Not Flushed', '#eab839', 0, 0);
