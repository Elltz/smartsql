/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import np.com.ngopal.smart.sql.structure.models.DesignerIndex.IndexOrder;
import np.com.ngopal.smart.sql.structure.models.DesignerIndex.IndexStorageType;
import np.com.ngopal.smart.sql.structure.models.DesignerIndex.IndexType;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class DesignerTableWrapper {
    
    private String _name,_schema,_collation,_engine, _comments;
    
    private ArrayList<DesignerColumnWrapper> _columns;
    ArrayList<DesignerIndexWrapper> _indexes;
    ArrayList<DesignerForeignKeyWrapper> _foreignKeys;
    
    public DesignerTableWrapper(DesignerTable dt) {
        _collation = dt.collation().get();
        _columns = new ArrayList();
        for (DesignerColumn dc : dt.columns()) {
            _columns.add(new DesignerColumnWrapper(dc));
        }
        _comments = dt.comments().get();
        _engine = dt.engine().get();
        _foreignKeys = new ArrayList();
        for(DesignerForeignKey dfk : dt.foreignKeys()){
            _foreignKeys.add(new DesignerForeignKeyWrapper(dfk));
        }
        _indexes = new ArrayList();
        for(DesignerIndex di : dt.indexes()){
            _indexes.add(new DesignerIndexWrapper(di));
        }
        _name = dt.name().get();
        _schema = dt.schema().get();
    }
    
    public DesignerTable convertToDesignerTable(){ 
        ArrayList<DesignerIndex> di = new ArrayList();
        for(DesignerIndexWrapper diw : _indexes){
            di.add(diw.convertToDesignerIndex());
        }
        ArrayList<DesignerColumn> dc = new ArrayList();
        for(DesignerColumnWrapper dcw : _columns){
            dc.add(dcw.convertToDesignerColumn());
        }
        ArrayList<DesignerForeignKey> df = new ArrayList();
        for(DesignerForeignKeyWrapper dfw : _foreignKeys){
            df.add(dfw.convertToDesignerForeignKey());
        }
        return new DesignerTable(
                _name, _schema, _collation, _engine, _comments,
                dc, di, df);
    }
    
    private class DesignerIndexWrapper {

        private String di_name, di_keyBlockSize, di_parser, di_comment;

        private IndexType type = IndexType.INDEX;

        private ArrayList<DesignerColumnWrapper> columns;

        private HashMap<DesignerColumnWrapper, Integer> orderMap;

        private HashMap<DesignerColumnWrapper, IndexOrder> sortMap;
        
        private HashMap<DesignerColumnWrapper, Long> lengthMap;

        private IndexStorageType storageType;

        private DesignerForeignKeyWrapper foreignKey;

        public DesignerIndexWrapper(DesignerIndex di) {
            this.di_name = di.name().get();
            this.di_comment = di.comment().get();
            this.di_keyBlockSize = di.keyBlockSize().get();
            this.di_parser = di.parser().get();
            this.foreignKey = new DesignerForeignKeyWrapper(di.foreignKey().get());
            this.storageType = di.storageType().get();
            this.type = di.type().get();
            this.columns = new ArrayList();
            for(DesignerColumn dc : di.columns()){
                DesignerIndexWrapper.this.columns.add(new DesignerColumnWrapper(dc));
            }
            orderMap = new HashMap();

            for(Map.Entry<DesignerColumn,Integer> en : di.orderMap().entrySet()){
                orderMap.put(new DesignerColumnWrapper(en.getKey()), en.getValue());
            }
            
            sortMap = new HashMap();
            
            for(Map.Entry<DesignerColumn,IndexOrder> en : di.sortMap().entrySet()){
               sortMap.put(new DesignerColumnWrapper(en.getKey()), en.getValue());
            }
            
            lengthMap = new HashMap();
            for(Map.Entry<DesignerColumn,Long> en : di.lengthMap().entrySet()){
               lengthMap.put(new DesignerColumnWrapper(en.getKey()), en.getValue());
            }
            
        }

        public DesignerIndex convertToDesignerIndex() {
            ArrayList<DesignerColumn> dcArray = new ArrayList();
            for(DesignerColumnWrapper dcw : columns){
                dcArray.add(dcw.convertToDesignerColumn());
            }
            
            HashMap<DesignerColumn, Integer> order = new HashMap();
            
            for(Map.Entry<DesignerColumnWrapper, Integer> en : orderMap.entrySet()){
                order.put(en.getKey().convertToDesignerColumn(),en.getValue());
            }
            HashMap<DesignerColumn, IndexOrder> sort = new HashMap();
            for(Map.Entry<DesignerColumnWrapper, IndexOrder> en : sortMap.entrySet()){
                sort.put(en.getKey().convertToDesignerColumn(),en.getValue());
            }
            HashMap<DesignerColumn, Long> len = new HashMap();
            for(Map.Entry<DesignerColumnWrapper, Long> en : lengthMap.entrySet()){
                len.put(en.getKey().convertToDesignerColumn(),en.getValue());
            }
            
            return new DesignerIndex(
                    di_name, type, dcArray, order, sort, len,
                    storageType, di_keyBlockSize, di_parser, di_comment, foreignKey.convertToDesignerForeignKey());
        }

    }
    
    private class DesignerColumnWrapper {
        
    private String dc_name,dc_collation, dc_comments, dc_datatype,dc_default;
    private boolean dc_storage,dc_virtual,dc_primaryKey,dc_notnull,dc_unique,dc_binary,
            dc_unsigned,dc_zerofill,dc_autoin,dc_generated;
        
        public DesignerColumnWrapper(DesignerColumn dc){
            dc_autoin = dc.autoIncrement().get();
            dc_binary = dc.binary().get();
            dc_collation = dc.collation().get();
            dc_comments = dc.comments().get();
            dc_datatype = dc.dataType().get();
            dc_default = dc.defaultValue().get();
            dc_generated = dc.generated().get();
            dc_name = dc.name().get();
            dc_notnull = dc.notNull().get();
            dc_primaryKey = dc.primaryKey().get();
            dc_storage = dc.storage().get();
            dc_unique = dc.unique().get();
            dc_unsigned = dc.unsigned().get();
            dc_virtual = dc.virtual().get();
            dc_zerofill = dc.zeroFill().get();
        }
        
        public DesignerColumn convertToDesignerColumn(){
            return new DesignerColumn(dc_name, dc_collation, dc_comments,
                    dc_datatype, dc_default, dc_storage, dc_virtual, dc_primaryKey,
                    dc_notnull, dc_unique, dc_binary, dc_unsigned, dc_zerofill, 
                    dc_autoin, dc_generated);
        }
        
    }
    
    private class DesignerForeignKeyWrapper {
    
    private String dfk_name,dfk_comment,uuid;
    private DesignerTableWrapper refTable;
    private ArrayList<DesignerColumnWrapper> dfk_column;
    private HashMap<DesignerColumnWrapper,DesignerColumnWrapper> refColumn;
    private DesignerForeignKey.OnAction dfk_onUpdate;
    private DesignerForeignKey.OnAction dfk_onDelete;
    private boolean skipSql;
        
        public DesignerForeignKeyWrapper(DesignerForeignKey dfk){
            
            refTable = new DesignerTableWrapper(dfk.referencedTable().get());
            dfk_column = new ArrayList();
            for(DesignerColumn dc : dfk.columns()){
                dfk_column.add(new DesignerColumnWrapper(dc));
            }
            dfk_name =dfk.name().get();
            dfk_comment = dfk.comment().get();
            dfk_onDelete = dfk.onDelete().get();
            dfk_onUpdate = dfk.onUpdate().get();
            skipSql = dfk.skipSqlGeneration().get();
            refColumn = new HashMap();
            for(Map.Entry<DesignerColumn,DesignerColumn> en : dfk.referencedColumns().entrySet()){
                refColumn.put(new DesignerColumnWrapper(en.getKey()),
                        new DesignerColumnWrapper(en.getValue()));
            }
            uuid = dfk.uuid().get();
        }
        
        public DesignerForeignKey convertToDesignerForeignKey(){
            HashMap<DesignerColumn,DesignerColumn> map = new HashMap();
            for(Map.Entry<DesignerColumnWrapper,DesignerColumnWrapper> en :
                    refColumn.entrySet()){
                map.put(en.getKey().convertToDesignerColumn(), 
                        en.getValue().convertToDesignerColumn());
            }
            ArrayList<DesignerColumn> col = new ArrayList();
            for(DesignerColumnWrapper dc : dfk_column){
                col.add(dc.convertToDesignerColumn());
            }
            
            return new DesignerForeignKey(dfk_name, dfk_comment, refTable.convertToDesignerTable(),
                    col, map, dfk_onUpdate, dfk_onDelete, skipSql,uuid);
        }
        
    }
    
}
