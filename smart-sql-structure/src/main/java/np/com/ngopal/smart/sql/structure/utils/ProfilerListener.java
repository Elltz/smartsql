/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import javafx.collections.ObservableList;
import np.com.ngopal.smart.sql.structure.misc.ProfilerInnerControllerWorkProcAuth;
import np.com.ngopal.smart.sql.structure.models.ProfilerData;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public interface ProfilerListener {

    public void startProfiler(Date startDate);

    public void stopProfiler();

    public void clearProfilerData();

    public void userChartsChanged();

    public void scrollForward();

    public void scrollBackward();

    public void zoomInAction();

    public void zoomOutAction();

    public void showAllAction();

    public void openData(ProfilerData pd);

    public void saveProfilerData(ProfilerData pd);

    public void showProfilerData(Date fromDate);

    public void exportToXls(
            List<String> sheets,
            List<ObservableList<ObservableList>> totalData,
            List<LinkedHashMap<String, Boolean>> totalColumns,
            List<List<Integer>> totalColumnWidths,
            List<byte[]> images,
            List<Integer> heights,
            List<Integer> colspans);

    public void destroy();
    
    public ProfilerInnerControllerWorkProcAuth getInnerControllerWorkProcAuthority();

}
