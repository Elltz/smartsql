/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.modules;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.components.DynamicMenuItem;
import np.com.ngopal.smart.sql.components.IconedMenuItem;
import np.com.ngopal.smart.sql.components.TextMenuItem;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import np.com.ngopal.smart.sql.db.DeadlockService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.ProfilerTabContentController;
import np.com.ngopal.smart.sql.structure.models.ProfilerData;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.constructImageViewWithSize;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.showDebugger;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.showSlowQueryAnalyzer;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.showTunerSql;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public abstract class ProfilerModule extends ModuleController  implements ConnectionModuleInterface {
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerDataManager profilerService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerDataManager dbProfilerService;
    
    @Setter(AccessLevel.PUBLIC)
    protected PreferenceDataService preferenceDataService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ColumnCardinalityService columnCardinalityService;

    @Setter(AccessLevel.PUBLIC)
    protected QAPerformanceTuningService performanceTuningService;

    @Setter(AccessLevel.PUBLIC)
    protected QARecommendationService recommendationService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerSettingsService settingsService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerChartService chartService; 
        
    @Setter(AccessLevel.PUBLIC)
    protected DeadlockService deadlockService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ConnectionParamService paramService;
    
    @Setter(AccessLevel.PUBLIC)
    protected DBDebuggerProfilerService debuggerProfilerService;
    
    @Setter(AccessLevel.PUBLIC)
    protected UserUsageStatisticService usageService;

    @Setter(AccessLevel.PUBLIC)
    protected ReadOnlyObjectProperty<Database> databaseSelectedItemProperty;
        
    protected final Map<ConnectionSession, ProfilerTabContentController> profilerControllers = new HashMap<>();

    protected final List<ConnectionParams> openedParams = new ArrayList<>();

    protected MenuItem newItem;
    protected MenuItem openItem;
    protected MenuItem saveItem;
    protected MenuItem saveAsItem;
    protected MenuItem exportItem;
    protected MenuItem exitItem;

    protected MenuItem timeElapsedItem;
    protected MenuItem zoomText;
    protected MenuItem startItem;
    protected MenuItem scrollForwardItem;
    protected MenuItem scrollBackwardItem;
    protected MenuItem chartEditItem;

    protected MenuItem clearCachedDataItem;

    protected MenuItem stopItem;
    protected MenuItem settingsItem;
    protected MenuItem zoomOut;
    protected MenuItem zoomAlt;
    protected MenuItem zoomIn;
    protected MenuItem tTime;
    
    protected MenuItem selectTime;

    protected MenuItem monitoringStatusItem;
    protected MenuItem debuggerItem;
    protected MenuItem slowQueryAnalyzer;
    protected MenuItem tunerItem;

    
    protected long lastTime = 0l;
    protected static final long LAST_HOUR_1 = 60 * 60 * 1000;
    protected static final long LAST_HOUR_3 = 3 * 60 * 60 * 1000;
    protected static final long LAST_HOUR_6 = 6 * 60 * 60 * 1000;
    protected static final long LAST_HOUR_12 = 12 * 60 * 60 * 1000;
    protected static final long LAST_HOUR_24 = 24 * 60 * 60 * 1000;
    protected static final long LAST_DAYS_3 = 3 * 24 * 60 * 60 * 1000;
    protected static final long LAST_WEEK_1 = 7 * 24 * 60 * 60 * 1000;
    
    protected SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd,YY h:mm:ss aa");            
            
    protected void selectLast(long time) {
        lastTime = time;
        
        if (time == 0) {
            getCurrentProfilerController().clearProfilerData();
            selectTime.setText("Not selected");
        } else {
            Date curretDate = new Date();
            Date prevDate = new Date(curretDate.getTime() - time);
            selectTime.setText(dateFormat.format(prevDate) + " - " + dateFormat.format(curretDate));
            
            getCurrentProfilerController().showProfilerData(prevDate);
        }
    }
        
    @Override
    public synchronized boolean canOpenConnection(ConnectionParams params) {
        return false;
    }

    @Override
    public void connectionOpened(ConnectionParams params) {
        log.info("connection opened on THREAD " + Thread.currentThread().getName());
        Platform.runLater(() -> {
            installListenersOnMenuItemObjects();
        });
    }

    @Override
    public boolean install() {
        super.install();
        startItem = new MenuItem("Start", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getStart_image()), 20, 20));
        startItem.setOnAction((ActionEvent event) -> {
            selectLast(0);
            getCurrentProfilerController().startProfiler();
        });

        stopItem = new MenuItem("Stop", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getStop_image()),20,20));
        stopItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getCurrentProfilerController().stopProfiler();
            }
        });

        newItem = new MenuItem("New", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getNew_image()), 20, 20));
        newItem.setOnAction((ActionEvent event) -> {
            newProfiler();
        });

        openItem = new MenuItem("Open", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getOpen_image()), 20, 20));
        openItem.setOnAction((ActionEvent event) -> {
            openProfilerData();
        });

        saveItem = new MenuItem("Save", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getSave_image()), 20, 20));
        saveItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().saveProfilerData(false);
        });

        saveAsItem = new MenuItem("Save as", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getSaveAs_image()), 20, 20));
        saveAsItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().saveProfilerData(true);
        });

        exportItem = new MenuItem("Export bottom grids");
        exportItem.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getXls_export_image()), 20, 20));
        exportItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().exportToXls();
        });

        exitItem = new MenuItem("Exit", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getExit_image()), 20, 20));
        exitItem.setOnAction((ActionEvent event) -> {
            closeProfiler();
        });

        scrollBackwardItem = new MenuItem("Scroll back", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getScroll_backward_image()), 20, 20));
        scrollBackwardItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().scrollBackward();
        });

        scrollForwardItem = new MenuItem("Scroll forward", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getScroll_forward_image()), 20, 20));
        scrollForwardItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().scrollForward();
        });

        chartEditItem = new MenuItem("User charts", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getChart_edit_image()), 20, 20));
        chartEditItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().showUserCharts();
        });

        clearCachedDataItem = new MenuItem("Clear cached profiler data", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getClear_profiler_history_image()), 20, 20));
        clearCachedDataItem.setOnAction((ActionEvent event) -> {

            getCurrentProfilerController().makeBusy(true);

            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                    DialogHelper.showInfo(controller, "Done", "Data cleared!", null);
                    getCurrentProfilerController().makeBusy(false);
                }

                @Override
                public void run() {
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY_TEMPLATE_PQTABLE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQSTATE_STAT));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY_STATS));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQSTATE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQTABLE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQDB));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY_TEMPLATE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUSER_HOST));

                    callUpdate = true;
                }
            });
        });

        settingsItem = new MenuItem("Profiler settings");
        settingsItem.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getSettings_image()), 20, 20));
        settingsItem.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().showSettings();
        });

        zoomOut = new IconedMenuItem("Zoom Out");
        zoomOut.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getZoom_left_image()), 20, 20));
        zoomOut.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().zoomOutAction();
        });

        zoomAlt = new MenuItem("Show all");
        zoomAlt.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getProfiler_showAll_small_image()), 20, 20));
        zoomAlt.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().showAllAction();
        });

        zoomIn = new IconedMenuItem("Zoom In");
        zoomIn.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getZoom_right_image()), 20, 20));
        zoomIn.setOnAction((ActionEvent event) -> {
            getCurrentProfilerController().zoomInAction();
        });

        tTime = new MenuItem("Toggle time visibility");
        tTime.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getShow_time_image()), 20, 20));
        tTime.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            }
        });

        selectTime = new DynamicMenuItem("Not selected") {

            @Override
            public Node createNode() {

                Popup p = new Popup();
                p.setAutoHide(true);

                Button b = new Button();

                b.textProperty().bind(textProperty());
                b.disableProperty().bind(disableProperty());

                b.setId("tollbarlabel");
                b.setGraphic(constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getClock_image()), 20, 20));
                b.setMinWidth(200);
                b.setFocusTraversable(false);

                Button last1 = new Button("Last 1 hour");
                last1.setId("tollbarlabel");
                last1.setMaxWidth(Double.MAX_VALUE);
                last1.setFocusTraversable(false);
                last1.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_HOUR_1);
                    p.hide();
                });

                Button last3 = new Button("Last 3 hours");
                last3.setId("tollbarlabel");
                last3.setMaxWidth(Double.MAX_VALUE);
                last3.setFocusTraversable(false);
                last3.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_HOUR_3);
                    p.hide();
                });

                Button last6 = new Button("Last 6 hours");
                last6.setId("tollbarlabel");
                last6.setMaxWidth(Double.MAX_VALUE);
                last6.setFocusTraversable(false);
                last6.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_HOUR_6);
                    p.hide();
                });

                Button last12 = new Button("Last 12 hours");
                last12.setId("tollbarlabel");
                last12.setMaxWidth(Double.MAX_VALUE);
                last12.setFocusTraversable(false);
                last12.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_HOUR_12);
                    p.hide();
                });

                Button last24 = new Button("Last 24 hours");
                last24.setId("tollbarlabel");
                last24.setMaxWidth(Double.MAX_VALUE);
                last24.setFocusTraversable(false);
                last24.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_HOUR_24);
                    p.hide();
                });

                Button last3days = new Button("Last 3 days");
                last3days.setId("tollbarlabel");
                last3days.setMaxWidth(Double.MAX_VALUE);
                last3days.setFocusTraversable(false);
                last3days.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_DAYS_3);
                    p.hide();
                });

                Button last1week = new Button("Last 1 week");
                last1week.setId("tollbarlabel");
                last1week.setMaxWidth(Double.MAX_VALUE);
                last1week.setFocusTraversable(false);
                last1week.setOnAction((ActionEvent event) -> {
                    selectLast(LAST_WEEK_1);
                    p.hide();
                });

                VBox vbox = new VBox(last1, last3, last6, last12, last24, last3days, last1week);
                vbox.prefWidthProperty().bind(b.widthProperty());
                vbox.setFillWidth(true);
                vbox.setStyle("-fx-background-color: #c4ceda");

                p.getContent().add(vbox);

                b.setOnMouseClicked((MouseEvent event) -> {
                    Bounds bounds = b.localToScreen(b.boundsInLocalProperty().getValue());
                    p.show(Stage.impl_getWindows().next(), bounds.getMinX(), bounds.getMaxY());
                });

                return b;
            }
        };

        timeElapsedItem = new TextMenuItem();
        timeElapsedItem.setUserData("Time frame indicating the start of profiling"
                + " to the current time or end time.  Time frame format is "
                + " the form of "
                + "[start time] - [current/end time]");
        timeElapsedItem.getStyleClass().add("time");

        zoomText = new TextMenuItem();
        zoomText.setUserData("Current zoom ratio");

        monitoringStatusItem = new TextMenuItem();
        monitoringStatusItem.getStyleClass().add("time");

        debuggerItem = new MenuItem("Debugger", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getDebugger_image()), 20, 20));
        debuggerItem.setOnAction((ActionEvent event) -> {
            showDebugger(controller, databaseSelectedItemProperty, chartService, dbProfilerService, preferenceDataService,
                    deadlockService, recommendationService, columnCardinalityService, performanceTuningService,
                    settingsService, paramService, debuggerProfilerService, usageService);
        });

        slowQueryAnalyzer = new MenuItem("MySQL Slow Log Query Analyzer", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getSlow_analyzer_image()), 20, 20));
        slowQueryAnalyzer.setOnAction((ActionEvent event) -> {
            showSlowQueryAnalyzer(controller, dbProfilerService, preferenceDataService, recommendationService, columnCardinalityService, performanceTuningService, paramService, usageService);
        });

        tunerItem = new MenuItem("MySQL Conf Tuner", constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getTuner_image()), 20, 20));
        tunerItem.setOnAction((ActionEvent event) -> {
            //TODO move turner controller to structure and uncomment this code
            showTunerSql(controller, usageService);
        });
        
        return true;
    }
    
    protected void newProfiler() {
        ProfilerTabContentController ptc = getCurrentProfilerController();
        if (ptc != null) {
            ptc.stopProfiler();
            ptc.clearProfilerData();
        }
    }

    protected void openProfilerData() {
        ProfilerTabContentController ptc = getCurrentProfilerController();
        if (ptc != null) {

            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("SmartSQL Profiller", "*.ssp"),
                    new FileChooser.ExtensionFilter("All Files", "*.*"));
            File file = fc.showOpenDialog(controller.getStage());
            if (file != null) {
                ptc.setSavedPath(file.getAbsolutePath());

                Gson gson = new Gson();

                try {
                    ProfilerData data = gson.fromJson(new FileReader(file), ProfilerData.class);
                    if (data != null) {
                        ptc.openData(data);
                    }
                } catch (Throwable ex) {
                    DialogHelper.showError(controller, "Error saveing script", ex.getMessage(), ex);
                }
            }
        }
    }

    protected void closeProfiler() {
        ProfilerTabContentController ptc = getCurrentProfilerController();
        if (ptc != null) {
            controller.closeTab(ptc);
        }
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        return true;
    }

    @Override
    public boolean uninstall() {
        timeElapsedItem = null;
        return true;
    }

    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {
        log.info("post connection called in profilerModule on THREAD " + Thread.currentThread().getName());
        openedParams.add(service.getConnectionParam());
        profilerControllers.put(service, (ProfilerTabContentController) tab.getUserData());

        /**
         * The reason i am adding the below code is, because, i want the
         * profiler connection to die along with the session, so i am adding a
         * change listener that will be triggered when the tab is dying safe to
         * say it has been detached from the tabpane by result of clossing
         * session.
         *
         * Also i will remove the connection params from the openparams
         * arraylist so as not to prevent the user from opening a new profiler
         * session when none exist after the first closed session.
         */
        tab.tabPaneProperty().addListener(new ChangeListener<TabPane>() {
            @Override
            public void changed(ObservableValue<? extends TabPane> observable,
                    TabPane oldValue, TabPane newValue) {
                if (newValue == null) {
                    profilerControllers.remove(service);
                    openedParams.remove(service.getConnectionParam());
                }
            }
        });
    }

    protected ProfilerTabContentController getCurrentProfilerController() {
        return profilerControllers.get(controller.getSelectedConnectionSession().get());
    }

    @Override
    public boolean isConnection() {
        return true;
    }

    @Override
    public Class getDependencyModuleClass() {
        return getClass();
    }

    @Override
    public int getOrder() {
        return 3;
    }
    
    @Override
    public String getInfo() {
        return "Profile your mysql connections & queries";
    }

    protected void installListenersOnMenuItemObjects() {
        
        ProfilerTabContentController profilercontroller = getCurrentProfilerController();
        if (profilercontroller != null) {
//            if (getCurrentProfilerController().getCurrentProfilerEntity() instanceof MetricMonitorController) {                
//                monitoringStatusItem.setDisable(false);
//                monitoringStatusItem.textProperty().bind(
//                        ((MetricMonitorController) getCurrentProfilerController().getCurrentProfilerEntity())
//                        .getCurrentMonitoryStatusTextProperty());
//
//                for (MenuItem mi : Arrays.asList(startItem, stopItem, addNewMetricChartItem, monitoringStatusItem)) {
//                    mi.getProperties().put(currentActiveItemCollectionKey, currentActiveItemCollectionValue);
//                }                
//                saveOsServerDetails();
//            } else {
                
//            monitoringStatusItem.disableProperty().bind(getCurrentProfilerController().metricMonitorSelected());
//            monitoringStatusItem.textProperty().bind(getCurrentProfilerController().getMetricMonitorController().getCurrentMonitoryStatusTextProperty());

            zoomText.textProperty().bind(profilercontroller.zoomCountStage());
            startItem.graphicProperty().bind(profilercontroller.recordGraphic());
            timeElapsedItem.textProperty().bind(profilercontroller.timeElapsedProperty());
            scrollBackwardItem.disableProperty().bind(profilercontroller.zoomDragged().not());
            scrollForwardItem.disableProperty().bind(profilercontroller.zoomDragged().not());
            saveItem.disableProperty().bind(profilercontroller.profilerStarted());
            saveAsItem.disableProperty().bind(profilercontroller.profilerStarted());
            chartEditItem.disableProperty().bind(profilercontroller.profilerStarted());
            zoomAlt.setDisable(false);
            zoomOut.setDisable(false);
            zoomIn.setDisable(false);
            settingsItem.setDisable(false);
            clearCachedDataItem.setDisable(false);
            exitItem.setDisable(false);
            exportItem.setDisable(false);
            newItem.setDisable(false);
            openItem.setDisable(false);
            tTime.setDisable(false);
            timeElapsedItem.setDisable(false);
            selectTime.disableProperty().bind(profilercontroller.profilerStarted());

//            for (MenuItem mi : Arrays.asList(addNewMetricChartItem, newItem, openItem, saveItem, saveAsItem, exportItem, exitItem, startItem, stopItem, settingsItem, clearCachedDataItem,
//                    exportItem, scrollBackwardItem, scrollForwardItem, chartEditItem, zoomAlt, zoomText, zoomOut, zoomIn, tTime, timeElapsedItem)) {
//                mi.getProperties().put(currentActiveItemCollectionKey, currentActiveItemCollectionValue);
//            }
//            }
            
            startItem.disableProperty().bind(profilercontroller.profilerStarted());
            stopItem.disableProperty().bind(profilercontroller.profilerStarted().not());
        }
    }

    public void refreshTopMenuItems() {
        unbindTopMenuItems();
        installListenersOnMenuItemObjects();
    }

    protected void unbindTopMenuItems() {
        selectTime.disableProperty().unbind();
        zoomText.textProperty().unbind();
        timeElapsedItem.textProperty().unbind();
        startItem.graphicProperty().unbind();
        scrollBackwardItem.disableProperty().unbind();
        scrollForwardItem.disableProperty().unbind();
        saveItem.disableProperty().unbind();
        saveAsItem.disableProperty().unbind();
        chartEditItem.disableProperty().unbind();
        clearCachedDataItem.disableProperty().unbind();
        exitItem.disableProperty().unbind();
        exportItem.disableProperty().unbind();
        newItem.disableProperty().unbind();
        openItem.disableProperty().unbind();
        startItem.disableProperty().unbind();
        stopItem.disableProperty().unbind();
        settingsItem.disableProperty().unbind();
        zoomText.disableProperty().unbind();
        zoomIn.disableProperty().unbind();
        zoomOut.disableProperty().unbind();
        zoomAlt.disableProperty().unbind();
        monitoringStatusItem.disableProperty().unbind();
        tTime.disableProperty().unbind();
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return getCurrentProfilerController() == null || getCurrentProfilerController().isNeedOpenInNewTab();
    }
}
