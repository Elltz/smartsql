package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import np.com.ngopal.smart.sql.model.PreferenceData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface DebugTimeProvider {

    public static final String COLOR_RED = "#ff0000";
    public static final String COLOR_RED_LIGHT = "#c44444";
    public static final String COLOR_RED_MORE_LIGHT = "#b75959";
    public static final String COLOR_GREEN = "green";
    public static final String COLOR_WHITE = "white";
    public static final String[] COLORS = new String[] {COLOR_RED, COLOR_RED_LIGHT, COLOR_RED_MORE_LIGHT, COLOR_GREEN, COLOR_WHITE};
    
    public BigDecimal getTime();
    
    public String getState();
    
    public DebugTimeProvider getTimeProvider();
    
    public String getColor(PreferenceData pd);
}
