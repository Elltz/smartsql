/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import np.com.ngopal.smart.sql.structure.models.ProfilerQueryData;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class TimeTableCell extends TableCell<ProfilerQueryData, Number> {

    @Override
    protected void updateItem(Number item, boolean empty) {
        super.updateItem(item, empty);
        this.setText(item != null ? new BigDecimal(item.doubleValue()).setScale(2, RoundingMode.HALF_UP).toString() : "");
        this.setAlignment(Pos.CENTER_RIGHT);
    }
}
