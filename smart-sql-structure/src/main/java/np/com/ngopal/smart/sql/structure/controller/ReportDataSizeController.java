package np.com.ngopal.smart.sql.structure.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @TODO Need move all calculation from table cells
 */
@Slf4j
public class ReportDataSizeController extends BaseController implements Initializable, ReportsContract{
    
    public static final String DATABASE_ITEM__EMPTY      = "";
    public static final String DATABASE_ITEM__ALL_TABLES = "All tables";
    public static final String ENGINE_ITEM__EMPTY        = "";
    public static final String ENGINE_ITEM__ALL_ENGINES  = "All engines";
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
    @FXML
    private ComboBox<String> databaseCombobox;
    @FXML
    private ComboBox<String> engineCombobox; 
    @FXML
    private Button exportButton;
    
    @FXML
    private ToggleButton gridButton;
    @FXML
    private ToggleButton piechartButton;
    @FXML
    private ToggleButton barchartButton;
    
    private TableColumn databaseColumn;
    private TableColumn databaseSizeColumn;
    private TableColumn databaseSizePercentColumn;
    private TableColumn dataSizeColumn;
    private TableColumn dataSizePercentColumn;
    private TableColumn indexSizeColumn;
    private TableColumn indexSizePercentColumn;
    private TableColumn fragmentationSizeColumn;
    private TableColumn fragmentationSizePercentColumn;
    private TableColumn engineColumn;
    
    @FXML
    private HBox filterBox;
    
    @FXML
    private PieChart piechart;
    @FXML
    private StackedBarChart<String, Number> barchart;
    @FXML
    private CategoryAxis categoryAxis;
    @FXML
    private NumberAxis numberAxis;
        
    private ConnectionSession session;
    
    public boolean disableListen = true;
    
    private Double totalSize = 0.0;
    private Double totalDataSize = 0.0;
    private Double totalIndexSize = 0.0;
    private Double totalFragmentationSize = 0.0;
    
    public boolean isDatabaseMode() {
        return DATABASE_ITEM__EMPTY.equals(databaseCombobox.getSelectionModel().getSelectedItem());
    }
    
    public boolean isTableMode() {
        return DATABASE_ITEM__ALL_TABLES.equals(databaseCombobox.getSelectionModel().getSelectedItem());
    }
    
    public boolean isEngineMode() {
        return !ENGINE_ITEM__EMPTY.equals(engineCombobox.getSelectionModel().getSelectedItem());
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            byte[] pieChartImage;
            try {
                WritableImage image = piechart.snapshot(new SnapshotParameters(), null);
                
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

                pieChartImage = bos.toByteArray();
            } catch (Throwable e) {
                log.error("Error", e);
                pieChartImage = null;                
            }
            
            byte[] barChartImage;
            try {
                WritableImage image = barchart.snapshot(new SnapshotParameters(), null);
                
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

                barChartImage = bos.toByteArray();
            } catch (Throwable e) {
                log.error("Error", e);
                barChartImage = null;                
            }
            
            byte[] barChartImg = barChartImage;
            byte[] pieChartImg = pieChartImage;
            
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void run() {

                    List<String> sheets = new ArrayList<>();
                    List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                    List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                    List<List<Integer>> totalColumnWidths = new ArrayList<>();
                    List<byte[]> images = new ArrayList<>();
                    List<Integer> heights = new ArrayList<>();
                    List<Integer> colspans = new ArrayList<>();

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (ReportDataSize rds : (List<ReportDataSize>) (List) tableView.getCopyItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            if (isEngineMode()) {
                                row.add(rds.getEngine());
                            } else {
                                row.add(rds.getDatabase());
                            }

                            row.add(rds.getDatabaseSize());
                            row.add(rds.getDatabaseSizePercent());
                            row.add(rds.getDataSize());
                            row.add(rds.getDataSizePercent());
                            row.add(rds.getIndexSize());
                            row.add(rds.getIndexSizePercent());
                            row.add(rds.getFragmentationSize());
                            row.add(rds.getFragmentationSizePercent());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        if (isEngineMode()) {
                            columns.put("Engine", true);
                            columnWidths.add((int) engineColumn.getWidth());
                        } else {
                            columns.put("Name", true);
                            columnWidths.add((int) databaseColumn.getWidth());
                        }

                        columns.put("Total Size(MB)", true);
                        columnWidths.add((int) databaseSizeColumn.getWidth());

                        columns.put("(%)", true);
                        columnWidths.add((int) databaseSizePercentColumn.getWidth());

                        columns.put("Data Size(MB)", true);
                        columnWidths.add((int) dataSizeColumn.getWidth());

                        columns.put(" (%)", true);
                        columnWidths.add((int) dataSizePercentColumn.getWidth());

                        columns.put("Index Size(MB)", true);
                        columnWidths.add((int) indexSizeColumn.getWidth());

                        columns.put("  (%)", true);
                        columnWidths.add((int) indexSizePercentColumn.getWidth());

                        columns.put("Fragmentation size(MB)", true);
                        columnWidths.add((int) fragmentationSizeColumn.getWidth());

                        columns.put("    (%)", true);
                        columnWidths.add((int) fragmentationSizePercentColumn.getWidth());

                        sheets.add("Data size report");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(null);
                        heights.add(null);
                        colspans.add(null);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        sheets.add("Bar chart");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);

                        images.add(barChartImg);
                        heights.add(30);
                        colspans.add(14);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        sheets.add("Pie chart");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);

                        images.add(pieChartImg);
                        heights.add(30);
                        colspans.add(14);
                    }

                    ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                        @Override
                        public void error(String errorMessage, Throwable th) {
                            Platform.runLater(() -> DialogHelper.showError(getController(), "Error", errorMessage, th, getStage()));
                        }

                        @Override
                        public void success() {
                            Platform.runLater(() -> DialogHelper.showInfo(getController(), "Success", "Data export seccessfull", null, getStage()));
                        }
                    });
                }
            });
        }
    }
    
    
    @FXML
    public void refreshAction(ActionEvent event) {
        totalSize = 0.0;
        totalDataSize = 0.0;
        totalIndexSize = 0.0;
        totalFragmentationSize = 0.0;
        
        if (session != null) {
            
            String query;
            
            if (isEngineMode()) {
                String database = databaseCombobox.getSelectionModel().getSelectedItem();
                if (database != null && !DATABASE_ITEM__EMPTY.equals(database) && !DATABASE_ITEM__ALL_TABLES.equals(database)) {
                    query = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_DATABASE_SIZE_4), database);
                } else {
                    query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_DATABASE_SIZE_3);
                } 
                
                tableView.getTable().getColumns().remove(databaseColumn);
                if (!tableView.getTable().getColumns().contains(engineColumn)) {
                    tableView.addTableColumn(1, engineColumn, 0.09);
                }
                
            } else {
                tableView.getTable().getColumns().remove(engineColumn);
                if (!tableView.getTable().getColumns().contains(databaseColumn)) {
                    tableView.addTableColumn(1, databaseColumn, 0.39);
                }
                
                if (isDatabaseMode()) {
                    query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_DATABASE_SIZE_0);
                } else {
                    if (isTableMode()) {
                        query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_DATABASE_SIZE_1);
                    } else {
                        query = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_DATABASE_SIZE_2), databaseCombobox.getSelectionModel().getSelectedItem());
                    }
                }
            }
            
            boolean databaseMode = isDatabaseMode();
            
            int maxPieLength = 10;
            piechart.getData().clear();        
            
            
            barchart.getData().clear();
            
            XYChart.Series seriesDataSize = new XYChart.Series();
            seriesDataSize.setName("Data size");

            XYChart.Series seriesIndexSize = new XYChart.Series();
            seriesIndexSize.setName("Index size");

            XYChart.Series seriesFragmentationSize = new XYChart.Series();
            seriesFragmentationSize.setName("Fragmentation size");
            
            double max = 0;
            try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
                if (rs != null) {                    
                    List<ReportDataSize> list = new ArrayList<>();
                    int i = 0;
                    
                    boolean hasOthers = false;
                    double otherTotalSize = 0.0;
                    double otherDataSize = 0.0;
                    double otherIndexSize = 0.0;
                    double otherFragmentSize = 0.0;
                                        
                    while (rs.next()) {
                        ReportDataSize rds = new ReportDataSize(
                                isEngineMode() ? "" : rs.getString(1), 
                                new BigDecimal(rs.getDouble(2)).setScale(2, RoundingMode.HALF_UP), 
                                new BigDecimal(rs.getDouble(3)).setScale(2, RoundingMode.HALF_UP),
                                new BigDecimal(rs.getDouble(4)).setScale(2, RoundingMode.HALF_UP),
                                new BigDecimal(rs.getDouble(5)).setScale(2, RoundingMode.HALF_UP),
                                isEngineMode() ? rs.getString(1) : isDatabaseMode() ? "" : rs.getString(6)
                        );
                                                
                        boolean wasAdded = false;
                        
                        String engine = engineCombobox.getSelectionModel().getSelectedItem();
                        if (engine != null && !ENGINE_ITEM__ALL_ENGINES.equals(engine)&& !ENGINE_ITEM__EMPTY.equals(engine)) {
                            if (engine.equals(rds.getEngine())) {
                                list.add(rds);
                                wasAdded = true;
                            }
                        } else {
                            list.add(rds);
                            wasAdded = true;
                        }
                        
                        if (wasAdded) {
                            if (i < maxPieLength) {

                                EventHandler<MouseEvent> handler = (MouseEvent event1) -> {
                                    databaseCombobox.getSelectionModel().select(rds.getDatabase());
                                };

                                PieChart.Data d = new PieChart.Data(isEngineMode() ? rds.getEngine() : rds.getDatabase(), rds.getDatabaseSize().doubleValue());

                                BigDecimal dataSize = rds.getDataSize();
                                BigDecimal indexSize = rds.getIndexSize();
                                BigDecimal fragmentationSize = rds.getFragmentationSize();
                                
                                String text = "";
                                if (isEngineMode()) {
                                    text = "Engine: " + d.getName() + "\n";
                                } else {
                                    text = (databaseMode ? "Database: " : "Table: ") + d.getName() + "\n";
                                }
                                text += "--------------------\n" +
                                        "Data size = " + dataSize.toString() + "MB\n" + 
                                        "Index size = " + indexSize.toString() + "MB\n" + 
                                        "Fragmentation size = " + fragmentationSize.toString() + "MB";

                                Tooltip tooltip = new Tooltip();
                                tooltip.setText(text);

                                d.nodeProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                                    if (newValue instanceof Node) {
                                        Tooltip.install((Node) newValue, tooltip);

                                        if (databaseMode) {
                                            ((Node)newValue).setOnMouseClicked(handler);
                                        }
                                    }
                                });
                                piechart.getData().add(d);

                                i++;

                                max = max < rds.getDatabaseSize().doubleValue() ? rds.getDatabaseSize().doubleValue() : max;

                                String text0 = "Data size = " + dataSize.toString() + "MB";
                                Tooltip tooltip0 = new Tooltip();
                                tooltip0.setText(text0);
                                XYChart.Data d0 = new XYChart.Data(i + ". " + d.getName(), dataSize);
                                d0.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                    if (newValue instanceof Node) {
                                        Tooltip.install((Node) newValue, tooltip0);

                                        if (databaseMode) {
                                            ((Node)newValue).setOnMouseClicked(handler);
                                        }
                                    }
                                });
                                seriesDataSize.getData().add(d0);

                                String text1 = "Index size = " + indexSize.toString() + "MB";
                                Tooltip tooltip1 = new Tooltip();
                                tooltip1.setText(text1);
                                XYChart.Data d1 = new XYChart.Data(i + ". " + d.getName(), indexSize);
                                d1.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                    if (newValue instanceof Node) {
                                        Tooltip.install((Node) newValue, tooltip1);

                                        if (databaseMode) {
                                            ((Node)newValue).setOnMouseClicked(handler);
                                        }
                                    }
                                });
                                seriesIndexSize.getData().add(d1);

                                String text2 = "Fragmentation size = " + fragmentationSize.toString() + "MB";
                                Tooltip tooltip2 = new Tooltip();
                                tooltip2.setText(text2);                                
                                XYChart.Data d2 = new XYChart.Data(i + ". " + d.getName(), fragmentationSize);
                                d2.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                    if (newValue instanceof Node) {
                                        Tooltip.install((Node) newValue, tooltip2);

                                        if (databaseMode) {
                                            ((Node)newValue).setOnMouseClicked(handler);
                                        }
                                    }
                                });
                                seriesFragmentationSize.getData().add(d2);
                            } else {
                                hasOthers = true;
                                otherTotalSize += rds.getDatabaseSize().doubleValue();
                                otherDataSize += rds.getDataSize().doubleValue();
                                otherIndexSize += rds.getIndexSize().doubleValue();
                                otherFragmentSize += rds.getFragmentationSize().doubleValue();
                            }


                            totalSize += rds.getDatabaseSize().doubleValue();
                            totalDataSize += rds.getDataSize().doubleValue();
                            totalIndexSize += rds.getIndexSize().doubleValue();
                            totalFragmentationSize += rds.getFragmentationSize().doubleValue();
                        }
                    }
                    
                    
                    for (ReportDataSize rds: list) {
                        rds.setDatabaseSizePercent(calcPercent(rds.getDatabaseSize().doubleValue(), "databaseSize") + "%");
                        rds.setDataSizePercent(calcPercent(rds.getDataSize().doubleValue(), "dataSize") + "%");                        
                        rds.setIndexSizePercent(calcPercent(rds.getIndexSize().doubleValue(), "indexSize") + "%");
                        rds.setFragmentationSizePercent(calcPercent(rds.getFragmentationSize().doubleValue(), "fragmentationSize") + "%");
                    }
                    
                    max = max < otherTotalSize ? otherTotalSize : max;
                    tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));

                    if (hasOthers) {
                        PieChart.Data other = new PieChart.Data("others", otherTotalSize);
                        
                        BigDecimal dataSize = new BigDecimal(otherDataSize).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal indexSize = new BigDecimal(otherIndexSize).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal fragmentationSize = new BigDecimal(otherFragmentSize).setScale(2, RoundingMode.HALF_UP);

                       String text = "";
                                if (isEngineMode()) {
                                    text = "Engine: others\n";
                                } else {
                                    text = (databaseMode ? "Database: " : "Table: ") + "others\n";
                                }
                                text += "--------------------\n" +
                                        "Data size = " + dataSize.toString() + "MB\n" + 
                                        "Index size = " + indexSize.toString() + "MB\n" + 
                                        "Fragmentation size = " + fragmentationSize.toString() + "MB";

                        Tooltip tooltip = new Tooltip();
                        tooltip.setText(text);
                        other.nodeProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip);
                            }
                        });
                        piechart.getData().add(other); 

                        
                        String text0 = "Data size = " + dataSize.toString() + "MB";
                        Tooltip tooltip0 = new Tooltip();
                        tooltip0.setText(text0);
                        XYChart.Data d0 = new XYChart.Data("others", dataSize);
                        d0.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip0);
                            }
                        });
                        seriesDataSize.getData().add(d0);                        

                        String text1 = "Index size = " + indexSize.toString() + "MB";
                        Tooltip tooltip1 = new Tooltip();
                        tooltip1.setText(text1);
                        XYChart.Data d1 = new XYChart.Data("others", indexSize);
                        d1.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip1);
                            }
                        });
                        seriesIndexSize.getData().add(d1);

                        String text2 = "Fragmentation size = " + fragmentationSize.toString() + "MB";
                        Tooltip tooltip2 = new Tooltip();
                        tooltip2.setText(text2);
                        XYChart.Data d2 = new XYChart.Data("others", fragmentationSize);
                        d2.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip2);
                            }
                        });
                        seriesFragmentationSize.getData().add(d2);
                    }
                }
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", ex.getMessage(), null);
            }
            
            numberAxis.setUpperBound(max + max / 10.0);
            barchart.getData().addAll(seriesDataSize, seriesIndexSize, seriesFragmentationSize);
        }
    }
        
    
    public void init(ConnectionSession session, String database, String engine) {
        this.session = session;
        
        disableListen = false;
        
        databaseCombobox.getItems().add(DATABASE_ITEM__EMPTY);
        databaseCombobox.getItems().add(DATABASE_ITEM__ALL_TABLES);
        databaseCombobox.getSelectionModel().select(database);
        
        engineCombobox.getItems().add(ENGINE_ITEM__EMPTY);
        engineCombobox.getItems().add(ENGINE_ITEM__ALL_ENGINES);        
        engineCombobox.getSelectionModel().select(engine);
        
        if (session != null && session.getConnectionParam() != null) {
            if (session.getConnectionParam().getDatabases() != null) {
                for (Database db: session.getConnectionParam().getDatabases()) {
                    databaseCombobox.getItems().add(db.getName());
                }
            }
            
            try {
                List<String> list = session.getService().getEngine();
                if (list != null) {
                    for (String e: list) {
                        engineCombobox.getItems().add(e);
                    }
                }                
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        
        disableListen = true;
        
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    private Callback<TableColumn<ReportDataSize, String>, TableCell<ReportDataSize, String>> percentCallback = new Callback<TableColumn<ReportDataSize, String>, TableCell<ReportDataSize, String>>() {
        @Override
        public TableCell<ReportDataSize, String> call(TableColumn<ReportDataSize, String> param) {
            return new TableCell<ReportDataSize, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item != null ? item : "");
                    setStyle("-fx-padding: 0 0 2 0");
                    setAlignment(Pos.CENTER_RIGHT);
                }

            };
        }
    };
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        tableView.setSearchEnabled(true);
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        piechart.managedProperty().bind(piechart.visibleProperty());
        tableView.managedProperty().bind(tableView.visibleProperty());
        
        numberAxis.setAutoRanging(false);
        numberAxis.upperBoundProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            log.error("UPPER: " + newValue);
        });
        
        barchart.setCategoryGap(50);
        
        gridButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                tableView.toFront();
            }
        });
        
        barchartButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                barchart.toFront();
            }
        });
        
        piechartButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                piechart.toFront();
            }
        });
        
        databaseColumn = createColumn("Name", "database");
        databaseColumn.setCellFactory(new Callback<TableColumn<ReportDataSize, String>, TableCell<ReportDataSize, String>>() {
            @Override
            public TableCell<ReportDataSize, String> call(TableColumn<ReportDataSize, String> param) {
                TableCell tc = new TableCell<ReportDataSize, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setText(null);
                        setGraphic(null);
                            
                        if (item != null && !item.isEmpty() && !empty) {
                                                        
                            if (isDatabaseMode()) {
                                Hyperlink link = new Hyperlink(item);
                                link.setFocusTraversable(false);
                                link.setOnAction((ActionEvent event) -> {
                                    Platform.runLater(() -> {
                                        databaseCombobox.getSelectionModel().select(item);
                                    });                                
                                });

                                setGraphic(link);
                            } else {
                                
                                setText(item);
                                setTextFill(Color.BLACK);
                                
                                List list = tableView.getCopyItems();
                                int row = getTableRow().getIndex();
                                if (row > -1 && list.size() > row) {

                                    ReportDataSize rds = (ReportDataSize) list.get(row);
                                    if (rds.getDataSize().compareTo(rds.getIndexSize()) < 0 || rds.getDataSize().compareTo(rds.getFragmentationSize()) < 0) {
                                        if (rds.getDataSize().add(rds.getIndexSize()).add(rds.getFragmentationSize()).compareTo(new BigDecimal(50 * 1024)) > 0) {
                                            setTextFill(Color.RED);
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
                return tc;
            }
        });
        
        databaseSizeColumn = createColumn("Total Size(MB)", "databaseSize");  
        databaseSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataSize, BigDecimal>, TableCell<ReportDataSize, BigDecimal>>() {
            @Override
            public TableCell<ReportDataSize, BigDecimal> call(TableColumn<ReportDataSize, BigDecimal> param) {
                return new NumberTableCell("databaseSize");
            }
        });
        
        databaseSizePercentColumn = createColumn("(%)", "databaseSizePercent");
        databaseSizePercentColumn.setCellFactory(percentCallback);
        
        dataSizeColumn = createColumn("Data Size(MB)", "dataSize");
        dataSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataSize, BigDecimal>, TableCell<ReportDataSize, BigDecimal>>() {
            @Override
            public TableCell<ReportDataSize, BigDecimal> call(TableColumn<ReportDataSize, BigDecimal> param) {
                return new NumberTableCell("dataSize");
            }
        });
        
        dataSizePercentColumn = createColumn("(%)", "dataSizePercent");        
        dataSizePercentColumn.setCellFactory(percentCallback);
        
        indexSizeColumn = createColumn("Index Size(MB)", "indexSize");        
        indexSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataSize, BigDecimal>, TableCell<ReportDataSize, BigDecimal>>() {
            @Override
            public TableCell<ReportDataSize, BigDecimal> call(TableColumn<ReportDataSize, BigDecimal> param) {
                return new NumberTableCell("indexSize");
            }
        });
        
        indexSizePercentColumn = createColumn("(%)", "indexSizePercent");
        indexSizePercentColumn.setCellFactory(percentCallback);
        
        fragmentationSizeColumn = createColumn("Fragmentation size(MB)", "fragmentationSize");
        fragmentationSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataSize, BigDecimal>, TableCell<ReportDataSize, BigDecimal>>() {
            @Override
            public TableCell<ReportDataSize, BigDecimal> call(TableColumn<ReportDataSize, BigDecimal> param) {
                return new NumberTableCell("fragmentationSize");
            }
        });
        
        fragmentationSizePercentColumn = createColumn("(%)", "fragmentationSizePercent");
        fragmentationSizePercentColumn.setCellFactory(percentCallback);
        
        engineColumn = createColumn("Engine", "engine");
        engineColumn.setCellFactory(new Callback<TableColumn<ReportDataSize, String>, TableCell<ReportDataSize, String>>() {
            @Override
            public TableCell<ReportDataSize, String> call(TableColumn<ReportDataSize, String> param) {
                return new TableCell<ReportDataSize, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setText(null);
                        setGraphic(null);
                            
                        if (item != null && !item.isEmpty() && !empty) {
                            
                            Hyperlink link = new Hyperlink(item);
                            link.setFocusTraversable(false);
                            link.setOnAction((ActionEvent event) -> {
                                Platform.runLater(() -> {
                                    engineCombobox.getSelectionModel().select(item);
                                });                                
                            });

                            setGraphic(link);
                        }
                    }
                };
            }
        });
        
        tableView.addTableColumn(databaseColumn, 0.39);
        tableView.addTableColumn(databaseSizeColumn, 0.07);
        tableView.addTableColumn(databaseSizePercentColumn, 38, 38, false);
        tableView.addTableColumn(dataSizeColumn, 0.07);
        tableView.addTableColumn(dataSizePercentColumn, 38, 38, false);
        tableView.addTableColumn(indexSizeColumn, 0.08);
        tableView.addTableColumn(indexSizePercentColumn, 38, 38, false);
        tableView.addTableColumn(fragmentationSizeColumn, 0.12);
        tableView.addTableColumn(fragmentationSizePercentColumn, 38, 38, false);
//        tableView.addTableColumn(engineColumn, 0.09);       
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        databaseCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            boolean databaseMode = isDatabaseMode();
            engineCombobox.setDisable(databaseMode);
            if (databaseMode) {
                engineCombobox.getSelectionModel().select(0);
            }
            
            if (disableListen) {
                refreshAction(null);
            }
        });
        
        engineCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!isDatabaseMode() && disableListen) {
                refreshAction(null);
            }
        });
        
    }
        
    private class NumberTableCell extends TableCell<ReportDataSize, BigDecimal> {    
        
        private final String property;      
        
        public NumberTableCell(String property) {
            this.property = property;
        }
        
        @Override
        protected void updateItem(BigDecimal item, boolean empty) {
            super.updateItem(item, empty); 
            
            setStyle("-fx-padding: 0 0 2 0");
            setText("");
            
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            setAlignment(Pos.CENTER_RIGHT);
            if (!empty) {
                if (item != null) {
                                        
                    setText(item.toString());                    
                    if (item.compareTo(BigDecimal.ZERO) > 0) {
                        
                        List list = tableView.getCopyItems();
                        int row = getTableRow().getIndex();
                        if (row > -1 && list.size() > row) {
                            
                            Label l = new Label(item.toString());
                            l.setAlignment(Pos.CENTER_RIGHT);
                            l.setStyle("-fx-padding: 2 0 0 0; -fx-background-insets: 4 0 0 0; "
                                    + "-fx-background-color: linear-gradient(from 0% 100% to 100% 100%, " 
                                        + calcColor((ReportDataSize) list.get(row), this, property) + " 0%, transparent " 
                                        + calcPercent(item.doubleValue(), property) + "%)");
                            l.setMaxWidth(Double.MAX_VALUE);
                            
                            setGraphic(l);
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        }
                    }
                }
            }
        }
    }
    
    private BigDecimal calcPercent(Double value, String property) {
        
        switch (property) {
            case "databaseSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            case "dataSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            case "indexSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            case "fragmentationSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            default:
                return BigDecimal.ZERO.setScale(0, RoundingMode.HALF_UP);
        }
    }
    
    private String calcColor(ReportDataSize rds, NumberTableCell cell, String property) {
        
        switch (property) {
            case "databaseSize":
                String color = "yellow";
                double total = rds.getDatabaseSize().doubleValue() + rds.getFragmentationSize().doubleValue();
                if (total > 50 * 1024) {
                    color = "yellow";
                } else if (total > 100 * 1024) {
                    color = "blue";
                } else if (total > 200 * 1024) {
                    color = "#6F4E37";
                } else if (total > 300 * 1024) {
                    color = "red";
                }
                
                return color;
                
            case "dataSize":
                if (rds.getFragmentationSize().doubleValue() < 1.0 && rds.getFragmentationSize().doubleValue() < rds.getDataSize().doubleValue()) {
                    return "green";
                } else {
                    return "yellow";
                }
                
            case "indexSize":
                if (rds.getDataSize().doubleValue() < rds.getIndexSize().doubleValue()) {
                    cell.setTooltip(new Tooltip("It looks the " + (isDatabaseMode() ? "database" : "table") + " has too many indexes."));
                    return "red";
                } else {
                    cell.setTooltip(null);
                    return "yellow";
                }
                
            case "fragmentationSize":
                if (rds.getDataSize().doubleValue() < rds.getFragmentationSize().doubleValue()) {
                    cell.setTooltip(new Tooltip("The " + (isDatabaseMode() ? "database" : "table") + " has data fragmentation."));
                    return "red";
                } else {
                    cell.setTooltip(null);
                    return "yellow";
                }
                
            default:
                return "yellow";
        }
    }
}
