package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerLayer implements DesignerData, InvalidationListener {

    private DesignerErrDiagram parent;
    
    private final SimpleStringProperty uuid;
    
    private final SimpleStringProperty name;
    private final SimpleStringProperty description;
    
    private final SimpleStringProperty color;
    
    private final SimpleDoubleProperty x;
    private final SimpleDoubleProperty y;
    private final SimpleDoubleProperty width;
    private final SimpleDoubleProperty height;
    
    private final WeakInvalidationListener weakListner = new WeakInvalidationListener(this);
    
    public DesignerLayer() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty("");
        name.addListener(weakListner);
        
        description = new SimpleStringProperty("");
        description.addListener(weakListner);
        
        color = new SimpleStringProperty("#f0f1fe");
        color.addListener(weakListner);
        
        x = new SimpleDoubleProperty();
        x.addListener(weakListner);
        
        y = new SimpleDoubleProperty();
        y.addListener(weakListner);
        
        width = new SimpleDoubleProperty();
        width.addListener(weakListner);
        
        height = new SimpleDoubleProperty();
        height.addListener(weakListner);
    }
    
    
    @Override
    public void invalidated(Observable observable) {
        if (parent != null) {
            parent.invalidated(observable);
        }
    }
    
    @Override
    public void setParent(DesignerErrDiagram parent) {
        this.parent = parent;
    }
    
    @Override
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    @Override
    public SimpleStringProperty name() {
        return name;
    }
    
    public SimpleStringProperty description() {
        return description;
    }
    
    public SimpleStringProperty color() {
        return color;
    }
    
    public SimpleDoubleProperty x() {
        return x;
    }
    
    public SimpleDoubleProperty y() {
        return y;
    }
    
    public SimpleDoubleProperty width() {
        return width;
    }
    
    public SimpleDoubleProperty height() {
        return height;
    }
    
    
    @Override
    public String toString() {
        return name.get();
    }
    
    
    @Override
    public DesignerData copyData() {
        
        DesignerLayer l = new DesignerLayer();
        l.color.set(color.get());
        l.description.set(description.get());
        l.height.set(height.get());
        l.name.set(name.get());
        l.width.set(width.get());
        l.x.set(x.get());
        l.y.set(y.get());
        
        return l;
    }

    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        map.put("description", description.get());
        
        map.put("color", color.get());
                
        map.put("x", x.get());
        map.put("y", y.get());
        map.put("width", width.get());
        map.put("height", height.get());
                
        return map;
    }
    
    
    public void fromMap(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set(getNotNullStringValue(map, "name"));
            description.set(getNotNullStringValue(map, "description"));
            
            color.set(getNotNullStringValue(map, "color"));
            
            Double x0 = (Double)map.get("x");
            x.set(x0 != null ? x0 : -1);
            
            Double y0 = (Double)map.get("y");
            y.set(y0 != null ? y0 : -1);
            
            Double width0 = (Double)map.get("width");
            width.set(width0 != null ? width0 : -1);
            
            Double height0 = (Double)map.get("height");
            height.set(height0 != null ? height0 : -1);
        }
    }

    public boolean inside(double x0, double y0) {
        return x0 > x.get() && x0 < x.get() + width.get() && y0 > y.get() && y0 < y.get() + height.get();
    }
    
    @Override
    public List<DesignerProperty> getProperties() {
        
        List<DesignerProperty> list = new ArrayList<>();
        list.add(new DesignerProperty("Name", name, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Color", color, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("X", x, DesignerProperty.PropertyType.DOUBLE, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Y", y, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Height", height, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Width", width, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        
        return list;
    }
    
    private String getNotNullStringValue(Map<String, Object> map, String key) {
        Object v = map.get(key);
        return v != null ? v.toString() : "";
    }
}
