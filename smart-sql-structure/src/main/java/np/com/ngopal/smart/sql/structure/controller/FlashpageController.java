/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class FlashpageController extends BaseController implements Initializable  {

    @FXML
    private Label smartLabel;
    
    @FXML
    private AnchorPane mainUI;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainUI.boundsInLocalProperty().addListener(new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> observable,
                    Bounds oldValue, Bounds newValue) {
                if (newValue != null) {
                    Platform.runLater(() -> {
                        double width = newValue.getWidth();
                        double height = newValue.getHeight();

                        width = (width - smartLabel.prefWidth(-1) / 2) / 2;
                        height = (height - smartLabel.prefHeight(-1) / 2) / 2;

                        AnchorPane.setTopAnchor(smartLabel, height);
                        AnchorPane.setLeftAnchor(smartLabel, width);
                    });
                }
            }
        });
    }
    
    public void setFlashTitle(String text){
        smartLabel.setText(text);
    }
    
}
