package np.com.ngopal.smart.sql.structure.factories;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.models.GraphData;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.Flags;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class GraphQETITableCell extends TableCell<GraphData, String> {       
    
    private static final String[] QETI_CATEGORIES = new String[] {"1 μs", "10 μs", "100 μs", "1 ms", "10 ms", "100 ms", "1 s", "10 s", "10+ s"};
    private static final Integer[] QETI_TIMES = new Integer[] {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000};
    
    private final Image[] BAR_IMAGES = new Image[] {
        StandardDefaultImageProvider.getInstance().getBar_0_image(),
        StandardDefaultImageProvider.getInstance().getBar_1_image(),
        StandardDefaultImageProvider.getInstance().getBar_2_image(),
        StandardDefaultImageProvider.getInstance().getBar_3_image(),
        StandardDefaultImageProvider.getInstance().getBar_4_image()
    };
    
    private final Random GRAPH_RANDOM = new Random();
   
    private final Stage parent;
    public GraphQETITableCell(Stage s) {
        this.parent = s;
    }
    
    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty); 

        if (!empty && item != null) {
            setAlignment(Pos.CENTER);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            Button b = new Button();
            b.setPadding(new Insets(1));
            b.setGraphic(new ImageView(BAR_IMAGES[GRAPH_RANDOM.nextInt(5)]));
            b.setOnAction((ActionEvent event) -> {
                String[] times = item.split(",");

                List<String> list = Arrays.asList(times);
                Collections.sort(list, (String o1, String o2) -> {
                    try {
                        return new BigDecimal(o1.trim()).compareTo(new BigDecimal(o2.trim()));
                    } catch (NumberFormatException ex) {
                        return 0;
                    }
                });

                Integer countFrom1to10 = 0;

                Map<Integer, Integer> map = new LinkedHashMap<>();
                if (times.length > 0) {

                    Integer lowerY = 0;
                    Integer greaterY = null;
                    // for testing
                    //    list = Arrays.asList(new String[]{"0.000001", "0.000001", "0.000001", "0.00001", "0.0001", "0.0001", "0.001", "0.001", "0.001", "0.01", "0.1", "1", "1", "1", "1", "5", "10", "10", "11", "11"});
                    for (String t: list) {
                        Integer d = new BigDecimal(t.trim()).multiply(new BigDecimal(1000000)).intValue();

                        if (d >= 1000000 && d <= 10000000) {
                            countFrom1to10++;
                        }

                        Integer currentValue = 0;
                        for (Integer i: QETI_TIMES) {
                            if (d >= i) {
                                currentValue++;
                            } else  {
                                break;
                            }
                        }

                        Integer count = map.get(currentValue);
                        if (count == null)
                        {
                            count = 0;
                        }

                        count++;
                        map.put(currentValue, count);


                        if (greaterY == null) {
                            greaterY = count;
                        } else if (greaterY < count) {
                            greaterY = count;
                        }
                    }

                    BorderPane box = new BorderPane();
                    box.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);

                    long stepY = (greaterY - lowerY) / 10;
                    if (stepY == 0) {
                        stepY = 1;
                    }

                    CategoryAxis xAxis = new CategoryAxis(FXCollections.observableArrayList(QETI_CATEGORIES));
                    NumberAxis yAxis = new NumberAxis(lowerY, greaterY, stepY);                        
                    xAxis.setLabel("Time");
                    yAxis.setLabel("Count");


                    //creating the chart
                    BarChart<String, Number> lineChart = new BarChart<>(xAxis, yAxis);
                    lineChart.getStyleClass().addAll(Flags.METRIC_INNERS_STYLE_CLASS);

                    //defining a series
                    XYChart.Series series = new XYChart.Series();

                    lineChart.setLegendVisible(false);

                    for (Integer key: map.keySet()) {
                        try {          
                            if (key == map.size() - 1) {
                                String x = QETI_CATEGORIES[key];
                                Integer y = countFrom1to10;
                                XYChart.Data data = getNewData(x, y);
                                series.getData().add(data);

                                Label l = new Label();
                                l.setPrefSize(10, 10);
                                l.setTooltip(new Tooltip(y + " : " + x));
                                data.setNode(l);

                                x = QETI_CATEGORIES[QETI_CATEGORIES.length - 1];
                                y = map.get(key);
                                data = getNewData(x, y);
                                series.getData().add(data);

                                l = new Label();
                                l.setPrefSize(10, 10);
                                l.setTooltip(new Tooltip(y + " : " + x));
                                data.setNode(l);
                            } else {
                                String x = QETI_CATEGORIES[key];
                                Integer y = map.get(key);
                                XYChart.Data data = getNewData(x, y);
                                series.getData().add(data);

                                Label l = new Label();
                                l.setPrefSize(10, 10);
                                l.setTooltip(new Tooltip(y + " : " + x));
                                data.setNode(l);
                            }                                

                        } catch (Throwable th) {
                            log.error("Graph QETI error", th);
                        }
                    }

                    lineChart.getData().add(series);

                    box.setCenter(lineChart);

                    Stage stage = new Stage(StageStyle.UTILITY);
                    stage.initOwner(parent);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setTitle("Query Execution Time Interval");
                    stage.setX(getScene().getWindow().getX() - 300 + getScene().getWindow().getWidth()/2);
                    stage.setY(getScene().getWindow().getY() + 25);

                    Scene scene = new Scene(box, 600, 700);
                    Label l = new Label("Query Execution Time Interval");
                    l.setStyle("-fx-font-size: 14; -fx-font-weight: bold; -fx-text-fill: white;");
                    l.setAlignment(Pos.CENTER);
                    l.setMaxWidth(Double.MAX_VALUE);
                    BorderPane.setMargin(l, new Insets(5, 0, 5, 0));
                    box.setTop(l);
                    
                    scene.getStylesheets().addAll(getScene().getStylesheets());
                    stage.setScene(scene);
                    stage.show();
                }
            });
            setGraphic(b);
        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            setText("");
        }
    }
    
    private Color[] standardColor = new Color[]{Color.ALICEBLUE, Color.BISQUE,
        Color.DARKGRAY,Color.CHOCOLATE};    
    int pos = -1;
    
    private XYChart.Data getNewData(String x, Integer y) {
        XYChart.Data data = new XYChart.Data(x, y);
        data.nodeProperty().addListener(new ChangeListener<Node>() {            
            @Override
            public void changed(ObservableValue<? extends Node> observable,
                    Node oldValue, Node newNode) {
               if (newNode != null) {
                    if(newNode.getUserData() == null){
                        pos++;
                        if(pos >= standardColor.length){ pos = 0;}
                        newNode.setUserData(standardColor[pos]);
                    }
                    String c = newNode.getUserData().toString().substring(2);
                    newNode.setStyle("-fx-bar-fill: " + c +";"
                            + "-fx-background-color: " + c+ ";");
                }
            }
        });
        return data;
    }
}