package np.com.ngopal.smart.sql.structure.provider;

import com.google.inject.Singleton;
import java.util.Map;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.QueryTemplateCoding;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Singleton
public class DefaultProfilerDataManager extends ProfilerDataManager {

    
    
    public DefaultProfilerDataManager() {        
    }
    
    @Override
    protected Double convertIfNan(Double v) {
        if (QueryProvider.DB__MYSQL.equals(getStorageType())) {
            return v != null && v.isNaN() ? 0 : v;
        } else {
            return v;
        }
    }
    
    @Override
    protected String makeQueryTemplate(String query) throws Exception {
        String template = QueryTemplateCoding.makeQueryTemplate(query).getTemplate();                        
        QueryFormat.parseQuery(template);        
        return template;
    }

    @Override
    protected Map<String, String> getTableNamesFromQuery(String query) {
        return QueryFormat.getTableNamesFromQuery(query);
    }

    private String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, queryId);
    }
    
    @Override
    protected String queryDropTable() {        
        return getQuery(QueryProvider.QUERY__DROP_TABLE);
    }

    @Override
    protected String querySelectGsTables() {
        return getQuery(QueryProvider.QUERY__SELECT_GS_TABLES);
    }

    @Override
    protected String queryRemoveFromGsTables() {
        return getQuery(QueryProvider.QUERY__REMOVE_FROM_GS_TABLES);
    }

    @Override
    protected String querySelectSdTables() {
        return getQuery(QueryProvider.QUERY__SELECT_SD_TABLES);
    }

    @Override
    protected String queryRemoveFromSdTables() {
        return getQuery(QueryProvider.QUERY__REMOVE_FROM_SD_TABLES);
    }

    @Override
    protected String queryFindSdTables() {
        return getQuery(QueryProvider.QUERY__FIND_SD_TABLES_BY_TABLENAME);
    }

    @Override
    protected String queryCreateSdTable() {
        return getQuery(QueryProvider.QUERY__CREATE_SD_TABLE);
    }

    @Override
    protected String queryInsertSdTables() {
        return getQuery(QueryProvider.QUERY__INSERT_SD_TABLES);
    }

    @Override
    protected String querySelectSsTables() {
        return getQuery(QueryProvider.QUERY__SELECT_SS_TABLES);
    }

    @Override
    protected String queryRemoveFromSsTables() {
        return getQuery(QueryProvider.QUERY__REMOVE_FROM_SS_TABLES);
    }

    @Override
    protected String querySelectEisTables() {
        return getQuery(QueryProvider.QUERY__SELECT_EIS_TABLES);
    }

    @Override
    protected String queryRemoveFromEisTables() {
        return getQuery(QueryProvider.QUERY__REMOVE_FROM_EIS_TABLES);
    }

    @Override
    protected String querySelectSqTables() {
        return getQuery(QueryProvider.QUERY__SELECT_SQ_TABLES);
    }

    @Override
    protected String queryRemoveFromSqTables() {
        return getQuery(QueryProvider.QUERY__REMOVE_SLOW_TABLE_NAME);
    }

    @Override
    protected String queryFindSqTables() {
        return getQuery(QueryProvider.QUERY__FIND_SQ_TABLES_BY_TABLE_NAME);
    }

    @Override
    protected String queryCreateSqTable() {
        return getQuery(QueryProvider.QUERY__CREATE_SQ_TABLES);
    }

    @Override
    protected String queryInsertSqTables() {
        return getQuery(QueryProvider.QUERY__INSERT_SQ_TABLES);
    }

    @Override
    protected String queryLoadPQueryById() {
        return getQuery(QueryProvider.QUERY__LOAD_PQUERY_QUERY_BY_ID);
    }

    @Override
    protected String queryDeleteGVByConnectionId() {
        return getQuery(QueryProvider.QUERY__DELETE_GV_BY_CONNECTION_ID);
    }

    @Override
    protected String queryRemoveGsStart() {
        return getQuery(QueryProvider.QUERY__REMOVE_GS_START);
    }

    @Override
    protected String queryRemoveGv() {
        return getQuery(QueryProvider.QUERY__REMOVE_GV);
    }

    @Override
    protected String queryFindGvTablesByTableName() {
        return getQuery(QueryProvider.QUERY__FIND_GV_TABLES_BY_TABLENAME);
    }

    @Override
    protected String queryCreateGvTable() {
        return getQuery(QueryProvider.QUERY__CREATE_GV_TABLE);
    }

    @Override
    protected String queryInsertGvTables() {
        return getQuery(QueryProvider.QUERY__INSERT_GV_TABLES);
    }

    @Override
    protected String queryFindGsStartTablesByTableName() {
        return getQuery(QueryProvider.QUERY__FIND_GS_START_TABLES_BY_TABLENAME);
    }

    @Override
    protected String queryCreateGsStartTable() {
        return getQuery(QueryProvider.QUERY__CREATE_GS_START_TABLE);
    }

    @Override
    protected String queryInsertGsStartTables() {
        return getQuery(QueryProvider.QUERY__INSERT_GS_START_TABLES);
    }

    @Override
    protected String queryFindGsTablesByTableName() {
        return getQuery(QueryProvider.QUERY__FIND_GS_TABLES_BY_TABLENAME);
    }

    @Override
    protected String queryCreateGsTable() {
        return getQuery(QueryProvider.QUERY__CREATE_GS_TABLE);
    }

    @Override
    protected String queryInsertGsTable() {
        return getQuery(QueryProvider.QUERY__INSERT_GS_TABLES);
    }

    @Override
    protected String queryUpdateGsStartTables() {
        return getQuery(QueryProvider.QUERY__UPDATE_GS_START_TABLES);
    }

    @Override
    protected String queryUpdateStartTimeGsStartTables() {
        return getQuery(QueryProvider.QUERY__UPDATE_START_TIME_GS_START_TABLES);
    }

    @Override
    protected String querySelectAll() {
        return getQuery(QueryProvider.QUERY__SELECT_ALL);
    }

    @Override
    protected String queryFindSsTablesByTableName() {
        return getQuery(QueryProvider.QUERY__FIND_SS_TABLES_BY_TABLENAME);
    }

    @Override
    protected String queryCreateSsTable() {
        return getQuery(QueryProvider.QUERY__CREATE_SS_TABLE);
    }

    @Override
    protected String queryInsertSsTable() {
        return getQuery(QueryProvider.QUERY__INSERT_SS_TABLES);
    }

    @Override
    protected String queryFindEisTablesByTableName() {
        return getQuery(QueryProvider.QUERY__FIND_EIS_TABLES_BY_TABLENAME);
    }

    @Override
    protected String queryCreateEisTable() {
        return getQuery(QueryProvider.QUERY__CREATE_EIS_TABLE);
    }

    @Override
    protected String queryInsertEisTable() {
        return getQuery(QueryProvider.QUERY__INSERT_EIS_TABLES);
    }

    @Override
    protected String queryInsertGs() {
        return getQuery(QueryProvider.QUERY__INSERT_GS);
    }

    @Override
    protected String queryInsertEis() {
        return getQuery(QueryProvider.QUERY__INSERT_EIS);
    }

    @Override
    protected String queryInsertSs() {
        return getQuery(QueryProvider.QUERY__INSERT_SS);
    }

    @Override
    protected String queryInsertGsStart() {
        return getQuery(QueryProvider.QUERY__INSERT_GS_START);
    }

    @Override
    protected String queryLoadPqDb() {
        return getQuery(QueryProvider.QUERY__LOAD_PQ_DB);
    }

    @Override
    protected String queryInsertPqDb() {
        return getQuery(QueryProvider.QUERY__INSERT_PQ_DB);
    }

    @Override
    protected String queryInsertPqTable() {
        return getQuery(QueryProvider.QUERY__INSERT_PQ_TABLE);
    }

    @Override
    protected String queryLoadPqTable() {
        return getQuery(QueryProvider.QUERY__LOAD_PQ_TABLE);
    }

    @Override
    protected String queryLoadPQueryIds() {
        return getQuery(QueryProvider.QUERY__LOAD_PQUERY_IDS);
    }

    @Override
    protected String queryLoadPQueryTemplateId() {
        return getQuery(QueryProvider.QUERY__LOAD_PQUERY_TEMPLATE_ID);
    }

    @Override
    protected String queryInsertPQueryTemplate() {
        return getQuery(QueryProvider.QUERY__INSERT_PQUERY_TEMPLATE);
    }

    @Override
    protected String queryInsertPQuery() {
        return getQuery(QueryProvider.QUERY__INSERT_PQUERY);
    }

    @Override
    protected String queryLoadTemplateTableRelation() {
        return getQuery(QueryProvider.QUERY__LOAD_TEMPLATE_TABLE_RELATION);
    }

    @Override
    protected String queryInsertTemplateTableRelation() {
        return getQuery(QueryProvider.QUERY__INSERT_TEMPLATE_TABLE_RELATION);
    }

    @Override
    protected String queryLoadPqUserHost() {
        return getQuery(QueryProvider.QUERY__LOAD_PQ_USER_HOST);
    }

    @Override
    protected String queryInsertPqUserHost() {
        return getQuery(QueryProvider.QUERY__INSERT_PQ_USER_HOST);
    }

    @Override
    protected String queryLoadPqState() {
        return getQuery(QueryProvider.QUERY__LOAD_PQ_STATE);
    }

    @Override
    protected String queryInsertPqState() {
        return getQuery(QueryProvider.QUERY__INSERT_PQ_STATE);
    }

    @Override
    protected String queryLoadPQueryStats() {
        return getQuery(QueryProvider.QUERY__LOAD_PQUERY_STATS);
    }

    @Override
    protected String queryInsertPQueryStats() {
        return getQuery(QueryProvider.QUERY__INSERT_PQUERY_STATS);
    }

    @Override
    protected String queryUpdatePQueryStats() {
        return getQuery(QueryProvider.QUERY__UPDATE_PQUERY_STATS);
    }

    @Override
    protected String queryLoadPqStateStat() {
        return getQuery(QueryProvider.QUERY__LOAD_PQ_STATE_STAT);
    }

    @Override
    protected String queryUpdatePqStateStat() {
        return getQuery(QueryProvider.QUERY__UPDATE_PQ_STATE_STAT);
    }

    @Override
    protected String queryInsertPqStateStat() {
        return getQuery(QueryProvider.QUERY__INSERT_PQ_STATE_STAT);
    }

    @Override
    protected String queryLoadDigestText(Integer connectionId, Integer schemaId, String digest) {
        return String.format("SELECT ID FROM DIGEST_TEXT WHERE CONNECTION_ID = %s AND %s AND DIGEST = '%s'",
                connectionId, schemaId == null ? "SCHEMA_ID IS NULL" : "SCHEMA_ID = " + schemaId, digest);
    }

    @Override
    protected String queryInsertDigestText() {
        return getQuery(QueryProvider.QUERY__INSERT_DIGEST_TEXT);
    }

    @Override
    protected String queryInsertDigestQuery() {
        return getQuery(QueryProvider.QUERY__INSERT_DIGEST_QUERY);
    }

    @Override
    protected String queryInsertSd() {
        return getQuery(QueryProvider.QUERY__INSERT_SD);
    }

    @Override
    protected String querySelectGs() {
        return getQuery(QueryProvider.QUERY__SELECT_GS);
    }

    @Override
    protected String querySelectEis() {
        return getQuery(QueryProvider.QUERY__SELECT_EIS);
    }

    @Override
    protected String querySelectSs() {
        return getQuery(QueryProvider.QUERY__SELECT_SS);
    }

    @Override
    protected String queryInsertGv() {
        return getQuery(QueryProvider.QUERY__INSERT_GV);
    }

    @Override
    protected String queryInsertSqTable() {
        return getQuery(QueryProvider.QUERY__INSERT_SQ_TABLE);
    }

}
