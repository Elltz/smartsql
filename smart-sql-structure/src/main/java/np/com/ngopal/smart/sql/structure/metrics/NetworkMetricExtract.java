/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class NetworkMetricExtract extends MetricExtract{
   
    public static final String LOCALHOST_INTERFACE = "lo";

    public NetworkMetricExtract(MetricTypes type, String allResponseToParse) {
        super(type, allResponseToParse);
    }

    public NetworkMetricExtract(MetricTypes type) {
        super(type);
    }

    @Override
    public void extract() {
        super.extract(); 
        
        /**
         * example of its output without grep below.
         * NB grep only removes the headers.
         * 
         * Inter-|   Receive                                                |  Transmit
           face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
                lo: 1447413   20168    0    0    0     0          0         0  1447413   20168    0    0    0     0       0          0
                ens33: 515932874  507011    0    0    0     0          0         0 41427865  171001    0    0    0     0       0          0
         */
        
        String[] lines = response.trim().split(System.lineSeparator());        
        for(String s : lines){
            
            int indexOfSemiColon = s.indexOf(':');
            String[] splittedValsFirst = s.substring(indexOfSemiColon + 1).split(" ");
            String[] splittedVals = new String[splittedValsFirst.length];
            StringBuilder sb = new StringBuilder();
            
            //remove the empty strings in the string array
            int posInSplittedVals = 0;
            for(String a : splittedValsFirst){
                if(a != null && !a.isEmpty()){
                    splittedVals[posInSplittedVals] = a;
                    posInSplittedVals++;
                }
            }
            
            /**
            * FINAL ARRANGEMENT IS AS FOLLOWS
            * 
            * INDEX        |   VALUE
            *  0           |   received bytes
            *  1           |   received packets
            *  8           |   sent bytes
            *  9           |   sent packets
            */
            
            for(int i = 0; i < posInSplittedVals + 1; i++){
                switch(i){
                    case 0:
                        sb.append(splittedVals[i]).append(",");
                        break;
                    case 1:
                        sb.append(splittedVals[i]).append(",");
                        break;
                    case 8:
                        sb.append(splittedVals[i]).append(",");
                        break;
                    case 9:
                        sb.append(splittedVals[i]).append(",");
                        break;
                }
            }
            data.put(s.substring(0,indexOfSemiColon).trim(), sb.toString());
            
        }
    }
    
    public String getLocalHostSentBytes(){
        return getValuesFromInterface(LOCALHOST_INTERFACE)[2];
    }
    public String getLocalHostSentPackets(){
        return getValuesFromInterface(LOCALHOST_INTERFACE)[2];
    }
    public String getLocalHostReceivedBytes(){
        return getValuesFromInterface(LOCALHOST_INTERFACE)[0];
    }
    public String getLocalHostPackets(){
        return getValuesFromInterface(LOCALHOST_INTERFACE)[1];
    }
    
    public String getNetworkInterfaceSentBytes(String network){
        return getValuesFromInterface(network)[2];
    }
    public String getNetworkInterfaceSentPackets(String network){
        return getValuesFromInterface(network)[3];
    }
    public String getNetworkInterfaceReceivedPackets(String network){
        return getValuesFromInterface(network)[1];
    }
    public String getNetworkInterfaceReceivedBytes(String network){
        return getValuesFromInterface(network)[0];
    }
    
     /**
         * 
         * INDEX        |   VALUE
         *  0           |   received bytes
         *  1           |   received packets
         *  2           |   sent bytes
         *  3           |   sent packets
         */
    private String[] getValuesFromInterface(String in){
        //we convert this one to lower case just to make sure
        return getRemoteVal(in.toLowerCase()).split(",");
    }
    
    public String[] getInterfaces(){
        return getKeys();
    }
    
}
