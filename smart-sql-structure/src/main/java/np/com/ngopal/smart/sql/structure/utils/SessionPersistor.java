package np.com.ngopal.smart.sql.structure.utils;
import java.util.Collection;
import java.util.Map;
import java.util.function.Consumer;
import np.com.ngopal.smart.sql.ConnectionSession;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */

public interface SessionPersistor {
        
        Map<?,?> getPersistingMap();
        
        void onRestore(Map<?,?> restoreImage, ConnectionSession session, Long connection, Consumer<ConnectionSession> parentCallback);
        
        default boolean hasMoreConnections(int current, Collection<String> ls){
            for(String s : ls){
                if(Integer.valueOf(s) > current){
                    return true;
                }
            }
            
            return false;
        }
        
}
