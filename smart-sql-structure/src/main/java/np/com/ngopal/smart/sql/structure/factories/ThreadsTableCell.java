package np.com.ngopal.smart.sql.structure.factories;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.scene.Group;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ThreadsTableCell<T> extends TableCell<T, BigDecimal> {
    
    @Override
    protected void updateItem(BigDecimal item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty && item != null) {
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            
            ProgressBar bar = new ProgressBar(item.doubleValue());
            bar.setFocusTraversable(false);
            if (item.compareTo(new BigDecimal("0.25")) < 0) {
                bar.setId("threadsProgressBar-green");
            } else if (item.compareTo(BigDecimal.ONE) < 0) {
                bar.setId("threadsProgressBar-blue");
            } else {
                bar.setId("threadsProgressBar-red");
            }                

            HBox box = new HBox(bar, new Group(new Label(item.setScale(3, RoundingMode.HALF_UP).toString())));
            box.setSpacing(3);
            
            bar.setMaxWidth(Double.MAX_VALUE);
            HBox.setHgrow(bar, Priority.ALWAYS);

            setGraphic(box);
        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }
}
