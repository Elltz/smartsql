
package np.com.ngopal.smart.sql.structure.visualexplain;

import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class VisualTable {
    
    public enum TableType {
        SYSTEM("Single row: system constant"),
        CONST("Single row: constant"),
        QUERY("query_block"),
        FULL("Full Table Scan"),
        RANGE("Index Range Scan"),
        FULLTEXT("Fulltext Index Search"),
        REF_OR_NULL("Key Lookup + Fetch NULL Values"),
        INDEX_MERGE("Index Merge"),
        INDEX("Full Index Scan"),
        NON_UNIQUE("Non-unique Key Lookup"),
        UNIQUE("Unique Key Lookup"),
        UNION("UNION"),
        UNIQUE_SUBQUERY("Unique Key Lookup into table of subquery"),
        INDEX_SUBQUERY("Non-Unique Key Lookup into table of subquery"),
        UNKNOWN("unknown");
        
        private final String name;
        
        TableType(String name) {
            this.name = name;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    
    public double x;
    public double y;
    public double width;
    public double height;
    public TableType type;
    public String name;
    public String key;
    public Long rowsNumber;
    public Double cost;
    public String extra;

    public Object prevComponent;

    public VisualGroupBy groupBy;
    public VisualOrderBy orderBy;
    
    public List<VisualTable> insideTables;
}
