/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class Recycler {

    /**
     * this contains re-usable things. Anything i want to persist and retrieve
     * later
     */
    private static ArrayList<Object> RecyclingStash = new ArrayList<>();
    private static Object locker = new Object();
    private static ArrayDeque<WorkProc> stacksArrayDeque = new ArrayDeque<>(); 
    private static final ArrayDeque<SynchorniseWorkProccessor> backgroundWorkingThread = new ArrayDeque<>(10);
    // TODO Not sure that this is good, but 1min (60000) its too long
    public static final long BACKGROUND_WAITING_TIME = 5000;
    
    
    public static SynchorniseWorkProccessor getMainSWP() {
        synchronized (backgroundWorkingThread) {
            for (SynchorniseWorkProccessor swp : backgroundWorkingThread) {
                if (swp.isMain) {
                    return swp;
                }
            }
        }
        throw new RuntimeException("No main SynchroniseWorkProccessor declared.");
    }
    
    public static SynchorniseWorkProccessor getLessBusySWP(){
        synchronized(backgroundWorkingThread){
             for(SynchorniseWorkProccessor swp : backgroundWorkingThread){
                 if(!swp.isMain && !swp.isCurrentlyBusy()){
                     return swp;
                 }
             }
        }
        return null;
    }
    
    public static void initSWP(SynchorniseWorkProccessor swp) {
        synchronized (backgroundWorkingThread) {
            if (!backgroundWorkingThread.contains(swp)) {
                backgroundWorkingThread.addLast(swp);
                swp.setName("SYNCHRONIZE_THREAD - " + String.valueOf(backgroundWorkingThread.size()));
            }
        }
    }

    public static boolean hasWorkingWorkProc() {
        synchronized (locker) {
            for (WorkProc wp : stacksArrayDeque) {
                if (!(wp instanceof ConstantWorkProcedures)) {
                    return true;
                }
            }
        }
        return false;
    }

    // ConcstantWorkProcs behave differently from normal workProcs
    
    public static WorkProc getWorkProc() {
        WorkProc consWP = null;
        synchronized (locker) {
            if (stacksArrayDeque != null) {
                consWP = stacksArrayDeque.poll();
            }
        }
        
//        System.out.println("getWorkProc - " + consWP);
        return consWP;
    }
    
    public static void lockThreadWithLocker() throws InterruptedException {
        synchronized (locker) {
            locker.wait(BACKGROUND_WAITING_TIME);
        }
    }
    
    public static void addWorkProc(WorkProc work) {
        synchronized (locker) {
            if (stacksArrayDeque != null) {
                stacksArrayDeque.add(work);
//                System.out.println("addWorkProc - " + work);
                if (!(work instanceof ConstantWorkProcedures)) {
                    locker.notify();
                }
            }
        }
    }

    static void killSWP(SynchorniseWorkProccessor aThis) {
        synchronized (backgroundWorkingThread) {
            backgroundWorkingThread.remove(aThis);
        }
    }
    
    public void addManyWorkProc(List<? extends WorkProc> work) {
        synchronized (locker) {
            stacksArrayDeque.addAll(work);
            locker.notifyAll();
        }
    }

    public static synchronized final <E> E getFromBin(Class<E> c) {
        for (Object rcs : RecyclingStash) {
            if (rcs.getClass().isAssignableFrom(c)) {
                RecyclingStash.remove(rcs);
                return (E) rcs;
            }
        }
        return null;
    }

    public static synchronized void addItem(Object o) {
        if (RecyclingStash != null) {
            RecyclingStash.add(o);
        }
    }

    public static void dispose() {
        ConstantWorkProcedures.cWorkloads.clear();
        synchronized (backgroundWorkingThread) {
            for (SynchorniseWorkProccessor swp : backgroundWorkingThread) {
                swp.destroy();//destory all sync threads.
            }
            backgroundWorkingThread.clear();
        }
        RecyclingStash.clear();
        RecyclingStash = null;
        synchronized (locker) {
            stacksArrayDeque.clear();
            stacksArrayDeque = null;
        }
    }

      /////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * This is to ensure that we do not get errors of this kind " Table
     * `actor_html.address` doesnt exist i am guessing the problem is actor_html
     * is the database and address is the table name.
     *
     * @param database
     * @param table
     */
    public static String verifyTableName(String database, String table) {
        System.out.println("database name is  :" + database);
        System.out.println("table name is  :" + table);

        short lenOfData = (short) database.length(), lenOfTable = (short) table.length();
        /**
         * why i am checking for length of the two is the scenario above has
         * both the database and the table name hence its greater. There might
         * be some situations where the table name is just greater than the
         * database name without the above effect.
         *
         * if its else the table name is legit
         */
        if (lenOfTable > lenOfData) {
            String subString = table.substring(0, lenOfData), toReturn = "";
            //we got 'em
            if (subString.equals(database)) {
                toReturn = table.substring(lenOfData);
            } else {
                //is does not equal it.
                int index = table.lastIndexOf(".");
                if (index != -1) {
                    toReturn = table.substring(index);
                } else {
                    toReturn = table;
                }
            }

            return toReturn;
        } else {
            return table;
        }
    }
}
