/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class InnodbRowOperationsData {
    private final SimpleStringProperty ins;
        private final SimpleStringProperty upd;
        private final SimpleStringProperty read;
        private final SimpleStringProperty del;
        private final SimpleStringProperty insSec;
        private final SimpleStringProperty updSec;
        private final SimpleStringProperty readSec;
        private final SimpleStringProperty delSec;
        
        public InnodbRowOperationsData() {
            this.ins = new SimpleStringProperty();
            this.upd = new SimpleStringProperty();
            this.read = new SimpleStringProperty();
            this.del = new SimpleStringProperty();
            this.insSec = new SimpleStringProperty();
            this.updSec = new SimpleStringProperty();
            this.readSec = new SimpleStringProperty();
            this.delSec = new SimpleStringProperty();
        }
        
        public SimpleStringProperty ins() {
            return ins;
        }
        
        public SimpleStringProperty upd() {
            return upd;
        }
        
        public SimpleStringProperty read() {
            return read;
        }
        
        public SimpleStringProperty del() {
            return del;
        }
        
        public SimpleStringProperty insSec() {
            return insSec;
        }
        
        public SimpleStringProperty updSec() {
            return updSec;
        }
        
        public SimpleStringProperty readSec() {
            return readSec;
        }
        
        public SimpleStringProperty delSec() {
            return delSec;
        }
}
