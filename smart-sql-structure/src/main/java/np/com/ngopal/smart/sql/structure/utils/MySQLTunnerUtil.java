/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.image.Image;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.structure.metrics.MemoryMetricExtract;
import np.com.ngopal.smart.sql.structure.metrics.MetricExtract;
import np.com.ngopal.smart.sql.structure.metrics.MetricListener;
import np.com.ngopal.smart.sql.structure.metrics.MetricTypes;
import np.com.ngopal.smart.sql.structure.metrics.RemoteMetricExtractor;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class MySQLTunnerUtil {
    
    public static Image getStatusImage(TunerResult.Status s) {
        switch (s) {
            case BAD:
                return StandardDefaultImageProvider.getInstance().getTuner_bad_image();
            case GOOD:
                return StandardDefaultImageProvider.getInstance().getTuner_good_image();
            case NEUTRAL:
                return StandardDefaultImageProvider.getInstance().getTuner_neutral_image();
        }
        return null;
    }

    public  final String TUNER__SELECT_ENGINES_5_5 = "SELECT ENGINE,SUPPORT FROM information_schema.ENGINES ORDER BY ENGINE ASC";
    public  final String TUNER__SELECT_ENGINES_5_1_5 = "SELECT ENGINE,SUPPORT FROM information_schema.ENGINES WHERE ENGINE NOT IN ('performance_schema','MyISAM','MERGE','MEMORY') ORDER BY ENGINE ASC";
    public  final String TUNER__SELECT_ENGINES_SUM_5_1_5 = "SELECT ENGINE,SUM(DATA_LENGTH+INDEX_LENGTH),COUNT(ENGINE),SUM(DATA_LENGTH),SUM(INDEX_LENGTH) FROM information_schema.TABLES WHERE TABLE_SCHEMA NOT IN ('information_schema', 'performance_schema', 'mysql') AND ENGINE IS NOT NULL GROUP BY ENGINE ORDER BY ENGINE ASC";
    public  final String TUNER__SELECT_FRAGMENTED_TABLES = "SELECT CONCAT(CONCAT(TABLE_SCHEMA, '.'), TABLE_NAME),DATA_FREE FROM information_schema.TABLES WHERE TABLE_SCHEMA NOT IN ('information_schema','performance_schema', 'mysql') AND DATA_LENGTH/1024/1024>100 AND DATA_FREE*100/(DATA_LENGTH+INDEX_LENGTH+DATA_FREE) > 10 AND NOT ENGINE='MEMORY' %s";
    public  final String TUNER__SHOW_DATABASES = "SHOW DATABASES";
    public  final String TUNER__SELECT_DATABASES_NAMES = "SELECT DISTINCT TABLE_SCHEMA FROM information_schema.TABLES WHERE TABLE_SCHEMA NOT IN ('mysql', 'performance_schema', 'information_schema', 'sys')";
    public  final String TUNER__SELECT_ANALYZE = "SELECT * FROM %s.%s PROCEDURE ANALYSE(100000)";
    public  final String TUNER__SHOW_TABLES = "SHOW TABLE STATUS FROM `%s`";
    public  final String TUNER__SELECT_MAX_INT = "SELECT ~0";
    public  final String TUNER__SELECT_DB_TOTAL_INFO = "SELECT SUM(TABLE_ROWS), SUM(DATA_LENGTH), SUM(INDEX_LENGTH), SUM(DATA_LENGTH+INDEX_LENGTH), COUNT(TABLE_NAME),COUNT(DISTINCT(TABLE_COLLATION)),COUNT(DISTINCT(ENGINE)) FROM information_schema.TABLES WHERE TABLE_SCHEMA NOT IN ( 'mysql', 'performance_schema', 'information_schema', 'sys' )";
    public  final String TUNER__SELECT_DBS_COLLATION = "SELECT DISTINCT(TABLE_COLLATION) FROM information_schema.TABLES";
    public  final String TUNER__SELECT_DB_COLLATION = "SELECT DISTINCT(TABLE_COLLATION) FROM information_schema.TABLES WHERE TABLE_SCHEMA='%s'";
    public  final String TUNER__SELECT_DISTINCT_ENGINES = "SELECT DISTINCT(ENGINE) FROM information_schema.TABLES";
    public  final String TUNER__SELECT_DISTINCT_ENGINE = "SELECT DISTINCT(ENGINE) FROM information_schema.TABLES WHERE TABLE_SCHEMA='%s'";
    public  final String TUNER__SELECT_DB_INFO = "SELECT TABLE_SCHEMA, SUM(TABLE_ROWS), SUM(DATA_LENGTH), SUM(INDEX_LENGTH) , SUM(DATA_LENGTH+INDEX_LENGTH), COUNT(DISTINCT ENGINE),COUNT(TABLE_NAME),COUNT(DISTINCT(TABLE_COLLATION)),COUNT(DISTINCT(ENGINE)) FROM information_schema.TABLES WHERE TABLE_SCHEMA='%s' GROUP BY TABLE_SCHEMA ORDER BY TABLE_SCHEMA";
    public  final String TUNER__SELECT_DISTINCT_COLUMN_CHARSET = "SELECT DISTINCT(CHARACTER_SET_NAME) FROM information_schema.COLUMNS WHERE CHARACTER_SET_NAME IS NOT NULL AND TABLE_SCHEMA ='%s'";
    public  final String TUNER__SELECT_DISTINCT_COLLATION = "SELECT DISTINCT(COLLATION_NAME) FROM information_schema.COLUMNS WHERE COLLATION_NAME IS NOT NULL AND TABLE_SCHEMA ='%s'";
    public  final String TUNER__SELECT_INDEX_REQ_0 = "SELECT CONCAT(CONCAT(t.TABLE_SCHEMA, '.'), t.TABLE_NAME) AS 'table', CONCAT(CONCAT(CONCAT(s.INDEX_NAME, '('),s.COLUMN_NAME), ')') AS 'index', s.SEQ_IN_INDEX AS 'seq', s2.max_columns AS 'maxcol', s.CARDINALITY  AS 'card', t.TABLE_ROWS   AS 'est_rows', INDEX_TYPE as type, ROUND(((s.CARDINALITY / IFNULL(t.TABLE_ROWS, 0.01)) * 100), 2) AS 'sel' FROM INFORMATION_SCHEMA.STATISTICS s INNER JOIN INFORMATION_SCHEMA.TABLES t ON s.TABLE_SCHEMA = t.TABLE_SCHEMA AND s.TABLE_NAME = t.TABLE_NAME INNER JOIN (SELECT TABLE_SCHEMA, TABLE_NAME, INDEX_NAME, MAX(SEQ_IN_INDEX) AS max_columns FROM INFORMATION_SCHEMA.STATISTICS WHERE TABLE_SCHEMA NOT IN ('mysql', 'information_schema', 'performance_schema') AND INDEX_TYPE <> 'FULLTEXT' GROUP BY TABLE_SCHEMA, TABLE_NAME, INDEX_NAME) AS s2 ON s.TABLE_SCHEMA = s2.TABLE_SCHEMA AND s.TABLE_NAME = s2.TABLE_NAME AND s.INDEX_NAME = s2.INDEX_NAME WHERE t.TABLE_SCHEMA NOT IN ('mysql', 'information_schema', 'performance_schema')\n AND t.TABLE_ROWS > 10 AND s.CARDINALITY IS NOT NULL AND (s.CARDINALITY / IFNULL(t.TABLE_ROWS, 0.01)) < 8.00\n ORDER BY sel LIMIT 10";
    public  final String TUNER__SELECT_INDEX_REQ_1 = "SELECT CONCAT(CONCAT(object_schema,'.'),object_name) AS 'table', index_name FROM performance_schema.table_io_waits_summary_by_index_usage WHERE index_name IS NOT NULL AND count_star =0 AND index_name <> 'PRIMARY' AND object_schema != 'mysql' ORDER BY count_star, object_schema, object_name";
    public  final String TUNER__SELECT_USERS = "SELECT CONCAT(user, '\\@', host) FROM mysql.user WHERE TRIM(USER) = '' OR USER IS NULL";
    public  final String TUNER__SELECT_USERS_0 = "SELECT CONCAT(user, '\\@', host) FROM mysql.user WHERE ($PASS_COLUMN_NAME = '' OR $PASS_COLUMN_NAME IS NULL) AND plugin NOT IN ('unix_socket', 'win_socket')";
    public  final String TUNER__SELECT_USERS_1 = "SELECT CONCAT(user, '\\@', host) FROM mysql.user WHERE ($PASS_COLUMN_NAME = '' OR $PASS_COLUMN_NAME IS NULL)";
    public  final String TUNER__SELECT_PLUGIN = "select count(*) from information_schema.plugins where PLUGIN_NAME='validate_password' AND PLUGIN_STATUS='ACTIVE'";
    public  final String TUNER__SELECT_USERS_2 = "SELECT CONCAT(user, '\\@', host) FROM mysql.user WHERE CAST($PASS_COLUMN_NAME as Binary) = PASSWORD(user) OR CAST($PASS_COLUMN_NAME as Binary) = PASSWORD(UPPER(user)) OR CAST($PASS_COLUMN_NAME as Binary) = PASSWORD(CONCAT(UPPER(LEFT(User, 1)), SUBSTRING(User, 2, LENGTH(User))))";
    public  final String TUNER__SELECT_USERS_3 = "SELECT CONCAT(user, '\\@', host) FROM mysql.user WHERE HOST='%'";
    public  final String TUNER__SELECT_USERS_4 = "SELECT CONCAT(user, '\\@', host) FROM mysql.user WHERE %s = PASSWORD('%s') OR $PASS_COLUMN_NAME = PASSWORD(UPPER('%s')) OR $PASS_COLUMN_NAME = PASSWORD(CONCAT(UPPER(LEFT('%s', 1)), SUBSTRING('%s', 2, LENGTH('%s'))))";
    public  final String TUNER__SELECT_PS_STATUS = "SHOW ENGINE PERFORMANCE_SCHEMA STATUS";
    
    public  final Pattern PATTERN_HR_RAW__G = Pattern.compile("^(\\d+)G$");
    public  final Pattern PATTERN_HR_RAW__M = Pattern.compile("^(\\d+)M$");
    public  final Pattern PATTERN_HR_RAW__K = Pattern.compile("^(\\d+)K$");
    public  final Pattern PATTERN_HR_RAW = Pattern.compile("^(\\d+)$");
    
    private ConnectionSession session;
    private  Long totalMemory = 0l;
    
    private Map<String, String> statusCache;
    private Map<String, String> variablesCache;
    private Map<String, Object> calculationCache = new HashMap<>();

    public MySQLTunnerUtil(ConnectionSession session) {
        this.session = session;
    }
    
    public Map<String, List<TunerResult>> runTuner() throws Exception {
        try {
            ((MysqlDBService) session.getService()).setServiceBusy(true);

            String address = session.getConnectionParam().getAddress();
            if ("localhost".equals(address) || "127.0.0.1".equals(address)) {
                totalMemory = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize();

            } else if (session.getConnectionParam().isRemote() && session.getConnectionParam().hasOSDetails()) {
                final RemoteMetricExtractor remoteMetricExtractor = new RemoteMetricExtractor(session.getConnectionParam(), MetricTypes.MEMORY_METRIC);
                remoteMetricExtractor.setMetricListener(new MetricListener() {
                    @Override
                    public void onReceiveRemoteMetrics(MetricTypes mt, MetricExtract extract) {
                        totalMemory = ((MemoryMetricExtract) extract).getTotalMemory();
                        remoteMetricExtractor.stop();
                    }

                    @Override
                    public void onDone() {}
                });
                remoteMetricExtractor.connectAndExecuteServer();
            } else {
                try {
                    totalMemory = new BigDecimal(session.getConnectionParam().getOsMetricRamSize()).multiply(new BigDecimal(1024 * 1024 * 1024)).longValue();
                } catch (Throwable th) {
                }
            }

            log.info("Tuner start time: " + new Date());
            Map<String, List<TunerResult>> map = new LinkedHashMap<>();

            Map<String, Object> result = new HashMap<>();
            List<String> generalrec = new ArrayList<>();
            List<String> adjvars = new ArrayList<>();
            
            initData(result);

            if (variablesCache == null) {
                variablesCache = new HashMap<>();
            }
            if (statusCache == null) {
                statusCache = new HashMap<>();
            }

            variablesCache.putAll(loadGlobalVariables(session.getService()));
            statusCache.putAll(loadGlobalStatus(session.getService()));

            // IGNORING
            // validate_tuner_version;    # Check last version
            // mysql_setup;               # Gotta login first
            // os_setup;                  # Set up some OS variables
            // get_tuning_info;           # Get information about the tuning connexion
            // validate_mysql_version;    # Check current MySQL version
            // check_architecture;        # Suggest 64-bit upgrade
            // TODO Not sure that need
            // system_recommendations  (it working only for linux)
            // log_file_recommandations 
            // cve_recommendations
            // get_replication_status 
            // Toss variables/status into hashes
            getAllVars(generalrec, adjvars, result, map);
            // Show enabled storage engines
            checkStorageEngines(generalrec, adjvars, result, map);
            // Calculate everything we need
            calculations(result, map);
            // Display some security recommendations
            securityRecommendations(generalrec, adjvars, result, map);
            // Print MyISAM stats
            mysqlMyisam(generalrec, adjvars, result, map);
            // Print InnoDB stats
            mysqlInnodb(generalrec, adjvars, result, map);
            // Print the server stats
            mysqlStats(generalrec, adjvars, result, map);
            // Print Performance schema info
            mysqlPfs(generalrec, adjvars, result, map);
            // Print replication info
            getReplicationStatus(generalrec, adjvars, result, map);
            // Make recommendations based
            makeRecommendations(generalrec, adjvars, result, map);

            map.put("Database Metrics", null);
            map.put("Table Column Metrics", null);
            map.put("Indexes Metrics", null);

//        mysqlDatabases(generalRecords, result, variables, status, map);        
//        mysqlTables(generalRecords, result, variables, status, map);        
//        mysqlIndexes(generalRecords, result, variables, status, map);
            log.info("Tuner end time: " + new Date());

            return map;
        } finally {
            ((MysqlDBService) session.getService()).setServiceBusy(false);
        }
    }
    
    private void initData(Map<String, Object> result) {
        
        String multiQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_VARIABLES);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }
        
        multiQuery += "SHOW GLOBAL VARIABLES;";
        
        multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_STATUS);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }  
        
        multiQuery += "SHOW GLOBAL STATUS;";
        
        multiQuery += TUNER__SHOW_DATABASES;
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }
        
        variablesCache = new HashMap<>();        
        statusCache = new HashMap<>();
        
        List<String> databases = new ArrayList<>();
        result.put("Databases", new HashMap<>());
        
        ((Map<String, Object>)result.get("Databases")).put("List", databases);
                
        
        try {
            QueryResult qr = new QueryResult();
            session.getService().execute(multiQuery, qr);
            
            ResultSet rs = (ResultSet) qr.getResult().get(0);            
            while (rs.next()) {        
                variablesCache.put(rs.getString(1), rs.getString(2));
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(1);            
            while (rs.next()) {        
                variablesCache.put(rs.getString(1), rs.getString(2));
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(2);            
            while (rs.next()) {        
                statusCache.put(rs.getString(1), rs.getString(2));
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(3);            
            while (rs.next()) {        
                statusCache.put(rs.getString(1), rs.getString(2));
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(4);      
            while (rs.next()) {
                databases.add(rs.getString(1));
            }
            rs.close();
            
        } catch (SQLException ex) { ex.printStackTrace(); }
                
    }
    
    public List<TunerResult> getRecommendations() {
        try {
            ((MysqlDBService) session.getService()).setServiceBusy(true);

            Map<String, List<TunerResult>> map = new LinkedHashMap<>();

            Map<String, Object> result = new HashMap<>();
            List<String> generalrec = new ArrayList<>();
            List<String> adjvars = new ArrayList<>();

            initData(result);

//        variablesCache.put(loadVariables(session));
//        statusCache.put(loadStatus(session));
            // Show enabled storage engines
            checkStorageEngines(generalrec, adjvars, result, map);

            // Calculate everything we need
            calculations(result, map);

            mysqlStats(generalrec, adjvars, result, map);

            List<TunerResult> recommendations = new ArrayList<>();
            List<TunerResult> list = map.get("Performance Metrics");
            if (list != null) {
                for (TunerResult tr : list) {
                    if ("Recommendations".equals(tr.getGroup()) && tr.getStatus() == TunerResult.Status.BAD) {
                        recommendations.add(tr);
                    }
                }
            }

            return recommendations;
        } finally {
            ((MysqlDBService) session.getService()).setServiceBusy(false);
        }
    }

    public  String getVariable(String variable) {
        return variablesCache.get(variable);
    }
    
    public  boolean containsVariable(String variable) {
        return variablesCache.containsKey(variable);
    }
    
    public  String putVariable(String variable, String value) {
        return variablesCache.put(variable, value);
    }
    
    public  String getStatus(String status) {
        return statusCache.get(status);
    }
    
    public  String putStatus(String status, String value) {
        return statusCache.put(status, value);
    }
    
    private  boolean containsStatus(String status) {
        return statusCache.containsKey(status);
    }
    
    public  Object getCalculation(String variable) {
        return calculationCache.get(variable);
    }
    
    public  boolean containsCalculation(String calculation) {        
        return calculationCache.containsKey(calculation);
    }
    
    
    // <editor-fold defaultstate="collapsed" desc=" get_all_vars (Toss variables/status into hashes) "> 
        
    private  void getAllVars(List<String> generalrec, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {

        // We need to initiate at least one query so that our data is useable
        String dummyselect = selectOne("SELECT VERSION()");
        if (dummyselect.contains("-")) {
            dummyselect = dummyselect.substring(0, dummyselect.indexOf("-"));
        }
        
        Map<String, Object> m = (Map<String, Object>)result.get("MySQL Client");
        if (m == null) {
            m = new HashMap<>();
            result.put("MySQL Client", m);
        }
        
        m.put("Version", dummyselect);

        result.put("Variables", variablesCache);
        result.put("Status", statusCache);

        putVariable("have_galera", "NO");
        if (containsVariable("wsrep_provider_options")
            && !"".equals(getVariable("wsrep_provider_options")))
        {
            putVariable("have_galera", "YES");
        }

        // Workaround for MySQL bug #59393 wrt. ignore-builtin-innodb
        if ("ON".equalsIgnoreCase(getVariable("ignore_builtin_innodb"))) {
            putVariable( "have_innodb", "NO");
        }

        // Support GTID MODE FOR MARIADB
        // Issue MariaDB GTID mode #272
        if (containsVariable( "gtid_strict_mode")) {
            putVariable( "gtid_mode", getVariable( "gtid_strict_mode"));
        }

        putVariable( "have_threadpool", "NO");
        if (containsVariable( "thread_pool_size")
            && getDoubleVariableValue("thread_pool_size") > 0)
        {
            putVariable("have_threadpool", "YES");
        }

        // have_* for engines is deprecated and will be removed in MySQL 5.6;
        // check SHOW ENGINES and set corresponding old style variables.
        // Also works around MySQL bug #59393 wrt. skip-innodb
        
        m = (Map<String, Object>)result.get("Storage Engines");
        if (m == null) {
            m = new HashMap<>();
            result.put("Storage Engines", m);
        }

        try (ResultSet rs = (ResultSet) session.getService().execute("SHOW ENGINES")) {
            if (rs != null) {
                while (rs.next()) {
                    String engine = rs.getString(1);
                    if ("federated".equalsIgnoreCase(engine) || "blackhole".equalsIgnoreCase(engine)) {
                        engine += "_engine";
                    } else if ("berkeleydb".equalsIgnoreCase(engine)) {
                        engine = "bdb";
                    }
                    
                    String value = rs.getString(2);
                    if ("DEFAULT".equalsIgnoreCase(value)) {
                        value = "YES";
                    }
                    
                    putVariable("have_" + engine, value);
                    

                    m.put("Version", dummyselect);
                    m.put(engine, rs.getString(2));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'selectOne'", ex);
        }
    }
   // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" check_storage_engines (Show enabled storage engines) ">  
    private  void checkStorageEngines(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
        
        
        String multiQuery = "";
        
        if (mysqlVersion(session, 5, 5)) {
            
            multiQuery += TUNER__SELECT_ENGINES_5_5;
            
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
        } else if (mysqlVersion(session, 5, 1, 5)) {
            
            multiQuery += TUNER__SELECT_ENGINES_5_1_5;
            
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
        }
        
        List<String> databases = (List<String>) ((Map<String, Object>) result.getOrDefault("Databases", new HashMap<>())).get("List");
        for (String db : databases) {
            if (db.equals("information_schema")
                    || db.equals("performance_schema")
                    || db.equals("mysql")
                    || db.equals("lost+found")) {
                continue;
            }

            multiQuery += String.format(TUNER__SHOW_TABLES, db);
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
        }

        multiQuery += TUNER__SELECT_MAX_INT;
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }
        
        if (mysqlVersion(session, 5, 1, 5)) {
            multiQuery += TUNER__SELECT_ENGINES_SUM_5_1_5;
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
            
            String not_innodb = "";
            if (getVariable("innodb_file_per_table") == null) {
                not_innodb = "AND NOT ENGINE='InnoDB'";
            }
            else if (getVariable("innodb_file_per_table").equals("OFF")) {
                not_innodb = "AND NOT ENGINE='InnoDB'";
            }
            
            multiQuery += String.format(TUNER__SELECT_FRAGMENTED_TABLES, not_innodb);
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
        }
        
        String message = "";
        
        try {
            QueryResult qr = new QueryResult();
            session.getService().execute(multiQuery, qr);
            
            ResultSet rs = null;
            
            int resultIndex = 0;
            if (mysqlVersion(session, 5, 5)) {
                rs = (ResultSet) qr.getResult().get(resultIndex);    
                resultIndex++;
                while (rs.next()) {        
                    String engine = rs.getString(1);
                        String engineEnabled = rs.getString(2);
                        
                        if (engineEnabled.equalsIgnoreCase("YES") || engineEnabled.equalsIgnoreCase("DEFAULT")) {
                            message += "+";
                        } else {
                            message += "-";
                        }
                        
                        message += engine + " ";
                }
                rs.close();
            } else if (mysqlVersion(session, 5, 1, 5)) {
                
                rs = (ResultSet) qr.getResult().get(resultIndex);            
                resultIndex++;
                while (rs.next()) {
                    String engine = rs.getString(1);
                    String engineEnabled = rs.getString(2);

                    if (engineEnabled.equalsIgnoreCase("YES") || engineEnabled.equalsIgnoreCase("DEFAULT")) {
                        message += "+";
                    } else {
                        message += "-";
                    }

                    message += engine + " ";
                }
            } else {
                if ("YES".equalsIgnoreCase(getVariable("have_archive"))) {
                    message += "+Archive ";
                } else {
                    message += "-Archive ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_bdb"))) {
                    message += "+BDB ";
                } else {
                    message += "-BDB ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_federated_engine"))) {
                    message += "+Federated ";
                } else {
                    message += "-Federated ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_innodb"))) {
                    message += "+InnoDB ";
                } else {
                    message += "-InnoDB ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_isam"))) {
                    message += "+ISAM ";
                } else {
                    message += "-ISAM ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_ndbcluster"))) {
                    message += "+NDBCluster ";
                } else {
                    message += "-NDBCluster ";
                }if ("YES".equalsIgnoreCase(getVariable("have_archive"))) {
                    message += "+Archive ";
                } else {
                    message += "-Archive ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_bdb"))) {
                    message += "+BDB ";
                } else {
                    message += "-BDB ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_federated_engine"))) {
                    message += "+Federated ";
                } else {
                    message += "-Federated ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_innodb"))) {
                    message += "+InnoDB ";
                } else {
                    message += "-InnoDB ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_isam"))) {
                    message += "+ISAM ";
                } else {
                    message += "-ISAM ";
                }

                if ("YES".equalsIgnoreCase(getVariable("have_ndbcluster"))) {
                    message += "+NDBCluster ";
                } else {
                    message += "-NDBCluster ";
                }
            }
            
            List<TunerResult> tunerResult = new ArrayList<>();
            tunerMap.put("Storage Engine Statistics", tunerResult);

            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, message));

            if (result.get("Engine") == null) {
                result.put("Engine", new HashMap<>());
            }
        
            List<Map<String, Object>> tables = new ArrayList<>();
            
            for (String db: databases) {
                if (db.equals("information_schema")
                    || db.equals("performance_schema")
                    || db.equals("mysql")
                    || db.equals("lost+found") ) {
                    continue;
                }
                
                rs = (ResultSet) qr.getResult().get(resultIndex);
                resultIndex++;
                while (rs.next()) {
                    Map<String, Object> m = new HashMap<>();
                    m.put("name", rs.getString("Name"));
                    m.put("engine", rs.getString("Engine"));
                    m.put("size", rs.getLong("Data_length"));
                    m.put("datafree", rs.getLong("Data_free"));
                    m.put("autoincrement", rs.getLong("Auto_increment"));
                    m.put("db", db);

                    tables.add(m);
                }
                rs.close();
            }
            
            rs = (ResultSet) qr.getResult().get(resultIndex);
            resultIndex++;
            if (rs.next()) {
                result.put("MaxInt", rs.getDouble(1));
            }
            rs.close();
            
            Map<String, Long> enginestats = new HashMap<>();
            Map<String, Long> enginecount = new HashMap<>();

            int fragtables = 0;
            if (mysqlVersion(session, 5, 1, 5)) {
                
                rs = (ResultSet) qr.getResult().get(resultIndex);
                resultIndex++;
                while (rs.next()) {
                    String engine = rs.getString(1);
                    Long size = rs.getLong(2);
                    Long count = rs.getLong(3);
                    Long dsize = rs.getLong(4);
                    Long isize = rs.getLong(5);

                    enginestats.put(engine, size);
                    enginecount.put(engine, count);

                    Map<String, Object> m = (Map<String, Object>) result.get("Engine");
                    Map<String, Object> me = (Map<String, Object>) m.get(engine);
                    if (me == null) {
                        me = new HashMap<>();
                        m.put(engine, me);
                    }
                    me.put("Table Number", count);
                    me.put("Total Size", size);
                    me.put("Data Size", dsize);
                    me.put("Index Size", isize);
                }
                rs.close();
                
                if (result.get("Tables") == null) {
                    result.put("Tables", new HashMap<>());
                }

                List<Object[]> fragmentedTables = new ArrayList<>();
                ((Map<String, Object>)result.get("Tables")).put("Fragmented tables", fragmentedTables);
                
                rs = (ResultSet) qr.getResult().get(resultIndex);
                resultIndex++;
                while (rs.next()) {
                    fragmentedTables.add(new Object[]{rs.getString(1), rs.getLong(2)});
                }
                rs.close();
                
                fragtables = fragmentedTables.size();
                
            }  else {
                        
                // Parse through the table list to generate storage engine counts/statistics
                fragtables = 0;
                for (Map<String, Object> t: tables) {
                    if (t.get("engine") == null) {
                        continue;
                    }

                    if (enginestats.get(t.get("engine")) != null) {
                        enginestats.put("engine", enginestats.get(t.get("engine")) + (Long)t.get("size"));
                        enginecount.put("engine", enginecount.get(t.get("engine")) + 1);
                    } else {
                        enginestats.put("engine", (Long)t.get("size"));
                        enginecount.put("engine", 1L);
                    }

                    if ((Long)t.get("datafree") > 0 ) {
                        fragtables++;
                    }
                }
            }
            
            for (String engine: enginestats.keySet()) {
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                    "Data in " + engine + " tables: " + roundBytes(enginestats.get(engine)) + 
                    " (Tables: " + enginecount.get(engine) + ")"));
            }        

            // If the storage engine isn't being used, recommend it to be disabled
            if (enginestats.get("InnoDB") == null
                && getVariable("have_innodb") != null
                && getVariable("have_innodb").equalsIgnoreCase("YES")) {

                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "InnoDB is enabled but isn't being used"));
                generalRecords.add("Add skip-innodb to MySQL configuration to disable InnoDB");
            }
            if (enginestats.get("BerkeleyDB") == null
                && getVariable("have_bdb") != null
                && getVariable("have_bdb").equalsIgnoreCase("YES")) { 

                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "BDB is enabled but isn't being used"));
                generalRecords.add("Add skip-bdb to MySQL configuration to disable BDB");
            }

            if (enginestats.get("ISAM") == null
                && getVariable("have_isam") != null
                && getVariable("have_isam").equalsIgnoreCase("YES")) { 

                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "MYISAM is enabled but isn't being used"));
                generalRecords.add("Add skip-isam to MySQL configuration to disable ISAM (MySQL > 4.1.0)");
            }

            // Fragmented tables
            if (fragtables > 0) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Total fragmented tables: " + fragtables));
                generalRecords.add("Run OPTIMIZE TABLE to defragment tables for better performance");

                int totalFree = 0;
                Map<String, Object> ts = (Map<String, Object>)result.get("Tables");
                if (ts != null) {
                    List<Object[]> list = (List<Object[]>)ts.get("Fragmented tables");
                    if (list != null) {
                        for (Object[] tableLine: list) {
                            String name = (String) tableLine[0];
                            Long dataFree = (Long) tableLine[1];

                            dataFree = dataFree / 1024 / 1024;
                            totalFree += dataFree;

                            String[] names = name.split("\\.");
                            generalRecords.add("OPTIMIZE TABLE `" + names[0] + "`.`" + names[1] + "`; -- can free " + dataFree + " MB");
                        }

                        generalRecords.add("Total freed space after theses OPTIMIZE TABLE : " + totalFree + " Mb");
                    }
                }

            } else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "Total fragmented tables: " + fragtables));
            }

            for (Map<String, Object> t: tables) {

                Long autoincrement = (Long) t.get("autoincrement");

                if (autoincrement != null) {
                    Double percent = percentage(autoincrement, (Number) result.get("MaxInt"));
                    Map<String, Object> m = (Map<String, Object>) result.get("PctAutoIncrement");
                    if (m == null) {
                        m = new HashMap<>();
                        result.put("PctAutoIncrement", m);
                    }

                    m.put((String)t.get("name"), percent);

                    if (percent >= 75) {
                        tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Table '" + t.get("name") + "' has an autoincrement value near max capacity (" + percent + "%)"));
                    }
                }
            }
            
        } catch (SQLException ex) {
            log.error("Error on 'checkStorageEngines'", ex);
        }
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" mysql_databases (Show informations about databases) ">
    public  void mysqlDatabases(List<String> generalRecords, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
    
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Database Metrics", tunerResult);
        
        if (!mysqlVersion(session, 5, 5)) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Skip Database metrics from information schema missing in this version"));
            return;
        }

        List<String> dblist = new ArrayList<>();
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_DATABASES_NAMES)) {
            if (rs != null) {
                while (rs.next()) {
                    dblist.add(rs.getString(1));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'mysqlDatabases'", ex);
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "There is " + dblist.size() + " Database(s)."));
    
        Double[] totalDbInfo = new Double[7];
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_DB_TOTAL_INFO)) {
            if (rs != null && rs.next()) {
                totalDbInfo[0] = rs.getDouble(1);
                totalDbInfo[1] = rs.getDouble(2);
                totalDbInfo[2] = rs.getDouble(3);
                totalDbInfo[3] = rs.getDouble(4);
                totalDbInfo[4] = rs.getDouble(5);
                totalDbInfo[5] = rs.getDouble(6);
                totalDbInfo[6] = rs.getDouble(7);
            }
        } catch (SQLException ex) {
            log.error("Error on 'mysqlDatabases'", ex);
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "All User Databases:"));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- TABLE : " + (totalDbInfo[4] != null ? totalDbInfo[4] : 0)));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- ROWS  : " + (totalDbInfo[0] != null ? totalDbInfo[0] : 0)));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- DATA  : " + bytes(totalDbInfo[1]) + "(" + percentage(totalDbInfo[1], totalDbInfo[3]) + "%)"));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- INDEX  : " + bytes(totalDbInfo[2]) + "(" + percentage(totalDbInfo[2], totalDbInfo[3]) + "%)"));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- SIZE  : " + bytes(totalDbInfo[3])));

        String collation = "";
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_DBS_COLLATION)) {
            if (rs != null) {
                while (rs.next()) {
                    if (!collation.isEmpty()) {
                        collation += ", ";
                    }
                    collation += rs.getString(1);
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'mysqlDatabases'", ex);
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- COLLA  : " + (totalDbInfo[5] != null ? totalDbInfo[5] : 0) + " (" + collation + ")"));
        
        String engines = "";
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_DISTINCT_ENGINES)) {
            if (rs != null) {
                while (rs.next()) {
                    if (!engines.isEmpty()) {
                        engines += ", ";
                    }
                    engines += rs.getString(1);
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'mysqlDatabases'", ex);
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- ENGIN  : " + (totalDbInfo[6] != null ? totalDbInfo[6] : 0) + " (" + engines + ")"));
        
        Map<String, Object> md = (Map<String, Object>) result.get("Databases");
        if (md == null) {
            md = new HashMap<>();
            result.put("Databases", md);
        }
        
        Map<String, Object> mall = (Map<String, Object>) md.get("All databases");
        if (mall == null) {
            mall = new HashMap<>();
            md.put("All databases", mall);
        }
        
        mall.put("Rows", totalDbInfo[0] != null ? totalDbInfo[0] : 0);
        mall.put("Data Size", totalDbInfo[1] != null ? totalDbInfo[1] : 0);
        mall.put("Data Pct", percentage(totalDbInfo[1], totalDbInfo[3]) + "%");
        mall.put("Index Size", totalDbInfo[2] != null ? totalDbInfo[2] : 0);
        mall.put("Index Pct", percentage(totalDbInfo[2], totalDbInfo[3]) + "%");
        mall.put("Total Size", totalDbInfo[3] != null ? totalDbInfo[3] : 0);
        
        for (String db: dblist) {
            try (ResultSet rs = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_DB_INFO, db))) {
                if (rs != null) {
                    while (rs.next()) {
                        
                        String name = rs.getString(1);
                        Double rowsSum = rs.getDouble(2);
                        Double dataLengthSum = rs.getDouble(3);
                        Double indexLengthSum = rs.getDouble(4);
                        Double allLengthSum = rs.getDouble(5);
                        Long engineCount = rs.getLong(6);
                        Long tableCount = rs.getLong(7);
                        Long collationCount = rs.getLong(8);
                        Long dEngineCount = rs.getLong(9);
                        
                        
                        if (name != null) {
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Database: " + name));
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- TABLE: " + (tableCount < 0 ? 0 : tableCount)));
                            
                            String coll = "";
                            try (ResultSet rs0 = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_DB_COLLATION, name))) {
                                if (rs0 != null) {
                                    while (rs0.next()) {
                                        if (!coll.isEmpty()) {
                                            coll += ", ";
                                        }
                                        coll += rs0.getString(1);
                                    }
                                }
                            } catch (SQLException ex) {
                                log.error("Error on 'mysqlDatabases'", ex);
                            }
                            
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- COLL: " + (collationCount < 0 ? 0 : collationCount) + "(" + coll + ")"));                            
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- ROWS: " + (rowsSum < 0 ? 0 : rowsSum)));
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- DATA: " + bytes(dataLengthSum) + "(" + percentage(dataLengthSum, allLengthSum) + "%)"));
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- INDEX: " + bytes(indexLengthSum) + "(" + percentage(indexLengthSum, allLengthSum) + "%)"));
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- TOTAL: " + bytes(allLengthSum)));
                            
                            String engin = "";
                            try (ResultSet rs0 = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_DISTINCT_ENGINE, name))) {
                                if (rs0 != null) {
                                    while (rs0.next()) {
                                        if (!engin.isEmpty()) {
                                            engin += ", ";
                                        }
                                        engin += rs0.getString(1);
                                    }
                                }
                            } catch (SQLException ex) {
                                log.error("Error on 'mysqlDatabases'", ex);
                            }

                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- ENGIN: " + (dEngineCount < 0 ? 0 : dEngineCount) + "(" + engin + ")"));
                            
                            if (dataLengthSum >= 0 && indexLengthSum >= 0 && dataLengthSum < indexLengthSum) {
                                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Index size is larger than data size for " + name));
                            }
                            
                            if (engineCount > 1) {
                                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "There are " + engineCount + " storage engines. Be careful."));
                            }
                            
                            Map<String, Object> m = (Map<String, Object>) md.get(name);
                            if (m == null) {
                                m = new HashMap<>();
                                md.put(name, m);
                            }

                            m.put("Rows", rowsSum);
                            m.put("Tables", tableCount);
                            m.put("Collations", collationCount);
                            m.put("Data Size", dataLengthSum);
                            m.put("Data Pct", percentage(dataLengthSum, allLengthSum) + "%");
                            m.put("Index Size", indexLengthSum);
                            m.put("Index Pct", percentage(indexLengthSum, allLengthSum) + "%");
                            m.put("Total Size", allLengthSum);
                            
                            if (collationCount > 1) {
                                tunerResult.add(new TunerResult(TunerResult.Status.BAD, collationCount + " different collations for database " + name));
                                generalRecords.add("Check all table collations are identical for all tables in " + name + " database.");
                                
                            } else {
                                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, collationCount + " collation for " + name + " database."));
                            }
                            
                            if (dEngineCount > 1) {
                                tunerResult.add(new TunerResult(TunerResult.Status.BAD, dEngineCount + " different engines for database " + name));
                                generalRecords.add("Check all table engines are identical for all tables in " + name + " database.");
                                
                            } else {
                                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, dEngineCount + " engine for " + name + " database."));
                            }
                            
                            List<String> columnCharsetList = new ArrayList<>();
                            try (ResultSet rs0 = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_DISTINCT_COLUMN_CHARSET, name))) {
                                if (rs0 != null) {
                                    while (rs0.next()) {
                                        columnCharsetList.add(rs0.getString(1));
                                    }
                                }
                            } catch (SQLException ex) {
                                log.error("Error on 'mysqlDatabases'", ex);
                            }
                            
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Charsets for " + name + " database table column: " + String.join(", ", columnCharsetList)));
                            
                            if (columnCharsetList.size() > 1) {
                                tunerResult.add(new TunerResult(TunerResult.Status.BAD, name + " table column(s) has several charsets defined for all text like column(s)."));
                                generalRecords.add("Limit charset for column to one charset if possible for " + name + " database.");
                                
                            } else {
                                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, name + " table column(s) has same charset defined for all text like column(s)."));
                            }
                            
                            List<String> collationList = new ArrayList<>();
                            try (ResultSet rs0 = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_DISTINCT_COLLATION, name))) {
                                if (rs0 != null) {
                                    while (rs0.next()) {
                                        collationList.add(rs0.getString(1));
                                    }
                                }
                            } catch (SQLException ex) {
                                log.error("Error on 'mysqlDatabases'", ex);
                            }
                            
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Collations for " + name + " database table column: " + String.join(", ", collationList)));
                            
                            if (collationList.size() > 1) {
                                tunerResult.add(new TunerResult(TunerResult.Status.BAD, name + " table column(s) has several collations defined for all text like column(s)."));
                                generalRecords.add("Limit collations for column to one collation if possible for " + name + " database.");
                                
                            } else {
                                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, name + " table column(s) has same collation defined for all text like column(s)."));
                            }
                        }
                    }
                }
            } catch (SQLException ex) {
                log.error("Error on 'mysqlDatabases'", ex);
            }
        }
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" mysql_tables (Recommendations for database columns) "> 
    public  void mysqlTables(List<String> generalRecords, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
        
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Table Column Metrics", tunerResult);
        
        if (!mysqlVersion(session, 5, 5)) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Skip Database metrics from information schema missing in this version"));
            return;
        }

        List<String> dblist = new ArrayList<>();
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_DATABASES_NAMES)) {
            if (rs != null) {
                while (rs.next()) {
                    String db = rs.getString(1);
                    dblist.add(db);
                    
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Database: " + db));
                    
                    DatabaseCache.getInstance().syncDatabase(db, session.getConnectionParam().cacheKey(), (AbstractDBService) session.getService());
                    
                    for (DBTable t: (List<DBTable>)(List)DatabaseCache.getInstance().getTablesKeys(session.getConnectionParam().cacheKey())) {
                        if (t.getDatabase().getName().equalsIgnoreCase(db)) {
                            String table = t.getName();
                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- TABLE: " + table));

                            for (Column column: t.getColumns()) {
                                String name = column.getName();
                                String type = column.getDataType().getName();
                                if (column.isNotNull()) {
                                    type += " NOT NULL";
                                }

                                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "     +-- Column " + table + "." + name + ":"));

                                try (ResultSet rs2 = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_ANALYZE, db, table))) {
                                    if (rs2 != null && rs2.next()) {

                                        String optimalType = rs2.getString("Optimal_fieldtype");
                                        if (!type.equalsIgnoreCase(optimalType)) {
                                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "      Current Fieldtype: " + type));
                                            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "      Optimal Fieldtype: " + optimalType));
                                            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Consider changing type for column " + name + " in table " + db + "." + table));

                                            generalRecords.add("ALTER TABLE " + db + "." + table + " MODIFY " + name + " " + optimalType + ";");
                                        } else {
                                            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, db + "." + table + "(" + name + ") type: " + type));
                                        }
                                    }
                                } catch (SQLException ex) {
                                    log.error("Error on 'mysqlTables'", ex);
                                }
                            }
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'mysqlTables'", ex);
        }
        
    }
    // </editor-fold>
  
  
    // <editor-fold defaultstate="collapsed" desc=" mysql_indexes (Recommendations for Indexes metrics) "> 
    public  void mysqlIndexes(List<String> generalRecords, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
        
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Indexes Metrics", tunerResult);
        
        if (!mysqlVersion(session, 5, 5)) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Skip Index metrics from information schema missing in this version"));
            return;
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Worst selectivity indexes:"));
        
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_INDEX_REQ_0)) {
            if (rs != null) {
                while (rs.next()) {
                    
                    String column = rs.getString(1);                    
                    String index = rs.getString(2);                    
                    Integer seq = rs.getInt(3);
                    Integer columnNumbers = rs.getInt(4);
                    Long cardinality = rs.getLong(5);
                    Long rows = rs.getLong(6);
                    String type = rs.getString(7);
                    Double selectivity = rs.getDouble(8);
                    
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Index: " + index));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- COLUMN      : " + column));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- NB SEQS     : " + seq + " column(s)"));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- NB COLS     : " + columnNumbers));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- CARDINALITY : " + cardinality + " distinct values"));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- NB ROWS     : " + rows + " rows"));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- TYPE        : " + type));
                    tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " +-- SELECTIVITY : " + selectivity + "%"));
                    
                    Map<String, Object> m = (Map<String, Object>) result.get("Indexes");
                    if (m == null) {
                        m = new HashMap<>();
                        result.put("Indexes", m);
                    }
                    
                    Map<String, Object> indexMap = new HashMap<>();
                    indexMap.put("Column", column);
                    indexMap.put("Sequence number", seq);
                    indexMap.put("Number of column", columnNumbers);
                    indexMap.put("Cardinality", cardinality);
                    indexMap.put("Row number", rows);
                    indexMap.put("Index Type", type);
                    indexMap.put("Selectivity", selectivity);

                    m.put(index, indexMap);
                    
                    if (selectivity < 25) {
                        tunerResult.add(new TunerResult(TunerResult.Status.BAD, index + " has a low selectivity"));
                    }
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'mysqlTables'", ex);
        }
        
        if (getVariable("performance_schema") != null && "ON".equals(getVariable("performance_schema"))) {
            
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Unused indexes:"));
             
            try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_INDEX_REQ_1)) {
                if (rs != null) {
                    
                    List<String> unusedIndexes = new ArrayList<>();
                    boolean first = true;
                    while (rs.next()) {

                        if (first) {
                            first = false;
                            generalRecords.add("Remove unused indexes.");
                        }
                        
                        String table = rs.getString(1);                    
                        String index = rs.getString(2);
                        
                        tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Index: " + index + " on " + table + " is not used."));
                        
                        unusedIndexes.add(table + "." + index);
                    }
                    
                    Map<String, Object> m = (Map<String, Object>) result.get("Indexes");
                    if (m == null) {
                        m = new HashMap<>();
                        result.put("Indexes", m);
                    }
                    m.put("Unused Indexes", unusedIndexes);
                }
            } catch (SQLException ex) {
                log.error("Error on 'mysqlTables'", ex);
            }
        }
        
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" security_recommendations (Display some security recommendations) "> 
    private  void securityRecommendations(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
    
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Security Recommendations", tunerResult);
        
        String PASS_COLUMN_NAME = "password";
        if (mysqlVersion(session, 5, 7)) {
            PASS_COLUMN_NAME = "authentication_string";
        }
        

        List<String> users = new ArrayList<>();
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_USERS)) {
            if (rs != null) {

                while (rs.next()) {
                    users.add(rs.getString(1));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'securityRecommendations'", ex);
        }
        
        if (!users.isEmpty()) {
            for (String user: users) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "User '" + user + "' is an anonymous account."));
            }
            
            generalRecords.add("Remove Anonymous User accounts - there are " + users.size() + " anonymous accounts.");
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "There are no anonymous accounts for any database users",
                "If accounts for anonymous users were created, these have an empty user name. The anonymous accounts have no password, so anyone can use them to connect to the MySQL server."));
        }

        if (!mysqlVersion(session, 5, 1)) {
//            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "No more password checks for MySQL version <= 5.1"));
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "MySQL version <= 5.1 are deprecated and end of support.",
                "MySQL has been improved  and increased db user encription complexity and it has been implemented in from 5.5 version. We recommend to upgrade mysql 5.5 for high security. "));
            return;
        }

        String query = TUNER__SELECT_USERS_1;
        // Looking for Empty Password
        if (mysqlVersion(session, 5, 5)) {
            query = TUNER__SELECT_USERS_0;
        }
        
        query = query.replaceAll("\\$PASS_COLUMN_NAME", PASS_COLUMN_NAME);
        
        users = new ArrayList<>();
        try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
            if (rs != null) {

                while (rs.next()) {
                    users.add(rs.getString(1));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'securityRecommendations'", ex);
        }
        
        if (!users.isEmpty()) {
            for (String user: users) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "User '" + user + "' has no password set."));
            }
            
            generalRecords.add("Set up a Password for user with the following SQL statement ( SET PASSWORD FOR 'user'\\@'SpecificDNSorIp' = PASSWORD('secure_password'); )");
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "All database users have passwords assigned"));
        }
        
        if (mysqlVersion(session, 5, 7)) {
            try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_PLUGIN)) {
                if (rs != null && rs.next()) {
                    Integer valPlugin = rs.getInt(1);
                    if (valPlugin >= 1) {
                        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Bug #80860 MySQL 5.7: Avoid testing password when validate_password is activated"));
                        return;
                    }
                }
            } catch (SQLException ex) {
                log.error("Error on 'securityRecommendations'", ex);
            }
        }

        // Looking for User with user/ uppercase /capitalise user as password
        users = new ArrayList<>();
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_USERS_2.replaceAll("\\$PASS_COLUMN_NAME", PASS_COLUMN_NAME))) {
            if (rs != null) {

                while (rs.next()) {
                    users.add(rs.getString(1));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'securityRecommendations'", ex);
        }
        
        if (!users.isEmpty()) {
            for (String u: users) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "User '" + u + "' has user name as password."));
            }
            
            generalRecords.add("Set up a Secure Password for user\\@host ( SET PASSWORD FOR 'user'\\@'SpecificDNSorIp' = PASSWORD('secure_password'); )");
        }
        
        
        users = new ArrayList<>();
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_USERS_3.replaceAll("\\$PASS_COLUMN_NAME", PASS_COLUMN_NAME))) {
            if (rs != null) {

                while (rs.next()) {
                    users.add(rs.getString(1));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'securityRecommendations'", ex);
        }
        
        if (!users.isEmpty()) {
            for (String u: users) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "User '" + u + "' hasn't specific host restriction."));
            }
            
            generalRecords.add("Restrict Host for user\\@% to user\\@SpecificDNSorIp");
        }
        
        List<String> pswds  = FunctionHelper.streamToStringArray(StandardResourceProvider.getInstance().getTunerBasicPasswords());
        if (pswds == null) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "There is no basic password file list!"));
            return;
        }
        
        try {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "There are " + pswds.size() + " basic passwords in the list."));
            
            int nbins = 0;
            if (!pswds.isEmpty()) {
                for (String p: pswds) {
                    
                    users = new ArrayList<>();
                    // Looking for User with user/ uppercase /capitalise weak password
                    try (ResultSet rs = (ResultSet) session.getService().execute(String.format(TUNER__SELECT_USERS_4.replaceAll("\\$PASS_COLUMN_NAME", PASS_COLUMN_NAME), PASS_COLUMN_NAME, p, p, p, p, p))) {
                        if (rs != null) {

                            while (rs.next()) {
                                users.add(rs.getString(1));
                            }
                        }
                    } catch (SQLException ex) {
                        log.error("Error on 'securityRecommendations'", ex);
                    }
                    
                    if (!users.isEmpty()) {
                        for (String u: users) {
                            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "User '" + u + "' is using weak password: " + p + " in a lower, upper or capitalize derivative version."));
                            nbins++;
                        }
                    }
                }
            }
            
            if (nbins > 0) {
                generalRecords.add(nbins + " user(s) used basic or weak password.");
            }            
        } catch (Exception ex) {
            log.error("Error on 'securityRecommendations'", ex);
        } 
        
        tunerResult.add(new TunerResult(TunerResult.Status.UNKNOWN, 
            "skip-grant-tables",
            "This option causes the server to start without using the privilege system at all, which gives anyone with access to the server unrestricted access to all databases."));

        String skip_name_resolve = containsVariable("skip_name_resolve") ? getVariable("skip_name_resolve") : "";
        if (skip_name_resolve != null && "ON".equalsIgnoreCase(skip_name_resolve.trim())) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "skip_name_resolve",
                "This variable is set from the value of the --skip-name-resolve option. If it is OFF, mysqld resolves host names when checking client connections. If it is ON, mysqld uses only IP numbers; in this case, all Host column values in the grant tables must be IP addresses or localhost."));
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "skip_name_resolve",
                "This variable is set from the value of the --skip-name-resolve option. If it is OFF, mysqld resolves host names when checking client connections. If it is ON, mysqld uses only IP numbers; in this case, all Host column values in the grant tables must be IP addresses or localhost."));
        }
        
        String skip_show_database = containsVariable("skip_show_database") ? getVariable("skip_show_database") : "";
        if (skip_show_database != null && "ON".equalsIgnoreCase(skip_show_database.trim())) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "skip_show_database",
                "If the variable value is ON, the SHOW DATABASES statement is permitted only to users who have the SHOW DATABASES privilege, and the statement displays all database names. If the value is OFF, SHOW DATABASES is permitted to all users, but displays the names of only those databases for which the user has the SHOW DATABASES or other privilege"));
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "skip_show_database",
                "If the variable value is ON, the SHOW DATABASES statement is permitted only to users who have the SHOW DATABASES privilege, and the statement displays all database names. If the value is OFF, SHOW DATABASES is permitted to all users, but displays the names of only those databases for which the user has the SHOW DATABASES or other privilege"));
        }
        
        String secure_file_priv = containsVariable("secure_file_priv") ? getVariable("secure_file_priv") : "";
        if (secure_file_priv != null && !secure_file_priv.trim().isEmpty()) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "secure_file_priv",
                "If empty, the variable has no effect. This is not a secure setting."));
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "secure_file_priv",
                "If empty, the variable has no effect. This is not a secure setting."));
        }
        
        String secure_auth = containsVariable("secure_auth") ? getVariable("secure_auth") : "";
        if (secure_auth != null && "ON".equalsIgnoreCase(secure_auth.trim())) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "secure_auth",
                "If this variable is enabled, the server blocks connections by clients that attempt to use accounts that have passwords stored in the old (pre-4.1) format. Enable this variable to prevent all use of passwords employing the old format (and hence insecure communication over the network)."));
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "secure_auth",
                "If this variable is enabled, the server blocks connections by clients that attempt to use accounts that have passwords stored in the old (pre-4.1) format. Enable this variable to prevent all use of passwords employing the old format (and hence insecure communication over the network)."));
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.UNKNOWN, 
            "safe-user-create",
            "If this option is enabled, a user cannot create new MySQL users by using the GRANT statement unless the user has the INSERTprivilege for the mysql.user table or any column in the table"));

        
        String old_passwords = containsVariable("old_passwords") ? getVariable("old_passwords") : "";
        if (old_passwords != null && "ON".equalsIgnoreCase(old_passwords.trim())) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "old_passwords",
                "This variable controls the password hashing method used by the PASSWORD() function"));
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "old_passwords",
                "This variable controls the password hashing method used by the PASSWORD() function"));
        }
        
        String local_infile = containsVariable("local_infile") ? getVariable("local_infile") : "";
        if (local_infile != null && "ON".equalsIgnoreCase(local_infile.trim())) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "local_infile",
                "This variable controls server-side LOCAL capability for LOAD DATA statements. Depending on the local_infile setting, the server refuses or permits local data loading by clients that have LOCAL enabled on the client side."));
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "local_infile",
                "This variable controls server-side LOCAL capability for LOAD DATA statements. Depending on the local_infile setting, the server refuses or permits local data loading by clients that have LOCAL enabled on the client side."));
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.UNKNOWN, 
            "des-key-file",
            "Read the default DES keys from this file. These keys are used by the DES_ENCRYPT() and DES_DECRYPT() functions."));


    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" mysql_myisam (Recommendations for MyISAM) "> 
    private  void mysqlMyisam(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
    
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("MyISAM Metrics", tunerResult);
        
        // Key buffer usage
        if (containsCalculation("pct_key_buffer_used")) {
            if (getDoubleCalculationValue("pct_key_buffer_used") < 90) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Key buffer used: " + getDoubleCalculationValue("pct_key_buffer_used") + "% ("
                    + hrNum(getDoubleVariableValue("key_buffer_size") * getDoubleCalculationValue("pct_key_buffer_used") / 100) 
                    + " used / " 
                    + hrNum(getDoubleVariableValue("key_buffer_size")) 
                    + " cache)")
                );
                
                //#push(@adjvars,"key_buffer_size (\~ ".hr_num( $myvar{'key_buffer_size'} * $mycalc{'pct_key_buffer_used'} / 100).")");
            } else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "Key buffer used: " + getDoubleCalculationValue("pct_key_buffer_used") + "% ("
                    + hrNum(getDoubleVariableValue("key_buffer_size") * getDoubleCalculationValue("pct_key_buffer_used") / 100)
                    + " used / "
                    + hrNum(getDoubleVariableValue("key_buffer_size"))
                    + " cache)")
                );
            }
        }
        
        // Key buffer
        if (!containsCalculation("total_myisam_indexes") && session.getConnectionParam().isRemote()) {
            generalRecords.add("Unable to calculate MyISAM indexes on remote MySQL server < 5.0.0");
        }
        else if ("fail".equals(getCalculation("total_myisam_indexes"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Cannot calculate MyISAM index size - re-run script as root user"));
        }
        else if (getDoubleCalculationValue("total_myisam_indexes") == 0.0) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "None of your MyISAM tables are indexed - add indexes immediately"));
        }
        else {
            if (getDoubleVariableValue("key_buffer_size") < getDoubleCalculationValue("total_myisam_indexes")
                && getDoubleCalculationValue("pct_keys_from_mem") < 95)
            {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Key buffer size / total MyISAM indexes: "
                  + bytes(getDoubleVariableValue("key_buffer_size")) + "/"
                  + bytes(getDoubleCalculationValue("total_myisam_indexes")) + ". key_buffer_size (> "
                      + bytes(getDoubleVariableValue("total_myisam_indexes"))
                      + ")"));
                
                adjvars.add(
                        "key_buffer_size (> "
                      + bytes(getDoubleVariableValue("total_myisam_indexes"))
                      + ")" );
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "Key buffer size / total MyISAM indexes: "
                  + bytes(getDoubleVariableValue("key_buffer_size")) + "/"
                  + bytes(getDoubleCalculationValue("total_myisam_indexes"))));
            }
            
            if (getDoubleStatusValue("Key_read_requests") > 0) {
                if (getDoubleCalculationValue("pct_keys_from_mem") < 95) {
                    tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                        "Read Key buffer hit rate: " + doubleToString(getDoubleCalculationValue("pct_keys_from_mem")) + " ("
                      + hrNum(getDoubleStatusValue("Key_read_requests"))
                      + " cached / "
                      + hrNum(getDoubleStatusValue("Key_reads"))
                      + " reads)"));
                }
                else {
                    tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                        "Read Key buffer hit rate: " + doubleToString(getDoubleCalculationValue("pct_keys_from_mem")) + " ("
                      + hrNum(getDoubleStatusValue("Key_read_requests") )
                      + " cached / "
                      + hrNum(getDoubleStatusValue("Key_reads"))
                      + " reads)"));
                }
            }
            
            if (getDoubleStatusValue("Key_write_requests") > 0) {
                if (getDoubleCalculationValue("pct_wkeys_from_mem") < 95) {
                    tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                      "Write Key buffer hit rate: " + doubleToString(getDoubleCalculationValue("pct_wkeys_from_mem")) + " ("
                      + hrNum(getDoubleStatusValue("Key_write_requests"))
                      + " cached / "
                      + hrNum(getDoubleStatusValue("Key_writes"))
                      + " writes)"));
                }
                else {
                    tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                      "Write Key buffer hit rate: " + doubleToString(getDoubleCalculationValue("pct_wkeys_from_mem")) + " ("
                      + hrNum(getDoubleStatusValue("Key_write_requests"))
                      + " cached / "
                      + hrNum(getDoubleStatusValue("Key_writes"))
                      + " writes)"));
                }
            }
        }
    }
    // </editor-fold>


    // <editor-fold defaultstate="collapsed" desc=" mysql_innodb (Recommendations for InnoDB) "> 
    private  void mysqlInnodb(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
    
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("InnoDB Metrics", tunerResult);
 
        Map<String, Object> m = (Map<String, Object>) result.get("Engine");
        Long innodbSize = m != null && m.containsKey("InnoDB") ? (Long)((Map<String, Object>)m.get("InnoDB")).get("Total Size") : null;
        if (innodbSize == null) {
            innodbSize = 0l;
        }
        
        // InnoDB
        if (!(containsVariable("have_innodb") 
                || "YES".equalsIgnoreCase(getVariable("have_innodb"))
                || innodbSize != null))
        {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "InnoDB is disabled."));
            if (mysqlVersion(session, 5, 5)) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "InnoDB Storage engine is disabled. InnoDB is the default storage engine"));
            }
            return;
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "InnoDB is enabled."));

        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "InnoDB Buffers"));
        
        if (containsVariable("innodb_buffer_pool_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Buffer Pool: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))));
        }
        
        if (containsVariable("innodb_buffer_pool_instances")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Buffer Pool Instances: "
                + getVariable("innodb_buffer_pool_instances")));
        }

        if (containsVariable("innodb_buffer_pool_chunk_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Buffer Pool Chunk Size: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_chunk_size"))));
        }
              
        if (containsVariable("innodb_additional_mem_pool_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Additional Mem Pool: "
                + bytes(getDoubleVariableValue("innodb_additional_mem_pool_size"))));
        }
              
        if (containsVariable("innodb_log_file_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Log File Size: "
                + bytes(getDoubleVariableValue("innodb_log_file_size"))));
        }
              
        if (containsVariable("innodb_log_files_in_group")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Log File In Group: "
                + getVariable("innodb_log_files_in_group")));
        }
              
        if (containsVariable("innodb_log_files_in_group")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Total Log File Size: "
                + bytes(getDoubleVariableValue("innodb_log_files_in_group") *
                    getDoubleVariableValue("innodb_log_file_size")) 
                + "("
                + doubleToString(getCalculation("innodb_log_size_pct"))
                + " % of buffer pool)"));
        }
                  
        if (containsVariable("innodb_log_buffer_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Log Buffer: "
                + bytes(getDoubleVariableValue("innodb_log_buffer_size"))));
        }
              
        if (containsStatus("Innodb_buffer_pool_pages_free")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Log Buffer Free: "
                + bytes(getDoubleStatusValue("Innodb_buffer_pool_pages_free"))));
        }
        
        if (containsStatus("Innodb_buffer_pool_pages_total")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                " +-- InnoDB Log Buffer Used: "
                + bytes(getDoubleStatusValue("Innodb_buffer_pool_pages_total"))));
        }
              
        if (containsVariable("innodb_thread_concurrency")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "InnoDB Thread Concurrency: "
                + getVariable("innodb_thread_concurrency")));
        }

        // InnoDB Buffer Pool Size
        if ("ON".equals(getVariable("innodb_file_per_table"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "InnoDB File per table is activated"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "InnoDB File per table is not activated. innodb_file_per_table=ON"));
            adjvars.add("innodb_file_per_table=ON" );
        }

        // InnoDB Buffer Pool Size
        if (getDoubleVariableValue("innodb_buffer_pool_size") > innodbSize) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "InnoDB buffer pool / data size: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size")) 
                + "/"
                + bytes(innodbSize.doubleValue())));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "InnoDB buffer pool / data size: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                + "/"
                + bytes(innodbSize.doubleValue()) + ". Please increase innodb_buffer_pool_size (>= "
                  + roundBytes(innodbSize.doubleValue())
                  + ") if possible."));
            adjvars.add(
                    "innodb_buffer_pool_size (>= "
                  + roundBytes(innodbSize.doubleValue())
                  + ") if possible." );
        }
        
        if (getDoubleCalculationValue("innodb_log_size_pct") < 20
            || getDoubleCalculationValue("innodb_log_size_pct") > 30 )
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Ratio InnoDB log file size / InnoDB Buffer pool size ("
                + doubleToString(getCalculation("innodb_log_size_pct")) 
                + " %): "
                + bytes(getDoubleVariableValue("innodb_log_file_size"))
                + " * "
                + getVariable("innodb_log_files_in_group")
                + "/"
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                + " should be equal 25%" + ". innodb_log_file_size should be (="
                    + roundBytes(
                      getDoubleVariableValue("innodb_buffer_pool_size") /
                        getDoubleVariableValue("innodb_log_files_in_group") / 4
                    )
                    + ") if possible, so InnoDB total log files size equals to 25% of buffer pool size."));
            adjvars.add(
                  "innodb_log_file_size should be (="
                    + roundBytes(
                      getDoubleVariableValue("innodb_buffer_pool_size") /
                        getDoubleVariableValue("innodb_log_files_in_group") / 4
                    )
                    + ") if possible, so InnoDB total log files size equals to 25% of buffer pool size."
            );
            generalRecords.add("Read this before changing innodb_log_file_size and/or innodb_log_files_in_group: http://bit.ly/2wgkDvS");
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "Ratio InnoDB log file size / InnoDB Buffer pool size: "
                + bytes(getDoubleVariableValue("innodb_log_file_size"))
                + " * "
                + getVariable("innodb_log_files_in_group")
                + "/"
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                + " should be equal 25%"));
        }

        // InnoDB Buffer Pool Instances (MySQL 5.6.6+)
        if (containsVariable("innodb_buffer_pool_instances")) {

            // Bad Value if > 64
            if (getDoubleVariableValue("innodb_buffer_pool_instances") > 64) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                    "InnoDB buffer pool instances: "
                    + getVariable("innodb_buffer_pool_instances") + ". innodb_buffer_pool_instances (<= 64)"));
                
                adjvars.add("innodb_buffer_pool_instances (<= 64)" );
            }

            // InnoDB Buffer Pool Size > 1Go
            if (getDoubleVariableValue("innodb_buffer_pool_size") > 1024 * 1024 * 1024) {

                //# InnoDB Buffer Pool Size / 1Go = InnoDB Buffer Pool Instances limited to 64 max.

                //#  InnoDB Buffer Pool Size > 64Go
                int maxInnodbBufferPoolInstances = (int) (getDoubleVariableValue("innodb_buffer_pool_size") / ( 1024 * 1024 * 1024));
                if (maxInnodbBufferPoolInstances > 64) {
                    maxInnodbBufferPoolInstances = 64;
                }

                if (getDoubleVariableValue("innodb_buffer_pool_instances").intValue() != maxInnodbBufferPoolInstances)
                {
                    tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                        "InnoDB buffer pool instances: "
                        + getVariable("innodb_buffer_pool_instances") + ". innodb_buffer_pool_instances(="
                          + maxInnodbBufferPoolInstances
                          + ")"));
                    adjvars.add(
                            "innodb_buffer_pool_instances(="
                          + maxInnodbBufferPoolInstances
                          + ")" );
                }
                else {
                    tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                        "InnoDB buffer pool instances: "
                        + getVariable("innodb_buffer_pool_instances")));
                }

            // InnoDB Buffer Pool Size < 1Go
            }
            else {
                if (getDoubleVariableValue("innodb_buffer_pool_instances").intValue() != 1) {
                    tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                        "InnoDB buffer pool <= 1G and Innodb_buffer_pool_instances(!=1)." + " innodb_buffer_pool_instances (=1)"));
                    adjvars.add("innodb_buffer_pool_instances (=1)" );
                }
                else {
                    tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                        "InnoDB buffer pool instances: "
                        + getVariable("innodb_buffer_pool_instances")));
                }
            }
        }

        // InnoDB Used Buffer Pool Size vs CHUNK size
        if (!containsVariable("innodb_buffer_pool_chunk_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,
                "InnoDB Buffer Pool Chunk Size not used or defined in your version"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,
                "Number of InnoDB Buffer Pool Chunk : "
                + getDoubleVariableValue("innodb_buffer_pool_size").intValue() /
                  getDoubleVariableValue("innodb_buffer_pool_chunk_size").intValue() 
                + " for "
                + getVariable("innodb_buffer_pool_instances")
                + " Buffer Pool Instance(s)"));

            if (getDoubleVariableValue("innodb_buffer_pool_size").intValue() % (
                    getDoubleVariableValue("innodb_buffer_pool_chunk_size") *
                    getDoubleVariableValue("innodb_buffer_pool_instances")
                ) == 0
              )
            {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Innodb_buffer_pool_size aligned with Innodb_buffer_pool_chunk_size & Innodb_buffer_pool_instances"));
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                    "Innodb_buffer_pool_size aligned with Innodb_buffer_pool_chunk_size & Innodb_buffer_pool_instances" + 
                    ". Adjust innodb_buffer_pool_instances, innodb_buffer_pool_chunk_size with innodb_buffer_pool_size" + 
                    ". innodb_buffer_pool_size must always be equal to or a multiple of innodb_buffer_pool_chunk_size * innodb_buffer_pool_instances"));

                adjvars.add("Adjust innodb_buffer_pool_instances, innodb_buffer_pool_chunk_size with innodb_buffer_pool_size");
                adjvars.add("innodb_buffer_pool_size must always be equal to or a multiple of innodb_buffer_pool_chunk_size * innodb_buffer_pool_instances");
            }
        }

        // InnoDB Read efficency
        if (containsCalculation("pct_read_efficiency")
            && getDoubleCalculationValue("pct_read_efficiency") < 90)
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "InnoDB Read buffer efficiency: "
              + doubleToString(getCalculation("pct_read_efficiency"))
              + "% ("
              + doubleToString(getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                 getDoubleStatusValue("Innodb_buffer_pool_reads"))
              + " hits/ "
              + getStatus("Innodb_buffer_pool_read_requests")
              + " total)"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "InnoDB Read buffer efficiency: "
              + doubleToString(getCalculation("pct_read_efficiency"))
              + "% ("
              + doubleToString(getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                 getDoubleStatusValue("Innodb_buffer_pool_reads"))
              + " hits/ "
              + getStatus("Innodb_buffer_pool_read_requests")
              + " total)"));
        }

        // InnoDB Write efficiency
        if (containsCalculation("pct_write_efficiency")
            && getDoubleCalculationValue("pct_write_efficiency") < 90 )
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "InnoDB Write Log efficiency: "
              + doubleToString(Math.abs(getDoubleCalculationValue("pct_write_efficiency")))
              + "% ("
              + doubleToString(Math.abs(getDoubleStatusValue("Innodb_log_write_requests") -
                         getDoubleStatusValue("Innodb_log_writes")))
              + " hits/ "
              + getStatus("Innodb_log_write_requests")
              + " total)"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "InnoDB Write log efficiency: "
              + doubleToString(getCalculation("pct_write_efficiency"))
              + "% ("
              + doubleToString(getDoubleStatusValue("Innodb_log_write_requests") -
                 getDoubleStatusValue("Innodb_log_writes") )
              + " hits/ "
              + getStatus("Innodb_log_write_requests")
              + " total)"));
        }

        // InnoDB Log Waits
        if (containsStatus("Innodb_log_waits")
            && getDoubleStatusValue("Innodb_log_waits") > 0 )
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "InnoDB log waits: "
              + percentage(getDoubleStatusValue("Innodb_log_waits"), getDoubleStatusValue("Innodb_log_writes"))
              + "% ("
              + getStatus("Innodb_log_waits")
              + " waits / "
              + getStatus("Innodb_log_writes")
              + " writes)" + 
                ". innodb_log_buffer_size (>= "
                  + roundBytes(getDoubleVariableValue("innodb_log_buffer_size"))
                  + ")"));
            adjvars.add(
                    "innodb_log_buffer_size (>= "
                  + roundBytes(getDoubleVariableValue("innodb_log_buffer_size"))
                  + ")" );
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "InnoDB log waits: "
              + percentage(getDoubleStatusValue("Innodb_log_waits"), getDoubleStatusValue("Innodb_log_writes"))
              + "% ("
              + getStatus("Innodb_log_waits")
              + " waits / "
              + getStatus("Innodb_log_writes")
              + " writes)"));
        }
        
        result.put("Calculations", calculationCache);
    }
    // </editor-fold>
    
        
    // <editor-fold defaultstate="collapsed" desc=" calculations (Calculate values) "> 
        
    private void calculations(Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
        
        Map<String, Object> mycalc = calculationCache;
        
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Calculations", tunerResult);
        
        if (getDoubleStatusValue("Questions") < 1 ) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "Your server has not answered any queries - cannot continue..."));
            return;
        }

        // Per-thread memory
        if (mysqlVersion(session, 4)) {
            mycalc.put("per_thread_buffers", 
                getDoubleVariableValue("read_buffer_size") +
                getDoubleVariableValue("read_rnd_buffer_size") +
                getDoubleVariableValue("sort_buffer_size") +
                getDoubleVariableValue("thread_stack") +
                getDoubleVariableValue("join_buffer_size"));
        }
        else {
            mycalc.put("per_thread_buffers",
                getDoubleVariableValue("record_buffer") +
                getDoubleVariableValue("record_rnd_buffer") +
                getDoubleVariableValue("sort_buffer") +
                getDoubleVariableValue("thread_stack") +
                getDoubleVariableValue("join_buffer_size"));
        }
        
        mycalc.put("total_per_thread_buffers", getDoubleValue(mycalc, "per_thread_buffers") * getDoubleVariableValue("max_connections"));
        mycalc.put("max_total_per_thread_buffers", getDoubleValue(mycalc, "per_thread_buffers") * getDoubleStatusValue("Max_used_connections"));

        // Server-wide memory
        mycalc.put("max_tmp_table_size", 
            getDoubleVariableValue("tmp_table_size") > getDoubleVariableValue("max_heap_table_size")
                ? getDoubleVariableValue("max_heap_table_size")
                : getDoubleVariableValue("tmp_table_size"));
        
        mycalc.put("server_buffers", getDoubleVariableValue("key_buffer_size") + getDoubleValue(mycalc, "max_tmp_table_size"));
          
        mycalc.put("server_buffers", 
            getDoubleValue(mycalc, "server_buffers") + 
            (containsVariable("innodb_buffer_pool_size") 
                ? getDoubleVariableValue("innodb_buffer_pool_size") 
                : 0
            )
        );
        
        mycalc.put("server_buffers", 
            getDoubleValue(mycalc, "server_buffers") + 
            (containsVariable("innodb_additional_mem_pool_size") 
                ? getDoubleVariableValue("innodb_additional_mem_pool_size") 
                : 0
            )
        );
        
        mycalc.put("server_buffers", 
            getDoubleValue(mycalc, "server_buffers") + 
            (containsVariable("innodb_log_buffer_size") 
                ? getDoubleVariableValue("innodb_log_buffer_size") 
                : 0
            )
        );
        
        mycalc.put("server_buffers", 
            getDoubleValue(mycalc, "server_buffers") + 
            (containsVariable("query_cache_size") 
                ? getDoubleVariableValue("query_cache_size") 
                : 0
            )
        );
        
        mycalc.put("server_buffers", 
            getDoubleValue(mycalc, "server_buffers") + 
            (containsVariable("aria_pagecache_buffer_size") 
                ? getDoubleVariableValue("aria_pagecache_buffer_size") 
                : 0
            )
        );

        // Global memory
        // Max used memory is memory used by MySQL based on Max_used_connections
        // This is the max memory used theorically calculated with the max concurrent connection number reached by mysql
        mycalc.put("max_used_memory", 
            getDoubleValue(mycalc, "server_buffers") +
            getDoubleValue(mycalc, "max_total_per_thread_buffers") +
            getPfMemory() +
            getGcacheMemory()
        );
        
        mycalc.put("pct_max_used_memory", percentage(getDoubleValue(mycalc, "max_used_memory"), getPhysicalMemory()));

        // Total possible memory is memory needed by MySQL based on max_connections
        // This is the max memory MySQL can theorically used if all connections allowed has opened by mysql
        mycalc.put("max_peak_memory",
            getDoubleValue(mycalc, "server_buffers") +
            getDoubleValue(mycalc, "total_per_thread_buffers") +
            getPfMemory() +
            getGcacheMemory()
        );
          
        mycalc.put("pct_max_physical_memory", percentage(getDoubleValue(mycalc, "max_peak_memory"), getPhysicalMemory()));


        // Slow queries
        mycalc.put("pct_slow_queries", (long)(getDoubleStatusValue("Slow_queries") / getDoubleStatusValue("Questions")) * 100);

        // Connections
        mycalc.put("pct_connections_used", (long)(getDoubleStatusValue("Max_used_connections") / getDoubleVariableValue("max_connections")) * 100);
        mycalc.put("pct_connections_used", getDoubleValue(mycalc, "pct_connections_used") > 100 ? 100 : getDoubleValue(mycalc, "pct_connections_used"));

        // Aborted Connections
        mycalc.put("pct_connections_aborted", percentage(getDoubleStatusValue("Aborted_connects"), getDoubleStatusValue("Connections")));

        // Key buffers
        if (mysqlVersion(session, 4, 1) && getDoubleVariableValue("key_buffer_size") > 0 ) {
            mycalc.put("pct_key_buffer_used", 
                new BigDecimal(
                    (1 - (double) (getDoubleStatusValue("Key_blocks_unused") * getDoubleVariableValue("key_cache_block_size")) / (double) getDoubleVariableValue("key_buffer_size")) * 100
                ).setScale(2, RoundingMode.HALF_UP).toString()
            );
            
        } else {
            mycalc.put("pct_key_buffer_used", 0);
        }

        if (getDoubleStatusValue("Key_read_requests") > 0) {
            mycalc.put("pct_keys_from_mem", 
                new BigDecimal(
                    100 - ((double) getDoubleStatusValue("Key_reads") / (double) getDoubleStatusValue("Key_read_requests")) * 100
                ).setScale(2, RoundingMode.HALF_UP).toString()
            );
            
        } else {
            mycalc.put("pct_keys_from_mem", 0);
        }
        
        if (containsStatus("Aria_pagecache_read_requests") && getDoubleStatusValue("Aria_pagecache_read_requests") > 0) {
            
            mycalc.put("pct_aria_keys_from_mem", 
                new BigDecimal(
                    100 - ((double) getDoubleStatusValue("Aria_pagecache_reads") / (double) getDoubleStatusValue("Aria_pagecache_read_requests")) * 100
                ).setScale(2, RoundingMode.HALF_UP).toString()
            );
            
        } else {
            mycalc.put("pct_aria_keys_from_mem", 0);
        }
        
        if (getDoubleStatusValue("Key_write_requests") > 0) {
            
            mycalc.put("pct_wkeys_from_mem", 
                new BigDecimal(
                    ((double) getDoubleStatusValue("Key_writes") / (double) getDoubleStatusValue("Key_write_requests")) * 100
                ).setScale(2, RoundingMode.HALF_UP).toString()
            );
            
        } else {
            mycalc.put("pct_wkeys_from_mem", 0);
        }


        if (session.getConnectionParam().isRemote() && !mysqlVersion(session, 5)) {
            mycalc.put("total_myisam_indexes", findFilesSize(getVariable("datadir"), "*.MYI"));
            mycalc.put("total_aria_indexes", 0);        
            
        } else if (mysqlVersion(session, 5)) {
            mycalc.put("total_myisam_indexes", selectOne("SELECT IFNULL(SUM(INDEX_LENGTH),0) FROM information_schema.TABLES WHERE TABLE_SCHEMA NOT IN ('information_schema') AND ENGINE = 'MyISAM'"));
            mycalc.put("total_aria_indexes", selectOne("SELECT IFNULL(SUM(INDEX_LENGTH),0) FROM information_schema.TABLES WHERE TABLE_SCHEMA NOT IN ('information_schema') AND ENGINE = 'Aria'"));
        }
        
        if (mycalc.containsKey("total_myisam_indexes") && getDoubleValue(mycalc, "total_myisam_indexes") == 0 ) {
            mycalc.put("total_myisam_indexes", "fail");            
        }
            
        if (mycalc.containsKey("total_aria_indexes") && getDoubleValue(mycalc, "total_aria_indexes") == 0 ) {
            mycalc.put("total_aria_indexes", 1);            
        }
        

        // Query cache
        if (mysqlVersion(session, 4)) {
            
            mycalc.put("query_cache_efficiency", 
                new BigDecimal(
                    ((double) getDoubleStatusValue("Qcache_hits") / (double) (getDoubleStatusValue("Com_select") + getDoubleStatusValue("Qcache_hits"))) * 100
                ).setScale(2, RoundingMode.HALF_UP).toString()
            );
            
            if (getDoubleVariableValue("query_cache_size") > 0) {
                
                mycalc.put("pct_query_cache_used", 
                    new BigDecimal(
                        100 - ((double) getDoubleStatusValue("Qcache_free_memory") / (double) getDoubleVariableValue("query_cache_size")) * 100
                    ).setScale(2, RoundingMode.HALF_UP).toString()
                );
            }
            
            if (getDoubleStatusValue("Qcache_lowmem_prunes") == 0 ) {
                mycalc.put("query_cache_prunes_per_day", 0);                
            } else {
                mycalc.put("query_cache_prunes_per_day", (long) ((double) getDoubleStatusValue("Qcache_lowmem_prunes") / ((double) getDoubleStatusValue("Uptime") / 86400.0)));
            }
        }

        // Sorting
        mycalc.put("total_sorts", getDoubleStatusValue("Sort_scan") + getDoubleStatusValue("Sort_range"));
        if (getDoubleValue(mycalc, "total_sorts") > 0) {
            mycalc.put("pct_temp_sort_table", (long) ((double)getDoubleStatusValue("Sort_merge_passes") / (double)getDoubleValue(mycalc, "total_sorts")) * 100);
        }

        // Joins
        mycalc.put("joins_without_indexes", getDoubleStatusValue("Select_range_check") + getDoubleStatusValue("Select_full_join"));
        mycalc.put("joins_without_indexes_per_day", (long) ((double)getDoubleValue(mycalc, "joins_without_indexes") / ((double)getDoubleStatusValue("Uptime") / 86400.0)));

        // Temporary tables
        if (getDoubleStatusValue("Created_tmp_tables") > 0) {
            if (getDoubleStatusValue("Created_tmp_disk_tables") > 0) {
                mycalc.put("pct_temp_disk", (long)
                    ((double)getDoubleStatusValue("Created_tmp_disk_tables") /(double)getDoubleStatusValue("Created_tmp_tables")) * 100
                );
            } else {
                mycalc.put("pct_temp_disk", 0);
            }
        }

        // Table cache
        if (getDoubleStatusValue("Opened_tables") > 0) {
            mycalc.put("table_cache_hit_rate", (long) ((double)getDoubleStatusValue("Open_tables") * 100 / (double)getDoubleStatusValue("Opened_tables")));
        } else {
            mycalc.put("table_cache_hit_rate", 100);
        }

        // Open files
        if (getDoubleVariableValue("open_files_limit") > 0) {
            mycalc.put("pct_files_open", (long) ((double)getDoubleStatusValue("Open_files") * 100 / (double)getDoubleVariableValue("open_files_limit")));
        }

        // Table locks
        if (getDoubleStatusValue("Table_locks_immediate") > 0) {
            if (getDoubleStatusValue("Table_locks_waited") == 0) {
                mycalc.put("pct_table_locks_immediate", 100);
            } else {
                mycalc.put("pct_table_locks_immediate", (long) ((double)getDoubleStatusValue("Table_locks_immediate") * 100 / (double)(getDoubleStatusValue("Table_locks_waited") + getDoubleStatusValue("Table_locks_immediate"))));
            }
        }

        // Thread cache
        mycalc.put("thread_cache_hit_rate", (long) (100 -(((double)getDoubleStatusValue("Threads_created") / (double)getDoubleStatusValue("Connections")) * 100)));

        // Other
        if (getDoubleStatusValue("Connections") > 0) {
            mycalc.put("pct_aborted_connections", (long) (((double)getDoubleStatusValue("Aborted_connects") / (double)getDoubleStatusValue("Connections")) * 100));
        }
        
        if (getDoubleStatusValue("Questions") > 0) {
            mycalc.put("total_reads", getStatus("Com_select"));
            mycalc.put("total_writes", 
                getDoubleStatusValue("Com_delete") +
                getDoubleStatusValue("Com_insert") +
                getDoubleStatusValue("Com_update") +
                getDoubleStatusValue("Com_replace")
            );
            
            if (getDoubleValue(mycalc, "total_reads") == 0) {
                mycalc.put("pct_reads", 0);
                mycalc.put("pct_writes", 100);
            } else {
                mycalc.put("pct_reads", (long) (((double)getDoubleValue(mycalc, "total_reads") / (double)(getDoubleValue(mycalc, "total_reads") + getDoubleValue(mycalc, "total_writes"))) * 100));
                mycalc.put("pct_writes", 100 - getDoubleValue(mycalc, "pct_reads"));
            }
        }

        // InnoDB
        Map<String, Object> m = (Map<String, Object>) result.get("Engine");
        Long innodbSize = m != null && m.containsKey("InnoDB") ? (Long)((Map<String, Object>)m.get("InnoDB")).get("Total Size") : null;
        
        if ("YES".equalsIgnoreCase(getVariable("have_innodb")) || innodbSize != null) {
            mycalc.put("innodb_log_size_pct",
                (getDoubleVariableValue("innodb_log_file_size") *
                  getDoubleVariableValue("innodb_log_files_in_group") * 100 /
                  getDoubleVariableValue("innodb_buffer_pool_size")
                )
            );
        }        

        // InnoDB Buffer pool read cache effiency
        if (getDoubleStatusValue("Innodb_buffer_pool_reads") == 0) {
            putStatus("Innodb_buffer_pool_read_requests", "1");
            putStatus("Innodb_buffer_pool_reads", "1");
        }

        if (getDoubleStatusValue("Innodb_buffer_pool_reads") > 0) {
            mycalc.put("pct_read_efficiency", percentage(
                (
                    getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                    getDoubleStatusValue("Innodb_buffer_pool_reads")
                ),
                getDoubleStatusValue("Innodb_buffer_pool_read_requests"))
            );
        }
        
        // InnoDB log write cache effiency
        if (getDoubleStatusValue("Innodb_log_writes") == 0) {
            putStatus("Innodb_log_write_requests", "1");
            putStatus("Innodb_log_writes", "1");
        }
        
        if (getDoubleStatusValue("Innodb_log_write_requests") > 0) {
            mycalc.put("pct_write_efficiency", percentage(
                (
                    getDoubleStatusValue("Innodb_log_write_requests") -
                    getDoubleStatusValue("Innodb_log_writes")
                ),
                getDoubleStatusValue("Innodb_log_write_requests"))
            );
        }
        
        if (getDoubleStatusValue("Innodb_buffer_pool_pages_total") > 0) {
            mycalc.put("pct_innodb_buffer_used", percentage(
                (
                    getDoubleStatusValue("Innodb_buffer_pool_pages_total") -
                    getDoubleStatusValue("Innodb_buffer_pool_pages_free")
                ),
                getDoubleStatusValue("Innodb_buffer_pool_pages_total"))
            );
        }        

        // Binlog Cache
        if (!"OFF".equalsIgnoreCase(getVariable("log_bin"))) {
            mycalc.put("pct_binlog_cache", percentage(
                getDoubleStatusValue("Binlog_cache_use") - getDoubleStatusValue("Binlog_cache_disk_use"),
                getDoubleStatusValue("Binlog_cache_use"))
            );
        }
    }
    // </editor-fold>
        
    
    // <editor-fold defaultstate="collapsed" desc=" mysql_stats (Print the server stats) "> 
        
    private  void mysqlStats(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
                
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Performance Metrics", tunerResult);

        // Show uptime, queries per second, connections, traffic stats
        String qps = null;
        if (getDoubleStatusValue("Uptime") > 0) {
            qps = new BigDecimal(getDoubleStatusValue("Questions") / getDoubleStatusValue("Uptime")).setScale(3, RoundingMode.HALF_UP).toString();
        }
        
        if (getDoubleStatusValue("Uptime") >= 86400) {
            generalRecords.add("MySQL started within last 24 hours - recommendations may be inaccurate");
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Uptime",
            prettyUptime(getDoubleStatusValue("Uptime")),
            "Up for: "
          + prettyUptime(getDoubleStatusValue("Uptime")) 
          + " ("
          + hrNum(getDoubleStatusValue("Questions")) 
          + " q ["
          + hrNum(new Double(qps))
          + " qps], "
          + hrNum(getDoubleStatusValue("Connections"))
          + " conn," 
          + " TX: "
          + roundBytes(getDoubleStatusValue("Bytes_sent"))
          + ", RX: "
          + roundBytes(getDoubleStatusValue("Bytes_received")) 
          + ")",
            "Uptime:The number of seconds that the server has been up. fx: (show global status like 'uptime')"));
          
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "QPS On DB",
            hrNum(new Double(qps)) + " QPS",
            hrNum(new Double(qps)) + " QPS (Quereis pre second)",
            "Queries: The number of statements executed by the server. This variable includes statements executed within stored programs. fx: (show global status like 'Queries') / uptime (show global status like 'uptime');"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Reads / Writes (ratio) On DB",
            percentToString(getCalculation("pct_reads")) + "% / "
          + percentToString(getCalculation("pct_writes")) + "%",
            "Reads / Writes: "
          + percentToString(getCalculation("pct_reads")) + "% / "
          + percentToString(getCalculation("pct_writes")) + "%",
            "Reads: The avrage numer of SELECT statements exeucted by the server per second / Writes: The avrage numer of WRITES(INSERT/UPDATE/DELETE/REPLACE) statements eecuted by the server per second. Fx (show global status like 'Com_select') / uptime (show global status like 'uptime') READ /  Reads (show global status like 'Com_insert' + show global status like 'Com_update'+show global status like 'Com_delete'+show global status like 'Com_replace'+  ) / uptime (show global status like 'uptime');"));

        // Binlog Cache
        if ("OFF".equalsIgnoreCase(getVariable("log_bin"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Binlog Status",
                "OFF",
                "Binary logging is disabled",
                "log_bin: This variable reports only on the status of binary logging. The binary log contains “events” that describe database changes such as table creation operations or changes to table data. It also contains events for statements that potentially could have made changes show VARIABLES like 'log_bin'"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Binlog Status",
                "ON",
                "Binary logging is enabled (GTID MODE: "
              + (containsVariable("gtid_mode") ? getVariable("gtid_mode") : "OFF")
              + ")",
                "log_bin: This variable reports only on the status of binary logging. The binary log contains “events” that describe database changes such as table creation operations or changes to table data. It also contains events for statements that potentially could have made changes show VARIABLES like 'log_bin'"));
        }

        // Memory usage
        
        Long otherProcessMemory = getOtherProcessMemory();
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "DB Server's RAM Size",
            bytes(getPhysicalMemory()),
            "Physical Memory     : " + bytes(getPhysicalMemory()),
            "RAM:DB Server's RAM Size"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Max MySQL Memory",
            bytes(getDoubleCalculationValue("max_peak_memory")),
            "Max MySQL memory    : " + bytes(getDoubleCalculationValue("max_peak_memory")),
            "Max MySQL Memory:Total Global Buffers + max_connection(er Thread Buffers)"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Other Process Memory",
            bytes(otherProcessMemory),
            "Other process memory: " + bytes(otherProcessMemory),
            "Other process memory"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Total Buffers",
            bytes(getDoubleCalculationValue("server_buffers") + getDoubleCalculationValue("per_thread_buffers") * getDoubleVariableValue("max_connections")),
            "Total buffers: "
          + bytes(getDoubleCalculationValue("server_buffers"))
          + " global + "
          + bytes(getDoubleCalculationValue("per_thread_buffers"))
          + " per thread (" + doubleToString(getDoubleVariableValue("max_connections")) + " max threads)",
            "Total Buffers:Total Global Buffers + max_connection(er Thread Buffers)"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "P_S max Memory Usage",
            roundBytes(getPfMemory()),
            "P_S Max memory usage: " + roundBytes(getPfMemory()),
            "P_S(Performance_schema) max memory usage: Maximum memory used byPerormance_schema. SHOW ENGINE PERFORMANCE_SCHEMA STATUS"));
        
        Map<String, Object> ps = (Map<String, Object>) result.get("P_S");
        if (ps == null) {
            ps = new HashMap<>();
            result.put("P_S", ps);
        }
        ps.put("memory", otherProcessMemory);
        ps.put("pretty_memory", roundBytes(otherProcessMemory));
          
        
        Long gcacheMemory = getGcacheMemory();
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Galera GCache",
            roundBytes(gcacheMemory),
            "Galera GCache Max memory usage: " + roundBytes(gcacheMemory),
            "Maximum memory used by Galary gcache."));
        
        Map<String, Object> gl = (Map<String, Object>) result.get("Galera");
        if (gl == null) {
            gl = new HashMap<>();
            result.put("Galera", gl);
        }
        
        Map<String, Object> gc = (Map<String, Object>) gl.get("GCache");
        if (gc == null) {
            gc = new HashMap<>();
            gl.put("GCache", gc);
        }
        
        gc.put("memory", gcacheMemory);
        gc.put("pretty_memory", roundBytes(gcacheMemory));

        //if ( $opt{buffers} ne 0 ) {
        tunerResult.add(new TunerResult("Global Buffers"));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Global Buffers",
            "Key Buffer",
            bytes(getDoubleVariableValue("key_buffer_size")),
            "Key Buffer: " + bytes(getDoubleVariableValue("key_buffer_size")),
            "Kye buffer: Index blocks for MyISAM tables are buffered and are shared by all threads. key_buffer_size is the size of the buffer used for index blocks. The key buffer is also known as the key cache.Show VARIABLES like 'key_buffer_size'"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Global Buffers",
            "Max Tmp Table",
            bytes(getDoubleCalculationValue("max_tmp_table_size")),
            "Max Tmp Table: " + bytes(getDoubleCalculationValue("max_tmp_table_size")),
            "Max Temp Tabel: This variable sets the maximum size to which user-created MEMORY tables are permitted to grow. or This variable sets the maximum size to which user-created MEMORY tables are permitted to grow. MAX(show VARIABLES like 'max_heap_table_size', show VARIABLES like 'tmp_table_size')"));
        
        if (containsVariable("innodb_buffer_pool_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Global Buffers",
                "InnoDB Buffer Pool",
                bytes(getDoubleVariableValue("innodb_buffer_pool_size")),
                "InnoDB Buffer Pool: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size")),
                "InnoDB Buffer Pool: InnoDB maintains a storage area called the buffer pool for caching data and indexes in memory. Knowing how the InnoDB buffer pool works, and taking advantage of it to keep frequently accessed data in memory, is an important aspect of MySQL tuning.Show VARIABLES like 'innodb_buffer_pool_size'"));
        }
        
        if (containsVariable("innodb_additional_mem_pool_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Global Buffers",
                "InnoDB additional mem Pool",
                bytes(getDoubleVariableValue("innodb_additional_mem_pool_size")),
                "InnoDB Additional Mem Pool: "
                + bytes(getDoubleVariableValue("innodb_additional_mem_pool_size")),
                "InnoDB additional mem pool: The size in bytes of a memory pool InnoDB uses to store data dictionary information and other internal data structures. The more tables you have in your application, the more memory you allocate here.show VARIABLES like 'innodb_additional_mem_pool_size'"));
        }
        
        if (containsVariable("innodb_log_buffer_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Global Buffers",
                "InnoDB Log Buffer",
                bytes(getDoubleVariableValue("innodb_log_buffer_size")),
                "InnoDB Log Buffer: "
                + bytes(getDoubleVariableValue("innodb_log_buffer_size")),
                "InnoDB log buffer:The size in bytes of the buffer that InnoDB uses to write to the log files on disk. fx: show VARIABLES like 'innodb_log_buffer_size'"));
        }
        
        if (containsVariable("query_cache_type")) {
            tunerResult.add(new TunerResult("Query Cache Buffers(Global buffers)"));
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Query Cache Buffers(Global buffers)",
                "Query Cache",
                getVariable("query_cache_type"),
                "Query Cache: "
              + getVariable("query_cache_type") + " - "
              + (
                getDoubleVariableValue("query_cache_type") == 0 ||
                  "OFF".equalsIgnoreCase(getVariable("query_cache_type")) ? "DISABLED"
                : (
                    getDoubleVariableValue("query_cache_type") == 1 ? "ALL REQUESTS"
                    : "ON DEMAND"
                )
              ),
                "Query Cache: Setting the GLOBAL query_cache_type value determines query cache behavior for all clients that connect after the change is made.  show VARIABLES like 'query_cache_type'"));
            
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,
                "Query Cache Buffers(Global buffers)",
                "Query Cache Size",
                bytes(getDoubleVariableValue("query_cache_size")),
                "Query Cache Size: " + bytes(getDoubleVariableValue("query_cache_size")),
                "Qery Cache Size: The amount of memory allocated for caching query results.show VARIABLES like 'query_cache_size'"));
        }

        tunerResult.add(new TunerResult("Per Thread Buffers"));
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Per Thread Buffers",
            "Read Buffer",
            bytes(getDoubleVariableValue("read_buffer_size")),
            "Read Buffer: " + bytes(getDoubleVariableValue("read_buffer_size")),
            "Read Buffer: Each thread that does a sequential scan for a MyISAM table allocates a buffer of this size (in bytes) for each table it scans. If you do many sequential scans, you might want to increase this value. fx: show VARIABLES like ' read_buffer_size'"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Per Thread Buffers",
            "Read RND Buffer",
            bytes(getDoubleVariableValue("read_rnd_buffer_size")),
            "Read RND Buffer: " + bytes(getDoubleVariableValue("read_rnd_buffer_size")),
            "Read RND Buffer: This variable is used for reads from MyISAM tables, and, for any storage engine, for Multi-Range Read optimization.show VARIABLES like 'read_rnd_buffer_size'"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Per Thread Buffers",
            "Sort Buffer",
            bytes(getDoubleVariableValue("sort_buffer_size")),
            "Sort Buffer: " + bytes(getDoubleVariableValue("sort_buffer_size")),
            "Sort Buffer:Each session that must perform a sort allocates a buffer of this size. sort_buffer_size is not specific to any storage engine and applies in a general manner for optimization. fx: show VARIABLES like 'sort_buffer_size'"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,
            "Per Thread Buffers",
            "Thread Stack",
            bytes(getDoubleVariableValue("thread_stack")),
            "Thread stack: " + bytes(getDoubleVariableValue("thread_stack")),
            "Thread Stack:The stack size for each thread. The default of 256KB  is large enough for normal operation. fx show VARIABLES like 'thread_stack'"));
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Per Thread Buffers",
            "Join Buffer",
            bytes(getDoubleVariableValue("join_buffer_size")),
            "Join Buffer: " + bytes(getDoubleVariableValue("join_buffer_size")),
            "Join Buffer: The minimum size of the buffer that is used for plain index scans, range index scans, and joins that do not use indexes and thus perform full table scans. Normally, the best way to get fast joins is to add indexes. fx show VARIABLES like 'join_buffer_size'"));
           
        if (!"OFF".equalsIgnoreCase(getVariable("log_bin"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Binlog Cache Buffers"));
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Binlog Cache: " + bytes(getDoubleVariableValue("binlog_cache_size"))));
        }
        
        
        tunerResult.add(new TunerResult("InnoDB Buffers"));
        
        if (containsVariable("innodb_buffer_pool_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "InnoDB Buffers",
                "InnoDB Buffer Pool",
                bytes(getDoubleVariableValue("innodb_buffer_pool_size")),
                "InnoDB Buffer Pool: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_size")),
                "InnoDB Buffer Pool: InnoDB maintains a storage area called the buffer pool for caching data and indexes in memory. Knowing how the InnoDB buffer pool works, and taking advantage of it to keep frequently accessed data in memory, is an important aspect of MySQL tuning.Show VARIABLES like 'innodb_buffer_pool_size'"));
        }
        
        if (containsVariable("innodb_buffer_pool_instances")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,  
                "InnoDB Buffers",
                "InnoDB Buffer Pool Instances",
                getVariable("innodb_buffer_pool_instances"),
                "InnoDB Buffer Pool Instances: "
                + getVariable("innodb_buffer_pool_instances"),
                "InnoDB Buffer Pool Instances:The number of regions that the InnoDB buffer pool is divided into. For systems with buffer pools in the multi-gigabyte range, dividing the buffer pool into separate instances can improve concurrency, by reducing contention as different threads read and write to cached pages. fx: show VARIABLES like 'innodb_buffer_pool_instances'"));
        }

        if (containsVariable("innodb_buffer_pool_chunk_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,   
                "InnoDB Buffers",
                "InnoDB Buffer Pool Chunk Size",
                bytes(getDoubleVariableValue("innodb_buffer_pool_chunk_size")),
                "InnoDB Buffer Pool Chunk Size: "
                + bytes(getDoubleVariableValue("innodb_buffer_pool_chunk_size")),
                "InnoDB Log File Size: innodb_buffer_pool_chunk_size defines the chunk size for InnoDB buffer pool resizing operations. fx: show VARIABLES like 'innodb_buffer_pool_chunk_size'"));
        }
              
        if (containsVariable("innodb_log_file_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,    
                "InnoDB Buffers",
                "InnoDB Log File Size",
                bytes(getDoubleVariableValue("innodb_log_file_size")),
                "InnoDB Log File Size: "
                + bytes(getDoubleVariableValue("innodb_log_file_size")),
                "InnoDB Log File Size: The size in bytes of each log file in a log group. fx: show VARIABLES like 'innodb_log_file_size'"));
        }
              
        if (containsVariable("innodb_log_files_in_group")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,    
                "InnoDB Buffers",
                "InnoDB Log File In Group",
                getVariable("innodb_log_files_in_group"),
                "InnoDB Log File In Group: "
                + getVariable("innodb_log_files_in_group"),
                "InnoDB Log File in Group: The number of log files in the log group. InnoDB writes to the files in a circular fashion. fx: show VARIABLES like 'innodb_log_files_in_group'"));
        }
              
        if (containsVariable("innodb_log_files_in_group")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "InnoDB Buffers",
                "InnoDB Total Log File Size",
                bytes(getDoubleVariableValue("innodb_log_files_in_group") *
                    getDoubleVariableValue("innodb_log_file_size")) 
                    + "("
                    + getCalculation("innodb_log_size_pct")
                    + " % of buffer pool)",
                "InnoDB Total Log File Size: "
                + bytes(getDoubleVariableValue("innodb_log_files_in_group") *
                    getDoubleVariableValue("innodb_log_file_size")) 
                + "("
                + percentToString(getCalculation("innodb_log_size_pct"))
                + " % of buffer pool)",
                "InnoDB Total Log File Size:  The combined size of log files (innodb_log_file_size * innodb_log_files_in_group) fx: show VARIABLES like 'innodb_log_files_in_group' *  show VARIABLES like 'innodb_log_file_size'"));
        }        
                  
        if (containsVariable("innodb_log_buffer_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,    
                "InnoDB Buffers",
                "InnoDB Log Buffer",
                bytes(getDoubleVariableValue("innodb_log_buffer_size")),
                "InnoDB Log Buffer: "
                + bytes(getDoubleVariableValue("innodb_log_buffer_size")),
                "InnoDB Log Buffer: The size in bytes of the buffer that InnoDB uses to write to the log files on disk fx: show VARIABLES like 'innodb_log_buffer_size'"));
        }
              
        if (containsStatus("Innodb_buffer_pool_pages_free")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,    
                "InnoDB Buffers",
                "InnoDB Log Buffer Free",
                bytes(getDoubleStatusValue("Innodb_buffer_pool_pages_free")),
                "InnoDB Log Buffer Free: "
                + bytes(getDoubleStatusValue("Innodb_buffer_pool_pages_free")),
                "InnoDB Log Buffer Free: The number of free pages in the InnoDB buffer pool. fx: show STATUS like 'Innodb_buffer_pool_pages_free'"));
        }
        
        if (containsStatus("Innodb_buffer_pool_pages_total")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,    
                "InnoDB Buffers",
                "InnoDB Log Buffer Used",
                bytes(getDoubleStatusValue("Innodb_buffer_pool_pages_total")),
                "InnoDB Log Buffer Used: "
                + bytes(getDoubleStatusValue("Innodb_buffer_pool_pages_total")),
                "InnoDB Log Buffer Used: The number of used pages in the InnoDB buffer pool. fx: show STATUS like 'Innodb_buffer_pool_pages_total' - show STATUS like 'Innodb_buffer_pool_pages_free'"));
        }
        
        tunerResult.add(new TunerResult("MyISM Buffers"));
        
        if (containsCalculation("pct_key_buffer_used")) {
            if (getDoubleCalculationValue("pct_key_buffer_used") < 90) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "MyISM Buffers",
                    "Key buffer used",
                        percentToString(getDoubleCalculationValue("pct_key_buffer_used")) + "% ("
                        + hrNum(getDoubleVariableValue("key_buffer_size") * getDoubleCalculationValue("pct_key_buffer_used") / 100) 
                        + " used / " 
                        + hrNum(getDoubleVariableValue("key_buffer_size")) 
                        + " cache)",
                    "Key buffer used: " + percentToString(getDoubleCalculationValue("pct_key_buffer_used")) + "% ("
                        + hrNum(getDoubleVariableValue("key_buffer_size") * getDoubleCalculationValue("pct_key_buffer_used") / 100) 
                        + " used / " 
                        + hrNum(getDoubleVariableValue("key_buffer_size")) 
                        + " cache)",
                    "Kye buffer: Index blocks for MyISAM tables are buffered and are shared by all threads. key_buffer_size is the size of the buffer used for index blocks. The key buffer is also known as the key cache.Show VARIABLES like 'key_buffer_size'")
                );
                
                //#push(@adjvars,"key_buffer_size (\~ ".hr_num( $myvar{'key_buffer_size'} * $mycalc{'pct_key_buffer_used'} / 100).")");
            } else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                    "MyISM Buffers",
                    "Key buffer used",
                    percentToString(getDoubleCalculationValue("pct_key_buffer_used")) + "% ("
                        + hrNum(getDoubleVariableValue("key_buffer_size") * getDoubleCalculationValue("pct_key_buffer_used") / 100)
                        + " used / "
                        + hrNum(getDoubleVariableValue("key_buffer_size"))
                        + " cache)",
                    "Key buffer used: " + getDoubleCalculationValue("pct_key_buffer_used") + "% ("
                        + hrNum(getDoubleVariableValue("key_buffer_size") * getDoubleCalculationValue("pct_key_buffer_used") / 100)
                        + " used / "
                        + hrNum(getDoubleVariableValue("key_buffer_size"))
                        + " cache)",
                    "Kye buffer: Index blocks for MyISAM tables are buffered and are shared by all threads. key_buffer_size is the size of the buffer used for index blocks. The key buffer is also known as the key cache.Show VARIABLES like 'key_buffer_size'")
                );
            }
        }
        
        if (getDoubleVariableValue("key_buffer_size") < getDoubleCalculationValue("total_myisam_indexes")
            && getDoubleCalculationValue("pct_keys_from_mem") < 95)
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "MyISM Buffers",
                "Key Buffer Hit Ratio",
                bytes(getDoubleVariableValue("key_buffer_size")) + "/"
                    + bytes(getDoubleCalculationValue("total_myisam_indexes")),
                "Key buffer size / total MyISAM indexes: "
                    + bytes(getDoubleVariableValue("key_buffer_size")) + "/"
                    + bytes(getDoubleCalculationValue("total_myisam_indexes")) + 
                    ". key_buffer_size (> "
                  + bytes(getDoubleVariableValue("total_myisam_indexes"))
                  + ")",
                "Key Buffer Hit Ratio: the ratio of  Key buffer size / total MyISAM indexes" + 
                    ". key_buffer_size (> "
                  + bytes(getDoubleVariableValue("total_myisam_indexes"))
                  + ")"));
            
            adjvars.add(
                    "key_buffer_size (> "
                  + bytes(getDoubleVariableValue("total_myisam_indexes"))
                  + ")" );
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "MyISM Buffers",
                "Key Buffer Hit Ratio",
                bytes(getDoubleVariableValue("key_buffer_size")) + "/"
                    + bytes(getDoubleCalculationValue("total_myisam_indexes")),
                "Key buffer size / total MyISAM indexes: "
                    + bytes(getDoubleVariableValue("key_buffer_size")) + "/"
                    + bytes(getDoubleCalculationValue("total_myisam_indexes")),
                "Key Buffer Hit Ratio: the ratio of  Key buffer size / total MyISAM indexes"));
        }

        if (getDoubleStatusValue("Key_read_requests") > 0) {
            if (getDoubleCalculationValue("pct_keys_from_mem") < 95) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "MyISM Buffers",
                    "Read Key Buffer Hit Rate",
                    doubleToString(getDoubleCalculationValue("pct_keys_from_mem")) + " ("
                        + hrNum(getDoubleStatusValue("Key_read_requests"))
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Key_reads"))
                        + " reads)",
                    "Read Key buffer hit rate: " + doubleToString(getDoubleCalculationValue("pct_keys_from_mem")) + " ("
                        + hrNum(getDoubleStatusValue("Key_read_requests"))
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Key_reads"))
                        + " reads)",
                    "Read key Bufferhit Rate: ratio of Key_writes/Key_write_requests"));
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                    "MyISM Buffers",
                    "Read Key Buffer Hit Rate",
                    doubleToString(getDoubleCalculationValue("pct_keys_from_mem")) + " ("
                        + hrNum(getDoubleStatusValue("Key_read_requests") )
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Key_reads"))
                        + " reads)",
                    "Read Key buffer hit rate: " + doubleToString(getDoubleCalculationValue("pct_keys_from_mem")) + " ("
                        + hrNum(getDoubleStatusValue("Key_read_requests") )
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Key_reads"))
                        + " reads)",
                    "Read key Bufferhit Rate: ratio of Key_writes/Key_write_requests"));
            }
        }
        
        tunerResult.add(new TunerResult("Recommendations"));
        
        if (is32Os() && getDoubleCalculationValue("max_used_memory") > 2 * 1024 * 1024 * 1024)
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Recommendations",
                "Allocating",
                "> 2GB",
                "Allocating > 2GB RAM on 32-bit systems can cause system instability",
                "Allocating > 2GB RAM on 32-bit systems can cause system instability"));
            
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Maximum Reached Memory Usage",
                bytes(getDoubleCalculationValue("max_used_memory"))
                    + " (" + percentToString(getCalculation("pct_max_used_memory")) + "% of installed RAM)",
                "Maximum reached memory usage: "
                    + bytes(getDoubleCalculationValue("max_used_memory"))
                    + " (" + percentToString(getCalculation("pct_max_used_memory")) + "% of installed RAM). We recommend to increase instance RAM",
                "Maximum Reached Memory Usage:Total global buffers and no.of open connection * threads buffer"));
        }
        else if (getDoubleCalculationValue("pct_max_used_memory") > 85) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Maximum Reached Memory Usage",
                bytes(getDoubleCalculationValue("max_used_memory"))
                    + " (" + percentToString(getCalculation("pct_max_used_memory")) + "% of installed RAM)",
                "Maximum reached memory usage: "
                    + bytes(getDoubleCalculationValue("max_used_memory"))
                    + " (" + percentToString(getCalculation("pct_max_used_memory")) + "% of installed RAM). We recommend to increase instance RAM",
                "Maximum Reached Memory Usage:Total global buffers and no.of open connection * threads buffer"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Maximum Reached Memory Usage",
                bytes(getDoubleCalculationValue("max_used_memory"))
                + " (" + percentToString(getCalculation("pct_max_used_memory")) + "% of installed RAM)",
                "Maximum reached memory usage: "
                    + bytes(getDoubleCalculationValue("max_used_memory"))
                    + " (" + percentToString(getCalculation("pct_max_used_memory")) + "% of installed RAM). We recommend to increase instance RAM",
                "Maximum Reached Memory Usage:Total global buffers and no.of open connection * threads buffer"));
        }

        if (getDoubleCalculationValue("pct_max_physical_memory") > 85) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Maximum Possible Memory Usage",
                bytes(getDoubleCalculationValue("max_peak_memory"))
                    + " (" + percentToString(getCalculation("pct_max_physical_memory")) + "% of installed RAM)",
                "Maximum possible memory usage: "
                    + bytes(getDoubleCalculationValue("max_peak_memory"))
                    + " (" + percentToString(getCalculation("pct_max_physical_memory")) + "% of installed RAM)",
                "Maximum possible memory usage:Total global buffers and max connections * threads buffer"));
            
            generalRecords.add("Reduce your overall MySQL memory footprint for system stability");
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Maximum Possible Memory Usage",
                bytes(getDoubleCalculationValue("max_peak_memory"))
                    + " (" + percentToString(getCalculation("pct_max_physical_memory")) + "% of installed RAM)",
                "Maximum possible memory usage: "
                    + bytes(getDoubleCalculationValue("max_peak_memory"))
                    + " (" + percentToString(getCalculation("pct_max_physical_memory")) + "% of installed RAM)",
                "Maximum possible memory usage:Total global buffers and max connections * threads buffer"));
        }

        if (getPhysicalMemory() < (getDoubleCalculationValue("max_peak_memory") + otherProcessMemory))
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "MySQL Configuration With Respended System RAM",
                "bad",
                "Overall possible memory usage with other process exceeded memory",
                "MySQL configuration with respended system RAM:Overall possible memory usage with other process is compatible with memory available"));
            
            generalRecords.add("Dedicate this server to your database for highest performance.");
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "MySQL Configuration With Respended System RAM",
                "good",
                "Overall possible memory usage with other process is compatible with memory available",
                "MySQL configuration with respended system RAM:Overall possible memory usage with other process is compatible with memory available"));
        }

        // Slow queries
        if (getDoubleCalculationValue("pct_slow_queries") > 5) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Slow Queries",
                    percentToString(getCalculation("pct_slow_queries")) + "% ("
                        + hrNum(getDoubleStatusValue("Slow_queries")) + "/"
                        + hrNum(getDoubleStatusValue("Questions")) + ")",
                "Slow queries: " + percentToString(getCalculation("pct_slow_queries")) + "% ("
                    + hrNum(getDoubleStatusValue("Slow_queries")) + "/"
                    + hrNum(getDoubleStatusValue("Questions")) + ")",
                "Slow queries: No.of slow queries loged "));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Slow Queries",
                percentToString(getCalculation("pct_slow_queries")) + "% ("
                    + hrNum(getDoubleStatusValue("Slow_queries")) + "/"
                    + hrNum(getDoubleStatusValue("Questions")) + ")",
                "Slow queries: " + percentToString(getCalculation("pct_slow_queries")) + "% ("
                    + hrNum(getDoubleStatusValue("Slow_queries")) + "/"
                    + hrNum(getDoubleStatusValue("Questions")) + ")",
                "Slow queries: No.of slow queries loged "));
        }
              
        if (getDoubleVariableValue("long_query_time") > 10) {
            adjvars.add("long_query_time (<= 10)");
        }
        
        if (containsVariable("log_slow_queries")) {
            if ("OFF".equalsIgnoreCase(getVariable("log_slow_queries"))) {
                generalRecords.add("Enable the slow query log to troubleshoot bad queries");
            }
        }

        // Connections
        if (getDoubleCalculationValue("pct_connections_used") > 85) {
            
            String adjvarsText = 
                "max_connections (> " + getVariable("max_connections") + "), " + 
                "wait_timeout (< " + getVariable("wait_timeout") + "), " + 
                "interactive_timeout (< " + getVariable("interactive_timeout") + ")";
                    
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Maximum Used Connections",
                percentToString(getCalculation("pct_connections_used")) + "%  (" + getStatus("Max_used_connections") + "/" + getVariable("max_connections") + ")",
                "Highest connection usage: " + percentToString(getCalculation("pct_connections_used")) + "%  (" + getStatus("Max_used_connections") + "/" + getVariable("max_connections") + "). " + adjvarsText,
                "Maximum Used Connections: Maximum used connections at high traffic fx: (show VARIABLES like 'max_connections'/show status like 'Max_used_connections'). " + adjvarsText));
            
            adjvars.add("max_connections (> " + getVariable("max_connections") + ")");
            adjvars.add("wait_timeout (< " + getVariable("wait_timeout") + ")");
            adjvars.add("interactive_timeout (< " + getVariable("interactive_timeout") + ")" );
            
            generalRecords.add("Reduce or eliminate persistent connections to reduce connection usage");
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Maximum Used Connections",
                percentToString(getCalculation("pct_connections_used")) + "% (" + getStatus("Max_used_connections") + "/" + getVariable("max_connections") + ")",
                "Highest usage of available connections: " + percentToString(getCalculation("pct_connections_used")) + "% (" + getStatus("Max_used_connections") + "/" + getVariable("max_connections") + ")",
                "Maximum Used Connections: Maximum used connections at high traffic fx: (show VARIABLES like 'max_connections'/show status like 'Max_used_connections')"));
        }

        // Aborted Connections
        if (getDoubleCalculationValue("pct_connections_aborted") > 3) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",    
                "Connection Closed By MySQL Server",
                percentToString(getCalculation("pct_connections_aborted")) + "%  (" + getStatus("Aborted_connects") + "/" + getStatus("Connections") +")",
                "Aborted connections: " + percentToString(getCalculation("pct_connections_aborted")) + "%  (" + getStatus("Aborted_connects") + "/" + getStatus("Connections") +")",
                "Connection Closed By MySQL Server: MySQL server closed sleep connection when those are reached max_timeout "));
            generalRecords.add("Reduce or eliminate unclosed connections and network issues");
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",    
                "Connection Closed By MySQL Server",
                percentToString(getCalculation("pct_connections_aborted")) + "%  (" + getStatus("Aborted_connects") + "/" + getStatus("Connections") +")",
                "Aborted connections: " + percentToString(getCalculation("pct_connections_aborted")) + "%  (" + getStatus("Aborted_connects") + "/" + getStatus("Connections") +")",
                "Connection Closed By MySQL Server: MySQL server closed sleep connection when those are reached max_timeout "));
        }

        // name resolution
        if (containsVariable("skip_networking") && "ON".equalsIgnoreCase(getVariable("skip_networking")))
        {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Recommendations", 
                "Skip Name Resolution",
                "ON",
                "Skipped name resolution test due to skip_networking=ON in system variables.",
                "Skip Name Resolution : show VARIABLES like 'skip_name_resolve';"));
        }
        else if (!containsVariable("skip_name_resolve")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Recommendations", 
                "Skip Name Resolution",
                "MISSED",
                "Skipped name resolution test due to missing skip_name_resolve in system variables.",
                "Skip Name Resolution : show VARIABLES like 'skip_name_resolve';"));
        }
        else if ("OFF".equalsIgnoreCase(getVariable("skip_name_resolve"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations", 
                "Skip Name Resolution",
                "OFF",
                "Name resolution is active : a reverse name resolution is made for each new connection and can reduce performance",
                "Skip Name Resolution : show VARIABLES like 'skip_name_resolve';"));
            generalRecords.add("Configure your accounts with ip or subnets only, then update your configuration with skip-name-resolve=1");
        }

        // Query cache
        if (!mysqlVersion(session, 4)) {

            // MySQL versions < 4.01 don't support query caching
            generalRecords.add("Upgrade MySQL to version 4+ to utilize query caching");
        }
        else if (/*getDoubleVariableValue("query_cache_size") < 1 &&*/ "OFF".equalsIgnoreCase(getVariable("query_cache_type")))
        {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations", 
                "Query Cache Type",
                "OFF",
                "Query cache is disabled by default due to mutex contention on multiprocessor machines.",
                "Query Cache Type:show VARIABLES like 'query_cache_type';"));
        }
        else if (getDoubleStatusValue("Com_select") == 0) {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations", 
                "Query Cache Type",
                "Com_select == 0",
                "Query cache cannot be analyzed - no SELECT statements executed",
                "Query Cache Type:show VARIABLES like 'query_cache_type';"));
        }
        else {
            
            String adjvarsText = "query_cache_size (=0), query_cache_type (=0)";
            
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Recommendations", 
                "Query Cache Type",
                "OFF",
                "Query cache may be disabled by default due to mutex contention. " + adjvarsText,
                "Query Cache Type:show VARIABLES like 'query_cache_type';. " + adjvarsText));
            
            adjvars.add("query_cache_size (=0)");
            adjvars.add("query_cache_type (=0)");
            
            if (getDoubleCalculationValue("query_cache_efficiency") < 20) {
                
                adjvarsText = "query_cache_limit (> "
                      + roundBytes(getDoubleVariableValue("query_cache_limit"))
                      + ", or use smaller result sets)";
                        
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "Recommendations",
                    "Query cache efficiency",
                    percentToString(getCalculation("query_cache_efficiency")) + "% ("
                        + hrNum(getDoubleStatusValue("Qcache_hits"))
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Qcache_hits") + getDoubleStatusValue("Com_select"))
                        + " selects). " + adjvarsText,
                    "Query cache efficiency: " + percentToString(getCalculation("query_cache_efficiency")) + "% ("
                        + hrNum(getDoubleStatusValue("Qcache_hits"))
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Qcache_hits") + getDoubleStatusValue("Com_select"))
                        + " selects). " + adjvarsText,
                    ""));
                
                
                adjvars.add(
                        "query_cache_limit (> "
                      + roundBytes(getDoubleVariableValue("query_cache_limit"))
                      + ", or use smaller result sets)");
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                    "Recommendations",
                    "Query cache efficiency",
                    getCalculation("query_cache_efficiency") + "% ("
                        + hrNum(getDoubleStatusValue("Qcache_hits"))
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Qcache_hits") + getDoubleStatusValue("Com_select"))
                        + " selects)",
                    "Query cache efficiency: " + getCalculation("query_cache_efficiency") + "% ("
                        + hrNum(getDoubleStatusValue("Qcache_hits"))
                        + " cached / "
                        + hrNum(getDoubleStatusValue("Qcache_hits") + getDoubleStatusValue("Com_select"))
                        + " selects)",
                    ""));
            }
            
            if (getDoubleCalculationValue("query_cache_prunes_per_day") > 98) {
                
                adjvarsText = "";
                if (getDoubleVariableValue("query_cache_size") >= 128 * 1024 * 1024) {
                    
                    adjvarsText = "query_cache_size (> "
                          + roundBytes(getDoubleVariableValue("query_cache_size"))
                          + ") [see warning above]";
                    
                    generalRecords.add("Increasing the query_cache size over 128M may reduce performance");
                    adjvars.add(
                            "query_cache_size (> "
                          + roundBytes(getDoubleVariableValue("query_cache_size"))
                          + ") [see warning above]");
                }
                else {
                    adjvarsText = "query_cache_size (> "
                          + roundBytes(getDoubleVariableValue("query_cache_size"))
                          + ")";
                    
                    adjvars.add(
                            "query_cache_size (> "
                          + roundBytes(getDoubleVariableValue("query_cache_size"))
                          + ")");
                }
                
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "Recommendations",
                    "Query Cache Prunes Per Day",
                    doubleToString(getCalculation("query_cache_prunes_per_day")),
                    "Query cache prunes per day: " + doubleToString(getCalculation("query_cache_prunes_per_day")) + ". " + adjvarsText,
                    adjvarsText));
                
                
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                    "Recommendations",
                    "Query Cache Prunes Per Day",
                    doubleToString(getCalculation("query_cache_prunes_per_day")),
                    "Query cache prunes per day: " + doubleToString(getCalculation("query_cache_prunes_per_day")),
                    ""));
            }
        }

        // Sorting
        if (getDoubleCalculationValue("total_sorts") == 0) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Temporary Tables Created For query Result Sorting",
                "no",
                "No Sort requiring temporary tables",
                "Temporary Tables Created For query Result Sorting: The ratio of temp sorts / sorts"));
        }
        else if (getDoubleCalculationValue("pct_temp_sort_table") > 10 ) {
            
            String adjvarsText = "sort_buffer_size (> "
                  + roundBytes(getDoubleVariableValue("sort_buffer_size"))
                  + "), read_rnd_buffer_size (> "
                  + roundBytes(getDoubleVariableValue("read_rnd_buffer_size"))
                  + ")";
            
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Temporary Tables Created For query Result Sorting",
                    percentToString(getCalculation("pct_temp_sort_table")) +"%",
                "Sorts requiring temporary tables: " + percentToString(getCalculation("pct_temp_sort_table")) +"% ("
                    + hrNum(getDoubleStatusValue("Sort_merge_passes"))
                    + " temp sorts / "
                    + hrNum(getDoubleCalculationValue("total_sorts"))
                    + " sorts). " + adjvarsText,
                "Temporary Tables Created For query Result Sorting: The ratio of temp sorts / sorts. " + adjvarsText));
            adjvars.add(
                    "sort_buffer_size (> "
                  + roundBytes(getDoubleVariableValue("sort_buffer_size"))
                  + ")");
            adjvars.add(
                    "read_rnd_buffer_size (> "
                  + roundBytes(getDoubleVariableValue("read_rnd_buffer_size"))
                  + ")");
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Temporary Tables Created For query Result Sorting",
                percentToString(getCalculation("pct_temp_sort_table")) + "%",
                "Sorts requiring temporary tables: " + percentToString(getCalculation("pct_temp_sort_table")) + "% ("
                    + hrNum(getDoubleStatusValue("Sort_merge_passes"))
                    + " temp sorts / "
                    + hrNum(getDoubleCalculationValue("total_sorts"))
                    + " sorts)",
                "Temporary Tables Created For query Result Sorting: The ratio of temp sorts / sorts"));
        }

        
        // Joins
        
        if (getDoubleCalculationValue("joins_without_indexes_per_day") > 250 ) {
            
            String adjvarsText = "join_buffer_size (> "
                  + roundBytes(getDoubleVariableValue("join_buffer_size"))
                  + ", or always use indexes with joins)";
            
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Recommendations",
                "Queries Join With Out Index",
                doubleToString(getCalculation("joins_without_indexes")),
                "Joins performed without indexes: " + doubleToString(getCalculation("joins_without_indexes")) + ". " + adjvarsText,
                "Qeries Join With Out Index: Total no of queres which are not using for index for table join. " + adjvarsText));
            adjvars.add(
                    "join_buffer_size (> "
                  + roundBytes(getDoubleVariableValue("join_buffer_size"))
                  + ", or always use indexes with joins)");
            generalRecords.add("Adjust your join queries to always utilize indexes" );
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Queries Join With Out Index",
                "no",
                "No joins without indexes",
                "Qeries Join With Out Index: Total no of queres which are not using for index for table join. "));

            // No joins have run without indexes
        }

        // Temporary tables
        if (getDoubleStatusValue("Created_tmp_tables") > 0) {
            if (getDoubleCalculationValue("pct_temp_disk") > 25
                && getDoubleCalculationValue("max_tmp_table_size") < 256 * 1024 * 1024)
            {
                String adjvarsText = "tmp_table_size (> "
                      + roundBytes(getDoubleVariableValue("tmp_table_size"))
                      + "), " + "max_heap_table_size (> "
                      + roundBytes(getDoubleVariableValue("max_heap_table_size"))
                      + ")";
                
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "Recommendations",
                    "Temporary Tables Created On disk",
                    percentToString(getCalculation("pct_temp_disk")) + "% ("
                        + hrNum(getDoubleStatusValue("Created_tmp_disk_tables"))
                        + " on disk / "
                        + hrNum(getDoubleStatusValue("Created_tmp_tables"))
                        + " total)",
                    "Temporary tables created on disk: " + percentToString(getCalculation("pct_temp_disk")) + "% ("
                        + hrNum(getDoubleStatusValue("Created_tmp_disk_tables"))
                        + " on disk / "
                        + hrNum(getDoubleStatusValue("Created_tmp_tables"))
                        + " total). " + adjvarsText,
                    "Temporary Tables Created On disk:Temporary tables created on disk for query sort. " + adjvarsText));
                
                adjvars.add(
                        "tmp_table_size (> "
                      + roundBytes(getDoubleVariableValue("tmp_table_size"))
                      + ")" );
                adjvars.add(
                        "max_heap_table_size (> "
                      + roundBytes(getDoubleVariableValue("max_heap_table_size"))
                      + ")");
                
                generalRecords.add("When making adjustments, make tmp_table_size/max_heap_table_size equal");
                generalRecords.add("Reduce your SELECT DISTINCT queries which have no LIMIT clause");
            }
            else if (getDoubleCalculationValue("pct_temp_disk") > 25
                && getDoubleCalculationValue("'max_tmp_table_size") >= 256 * 1024 * 1024 )
            {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "Recommendations",
                    "Temporary Tables Created On disk",
                    percentToString(getCalculation("pct_temp_disk")) + "% ("
                        + hrNum(getDoubleStatusValue("Created_tmp_disk_tables"))
                        + " on disk / "
                        + hrNum(getDoubleStatusValue("Created_tmp_tables"))
                        + " total)",
                    "Temporary tables created on disk: " + percentToString(getCalculation("pct_temp_disk")) + "% ("
                        + hrNum(getDoubleStatusValue("Created_tmp_disk_tables"))
                        + " on disk / "
                        + hrNum(getDoubleStatusValue("Created_tmp_tables"))
                        + " total)",
                    "Temporary Tables Created On disk:Temporary tables created on disk for query sort."));
                
                generalRecords.add("Temporary table size is already large - reduce result set size");
                generalRecords.add("Reduce your SELECT DISTINCT queries without LIMIT clauses");
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Recommendations",
                    "Temporary Tables Created On disk",
                    percentToString(getCalculation("pct_temp_disk")) + "% ("
                        + hrNum(getDoubleStatusValue("Created_tmp_disk_tables"))
                        + " on disk / "
                        + hrNum(getDoubleStatusValue("Created_tmp_tables"))
                        + " total)",
                    "Temporary tables created on disk: " + percentToString(getCalculation("pct_temp_disk")) + "% ("
                        + hrNum(getDoubleStatusValue("Created_tmp_disk_tables"))
                        + " on disk / "
                        + hrNum(getDoubleStatusValue("Created_tmp_tables"))
                        + " total)",
                    "Temporary Tables Created On disk:Temporary tables created on disk for query sort."));
            }
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "Temporary Tables Created On disk",
                "no",
                "No tmp tables created on disk",
                "Temporary Tables Created On disk:Temporary tables created on disk for query sort."));
        }

        // Thread cache
        if (getDoubleVariableValue("thread_cache_size") == 0) {
            String adjvarsText = "thread_cache_size (start at 4)";
            
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "Thread Cache Hit Rate",
                "disabled",
                "Thread cache is disabled. " + adjvarsText,
                "Thread cache hit ratio: the ratio of  created connection / total connections. " + adjvarsText));
            generalRecords.add("Set thread_cache_size to 4 as a starting value");
            
            adjvars.add("thread_cache_size (start at 4)");
        }
        else {
            if (containsVariable("thread_handling")
                && "pools-of-threads".equalsIgnoreCase(getVariable("thread_handling")))
            {
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,                    
                    "Recommendations",
                    "Thread Cache Hit Rate",
                    "Not used with pool-of-threads",
                    "Thread cache hit rate: not used with pool-of-threads",
                    "Thread cache hit ratio: the ratio of  created connection / total connections"));
            }
            else {
                if (getDoubleCalculationValue("thread_cache_hit_rate") <= 50) {
                    String adjvarsText = "thread_cache_size (> " + getVariable("thread_cache_size") + ")";
                    tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                        "Recommendations",
                        "Thread Cache Hit Rate",
                        percentToString(getCalculation("thread_cache_hit_rate")) + "% ("
                            + hrNum(getDoubleStatusValue("Threads_created"))
                            + " created / "
                            + hrNum(getDoubleStatusValue("Connections"))
                            + " connections)",
                        "Thread cache hit rate: " + percentToString(getCalculation("thread_cache_hit_rate")) + "% ("
                            + hrNum(getDoubleStatusValue("Threads_created"))
                            + " created / "
                            + hrNum(getDoubleStatusValue("Connections"))
                            + " connections). " + adjvarsText,
                        "Thread cache hit ratio: the ratio of  created connection / total connections. " + adjvarsText));
                    
                    adjvars.add("thread_cache_size (> " + getVariable("thread_cache_size") + ")");
                }
                else {
                    tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                        "Recommendations",
                        "Thread Cache Hit Rate",
                        percentToString(getCalculation("thread_cache_hit_rate")) + "% ("
                            + hrNum(getDoubleStatusValue("Threads_created") )
                            + " created / "
                            + hrNum(getDoubleStatusValue("Connections") )
                            + " connections)",
                        "Thread cache hit rate: " + percentToString(getCalculation("thread_cache_hit_rate")) + "% ("
                            + hrNum(getDoubleStatusValue("Threads_created") )
                            + " created / "
                            + hrNum(getDoubleStatusValue("Connections") )
                            + " connections)",
                        "Thread cache hit ratio: the ratio of  created connection / total connections"));
                }
            }
        }

        // Table cache
        String tableCacheVar = "";
        if (getDoubleStatusValue("Open_tables") > 0) {
            if (getDoubleCalculationValue("table_cache_hit_rate") < 20) {
                
                if (mysqlVersion(session, 5, 1)) {
                    tableCacheVar = "table_open_cache";
                }
                else {
                    tableCacheVar = "table_cache";
                }
                
                String adjvarsText = tableCacheVar + " (> " + getVariable(tableCacheVar) + ")";
                
                tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                    "Recommendations",
                    "Table Cache Hit Rate",
                    percentToString(getCalculation("table_cache_hit_rate")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_tables"))
                        + " open / "
                        + hrNum(getDoubleStatusValue("Opened_tables"))
                        + " opened)",
                    "Table cache hit rate: " + percentToString(getCalculation("table_cache_hit_rate")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_tables"))
                        + " open / "
                        + hrNum(getDoubleStatusValue("Opened_tables"))
                        + " opened). " + adjvarsText,
                    "Table cache hit ratio: The ratio of open tables / opened tables. " + adjvarsText));
                
                adjvars.add(tableCacheVar + " (> " + getVariable(tableCacheVar) + ")" );
                    
                generalRecords.add(
                        "Increase "
                      + tableCacheVar
                      + " gradually to avoid file descriptor limits");
                generalRecords.add(
                        "Read this before increasing "
                      + tableCacheVar
                      + " over 64: http://bit.ly/1mi7c4C");
                generalRecords.add(
                        "Beware that open_files_limit ("
                      + getVariable("open_files_limit")
                      + ") variable ");
                generalRecords.add(
                        "should be greater than " + tableCacheVar + " ("
                      + getVariable(tableCacheVar)
                      + ")");
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Recommendations",
                    "Table Cache Hit Rate",
                    percentToString(getCalculation("table_cache_hit_rate")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_tables"))
                        + " open / "
                        + hrNum(getDoubleStatusValue("Opened_tables"))
                        + " opened)",
                    "Table cache hit rate: " + percentToString(getCalculation("table_cache_hit_rate")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_tables"))
                        + " open / "
                        + hrNum(getDoubleStatusValue("Opened_tables"))
                        + " opened)",
                    "Table cache hit ratio: The ratio of open tables / opened tables"));
            }
        }

        // Open files
        if (containsCalculation("pct_files_open")) {
            if (getDoubleCalculationValue("pct_files_open") > 85 ) {
                
                String adjvarsText = "open_files_limit (> " + getVariable("open_files_limit") + ")" ;
                
                tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                    "Recommendations",
                    "Open File Limit Used",
                    percentToString(getCalculation("pct_files_open")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_files")) + "/"
                        + hrNum(getDoubleVariableValue("open_files_limit")) + ")",
                    "Open file limit used: " + percentToString(getCalculation("pct_files_open")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_files")) + "/"
                        + hrNum(getDoubleVariableValue("open_files_limit")) + "). " + adjvarsText,
                    "Open file limit used: the ratio of open files / opened tables. " + adjvarsText));
                
                adjvars.add("open_files_limit (> " + getVariable("open_files_limit") + ")" );
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Recommendations",
                    "Open File Limit Used",
                    percentToString(getCalculation("pct_files_open")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_files")) + "/"
                        + hrNum(getDoubleVariableValue("open_files_limit")) + ")",
                    "Open file limit used: " + percentToString(getCalculation("pct_files_open")) + "% ("
                        + hrNum(getDoubleStatusValue("Open_files")) + "/"
                        + hrNum(getDoubleVariableValue("open_files_limit")) + ")",
                    "Open file limit used: the ratio of open files / opened tables."));
            }
        }

        // Table locks
        if (containsCalculation("pct_table_locks_immediate")) {
            if (getDoubleCalculationValue("pct_table_locks_immediate") < 95) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                    "Recommendations",
                    "Table Locks acquired immediately",
                    percentToString(getCalculation("pct_table_locks_immediate")) + "%",
                    "Table locks acquired immediately: " + percentToString(getCalculation("pct_table_locks_immediate")) + "%",
                    "Table Locks acquired immediately: Lock table Table locks acquired immediately"));
                
                generalRecords.add("Optimize queries and/or use InnoDB to reduce lock wait");
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Recommendations",
                    "Table Locks acquired immediately",
                    percentToString(getCalculation("'pct_table_locks_immediate")) + "% ("
                        + hrNum(getDoubleStatusValue("Table_locks_immediate"))
                        + " immediate / "
                        + hrNum(getDoubleStatusValue("Table_locks_waited") +
                            getDoubleStatusValue("Table_locks_immediate"))
                        + " locks)",
                    "Table locks acquired immediately: " + percentToString(getCalculation("'pct_table_locks_immediate")) + "% ("
                        + hrNum(getDoubleStatusValue("Table_locks_immediate"))
                        + " immediate / "
                        + hrNum(getDoubleStatusValue("Table_locks_waited") +
                            getDoubleStatusValue("Table_locks_immediate"))
                        + " locks)",
                    "Table Locks acquired immediately: Lock table Table locks acquired immediately"));
            }
        }

        // Binlog cache
        if (containsCalculation("pct_binlog_cache")) {
            if (getDoubleCalculationValue("pct_binlog_cache") < 90
                && getDoubleStatusValue("Binlog_cache_use") > 0)
            {
                String adjvarsText = "binlog_cache_size ("
                      + bytes(getDoubleVariableValue("binlog_cache_size") + 16 * 1024 * 1024)
                      + ")";
                        
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                    "Recommendations",
                    "Binlog Cache Memory Access",
                    percentToString(getCalculation("pct_binlog_cache")) + "% ("
                        + doubleToString(getDoubleStatusValue("Binlog_cache_use") - getDoubleStatusValue("Binlog_cache_disk_use"))
                        + " Memory / "
                        + getStatus("Binlog_cache_use")
                        + " Total)",
                    "Binlog cache memory access: "
                        + percentToString(getCalculation("pct_binlog_cache")) + "% ("
                        + doubleToString(getDoubleStatusValue("Binlog_cache_use") - getDoubleStatusValue("Binlog_cache_disk_use"))
                        + " Memory / "
                        + getStatus("Binlog_cache_use")
                        + " Total). " + adjvarsText,
                    adjvarsText));
                
                generalRecords.add(
                    "Increase binlog_cache_size (Actual value: "
                  + getVariable("binlog_cache_size")
                  + ")");
                adjvars.add(
                        "binlog_cache_size ("
                      + bytes(getDoubleVariableValue("binlog_cache_size") + 16 * 1024 * 1024)
                      + ")" );
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Recommendations",
                    "Binlog Cache Memory Access",
                    percentToString(getCalculation("pct_binlog_cache")) + "% ("
                        + doubleToString(getDoubleStatusValue("Binlog_cache_use") - getDoubleStatusValue("Binlog_cache_disk_use") )
                        + " Memory / "
                        + getStatus("Binlog_cache_use")
                        + " Total)",
                    "Binlog cache memory access: "
                        + percentToString(getCalculation("pct_binlog_cache")) + "% ("
                        + doubleToString(getDoubleStatusValue("Binlog_cache_use") - getDoubleStatusValue("Binlog_cache_disk_use") )
                        + " Memory / "
                        + getStatus("Binlog_cache_use")
                        + " Total)",
                    ""));
//                debugprint "Not enough data to validate binlog cache size\n"
//                  if $mystat{'Binlog_cache_use'} < 10;
            }
        }
        
        if (containsVariable("innodb_thread_concurrency")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Recommendations",
                "InnoDB Thread Concurrency",
                getVariable("innodb_thread_concurrency"),
                "InnoDB Thread Concurrency: "
                    + getVariable("innodb_thread_concurrency"),
                "InnoDB Thred Concurrentcy: InnoDB tries to keep the number of operating system threads concurrently inside InnoDB less than or equal to the limit given by this variable.Once the number of threads reaches this limit, additional threads are placed into a wait state within a “First In, First Out” (FIFO) queue for execution. Threads waiting for locks are not counted in the number of concurrently executing threads."));
        }

        if ("ON".equals(getVariable("innodb_file_per_table"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "InnoDB File Per Table",
                "ON",
                "InnoDB File per table is activated",
                "InnoDB File Per Table:When innodb_file_per_table is enabled (the default), InnoDB stores the data and indexes for each newly created table in a separate .ibd file instead of the system tablespace. The storage for these tables is reclaimed when the tables are dropped or truncated. This setting enables InnoDBfeatures such as table compression"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "InnoDB File Per Table",
                "OFF",
                "InnoDB File per table is not activated",
                "InnoDB File Per Table:When innodb_file_per_table is enabled (the default), InnoDB stores the data and indexes for each newly created table in a separate .ibd file instead of the system tablespace. The storage for these tables is reclaimed when the tables are dropped or truncated. This setting enables InnoDBfeatures such as table compression"));
        }
        
        Map<String, Object> m = (Map<String, Object>) result.get("Engine");
        Long innodbSize = m != null && m.containsKey("InnoDB") ? (Long)((Map<String, Object>)m.get("InnoDB")).get("Total Size") : null;
        if (innodbSize == null) {
            innodbSize = 0l;
        }
        
        // InnoDB Buffer Pool Size
        if (getDoubleVariableValue("innodb_buffer_pool_size") > innodbSize) {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD, 
                "Recommendations",
                "InnoDB Buffer Pool (%)",
                ((int)(getDoubleVariableValue("innodb_buffer_pool_size") / innodbSize.doubleValue())) + "% (" 
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size")) 
                    + "/"
                    + bytes(innodbSize.doubleValue()) + ")",
                "InnoDB buffer pool / data size: "
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size")) 
                    + "/"
                    + bytes(innodbSize.doubleValue()),
                "InnoDB Buffer Pool (%): InnoDB maintains a storage area called the buffer pool for caching data and indexes in memory"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, 
                "Recommendations",
                "InnoDB Buffer Pool (%)",
                ((int)(getDoubleVariableValue("innodb_buffer_pool_size") / innodbSize.doubleValue())) + "% (" 
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size")) 
                    + "/"
                    + bytes(innodbSize.doubleValue()) + ")",
                "InnoDB buffer pool / data size: "
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                    + "/"
                    + bytes(innodbSize.doubleValue()) + ". Please increase innodb_buffer_pool_size > innodb data size if possible",
                "InnoDB Buffer Pool (%): InnoDB maintains a storage area called the buffer pool for caching data and indexes in memory"));            
        }
        
        if (getDoubleCalculationValue("innodb_log_size_pct") < 20
            || getDoubleCalculationValue("innodb_log_size_pct") > 30 )
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Recommendations",  
                "InnoDB Log File Size (%)",
                percentToString(getCalculation("innodb_log_size_pct"))
                    + " %): "
                    + bytes(getDoubleVariableValue("innodb_log_file_size"))
                    + " * "
                    + getVariable("innodb_log_files_in_group")
                    + "/"
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                    + " [should be equal 25%]",
                "Ratio InnoDB log file size / InnoDB Buffer pool size ("
                    + percentToString(getCalculation("innodb_log_size_pct"))
                    + " %): "
                    + bytes(getDoubleVariableValue("innodb_log_file_size"))
                    + " * "
                    + getVariable("innodb_log_files_in_group")
                    + "/"
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                    + " should be equal 25%",
                "InnoDB Log File Size (%): The ratio of  InnoDB log file size / InnoDB Buffer pool size"));            
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "Recommendations",  
                "InnoDB Log File Size (%)",
                percentToString(getCalculation("innodb_log_size_pct"))
                    + " %): "
                    + bytes(getDoubleVariableValue("innodb_log_file_size"))
                    + " * "
                    + getVariable("innodb_log_files_in_group")
                    + "/"
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                    + " [should be equal 25%]",
                "Ratio InnoDB log file size / InnoDB Buffer pool size: "
                    + percentToString(getCalculation("innodb_log_size_pct"))
                    + " %): "
                    + bytes(getDoubleVariableValue("innodb_log_file_size"))
                    + " * "
                    + getVariable("innodb_log_files_in_group")
                    + "/"
                    + bytes(getDoubleVariableValue("innodb_buffer_pool_size"))
                    + " should be equal 25%",
                "InnoDB Log File Size (%): The ratio of  InnoDB log file size / InnoDB Buffer pool size"));
        }
        
        if (containsVariable("innodb_buffer_pool_instances")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "Recommendations",  
                "InnoDB Buffer Pool Instances",
                getVariable("innodb_buffer_pool_instances"),
                "InnoDB Buffer Pool Instances: "
                    + getVariable("innodb_buffer_pool_instances"),
                "InnoDB Buffer Pool Instances: Generally, the combined size of the log files should be large enough that the server can smooth out peaks and troughs in workload activity, which often means that there is enough redo log space to handle more than an hour of write activity. The larger the value, the less checkpoint flush activity is required in the buffer pool, saving disk I/O. Larger log files also make crash recovery slower, although improvements to recovery performance in MySQL 5.5 and higher make the log file size less of a consideration."));
        }
        
        // InnoDB Used Buffer Pool Size vs CHUNK size
        if (!containsVariable("innodb_buffer_pool_chunk_size")) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,
                "Recommendations",  
                "Number Of InnoDB Buffer Pool Chunk",
                "Not used",
                "InnoDB Buffer Pool Chunk Size not used or defined in your version",
                "Number Of InnoDB Buffer Pool Chunk: The number of regions that the InnoDB buffer pool is divided into. For systems with buffer pools in the multi-gigabyte range, dividing the buffer pool into separate instances can improve concurrency, by reducing contention as different threads read and write to cached pages. Each page that is stored in or read from the buffer pool is assigned to one of the buffer pool instances randomly, using a hashing function. Each buffer pool manages its own free lists, flush lists, LRUs, and all other data structures connected to a buffer pool, and is protected by its own buffer pool mutex"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL,
                "Recommendations",  
                "Number Of InnoDB Buffer Pool Chunk",
                String.valueOf(getDoubleVariableValue("innodb_buffer_pool_size").intValue() /
                      getDoubleVariableValue("innodb_buffer_pool_chunk_size").intValue()),
                "Number of InnoDB Buffer Pool Chunk : "
                    + getDoubleVariableValue("innodb_buffer_pool_size").intValue() /
                      getDoubleVariableValue("innodb_buffer_pool_chunk_size").intValue() 
                    + " for "
                    + getVariable("innodb_buffer_pool_instances")
                    + " Buffer Pool Instance(s)",
                "Number Of InnoDB Buffer Pool Chunk: The number of regions that the InnoDB buffer pool is divided into. For systems with buffer pools in the multi-gigabyte range, dividing the buffer pool into separate instances can improve concurrency, by reducing contention as different threads read and write to cached pages. Each page that is stored in or read from the buffer pool is assigned to one of the buffer pool instances randomly, using a hashing function. Each buffer pool manages its own free lists, flush lists, LRUs, and all other data structures connected to a buffer pool, and is protected by its own buffer pool mutex"));

            if (getDoubleVariableValue("innodb_buffer_pool_size").intValue() % (
                    getDoubleVariableValue("innodb_buffer_pool_chunk_size") *
                    getDoubleVariableValue("innodb_buffer_pool_instances")
                ) == 0
              )
            {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                    "Recommendations",  
                    "InnoDB Buffer Pools Check",
                    "good",
                    "Innodb_buffer_pool_size aligned with Innodb_buffer_pool_chunk_size & Innodb_buffer_pool_instances",
                    "InnoDB Buffer Pools Check: Innodb_buffer_pool_chunk_size defines the chunk size for InnoDB buffer pool resizing operations. Theinnodb_buffer_pool_size parameter is dynamic, which allows you to resize the buffer pool without restarting the server."));
            }
            else {
                
                String adjvarsText = 
                    "Adjust innodb_buffer_pool_instances, innodb_buffer_pool_chunk_size with innodb_buffer_pool_size, " + 
                    "innodb_buffer_pool_size must always be equal to or a multiple of innodb_buffer_pool_chunk_size * innodb_buffer_pool_instances";
                
                tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                    "Recommendations",  
                    "InnoDB Buffer Pools Check",
                    "bad",
                    "Innodb_buffer_pool_size aligned with Innodb_buffer_pool_chunk_size & Innodb_buffer_pool_instances. " + adjvarsText,
                    "InnoDB Buffer Pools Check: Innodb_buffer_pool_chunk_size defines the chunk size for InnoDB buffer pool resizing operations. Theinnodb_buffer_pool_size parameter is dynamic, which allows you to resize the buffer pool without restarting the server. " + adjvarsText));

                adjvars.add("Adjust innodb_buffer_pool_instances, innodb_buffer_pool_chunk_size with innodb_buffer_pool_size");
                adjvars.add("innodb_buffer_pool_size must always be equal to or a multiple of innodb_buffer_pool_chunk_size * innodb_buffer_pool_instances");
            }
        }
        
        // InnoDB Read efficency
        if (containsCalculation("pct_read_efficiency")
            && getDoubleCalculationValue("pct_read_efficiency") < 90)
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Recommendations", 
                "InnoDB Read Buffer Effficiency",
                percentToString(getCalculation("pct_read_efficiency"))
                    + "% ("
                    + doubleToString(getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
                    + " hits/ "
                    + getStatus("Innodb_buffer_pool_read_requests")
                    + " total)",
                "InnoDB Read buffer efficiency: "
                    + percentToString(getCalculation("pct_read_efficiency"))
                    + "% ("
                    + doubleToString(getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
                    + " hits/ "
                    + getStatus("Innodb_buffer_pool_read_requests")
                    + " total)",
                "InnoDB Read Buffer Effficiency: The ratio of Innodb_pages_written / Innodb_buffer_pool_write_requests"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "Recommendations", 
                "InnoDB Read Buffer Effficiency",
                percentToString(getCalculation("pct_read_efficiency"))
                    + "% ("
                    + doubleToString(getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
                    + " hits/ "
                    + getStatus("Innodb_buffer_pool_read_requests")
                    + " total)",
                "InnoDB Read buffer efficiency: "
                    + percentToString(getCalculation("pct_read_efficiency"))
                    + "% ("
                    + doubleToString(getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
                    + " hits/ "
                    + getStatus("Innodb_buffer_pool_read_requests")
                    + " total)",
                "InnoDB Read Buffer Effficiency: The ratio of Innodb_pages_written / Innodb_buffer_pool_write_requests"));
        }
        
//        // InnoDB Write efficency
//        if (containsCalculation("pct_write_efficiency")
//            && getDoubleCalculationValue("pct_write_efficiency") < 90)
//        {
//            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
//                "Recommendations", 
//                "InnoDB Write Buffer Effficiency",
//                getCalculation("pct_read_efficiency") 
//                    + "% ("
//                    + (getDoubleStatusValue("Innodb_pages_written"))
//                    + " hits/ "
//                    + getStatus("Innodb_buffer_pool_read_requests")
//                    + " total)",
//                "InnoDB Write buffer efficiency: "
//                    + getCalculation("pct_read_efficiency") 
//                    + "% ("
//                    + (getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
//                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
//                    + " hits/ "
//                    + getStatus("Innodb_buffer_pool_read_requests")
//                    + " total)",
//                "InnoDB Write Buffer Effficiency: the ratio of Innodb_pages_written / Innodb_buffer_pool_read_requests"));
//        }
//        else {
//            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
//                "Recommendations", 
//                "InnoDB Read Buffer Effficiency",
//                getCalculation("pct_read_efficiency") 
//                    + "% ("
//                    + (getDoubleStatusValue("Innodb_buffer_pool_write_requests") -
//                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
//                    + " hits/ "
//                    + getStatus("Innodb_buffer_pool_read_requests")
//                    + " total)",
//                "InnoDB Read buffer efficiency: "
//                    + getCalculation("pct_read_efficiency") 
//                    + "% ("
//                    + (getDoubleStatusValue("Innodb_buffer_pool_read_requests") -
//                       getDoubleStatusValue("Innodb_buffer_pool_reads"))
//                    + " hits/ "
//                    + getStatus("Innodb_buffer_pool_read_requests")
//                    + " total)",
//                "InnoDB Write Buffer Effficiency: the ratio of Innodb_pages_written / Innodb_buffer_pool_read_requests"));
//        }

        // InnoDB Write log efficiency
        if (containsCalculation("pct_write_efficiency")
            && getDoubleCalculationValue("pct_write_efficiency") < 90 )
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Recommendations", 
                "InnoDB Write Log Efficiency",
                percentToString(Math.abs(getDoubleCalculationValue("pct_write_efficiency")))
                    + "% ("
                    + doubleToString(Math.abs(getDoubleStatusValue("Innodb_log_write_requests") -
                               getDoubleStatusValue("Innodb_log_writes")))
                    + " hits/ "
                    + getStatus("Innodb_log_write_requests")
                    + " total)",
                "InnoDB Write Log efficiency: "
                    + percentToString(Math.abs(getDoubleCalculationValue("pct_write_efficiency")))
                    + "% ("
                    + doubleToString(Math.abs(getDoubleStatusValue("Innodb_log_write_requests") -
                               getDoubleStatusValue("Innodb_log_writes")))
                    + " hits/ "
                    + getStatus("Innodb_log_write_requests")
                    + " total)",
                "InnoDB Write Log Efficiency:(Innodb_log_write_requests-Innodb_log_writes)/Innodb_log_write_requests"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "Recommendations", 
                "InnoDB Write Log Efficiency",
                percentToString(getCalculation("pct_write_efficiency"))
                    + "% ("
                    + doubleToString(getDoubleStatusValue("Innodb_log_write_requests") -
                       getDoubleStatusValue("Innodb_log_writes") )
                    + " hits/ "
                    + getStatus("Innodb_log_write_requests")
                    + " total)",
                "InnoDB Write log efficiency: "
                    + percentToString(getCalculation("pct_write_efficiency"))
                    + "% ("
                    + doubleToString(getDoubleStatusValue("Innodb_log_write_requests") -
                       getDoubleStatusValue("Innodb_log_writes") )
                    + " hits/ "
                    + getStatus("Innodb_log_write_requests")
                    + " total)",
                "InnoDB Write Log Efficiency:(Innodb_log_write_requests-Innodb_log_writes)/Innodb_log_write_requests"));
        }
        
        // InnoDB Log Waits
        if (containsStatus("Innodb_log_waits")
            && getDoubleStatusValue("Innodb_log_waits") > 0 )
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD,
                "Recommendations", 
                "InnoDB Log Waits",
                percentage(getDoubleStatusValue("Innodb_log_waits"), getDoubleStatusValue("Innodb_log_writes"))
                    + "% ("
                    + getStatus("Innodb_log_waits")
                    + " waits / "
                    + getStatus("Innodb_log_writes")
                    + " writes)",
                "InnoDB log waits: "
                    + percentage(getDoubleStatusValue("Innodb_log_waits"), getDoubleStatusValue("Innodb_log_writes"))
                    + "% ("
                    + getStatus("Innodb_log_waits")
                    + " waits / "
                    + getStatus("Innodb_log_writes")
                    + " writes)",
                "InnoDB Log Waits: the ratio of Innodb_log_waits/Innodb_log_writes"));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.GOOD,
                "Recommendations", 
                "InnoDB Log Waits",
                percentage(getDoubleStatusValue("Innodb_log_waits"), getDoubleStatusValue("Innodb_log_writes"))
                    + "% ("
                    + getStatus("Innodb_log_waits")
                    + " waits / "
                    + getStatus("Innodb_log_writes")
                    + " writes)",
                "InnoDB log waits: "
                    + percentage(getDoubleStatusValue("Innodb_log_waits"), getDoubleStatusValue("Innodb_log_writes"))
                    + "% ("
                    + getStatus("Innodb_log_waits")
                    + " waits / "
                    + getStatus("Innodb_log_writes")
                    + " writes)",
                "InnoDB Log Waits: the ratio of Innodb_log_waits/Innodb_log_writes"));
        }
        
        // Performance options
        if (!mysqlVersion(session, 5, 1)) {
            generalRecords.add("Upgrade to MySQL 5.5+ to use asynchronous write" );
        }
        else if ("OFF".equalsIgnoreCase(getVariable("concurrent_insert"))) {
            generalRecords.add("Enable concurrent_insert by setting it to 'ON'");
        }
        else if (getDoubleVariableValue("concurrent_insert") == 0 ) {
            generalRecords.add("Enable concurrent_insert by setting it to 1");
        }
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" mysql_stats (Recommendations for Performance Schema) "> 
        
    private  void mysqlPfs(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {
                
        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Performance schema", tunerResult);

        // Performance Schema
        if (!containsVariable("performance_schema")) {
            putVariable("performance_schema", "OFF");
        }
        
        if (!"ON".equalsIgnoreCase(getVariable("performance_schema"))) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Performance schema is disabled."));
            
            if (mysqlVersion(session, 5, 6)) {
                generalRecords.add("Performance should be activated for better diagnostics");
                adjvars.add("performance_schema = ON enable PFS");
            }
        } else {
             if (!mysqlVersion(session, 5, 5)) {
                generalRecords.add("Performance shouldn't be activated for MySQL and MariaDB 5.5 and lower version");
                adjvars.add("performance_schema = OFF disable PFS");
            }
        }
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Memory used by P_S: " + bytes(getPfMemory())));

        if (!((List<String>)((Map<String, Object>)result.get("Databases")).get("List")).contains("sys")) {
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Sys schema isn't installed."));
            return;
        } else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Sys schema is installed."));
        }

        if (!"ON".equals(getVariable("performance_schema"))) {
            return;
        }

        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Sys schema Version: " + selectOne("select sys_version from sys.version")));

        // Top user per connection
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per connection",
            "No information found or indicators desactivated.",
            "select user, total_connections from sys.user_summary order by total_connections desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " conn(s)";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );    

        // Top user per statement
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per statement",
            "No information found or indicators desactivated.",
            "select user, statements from sys.user_summary order by statements desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " stmt(s)";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
   
        // Top user per statement latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per statement latency",
            "No information found or indicators desactivated.",
            "select user, statement_avg_latency from sys.user_summary order by statement_avg_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top user per lock latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per lock latency",
            "No information found or indicators desactivated.",
            "select user, lock_latency from sys.user_summary_by_statement_latency order by lock_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top user per full scans
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per nb full scans",
            "No information found or indicators desactivated.",
            "select user, full_scans from sys.user_summary_by_statement_latency order by full_scans desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top user per row_sent
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per rows sent",
            "No information found or indicators desactivated.",
            "select user, rows_sent from sys.user_summary_by_statement_latency order by rows_sent desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top user per row modified
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per rows modified",
            "No information found or indicators desactivated.",
            "select user, rows_affected from sys.user_summary_by_statement_latency order by rows_affected desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top user per io
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per io",
            "No information found or indicators desactivated.",
            "select user, file_ios from sys.user_summary order by file_ios desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top user per io latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 user per io latency",
            "No information found or indicators desactivated.",
            "select user, file_io_latency from sys.user_summary order by file_io_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per connection
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per connection",
            "No information found or indicators desactivated.",
            "select host, total_connections from sys.host_summary order by total_connections desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " conn(s)";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per statement
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per statement",
            "No information found or indicators desactivated.",
            "select host, statements from sys.host_summary order by statements desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " stmt(s)";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per statement latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per statement latency",
            "No information found or indicators desactivated.",
            "select host, statement_avg_latency from sys.host_summary order by statement_avg_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per lock latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per lock latency",
            "No information found or indicators desactivated.",
            "select host, lock_latency from sys.host_summary_by_statement_latency order by lock_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per full scans
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per nb full scans",
            "No information found or indicators desactivated.",
            "select host, full_scans from sys.host_summary_by_statement_latency order by full_scans desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per rows sent
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per rows sent",
            "No information found or indicators desactivated.",
            "select host, rows_sent from sys.host_summary_by_statement_latency order by rows_sent desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per rows modified
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per rows modified",
            "No information found or indicators desactivated.",
            "select host, rows_affected from sys.host_summary_by_statement_latency order by rows_affected desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top host per io
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per io",
            "No information found or indicators desactivated.",
            "select host, file_ios from sys.host_summary order by file_ios desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top 5 host per io latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per io latency",
            "No information found or indicators desactivated.",
            "select host, file_io_latency from sys.host_summary order by file_io_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top IO type order by total io
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top IO type order by total io",
            "No information found or indicators desactivated.",
            "select substring(event_name,14), SUM(total) AS total from sys.host_summary_by_file_io_type GROUP BY substring(event_name,14) ORDER BY total DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " i/o";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top IO type order by total latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top IO type order by total latency",
            "No information found or indicators desactivated.",
            "select substring(event_name,14), sys.format_time(ROUND(SUM(total_latency),1)) AS total_latency from sys.host_summary_by_file_io_type GROUP BY substring(event_name,14) ORDER BY total_latency DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top IO type order by max latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top IO type order by max latency",
            "No information found or indicators desactivated.",
            "select substring(event_name,14), MAX(max_latency) as max_latency from sys.host_summary_by_file_io_type GROUP BY substring(event_name,14) ORDER BY max_latency DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top Stages order by total io
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top Stages order by total io",
            "No information found or indicators desactivated.",
            "select substring(event_name,7), SUM(total)AS total from sys.host_summary_by_stages GROUP BY substring(event_name,7) ORDER BY total DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " i/o";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Top Stages order by total latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top Stages order by total latency",
            "No information found or indicators desactivated.",
            "select substring(event_name,7), sys.format_time(ROUND(SUM(total_latency),1)) AS total_latency from sys.host_summary_by_stages GROUP BY substring(event_name,7) ORDER BY total_latency DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top Stages order by avg latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top Stages order by avg latency",
            "No information found or indicators desactivated.",
            "select substring(event_name,7), MAX(avg_latency) as avg_latency from sys.host_summary_by_stages GROUP BY substring(event_name,7) ORDER BY avg_latency DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );        

        // Top host per table scans
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 host per table scans",
            "No information found or indicators desactivated.",
            "select host, table_scans from sys.host_summary order by table_scans desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // InnoDB Buffer Pool by schema
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: InnoDB Buffer Pool by schema",
            "No information found or indicators desactivated.",
            "select object_schema, allocated, data, pages from sys.innodb_buffer_stats_by_schema ORDER BY pages DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
    
        // InnoDB Buffer Pool by table
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: InnoDB Buffer Pool by table",
            "No information found or indicators desactivated.",
            "select CONCAT(object_schema,CONCAT('.', object_name)), allocated, data, pages from sys.innodb_buffer_stats_by_table ORDER BY pages DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " page(s)";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Process per allocated memory
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Process per allocated memory",
            "No information found or indicators desactivated.",
            "select concat(user,concat('/', IFNULL(Command,'NONE'))) AS PROC, current_memory from sys.processlist ORDER BY current_memory DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // InnoDB Lock Waits
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: InnoDB Lock Waits",
            "No information found or indicators desactivated.",
            "select wait_age_secs, locked_table, locked_type, waiting_query from sys.innodb_lock_waits order by wait_age_secs DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Threads IO Latency
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Thread IO Latency",
            "No information found or indicators desactivated.",
            "select user, total_latency, max_latency from sys.io_by_thread_by_latency order by total_latency",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
    
        // High Cost SQL statements
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 Most latency statements",
            "No information found or indicators desactivated.",
            "select query, avg_latency from sys.statement_analysis order by avg_latency desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top 5% slower queries
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 5 slower queries",
            "No information found or indicators desactivated.",
            "select query, exec_count from sys.statements_with_runtimes_in_95th_percentile order by exec_count desc LIMIT 5",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " s";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

        // Top 10 nb statement type
        printCheckingForMysqslPfs(
            tunerResult,
            "Performance schema: Top 10 nb statement type",
            "No information found or indicators desactivated.",
            "select statement, sum(total) as total from sys.host_summary_by_statement_type group by statement order by total desc LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " s";
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Top statement by total latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top statement by total latency",
            "No information found or indicators desactivated.",
            "select statement, sum(total_latency) as total from sys.host_summary_by_statement_type group by statement order by total desc LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Top statement by lock latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top statement by lock latency",
            "No information found or indicators desactivated.",
            "select statement, sum(lock_latency) as total from sys.host_summary_by_statement_type group by statement order by total desc LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Top statement by full scans
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top statement by full scans",
            "No information found or indicators desactivated.",
            "select statement, sum(full_scans) as total from sys.host_summary_by_statement_type group by statement order by total desc LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Top statement by rows sent
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top statement by rows sent",
            "No information found or indicators desactivated.",
            "select statement, sum(rows_sent) as total from sys.host_summary_by_statement_type group by statement order by total desc LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Top statement by rows modified
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top statement by rows modified",
            "No information found or indicators desactivated.",
            "select statement, sum(rows_affected) as total from sys.host_summary_by_statement_type group by statement order by total desc LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Use temporary tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Some queries using temp table",
            "No information found or indicators desactivated.",
            "select query from sys.statements_with_temp_tables LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Unused Indexes
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Unused indexes",
            "No information found or indicators desactivated.",
            "select * from sys.schema_unused_indexes",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Full table scans
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Tables with full table scans",
            "No information found or indicators desactivated.",
            "select * from sys.schema_tables_with_full_table_scans order by rows_full_scanned DESC",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Latest file IO by latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Latest FILE IO by latency",
            "No information found or indicators desactivated.",
            "select thread, file, latency, operation from sys.latest_file_io ORDER BY latency LIMIT 10",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // FILE by IO read bytes
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: FILE by IO read bytes",
            "No information found or indicators desactivated.",
            "(select file, total_read from sys.io_global_by_file_by_bytes where total_read like '%MiB' order by total_read DESC) UNION (select file, total_read from sys.io_global_by_file_by_bytes where total_read like '%KiB' order by total_read DESC LIMIT 15)",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // FILE by IO written bytes
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: FILE by IO written bytes",
            "No information found or indicators desactivated.",
            "(select file, total_written from sys.io_global_by_file_by_bytes where total_written like '%MiB' order by total_written DESC) UNION (select file, total_written from sys.io_global_by_file_by_bytes where total_written like '%KiB' order by total_written DESC LIMIT 15)",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // file per IO total latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: file per IO total latency",
            "No information found or indicators desactivated.",
            "select file, total_latency from sys.io_global_by_file_by_latency ORDER BY total_latency DESC LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // file per IO read latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: file per IO read latency",
            "No information found or indicators desactivated.",
            "select file, read_latency from sys.io_global_by_file_by_latency ORDER BY read_latency DESC LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // file per IO write latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: file per IO write latency",
            "No information found or indicators desactivated.",
            "select file, write_latency from sys.io_global_by_file_by_latency ORDER BY write_latency DESC LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Event Wait by read bytes
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Event Wait by read bytes",
            "No information found or indicators desactivated.",
            "(select event_name, total_read from sys.io_global_by_wait_by_bytes where total_read like '%MiB' order by total_read DESC) UNION (select event_name, total_read from sys.io_global_by_wait_by_bytes where total_read like '%KiB' order by total_read DESC LIMIT 15)",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Event Wait by write bytes
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Event Wait written bytes",
            "No information found or indicators desactivated.",
            "(select event_name, total_written from sys.io_global_by_wait_by_bytes where total_written like '%MiB' order by total_written DESC) UNION (select event_name, total_written from sys.io_global_by_wait_by_bytes where total_written like '%KiB' order by total_written DESC LIMIT 15)",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // event per wait total latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: event per wait total latency",
            "No information found or indicators desactivated.",
            "select event_name, total_latency from sys.io_global_by_wait_by_latency ORDER BY total_latency DESC LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // event per wait read latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: event per wait read latency",
            "No information found or indicators desactivated.",
            "select event_name, read_latency from sys.io_global_by_wait_by_latency ORDER BY read_latency DESC LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // event per wait write latency
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: event per wait write latency",
            "No information found or indicators desactivated.",
            "select event_name, write_latency from sys.io_global_by_wait_by_latency ORDER BY write_latency DESC LIMIT 20",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 most read index
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 most read indexes",
            "No information found or indicators desactivated.",
            "select table_schema, table_name,index_name, rows_selected from sys.schema_index_statistics ORDER BY ROWs_selected DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 most used index
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 most modified indexes",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, index_name, rows_inserted+rows_updated+rows_deleted AS changes from sys.schema_index_statistics ORDER BY rows_inserted+rows_updated+rows_deleted DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high read latency index
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high read latency index",
            "No information found or indicators desactivated.",
            "select table_schema, table_name,index_name, select_latency from sys.schema_index_statistics ORDER BY select_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high insert latency index
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 most modified indexes",
            "No information found or indicators desactivated.",
            "select table_schema, table_name,index_name, insert_latency from sys.schema_index_statistics ORDER BY insert_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high update latency index
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high update latency index",
            "No information found or indicators desactivated.",
            "select table_schema, table_name,index_name, update_latency from sys.schema_index_statistics ORDER BY update_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high delete latency index
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high delete latency index",
            "No information found or indicators desactivated.",
            "select table_schema, table_name,index_name, delete_latency from sys.schema_index_statistics ORDER BY delete_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 most read tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 most read tables",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, rows_fetched from sys.schema_table_statistics ORDER BY ROWs_fetched DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 most used tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 most modified tables",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, rows_inserted+rows_updated+rows_deleted AS changes from sys.schema_table_statistics ORDER BY rows_inserted+rows_updated+rows_deleted DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high read latency tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high read latency tables",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, fetch_latency from sys.schema_table_statistics ORDER BY fetch_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high insert latency tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high insert latency tables",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, insert_latency from sys.schema_table_statistics ORDER BY insert_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high update latency tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high update latency tables",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, update_latency from sys.schema_table_statistics ORDER BY update_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // TOP 15 high delete latency tables
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 high delete latency tables",
            "No information found or indicators desactivated.",
            "select table_schema, table_name, delete_latency from sys.schema_table_statistics ORDER BY delete_latency DESC LIMIT 15",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        // Redundant indexes
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Redundant indexes",
            "No information found or indicators desactivated.",
            "select * from sys.schema_redundant_indexes",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7) + " " + rs.getString(8) + " " + rs.getString(9) + " " + rs.getString(10);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Tables not using InnoDB buffer",
            "No information found or indicators desactivated.",
            "Select table_schema, table_name from sys.schema_table_statistics_with_buffer where innodb_buffer_allocated IS NULL;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
                
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Table not using InnoDB buffer",
            "No information found or indicators desactivated.",
            "Select table_schema, table_name from sys.schema_table_statistics_with_buffer where innodb_buffer_allocated IS NULL;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top 15 Tables using InnoDB buffer",
            "No information found or indicators desactivated.",
            "select table_schema,table_name,innodb_buffer_allocated from sys.schema_table_statistics_with_buffer where innodb_buffer_allocated IS NOT NULL ORDER BY innodb_buffer_allocated DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top 15 Tables with InnoDB buffer free",
            "No information found or indicators desactivated.",
            "select table_schema,table_name,innodb_buffer_free from sys.schema_table_statistics_with_buffer where innodb_buffer_allocated IS NOT NULL ORDER BY innodb_buffer_free DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top 15 Most executed queries",
            "No information found or indicators desactivated.",
            "select db, query, exec_count from sys.statement_analysis order by exec_count DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Latest SQL queries in errors or warnings",
            "No information found or indicators desactivated.",
            "select query, last_seen from sys.statements_with_errors_or_warnings ORDER BY last_seen LIMIT 100;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top 20 queries with full table scans",
            "No information found or indicators desactivated.",
            "select db, query, exec_count from sys.statements_with_full_table_scans order BY exec_count DESC LIMIT 20;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Last 50 queries with full table scans",
            "No information found or indicators desactivated.",
            "select db, query, last_seen from sys.statements_with_full_table_scans order BY last_seen DESC LIMIT 50;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 reader queries (95% percentile)",
            "No information found or indicators desactivated.",
            "select db, query, rows_sent from sys.statements_with_runtimes_in_95th_percentile ORDER BY ROWs_sent DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 most row look queries (95% percentile)",
            "No information found or indicators desactivated.",
            "select db, query, rows_examined AS search from sys.statements_with_runtimes_in_95th_percentile ORDER BY rows_examined DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 total latency queries (95% percentile)",
            "No information found or indicators desactivated.",
            "select db, query, total_latency AS search from sys.statements_with_runtimes_in_95th_percentile ORDER BY total_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 max latency queries (95% percentile)",
            "No information found or indicators desactivated.",
            "select db, query, max_latency AS search from sys.statements_with_runtimes_in_95th_percentile ORDER BY max_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 average latency queries (95% percentile)",
            "No information found or indicators desactivated.",
            "select db, query, avg_latency AS search from sys.statements_with_runtimes_in_95th_percentile ORDER BY avg_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top 20 queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, exec_count from sys.statements_with_sorting order BY exec_count DESC LIMIT 20;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Last 50 queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, last_seen from sys.statements_with_sorting order BY last_seen DESC LIMIT 50;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 row sorting queries with sort",
            "No information found or indicators desactivated.",
            "select db, query , rows_sorted from sys.statements_with_sorting ORDER BY ROWs_sorted DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 total latency queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, total_latency AS search from sys.statements_with_sorting ORDER BY total_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 merge queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, sort_merge_passes AS search from sys.statements_with_sorting ORDER BY sort_merge_passes DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 average sort merges queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, avg_sort_merges AS search from sys.statements_with_sorting ORDER BY avg_sort_merges DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 scans queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, sorts_using_scans AS search from sys.statements_with_sorting ORDER BY sorts_using_scans DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 range queries with sort",
            "No information found or indicators desactivated.",
            "select db, query, sort_using_range AS search from sys.statements_with_sorting ORDER BY sort_using_range DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Top 20 queries with temp table",
            "No information found or indicators desactivated.",
            "select db, query, exec_count from sys.statements_with_temp_tables order BY exec_count DESC LIMIT 20;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: Last 50 queries with temp table",
            "No information found or indicators desactivated.",
            "select db, query, last_seen from sys.statements_with_temp_tables order BY last_seen DESC LIMIT 50;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 total latency queries with temp table",
            "No information found or indicators desactivated.",
            "select db, query, total_latency AS search from sys.statements_with_temp_tables ORDER BY total_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 queries with temp table to disk",
            "No information found or indicators desactivated.",
            "select db, query, disk_tmp_tables from sys.statements_with_temp_tables ORDER BY disk_tmp_tables DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 class events by number",
            "No information found or indicators desactivated.",
            "select event_class, total from sys.wait_classes_global_by_latency ORDER BY total DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 30 events by number",
            "No information found or indicators desactivated.",
            "select events, total from sys.waits_global_by_latency ORDER BY total DESC LIMIT 30;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 class events by total latency",
            "No information found or indicators desactivated.",
            "select event_class, total_latency from sys.wait_classes_global_by_latency ORDER BY total_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 30 events by total latency",
            "No information found or indicators desactivated.",
            "select events, total_latency from sys.waits_global_by_latency ORDER BY total_latency DESC LIMIT 30;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 15 class events by max latency",
            "No information found or indicators desactivated.",
            "select event_class, max_latency from sys.wait_classes_global_by_latency ORDER BY max_latency DESC LIMIT 15;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );
        
        printCheckingForMysqslPfs(
             tunerResult,
            "Performance schema: TOP 30 events by max latency",
            "No information found or indicators desactivated.",
            "select events, max_latency from sys.waits_global_by_latency ORDER BY max_latency DESC LIMIT 30;",
            (index, rs) -> {
                try {
                    return " +-- " + index + ": " + rs.getString(1) + " " + rs.getString(2);
                } catch (SQLException ex) {
                    log.error("Error on 'printCheckingForMysqslPfs'", ex);
                }
                return null;
            }
        );

}
    
    private  void printCheckingForMysqslPfs(List<TunerResult> tunerResult, String title, String emptyMessage, String query, BiFunction<Integer, ResultSet, String> printFunction) {
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, title));

        int nbL = 1;
        try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
            if (rs != null) {
                while (rs.next()) {
                    String r = printFunction.apply(nbL, rs);
                    if (r != null) {
                        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, r));
                        nbL++;
                    } else {
                        break;
                    }
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'selectOne'", ex);
        }

        if (nbL == 1) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, " " + emptyMessage));
        }
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" mysql_stats (Recommendations for Performance Schema) "> 
        
    private  void getReplicationStatus(List<String> generalRecords, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {

        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Replication Metrics", tunerResult);
        
        tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
            "Galera Synchronous replication: " + getVariable("have_galera"),
            "Galera replication happens at transaction commit time by broadcasting the transaction write set to the cluster for applying. The client connects directly to the DBMS and experiences close to native DBMS behavior."));
        
        int salvesCount = getMySlaves().size();
        if (salvesCount == 0 ) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "No replication slave(s) for this server."));
        }
        else {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                "This server is acting as master for "
              + salvesCount
              + " server(s)."));
        }
        
        if (mysqlVersion(session, 5, 6)) {
            Double slave_parallel_workers = containsVariable("slave_parallel_workers") ? getDoubleVariableValue("slave_parallel_workers") : 0;
            TunerResult.Status status = TunerResult.Status.BAD;
            if (slave_parallel_workers > 4) {
                status = TunerResult.Status.GOOD;
            } else if (slave_parallel_workers == 0) {
                status = TunerResult.Status.NEUTRAL;
            }
            
            tunerResult.add(new TunerResult(
                status, 
                "slave_parallel_workers",
                "Sets the number of slave worker threads for executing replication events (transactions) in parallel. Setting this variable to 0 (the default) disables parallel execution. The maximum is 1024."));
        }
        
        if (containsVariable("slave_parallel_type")) {
            TunerResult.Status status = TunerResult.Status.NEUTRAL;
            if ("DATABASE".equalsIgnoreCase(getVariable("slave_parallel_type"))) {
                status = TunerResult.Status.GOOD;
            }
            
            tunerResult.add(new TunerResult(
                status, 
                "slave_parallel_type",
                "When using a multi-threaded slave (slave_parallel_workers is greater than 0), this option specifies the policy used to decide which transactions are allowed to execute in parallel on the slave. The possible values are: 1)  DATABASE: Transactions that update different databases are applied in parallel. This value is only appropriate if data is partitioned into multiple databases which are being updated independently and concurrently on the master. 2) LOGICAL_CLOCK: Transactions that are part of the same binary log group commit on a master are applied in parallel on a slave. There are no cross-database constraints, and data does not need to be partitioned into multiple databases."));
        }
        
        Double innodb_flush_log_at_trx_commit = containsVariable("innodb_flush_log_at_trx_commit") ? getDoubleVariableValue("innodb_flush_log_at_trx_commit") : 0;
        TunerResult.Status status = TunerResult.Status.NEUTRAL;
        if (innodb_flush_log_at_trx_commit == 2) {
            status = TunerResult.Status.GOOD;
        }

        tunerResult.add(new TunerResult(
            status, 
            "slave_parallel_workers",
            "You can achieve better performance by changing the default value but then you can lose up to a second of transactions in a crash."));
        
        Map<String, String> myrepl = getMyRepl();

        if (myrepl.isEmpty() && salvesCount == 0) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "This is a standalone server."));
            return;
        }
        
        if (myrepl.isEmpty()) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "No replication setup for this server."));
            return;
        }
        
        Map<String, Object> m = (Map<String, Object>)result.get("Replication");
        if (m == null) {
            m = new HashMap<>();
            result.put("Replication", m);
        }
        
        m.put("status", myrepl);
        
        String io_running = myrepl.get("Slave_IO_Running");
        String sql_running = myrepl.get("Slave_SQL_Running");
        Double seconds_behind_master = 0.0;
        try {
            seconds_behind_master = Double.valueOf(myrepl.get("Seconds_Behind_Master"));
        } catch (Throwable th) {
        }
        
        if (io_running != null && ("yes".equalsIgnoreCase(io_running) || "yes".equalsIgnoreCase(sql_running)))
        {
            tunerResult.add(new TunerResult(TunerResult.Status.BAD, "This replication slave is not running but seems to be configured."));
        }
        
        if (io_running != null
            && "yes".equalsIgnoreCase(io_running)
            && "yes".equalsIgnoreCase(sql_running))
        {
            if ("OFF".equalsIgnoreCase(getVariable("read_only"))) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "This replication slave is running with the read_only option disabled."));
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "This replication slave is running with the read_only option enabled."));
            }
            if (seconds_behind_master > 0 ) {
                tunerResult.add(new TunerResult(TunerResult.Status.BAD, "This replication slave is lagging and slave has " + doubleToString(seconds_behind_master) + " second(s) behind master host."));
            }
            else {
                tunerResult.add(new TunerResult(TunerResult.Status.GOOD, "This replication slave is up to date with master."));
            }
        }
    }
    
    // </editor-fold>
     
    
    // <editor-fold defaultstate="collapsed" desc=" make_recommendations (Make recommendations based on stats) "> 
        
    private  void makeRecommendations(List<String> generalrec, List<String> adjvars, Map<String, Object> result, Map<String, List<TunerResult>> tunerMap) {

        List<TunerResult> tunerResult = new ArrayList<>();
        tunerMap.put("Recommendations", tunerResult);
        
        result.put("Recommendations", generalrec);
        result.put("Adjust variables", adjvars);
        
        if (!generalrec.isEmpty()) {            
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "General recommendations:"));
            for(String s: generalrec) {
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "    " + s)); 
            }
        }
        
        if (!adjvars.isEmpty()) {            
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "Variables to adjust:"));
            
            if (getDoubleCalculationValue("pct_max_physical_memory") > 90 ) {
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                  "  *** MySQL's maximum memory usage is dangerously high ***"));
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, 
                  "  *** Add RAM before increasing MySQL buffer variables ***"));
            }
            
            for(String s: adjvars) {
                tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "    " + s)); 
            }
        }
        
        if (generalrec.isEmpty() && adjvars.isEmpty()) {
            tunerResult.add(new TunerResult(TunerResult.Status.NEUTRAL, "No additional performance recommendations are available."));
        }
    }
    
    // </editor-fold>
    
    
    private  Map<String, String> getMySlaves() {
        Map<String, String> m = new HashMap<>();
        try (ResultSet rs = (ResultSet) session.getService().execute("SHOW SLAVE HOSTS")) {
            if (rs != null) {
                while (rs.next()) {
                    m.put(rs.getString(1), rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5));
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'selectOne'", ex);
        }
        
        return m;
    }
    
    private  Map<String, String> getMyRepl() {
        Map<String, String> m = new HashMap<>();
        try (ResultSet rs = (ResultSet) session.getService().execute("SHOW SLAVE STATUS")) {
            if (rs != null) {

                while (rs.next()) {
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        m.put(rs.getMetaData().getColumnName(i), rs.getString(i));
                    }
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'selectOne'", ex);
        }
        
        return m;
    }  
       
    private  String doubleToString(Object value) {
        try {
            return new BigDecimal(value.toString()).setScale(1, RoundingMode.HALF_UP).toString();
        } catch (Throwable th) {
            return value != null ? value.toString() : "";
        }
    }
    
    private  String percentToString(Object value) {
        try {
            return new BigDecimal(value.toString()).setScale(2, RoundingMode.HALF_UP).toString();
        } catch (Throwable th) {
            return value != null ? value.toString() : "";
        }
    }
       
    private  Long getOtherProcessMemory() {
        	 	
        // TODO Check this
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        
        return hal.getMemory().getTotal() - hal.getMemory().getAvailable();
    }
    
    
    public String selectOne(String query) {
        try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
            if (rs != null && rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            log.error("Error on 'selectOne'", ex);
        }
        
        return null;
    }
    
    public Double getDoubleValue(Map<String, Object> m, String key) {
        try {
            return Double.valueOf(m.get(key).toString());
        } catch (Throwable ex) {
            //log.debug("MySQLTuner: ", ex);
            return 0.0;
        }
    }
    
    public Double getDoubleCalculationValue(String key) {
        try {
            return Double.valueOf(getCalculation(key).toString());
        } catch (Throwable ex) {
            //log.debug("MySQLTuner: ", ex);
            return 0.0;
        }
    }
    
    public Double getDoubleVariableValue(String key) {
        try {
            return Double.valueOf(getVariable(key).toString());
        } catch (Throwable ex) {
            //log.debug("MySQLTuner: ", ex);
            return 0.0;
        }
    }
    
    public Double getDoubleStatusValue(String key) {
        try {
            return Double.valueOf(getStatus(key).toString());
        } catch (Throwable ex) {
            //log.debug("MySQLTuner: ", ex);
            return 0.0;
        }
    }
      
    public Long getPfMemory() {

        // Performance Schema
        String value = getVariable("performance_schema");
        if (value == null || "OFF".equals(value)) {
            return 0L;
        }
        
        try (ResultSet rs = (ResultSet) session.getService().execute(TUNER__SELECT_PS_STATUS)) {
            if (rs != null) {
                while (rs.next()) {
                    if ("performance_schema.memory".equals(rs.getString(2))) {
                        return rs.getLong(3);
                    }
                }
            }
        } catch (SQLException ex) {
            log.error("Error on 'securityRecommendations'", ex);
        }
        
        return 0L;
    }
    
    
    public Long getGcacheMemory() {
        Long gCacheMem = hrRaw(getWsrepOption("gcache.size"));        
        return gCacheMem == null ? 0L : gCacheMem;
    }

    public String getWsrepOption(String key) {
        
        if (!containsVariable("wsrep_provider_options")) {
            return "";
        }
        
        List<String> galeraOptions = getWsrepOptions();
        if (galeraOptions.isEmpty()) {
            return "";
        }
        
        for (String s: galeraOptions) {
            String[] strs = s.split("=");
            if (key.equalsIgnoreCase(strs[0])) {
                String memValue = strs[1].trim();
                if (memValue.isEmpty()) {
                    return "";
                }

                return memValue;
            }
        }
        
        return "";
    }
        
    public List<String> getWsrepOptions() {
        if (!containsVariable("wsrep_provider_options")) {
            return new ArrayList<>();
        }
        
        List<String> galeraOptions = Arrays.asList(getVariable("wsrep_provider_options").split(";"));

        String wsrepSlaveThreads = getVariable("wsrep_slave_threads");
        galeraOptions.add(" wsrep_slave_threads = " + wsrepSlaveThreads);
        
        List<String> result = new ArrayList<>();
        for (String s: galeraOptions) {
            if (!s.trim().isEmpty()) {
                result.add(s.trim());
            }
        }
        
        return result;
    }
    
    public Long hrRaw(String num) {
        if (num == null || num.isEmpty() || "NULL".equalsIgnoreCase(num)) {
            return 0L;
        }
        
        Matcher m = PATTERN_HR_RAW__G.matcher(num);
        if (m.find()) {
            return Long.valueOf(m.group(1)) * 1024 * 1024 * 1024;
        }
        
        m = PATTERN_HR_RAW__M.matcher(num);
        if (m.find()) {
            return Long.valueOf(m.group(1)) * 1024 * 1024;
        }
        
        m = PATTERN_HR_RAW__K.matcher(num);
        if (m.find()) {
            return Long.valueOf(m.group(1)) * 1024;
        }
        
        m = PATTERN_HR_RAW.matcher(num);
        if (m.find()) {
            return Long.valueOf(m.group(1));
        }
        
        return 0L;
    }
                    
    public  Long getPhysicalMemory() {
        return totalMemory;
    }
    
//    mysql_tables;              # Show informations about table column
//
//    mysql_indexes;             # Show informations about indexes

//    cve_recommendations;       # Display related CVE
//    calculations;              # Calculate everything we need
//    mysql_stats;               # Print the server stats
//    mysqsl_pfs;                # Print Performance schema info
//    mariadb_threadpool;        # Print MaraiDB ThreadPool stats
//    mysql_myisam;              # Print MyISAM stats
//    mysql_innodb;              # Print InnoDB stats
//    mariadb_ariadb;            # Print MaraiDB AriaDB stats
//    mariadb_tokudb;            # Print MariaDB Tokudb stats
//    mariadb_xtradb;            # Print MariaDB XtraDB stats
//    mariadb_rockdb;            # Print MariaDB RockDB stats
//    mariadb_spider;            # Print MariaDB Spider stats
//    mariadb_connect;           # Print MariaDB Connect stats
//    mariadb_galera;            # Print MariaDB Galera Cluster stats
//    get_replication_status;    # Print replication info
//    make_recommendations;      # Make recommendations based on stats
//    dump_result;               # Dump result if debug is on
//    close_outputfile;          # Close reportfile if needed
    
    
}
