
package np.com.ngopal.smart.sql.structure.models;

import java.util.Date;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SlowQueryData implements FilterableData, GraphData {
    
    private final SimpleStringProperty user;
    private final SimpleStringProperty host;
    private final SimpleObjectProperty ptc;
    private final SimpleObjectProperty calls;
    private final SimpleObjectProperty avg;
    private final SimpleStringProperty database;
    private final SimpleStringProperty table;
    
    private final SimpleObjectProperty responceTime;
    private final SimpleObjectProperty minResponceTime;
    private final SimpleObjectProperty maxResponceTime;
    private final SimpleObjectProperty avgResponceTime;
    private final SimpleObjectProperty stddevResponceTime;
    
    private final SimpleObjectProperty lockTime;
    private final SimpleObjectProperty avgLockTime;
    
    private final SimpleObjectProperty rowsSent;
    private final SimpleObjectProperty avgRowsSent;
    
    private final SimpleObjectProperty rowsExamined;
    private final SimpleObjectProperty avgRowsExamined;
    
    private final SimpleStringProperty explain;
    private final SimpleStringProperty queryTemplate;
    
    private final SimpleObjectProperty queryRating;
    
    private final SimpleStringProperty graph1;
    private final SimpleStringProperty graph2;
    
    private final SimpleStringProperty recommendedIndex = new SimpleStringProperty();
    private final SimpleStringProperty recommendations = new SimpleStringProperty();
    
    private Date date;
    
    private Long rowsSum = null;
 
    public SlowQueryData(String user, String host, Double ptc, Long calls, Double avg, String database, String table, Double responceTime, Double minResponceTime, Double maxResponceTime, Double avgResponceTime, Double stddevResponceTime, Double lockTime, Double avgLockTime, Long rowsSent, Double avgRowsSent, Double rowsExamined, Double avgRowsExamined, String explain, String queryTemplate, Long queryRating, String graph1, String graph2) {
        this.user = new SimpleStringProperty(user);
        this.host = new SimpleStringProperty(host);
        this.ptc = new SimpleObjectProperty(ptc);
        this.calls = new SimpleObjectProperty(calls);
        this.avg = new SimpleObjectProperty(avg);
        this.database = new SimpleStringProperty(database);
        this.table = new SimpleStringProperty(table);
        
        this.responceTime = new SimpleObjectProperty(responceTime);
        this.minResponceTime = new SimpleObjectProperty(minResponceTime);
        this.maxResponceTime = new SimpleObjectProperty(maxResponceTime);
        this.avgResponceTime = new SimpleObjectProperty(avgResponceTime);
        this.stddevResponceTime = new SimpleObjectProperty(stddevResponceTime);
        
        this.lockTime = new SimpleObjectProperty(lockTime);
        this.avgLockTime = new SimpleObjectProperty(avgLockTime);
        
        this.rowsSent = new SimpleObjectProperty(rowsSent);
        this.avgRowsSent = new SimpleObjectProperty(avgRowsSent);
        
        this.rowsExamined = new SimpleObjectProperty(rowsExamined);
        this.avgRowsExamined = new SimpleObjectProperty(avgRowsExamined);
        
        this.explain = new SimpleStringProperty(explain);
        this.queryTemplate = new SimpleStringProperty(queryTemplate);
        
        this.queryRating = new SimpleObjectProperty(queryRating);
        
        this.graph1 = new SimpleStringProperty(graph1);
        this.graph2 = new SimpleStringProperty(graph2);
    }
 
    public void setDate(Date date) {
        this.date = date;
    }
 
    public void setRowsSum(Long rowsSum) {
        this.rowsSum = rowsSum;
    }
    public Long getRowsSum() {
        return rowsSum;
    }    
    
    public String getUser() {
        return user.get();
    }
    public void setUser(String user) {
        this.user.set(user);
    }    
    
    public String getHost() {
        return host.get();
    }
    public void setHost(String host) {
        this.host.set(host);
    }
    
    public Double getPtc() {
        return (Double) ptc.get();
    }
    public void setPtc(Double ptc) {
        this.ptc.set(ptc);
    }
    
    public Long getCalls() {
        return (Long) calls.get();
    }
    public void setCalls(Long calls) {
        this.calls.set(calls);
    }
    
    public String getDatabase() {
        return database.get();
    }
    public void setDatabase(String database) {
        this.database.set(database);
    }
    
    public String getTable() {
        return table.get();
    }
    public void setTable(String table) {
        this.table.set(table);
    }
    
    @Override
    public String getGraph1() {
        return graph1.get();
    }
    @Override
    public void setGraph1(String graph1) {
        this.graph1.set(graph1);
    }     
    
    @Override
    public String getGraph2() {
        return graph2.get();
    }
    @Override
    public void setGraph2(String graph2) {
        this.graph2.set(graph2);
    }
     
        
    public Double getAvg() {
        return (Double) avg.get();
    }
    public void setAvg(Double avg) {
        this.avg.set(avg);
    }
    
    public Long getQueryRating() {
        return (Long) queryRating.get();
    }
    public void setQueryRating(Long queryRating) {
        this.queryRating.set(queryRating);
    }
    
    public Double getResponceTime() {
        return (Double) responceTime.get();
    }
    public void setResponceTime(Double responceTime) {
        this.responceTime.set(responceTime);
    }
    
    public Double getMinResponceTime() {
        return (Double) minResponceTime.get();
    }
    public void setMinResponceTime(Double minResponceTime) {
        this.minResponceTime.set(minResponceTime);
    }
    
    public Double getMaxResponceTime() {
        return (Double) maxResponceTime.get();
    }
    public void setMaxResponceTime(Double maxResponceTime) {
        this.maxResponceTime.set(maxResponceTime);
    }
    
    public Double getAvgResponceTime() {
        return (Double) avgResponceTime.get();
    }
    public void setAvgResponceTime(Double avgResponceTime) {
        this.avgResponceTime.set(avgResponceTime);
    }
    
    public Double getStddevResponceTime() {
        return (Double) stddevResponceTime.get();
    }
    public void setStddevResponceTime(Double stddevResponceTime) {
        this.stddevResponceTime.set(stddevResponceTime);
    }
    
    public Double getLockTime() {
        return (Double) lockTime.get();
    }
    public void setLockTime(Double lockTime) {
        this.lockTime.set(lockTime);
    }
    
    public Double getAvgLockTime() {
        return (Double) avgLockTime.get();
    }
    public void setAvgLockTime(Double avgLockTime) {
        this.avgLockTime.set(avgLockTime);
    }
    
    public Long getRowsSent() {
        return (Long) rowsSent.get();
    }
    public void setRowsSent(Long rowsSent) {
        this.rowsSent.set(rowsSent);
    }
    
    public Double getAvgRowsSent() {
        return (Double) avgRowsSent.get();
    }
    public void setAvgRowsSent(Double avgRowsSent) {
        this.avgRowsSent.set(avgRowsSent);
    }
    
    public Double getRowsExamined() {
        return (Double) rowsExamined.get();
    }
    public void setRowsExamined(Double rowsExamined) {
        this.rowsExamined.set(rowsExamined);
    }
    
    public Double getAvgRowsExamined() {
        return (Double) avgRowsExamined.get();
    }
    public void setAvgRowsExamined(Double avgRowsExamined) {
        this.avgRowsExamined.set(avgRowsExamined);
    }
    
    public String getExplain() {
        return explain.get();
    }
    public void setExplain(String explain) {
        this.explain.set(explain);
    }
    
    public String getQueryTemplate() {
        return queryTemplate.get();
    }
    public void setQueryTemplate(String queryTemplate) {
        this.queryTemplate.set(queryTemplate);
    }
    
    public String getRecommendedIndex() {
        return recommendedIndex.get();
    }
    public void setRecommendedIndex(String recommendedIndex) {
        this.recommendedIndex.set(recommendedIndex);
    } 
    
    public String getRecommendations() {
        return recommendations.get();
    }
    public void setRecommendations(String recommendations) {
        this.recommendations.set(recommendations);
    } 
    
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getPtc() != null ? getPtc().toString() : "", 
            getCalls() != null ? getCalls().toString() : "", 
            getAvg() != null ? getAvg().toString() : "", 
            getTable() != null ? getTable() : "", 
            
            getResponceTime() != null ? getResponceTime().toString() : "", 
            getMinResponceTime() != null ? getMinResponceTime().toString() : "", 
            getMaxResponceTime() != null ? getMaxResponceTime().toString() : "", 
            getAvgResponceTime() != null ? getAvgResponceTime().toString() : "", 
            getStddevResponceTime() != null ? getStddevResponceTime().toString() : "", 
            
            getLockTime() != null ? getLockTime().toString() : "", 
            getAvgLockTime() != null ? getAvgLockTime().toString() : "", 
            
            getRowsSent() != null ? getRowsSent().toString() : "", 
            getAvgRowsSent() != null ? getAvgRowsSent().toString() : "", 
            
            getRowsExamined() != null ? getRowsExamined().toString() : "", 
            getAvgRowsExamined() != null ? getAvgRowsExamined().toString() : "", 
            
            getQueryTemplate() != null ? getQueryTemplate() : "",
            
            getRecommendedIndex() != null ? getRecommendedIndex() : "",            
            getRecommendations() != null ? getRecommendations() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getPtc(),
            getCalls(),
            getAvg(),
            getTable(),
            
            getDatabase(),
            
            getResponceTime(),
            getMinResponceTime(),
            getMaxResponceTime(),
            getAvgResponceTime(),
            getStddevResponceTime(),
            
            getLockTime(),
            getAvgLockTime(),

            getRowsSent(),
            getAvgRowsSent(),

            getRowsExamined(),
            getAvgRowsExamined(),
            
            getExplain(),
            "",
            
            getQueryTemplate(),
            
            getRecommendedIndex(),
            getRecommendations(),
            
            getUser(),
            getHost(),
            
            getGraph1(),
            getGraph2(),
            
            getQueryRating()
        };
    }
}