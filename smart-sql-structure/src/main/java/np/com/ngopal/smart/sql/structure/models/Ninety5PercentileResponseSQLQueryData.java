/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class Ninety5PercentileResponseSQLQueryData implements FilterableData {
    
    private SimpleStringProperty query;
    private SimpleStringProperty db;
    private SimpleStringProperty full_scan;
    private SimpleStringProperty exec_count;
    private SimpleStringProperty err_count;
    private SimpleStringProperty warn_count;
    private SimpleStringProperty total_latency;
    private SimpleStringProperty max_latency;
    private SimpleStringProperty avg_latency;
    private SimpleStringProperty rows_sent;
    private SimpleStringProperty rows_sent_avg;
    private SimpleStringProperty rows_examined;
    private SimpleStringProperty rows_examined_avg;
    private SimpleStringProperty first_seen;
    private SimpleStringProperty last_seen;
    private SimpleStringProperty digest;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("SSS 'ms'");

    public Ninety5PercentileResponseSQLQueryData(String query, String db, String full_scan, String exec_count, String err_count,
            String warn_count, String total_latency, String max_latency, String avg_latency, String rows_sent, String rows_sent_avg,
            String rows_examined, String rows_examined_avg, String first_seen, String last_seen, String digest) {
        
        this.query = new SimpleStringProperty(query);
        this.db = new SimpleStringProperty(db);
        this.full_scan = new SimpleStringProperty(full_scan);
        this.exec_count = new SimpleStringProperty(exec_count);
        this.err_count = new SimpleStringProperty(err_count);
        this.warn_count = new SimpleStringProperty(warn_count);
        
        //these values are in picoSeconds & we convert to milliseconds
        this.total_latency = new SimpleStringProperty( convert(Long.valueOf(total_latency) / 1000000000l) );
        this.max_latency = new SimpleStringProperty( convert(Long.valueOf(max_latency) / 1000000000l) );
        this.avg_latency = new SimpleStringProperty( convert(Long.valueOf(avg_latency) / 1000000000l) );
        
        this.rows_sent = new SimpleStringProperty(rows_sent);
        this.rows_sent_avg = new SimpleStringProperty(rows_sent_avg);
        this.rows_examined = new SimpleStringProperty(rows_examined);
        this.rows_examined_avg = new SimpleStringProperty(rows_examined_avg);
        this.first_seen = new SimpleStringProperty(first_seen);
        this.last_seen = new SimpleStringProperty(last_seen);
        this.digest = new SimpleStringProperty(digest);
    }

    
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[]{
            getQuery(), getDb(), getFull_scan(), getExec_count(), getErr_count(), getWarn_count(), getTotal_latency(),
            getMax_latency(), getAvg_latency(), getRows_sent(), getRows_sent_avg(), getRows_examined(), getRows_examined_avg(),
            getFirst_seen(),getLast_seen(), getDigest()
        };
    }

    public String getQuery() {
        return query.getValue();
    }

    public String getDb() {
        return db.getValue();
    }

    public String getFull_scan() {
        return full_scan.getValue();
    }

    public String getExec_count() {
        return exec_count.getValue();
    }

    public String getErr_count() {
        return err_count.getValue();
    }

    public String getWarn_count() {
        return warn_count.getValue();
    }

    public String getTotal_latency() {
        return total_latency.getValue();
    }

    public String getMax_latency() {
        return max_latency.getValue();
    }

    public String getAvg_latency() {
        return avg_latency.getValue();
    }

    public String getRows_sent() {
        return rows_sent.getValue();
    }

    public String getRows_sent_avg() {
        return rows_sent_avg.getValue();
    }

    public String getRows_examined() {
        return rows_examined.getValue();
    }

    public String getRows_examined_avg() {
        return rows_examined_avg.getValue();
    }

    public String getFirst_seen() {
        return first_seen.getValue();
    }

    public String getLast_seen() {
        return last_seen.getValue();
    }

    public String getDigest() {
        return digest.getValue();
    }
    
    private String convert(Long val){
        //val is in milliseconds
        TimeUnit unit = TimeUnit.MILLISECONDS;
        if(unit.toHours(val) > 0){
            dateFormat.applyPattern("HH 'h'");
        }else if(unit.toMinutes(val) > 0){
            dateFormat.applyPattern("mm 'm'");
        }else if(unit.toSeconds(val) > 0){
            dateFormat.applyPattern("ss 's'");
        }else {
            dateFormat.applyPattern("SSS 'ms'");
        }        
        return dateFormat.format(new Date(val));
    }
    
}
