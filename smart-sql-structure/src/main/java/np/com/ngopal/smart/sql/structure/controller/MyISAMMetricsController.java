package np.com.ngopal.smart.sql.structure.controller;

import javafx.fxml.Initializable;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.GSChartVariable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import np.com.ngopal.smart.sql.model.GlobalStatusStart;
import np.com.ngopal.smart.sql.model.OsServerSpecs;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class MyISAMMetricsController extends BaseController implements Initializable, ProfilerListener {

    protected ProfilerDataManager dbProfilerService;
    protected ProfilerChartService chartService;

    private Long firstRunDate;

    private final ObjectProperty<Long> hoveredX = new SimpleObjectProperty<>(0l);

    public ObjectProperty<Long> hoveredX() {
        return hoveredX;
    }

    private final BooleanProperty hoveredOnChart = new SimpleBooleanProperty(false);

    public BooleanProperty hoveredOnChart() {
        return hoveredOnChart;
    }

    @Getter
    private ProfilerTabContentController parentController;

    public MyISAMMetricsController() {
    }

    private Map<String, GlobalStatusStart> changedGlobalStatus = new HashMap<>();

    private final Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
    private final Map<XYChart.Series<String, Number>, String> barSeriesColors = new HashMap<>();

    @FXML
    private AnchorPane mainUI;

    @Getter
    @FXML
    private BorderPane chartContainer;

    @FXML
    private VBox flowContainer;

    @FXML
    private ScrollPane scrollContainer;

    //private WorkProc allBackgroundTask;
    private ProfilerInnerControllerWorkProcAuth myISAMMetricsControllerWorkProcAuth;

    private Long dragedStart;
    private Long dragedFinish;

    private final List<ProfilerChartController> chartTabs = new ArrayList<>();
    private final List<ProfilerBarChartController> barChartTabs = new ArrayList<>();

    private final Map<String, TitledPane> titledPanes = new HashMap<>();

    @Getter
    private Date startDate;
    
    protected ProfilerSettings getCurrentSettings() {
        return getParentController().getCurrentSettings();
    }

    public void openData(ProfilerData data) {

        firstRunDate = (long) data.getLower();

        for (ProfilerChartController t : chartTabs) {
            t.clear();
            if (t.isActive()) {
                t.initXAxis(data.getUpper(), data.getLower());
            }
        }

        for (ProfilerBarChartController t : barChartTabs) {
            t.clear();
            if (t.isActive()) {
                t.initXAxis(data.getUpper(), data.getLower());
            }
        }

        firstRunDate = null;

        Map<String, List<GSChart>> chartsMap = new HashMap<>();
        Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = initSeries(chartsMap);

        initChartData(chartsMap, chartSeriesMap, data.getListChangedGlobalStatus());
    }

    @Override
    public void userChartsChanged() {
    }

    @Override
    public void zoomInAction() {

        clearDraggedData();
        updateCharts();
    }

    @Override
    public void scrollForward() {

        Long diff = dragedFinish - dragedStart;
        dragedStart += diff;
        dragedFinish += diff;

        updateCharts();
    }

    @Override
    public void scrollBackward() {

        Long diff = dragedFinish - dragedStart;
        dragedStart -= diff;
        dragedFinish -= diff;

        updateCharts();
    }

    public void clearDraggedData() {
        dragedStart = null;
        dragedFinish = null;
    }

    public void zoomDragedAction(Long start, Long finish) {

        if (parentController != null) {
            parentController.setShowAll(false);
        }

        dragedStart = start;
        dragedFinish = finish;

        updateCharts();
    }

    @Override
    public void zoomOutAction() {

        clearDraggedData();
        updateCharts();
    }

    @Override
    public void showAllAction() {

        clearDraggedData();
        updateCharts();
    }

    public void updateCharts() {
        if (chartTabs != null) {
            for (ProfilerChartController chartController : chartTabs) {
                if (chartController.isActive()) {
                    chartController.updateChart(parentController != null ? parentController.isShowAll() : true, parentController != null ? parentController.zoomCountStage() : null, firstRunDate, dragedStart, dragedFinish);
                }
            }
        }

        if (barChartTabs != null) {
            for (ProfilerBarChartController chartController : barChartTabs) {
                if (chartController.isActive()) {
                    chartController.updateChart(parentController != null ? parentController.isShowAll() : true, parentController != null ? parentController.zoomCountStage() : null, firstRunDate, dragedStart, dragedFinish);
                }
            }
        }
    }

    public void saveProfilerData(ProfilerData pd) {
    }

    @Override
    public void startProfiler(Date startDate) {
        this.startDate = startDate;

        for (ProfilerChartController chartController : chartTabs) {
            chartController.clear();
        }

        for (ProfilerBarChartController chartController : barChartTabs) {
            chartController.clear();
        }

        firstRunDate = null;

        createAndInitChartThread();
    }

    @Override
    public void clearProfilerData() {

        for (ProfilerChartController chartController : chartTabs) {
            chartController.clear();
        }

        for (ProfilerBarChartController chartController : barChartTabs) {
            chartController.clear();
        }

        firstRunDate = null;
    }

    private Map<Long, Map<String, GlobalStatusStart>> convertToHourly(Map<Long, Map<String, GlobalStatusStart>> map, Map<Long, Map<String, Integer>> hourlyCounts) {
        Map<Long, Map<String, GlobalStatusStart>> result = new LinkedHashMap<>();

        Map<Long, Map<String, Integer>> resultCount = new LinkedHashMap<>();

        int hour = 60 * 60 * 1000;
        for (Long time : map.keySet()) {
            Map<String, GlobalStatusStart> m = map.get(time);
            if (m == ProfilerDataManager.EMPTY_STATUSES) {
                continue;
            }

            Long hourlyTime = (time / hour) * hour;

            Map<String, GlobalStatusStart> hourMap = result.get(hourlyTime);
            Map<String, Integer> mCount = resultCount.get(hourlyTime);
            if (hourMap == null) {
                hourMap = new HashMap<>();
                for (String k: m.keySet()) {
                    GlobalStatusStart gss = m.get(k);
                    hourMap.put(k, gss.copyValue());
                }
                result.put(hourlyTime, hourMap);

                mCount = new HashMap<>();
                for (String s : m.keySet()) {
                    mCount.put(s, 1);
                }
                resultCount.put(hourlyTime, mCount);
                continue;
            }

            for (String key : m.keySet()) {
                GlobalStatusStart eis0 = m.get(key);
                GlobalStatusStart eis1 = hourMap.get(key);

                if (eis1 != null) {
                    int count = mCount.get(key);
                    eis1.setValue(eis1.getValue() * count + eis0.getValue());
                    mCount.put(key, count + 1);

                } else {
                    hourMap.put(key, eis0.copyValue());
                    mCount.put(key, 1);
                }
            }
        }

        for (Long time : result.keySet()) {

            Map<String, Integer> mCount = resultCount.get(time);
            Map<String, GlobalStatusStart> hourMap = result.get(time);

            for (String s : hourMap.keySet()) {
                GlobalStatusStart eis = hourMap.get(s);
                Integer count = mCount.get(s);

                eis.setValue(eis.getValue() / (double) count);

                Map<String, Integer> counts = hourlyCounts.get(s);
                if (counts == null) {
                    counts = new HashMap<>();
                    hourlyCounts.put(time, counts);
                }

                counts.put(eis.getName(), count);
            }
        }

        return result;
    }

    public void showProfilerData(Date dateFrom) {

        firstRunDate = null;

        makeBusy(true);
        Thread th = new Thread(() -> {

            Map<String, List<GSChart>> chartsMap = new HashMap<>();
            Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = initSeries(chartsMap);
            Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();

            Platform.runLater(() -> {

                for (XYChart.Series<Number, Number> series : chartSeriesData.keySet()) {
                    synchronized (series) {
                        series.getData().addAll(chartSeriesData.get(series));
                    }
                }

                for (ProfilerChartController chartController : chartTabs) {
                    if (chartController.isActive()) {
                        chartController.clear();
                        chartController.init(chartSeriesMap.get(chartController.getChart()).values(), seriesColors);
                        chartController.initXAxis(new Date().getTime(), dateFrom.getTime());
                        chartController.updateChart(true, parentController != null ? parentController.zoomCountStage() : null, dateFrom.getTime(), dragedStart, dragedFinish);
                    }
                }

                makeBusy(false);
            });
        });
        th.setDaemon(true);
        th.start();
    }

    public Double getDoubleGlobalVariable(String key) {
        return parentController.getDoubleGlobalVariable(key);
    }

    public boolean containsGlobalVariable(String key) {
        return parentController.containsGlobalVariable(key);
    }

    public boolean hasGlobalStatus(String key) {
        return parentController.hasGlobalStatus(key);
    }

    public Double getDoubleGlobalStatus(String key, boolean value) {
        return parentController.getDoubleGlobalStatus(key, value);
    }

    @Override
    public void stopProfiler() {
        startDate = null;
    }

    @Override
    public void destroy() {
        stopProfiler();
        System.out.println("destroyController() called");
        keepThreadsAlive = false;

        changedGlobalStatus.clear();

        chartService = null;
        chartTabs.clear();
        titledPanes.clear();
    }

    public void createConnection() throws SQLException {

        // clear old data
        if (parentController != null) {
            dbProfilerService.clearOldSSData(getCurrentSettings());
        }

        createChartThread();
        reinitChartTabs();
    }

    public void updateUserCharts() {
        usersChartsWasChanged = true;
        createChartThread();
        reinitChartTabs();
    }

    public ConnectionParams getConnectionParams() {
        return parentController != null ? parentController.getParams() : null;
    }

    public Object getLockObject() {
        return parentController;
    }

    private volatile boolean keepThreadsAlive = true;
    private volatile boolean usersChartsWasChanged = false;

    protected void initChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap, Map<Long, Map<String, GlobalStatusStart>> data) {
        try {
            //List<XYChart.Series<Number, Number>> seriesList = new ArrayList<>();
            //Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> map = new HashMap<>();

            Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();

            // collect Chart data
            synchronized (getLockObject()) {

                Long lastDateTime = null;
                for (Long time : data.keySet()) {

                    Date date = new Date(time);
                    if (firstRunDate == null) {
                        firstRunDate = date.getTime();
                    }

                    Map<String, GlobalStatusStart> map = data.get(time);

                    boolean needEmpty = lastDateTime != null && time - lastDateTime > ProfilerDataManager.EMPTY_DELAY__GS;

                    //log.error("areaChartThread ********************************");
                    for (String s : map.keySet()) {

                        GlobalStatusStart gs = map.get(s);
                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {
                                if (chart.getHourly() != 1) {
                                    XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }

                                        GSChartVariable v = chart.getVariableByName(gs.getName());

                                        listSeriesData.add(new XYChart.Data(date.getTime(), v.getType() == 1 ? gs.getDifferece() : gs.getValue()));
                                    }
                                }
                            }
                        }
                    }

                    for (GSChart chart : chartSeriesMap.keySet()) {
                        if (chart.getVariables() != null) {
                            for (GSChartVariable v : chart.getVariables()) {
                                if (v.getVariable() != null && v.getVariable() == 1) {

                                    XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(v.getVariableName().toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }

                                        BigDecimal value = BigDecimal.ZERO;
                                        Double val = getDoubleGlobalVariable(v.getVariableName().toUpperCase());
                                        if (val != null) {
                                            value = new BigDecimal(val);
                                        }

                                        if (needEmpty) {
                                            listSeriesData.add(new XYChart.Data(lastDateTime + 1000, BigDecimal.ZERO));
                                            listSeriesData.add(new XYChart.Data(time - 1000, BigDecimal.ZERO));
                                        }

                                        listSeriesData.add(new XYChart.Data(date.getTime(), value != null ? value : BigDecimal.ZERO));
                                    }
                                }
                            }
                        }
                    }

                    lastDateTime = time;
                }
            }

            //log.error("areaChartThread ===============================");
            Platform.runLater(() -> {

                for (XYChart.Series<Number, Number> series : chartSeriesData.keySet()) {
                    synchronized (series) {
                        series.getData().addAll(chartSeriesData.get(series));
                    }
                }

                for (ProfilerChartController chartController : chartTabs) {
                    if (chartController.isActive()) {
                        chartController.updateChart(parentController != null ? parentController.isShowAll() : true, parentController != null ? parentController.zoomCountStage() : null, firstRunDate, dragedStart, dragedFinish);
                    }
                }
            });

        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }

    protected void initBarChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap, Map<Long, Map<String, GlobalStatusStart>> data, Map<Long, Map<String, Integer>> hourlyCounts) {
        try {

            Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> chartSeriesData = new HashMap<>();

            // collect Chart data
            synchronized (getLockObject()) {

                long hour = 60 * 60 * 1000;
                for (Long time : data.keySet()) {

                    Date date = new Date(time);
                    if (firstRunDate == null) {
                        firstRunDate = date.getTime();
                    }

                    Map<String, GlobalStatusStart> map = data.get(time);

                    //log.error("areaChartThread ********************************");
                    for (String s : map.keySet()) {

                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {
                                if (chart.getHourly() == 1) {
                                    XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }
                                        String xValue = ProfilerBarChartController.formatToXValue((long) (date.getTime() / hour) * hour);
                                        listSeriesData.add(new XYChart.Data(xValue, map.get(s).getValue()));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //log.error("areaChartThread ===============================");
            Platform.runLater(() -> {

                for (XYChart.Series<String, Number> series : chartSeriesData.keySet()) {

                    String propertyKey = null;
                    for (Map<String, XYChart.Series<String, Number>> ss : chartSeriesMap.values()) {

                        for (String s : ss.keySet()) {
                            if (series.equals(ss.get(s))) {
                                propertyKey = s;
                                break;
                            }

                            if (propertyKey != null) {
                                break;
                            }
                        }
                    }

                    for (Data<String, Number> d : chartSeriesData.get(series)) {

                        Map<String, Integer> counts = hourlyCounts.get(d.getXValue());
                        if (counts == null) {
                            counts = new HashMap<>();
                            hourlyCounts.put(ProfilerBarChartController.formatFromXValue(d.getXValue()), counts);
                        }

                        synchronized (series) {
                            boolean found = false;
                            for (Data<String, Number> d0 : series.getData()) {

                                if (d0.getXValue().equals(d.getXValue())) {

                                    if (propertyKey != null) {
                                        Integer v = counts.get(propertyKey);
                                        if (v == null) {
                                            v = 0;
                                        }

                                        counts.put(propertyKey, v + 1);

                                        double value = ((d0.getYValue().doubleValue() * v) + d.getYValue().doubleValue()) / (double) (v + 1);
                                        d0.setYValue(value);
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (!found) {
                                counts.put(propertyKey, 1);
                                series.getData().add(d);
                            }
                        }
                    }
                }

                for (ProfilerBarChartController chartController : barChartTabs) {
                    if (chartController.isActive()) {
                        chartController.updateChart(parentController != null ? parentController.isShowAll() : true, parentController != null ? parentController.zoomCountStage() : null, firstRunDate, dragedStart, dragedFinish);
                    }
                }
            });

        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }

    private Map<GSChart, Map<String, XYChart.Series<Number, Number>>> initSeries(Map<String, List<GSChart>> chartsMap) {
        seriesColors.clear();
        Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = new HashMap<>();

        List<GSChart> charts = chartService.getAll("MyISAMMetrics");
        for (GSChart chart : charts) {

            if (chart.isStatus() && chart.getHourly() != 1) {
                Map<String, XYChart.Series<Number, Number>> map = new LinkedHashMap<>();
                if (chart.getVariables() != null) {
                    for (GSChartVariable v : chart.getVariables()) {
                        String value = v.getVariableName().toUpperCase();
                        //log.error(" variable: " + value);
                        List<GSChart> list = chartsMap.get(value);
                        if (list == null) {
                            list = new ArrayList<>();
                            chartsMap.put(value, list);
                        }
                        list.add(chart);

                        final XYChart.Series series = new XYChart.Series();
                        series.setName(v.getDisplayName());

                        if (!"Random".equals(v.getDisplayColor())) {
                            seriesColors.put(series, v.getDisplayColor());
                        }

                        map.put(value, series);
                    }
                }

                chartSeriesMap.put(chart, map);
            }
        }

        return chartSeriesMap;
    }

    private Map<GSChart, Map<String, XYChart.Series<String, Number>>> initBarSeries(Map<String, List<GSChart>> chartsMap) {
        barSeriesColors.clear();
        Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap = new HashMap<>();

        List<GSChart> charts = chartService.getAll("MyISAMMetrics");
        for (GSChart chart : charts) {

            if (chart.isStatus() && chart.getHourly() == 1) {
                Map<String, XYChart.Series<String, Number>> map = new LinkedHashMap<>();
                if (chart.getVariables() != null) {
                    for (GSChartVariable v : chart.getVariables()) {
                        String value = v.getVariableName().toUpperCase();
                        //log.error(" variable: " + value);
                        List<GSChart> list = chartsMap.get(value);
                        if (list == null) {
                            list = new ArrayList<>();
                            chartsMap.put(value, list);
                        }
                        list.add(chart);

                        final XYChart.Series series = new XYChart.Series();
                        series.setName(v.getDisplayName());

                        if (!"Random".equals(v.getDisplayColor())) {
                            barSeriesColors.put(series, v.getDisplayColor());
                        }

                        map.put(value, series);
                    }
                }

                chartSeriesMap.put(chart, map);
            }
        }

        return chartSeriesMap;
    }

    public OsServerSpecs getCurrentServerSpecs() {
        return null;
    }

    protected void reinitChartTabs() {
        Platform.runLater(() -> {
            for (ProfilerChartController chartController : chartTabs) {
                chartController.clear();
            }

            for (ProfilerBarChartController chartController : barChartTabs) {
                chartController.clear();
            }

            firstRunDate = null;
            //chartsTabPane.getTabs().clear();
            chartTabs.clear();
            barChartTabs.clear();
            titledPanes.clear();

//            GSChart defaultChart = new GSChart();
//            defaultChart.setChartName("Default chart");
//            ProfilerChartController pcc = loadProfilerChartEntityMetricControllers(defaultChart);
//            flowContainer.getChildren().add(0, pcc.getUI());
//            chartTabs.add(pcc);
//            Utitlities.prepFlowPaneDimensionConditionWithParent(flowContainer, 1.0, true);
            List<GSChart> charts = chartService.getAll("MyISAMMetrics");

            VBox lastVbox = null;
            for (GSChart chart : charts) {
                if (chart.isStatus()) {

                    HBox box = null;
                    VBox vbox = null;

                    int groupWidth = 2;
//                    if ("InnoDB I/O".equals(chart.getGroupName()) || "Buffer Pool I/O".equals(chart.getChartName())) {
//                        groupWidth = 3;
//                    }

                    if (chart.getGroupName() != null && !chart.getGroupName().isEmpty()) {
                        TitledPane tp = titledPanes.get(chart.getGroupName());
                        if (tp == null) {
                            tp = new TitledPane();
                            tp.setFocusTraversable(false);
                            tp.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            tp.setText(chart.getGroupName());

                            VBox vx = new VBox(5);
                            vx.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vx.setPadding(new Insets(-3.0));
                            vx.setAlignment(Pos.CENTER);
                            tp.setContent(vx);

                            flowContainer.getChildren().add(0, tp);

                            titledPanes.put(chart.getGroupName(), tp);
                        }

                        vbox = (VBox) tp.getContent();

                    } else if (lastVbox == null) {
                        vbox = new VBox(5);
                        lastVbox = vbox;
                        flowContainer.getChildren().add(0, vbox);
                    } else {
                        vbox = lastVbox;
                    }

                    if (vbox.getChildren().isEmpty()) {
                        box = new HBox(5);
                        box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                        vbox.getChildren().add(0, box);
                    } else {

                        HBox oldBox = (HBox) vbox.getChildren().get(0);

                        if (chart.getGroupWidth() == groupWidth) {
                            box = new HBox(5);
                            box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vbox.getChildren().add(0, box);
                        } else if (oldBox.getChildren().size() == groupWidth || ((GSChart) oldBox.getUserData()).getGroupWidth() == groupWidth) {
                            box = new HBox(5);
                            box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vbox.getChildren().add(0, box);
                        }

                        if (box == null) {
                            box = oldBox;
                        } else if (oldBox.getChildren().size() == 1 && ((GSChart) oldBox.getUserData()).getGroupWidth() == 1) {
                            Label empty = new Label();
                            empty.setMaxWidth(Double.MAX_VALUE);
                            oldBox.getChildren().add(empty);
                            HBox.setHgrow(empty, Priority.ALWAYS);
                        }
                    }

                    box.setAlignment(Pos.CENTER);
                    box.setUserData(chart);

                    if (chart.getHourly() == 1) {
                        ProfilerBarChartController pcc = loadProfilerBarChartEntityMetricControllers(chart);
                        box.getChildren().add(0, pcc.getUI());

                        pcc.getUI().setMaxWidth(Double.MAX_VALUE);
                        HBox.setHgrow(pcc.getUI(), Priority.ALWAYS);

                        barChartTabs.add(pcc);
                    } else {
                        ProfilerChartController pcc = loadProfilerChartEntityMetricControllers(chart);
                        box.getChildren().add(0, pcc.getUI());

                        pcc.getUI().setMaxWidth(Double.MAX_VALUE);
                        HBox.setHgrow(pcc.getUI(), Priority.ALWAYS);

                        chartTabs.add(pcc);
                    }
                }
            }
            Platform.runLater(() -> usersChartsWasChanged = false);
        });
    }

    private void createAndInitChartThread() {
        createChartThread();
        // THIS NOT WORK WITH THIS BACKGROUND THREADS
        // SOME TIMES IT JUST IGNORING WORKPROC, 
        // ALSO WE HAVE ONLY 3 THREAD - THIS IS NOT ENOUGH
        //Recycler.getLessBusySWP().addWorkProc(allBackgroundTask);
//        
//        Thread th = new Thread(() -> {
//            allBackgroundTask.run();
//        }, "MyISAMMetrics");
//        th.setDaemon(true);
//        th.start();
    }

    private void createChartThread() {
        // generate server hash code
        final String serverHashCode = serverHashCode(getConnectionParams(), "");
        myISAMMetricsControllerWorkProcAuth = new ProfilerInnerControllerWorkProcAuth() {

            long lastOverallCodeExecutionRun = 0l;
            long lastUpdateExecutionRun = 0l;

            Map<String, List<GSChart>> chartsMap;
            Map<String, List<GSChart>> barChartsMap;

            Map<Long, Map<String, Integer>> hourlyCounts;
            Map<Long, Map<String, GlobalStatusStart>> changedCache;

            boolean first = true;
            // read latest data
            Date currentDate;
            Date readDate;

//                    Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();
            Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap;

            Map<Long, Map<String, GlobalStatusStart>> result;
//                    Map<Long, Map<String, GlobalStatusStart>> result = readData(, chartSeriesData, chartsMap, chartSeriesMap, );

//                    Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> barChartSeriesData = new HashMap<>();
            Map<GSChart, Map<String, XYChart.Series<String, Number>>> barChartSeriesMap;
//                    result = readHourlyData(readDate, barChartSeriesData, barChartsMap, barChartSeriesMap, currentDate);

            Map<Long, Map<String, GlobalStatusStart>> barResult;

            Map<String, GSChartVariable> variablesNames;

            Long lastDateTime = null;

            @Override
            public boolean shouldExecuteWorkProc() {
                return (getParentController() == null || getParentController().isDataLoaded() && getParentController().profilerStarted().get()) && keepThreadsAlive && !usersChartsWasChanged
                        && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastOverallCodeExecutionRun) >= 1;
            }

            @Override
            public void runWork() {
                if (chartSeriesMap == null) {
                    firstRunDate = null;
                    chartsMap = new HashMap<>();
                    barChartsMap = new HashMap<>();
                    hourlyCounts = new HashMap<>();
                    changedCache = new LinkedHashMap<>();
                    // read latest data
                    currentDate = new Date();
                    readDate = new Date(currentDate.getTime() - ChartScale.SCALE_8H.getValue());

//                    Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();
                    chartSeriesMap = initSeries(chartsMap);
                    result = dbProfilerService.selectGSDistinction(serverHashCode(getConnectionParams(), ""), readDate, ProfilerDataManager.EMPTY_DELAY__GS, currentDate);
//                    Map<Long, Map<String, GlobalStatusStart>> result = readData(, chartSeriesData, chartsMap, chartSeriesMap, );

//                    Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> barChartSeriesData = new HashMap<>();
                    barChartSeriesMap = initBarSeries(barChartsMap);
//                    result = readHourlyData(readDate, barChartSeriesData, barChartsMap, barChartSeriesMap, currentDate);
                    barResult = convertToHourly(result, hourlyCounts);
                    variablesNames = chartService.getVariablesNames("MyISAMMetrics");
                    lastDateTime = null;

                    Platform.runLater(() -> {
//                        for (XYChart.Series<Number, Number> series: chartSeriesData.keySet()) {
//                            series.getData().addAll(chartSeriesData.get(series));
//                        }
//                        
                        // TODO Check 
//                        for (XYChart.Series<String, Number> series: barChartSeriesData.keySet()) {
//                            series.getData().addAll(barChartSeriesData.get(series));
//                        }

                        for (ProfilerChartController chartController : chartTabs) {
                            if (chartController.isActive()) {
                                chartController.clear();
                            }
                        }

                        for (ProfilerBarChartController chartController : barChartTabs) {
                            if (chartController.isActive()) {
                                chartController.clear();
                            }
                        }
                        initChartData(chartsMap, chartSeriesMap, result);
                        initBarChartData(barChartsMap, barChartSeriesMap, barResult, hourlyCounts);
                    });

                }
                synchronized (getLockObject()) {

                    Map<Long, Map<String, GlobalStatusStart>> lastGS = dbProfilerService.selectLastGS(serverHashCode, startDate);
                    Long maxTime = startDate.getTime();
                    for (Long time : lastGS.keySet()) {
                        if (time > maxTime) {
                            maxTime = time;
                        }

                        Map<String, GlobalStatusStart> changedGS = lastGS.get(time);

                        // need add with 0 all not found
                        for (String key : variablesNames.keySet()) {

                            GSChartVariable v = variablesNames.get(key);
                            if (v != null && v.getVariable() != null && v.getVariable() == 0) {

                                String[] names = v.getVariableName().split("\\s+");

                                boolean found = false;
                                if (names.length == 1) {
                                    for (String k : changedGS.keySet()) {
                                        if (k.equalsIgnoreCase(key)) {
                                            found = true;
                                        }
                                    }
                                }
                                if (!found) {
                                    if (names.length >= 2) {
                                        String function = names[0].trim();
                                        String name = names[1].trim().toUpperCase();

                                        double value = 0l;
                                        if ("AVG".equalsIgnoreCase(function)) {
                                            List<GSChart> gsc = chartsMap.get(name);
                                            if (gsc != null && !gsc.isEmpty()) {
                                                Map<String, XYChart.Series<Number, Number>> ss = chartSeriesMap.get(gsc.get(0));
                                                if (ss != null && ss.containsKey(name)) {

                                                    synchronized (ss.get(name)) {
                                                        long avg = 0l;
                                                        List<Data<Number, Number>> list = ss.get(name).getData();
                                                        int size = list.size();

                                                        for (Data<Number, Number> data : list) {
                                                            avg += data.getYValue().longValue();
                                                        }

                                                        if (changedGS.containsKey(name)) {
                                                            avg += (Double) changedGS.get(name).getValue();
                                                            size++;
                                                        }

                                                        value = avg / size;
                                                    }
                                                }
                                            }
                                        } else if ("DIVIDE".equalsIgnoreCase(function) && names.length >= 3) {
                                            String divider = names[2].toUpperCase();

                                            double dividerValue = 0l;
                                            double currentValue = 0l;

                                            if (changedGS.containsKey(name)) {
                                                currentValue = (Double) changedGS.get(name).getValue();
                                            }

                                            if (changedGS.containsKey(divider)) {
                                                dividerValue = (Double) changedGS.get(divider).getValue();
                                            }

                                            if (dividerValue > 0) {
                                                value = currentValue / dividerValue;

                                                if (names.length == 4) {
                                                    try {
                                                        value *= Long.valueOf(names[3]);
                                                    } catch (NumberFormatException ex) {
                                                    }
                                                }
                                            }
                                        }

                                        changedGS.put(key, new GlobalStatusStart(key, value, value));

                                    } else {

                                        changedGS.put(key, new GlobalStatusStart(key, 0.0, 0.0));
                                    }
                                }
                            }
                        }

                        if (lastDateTime == null) {
                            changedCache.put(time - 1000, ProfilerDataManager.EMPTY_STATUSES);

                        } else if (time - lastDateTime > ProfilerDataManager.EMPTY_DELAY__GS) {

                            changedCache.put(lastDateTime + 1000, ProfilerDataManager.EMPTY_STATUSES);
                            changedCache.put(time - 1000, ProfilerDataManager.EMPTY_STATUSES);
                        }
                        changedCache.put(time, changedGS);
                        lastDateTime = time;
                    }

                    startDate = new Date(maxTime);
                }

                if (first) {
                    first = false;
                    // add serieas to each chart
                    Platform.runLater(() -> {
                        //log.error("start init");
                        for (ProfilerChartController chartController : chartTabs) {
                            if (chartController.isActive() && chartSeriesMap.get(chartController.getChart()) != null) {
                                chartController.init(chartSeriesMap.get(chartController.getChart()).values(), seriesColors);
                            }
                        }
                        for (ProfilerBarChartController chartController : barChartTabs) {
                            if (chartController.isActive() && barChartSeriesMap.get(chartController.getChart()) != null) {
                                chartController.init(barChartSeriesMap.get(chartController.getChart()).values(), barSeriesColors);
                            }
                        }
                    });
                }

                lastOverallCodeExecutionRun = System.currentTimeMillis();

                if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastUpdateExecutionRun) >= getCurrentSettings().getDataRefresh()) {
                    initChartData(chartsMap, chartSeriesMap, changedCache);
                    initBarChartData(chartsMap, barChartSeriesMap, changedCache, hourlyCounts);

                    changedCache.clear();

                    lastUpdateExecutionRun = System.currentTimeMillis();

                }
            }
        };
        usersChartsWasChanged = false;
    }

    public void exportToXls(
            List<String> sheets,
            List<ObservableList<ObservableList>> totalData,
            List<LinkedHashMap<String, Boolean>> totalColumns,
            List<List<Integer>> totalColumnWidths,
            List<byte[]> images,
            List<Integer> heights,
            List<Integer> colspans) {
    }

    public double getUpper() {
        return chartTabs.get(0).getUpper();
    }

    public double getLower() {
        return chartTabs.get(0).getLower();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public void setParentController(ProfilerTabContentController parentController) {
        this.parentController = parentController;

        createAllEntities();

        resetBanners();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private void resetBanners() {

    }

    private ProfilerBarChartController loadProfilerBarChartEntityMetricControllers(GSChart chart) {

        ProfilerBarChartController pcc = loadEntityMetricControllers(ProfilerBarChartController.class);
        pcc.insertGSChart(chart);
        pcc.setChartControllerEventHandler(new ProfilerChartControllerEventHandler() {
            @Override
            public void zoomDragedAction(Long start, Long finish) {
                MyISAMMetricsController.this.zoomDragedAction(start, finish);
            }
        });
        if (parentController != null) {
            pcc.currentScaleIndex().bind(parentController.currentScaleIndex());
        }
//        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
//        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }

    private ProfilerChartController loadProfilerChartEntityMetricControllers(GSChart chart) {
        ProfilerChartController pcc = loadEntityMetricControllers(ProfilerChartController.class);
        pcc.insertGSChart(chart);
        pcc.setChartControllerEventHandler(new ProfilerChartControllerEventHandler() {
            @Override
            public void zoomDragedAction(Long start, Long finish) {
                MyISAMMetricsController.this.zoomDragedAction(start, finish);
            }
        });
        if (parentController != null) {
            pcc.currentScaleIndex().bind(parentController.currentScaleIndex());
        }
        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }

    private <T extends MetricEntityContainerController> T loadEntityMetricControllers(Class<T> meccClass) {
        try {
            T t = StandardBaseControllerProvider.getController(meccClass, getController(), "MetricEntityContainer");
            t.activateMetric();
            return t;
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

//    private TabularBannerNodeController loadEntityTabularBannerControllers(String description) {
//        try {
//            TabularBannerNodeController controller = StandardBaseControllerProvider.getController("TabularBannerNode");
//            controller.init(description, getParentController());
//            return controller;
//        } catch (Exception ex) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
    protected void createAllEntities() {
        if (getUI().getScene() == null) {
            getUI().sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
                if (newValue != null && oldValue == null) {
                    applyNewUiModifications();
                }
            });
        } else {
            applyNewUiModifications();
        }
    }

    private void applyNewUiModifications() {
        scrollContainer.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
    }

    public void finalizeConnectionCreation() {
        getUI().requestLayout();
    }

    @Override
    public ProfilerInnerControllerWorkProcAuth getInnerControllerWorkProcAuthority() {
        return myISAMMetricsControllerWorkProcAuth;
    }

    public void setDbProfilerService(ProfilerDataManager dbProfilerService) {
        this.dbProfilerService = dbProfilerService;
    }

    public void setChartService(ProfilerChartService chartService) {
        this.chartService = chartService;
    }

}
