package np.com.ngopal.smart.sql.structure.dialogs;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public enum DialogResponce {
    
    OK_YES,
    NO,
    CANCEL,
    YES_TO_ALL,
    NO_TO_ALL
}
