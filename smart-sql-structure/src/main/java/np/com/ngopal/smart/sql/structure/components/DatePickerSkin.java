package np.com.ngopal.smart.sql.structure.components;

import javafx.beans.value.ObservableValue;
import javafx.event.Event;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DatePickerSkin extends com.sun.javafx.scene.control.skin.DatePickerSkin
{
    
    
    public DatePickerSkin(final DatePicker datePicker)
    {
        super(datePicker);
        
        
        datePicker.setOnShowing((final Event event) ->
        {
            setTextFromTextFieldIntoComboBoxValue();
        });
        
        datePicker.getEditor().focusedProperty().addListener((final ObservableValue<? extends Boolean> observable, final Boolean oldValue, final Boolean newValue) ->
        {
            if (!newValue)
            {
                setTextFromTextFieldIntoComboBoxValue();
            }
        });
    }
    
    
}
