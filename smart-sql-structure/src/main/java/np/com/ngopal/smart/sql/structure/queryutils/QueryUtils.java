/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.queryutils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import tools.java.pats.models.SqlFormatter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class QueryUtils {
    
    
    public static final Map<ConnectionSession, List<String>> highlightGreenMap = new HashMap<>();
    public static final Map<ConnectionSession, List<String>> highlightMaroonMap = new HashMap<>();
    public static byte CARET_POSITION_FROM_START = 0;
    public static byte CARET_POSITION_FROM_LINE = 1;
    
    public static String formatCurrentQuery(PreferenceDataService preferenceService, String query) {
        SqlFormatter formatter = new SqlFormatter();
        List<PreferenceData> list = preferenceService.getAll();
        if (!list.isEmpty()) {
            return formatter.formatSql(query, list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle());
        } else {
            return formatter.formatSql(query, "", "2", "block");
        }
    }
    
}
