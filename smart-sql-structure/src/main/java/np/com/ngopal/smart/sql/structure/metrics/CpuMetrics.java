/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import javafx.application.Platform;
import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class CpuMetrics extends Metric {

    //private HashMap<String, String> currentResults = new HashMap();
    private final String physical_id = "physical id";
    private final String cpu_speed = "cpu MHz";
    private final int cpuRowPositionInResponse = 2;
    private final String linuxTopCommandPingingValue = "1";

    public CpuMetrics() {
        super("CPU METRIC",
                "CPU METRIC CHART \n Measures the current CPU usage rate of your remote server, every second.\n"
                        + " The responses might be delayed, but gives a roughly accurate state of your CPU usage over second period");
    }

    @Override
    public StringConverter<Number> yConverter() {
        return new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return String.valueOf(object.doubleValue()) + " %";
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string.split(" ")[0]);
            }
        };
    }

    @Override
    public long getMaxYValue() {
        //this is not [thousand] but rather Long Type for [hundered]
        return 100l;
    }

    @Override
    public int getUnitTimeValue() {
        return super.getUnitTimeValue(); 
    }

    @Override
    public XYChart.Series<Number, Number>[] getXYChartSeries() {
        XYChart.Series<Number, Number>[] series = new XYChart.Series[1];
        series[0] = getNewXYChart();
        series[0].setName("CPU CLOCK SPEED");
        return series;
    }
    
    private CpuMetricExtract getCpuMetricExtract(){
        return (CpuMetricExtract) metricExtract;
    }

    @Override
    public String[] getChartColors() {
        return new String[]{"khaki"};
    }

    @Override
    public void updateUI(long timeInMillis, MetricExtract extract) {
        super.updateUI(timeInMillis, extract);
        if(extract != null){
            for(XYChart.Series<Number,Number>  serie : data){
                XYChart.Data<Number, Number> myData = new XYChart.Data();
                myData.setXValue(timeInMillis);
                myData.setYValue(Double.valueOf(getCpuMetricExtract().getTotalUsedRam()));
                serie.getData().add(myData);
            }
        }
    }
    
}
