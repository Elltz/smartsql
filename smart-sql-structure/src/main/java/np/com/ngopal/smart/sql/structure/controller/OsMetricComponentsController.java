/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import np.com.ngopal.smart.sql.structure.controller.MetricEntityContainerController;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Popup;
import np.com.ngopal.smart.sql.structure.metrics.Metric;
import np.com.ngopal.smart.sql.structure.utils.Flags;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class OsMetricComponentsController extends MetricEntityContainerController implements EventHandler<MouseEvent> {
    
    protected AreaChart metricChart;
    protected NumberAxis xAxis; //stands with time
    protected NumberAxis yAxis;
    protected Metric metricComponent;
    private Line mouseLine;
    private Popup popUp = new Popup();
     private Label popupLabel = new Label();
    private Map<XYChart.Series<Number, Number>, Label> popupValueLabels = new HashMap<>();
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//"mm:ss"
    private Date date = new Date();
    private Rectangle rectOverlay;
    private Long dragPrefixTime;
    private Bounds chartContentScreenBounds;
    private Region chartPlotBackground;
    private boolean draggingMode;
    
    @Override
    public String representableName() {
        return metricChart != null ? metricChart.getTitle() : super.representableName();
    }

    @Override
    public Node getContentNodeAsType() {
        return super.getContentNodeAsType();
    }

    @Override
    public void setContent(Node t) {
        super.setContent(t);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources); 
        createEntityAsModule();
        prepareAxis();
        areaChartDefaultProperties(); 
        
        mouseLine = new Line();
        mouseLine.setStroke(Color.RED);
        mouseLine.setStrokeWidth(0.5);
        mouseLine.setStartY(0);   
        
        rectOverlay = new Rectangle();
        rectOverlay.setFill(Color.WHITESMOKE);
        rectOverlay.setOpacity(0.3);
    }

    @Override
    public AnchorPane getUI() {
        return super.getUI();
    }
    
    public void setMetric(Metric m){
        metricComponent = m;        
        prepareSeries();
    }
    
    private void prepareAxis() {  
        long now = System.currentTimeMillis();
        int daysBack = 15;
        xAxis = new NumberAxis(now - TimeUnit.DAYS.toMillis(daysBack), now, TimeUnit.DAYS.toMillis(3));
        xAxis.setTickLength(1);
        xAxis.setAnimated(true);
        xAxis.setTickLabelGap(2);        
        
        yAxis = new NumberAxis(0, 1, 10);
        yAxis.setAnimated(true);
        yAxis.setUpperBound(100);
        yAxis.setMinorTickCount(0);
        yAxis.setMinorTickVisible(false);
        yAxis.setTickLabelGap(2);
    }
    
    protected void areaChartDefaultProperties(){
        metricChart = new AreaChart(xAxis, yAxis);
        metricChart.setCreateSymbols(false);
        metricChart.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
        metricChart.setPadding(new Insets(6));
        
        GridPane gp = new GridPane();
        gp.getStyleClass().add("profilerChartDataTooltip");
        popupLabel.getStyleClass().add("profilerChartDataTooltipTitleLabel");
        gp.add(popupLabel, 0, 0, 2, 1);
        GridPane.setHalignment(popupLabel, HPos.CENTER);
        popUp.getContent().add(gp);
        
        metricChart.setOnMouseMoved(this);
        metricChart.setOnMouseExited(this);    
        metricChart.setOnMouseDragged(this);
        metricChart.setOnMouseDragReleased(this);
        //metricChart.setOnMouseDragExited(this);
        //metricChart.setOnMouseDragEntered(this);
        metricChart.setOnDragDetected(this);
        setContent(metricChart);
    }
    
    public void prepareSeries() {        
        metricChart.setTitle(metricComponent.getMetricName());        
        setDescription(metricComponent.getMetricDescription()); 
        //metricChart.setStyle(metricComponent.generateStyleColorForChart());
        
        if(metricComponent.yConverter() != null){
            yAxis.setTickLabelFormatter(metricComponent.yConverter());
        }
        if(metricComponent.yConverter() != null){
            xAxis.setTickLabelFormatter(metricComponent.xConverter());
        }
        
        chartPlotBackground = (Region) metricChart.lookup(".chart-plot-background"); 
        
        metricChart.setData(metricComponent.getData());
        metricChart.getData().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                /**
                 * THis is where i am able to detect that the MetricExtract has been added to its Metric Instance,
                 * before this method is called metricComponent's MetricExtract is null.
                 * 
                 * THis is where i am able to detect that the series have been laid out into the Chart
                 * 
                 * if you have a better way of detecting, write it and put these code below
                 */
                if (metricComponent.getMaxYValue() > 0) {
                    yAxis.setUpperBound(metricComponent.getMaxYValue());
                    xAxis.setTickUnit(metricComponent.getTimeUnitSpaceInMillis());                    
                    // listener code for non-recursive calls - this is to stop the calling all the time.
                    metricChart.getData().removeListener(this);
                }
            }
        });
    }

    private void tooltipDisplay(double relativePositionX, double relativePositionY) {
        if (metricComponent.getData() != null && !metricComponent.getData().isEmpty()) {
            GridPane gp = (GridPane) popUp.getContent().get(0);

            long valueOnDisplay = xAxis.getValueForDisplay(xAxis.screenToLocal(relativePositionX, relativePositionY).getX()).longValue();
            date.setTime(valueOnDisplay);

            int positionInSerieData = -1;
            int row = 1;
            popupLabel.setText(sdf.format(date));

            for (XYChart.Series<Number, Number> xySerie : metricComponent.getData()) {
                //here we try get the position of the data value we are looking in the serie data
                if (positionInSerieData == -1) {
                    for (int pos = 0; pos < xySerie.getData().size(); pos++) {
                        XYChart.Data<Number, Number> myData = xySerie.getData().get(pos);
                        date.setTime(myData.getXValue().longValue());
                        long absLong = Math.abs(myData.getXValue().longValue() - valueOnDisplay);

                        if (absLong < TimeUnit.SECONDS.toMillis(3)) {
                            positionInSerieData = pos;
                        }
                    }
                }

                if (popupValueLabels.get(xySerie) == null) {
                    Line l = new Line(0, 15, 15, 15);
                    l.setStrokeWidth(2);
                    l.setStroke(Color.web(metricComponent.getChartColors()[metricComponent.getData().indexOf(xySerie)]));

                    Label title = new Label();
                    title.getStyleClass().add("profilerChartDataTooltipLabel");
                    title.setText(xySerie.getName() + ": ");
                    title.setGraphic(l);

                    Label value = new Label();
                    value.getStyleClass().add("profilerChartDataTooltipBoldLabel");

                    gp.add(title, 0, row);
                    GridPane.setHgrow(title, Priority.ALWAYS);

                    gp.add(value, 1, row);
                    GridPane.setHalignment(value, HPos.RIGHT);
                    popupValueLabels.put(xySerie, value);
                    row++;
                }

                //now we we retrieve the data
                String val = "0";
                if (positionInSerieData > -1) {
                    XYChart.Data<Number, Number> myData = xySerie.getData().get(positionInSerieData);
                    val = String.valueOf(myData.getYValue());
                }
                popupValueLabels.get(xySerie).setText(val);
            }

            mouseLine.setVisible(true);
            Group group = ((Group) metricComponent.getData().get(metricComponent.getData().size() - 1).getNode());
            if (!group.getChildren().contains(mouseLine)) {
                group.getChildren().add(mouseLine);
                mouseLine.setEndY(yAxis.getHeight());
            }

            double displayLoc = mouseLine.screenToLocal(relativePositionX, relativePositionY).getX();
            mouseLine.setStartX(displayLoc);
            mouseLine.setEndX(displayLoc);
            popUp.show(metricChart.getScene().getWindow(), relativePositionX + 10, relativePositionY + 10);
        }
    }
    
    public void manualAdjustTimeBounds(Long minTimeBound, Long maxTimeBound) {
        if(minTimeBound == null || maxTimeBound == null){ return; }
        xAxis.setLowerBound(minTimeBound);
        xAxis.setUpperBound(maxTimeBound);
        double interval = xAxis.getUpperBound() - minTimeBound;
        xAxis.setTickUnit(
                metricComponent.getTimeUnitSpaceInMillis()
                * (interval / TimeUnit.HOURS.toMillis(1))
        );
    }
    
    private void preDragSelectCatch(double relativePositionX, double relativePositionY){
        Point2D point = xAxis.screenToLocal(relativePositionX, relativePositionY);
        dragPrefixTime = xAxis.getValueForDisplay(point.getX()).longValue();
        if (rectOverlay.getParent() == null) {
            Group group = ((Group) metricComponent.getData().get(metricComponent.getData().size() - 1).getNode());
            group.getChildren().add(rectOverlay);
        }
        
        chartContentScreenBounds = chartPlotBackground.localToScreen(chartPlotBackground.getLayoutBounds());
        
        if(chartContentScreenBounds.contains(relativePositionX, relativePositionY)){
            System.out.println("gotcha");
            hideTooltips();
            point = chartPlotBackground.screenToLocal(relativePositionX, relativePositionY);
            rectOverlay.setX(point.getX());
            rectOverlay.setY(point.getY());
            draggingMode = true;
        }
    }
    
    private void onDragSelectCatch(double relativePositionX, double relativePositionY){
        if(draggingMode && chartContentScreenBounds.contains(relativePositionX, relativePositionY)){
            Point2D point = chartPlotBackground.screenToLocal(relativePositionX, relativePositionY);
            rectOverlay.setWidth(point.getX() - rectOverlay.getX());
            rectOverlay.setHeight(point.getY() - rectOverlay.getY());
        }        
    }
    
    private void postDragSelectCatch(double relativePositionX, double relativePositionY){
        Point2D point = xAxis.screenToLocal(relativePositionX, relativePositionY);
        long valueOnDisplay = xAxis.getValueForDisplay(point.getX()).longValue();
        Platform.runLater(()->{
            Group group = ((Group) metricComponent.getData().get(metricComponent.getData().size() - 1).getNode());
            group.getChildren().remove(rectOverlay);
            manualAdjustTimeBounds(dragPrefixTime, valueOnDisplay);
        });
        draggingMode = false;
    }

    @Override
    public void handle(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_EXITED) {
            hideTooltips();
        }else if(event.getEventType() == MouseEvent.MOUSE_MOVED){
            tooltipDisplay(event.getScreenX(), event.getScreenY());
        }else if(event.getEventType() == MouseEvent.DRAG_DETECTED){
            ((Node)event.getSource()).startFullDrag();
            preDragSelectCatch(event.getScreenX(), event.getScreenY());
        }else if(event.getEventType() == MouseEvent.MOUSE_DRAGGED){
            onDragSelectCatch(event.getScreenX(), event.getScreenY());
        }else if(event.getEventType() == MouseDragEvent.MOUSE_DRAG_RELEASED){
            postDragSelectCatch(event.getScreenX(), event.getScreenY());
        }else if(event.getEventType() == MouseDragEvent.MOUSE_DRAG_EXITED){
        }else if(event.getEventType() == MouseDragEvent.MOUSE_DRAG_ENTERED){}
    }
    
    private void hideTooltips(){
        popUp.hide();
        mouseLine.setVisible(false);
    }
}
