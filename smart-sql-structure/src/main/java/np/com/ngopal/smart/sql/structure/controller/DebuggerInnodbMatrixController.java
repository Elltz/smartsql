package np.com.ngopal.smart.sql.structure.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import static np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService.CACHE_TIME;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.EngineInnodbStatus;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DebuggerInnodbMatrixController extends InnodbMatrixController {

    private DebuggerTabController parentController;
    
    public DebuggerInnodbMatrixController() {}
    
    public void init(DebuggerTabController parentController, DBDebuggerProfilerService dbProfilerService, ProfilerChartService chartServ) {
        this.dbProfilerService = dbProfilerService;
        this.parentController = parentController;     
        this.chartService = chartServ;
        createAllEntities();
        reinitChartTabs();
    }
    
    
    @Override
    protected ProfilerSettings getCurrentSettings() {
        return parentController.getCurrentSettings();
    }
        
    @Override
    public synchronized Double getDoubleGlobalVariable(String key) {
        return parentController.getDoubleGlobalVariable(key);
    }

    @Override
    public synchronized boolean containsGlobalVariable(String key) {
        return parentController.containsGlobalVariable(key);
    }
    
    @Override
    public synchronized boolean hasGlobalStatus(String key) {
        return parentController.hasGlobalStatus(key);
    }
    
    @Override
    public synchronized Double getDoubleGlobalStatus(String key, boolean value) {
        return parentController.getDoubleGlobalStatus(key, value);
    }

    @Override
    public ConnectionParams getConnectionParams() {
        return parentController != null ? parentController.getParams() : null;
    }    

    @Override
    public Object getLockObject() {
        return parentController;
    }

    @Override
    protected void initChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap, Map<Long, Map<String, EngineInnodbStatus>> data) {
        super.initChartData(chartsMap, chartSeriesMap, data);
        
        Long minTime = new Date().getTime() - CACHE_TIME;
        Platform.runLater(() -> {
            
            for (Map<String, XYChart.Series<Number, Number>> m: chartSeriesMap.values()) {
                for (XYChart.Series<Number, Number> s: m.values()) {
                    synchronized (s) {
                        for (Data<Number, Number> d: new ArrayList<>(s.getData())) {
                            if (d.getXValue().longValue() < minTime) {
                                s.getData().remove(d);

                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void initBarChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap, Map<Long, Map<String, EngineInnodbStatus>> data, Map<Long, Map<String, Integer>> hourlyCounts) {
        super.initBarChartData(chartsMap, chartSeriesMap, data, hourlyCounts); 
        
        Long minTime = new Date().getTime() - CACHE_TIME;

        Platform.runLater(() -> {
        
            for (Map<String, XYChart.Series<String, Number>> m: chartSeriesMap.values()) {
                for (XYChart.Series<String, Number> s: m.values()) {
                    synchronized (s) {
                        for (Data<String, Number> d: new ArrayList<>(s.getData())) {
                            if (ProfilerBarChartController.formatFromXValue(d.getXValue()) < minTime) {
                                s.getData().remove(d);

                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    
    
    
    
    
    
    
}
