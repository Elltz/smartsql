package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerDatabase {

    private final SimpleStringProperty name = new SimpleStringProperty("");
    
    private final ObservableList<DesignerTable> tables = FXCollections.observableArrayList();
    
    
    public SimpleStringProperty name() {
        return name;
    }
    
    public ObservableList<DesignerTable> tables() {
        return tables;
    }
}
