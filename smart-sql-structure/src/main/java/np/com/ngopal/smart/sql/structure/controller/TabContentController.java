/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.db.ConnectionService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.*;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public abstract class TabContentController extends BaseController implements
        Initializable, ConnectionService {

    @Setter
    @Getter
    private ConnectionSession session;

    public ConnectionParams getConnectionParams() {
        return session.getConnectionParam();
    }
    
    @Getter
    @Setter
    private DBService dbService;
    
    @Getter
    private final Map<HookKey, Hookable> hooks = new LinkedHashMap<>();

    public abstract void focusLeftTree();
    /**
     * This function helps to open new connection
     */
    public abstract void openConnection(ConnectionParams params) throws SQLException;

    public abstract void addTab(Tab t);

    public abstract void showTab(Tab t);
    
    public abstract Tab getTab(String id, String text);

    public abstract void addTabToBottomPane(Tab... tab);
    
    public abstract void removeTabFromBottomPane(Tab... tab);
    
    public abstract ReadOnlyObjectProperty<Tab> getSelectedTab();

    public abstract List<Tab> getBottamTabs();

    public abstract ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem();

    public abstract boolean isNeedOpenInNewTab();

    public abstract String getTabName();

    public abstract DisconnectPermission destroyController(DisconnectPermission permission);

    public abstract void refreshTree(boolean alertkDatabaseCount);
    
    public abstract Object getTreeViewUserData();
    /**
     * This is called whenever a tab content is visible or active to the user
     * currently
     */
    public abstract void  isFocusedNow();
    
    /**
     * Hides left content if showing
     */
    public abstract void hideLeftPane();
    
    /**
     * Shows left content if hidden
     */    
    public abstract void showLeftPane();
    
    
    public abstract void addOnDoubleMouseClickedOnLeftTree(Consumer listener);
    
    public abstract void removeOnDoubleMouseClickedOnLeftTree(Consumer listener);    
    
    public abstract ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty();
    
    
    public abstract void changeFont(String font);
}
