/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.util.function.Consumer;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.SmartPropertiesData;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public interface UIController {

    /**
     * For adding new MenuItem inside the main Menubar of the application.
     * <p>
     * @param depencence Class
     * @param category MenuCategory
     * @param items MenuItem[]
     */
    public void addMenuItems(Class depencence, MenuCategory category, MenuItem[] items);

    public void clearMenuItems(Class depencence, MenuCategory category);

    public void addToolbarContainerTabListener(Consumer<MenuCategory> listener);

    public void removeToolbarContainerTabListener(Consumer<MenuCategory> listener);

    public void addCustomKeyCombinations(KeyCombination key, Consumer action);

    public void nextTab();

    public void prevTab();

    public void selectTab(int index);

    public void selectLast();

    /**
     * This will help for adding new tab on preferred Connection. This will be
     * at the right-centered pane of main panel.
     * <p>
     * @param tab Tab
     * @param session ConnectionSession
     */
    public void addTab(Tab tab, ConnectionSession session);

    /**
     * For getting specific selected Tab from the connection pane. Used while
     * user does the context Menu click action.
     * <p>
     * @param session
     * @return TreeItem
     */
    public ReadOnlyObjectProperty<Tab> getSelectedConnectionTab(ConnectionSession session);

    public TabContentController getTabControllerBySession(ConnectionSession session);

    /**
     * For making the tab selected and active
     * <p>
     * @param tab
     */
    public void showTab(Tab tab, ConnectionSession session);

    public Tab getTab(String id, String text, ConnectionSession session);

    /**
     * For getting specific selected TreeItem from the left pane. Used while
     * user does the context Menu click action.
     * <p>
     * @param session
     * @return TreeItem
     */
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem(ConnectionSession session);

    /**
     * For refreshing specific Tree from the left pane. Used while user does the
     * context Menu click action or in toollbar.
     *
     * @param session
     */
    public void refreshTree(ConnectionSession session, boolean alertDatabaseCount);

    /**
     * For hiding left pane. Used while user does the context Menu click action
     * or in toollbar.
     *
     * @param session
     */
    public void toggleLeftPane();

    public void hideContentPane(ConnectionSession session, boolean visibility);

    public void hideResultPane(ConnectionSession session, boolean visibility);

    /**
     * For using current connection session id when user does the context Menu
     * click action or toollbar.
     *
     * @return ReadOnlyObjectProperty<ConnectionSession> selected connection
     * session
     */
    public ReadOnlyObjectProperty<ConnectionSession> getSelectedConnectionSession();

    public ModuleController getModuleByClassName(String moduleClassname);

    /**
     * The main javafx Stage class of the application.
     * <p>
     * @return Stage
     */
    public Stage getStage();

    public void addTabInConnection(Tab tab, int i);

    public void closeAllTabs();

    public void closeTab(TabContentController tcc);

    public void addTabToBottomPane(ConnectionSession session, Tab... tab);

    public void removeTabFromBottomPane(ConnectionSession session, Tab... tab);

    public DisconnectPermission disconnect(ConnectionSession session, DisconnectPermission permission);

    /**
     * it is true only if selected tab is not a query tab in center tabpane
     *
     * @return
     */
    public BooleanBinding centerTabForQueryTabBooleanProperty();

    public BooleanBinding centerTabForOnlyQueryTabBooleanProperty();

    public ObjectBinding selectedTabProperty();

    public void reachHistoryTab();

    public void reachFeqTab();

    public void saveSessions();

    public void saveSession(ConnectionSession session);

    public void saveSessionAs(ConnectionSession session);

    public void saveSessionTab(ConnectionSession session, String tab);

    public void openSessionPoint();

    public void endSession(ConnectionSession session);

    public void aboutPage();

    public void addBackgroundWork(WorkProc wp);

    public void togleTabPaneLayerToRight(boolean bool);

    public HBox getApplicationFooter();

    public void hideApplicationFooter();

    public void showApplicationFooter();

    public SmartPropertiesData getSmartProperties();
}
