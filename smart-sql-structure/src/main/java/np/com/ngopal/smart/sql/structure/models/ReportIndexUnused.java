
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportIndexUnused implements FilterableData {
    
    private final SimpleStringProperty database;
    private final SimpleStringProperty tableName;
    private final SimpleStringProperty indexName;
    
 
    public ReportIndexUnused(
            String database, 
            String tableName, 
            String indexName) {
        
        this.database = new SimpleStringProperty(database);
        this.tableName = new SimpleStringProperty(tableName);
        this.indexName = new SimpleStringProperty(indexName);
    }

    public String getDatabase() {
        return database.get();
    }

    public String getTableName() {
        return tableName.get();
    }

    public String getIndexName() {
        return indexName.get();
    }

    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getDatabase() != null ? getDatabase() : "", 
            getTableName() != null ? getTableName() : "",
            getIndexName() != null ? getIndexName() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDatabase(),
            getTableName(),
            getIndexName()
        };
    }
}