
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class WarningsResult {
    
    private final SimpleStringProperty level;
    private final SimpleLongProperty code;
    private final SimpleStringProperty message;
 
    public WarningsResult(String level, Long code, String message) {
        this.level = new SimpleStringProperty(level);
        this.code = new SimpleLongProperty(code);
        this.message = new SimpleStringProperty(message);
    }
 
        
    public String getLevel() {
        return level.get();
    }
    
    
    public Long getCode() {
        return code.get();
    }
    
    public String getMessage() {
        return message.get();
    }
}