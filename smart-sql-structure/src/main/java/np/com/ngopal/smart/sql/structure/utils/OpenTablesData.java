/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class OpenTablesData {
    private final SimpleStringProperty database;
        private final SimpleStringProperty table;
        private final SimpleObjectProperty<Integer> inUse;
        private final SimpleObjectProperty<Integer> nameLocked;
        
        public OpenTablesData() {
            this.database = new SimpleStringProperty();
            this.table = new SimpleStringProperty();
            this.inUse = new SimpleObjectProperty<>();
            this.nameLocked = new SimpleObjectProperty<>();
        }
        
        public SimpleStringProperty database() {
            return database;
        }
        
        public SimpleStringProperty table() {
            return table;
        }
        
        public SimpleObjectProperty<Integer> inUse() {
            return inUse;
        }
        
        public SimpleObjectProperty<Integer> nameLocked() {
            return nameLocked;
        }
}
