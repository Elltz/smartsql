/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.structure.utils.Flags;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class MemoryMetrics extends Metric {
            
    public MemoryMetrics() {
        super("MEMORY METRIC", "MEMORY METRIC CHART \n"
                + "This metric measures your memory capacity at any given time or current time. \n "
                + "You can identify how your remote server is doing is terms of memory capacity.\n"
                + "This is specific to the current \"FREE\" Memory on your server, unallocated memory space of your server");
    }
    
    @Override
    public long getMaxYValue() {
        return convertToUnitBytes(getMemoryMetricExtract().getTotalMemory(), Flags.MEGABYTE);
    }

    @Override
    public XYChart.Series<Number, Number>[] getXYChartSeries() {
        XYChart.Series<Number, Number>[] series = new XYChart.Series[1];
       
       series[0] = getNewXYChart();
       series[0].setName("USED MEMORY");
        
        return series;
    }

    @Override
    public void updateUI(long timeInMillis, MetricExtract extract) {
        super.updateUI(timeInMillis, extract);
        if(extract != null){
            for(XYChart.Series<Number,Number>  serie : data){
                XYChart.Data<Number, Number> myData = new XYChart.Data();
                myData.setXValue(timeInMillis);
                myData.setYValue(convertToUnitBytes(getMemoryMetricExtract().getTotalMemory() -
                            getMemoryMetricExtract().getFreeMemory(), Flags.MEGABYTE));
                serie.getData().add(myData);
            }
        }
    }
    
    @Override
    public StringConverter<Number> yConverter() {
        return new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return String.valueOf(object.doubleValue()) + " MB";
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string.split(" ")[0]);
            }
        };
    }
    
    private MemoryMetricExtract getMemoryMetricExtract(){
        return (MemoryMetricExtract) metricExtract;
    }

    @Override
    public int getYTickUnitCount() {
        return 5;
    }

    @Override
    public String[] getChartColors() {
        return new String[]{"green"};
    }
    
}
