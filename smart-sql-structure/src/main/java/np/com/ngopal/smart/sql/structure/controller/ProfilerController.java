package np.com.ngopal.smart.sql.structure.controller;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.MySQLProfilerStorageProvider;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ProfilerController extends BaseController implements Initializable {
    
    private enum ProfilerPollInterval {
        VERY_SHORT("Very short", new BigDecimal("0.1")),
        SHORT("Short ", new BigDecimal("0.25")),
        NORMAL("Normal", new BigDecimal("1")),
        LONG("Long", new BigDecimal("3")),
        VERY_LONG("Very long", new BigDecimal("10"));
        
        private final String title;
        private final BigDecimal value;
        
        ProfilerPollInterval(String title, BigDecimal value) {
            this.title = title;
            this.value = value;
        }
        
        public static ProfilerPollInterval of(BigDecimal value) {
            if (VERY_SHORT.value.compareTo(value) == 0) {
                return VERY_SHORT;
            } else if (SHORT.value.compareTo(value) == 0) {
                return SHORT;
            } else if (NORMAL.value.compareTo(value) == 0) {
                return NORMAL;
            } else if (LONG.value.compareTo(value) == 0) {
                return LONG;
            } else if (VERY_LONG.value.compareTo(value) == 0) {
                return VERY_LONG;
            }
            
            return null;
        }
        
        @Override
        public String toString() {
            return title;
        }
    }
    
        
    @Setter
    ProfilerSettingsService profilerService;
    @Setter
    ConnectionParamService paramsService;
    @Setter
    ProfilerDataManager profilerDataManager;
    
    @FXML
    private ComboBox<ProfilerPollInterval> pollIntervalBox;
    @FXML
    private TextField slowQueryThresholdField;
    @FXML
    private TextField dataRotationField;
    @FXML
    private TextField grapshRefreshField;
    
    @FXML
    private RadioButton h2StorageType;
    @FXML
    private RadioButton mysqlStorageType;
    
    @FXML
    private GridPane mysqlPane;
    @FXML
    private ComboBox<ConnectionParams> connectionCombobox;
    @FXML
    private ComboBox<String> databaseCombobox;
    

    @FXML
    private AnchorPane mainUI;

    private ProfilerSettings profilerSettings;

    private boolean wasChanged = false;
    
    public boolean wasChanged() {
        return wasChanged;
    }
    
    @FXML
    public void saveAction(ActionEvent event) {
        
        if (mysqlStorageType.isSelected() && (connectionCombobox.getSelectionModel().getSelectedItem() == null || databaseCombobox.getEditor().getText().trim().isEmpty())) {
            DialogHelper.showInfo(getController(), "Error", "Choose connection and input database for MySQL storage type", null);
            return;
        }
        
        ProfilerPollInterval selectedPollInterval = pollIntervalBox.getSelectionModel().getSelectedItem();
        if (selectedPollInterval == null) {
            DialogHelper.showError(getController(), "Error", "Choose poll interval!", getController().getSelectedConnectionSession().get().getConnectionParam(), null);
            return;
        }
        
        Long slowQueryThreshold;
        try {
            slowQueryThreshold = Long.parseLong(slowQueryThresholdField.getText());
        } catch (NumberFormatException ex) {
            DialogHelper.showError(getController(), "Error", "the Slow query threshold must be not empty integer!", null);
            return;
        }
        
        Long dataRotation;
        try {
            dataRotation = Long.parseLong(dataRotationField.getText());
        } catch (NumberFormatException ex) {
            DialogHelper.showError(getController(), "Error", "the Data rotation must be not empty integer!", null);
            return;
        }
        
        Long grapshRefresh;
        try {
            grapshRefresh = Long.parseLong(grapshRefreshField.getText());
        } catch (NumberFormatException ex) {
            DialogHelper.showError(getController(), "Error", "Graphs refresh must be not empty integer!", getController().getSelectedConnectionSession().get().getConnectionParam(), null);
            return;
        }
        
        profilerSettings.setPollInterval(selectedPollInterval.value);
        profilerSettings.setSlowQueryThreshold(slowQueryThreshold);
        profilerSettings.setDataRotation(dataRotation);
        profilerSettings.setDataRefresh(grapshRefresh);
        
        if (h2StorageType.isSelected()) {
            profilerSettings.setStorageType(0);
        } else {
            profilerSettings.setStorageType(1);
        }
        
        ConnectionParams cp = connectionCombobox.getSelectionModel().getSelectedItem();
        profilerSettings.setConnectionId(cp != null ? cp.getId() : null);
        profilerSettings.setDatabaseName(databaseCombobox.getEditor().getText());
        
        profilerService.save(profilerSettings);
        wasChanged = true;
        
        if (profilerSettings.getStorageType() == 0 && !profilerDataManager.getStorageType().equals(QueryProvider.DB__H2)) {
            profilerDataManager.activateStorage(QueryProvider.DB__H2);
            
        } else if (profilerSettings.getStorageType() == 1) {
            MySQLProfilerStorageProvider storage = (MySQLProfilerStorageProvider) profilerDataManager.activateStorage(QueryProvider.DB__MYSQL);
            try {
                storage.init(cp, profilerSettings.getDatabaseName());
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        
        getStage().close();
    }

    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pollIntervalBox.getItems().setAll(ProfilerPollInterval.values());
        
        h2StorageType.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            mysqlPane.setDisable(newValue);
        });
        
        new AutoCompleteComboBoxListener<>(connectionCombobox);
        new AutoCompleteComboBoxListener<>(databaseCombobox);
    }

    public void init() {
        
        profilerSettings = profilerService.getProfilerSettings();
        if (profilerSettings == null) {
            profilerSettings = new ProfilerSettings();
        }
                
        pollIntervalBox.getSelectionModel().select(ProfilerPollInterval.of(profilerSettings.getPollInterval()));
        
        slowQueryThresholdField.setText("" + profilerSettings.getSlowQueryThreshold());
        dataRotationField.setText("" + profilerSettings.getDataRotation());
        grapshRefreshField.setText("" + profilerSettings.getDataRefresh());
        
        if (profilerSettings.getStorageType() == 0) {
            h2StorageType.setSelected(true);
        } else {
            mysqlStorageType.setSelected(true);
        }
            
        
        connectionCombobox.setConverter(new StringConverter<ConnectionParams>() {
            @Override
            public String toString(ConnectionParams object) {
                return object != null ? object.getName() : "";
            }
            
            @Override
            public ConnectionParams fromString(String string) {
                for (ConnectionParams cp: connectionCombobox.getItems()) {
                    if (cp.getName().equals(string)) {
                        return cp;
                    }
                }

                return null;
            }
        });
        connectionCombobox.getItems().addAll(paramsService.getAll());
        
        Collections.sort(connectionCombobox.getItems(), (ConnectionParams o1, ConnectionParams o2) -> {
            return o1.getName().compareTo(o2.getName());
        });
        
        connectionCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ConnectionParams> observable, ConnectionParams oldValue, ConnectionParams newValue) -> {
            
            databaseCombobox.getItems().clear();
            if (newValue != null) {            
                makeBusy(true);
                Thread th = new Thread(() -> {
                    try {
                        MysqlDBService db = new MysqlDBService(null);
                        db.connect(newValue);

                        List<Database> databases = db.getDatabases();

                        if (databases != null) {
                            for (Database database: databases) {
                                databaseCombobox.getItems().add(database.getName());
                            }
                        }
                    } catch (Throwable ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage());
                    }
                    makeBusy(false);
                });
                th.setDaemon(true);
                th.start();
            }
        });
        
        ConnectionParams cp = null;
        if (profilerSettings.getConnectionId() != null) {
            for (ConnectionParams cp0: connectionCombobox.getItems()) {
                if (cp0.getId().equals(profilerSettings.getConnectionId())) {
                    cp = cp0;
                    break;
                }
            }
        }
        
        connectionCombobox.getSelectionModel().select(cp);
        
        if (profilerSettings.getDatabaseName() != null) {
            databaseCombobox.getEditor().setText(profilerSettings.getDatabaseName());
        }
    }


    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
} 