/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.provider;

import javafx.scene.image.Image;
import lombok.AccessLevel;
import lombok.Getter;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public abstract class DefaultImageProvider {
    
    public abstract String getImagePathRoute();
    
    public Image getNonDeclaredImage(String imageFileName){
        return new Image(getImagePathRoute() + imageFileName);
    }
    
    public Image getNonDeclaredImage(String imageFileName, double width, double height){
        return new Image(getImagePathRoute() + imageFileName, width,height, false,true);
    }
    
    public static int IMAGE_SIZE_WIDTH = 27;    
    public static int IMAGE_SIZE_HEIGHT = 27;
}
