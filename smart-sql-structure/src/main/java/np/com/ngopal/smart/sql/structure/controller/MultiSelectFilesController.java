package np.com.ngopal.smart.sql.structure.controller;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class MultiSelectFilesController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    @FXML
    private HBox dragAndDropPane;
    
    @FXML
    private VBox fileChooserContainer;
    
    private Stage dialog;
    
    
    private final List<FileChooserRow> rows = new ArrayList<>();
    
    private List<File> selectedFiles;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        selectedFiles = null;
        getStage().close();
    }
    
    @FXML
    public void okAction(ActionEvent event) {
        selectedFiles = new ArrayList<>();
        for (FileChooserRow row: rows) {
            if (row.userFile != null) {
                selectedFiles.add(row.userFile);
            }
        }
        
        getStage().close();
    }
    
    @FXML
    public void addChooserAction(ActionEvent event) {
        
        FileChooserRow row = new FileChooserRow();
        fileChooserContainer.getChildren().add(row);
        
        rows.add(row);
    }
    
    @FXML
    public void clearDataAction(ActionEvent event) {
        
        DialogResponce responce = DialogHelper.showConfirm(getController(), "Clear data", "Do you Want to Remove all Content in the Dashboard?");
        
        if (responce == DialogResponce.NO || responce == DialogResponce.CANCEL) {
            return;
        }
        
        for (FileChooserRow row: rows) {
            row.clear();
        }
        
        int size = rows.size();
        while (size > 5) {
            FileChooserRow row = rows.remove(size - 1);
            fileChooserContainer.getChildren().remove(row);
            
            size = rows.size();
        }
        
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addChooserAction(null);
        addChooserAction(null);
        addChooserAction(null);
        addChooserAction(null);
        addChooserAction(null);
        
        mainUI.setOnDragOver(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                if (event.getGestureSource() != mainUI && event.getDragboard().hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
                
                dragAndDropPane.toFront();
            }
        });
        
        mainUI.setOnDragDone(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                dragAndDropPane.toBack();
            }
        });
        
        mainUI.setOnDragExited(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                dragAndDropPane.toBack();
            }
        });

        mainUI.setOnDragDropped(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    
                    for (File f: db.getFiles()) {
                        
                        boolean added = false;
                        for (FileChooserRow row: rows) {
                            if (row.userFile == null) {
                                row.setFile(f);
                                added = true;
                                break;
                            }
                        }
                        
                        if (!added) {
                            addChooserAction(null);
                            rows.get(rows.size() - 1).setFile(f);
                        }
                    }
                    
                    success = true;
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);

                event.consume();
                
                dragAndDropPane.toBack();;
            }
        });
    }
    
    public List<File> getSelectedFiles() {
        return selectedFiles;
    }
    
    public void init(Window win) {        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(690);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Multi chooser");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();
        
    }
    
    private class FileChooserRow extends HBox {
        
        private File userFile;
        
        private TextField field = new TextField();
        private Button button = new Button("Browse");
        
        public FileChooserRow() {
            
            setSpacing(10);
            
            field.setPromptText("Select log file for uploading...");
            field.setEditable(false);
            button.setPrefSize(80, 25);
            button.setMinSize(80, 25);
            button.setMaxSize(80, 25);
            
            getChildren().addAll(field, button);
            
            field.setMaxWidth(Double.MAX_VALUE);
            HBox.setHgrow(field, Priority.ALWAYS);
            
            field.setFocusTraversable(false);
            button.setFocusTraversable(false);
            
            button.setOnAction((ActionEvent event) -> {
                
                FileChooser openFileChooser = new FileChooser();
//                openFileChooser.getExtensionFilters().addAll(
//                        new FileChooser.ExtensionFilter("Log Files", "*.log"),
//                        new FileChooser.ExtensionFilter("All Files", "*.*"));
                
                setFile(openFileChooser.showOpenDialog(getStage()));
            });
        }
        
        public void setFile(File f) {
            userFile = f;
            if (userFile != null) {
                field.setText(userFile.getAbsolutePath());
            }
        }
        
        public void clear() {
            userFile = null;
            field.setText("");
        }
    }

    
    
    @Override
    public Stage getStage() {
        return (Stage) dialog;
    }
}
