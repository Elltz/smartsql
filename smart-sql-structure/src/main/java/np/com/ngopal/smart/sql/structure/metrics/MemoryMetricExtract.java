/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class MemoryMetricExtract extends MetricExtract{
    
    private String MAX_MEMORY = "max";
    private String FREE_MEMORY = "free";
    private String AVAI_MEMORY = "avai";
    
    public MemoryMetricExtract(MetricTypes type, String allResponseToParse) {
        super(type, allResponseToParse);
    }

    public MemoryMetricExtract(MetricTypes type) {
        super(type);
    }

    @Override
    public void extract() {
        super.extract();
        if (response.startsWith("top")) {

        } else {
            String[] lines = response.trim().split(System.lineSeparator());
            
            for(int i = 0; i < lines.length; i++){
                
                String[] pairs = retrieveKeyValueFromMainLine(lines[i]);
                data.put(pairs[0], pairs[1]);
                
                switch(i){
                    case 0:
                        MAX_MEMORY = pairs[0];
                        break;
                    case 1:
                        FREE_MEMORY = pairs[0];
                        break;
                    case 2:
                        AVAI_MEMORY = pairs[0];
                        break;                             
                }
            }
        }
    }
    
    private String retrieveValueFromMainLine(String a){
        return a.substring(a.lastIndexOf(':') + 1).trim().split(" ")[0];
    }
    
    private String[] retrieveKeyValueFromMainLine(String a){
        int semiColonPos = a.lastIndexOf(':');
        String key = a.substring(0, semiColonPos);
        String val = a.substring(semiColonPos +1).trim().split(" ")[0];
        return new String[]{key,val};
    }
    
    // in bytes
    public Long getTotalMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(MAX_MEMORY)) * 1024;
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
    public Long getFreeMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(FREE_MEMORY)) * 1024;
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
    public Long getAvailableMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(AVAI_MEMORY)) * 1024;
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
}
