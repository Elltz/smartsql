/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import java.util.function.BiConsumer;
import java.util.function.Function;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import lombok.Getter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryTableCell<T, S> extends TableCell<T, S> {
    private final int maxLength;
    private final BiConsumer<QueryTableCell.QueryTableCellData, S> actionFunction;
    private final Function<S, String> lengthFunction;
    
    protected Label label;
    
    public QueryTableCell(int maxLength, Function<S, String> lengthFunction, BiConsumer<QueryTableCell.QueryTableCellData, S> actionFunction) {
        this.maxLength = maxLength;
        this.actionFunction = actionFunction;
        this.lengthFunction = lengthFunction;
        setAlignment(Pos.CENTER);
    }

    @Override
    protected void updateItem(S item, boolean empty) {
        super.updateItem(item, empty); 
        setText("");

        int length = item != null ? item.toString().length(): 0;
        if (length > maxLength) {
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            setStyle("-fx-padding: 0;");
            
            String itemStr = item != null ? item.toString() : "";
            label = new Label(itemStr.startsWith("x'") && itemStr.endsWith("'") ? "(Binary / Image)" : itemStr.replaceAll("\r", "").replaceAll("\n", ""));
            label.setAlignment(Pos.CENTER_LEFT);
            label.setMaxSize(Double.MAX_VALUE, Label.USE_COMPUTED_SIZE);
            label.styleProperty().bind(styleProperty());

            Button b = new Button(lengthFunction.apply(item));
            b.setPadding(new Insets(1, 2, 1, 2));
            b.setAlignment(Pos.CENTER_RIGHT);
            b.setPrefWidth(40);
            //b.setStyle("-fx-padding: 0em 0.833333em 0em 0.833333em;");
            b.prefHeightProperty().bind(label.prefHeightProperty());
            b.setMaxHeight(Button.USE_PREF_SIZE);
            b.setFocusTraversable(false);
            b.setOnAction((ActionEvent event) -> {
                
                if (actionFunction != null) {                    
                    QueryTableCellData data = new QueryTableCellData();
                    data.button = b;
                    data.row = getIndex();
                    data.column = getTableView().getColumns().indexOf(getTableColumn());
                    
                    actionFunction.accept(data, item);
                }
            });

            HBox box = new HBox(label, new Group(b));
            box.getStyleClass().add("table-cell-hbox");
            box.setAlignment(Pos.CENTER_LEFT);
            box.setMaxHeight(USE_PREF_SIZE);            
            HBox.setHgrow(label, Priority.ALWAYS);
            setGraphic(box);            
        } else {
            label = null;
            getStyleClass().add("table-cell-label");
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            this.setText(item != null ? item.toString() : "");
        }
    }

    @Override
    public void updateSelected(boolean selected) {
        super.updateSelected(selected);
    }    
    
    @Getter
    public class QueryTableCellData {
        int row;
        int column;
        Button button;
    }
}