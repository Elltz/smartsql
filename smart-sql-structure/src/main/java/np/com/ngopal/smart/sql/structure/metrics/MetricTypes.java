/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public enum MetricTypes {
 
    NETWORK_METRIC ,
    DISK_IO_METRIC ,
    MEMORY_METRIC ,
    SWAP_MEMORY_METRIC,
    DISK_LATENCY_METRIC,
    CPU_METRIC;
    
    private String params;
    public String getParams() {
        return params;
    }
    
    
    
    public static MetricTypes getType(String delimiterReference){
        if(delimiterReference.equals(MetricTypes.CPU_METRIC.name())){
            return MetricTypes.CPU_METRIC;
        }else if(delimiterReference.equals(MetricTypes.DISK_IO_METRIC.name())){
            return MetricTypes.DISK_IO_METRIC;
        }else if(delimiterReference.equals(MetricTypes.DISK_LATENCY_METRIC.name())){
            return MetricTypes.DISK_LATENCY_METRIC;
        }else if(delimiterReference.equals(MetricTypes.MEMORY_METRIC.name())){
            return MetricTypes.MEMORY_METRIC;
        }else if(delimiterReference.equals(MetricTypes.NETWORK_METRIC.name())){
            return MetricTypes.NETWORK_METRIC;
        }else if(delimiterReference.equals(MetricTypes.SWAP_MEMORY_METRIC.name())){
            return MetricTypes.SWAP_MEMORY_METRIC;
        }else{
            return null;
        }
    }
    
}
