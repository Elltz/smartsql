/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.OsServerSpecs;
import np.com.ngopal.smart.sql.structure.metrics.CpuMetrics;
import np.com.ngopal.smart.sql.structure.metrics.DiskMetrics;
import np.com.ngopal.smart.sql.structure.metrics.MemoryMetrics;
import np.com.ngopal.smart.sql.structure.metrics.Metric;
import np.com.ngopal.smart.sql.structure.metrics.MetricExtract;
import np.com.ngopal.smart.sql.structure.metrics.MetricExtractSerializeableBundle;
import np.com.ngopal.smart.sql.structure.metrics.MetricListener;
import static np.com.ngopal.smart.sql.structure.metrics.MetricTypes.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.MetricEntityContainerController;
import np.com.ngopal.smart.sql.structure.controller.MysqlProfilingController;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;


/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class MetricMonitorController extends BaseController implements Initializable, ProfilerListener, MetricListener {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private VBox contentHolder;
    private OsServerSpecs osServerSpecs;
    private SimpleStringProperty osTextProperty = new SimpleStringProperty("");
    private SimpleBooleanProperty metricReadyStatusProperty = new SimpleBooleanProperty(false);
    private RemoteMetricExtractor remoteMetricExtractor;
    private HashMap<MetricTypes, Metric> metrics = new HashMap();
    private SimpleBooleanProperty monitoringStatusProperty = new SimpleBooleanProperty(false);
    private OsParamsConfigurationUpdater osParamsConfigurationUpdater;    
    private MetricExtractSerializeableBundle extractSerializeableBundle;
    private ProfilerInnerControllerWorkProcAuth metricMonitorControllerWorkProcAuthority;

    @Setter
    @Getter
    private ProfilerTabContentController parentController;

    private void changeMonitoringStatus(boolean a) {
        if (a) {
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    remoteMetricExtractor.connectAndExecuteServer();
                }
            });
        } else if (!a && monitoringStatusProperty.get()) {
            remoteMetricExtractor.stop();
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {}

                @Override
                public void run() { 
                    try { extractSerializeableBundle.serializeDataToBundler(); }
                    catch (Exception e) { e.printStackTrace(); }
                }
            });
        }
    }

    @Override
    public void zoomInAction() {
        changeMetricScale();
    }

    @Override
    public void scrollBackward() {
    }

    @Override
    public void scrollForward() {
    }

    @Override
    public void exportToXls(
            List<String> sheets,
            List<ObservableList<ObservableList>> totalData,
            List<LinkedHashMap<String, Boolean>> totalColumns,
            List<List<Integer>> totalColumnWidths,
            List<byte[]> images,
            List<Integer> heights,
            List<Integer> colspans) {

    }
    
    private void changeMetricScale(){
        changeMetricScale(ChartScale.values()[getParentController().currentScaleIndex()
                .get()].getValue() / ((double)TimeUnit.HOURS.toMillis(1)));
    }
    
    private void changeMetricScale(double increment){         
        for(Metric m : metrics.values()){
            m.setScaleValue(increment);
        }
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        activateBlackThemeForMetric();        
        mainUI.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
    }

    @Override
    public void showProfilerData(Date fromDate) {
        //the from date here is the minimum
        long max = System.currentTimeMillis();
        for (MetricEntityContainerController m : MetricEntityContainerController.allMetricsController) {
            if (m.isOEntityController()) {
                OsMetricComponentsController osm = (OsMetricComponentsController) m;
                osm.manualAdjustTimeBounds(fromDate.getTime(), max);
            }
        }
    }
    
    private void createRemoteConnectorInstance() {
        remoteMetricExtractor = new RemoteMetricExtractor(parentController.getParams(),
                new MetricTypes[]{MetricTypes.CPU_METRIC,
                    MetricTypes.DISK_IO_METRIC,
                    MetricTypes.MEMORY_METRIC,
                    MetricTypes.NETWORK_METRIC,
                    MetricTypes.SWAP_MEMORY_METRIC});
        remoteMetricExtractor.setMetricListener(this);
    }

    private void initConnections() {
        
        if (remoteMetricExtractor.testConnection()) {
            metricReadyStatusProperty.set(true);
            initializationPreparations();
            try { loadMetricHistory(); }catch(Exception e){ e.printStackTrace(); }
        } else { metricReadyStatusProperty.set(false); }
    }

    /**
     * These should be called/initialize before the session is connected
     */
    private void initializationPreparations() {
        metrics.clear();
        for (MetricTypes mt : remoteMetricExtractor.getBindingMetricTypes(false)) {
            if (metrics.keySet().contains(mt)) {
                continue;
            }
            Metric m = getRespectiveMetric(mt);
            OsMetricComponentsController omcc = loadOsMetricComponentsControllers();
            omcc.applyBlackTheme();
            omcc.setMetric(m);
            metrics.put(mt, m);
        }
        Platform.runLater(() -> areaChartPreparation());
    }
    
    private void loadMetricHistory() throws Exception {
        extractSerializeableBundle = new MetricExtractSerializeableBundle(StandardApplicationManager.getInstance()
                .getApplicationPublicFilesAndStorageDirectory("Metric Bundlers").getPath(),
                parentController.getParams().getId());
        for (MetricTypes mt : MetricTypes.values()) {
            SortedMap<Long, String> sortedMap = extractSerializeableBundle.getFullCountHistoryData(mt);
            if (sortedMap != null && !sortedMap.isEmpty()) {
                MetricExtract me = MetricExtract.getMetricExtract(mt);
                for (Map.Entry<Long, String> en : sortedMap.entrySet()) {
                    me.update(en.getValue());
                    synchronized (this) {
                        Platform.runLater(() -> {
                            synchronized (MetricMonitorController.this) {
                                metrics.get(mt).updateUI(en.getKey(), me);
                                MetricMonitorController.this.notify();
                            }
                        });
                        this.wait();
                    }
                }
            }
        }
    }

    private Metric getRespectiveMetric(MetricTypes mt) {
        switch (mt) {
            case CPU_METRIC:
                return new CpuMetrics();
            case DISK_IO_METRIC:
                return new DiskMetrics();
            case NETWORK_METRIC:
                return new NetworkMetrics();
            case MEMORY_METRIC:
                return new MemoryMetrics();
            case SWAP_MEMORY_METRIC:
                return new SwapMemoryMetric();
            default:
                return null;
        }
    }

    private OsMetricComponentsController loadOsMetricComponentsControllers() {
        try {
            OsMetricComponentsController t = StandardBaseControllerProvider.getController(getController(), "MetricEntityContainer");
            t.activateMetric();
            return t;
        } catch (Exception ex) {
            Logger.getLogger(MysqlProfilingController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void destroy() {
        changeMonitoringStatus(false);
        System.gc();
    }

    private void areaChartPreparation() {
        addAllMetricChildrenToParent();
    }

    private void addAllMetricChildrenToParent() {
        contentHolder.getChildren().clear();
        System.gc();
        int count = 0;
        for (MetricEntityContainerController m : MetricEntityContainerController.allMetricsController) {
            if (m.isOEntityController()) {
                HBox parent = null;
                if (count % 2 == 0) {
                    parent = new HBox(10);
                    parent.setPrefWidth(Pane.USE_COMPUTED_SIZE);
                    parent.setMaxWidth(Double.MAX_VALUE);
                    parent.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS);
                    contentHolder.getChildren().add(parent);
                } else {
                    parent = (HBox) contentHolder.getChildren().get(contentHolder.getChildren().size() - 1);
                }
                m.getUI().setPrefWidth(Pane.USE_COMPUTED_SIZE);
                m.getUI().setMaxWidth(Double.MAX_VALUE);
                HBox.setHgrow(m.getUI(), Priority.ALWAYS);
                m.getUI().setPrefHeight(260);
                parent.getChildren().add(m.getUI());
                count++;
            }
        }
    }

    @Override
    public void saveProfilerData(ProfilerData pd) {
    }

    @Override
    public void openData(ProfilerData data) {
    }

    private void activateBlackThemeForMetric() {
        contentHolder.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
    }

    @Override
    public void zoomOutAction() {
        changeMetricScale();
    }

    @Override
    public void showAllAction() {
        //implement if need
        System.out.println();
    }

    @Override
    public void startProfiler(Date startDate) {
        if(metricMonitorControllerWorkProcAuthority == null){
            metricMonitorControllerWorkProcAuthority = new ProfilerInnerControllerWorkProcAuth() {
                @Override
                public boolean shouldExecuteWorkProc() {
                    return false;
                }
            };
        }
        changeMonitoringStatus(!monitoringStatusProperty.get());
        monitoringStatusProperty.set(!monitoringStatusProperty.get());
    }

    @Override
    public void clearProfilerData() {
        if(extractSerializeableBundle != null){
            extractSerializeableBundle.deleteOutputFile();
        }
    }

    @Override
    public void stopProfiler() {
        changeMonitoringStatus(!monitoringStatusProperty.get());
        monitoringStatusProperty.set(!monitoringStatusProperty.get());
    }

    public void createConnection() throws SQLException {
        createRemoteConnectorInstance();
        initConnections();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                activityState(true);
            }
        });
    }

    @Override
    public void userChartsChanged() {
    }

    public boolean isReady() {
        return metricReadyStatusProperty.get();
    }

    @Override
    public void onDone() {

    }
    
    @Override
    public void onReceiveRemoteMetrics(MetricTypes mt, MetricExtract extract) {
        long executeTime = extractSerializeableBundle.addExtractData(mt, extract.toString());
        Platform.runLater(() -> {
            metrics.get(mt).updateUI(executeTime, extract);
        });
    }
    
    public void activityState(boolean state) {
        if (state && !isReady()) {

            try {
                if (osParamsConfigurationUpdater == null) {
                    osParamsConfigurationUpdater = StandardBaseControllerProvider.getController(getController(), "OsParamsConfigurationUpdater");
                    osParamsConfigurationUpdater.setParams(parentController.getParams());
                    osParamsConfigurationUpdater.setAuthenticationCodeInterface(new WorkProc() {
                        @Override
                        public void updateUI() {}

                        @Override
                        public void run() {
                            Platform.runLater(() -> osParamsConfigurationUpdater.makeBusy(true));
                            remoteMetricExtractor.updateParamDetails(parentController.getParams());
                            try { createConnection(); }catch(Exception e){e.printStackTrace();}
                            Platform.runLater(() -> osParamsConfigurationUpdater.makeBusy(false));
                        }
                    });

                    AnchorPane.setTopAnchor(osParamsConfigurationUpdater.getUI(), 0.0);
                    AnchorPane.setBottomAnchor(osParamsConfigurationUpdater.getUI(), 0.0);
                    AnchorPane.setRightAnchor(osParamsConfigurationUpdater.getUI(), 0.0);
                    AnchorPane.setLeftAnchor(osParamsConfigurationUpdater.getUI(), 0.0);
                    getUI().getChildren().add(osParamsConfigurationUpdater.getUI());
                }

                osParamsConfigurationUpdater.getUI().toFront();
                osParamsConfigurationUpdater.getUI().setVisible(true);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (state && isReady()) {
            if (osParamsConfigurationUpdater != null) {
                osParamsConfigurationUpdater.getUI().toBack();
                osParamsConfigurationUpdater.getUI().setVisible(false);
            }
            changeMonitoringStatus(monitoringStatusProperty.get());
        }
    }

    @Override
    public ProfilerInnerControllerWorkProcAuth getInnerControllerWorkProcAuthority() {
        return metricMonitorControllerWorkProcAuthority;
    }

}
