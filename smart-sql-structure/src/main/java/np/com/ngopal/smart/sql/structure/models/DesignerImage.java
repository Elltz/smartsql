package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerImage implements DesignerData, InvalidationListener {

    private DesignerErrDiagram parent;
    
    private final SimpleStringProperty uuid;
    
    private final SimpleStringProperty color;
    
    private final SimpleStringProperty name;
    private final SimpleStringProperty filename;
    
    private final SimpleDoubleProperty x;
    private final SimpleDoubleProperty y;
    private final SimpleDoubleProperty width;
    private final SimpleDoubleProperty height;
    
    private final SimpleStringProperty imageData;
    private final SimpleDoubleProperty imageDefaultWidth;
    private final SimpleDoubleProperty imageDefaultHeight;
    
    private final SimpleBooleanProperty aspectRatio;
    
    private final SimpleStringProperty layerUuid;
    
    private final SimpleStringProperty description;
    
    private final WeakInvalidationListener weakListner = new WeakInvalidationListener(this);
    
    public DesignerImage() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty("");
        name.addListener(weakListner);
        
        filename = new SimpleStringProperty("");
        filename.addListener(weakListner);
        
        color = new SimpleStringProperty("");
        color.addListener(weakListner);
        
        
        x = new SimpleDoubleProperty();
        x.addListener(weakListner);
        
        y = new SimpleDoubleProperty();
        y.addListener(weakListner);
        
        width = new SimpleDoubleProperty();
        width.addListener(weakListner);
        
        height = new SimpleDoubleProperty();
        height.addListener(weakListner);
        
        
        imageData = new SimpleStringProperty("");
        imageData.addListener(weakListner);
        
        imageDefaultWidth = new SimpleDoubleProperty();
        imageDefaultWidth.addListener(weakListner);
        
        imageDefaultHeight = new SimpleDoubleProperty();
        imageDefaultHeight.addListener(weakListner);
        
        aspectRatio = new SimpleBooleanProperty(false);
        aspectRatio.addListener(weakListner);
        
        layerUuid = new SimpleStringProperty("");
        layerUuid.addListener(weakListner);
        
        description = new SimpleStringProperty("");
        description.addListener(weakListner);
    }
    
    
    @Override
    public void invalidated(Observable observable) {
        if (parent != null) {
            parent.invalidated(observable);
        }
    }
    
    @Override
    public void setParent(DesignerErrDiagram parent) {
        this.parent = parent;
    }
    
    @Override
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    @Override
    public SimpleStringProperty name() {
        return name;
    }
    
    @Override
    public SimpleStringProperty description() {
        return description;
    }
    
    public SimpleStringProperty filename() {
        return filename;
    }
    
    public SimpleStringProperty color() {
        return color;
    }
    
    @Override
    public SimpleDoubleProperty x() {
        return x;
    }
    
    @Override
    public SimpleDoubleProperty y() {
        return y;
    }
    
    @Override
    public SimpleDoubleProperty width() {
        return width;
    }
    
    @Override
    public SimpleDoubleProperty height() {
        return height;
    }
    
    public SimpleStringProperty imageData() {
        return imageData;
    }
    
    public SimpleDoubleProperty imageDefaultWidth() {
        return imageDefaultWidth;
    }
    
    public SimpleDoubleProperty imageDefaultHeight() {
        return imageDefaultHeight;
    }
    
    public SimpleBooleanProperty aspectRatio() {
        return aspectRatio;
    }
    
    public SimpleStringProperty layerUuid() {
        return layerUuid;
    }
    
    @Override
    public String toString() {
        return name.get();
    }

    @Override
    public DesignerData copyData() {
        
        DesignerImage img = new DesignerImage();
        img.aspectRatio.set(aspectRatio.get());
        img.color.set(color.get());
        img.filename.set(filename.get());
        img.height.set(height.get());
        img.imageData.set(imageData.get());
        img.imageDefaultHeight.set(imageDefaultHeight.get());
        img.imageDefaultWidth.set(imageDefaultWidth.get());
        img.layerUuid.set(layerUuid.get());
        img.name.set(name.get());
        img.width.set(width.get());
        img.x.set(x.get());
        img.y.set(y.get());
        
        img.description.set(description.get());
        
        return img;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        map.put("filename", filename.get());
                
        map.put("color", color.get());
                
        map.put("x", x.get());
        map.put("y", y.get());
        map.put("width", width.get());
        map.put("height", height.get());
        
        map.put("imageData", imageData.get());
        
        map.put("imageDefaultWidth", imageDefaultWidth.get());
        map.put("imageDefaultHeight", imageDefaultHeight.get());
        map.put("aspectRatio", aspectRatio.get());
        
        map.put("layerUuid", layerUuid.get());
        
        map.put("description", description.get());
                
        return map;
    }
    
    
    public void fromMap(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set(getNotNullStringValue(map, "name"));
            filename.set(getNotNullStringValue(map, "filename"));
                        
            color.set(getNotNullStringValue(map, "color"));
            
            Double x0 = (Double)map.get("x");
            x.set(x0 != null ? x0 : -1);
            
            Double y0 = (Double)map.get("y");
            y.set(y0 != null ? y0 : -1);
            
            Double width0 = (Double)map.get("width");
            width.set(width0 != null ? width0 : -1);
            
            Double height0 = (Double)map.get("height");
            height.set(height0 != null ? height0 : -1);
            
            imageData.set(getNotNullStringValue(map, "imageData"));
            
            Double imageDefaultWidth0 = (Double)map.get("imageDefaultWidth");
            imageDefaultWidth.set(imageDefaultWidth0 != null ? imageDefaultWidth0 : width.get());
            
            Double imageDefaultHeight0 = (Double)map.get("imageDefaultHeight");
            imageDefaultHeight.set(imageDefaultHeight0 != null ? imageDefaultHeight0 : height.get());
            
            Boolean b = (Boolean) map.get("aspectRatio");
            aspectRatio.set(b != null ? b : false);
            
            layerUuid.set(getNotNullStringValue(map, "layerUuid"));
            
            description.set(getNotNullStringValue(map, "description"));
        }
    }

    @Override
    public List<DesignerProperty> getProperties() {
        
        List<DesignerProperty> list = new ArrayList<>();
        list.add(new DesignerProperty("Name", name, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Color", color, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("File Name", filename, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Aspect Ratio", aspectRatio, DesignerProperty.PropertyType.BOOLEAN, v -> Boolean.valueOf(v)));
        list.add(new DesignerProperty("X", x, DesignerProperty.PropertyType.DOUBLE, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Y", y, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Height", height, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Width", width, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        
        return list;
    }
    
    private String getNotNullStringValue(Map<String, Object> map, String key) {
        Object v = map.get(key);
        return v != null ? v.toString() : "";
    }
}
