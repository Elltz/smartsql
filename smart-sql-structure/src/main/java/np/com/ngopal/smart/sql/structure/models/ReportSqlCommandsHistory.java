
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportSqlCommandsHistory implements FilterableData {
    
    private final SimpleStringProperty schemaName;
    private final SimpleStringProperty statement;
    private final SimpleStringProperty sqlText;
    private final SimpleObjectProperty sumTimerWait;
    private final SimpleObjectProperty minTimerWait;
    private final SimpleObjectProperty avgTimerWait;
    private final SimpleObjectProperty countStar;
    private final SimpleObjectProperty sumRowsSent;
    private final SimpleObjectProperty sumRowsExamined;
    private final SimpleObjectProperty sumCreatedTmpDiskTables;
    private final SimpleObjectProperty sumCreatedTmpTables;
    
 
    public ReportSqlCommandsHistory(
            String schemaName, 
            String statement, 
            String sqlText, 
            Long sumTimerWait, 
            Long minTimerWait, 
            Long avgTimerWait, 
            Long countStar, 
            Long sumRowsSent,
            Long sumRowsExamined,
            Long sumCreatedTmpDiskTables,
            Long sumCreatedTmpTables) {
        
        this.schemaName = new SimpleStringProperty(schemaName);
        this.statement = new SimpleStringProperty(statement);
        this.sqlText = new SimpleStringProperty(sqlText);
        this.sumTimerWait = new SimpleObjectProperty(sumTimerWait);
        this.minTimerWait = new SimpleObjectProperty(minTimerWait);
        this.avgTimerWait = new SimpleObjectProperty(avgTimerWait);
        this.countStar = new SimpleObjectProperty(countStar);
        this.sumRowsSent = new SimpleObjectProperty(sumRowsSent);
        this.sumRowsExamined = new SimpleObjectProperty(sumRowsExamined);
        this.sumCreatedTmpDiskTables = new SimpleObjectProperty(sumCreatedTmpDiskTables);
        this.sumCreatedTmpTables = new SimpleObjectProperty(sumCreatedTmpTables);
    }

    public String getSchemaName() {
        return schemaName.get();
    }

    public String getStatement() {
        return statement.get();
    }

    public String getSqlText() {
        return sqlText.get();
    }

    public Long getSumTimerWait() {
        return (Long) sumTimerWait.get();
    }

    public Long getMinTimerWait() {
        return (Long) minTimerWait.get();
    }

    public Long getAvgTimerWait() {
        return (Long) avgTimerWait.get();
    }

    public Long getCountStar() {
        return (Long) countStar.get();
    }

    public Long getSumRowsSent() {
        return (Long) sumRowsSent.get();
    }

    public Long getSumCreatedTmpDiskTables() {
        return (Long) sumCreatedTmpDiskTables.get();
    }

    public Long getSumRowsExamined() {
        return (Long) sumRowsExamined.get();
    }

    public Long getSumCreatedTmpTables() {
        return (Long) sumCreatedTmpTables.get();
    }
 
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getSchemaName() != null ? getSchemaName() : "", 
            getStatement() != null ? getStatement() : "",
            getSqlText() != null ? getSqlText() : "", 
            getSumTimerWait() != null ? getSumTimerWait().toString() : "",
            getMinTimerWait() != null ? getMinTimerWait().toString() : "",
            getAvgTimerWait() != null ? getAvgTimerWait().toString() : "",
            getCountStar() != null ? getCountStar().toString() : "",
            getSumRowsSent() != null ? getSumRowsSent().toString() : "",
            getSumCreatedTmpDiskTables() != null ? getSumCreatedTmpDiskTables().toString() : "",
            getSumRowsExamined() != null ? getSumRowsExamined().toString() : "",
            getSumCreatedTmpTables() != null ? getSumCreatedTmpTables().toString() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getSchemaName(),
            getStatement(),
            getSqlText(),
            getSumTimerWait(),
            getMinTimerWait(),
            getAvgTimerWait(),
            getCountStar(), 
            getSumRowsSent(),
            getSumCreatedTmpDiskTables(),
            getSumRowsExamined(),
            getSumCreatedTmpTables()
        };
    }
}