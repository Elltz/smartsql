/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import np.com.ngopal.smart.sql.structure.models.TunerResult;
import np.com.ngopal.smart.sql.structure.utils.MySQLTunnerUtil;

/**
 *
 * @author Trezyl
 */
public class TunerStatusCell extends TableCell<TunerResult, TunerResult.Status> {        
        
        private static final int DEFAULT_MAX_LENGTH = 50;
        
        @Override
        protected void updateItem(TunerResult.Status item, boolean empty) {
            super.updateItem(item, empty); 
            
            if (!empty && item != null) {
                
                Image img = MySQLTunnerUtil.getStatusImage(item);                
                if (img != null) {
                    setGraphic(new ImageView(img));
                }
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setStyle("-fx-alignment: center");
                
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
                setText("");
            }
            
            this.setAlignment(Pos.CENTER_RIGHT);
        }
    }