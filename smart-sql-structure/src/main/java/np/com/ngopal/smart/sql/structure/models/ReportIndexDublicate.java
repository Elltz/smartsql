
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportIndexDublicate implements FilterableData {
    
    private final SimpleStringProperty database;
    private final SimpleStringProperty tableName;
    private final SimpleStringProperty redundantIndexName;
    private final SimpleStringProperty redundantIndexColumns;
    private final SimpleStringProperty redundantIndexType;
    private final SimpleStringProperty dominantIndexName;
    private final SimpleStringProperty dominantIndexColumns;
    private final SimpleStringProperty dominantIndexType;
    private final SimpleStringProperty comments;
    private final SimpleStringProperty subpartExists;
    private final SimpleStringProperty dropSql;
    
 
    public ReportIndexDublicate(
            String database, 
            String tableName, 
            String redundantIndexName, 
            String redundantIndexColumns, 
            String redundantIndexType, 
            String dominantIndexName, 
            String dominantIndexColumns, 
            String dominantIndexType,
            String comments,
            String subpartExists,
            String dropSql) {
        
        this.database = new SimpleStringProperty(database);
        this.tableName = new SimpleStringProperty(tableName);
        this.redundantIndexName = new SimpleStringProperty(redundantIndexName);
        this.redundantIndexColumns = new SimpleStringProperty(redundantIndexColumns);
        this.redundantIndexType = new SimpleStringProperty(redundantIndexType);
        this.dominantIndexName = new SimpleStringProperty(dominantIndexName);
        this.dominantIndexColumns = new SimpleStringProperty(dominantIndexColumns);
        this.dominantIndexType = new SimpleStringProperty(dominantIndexType);
        this.comments = new SimpleStringProperty(comments);
        this.subpartExists = new SimpleStringProperty(subpartExists);
        this.dropSql = new SimpleStringProperty(dropSql);
    }

    public String getDatabase() {
        return database.get();
    }

    public String getTableName() {
        return tableName.get();
    }

    public String getRedundantIndexName() {
        return redundantIndexName.get();
    }

    public String getRedundantIndexColumns() {
        return redundantIndexColumns.get();
    }

    public String getRedundantIndexType() {
        return redundantIndexType.get();
    }

    public String getDominantIndexName() {
        return dominantIndexName.get();
    }

    public String getDominantIndexColumns() {
        return dominantIndexColumns.get();
    }

    public String getDominantIndexType() {
        return dominantIndexType.get();
    }

    public String getComments() {
        return comments.get();
    }

    public String getSubpartExists() {
        return subpartExists.get();
    }

    public String getDropSql() {
        return dropSql.get();
    }
 
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getDatabase() != null ? getDatabase() : "", 
            getTableName() != null ? getTableName() : "",
            getRedundantIndexName() != null ? getRedundantIndexName() : "", 
            getRedundantIndexColumns() != null ? getRedundantIndexColumns() : "",
            getRedundantIndexType() != null ? getRedundantIndexType() : "",
            getDominantIndexName() != null ? getDominantIndexName() : "",
            getDominantIndexColumns() != null ? getDominantIndexColumns() : "",
            getDominantIndexType() != null ? getDominantIndexType() : "",
            getComments() != null ? getComments() : "",
            getSubpartExists() != null ? getSubpartExists() : "",
            getDropSql() != null ? getDropSql() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDatabase(),
            getTableName(),
            getRedundantIndexName(),
            getRedundantIndexColumns(),
            getRedundantIndexType(),
            getDominantIndexName(),
            getDominantIndexColumns(), 
            getDominantIndexType(),
            getComments(),
            getSubpartExists(),
            getDropSql()
        };
    }
}