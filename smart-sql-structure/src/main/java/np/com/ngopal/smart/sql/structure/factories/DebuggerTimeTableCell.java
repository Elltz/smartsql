/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import javafx.scene.control.TableCell;
import np.com.ngopal.smart.sql.structure.models.DebugTimeProvider;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class DebuggerTimeTableCell extends TableCell<DebugTimeProvider, DebugTimeProvider> {

        String color = "white";

        @Override
        protected void updateItem(DebugTimeProvider item, boolean empty) {
            super.updateItem(item, empty);

            DebugTimeProvider p = item;

            if (!empty && p != null && p.getTime() != null) {

                setText(p.getTime().toString());
                
                if ("Sleep".equalsIgnoreCase(p.getState())) {
                    color = "white";
                } else {
                    if (p.getTime().doubleValue() <= 1) {
                        color = "green";
                    } else if (p.getTime().doubleValue() <= 3) {
                        color = "#b75959";
                    } else if (p.getTime().doubleValue() <= 6) {
                        color = "#c44444";
                    } else if (p.getTime().doubleValue() > 6) {
                        color = "#ff0000";
                    } else {
                        color = "white";
                    }
                }
                
                setStyle("-fx-text-fill : " + color + ";");

            } else {
                setText("");
                setStyle("");
            }

        }
    }