package np.com.ngopal.smart.sql.structure.controller;

import com.google.gson.Gson;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DigestQuery;
import np.com.ngopal.smart.sql.model.DigestText;
import np.com.ngopal.smart.sql.model.GSChartVariable;
import np.com.ngopal.smart.sql.model.GlobalProcessList;
import np.com.ngopal.smart.sql.model.GlobalStatusStart;
import np.com.ngopal.smart.sql.model.GlobalVariables;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.SlaveStatus;
import np.com.ngopal.smart.sql.model.StatementDigest;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import static np.com.ngopal.smart.sql.structure.utils.DialogHelper.showConfirm;
import static np.com.ngopal.smart.sql.structure.utils.DialogHelper.showInfo;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class ProfilerTabContentController extends TabContentController {

    private List<ProfilerListener> profilerListeners = new ArrayList<>();

    public void addProfilerListener(ProfilerListener listener) {
        if (!profilerListeners.contains(listener)) {
            profilerListeners.add(listener);
        }
    }

    public void removeProfilerListener(ProfilerListener listener) {
        profilerListeners.remove(listener);
    }

    @Override
    public void changeFont(String font) {
    }

    @FXML
    protected TabPane profilerEntityTabPane;

    @FXML
    private Tab mysql_profiling_tab;

    @FXML
    private Tab mysql_replication_tab;

    @FXML
    private Tab innodb_matrix_tab;

    @FXML
    private Tab myisam_metrics_tab;

    @FXML
    private Tab qaProfilingTab;

    @FXML
    private Tab qaObjectsTab;

    @FXML
    private Tab os_metric_profiling_tab;

    @FXML
    private AnchorPane mainUI;

    @Getter
    @Setter
    private boolean showAll = false;

    private Map<String, Object> globalVariables = new HashMap<>();
    
    private String engineInnodbStatusQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ENGINE_INNODB_STATUS);
    private String slaveStatusQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_SLAVE_STATUS);
    private String fullProcessQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_FULL_PROCESS_LIST);
    private String globalVariablesQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_VARIABLES);
    private String globalStatusQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_STATUS);
    
    public synchronized void putGlobalVariable(String key, Object value) {
        globalVariables.put(key.toUpperCase(), value);
    }

    public synchronized boolean hasGlobalVariable(String key) {
        return globalVariables.containsKey(key.toUpperCase());
    }

    public synchronized Double getDoubleGlobalVariable(String key) {
        Object value = globalVariables.get(key.toUpperCase());
        try {
            return value != null ? Double.valueOf(value.toString()) : 0.0;
        } catch (NumberFormatException ex) {
            return 0.0;
        }
    }

    public synchronized String getStringGlobalVariable(String key) {
        Object value = globalVariables.get(key.toUpperCase());
        return value != null ? value.toString() : "";
    }

    public synchronized boolean containsGlobalVariable(String key) {
        return globalVariables.containsKey(key.toUpperCase());
    }

    private Map<String, GlobalStatusStart> changedGlobalStatus = new HashMap<String, GlobalStatusStart>();

    public synchronized boolean hasGlobalStatus(String key) {
        return changedGlobalStatus.containsKey(key.toUpperCase());
    }

    // Care use this method outside - its only latest values
    public synchronized Double getDoubleGlobalStatus(String key, boolean value) {
        GlobalStatusStart gss = changedGlobalStatus.get(key.toUpperCase());
        if (gss != null) {
            return value ? gss.getValue() : gss.getDifferece();
        } else {
            return 0.0;
        }
    }

    private final ObjectProperty<TreeItem> nullTreeItem = new SimpleObjectProperty<>();
    private final ObjectProperty<Tab> nullTab = new SimpleObjectProperty<>();

    private volatile boolean keepThreadsAlive = true;

    private boolean qaNewAlgorithm = false;
    
    @Getter
    private long startTime = 0l;
    private final BooleanProperty profilerStarted = new SimpleBooleanProperty(false);

    public BooleanProperty profilerStarted() {
        return profilerStarted;
    }

    private final SimpleBooleanProperty zoomDragged = new SimpleBooleanProperty(false);

    public SimpleBooleanProperty zoomDragged() {
        return zoomDragged;
    }

    @Getter
    protected ProfilerSettings currentSettings;

    @Setter(AccessLevel.PUBLIC)
    protected ProfilerDataManager dbProfilerService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerDataManager profilerService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerChartService chartService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerSettingsService settingsService;
    
    @Setter(AccessLevel.PUBLIC)
    protected PreferenceDataService preferenceDataService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ColumnCardinalityService columnCardinalityService;
    
    @Setter(AccessLevel.PUBLIC)
    protected QAPerformanceTuningService aPerformanceTuningService;
    
    @Setter(AccessLevel.PUBLIC)
    protected QARecommendationService qARecommendationService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ConnectionParamService connectionParamService;
    
    @Setter(AccessLevel.PUBLIC)
    protected ProfilerDataManager profilerDataManager;
    
    @Setter(AccessLevel.PUBLIC)
    protected UserUsageStatisticService usageService;

    private TranslateTransition translatet = new TranslateTransition(Duration.millis(200));

    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd,YY h:mm:ss aa");

    private Date topDate = new Date();

    private SimpleStringProperty timeElapsedProperty = new SimpleStringProperty(dateFormat.format(topDate));

    public SimpleStringProperty timeElapsedProperty() {
        return timeElapsedProperty;
    }

    private SimpleObjectProperty<ImageView> recordGraphic = new SimpleObjectProperty(new ImageView(StandardDefaultImageProvider.getInstance().getStart_image()));

    public SimpleObjectProperty<ImageView> recordGraphic() {
        return recordGraphic;
    }
    
    private ArrayList<ProfilerInnerControllerWorkProcAuth> workProcAuths = new ArrayList(5);

    @Getter
    private MetricMonitorController metricMonitorController;
    
    private void setTimeOnProperty(String time) {
        Platform.runLater(() -> {

            if (timeElapsedProperty.get().isEmpty()) {
                topDate.setTime(System.currentTimeMillis());
                timeElapsedProperty.set(dateFormat.format(topDate) + " - " + time);
            } else if (profilerStarted.get()) {
                timeElapsedProperty.set(dateFormat.format(topDate) + " - " + time);
            } else {
                String oldTime = dateFormat.format(topDate);
                topDate.setTime(System.currentTimeMillis());
                timeElapsedProperty.set(oldTime + " - " + dateFormat.format(topDate));
            }
        });
    }

    private final WorkProc delayChange = new WorkProc() {
        @Override
        public void updateUI() {
            recordGraphic.get().setImage(StandardDefaultImageProvider.getInstance().getStart_image());
        }

        @Override
        public void run() {
            try {
                Thread.sleep(3000);
                callUpdate = true;
            } catch (InterruptedException ex) {
            }
        }
    };

    public void updateSettings() {
        currentSettings = settingsService.getProfilerSettings();
    }

    private void updateRecordTime() {
        long currentTime = new Date().getTime();

        if (profilerStarted.get()) {
            long diff = currentTime - startTime;

            long hour = diff / (60 * 60 * 1000);
            diff -= hour * 60 * 60 * 1000;

            long minutes = diff / (60 * 1000);
            diff -= minutes * 60 * 1000;

            long seconds = diff / 1000;

            setTimeOnProperty(String.format("%02d:%02d:%02d", hour, minutes, seconds));
        }
    }

    public synchronized void showUserCharts() {
        final Stage dialog = new Stage();
        final ProfilerUserChartsController profilerUserChartsController = StandardBaseControllerProvider.getController(getController(), "ProfilerUserCharts");
        profilerUserChartsController.init(chartService);

        final Parent p = profilerUserChartsController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle("Profiler settings");

        dialog.showAndWait();

        if (profilerUserChartsController.wasChanged()) {
            profilerListeners.forEach(ProfilerListener::userChartsChanged);
        }
    }

    public synchronized void clearProfilerData() {

        lastTimeFullProcessExecutionRun = 0;
        lastTimeGlobalStatusExecutionRun = 0;
        lastTimeGlobalVariablesRun = 0;
        lastTimeSlaveStatusExecutionRun = 0;

        profilerListeners.forEach(ProfilerListener::clearProfilerData);
    }

    public synchronized void startProfiler() {

        if (profilerStarted.get()) { return;  }

        lastTimeFullProcessExecutionRun = 0;
        lastTimeGlobalStatusExecutionRun = 0;
        lastTimeGlobalVariablesRun = 0;
        lastTimeSlaveStatusExecutionRun = 0;
        startTime = new Date().getTime();
        timeElapsedProperty.set("");
        profilerStarted.set(true);
        // profilerButton.setGraphic(iv);
        ImageView iv = recordGraphic.get();
        iv.setImage(StandardDefaultImageProvider.getInstance().getProfiler_on_image());
        setTimeOnProperty("00:00:00");
        profilerListeners.forEach((ProfilerListener l) -> l.startProfiler(new Date(startTime)));
        
        String serverHashCode = serverHashCode(getConnectionParams(), "");
        workProcAuths.add(getGlobalStatusWorkProcAuth(serverHashCode));
        workProcAuths.add(getGlobalVariableWorkProcAuth(serverHashCode));
        workProcAuths.add(getFullProcessWorkProcAuth(serverHashCode));
        workProcAuths.add(getSlaveStatusWorkProcAuth(serverHashCode));
        workProcAuths.add(getEngineInnodbStatusWorkProcAuth(serverHashCode));
        
        getController().addBackgroundWork(getProfilerBackgroundTaskExceutor());
    }

    public synchronized void stopProfiler() {
        if (profilerStarted.get()) {
            profilerStarted.set(false);
            recordGraphic.get().setImage(StandardDefaultImageProvider.getInstance().getProfiler_off_image());

            setTimeOnProperty(null);
            getController().addBackgroundWork(delayChange);

            profilerListeners.forEach(ProfilerListener::stopProfiler);
        }
    }

    long lastTimeGlobalStatusExecutionRun = 0l;
    long lastTimeGlobalVariablesRun = 0l;
    long lastTimeFullProcessExecutionRun = 0l;
    long lastTimeSlaveStatusExecutionRun = 0l;
    long lastTimeEngineInnodbStatusRunned = 0l;

    public boolean isDataLoaded() {
        return lastTimeGlobalStatusExecutionRun > 0
                && lastTimeGlobalVariablesRun > 0
                && lastTimeFullProcessExecutionRun > 0
                && lastTimeSlaveStatusExecutionRun > 0
                && lastTimeEngineInnodbStatusRunned > 0
                && !changedGlobalStatus.isEmpty();
    }

//    private MysqlProfilingController mysqlProfilingController;
//    private MysqlReplicationController mysqlReplicationController;
//    private MetricMonitorController metricMonitorController;
//    private SlowQueryAnalyzerTabController qaProfilingController;
    private SimpleStringProperty zoomCountStage = new SimpleStringProperty("--%");

    public SimpleStringProperty zoomCountStage() {
        return zoomCountStage;
    }

    private final SimpleIntegerProperty currentScaleIndex = new SimpleIntegerProperty(Flags.DEFAULT_SCALE_INDEX);

    public SimpleIntegerProperty currentScaleIndex() {
        return currentScaleIndex;
    }

    @Override
    public void setDbService(DBService dbService) {
        super.setDbService(dbService);        
    }

    @Getter
    @Setter
    public String savedPath;

    public synchronized void scrollForward() {
        profilerListeners.forEach(ProfilerListener::scrollForward);
    }

    public synchronized void scrollBackward() {
        profilerListeners.forEach(ProfilerListener::scrollBackward);
    }

    public void showSettings() {
        
        if (profilerStarted.get()) {
            DialogHelper.showInfo(getController(), "Info", "Stop first Profiler process!", null);
            return;
        }
        
        final Stage dialog = new Stage();
        final ProfilerController profilerController = StandardBaseControllerProvider.getController(getController(), "ProfilerSettings");
        profilerController.setProfilerService(settingsService);
        profilerController.setParamsService(connectionParamService);
        profilerController.setProfilerDataManager(profilerDataManager);
        profilerController.init();

        final Parent p = profilerController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle("Profiler settings");

        dialog.showAndWait();

        if (profilerController.wasChanged()) {
            updateSettings();
        }
    }

    public synchronized void zoomInAction() {

        if (currentScaleIndex.get() < ChartScale.values().length - 1) {
            currentScaleIndex.set(currentScaleIndex.get() + 1);
        }
        showAll = false;

        profilerListeners.forEach(ProfilerListener::zoomInAction);
    }

    public synchronized void zoomOutAction() {

        if (currentScaleIndex.get() > 0) {
            currentScaleIndex.set(currentScaleIndex.get() - 1);
        }

        showAll = false;
        profilerListeners.forEach(ProfilerListener::zoomOutAction);
    }

    public synchronized void showAllAction() {
        showAll = !showAll;

        profilerListeners.forEach(ProfilerListener::showAllAction);
    }

    @Override
    public void addOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }

    @Override
    public void removeOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }

    private boolean canProfileOs() {
        return getRefreshableConnectionParams().getOsHost() != null && !getRefreshableConnectionParams().getOsHost().isEmpty()
                && getRefreshableConnectionParams().getOsName() != null && !getRefreshableConnectionParams().getOsName().isEmpty()
                && (getRefreshableConnectionParams().isOsUsePassword()
                        ? getRefreshableConnectionParams().getOsPassword() != null && !getRefreshableConnectionParams().getOsPassword().isEmpty()
                        : getRefreshableConnectionParams().getOsPrivateKey() != null && !getRefreshableConnectionParams().getOsPrivateKey().isEmpty())
                && getRefreshableConnectionParams().getOsUser() != null && !getRefreshableConnectionParams().getOsUser().isEmpty();
    }

    private boolean canProfileMysql() {
        return true;
    }

    @Override
    public Object getTreeViewUserData() {
        return new Object();
    }

    @Getter
    private ConnectionParams params;

    @Override
    public void openConnection(ConnectionParams params) throws SQLException {
        this.params = params;

        getUI().setOpacity(0.5);
        params.setMultiQueries(true);
        getDbService().connect(params);    
        getUI().setOpacity(1.0);
        
        boolean canProfileOs = !isProfilingOsMetrics() && canProfileOs();
        boolean canProfileMysql = !isProfilingMysqlDatabase() && canProfileMysql();
        
        usageService.savePUsage(PUsageFeatures.PROFILER);

        if (canProfileMysql && canProfileOs) {
            /**
             * here the logic is if it is capable of profiling both that is both
             * proifile information are available, show a dialog to the user to
             * specify which one he want to profile.
             *
             * if for example user select mysql profile, then we toggle the
             * canProfileOs to false. so when we check it is false.
             */

            DialogResponce dr = showConfirm(getController(), "PROFILER CONNECTION ALERT",
                    "Application has detected REMOTE SERVER CONNECTION details. \n would you like to profile your REMOTE SERVER ??");
            canProfileOs = (dr == DialogResponce.OK_YES || dr == DialogResponce.YES_TO_ALL);
        }

        try {

            if (canProfileMysql) {
                log.info("ABOUT TO PROFILE MYSQL");
                openConnectionForGeneralProfiling();
            }

            if (canProfileOs) {

                openConnectionForOsMetric();
                log.info("ABOUT TO PROFILE OS METRIC ");
            }
        } catch (SQLException se) {
            log.info("IN EXCEPTION ");
            if (!isProfilingOsMetrics() && !isProfilingMysqlDatabase()) { throw se; }
            Platform.runLater(() -> {
                DialogHelper.showError(getController(), "PROFILER " + (isProfilingMysqlDatabase() ? "OS ERROR" : "MySQL ERROR"), se.getMessage(), getController().getSelectedConnectionSession().get().getConnectionParam(), se);
            });
        }

        List<Database> list = getDbService().getDatabases();
        getConnectionParams().setDatabases(new ArrayList<>(list));
    }

    public synchronized void openData(ProfilerData pd) {
        profilerListeners.forEach(l -> l.openData(pd));
    }

    public synchronized void saveProfilerData(boolean saveAs) {
        String userFile = null;
        if (getSavedPath() == null || saveAs) {
            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("SmartSQL Profiller", "*.ssp"),
                    new FileChooser.ExtensionFilter("All Files", "*.*"));
            File file = fc.showSaveDialog(getUI().getScene().getWindow());
            if (file != null) {
                userFile = file.getAbsolutePath();
                if (!userFile.endsWith(".ssp")) {
                    userFile += ".ssp";
                }
            }
        } else {
            userFile = getSavedPath();
        }

        if (userFile != null) {
            setSavedPath(userFile);

            ProfilerData pd = new ProfilerData();

            profilerListeners.forEach(l -> l.saveProfilerData(pd));

            String data = new Gson().toJson(pd, ProfilerData.class);
            try {
                Files.write(new File(userFile).toPath(), data.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (Throwable ex) {
                DialogHelper.showError(getController(), "Error saveing script", ex.getMessage(), getController().getSelectedConnectionSession().get().getConnectionParam(), ex);
            }
        }
    }

    private void openConnectionForGeneralProfiling() throws SQLException {
        openConnectionForMysqlProfiling();
        openConnectionForMysqlReplication();
        openConnectionForInnodbMatrix();
        openConnectionForMyISAMMetrics();
        openConnectionForQAProfiling();
    }

    private void openConnectionForMysqlProfiling() throws SQLException {
        MysqlProfilingController mysqlProfilingController = StandardBaseControllerProvider.getController(getController(), "MysqlProfiling");
        mysqlProfilingController.setParentController(this);
        mysqlProfilingController.setChartService(chartService);
        mysqlProfilingController.setDbProfilerService(dbProfilerService);
        mysqlProfilingController.createConnection();
        addProfilerListener(mysqlProfilingController);
        Platform.runLater(() -> {
            mysql_profiling_tab.setContent(mysqlProfilingController.getUI());
            mysql_profiling_tab.setDisable(false);
            profilerEntityTabPane.getSelectionModel().select(mysql_profiling_tab);
            mysqlProfilingController.finalizeConnectionCreation();
        });
    }

    private void openConnectionForMysqlReplication() throws SQLException {
        MysqlReplicationController mysqlReplicationController = StandardBaseControllerProvider.getController(getController(), "MysqlReplication");
        mysqlReplicationController.setParentController(this);
        mysqlReplicationController.setChartService(chartService);
        mysqlReplicationController.setDbProfilerService(dbProfilerService);
        mysqlReplicationController.createConnection();
        addProfilerListener(mysqlReplicationController);
        Platform.runLater(() -> {
            mysql_replication_tab.setContent(mysqlReplicationController.getUI());
            mysql_replication_tab.setDisable(false);
            mysqlReplicationController.finalizeConnectionCreation();
        });
    }

    private void openConnectionForInnodbMatrix() throws SQLException {
        InnodbMatrixController innodbMatrixController = StandardBaseControllerProvider.getController(getController(), "InnodbMatrix");
        innodbMatrixController.setParentController(this);
        innodbMatrixController.setChartService(chartService);
        innodbMatrixController.setDbProfilerService(dbProfilerService);
        innodbMatrixController.createConnection();
        addProfilerListener(innodbMatrixController);
        Platform.runLater(() -> {
            innodb_matrix_tab.setContent(innodbMatrixController.getUI());
            innodb_matrix_tab.setDisable(false);
            innodbMatrixController.finalizeConnectionCreation();
        });
    }

    private void openConnectionForMyISAMMetrics() throws SQLException {
        MyISAMMetricsController myISAMMetricsController = StandardBaseControllerProvider.getController(getController(), "MyISAMMetrics");
        myISAMMetricsController.setParentController(this);
        myISAMMetricsController.setChartService(chartService);
        myISAMMetricsController.setDbProfilerService(dbProfilerService);
        myISAMMetricsController.createConnection();
        addProfilerListener(myISAMMetricsController);
        Platform.runLater(() -> {
            myisam_metrics_tab.setContent(myISAMMetricsController.getUI());
            myisam_metrics_tab.setDisable(false);
            myISAMMetricsController.finalizeConnectionCreation();
        });
    }

    private void openConnectionForQAProfiling() throws SQLException {
        SlowQueryAnalyzerTabController qaProfilingController = StandardBaseControllerProvider.getController(getController(), "SlowQueryAnalyzerTab");
        qaProfilingController.injectServiceEntities(profilerService, 
                preferenceDataService,
                qARecommendationService,
                columnCardinalityService,
                aPerformanceTuningService,
                connectionParamService,
                usageService);
        qaProfilingController.setSlowQueryDBService((MysqlDBService)getDbService());
        qaProfilingController.initAsProfilerQA(this, getParams());
        addProfilerListener(qaProfilingController);
        qaProfilingTab.setUserData(qaProfilingController);
        Platform.runLater(() -> {
            qaProfilingTab.setContent(qaProfilingController.getUI());
            qaProfilingTab.setDisable(false);
        });
    }

    private void openConnectionForOsMetric() throws SQLException {
        os_metric_profiling_tab.setDisable(false);
        boolean needsCreateNewController = os_metric_profiling_tab.getUserData() == null;
        if (needsCreateNewController) {
            metricMonitorController = StandardBaseControllerProvider.getController(getController(), "MetricMonitor");
            os_metric_profiling_tab.setUserData(metricMonitorController);
            addProfilerListener(metricMonitorController);
            metricMonitorController.setParentController(this);
            Platform.runLater(() -> {
                os_metric_profiling_tab.setContent(metricMonitorController.getUI());
            });
        } else {
            metricMonitorController = (MetricMonitorController) os_metric_profiling_tab.getUserData();
        }
        metricMonitorController.createConnection();
    }

    @Override
    public void focusLeftTree() {
        // do nothing
    }

    @Override
    public void addTab(Tab t) {
        profilerEntityTabPane.getTabs().add(t);
    }

    @Override
    public Tab getTab(String id, String text) {
        return null;
    }

    @Override
    public void showTab(Tab t) {
        profilerEntityTabPane.getSelectionModel().select(t);
    }

    @Override
    public void addTabToBottomPane(Tab... tab) {
        // Do nothing
    }

    @Override
    public void removeTabFromBottomPane(Tab... tab) {
        // Do nothing
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
        return nullTab;
    }

    @Override
    public List<Tab> getBottamTabs() {
        return null;
    }

    @Override
    public ObjectProperty<TreeItem> getSelectedTreeItem() {
        return nullTreeItem;
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return !isProfilingOsMetrics() && !isProfilingMysqlDatabase();
    }

    @Override
    public String getTabName() {
        return "Profiler";
    }

    @Override
    public synchronized DisconnectPermission destroyController(DisconnectPermission permission) {

        keepThreadsAlive = false;

        if (profilerStarted.get()) {
            stopProfiler();
        }

        profilerListeners.forEach(ProfilerListener::destroy);
        profilerListeners.clear();

        return permission;
    }

    public synchronized void showProfilerData(Date fromDate) {
        profilerListeners.forEach(l -> l.showProfilerData(fromDate));
    }

    @Override
    public void refreshTree(boolean alertDatabaseCount) {
        // Do nothing
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        ((ImageView)qaProfilingTab.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getAnalyzer_image());
        ((ImageView)myisam_metrics_tab.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getProfilerTabIcon_image());
        ((ImageView)mysql_profiling_tab.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getProfilerTabIcon_image());
        ((ImageView)mysql_replication_tab.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getProfilerTabIcon_image());
        ((ImageView)innodb_matrix_tab.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getProfilerTabIcon_image());        
    }

    @Override
    public void isFocusedNow() {}

    public boolean isProfilingMysqlDatabase() {
        return !mysql_profiling_tab.isDisabled();
    }

    public boolean isProfilingOsMetrics() {
        return !os_metric_profiling_tab.isDisabled();
    }

    private ConnectionParams getRefreshableConnectionParams() {
        return (getController().getSelectedConnectionSession().get() != null && getController().getSelectedConnectionSession().get().getConnectionParam() != null)
                ? getController().getSelectedConnectionSession().get().getConnectionParam() : params;
    }

    @Override
    public void showLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hideLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty() {
        return null;
    }

    public void exportToXls() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XLS", "*.xls"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {

                    List<String> sheets = new ArrayList<>();
                    List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                    List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                    List<List<Integer>> totalColumnWidths = new ArrayList<>();
                    List<byte[]> images = new ArrayList<>();
                    List<Integer> heights = new ArrayList<>();
                    List<Integer> colspans = new ArrayList<>();

                    synchronized (ProfilerTabContentController.this) {
                        profilerListeners.forEach(l -> l.exportToXls(sheets, totalData, totalColumns, totalColumnWidths, images, heights, colspans));
                    }

                    ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                        @Override
                        public void error(String errorMessage, Throwable th) {
                            Platform.runLater(() -> DialogHelper.showError(getController(), "Error", errorMessage, th, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage()));
                        }

                        @Override
                        public void success() {
                            Platform.runLater(() -> showInfo(getController(), "Success", "Data export seccessfull", null, getStage()));
                        }
                    });
                }
            });
        }
    }

    private ProfilerInnerControllerWorkProcAuth getGlobalStatusWorkProcAuth(String serverHashCode){
        return new ProfilerInnerControllerWorkProcAuth() {
            Set<String> usedVaribales;

            @Override
            public boolean shouldExecuteWorkProc() {
                return profilerStarted.get() && keepThreadsAlive && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastTimeGlobalStatusExecutionRun) >= 1;
            }

            @Override
            public void runWork() {
                if (usedVaribales == null) {
                    dbProfilerService.clearGlobalStatusStartTable(serverHashCode);
                    
                    Map<String, GSChartVariable> variables = chartService.getVariablesNames();
                    usedVaribales = new HashSet<>();
                    for (String key : variables.keySet()) {
                        String[] names = key.split("\\s+");

                        if (names.length == 1) {
                            usedVaribales.add(key.toUpperCase());

                        } else if (names.length >= 2) {
                            String function = names[0].trim();
                            String name = names[1].trim().toUpperCase();

                            if ("AVG".equalsIgnoreCase(function)) {
                                usedVaribales.add(name);

                            } else if ("DIVIDE".equalsIgnoreCase(function) && names.length >= 3) {

                                String divider = names[2].toUpperCase();

                                usedVaribales.add(name);
                                usedVaribales.add(divider);
                            }
                        }
                    }
                    // Add variables that not in GSCharts
                    usedVaribales.add("UPTIME");
                    usedVaribales.add("QUERIES");
                    usedVaribales.add("INNODB_BUFFER_POOL_SIZE");
                    usedVaribales.add("INNODB_PAGE_SIZE");
                    usedVaribales.add("INNODB_BUFFER_POOL_PAGES_TOTAL");
                    usedVaribales.add("INNODB_BUFFER_POOL_PAGES_DIRTY");
                    usedVaribales.add("INNODB_BUFFER_POOL_PAGES_DATA");
                    usedVaribales.add("INNODB_BUFFER_POOL_PAGES_MISC");
                    usedVaribales.add("INNODB_BUFFER_POOL_PAGES_FREE");
                    usedVaribales.add("INNODB_BUFFER_POOL_READ_REQUESTS");
                    usedVaribales.add("INNODB_BUFFER_POOL_READS");

                    usedVaribales.add("KEY_WRITE_REQUESTS");
                    usedVaribales.add("KEY_WRITES");
                    usedVaribales.add("KEY_READ_REQUEST");
                    usedVaribales.add("KEY_READS");

                }
                                
                /**
                 * *************************************************
                 * GLOBAL STATUS STUFF shows global status runs every second
                 * ************************************************
                 */
                try {
                    // date of execution qeury
                    Date currentDate = new Date();
                    // prepare table for global status values
                    Map<String, GlobalStatusStart> lastGlobalStatus = dbProfilerService.prepareGlobalStatusTables(serverHashCode, currentDate);

                    boolean wasEmpty = lastGlobalStatus.isEmpty();

                    dbProfilerService.updateStartTimeGSStartTable(serverHashCode, currentDate);

                    synchronized (ProfilerTabContentController.this) {
                        try (ResultSet rs = getDbService().getDB().prepareStatement(globalStatusQuery).executeQuery()) {

                            // log.error("showGlobalStatusThread ********************************");
                            while (rs.next()) {
                                try {
                                    String name = rs.getString(1);
                                    Double value = rs.getDouble(2);

                                    if (name != null) {
                                        name = name.toUpperCase();
                                    }

                                    if (usedVaribales.contains(name)) {

                                        // if empty - than its first time runnig
                                        // we must isert all variables
                                        if (wasEmpty) {

                                            GlobalStatusStart gs = new GlobalStatusStart(name, value, 0.0);
                                            dbProfilerService.save(gs, serverHashCode);

                                            // collect into map
                                            lastGlobalStatus.put(gs.getName(), gs);

                                        } else {
                                            GlobalStatusStart old = changedGlobalStatus.get(name);

                                            GlobalStatusStart gs = new GlobalStatusStart(name, value, 0.0);
                                            changedGlobalStatus.put(gs.getName(), gs);

                                            GlobalStatusStart oldStart = lastGlobalStatus.get(gs.getName());
                                            if (oldStart != null) {
                                                Double oldValue = old != null ? old.getValue() : oldStart.getValue();
                                                gs.setDifferece(gs.getValue() - oldValue);

                                                dbProfilerService.insertGSDistinction(oldStart.getId(), oldStart.getName(), gs.getValue(), serverHashCode, currentDate);

                                                // update old value to new
                                                if (old != null) {
                                                    old.setValue(gs.getValue());
                                                }
                                                changedGlobalStatus.put(gs.getName(), gs);

                                            } else {
                                                // TODO What we need to do if no old?
                                            }
                                        }
                                    }
                                } catch (SQLException ex) { }
                            }
                        }
                    }
                } catch (SQLException ex) {
                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_GLOBAL_STATUS'", ex);
                }

                lastTimeGlobalStatusExecutionRun = System.currentTimeMillis();
                updateRecordTime();
            }
        };
    }
    
    private ProfilerInnerControllerWorkProcAuth getGlobalVariableWorkProcAuth(String serverHashCode){
        
        return new ProfilerInnerControllerWorkProcAuth() {
            @Override
            public boolean shouldExecuteWorkProc() {
                return profilerStarted.get() && keepThreadsAlive && TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - lastTimeGlobalVariablesRun) >= 30;
            }

            @Override
            public void runWork() {
                /**
                 * ***************************************************
                 * GLOBAL VARIABLES STUFF . shows global variables runs every 30
                 * minutes **************************************************
                 */
                try {
                    // clear global variables for current connection id
                    dbProfilerService.prepareGlobalVariablesTables(serverHashCode);
                    synchronized (ProfilerTabContentController.this) {
                        try (ResultSet rs = getDbService().getDB().prepareStatement(globalVariablesQuery).executeQuery()) {
                            while (rs.next()) {
                                // get each key, value and save it
                                dbProfilerService.save(new GlobalVariables(rs.getString(1), rs.getString(2)), serverHashCode);

                                putGlobalVariable(rs.getString(1).toUpperCase(), rs.getObject(2));
                            }
                        }
                    }
                    lastTimeGlobalVariablesRun = System.currentTimeMillis();
//                        log.info("globalVariableThread: COLLECTED");
                } catch (SQLException ex) {
                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_GLOBAL_VARIABLES'", ex);
                }
            }
        };
    }
    
    private ProfilerInnerControllerWorkProcAuth getFullProcessWorkProcAuth(String serverHashCode) {
        
        return new ProfilerInnerControllerWorkProcAuth() {
            
            private volatile int waitInSec = 1;
            private volatile boolean digestTextWasCalled = false;
            
            @Override
            public boolean shouldExecuteWorkProc() {
                return profilerStarted.get() && keepThreadsAlive && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastTimeFullProcessExecutionRun) >= waitInSec;
            }

            @Override
            public void runWork() {
                
                synchronized (ProfilerTabContentController.this) {
                    
                    Date currentDate = new Date();

                    // we need to check wich algoritm will use
                    
                    // new algorithm available only for > 5.6
                    if (getDbService().getDbSQLVersionMaj() > 5 || getDbService().getDbSQLVersionMaj() == 5 && getDbService().getDbSQLVersionMin() >= 6) {
                        QueryResult qr = new QueryResult();
                        
                        String query = "show global variables like 'performance_schema';";
                        query += "SELECT count(if(ENABLED='YES',1,0)) FROM performance_schema.setup_consumers WHERE NAME in ('events_statements_current','statements_digest');";
                     
                        try {
                            getDbService().execute(query, qr);
                            
                            ResultSet rs = (ResultSet)qr.getResult().get(0);
                            if (rs.next()) {
                                qaNewAlgorithm = "ON".equals(rs.getString(2));
                            }
                            rs.close();
                            
                            rs = (ResultSet)qr.getResult().get(1);
                            if (qaNewAlgorithm) {
                                if (rs.next()) {
                                    qaNewAlgorithm = rs.getInt(1) == 2;
                                }
                            }
                            rs.close();
                            
                        } catch (SQLException ex) {
                            log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_FULL_PROCESSLIST'", ex);
                        }
                    }
                    
                    if (qaNewAlgorithm) {
                        // new algorithm must be runned every 15 sec
                        waitInSec = 15;
                        performanceSchemaAlgorithm(currentDate);
                        
                    } else {
                        // old algorithm must be runned every 1 sec
                        waitInSec = 1;
                        fullProcessListAlgorithm(currentDate);
                    }
                }

                lastTimeFullProcessExecutionRun = System.currentTimeMillis();
//                    log.info("fullProcessThread: COLLECTED");
            }
            
            private void performanceSchemaAlgorithm(Date currentDate) {
                try {
                    dbProfilerService.prepareStatementDigestTables(serverHashCode, currentDate);
                    
                    synchronized (ProfilerTabContentController.this) {
                        
                        // one time on start
                        if (!digestTextWasCalled) {
                            
                            try (ResultSet rs = getDbService().getDB().prepareStatement(
                                    QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_STATEMENTS_BY_DIGEST_0)).executeQuery()) {
                                
                                while (rs.next()) {
                                    dbProfilerService.insertDigestText(
                                        new DigestText(
                                            getConnectionParams().getId().intValue(), 
                                            rs.getString(1), 
                                            rs.getString(2), 
                                            rs.getString(3)
                                        )
                                    );
                                }
                            }
                            
                            digestTextWasCalled = true;
                        }
                        
                        // every 15 sec
                        try (ResultSet rs = getDbService().getDB().prepareStatement(
                                QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_STATEMENT_HISTORY)).executeQuery()) {

                            while (rs.next()) {
                                dbProfilerService.insertDigestQuery(
                                    new DigestQuery(
                                        getConnectionParams().getId().intValue(), 
                                        rs.getString(1), 
                                        rs.getString(2), 
                                        rs.getString(3), 
                                        rs.getString(4)
                                    )
                                );
                            }
                        }
                        
                        // every 15 sec
                        try (ResultSet rs = getDbService().getDB().prepareStatement(
                                QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_STATEMENTS_BY_DIGEST_1)).executeQuery()) {

                            Map<String, StatementDigest> prevDigestMap = new HashMap<>();
                            while (rs.next()) {
                                
                                StatementDigest prev = prevDigestMap.get(rs.getString(2));
                                
                                StatementDigest sd = new StatementDigest(
                                    getConnectionParams().getId().intValue(), 
                                    rs.getString(1), 
                                    rs.getString(2), 
                                    rs.getString(3), 
                                    rs.getLong(4), prev != null ? rs.getLong(4) - prev.getCountStar() : 0l,
                                    rs.getLong(5), prev != null ? rs.getLong(5) - prev.getSumTimerWait() : 0l, 
                                    rs.getLong(6), prev != null ? rs.getLong(6) - prev.getMinTimerWait() : 0l, 
                                    rs.getLong(7), prev != null ? rs.getLong(7) - prev.getAvgTimerWait() : 0l, 
                                    rs.getLong(8), prev != null ? rs.getLong(8) - prev.getMaxTimerWait() : 0l, 
                                    rs.getLong(9), prev != null ? rs.getLong(9) - prev.getSumLockTime() : 0l, 
                                    rs.getLong(10), prev != null ? rs.getLong(10) - prev.getSumErrors() : 0l, 
                                    rs.getLong(11), prev != null ? rs.getLong(11) - prev.getSumWarnings() : 0l, 
                                    rs.getLong(12), prev != null ? rs.getLong(12) - prev.getSumRowsAffected() : 0l, 
                                    rs.getLong(13), prev != null ? rs.getLong(13) - prev.getSumRowsSent() : 0l, 
                                    rs.getLong(14), prev != null ? rs.getLong(14) - prev.getSumRowsExamined() : 0l, 
                                    rs.getLong(15), prev != null ? rs.getLong(15) - prev.getSumCreatedTmpDiskTables() : 0l, 
                                    rs.getLong(16), prev != null ? rs.getLong(16) - prev.getSumCreatedTmpTables() : 0l, 
                                    rs.getLong(17), prev != null ? rs.getLong(17) - prev.getSumSelectFullJoin() : 0l, 
                                    rs.getLong(18), prev != null ? rs.getLong(18) - prev.getSumSelectFullRangeJoin() : 0l, 
                                    rs.getLong(19), prev != null ? rs.getLong(19) - prev.getSumSelectRange() : 0l, 
                                    rs.getLong(20), prev != null ? rs.getLong(20) - prev.getSumSelectRangeCheck() : 0l, 
                                    rs.getLong(21), prev != null ? rs.getLong(21) - prev.getSumSelectScan() : 0l, 
                                    rs.getLong(22), prev != null ? rs.getLong(22) - prev.getSumSortMergePasses() : 0l, 
                                    rs.getLong(23), prev != null ? rs.getLong(23) - prev.getSumSortRange() : 0l, 
                                    rs.getLong(24), prev != null ? rs.getLong(24) - prev.getSumSortRows() : 0l, 
                                    rs.getLong(25), prev != null ? rs.getLong(25) - prev.getSumSortScan() : 0l, 
                                    rs.getLong(26), prev != null ? rs.getLong(26) - prev.getSumNoIndexUsed() : 0l, 
                                    rs.getLong(27), prev != null ? rs.getLong(27) - prev.getSumNoGoodIndexUsed() : 0l,
                                    rs.getTimestamp(28),
                                    rs.getTimestamp(29)
                                );
                                
                                prevDigestMap.put(sd.getDigest(), sd);
                                
                                dbProfilerService.insertSD(sd, serverHashCode, currentDate);
                            }
                        }
                    }
                    lastTimeGlobalVariablesRun = System.currentTimeMillis();
//                        log.info("globalVariableThread: COLLECTED");
                } catch (SQLException ex) {
                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_GLOBAL_VARIABLES'", ex);
                }
            }
            
            private void fullProcessListAlgorithm(Date currentDate) {
                try (ResultSet rs = getDbService().getDB().prepareStatement(fullProcessQuery).executeQuery()) {

                    while (rs.next()) {
                        // save only with not null info
                        String info = rs.getString(DB_PROFILER__VARIABLE__INFO);
                        String command = rs.getString(DB_PROFILER__VARIABLE__COMMAND);
                        String user = rs.getString(DB_PROFILER__VARIABLE__USER);

                        // no need save root commands
                        if (info != null
                                && !info.equalsIgnoreCase(globalVariablesQuery)
                                && !info.equalsIgnoreCase(globalStatusQuery)
                                && !info.equalsIgnoreCase(fullProcessQuery)
                                && !"sleep".equalsIgnoreCase(command)
                                && !"system user".equalsIgnoreCase(user)) {

                            String host = rs.getString(DB_PROFILER__VARIABLE__HOST);
                            if (host.contains(":")) {
                                host = host.substring(0, host.indexOf(":"));
                            }

                            dbProfilerService.save(
                                    new GlobalProcessList(
                                            rs.getInt(DB_PROFILER__VARIABLE__ID),
                                            user,
                                            host,
                                            rs.getString(DB_PROFILER__VARIABLE__DB),
                                            command,
                                            rs.getBigDecimal(DB_PROFILER__VARIABLE__TIME),
                                            rs.getString(DB_PROFILER__VARIABLE__STATE),
                                            info,
                                            currentDate
                                    )
                            );
                        }
                    }
                } catch (SQLException ex) {
                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_FULL_PROCESSLIST'", ex);
                }
            }
        };
    }

    private ProfilerInnerControllerWorkProcAuth getSlaveStatusWorkProcAuth(String serverHashCode) {
        return new ProfilerInnerControllerWorkProcAuth() {
            List<String> columns = new ArrayList<>();
            
            @Override
            public boolean shouldExecuteWorkProc() {
                return profilerStarted.get() && keepThreadsAlive && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastTimeSlaveStatusExecutionRun) >= 15;
            }

            @Override
            public void runWork() {
                /**
                 * *************************************************
                 * SALVE STATUS STUFF every 15 seconds
                 * ************************************************
                 */
                try {
                    Date currentDate = new Date();
                    // prepare table for global status values
                    dbProfilerService.prepareSlaveStatusTables(serverHashCode, currentDate);
                    synchronized (ProfilerTabContentController.this) {

                        try (ResultSet rs = getDbService().getDB().prepareStatement(slaveStatusQuery).executeQuery()) {

                            if (columns.isEmpty()) {
                                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                    columns.add(rs.getMetaData().getColumnName(i));
                                }
                            }

                            while (rs.next()) {
                                try {
                                    SlaveStatus changedSlaveStatus = new SlaveStatus();
                                    for (String s : columns) {
                                        changedSlaveStatus.setValue(s, rs);
                                    }

                                    dbProfilerService.insertSSDistinction(changedSlaveStatus, serverHashCode, currentDate);
                                } catch (SQLException ex) {
                                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_SLAVE_STATUS'", ex);
                                }
                            }
                        }

                        // Just for emulate
//                                SlaveStatus changedSlaveStatus = SlaveStatus.random();                                        
//                                dbProfilerService.insertSSDistinction(changedSlaveStatus, serverHashCode, currentDate);
                    }

                } catch (SQLException ex) {
                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_SLAVE_STATUS'", ex);
                }

                lastTimeSlaveStatusExecutionRun = System.currentTimeMillis();
//                    log.info("slaveStatusThread: COLLECTED");
            }
        };
    }

    private ProfilerInnerControllerWorkProcAuth getEngineInnodbStatusWorkProcAuth(String serverHashCode) {
        return new ProfilerInnerControllerWorkProcAuth() {
            Double oldPagesFlushed = null;

            @Override
            public boolean shouldExecuteWorkProc() {
                return profilerStarted.get() && keepThreadsAlive && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastTimeEngineInnodbStatusRunned) >= 60;
            }

            @Override
            public void runWork() {

                /**
                 * ***************************************************
                 * ENGINE INNODB STATUS STUFF runs every 1 minutes
                 * **************************************************
                 */
                try {
                    Date currentDate = new Date();
                    dbProfilerService.prepareEngineInnodbStatusTables(serverHashCode, currentDate);
                    synchronized (ProfilerTabContentController.this) {
                        try (ResultSet rs = getDbService().getDB().prepareStatement(engineInnodbStatusQuery).executeQuery()) {
                            while (rs.next()) {

                                Double innodbPageSize = getDoubleGlobalStatus("Innodb_page_size", true);
                                Double innodbBufferPoolPagesTotal = getDoubleGlobalStatus("Innodb_buffer_pool_pages_total", true);
                                Double innodbBufferPoolPagesDirty = getDoubleGlobalStatus("Innodb_buffer_pool_pages_dirty", true);

                                dbProfilerService.insertEISDistinction("BUFFER_POOL__DATA_TOTAL", innodbBufferPoolPagesTotal * innodbPageSize, serverHashCode, currentDate);
                                dbProfilerService.insertEISDistinction("BUFFER_POOL__DATA_DIRTY", innodbBufferPoolPagesDirty * innodbPageSize, serverHashCode, currentDate);

                                Double innodbBufferPoolReadRequests = getDoubleGlobalStatus("Innodb_buffer_pool_read_requests", true);
                                Double innodbBufferPoolReads = getDoubleGlobalStatus("Innodb_buffer_pool_reads", true);

                                dbProfilerService.insertEISDistinction("BUFFER_POOL_IO__HIT_RATIO", (innodbBufferPoolReadRequests / (innodbBufferPoolReadRequests + innodbBufferPoolReads)) * 100, serverHashCode, currentDate);

                                String text = rs.getString(3);

                                boolean done0 = false;
                                boolean done1 = false;
                                boolean done2 = false;
                                boolean done3 = false;
                                boolean done4 = false;
                                boolean done5 = false;

                                Matcher m = PATTERN__ROW_OPERATIONS.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__ROW_OPERATIONS_0.matcher(data);
                                        if (m != null && m.find()) {
                                            try {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__INSERTED", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__INSERTED", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__UPDATED", Double.valueOf(m.group(2)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__UPDATED", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__DELETED", Double.valueOf(m.group(3)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__DELETED", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__READ", Double.valueOf(m.group(4)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("ROW_OPERATIONS__READ", 0.0, serverHashCode, currentDate);
                                            }

                                            done0 = true;
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("ROW_OPERATIONS__INSERTED", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("ROW_OPERATIONS__UPDATED", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("ROW_OPERATIONS__DELETED", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("ROW_OPERATIONS__READ", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                done1 = false;
                                m = PATTERN__TRANSACTIONS.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__TRANSACTIONS_0.matcher(data);
                                        if (m != null) {
                                            Double lockWait = 0.0;
                                            Integer count = 0;
                                            while (m.find()) {
                                                String lockWaitText = m.group(1);
                                                if (lockWaitText != null) {
                                                    try {
                                                        lockWait += Double.valueOf(lockWaitText);
                                                        count++;
                                                    } catch (NumberFormatException ex) {
                                                    }
                                                }
                                            }

                                            dbProfilerService.insertEISDistinction("TRANSACTIONS__LOCK_WAIT", lockWait, serverHashCode, currentDate);
                                            dbProfilerService.insertEISDistinction("TRANSACTIONS__AVG__LOCK_WAIT", count > 0 ? lockWait / count.intValue() : 0.0, serverHashCode, currentDate);

                                            done0 = true;
                                        }

                                        m = PATTERN__TRANSACTIONS_1.matcher(data);
                                        if (m != null) {
                                            Double historyLength = 0.0;
                                            if (m.find()) {
                                                String historyLengthText = m.group(1);
                                                if (historyLengthText != null) {
                                                    try {
                                                        historyLength += Double.valueOf(historyLengthText);
                                                    } catch (NumberFormatException ex) {
                                                    }
                                                }
                                            }

                                            dbProfilerService.insertEISDistinction("TRANSACTIONS__HISTORY_LENGTH", historyLength, serverHashCode, currentDate);
                                            done1 = true;
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("TRANSACTIONS__LOCK_WAIT", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("TRANSACTIONS__AVG__LOCK_WAIT", 0.0, serverHashCode, currentDate);
                                }

                                if (!done1) {
                                    dbProfilerService.insertEISDistinction("TRANSACTIONS__HISTORY_LENGTH", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                done1 = false;
                                m = PATTERN__FILE_IO.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__FILE_IO_0.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("FILE_IO__LOG", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("FILE_IO__LOG", 0.0, serverHashCode, currentDate);
                                            }

                                            done0 = true;
                                        }

                                        m = PATTERN__FILE_IO_1.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("FILE_IO__READS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("FILE_IO__READS", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("FILE_IO__WRITES", Double.valueOf(m.group(2)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("FILE_IO__WRITES", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("FILE_IO__FSYNCS", Double.valueOf(m.group(3)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("FILE_IO__FSYNCS", 0.0, serverHashCode, currentDate);
                                            }

                                            done1 = true;
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("FILE_IO__LOG", 0.0, serverHashCode, currentDate);
                                }

                                if (!done1) {
                                    dbProfilerService.insertEISDistinction("FILE_IO__READS", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("FILE_IO__WRITES", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("FILE_IO__FSYNCS", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                m = PATTERN__LOG.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__LOG_0.matcher(data);
                                        if (m != null && m.find()) {

                                            try {

                                                Double pagesFlushed = Double.valueOf(m.group(1));

                                                dbProfilerService.insertEISDistinction("LOG__PAGES_FLUSHED", pagesFlushed, serverHashCode, currentDate);

                                                if (oldPagesFlushed != null) {

                                                    Double innodbLogFileSize = getDoubleGlobalVariable("innodb_log_file_size");
                                                    Double innodbBufferSize = getDoubleGlobalVariable("innodb_log_buffer_size");

                                                    Double v;
                                                    if (oldPagesFlushed > pagesFlushed) {
                                                        v = innodbLogFileSize - oldPagesFlushed + pagesFlushed;
                                                    } else {
                                                        v = pagesFlushed - oldPagesFlushed;
                                                    }

//                                                        v = 10.0;
                                                    dbProfilerService.insertEISDistinction("LOG__HOURLY__PAGES_FLUSHED", v, serverHashCode, currentDate);

                                                    dbProfilerService.insertEISDistinction("LOG__INNODB_LOG_FILE_SIZE__TIME", v > 0 ? innodbLogFileSize / v : 0.0, serverHashCode, currentDate);
                                                    dbProfilerService.insertEISDistinction("LOG__INNODB_LOG_BUFFER_SIZE__TIME", v > 0 ? innodbBufferSize / v : 0.0, serverHashCode, currentDate);
                                                } else {
                                                    dbProfilerService.insertEISDistinction("LOG__HOURLY__PAGES_FLUSHED", 0.0, serverHashCode, currentDate);

                                                    dbProfilerService.insertEISDistinction("LOG__INNODB_LOG_FILE_SIZE__TIME", 0.0, serverHashCode, currentDate);
                                                    dbProfilerService.insertEISDistinction("LOG__INNODB_LOG_BUFFER_SIZE__TIME", 0.0, serverHashCode, currentDate);
                                                }

                                                oldPagesFlushed = pagesFlushed;
                                                done0 = true;
                                            } catch (NumberFormatException ex) {
                                            }
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("LOG__HOURLY__PAGES_FLUSHED", 0.0, serverHashCode, currentDate);

                                    dbProfilerService.insertEISDistinction("LOG__INNODB_LOG_FILE_SIZE__TIME", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("LOG__INNODB_LOG_BUFFER_SIZE__TIME", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                m = PATTERN__BUFFER_POOL_AND_MEMORY.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__BUFFER_POOL_AND_MEMORY_0.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__READ", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__READ", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__CREATED", Double.valueOf(m.group(2)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__CREATED", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__WRITTEN", Double.valueOf(m.group(3)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__WRITTEN", 0.0, serverHashCode, currentDate);
                                            }

                                            done0 = true;
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__READ", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__CREATED", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("BUFFER_POOL_AND_MEMORY__WRITTEN", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                done1 = false;
                                m = PATTERN__INSERT_BUFFER.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__INSERT_BUFFER_0.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGES", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGES", 0.0, serverHashCode, currentDate);
                                            }

                                            done0 = true;
                                        }

                                        m = PATTERN__INSERT_BUFFER_1.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_INSERT", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_INSERT", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_DELETE_MARK", Double.valueOf(m.group(2)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_DELETE_MARK", 0.0, serverHashCode, currentDate);
                                            }

                                            try {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_DELETE", Double.valueOf(m.group(3)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_DELETE", 0.0, serverHashCode, currentDate);
                                            }

                                            done1 = true;
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGES", 0.0, serverHashCode, currentDate);
                                }

                                if (!done1) {
                                    dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_INSERT", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_DELETE_MARK", 0.0, serverHashCode, currentDate);
                                    dbProfilerService.insertEISDistinction("INSERT_BUFFER__MERGED_DELETE", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                done1 = false;
                                done2 = false;
                                done3 = false;
                                done4 = false;
                                done5 = false;
                                m = PATTERN__SEMAPHORES.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        m = PATTERN__SEMAPHORES_0.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_S_WAITS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_S_WAITS", 0.0, serverHashCode, currentDate);
                                            }

                                            done0 = true;
                                        }

                                        m = PATTERN__SEMAPHORES_1.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_X_WAITS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_X_WAITS", 0.0, serverHashCode, currentDate);
                                            }

                                            done1 = true;
                                        }

                                        m = PATTERN__SEMAPHORES_2.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_SX_WAITS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_SX_WAITS", 0.0, serverHashCode, currentDate);
                                            }

                                            done2 = true;
                                        }

                                        m = PATTERN__SEMAPHORES_3.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_S_ROUNDS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_S_ROUNDS", 0.0, serverHashCode, currentDate);
                                            }

                                            done3 = true;
                                        }

                                        m = PATTERN__SEMAPHORES_4.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_X_ROUNDS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_X_ROUNDS", 0.0, serverHashCode, currentDate);
                                            }

                                            done4 = true;
                                        }

                                        m = PATTERN__SEMAPHORES_5.matcher(data);
                                        if (m != null && m.find()) {

                                            try {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_SX_ROUNDS", Double.valueOf(m.group(1)), serverHashCode, currentDate);
                                            } catch (NumberFormatException ex) {
                                                dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_SX_ROUNDS", 0.0, serverHashCode, currentDate);
                                            }

                                            done5 = true;
                                        }
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_S_WAITS", 0.0, serverHashCode, currentDate);
                                }

                                if (!done1) {
                                    dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_X_WAITS", 0.0, serverHashCode, currentDate);
                                }

                                if (!done2) {
                                    dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_SX_WAITS", 0.0, serverHashCode, currentDate);
                                }

                                if (!done3) {
                                    dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_S_ROUNDS", 0.0, serverHashCode, currentDate);
                                }

                                if (!done4) {
                                    dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_X_ROUNDS", 0.0, serverHashCode, currentDate);
                                }

                                if (!done5) {
                                    dbProfilerService.insertEISDistinction("SEMAPHORES__RW_LOCKS_SX_ROUNDS", 0.0, serverHashCode, currentDate);
                                }

                                done0 = false;
                                m = PATTERN__DEADLOCKS.matcher(text);
                                if (m != null && m.find()) {
                                    String data = m.group(1);
                                    if (data != null) {
                                        Double count = 0.0;
                                        m = PATTERN__DEADLOCKS_0.matcher(data);
                                        while (m != null && m.find()) {
                                            count++;
                                        }

                                        try {
                                            dbProfilerService.insertEISDistinction("DEADLOCKS__DEADLOCKS", count, serverHashCode, currentDate);
                                        } catch (NumberFormatException ex) {
                                            dbProfilerService.insertEISDistinction("DEADLOCKS__DEADLOCKS", 0.0, serverHashCode, currentDate);
                                        }

                                        done0 = true;
                                    }
                                }

                                if (!done0) {
                                    dbProfilerService.insertEISDistinction("DEADLOCKS__DEADLOCKS", 0.0, serverHashCode, currentDate);
                                }
                            }
                        }
                    }

                    lastTimeEngineInnodbStatusRunned = System.currentTimeMillis();
//                        log.info("engineInnodbStatusThread: COLLECTED");
                } catch (SQLException ex) {
                    log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_ENGINE_INNODB_STATUS'", ex);
                }
            }
        };
    }

    private WorkProc getProfilerBackgroundTaskExceutor(){
        return new WorkProc() {
            @Override
            public void updateUI() { }

            @Override
            public void run() { 
                System.out.println("ALL BACKGROUND TASK WORK PROC STARTING");  
                System.gc();System.gc();
                while(profilerStarted.get()){
                    
                    for(ProfilerInnerControllerWorkProcAuth procAuth : workProcAuths){
                        if(procAuth.shouldExecuteWorkProc()){
                            procAuth.runWork();
                            try{ Thread.sleep(100); }catch(Exception e){ e.printStackTrace(); }
                        }
                    }
                    
                    for(ProfilerListener pl: profilerListeners.toArray(new ProfilerListener[0])){
                        if(pl.getInnerControllerWorkProcAuthority().shouldExecuteWorkProc()){
                            pl.getInnerControllerWorkProcAuthority().runWork();
                            try{ Thread.sleep(100); }catch(Exception e){ e.printStackTrace(); }
                        }
                    }
                    
                    try{ Thread.sleep(250); }catch(Exception e){ e.printStackTrace(); }
                }
                
                workProcAuths.clear();
                System.gc();System.gc();
                System.out.println("ALL BACKGROUND TASK WORK PROC FINISHING");
            }
        };
    }
    
}
