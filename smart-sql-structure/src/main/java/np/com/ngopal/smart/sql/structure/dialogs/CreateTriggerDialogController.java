package np.com.ngopal.smart.sql.structure.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.utils.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CreateTriggerDialogController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private TextField triggerNameField;
    @FXML
    private ComboBox<String> tableCombox;
    @FXML
    private ComboBox<String> eventCombobx;
    @FXML
    private ComboBox<String> beforeAfterCombobx;
    
    @FXML
    private Button okButton;
    
    private Database database;
    
    private boolean audit = false;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
    private BooleanPropertyBase tableCorrect = new SimpleBooleanProperty(false);
        
    @FXML
    public void okAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        responce = DialogResponce.CANCEL;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        new AutoCompleteComboBoxListener(tableCombox);
        
        okButton.disableProperty().bind(
            Bindings.isEmpty(triggerNameField.textProperty())
            .or(Bindings.isNull(tableCombox.getSelectionModel().selectedItemProperty()).or(tableCorrect.not()))
            .or(Bindings.isNull(eventCombobx.getSelectionModel().selectedItemProperty()))
            .or(Bindings.isNull(beforeAfterCombobx.getSelectionModel().selectedItemProperty()))
        );
        
        EventHandler<KeyEvent> handler = (KeyEvent event) -> {
            if (!okButton.isDisable()) {
                switch (event.getCode()) {

                    case ENTER:
                        okAction(null);
                        break;

                    case ESCAPE:
                        cancelAction(null);
                        break;                    
                }
            }
        };
        
        triggerNameField.setOnKeyPressed(handler);
                
        beforeAfterCombobx.getItems().addAll("BEFORE", "AFTER");
        beforeAfterCombobx.getSelectionModel().selectFirst();
        
        eventCombobx.getItems().addAll("INSERT", "UPDATE", "DELETE");
        eventCombobx.getSelectionModel().select("UPDATE");
                
        eventCombobx.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            calculateTriggerName();
        });
        
        tableCombox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            calculateTriggerName();
        });
    }
    
    public void calculateTriggerName() {
        if (audit) {
            triggerNameField.setText(tableCombox.getSelectionModel().getSelectedItem() + "AuditLog" + eventCombobx.getSelectionModel().getSelectedItem());
        } else {
            triggerNameField.setText(tableCombox.getSelectionModel().getSelectedItem() + "_Trigger_" + eventCombobx.getSelectionModel().getSelectedItem());
        }
    }
    
    public DialogResponce getResponce() {
        return responce;
    }
    
    public String getTriggerNameValue() {
        return triggerNameField.getText();
    }
    
    public String getTriggerTableValue() {
        return tableCombox.getEditor().getText();
    }
    
    public String getTriggerEventValue() {
        return eventCombobx.getSelectionModel().getSelectedItem();
    }
    
    public String getBeforeAfterValue() {
        return beforeAfterCombobx.getSelectionModel().getSelectedItem();
    }

    public String getComment() {

        DBTable t = null;
        for (DBTable table: database.getTables()) {
            if (table.getName().equals(getTriggerTableValue())) {
                t = table;
                break;
            }
        }

        String comment = "";

        if (audit) {
            if (t != null) {
                comment += "/* Create following table before run the trigger \n";
                comment += "    CREATE TABLE `" + t.getName() + "_audit` (\n";
                
                if (t.getColumns() != null) {
                    for (Column c: t.getColumns()) {                        
                        comment += MysqlDBService.columnSQLForAuditLog(c, "        %s %s%s%s%s%s%s%s%s%s%s%s,\n");
                    }
                }
                
                comment += "        `CreateAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,\n" +
                           "        `UpdateAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n" +
                           "        `SQLEvent` enum('INSERT', 'UPDATE', 'DELETE')\n" +
                           "    )";
                if (t.getEngineType() != null) {
                    comment += " ENGINE=" + t.getEngineType().getDisplayValue();
                }
                
                if (t.getCharacterSet() != null && !t.getCharacterSet().trim().isEmpty()) {
                    comment += " DEFAULT CHARSET=" + t.getCharacterSet();
                }
                
                if (t.getCollation() != null && !t.getCollation().trim().isEmpty()) {
                    comment += " COLLATE=" + t.getCollation();
                }
                
                comment += "\n    */";
            }
        } else {
            
            if (t != null && t.getColumns() != null) {
                comment += "/* You can use\n";

                String newColumns = "    ";
                String oldColumns = "    ";
                for (Column c: t.getColumns()) {
                    if (!newColumns.trim().isEmpty()) {
                        newColumns += ", ";
                    }
                    newColumns += "new." + c.getName();

                    if (!oldColumns.trim().isEmpty()) {
                        oldColumns += ", ";
                    }
                    oldColumns += "old." + c.getName();
                }

                comment += newColumns + "\n";
                if (!"insert".equalsIgnoreCase(getTriggerEventValue())) {
                    comment += oldColumns + "\n";
                }
                comment += "    in the trigger logic */";
            }
        }
        return comment;
    }
    
    public String getTriggerBody() {
        String body = "";
        if (audit) {
            
            DBTable t = null;
            for (DBTable table: database.getTables()) {
                if (table.getName().equals(getTriggerTableValue())) {
                    t = table;
                    break;
                }
            }
            
            if (t != null && t.getColumns() != null) {
                body += "INSERT INTO `" + database.getName() + "`.`" + t.getName() + "_audit`\n";
                body += "    (\n";
                for (Column c: t.getColumns()) {
                    body += "        `" + c.getName() + "`,\n";
                }
                body += "         `SQLEvent`\n";
                body += "    )\n";
                body += "    VALUES\n";
                body += "    (\n";
                for (Column c: t.getColumns()) {
                    if (getTriggerEventValue().equalsIgnoreCase("insert")) {
                        body += "        new.`" + c.getName() + "`,\n";
                    } else {
                        body += "        old.`" + c.getName() + "`,\n";
                    }
                }
                body += "        '" + getTriggerEventValue() + "'\n";                  
                body += "    );";
            }
        }
        
        return body;
    }
    
    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }

    public void init(ConnectionParams params, Database database, String tableName, boolean audit) {
        this.audit = audit;
        
        if (audit) {
            beforeAfterCombobx.getItems().remove("BEFORE");
            beforeAfterCombobx.getSelectionModel().selectFirst();
        }
        
        this.database = DatabaseCache.getInstance().getDatabase(params.cacheKey(), database.getName());
        
        for (DBTable table: this.database.getTables()) {
            tableCombox.getItems().add(table.getName());
        }
        
        if (tableName != null) {
            tableCombox.getSelectionModel().select(tableName);
        }
        
        tableCombox.getEditor().textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            tableCorrect.set(false);
            for (String table: tableCombox.getItems()) {
                if (table.equalsIgnoreCase(newValue)) {
                    tableCorrect.set(true);
                }
            }
        });
    }

    @Override
    public String getInputValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initDialog(Object... params) {
        init((ConnectionParams)params[0], (Database)params[1], (String)params[2], (Boolean)params[3]);
        
        getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
    }
}


