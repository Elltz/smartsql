package np.com.ngopal.smart.sql.structure.models;

import java.util.function.Function;
import javafx.beans.value.ObservableValue;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerProperty {

    public enum PropertyType {
        STRING,
        DOUBLE,
        BOOLEAN
    }
    
    private final String name;
    private final ObservableValue observable;
    private final PropertyType type;
    
    private final Function<String, Object> convertFunction;
    
    public DesignerProperty(String name, ObservableValue observable, PropertyType type, Function<String, Object> convertFunction) {
        this.name = name;
        this.observable = observable;
        this.convertFunction = convertFunction;
        this.type = type;
    }
    
    public String getName() {
        return name;
    }
    
    public ObservableValue observable() {
        return observable;
    }

    public PropertyType getType() {
        return type;
    }
    
    public Object convertValue(String value) {
        return convertFunction.apply(value);
    }
}
