/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import javafx.scene.control.TableCell;
import np.com.ngopal.smart.sql.structure.models.DebugInnoTopData;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class DebuggerInnoTopTimeTableCell extends TableCell<DebugInnoTopData, DebugInnoTopData> {

    public static final String INNO_TOP__NOT_STARTED = "Not started";
    String color = "white";

    @Override
    protected void updateItem(DebugInnoTopData item, boolean empty) {
        super.updateItem(item, empty);

        if (!empty && item != null && item.getTime() != null) {

            setText(item.getTime().toString());

            String[] times = item.getTime().split(":");
            Long secs = 0l;
            if (times.length > 2) {
                secs = Long.valueOf(times[0]) * 36000 + Long.valueOf(times[1]) * 60 + Long.valueOf(times[2]);
            }

            if (INNO_TOP__NOT_STARTED.equalsIgnoreCase(item.getStatus())) {
                color = "white";
            } else if (secs <= 10) {
                color = "green";
            } else if (secs <= 30) {
                color = "#ff5400";
            } else if (secs <= 50) {
                color = "#ff3b00";
            } else {
                color = "#ff0000";
            }

            setStyle("-fx-text-fill : " + color + ";");

        } else {
            setText("");
            setStyle("");
        }

    }
}
