package np.com.ngopal.smart.sql.structure.controller;

import com.sun.javafx.PlatformUtil;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.executable.UpdatesInstallerUtils;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class CheckForUpdatesController extends BaseController implements Initializable {
        
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label appnameLabel;
    
    @FXML
    private Label os_version;
        
    @FXML
    private Label update_notice_label;
    
    @FXML
    private ScrollPane update_notice_scroll_container;
        
    @FXML
    private Button updateButton;
    
    @FXML
    private ProgressIndicator checkProgressBar;
    
    @FXML
    private HBox footer;
    
    @FXML
    private Button cancelButton;
    
    @FXML
    private ImageView checkboxImageView;
    
    @FXML
    private ImageView failboxImageView;
    
    private UpdatesInstallerUtils installerUtils;
    
    public void init() {
        if (Flags.MODE_UPDATEABLE_PACKAGE == (byte) StandardApplicationManager.getInstance().getSmartsqlSavedData().getPackageType()) {
            installerUtils.checkForUpdates();
        }else{
            Platform.runLater(()->{
                installerUtils.onUpdatesCheckDone(false, false, 
                        StandardApplicationManager.getInstance().getSmartsqlSavedData().getVersion());
                update_notice_label.setText(StandardApplicationManager.getInstance().getApplicationBinaryName()
                        + " version : " + StandardApplicationManager.getInstance().getSmartsqlSavedData().getVersion());
                disableNormalMode();                
            });
        }
    }
        
    @FXML
    public void cancelAction(ActionEvent event) {
        installerUtils.stopCurrentOperation();
        updateButton.setVisible(true);
        if(event != null && FunctionHelper.getApplicationMainStage() != getStage()){
            installerUtils = null;
            getStage().close();
        }
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        footer.getChildren().remove(cancelButton);
        cancelButton.visibleProperty().bind(updateButton.visibleProperty().not());
        update_notice_label.setText("");
        updateButton.visibleProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    footer.getChildren().remove(cancelButton);
                    footer.getChildren().add(updateButton);
                }else{
                    footer.getChildren().add(cancelButton);
                    footer.getChildren().remove(updateButton);                    
                }
            }
        });
        
        toggleNotificationState(null);
        
        installerUtils = new UpdatesInstallerUtils(checkProgressBar, update_notice_label, update_notice_label) {
            @Override
            public void onUpdatesCheckDone(boolean needsUpdates, boolean needRestructuring, String version) {
                if(needsUpdates || needRestructuring){
                    enableNormalMode();
                    if(needRestructuring)
                        updateButton.setText("Install patches");
                    updateButton.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            updateButton.setVisible(false);
                            toggleNotificationState(checkProgressBar);
                            enableFullTextMode();
                            downloadRequiredUpdates();
                        }
                    });
                }else {
                    toggleNotificationState(checkboxImageView);
                    footer.setVisible(false);
                }
            }
            
            @Override
            public void onDownloadUpdatesDone() {
                toggleNotificationState(null);
                updateButton.setVisible(true);
                updateButton.setText("apply updates & patches");
                updateButton.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            updateButton.setVisible(false);
                            toggleNotificationState(checkProgressBar);
                            DialogHelper.showInfo(getController(), "UPDATE NOTICE", "Application will restart very soon after patching" ,null);
                            installUpdates();
                        }
                    });
            }

            @Override
            public void onUpdatesCheckFailed() {
                updateButton.setVisible(true);
                toggleNotificationState(failboxImageView);
            }

            @Override
            public void onUpdatesCheckStarted() {
                toggleNotificationState(checkProgressBar);
            }

            @Override
            public void onApplyUpdatesFailed() {
                toggleNotificationState(failboxImageView);
                cancelAction(null);
            }

        };
        
        appnameLabel.setText(StandardApplicationManager.getInstance().getApplicationBinaryName());
        os_version.setText(System.getProperty("os.name"));
        ImageView osImageView = (ImageView) os_version.getGraphic();
        
        if(PlatformUtil.isLinux()){
            //osImageView.setImage(StandardDefaultImageProvider.getInstance().getOSUbuntu_image());
        }else if(PlatformUtil.isWindows()){
            osImageView.setImage(StandardDefaultImageProvider.getInstance().getOSWindows_image());
        }else if(PlatformUtil.isMac()){
            osImageView.setImage(StandardDefaultImageProvider.getInstance().getOSMac_image());
        }
    }
    
    @Override
    public Stage getStage() {
        return getUI().getScene() == null ? null : (Stage) getUI().getScene().getWindow();
    }
    
    public void enableNormalMode(){
        if(getStage() != null && !getStage().isShowing()){
            getStage().show();
        }
    }
    
    public void disableNormalMode(){
        if(getStage() != null && !getStage().isShowing()){
            //here we nullify everything and make it garbage collectable
            getStage().initOwner(null);
            getStage().setScene(null);
            getUI().getScene().getStylesheets().clear();
            System.gc(); System.gc(); System.gc(); System.gc();
        }
    }
    
    public void enableFullTextMode(){
        enableNormalMode();
        getStage().setResizable(true);
        getStage().setHeight(500);
        getStage().setWidth(550);
        update_notice_label.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                update_notice_scroll_container.setVvalue(Double.MAX_VALUE);
            }
        });
    }
    
    private void toggleNotificationState(Node activeNode){
        if(activeNode != null){
            activeNode.setVisible(true);
        }
        
        if(checkProgressBar != activeNode){
            checkProgressBar.setVisible(false);
        }
        
        if(failboxImageView != activeNode){
            failboxImageView.setVisible(false);
        }
        
        if(checkboxImageView != activeNode){
            checkboxImageView.setVisible(false);
        }        
    }
    
}
