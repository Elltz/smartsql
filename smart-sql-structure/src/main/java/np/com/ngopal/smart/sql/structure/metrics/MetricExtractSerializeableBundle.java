/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import np.com.ngopal.smart.sql.db.RemoteMetricModelService;
import np.com.ngopal.smart.sql.model.RemoteMetricModel;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class MetricExtractSerializeableBundle {
   
    private RemoteMetricModelService modelService;    
    private HashMap<String,TreeMap<Long,String>>  metricExtratsBundler; 
    private Gson gson = new Gson();
    private final String pathDirectory;
    private long paramId;
    private int historyLifeSpan;
    private boolean isDatabaseStorage = true;
    private Long bundlerTimeValueMarker = 0l;
    private List<RemoteMetricModel> cachedModels;

    public MetricExtractSerializeableBundle(String pathDirectory, long id) {
        this(pathDirectory, id, 7);
    }
    
    public MetricExtractSerializeableBundle(String pathDirectory, long id, int historyLifeSpan) {
        this.pathDirectory = pathDirectory;
        this.paramId = id;
        this.historyLifeSpan = historyLifeSpan;
        new File(pathDirectory).mkdirs();
        try {
            deriveDataToBundler();
            if (canTrimHistoryData()) {
                trimHistoryDataToLoopBackCount();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if(!isDatabaseStorage){
                if (getOutputFile().exists()) { deleteOutputFile(); }
            }
        }
    }
    
    private void initialiseMetricBundler() {
        if (metricExtratsBundler == null) {
            metricExtratsBundler = new HashMap<>(MetricTypes.values().length);
            for (MetricTypes mt : MetricTypes.values()) {
                metricExtratsBundler.put(mt.name(), new TreeMap<Long, String>());
            }
            updateBundlerTimeMarker();
        }
    }
    
    private final File getOutputFile(){
        return new File(pathDirectory, MetricExtractSerializeableBundle.class.getSimpleName()+ "_" + String.valueOf(paramId));
    }
    
    public void deleteOutputFile(){
        getOutputFile().delete();
    }
    
    private boolean isMetricTypeEqualToRemoteModel(RemoteMetricModel model , MetricTypes mt){
        return mt.name().equalsIgnoreCase(model.getMetricType());
    }
    
    private synchronized void updateBundlerTimeMarker() {
        try {
            for (TreeMap<Long, String> treeMap : metricExtratsBundler.values()) {
                long l = treeMap.lastKey();
                if (l > bundlerTimeValueMarker) {
                    bundlerTimeValueMarker = l;
                }
            }
        } catch (NoSuchElementException nsee) {
            //its okay - it means metricExtratsBundler is empty
            bundlerTimeValueMarker = System.currentTimeMillis();
        }
    }
    
    private final void deriveDataToBundler() throws Exception {
            initialiseMetricBundler();
        if (isDatabaseStorage) {
            modelService = new RemoteMetricModelService("profiler-persistence-unit");
            List<RemoteMetricModel> models = modelService.getModelsByParamIdentifier(paramId);
            for (RemoteMetricModel model : models) {
                metricExtratsBundler.get(model.getMetricType()).put(model.getTimeStamp(), model.getValue());
            }    
            
            updateBundlerTimeMarker();
        } else {
            String retrieveString = new String(Files.readAllBytes(getOutputFile().toPath()), "UTF-8");
            TypeToken mapType = new TypeToken<HashMap<String, TreeMap<Long, String>>>() {};
            metricExtratsBundler = gson.fromJson(retrieveString, mapType.getType());
        }
        
        System.gc();
    }
    
    public synchronized void serializeDataToBundler() throws Exception {
        if(isDatabaseStorage){
            for (MetricTypes mt : MetricTypes.values()) {
                String name = mt.name();
                for(Entry<Long,String> en : metricExtratsBundler.get(name).tailMap(bundlerTimeValueMarker).entrySet()){
                    RemoteMetricModel model = new RemoteMetricModel(en.getKey(), en.getValue(), name, paramId);
                    modelService.save(model);
                }
            }
            updateBundlerTimeMarker();
        }else{
            TypeToken mapType = new TypeToken<HashMap<String,TreeMap<Long,String>>>(){};
            String serializeString = gson.toJson(metricExtratsBundler, mapType.getType());
            Files.write(getOutputFile().toPath(),
                    serializeString.getBytes("UTF-8"),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.WRITE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        }
        System.gc();
    }
    
    public Long addExtractData(MetricExtract extract){
        return addExtractData(extract.getType(), extract.response);
    }
    
    public synchronized Long addExtractData(MetricTypes mt, String extract){        
        TreeMap<Long,String> containee = metricExtratsBundler.get(mt.name());
        long treeKey = System.currentTimeMillis();
        /**
         * Here we are only saving data in Minutes Duration.
         * Since addExtractData is called less than seconds, we add check to see
         * if last data time is 1 minute less than current
         */        
        Entry<Long,String> lastEntry = containee.lastEntry();
        if (lastEntry == null || (treeKey - lastEntry.getKey()) > TimeUnit.MINUTES.toMillis(1)) {
            containee.put(treeKey, extract);
            //try { serializeDataToBundler(); } catch (Exception e) { e.printStackTrace(); }  
        }
        return treeKey;
    }
    
    private Calendar getLoopBackDate(){
        Calendar loopBackDate = Calendar.getInstance();
        loopBackDate.set(Calendar.HOUR_OF_DAY, 0);
        loopBackDate.set(Calendar.MINUTE, 0);
        loopBackDate.set(Calendar.SECOND, 0);
        loopBackDate.add(Calendar.DAY_OF_MONTH, -historyLifeSpan);
        
        System.out.println();
        System.out.println("LOOP BACK DATE = { " +new SimpleDateFormat("yyyy/MM/dd HH:mm").format(loopBackDate.getTime()));
        System.out.println();
        
        return loopBackDate;
    }
    
    public SortedMap<Long,String> getFullCountHistoryData(MetricTypes mt){
        return getHistoricDataFromPointInTime(mt, getLoopBackDate().getTimeInMillis());
    }
    
    public synchronized SortedMap<Long,String> getHistoricDataFromPointInTime(MetricTypes mt, Long keyTime){
        return metricExtratsBundler.get(mt.name()).tailMap(keyTime);
    }
    
    private synchronized boolean canTrimHistoryData(){
        boolean cantrimHistory = false;
        
        try{ 
            cantrimHistory = getLoopBackDate().getTimeInMillis() > metricExtratsBundler.values().iterator().next().firstKey();
        }catch(NoSuchElementException nsee){ }
        
        return cantrimHistory;
    }
    
    private synchronized void trimHistoryDataToLoopBackCount() {
        RemoteMetricModel metricModel = null;
        for (Entry<String, TreeMap<Long, String>> treeMapEntry : metricExtratsBundler.entrySet()) {
            long pre = System.currentTimeMillis();
            SortedMap<Long, String> headmap = treeMapEntry.getValue().headMap(getLoopBackDate().getTimeInMillis());
            if (isDatabaseStorage) {
                if(metricModel == null){ metricModel = new RemoteMetricModel(); }
                metricModel.setMetricType(treeMapEntry.getKey());
                metricModel.setParamIdentifier(paramId);
                for (Entry<Long, String> en : headmap.entrySet()) {
                    metricModel.setTimeStamp(en.getKey());
                    metricModel.setValue(en.getValue());
                    modelService.delete(getInBreadFromAlikeProps(metricModel));
                }
                splitCache();
            }
            headmap.clear();
            System.out.println("TIME TAKEN TO TRIM HISTORY DATA (secs) " + String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - pre)));
        }
    }
    
    public List<RemoteMetricModel> getAll(boolean cachedReturn) {
        if(cachedReturn){
            if(cachedModels == null){
                cachedModels = modelService.getAll();
            }
            return cachedModels;
        }else{
            splitCache();
            return modelService.getAll();
        }
    }
    
    public void splitCache() {
        cachedModels = null;
        System.gc();
    }
    
    public RemoteMetricModel getInBreadFromAlikeProps(RemoteMetricModel a){
        for(RemoteMetricModel model : getAll(true)){
            if(a.isMutualOnProps(model)){
                return model;
            }
        }
        return null;
    }
}
