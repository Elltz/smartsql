/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class FullTableSQLScanData implements FilterableData {

    private final SimpleStringProperty query;
    private final SimpleStringProperty db;
    private final SimpleStringProperty execCount;
    private final SimpleStringProperty totalLatency;
    private final SimpleStringProperty indexUsedCount;
    private final SimpleStringProperty goodIndexUsedCount;
    private final SimpleStringProperty indexUsedPct;
    private final SimpleStringProperty rowSent;
    private final SimpleStringProperty rowsExamined;
    private final SimpleStringProperty rowsSentAvg;
    private final SimpleStringProperty rowsExaminedAvg;
    private final SimpleStringProperty firstSeen;
    private final SimpleStringProperty lastSeen;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public FullTableSQLScanData(String query, String db, String exeCount, String totalLatency, String indexUsedCount, String goodIndexUsedCount,
            String indexUsedPct, String rowSent, String rowsExamined, String rowsSentAvg, String rowsExaminedAvg, String firstSeen, String lastSeen) {
        this.query = new SimpleStringProperty(query);
        this.db = new SimpleStringProperty(db);
        this.execCount = new SimpleStringProperty(exeCount);
        this.totalLatency = new SimpleStringProperty( convert(Long.valueOf(totalLatency) / 1000000000l) );
        this.indexUsedCount = new SimpleStringProperty(indexUsedCount);
        this.goodIndexUsedCount = new SimpleStringProperty(goodIndexUsedCount);
        this.indexUsedPct = new SimpleStringProperty(indexUsedPct);
        this.rowSent = new SimpleStringProperty(rowSent);
        this.rowsExamined = new SimpleStringProperty(rowsExamined);
        this.rowsSentAvg = new SimpleStringProperty(rowsSentAvg);
        this.rowsExaminedAvg = new SimpleStringProperty(rowsExaminedAvg);
        this.firstSeen = new SimpleStringProperty(firstSeen);
        this.lastSeen = new SimpleStringProperty(lastSeen);
    }

    @Override
    public String[] getValuesFoFilter() {
        return new String[]{getQuery(), getDb(), getExecCount(), getTotalLatency(), getIndexUsedCount(), getGoodIndexUsedCount(), getIndexUsedPct(),
            getRowSent(), getRowsExamined(), getRowsSentAvg(), getRowsExaminedAvg(), getFirstSeen(), getLastSeen()};
    }

    public String getQuery() {
        return query.getValue();
    }

    public String getDb() {
        return db.getValue();
    }

    public String getExecCount() {
        return execCount.getValue();
    }

    public String getTotalLatency() {
        return totalLatency.getValue();
    }

    public String getIndexUsedCount() {
        return indexUsedCount.getValue();
    }

    public String getGoodIndexUsedCount() {
        return goodIndexUsedCount.getValue();
    }

    public String getIndexUsedPct() {
        return indexUsedPct.getValue();
    }

    public String getRowSent() {
        return rowSent.getValue();
    }

    public String getRowsExamined() {
        return rowsExamined.getValue();
    }

    public String getRowsSentAvg() {
        return rowsSentAvg.getValue();
    }

    public String getRowsExaminedAvg() {
        return rowsExaminedAvg.getValue();
    }

    public String getFirstSeen() {
        return firstSeen.getValue();
    }

    public String getLastSeen() {
        return lastSeen.getValue();
    }

    private String convert(Long val){
        //val is in milliseconds
        TimeUnit unit = TimeUnit.MILLISECONDS;
        if(unit.toHours(val) > 0){
            dateFormat.applyPattern("HH 'h'");
        }else if(unit.toMinutes(val) > 0){
            dateFormat.applyPattern("mm 'm'");
        }else if(unit.toSeconds(val) > 0){
            dateFormat.applyPattern("ss 's'");
        }else {
            dateFormat.applyPattern("SSS 'ms'");
        }        
        return dateFormat.format(new Date(val));
    }
    
}
