package np.com.ngopal.smart.sql.structure.components;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import lombok.extern.slf4j.Slf4j;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class TooltipHelper {

    private Map<String, List<String>> functions = new HashMap<>();
    
    private static TooltipHelper instance;
    private Popup popup;
    private RSyntaxTextArea control;
    private int initPosition = -1;
    
    private KeyListener keyListener;
    
    private TooltipHelper() {
        try {
            InputStream is = getClass().getResourceAsStream("/META-INF/completionsBaseFunctions");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split("=");
                if (data.length == 2) {
                    List<String> values = functions.get(data[0]);
                    if (values == null) {
                        values = new ArrayList<>();
                        functions.put(data[0], values);
                    }
                    values.add(data[1]);
                }
            }
            // FOr test dubplicates
//            for (String key: functions.keySet()) {
//                if (functions.get(key).size() > 1) {
//                    log.debug("DUBLICATE KEY: " + key);
//                }
//            }
            
            br.close();
            isr.close();
            is.close();            
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
    
    public static TooltipHelper getInstance() {
        if (instance == null) {
            instance = new TooltipHelper();
        }
        
        return instance;
    }
    
    
    private List<String> getFunctionsCompletion(String enteredText) {
        List<String> result = new ArrayList<>();
        
        for (String f: functions.keySet()) {
            if (f != null && f.toLowerCase().startsWith(enteredText.toLowerCase())) {
                if (functions.get(f) != null) {
                    for (String s: functions.get(f)) {
                        result.add(s);
                    }
                }
            }
        }

        return result;
    }
    
    public void hideTooltip() {
        if (popup != null && popup.isShowing()) {
            popup.hide();
        }

        if (keyListener != null && this.control != null) {
            this.control.removeKeyListener(keyListener);
        }
    }
    
    public boolean isShowed() {
        return popup != null && popup.isShowing();
    }
    
    public void showTooltip(Stage owner, RSyntaxTextArea control, String functionKey) {        
        
        List<String> completions = getFunctionsCompletion(functionKey);
        if (completions.isEmpty()) {
            return;
        }
        
        
        if (popup != null && popup.isShowing()) {
            popup.hide();
        }

        if (keyListener != null && this.control != null) {
            this.control.removeKeyListener(keyListener);
        }
        
        this.control = control;
        this.initPosition = control.getCaretPosition() + 1;
        
        popup = new Popup();
        popup.setAutoHide(true);
        popup.onCloseRequestProperty().addListener((ObservableValue<? extends EventHandler<WindowEvent>> observable, EventHandler<WindowEvent> oldValue, EventHandler<WindowEvent> newValue) -> {
            control.removeKeyListener(keyListener);
        });
        
        MagicBox box = new MagicBox(completions);
        popup.getContent().add(box);
        
        keyListener = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (initPosition > -1 && control.getCaretPosition() > initPosition) {
                    try {
                        String text = control.getText(initPosition, control.getCaretPosition() - initPosition);
                        box.setPosition(text.split(",", -1).length - 1);
                    } catch (BadLocationException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                }
            }
        };
        
        control.addKeyListener(keyListener);
        
        Rectangle r = null;
        try {
            r = control.modelToView(control.getCaretPosition());
        } catch (BadLocationException ble) {
            ble.printStackTrace();
            return;
        }
                
        // 8 and 18 - shift for popup maybe need cacl it depeneded on font
        Point p = new Point(r.x + 8, r.y + 18);
        SwingUtilities.convertPointToScreen(p, control);
        
        popup.show(owner, p.x, p.y);
    }
    
    
    private class MagicBox extends VBox {
        
        private Map<String, List<Label>> labels = new HashMap<>();
        private int position = -1;
        
        public MagicBox(List<String> functions) {
            
            setStyle("-fx-padding: 3px; -fx-border-color: #c0c0c0 black black #c0c0c0; -fx-background-color: #ffffe1");
            
            for (String f: functions) {
                String[] row = f.split(",");
                
                HBox hb = new HBox();
                for (int i = 0; i < row.length; i++) {
                    
                    String r = row[i];
                    
                    if (!hb.getChildren().isEmpty()) {
                        hb.getChildren().add(new Label(","));
                    }
                    
                    if (i == 0) {
                        String[] s = r.split("\\(");
                        hb.getChildren().add(new Label(s[0] + "("));
                        
                        r = s[1];
                    }

                    boolean last = false;
                    if (i == row.length - 1) {
                        last = true;
                        r = r.substring(0, r.length() - 1);
                    }
                    
                    Label l = new Label(r);
                    
                    List<Label> ls = labels.get(f);
                    if (ls == null) {
                        ls = new ArrayList<>();
                        labels.put(f, ls);
                    }
                    
                    ls.add(l);
                    
                    hb.getChildren().add(l);
                    
                    if (last) {
                        hb.getChildren().add(new Label(")"));
                    }
                }
                
                getChildren().add(hb);
            }
            
            setPosition(0);
        }
        
        public void setPosition(int position) {

            Platform.runLater(() -> {
                for (List<Label> list: labels.values()) {

                    for (Label l: list) {
                        l.setStyle("");
                    }
                    
                    Label l = null;
                    for (int i = 0; i < list.size(); i++) {
                        l = list.get(i);
                        l.setStyle("");
                        if (i == position) {
                            break;
                        }
                    }

                    if (l != null) {
                        l.setStyle("-fx-text-fill: red");
                    }
                }
            });
        }
    }
}
