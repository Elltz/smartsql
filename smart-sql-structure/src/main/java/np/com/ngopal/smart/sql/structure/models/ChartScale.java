/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public enum ChartScale {

    SCALE_8H("25%", 8 * 60 * 60 * 1000),
    SCALE_4H("50%", 4 * 60 * 60 * 1000),
    SCALE_2H("100%", 2 * 60 * 60 * 1000),
    SCALE_1H("200%", 1 * 60 * 60 * 1000),
    SCALE_30M("400%", 30 * 60 * 1000),
    SCALE_15M("800%", 15 * 60 * 1000),
    SCALE_450MS("1600%", 450 * 1000),
    SCALE_225MS("3200%", 225 * 1000),
    SCALE_112MS("6400%", 112 * 1000),
    SCALE_56MS("12800%", 56 * 1000),
    SCALE_28MS("25600%", 28 * 1000),
    SCALE_10MS("51200%", 10 * 1000);

    private final String name;
    private final long value;

    ChartScale(String name, long value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public long getValue() {
        return value;
    }

}
