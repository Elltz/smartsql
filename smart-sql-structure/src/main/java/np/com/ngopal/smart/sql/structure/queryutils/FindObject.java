package np.com.ngopal.smart.sql.structure.queryutils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface FindObject {

    public int getCaretPosition(byte a);
    
    public void replaceText(int from, int to, String text);
    
    public String getText();
    
    public void clear();
    
    public void appendText(String text);
    
    public void selectRange(int from, int to);
}
