
package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import java.util.Objects;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import np.com.ngopal.smart.sql.model.PreferenceData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DebugRunningQueryData implements FilterableData, DebugTimeProvider, DebugExplainProvider {
    
    private final SimpleObjectProperty id;
    private final SimpleStringProperty user;
    private final SimpleStringProperty host;
    private final SimpleStringProperty database;
    private final SimpleStringProperty command;
    private final SimpleObjectProperty time;
    private final SimpleStringProperty state;
    private final SimpleStringProperty query;
      
 
    private Long rowsSum = null;
    
    public DebugRunningQueryData(Long id, String user, String host, String command, String state, BigDecimal time, String query, String database) {        
        this.id = new SimpleObjectProperty(id);
        this.time = new SimpleObjectProperty(time);
        this.query = new SimpleStringProperty(query);
        this.database = new SimpleStringProperty(database);
        this.user = new SimpleStringProperty(user);
        this.host = new SimpleStringProperty(host);
        this.command = new SimpleStringProperty(command);
        this.state = new SimpleStringProperty(state);
    }
    
    @Override
    public void setRowsSum(Long rowsSum) {
        this.rowsSum = rowsSum;
    }
    @Override
    public Long getRowsSum() {
        return rowsSum;
    } 
    
    @Override
    public BigDecimal getTime() {
        return (BigDecimal) time.get();
    }
    public void setTime(BigDecimal time) {
        this.time.set(time);
    }
    
    public Long getId() {
        return (Long) id.get();
    }
    public void setId(Long id) {
        this.id.set(id);
    }
    
    public String getUser() {
        return user.get();
    }
    public void setUser(String user) {
        this.user.set(user);
    }
    
    public String getHost() {
        return host.get();
    }
    public void setHost(String host) {
        this.host.set(host);
    }
    
    public String getCommand() {
        return command.get();
    }
    public void setCommand(String command) {
        this.command.set(command);
    }
    
    @Override
    public DebugTimeProvider getTimeProvider() {
        return this;
    }
    
    @Override
    public String getState() {
        return state.get();
    }
    public void setState(String state) {
        this.state.set(state);
    }
    
    public String getQuery() {
        return query.get();
    }
    public void setQuery(String query) {
        this.query.set(query);
    }
    
    @Override
    public String getDatabase() {
        return database.get();
    }
    public void setDatabase(String database) {
        this.database.set(database);
    }
    
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {            
            getUser() != null ? getUser() : "",
            getHost() != null ? getHost() : "",
            getDatabase() != null ? getDatabase() : "",
            getCommand() != null ? getCommand() : "",
            getTime() != null ? getTime().toString() : "",
            getState() != null ? getState() : "",
            getQuery() != null ? getQuery() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getUser(),
            getHost(),
            getDatabase(),
            getCommand(),
            getTime(),
            getState(),
            getQuery()
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DebugRunningQueryData) {
            DebugRunningQueryData d = (DebugRunningQueryData) obj;
            
            return Objects.equals(d.getId(), getId()) && Objects.equals(d.getQuery(), getQuery()) && Objects.equals(d.getDatabase(), getDatabase());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        hash = 41 * hash + Objects.hashCode(this.query);
        hash = 41 * hash + Objects.hashCode(this.database);
        return hash;
    }
    
    

    @Override
    public String getColor(PreferenceData pd) {
        String color = COLORS[4];
        
        int slow = 6;
        try {
            slow = Integer.valueOf(pd.getDebuggerRQSlow());
        } catch (Throwable ex) {}
        
        int medium = 3;
        try {
            medium = Integer.valueOf(pd.getDebuggerRQMedium());
        } catch (Throwable ex) {}
        
        int normal = 1;
        try {
            normal = Integer.valueOf(pd.getDebuggerRQNormal());
        } catch (Throwable ex) {}
        
        if (!"Sleep".equalsIgnoreCase(getState())) {            
            if (getTime().doubleValue() > slow) {
                color = COLORS[0];
            } else if (getTime().doubleValue() > medium) {
                color = COLORS[2];
            } else if (getTime().doubleValue() > normal) {
                color = COLORS[3];
            }
        }
        
        return color;
    }

}