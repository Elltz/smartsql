/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class RemoteMetricExtractor {

    private String osType;
    private String remoteHostIpAddr;
    private char[] remotePassPhrase;
    private String remotePrivateKey;
    private String remoteUserName;
    private MetricTypes[] bindingMetricTypes;
    private long delayInSeconds;
    private int port;
    private JSch jsch;
    private Session session;
    private Channel channel;
    private String remoteUserRootPassword;
    private String linuxTopCommandPingingValue;
    private WorkProc scriptsExecutor;
    private volatile boolean executionStatus, runningStatus;
    private String remoteCommandResponse;
    private MetricListener metricListener;
    private final String replaceableText = " ";

    private MetricExtract cpuExtract, diskIoExtract, memoryExtract, swapMemExtract, networkExtract;

    public RemoteMetricExtractor(ConnectionParams params, MetricTypes... bindingMetricTypes) {
        this(params, bindingMetricTypes, null);
    }

    public RemoteMetricExtractor(ConnectionParams params, MetricTypes[] bindingMetricTypes, MetricListener metricListener) {
        this(params.getOsName(), params.getOsHost(), params.getOsPassword().toCharArray(), params.getOsPrivateKey(), params.getOsUser(), bindingMetricTypes, 1, params.getOsPort(), params.getSshPassword(), metricListener);
    }

    public RemoteMetricExtractor(String osType, String remoteUserName, String remoteHostIpAddr, char[] remotePassPhrase, int port, MetricTypes[] bindingMetricTypes) {
        this(osType, remoteHostIpAddr, remotePassPhrase, null, remoteUserName, bindingMetricTypes, 1, port);
    }

    public RemoteMetricExtractor(String osType, String remoteUserName, String remoteHostIpAddr, String remotePrivateKey, int port, MetricTypes[] bindingMetricTypes) {
        this(osType, remoteHostIpAddr, null, remotePrivateKey, remoteUserName, bindingMetricTypes, 1, port);
    }

    public RemoteMetricExtractor(String osType, String remoteHostIpAddr, char[] remotePassPhrase, String remotePrivateKey, String remoteUserName, MetricTypes[] bindingMetricTypes, long delayInSeconds, int port) {
        this(osType, remoteHostIpAddr, remotePassPhrase, remotePrivateKey, remoteUserName, bindingMetricTypes, delayInSeconds, port, null, null);
    }

    public RemoteMetricExtractor(String osType, String remoteHostIpAddr, char[] remotePassPhrase, String remotePrivateKey, String remoteUserName,
            MetricTypes[] bindingMetricTypes, long delayInSeconds, int port, String remoteUserRootPassword, MetricListener metricListener) {
        //this.osType = osType;
        this.remoteHostIpAddr = remoteHostIpAddr;
        this.remotePassPhrase = remotePassPhrase;
        this.remotePrivateKey = remotePrivateKey;
        this.remoteUserName = remoteUserName;
        this.bindingMetricTypes = bindingMetricTypes;
        this.delayInSeconds = delayInSeconds;
        this.port = port;
        this.remoteUserRootPassword = remoteUserRootPassword;
        this.metricListener = metricListener;
        createJSCH();
        createExtracts();
        if(linuxTopCommandPingingValue == null){
            linuxTopCommandPingingValue = "1";
        }
    }
    
    public void updateParamDetails(ConnectionParams params){
        this.osType = null/*params.getOsName()*/;
        this.remoteHostIpAddr = params.getOsHost();
        this.remotePassPhrase = params.getOsPassword().toCharArray();
        this.remotePrivateKey = params.getOsPrivateKey();
        this.remoteUserName = params.getOsUser();
        this.port = params.getOsPort();
        this.remoteUserRootPassword = params.getSshPassword();
        jschPrivateKeySet();
    }

    private final void createJSCH() {
        jsch = new JSch();
        jschPrivateKeySet();
    }

    private final boolean isUsingPrivateKey() {
        return remotePrivateKey != null && !remotePrivateKey.isEmpty();
    }

    private final void jschPrivateKeySet() {
        try {
            jsch.removeAllIdentity();
            if (isUsingPrivateKey()) {
                jsch.addIdentity(remotePrivateKey, new String(remotePassPhrase));
            }
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    public void disconnectCurrentStream() {

        if (channel != null) {
            channel.disconnect();
            channel = null;
        }

        if (session != null) {
            session.disconnect();
            session = null;
        }

    }

    private boolean jschRemoteConnector() {
        boolean res = false;
        try {
            session = jsch.getSession(remoteUserName, remoteHostIpAddr, port);
            session.setConfig("StrictHostKeyChecking", "no");
            if (!isUsingPrivateKey()) {
                session.setPassword(new String(remotePassPhrase));
            }
            session.setServerAliveCountMax(3);
            session.setServerAliveInterval((int) TimeUnit.MINUTES.toMillis(1));//1 minute
            session.setDaemonThread(true);
            session.connect();
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private InputStream jschStream() {
        InputStream in = null;
        try {
            channel = session.openChannel("exec");
            in = channel.getInputStream();
            ((ChannelExec) channel).setErrStream(System.err);
            ((ChannelExec) channel).setCommand(getOsScripts().trim());
            channel.connect();
            //Thread.sleep(5 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return in;
        }
    }

    private void appendCommandsToBuilder_windows(MetricTypes m, StringBuilder queriesLooper) {
        defaultMetricTypeDelimitor(queriesLooper, m.name(), "&");

        switch (m) {
            case CPU_METRIC:
                break;
            case DISK_IO_METRIC:
                break;
            case DISK_LATENCY_METRIC:
                break;
            case MEMORY_METRIC:
                break;
            case NETWORK_METRIC:
                break;
            case SWAP_MEMORY_METRIC:
                break;
        }

        defaultMetricTypeDelimitor(queriesLooper, m.name(), "&");
    }

    private void appendCommandsToBuilder_linux(MetricTypes m, StringBuilder queriesLooper) {
        defaultMetricTypeDelimitor(queriesLooper, m.name(), ";");
        try {
            
            switch (m) {
                case CPU_METRIC:
                /*case MEMORY_METRIC:*/
                case SWAP_MEMORY_METRIC:
                    queriesLooper.append("top -b -n ").append(linuxTopCommandPingingValue);
                    break;
                case DISK_IO_METRIC:                    
                    queriesLooper.append("df /");
                    break;
                case DISK_LATENCY_METRIC:
                    break;
                case NETWORK_METRIC:
                    queriesLooper.append("cat /proc/net/dev | grep :");
                    break;
                case MEMORY_METRIC:
                    queriesLooper.append("cat /proc/meminfo");
            }
            
            queriesLooper.append("; ");
        } catch (Exception e) {
            e.printStackTrace();
        }

        defaultMetricTypeDelimitor(queriesLooper, m.name(), ";");
    }

    private final void defaultMetricTypeDelimitor(StringBuilder queriesLooper, String name, String commandConcatnator) {
        queriesLooper.append("echo ")
                .append(name)
                .append(commandConcatnator)
                .append(" ");
    }

    public String getOsScripts() {
        StringBuilder queriesLooper = new StringBuilder(1000);
        /**
         * here we bing the individual commands with the server specific set of
         * codes
         */
        if (osType != null && osType.startsWith("Win")) {
            queriesLooper.append("@echo off \r\n");
            queriesLooper.append("for /L %%A in (1,0,1) DO ");

            for (MetricTypes m : getBindingMetricTypes(true)) {
                appendCommandsToBuilder_windows(m, queriesLooper);
            }
            queriesLooper.append("timeout ");
            queriesLooper.append(delayInSeconds);
            queriesLooper.append(" > nul");
            System.out.println(queriesLooper.toString());
            try {
                Path f = Files.createTempFile("tempExecutorPath", ".bat");
                f.toFile().deleteOnExit();
                Files.write(f, queriesLooper.toString().getBytes());
                queriesLooper.delete(0, 10000);

                queriesLooper.append(f.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else /*if(serverType.startsWith("Lin"))*/ {

            queriesLooper.append("while sleep ");
            queriesLooper.append(delayInSeconds);
            queriesLooper.append("; do ");
            for (MetricTypes m : getBindingMetricTypes(true)) {
                appendCommandsToBuilder_linux(m, queriesLooper);
            }
            queriesLooper.append("done");
            System.out.println(queriesLooper.toString());
        }

        return queriesLooper.toString();
    }

    public boolean testConnection() {
        boolean res = connectToServer();
        disconnectCurrentStream();
        return res;
    }

    public void connectAndExecuteServer() {
        if (connectToServer()) {
            executeCommands();
        }
    }

    public boolean connectToServer() {
        return jschRemoteConnector();
    }

    public void executeCommands() {
        if (scriptsExecutor == null) {
            scriptsExecutor = new WorkProc() {

                private void loopTillCatchCondtion(BufferedReader buffReader) throws Exception {
                    int numberOfNotReadyLoops = 0;
                    while (!buffReader.ready()) {
                        if (!isRunningStatus() || numberOfNotReadyLoops >= Flags.MAXIMUM_NUMBER_OF_WAITING_CYCLE_COUNT) {
                            break;
                        }
                        Thread.sleep(TimeUnit.SECONDS.toMillis(delayInSeconds));
                        numberOfNotReadyLoops++;
                    }
                }

                @Override
                public void updateUI() { }

                @Override
                public void run() {
                    System.out.println("REMOTEMTRICEXTRACTOR : = STARTING CODE ON SCRIPT EXECUTOR RUN");
                    InputStream stream = null;
                    setRunningStatus(true);

                    try {
                        stream = jschStream();
                        if (stream != null) {
                            StringBuffer currentActiveStream = new StringBuffer(1000);
                            BufferedReader buffReader = new BufferedReader(new InputStreamReader(stream));
                            /**
                             * Wait for the process to issue its first response
                             * meaning, it has some data
                             */
                            loopTillCatchCondtion(buffReader);
                            
                            while (isRunningStatus()&& buffReader.ready()) {
                                String line = buffReader.readLine();
                                if (line.isEmpty()) { continue; }
                                
                                if(isExecutionStatus()){                                
                                    boolean endsWithTag = line.endsWith(Flags.METRIC_DELIMITER_TAGS);

                                    if (endsWithTag && currentActiveStream.length() > 0) {
                                        //end tags
                                        parseStringToMetricStandard(line, currentActiveStream.toString().trim());
                                        currentActiveStream.delete(0, currentActiveStream.length() + 1);
                                        
                                    } else if (endsWithTag && currentActiveStream.length() == 0) {
                                        //start tags   
                                        currentActiveStream.append(replaceableText);
                                        
                                    } else if (!endsWithTag && currentActiveStream.length() > 0) {
                                        //extracting point
                                        currentActiveStream.append(line).append(System.lineSeparator());

                                    } else {
                                        //non-configured point
                                        System.out.println("ELSE :" + line);
                                    
                                    }

                                    loopTillCatchCondtion(buffReader);
                                }else{
                                    Thread.sleep(1000);                                    
                                }
                            }
                        }

                        stop();
                        System.gc(); System.gc(); System.gc();

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        stream = null;
                        stop();
                    }
                    System.out.println("REMOTEMTRICEXTRACTOR : = ENDING CODE ON SCRIPT EXECUTOR RUN");
                }
            };
        }

        if (isRunningStatus()) {
            throw new RuntimeException("RemoteMetricExtractor instance already running");
        }
        setExecutionStatus(true);
        Recycler.addWorkProc(scriptsExecutor);
    }

    public void stop() {
        setRunningStatus(false);
        if(metricListener != null){
            metricListener.onDone();
        }
    }

    private void parseStringToMetricStandard(String delimiterReference, String allResponse) {
        //System.out.println(delimiterReference);
       // System.out.println(allResponse);

        //No need parsing when we have no metricListener
        if (metricListener == null) { return; }

        MetricTypes type = MetricTypes.getType(delimiterReference);
        /**
         * this section is for parsed extract linux or Unix server
         */
        if (osType == null) {
            switch (type) {
                case CPU_METRIC:
                    cpuExtract.update(allResponse);
                    metricListener.onReceiveRemoteMetrics(MetricTypes.CPU_METRIC, cpuExtract);
                    swapMemExtract.update(allResponse);
                    metricListener.onReceiveRemoteMetrics(MetricTypes.SWAP_MEMORY_METRIC, swapMemExtract);
                    break;
                case DISK_IO_METRIC:
                    diskIoExtract.update(allResponse);
                    metricListener.onReceiveRemoteMetrics(MetricTypes.DISK_IO_METRIC, diskIoExtract);
                    break;
                case NETWORK_METRIC:
                    networkExtract.update(allResponse);
                    metricListener.onReceiveRemoteMetrics(MetricTypes.NETWORK_METRIC, networkExtract);
                    break;
                case MEMORY_METRIC:
                    memoryExtract.update(allResponse);
                    metricListener.onReceiveRemoteMetrics(MetricTypes.MEMORY_METRIC, memoryExtract);
                    break;
            }
        }
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getRemoteHostIpAddr() {
        return remoteHostIpAddr;
    }

    public void setRemoteHostIpAddr(String remoteHostIpAddr) {
        this.remoteHostIpAddr = remoteHostIpAddr;
    }

    public String getRemotePrivateKey() {
        return remotePrivateKey;
    }

    public void setRemotePrivateKey(String remotePrivateKey) {
        this.remotePrivateKey = remotePrivateKey;
    }

    public String getRemoteUserName() {
        return remoteUserName;
    }

    public void setRemoteUserName(String remoteUserName) {
        this.remoteUserName = remoteUserName;
    }

    public long getDelayInSeconds() {
        return delayInSeconds;
    }

    public void setDelayInSeconds(long delayInSeconds) {
        this.delayInSeconds = delayInSeconds;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getRemoteUserRootPassword() {
        return remoteUserRootPassword;
    }

    public void setRemoteUserRootPassword(String remoteUserRootPassword) {
        this.remoteUserRootPassword = remoteUserRootPassword;
    }

    public String getLinuxTopCommandPingingValue() {
        return linuxTopCommandPingingValue;
    }

    public void setLinuxTopCommandPingingValue(String linuxTopCommandPingingValue) {
        this.linuxTopCommandPingingValue = linuxTopCommandPingingValue;
    }

    public synchronized boolean isExecutionStatus() {
        return executionStatus;
    }

    public synchronized void setExecutionStatus(boolean executionStatus) {
        this.executionStatus = executionStatus;
    }

    public synchronized boolean isRunningStatus() {
        return runningStatus;
    }

    private synchronized void setRunningStatus(boolean runningStatus) {
        this.runningStatus = runningStatus;
    }

    public MetricListener getMetricListener() {
        return metricListener;
    }

    public void setMetricListener(MetricListener metricListener) {
        this.metricListener = metricListener;
    }

    private void createExtracts() {
        memoryExtract = new MemoryMetricExtract(MetricTypes.MEMORY_METRIC);
        cpuExtract = new CpuMetricExtract(MetricTypes.CPU_METRIC);
        diskIoExtract = new DiskIOMetricExtract(MetricTypes.DISK_IO_METRIC);
        swapMemExtract = new SwapMemoryMetricExtract(MetricTypes.SWAP_MEMORY_METRIC);
        networkExtract = new NetworkMetricExtract(MetricTypes.NETWORK_METRIC);
    }

    public MetricTypes[] getBindingMetricTypes(boolean filtered) {
        MetricTypes[] metricTypesToReturn = bindingMetricTypes;
        if (filtered && bindingMetricTypes != null) {

            boolean containsCpu = false;
            boolean containsMemory = false;
            boolean containSwapMemory = false;
            boolean containsNetwork = false;
            boolean containsDisk = false;
            boolean containsDisk_lat = false;

            for (MetricTypes m : bindingMetricTypes) {
                switch (m) {
                    case CPU_METRIC:
                        containsCpu = true;
                        break;
                    case DISK_IO_METRIC:
                        containsDisk = true;
                        break;
                    case DISK_LATENCY_METRIC:
                        containsDisk_lat = true;
                        break;
                    case MEMORY_METRIC:
                        containsMemory = true;
                        break;
                    case NETWORK_METRIC:
                        containsNetwork = true;
                        break;
                    case SWAP_MEMORY_METRIC:
                        containSwapMemory = true;
                        break;
                }
            }

            if (osType != null && !osType.startsWith("Win")) {
                if (containsCpu | containSwapMemory) {
                    MetricTypes[] mt = new MetricTypes[bindingMetricTypes.length];
                    mt[0] = MetricTypes.CPU_METRIC;
                    int i = 1;
                    for (MetricTypes m : bindingMetricTypes) {
                        if (m != MetricTypes.CPU_METRIC && /*m != MetricTypes.MEMORY_METRIC &&*/ m != MetricTypes.SWAP_MEMORY_METRIC) {
                            mt[i] = m;
                            i++;
                        }
                    }
                    metricTypesToReturn = new MetricTypes[i];
                    for (i = 0; i < metricTypesToReturn.length; i++) {
                        metricTypesToReturn[i] = mt[i];
                    }
                }
            }
        }
        
        return metricTypesToReturn;
    }
    
}
