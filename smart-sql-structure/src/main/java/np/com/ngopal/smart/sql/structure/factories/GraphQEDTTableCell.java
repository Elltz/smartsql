package np.com.ngopal.smart.sql.structure.factories;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.models.GraphData;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.Flags;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class GraphQEDTTableCell extends TableCell<GraphData, String> {        
    
    private final Image[] GRAPH_IMAGES = new Image[] {
        StandardDefaultImageProvider.getInstance().getGraph_0_image(),
        StandardDefaultImageProvider.getInstance().getGraph_1_image(),
        StandardDefaultImageProvider.getInstance().getGraph_2_image(),
        StandardDefaultImageProvider.getInstance().getGraph_3_image(),
        StandardDefaultImageProvider.getInstance().getGraph_4_image()
    };
    
    private final Random GRAPH_RANDOM = new Random();
   
    private final SimpleDateFormat GRAPH1_FORMAT = new SimpleDateFormat("yy-MM-dd HH:mm:ss.S");
    
    private final int QEDT_GROUP_STEP = 60 * 1000;
    
    private Stage parent;
    
    public GraphQEDTTableCell(Stage s) {
        this.parent = s;
    }
    
    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty); 

        if (!empty && item != null) {
            setAlignment(Pos.CENTER);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            Button b = new Button();
            b.setPadding(new Insets(3));
            b.setGraphic(new ImageView(GRAPH_IMAGES[GRAPH_RANDOM.nextInt(5)]));
            b.setOnAction((ActionEvent event) -> {
                String[] times = item.split(",");
                Map<Date, Integer> map = new LinkedHashMap<>();
                if (times.length > 0) {
                    Date lowerX = null;
                    Date greaterX = null;
                    Integer lowerY = 0;
                    Integer greaterY = 0;
                    for (String t: times) {
                        try {
                            Date d = GRAPH1_FORMAT.parse(t.trim());
                            if (lowerX == null) {
                                lowerX = d;
                            } else if (lowerX.after(d)) {
                                lowerX = d;
                            }

                            if (greaterX == null) {
                                greaterX = d;
                            } else if (greaterX.before(d)) {
                                greaterX = d;
                            }

                            Integer count = map.get(d);
                            if (count == null) {
                                count = 0;
                            }
                            count++;

                            if (greaterY < count) {
                                greaterY = count;
                            }

                            map.put(d, count);
                        } catch (ParseException ex) {
                            log.error("Error", ex);
                        }
                    }

                    BorderPane box = new BorderPane();
                    box.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS);

                    long stepY = (greaterY - lowerY) / 10;
                    if (stepY == 0) {
                        stepY = 1;
                    }

                    // if rows more that 19k need add step 1 min or 60000 ms
                    long stepX = (greaterX.getTime() - lowerX.getTime()) / 10;
                    if (stepX < 1000) {
                        stepX = 1000;
                    }            

                    if (stepX < 60000 && times.length > 10000) {
                        stepX = 60000;
                    }


                    NumberAxis xAxis = new NumberAxis(lowerX.getTime() - 2000, greaterX.getTime() + 2000, stepX);
                    NumberAxis yAxis = new NumberAxis(lowerY - 1, greaterY + 1, stepY);
                    yAxis.setLabel("Queries");
                    xAxis.setTickLabelRotation(-90);
                    xAxis.setLabel("Time");
                    xAxis.setTickLabelFormatter(new StringConverter<Number>() {

                        @Override
                        public String toString(Number t) {
                            return GRAPH1_FORMAT.format(new Date(t.longValue()));
                        }

                        @Override
                        public Number fromString(String string) {
                            try {
                                return GRAPH1_FORMAT.parse(string).getTime();
                            } catch (ParseException ex) {
                                log.error("Error", ex);
                            }

                            return 0;
                        }
                    });

                    //creating the chart
                    LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
                    lineChart.getStyleClass().addAll(Flags.METRIC_INNERS_STYLE_CLASS);

                    //defining a series
                    XYChart.Series series = new XYChart.Series();

                    lineChart.setLegendVisible(false);

                    //populating the series with data

                    boolean needGroup = map.size() > 1000;
                    Map.Entry<Date, Integer> first = null;
                    for (Map.Entry<Date, Integer> m: map.entrySet()) {

                        boolean needAdd = !needGroup;

                        if (needGroup) {
                            if (first == null) {
                                first = m;
                            } else {
                                if (m.getKey().getTime() - first.getKey().getTime() > QEDT_GROUP_STEP) {                                        
                                    needAdd = true;
                                    first = null;
                                }
                            }
                        } 

                        if (needAdd) {
                            try {

                                XYChart.Data data = new XYChart.Data(m.getKey().getTime(), m.getValue());
                                series.getData().add(data);

                                Label l = new Label();
                                l.setPrefSize(10, 10);
                                l.setTooltip(new Tooltip(m.getValue() + " : " + GRAPH1_FORMAT.format(new Date(m.getKey().getTime()))));
                                data.setNode(l);

                            } catch (Throwable th) {
                                log.error("Graph QEDT error", th);
                            }
                        }
                    }

                    if (first != null) {
                        try {

                            XYChart.Data data = new XYChart.Data(first.getKey().getTime(), first.getValue());
                            series.getData().add(data);

                            Label l = new Label();
                            l.setPrefSize(10, 10);
                            l.setTooltip(new Tooltip(first.getValue() + " : " + GRAPH1_FORMAT.format(new Date(first.getKey().getTime()))));
                            data.setNode(l);

                        } catch (Throwable th) {
                            log.error("Graph QEDT error", th);
                        }
                    }

                    //log.debug("QEDT series size = " + series.getData().size());
                    lineChart.getData().add(series);

                    box.setCenter(lineChart);
                    
                    Stage stage = new Stage(StageStyle.UTILITY);
                    stage.initOwner(parent);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setTitle("Query Execution Date Interval");

                    Scene scene = new Scene(box, getScene().getWindow().getWidth(), getScene().getWindow().getHeight() - 50);
                    Label l = new Label("Query Execution Date Interval");
                    l.setStyle("-fx-font-size: 14; -fx-font-weight: bold; -fx-text-fill: white;");
                    l.setAlignment(Pos.CENTER);
                    l.setMaxWidth(Double.MAX_VALUE);
                    BorderPane.setMargin(l, new Insets(5, 0, 5, 0));
                    box.setTop(l);
                    
                    scene.getStylesheets().addAll(getScene().getStylesheets());
                    stage.setScene(scene);
                    stage.setX(getScene().getWindow().getX());
                    stage.setY(getScene().getWindow().getY() + 25);
                    stage.show();
                }
            });
            setGraphic(b);
        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            setText("");
        }
    }
}
