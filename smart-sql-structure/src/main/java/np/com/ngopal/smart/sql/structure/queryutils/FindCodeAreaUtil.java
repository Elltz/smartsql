/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.structure.queryutils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javax.swing.SwingUtilities;
import np.com.ngopal.smart.sql.structure.models.RowData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class FindCodeAreaUtil
{
    /**
     *  Find next word in CodeArea and select it
     * 
     * @param codeArea QueryAreaWrapper
     * @param findData find data with all need info for search
     */
    public static final void findNext(FindObject codeArea, FindCodeAreaData findData)
    {
        findData.setLastPosition(-1);
        
        String findText = findData.getFindText();
        if (!findText.isEmpty())
        {
            int currentPosition = findData.getCurretPosition();
            
            String textForSearch = "";
            if (findData.getDirection() == FindCodeAreaData.FindDirection.DOWN) {
                textForSearch = codeArea.getText().substring(currentPosition);
            } else {
                textForSearch = codeArea.getText().substring(0, currentPosition);
//                if (textForSearch.toLowerCase().endsWith(findText.toLowerCase())) {
//                    textForSearch = textForSearch.substring(0, textForSearch.length() - findText.length());
//                }
            }
                        
            int findTextLength = findText.length();
            
            findText = Pattern.quote(findText);
            if (findData.isMatchWholeWord())
            {
                findText = "\\b(" + findText + ")\\b";
            }
            else
            {
                findText = "(" + findText + ")";
            }
            
            if (!textForSearch.isEmpty()) {
            
                Pattern p = findData.isMatchCase() ? Pattern.compile(findText) : Pattern.compile(findText, Pattern.CASE_INSENSITIVE);            
                
                if (findData.getDirection() == FindCodeAreaData.FindDirection.DOWN)
                {   
                    Matcher matcher = p.matcher(textForSearch);
                    if (matcher.find())
                    {
                        int index = matcher.start();
                        if (index >= 0)
                        {
                            if (findData.isSelectingRange()) {
                                SwingUtilities.invokeLater(() -> {
                                    codeArea.selectRange(currentPosition + index, currentPosition + index + findTextLength);
                                });
                            }

                            findData.setCurretPosition(currentPosition + index + 1);
                            findData.setLastPosition(currentPosition);
                            findData.setFoundedLength(matcher.end() - index);
                        }
                    }
                }
                else
                {
                    int index = -1;
                    int end = -1;
                    int i = 0;
                    
                    int currentIndex = textForSearch.length() - findData.getFindText().length() * 2;
                    if (currentIndex > 0) {
                        String tempString = textForSearch.substring(currentIndex);

                        while (end == -1 && index == -1) {
                            Matcher matcher = p.matcher(tempString);
                            while(matcher.find(i)) {
                                i++;
                                index = matcher.start();
                                end = matcher.end();
                            }

                            if (end > -1 && index > -1) {
                                end += currentIndex;
                                index += currentIndex;
                                break;
                            }

                            int idx = currentIndex - findData.getFindText().length() * 2;
                            tempString = textForSearch.substring(idx < 0 ? 0 : idx);
                            currentIndex -= findData.getFindText().length() * 2;
                            if (currentIndex < 0) {
                                currentIndex = 0;
                                if (idx < 0) {
                                    break;
                                }
                            }
                        }
                    }
                    
                    

                    if (index >= 0)
                    {
                        if (findData.isSelectingRange()) {
                            
                            int from = index;
                            int to = index + findTextLength;
                            
                            if (findData.isSelectingRange()) {
                                SwingUtilities.invokeLater(() -> {
                                    codeArea.selectRange(to, from); 
                                });
                            }
                                            
                        }

                        findData.setCurretPosition(index + findTextLength - 1);
                        findData.setLastPosition(currentPosition);
                        findData.setFoundedLength(end - index);
                    }
                }
            }
        }
    }
    
    
    public static final void findNext(TableView<RowData> tableView, FindCodeAreaData findData)
    {
        findData.setLastPosition(-1);
        
        String findText = findData.getFindText();
        if (!findText.isEmpty())
        {
            int columns = tableView.getColumns().size();
            int rows = tableView.getItems().size();
            
            if (findData.getDirection() == FindCodeAreaData.FindDirection.DOWN) {
            
                int columnIndex = 1;
                int rowIndex = 0;
            
                TablePosition currentPosition = findData.getTablePosition();
                if (currentPosition != null) {
                    columnIndex = currentPosition.getColumn() + 1;
                    rowIndex = currentPosition.getRow();
                }

                for (int i = rowIndex; i < rows; i++) {
                    for (int j = columnIndex; j < columns; j++) {                        
                        Object value = tableView.getItems().get(i).getData().get(j - 1);
                        if (value != null) {
                            if (find(tableView, value.toString(), findText, findData, i, j)) {
                                return;
                            }
                        }
                    }
                    columnIndex = 1;
                }
            } else {
                
                int columnIndex = columns - 1;
                int rowIndex = rows - 1;
                
                TablePosition currentPosition = findData.getTablePosition();
                if (currentPosition != null) {
                    columnIndex = currentPosition.getColumn() - 1;
                    rowIndex = currentPosition.getRow();
                }

                for (int i = rowIndex; i >= 0; i--) {
                    for (int j = columnIndex; j >= 1; j--) {                        
                        Object value = tableView.getItems().get(i).getData().get(j - 1);
                        if (value != null) {
                            if (find(tableView, value.toString(), findText, findData, i, j)) {
                                return;
                            }
                        }
                    }
                    columnIndex = columns - 1;
                }
            }
        }
    }
    
    private static boolean find(TableView<RowData> tableView, String textForSearch, String findText, FindCodeAreaData findData, int row, int column) {
        
        findText = Pattern.quote(findText);
        if (findData.isMatchWholeWord())
        {
            findText = "\\b(" + findText + ")\\b";
        }
        else
        {
            findText = "(" + findText + ")";
        }

        if (!textForSearch.isEmpty()) {

            Pattern p = findData.isMatchCase() ? Pattern.compile(findText) : Pattern.compile(findText, Pattern.CASE_INSENSITIVE);            

            Matcher matcher = p.matcher(textForSearch);
            if (matcher.find())
            {
                int index = matcher.start();
                if (index >= 0)
                {
                    if (findData.isSelectingRange()) {
                        Platform.runLater(() -> {
                            tableView.getSelectionModel().select(row, tableView.getColumns().get(column));
                            tableView.scrollTo(row);
                        });
                    }

                    findData.setTablePosition(new TablePosition(tableView, row, tableView.getColumns().get(column)));
                    return true;
                }
            }
        }
        
        return false;
    }
}
 