
package np.com.ngopal.smart.sql.structure.utils;

import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyEvent;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

    private final ComboBox<T> comboBox;
    private final StringBuilder sb;
    private final ObservableList<T> data;
    private int caretPos = -1;
    
    private T lastSelectedItem;

    public AutoCompleteComboBoxListener(final ComboBox<T> comboBox) {
        this.comboBox = comboBox;
        sb = new StringBuilder();
        data = comboBox.getItems();

        this.comboBox.setEditable(true);
        this.comboBox.setOnKeyPressed((KeyEvent t) -> {
            comboBox.hide();
        });
        this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
    }
    
    @Override
    public synchronized void handle(KeyEvent event) {
        
        if (event.isShortcutDown()) {
            return;
        }
        
        String text = comboBox.getEditor().getText();
        
        switch (event.getCode()) {
            case UP:
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length(), -1);
                return;

            case DOWN:
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length(), -1);
                return;

            case BACK_SPACE:
                caretPos = comboBox.getEditor().getCaretPosition();
                comboBox.hide();
                
                lastSelectedItem = null;
                comboBox.getSelectionModel().clearSelection();
                comboBox.getEditor().setText(text);
                comboBox.getEditor().positionCaret(caretPos);
                
                return;

            case DELETE:
                
                caretPos = comboBox.getEditor().getCaretPosition();
                comboBox.hide();
                
                lastSelectedItem = null;
                comboBox.getSelectionModel().clearSelection();
                comboBox.getEditor().setText(text);
                comboBox.getEditor().positionCaret(caretPos);
                return;

            case RIGHT:
            case LEFT:
            case HOME:
            case END:
            case TAB:
            case ESCAPE:
            case ENTER:
                
                if (lastSelectedItem != null) {
                    comboBox.getSelectionModel().select(lastSelectedItem);
                    lastSelectedItem = null;
                }
                
                comboBox.hide();
                return;

            case SHIFT:
            case CONTROL:
                return;
        }

        
        lastSelectedItem = null;
        
        String searchText = comboBox.getEditor().getText();
        int searchLength = searchText.length() - comboBox.getEditor().getSelectedText().length();
        
        int lastPosition = comboBox.getEditor().getCaretPosition();
        
        comboBox.getSelectionModel().clearSelection();
        caretPos = -1;

        boolean founded = false;
        for (T item : comboBox.getItems()) {
            String itemValue = comboBox.getConverter().toString(item);
            if (itemValue.toLowerCase().startsWith(searchText.toLowerCase())) {
                
                lastSelectedItem = item;
                        
                comboBox.getSelectionModel().select(item);
                ListView lv = ((ComboBoxListViewSkin) comboBox.getSkin()).getListView();
                if (lv != null) {
                    lv.scrollTo(item);
                }
                searchText = searchText + itemValue.substring(searchText.length());
                founded = true;
                break;
            }
        }

        if (founded) {
            comboBox.getEditor().setText(searchText);
            
            moveCaret(comboBox.getEditor().getText().length(), searchLength);
            
            comboBox.show();
        } else {
            comboBox.getEditor().setText(searchText);
            comboBox.getEditor().positionCaret(lastPosition);
        }
    }

    private void moveCaret(int currentPosition, int startSelectIndex) {
        if (caretPos == -1) {
            System.out.println("move: " + currentPosition + ", " + startSelectIndex);
            comboBox.getEditor().positionCaret(currentPosition);
            comboBox.getEditor().selectRange(currentPosition, startSelectIndex);   
        } else {
            comboBox.getEditor().positionCaret(caretPos);
        }
    }

}
