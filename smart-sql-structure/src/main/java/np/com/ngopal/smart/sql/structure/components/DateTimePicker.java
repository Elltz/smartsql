package np.com.ngopal.smart.sql.structure.components;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.function.Consumer;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Skin;
import javafx.util.StringConverter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DateTimePicker extends DatePicker {

    public static final String DEFAUL_FORMAT                                    = "dd.MM.yyyy HH:mm";
    
    private boolean showButtons = true;
    
    public static LocalTime defaultNotNullLocalTime(final LocalTime time)
    {
        return time != null ? time : LocalTime.of(0, 0, 0);
    }
    
    
    
    
    
    private final ObjectProperty<LocalTime> timeValue = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> dateTimeValue = new SimpleObjectProperty<LocalDateTime>()
    {
        @Override
        public void set(LocalDateTime newValue)
        {
            if (newValue != null)
            {
                // trim seconds and nanos on each set date-time value
                newValue = newValue.withNano(0);
            }

            super.set(newValue);
        }
    };
    

    private final ChangeListener<? super LocalDateTime> dateTimeListener;
    private final InvalidationListener invalidationListener;
    
    private Consumer<LocalDateTime> onAccept;
    private Consumer<LocalDateTime> onCancel;
    
    private final SimpleBooleanProperty showSeconds = new SimpleBooleanProperty(false);
    
    
    public DateTimePicker()
    {
        this((LocalDateTime)null);
    }
    
    
    public DateTimePicker(final Date dateTime)
    {
        this(dateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }
    
    
    public DateTimePicker(final LocalDateTime dateTime)
    {
        getStyleClass().add("GFA-DateTimePicker");
        
        dateTimeListener = (final ObservableValue<? extends LocalDateTime> observable, final LocalDateTime oldValue, final LocalDateTime newValue) ->
        {
            // update date and time value separatelly
            updateDateTime(newValue);
        };
        
        //
        // Add listener on date-time changing
        //
        dateTimeValueProperty().addListener(dateTimeListener);
        
        
        // set current (initial) date-time value
        setDateTimeValue(dateTime);
        
        // and update initial date and time value separatelly
        // (we should do this first time)
        updateDateTime(dateTime);
        
        invalidationListener = (final Observable observable) ->
        {
            // if date value changed (not time) - then put date-time value
            if (getDateValue() != null)
            {
                dateTimeValueProperty().set(LocalDateTime.of(getDateValue(), defaultNotNullLocalTime(getTimeValue())));
            }
        };
        
        
        //
        // Add listener on date changing
        //
        dateValueProperty().addListener(invalidationListener);
    }

    public boolean isShowButtons() {
        return showButtons;
    }

    public void setShowButtons(boolean showButtons) {
        this.showButtons = showButtons;
    }
    
    
    
    public void setOnAccept(Consumer<LocalDateTime> onAccept) {
        this.onAccept = onAccept;
    }
    
    public void setOnCancel(Consumer<LocalDateTime> onCancel) {
        this.onCancel = onCancel;
    }
    
    
    public void accept() {
        if (onAccept != null) {
            onAccept.accept(getDateTimeValue());
        } else {
            hide();
        }
    }
    
    public void cancel() {
        if (onCancel != null) {
            onCancel.accept(getDateTimeValue());
        } else {
            hide();
        }
    }
    
    public void today() {
        setDateTimeValue(LocalDateTime.now());
        accept();
    }
    
    
    @Override
    protected void clearDateValue()
    {
        super.clearDateValue();
        
        setDateTimeValue(null);
    }
    
    
    
    
    @Override
    protected StringConverter<LocalDate> createConverter()
    {
        return new DateTimeStringConverter();
    }
    
    
    
    
    
    
    private void updateDateTime(final LocalDateTime dateTime)
    {
        if (dateTime != null)
        {
            setTimeValue(dateTime.toLocalTime());
            setDateValue(dateTime.toLocalDate());            
        }
//        else
//        {
//            // set current date and current time value if
//            // date-time value is NULL
//            setDateValue(LocalDate.now(ZoneId.systemDefault()));
//            setTimeValue(LocalTime.now(ZoneId.systemDefault()));
//        }
        
        if (getSkin() != null)
        {
            // and forse update textfield value
            ((DateTimePickerSkin)getSkin()).updateValueDisplay();
        }
    }
    

    
    
    @Override
    protected Skin<?> createDefaultSkin()
    {
        // return own custom skin
        return new DateTimePickerSkin(this);
    }

    
    
    
    
    
    
    
    
    
    public final LocalDate getDateValue()
    {
        return super.getValue();
    }
    public final void setDateValue(final LocalDate dateValue)
    {
        super.setValue(dateValue);
    }

    public final ObjectProperty<LocalDate> dateValueProperty()
    {
        return super.valueProperty();
    }
    
    
    
    
    
    public final LocalTime getTimeValue()
    {
        return timeValue.get();
    }
    public final void setTimeValue(final LocalTime timeValue)
    {
        this.timeValue.set(timeValue);
    }

    public final ObjectProperty<LocalTime> timeValueProperty()
    {
        return timeValue;
    }


    
    
    public final LocalDateTime getDateTimeValue()
    {
        return dateTimeValueProperty().get();
    }
    public final void setDateTimeValue(final LocalDateTime dateTimeValue)
    {
        dateTimeValueProperty().set(dateTimeValue);
    }

    public final ObjectProperty<LocalDateTime> dateTimeValueProperty()
    {
        return dateTimeValue;
    }

    
    
    
    
    
    public boolean isShowSeconds()
    {
        return showSeconds.get();
    }
    public void setShowSeconds(boolean showSeconds)
    {
        this.showSeconds.set(showSeconds);
    }
    public BooleanProperty showSecondsProperty()
    {
        return showSeconds;
    }
    
    
    

    

    @Override
    public void destroyComponent()
    {
        dateTimeValueProperty().removeListener(dateTimeListener);
        dateValueProperty().removeListener(invalidationListener);
    }
    
    
    
    
    
    
    private class DateTimeStringConverter extends StringConverter<LocalDate>
    {
        private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAUL_FORMAT);
        
        private final ChangeListener<? super String> formatListener;
        
        private DateTimeStringConverter()
        {
            formatListener = (final ObservableValue<? extends String> observable, final String oldValue, final String newValue) ->
            {
                // recreate formatter instance
                try
                {
                    formatter = DateTimeFormatter.ofPattern(newValue);
                }
                catch (final IllegalArgumentException invalid)
                {
                    // if format string invalid - use default format
                    formatter = DateTimeFormatter.ofPattern(DEFAUL_FORMAT);
                }
            };
                        
            formatProperty().addListener(formatListener);
        }
        
        
        @Override
        public String toString(final LocalDate object)
        {           
            final LocalDateTime dateTime = getDateTimeValue();

            if (dateTime != null)
            {
                return dateTime.format(formatter);
            }
            else
            {
                return "";
            }
        }

        
        @Override
        public LocalDate fromString(final String string)
        {
            LocalDateTime dateTime = null;

            if (string != null && !string.isEmpty())
            {
                dateTime = LocalDateTime.parse(string, formatter);
            }


            if (dateTime != null)
            {
                setTimeValue(dateTime.toLocalTime());
            }
            
            setDateTimeValue(dateTime);


            return getDateValue();
        }
    }
    

}
