package np.com.ngopal.smart.sql.structure.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@AllArgsConstructor
@Getter
public class SyncCompareInfo {

    private final ConnectionParams source;
    private final ConnectionParams target;
    
    @Setter
    private Database sourceDatabase;
    @Setter
    private Database targetDatabase;
    
    private final boolean optionIgnoreAutoincTable;
    private final boolean optionIgnoreAverageRow;
    private final boolean optionIgnoreColumnDefValue;
    private final boolean optionIgnoreDefiner;
    private final boolean optionIgnoreEnds;
    private final boolean optionIgnoreForeignKeys;
    private final boolean optionIgnoreStarts;
    private final boolean optionIgnoreTableEngine;
    private final boolean optionForceColumnOrder;
    
    private final boolean filterTables;
    private final boolean filterEvents;   
    private final boolean filterFunctions; 
    private final boolean filterProcedures;
    private final boolean filterTriggers;
//    private final boolean filterUDFs;
    private final boolean filterViews;
    
}
