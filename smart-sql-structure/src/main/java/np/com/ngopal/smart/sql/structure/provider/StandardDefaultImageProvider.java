/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.provider;

import javafx.scene.image.Image;
import javax.swing.ImageIcon;
import lombok.AccessLevel;
import lombok.Getter;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class StandardDefaultImageProvider extends DefaultImageProvider {

    private static StandardDefaultImageProvider imageProvider;

    public static StandardDefaultImageProvider getInstance() {
        if (imageProvider == null) {
            imageProvider = new StandardDefaultImageProvider();
        }
        return imageProvider;
    }

    @Getter(AccessLevel.PUBLIC)
    private final ImageIcon warningIcon_image = new ImageIcon(getClass().getResource(getImagePathRoute() + "warning.png"));

    @Getter(AccessLevel.PUBLIC)
    private final ImageIcon errorIcon_image = new ImageIcon(getClass().getResource(getImagePathRoute() + "error.png"));

    @Override
    public String getImagePathRoute() {
        return "/np/com/ngopal/smart/sql/structure/ui/images/";
    }

    public Image getSplash_gif(int idx, int width, int height) {
        return getNonDeclaredImage("splash/splash" + idx + ".gif", width, height);
    }
    
    public Image getSplash0_image(int width, int height) {
        return getNonDeclaredImage("splash/splash0.png", width, height);
    }

    public Image getSplash1_image(int width, int height) {
        return getNonDeclaredImage("splash/splash1.png", width, height);
    }
    
    public Image getSplash2_image(int width, int height) {
        return getNonDeclaredImage("splash/splash2.png", width, height);
    }
    
    public Image getSplash3_image(int width, int height) {
        return getNonDeclaredImage("splash/splash3.png", width, height);
    }
    
    public Image getSplash4_image(int width, int height) {
        return getNonDeclaredImage("splash/splash4.png", width, height);
    }
    
    public Image getSplash5_image(int width, int height) {
        return getNonDeclaredImage("splash/splash5.png", width, height);
    }
    
    public Image getInfo_image() {
        return getNonDeclaredImage("info.png");
    }

    public Image getStart_image() {
        return getNonDeclaredImage("start.png");
    }

    public Image getStop_image() {
        return getNonDeclaredImage("stop.png");
    }

    public Image getNew_image() {
        return getNonDeclaredImage("new.png");
    }

    public Image getOpen_image() {
        return getNonDeclaredImage("open.png");
    }

    public Image getMoveUp_image() {
        return getNonDeclaredImage("connection/move_up.png");
    }

    public Image getMoveDown_image() {
        return getNonDeclaredImage("connection/move_down.png");
    }

    public Image getSave_image() {
        return getNonDeclaredImage("connection/save.png");
    }

    public Image getSaveAs_image() {
        return getNonDeclaredImage("connection/save_as.png");
    }

    public Image getXls_export_image() {
        return getNonDeclaredImage("xls.png");
    }

    public Image getXlsSmall_image() {
        return getNonDeclaredImage("xls_small.png");
    }

    public Image getExit_image() {
        return getNonDeclaredImage("connection/exit.png");
    }

    public Image getScroll_backward_image() {
        return getNonDeclaredImage("scroll_backward.png");
    }

    public Image getScroll_forward_image() {
        return getNonDeclaredImage("scroll_forward.png");
    }

    public Image getChart_edit_image() {
        return getNonDeclaredImage("chart_edit.png");
    }

    public Image getClear_profiler_history_image() {
        return getNonDeclaredImage("clear_profiler_history.png");
    }

    public Image getSettings_image() {
        return getNonDeclaredImage("settings.png");
    }

    public Image getZoom_left_image() {
        return getNonDeclaredImage("zoom_left.png");
    }

    public Image getProfiler_showAll_small_image() {
        return getNonDeclaredImage("profiler_showAll_small.png");
    }

    public Image getZoom_right_image() {
        return getNonDeclaredImage("zoom_right.png");
    }

    public Image getShow_time_image() {
        return getNonDeclaredImage("show_time.png");
    }

    public Image getHide_time_image() {
        return getNonDeclaredImage("hide_time.png");
    }

    public Image getClock_image() {
        return getNonDeclaredImage("clock.png");
    }

    public Image getDebugger_image() {
        return getNonDeclaredImage("debug.png");
    }
    
    public Image getDebugTab_image() {
        return getNonDeclaredImage("debug_tab.png");
    }

    public Image getSlow_analyzer_image() {
        return getNonDeclaredImage("slow_analyze.png");
    }
    
    public Image getSlow_analyzer_tab_image() {
        return getNonDeclaredImage("slow_analyze_tab.png");
    }

    public Image getTuner_image() {
        return getNonDeclaredImage("tuner/tuner.png");
    }

    public Image getProfiler_logo_bg_image() {
        return getNonDeclaredImage("profiler_logo_bg.png");
    }

    public Image getProfiler_off_image() {
        return getNonDeclaredImage("profiler_off.png");
    }

    public Image getProfiler_on_image() {
        return getNonDeclaredImage("profiler_on.png");
    }

    public Image getRating_green_image() {
        return getNonDeclaredImage("rating_green.png");
    }

    public Image getRating_blue_image() {
        return getNonDeclaredImage("rating_blue.png");
    }

    public Image getRating_red_image() {
        return getNonDeclaredImage("rating_red.png");
    }

    public Image getRating_yellow_image() {
        return getNonDeclaredImage("rating_yellow.png");
    }
    
    public Image getRating_0_image() {
        return getNonDeclaredImage("rating_0.png");
    }
    
    public Image getRating_1_image() {
        return getNonDeclaredImage("rating_1.png");
    }
    
    public Image getRating_2_image() {
        return getNonDeclaredImage("rating_2.png");
    }
    
    public Image getRating_3_image() {
        return getNonDeclaredImage("rating_3.png");
    }
    
    public Image getRating_4_image() {
        return getNonDeclaredImage("rating_4.png");
    }
    
    public Image getRating_5_image() {
        return getNonDeclaredImage("rating_5.png");
    }

    public Image getProfilerTabIcon_image() {
        return getNonDeclaredImage("profiler_icon.png");
    }

    public Image getVisualExplain_image() {
        return getNonDeclaredImage("connection/visualExplain_tab.png");
    }

    public Image getExecute_image() {
        return getNonDeclaredImage("execute.png");
    }

    public Image getRefresh_image() {
        return getNonDeclaredImage("connection/refresh.png");
    }

    public Image getAnalyzer_image() {
        return getNonDeclaredImage("connection/analyze.png");
    }
    
    public Image getAnalyzer_image(int width, int height) {
        return getNonDeclaredImage("connection/analyze.png", width, height);
    }

    public Image getMaximum_image() {
        return getNonDeclaredImage("maxi.png");
    }

    public Image getMinimum_image() {
        return getNonDeclaredImage("mini.png");
    }

    public Image getProfilerZoomIn_image() {
        return getNonDeclaredImage("profiler_zoomIn.png");
    }

    public Image getProfilerZoomIn_small_image() {
        return getNonDeclaredImage("profiler_zoomIn_small.png");
    }

    public Image getProfilerShowAll_image() {
        return getNonDeclaredImage("profiler_showAll.png");
    }

    public Image getProfilerZoomOut_small_image() {
        return getNonDeclaredImage("profiler_zoomOut_small.png");
    }

    public Image getProfilerZoomOut_image() {
        return getNonDeclaredImage("profiler_zoomOut.png");
    }

    public Image getArrowRedo_image() {
        return getNonDeclaredImage("arrow-redo.png");
    }

    public Image getArrowUndo_image() {
        return getNonDeclaredImage("arrow-undo.png");
    }

    public Image getDialogError_image() {
        return getNonDeclaredImage("dialog-error.png");
    }

    public Image getQueryAnalyzeTab_image() {
        return getNonDeclaredImage("connection/analyze_tab.png");
    }

    public Image getSimpleClose_image() {
        return getNonDeclaredImage("simple-close.png");
    }

    public Image getWarning_image() {
        return getNonDeclaredImage("warning.png");
    }

    public Image getError_image() {
        return getNonDeclaredImage("error.png");
    }

    public Image getTableTab_image() {
        return getNonDeclaredImage("table_tab.png");
    }
    
    public Image getExecuteQuery_image() {
        return getNonDeclaredImage("execute_query.png");
    }
    
    public Image getViewTab_image() {
        return getNonDeclaredImage("view_tab.png");
    }

    public Image getProcTab_image() {
        return getNonDeclaredImage("storedprocs_tab.png");
    }

    public Image getFunctionTab_image() {
        return getNonDeclaredImage("function_tab.png");
    }

    public Image getTriggerTab_image() {
        return getNonDeclaredImage("trigger_tab.png");
    }

    public Image getEventTab_image() {
        return getNonDeclaredImage("event_tab.png");
    }

    public Image getOperation_None_image() {
        return getNonDeclaredImage("operation_none.png");
    }

    public Image getOperation_Create_image() {
        return getNonDeclaredImage("operation_create.png");
    }

    public Image getOperation_Update_image() {
        return getNonDeclaredImage("operation_update.png");
    }

    public Image getOperation_Drop_image() {
        return getNonDeclaredImage("operation_drop.png");
    }

    public Image getTuner_good_image() {
        return getNonDeclaredImage("tuner/good.png");
    }

    public Image getTuner_neutral_image() {
        return getNonDeclaredImage("tuner/neutral.png");
    }

    public Image getTuner_bad_image() {
        return getNonDeclaredImage("tuner/bad.png");
    }

    public Image getTunerTab_image(){
        return getNonDeclaredImage("tuner/tuner_tab.png");
    }
    
    public Image getCancelChanges_image(){
        return getNonDeclaredImage("connection/cancel_changes.png");
    }
    
    public Image getCancelButton_image(){
        return getNonDeclaredImage("connection/cancel_button.png");
    }
    
    public Image getCloneButton_image(){
        return getNonDeclaredImage("connection/clone_button.png");
    }
    
    public Image getConnectionButton_image(){
        return getNonDeclaredImage("connection/connect_button.png");
    }
    
    public Image getDeleteButton_image(){
        return getNonDeclaredImage("connection/delete_button.png");
    }
    
    public Image getNewButton_image(){
        return getNonDeclaredImage("connection/new_button.png");
    }
    
    public Image getSaveButton_image(){
        return getNonDeclaredImage("connection/save_button.png");
    }
    
    public Image getTestConnectionButton_image(){
        return getNonDeclaredImage("connection/test_connection_button.png");
    }
    
    public Image getRenameButton_image(){
        return getNonDeclaredImage("connection/rename_button.png");
    }
    
    public Image getPause_image(){
        return getNonDeclaredImage("puase.png");
    }
    
    public Image getGoogle_image(){
        return getNonDeclaredImage("google.png");
    }
    public Image getYahoo_image(){
        return getNonDeclaredImage("yahoo.png");
    }
    public Image getBing_image(){
        return getNonDeclaredImage("bing.png");
    }
    public Image getStackOverflow_image(){
        return getNonDeclaredImage("stackoverflow.png");
    }
    public Image getMessageInformation_image(){
        return getNonDeclaredImage("message_information.png");
    }
    
     public Image getGraph_0_image() {
        return getNonDeclaredImage("graph0.png", 40, 12);
    }
    
    public Image getGraph_1_image() {
        return getNonDeclaredImage("graph1.png", 40, 12);
    }
    
    public Image getGraph_2_image() {
        return getNonDeclaredImage("graph2.png", 40, 12);
    }
    
    public Image getGraph_3_image() {
        return getNonDeclaredImage("graph3.png", 40, 12);
    }
    
    public Image getGraph_4_image() {
        return getNonDeclaredImage("graph4.png", 40, 12);
    }
    
    public Image getBar_0_image() {
        return getNonDeclaredImage("bar0.png", 40, 12);
    }
    
    public Image getBar_1_image() {
        return getNonDeclaredImage("bar1.png", 40, 12);
    }
    
    public Image getBar_2_image() {
        return getNonDeclaredImage("bar2.png", 40, 12);
    }
    
    public Image getBar_3_image() {
        return getNonDeclaredImage("bar3.png", 40, 12);
    }
    
    public Image getBar_4_image() {
        return getNonDeclaredImage("bar4.png", 40, 12);
    }
    
    public Image getBarChart_image() {
        return getNonDeclaredImage("barchart.png");
    }
    
    public Image getDialogMoreDetails_image(){
        return getNonDeclaredImage("dialog-more-details.png", 21, 21);
    }
    public Image getDialogLessDetails_image(){
        return getNonDeclaredImage("dialog-fewer-details.png", 21, 21);
    }
    public Image getOk_small_image (){
        return getNonDeclaredImage("ok_small.png", 16, 16);
    }
    
    public Image getReportDataSize_image (){
        return getNonDeclaredImage("reports/report_data_size_tab.png", 16, 16);
    }
    
    public Image getReportIndexDuplicateTab_image (){
        return getNonDeclaredImage("reports/report_dublicate_index_tab.png", 16, 16);
    }
    
    public Image getReportIndexUnusedTab_image (){
        return getNonDeclaredImage("reports/report_index_unused_tab.png", 16, 16);
    }
    
    public Image getPieChart_image (){
        return getNonDeclaredImage("piechart.png");
    }
    
    public Image getGridView_image(){
        return getNonDeclaredImage("grid_view.png");
    }
    
    public Image getUpdatesInstalled_image() {
        return getNonDeclaredImage("updates_installed.png");
    }
    
    public Image getOSUbuntu_image() {
        return getNonDeclaredImage("os_ubuntu.png");
    }
    
    public Image getOSRedhat_image() {
        return getNonDeclaredImage("os_redhat.png");
    }
    
    public Image getOSSuse_image() {
        return getNonDeclaredImage("os_suse.png");
    }
    
    public Image getOSDebian_image() {
        return getNonDeclaredImage("os_debian.png");
    }
    
    public Image getOSWindows_image() {
        return getNonDeclaredImage("os_windows.png");
    }
    
    public Image getOSMac_image() {
        return getNonDeclaredImage("os_mac.png");
    }
    
    public Image getLoader_image() {
        return getNonDeclaredImage("loader.png");
    }
    
    public Image get404NotFound_image() {
        return getNonDeclaredImage("404notfound.png");
    }
}
