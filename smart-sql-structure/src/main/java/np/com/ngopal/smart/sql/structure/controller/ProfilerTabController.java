package np.com.ngopal.smart.sql.structure.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebErrorEvent;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ProfilerTabController extends BaseController implements Initializable{
            
    @FXML
    private AnchorPane mainUI;       
   
    @FXML
    private WebView webView;

    @Override
    public void requestFocus() {
        webView.requestFocus();
    }
    

    private static final List<String> AVAILABLE_PROFILE_VARIABLES = new ArrayList(Arrays.asList(new String[] {"freeing items", "Sending data", "starting", "init", "statistics", "Opening tables", "cleaning up"}));
    private static final Map<String, String> AVAILABLE_STATUS_VARIABLES = new HashMap<>();
  
    private static final List<String> GLOBAL_VARIABLES = new ArrayList(Arrays.asList(new String[] {"Innodb_buffer_pool_read_requests", "Innodb_rows_read"}));
    private static final List<String> ACTUAL_VARIABLES = new ArrayList(Arrays.asList(new String[] {"Last_query_cost", "Open_tables"}));
    
    private static final Collator STRING_COLLATOR = Collator.getInstance();
    
    @Getter
    private boolean fullInited = false;
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    public void doFullInit(DBService dbService, String query, boolean showProfile) throws SQLException {
        fullInited = true;
        
        if (showProfile) {
            
            String profileQuery = "SET PROFILING = 1;";
            profileQuery += "SHOW PROFILES;";
            
            Long queryId = null;
            Statement statement = dbService.getDB().createStatement();
            try (ResultSet rs0 = (ResultSet) statement.executeQuery(profileQuery)) {
                
                ResultSet rs = null;
                while (statement.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                    if (statement.getResultSet() != null) {
                        rs = statement.getResultSet();
                    }
                }
                
                if (rs != null) {
                    
                    while (rs.next()) {
                        query = query.trim();
                        if (query.endsWith(";")) {
                            query = query.substring(0, query.length() - 1);
                        }
                        
                        String[] strs = rs.getString("Query").trim().split(";");
                        for (String s: strs) {

                            if (query.startsWith(s)) {
                                queryId = rs.getLong("Query_ID");
                                break;
                            }
                        }
                        
                        if (queryId != null) {
                            break;
                        }
                    }
                }
                
                rs.close();
            }
        
        
            Map<String, Double> result = new HashMap<>();
            profileQuery = "SELECT state, ROUND(SUM(duration),5) AS `duration (summed) in sec` FROM information_schema.profiling WHERE query_id = " + queryId + " GROUP BY state ORDER BY `duration (summed) in sec` DESC;";
            profileQuery += "SET PROFILING = 0;";
            try (ResultSet rs = (ResultSet) dbService.getDB().createStatement().executeQuery(profileQuery)) {

                if (rs != null) {
                    while (rs.next()) {
                        String key = rs.getString(1);
                        Double value = rs.getDouble(2);

                        Double oldValue = result.get(key);
                        if (oldValue != null) {
                            value += oldValue;
                        }

                        result.put(key, value);                    
                    }
                }
            }     
            
            updateProfile(webView.getEngine().getDocument(), result);
        }
    }
        
    
    public void init(Map<String, Object> showStatusBefore, Map<String, Object> showStatusAfter, List<ExplainResult> explainResult, Map<String, String> extendedResult) {
        Platform.runLater(() -> {
            webView.getEngine().load(StandardResourceProvider.getInstance().getProfileMarkUpURL().toExternalForm());
            webView.getEngine().setOnError((WebErrorEvent event) -> {
                log.error(event.getMessage(), event.getException());
            });
        });        
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateData(newValue, showStatusBefore, showStatusAfter, explainResult, extendedResult);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateData(doc, showStatusBefore, showStatusAfter, explainResult, extendedResult);
            }
        });
    }
            
    private void saveProfilerToFile() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("HTML Files", "*.htm,*.html"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File userFile = fc.showSaveDialog(getController().getStage());
        if (userFile != null) {
            try {
                
                Document doc = webView.getEngine().getDocument();
                try (FileOutputStream fo = new FileOutputStream(userFile)) {
                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                    transformer.transform(new DOMSource(doc), new StreamResult(new OutputStreamWriter(fo, "UTF-8")));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }                   

            } catch (Throwable ex) {
                DialogHelper.showError(getController(), "Error saveing script", ex.getMessage(), getController().getSelectedConnectionSession().get().getConnectionParam(), ex);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {      
        webView.setContextMenuEnabled(false);
        
        ContextMenu contextMenu = new ContextMenu();
        MenuItem save = new MenuItem("Save Profiler To File...");
        save.setOnAction(e -> saveProfilerToFile());
        contextMenu.getItems().addAll(save);

        webView.setOnMousePressed(e -> {
            if (e.getButton() == MouseButton.SECONDARY) {
                contextMenu.show(webView, e.getScreenX(), e.getScreenY());
            } else {
                contextMenu.hide();
            }
        });
        
        AVAILABLE_STATUS_VARIABLES.put("Bytes_received", "Bytes sent from the client to the server");
        AVAILABLE_STATUS_VARIABLES.put("Bytes_sent", "Bytes sent from the server to the client");
        AVAILABLE_STATUS_VARIABLES.put("Com_select", "Number of SELECT statements that have been executed");
        AVAILABLE_STATUS_VARIABLES.put("Handler_commit", "Number of internal commit statements");
        AVAILABLE_STATUS_VARIABLES.put("Handler_read_first", "Number of times the first entry was read from an index. A high value indicates that full index scans were done");
        AVAILABLE_STATUS_VARIABLES.put("Handler_read_key", "Number of requests to read a row based on a key");
        AVAILABLE_STATUS_VARIABLES.put("Handler_read_rnd_next", "Number of requests to read the next row in the data file. A high value indicates that a full table scan was required");
        AVAILABLE_STATUS_VARIABLES.put("Innodb_buffer_pool_read_requests", "The number of logical read requests Innodb has done");
        AVAILABLE_STATUS_VARIABLES.put("Innodb_rows_read", "The number of rows read from Innodb tables");
        AVAILABLE_STATUS_VARIABLES.put("Last_query_cost", "The total cost of this query as computed by the query optimizer");
        AVAILABLE_STATUS_VARIABLES.put("Open_tables", "The number of tables that are open");
        AVAILABLE_STATUS_VARIABLES.put("Opened_tables", "Number of new tables that have been opened. A high value indicates an insufficient table cache");
        AVAILABLE_STATUS_VARIABLES.put("Questions", "Number of statements executed by the server");
        AVAILABLE_STATUS_VARIABLES.put("Select_scan", "Number of full table scans of the first table in the query");
    }
      
    private void updateData(Document doc, Map<String, Object> showStatusBefore, Map<String, Object> showStatusAfter, List<ExplainResult> explainResult, Map<String, String> extendedResult) {
        updateStatus(doc, showStatusBefore, showStatusAfter);
        updateExplain(doc, explainResult);
        updateExtended(doc, extendedResult);
    }
    
    private void updateProfile(Document doc, Map<String, Double> profileData) {
        
        if (profileData != null) {
            double total = 0;
            List<ProfileData> data = new ArrayList<>();
            for (String key: profileData.keySet()) {            
                if (AVAILABLE_PROFILE_VARIABLES.contains(key)) {                
                    ProfileData pd = new ProfileData();
                    pd.state = key;
                    pd.duration = new BigDecimal(profileData.get(key));

                    total += profileData.get(key);

                    data.add(pd);
                }
            }

            data.sort((ProfileData o1, ProfileData o2) -> o2.duration.compareTo(o1.duration));

            Element table = doc.getElementById("showProfile");
            boolean odd = false;
            for (ProfileData pd: data) {
                pd.percent = new BigDecimal(pd.duration.doubleValue() * 100 / total);
                addRow(pd.asRow(), doc, table, odd);
                odd = !odd;
            }

            addRow(new String[] {"Total", new BigDecimal(total).setScale(5, RoundingMode.HALF_UP).toString(), "100.00000"}, doc, table, odd);
        } else {
            Element el = doc.getElementById("profileBlock");
            el.setAttribute("style", "display: none");
        }
    }
    
    private void updateStatus(Document doc, Map<String, Object> showStatusBefore, Map<String, Object> showStatusAfter) {
   
        if (showStatusAfter != null && showStatusBefore != null) {
            List<StatusData> data = new ArrayList<>();
            for (String key: showStatusAfter.keySet()) {            

                if (AVAILABLE_STATUS_VARIABLES.containsKey(key)) {                
                    StatusData sd = new StatusData();

                    sd.variable = key;
                    if (GLOBAL_VARIABLES.contains(key)) {
                        sd.variable += "#";
                    }

                    if (ACTUAL_VARIABLES.contains(key)) {
                        sd.variable += "*";
                        sd.value = (String) showStatusAfter.get(key);
                    } else {
                        BigDecimal v0 = new BigDecimal((String)showStatusBefore.get(key));
                        BigDecimal v1 = new BigDecimal((String)showStatusAfter.get(key));

                        sd.value = v1.subtract(v0).toString();
                    }

                    sd.description = AVAILABLE_STATUS_VARIABLES.get(key);

                    data.add(sd);
                }
            }

            data.sort((StatusData o1, StatusData o2) -> STRING_COLLATOR.compare(o1.variable, o2.variable));

            Element table = doc.getElementById("showStatus");
            boolean odd = false;
            for (StatusData pd: data) {
                addRow(pd.asRow(), doc, table, odd);
                odd = !odd;
            }
        } else {
            Element el = doc.getElementById("statusBlock");
            el.setAttribute("style", "display: none");
        }
    }
    
    
    private void updateExplain(Document doc, List<ExplainResult> explainResult) {
           
        if (explainResult != null) {
            Element table = doc.getElementById("showExplain");
            boolean odd = false;
            for (ExplainResult er: explainResult) {
                addRow(er.asRow(), doc, table, odd);
                odd = !odd;
            }
        } else {
            Element el = doc.getElementById("explainBlock");
            el.setAttribute("style", "display: none");
        }
    }
    
    private void updateExtended(Document doc, Map<String, String> extendedResult) {
           
        if (extendedResult != null) {
            Element levelSpan = doc.getElementById("levelSpan");
            levelSpan.setTextContent(extendedResult.get("level"));
            
            Element codeSpan = doc.getElementById("codeSpan");
            codeSpan.setTextContent(extendedResult.get("code"));
            
            Element table = doc.getElementById("extendedSpan");
            addRow(new String[] {extendedResult.get("query")}, doc, table, false);
        } else {
            Element el = doc.getElementById("extendedBlock");
            el.setAttribute("style", "display: none");
        }
    }
    
    private void addRow(String[] row, Document doc, Element table, boolean odd) {
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: " + (odd ? "#e5e5e5;" : "white;"));
        
        for (int i = 0; i < row.length; i++) {
            Element cell = doc.createElement("td");
            
            String value = row[i];
            if (value != null && value.startsWith("img::")) {
                
                Element img = doc.createElement("img");
                img.setAttribute("src", value.substring(5));
                cell.appendChild(img);
            } else {
                cell.appendChild(doc.createTextNode(value != null ? value : "(NULL)"));
            }
            newRow.appendChild(cell);
        }        
        
        table.appendChild(newRow);
    }
    
    private class ProfileData {
        String state;
        BigDecimal duration;
        BigDecimal percent;
        
        String[] asRow() {
            return new String[] {state, duration.setScale(5, RoundingMode.HALF_UP).toString(), percent.setScale(5, RoundingMode.HALF_UP).toString()};
        }
    }
    
    private class StatusData {
        String variable;
        String value;
        String description;
        
        String[] asRow() {
            return new String[] {variable, value, description};
        }
    }
    
}
