package np.com.ngopal.smart.sql.structure.models;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Setter
@Getter
public class FilterData {
    
    private String column;
    private String condition;
    private String value;
}
