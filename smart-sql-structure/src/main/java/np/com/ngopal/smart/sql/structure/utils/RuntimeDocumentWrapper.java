/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import com.google.gson.Gson;
import java.util.Map;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class RuntimeDocumentWrapper {
    
    private transient Gson gson;
    private transient String documentWrapperString;
    private boolean forceConvert;
    private byte builttype;
    
    private String newUpdateNotice, version;
    private String bannerText;
    private String youtubeText;
    private String onlineServer;
    private long appSize;
    private String appHash;
    private Map<String,String> dependencyHashes;
    private Map<String,String> dependencyDirectory;
    
    
    public RuntimeDocumentWrapper(Gson gson) { this.gson = gson; }
    
    public static RuntimeDocumentWrapper getRuntimeDocumentWrapperInstance(Gson gson , String documentWrap){
        return gson.fromJson(documentWrap, RuntimeDocumentWrapper.class);
    }
    
    @Override
    public String toString() {
        if(forceConvert || documentWrapperString == null || documentWrapperString.isEmpty()){
            documentWrapperString = gson.toJson(RuntimeDocumentWrapper.this, RuntimeDocumentWrapper.class);
        }
        return documentWrapperString;
    }

    public String getNewUpdateNotice() {
        return newUpdateNotice;
    }

    public long getAppSize() {
        return appSize;
    }

    public String getAppHash() {
        return appHash;
    }

    public Map<String, String> getDependencyHashes() {
        return dependencyHashes;
    }

    public String getVersion() {
        return version;
    }

    public Map<String, String> getDependencyDirectory() {
        return dependencyDirectory;
    }
    
    public byte getBuilttype() {
        return builttype;
    }

    public String getBannerText() {
        return bannerText;
    }
    
    public String getYoutubeText() {
        return youtubeText;
    }
    
    public String getOnlineServer() {
        return onlineServer;
    }
}
