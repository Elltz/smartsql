package np.com.ngopal.smart.sql.structure.controller;

import np.com.ngopal.smart.sql.model.utils.SqlUtils;
import java.io.StringReader;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebErrorEvent;
import javafx.scene.web.WebView;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.QOIndexRecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.queryutils.RSyntaxTextAreaBuilder;
import tools.java.pats.models.SqlFormatter;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class QueryAnalyzerTabController extends BaseController implements Initializable {

    private RunnedFrom runnedFrom;
    
    @Setter(AccessLevel.PUBLIC)
    private DBService service;
        
    @FXML 
    private Button analyzeButton;
    
    @FXML
    private Button visualExplainButton;
    
    @FXML
    private Button formatQueryButton;
    
    @FXML
    private Button clearButton;
    
    @FXML
    private Button copySqlScriptButton;
    
    @FXML
    private Button copyAlterSqlScriptButton;
    
    @FXML
    private Button createIndexButton;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private BorderPane queryAreaPane;
    
    private RSyntaxTextArea queryArea;
    
    @FXML
    private CheckBox queryCheckBox;
    
    @FXML
    private CheckBox batchFileCheckBox;
    
    @FXML
    private CheckBox slowQueryLogCheckBox;
    
    @FXML
    private CheckBox generalLogCheckBox;
            
    @FXML
    private RadioButton onlineRadioButton;
            
    @FXML
    private CheckBox limitTuningTimeCheckBox;
    @FXML
    private HBox limitTuningBox;
    @FXML 
    private DatePicker limitTuningDatePicker;
    @FXML
    private TextField limitTuningTimeField;
            
    @FXML
    private CheckBox indexTuningCheckBox;
    @FXML
    private CheckBox avoidTemTablesCheckBox;
    @FXML
    private CheckBox avoidFileSortingCheckBox;
    @FXML
    private CheckBox avoidJoinBufferCheckBox;
    @FXML
    private CheckBox dataTypesTuningCheckBox;
    @FXML
    private CheckBox joinConditionTuningCheckBox;       
    
    @FXML
    private CheckBox useCacheCheckbox;
            
    @FXML
    private RadioButton noPartitionRadioButton;
    
    @FXML
    private TabPane tabPane;
    
    @FXML
    private VBox webViewContainer;
    
    private WebView webView;
    
    @FXML
    private TextField indexMaxNoColumnsField;
    
    @FXML
    private TextField indexSubstrLengthField;
    
    private ChangeListener<Worker.State> webViewStateListener;
    //private TableView<AnalyzerResult.RecommendationResult> analyzerResultTable;
    
    private final EventListener linkListener = (org.w3c.dom.events.Event ev) -> {
        URLUtils.openUrl("https://dev.mysql.com/doc/refman/5.7/en/mysql-indexes.html");
    };
    
    private final List<QORecommendation> results = new ArrayList<>();
    
    private final List<Tab> visualExplainTabs = new ArrayList<>();
    
    @Setter(AccessLevel.PUBLIC)
    private PreferenceDataService preferenceService;
    
    @Setter(AccessLevel.PUBLIC)
    private QARecommendationService qars;
    
    @Setter(AccessLevel.PUBLIC)
    private ColumnCardinalityService columnCardinalityService;
    
    @Setter(AccessLevel.PUBLIC)
    private QAPerformanceTuningService performanceTuningService;
    
    @Setter(AccessLevel.PUBLIC)
    private UserUsageStatisticService usageService;
    
    private Database currentDatabase;
    
    @FXML 
    public void formatQuery(ActionEvent event) {
        String selectedText = queryArea.getText();
        if (selectedText != null && !selectedText.isEmpty()) {

            String formatedText;
            SqlFormatter formatter = new SqlFormatter();

            List<PreferenceData> list = preferenceService.getAll();
            if (!list.isEmpty()) {
                formatedText = formatter.formatSql(selectedText, list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle());
            } else {
                formatedText = formatter.formatSql(selectedText, "", "2", "block");
            }
            
            SwingUtilities.invokeLater(() -> {
                queryArea.setText(formatedText);
            });
        }
    }
    
    @FXML 
    public void visualExplainQueryAction(ActionEvent event) {
        for (Tab t: visualExplainTabs) {
            tabPane.getTabs().remove(t);
        }
        
        visualExplainTabs.clear();
        
        try {
            ScriptRunner sr = new ScriptRunner(service.getDB(), true);
            Map<Integer, List<String>> result = sr.findQueries(new StringReader(queryArea.getText()));
            if (result != null) {
                
                boolean needBreak = false;
                for (List<String> queries: result.values()) {
                    if (queries != null) {
                        for (String query: queries) {
                            
                            Tab t = FunctionHelper.showVisualExplain(currentDatabase != null ? currentDatabase : getController().getSelectedConnectionSession().get().getConnectionParam().getSelectedDatabase(), 
                                    query, getController(), columnCardinalityService,preferenceService, performanceTuningService, qars, usageService, tabPane);
                            if (t != null) {
                                visualExplainTabs.add(t);
                                tabPane.getTabs().add(t);
                                tabPane.getSelectionModel().select(t);
                            } else {
                                // if some error just break it
                                needBreak = true;
                                break;
                            }
                        }
                    }
                    
                    if (needBreak) {
                        break;
                    }
                }
            }
        } catch (Throwable ex) {
            log.error("Error", ex);
        }
    }
    
    @FXML 
    public void clearAction(ActionEvent event) {
        queryArea.setText("");
        if (webView != null) {
            webView.setVisible(false);
        }
        
        createIndexButton.setDisable(true);
        copySqlScriptButton.setDisable(true);
        copyAlterSqlScriptButton.setDisable(true);
        
        highlightButton(createIndexButton, true);
        highlightButton(copySqlScriptButton, true);
        highlightButton(copyAlterSqlScriptButton, true);
    }
    
    @FXML 
    public void copySqlScriptAction(ActionEvent event) {
        
        highlightButton(copySqlScriptButton, true);
        
        ClipboardContent content = new ClipboardContent();
        content.putString(queryArea.getText());
        Clipboard.getSystemClipboard().setContent(content);
    }
    
    @FXML 
    public void copyAlterSqlScriptAction(ActionEvent event) {        
        
        highlightButton(copyAlterSqlScriptButton, true);
        
        String query = "";
        for (QORecommendation l: results) {
            for (List<QOIndexRecommendation> idxs: l.getIndexes().values()) {
                for (QOIndexRecommendation idx: idxs) {
                    query += (query.isEmpty() ? "" : "\n") + idx.getIndexQuery();
                }
            }
        }

        ClipboardContent content = new ClipboardContent();
        content.putString(query);
        Clipboard.getSystemClipboard().setContent(content);
    }
    
    @FXML 
    public void createIndexAction(ActionEvent event) {
        
        highlightButton(createIndexButton, true);
        
        CreateIndexAndExamineQueryPerformanceController createController = StandardBaseControllerProvider.getController(getController(), "CreateIndexAndExamineQueryPerformance");
        createController.init(getStage());
        
        if (createController.isAgree()) {
            ExecutingAnalyzerIndexesController executingController = StandardBaseControllerProvider.getController(getController(), "ExecutingAnalyzerIndexes");
        
            ConnectionSession session = getController().getSelectedConnectionSession().get();            
            List<ImprovementResult> list = new ArrayList<>();
            for (QORecommendation l: results) {
                for (String table: l.getIndexes().keySet()) {
                    for (QOIndexRecommendation rr: l.getIndexes().get(table)) {
                        if (rr.getIndexQuery() != null && !rr.getIndexQuery().trim().isEmpty()) {
                            ImprovementResult ir = new ImprovementResult();
                            ir.indexName().set(rr.getIndexName());
                            ir.index().set(rr.getIndexQuery());
                            ir.database().set(l.getDatabase());
                            ir.table().set(table);
                            ir.oldRows().set(l.getRowsMulti());
                            ir.mainQuery().set(l.getSmartSqlQuery() != null ? l.getSmartSqlQuery() : l.getQuery());
                            ir.recomendation().set(rr.getRecommendation());
                            ir.setQAPerformanceTuning(
                                performanceTuningService.findUnique(session.getConnectionParam().getId(), l.getDatabase(), l.getQuery(), rr.getRecommendation())
                            );
                            
                            list.add(ir);
                        }
                    }
                }
            }
            executingController.init(getStage(), list);
        }
    }
    
//    public static void closeVisualExplain(MainController controller, Tab visualExplainTab) {
//        if (visualExplainTab != null) {
//            controller.getTremoveTabFromBottomPane(controller.getSelectedConnectionSession().get(), visualExplainTab);
//        }
//    }
    
    public void setAreaEditable(boolean editable) {
        SwingUtilities.invokeLater(() -> {
           queryArea.setEditable(editable); 
        });        
    }
    
    @FXML
    public void analyzeQueryAction(ActionEvent event) {
                
        makeBusy(true);
        Platform.runLater(() -> {
            if (webView != null) {
                webView.setVisible(false);
            }

            createIndexButton.setDisable(true);
            copySqlScriptButton.setDisable(true);
            copyAlterSqlScriptButton.setDisable(true);

            highlightButton(createIndexButton, true);
            highlightButton(copySqlScriptButton, true);
            highlightButton(copyAlterSqlScriptButton, true);

            results.clear();
        });;
        
        SwingUtilities.invokeLater(() -> {
            String currentQuery = queryArea.getText();


            Thread t = new Thread(() -> {

                usedQA();

    //            try {
    //                Thread.sleep(3000);
    //            } catch (InterruptedException ex) {
    //                Logger.getLogger(QueryAnalyzerTabController.class.getName()).log(Level.SEVERE, null, ex);
    //            }

                boolean hasAnalyzerResults = false;
                boolean emptyWithOr = false;

                try {
                    try {

                        Database database = currentDatabase != null ? currentDatabase : getController().getSelectedConnectionSession().get().getConnectionParam().getSelectedDatabase();

                        // reloading selected database
                        // data maybe was changed - this need for getting latest data
                        if (database != null) {
                            DatabaseCache.getInstance().syncDatabase(database.getName(), getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), (AbstractDBService) service);
                        }

                        ((AbstractDBService)service).setServiceBusy(true);

                        boolean stopedAnalyze = false;

                        String queryList = "";

                        ScriptRunner sr = new ScriptRunner(service.getDB(), true);
                        Map<Integer, List<String>> queries = sr.findQueries(new StringReader(currentQuery));

                        if (queries != null) {
                            if (database != null) {                
                                for (List<String> list: queries.values()) {

                                    if (list != null) {
                                        boolean showExpired = true;
                                        for (String query: list) {

                                            query = QueryFormat.removeComments(query);
                                            if (!query.trim().isEmpty() && !";".equals(query.trim())) {

                                                if (!queryList.isEmpty()) {
                                                    queryList += "\n";
                                                }

                                                queryList += SqlUtils.makeQueryOneLine(query);

                                                log.info("QA start");

                                                int indexesMaxCount = 4;
                                                try {
                                                    indexesMaxCount = Integer.valueOf(indexMaxNoColumnsField.getText().trim());
                                                } catch (Throwable th) {
                                                }

                                                int indexSubstrLength = 16;
                                                try {
                                                    indexSubstrLength = Integer.valueOf(indexSubstrLengthField.getText().trim());
                                                } catch (Throwable th) {
                                                }

                                                QORecommendation qaRecommendations = QACache.getQARecommandation(getController(), qars, columnCardinalityService, performanceTuningService, database, query, !useCacheCheckbox.isSelected(), indexesMaxCount, indexSubstrLength, false, false, false, showExpired);
                                                showExpired = qaRecommendations.isHasCredits();

                                                results.add(qaRecommendations);
                                                hasAnalyzerResults = qaRecommendations.isHasAnalyzerResults();
                                                emptyWithOr = qaRecommendations.isEmptyWithOr();
                                                log.info("QA finish");
                                            }
                                        }
                                    }
                                }
                            } else {
                                Platform.runLater(() -> {
                                    DialogHelper.showInfo(getController(), "Info", "Please select database first", null, getStage());
                                });
                                return;
                            }
                        }

                        String queryListTemp = queryList;

                        Platform.runLater(() -> {

                            if (webView != null) {
                                if (webViewStateListener != null) {
                                    webView.getEngine().getLoadWorker().stateProperty().removeListener(webViewStateListener);
                                }

                                webViewContainer.getChildren().remove(webView);
                            }

                            webView = new WebView();
                            webView.setContextMenuEnabled(false);
                            webView.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                            webViewContainer.getChildren().add(webView);

                            VBox.setVgrow(webView, Priority.ALWAYS);

                            webViewStateListener = (ObservableValue<? extends Worker.State> ov, Worker.State oldState, Worker.State newState) -> {
                                //log.error("STATE = " + newState);
                                if (newState == Worker.State.SUCCEEDED) {
                                    boolean hasIndexScript = false;

                                    Document doc = webView.getEngine().getDocument();
                                    Element table = doc.getElementById("analyzerTable");

                                    while (table.getFirstChild() != null) {
                                        table.removeChild(table.getFirstChild());
                                    }

                                    int index = 1;

                                    for (QORecommendation ars: results) {

                                        Long rowsMulty = 0l;
                                        if (!ars.getIndexes().isEmpty()) {
                                            hasIndexScript = true;
                                            rowsMulty = ars.getRowsMulti();
                                        }

                                        addRow(index, doc, table, ars.getSmartSqlQuery(), ars.getOptimizedQuery(), ars, queryListTemp, rowsMulty);
                                        index++;

                                    }

                                    if (hasIndexScript) {
                                        Platform.runLater(() -> {
                                            createIndexButton.setDisable(false);
                                            copySqlScriptButton.setDisable(false);
                                            copyAlterSqlScriptButton.setDisable(false);

                                            highlightButton(createIndexButton, false);
                                            highlightButton(copySqlScriptButton, false);
                                            highlightButton(copyAlterSqlScriptButton, false);
                                        });
                                    } else {
                                        Platform.runLater(() -> {
                                            createIndexButton.setDisable(true);
                                            copySqlScriptButton.setDisable(true);
                                            copyAlterSqlScriptButton.setDisable(true);

                                            highlightButton(createIndexButton, true);
                                            highlightButton(copySqlScriptButton, true);
                                            highlightButton(copyAlterSqlScriptButton, true);
                                        });
                                    }

                                    webView.setVisible(true);
                                }
                            };

                            webView.getEngine().getLoadWorker().stateProperty().addListener(webViewStateListener);
                            webView.getEngine().load(StandardResourceProvider.getInstance().getAnalyzerResultMarkUpURL().toExternalForm());   
                            webView.getEngine().onErrorProperty().addListener((ObservableValue<? extends EventHandler<WebErrorEvent>> observable, EventHandler<WebErrorEvent> oldValue, EventHandler<WebErrorEvent> newValue) -> {
                                if (newValue != null) {
                                    System.out.println("1111");
                                }
                            });

                        });

                        if (!hasAnalyzerResults && !stopedAnalyze) {
                            Platform.runLater(() -> {
                                DialogHelper.showInfo(getController(), "Info",
                                        "We could not identify any improvements.\nPlease contact TopDBs DB experts support to tune this query.\nTopDBs DB expert can improve performance with 100% guarantee. Should they fail to improve it, then the issue will be returned back to you. Please contact TopDBs DB \nexpert support www.topdbs.in/consultancyService"
                                        , null, getStage());
                            });
                        }
                    } finally {
                        ((AbstractDBService)service).setServiceBusy(false);
                    }
                } catch (Throwable th) {
                    Platform.runLater(() -> {
                        if (th instanceof SQLException) {
                            DialogHelper.showError(getController(), "Error on analyzing query", th.getMessage(), null);
                        } else {
                            // What do if not SQL exception?
                            log.error("Error on analyzing query", th);
                        }
                    });
                } finally {
                    makeBusy(false);
                }
            });
            t.setDaemon(true);        
            t.start();
        });
    }
    
    private void usedQA() {
        switch (runnedFrom) {
            case QA:
                usageService.savePUsage(PUsageFeatures.QA);
                break;
            
            case SQAQA:
                usageService.savePUsage(PUsageFeatures.SQAQA);
                break;
                
            case DQA:
                usageService.savePUsage(PUsageFeatures.DQA);
                break;
                
            case PQA:
                usageService.savePUsage(PUsageFeatures.PQA);
                break;
        }
    }
    
    private void highlightButton(Button b, boolean disabled) {
        if (disabled) {
            b.getStyleClass().remove("highlightButton");
        } else {
            if (!b.getStyleClass().contains("highlightButton")) {
                b.getStyleClass().add("highlightButton");
            }
        }
    }
    
    private boolean isEmpty(QORecommendation recommends) {
        boolean empty = true;
        for (List<String> list: recommends.getRecommendations().values()) {
            for (String s: list) {
                empty = s.isEmpty() || AnalyzerResult.NO_IMPROVEMENTS.equals(s) || AnalyzerResult.EMPTY_OR.equals(s);
                if (!empty) {
                    break;
                }
            }
        }
        
        if (empty) {
            if (!recommends.getIndexes().isEmpty()) {
                empty = false;
            }
        }
        
        return empty;
    }
    
//    private boolean isEmptyWithOr(QACache.QARecommendationCache recommends) {
//        return recommends.isEmptyWithOr();
//    }
    
//    private boolean isEmpty(List<AnalyzerResult.RecommendationResult> list) {
//        return list == null || list.isEmpty() || (list.size() == 1 && (AnalyzerResult.NO_IMPROVEMENTS.equals(list.get(0).getRecommendation()) || AnalyzerResult.EMPTY_OR.equals(list.get(0).getRecommendation())));
//    }
    
    private void addRow(int index, Document doc, Element table, String smartSqlQuery, String optimizedQuery, QORecommendation recommends, String queryList, Long rowsMulti) {
        
        // Query No
        {
            Element newRow = doc.createElement("tr");
         
            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #ff9900");
            
            Element el = doc.createElement("span");
            el.setTextContent("Query No: ");
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);
            cell.appendChild(doc.createTextNode(String.valueOf(index)));
            
            newRow.appendChild(cell);
            table.appendChild(newRow);
        }
        
        {
            Element newRow = doc.createElement("tr");

            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #ff00ff");


            Element el = doc.createElement("span");
            el.setTextContent("Examined Query: ");
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);

            // Examined Query
//            for (String query: map.keySet()) {        
//                cell.appendChild(doc.createElement("br"));
//                cell.appendChild(doc.createTextNode(query));
//            }
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode(queryList));
            
            newRow.appendChild(cell);
            table.appendChild(newRow);            
        }
        
        // Query executes on Table/s:
        
        Map<String, String> namesMap = QueryFormat.getTableNamesFromQuery(queryList);
        String tables = "";
        for (String name: namesMap.keySet()) {
            if (!tables.isEmpty()) {
                tables += ", ";
            }

            tables += name;
        }
        {
            Element newRow = doc.createElement("tr");

            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #a64d79");

            Element el = doc.createElement("span");
            el.setTextContent("Query executes on Table/s: ");
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);
            cell.appendChild(doc.createTextNode(tables));

            newRow.appendChild(cell);
            table.appendChild(newRow);
        }
        
        // Recommending Index
        if (!isEmpty(recommends)) {
            Element newRow = doc.createElement("tr");

            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px;");

            Element el = doc.createElement("span");
            el.setTextContent("Recommending Index: ");
            el.setAttribute("style", "font-weight: bold; color: #000000");
            cell.appendChild(el);
            cell.appendChild(doc.createElement("br"));

            for (List<QOIndexRecommendation> list: recommends.getIndexes().values()) {
                
                for (QOIndexRecommendation rr: list) {
                    if (rr.getIndexQuery() != null) {
                        Element el0 = doc.createElement("span");
                        el0.setTextContent(rr.getIndexQuery());
                        el0.setAttribute("style", "font-weight: bold; color: #0000ff");

                        cell.appendChild(el0);
                        cell.appendChild(doc.createElement("br"));
                    }
                }

                newRow.appendChild(cell);
                table.appendChild(newRow);
            }
            
        }
    
        
        // SmartMySQL rewritten query
        if (smartSqlQuery != null && !smartSqlQuery.isEmpty()) {
            Element newRow = doc.createElement("tr");
         
            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px;");
            
            Element el = doc.createElement("span");
            el.setTextContent("SmartMySQL rewritten query: ");
            el.setAttribute("style", "font-weight: bold; color: #000000");
            cell.appendChild(el);
            cell.appendChild(doc.createElement("br"));
            
            Element el0 = doc.createElement("span");
            el0.setTextContent(smartSqlQuery);
            el0.setAttribute("style", "color: #000000");

            cell.appendChild(el0);
            
            newRow.appendChild(cell);
            table.appendChild(newRow);
        }
        
           
        // Recommandations
        {
            Element newRow = doc.createElement("tr");
         
            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #6aa84f");
            
            Element el = doc.createElement("span");
            el.setTextContent("Recommendations: ");
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);
            cell.appendChild(doc.createElement("br"));
            
            boolean hasRecomendation = false;
            int i = 1;
            for (String query: recommends.getRecommendations().keySet()) {
                List<String> list = recommends.getRecommendations().get(query);
                if (list != null && !list.isEmpty()) {
                    for (String rr: list) {
                        
                        String[] result = rr.split("\n");                        
                        cell.appendChild(doc.createTextNode(i + ". " + result[0]));
                        cell.appendChild(doc.createElement("br"));
                        for (int j = 1; j < result.length; j++) {
                            cell.appendChild(doc.createTextNode(result[j]));
                            cell.appendChild(doc.createElement("br"));
                        }
                        i++;
                    } 

                    hasRecomendation = true;
                }
            }
            
//            Element el0 = doc.createElement("span");
//            el0.setTextContent("Next queries: ");
//            el0.setAttribute("style", "font-weight: bold;");
//            
//            cell.appendChild(doc.createElement("br"));
//            cell.appendChild(el0);
//            cell.appendChild(doc.createElement("br"));
            
            if (!hasRecomendation) {
                
//                for (String query: empty) {
//                    cell.appendChild(doc.createTextNode(query));
//                    cell.appendChild(doc.createElement("br"));
//                }
                if (recommends.isEmptyWithOr()) {
                    cell.appendChild(doc.createTextNode("1. The MySQL optimiser could not use any index because the outer logical"));
                    cell.appendChild(doc.createElement("br"));
                    cell.appendChild(doc.createTextNode("operator is \"OR\" and having more than two filter conditions. You may get good performance if you"));
                    cell.appendChild(doc.createElement("br"));
                    cell.appendChild(doc.createTextNode("rewrite two query without \"OR\" and merge result with \"UNION\" then check performance"));
                    cell.appendChild(doc.createElement("br"));
                    cell.appendChild(doc.createTextNode("difference.  Please contact to SMS(SmartMySQL) support team if you have any doubts."));
                } else {
                    cell.appendChild(doc.createTextNode("1. The SmartMySQL could not identified any improvements."));
                    cell.appendChild(doc.createElement("br"));
                    cell.appendChild(doc.createTextNode("Please contact to TopDBs DB experts support to tune this query. TopDBs DB expert can improve performance with 100% gurantee incase if they"));
                    cell.appendChild(doc.createElement("br"));
                    cell.appendChild(doc.createTextNode(" could not improve then amount will be return back to you. Please contact TopDBs DB expert support  www.topdbs.in/consultancyService"));
                    cell.appendChild(doc.createElement("br"));
                }
            }
            
            newRow.appendChild(cell);
            table.appendChild(newRow);
        }
        
        // Expecting to improve performance with new index
        if (!isEmpty(recommends) && rowsMulti > 0) {
            
            String multi = "500% (6X Speed)";
            if (rowsMulti < 100) {
                multi = "50% (1.5X Speed)";
            } else if (rowsMulti < 300) {
                multi = "70% (1.7X Speed)";
            } else if (rowsMulti < 500) {
                multi = "80% (1.8X Speed)";
            } else if (rowsMulti < 1000) {
                multi = "100% (2.0X Speed)";
            } else if (rowsMulti < 5000) {
                multi = "150% (2.5X Speed)";
            } else if (rowsMulti < 10000) {
                multi = "200% (3X Speed)";
            } else if (rowsMulti < 20000) {
                multi = "300% (4X Speed)";
            } else if (rowsMulti < 50000) {
                multi = "350% (4.5X Speed)";
            } else if (rowsMulti < 100000) {
                multi = "400% (5X Speed)";
            } else if (rowsMulti < 200000) {
                multi = "430% (5.3X Speed)";
            }
            
            Element newRow = doc.createElement("tr");
         
            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #00ff00");
            
            Element el = doc.createElement("span");
            el.setTextContent("Expecting performance improvement after the Recommendations & index/es creation: " + multi);
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);
            cell.appendChild(doc.createElement("br"));
            
            newRow.appendChild(cell);
            table.appendChild(newRow);
        }
                
        
        // MySQL Optimizer converted quer
        //if (!isEmpty(map)) {
        {
            String optimQuery = recommends.getOptimizedQuery();
            
            if (optimQuery == null) {
                optimQuery = optimizedQuery;
            }
            
            if (optimQuery == null || optimQuery.isEmpty()) {
                optimQuery = "NONE";
            }
            
            Element newRow = doc.createElement("tr");

            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #ff00ff");

            Element el = doc.createElement("span");
            el.setTextContent("MySQL Optimizer converted query: ");
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode(optimQuery));

            newRow.appendChild(cell);
            table.appendChild(newRow);
        }
        
        // Notes
        if (!isEmpty(recommends)) {
            Element newRow = doc.createElement("tr");
         
            Element cell = doc.createElement("td");
            cell.setAttribute("style", "font-size: 14px; color: #ff0000");
            
            Element el = doc.createElement("span");
            el.setTextContent("Notes:");
            el.setAttribute("style", "font-weight: bold");
            cell.appendChild(el);
            
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("• It may cause of table locking and slowdown DB server performance while adding new"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("index/es. It is safe to add new indexes on non-business hours with downtime window."));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("• It is always recommended to validate new index performance improvement statistics on"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("pre-production/Test/Dev/Staging DB server with recent production data backup before"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("adding index/es on the production DB server."));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("• The SmartMySQL tool recommends optimal indexes for your query. It can give"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("performance improvement more than 98% cases. There are other areas like database"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("design, MySQL Server configuration Tuning and OS/Hardware optimization need to"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("concentrate for rest of cases"));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("• We recommend using “Create Index” button to identify the efficiency of new recommended"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("optimal index/es."));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("• The following document provided by Oracle MySQL team. Please read it to know about"));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("MySQL indexes."));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createTextNode("https://dev.mysql.com/doc/refman/5.7/en/mysql-indexes.html"));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("• There is no guarantee that the operation of the software will be uninterrupted or error-free."));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createElement("br"));
            
            cell.appendChild(doc.createTextNode("YOU ACKNOWLEDGE AND AGREE THAT YOUR USE OF ALL SOFTWARE IS AT YOUR OWN RISK."));
            cell.appendChild(doc.createElement("br"));
            cell.appendChild(doc.createElement("br"));
            
            Element a = doc.createElement("a");            
            ((EventTarget) a).addEventListener("click", linkListener, false);
            a.setAttribute("href", "#");
            a.setTextContent("https://dev.mysql.com/doc/refman/5.7/en/mysql-indexes.html");
            cell.appendChild(a);            
            
            
            newRow.appendChild(cell);
            table.appendChild(newRow);
        }        
        
    }
    
    public static List<AnalyzerResult.RecommendationResult> collectAnalyzerRows(List<AnalyzerResult> list) {
        
        List<AnalyzerResult.RecommendationResult> result = new ArrayList<>();
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.getRecomendationsSize() > 0) {

                    for (AnalyzerResult.RecommendationResult rr: t.getRecomendations()) {
                        result.add(rr);
                    }
                }

                if (t.getChildren() != null) {
                    result.addAll(collectAnalyzerRows(t.getChildren()));
                } else {
                    result.add(t.createEmptyRecomendation(t.getQuery()));
                }
            }
        }
        
        return result;
    }
    
    public static boolean checkEmptyWithOr(List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.isRecommendationEmptyWithOR()) {
                    return true;
                }

                if (t.getChildren() != null){
                    if (checkEmptyWithOr(t.getChildren())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    
    public static boolean hasAnalyzerRows(List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.getRecomendationsSize() > 0) {
                    return true;

                }

                if (t.getChildren() != null){
                    if (hasAnalyzerRows(t.getChildren())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    
    
    public void init(String query, ConnectionSession session, RunnedFrom runnedFrom) {
        init(query, session, null, false, runnedFrom);
    }
    
    public void init(String query, ConnectionSession session, Database database, boolean autorun, RunnedFrom runnedFrom) {
        
        PreferenceData pd = preferenceService.getPreference();
        
        indexMaxNoColumnsField.setText(pd.getQaIndexMaxNoColumns());
        indexSubstrLengthField.setText(pd.getQaIndexSubstrLength());
        useCacheCheckbox.setSelected(pd.isQaOtherUseCache());
        
        this.runnedFrom = runnedFrom;
        
        currentDatabase = database;
        
        RSyntaxTextAreaBuilder rSyntaxTextAreaBuilder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), session);
        SwingUtilities.invokeLater(() -> {
            rSyntaxTextAreaBuilder.init();
            
            queryArea = rSyntaxTextAreaBuilder.getTextArea();        
            Platform.runLater(() -> {
                queryAreaPane.setCenter(rSyntaxTextAreaBuilder.getSwingNode());
            });

            queryArea.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    changed();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    changed();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    changed();
                }

                private void changed() {
                    boolean disable = queryArea.getText() == null || queryArea.getText().trim().isEmpty();

                    analyzeButton.setDisable(disable);
                    visualExplainButton.setDisable(disable);
                    clearButton.setDisable(disable);
                    formatQueryButton.setDisable(disable);
                }
            });

            queryArea.append(query);
            
            formatQuery(null);
//            rSyntaxTextAreaBuilder.getTextScrollPane().getVerticalScrollBar().setValue(0);

            //queryArea.scrollRectToVisible(aRect);
            if (autorun) {
                analyzeQueryAction(null);
            }
        });    
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        limitTuningDatePicker.setValue(LocalDate.now());
        
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, 1);
        limitTuningTimeField.setText(String.format("%02d:%02d", c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE)));
        limitTuningBox.disableProperty().bind(limitTuningTimeCheckBox.selectedProperty().not());     
    }
}