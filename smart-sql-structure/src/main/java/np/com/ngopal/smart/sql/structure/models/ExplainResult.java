
package np.com.ngopal.smart.sql.structure.models;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ExplainResult {
    
    private final Long id;
    private final String selectType;
    private final String table;
    private final String partitions;
    private final String type;
    private final String posibleKyes;
    private final String key;
    private final Long keyLen;
    private final String ref;
    private final Long rows;
    private final Long filtered;
    private final String extra;
 
    public ExplainResult(Long id, String selectType, String table, String partitions, String type, String posibleKyes, String key, Long keyLen, String ref, Long rows, Long filtered, String extra) {
        this.id = id;
        this.selectType = selectType;
        this.table = table;
        this.partitions = partitions;
        this.type = type;
        this.posibleKyes = posibleKyes;
        this.key = key;
        this.keyLen = keyLen;
        this.ref = ref;
        this.rows = rows;
        this.filtered = filtered;
        this.extra = extra;
    }
 
    public Long getId() {
        return id;
    }
        
    public String getSelectType() {
        return selectType;
    }
    
    public String getTable() {
        return table;
    }
    
    public String getPartitions() {
        return partitions;
    }
    
    public String getType() {
        return type;
    }
    
    public String getPosibleKyes() {
        return posibleKyes;
    }
    
    public String getKey() {
        return key;
    }
    
    public Long getKeyLen() {
        return keyLen;
    }
    
    public String getRef() {
        return ref;
    }
    
    public Long getRows() {
        return rows;
    }
    
    public Long getFiltered() {
        return filtered;
    }
    
    public String getExtra() {
        return extra;
    }
    
    public String[] asRow() {
        return new String[] {
            getStringValue(id), 
            getStringValue(selectType),
            getStringValue(table),
            getStringValue(partitions),
            getStringValue(type),
            getStringValue(posibleKyes),
            getStringValue(key),
            getStringValue(keyLen),
            getStringValue(ref),
            getStringValue(rows),
            getStringValue(filtered),
            getStringValue(extra)
        };
    }
    
    private String getStringValue(Object prop) {
        return prop != null ? prop.toString() : "(NULL)";
    }
}