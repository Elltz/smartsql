/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.structure.utils.Flags;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class NetworkMetrics extends Metric {

    private int totalLen;
    
    public NetworkMetrics() {
        super("NETWORK METRIC", "This metric measures metrics");        
    }

    @Override
    public XYChart.Series<Number, Number>[] getXYChartSeries() {
        totalLen = getNetworkMetricExtract().getInterfaces().length * 2;
        XYChart.Series<Number, Number>[] series = new XYChart.Series[totalLen];
        int i =0;
        
        for(String face : getNetworkMetricExtract().getInterfaces()){
            face = face.toUpperCase();
            series[i] = getNewXYChart();
            series[i].setName(face + " received bytes");
            i++;
            series[i] = getNewXYChart();
            series[i].setName(face + " sent bytes");
            i++;
        }
        
        return series;
    }

    @Override
    public void updateUI(long timeInMillis, MetricExtract extract) {
        super.updateUI(timeInMillis, extract);
        if(extract != null){
            for (int i = 0; i < totalLen; i++) {
                String[] ss = data.get(i).getName().split(" ");
                XYChart.Data<Number, Number> myData = new XYChart.Data();
                myData.setXValue(timeInMillis);
                if (ss[1].charAt(0) == 's') {
                    //represents sent bytes
                    myData.setYValue(convertToUnitBytes(Long.valueOf(getNetworkMetricExtract()
                            .getNetworkInterfaceSentBytes(ss[0])), Flags.MEGABYTE));
                } else {
                    //represents received bytes
                    myData.setYValue(convertToUnitBytes(Long.valueOf(getNetworkMetricExtract()
                            .getNetworkInterfaceReceivedBytes(ss[0])), Flags.MEGABYTE));
                }
                data.get(i).getData().add(myData);
            }
        }
    }

    /**
     * its in BYTES
     * @return 
     */
    @Override
    public long getMaxYValue() {
        long yValue = 0;
        for(String s : getNetworkMetricExtract().getInterfaces()){
            try { 
                long d = Long.valueOf(getNetworkMetricExtract().getNetworkInterfaceReceivedBytes(s));
                if(d > yValue){ yValue = d; }
            }catch(NumberFormatException nfe){}
            
            try{
                long d = Long.valueOf(getNetworkMetricExtract().getNetworkInterfaceSentBytes(s));
                if(d > yValue){ yValue = d; }
            }catch(NumberFormatException nfe){}
            
            try{
                long d = Long.valueOf(getNetworkMetricExtract().getNetworkInterfaceReceivedPackets(s));
                if(d > yValue){ yValue = d; }
            }catch(NumberFormatException nfe){}
            
            try{
                long d = Long.valueOf(getNetworkMetricExtract().getNetworkInterfaceSentPackets(s));
                if(d > yValue){ yValue = d; }
            }catch(NumberFormatException nfe){}
        }
        
        yValue = convertToUnitBytes(yValue,Flags.MEGABYTE);
        return  yValue + ((long)(yValue * 0.4));
    }
    
    
    @Override
    public StringConverter<Number> yConverter() {
        return new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return String.valueOf(object.doubleValue()) + " MB";
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string.split(" ")[0]);
            }
        };
    }
    
    private NetworkMetricExtract getNetworkMetricExtract(){
        return (NetworkMetricExtract) metricExtract;
    }

    @Override
    public int getUnitTimeValue() {
        return super.getUnitTimeValue(); 
    }

    @Override
    public int getYTickUnitCount() {
        return 7;
    }

    @Override
    public String[] getChartColors() {
        return new String[]{"cyan","coral","purple","olive"};
    }

}
