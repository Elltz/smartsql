/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class DBConnectionInfo {
    String name;
        Integer conns;
        Integer activeConns;
        Integer sleepConns;
        Double activePercent;
        Double sleepPercent;
        Double percentage;
        
        public DBConnectionInfo(String name, Integer conns, Integer activeConns, Integer sleepConns, Double activePercent, Double sleepPercent, Double percentage) {
            this.name = name;
            this.conns = conns;
            this.activeConns = activeConns;
            this.sleepConns = sleepConns;
            this.activePercent = activePercent;
            this.sleepPercent = sleepPercent;
            this.percentage = percentage;
        }
        
        public String getName() {
            return name;
        }
        
        public Integer getConns() {
            return conns;
        }
        
        public Integer getActiveConns() {
            return activeConns;
        }
        
        public Integer getSleepConns() {
            return sleepConns;
        }
        
        public Double getActivePercent() {
            return activePercent;
        }
        
        public Double getSleepPercent() {
            return sleepPercent;
        }
        
        public Double getPercentage() {
            return percentage;
        }
}
