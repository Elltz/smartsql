package np.com.ngopal.smart.sql.structure.components;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public abstract class TableViewCellCopyHandler<T> implements EventHandler<KeyEvent> {
    
    private TableView<T> table;
    
    public TableViewCellCopyHandler(TableView<T> table) {
        this.table = table;
        
        table.getSelectionModel().setCellSelectionEnabled(true);        
        table.addEventFilter(KeyEvent.KEY_PRESSED, this);
    }

    @Override
    public void handle(KeyEvent event) {
        if ((event.isShortcutDown() || event.isShortcutDown()) && event.getCode() == KeyCode.C) {
            copyCellAction();
            event.consume();
        }
    }

    private void copyCellAction() {
        T row = table.getSelectionModel().getSelectedItem();
        if (row != null) {
            List<TablePosition> list = table.getSelectionModel().getSelectedCells();
            if (!list.isEmpty()) {
                String value = copyCell(row, list.get(0).getTableColumn());
                
                Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
                Clipboard systemClipboard = defaultToolkit.getSystemClipboard();
                
                systemClipboard.setContents(new StringSelection(value), null);
            }
        }
    }
    
    protected abstract String copyCell(T row, TableColumn column);
}
