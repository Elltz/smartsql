package np.com.ngopal.smart.sql.structure.queryutils;

import np.com.ngopal.smart.sql.model.utils.SqlUtils;
import com.sun.javafx.scene.DirtyBits;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingNode;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Bounds;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.input.ZoomEvent;
import javafx.util.Callback;
import javafx.util.Pair;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextAreaUI;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.folding.FoldParserManager;
import org.fife.ui.rtextarea.GutterIconInfo;
import org.fife.ui.rtextarea.RTextAreaUI;
import org.fife.ui.rtextarea.RTextScrollPane;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.structure.components.TooltipHelper;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import static np.com.ngopal.smart.sql.structure.queryutils.QueryUtils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import org.fife.ui.rtextarea.ConfigurableCaret;


/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 *
 * This class creates rs codes
 */
@Slf4j
public class RSyntaxTextAreaBuilder {

    private static final String SYNTAX_STYLE_SMART_SQL = "text/smartSQL";
    
    private static Theme theme;
    static {
        AbstractTokenMakerFactory tokenMakerFactory = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        tokenMakerFactory.putMapping(SYNTAX_STYLE_SMART_SQL, SmartSQLTokenMaker.class.getName());
        
        try {
            theme = Theme.load(StandardResourceProvider.getInstance().getRsSyntaxTheme());
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }   
    }
    
    private Label autocompleteLabel;
    
    @Getter(AccessLevel.PUBLIC)
    private Label closeAutocomplete;
            
    private RSyntaxTextArea textArea;
    private Font textAreaFont;
    private CustomerSwingNode swingNode;
    private ArrayList<Callable<?>> documentChangesCodeExecutors = new ArrayList(10);
    private SmartSQLCurlyFoldParser smartSQLCurlyFoldParser;
    private RTextScrollPane rTextScrollPane;
    private final CustomCompletionProvider provider;
    private AutoCompletion autoCompletion;
    private QueryAreaWrapper queryAreaWrapper;
    private String uuid;
    private String relativePath;
    private MenuItem[] items;
    private ConnectionSession session;
    private String currentDatabaseName;
    private boolean needExplain = false;
    private boolean completionnAdditionsCalled = false;
    
    private Map<Integer, String> warningEmptyKeyResult = new HashMap<>();
    private Map<Integer, String> warningMoreThen10kRowsResult = new HashMap<>();
    
    //because Swing Nodes do not have Node.setUserData();
    private static final HashMap<RSyntaxTextArea, RSyntaxTextAreaBuilder> rSyntaxTextAreaData = new HashMap(5);
    
    private List<BasicCompletion> currentQueryAliasCompletion = new ArrayList<>();
    
    private UIController controller;
    
    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PUBLIC)
    private QAPerformanceTuningService performanceTuningService;
    
    private List<CustomBackground> customBackgrounds;
    
    private PreferenceDataService preferenceService;
    private UserUsageStatisticService usageService;
    
    private QARecommendationService qars;
    private ColumnCardinalityService columnCardinalityService;
    
    private Callback<File, Void> openFileCallback;
    private Callback<List<Object>, Integer> tryDragWithJoinCallback;
    
    public static RSyntaxTextAreaBuilder getRSyntaxTextAreaBuilder(RSyntaxTextArea syntaxTextArea){
        return rSyntaxTextAreaData.get(syntaxTextArea);
    }
    
//    public RSyntaxTextAreaBuilder(String uuid, MenuItem[] items, PreferenceDataService preferenceService, 
//            Callback<File, Void> openFileCallback, Callback<List<Object>, Integer> tryDragWithJoinCallback, UIController controller) {
//        this(uuid, items, controller.getSelectedConnectionSession().get(), preferenceService, openFileCallback, tryDragWithJoinCallback, controller);
//    }
//    public RSyntaxTextAreaBuilder(String uuid, MenuItem[] items, ConnectionSession session, PreferenceDataService preferenceService, 
//            Callback<File, Void> openFileCallback, Callback<List<Object>, Integer> tryDragWithJoinCallback, UIController controller) {
//        this(uuid, items, session, preferenceService, openFileCallback, tryDragWithJoinCallback, controller, null);
//    }
    public RSyntaxTextAreaBuilder(String uuid, MenuItem[] items, ConnectionSession session, PreferenceDataService preferenceService, 
            Callback<File, Void> openFileCallback, Callback<List<Object>, Integer> tryDragWithJoinCallback, UIController controller, QAPerformanceTuningService performanceTuningService, QARecommendationService qars, ColumnCardinalityService columnCardinalityService, UserUsageStatisticService usageService) {
        this.session = session;
        this.items = items;   
        this.uuid = uuid;
        this.preferenceService = preferenceService;
        this.usageService = usageService;
        this.controller = controller;
        this.performanceTuningService = performanceTuningService;
        this.qars = qars;
        this.columnCardinalityService = columnCardinalityService;
                
        this.openFileCallback = openFileCallback;
        this.tryDragWithJoinCallback = tryDragWithJoinCallback;
        
        closeAutocomplete = new Label();
        closeAutocomplete.managedProperty().bind(closeAutocomplete.visibleProperty());
        closeAutocomplete.setOnMouseClicked((MouseEvent event) -> {
            hideAutocomplete(true);
        });
        closeAutocomplete.setStyle("-fx-padding: 0 5 0 0");
        closeAutocomplete.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getSimpleClose_image()));
        
        provider = RsSyntaxCompletionHelper.getInstance().getProvider();
                
//        try {
            //SwingUtilities.invokeLater(() -> {
        
        
            //});
//        } catch (InterruptedException ex) {
//            Logger.getLogger(RSyntaxTextAreaBuilder.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (InvocationTargetException ex) {
//            Logger.getLogger(RSyntaxTextAreaBuilder.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        completionAdditions(); 
    }
    
    public void init() {
        init("");
    }
    
    public void init(String text) {
            initialisers(text);
            
            // apply our custom theme
            if (theme != null) {
                theme.apply(textArea);
            }

            textArea.setPopupMenu(null);

            // Don't remove this!!!
            // If remove this line - then ENTER, BACSPACE and some others - will calling 2 times
            // add all that need consume
            swingNode.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
                
                if (event.getCode() == KeyCode.BACK_SPACE && TooltipHelper.getInstance().isShowed()) {
                    TooltipHelper.getInstance().hideTooltip();
                    event.consume();
                    return;
                }
                
                if (event.getCode() == KeyCode.DIGIT9 && !event.isMetaDown() && event.isShiftDown() && !event.isAltDown()) {
                    TooltipHelper.getInstance().showTooltip(controller.getStage(), textArea, provider.getAlreadyEnteredText(textArea) + "(");
                    event.consume();
                    return;
                }
                
                if (event.getCode() == KeyCode.DIGIT0 && !event.isMetaDown() && event.isShiftDown() && !event.isAltDown()) {
                    TooltipHelper.getInstance().hideTooltip();
                    event.consume();
                    return;
                }
                
                if (event.getCode() == KeyCode.TAB && !event.isMetaDown() && !event.isShiftDown() && !event.isAltDown()) {
                    TooltipHelper.getInstance().hideTooltip();
                    
                    provider.tabState();
                    autoCompletion.setPopupWindowSize(210, 200);

                    autoCompletion.doCompletion();
                    event.consume();
                    return;
                }
                
                if (event.getCode() == KeyCode.Q && event.isControlDown() && event.isShiftDown()) {
                    
                    if (preferenceService.getPreference().isQbActivateAutoComplete() && preferenceService.getPreference().isQbFeq()) {
                        
                        TooltipHelper.getInstance().hideTooltip();
                        
                        // show FEQ list
                        provider.feqState();
                        autoCompletion.setPopupWindowSize(800, 300);

                        autoCompletion.doCompletion();
                    }

                    event.consume();
                    return;
                }

                if ((event.getCode() == KeyCode.DIGIT8 || event.getCode() == KeyCode.F) && event.isControlDown() && event.isShiftDown()) {
                    // show Favorite list
                    if (preferenceService.getPreference().isQbActivateAutoComplete() && preferenceService.getPreference().isQbFavorites()) {
                        
                        TooltipHelper.getInstance().hideTooltip();
                        
                        provider.favoriteState();
                        autoCompletion.setPopupWindowSize(800, 300);

                        autoCompletion.doCompletion();   
                    }

                    event.consume();
                    return;
                }


                if (event.getCode() == KeyCode.H && event.isControlDown() && event.isShiftDown()) {
                    // show History list
                    if (preferenceService.getPreference().isQbActivateAutoComplete() && preferenceService.getPreference().isQbHistory()) {
                        
                        TooltipHelper.getInstance().hideTooltip();
                        
                        provider.historyState();
                        autoCompletion.setPopupWindowSize(800, 300);

                        autoCompletion.doCompletion();
                    }

                    event.consume();
                    return;
                }

                switch (event.getCode()) {
                    case ENTER:
                    case BACK_SPACE:
                    case DELETE:
                        event.consume();
                        return;
                }

                needExplain = ";".equals(event.getText());

                // need consume actions from context menu!!! or they will call 2 times
                if (event.isShortcutDown()&&
                       (event.getCode() == KeyCode.F ||
                        event.getCode() == KeyCode.H ||
    //                    event.getCode() == KeyCode.F9 ||
    //                    event.getCode() == KeyCode.F8 ||
    //                    event.getCode() == KeyCode.F12 ||
                        event.getCode() == KeyCode.Z ||
                        event.getCode() == KeyCode.Y ||
                        event.getCode() == KeyCode.X ||
                        event.getCode() == KeyCode.C ||
                        event.getCode() == KeyCode.V ||
                        event.getCode() == KeyCode.A ||
                        event.getCode() == KeyCode.G ||
                        event.getCode() == KeyCode.SPACE ||
                        event.getCode() == KeyCode.ENTER ||
                        event.getCode() == KeyCode.PAGE_UP ||
                        event.getCode() == KeyCode.PAGE_DOWN)) {
                    event.consume();
                    return;
                }

                if (event.isShortcutDown()&& event.isShiftDown() &&
                       (event.getCode() == KeyCode.T ||
                        event.getCode() == KeyCode.DIGIT1 ||
                        event.getCode() == KeyCode.DIGIT2 ||
                        event.getCode() == KeyCode.DIGIT3 ||
                        event.getCode() == KeyCode.SPACE)) {
                    event.consume();
                    return;
                }

                if (event.isShiftDown() && (event.getCode() == KeyCode.INSERT || event.getCode() == KeyCode.F12)) {
                    event.consume();
                    return;
                }

                if (!autoCompletion.isPopupVisible()) {
                    provider.defaultState();
                    autoCompletion.setPopupWindowSize(210, 200);
                }
            });

            //This is to help swingNode know if he needs repaint children, since some events
            // in our app change layer and rendering of Jcomponent but they not see because
            // not all times swingNode pass those specific events for repaint
            swingNode.boundsInLocalProperty().addListener(new ChangeListener<Bounds>() {
                @Override
                public void changed(ObservableValue<? extends Bounds> observable,
                        Bounds oldValue, Bounds newValue) { 
                    if (oldValue == null || !oldValue.equals(newValue)) {
                        Platform.runLater(() -> {
                            refreshTextArea();
                        });
                    }
                }
            });

            swingNode.addEventFilter(EventType.ROOT, event -> {
                if (event instanceof DragEvent) {
                    final String type = event.getEventType().getName();
                    if (type.contains("DRAG") || type.contains("drag")) {

                        // Avoid drag event reaching the underlying swing components
                        event.consume();
                    }
                }
            });

            //to remove swing embeded npe exceptions
            swingNode.addEventFilter(DragEvent.DRAG_OVER, new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    Dragboard db = event.getDragboard();
                    if (db.hasString() || db.hasFiles()) {
                        event.acceptTransferModes(TransferMode.MOVE);
                    }
                    event.consume();
                 
                    ((ConfigurableCaret)textArea.getCaret()).setAlwaysVisible(true);
                    
                    double x = event.getX() - queryAreaWrapper.getDragShiftX();
                    double y = event.getY() - queryAreaWrapper.getDragShiftY();

                    int i = queryAreaWrapper.hitPosition(x, y);   
                    
                    textArea.setCaretPosition(i);
                }
            });
            
            swingNode.addEventFilter(DragEvent.DRAG_EXITED, new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    ((ConfigurableCaret)textArea.getCaret()).setAlwaysVisible(false);
                }
            });
            
            swingNode.addEventFilter(DragEvent.DRAG_DONE, new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    ((ConfigurableCaret)textArea.getCaret()).setAlwaysVisible(false);
                }
            });

            swingNode.addEventFilter(ZoomEvent.ZOOM, new EventHandler<ZoomEvent>() {
                @Override
                public void handle(ZoomEvent event) {
                    System.out.println("Zoom");
                    System.out.println(event.getTotalZoomFactor());
                    if (controller != null) {
                        controller.getSmartProperties().getFontSize().set(
                                controller.getSmartProperties().getFontSize().get() * event.getTotalZoomFactor());
                    }
                    event.consume();
                }
            });

            if(preferenceService != null && tryDragWithJoinCallback != null && openFileCallback != null && controller != null){
                //to remove swing embeded npe exceptions
                swingNode.addEventFilter(DragEvent.DRAG_DROPPED, getDraggedDropped(preferenceService, 
                        openFileCallback, tryDragWithJoinCallback));
            }
            // Add custom FoldParser for collapsing functionality with '(' and ')'
            FoldParserManager.get().addFoldParserMapping(SYNTAX_STYLE_SMART_SQL, smartSQLCurlyFoldParser);

            // Enabling folding and set SQL style edit
            textArea.setCodeFoldingEnabled(true);
            textArea.setSyntaxEditingStyle(SYNTAX_STYLE_SMART_SQL);
            textArea.setSelectedTextColor(java.awt.Color.WHITE);
            textArea.setCurrentLineHighlightColor(Color.WHITE);
            textArea.setSelectionColor(UIManager.getColor("TextArea.selectionBackground"));
            textArea.setMatchedBracketBorderColor(java.awt.Color.yellow);
            textArea.setMatchedBracketBGColor(java.awt.Color.yellow);
            textArea.setPaintMatchedBracketPair(true);                
            // Don't enable line wrap flag
    //        textArea.setLineWrap(true);

            rTextScrollPane.setIconRowHeaderEnabled(true);       
            autoCompletion.setAutoActivationDelay(0);
            autoCompletion.setShowDescWindow(false);
            autoCompletion.install(textArea);
            autoCompletion.setAutoCompleteSingleChoices(false);
            autoCompletion.setAutoActivationEnabled(true);
            autoCompletion.setPopupWindowSize(210, 200);
    //        autoCompletion.setTriggerKey(KeyStroke.getKeyStroke(Character.SPACE_SEPARATOR,
    //                java.awt.event.InputEvent.CTRL_DOWN_MASK));

            swingNode.setContent(rTextScrollPane);

            swingNode.setOnMouseClicked((MouseEvent event) -> {
                try {
                    if (controller != null && event.getX() <= 18) {
                        GutterIconInfo[] icons = rTextScrollPane.getGutter().getTrackingIcons(new Point((int)event.getX(), (int)event.getY()));
                        if (icons != null) {
                            for (GutterIconInfo i: icons) {
                                if (i.getIcon() == StandardDefaultImageProvider.getInstance().getWarningIcon_image()) {
                                    String query = warningEmptyKeyResult.get(i.getMarkedOffset() + 1);
                                    if (query == null) {
                                        query = warningMoreThen10kRowsResult.get(i.getMarkedOffset() + 1);
                                    }

                                    if (usageService != null) {
                                        usageService.savePUsage(PUsageFeatures.CBQ);
                                    }

                                    openQueryAnalyzer(query != null ? query : "", controller, RunnedFrom.QA, performanceTuningService, preferenceService, qars, columnCardinalityService, usageService, null);
                                    break;
                                }
                            }
                        }
                    }
                } catch (BadLocationException ex) {
                    log.error(ex.getMessage(), ex);
                }
            });

            if(controller != null){ 
                SmartSQLTokenMaker.smartSQLTokenMakerConnectionSession.bind(controller.getSelectedConnectionSession());

                //listeners to trigger automatic changes
                if (preferenceService != null) {
                    autoCompletion.setAutoCompleteEnabled(preferenceService.getPreference().isQbActivateAutoComplete());
                }

                controller.getSmartProperties().getFontColor().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable,
                            String oldValue, String newValue) {
                        if (newValue != null) {
                            textArea.setForeground(java.awt.Color.decode(newValue));
                        }
                    }
                });
                controller.getSmartProperties().getBackgroundColor().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable,
                            String oldValue, String newValue) {
                        if (newValue != null) {
                            textArea.setBackground(java.awt.Color.decode(newValue));
                        }
                    }
                });
                controller.getSmartProperties().getSelectionColor().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable,
                            String oldValue, String newValue) {
                        if (newValue != null) {
                            textArea.setSelectionColor(java.awt.Color.decode(newValue));
                        }
                    }
                });
    //            controller.getSmartProperties().getFontSize().addListener(new ChangeListener<Number>() {
    //                @Override
    //                public void changed(ObservableValue<? extends Number> observable,
    //                        Number oldValue, Number newValue) { 
    //                    if(newValue.doubleValue() == 0.0){return;}
    //                    textArea.setFont(textArea.getFont().deriveFont(newValue.floatValue()));
    //                }
    //            });
            }        
            //this is suppose to scroll to the bottom automatically
            ((DefaultCaret)textArea.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }
    
    public void listAllTags() {
        provider.fullState();
        doCompletion();
//        provider.defaultState();
    }
    
    public void setWarningEmptyKeyResult(Map<Integer, String> warningEmptyKeyResult) {
        this.warningEmptyKeyResult = warningEmptyKeyResult;
    }
    
    public void setWarningMoreThen10kRowsResult(Map<Integer, String> warningMoreThen10kRowsResult) {
        this.warningMoreThen10kRowsResult = warningMoreThen10kRowsResult;
    }
    
    public void updateAutoComplete() {
        if (preferenceService != null) {
            autoCompletion.setAutoCompleteEnabled(preferenceService.getPreference().isQbActivateAutoComplete());
        }
    }
    
    public static void updateAllAutoComplete() {
        for (RSyntaxTextAreaBuilder b: rSyntaxTextAreaData.values()) {
            b.updateAutoComplete();
        }
    }
    
    
    public static RSyntaxTextAreaBuilder createSimple(String uuid, ConnectionSession session) {
        return new RSyntaxTextAreaBuilder(uuid, null, session, null, null, null, null, null, null, null, null);
    }
    
    public void doCompletion() {
        if (autoCompletion != null && preferenceService.getPreference().isQbActivateAutoComplete()) {
            autoCompletion.doCompletion();
        }
    }

    public void setAutocompeteLabel(Label autocompleteLabel) {
        this.autocompleteLabel = autocompleteLabel;
    }
    
    public void hideAutocomplete(boolean hide) {
        if (autocompleteLabel != null) {
            autocompleteLabel.setVisible(!hide);            
            closeAutocomplete.setVisible(!hide);
        }
    }
    
    public static void hideAllAutocomplete(boolean hide) {
        for (RSyntaxTextAreaBuilder b: rSyntaxTextAreaData.values()) {
            b.hideAutocomplete(hide);
        }
    }
    
    public void changeFont(String font) {
        if (font != null) {
            textAreaFont = FontUtils.parseAWTFont(font);
            if (textAreaFont != null) {
                try {
                    textArea.setFont(textAreaFont);
                } catch (Throwable th) {
                    // some times textarea not connected to UI and it throws error in get graphics
                    // i think its not a problem
                }
            }
        }
    }
    
    public void changeTabSettings(String tabSize, boolean needSpaces) {
        try {
            textArea.setTabSize(Integer.valueOf(tabSize.trim()));
            textArea.setTabsEmulated(needSpaces);
        } catch (Throwable th) {}
    }
    
    public static void changeAllTabSettings(String tabSize, boolean needSpaces) {
        for (RSyntaxTextAreaBuilder b: rSyntaxTextAreaData.values()) {
            b.changeTabSettings(tabSize, needSpaces);
        }
    }
    
    
    public static void changeAllFont(String font) {
        for (RSyntaxTextAreaBuilder b: rSyntaxTextAreaData.values()) {
            b.changeFont(font);
        }
    }
    
//    private int resetCompletions(ConnectionParams cp, String s) {        
//        System.out.println("resetCompletions called");
//        
//        if (cp != null) {
//            synchronized (provider) {
//                provider.clear();
//                completionAddedDatabases.clear();
//                return addCompletions(cp, s);
//            }
//        }
//        
//        return 0;
//    }
//    
//    private int addCompletions(ConnectionParams cp, String s) {
//        List<Completion> list = RsSyntaxCompletionHelper.getInstance().getCompletionsForDatabase(cp, s);
//        provider.addCompletions(list);
//        
//        return list.size();
//    }
    
//    public synchronized int rebuildCompletions() {
//        if (session != null) {
//            RsSyntaxCompletionHelper.getInstance().loadCompletionList(session.getConnectionParam(), controller);
//            return resetCompletions(session.getConnectionParam(), currentDatabaseName);
//        }
//        return 0;
//    }
    
//    private void completionsAdder() {
//        Recycler.addWorkProc(new WorkProc<Object>() {
//            @Override
//            public void updateUI() {}
//
//            @Override
//            public void run() {
//                callUpdate = true;
//                rebuildCompletions();
//            }
//        });
//        completionnAdditionsCalled = true;
//    }
//    private void completionAdditions() {
//        if (controller != null && controller.getSmartProperties().getAutoRebuildTagsOnStartup().get()
//                && !completionnAdditionsCalled) {
//            if (controller.isApplicationInRestorationState()) {
//                controller.getApplicationRestoration_PersistingOperationState().addListener(new ChangeListener<Boolean>() {
//                    @Override
//                    public void changed(ObservableValue<? extends Boolean> observable,
//                            Boolean oldValue, Boolean newValue) { 
//                        if(newValue.equals(Boolean.FALSE)){
//                            completionsAdder();
//                            controller.getApplicationRestoration_PersistingOperationState().removeListener(this);
//                        }
//                    }
//                });                
//            } else { completionsAdder(); }
//        }
//    }

    private class CustomerSwingNode extends SwingNode {
        
        public void refreshNode() {
            setContent(getContent());
            
            impl_geomChanged();
            impl_markDirty(DirtyBits.NODE_GEOMETRY);
        }
        
    }
    
    private void initialisers(String text) {
        swingNode = new CustomerSwingNode();
        
        textArea = new RSyntaxTextArea(text) {
            
            public Font getFontForTokenType(int type) {
                Font f = super.getFontForTokenType(type);
                if (f != null && textAreaFont != null) {
                    f = f.deriveFont(textAreaFont.getStyle(), textAreaFont.getSize());
                }
                return f;
            }
            
            @Override
            protected RTextAreaUI createRTextAreaUI() {
                
                RSyntaxTextArea rsta = this;
                
                return new RSyntaxTextAreaUI(this) {
                    
                    boolean paintMatched = false;
                    
                    @Override
                    protected void paintMatchedBracketImpl(Graphics g, RSyntaxTextArea rsta, Rectangle r) {
                        paintMatched = true;
                        super.paintMatchedBracketImpl(g, rsta, r);
                    }
                    
                    
                    @Override
                    protected void paintMatchedBracket(Graphics g) {
                        
                        g.setColor(Color.white);
                        g.fillRect(0, 0, getBounds().width, getBounds().height);
                        
                        super.paintMatchedBracket(g);
                        int pos = textArea.getCaretPosition();
                        String str0 = "";
                        String str1 = "";
                        try {
                            str0 = textArea.getText(pos - 1, 1);
                            try {
                                str1 = textArea.getText(pos, 1);
                            } catch (Throwable th) {}
                        
                            boolean flag0 = "(".equals(str0) || ")".equals(str0);
                            boolean flag1 = "(".equals(str1) || ")".equals(str1);
                            if (!paintMatched && (flag0 || flag1)) {
                                
                                Rectangle rect = textArea.modelToView(flag0 ? pos - 1 : pos);
                                rect.width = 8;
                                
                                Color oldBG = rsta.getMatchedBracketBGColor();
                                Color oldB = rsta.getMatchedBracketBorderColor();
                                
                                Color c = new Color(255, 156, 156);
                                rsta.setMatchedBracketBGColor(c);
                                rsta.setMatchedBracketBorderColor(c);
                                
                                paintMatchedBracketImpl(g, rsta, rect);
                                
                                rsta.setMatchedBracketBGColor(oldBG);
                                rsta.setMatchedBracketBorderColor(oldB);
                            }
                        
                        } catch (Throwable th) {
                        }
                        
                        paintMatched = false;
                    }
                    
                    
                    @Override
                    public void update(Graphics g, JComponent c) {
                        Color defColor = g.getColor();
                        
                        g.setColor(Color.white);
                        g.fillRect(0, 0, getBounds().width, getBounds().height);
                        
                        if (customBackgrounds != null) {
                            for (CustomBackground cb: customBackgrounds) {
                                try {
                                    Rectangle start = textArea.modelToView(cb.start);
                                    Rectangle end = textArea.modelToView(cb.end);                                    
                                   
                                    g.setColor(cb.color);
                                    
                                    if (start.y != end.y) {
                                        int x = start.x;
                                        int y = start.y;
                                        int w = getBounds().width - x;
                                        int h = start.height;
                                        int length = (end.y - start.y) / start.height;
                                        for (int i = 0; i <= length; i++) {
                                            g.fillRect(x, y, w, h);
                                            x = 0;
                                            y += start.height;
                                            w = i + 1 == length ? end.x : getBounds().width;
                                        }
                                    } else {
                                        g.fillRect(start.x, start.y, end.x - start.x, start.height);
                                    }
                                    
                                    
                                } catch (Throwable ex) {
                                }
                            }
                        }
                        
                        g.setColor(defColor);
                        try {
                            super.update(g, c);
                        } catch (Throwable th) {
                            log.warn("Draw error", th);
                        }
                    }
                };
            }
        };
        
        textArea.getDocument().addDocumentListener(new DocumentListener() {
                        
            Field seqmentField = null;
            
            @Override
            public void insertUpdate(DocumentEvent e) { }

            @Override
            public void removeUpdate(DocumentEvent e) { }

            @Override
            public void changedUpdate(DocumentEvent e) {
//                if(seqmentField == null){
//                    seqmentField = org.fife.ui.rsyntaxtextarea.RSyntaxDocument.class.getDeclaredFields()[8];
//                    seqmentField.setAccessible(true);
//                }
                try {
//                    javax.swing.text.Segment s = (javax.swing.text.Segment) seqmentField.get(e.getDocument());
//                    if (s.length() > 0) {
                        update();//changed();
//                    }
                } catch (Exception ex) { ex.printStackTrace(); }
                
                
                preparedAliasCompletion();
            }
            
            private void update() {
                for (Callable c : documentChangesCodeExecutors) {
                    try {
                        c.call();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } 
            }
        });
        
        rTextScrollPane = new RTextScrollPane(textArea);
        smartSQLCurlyFoldParser = new SmartSQLCurlyFoldParser();
        autoCompletion = new AutoCompletion(provider) {
            @Override
            public void doCompletion() {
                // if we showing tooltip - no need any completions
                if (TooltipHelper.getInstance().isShowed()) {
                    return;
                }
                
                super.doCompletion();
            }
            
            
            @Override
            protected void insertCompletion(Completion c, boolean typedParamListStartChar) {
                
                String needAddAfterCompletion = null;
                if (c instanceof SmartSQLDatabaseReferenceCompletion) {
                    
                    Object obj = ((SmartSQLDatabaseReferenceCompletion)c).getDependedObject();                    
                    if (obj instanceof Database) {
                        
                        String databaseName = ((Database)obj).getName();
                        RsSyntaxCompletionHelper.getInstance().addCompletions(session.getConnectionParam(), databaseName);
                    }
                                    
                    Object data = ((SmartSQLDatabaseReferenceCompletion) c).getDependedObject();
                    String text = ((SmartSQLDatabaseReferenceCompletion) c).getCurrentText().trim().toLowerCase();
                    
                    if (data instanceof Database || data instanceof DBTable || data instanceof View) {
                        List<String> names = highlightGreenMap.get(session);
                        if (names == null) {
                            names = new ArrayList<>();
                            highlightGreenMap.put(session, names);
                        }

                        if (!names.contains(text)) {
                            names.add(text);
                        }

                    } else if (data instanceof StoredProc || data instanceof Function || data instanceof Event || data instanceof Trigger) {
                        List<String> names = highlightMaroonMap.get(session);
                        if (names == null) {
                            names = new ArrayList<>();
                            highlightMaroonMap.put(session, names);
                        }

                        if (!names.contains(text)) {
                            names.add(text);
                        }
                    }
                    
                    Pair<Integer, String> result = new ScriptRunner(null, true).findQueryAtPosition(textArea.getText(), textArea.getCaretPosition(), true);
                    if (result != null && result.getValue() != null) {
                        
                        String[] lines = result.getValue().split("\n");
                        String query = "";
                        boolean founded = false;
                        for (String line: lines) {
                            if (!founded && line.trim().toLowerCase().startsWith("select")) {
                                query = line;
                                founded = true;
                            } else {
                                if (!query.isEmpty()) {
                                    query += "\n";
                                }
                                 query += line;
                            }
                        }
                        
                        int pos = textArea.getText().indexOf(query);
                        
                        try {
                            needAddAfterCompletion = 
                                getJoinConditionIfNeed(
                                    QueryFormat.parseQuery(query), 
                                    data,
                                    ((SmartSQLDatabaseReferenceCompletion) c).getReferenceDatabase(),
                                    query.substring(0, textArea.getCaretPosition() - pos), 
                                    query.substring(textArea.getCaretPosition() - pos)
                                );
                        } catch (Throwable th) {
                            log.error("Error", th);
                            log.error("TextArea: '" + textArea.getText() + "'");
                            log.error("Query: '" + query + "'");
                            log.error("Caret position: '" + textArea.getCaretPosition() + "'");
                        }
                    }
                }
                
                super.insertCompletion(c, typedParamListStartChar);
                
                if (needAddAfterCompletion != null) {
                    textArea.insert(needAddAfterCompletion, textArea.getCaretPosition());
                }
                
            }
            
            private String getJoinConditionIfNeed(QueryClause queryClause, Object table, String database, String left, String right) {
                // 1. Object must be a table
                // 2. Query must be select
                if (table instanceof DBTable && queryClause instanceof SelectCommand) {
                    
                    // left must end with word JOIN
                    String[] leftStrs = left.split("\\s+", -1);
                    if (leftStrs.length <= 1 || !"join".equalsIgnoreCase(leftStrs[leftStrs.length - 2])) {
                        return null;
                    }
                    
                    // right must start from word not "ON"
                    String[] rightStrs = right.trim().split("\\s+");
                    if (rightStrs.length > 0 && "on".equalsIgnoreCase(rightStrs[0])) {
                        return null;
                    }
                    
                    // try to build join condition
                    int joinIndex = left.toLowerCase().lastIndexOf("join");
                    int startOfQueryIndex = left.substring(0, joinIndex).lastIndexOf(";") + 1;
                    
                    JoinMaker.JoinResult result = JoinMaker.makeJoin(session, 
                        null, 
                        (MysqlDBService) session.getService(),
                        database,
                        (DBTable)table, 
                        left.substring(startOfQueryIndex, joinIndex), 
                        false
                    );
                    
                    if (result != null) {
                        List<JoinMaker.JoinCondition> conditions = result.getTableJoinCondition((DBTable)table);
                        if (conditions != null && !conditions.isEmpty()) {

                            StringBuilder sb = new StringBuilder();
                            result.processJoinConditions(conditions, sb);

                            return sb.toString();
                        }
                    }
                }

                return null;
            }
            
        };
        //autoCompletion.setChoicesWindowSize(RsSyntaxCompletionHelper.getInstance().getOverallWidth(), 200);
        rSyntaxTextAreaData.putIfAbsent(textArea, this);
    }
    
    
    
    
    public void preparedAliasCompletion() {        
        
        if (!currentQueryAliasCompletion.isEmpty()) {
            for (BasicCompletion bc: currentQueryAliasCompletion) {
                provider.removeCompletion(bc);
            }
            
            currentQueryAliasCompletion.clear();
        }
        
        if (session != null) {
            
            ConnectionParams cp = session.getConnectionParam();
            if (cp != null) {
                
                Database db = cp.getSelectedDatabase();

                if (db != null && controller != null && preferenceService != null && preferenceService.getPreference().isQbActivateAutoComplete()) {
                    
                    Pair<Integer, String> result = new ScriptRunner(null, true).findQueryAtPosition(textArea.getText(), textArea.getCaretPosition(), false);
                    if (result != null && result.getValue() != null && result.getValue().length() < 5000) {
                        prepareQuery(QueryFormat.parseQuery(result.getValue()), cp, db);                        
                    }
                }
            }
        }
    }
    
    
    private void prepareQuery(QueryClause query, ConnectionParams cp, Database db) {
        
        if (query instanceof SelectCommand) {
            
            FromClause fc = ((SelectCommand)query).getFromClause();
            
            if (fc != null && fc.getTables() != null) {
                for (TableReferenceClause t: fc.getTables()) {
                    prepareTableAlias(t, cp, db);                           
                }
            }

            if (((SelectCommand) query).getUnionSelectCommands() != null) {
                for (SelectCommand sc: ((SelectCommand) query).getUnionSelectCommands()) {
                    prepareQuery(sc, cp, db);
                }
            }

            if (!((SelectCommand) query).getQuerySources().isEmpty()) {
                for (String q: ((SelectCommand) query).getQuerySources()) {
                    q = q.trim();
                    if (q.startsWith("(")) {
                        q = q.substring(1);
                    }

                    if (q.endsWith(")")) {
                        q = q.substring(0, q.length() - 1);
                    }


                    prepareQuery(QueryFormat.parseQuery(q), cp, db);                    
                }
            }
        }
    }
    
    private String splitTableName(String table) {
        if (table.contains(".")) {
            String[] s = table.split("\\.");
            table = s[s.length - 1];
        }

        if (table.startsWith("`") && table.endsWith("`")) {
            table = table.substring(1, table.length() - 1);
        }
        
        return table;
    }
    
    
    private void prepareTableAlias(TableReferenceClause t, ConnectionParams cp, Database db) {
        if (t != null) {
            if (t.getAlias() != null && !t.getAlias().isEmpty()) {
                DBTable table = DatabaseCache.getInstance().getTable(cp.cacheKey(), db.getName(), splitTableName(t.getName()));

                if (table != null) {
                    for (Column col: table.getColumns()) {
                        StringBuilder sb = new StringBuilder(300);
                        sb.append(t.getAlias());
                        sb.append(".");
                        sb.append(col.getName());
                        SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, sb.toString(), col, (col.getView() == null) ? col.getTable().getDatabase().getName()
                            : col.getView().getDatabase().getName(), RsSyntaxCompletionHelper.columnIcon);
                        b.setRelevance(2);

                        currentQueryAliasCompletion.add(b);
                        provider.addCompletion(b);
                    }
                }
            }

            if (t.getJoinClauses() != null) {
                for (JoinClause jc: t.getJoinClauses()) {
                    prepareTableAlias(jc.getTable(), cp, db);
                }
            } 
        }
    }
    
    public QueryAreaWrapper getQueryWrapper() {
        if (queryAreaWrapper == null) {
            addDocumentListenerCodeExecutor(getDefaultDocumentListenerCodeExecutor());
            queryAreaWrapper = new QueryAreaWrapper(uuid, textArea, items);
            items = null;
        }
        return queryAreaWrapper;
    }

    public SwingNode getSwingNode() {
        return swingNode;
    }
    
    public void addDocumentListenerCodeExecutor(Callable<?>  c){
        documentChangesCodeExecutors.add(c);
    }
    
    private Callable getDefaultDocumentListenerCodeExecutor() {
        // workarround for clearing error selection
        return (Callable) () -> {
            textArea.setSelectionColor(UIManager.getColor("TextArea.selectionBackground"));
            //getrTextScrollPane().repaint();
            return null;
        };
    }

    public RSyntaxTextArea getTextArea() {
        return textArea;
    }

    public RTextScrollPane getTextScrollPane() {
        return rTextScrollPane;
    }
    
    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }
    
    public boolean getNeedExplain() {
        return needExplain;
    }
    public void setNeedExplain(boolean needExplain) {
        this.needExplain = needExplain;
    }
    
    public boolean isReferencingAFile(){
        return relativePath != null && new File(relativePath).exists();
    }
    
    private EventHandler<DragEvent> getDraggedDropped(PreferenceDataService preferenceService, 
            Callback<File, Void> openFileCallback, Callback<List<Object>, Integer> tryDragWithJoinCallback){
        
        return new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                
                ((ConfigurableCaret)textArea.getCaret()).setAlwaysVisible(false);
                
                Dragboard db = event.getDragboard();
                if (db.hasString()) {
                    String[] data = db.getString().split("\\|");
                    
                    double x = event.getX() - queryAreaWrapper.getDragShiftX();
                    double y = event.getY() - queryAreaWrapper.getDragShiftY();
                    
                    Recycler.addWorkProc(new WorkProc() {
                        Integer pos;

                        @Override
                        public void updateUI() { }

                        @Override
                        public void run() {
                            
                            String database = null;
                            String table = null;
                            String column = null;
                            String newQuery = null;
                            
                            // check query for join                            
                            int i = queryAreaWrapper.hitPosition(x, y);                            
                            
                            int line = queryAreaWrapper.getLineByPosition(i);        
                            
                            String[] lines = queryAreaWrapper.getText().split("\\n");
                            
                            
                            Map<String, Object> queryData = SqlUtils.getQueryInsideParents(queryAreaWrapper.getText(), i, i);
                            
                            boolean columnAsTable = "column".equals(data[0]) && queryData != null && (boolean)queryData.get("inside") && ((String)queryData.get("query")).trim().isEmpty();
                            
                            if ("tableOrView".equals(data[0])) {
                                database = data[2];
                                table = data[1];
                                column = null;
                                newQuery = data[3];
                                
                                List<String> names = highlightGreenMap.get(session);
                                if (names == null) {
                                    names = new ArrayList<>();
                                    highlightGreenMap.put(session, names);
                                }

                                if (!names.contains(data[1].toLowerCase())) {
                                    names.add(data[1].toLowerCase());
                                }

                                if (!names.contains(data[2].toLowerCase())) {
                                    names.add(data[2].toLowerCase());
                                }

                            } else if ("functionOrProcudure".equals(data[0])) {
                                database = null;
                                table = data[1];
                                column = null;
                                newQuery = data[3];
                                
                                List<String> names = highlightMaroonMap.get(session);
                                if (names == null) {
                                    names = new ArrayList<>();
                                    highlightMaroonMap.put(session, names);
                                }

                                if (!names.contains(data[1].toLowerCase())) {
                                    names.add(data[1].toLowerCase());
                                }
                            } else if ("column".equals(data[0])) {
                                database = data[3];
                                table = data[2];
                                column = data[1];
                                if (line >= lines.length || lines[line].trim().isEmpty() || columnAsTable) {
                                    newQuery = data[4];
                                } else {
                                    newQuery = column;
                                }
                            }
                            
                            
                            // exist only for table
                            if ("tableOrView".equals(data[0]) || columnAsTable) {
                                pos = tryDragWithJoinCallback.call(Arrays.asList(session, database, table, queryAreaWrapper, queryData, x, y));
                            }
                            
                            if (pos == null) {
                                pos = i;
                                if (lines.length > line && !lines[line].trim().isEmpty() && queryAreaWrapper.getText().length() > pos) {
                                    String left = queryAreaWrapper.getText().substring(0, pos);
                                    String right = queryAreaWrapper.getText().substring(pos);
                                                                        
                                    if (!"column".equals(data[0]) && !left.trim().endsWith("(") && !right.trim().startsWith(")")) {
                                        Platform.runLater(() -> {
                                           DialogHelper.showInfo(controller, "Info0", "Please drag query into new line or drag query into middle of ()", null);
                                        });
                                        return;
                                    } else if (newQuery.trim().endsWith(";")) {
                                        newQuery = newQuery.substring(0, newQuery.length() - 1);
                                    }
                                }
                                
                                if (queryAreaWrapper.getText().length() <= pos) {
                                    newQuery = "\n" + newQuery;
                                }
                                
                                PreferenceData pd = preferenceService.getPreference();
                                if (!pd.isQueriesUsingBackquote()) {
                                    newQuery = newQuery.replaceAll("`", "");
                                }
                                
                                String staticQuery = newQuery;
                                SwingUtilities.invokeLater(() -> {
                                    String q = staticQuery;
                                    try {
                                        q = formatCurrentQuery(preferenceService, staticQuery);
                                    } catch (Throwable th) {
                                    }
                                    textArea.insert(q, pos);
                                });
                            }
//                            SwingUtilities.invokeLater(() -> {
//                                formatCurrentQuery(controller, preferenceService, controller != null ? controller.getSelectedConnectionSession().get() : null, queryAreaWrapper, pos + 5);
//                            });
                        }
                    });
                } else if (db.hasFiles()) {
                    List<File> files = db.getFiles();
                    Thread th = new Thread(() -> {
                        if (files != null) {
                            for (File f : files) {
                                openFileCallback.call(f);
                            }
                        }
                    });
                    th.setDaemon(true);
                    th.start();
                }
                event.setDropCompleted(true);
                event.consume();
            }
        };
    }
    
    public boolean isLineEmpty(String line) {
        return line == null || line.trim().isEmpty() || SqlUtils.checkQuery(line);
    }
    
//    public void autoCompleteReCalculator() {
//        completionAdditions();
//    }   
//    
//    public void setCurrentDatabaseName(String s){
//        currentDatabaseName = s;
//        SwingUtilities.invokeLater(() -> {
//            resetCompletions(session != null ? session.getConnectionParam() : null, s);
//        });
//    }
    
    public void refreshTextArea() {
//        textArea.revalidate();
        SwingUtilities.invokeLater(() -> {
            rTextScrollPane.validate();
            swingNode.refreshNode();
        });
    }
    
    public void clearCustomBackground() {
        if (customBackgrounds != null) {
            customBackgrounds.clear();
        }
    }
    
    public void addCustomBackground(int start, int end, Color color) {
        if (customBackgrounds == null) {
            customBackgrounds = new ArrayList<>();
        }
        
        customBackgrounds.add(new CustomBackground(start, end, color));
    }
    
    
    @AllArgsConstructor
    private class CustomBackground {
        private int start;
        private int end;
        private Color color;
    }
}
