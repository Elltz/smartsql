/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.structure.utils;

import com.google.common.base.Objects;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PostgresDBService;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PgDatabase;
import np.com.ngopal.smart.sql.model.View;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class PgDatabasesCache {
    
    private static PgDatabasesCache pCache;
    private final Map<String, Map<String, PgDatabase>> databasesCache = new HashMap<>();
    private final Map<String, Map<String, Map<String, DBTable>>> tablesCache = new HashMap<>();
    private final Map<String, Map<String, View>> viewsCache = new HashMap<>();
    
    public static synchronized PgDatabasesCache getInstance(){
        if(pCache == null){
            pCache = new PgDatabasesCache();
        }
        return pCache;
    }
   
    public PgDatabase syncDatabase(String databaseName, String params, PostgresDBService service, boolean force) throws SQLException {
        PgDatabase database = null;
        Map<String, PgDatabase> databaseMap = databasesCache.getOrDefault(params, new LinkedHashMap<>());
        Map<String,Map<String,Object>> loadedDB = service.loadDBStructure();
        
         for(Database d : service.getParam().getDatabases()){
            if(d.getName().equalsIgnoreCase(databaseName)){ database = (PgDatabase) d; }
            
            databaseMap.put(databaseName.toLowerCase(), (PgDatabase) d);
            Map<String, Map<String, DBTable>> databaseTableMap = tablesCache.getOrDefault(params, new LinkedHashMap<>());
            // get the required object files from the loaded map
            Map<String, Object> dbStruc = loadedDB.get(d.getName().toLowerCase());
            // retrieve individual records
            databaseTableMap.put(d.getName().toLowerCase(), (Map<String, DBTable>) dbStruc.get(DBService.COLUMN_NAME__TABLES));
            
            tablesCache.put(params, databaseTableMap);
         }         
         
         databasesCache.put(params, databaseMap);
        
        return database;
    }
    
    public PgDatabase syncDatabase(String databaseName, String params, PostgresDBService service) throws SQLException {
        return syncDatabase(databaseName, params, service, true);
    }
    
    public PgDatabase getDatabase(String params, String databaseName) {
        Map<String, PgDatabase> m = databasesCache.get(params);
        return m != null ? m.get(databaseName.toLowerCase()) : null;
    }
    
}
