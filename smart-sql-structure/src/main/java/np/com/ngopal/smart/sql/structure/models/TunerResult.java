/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class TunerResult implements FilterableData {
    
    @Override
        public String[] getValuesFoFilter() {
            return new String[] {status.toString(), message};
        }
        
        public enum Status {
            UNKNOWN(""),
            GOOD("OK"),
            NEUTRAL("--"),
            BAD("!!");
            
            private String str;
            Status(String str) {
                this.str = str;
            }

            @Override
            public String toString() {
                return str;
            }
        }
        
        private Status status;
        private String message;
        private String name;
        private String value;
        private String details;
        
        private String group;
        
        private String index;
        
        public TunerResult(String group) {
            this.group = group;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }
        
        public TunerResult(Status status, String message) {
            this.status = status;
            this.message = message;
        }
        
        public TunerResult(Status status, String group, String name, String value, String message, String details) {
            this.status = status;
            this.group = group;
            this.name = name;
            this.value = value;
            this.message = message;
            this.details = details;
        }
        
        public TunerResult(Status status, String name, String value, String message, String details) {
            this.status = status;
            this.name = name;
            this.value = value;
            this.message = message;
            this.details = details;
        }
        
        public TunerResult(Status status, String message, String details) {
            this.status = status;
            this.message = message;
            this.details = details;
        }
        
        public Status getStatus() {
            return status;
        }

        public String getGroup() {
            return group;
        }        
        
        public String getMessage() {
            return message;
        }

        public String getName() {
            return name == null ? group : name;
        }

        public String getValue() {
            return value;
        }

        public String getDetails() {
            return details;
        }
}
