package np.com.ngopal.smart.sql.structure.controller;

import com.google.common.base.Objects;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class OsDetailsController extends BaseController implements Initializable {
   
    ConnectionParamService db;

    @FXML
    private AnchorPane mainUI;
 
    @FXML
    private TextField osHost;
    @FXML
    private TextField osPort;
    @FXML
    private TextField osUser;
    @FXML
    private PasswordField osPassword;

    @FXML
    private CheckBox osSavePasswordCheckBox;

    @FXML
    private CheckBox osUsePasswordCheckbox;

    @FXML
    private CheckBox osUsePrivateKeyCheckbox;

    @FXML
    private TextField osPrivateKeyField;

    @FXML
    private Button osOpenPrivateKeyButton;

    @FXML
    private TextField osDiskSizeField;
    @FXML
    private TextField osRamSizeField;

    @Getter
    private ConnectionParams selectedConnParam;
    
    @FXML
    public void saveBttnAction(ActionEvent event) {
        selectedConnParam = db.save(selectedConnParam);
        getStage().close();
    }
    
    @FXML
    void cancelBttnAction(ActionEvent event) {
        selectedConnParam = null;
        getStage().close();
    }
    
    @FXML
    void openPrivateKey(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PuTTY Private Key Files", "*.ppk"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            osPrivateKeyField.setText(file.getAbsolutePath());
        }
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
                        
        osSavePasswordCheckBox.setSelected(true);
        propertyListeners();

        osPrivateKeyField.disableProperty().bind(osUsePrivateKeyCheckbox.selectedProperty().not());
        osOpenPrivateKeyButton.disableProperty().bind(osUsePrivateKeyCheckbox.selectedProperty().not());

        osPassword.disableProperty().bind(osUsePasswordCheckbox.selectedProperty().not());

        mainUI.sceneProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                if (newValue != null && newValue instanceof Scene) {
                    mainUI.sceneProperty().removeListener(this);
                    ((Scene) newValue).windowProperty().addListener(this);
                } else if (newValue != null && newValue instanceof Window) {
                    ((Window) newValue).getScene().windowProperty().removeListener(this);
                    ((Window) newValue).setOnShowing(new EventHandler<WindowEvent>() {
                        @Override
                        public void handle(WindowEvent event) {
                            double constant = 0.0;
                            double stageWidtht = ((Stage) newValue).getOwner().getWidth();
                            double stageHeight = ((Stage) newValue).getOwner().getHeight();

                            if (stageWidtht > mainUI.getPrefWidth()) {
                                constant = Math.abs((stageWidtht - mainUI.getPrefWidth()) / 2);
                                ((Stage) newValue).setX(((Stage) newValue).getOwner().getX() + constant);
                            } else {
                                ((Stage) newValue).setX(((Stage) newValue).getOwner().getX());
                            }

                            if (stageHeight > mainUI.getPrefHeight()) {
                                constant = Math.abs((stageHeight - mainUI.getPrefHeight()) / 2);
                                ((Stage) newValue).setY(((Stage) newValue).getOwner().getY() + constant);
                            } else {
                                ((Stage) newValue).setY(((Stage) newValue).getOwner().getY());
                            }
                        }
                    });
                }
            }
        });
    }


    private void updateFields(ConnectionParams params) {

        // OS part
        osHost.setText(params != null ? params.getOsHost() : "");
        osPort.setText(params != null ? params.getOsPort() + "" : "");
        osUser.setText(params != null ? params.getOsUser() : "");
        osPassword.setText(params != null ? params.getOsPassword() : "");
        osUsePasswordCheckbox.setSelected(params != null && params.isOsUsePassword());
        osUsePrivateKeyCheckbox.setSelected(params != null && !params.isOsUsePassword());
        osPrivateKeyField.setText(params != null ? params.getOsPrivateKey() : "");

        osRamSizeField.setText(params != null ? params.getOsMetricRamSize() : "");
        osDiskSizeField.setText(params != null ? params.getOsMetricDiskSize() : "");       

    }


    public void propertyListeners() {

        osHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsHost(), newValue)) {
                        selectedConnParam.setOsHost(newValue);
                    }
                });

        osPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (newValue.matches("\\d*")) {
                        try {
                            int port = Integer.parseInt(newValue);
                            if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsPort(), port)) {
                                selectedConnParam.setOsPort(port);
                            }
                        } catch (NumberFormatException ex) {
                            if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsPort(), 0)) {
                                selectedConnParam.setOsPort(0);
                            }
                        }
                    } else {
                        osPort.setText(oldValue);
                    }
                });

        osUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsUser(), newValue)) {
                        selectedConnParam.setOsUser(newValue);
                    }
                });

        osDiskSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsMetricDiskSize(), newValue)) {
                        selectedConnParam.setOsMetricDiskSize(newValue);
                    }
                });

        osRamSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsMetricRamSize(), newValue)) {
                        selectedConnParam.setOsMetricRamSize(newValue);
                    }
                });

        osPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsPassword(), newValue)) {
                        selectedConnParam.setOsPassword(newValue);
                    }
                });

        osUsePasswordCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePrivateKeyCheckbox.setSelected(false);
                }
                if (selectedConnParam != null && newValue != null) {
                    selectedConnParam.setOsUsePassword(newValue);
                }
            }
        });

        osUsePrivateKeyCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePasswordCheckbox.setSelected(false);
                }
            }
        });

        osPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam != null && !Objects.equal(selectedConnParam.getOsPrivateKey(), newValue)) {
                        selectedConnParam.setOsPrivateKey(newValue);
                    }
                });
    }

    private Stage dialog;
    
    public void init(Window win, ConnectionParamService db, ConnectionParams params){
        
        this.selectedConnParam = params;
        this.db = db;
        
        updateFields(params);
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("OS Details");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();
        
    }

    @Override
    public Stage getStage() {
        return dialog;
    }

}
