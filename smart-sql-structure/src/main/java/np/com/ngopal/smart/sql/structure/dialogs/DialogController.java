/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.dialogs;

import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public abstract class DialogController extends BaseController implements Initializable {

    public void calculateHeightFlow(Label titleLabel, double maxWidth) {

        String[] lines = titleLabel.getText().split("\n");

        double width = maxWidth - titleLabel.getPadding().getLeft() - titleLabel.getPadding().getRight();

        double height = 0.0;
        for (String line : lines) {
            double w = computeTextWidth(titleLabel.getFont(), line);
            height += ((double) (w / width) + 1) * computeFontHeightWidth(titleLabel.getFont(), line);
        }

        titleLabel.setMinHeight(height + 10.0);
        titleLabel.setPrefHeight(height + 10.0);
        titleLabel.setPrefWidth(width);
    }

    private double computeTextWidth(Font font, String text) {
        Text helper = new Text();
        helper.setFont(font);
        helper.setText(text);
        // Note that the wrapping width needs to be set to zero before
        // getting the text's real preferred width.
        helper.setWrappingWidth(0);
        helper.setLineSpacing(0);
        double w = Math.min(helper.prefWidth(-1), 0.0);
        helper.setWrappingWidth((int) Math.ceil(w));
        double textWidth = Math.ceil(helper.getLayoutBounds().getWidth());
        return textWidth;
    }

    private double computeFontHeightWidth(Font font, String text) {
        Text helper = new Text();
        helper.setFont(font);
        helper.setText(text);
        // Note that the wrapping width needs to be set to zero before
        // getting the text's real preferred width.
        helper.setWrappingWidth(0);
        helper.setLineSpacing(0);
        double w = Math.min(helper.prefWidth(-1), 0.0);
        helper.setWrappingWidth((int) Math.ceil(w));
        double textHeight = Math.ceil(helper.getLayoutBounds().getHeight());
        return textHeight;
    }

    public final void onShowAnimationWorks() {
//        if (controller.getPreferenceService().getPreference().isQbEnableAnimation()) {
//            getStage().setOnShowing(new EventHandler<WindowEvent>() {
//                @Override
//                public void handle(WindowEvent event) {
//                    RotateTransition rt = new RotateTransition(Duration.millis(250), getUI());
//                    rt.setByAngle(2);
//                    rt.setCycleCount(4);
//                    rt.setAutoReverse(true);
//                    rt.play();
//                }
//            });
//        }
    }

    public abstract DialogResponce getResponce();
    
    public abstract String getInputValue();
    
    public abstract void initDialog(Object... params);

    
    
}
