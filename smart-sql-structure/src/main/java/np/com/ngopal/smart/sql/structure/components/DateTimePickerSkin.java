package np.com.ngopal.smart.sql.structure.components;

import com.sun.javafx.scene.control.skin.DatePickerContent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */

public class DateTimePickerSkin extends DatePickerSkin {

    
    private final DateTimePicker dateTimePicker;
    private DatePickerContent datePickerContent;

    
    private Slider hours;
    private Slider minutes;
    private Slider seconds;
    private Label value;
    private Button acceptButton;
    private Button todayButton;
    private Button cancelButton;
    
    private final ChangeListener<? super Number> hourChangeListener;
    private final ChangeListener<? super Number> minChangeListener;
    private final ChangeListener<? super Number> secChangeListener;
    private final EventHandler<? super MouseEvent> releasedMouseEventHandler;
    
    
    public DateTimePickerSkin(final DateTimePicker datePicker)
    {
        super(datePicker);
        
        
        this.dateTimePicker = datePicker;
        
        
        hourChangeListener = (ChangeListener<Number>)(final ObservableValue<? extends Number> observable, final Number oldValue, final Number newValue) ->
        {
            LocalTime time = DateTimePicker.defaultNotNullLocalTime(dateTimePicker.getTimeValue());
            
            // update time value on slider changing
            updateTime(time.withHour(newValue.intValue()));
        };
        
        minChangeListener = (ChangeListener<Number>)(final ObservableValue<? extends Number> observable, final Number oldValue, final Number newValue) ->
        {
            LocalTime time = DateTimePicker.defaultNotNullLocalTime(dateTimePicker.getTimeValue());
            
            // update time value on slider changing
            updateTime(time.withMinute(newValue.intValue()));
        };
        
        secChangeListener = (ChangeListener<Number>)(final ObservableValue<? extends Number> observable, final Number oldValue, final Number newValue) ->
        {
            LocalTime time = DateTimePicker.defaultNotNullLocalTime(dateTimePicker.getTimeValue());
            
            // update time value on slider changing
            updateTime(time.withSecond(newValue.intValue()));
        };
        
        releasedMouseEventHandler = (final MouseEvent event) ->
        {
            LocalDate dateValue = dateTimePicker.getDateValue();
            if (dateValue == null)
            {
                dateValue = LocalDate.now(ZoneId.systemDefault());
            }
            
            dateTimePicker.dateTimeValueProperty().set(LocalDateTime.of(dateValue, DateTimePicker.defaultNotNullLocalTime(dateTimePicker.getTimeValue())));
        };
    }
    
    

    @Override 
    public Node getPopupContent()
    {
        if (datePickerContent == null)
        {
            //
            // get current time value
            //
            LocalTime time = dateTimePicker.getTimeValue();
            
            
            //
            // remember content (for mark first time creation)
            //
            datePickerContent = (DatePickerContent)super.getPopupContent();
            datePickerContent.getStyleClass().add("DateTimePicker-popup");
                        
            //
            // Create time picker content
            //
            HBox timePicker = new HBox();
            timePicker.getStyleClass().add("timePicker");
            
            value = new Label();
            
            value.getStyleClass().add("value");
            
            timePicker.getChildren().add(value);
            timePicker.setAlignment(Pos.CENTER);
            
            
            VBox controls = new VBox();
            controls.getStyleClass().add("controls");
            controls.setMaxWidth(Double.MAX_VALUE);
            
            hours = new Slider(0, 23, (time != null ? time.getHour() : 0));
            hours.getStyleClass().add("hours");
            controls.getChildren().add(hours);
            
            minutes = new Slider(0, 59, (time != null ? time.getMinute() : 0));
            minutes.getStyleClass().add("minutes");
            controls.getChildren().add(minutes);
            
            if (dateTimePicker.isShowSeconds()) 
            {
                seconds = new Slider(0, 59, (time != null ? time.getSecond() : 0));
                seconds.getStyleClass().add("seconds");
                controls.getChildren().add(seconds);
            }

            
            timePicker.getChildren().add(controls);
            HBox.setHgrow(controls, Priority.ALWAYS);
            
            
            //
            // add components
            //
            datePickerContent.getChildren().addAll(timePicker);

            hours.setOnMouseReleased(releasedMouseEventHandler);
            hours.valueProperty().addListener(hourChangeListener);
            
            minutes.setOnMouseReleased(releasedMouseEventHandler);
            minutes.valueProperty().addListener(minChangeListener);
            
            if (dateTimePicker.isShowSeconds()) 
            {
                seconds.setOnMouseReleased(releasedMouseEventHandler);
                seconds.valueProperty().addListener(secChangeListener);
            }
            
            
            // forse update time value
            updateTime(time);
            
            
            dateTimePicker.setOnShowing((final Event event) -> {
                //
                // on DateTimePicker popup showing - update time value
                //
                LocalDateTime dateTime = dateTimePicker.getDateTimeValue();
                if (dateTime == null)
                {
                    dateTime = LocalDateTime.now(ZoneId.systemDefault());
                }
                
                hours.setValue(dateTime.getHour());
                minutes.setValue(dateTime.getMinute());
                
                if (dateTimePicker.isShowSeconds())
                {
                    seconds.setValue(dateTime.getSecond());
                }
            });
            
            if (dateTimePicker.isShowButtons()) {

                acceptButton = new Button("Accept");
                acceptButton.getStyleClass().add("acceptButton");
                acceptButton.setOnAction((ActionEvent event) -> {
                    dateTimePicker.accept();
                });

                todayButton = new Button("Today");
                todayButton.getStyleClass().add("todayButton");
                todayButton.setOnAction((ActionEvent event) -> {
                    dateTimePicker.today();
                });

                cancelButton = new Button("Cancel");
                cancelButton.getStyleClass().add("cancelButton");
                cancelButton.setOnAction((ActionEvent event) -> {
                    dateTimePicker.cancel();
                    hide();
                });


                HBox buttons = new HBox(acceptButton, todayButton, cancelButton);
                buttons.getStyleClass().add("buttons");


                datePickerContent.getChildren().add(buttons);
            }
        }
        
        
        return datePickerContent;
    }

    
    
    
    private void updateTime(final LocalTime time)
    {
        // firstly update timve value
        dateTimePicker.setTimeValue(time);
        
        
        // then update label value
        if (value != null)
        {
            if (time != null)
            {                
                value.setText(DateTimeFormatter.ofPattern(dateTimePicker.isShowSeconds() ? "HH:mm:ss" : "HH:mm").format(time));
            }
            else
            {
                value.setText(dateTimePicker.isShowSeconds() ? "00:00:00" : "00:00");
            }
        }
    }

    
    
    
    void updateValueDisplay()
    {
        //
        // workaround for update textfield value
        //
        handleControlPropertyChanged("VALUE");
    }
    
    
}