/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terh Laweh(rumorsapp@gmail.com);
 */
public class FileSortingSQLQueryData implements FilterableData {

    private SimpleStringProperty query;
    private SimpleStringProperty db;
    private SimpleStringProperty exec_count;
    private SimpleStringProperty total_latency;
    private SimpleStringProperty sort_merge_passes;
    private SimpleStringProperty avg_sort_merges;
    private SimpleStringProperty sorts_using_scans;
    private SimpleStringProperty sort_using_range;
    private SimpleStringProperty rows_sorted;
    private SimpleStringProperty avg_rows_sorted;
    private SimpleStringProperty first_seen;
    private SimpleStringProperty last_seen;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public FileSortingSQLQueryData(String query, String db, String exec_count, String total_latency, String sort_merge_passes,
            String avg_sort_merges, String sorts_using_scans, String sort_using_range, String rows_sorted, String avg_rows_sorted,
            String first_seen, String last_seen) {

        this.query = new SimpleStringProperty(query);
        this.db = new SimpleStringProperty(db);
        this.exec_count = new SimpleStringProperty(exec_count);
        this.total_latency = new SimpleStringProperty( convert(Long.valueOf(total_latency) / 1000000000l) );
        this.sort_merge_passes = new SimpleStringProperty(sort_merge_passes);
        this.avg_sort_merges = new SimpleStringProperty(avg_sort_merges);
        this.sorts_using_scans = new SimpleStringProperty(sorts_using_scans);
        this.sort_using_range = new SimpleStringProperty(sort_using_range);
        this.rows_sorted = new SimpleStringProperty(rows_sorted);
        this.avg_rows_sorted = new SimpleStringProperty(avg_rows_sorted);
        this.first_seen = new SimpleStringProperty(first_seen);
        this.last_seen = new SimpleStringProperty(last_seen);
    }

    @Override
    public String[] getValuesFoFilter() {
        return new String[]{
            getQuery(), getDb(), getExec_count(), getTotal_latency(), getSort_merge_passes(), getAvg_sort_merges(), getSorts_using_scans(),
            getSort_using_range(), getRows_sorted(), getAvg_rows_sorted(), getFirst_seen(), getLast_seen()
        };
    }

    public String getQuery() {
        return query.getValue();
    }

    public String getDb() {
        return db.getValue();
    }

    public String getExec_count() {
        return exec_count.getValue();
    }

    public String getTotal_latency() {
        return total_latency.getValue();
    }

    public String getSort_merge_passes() {
        return sort_merge_passes.getValue();
    }

    public String getAvg_sort_merges() {
        return avg_sort_merges.getValue();
    }

    public String getSorts_using_scans() {
        return sorts_using_scans.getValue();
    }

    public String getSort_using_range() {
        return sort_using_range.getValue();
    }

    public String getRows_sorted() {
        return rows_sorted.getValue();
    }

    public String getAvg_rows_sorted() {
        return avg_rows_sorted.getValue();
    }

    public String getFirst_seen() {
        return first_seen.getValue();
    }

    public String getLast_seen() {
        return last_seen.getValue();
    }
    
    private String convert(Long val){
        //val is in milliseconds
        TimeUnit unit = TimeUnit.MILLISECONDS;
        if(unit.toHours(val) > 0){
            dateFormat.applyPattern("HH 'h'");
        }else if(unit.toMinutes(val) > 0){
            dateFormat.applyPattern("mm 'm'");
        }else if(unit.toSeconds(val) > 0){
            dateFormat.applyPattern("ss 's'");
        }else {
            dateFormat.applyPattern("SSS 'ms'");
        }        
        return dateFormat.format(new Date(val));
    }
}
