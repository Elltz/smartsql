/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.util.ArrayList;
import javafx.application.Platform;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class SynchorniseWorkProccessor extends Thread {

    private volatile boolean running = true;    
    private volatile boolean currentlyBusy = false;
    public volatile boolean isMain = false;
    private static long nextPulse = 0l;
    public static volatile boolean runConstantCodeLogics;
    private static long startTime = 0l;
        
    @Override
    public void run() {
        createChildrenProcessors();
        while (running) { 
            runConstantWorkProcessors();            
            WorkProc workProc = getWorkProc();            
            if (workProc == null) {         
//                System.out.println(this + " SLEEP");
                try { Recycler.lockThreadWithLocker(); }catch(InterruptedException ie){}                
//                System.out.println(this + " UP");
                continue;
            }

            currentlyBusy = true;

            try { 
//                System.out.println("RUN " + workProc);
                workProc.run(); 
//                System.out.println("END " + workProc);
            } 
            catch (Exception e) {
                if(e instanceof InterruptedException){
                    currentlyBusy = false;
                    continue;
                }
                e.printStackTrace();
            }

            if (!workProc.callUpdate) { currentlyBusy = false; continue; }

            Platform.runLater(()->  workProc.updateUI() );
            
            currentlyBusy = false;
        }
    }
    
    public boolean wakeUp(){
        if(!currentlyBusy && getState() == Thread.State.TIMED_WAITING){
            interrupt();
            return true;
        }
        return false;
    }

    @Override
    public void destroy() {
        running = false;
        wakeUp();
    }

    public SynchorniseWorkProccessor(){
        setDaemon(true);
        Recycler.initSWP(this);
    }
    
    
    private final ArrayList<WorkProc> specialSWPwork = new ArrayList(2);
    
    public void addWorkProc(WorkProc myWorkProc) {
        synchronized (specialSWPwork) {
            specialSWPwork.add(myWorkProc);
        }
        System.out.println("wake up called with reurn " +wakeUp());
    }
    
    private WorkProc getWorkProc() {
        if (specialSWPwork == null || specialSWPwork.isEmpty()) {
            return Recycler.getWorkProc();
        } else {
            synchronized (specialSWPwork) {
                WorkProc wp = specialSWPwork.remove(0);
                return wp;
            }
        }
    }
    
    public void notifyMainSynWorkProc4Restoration() {
        if (isMain) {
            runConstantCodeLogics = true;
            nextPulse = 0;
            specialSWPwork.clear();
        }
    }
    
    private void createChildrenProcessors(){
        if(isMain){
            for(int i =0; i < 2; i++){
                new SynchorniseWorkProccessor().start();
            }
        }
    }
    
    private void runConstantWorkProcessors() {
        if (isMain) {
            runConstantCodeLogics = specialSWPwork.isEmpty() && (System.currentTimeMillis() - nextPulse) 
                    >= Recycler.BACKGROUND_WAITING_TIME;

            if (runConstantCodeLogics) {
                nextPulse = System.currentTimeMillis() + Recycler.BACKGROUND_WAITING_TIME;
                specialSWPwork.addAll(ConstantWorkProcedures.cWorkloads);
                runConstantCodeLogics = false;
            }

        }
    }
    
    public void makeMain(){
        isMain = true;        
    }
    
    public boolean isCurrentlyBusy(){
        return currentlyBusy;
    }
}
