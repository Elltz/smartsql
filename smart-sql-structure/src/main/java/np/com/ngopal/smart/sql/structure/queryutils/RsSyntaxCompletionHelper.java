/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.queryutils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.db.HistoryQueryService;
import np.com.ngopal.smart.sql.db.HistoryTemplateService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Favourites;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.CompletionCellRenderer;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import np.com.ngopal.smart.sql.structure.utils.*;


/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 *
 * This class is there to give api's to help manage completions in RsSyntax
 */
@Slf4j
public class RsSyntaxCompletionHelper {

    private CustomCompletionProvider provider;
    private static RsSyntaxCompletionHelper completionHelper;
    private HashMap<ConnectionParams, ArrayList<Completion>> loadedCache = new HashMap();
    
    private HashMap<ConnectionParams, Set<String>> completionAddedDatabases = new HashMap<>();
    
    @Setter
    private FavouritesService favoriteService;
    
    @Setter
    private HistoryQueryService historyQueryService;
    
    @Setter
    private HistoryTemplateService historyTemplateService;
    
    @Setter
    private PreferenceDataService preferenceDataService;
        
    private final List<SmartSQLCompletion> favorites = new ArrayList<>();
    
    @Getter
    private Long connectionId;

    public static final Icon databaseIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/database_tab.png"));
    public static final Icon tableIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/table_tab.png"));
    public static final Icon viewIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/view_tab.png"));
    public static final Icon columnIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/column_tab.png"));
    public static final Icon functionIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/function_tab.png"));
    public static final Icon triggerIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/trigger_tab.png"));
    public static final Icon procedureIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/storedprocs_tab.png"));
    public static final Icon keyIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/keyword_tab.png"));
    public static final Icon showVariableIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/show_variable.png"));
    public static final Icon showStatusIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/show_status.png"));
    public static final Icon emgineIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/engine.png"));
    public static final Icon favoriteIcon = new ImageIcon(RSyntaxTextAreaBuilder.class.getResource("/np/com/ngopal/smart/sql/ui/images/connection/organize_favorite_tab.png"));
    
    public static final RsSyntaxCompletionHelper getInstance() {
        return getInstance(null, null, null, null);
    }
    
    public static final RsSyntaxCompletionHelper getInstance(            
            FavouritesService favoriteService,
            HistoryQueryService historyQueryService,
            HistoryTemplateService historyTemplateService,
            PreferenceDataService preferenceDataService) {
        
        if (completionHelper == null) {
            completionHelper = new RsSyntaxCompletionHelper();
            completionHelper.setFavoriteService(favoriteService);
            completionHelper.setHistoryQueryService(historyQueryService);
            completionHelper.setHistoryTemplateService(historyTemplateService);
            completionHelper.setPreferenceDataService(preferenceDataService);            
        }
        
        return completionHelper;
    }

    private RsSyntaxCompletionHelper() {
        provider = new CustomCompletionProvider();
        
        provider.setListCellRenderer(new CompletionCellRenderer() {
            {
                setShowTypes(true);
                setForeground(Color.decode("#3c71b3"));
                setBackground(Color.decode("#e1ebf9"));
            }

            @Override
            public void setSize(int width, int height) {
                super.setSize(width, 20);
            }

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
            }

            @Override
            public Component getListCellRendererComponent(JList list, Object value,
                    int index, boolean selected, boolean hasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, selected, hasFocus);
                if (value instanceof SmartSQLDatabaseReferenceCompletion) {
                    StringBuilder sb = new StringBuilder("<html><nobr>");
                    sb.append(((SmartSQLDatabaseReferenceCompletion) value).getName());
                    ((JLabel) c).setText(sb.toString());
                } else if (value instanceof SmartSQLCompletion) {
                    StringBuilder sb = new StringBuilder("<html><nobr>");
                    sb.append(((SmartSQLCompletion) value).getName());
                    ((JLabel) c).setText(sb.toString());
                }

                return c;
            }
        });
        provider.setAutoActivationRules(true, ".");
    }
    
    public void updateProviders() {
        provider.setFavouritesService(favoriteService);
        provider.setHistoryQueryService(historyQueryService);
        provider.setHistoryTemplateService(historyTemplateService);
    }
    
    public synchronized void refreshFavorites() {
        for (SmartSQLCompletion sc: favorites) {
            provider.removeCompletion(sc);
        }
        
        favorites.clear();
        List<Favourites> listFavor = favoriteService.getAll();
        if (listFavor != null) {
                      
            for (Favourites f: listFavor) {
                if (f.getQuery() != null) {
                                        
                    SmartSQLCompletion sc = new SmartSQLCompletion(provider, f.getQuery(), f.getName(), favoriteIcon);
                    
                    favorites.add(sc);
                    provider.addCompletion(sc);
                }
            }
        }    
    }
    
    public synchronized int addCompletions(ConnectionParams cp, String s) {
        Set<String> databases = completionAddedDatabases.get(cp);
        if (databases == null) {
            databases = new HashSet<>();
            completionAddedDatabases.put(cp, databases);
        }
        
        refreshFavorites();    
        
        if (!databases.contains(s)) {
            databases.add(s);
            
            List<Completion> list = getCompletionsForDatabase(cp, s);
            provider.addCompletions(list);
            return list.size();
        }
        
        return 0;
    }
    
    public synchronized int setCompletions(ConnectionParams cp, String s) {
        provider.clear();
        
        Set<String> databases = completionAddedDatabases.get(cp);
        if (databases != null) {
            databases.clear();
        }
        
        return addCompletions(cp, s);
    }
    
    public synchronized int rebuildTags(ConnectionParams params, Collection<QueryAreaWrapper> list) {
        loadedCache.remove(params);
        loadCompletionList(params);
        
        int length = setCompletions(params, params.getSelectedDatabase() != null ? params.getSelectedDatabase().getName() : null);
        
        if (list != null) {
            for (QueryAreaWrapper q: list) {
                RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea) q.getWrappedObject());
                if (builder != null) {
                    builder.preparedAliasCompletion();
                }
            }
        }
        
        return length;
    }

    public synchronized void loadCompletionList(ConnectionParams cp) {
        boolean equals = false;
        for (ConnectionParams cps : loadedCache.keySet()) {
            if (cps.equals(cp) && loadedCache.get(cps) != null) {
                equals = !loadedCache.get(cps).isEmpty();
                break;
            }
        }

        if (!equals) {
            loadCompletionListImpl(cp);
        }
    }
    
    public synchronized void updateCompletionsListForViews(ConnectionParams cp, Database database) {
        ArrayList<Completion> completionList = loadedCache.putIfAbsent(cp, new ArrayList());
        if (completionList == null) {
            completionList = loadedCache.get(cp);
        }
        
        List<Completion> forRemove = new ArrayList<>();
        // remove old views for database
        for (Completion c: completionList) {
            if (c instanceof SmartSQLDatabaseReferenceCompletion) {
                SmartSQLDatabaseReferenceCompletion s = (SmartSQLDatabaseReferenceCompletion)c;
                if (s.getDependedObject() instanceof View && s.getReferenceDatabase().equals(database != null ? database.getName() : "")) {
                    forRemove.add(c);
                    provider.removeCompletion(c);
                }
            }
        }
        
        completionList.removeAll(forRemove);       
        
        // add new views
        for (View view: (List<View>)(List)DatabaseCache.getInstance().getViewsKeys(cp.cacheKey())) {
            SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, view.toString(), view, view.getDatabase().getName(), viewIcon);
            //b.setRelevance(4);
            if (!completionList.contains(b)) {
                completionList.add(b);
                provider.addCompletion(b);
            }

            b = new SmartSQLDatabaseReferenceCompletion(provider, view.getDatabase().getName() + "." + view.getName(), view, view.getDatabase().getName(), viewIcon);
            //b.setRelevance(3);
            if (!completionList.contains(b)) {
                completionList.add(b);
                provider.addCompletion(b);
            }
        }
        
    }
    
    public synchronized void updateCompletionsListForFunctions(ConnectionParams cp, Database database) {
        ArrayList<Completion> completionList = loadedCache.putIfAbsent(cp, new ArrayList());
        if (completionList == null) {
            completionList = loadedCache.get(cp);
        }
        
        List<Completion> forRemove = new ArrayList<>();
        // remove old functions for database
        for (Completion c: completionList) {
            if (c instanceof SmartSQLDatabaseReferenceCompletion) {
                SmartSQLDatabaseReferenceCompletion s = (SmartSQLDatabaseReferenceCompletion)c;
                if (s.getDependedObject() instanceof Function && s.getReferenceDatabase().equals(database != null ? database.getName() : "")) {
                    forRemove.add(c);
                    provider.removeCompletion(c);
                }
            }
        }
        
        completionList.removeAll(forRemove);       
        
        if (database != null && preferenceDataService.getPreference().isQbStoredProcedures()) {
            
            for (Function s: (List<Function>)(List)DatabaseCache.getInstance().getFunctionsKeys(cp.cacheKey())) {
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, ((Function)s).getDatabase().getName(), functionIcon);
                //b.setRelevance(1);
                if (!completionList.contains(b)) {
                    completionList.add(b);
                    provider.addCompletion(b);
                }
            }
        }  
    }
    
    public synchronized void updateCompletionsListForTriggers(ConnectionParams cp, Database database) {
        ArrayList<Completion> completionList = loadedCache.putIfAbsent(cp, new ArrayList());
        if (completionList == null) {
            completionList = loadedCache.get(cp);
        }
        
        List<Completion> forRemove = new ArrayList<>();
        // remove old functions for database
        for (Completion c: completionList) {
            if (c instanceof SmartSQLDatabaseReferenceCompletion) {
                SmartSQLDatabaseReferenceCompletion s = (SmartSQLDatabaseReferenceCompletion)c;
                if (s.getDependedObject() instanceof Trigger && s.getReferenceDatabase().equals(database != null ? database.getName() : "")) {
                    forRemove.add(c);
                    provider.removeCompletion(c);
                }
            }
        }
        
        completionList.removeAll(forRemove);       
        
        if (database != null && preferenceDataService.getPreference().isQbStoredProcedures()) {

            for (Trigger s: (List<Trigger>)(List)DatabaseCache.getInstance().getTriggersKeys(cp.cacheKey())) {
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, ((Trigger)s).getDatabase().getName(), triggerIcon);
                //b.setRelevance(1);
                if (!completionList.contains(b)) {
                    completionList.add(b);
                    provider.addCompletion(b);
                }
            }
        }  
    }
    
    public synchronized void updateCompletionsListForProcedures(ConnectionParams cp, Database database) {
        ArrayList<Completion> completionList = loadedCache.putIfAbsent(cp, new ArrayList());
        if (completionList == null) {
            completionList = loadedCache.get(cp);
        }
        
        List<Completion> forRemove = new ArrayList<>();
        // remove old functions for database
        for (Completion c: completionList) {
            if (c instanceof SmartSQLDatabaseReferenceCompletion) {
                SmartSQLDatabaseReferenceCompletion s = (SmartSQLDatabaseReferenceCompletion)c;
                if (s.getDependedObject() instanceof StoredProc && s.getReferenceDatabase().equals(database != null ? database.getName() : "")) {
                    forRemove.add(c);
                    provider.removeCompletion(c);
                }
            }
        }
        
        completionList.removeAll(forRemove);       
        
        if (database != null && preferenceDataService.getPreference().isQbStoredProcedures()) {

            for (StoredProc s: (List<StoredProc>)(List)DatabaseCache.getInstance().getProceduresKeys(cp.cacheKey())) {
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, ((StoredProc)s).getDatabase().getName(), procedureIcon);
                //b.setRelevance(1);
                if (!completionList.contains(b)) {
                    completionList.add(b);
                    provider.addCompletion(b);
                }
            }
        }  
    }

    private void loadCompletionListImpl(ConnectionParams cp) {
        ArrayList<Completion> completionList = loadedCache.putIfAbsent(cp, new ArrayList());
        if (completionList == null) {
            completionList = loadedCache.get(cp);
        }
        
        List<Completion> completionSet = new ArrayList<>();
        if (preferenceDataService.getPreference().isQbTablesViews()) {
            
//            int size = DatabaseCache.getInstance().getTablesKeys(cp).size();
            for (Object s : DatabaseCache.getInstance().getTablesKeys(cp.cacheKey())) {
                
                DBTable table = (DBTable) s;
                
                // add simple name
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, table.getDatabase().getName(), tableIcon);
                //b.setRelevance(4);
                completionSet.add(b);                
                
                // add name with database "."                 
                b = new SmartSQLDatabaseReferenceCompletion(provider, table.getDatabase().getName() + "." + table.getName(), s, table.getDatabase().getName(), tableIcon);
                //b.setRelevance(3);
                completionSet.add(b);
            }

            for (Object s : DatabaseCache.getInstance().getViewsKeys(cp.cacheKey())) {
                View view = (View) s;
                
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, view.getDatabase().getName(), viewIcon);
                //b.setRelevance(4);
                completionSet.add(b);
                
                b = new SmartSQLDatabaseReferenceCompletion(provider, view.getDatabase().getName() + "." + view.getName(), s, view.getDatabase().getName(), viewIcon);
                //b.setRelevance(3);
                completionSet.add(b);
            }
        }
        
         if (preferenceDataService.getPreference().isQbColumns()) {

            for (Object s : DatabaseCache.getInstance().getColumnKeys(cp.cacheKey())) {
                Column col = (Column) s;
                
                String databaseName = col.getView() == null ? col.getTable().getDatabase().getName(): col.getView().getDatabase().getName();
                String tableName = col.getView() == null ? col.getTable().getName() : col.getView().getName();
                
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, col.getName(), col, databaseName, columnIcon);
                //b.setRelevance(4);
                completionSet.add(b);
                
                b = new SmartSQLDatabaseReferenceCompletion(provider, tableName + "." + col.getName(), col, databaseName, columnIcon);
                //b.setRelevance(2);
                completionSet.add(b);
            }

        }

        if (preferenceDataService.getPreference().isQbVariables()) {

            for (String s : (List<String>) (List) DatabaseCache.getInstance().getKeywordsKeys()) {
                BasicCompletion b = new BasicCompletion(provider, s) {;
                    @Override
                    public boolean equals(Object obj) {
                        if (obj instanceof SmartSQLDatabaseReferenceCompletion) {
                            
                            SmartSQLDatabaseReferenceCompletion sc = (SmartSQLDatabaseReferenceCompletion) obj;
                            return Objects.equals(sc.getCurrentText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                            
                        } else if (obj instanceof BasicCompletion) {
                            
                            BasicCompletion sc = (BasicCompletion) obj;
                            return Objects.equals(sc.getReplacementText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                        }

                        return false;
                    }
                    
                    @Override
                    public int hashCode() {
                        int hash = 3;
                        hash = 37 * hash + Objects.hashCode(getReplacementText());
                        hash = 73 * hash + Objects.hashCode(getIcon());
                        return hash;
                    }
                };
                b.setIcon(keyIcon);
                //b.setRelevance(5);
                completionSet.add(b);
            }

            for (String s : (List<String>) (List) DatabaseCache.getInstance().getServerFunctionsKeys()) {
                BasicCompletion b = new BasicCompletion(provider, s) {;
                    @Override
                    public boolean equals(Object obj) {
                        if (obj instanceof SmartSQLDatabaseReferenceCompletion) {
                            
                            SmartSQLDatabaseReferenceCompletion sc = (SmartSQLDatabaseReferenceCompletion) obj;
                            return Objects.equals(sc.getCurrentText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                            
                        } else if (obj instanceof BasicCompletion) {
                            
                            BasicCompletion sc = (BasicCompletion) obj;
                            return Objects.equals(sc.getReplacementText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                        }

                        return false;
                    }
                    
                    @Override
                    public int hashCode() {
                        int hash = 3;
                        hash = 37 * hash + Objects.hashCode(getReplacementText());
                        hash = 73 * hash + Objects.hashCode(getIcon());
                        return hash;
                    }
                };
                b.setIcon(keyIcon);
                //b.setRelevance(0);
                completionSet.add(b);
            }

            for (String s : DatabaseCache.getInstance().getShowStatus(cp.cacheKey())) {
                BasicCompletion b = new BasicCompletion(provider, s) {;
                    @Override
                    public boolean equals(Object obj) {
                        if (obj instanceof SmartSQLDatabaseReferenceCompletion) {
                            
                            SmartSQLDatabaseReferenceCompletion sc = (SmartSQLDatabaseReferenceCompletion) obj;
                            return Objects.equals(sc.getCurrentText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                            
                        } else if (obj instanceof BasicCompletion) {
                            
                            BasicCompletion sc = (BasicCompletion) obj;
                            return Objects.equals(sc.getReplacementText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                        }

                        return false;
                    }
                    
                    @Override
                    public int hashCode() {
                        int hash = 3;
                        hash = 37 * hash + Objects.hashCode(getReplacementText());
                        hash = 73 * hash + Objects.hashCode(getIcon());
                        return hash;
                    }
                };
                b.setIcon(showStatusIcon);
                //b.setRelevance(0);
                completionSet.add(b);
            }

            for (String s : DatabaseCache.getInstance().getShowVariables(cp.cacheKey())) {
                BasicCompletion b = new BasicCompletion(provider, s) {;
                    @Override
                    public boolean equals(Object obj) {
                        if (obj instanceof SmartSQLDatabaseReferenceCompletion) {
                            
                            SmartSQLDatabaseReferenceCompletion sc = (SmartSQLDatabaseReferenceCompletion) obj;
                            return Objects.equals(sc.getCurrentText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                            
                        } else if (obj instanceof BasicCompletion) {
                            
                            BasicCompletion sc = (BasicCompletion) obj;
                            return Objects.equals(sc.getReplacementText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                        }

                        return false;
                    }
                                        
                    @Override
                    public int hashCode() {
                        int hash = 3;
                        hash = 37 * hash + Objects.hashCode(getReplacementText());
                        hash = 73 * hash + Objects.hashCode(getIcon());
                        return hash;
                    }
                };
                b.setIcon(showVariableIcon);
                //b.setRelevance(0);
                completionSet.add(b);
            }

        }
        
        for (String s : (List<String>) (List) DatabaseCache.getInstance().getEnginesKeys(cp.cacheKey())) {
            BasicCompletion b = new BasicCompletion(provider, s) {;
                @Override
                public boolean equals(Object obj) {
                    if (obj instanceof SmartSQLDatabaseReferenceCompletion) {

                        SmartSQLDatabaseReferenceCompletion sc = (SmartSQLDatabaseReferenceCompletion) obj;
                        return Objects.equals(sc.getCurrentText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());

                    } else if (obj instanceof BasicCompletion) {

                        BasicCompletion sc = (BasicCompletion) obj;
                        return Objects.equals(sc.getReplacementText(), getReplacementText()) && Objects.equals(sc.getIcon(), getIcon());
                    }

                    return false;
                }

                @Override
                public int hashCode() {
                    int hash = 3;
                    hash = 37 * hash + Objects.hashCode(getReplacementText());
                    hash = 73 * hash + Objects.hashCode(getIcon());
                    return hash;
                }
            };
            b.setIcon(emgineIcon);
            //b.setRelevance(0);
            completionSet.add(b);
        }

        if (preferenceDataService.getPreference().isQbDatabase()) {

            List<Database> databases = cp.getDatabases();//DatabaseCache.getInstance().getDatabases(cp);
            if (databases == null || databases.isEmpty()) {
                databases = cp.getDatabases();
            }
            
            if (databases != null) {
                for (Object s : databases) {
                    BasicCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, s.toString(), databaseIcon);
                    //b.setRelevance(5);
                    completionSet.add(b);
                }
            }
        }
        
        if (preferenceDataService.getPreference().isQbStoredProcedures()) {

            for (Object s : DatabaseCache.getInstance().getFunctionsKeys(cp.cacheKey())) {
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, ((Function)s).getDatabase().getName(), functionIcon);
                //b.setRelevance(1);
                completionSet.add(b);
            }

            for (Object s : DatabaseCache.getInstance().getTriggersKeys(cp.cacheKey())) {
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, ((Trigger)s).getDatabase().getName(), triggerIcon);
                //b.setRelevance(1);
                completionSet.add(b);
            }

            for (Object s : DatabaseCache.getInstance().getProceduresKeys(cp.cacheKey())) {
                SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(provider, s.toString(), s, ((StoredProc)s).getDatabase().getName(), procedureIcon);
                //b.setRelevance(1);
                completionSet.add(b);
            }

        }


        completionList.addAll(completionSet);
        System.out.println("loaded completion list size " + completionList.size());
    }

    public CustomCompletionProvider getProvider() {
        return provider;
    }

    public List<Completion> getCompletionsForDatabase(ConnectionParams cp, String database) {        

        List<Completion> completion = new ArrayList<>();

        if (cp != null) {
            connectionId = cp.getId();
            if (database != null) {
                List<Completion> list = loadedCache.get(cp);
                if (list != null) {
                    for (Completion com: list) {
                        // check maybe it have reference to database
                        // then need check for current database
                        if(!(com instanceof SmartSQLDatabaseReferenceCompletion) || 
                            ((SmartSQLDatabaseReferenceCompletion)com).getReferenceDatabase().equalsIgnoreCase(database) || ((SmartSQLDatabaseReferenceCompletion)com).getDependedObject() instanceof Database) {
                            completion.add(com);
                        }
                    }
                }
            } else {
                List<Completion> list = loadedCache.get(cp);
                if (list != null) {
                    completion.addAll(list);
                }
            }
        }
        
//        System.gc();
        System.out.println("completion list size for Database " + database + " = " + completion.size());        
        return completion;
    }    
}
