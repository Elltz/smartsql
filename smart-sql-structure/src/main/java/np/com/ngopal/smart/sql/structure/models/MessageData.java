package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class MessageData {

    public static enum MessageType {
        ALL("All"),
        ERRORS("Queries with errors"),
        WARNINGS("Queries with warnings"),
        ERRORS_WARNINGS("Queries with errors/warnings"),
        RESULT_SET("Queries with result set"),
        WITHOUT_RESULT_SER("Queries without result set");
        
        private final String name;
        MessageType(String name) {
            this.name = name;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    private int queriesSize;
    private int successCount;
    private int errorsCount;
    private int warningsCount;
    
    private List<MessageDataItem> items = new ArrayList<>();
    
    public void addItem(String query, String message, double executionTime, double transferTime, double responseTime, boolean error, boolean warning, List<String> warningCodes, List<String> warningMessages, boolean resultSet, int customRange0, int customRange1) {
        
        MessageDataItem item = new MessageDataItem();
        item.setQuery(query);
        item.setMessage(message);
        item.setExecutionTime(executionTime);
        item.setTransferTime(transferTime);
        item.setResponseTime(responseTime);
        item.setError(error);
        item.setWarning(warning);
        item.setWarningCodes(warningCodes);
        item.setWarningMessages(warningMessages);
        item.setResultSet(resultSet);
        
        if (customRange0 > -1 && customRange1 > -1) {
            item.setCustomRange(new Integer[] {customRange0, customRange1});
        }
        
        items.add(item);
    }
}
