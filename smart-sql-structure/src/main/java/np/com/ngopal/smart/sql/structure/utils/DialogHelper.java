/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.BaseControllerProvider;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class DialogHelper {
    
    private static final int STAGE_ANIMATION_FLOW_TIME = 3;
    
    private static BaseControllerProvider<DialogController> getDialogControllerProvider(UIController uic){
        return new BaseControllerProvider<DialogController>(uic) {
            @Override
            protected String getPreface() {
                return "/np/com/ngopal/smart/sql/structure/ui/";
            }
        };
    }

    public static DialogResponce showInfo(UIController uic, String titleBar, String infoMessage, String detailMessage, Window owner) {
        DialogController controller = getDialogControllerProvider(uic).get("InfoDialog");
        controller.setController(uic);
        controller.initDialog(infoMessage, detailMessage);

        show(titleBar, controller.getUI(), owner);

        return controller.getResponce();
    }

    public static DialogResponce showInfo(UIController uic, String titleBar, String infoMessage, String detailMessage) {
        return showInfo( uic, titleBar, infoMessage, detailMessage, FunctionHelper.getApplicationMainStage());
    }

    public static DialogResponce showConfirm(UIController uic, String titleBar, String infoMessage, Window owner, boolean hasMultiButtons) {
        DialogController controller = getDialogControllerProvider(uic).get("ConfirmDialog");
        controller.setController(uic);
        controller.initDialog(infoMessage, hasMultiButtons);

        show(titleBar, controller.getUI(), owner);

        return controller.getResponce();
    }

    public static DialogResponce showConfirm(UIController uic, String titleBar, String infoMessage) {
        return showConfirm( uic, titleBar, infoMessage, FunctionHelper.getApplicationMainStage(), false);
    }

    public static String showInput(UIController uic, String titleBar, String message, String label, String initialValue, Window owner) {
        DialogController controller = getDialogControllerProvider(uic).get("InputDialog");
        controller.setController(uic);
        controller.initDialog(message, label, initialValue);

        show(titleBar, controller.getUI(), owner);

        if (controller.getResponce() == DialogResponce.OK_YES) {
            return controller.getInputValue();
        } else {
            return null;
        }
    }

    public static String showInput(UIController uic, String titleBar, String message, String label, String initialValue) {
        return showInput( uic, titleBar, message, label, initialValue, FunctionHelper.getApplicationMainStage());
    }

    public static String showPassword(UIController uic, String titleBar, String message, String label, String initialValue, Window owner) {
        DialogController controller = getDialogControllerProvider(uic).get("PasswordDialog");
        controller.setController(uic);
        controller.initDialog(message, label, initialValue);

        show(titleBar, controller.getUI(), owner);

        if (controller.getResponce() == DialogResponce.OK_YES) {
            return controller.getInputValue();
        } else {
            return null;
        }
    }

    public static String showPassword(UIController uic, String titleBar, String message, String label, String initialValue) {
        return showPassword( uic, titleBar, message, label, initialValue, Stage.impl_getWindows().next());
    }

    public static DialogResponce showError(UIController uic, String titleBar, String infoMessage, Throwable throwable, ConnectionParams params, Window owner) {
        if (throwable != null) {
            log.error(titleBar, throwable);
        } else {
            log.error(infoMessage);
        }
        DialogController controller = getDialogControllerProvider(uic).get("ErrorDialog");
        controller.setController(uic);
        controller.initDialog(infoMessage, params, throwable, throwable != null ? ExceptionUtils.getStackTrace(throwable) : null);

        show(titleBar, controller.getUI(), owner);

        return controller.getResponce();
    }

    public static DialogResponce showError(UIController uic, String titleBar, String infoMessage, Throwable throwable, Window owner) {
        return showError(uic, titleBar, infoMessage, null, null, owner);
    }
    
    public static DialogResponce showError(UIController uic, String titleBar, String infoMessage) {
        return showError(uic, titleBar, infoMessage, null);
    }
    
    public static DialogResponce showError(UIController uic, String titleBar, String infoMessage, Throwable throwable) {
        return showError(uic, titleBar, infoMessage, null, throwable);
    }
    
    public static DialogResponce showError(UIController uic, String titleBar, String infoMessage, ConnectionParams params, Throwable throwable) {
        return showError( uic, titleBar, infoMessage, throwable, params, FunctionHelper.getApplicationMainStage());
    }

    private static Stage createShowPopUpStage(String title, Parent ui, Window owner) {
        Scene scene = new Scene(ui);
        Stage stage = new Stage() {
            @Override
            public void close() {
                fireEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSE_REQUEST));
            }

            @Override
            public void showAndWait() {
                /**
                 * We call platform runlater here because we are trying to force
                 * the code in the runnable as a nested event for this Stage's
                 * series of nested event loops.
                 *
                 * This will help the stage show and block all calls after the
                 * showAndWait call but will run this runnable for animated
                 * effect.
                 */
                Platform.runLater(() -> {
                    Recycler.addWorkProc(new WorkProc() {
                        @Override
                        public void run() {
                            if (owner != null) {
                                //setY((((Stage)owner).getHeight() - getHeight()) / 2);
                                final double stageLeft = (((Stage)owner).getWidth() - getWidth()) / 2;
                                double middle = stageLeft, incrementor = stageLeft / 10;
                                try {
                                    while (middle > incrementor) {
                                        synchronized (this) {
                                            wait(STAGE_ANIMATION_FLOW_TIME);
                                        }
                                        Platform.runLater(() -> {
                                            setX(getX() + incrementor);
                                        });
                                        middle -= incrementor;
                                    }
                                    
                                    setY((owner.getY() + owner.getHeight()) / 2 - (getHeight() / 2));
                                } catch (Exception e) {
                                }
                            }
                        }
                    });
                });
                super.showAndWait();
            }
        };

        stage.setTitle(title);
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(owner);
        stage.setScene(scene);
        stage.toFront();
        stage.sizeToScene();
        stage.setResizable(false);
        if (owner != null) {
            stage.setX(((Stage)owner).getX());
            stage.setY((owner.getY() + owner.getHeight()) / 2);
        }

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            boolean closingAnimationRunning = false;

            @Override
            public void handle(WindowEvent event) {
                if (!closingAnimationRunning) {
                    event.consume();
                    closingAnimationRunning = true;
                    Recycler.addWorkProc(new WorkProc() {
                        @Override
                        public void run() {
                        if (owner != null) {
                            double end = ((Stage)owner).getX() + ((Stage)owner).getWidth(),
                                    remains = end - (stage.getX() + stage.getWidth()),
                                    incrementor = remains / 3;

                            try {
                                while (incrementor > 0 && stage.getX() > 0 && stage.getX() < end) {
                                    Platform.runLater(() -> {
                                        stage.setX(stage.getX() + incrementor);
                                    });
                                    synchronized (this) {
                                        wait(STAGE_ANIMATION_FLOW_TIME);
                                    }
                                }
                                Platform.runLater(() -> {
                                    stage.hide();
                                });
                            } catch (Exception e) {
                            }
                        } else {
                            Platform.runLater(() -> {
                                stage.hide();
                            });
                        }
                        }
                    });
                }
            }
        });

        return stage;
    }

    public static void show(String title, Parent ui, Window owner) {

        if (Platform.isFxApplicationThread()) {
            createShowPopUpStage(title, ui, owner).showAndWait();
        } else {
            CountDownLatch cdl = new CountDownLatch(1);
            Platform.runLater(() -> {
                createShowPopUpStage(title, ui, owner).showAndWait();
                cdl.countDown();
            });

            try {
                cdl.await();
            } catch (InterruptedException ex) {
            }
        }
    }

}
