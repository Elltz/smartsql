
package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportDataFragmentation implements FilterableData {
    
    private final SimpleStringProperty database;
    private final SimpleStringProperty engine;
    private final SimpleStringProperty tableName;
    private final SimpleObjectProperty totalSize;
    private final SimpleObjectProperty dataSize;
    private final SimpleStringProperty dataSizePercent;
    private final SimpleObjectProperty indexSize;
    private final SimpleStringProperty indexSizePercent;
    private final SimpleObjectProperty fragmentationSize;
    private final SimpleStringProperty fragmentationSizePercent;
    private final SimpleObjectProperty fragmentationRation;
    
    
 
    public ReportDataFragmentation(String database, String tableName, BigDecimal fragmentationRation, BigDecimal dataSize, BigDecimal indexSize, BigDecimal fragmentationSize, String engine) {
        this.database = new SimpleStringProperty(database);
        this.tableName = new SimpleStringProperty(tableName);
        this.fragmentationRation = new SimpleObjectProperty(fragmentationRation);
        this.dataSize = new SimpleObjectProperty(dataSize);
        this.indexSize = new SimpleObjectProperty(indexSize);
        this.fragmentationSize = new SimpleObjectProperty(fragmentationSize);
        this.engine = new SimpleStringProperty(engine);
        
        this.totalSize = new SimpleObjectProperty(dataSize.add(indexSize).add(fragmentationSize));
        
        this.dataSizePercent = new SimpleStringProperty();
        this.indexSizePercent = new SimpleStringProperty();
        this.fragmentationSizePercent = new SimpleStringProperty();
    }
 
    public String getDatabase() {
        return database.get();
    }
    public void setDatabase(String database) {
        this.database.set(database);
    }
 
    public String getTableName() {
        return tableName.get();
    }
    public void setTableName(String tableName) {
        this.tableName.set(tableName);
    }
    
    public void setFragmentationRation(BigDecimal fragmentationRation) {
        this.fragmentationRation.set(fragmentationRation);
    }
    public BigDecimal getFragmentationRation() {
        return (BigDecimal) fragmentationRation.get();
    }    
    
    public void setTotalSize(BigDecimal totalSize) {
        this.totalSize.set(totalSize);
    }
    public BigDecimal getTotalSize() {
        return (BigDecimal) totalSize.get();
    }
        
    public void setDataSize(BigDecimal dataSize) {
        this.dataSize.set(dataSize);
    }
    public BigDecimal getDataSize() {
        return (BigDecimal) dataSize.get();
    }
    
    public void setDataSizePercent(String dataSizePercent) {
        this.dataSizePercent.set(dataSizePercent);
    }
    public String getDataSizePercent() {
        return (String) dataSizePercent.get();
    }
    
    public void setIndexSize(BigDecimal indexSize) {
        this.indexSize.set(indexSize);
    }
    public BigDecimal getIndexSize() {
        return (BigDecimal) indexSize.get();
    }
    
    public void setIndexSizePercent(String indexSizePercent) {
        this.indexSizePercent.set(indexSizePercent);
    }
    public String getIndexSizePercent() {
        return (String) indexSizePercent.get();
    }
    
    public void setFragmentationSize(BigDecimal fragmentationSize) {
        this.fragmentationSize.set(fragmentationSize);
    }
    public BigDecimal getFragmentationSize() {
        return (BigDecimal) fragmentationSize.get();
    }
    
    public void setFragmentationSizePercent(String fragmentationSizePercent) {
        this.fragmentationSizePercent.set(fragmentationSizePercent);
    }
    public String getFragmentationSizePercent() {
        return (String) fragmentationSizePercent.get();
    }
    
    public String getEngine() {
        return engine.get();
    }
    public void setEngine(String engine) {
        this.engine.set(engine);
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getDatabase() != null ? getDatabase() : "", 
            getTableName() != null ? getTableName() : "", 
            getFragmentationRation() != null ? getFragmentationRation().toString() : "", 
            getDataSize() != null ? getDataSize().toString() : "", 
            getDataSizePercent() != null ? getDataSizePercent() : "", 
            getIndexSize() != null ? getIndexSize().toString() : "",             
            getIndexSizePercent() != null ? getIndexSizePercent() : "",             
            getFragmentationSize() != null ? getFragmentationSize().toString() : "", 
            getFragmentationSizePercent() != null ? getFragmentationSizePercent() : "", 
            getEngine() != null ? getEngine() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDatabase(),
            getTableName(),
            getFragmentationRation(),
            getDataSize(),
            getIndexSize(),            
            getFragmentationSize(),
            getDataSizePercent(),
            getIndexSizePercent(),            
            getFragmentationSizePercent(),
            getEngine()
        };
    }
}