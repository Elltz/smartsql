package np.com.ngopal.smart.sql.structure.queryutils;

import com.google.common.io.LineReader;
import com.sun.javafx.PlatformUtil;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import static np.com.ngopal.smart.sql.structure.queryutils.QueryUtils.*;

/**
 * This wrapper need to delete in future
 * no it used for compatibility of CodeArea and RSyntaxArea
 * 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
public class QueryAreaWrapper implements FindObject {

    private String uuid;
    private final Object area;
    
    public QueryAreaWrapper(String uuid, Object area, MenuItem[] items) {
        
        this.area = area;
        this.uuid = uuid;
        
        if (items != null) {
            changeMenuItemsSizes(Arrays.asList(items));
            ContextMenu contextMenu = new ContextMenu();
            for (MenuItem mi : items) {
                contextMenu.getItems().add(mi);
            }

            if (area instanceof RSyntaxTextArea) {                
                ((RSyntaxTextArea) area).addKeyListener(new KeyListener() {
                    
                    private KeyCombination.ModifierValue getModifierValue(boolean a){
                        return a ? KeyCombination.ModifierValue.DOWN : KeyCombination.ModifierValue.UP;
                    }
                    
                    private boolean keyCombinationMatch(KeyCodeCombination a, KeyCodeCombination b) {
                        return (a.getCode().compareTo(b.getCode()) == 0) && (a.getAlt().compareTo(b.getAlt()) == 0)
                                && (a.getShift().compareTo(b.getShift()) == 0) && (PlatformUtil.isMac()
                                ? (((a.getMeta().compareTo(b.getMeta()) == 0) || (a.getShortcut().compareTo(b.getShortcut()) == 0) 
                                || (a.getShortcut().compareTo(b.getMeta()) == 0) ) && (a.getControl().compareTo(b.getControl()) == 0))
                                : ((a.getControl().compareTo(b.getControl()) == 0) || (a.getShortcut().compareTo(b.getShortcut()) == 0)
                                || (a.getShortcut().compareTo(b.getControl()) == 0)));
                        // && ((a.getShortcut().compareTo(b.getShortcut()) == 0) no need check shortcut again
                    }

//                    RSyntaxTextAreaBuilder rstab = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder(((RSyntaxTextArea)area));
                    
                    @Override
                    public void keyTyped(java.awt.event.KeyEvent e) {
//                        if(Character.isLetter(e.getKeyChar())){                            
//                            rstab.autoCompleteReCalculator();
//                        }
                    }

//                    for future sake
//                    HashMap<Integer,String> keyMap = new HashMap();
//                    {
//                        keyMap.put(java.awt.event.KeyEvent.VK_ENTER, "Enter");
//                        keyMap.put(java.awt.event.KeyEvent.VK_DOWN, "Down");
//                        keyMap.put(java.awt.event.KeyEvent.VK_LEFT, "Left");
//                        keyMap.put(java.awt.event.KeyEvent.VK_RIGHT, "Right");
//                        keyMap.put(java.awt.event.KeyEvent.VK_UP, "Up");
//                    }
                    
                    @Override
                    public void keyPressed(java.awt.event.KeyEvent e) {
                        KeyCode code = KeyCode.getKeyCode(java.awt.event.KeyEvent.getKeyText(e.getKeyCode()));
                        //if(code == null){  code = KeyCode.getKeyCode( keyMap.get(e.getKeyCode()) ); }
                        if(code == null || code.isModifierKey() || (!e.isControlDown() && !e.isAltDown() && !e.isMetaDown())){
                            return;
                        }
                        
                        KeyCodeCombination kc = new KeyCodeCombination(code, getModifierValue(e.isShiftDown()),
                                getModifierValue(e.isControlDown()), getModifierValue(e.isAltDown()), getModifierValue(e.isMetaDown()), 
                                getModifierValue( PlatformUtil.isMac() ? e.isMetaDown() : e.isControlDown() ));
                        
                        for (MenuItem mi : contextMenu.getItems()) {
                            if (mi.getAccelerator() != null && keyCombinationMatch(kc, (KeyCodeCombination)mi.getAccelerator())) {
                                Platform.runLater(() -> {
                                    mi.fire();
                                });                                
                                e.consume();
                                break;
                            }
                        }
                    }

                    @Override
                    public void keyReleased(java.awt.event.KeyEvent e) { }
                });
                
                ((RSyntaxTextArea) area).addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {}

                    @Override
                    public void mousePressed(MouseEvent e) {
                        defaultRSyntaxAreaSelectionColor();
                        
                        //here is faster, but it does the same thing with mouseclicked
                        if (e.getButton() == MouseEvent.BUTTON3) {
                            Platform.runLater(()-> {
                                contextMenu.show(((RSyntaxTextAreaBuilder) getUserData()).getSwingNode(),
                                        e.getXOnScreen(), e.getYOnScreen());
                            });                                
                        }else if (e.getButton() == MouseEvent.BUTTON1) {
                            if (contextMenu.isShowing()) {
                                Platform.runLater(() -> {
                                    contextMenu.hide();
                                });
                            }
                        }
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                    }
                });
            } else {
//                ((CodeArea) area).setContextMenu(contextMenu);
            }
        }
    }
    
    public String getUuid() {
        return uuid;
    }
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public void commectSelection() {
        try {
            String text = getSelectedText();
            LineReader lr = new LineReader(new StringReader(text));
            StringBuilder buff = new StringBuilder();
            String line;
            while ((line = lr.readLine()) != null) {
                if (line != null) {
                    if (buff.length() > 0) {
                        buff.append("\n");
                    }
                    buff.append("--");
                    buff.append(line);
                }
            }
            
            if (buff.length() > 0) {
                replaceText(getSelectionStart(), getSelectionEnd(), buff.toString());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void uncommectSelection() {
        try {
            String text = getSelectedText();
            LineReader lr = new LineReader(new StringReader(text));
            StringBuilder buff = new StringBuilder();
            String line;
            while ((line = lr.readLine()) != null) {
                if (line != null) {
                    
                    if (buff.length() > 0) {
                        buff.append("\n");
                    }
                    
                    if (line.trim().startsWith("--")) {
                        buff.append(line.trim().substring(2));
                    }
                }
            }
            
            if (buff.length() > 0) {
                replaceText(getSelectionStart(), getSelectionEnd(), buff.toString());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void requestFocus() {
        Platform.runLater(() -> {
            RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder(((RSyntaxTextArea)area));
            if (builder != null) {
                builder.getSwingNode().requestFocus();
                SwingUtilities.invokeLater(() -> ((RSyntaxTextArea)area).requestFocus());
            }
        });
    }
    
    private void defaultRSyntaxAreaSelectionColor() {
        // This is restoring default selection color it must called on every mouse pressed
        ((RSyntaxTextArea)area).setSelectionColor(UIManager.getColor("TextArea.selectionBackground"));
    }
            
    public Object getWrappedObject() {
        return area;
    }
    
    
    public int hitPosition(double x, double y) {
        if (area instanceof RSyntaxTextArea) {
            RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea)area);
            return ((RSyntaxTextArea)area).getUI().viewToModel((RSyntaxTextArea)area, new Point((int)x + builder.getTextScrollPane().getHorizontalScrollBar().getValue(), (int)y + builder.getTextScrollPane().getVerticalScrollBar().getValue()));
        } else {
            return 0;//((CodeArea)area).hitPosition(x, y);
        }
    }
    
    public int getDragShiftX() {
        if (area instanceof RSyntaxTextArea) {
            RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea)area);
            return builder.getTextScrollPane().getGutter().getWidth();
        } else {
            return 0;//((CodeArea)area).hitPosition(x, y);
        }
    }
    
    public int getDragShiftY() {
        return 0;
    }
    
    public String getText() {
        return area instanceof RSyntaxTextArea ? ((RSyntaxTextArea)area).getText() : "";//((CodeArea)area).getText();
    }
    
    public int getLineByPosition(int position) {
        if (area instanceof RSyntaxTextArea) {
            try {
                return ((RSyntaxTextArea)area).getLineOfOffset(position);
            } catch (BadLocationException ex) {
            }
        } else {
//            int length = 0;
//            int line = -1;
//            for (Paragraph p: ((CodeArea)area).getParagraphs()) {
//                line++;                
//                length += p.length();
//                
//                if (length >= position) {
//                    return line;
//                }
//            }
        }
        
        
        return -1;
    }
    
    public Object getUserData() {
        return area instanceof RSyntaxTextArea ? RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder(((RSyntaxTextArea)area))
                : null;//((CodeArea)area).getUserData(); 
    }
    
    
    public int getCaretPosition(byte a) {
        if(a ==  CARET_POSITION_FROM_LINE){
            return area instanceof RSyntaxTextArea ? ((RSyntaxTextArea)area).getCaretOffsetFromLineStart() : -1;//((CodeArea)area).getCaretPosition();
        }else if(a == CARET_POSITION_FROM_START){
            //because RSyntaxTextArea has it i am force to create one for codeArea
            return area instanceof RSyntaxTextArea ? ((RSyntaxTextArea)area).getCaretPosition() : -1;
                    //((CodeArea)area).getText(0, ((CodeArea)area).getCurrentParagraph()).length() - ((CodeArea)area).getCaretPosition();
        }
        return -1;        
    }
    
    
    public void insertText(int position, String text) {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                if (((RSyntaxTextArea)area).getSelectedText() != null) {
                    ((RSyntaxTextArea)area).replaceSelection(text);
                } else {
                    ((RSyntaxTextArea)area).insert(text, position);
                }
            });
        } else {
            //((CodeArea)area).insertText(position, text);
        }
    }
    
    
    public void appendText(String text) {
        if (area instanceof RSyntaxTextArea) {
            ((RSyntaxTextArea)area).append(text);            
        } else {
            //((CodeArea)area).appendText(text);
        }
    }
    
    
    public void selectCustomRange(int from, int to, javafx.scene.paint.Color color) {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).setSelectionColor(new Color((int) (255 * color.getRed()), (int) (255 * color.getGreen()), (int) (255 * color.getBlue())));
                ((RSyntaxTextArea)area).select(from, to);
            });
        } else {
            //((CodeArea)area).selectCustomRange(from, to, color);
        }
    }
    
        
    public String getSelectedText() {
        if (area instanceof RSyntaxTextArea) {
            return ((RSyntaxTextArea)area).getSelectedText();
        } else {
            return "";//((CodeArea)area).getSelectedText();
        }
    }
    
    
    public void selectAll() {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).selectAll();
            });
        } else {
            //((CodeArea)area).selectAll();
        }
    }
    
    
    public void replaceText(int from, int to, String text) {
        if (area instanceof RSyntaxTextArea) {
            ((RSyntaxTextArea)area).replaceRange(text, from, to);
        } else {
//            if (Platform.isFxApplicationThread()) {
//                ((CodeArea)area).replaceText(from, to, text);
//            } else {
//                Platform.runLater(() -> ((CodeArea)area).replaceText(from, to, text));
//            }
        }
    }
    
    
    public int getSelectionStart() {
        if (area instanceof RSyntaxTextArea) {
            return ((RSyntaxTextArea)area).getSelectionStart();
        } else {
            return -1;//((CodeArea)area).getSelection().getStart();
        }
    }
    
    
    public int getSelectionEnd() {
        if (area instanceof RSyntaxTextArea) {
            return ((RSyntaxTextArea)area).getSelectionEnd();
        } else {
            return -1;//((CodeArea)area).getSelection().getEnd();
        }
    }
    
    
    public void positionCaret(int position) {
        if (area instanceof RSyntaxTextArea) {
            ((RSyntaxTextArea)area).setCaretPosition(position);
        } else {
            //((CodeArea)area).positionCaret(position);
        }
    }
    
    
    public void clear() {
        if (area instanceof RSyntaxTextArea) {
            ((RSyntaxTextArea)area).setText("");
        } else {
            //((CodeArea)area).clear();
        }
    }
    
    
    public void undo() {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).undoLastAction();
            });
        } else {
            //((CodeArea)area).undo();
        }
    }
    
    
    public void redo() {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).redoLastAction();
            });
        } else {
            //((CodeArea)area).redo();
        }
    }
    
    
    public void cut() {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).cut();
            });
        } else {
            //((CodeArea)area).cut();
        }
    }
        
    
    public void copy() {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).copy();
            });
        } else {
            //((CodeArea)area).copy();
        }
    }
    
    
    public void paste() {
        if (area instanceof RSyntaxTextArea) {
            SwingUtilities.invokeLater(() -> {
                ((RSyntaxTextArea)area).paste();
            });
        } else {
            //((CodeArea)area).paste();
        }
    }
    
//    public int rebuildTags(){
//        if (isRSyntaxTextArea()) {
//            return RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea)area).rebuildCompletions();
//        }
//        return 0;
//    }
    
    public void listAllTags() {
        if (area instanceof RSyntaxTextArea) {
            RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea)area);
            if (builder != null) {
                SwingUtilities.invokeLater(() -> {
                    builder.listAllTags();
                });
            }
        } else {
//            ((CodeArea)area).fireEvent(
//                new KeyEvent(KeyEvent.KEY_RELEASED, "", " ", KeyCode.SPACE, false, true, false, false));
        }
    }
    
    
    public void listMatchingTags() {
        if (area instanceof RSyntaxTextArea) {
            RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea)area);
            if (builder != null) {
                SwingUtilities.invokeLater(() -> {
                    builder.doCompletion();
                });
            }
        } else {
//            ((CodeArea)area).fireEvent(
//                new KeyEvent(KeyEvent.KEY_RELEASED, "", " ", KeyCode.SPACE, false, true, false, false));
        }
    }
    
    
    public int getLength() {
        if (area instanceof RSyntaxTextArea) {
            return ((RSyntaxTextArea)area).getText().length();
        } else {
            return -1;//((CodeArea)area).getLength();
        }
    }
    
    
    public void selectRange(int from, int to) {
        if (area instanceof RSyntaxTextArea) {
            ((RSyntaxTextArea)area).select(from, to);
        } else {
            //((CodeArea)area).selectRange(from, to);
        }
    }
    
    
    public void moveToRow(int row) {
        if (area instanceof RSyntaxTextArea) {
            
            Platform.runLater(() -> {
                ((RSyntaxTextAreaBuilder)getUserData()).getSwingNode().requestFocus();
            });
            
            SwingUtilities.invokeLater(() -> {
                
                ((RSyntaxTextArea)area).requestFocusInWindow();
                
                String[] lines = ((RSyntaxTextArea)area).getText().split("\n");
                int position = 0;
                for (int i = 0; i < row - 1; i++) {
                    if (i < lines.length) {
                        position += lines[i].length() + 1;
                    }
                }
                
                ((RSyntaxTextArea)area).setCaretPosition(position);
                
            });
            
        } else {
//            int position = 0;
//            for (int i = 0; i < row - 1; i++)
//            {
//                position += ((CodeArea)area).getParagraph(i).fullLength();
//            }
//            ((CodeArea)area).moveTo(position);
        }
    }
    
    public void refreshQueryArea(){
        ((RSyntaxTextAreaBuilder)getUserData()).refreshTextArea();
    }
    
    public boolean isRSyntaxTextArea(){
        return area instanceof RSyntaxTextArea;
    }
    
    public String getRelativePath() {
        if(area instanceof RSyntaxTextArea){
            return ((RSyntaxTextAreaBuilder)getUserData()).getRelativePath();
        }else{
            return null;//((CodeAreaWrapperClosingData)getUserData()).getRelativePath();
        }
    }

    public void setRelativePath(String relativePath) {
       if(area instanceof RSyntaxTextArea){
            ((RSyntaxTextAreaBuilder)getUserData()).setRelativePath(relativePath);
        }else{
//            ((CodeAreaWrapperClosingData)getUserData()).setRelativePath(relativePath);
        }
    }
    
    public boolean isReferencingAFile(){
        if(area instanceof RSyntaxTextArea){
            return ((RSyntaxTextAreaBuilder)getUserData()).isReferencingAFile();
        }else{
            return false;//((CodeAreaWrapperClosingData)getUserData()).isReferencingAFile();
        }
    }
    
    public static void changeMenuItemsSizes(List<MenuItem> items) {
        if (items != null) {
            // change image size to standart sizes
            for (MenuItem mi : items) {
                ImageView iv = (ImageView) mi.getGraphic();
                if (iv != null) {
                    iv.setFitHeight(16);
                    iv.setFitWidth(16);
                }

                if (mi instanceof Menu) {
                    changeMenuItemsSizes(((Menu) mi).getItems());
                }
            }
        }
    }
    
//    public void setCurrentlySelectedDatabase(String s){
//        if(isRSyntaxTextArea()){
//            RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder(((RSyntaxTextArea)area)).setCurrentDatabaseName(s);
//        }
//    }
}
