/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.executable;

import com.google.gson.Gson;
import com.sun.javafx.PlatformUtil;
import com.sun.javafx.application.PlatformImpl;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.structure.executable.ProgressIndicatorInterface;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.SmartsqlSavedData;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.lockservice.LockService;
import liquibase.lockservice.LockServiceFactory;
import liquibase.resource.ClassLoaderResourceAccessor;
import np.com.ngopal.smart.sql.model.provider.StorageProvider;
import np.com.ngopal.smart.sql.structure.controller.IntroductionScreenController;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class StandardApplicationManager {

    private static StandardApplicationManager applicationManager;

    private Map<String, Object> tempRuntimeMapData;
    private String serverVersion;
    private Gson gson = new Gson();
    SmartsqlSavedData smartsqlSavedData;
    private String[] currentApplicationArgs;
    private ServerSocket listenerSocket = null;
    private volatile boolean shutdownCalled;
    private CommunicationProtocol communicationProtocol;

    public static StandardApplicationManager getInstance() {
        if (applicationManager == null) {
            applicationManager = new StandardApplicationManager();
        }
        return applicationManager;
    }

    private StandardApplicationManager() {
    }

    public void createSmartDataDirectoryIfNotExist() {
        File f = getApplicationPrivateFilesAndStorageDirectory(null);
        if (f.exists()) {
            return;
        }
        f.mkdir();
    }

    public SmartsqlSavedData getSmartsqlSavedData() {
        if (smartsqlSavedData == null) {
            try {
                smartsqlSavedData = SmartsqlSavedData.getSmartsqlSavedDataInstance(gson,
                        new String(Files.readAllBytes(getSmartDataDirectory(DATA_FILE).toPath()),
                                Charset.forName("UTF-8")));
            } catch (Exception e) {
                smartsqlSavedData = new SmartsqlSavedData(gson);
            }

            // we need get online server data from app_info.config if no any in smart data
            // this maybe hack but at now - only this way
//            if (smartsqlSavedData.getOnlineServer() == null || smartsqlSavedData.getOnlineServer().trim().isEmpty()) {
//                smartsqlSavedData.se
//            }
        }

        return smartsqlSavedData;
    }

    public boolean hasValidUser() {
        return smartsqlSavedData.getFullName() != null && smartsqlSavedData.getFullName().length() > 0
                && smartsqlSavedData.getEmailId() != null && smartsqlSavedData.getEmailId().length() > 0;
    }

    public void writeSmartMysqlDataToFile() {
        getSmartsqlSavedData().makeDirty();
        try {
            Files.write(getSmartDataDirectory(DATA_FILE).toPath(), getSmartsqlSavedData().toString().getBytes("UTF-8"),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public File downloadConfigIfNeed() {
        File configFile = getSmartDataDirectory(FILE_INFO);
        if (configFile == null || !configFile.exists()) {
            hasUpdates();
            downloadRemoteConfig(serverVersion);
        }

        return configFile;
    }

    public boolean[] checkForApplicationUpdates() {
        boolean detectedUpdates = hasUpdates();
        downloadRemoteConfig(serverVersion);
        boolean appSecurityVoidCheck = !hasIntegrity();
        return new boolean[]{detectedUpdates, appSecurityVoidCheck};
    }

    public File getSmartDataDirectory(Byte subFile) {
        File parent = null;

        if (subFile == null) {
            parent = getApplicationPrivateFilesAndStorageDirectory(null);
        } else {
            switch (subFile) {

                case CORE_APPLICATION_DIR:
                    parent = getApplicationPrivateFilesAndStorageDirectory("core_application");
                    if (!parent.exists()) {
                        parent.mkdir();
                    }
                    break;

                case FRAGILE_DIR:
                    parent = getApplicationPrivateFilesAndStorageDirectory(getMD5String("Shudheer"));
                    if (!parent.exists()) {
                        parent.mkdir();
                    }
                    break;

                case IDENTIFICATION_FILE:
                    parent = new File(getSmartDataDirectory(FRAGILE_DIR), "identification");
                    break;

                case FILE_INFO:
                    parent = new File(getSmartDataDirectory(FRAGILE_DIR), SMARTSQL_FILE_INFO);
                    break;

                case LOG_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory(SMARTSQL_LOG_FILE);
                    break;

                case DATA_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory(SMARTSQL_SAVED_DATA_FILE);
                    break;

                case LOG_2_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory(SMARTSQL_LOG_2_FILE);
                    break;

                case YOUTUBE_LINK_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory(SMARTSQL_YOUTUBE_LINK_FILE);
                    break;

                case SPLASH_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory(SMARTSQL_SPLASH_INDEX);
                    break;
                    
                case LAST_UPDATE_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory("lastUpdate");
                    break;
                    
                case CLEAR_H2_SQ_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory("h2_" + StorageProvider.LAST_PQ_CLEAR_PATH);
                    break;
                    
                case CLEAR_MYSQL_SQ_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory("mysql_" + StorageProvider.LAST_PQ_CLEAR_PATH);
                    break;
                    
                case VALID_FILE:
                    parent = getApplicationPrivateFilesAndStorageDirectory("validation");
                    break;

                default:
                    break;
            }
        }

        return parent;
    }

    private File getApplicationPrivateFilesAndStorageDirectory(String subDirectoryOrFile) {
        File parent;
        StringBuilder sb = new StringBuilder("SmartMySQL")
                .append(File.separator)
                .append(getApplicationBinaryName());

        if (PlatformUtil.isWindows()) {
            parent = new File(System.getenv("AppData"), sb.toString());
        } else {
            parent = new File(System.getProperty("user.home"), "." + sb.toString());
        }

        if (subDirectoryOrFile != null && !subDirectoryOrFile.isEmpty()) {
            parent = new File(parent, subDirectoryOrFile);
        }
        return parent;
    }

    public File getApplicationPublicFilesAndStorageDirectory(String subDirectoryOrFile) {
        StringBuilder sb = new StringBuilder(
                System.getProperty("user.home"))
                .append(File.separator)
                .append("Documents")
                .append(File.separator)
                .append(getApplicationBinaryName());

        File finalFile = new File(sb.toString());

        if (subDirectoryOrFile != null && !subDirectoryOrFile.isEmpty()) {
            finalFile = new File(sb.toString(), subDirectoryOrFile);
        }

        return finalFile;
    }

    public final String getJarHash(File f) {
        StringBuilder sb = new StringBuilder();
        try (FileInputStream smart_ui_stream = new FileInputStream(f);) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] dataBytes = new byte[1024];

            int nread = 0;
            while ((nread = smart_ui_stream.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            };

            byte[] mdbytes = md.digest();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff)
                        + 0x100, 16).substring(1));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

    public String getMD5String(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes("UTF-8"));
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public File getAppCurrentDirectory() {
        return new File(System.getProperty("user.dir"));
    }

    public File getAppDependenciesDirectory() {
        return new File(getAppCurrentDirectory(), "lib");
    }

    public String[] getNameHashFromJarFile(File file, boolean stripDashFromName) {
        String name = file.getName().replace(".jar", "");

        if (stripDashFromName) {
            //strip the last dash to get rid of versioning
            int index = name.lastIndexOf('-');
            if (index > 0) {
                name = name.substring(0, index);
            }
        }

        return new String[]{name, getJarHash(file)};
    }

    private File getLocalizedUpdaterScriptLcation() {
        File source, dest = null;
        try {
            source = new File(getAppCurrentDirectory(), SMARTSQL_UPDATE_UTILITY_JAR_FILENAME + ".jar");
            dest = new File(System.getProperty("java.io.tmpdir"), getMD5String(SMARTSQL_UPDATE_UTILITY_JAR_FILENAME) + ".jar");
            Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dest;
    }

    public void callUpdaterScript() throws Exception {
        /**
         * The script will take in these arguments respectively
         *
         * 1: The path to the installer dir 2: The path the SmartSQL parent
         * folder
         *
         */
        StringBuilder commandScript = new StringBuilder();
        commandScript.append("java -jar ");
        commandScript.append("\"").append(getLocalizedUpdaterScriptLcation().getPath())
                .append("\" ");
        /*add arguments*/
        //source dir
        commandScript.append("\"").append(getSmartDataDirectory(CORE_APPLICATION_DIR).getPath()).append("\" ");
        //target dir
        commandScript.append("\"").append(getAppCurrentDirectory().getPath()).append("\" ");
        //after update execution program
        File[] execFiles = getAppCurrentDirectory().getParentFile().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                //currently for windows
                return /*pathname.isFile() && */ pathname.getName().startsWith("Smart") && pathname.getName().endsWith(".exe");
            }
        });

        if (PlatformUtil.isMac()) {
            commandScript.append("\"").append(getAppCurrentDirectory().getAbsolutePath().replace("/Contents/Java", "")).append("\" ");
        } else {
            if (execFiles != null && execFiles.length > 0) {
                commandScript.append("\"").append(execFiles[0].getPath()).append("\" ");
            } else {
                commandScript.append("null ");
            }
        }
        //log file path
        commandScript.append("\"").append(getSmartDataDirectory(LOG_2_FILE).getPath()).append("\" ");
        //last known server configuration file path
        commandScript.append("\"").append(getSmartDataDirectory(FILE_INFO).getPath()).append("\" ");
        //flag for deletion of source dir
        commandScript.append("true").append(" ");

        System.out.println(commandScript.toString());

        Process process = null;
        //execute
        if (PlatformUtil.isMac()) {
            //TODO need test on linux & mac
            // java -jar "/Users/user/NetBeansProjects/smartsql/smart-sql-dependency-updater/target/update_utility.jar" "/Users/user/.SmartMySQL/SmartMySQLQB_QA/core_application" "/Applications/SmartMySQLQB_QA.app/Contents/Java" null "/Users/user/.SmartMySQL/SmartMySQLQB_QA/updatelogger.log" "/Users/user/.SmartMySQL/SmartMySQLQB_QA/ef20ad25f2f859b5cf674158a06b4590/app_info.config" true
            process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "osascript -e \"do shell script \\\"" + commandScript.toString() + "\\\" with administrator privileges\""});
            //process.waitFor();
//            BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//            String line;
//            String errorResult = "";
//            while ((line = br.readLine()) != null) {
//                errorResult += line + "\n";
//            }
//            
//            BufferedReader br0 = new BufferedReader(new InputStreamReader(process.getInputStream()));
//            String inputResult = "";
//            while ((line = br0.readLine()) != null) {
//                inputResult += line + "\n";
//            }
//            
//            System.out.println(errorResult);
//            System.out.println(inputResult);

        } else if (PlatformUtil.isUnix()) {
            // java -jar "/tmp/9ceaf0b867d360c2b5eaf75900c41cc8.jar" "/home/osboxes/.SmartMySQL/SmartMySQLQB_QA/core_application" "/home/osboxes/SmartMySQL/app" null "/home/osboxes/.SmartMySQL/SmartMySQLQB_QA/updatelogger.log" "/home/osboxes/.SmartMySQL/SmartMySQLQB_QA/ef20ad25f2f859b5cf674158a06b4590/app_info.config" true
            process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", commandScript.toString()});

        } else if (PlatformUtil.isWindows()) {
            commandScript.insert(0, "cmd.exe /c start \""
                    + new File(getAppCurrentDirectory(), "smart-console.lnk").getPath()
                    + "\" ");
            process = Runtime.getRuntime().exec(commandScript.toString());
        }
    }

    public Map<String, String> getFliteredNeededRemoteDependencies() {
        HashMap<String, String> fliteredDependencies = new HashMap(100);
        if (tempRuntimeMapData != null) {
            org.slf4j.LoggerFactory.getLogger(getClass()).info("FILES SECURITY HASH CHECK");
            Map<String, String> localDependenciesHashes = getSmartsqlSavedData().getDependencyHashes();
            Map<String, String> remoteDependenciesLink = (Map<String, String>) tempRuntimeMapData.get("dependencyDirectory");
            Map<String, String> remoteDependenciesHashes = (Map<String, String>) tempRuntimeMapData.get("dependencyHashes");

            for (Map.Entry<String, String> serverEntry : remoteDependenciesHashes.entrySet()) {
                org.slf4j.LoggerFactory.getLogger(getClass()).info("server : " + serverEntry.getKey() + " - " + serverEntry.getValue());
                org.slf4j.LoggerFactory.getLogger(getClass()).info("local : " + localDependenciesHashes.get(serverEntry.getKey()));
                if (!serverEntry.getValue().equals(localDependenciesHashes.get(serverEntry.getKey()))) {
                    fliteredDependencies.put(serverEntry.getKey(), remoteDependenciesLink.get(serverEntry.getKey()));
                    org.slf4j.LoggerFactory.getLogger(getClass()).info("DIDNT MATCH - says hash wrong");
                }
            }

            org.slf4j.LoggerFactory.getLogger(getClass()).info("APP HASH");
            String remoteAppHash = (String) tempRuntimeMapData.get("appHash");
            org.slf4j.LoggerFactory.getLogger(getClass()).info("server : " + remoteAppHash);
            org.slf4j.LoggerFactory.getLogger(getClass()).info("local : " + getSmartsqlSavedData().getAppHash());
            if (!remoteAppHash.equals(getSmartsqlSavedData().getAppHash())) {
                fliteredDependencies.put("main_app", remoteDependenciesLink.get("main_app"));
                org.slf4j.LoggerFactory.getLogger(getClass()).info("APP HAS INVALID");
            }
        }
        return fliteredDependencies;
    }

    public void resetAppDependencyCaches() {
        getSmartsqlSavedData().getDependencyDirectories().clear();
        getSmartsqlSavedData().getDependencyHashes().clear();
        getSmartsqlSavedData().setAppHash(null);
        getSmartsqlSavedData().setAppSize(0l);
    }

    public void deriveAppDependencyHashesAndLocation() {
        File[] dependencies = getAppDependenciesDirectory().listFiles();
        insertIntoSmartsqlDependencyData(dependencies);

        FileFilter jarFiles = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".jar") && !pathname.getName().startsWith("main");
            }
        };
        dependencies = getAppCurrentDirectory().listFiles(jarFiles);
        insertIntoSmartsqlDependencyData(dependencies);
    }

    private void insertIntoSmartsqlDependencyData(File[] dependencies) {
        if (dependencies != null) {
            for (File libJarFile : dependencies) {
                String[] name_hash = getNameHashFromJarFile(libJarFile, true);
                getSmartsqlSavedData().getDependencyDirectories().put(name_hash[0], libJarFile.getPath());
                getSmartsqlSavedData().getDependencyHashes().put(name_hash[0], name_hash[1]);
            }
        }
    }

    public void deriveMainJarsHashes() {
        File mainJar = new File(getAppCurrentDirectory(), "main_app.jar");
        getSmartsqlSavedData().setAppHash(getNameHashFromJarFile(mainJar, true)[1]);
        getSmartsqlSavedData().setAppSize(mainJar.length());
    }

    public void streamDownloader(String url, File outfile, ProgressIndicatorInterface pii) throws Exception {
        //System.out.println("URL = " + url);
        URLConnection conn = new URL(url).openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setReadTimeout(120000);
        conn.addRequestProperty("content-type", "binary/data");

        // not sure that need
        new File(outfile.getParent()).mkdirs();

        try (InputStream in = conn.getInputStream();
                OutputStream out = new FileOutputStream(outfile);) {
            if (pii != null) {
                pii.onStart(null);
            }
            long size = in.available();
            byte[] b = new byte[4096];
            int count;
            long current = 0l;

            while ((count = in.read(b)) > 0) {
                out.write(b, 0, count);
                current += count;
                if (pii != null) {
                    pii.onUpdate(current, size);
                }
            }

            if (pii != null) {
                pii.onDone(outfile);
            }
        }
    }

    public String getCloudVersionBucketLink() {
        return "http://storage.googleapis.com/"
                + System.getProperty(Flags.SMARTSQL_PROJECT_BUILD_MODULE,
                        getSmartsqlSavedData().getAppProjectName())
                + "_versions/"
                + ("2".equals(System.getProperty(Flags.SMARTSQL_BUILD_TYPE, "1"))
                ? "version_qa.json" : "version_production.json");
    }

    public String getCloudDependencyConfigurationLink(String version) {
        return "http://storage.googleapis.com/"
                + System.getProperty(Flags.SMARTSQL_PROJECT_BUILD_MODULE,
                        getSmartsqlSavedData().getAppProjectName())
                + ("2".equals(System.getProperty(Flags.SMARTSQL_BUILD_TYPE, "1")) ? "_qa_" : "_production_")
                + version
                + "/"
                + Flags.SMARTSQL_FILE_INFO;
    }

    private Map<String, Object> jsonResponseStringToMap(String responseString) {
        return gson.fromJson(responseString, HashMap.class);
    }

    public boolean isMainAppInGoodState() {
        return getSmartsqlSavedData().getAppHash() != null
                && (tempRuntimeMapData == null
                || getSmartsqlSavedData().getAppHash().equalsIgnoreCase((String) tempRuntimeMapData.get("appHash")));
    }

    public boolean hasIntegrity() {
        resetAppDependencyCaches();
        deriveAppDependencyHashesAndLocation();
        deriveMainJarsHashes();
        return getFliteredNeededRemoteDependencies().isEmpty();
    }

    public String getAvailableServerVersion(Map<String, Object> result) {

        if (result != null) {
            List<String> versionsArray = (List<String>) result.get("versions_array");
            if (versionsArray != null) {
                for (int i = versionsArray.size() - 1; i > 0; i--) {
                    if (downloadRemoteConfig(versionsArray.get(i))) {
                        return versionsArray.get(i);
                    }
                }
            }
        }

        return null;
    }

    public boolean hasUpdates() {
        try {
            File tempFile = Files.createTempFile("version", ".json").toFile();
            streamDownloader(getCloudVersionBucketLink(), tempFile, null);
            String protocolJsonString = new String(Files.readAllBytes(tempFile.toPath()));
            Map<String, Object> result = jsonResponseStringToMap(protocolJsonString);
            tempFile.delete();

            serverVersion = getAvailableServerVersion(result);

            if (serverVersion != null) {
                String appVersion = getSmartsqlSavedData().getVersion();

                int appVersionMajor = 0;
                int appVersionMinor = 0;
                try {
                    String[] v = appVersion.split("\\.");
                    appVersionMajor = Integer.valueOf(v[0]);
                    appVersionMinor = Integer.valueOf(v[1]);
                } catch (Throwable e) {
                }

                int serverVersionMajor = 0;
                int serverVersionMinor = 0;
                try {
                    String[] v = serverVersion.split("\\.");
                    serverVersionMajor = Integer.valueOf(v[0]);
                    serverVersionMinor = Integer.valueOf(v[1]);
                } catch (Throwable e) {
                }

                return serverVersionMajor > appVersionMajor
                        || (serverVersionMajor == appVersionMajor && serverVersionMinor > appVersionMinor);
            }
        } catch (Exception e) {
            if (!(e instanceof UnknownHostException)) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean downloadRemoteConfig(String version) {
        try {
            File tempFile = getSmartDataDirectory(FILE_INFO);
            streamDownloader(getCloudDependencyConfigurationLink(version.replaceAll("\\.", "_")), tempFile, null);
            String protocolJsonString = new String(Files.readAllBytes(tempFile.toPath()));
            tempRuntimeMapData = jsonResponseStringToMap(protocolJsonString);
            //tempFile.delete();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * dont use this helper method in this(StandardApplicationManager) class use
     * this method only outside this class. use the declared field inside this
     * class.
     *
     * @return
     */
    public String getServerVersion() {
        return serverVersion == null ? "" : serverVersion;
    }

    public boolean hasUnattendedUpdates() {
        File coreAppDir = getSmartDataDirectory(Flags.CORE_APPLICATION_DIR);
        boolean coreApplicationDirHasChildren = coreAppDir.exists()
                && coreAppDir.list().length > 0;
        int numChildren = 0;
        int numDirChildren = 0;
        int numEmptyDirChildren = 0;

        if (coreApplicationDirHasChildren) {
            File[] files = coreAppDir.listFiles();
            numChildren = files.length;
            for (File f : files) {
                if (f.isDirectory()) {
                    numDirChildren++;
                    if (f.list().length == 0) {
                        numEmptyDirChildren++;
                    }
                }
            }
        }

        return numChildren > numDirChildren
                || numDirChildren > numEmptyDirChildren;
    }

    public void handleApplicatEntryArguments(String[] args) {
        try {
            File logFile = getSmartDataDirectory(LOG_FILE);
            logFile.getParentFile().mkdirs();

            Files.write(logFile.toPath(),
                    new StringBuilder(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()))
                    .append(System.lineSeparator())
                    .append(System.lineSeparator())
                    .toString().getBytes(),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);

            System.setProperty("logfile.name", logFile.getPath());
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (args != null && args.length != 0) {
            for (int i = 0; i < args.length; i++) {
                System.out.println(args[i]);
            }
            currentApplicationArgs = args;
        }

        /**
         * HERE WE PROCESS APPLICATION RECONIZEABLE PROPERTY
         */
        appCommunicationMechanism();

        // read online server 
        try {
            File configFile = StandardApplicationManager.getInstance().downloadConfigIfNeed();
            Map<String, Object> serverConfig = new Gson().fromJson(new FileReader(configFile), HashMap.class);
            if (serverConfig.containsKey(Flags.SMARTSQL_ONLINE_SERVER)) {
                System.setProperty(Flags.SMARTSQL_ONLINE_SERVER, (String) serverConfig.get(Flags.SMARTSQL_ONLINE_SERVER));
            }
            if (serverConfig.containsKey(Flags.SMART_SQL_APP_ATTR_ENCRYPTION)) {
                System.setProperty(Flags.SMART_SQL_APP_ATTR_ENCRYPTION, (String) serverConfig.get(Flags.SMART_SQL_APP_ATTR_ENCRYPTION));
            }

            if (serverConfig.containsKey(Flags.SMARTSQL_BANNER_TEXT)) {
                System.setProperty(Flags.SMARTSQL_BANNER_TEXT, (String) serverConfig.get(Flags.SMARTSQL_BANNER_TEXT));
            }
            if (serverConfig.containsKey(Flags.SMARTSQL_YOUTUBE_TEXT)) {
                System.setProperty(Flags.SMARTSQL_YOUTUBE_TEXT, (String) serverConfig.get(Flags.SMARTSQL_YOUTUBE_TEXT));
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(StandardApplicationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        getSmartsqlSavedData().updateProperties(System.getProperties());
        writeSmartMysqlDataToFile();
        try {
            System.getProperties().load(StandardApplicationManager.class.getResourceAsStream("/config.properties"));
            System.getProperties().list(new PrintStream(new File(System.getProperty("logfile.name"))));
        } catch (Exception e) { e.printStackTrace(); }
    }

    public void appCommunicationMechanism() {
        try {
            listenerSocket = new ServerSocket(SMART_SQL_APP_SOCKET_PORT_ID);
            Thread listenerSocketThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    if (listenerSocket != null) {
                        try {
                            while (!isShutdownCalled()) {
                                final Socket clientSocket = listenerSocket.accept();
                                if (getCommunicationProtocol() != null) {
                                    Recycler.addWorkProc(new WorkProc() {
                                        @Override
                                        public void run() {
                                            try {
                                                getCommunicationProtocol().onConnected();
                                                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                                                String line;
                                                while ((line = in.readLine()) != null) {
                                                    getCommunicationProtocol().parseContext(line);
                                                }
                                                getCommunicationProtocol().onComplete();
                                                in.close();
                                                clientSocket.close();
                                                System.gc();
                                            } catch (Exception ioe) {
                                                ioe.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }
                            listenerSocket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            listenerSocketThread.setDaemon(true);
            listenerSocketThread.start();
        } catch (IOException e) {            
            if (e instanceof BindException) {
                Logger.getLogger(StandardApplicationManager.class.getName()).log(Level.SEVERE,
                        "CLOSE ANY OTHER INSTANCE OF SmartMySQLQB.");
                //THIS MEANS APP IS ALREADY RUNNING
                try {
                    Socket requestSocket = new Socket("127.0.0.1", SMART_SQL_APP_SOCKET_PORT_ID);
                    PrintWriter out = new PrintWriter(requestSocket.getOutputStream(), true);
                    for (String args : getCurrentApplicationArguments()) {
                        out.println(args);
                    }
                    out.close();
                    requestSocket.close();
                } catch (IOException ie) {
                    ie.printStackTrace();
                }
                System.exit(0);
            }else{ e.printStackTrace(); }
        }
    }

    public void initializeDatabaseDriver(String databaseConnectionFile, String changeLogFilePath) {
        OutputStreamWriter out0 = new OutputStreamWriter(System.out, Charset.forName("UTF-8"));
        Writer liquibaseWriter = new BufferedWriter(out0);

        try {
            Class.forName("org.h2.Driver");
            Connection c = DriverManager.getConnection("jdbc:h2:~/" + databaseConnectionFile, "app", "app");
            Liquibase liquibase = null;
            try {
                Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
                liquibase = new Liquibase(changeLogFilePath, new ClassLoaderResourceAccessor(), database);
                liquibase.update("", liquibaseWriter);
                liquibase.update("");

                // TODO refactor this
                // if lock exist we must release it for now
                LockService lockService = LockServiceFactory.getInstance().getLockService(database);
                if (lockService.listLocks() != null && lockService.listLocks().length > 0) {
                    lockService.forceReleaseLock();
                }

                database.close();
            } finally {

                if (c != null) {
                    try {
                        if (!c.isClosed()) {
                            c.commit();
                            c.close();
                        }
                    } catch (SQLException e) {
                        //nothing to do
                    }
                }
            }
        } catch (ClassNotFoundException | SQLException | LiquibaseException ex) {
            ex.printStackTrace();
        }
    }

    public void showStaticPreLoader(Stage primaryStage) {

    }

    public String getApplicationBinaryName() {
        String templateAppName = System.getProperty("name");
        if (templateAppName == null || templateAppName.isEmpty()) {
            templateAppName = System.getProperty(Flags.SMARTSQL_PROJECT_BUILD_MODULE);
            templateAppName = templateAppName.substring(templateAppName.lastIndexOf('_') + 1).toUpperCase();
            if (templateAppName.equalsIgnoreCase("UI")) {
                templateAppName = "QB";
            }
            if ("2".equals(System.getProperty(Flags.SMARTSQL_BUILD_TYPE, "2"))) {
                templateAppName = templateAppName + "_QA";
            }
            templateAppName = "SmartMySQL" + templateAppName;
        }
        return templateAppName;
    }

    public IntroductionScreenController showInteractivePreLoader(Stage primaryStage) {
        IntroductionScreenController screenController = StandardBaseControllerProvider.getController("IntroductionScreen");
        screenController.setSplashText(getApplicationBinaryName());
        screenController.setBottomLoaderText("STARTING APPLICATION...");

        Scene scene = new Scene(screenController.getUI());
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setResizable(false);
        primaryStage.show();

        screenController.startSplashAnimation();

        return screenController;
    }

    public void decorateApplicationAsStandard() {
        Platform.runLater(() -> {
            try {
                Stage w = FunctionHelper.getApplicationMainStage();
                Scene scene = w.getScene();
                w.hide();
                PlatformImpl.setTaskbarApplication(true);
                Field styleField = Stage.class.getDeclaredFields()[6];
                styleField.setAccessible(true);
                styleField.set(w, StageStyle.DECORATED);
                ((Stage) w).setScene(scene);
                ((Stage) w).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public String[] getCurrentApplicationArguments() {
        return currentApplicationArgs == null ? new String[0] : currentApplicationArgs;
    }

    public synchronized boolean isShutdownCalled() {
        return shutdownCalled;
    }

    public synchronized void shutdown() {
        shutdownCalled = true;
        writeSmartMysqlDataToFile();
    }

    public synchronized void installCommunicationProtocol(CommunicationProtocol protocol) {
        this.communicationProtocol = protocol;
    }

    public synchronized CommunicationProtocol getCommunicationProtocol() {
        return communicationProtocol;
    }
}
