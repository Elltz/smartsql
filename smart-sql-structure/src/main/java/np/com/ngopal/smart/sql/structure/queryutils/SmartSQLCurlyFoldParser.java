package np.com.ngopal.smart.sql.structure.queryutils;

import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenTypes;
import org.fife.ui.rsyntaxtextarea.folding.CurlyFoldParser;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SmartSQLCurlyFoldParser extends CurlyFoldParser {

    @Override
    public boolean isLeftCurly(Token t) {        
        return t.getType() == TokenTypes.SEPARATOR && t.isSingleChar('(');
    }

    @Override
    public boolean isRightCurly(Token t) {        
        return t.getType() == TokenTypes.SEPARATOR && t.isSingleChar(')');
    }
    
}
