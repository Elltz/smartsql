package np.com.ngopal.smart.sql.structure.queryutils;

import java.util.Objects;
import javax.swing.Icon;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SmartSQLCompletion extends BasicCompletion {

    private final String replacementText;
    private final String name;
    
    public SmartSQLCompletion(CompletionProvider provider, String replacementText, String name, Icon icon){
        super(provider, name, null);
        
        this.name = name;
        this.replacementText = replacementText;        
        setIcon(icon);
    }    

    @Override
    public String getReplacementText() {
        return replacementText;
    }

    @Override
    public String getInputText() {
        return getName();
    }  

    public String getName() {
        return name;
    }

    @Override
    public String getCompareText() {
        return getName();
    }

    
    
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SmartSQLCompletion) {
            SmartSQLCompletion sc = (SmartSQLCompletion) obj;
            
            return Objects.equals(sc.getName(), getName()) && Objects.equals(sc.getIcon(), getIcon());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(getName());
        hash = 73 * hash + Objects.hashCode(getIcon());
        return hash;
    }
}
