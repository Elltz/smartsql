/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class MysqlProfTutorialController implements Initializable {

    @FXML
    private VBox mainUI;
    
    @FXML
    private HBox footer;
    
    @FXML
    private Button backButton;
    
    @FXML
    private Button nextButton;
    
    @FXML
    private StackPane flipperContainer;
    
    private SimpleIntegerProperty currentIndexPos = new SimpleIntegerProperty(0);
    private Stage stage;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        nextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                currentIndexPos.set(currentIndexPos.get()+ 1);
                if(currentIndexPos.get() == flipperContainer.getChildren().size() - 1){
                    nextButton.setDisable(true);
                }
                backButton.setDisable(false);
                hideAllExceptIndex();
            }
        });
        
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                currentIndexPos.set(currentIndexPos.get() - 1);
                if(currentIndexPos.get() == 0){
                    backButton.setDisable(true);
                }
                nextButton.setDisable(false);
                hideAllExceptIndex();
            }
        });
        
        backButton.setDisable(true);
        hideAllExceptIndex();
        
    }
    
    private void hideAllExceptIndex() {
        for (int i = 0; i < flipperContainer.getChildren().size(); i++) {
            flipperContainer.getChildren().get(i).setVisible(i == currentIndexPos.get());
        }
    }
    
    public void showAsPopUp(double x, double y, Window owner){
        if(stage == null){
            stage = new Stage(StageStyle.UTILITY);
            stage.setResizable(false);
            stage.setWidth(444);
            stage.setHeight(350);
            stage.initOwner(owner);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(mainUI));
            stage.setTitle("    MySQL PROFILING HELP");
        }
        stage.setX(x);
        stage.setY(y);
        Platform.runLater(()-> stage.show() );
    }
}
