
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleStringProperty;
import np.com.ngopal.smart.sql.model.DeadlockData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DeadlockMonitorQueryData implements FilterableData, DebugExplainProvider {
    
    private final SimpleStringProperty details;
    private final SimpleStringProperty status;
    private final SimpleStringProperty deadlockTime;
    private final SimpleStringProperty transaction1;
    private final SimpleStringProperty transaction2;
    private final SimpleStringProperty queryTuningTransaction1;
    private final SimpleStringProperty recommendationsTransaction1;    
    private final SimpleStringProperty queryTuningTransaction2;
    private final SimpleStringProperty recommendationsTransaction2;
    private final SimpleStringProperty database;
    private final SimpleStringProperty rollback;
    
    private Long rowsSum;
    
    public DeadlockMonitorQueryData(String details, String status, String deadlockTime, String transaction1, String transaction2, String queryTuningTransaction1, String recommendationsTransaction1, String queryTuningTransaction2, String recommendationsTransaction2, String database, String rollback) {
        this.details = new SimpleStringProperty(details);
        this.status = new SimpleStringProperty(status);
        this.deadlockTime = new SimpleStringProperty(deadlockTime);
        this.transaction1 = new SimpleStringProperty(transaction1);
        this.transaction2 = new SimpleStringProperty(transaction2);
        this.queryTuningTransaction1 = new SimpleStringProperty(queryTuningTransaction1);
        this.recommendationsTransaction1 = new SimpleStringProperty(recommendationsTransaction1);
        this.queryTuningTransaction2 = new SimpleStringProperty(queryTuningTransaction2);
        this.recommendationsTransaction2 = new SimpleStringProperty(recommendationsTransaction2);
        this.database = new SimpleStringProperty(database);
        this.rollback = new SimpleStringProperty(rollback);
    }
    
    public DeadlockMonitorQueryData(DeadlockData d) {
        this(d.getDetails(),
                d.getStatus(),
                d.getDeadlockTime(),
                d.getTransaction1(),
                d.getTransaction2(),
                d.getQueryTuningTransaction1(),
                d.getRecommendationsTransaction1(),
                d.getQueryTuningTransaction2(),
                d.getRecommendationsTransaction2(),
                d.getDatabase(),
                d.getRollback());
    }
    
    public String getRollback() {
        return rollback.get();
    }
    public void setRollback(String rollback) {
        this.rollback.set(rollback);
    }
    
    public String getDeadlockTime() {
        return deadlockTime.get();
    }
    public void setDeadlockTime(String deadlockTime) {
        this.deadlockTime.set(deadlockTime);
    }
        
    public String getDetails() {
        return details.get();
    }
    public void setDetails(String details) {
        this.details.set(details);
    }
    
    public String getStatus() {
        return status.get();
    }
    public void setStatus(String status) {
        this.status.set(status);
    }    
    
    public String getTransaction1() {
        return transaction1.get();
    }
    public void setTransaction1(String transaction1) {
        this.transaction1.set(transaction1);
    }
    
    public String getTransaction2() {
        return transaction2.get();
    }
    public void setTransaction2(String transaction2) {
        this.transaction2.set(transaction2);
    }    
    
    public String getQueryTuningTransaction1() {
        return queryTuningTransaction1.get();
    }
    public void setQueryTuningTransaction1(String queryTuningTransaction1) {
        this.queryTuningTransaction1.set(queryTuningTransaction1);
    }
    
    public String getRecommendationsTransaction1() {
        return recommendationsTransaction1.get();
    }
    public void setRecommendationsTransaction1(String recommendationsTransaction1) {
        this.recommendationsTransaction1.set(recommendationsTransaction1);
    }
    
    public String getQueryTuningTransaction2() {
        return queryTuningTransaction2.get();
    }
    public void setQueryTuningTransaction2(String queryTuningTransaction2) {
        this.queryTuningTransaction2.set(queryTuningTransaction2);
    }
    
    public String getRecommendationsTransaction2() {
        return recommendationsTransaction2.get();
    }
    public void setRecommendationsTransaction2(String recommendationsTransaction2) {
        this.recommendationsTransaction2.set(recommendationsTransaction2);
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {            
            getDetails() != null ? getDetails() : "",
            getStatus() != null ? getStatus() : "",
            getDeadlockTime() != null ? getDeadlockTime() : "",
            getTransaction1() != null ? getTransaction1() : "",
            getTransaction2() != null ? getTransaction2() : "",
            getQueryTuningTransaction1() != null ? getQueryTuningTransaction1() : "",
            getRecommendationsTransaction1() != null ? getRecommendationsTransaction1() : "",
            getQueryTuningTransaction2() != null ? getQueryTuningTransaction2() : "",
            getRecommendationsTransaction2() != null ? getRecommendationsTransaction2() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDetails(),
            getStatus(),
            getDeadlockTime(),
            getTransaction1(),
            getTransaction2(),
            getQueryTuningTransaction1(),
            getRecommendationsTransaction1(),
            getQueryTuningTransaction2(),
            getRecommendationsTransaction2()
        };
    }    

    public DeadlockData toDeadlockData(String paramsKey) {
        return new DeadlockData(
            paramsKey, 
            details.get(), 
            status.get(), 
            deadlockTime.get(), 
            transaction1.get(), 
            transaction2.get(), 
            queryTuningTransaction1.get(), 
            recommendationsTransaction1.get(), 
            queryTuningTransaction2.get(), 
            recommendationsTransaction2.get(), 
            database.get(), 
            rollback.get());
    }
    
    @Override
    public void setRowsSum(Long value) {
        this.rowsSum = value;
    }

    @Override
    public Long getRowsSum() {
        return rowsSum;
    }

    @Override
    public String getDatabase() {
        return database.get();
    }
}