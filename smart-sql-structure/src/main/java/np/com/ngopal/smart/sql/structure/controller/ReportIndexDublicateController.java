package np.com.ngopal.smart.sql.structure.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportIndexDublicateController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
    
    @FXML
    private ComboBox<String> databaseCombobox;
    
    @FXML
    private Button exportButton;
    
    public boolean disableListen = true;
    
    private ConnectionSession session;
    
    private PreferenceDataService preferenceService;
    
    @FXML
    public void refreshAction(ActionEvent event) {
        makeBusy(true);
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                if (session != null) {
                    String db = databaseCombobox.getSelectionModel().getSelectedItem();

                    try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_DUBLICATE))) {
                        if (rs != null) {
                            List<ReportIndexDublicate> list = new ArrayList<>();
                            while (rs.next()) {

                                String database = rs.getString(1);

                                if (db == null || db.isEmpty() || db.equals(database)) {
                                    ReportIndexDublicate rid = new ReportIndexDublicate(
                                            database,
                                            rs.getString(2),
                                            rs.getString(3),
                                            rs.getString(4),
                                            rs.getString(5),
                                            rs.getString(6),
                                            rs.getString(7),
                                            rs.getString(8),
                                            rs.getString(9),
                                            rs.getString(10),
                                            rs.getString(11)
                                    );

                                    list.add(rid);
                                }
                            }

                            tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                        }
                    } catch (SQLException ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), null);
                    }
                }

                Platform.runLater(() -> makeBusy(false));
            }
        });
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void run() {
                    
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportIndexDublicate rds: (List<ReportIndexDublicate>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rds.getDatabase());
                        row.add(rds.getTableName());
                        row.add(rds.getRedundantIndexName());
                        row.add(rds.getRedundantIndexColumns());
                        row.add(rds.getRedundantIndexType());
                        row.add(rds.getDominantIndexName());
                        row.add(rds.getDominantIndexColumns());
                        row.add(rds.getDominantIndexType());
                        row.add(rds.getComments());
                        row.add(rds.getSubpartExists());
                        row.add(rds.getDropSql());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    columns.put("Table Schema", true);
                    columnWidths.add(100);

                    columns.put("Table name", true);
                    columnWidths.add(100);

                    columns.put("Redundant Index Name", true);
                    columnWidths.add(150);

                    columns.put("Redundant Index Columns", true);
                    columnWidths.add(150);

                    columns.put("Redundant Index TYPE", true);
                    columnWidths.add(150);
                    
                    columns.put("Dominant Index Name", true);
                    columnWidths.add(150);
                    
                    columns.put("Dominant Index Columns", true);
                    columnWidths.add(150);
                    
                    columns.put("Dominant Index TYPE", true);
                    columnWidths.add(150);
                    
                    columns.put("Comments", true);
                    columnWidths.add(150);
                    
                    columns.put("Subpart Exists", true);
                    columnWidths.add(150);
                    
                    columns.put("SQL DROP For Redundant Index", true);
                    columnWidths.add(150);
                                        
                    sheets.add("Index dublicate report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> DialogHelper.showError(getController(), "Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> DialogHelper.showInfo(getController(), "Success", "Data export seccessfull", null, getStage()));
                    }
                });
                }
            });
        }
    }
    
    
    public void init(ConnectionSession session, PreferenceDataService preferenceService) {
        this.session = session;        
        this.preferenceService = preferenceService;
        disableListen = false;
        
        databaseCombobox.getItems().add("");
        
        if (session != null && session.getConnectionParam() != null) {
            if (session.getConnectionParam().getDatabases() != null) {
                for (Database db: session.getConnectionParam().getDatabases()) {
                    databaseCombobox.getItems().add(db.getName());
                }
            }
        }
        
        databaseCombobox.getSelectionModel().select(0);
        
        disableListen = true;
        
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        TableColumn databaseColumn = createColumn("Table Schema", "database");        
        TableColumn tableNameColumn = createColumn("Table name", "tableName");
        
        TableColumn redundantIndexNameColumn = createColumn("Redundant Index Name", "redundantIndexName");
        redundantIndexNameColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn redundantIndexColumnsColumn = createColumn("Redundant Index Columns", "redundantIndexColumns");
        redundantIndexColumnsColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn redundantIndexTypeColumn = createColumn("Redundant Index TYPE", "redundantIndexType");
        redundantIndexTypeColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn dominantIndexNameColumn = createColumn("Dominant Index Name", "dominantIndexName");
        dominantIndexNameColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn dominantIndexColumnsColumn = createColumn("Dominant Index Columns", "dominantIndexColumns");
        dominantIndexColumnsColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn dominantIndexTypeColumn = createColumn("Dominant Index TYPE", "dominantIndexType");
        dominantIndexTypeColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn commentsColumn = createColumn("Comments", "comments");
        commentsColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        TableColumn subpartExistsColumn = createColumn("Subpart Exists", "subpartExists");
        
        TableColumn dropSqlColumn = createColumn("SQL DROP For Redundant Index", "dropSql");
        dropSqlColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
            }
        });
        
        tableView.setSearchEnabled(true);
        
        tableView.addTableColumn(databaseColumn, 150, 150, false);
        tableView.addTableColumn(tableNameColumn, 150, 150, false);
        tableView.addTableColumn(redundantIndexNameColumn, 170, 170, false);
        tableView.addTableColumn(redundantIndexColumnsColumn, 170, 170, false);
        tableView.addTableColumn(redundantIndexTypeColumn, 170, 170, false);
        tableView.addTableColumn(dominantIndexNameColumn, 170, 170, false);
        tableView.addTableColumn(dominantIndexColumnsColumn, 170, 170, false);
        tableView.addTableColumn(dominantIndexTypeColumn, 170, 170, false);
        tableView.addTableColumn(commentsColumn, 170, 170, false);
        tableView.addTableColumn(subpartExistsColumn, 170, 170, false);
        tableView.addTableColumn(dropSqlColumn, 170, 170, false);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        databaseCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (disableListen) {
                refreshAction(null);
            }
        });
    }
   
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {
        FunctionHelper.showSlowQueryData(getStage(), getController(), cellData, text, title, preferenceService);
    }
    
}
