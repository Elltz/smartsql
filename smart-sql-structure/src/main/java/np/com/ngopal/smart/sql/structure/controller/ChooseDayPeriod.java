package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ChooseDayPeriod extends BaseController implements Initializable {
    
    private static final String ERROR_LOG__1_DAY = "1 day log";
    private static final String ERROR_LOG__2_DAY = "2 day log";
    private static final String ERROR_LOG__3_DAY = "3 day log";
    private static final String ERROR_LOG__4_DAY = "4 day log";
    private static final String ERROR_LOG__5_DAY = "5 day log";
    private static final String ERROR_LOG__6_DAY = "6 day log";
    private static final String ERROR_LOG__7_DAY = "7 day log";
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private ComboBox<String> dayCombobx;
    
    private Stage dialog;
    
    @Getter
    private Integer days;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        days = null;
        getStage().close();
    }
    
    @FXML
    public void okAction(ActionEvent event) {
        
        switch (dayCombobx.getSelectionModel().getSelectedItem()) {
            case ERROR_LOG__1_DAY:
                days = 1;
                break;
            case ERROR_LOG__2_DAY:
                days = 2;
                break;
            case ERROR_LOG__3_DAY:
                days = 3;
                break;
            case ERROR_LOG__4_DAY:
                days = 4;
                break;
            case ERROR_LOG__5_DAY:
                days = 5;
                break;
            case ERROR_LOG__6_DAY:
                days = 6;
                break;
            case ERROR_LOG__7_DAY:
                days = 7;
                break;
        }
        
        getStage().close();
    }
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dayCombobx.getItems().addAll(new String[] {
            ERROR_LOG__1_DAY, ERROR_LOG__2_DAY, ERROR_LOG__3_DAY, ERROR_LOG__4_DAY,
            ERROR_LOG__5_DAY, ERROR_LOG__6_DAY, ERROR_LOG__7_DAY
        });
    }
    
    public void init(Window win) {        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(200);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Choose Day Period");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();
        
    }        
    
    @Override
    public Stage getStage() {
        return (Stage) dialog;
    }
}
