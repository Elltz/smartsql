package np.com.ngopal.smart.sql.structure.models;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class MessageDataItem {
    
    private String message;
    private String query;
    private boolean error = false;
    private boolean warning = false;
    private boolean resultSet = false;
    
    private List<String> warningCodes;
    private List<String> warningMessages;
    
    private double executionTime;
    private double transferTime;
    private double responseTime;
    
    private Integer[] customRange;
    
}
