/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import java.util.HashMap;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class MetricExtract {

    protected String response;
    protected HashMap<String,String> data;
    protected MetricTypes type;
    
    public MetricExtract(MetricTypes type) {
        this(type, null);
    }
    
    public MetricExtract(MetricTypes type, String allResponseToParse) {
        this.type = type;
        this.response = allResponseToParse;
        data = new HashMap<>();
    }
    
    public static  MetricExtract getMetricExtract(MetricTypes mt) {
        switch (mt) {
            case CPU_METRIC:
                return new CpuMetricExtract(mt);
            case DISK_IO_METRIC:
                return new DiskIOMetricExtract(mt);
            case NETWORK_METRIC:
                return new NetworkMetricExtract(mt);
            case MEMORY_METRIC:
                return new MemoryMetricExtract(mt);
            case SWAP_MEMORY_METRIC:
                return new SwapMemoryMetricExtract(mt);
            default:
                return null;
        }
    }
    
    public String[] getKeys(){
        return data.keySet().toArray(new String[0]);
    }
    
    public String getRemoteVal(String key){
        return data.get(key);
    }
    
    public void extract(){
        
    }

    public MetricTypes getType() {
        return type;
    }
    
    public synchronized final void update(String response){
        this.response = response;
        extract();
    }

    @Override
    public String toString() {
        return response;
    }
    
}
