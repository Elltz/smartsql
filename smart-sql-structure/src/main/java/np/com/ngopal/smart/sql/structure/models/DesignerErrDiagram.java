package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerErrDiagram implements InvalidationListener {
    
    private final SimpleBooleanProperty changed = new SimpleBooleanProperty(false);
    
    private final SimpleStringProperty uuid = new SimpleStringProperty(UUID.randomUUID().toString());
    
    private final SimpleStringProperty host = new SimpleStringProperty();
    private final SimpleStringProperty database = new SimpleStringProperty("");
    
    private final SimpleObjectProperty<Long> publicId = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<Long> domainId = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<Long> subdomainId = new SimpleObjectProperty<>();
    private final SimpleStringProperty description = new SimpleStringProperty();
    
    
    private final SimpleStringProperty name = new SimpleStringProperty();
    private final SimpleStringProperty user = new SimpleStringProperty();
    private final SimpleBooleanProperty domain = new SimpleBooleanProperty(false); // false - private, true - public
    
    private final ObservableList<DesignerTable> tables = FXCollections.observableArrayList();
    private final ObservableList<DesignerLayer> layers = FXCollections.observableArrayList();
    private final ObservableList<DesignerText> texts = FXCollections.observableArrayList();
    private final ObservableList<DesignerImage> images = FXCollections.observableArrayList();
    private final ObservableList<DesignerView> views = FXCollections.observableArrayList();
    
    private volatile boolean canListen = false;
    
    public DesignerErrDiagram() {
        WeakInvalidationListener listener = new WeakInvalidationListener(this);
        
        name.addListener(listener);
        tables.addListener(listener);
        layers.addListener(listener);
        texts.addListener(listener);
        images.addListener(listener);
        views.addListener(listener);
    }
       
    public void startListenChanges() {
        canListen = true;
    }

    @Override
    public void invalidated(Observable observable) {
        if (canListen) {
            changed.set(true);
        }
    }
    
    public String getDatabaseName() {
        return database.get() == null ? "" : database.getName();
    }
    
    public SimpleBooleanProperty changed() {
        return changed;
    }
    
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    public SimpleStringProperty host() {
        return host;
    }
    
    public SimpleStringProperty database() {
        return database;
    }
    
    public SimpleObjectProperty<Long> publicId() {
        return publicId;
    }
    
    public SimpleObjectProperty<Long> domainId() {
        return domainId;
    }
    
    public SimpleObjectProperty<Long> subdomainId() {
        return subdomainId;
    }
    
    public SimpleStringProperty description() {
        return description;
    }
    
    public SimpleStringProperty name() {
        return name;
    }
    
    public SimpleStringProperty user() {
        return user;
    }
    
    public SimpleBooleanProperty domain() {
        return domain;
    }
    
    public void addTable(DesignerTable table) {
        tables.add(table);
        table.setParent(this);
    }
    
    public ObservableList<DesignerTable> tables() {
        return tables;
    }
    
    
    public void addLayer(DesignerLayer layer) {
        layers.add(layer);
        layer.setParent(this);
    }
    
    public ObservableList<DesignerLayer> layers() {
        return layers;
    }
    
    
    public void addText(DesignerText text) {
        texts.add(text);
        text.setParent(this);
    }
    
    public ObservableList<DesignerText> texts() {
        return texts;
    }
    
    
    public void addImage(DesignerImage image) {
        images.add(image);
        image.setParent(this);
    }
    
    public ObservableList<DesignerImage> images() {
        return images;
    }
    
    
    public void addView(DesignerView view) {
        views.add(view);
        view.setParent(this);
    }
    
    public ObservableList<DesignerView> views() {
        return views;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("host", host.get());
        map.put("publicId", publicId.get());
        map.put("domainId", domainId.get());
        map.put("subdomainId", subdomainId.get());
        map.put("description", description.get());
        map.put("host", host.get());
        map.put("database", database.get() == null ? "" : database.get());
        map.put("name", name.get());
        map.put("user", user.get());
        map.put("domain", domain.get());
        
        List<Map<String, Object>> tableList = new ArrayList<>();
        map.put("tables", tableList);
        
        for (DesignerTable t: tables) {
            tableList.add(t.toMap());
        }
        
        List<Map<String, Object>> layersList = new ArrayList<>();
        map.put("layers", layersList);
        
        for (DesignerLayer l: layers) {
            layersList.add(l.toMap());
        }
        
        List<Map<String, Object>> textsList = new ArrayList<>();
        map.put("texts", textsList);
        
        for (DesignerText t: texts) {
            textsList.add(t.toMap());
        }
        
        List<Map<String, Object>> imagesList = new ArrayList<>();
        map.put("images", imagesList);
        
        for (DesignerImage i: images) {
            imagesList.add(i.toMap());
        }
        
        List<Map<String, Object>> viewsList = new ArrayList<>();
        map.put("views", viewsList);
        
        for (DesignerView i: views) {
            viewsList.add(i.toMap());
        }
        
        return map;
    }

    public void fromMap(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            if (uuid.get() == null) {
                uuid.set(UUID.randomUUID().toString());
            }
            
            host.set((String)map.get("host"));
            database.set((String)map.get("database"));
            if (database.get() == null) {
                database.set("");
            }
            
            Double d = (Double) map.get("publicId");
            publicId.set(d != null ? d.longValue() : null);
            
            Double d0 = (Double) map.get("domainId");
            domainId.set(d0 != null ? d0.longValue() : null);
            
            Double d1 = (Double) map.get("subdomainId");
            subdomainId.set(d1 != null ? d1.longValue() : null);
            description.set((String)map.get("description"));
                       
            name.set((String)map.get("name"));            
            user.set((String)map.get("user"));
            
            Boolean b = (Boolean)map.get("domain");
            domain.set(b != null ? b : false);
            
            List<Map<String, Object>> tablesList = (List<Map<String, Object>>)map.get("tables");
            if (tablesList != null) {
                
                Map<String, DesignerTable> mapTables = new HashMap<>();
                // first parse with out indexes and fks
                for (Map<String, Object> t: tablesList) {
                    DesignerTable table = new DesignerTable();
                    table.fromMapWithoutIndexesAndFks(t);
                    
                    addTable(table);
                    mapTables.put(table.uuid().get(), table);
                }
                
                // second parse indexes and fks
                for (Map<String, Object> t: tablesList) {
                    // cant be NullPointer
                    mapTables.get((String)t.get("uuid")).fromMapIndexesAndFks(t, mapTables);
                }
            }
            
            List<Map<String, Object>> layersList = (List<Map<String, Object>>)map.get("layers");
            if (layersList != null) {
                
                for (Map<String, Object> l: layersList) {
                    DesignerLayer layer = new DesignerLayer();
                    layer.fromMap(l);
                    
                    layers.add(layer);
                }                
            }
            
            List<Map<String, Object>> textsList = (List<Map<String, Object>>)map.get("texts");
            if (textsList != null) {
                
                for (Map<String, Object> t: textsList) {
                    DesignerText text = new DesignerText();
                    text.fromMap(t);
                    
                    texts.add(text);
                }                
            }
            
            List<Map<String, Object>> imagesList = (List<Map<String, Object>>)map.get("images");
            if (imagesList != null) {
                
                for (Map<String, Object> t: imagesList) {
                    DesignerImage img = new DesignerImage();
                    img.fromMap(t);
                    
                    images.add(img);
                }                
            }
            
            List<Map<String, Object>> viewsList = (List<Map<String, Object>>)map.get("views");
            if (viewsList != null) {
                
                for (Map<String, Object> t: viewsList) {
                    DesignerView view = new DesignerView();
                    view.fromMap(t);
                    
                    views.add(view);
                }                
            }
        }
    }

    public DesignerLayer findLayer(double x, double y) {
        for (DesignerLayer l: layers) {
            if (l.inside(x, y)) {
                return l;
            }
        }
        
        return null;
    }

    public List<DesignerData> findLayerData(String layerUuid) {
        
        List<DesignerData> list = new ArrayList<>();
        
        for (DesignerTable t: tables) {
            if (layerUuid.equals(t.layerUuid().get())) {
                list.add(t);
            }
        }
        
        for (DesignerText t: texts) {
            if (layerUuid.equals(t.layerUuid().get())) {
                list.add(t);
            }
        }
        
        for (DesignerImage img: images) {
            if (layerUuid.equals(img.layerUuid().get())) {
                list.add(img);
            }
        }
        
        for (DesignerView view: views) {
            if (layerUuid.equals(view.layerUuid().get())) {
                list.add(view);
            }
        }
        
        return list;
    }    
}
