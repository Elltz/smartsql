/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.misc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public enum RunnedFrom {
    QA,
    SQAQA,
    DQA,
    PQA
}
