/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.UIController;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public interface ReportsContract {
    
    BaseController getReportController();
    
    default void onEntry(){
        UIController contr = getReportController().getController();
        contr.getTabControllerBySession(contr.getSelectedConnectionSession().get())
                .hideLeftPane();
    }
    
    default void onExit(){
        UIController contr = getReportController().getController();
        contr.getTabControllerBySession(contr.getSelectedConnectionSession().get())
                .showLeftPane();
    }
    
}
