package np.com.ngopal.smart.sql.structure.queryutils;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public final class FontUtils {

    public static Font parseFXFont(String font) {
        Font currentFont = null;
        
        if (font != null) {
            String[] data = font.split(",");
            if (data.length == 2) {
                try {
                    currentFont = Font.font(data[0].trim(), Double.valueOf(data[1].trim()));
                } catch (Throwable th) {
                }
            } else if (data.length == 3) {
                try {
                    FontWeight fw = null;
                    FontPosture fp = null;
                    
                    String[] weight = data[1].trim().split(" ");
                    if (weight.length > 0) {
                        fw = FontWeight.findByName(weight[0].trim());
                        if (fw == null) {
                            fp = FontPosture.findByName(weight[0].trim());
                        }
                    }
                    
                    if (weight.length > 1) {
                        fp = FontPosture.findByName(weight[0].trim());
                    }
                    
                    currentFont = Font.font(data[0].trim(), fw, fp, Double.valueOf(data[2].trim()));
                } catch (Throwable th) {
                }
            }
        }
        
        return currentFont;
    }
    
    public static String parseCSSFont(String font) {
        String currentFont = "";
        
        if (font != null) {
            String[] data = font.split(",");
            if (data.length == 2) {
                try {
                    currentFont += "-fx-font-family: " + data[0].trim() + ";";
                    currentFont += "-fx-font-size: " + data[1].trim() + ";";
                } catch (Throwable th) {
                }
            } else if (data.length == 3) {
                try {
                    FontWeight fw = null;
                    FontPosture fp = null;
                    
                    String[] weight = data[1].trim().split(" ");
                    if (weight.length > 0) {
                        fw = FontWeight.findByName(weight[0].trim());
                        if (fw == null) {
                            fp = FontPosture.findByName(weight[0].trim());
                        }
                    }
                    
                    if (weight.length > 1) {
                        fp = FontPosture.findByName(weight[0].trim());
                    }
                    
                    currentFont += "-fx-font-family: " + data[0].trim() + ";";
                    if (fw != null) {
                        currentFont += "-fx-font-weight: " + fw.name() + ";";
                    }
                    if (fp != null) {
                        currentFont += "-fx-font-style: " + fp.name() + ";";
                    }
                    currentFont += "-fx-font-size: " + data[2].trim() + ";";
                } catch (Throwable th) {
                }
            }
        }
        
        return currentFont;
    }
    
    public static java.awt.Font parseAWTFont(String font) {
        java.awt.Font currentFont = null;
        String[] data = font.split(",");
        if (data.length == 3) {
            try {
                currentFont = new java.awt.Font(data[0].trim(), convertFontStrToType(data[1].trim()), Double.valueOf(data[2].trim()).intValue());
            } catch (Throwable th) {
                log.error("Error", th);
            }
        }        
        
        return currentFont;
    }
    
    private static int convertFontStrToType(String type) {
        switch (type.toLowerCase()) {
            case "regular":
                return java.awt.Font.PLAIN;
                
            case "bold":
                return java.awt.Font.BOLD;
                
            case "italic":
                return java.awt.Font.ITALIC;
                
            case "bold italic":
                return java.awt.Font.BOLD | java.awt.Font.ITALIC;
            
            default:
                return java.awt.Font.PLAIN;
        }
    }
    
    private FontUtils () {}
}
