/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
@Getter
public class SmartPropertiesData {

    private SimpleBooleanProperty autoRebuildTagsOnStartup,            
            pasteObjectOnDoubleClick,notifyUnsavedTabChanges;
    
    private SimpleStringProperty fontColor, backgroundColor,
            selectionColor;
    private SimpleDoubleProperty fontSize;
    
    public SmartPropertiesData(SmartsqlSavedData smartData) {
        
        
        autoRebuildTagsOnStartup = new SimpleBooleanProperty(smartData.isAutorebuildTags()){
            @Override
            protected void invalidated() {
                super.invalidated();
                smartData.setAutorebuildTags(get());
            }            
        };
        
//        enableAnimation = new SimpleBooleanProperty(smartData.isEnableAnimation()){
//            @Override
//            protected void invalidated() {
//                super.invalidated();
//                smartData.setEnableAnimation(get());
//            }            
//        };
        
        pasteObjectOnDoubleClick = new SimpleBooleanProperty(smartData.isPasteObjectOnDoubleClick()){
            @Override
            protected void invalidated() {
                super.invalidated();
                smartData.setPasteObjectOnDoubleClick(get());
            }            
        };
        
        notifyUnsavedTabChanges = new SimpleBooleanProperty(smartData.isNotifyUnSavedTab()){
            @Override
            protected void invalidated() {
                super.invalidated();
                smartData.setNotifyUnSavedTab(get());
            }            
        };        
        
        this.fontColor = new SimpleStringProperty(smartData.getFontColor()){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                smartData.setFontColor(get());
            }
            
        };
        this.backgroundColor = new SimpleStringProperty(smartData.getBackgroundColor()){
            @Override
            protected void invalidated() {
                super.invalidated();
                smartData.setBackgroundColor(get());
            }
            
        };
        this.selectionColor = new SimpleStringProperty(smartData.getSelectionColor()){
            @Override
            protected void invalidated() {
                super.invalidated();
                smartData.setSelectionColor(get());
            }
            
        };
        this.fontSize = new SimpleDoubleProperty(smartData.getQueryEditorSize()){
            @Override
            protected void invalidated() {
                super.invalidated();
                smartData.setQueryEditorSize(get());
            }            
        };
//        
//        menuButton_expandAnimation = new SimpleBooleanProperty(smartData.isMenuButton_expandAnim()){
//            @Override
//            protected void invalidated() {
//                super.invalidated();
//                smartData.setMenuButton_expandAnim(get());
//            }            
//        };
//        
//        menuButton_flipAnimation = new SimpleBooleanProperty(smartData.isMenuButton_flipAnim()){
//            @Override
//            protected void invalidated() {
//                super.invalidated();
//                smartData.setMenuButton_flipAnim(get());
//            }            
//        };
        
    }
    
}
