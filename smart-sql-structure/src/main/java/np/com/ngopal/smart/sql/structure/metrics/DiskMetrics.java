/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class DiskMetrics extends Metric {

    public DiskMetrics() {
        super("DISK METRIC", "DISK METRIC CHART \n This metric measures disk usage rate over a period of time (1second) \n"
                + "Disk read access and write operation of your remote server hard disk");
    }

    @Override
    public StringConverter<Number> yConverter() {
        return new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return String.valueOf(object.doubleValue()) + " MB";
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string.split(" ")[0]);
            }
        };
    }

    @Override
    public long getMaxYValue() {
        return Long.valueOf(getDiskIOMetricExtract().getTotalSize()) / 1024;
    }

    @Override
    public int getUnitTimeValue() {
        return super.getUnitTimeValue(); 
    }

    @Override
    public XYChart.Series<Number, Number>[] getXYChartSeries() {
        XYChart.Series<Number, Number>[] series = new XYChart.Series[1];
        series[0] = getNewXYChart();
        series[0].setName("Used Space");
        
//        series[1] = getNewXYChart();
//        series[1].setName("Available Space");
        
        return series;
    }

    @Override
    public void updateUI(long timeInMillis, MetricExtract extract) {
        super.updateUI(timeInMillis, extract);
        if(extract != null){
            for(XYChart.Series<Number, Number> serie : data){
                 XYChart.Data<Number, Number> myData = new XYChart.Data();
                myData.setXValue(timeInMillis);
//                myData.setYValue(Long.valueOf((serie.getName().charAt(0) == 'U') ? getDiskIOMetricExtract().getUsedSize() :
//                        getDiskIOMetricExtract().getAvailableSize()) / 1024);
                myData.setYValue(Long.valueOf(getDiskIOMetricExtract().getUsedSize()) / 1024);
                serie.getData().add(myData);
            }
        }
    }
    
    private DiskIOMetricExtract getDiskIOMetricExtract(){
        return (DiskIOMetricExtract) metricExtract;
    }

    @Override
    public int getYTickUnitCount() {
        return 4;
    }

    @Override
    public String[] getChartColors() {
        return new String[]{"blue","coral"};
    }
}
