package np.com.ngopal.smart.sql.structure.dialogs;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class InfoDialogController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;

    private Pattern URL_PATTER_0 = Pattern.compile("(http(s)?:\\/\\/.*\\.[a-z]{2,4})");
    private Pattern URL_PATTER_1 = Pattern.compile("www\\.");
    
    @FXML
    private VBox infoBox;
    @FXML
    private TextArea detailArea;
    @FXML
    private Label showDetailsLabel;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
    public void init(String titleText) {
        init(titleText, null);
    }
    
    public void init(String titleText, String detailText) {
        
        Matcher m = URL_PATTER_0.matcher(titleText);        
        int prev = 0;
        while (m.find()) {
            int start = m.start();
            int end = m.end();
            
            String text = titleText.substring(prev, start);
            if (!text.trim().isEmpty()) {
                Label l = new Label(text);
                l.setWrapText(true);

//                calculateHeightFlow(l, 450.0);
                l.setMaxHeight(200);

                infoBox.getChildren().add(l);
            }
            
            Hyperlink link = new Hyperlink(titleText.substring(start, end));
            link.setFocusTraversable(false);
            link.setOnAction((ActionEvent event) -> {       
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(new URL(link.getText()).toURI());
                    } catch (IOException | URISyntaxException e) {
                        log.error("Error while opening error link", e);
                    }
                }
            });
                    
            infoBox.getChildren().add(link);
            
            prev = end;
        }
        
        m = URL_PATTER_1.matcher(titleText);        
        prev = 0;
        while (m.find()) {
            int start = m.start();
            int end = titleText.indexOf(" ", start);
            
            String text = titleText.substring(prev, start);
            if (!text.trim().isEmpty()) {
                Label l = new Label(text);
                l.setWrapText(true);

//                calculateHeightFlow(l, 450.0);
                l.setMaxHeight(200);

                infoBox.getChildren().add(l);
            }
            
            Hyperlink link = new Hyperlink(titleText.substring(start, end));
            link.setFocusTraversable(false);
            link.setOnAction((ActionEvent event) -> {       
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(URI.create(link.getText()));
                    } catch (IOException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                }
            });
                    
            infoBox.getChildren().add(link);
            
            prev = end;
        }
        
        String text = titleText.substring(prev, titleText.length());
        if (!text.trim().isEmpty()) {
            Label l = new Label(text);
            l.setWrapText(true);

            //calculateHeightFlow(l, 450.0);
            l.setMaxHeight(200);
            
            infoBox.getChildren().add(l);
        }
                
        detailArea.setVisible(false);
        detailArea.setWrapText(true);
        
        detailArea.managedProperty().bind(detailArea.visibleProperty());
        showDetailsLabel.managedProperty().bind(showDetailsLabel.visibleProperty());
        
        if (detailText != null) {
            detailArea.setText(detailText);
            
            showDetailsLabel.setOnMouseClicked((MouseEvent event) -> {
                detailArea.setVisible(!detailArea.isVisible());
                showDetailsLabel.setGraphic(new ImageView(detailArea.isVisible() ? 
                        StandardDefaultImageProvider.getInstance().getDialogLessDetails_image()
                        : StandardDefaultImageProvider.getInstance().getDialogMoreDetails_image()));
                showDetailsLabel.setText(detailArea.isVisible() ? "Hide Details" : "Show Details");
                getStage().sizeToScene();
            });
        } else { showDetailsLabel.setVisible(false); }
    }
        
    @FXML
    public void okAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    public DialogResponce getResponce() {
        return responce;
    }

    @Override
    public void initDialog(Object... params) {
        init((String)params[0], (String)params[1]);
        
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }

    @Override
    public String getInputValue() {
        return "";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
