/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.DeadlockService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.TechnologyType;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.controller.DebuggerTabController;
import np.com.ngopal.smart.sql.structure.controller.QueryAnalyzerTabController;
import np.com.ngopal.smart.sql.structure.controller.ReportDataSizeController;
import np.com.ngopal.smart.sql.structure.controller.ReportIndexDublicateController;
import np.com.ngopal.smart.sql.structure.controller.ReportIndexUnusedController;
import np.com.ngopal.smart.sql.structure.controller.SlowQueryAnalyzerTabController;
import np.com.ngopal.smart.sql.structure.controller.SlowQueryTemplateCellController;
import np.com.ngopal.smart.sql.structure.controller.TunerSqlTabController;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.controller.VisualExplainController;
import np.com.ngopal.smart.sql.structure.factories.QueryTableCell;
import np.com.ngopal.smart.sql.structure.misc.MiscCacheHolder;
import np.com.ngopal.smart.sql.structure.misc.RunnedFrom;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class FunctionHelper {

    /**
     * If some component used combination, but doing something wrong need add it
     * to this exclusion
     *
     * @param event
     * @return
     */
    public static boolean checkExclusion(KeyEvent event) {
        return event.getCode() == KeyCode.F2 && !event.isAltDown() && !event.isControlDown() && !event.isShiftDown() && !event.isMetaDown()
                || /* Format all queries */ event.getCode() == KeyCode.F12 && event.isShiftDown()
                || /* Database Synchronization Wizard */ event.getCode() == KeyCode.W && event.isControlDown() && event.isAltDown()
                || event.getCode() == KeyCode.Q && event.isControlDown() && event.isAltDown()
                || event.getCode() == KeyCode.Q && event.isControlDown() && !event.isShiftDown()
                || event.getCode() == KeyCode.D && event.isControlDown() && event.isAltDown()
                || event.getCode() == KeyCode.PAGE_DOWN && event.isControlDown()
                || event.getCode() == KeyCode.PAGE_UP && event.isControlDown();
    }

    public static String serverHashCode(ConnectionParams params, String database) {
        CRC32 crc = new CRC32();
        crc.update((params.getAddress() + database).getBytes());
        return Long.toString(crc.getValue());
    }

    public static void enableProfiling(DBService dbService, SQLLogger logger) throws SQLException {

//        try (ResultSet rs = (ResultSet) dbService.execute("SELECT @@profiling")) {
//            if (rs != null && rs.next()) {
//                if (rs.getInt(1) != 0) {
//                    return false;
//                }
//            }
//        }
        dbService.execute("SET PROFILING = 1;");
        if (logger != null) {
            logger.log(dbService.getDB(), "SET PROFILING = 1;", false);
        }

//        dbService.execute("SET profiling_history_size = 0;");
//        if (logger != null) {
//            logger.log(dbService.getDB(), "SET profiling_history_size = 0;", false);
//        }
//        
//        dbService.execute("SET profiling_history_size = 15;");
//        if (logger != null) {
//            logger.log(dbService.getDB(), "SET profiling_history_size = 15;", false);
//        }
//        return true;
    }

    public static Map<String, Object> executeShowStatus(DBService dbService, SQLLogger logger) throws SQLException {
        try (ResultSet rs = (ResultSet) dbService.execute("SHOW STATUS;")) {

            if (logger != null) {
                logger.log(dbService.getDB(), "SHOW STATUS;", false);
            }

            return executeShowStatus(rs, false);
        }
    }

    public static Map<String, Object> executeShowStatus(ResultSet rs, boolean close) throws SQLException {
        Map<String, Object> result = new HashMap<>();

        if (rs != null) {
            while (rs.next()) {
                result.put(rs.getString(1), rs.getObject(2));
            }

            if (close) {
                rs.close();
            }
        }

        return result;
    }

    public static Map<String, Double> executeShowProfile(DBService dbService, String query, SQLLogger logger) throws SQLException {

        Connection conn = dbService.getDB();

//        boolean profileWasEnabled = false;
//        try (ResultSet rs = (ResultSet) conn.createStatement().executeQuery("SELECT @@profiling")) {
//            if (rs != null && rs.next()) {
//                if (rs.getInt(1) != 0) {
//                    profileWasEnabled = true;
//                }
//            }
//        }
//        if (!profileWasEnabled) {
        conn.createStatement().execute("SET PROFILING = 1;");
        if (logger != null) {
            logger.log(conn, "SET PROFILING = 1;", false);
        }
//        }

//        conn.createStatement().execute("SET profiling_history_size = 0;");
//        if (logger != null) {
//            logger.log(conn, "SET profiling_history_size = 0;", false);
//        }
//        
//        conn.createStatement().execute("SET profiling_history_size = 15;");
//        if (logger != null) {
//            logger.log(conn, "SET profiling_history_size = 15;", false);
//        }
//        try {
//            Thread.sleep(5000);
//        } catch (Throwable th) {
//        }
//        
        conn.createStatement().execute(query);
        if (logger != null) {
            logger.log(conn, query, false);
        }

        Long queryId = null;
        try (ResultSet rs = (ResultSet) conn.createStatement().executeQuery("SHOW PROFILES;")) {

            if (rs != null) {
                while (rs.next()) {
                    query = query.trim();
                    if (query.endsWith(";")) {
                        query = query.substring(0, query.length() - 1);
                    }
                    if (query.startsWith(rs.getString("Query").trim())) {
                        queryId = rs.getLong("Query_ID");
                        break;
                    }
                }
            }
        }
        if (logger != null) {
            logger.log(conn, "SHOW PROFILES;", false);
        }

        Map<String, Double> result = new HashMap<>();
        String query0 = "SELECT state, ROUND(SUM(duration),5) AS `duration (summed) in sec` FROM information_schema.profiling WHERE query_id = " + queryId + " GROUP BY state ORDER BY `duration (summed) in sec` DESC;";
        try (ResultSet rs = (ResultSet) conn.createStatement().executeQuery(query0)) {

            if (rs != null) {
                while (rs.next()) {
                    String key = rs.getString(1);
                    Double value = rs.getDouble(2);

                    Double oldValue = result.get(key);
                    if (oldValue != null) {
                        value += oldValue;
                    }

                    result.put(key, value);
                }
            }
        }
        if (logger != null) {
            logger.log(conn, query0, false);
        }

//        if (!profileWasEnabled) {
//            conn.createStatement().execute("SET PROFILING = 0;");
//            if (logger != null) {
//                logger.log(conn, "SET PROFILING = 0;", false);
//            }
//        }
        return result;
    }

    public static Map<String, Double> executeShowProfiles(DBService dbService, String query, SQLLogger logger) throws SQLException {

        Long queryId = null;
        try (ResultSet rs = (ResultSet) dbService.execute("SHOW PROFILES;")) {

            if (rs != null) {
                while (rs.next()) {
                    query = query.trim();
                    if (query.endsWith(";")) {
                        query = query.substring(0, query.length() - 1);
                    }

                    if (query.startsWith(rs.getString("Query").trim())) {
                        queryId = rs.getLong("Query_ID");
                        break;
                    }
                }
            }
        }
        if (logger != null) {
            logger.log(dbService.getDB(), "SHOW PROFILES;", false);
        }

        Map<String, Double> result = new LinkedHashMap<>();
        String query0 = "SELECT state, ROUND(SUM(duration),5) AS `duration (summed) in sec` FROM information_schema.profiling WHERE query_id = " + queryId + " GROUP BY state ORDER BY `duration (summed) in sec` DESC;";
        try (ResultSet rs = (ResultSet) dbService.execute(query0)) {

            if (rs != null) {
                while (rs.next()) {
                    result.put(rs.getString(1), rs.getDouble(2));
                }
            }
        }
        if (logger != null) {
            logger.log(dbService.getDB(), query0, false);
        }

        return result;
    }

    public static void disableProfiling(DBService dbService, SQLLogger logger) throws SQLException {
        dbService.execute("SET PROFILING = 0;");
        if (logger != null) {
            logger.log(dbService.getDB(), "SET PROFILING = 0;", false);
        }
    }

    public static Map<String, String> explainExtendedQuery(DBService dbService, String query, SQLLogger logger) throws SQLException {

        String query0;
        if (dbService.getDbSQLVersionMaj() > 5) {
            query0 = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), query);
        } else {
            query0 = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN_EXTENDED), query);
        }

        try (ResultSet rs0 = (ResultSet) dbService.execute(query0)) {
            if (logger != null) {
                logger.log(dbService.getDB(), query0, true);
            }

            try (ResultSet rs = (ResultSet) dbService.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_WARNINGS))) {
                return explainExtendedQuery(rs, false);
            }
        }
    }

    public static Map<String, String> explainExtendedQuery(ResultSet rs, boolean close) throws SQLException {

        Map<String, String> result = new HashMap<>();
        if (rs != null) {
            // get last, maybe need all?
            while (rs.next()) {
                result.put("level", rs.getString(1));
                result.put("code", rs.getString(2));
                result.put("query", rs.getString(3));
            }

            if (close) {
                rs.close();
            }
        }

        return result;
    }

    public static List<ExplainResult> explainQuery(DBService dbService, String query, SQLLogger logger) throws SQLException {
        String query0 = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), query);
        try (ResultSet rs = (ResultSet) dbService.execute(query0)) {
            return explainQuery(rs, false);
        }
    }

    public static List<ExplainResult> explainQuery(ResultSet rs, boolean close) throws SQLException {
        List<ExplainResult> result = new ArrayList<>();
        if (rs != null) {

            while (rs.next()) {
                Long id = getLongValue(rs, "id");
                if (id > 0) {

                    result.add(
                            new ExplainResult(
                                    id,
                                    getStringValue(rs, "select_type"),
                                    getStringValue(rs, "table"),
                                    getStringValue(rs, "partitions"),
                                    getStringValue(rs, "type"),
                                    getStringValue(rs, "possible_keys"),
                                    getStringValue(rs, "key"),
                                    getLongValue(rs, "key_len"),
                                    getStringValue(rs, "ref"),
                                    getLongValue(rs, "rows"),
                                    getLongValue(rs, "filtered"),
                                    getStringValue(rs, "extra")
                            )
                    );
                }
            }

            if (close) {
                rs.close();
            }
        }

        return result;
    }

    private static String getStringValue(ResultSet rs, String column) {
        String result = null;
        try {
            result = rs.getString(column);
        } catch (SQLException ex) {
        }

        return result;
    }

    private static Long getLongValue(ResultSet rs, String column) {
        Long result = null;
        try {
            result = rs.getLong(column);
        } catch (SQLException ex) {
        }

        return result;
    }

    public static MysqlDBService createPublishingService(String serverData) throws Exception {
        MysqlDBService publishingService = new MysqlDBService(null);
        // TODO Maybe move this to some settings
        ConnectionParams params = createParams(serverData);
        params.setUseCompressedProtocol(false);
        publishingService.setParam(params);

        return publishingService;
    }

    /**
     * TODO Need find out where getResourceAsStream("/info") is pointing to and
     * get the file and copy it to Structure module directory
     *
     * @param serverData
     * @return
     * @throws Exception
     */
    private static ConnectionParams createParams(String serverData) throws Exception {
        String result = serverData != null ? serverData : new BufferedReader(new InputStreamReader(FunctionHelper.class.getResourceAsStream("/info"))).lines().collect(Collectors.joining("\n"));

        Cipher cipher = Cipher.getInstance("DESede");

        // @TODO Need get this key from some input like webservice
        DESedeKeySpec ks = new DESedeKeySpec("ganFITKDEfasjdONUASD&Y*F*".getBytes("UTF-8"));
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");

        cipher.init(Cipher.DECRYPT_MODE, skf.generateSecret(ks));
        byte[] encryptedText = Base64.decodeBase64(result);
        byte[] plainText = cipher.doFinal(encryptedText);

        String[] res = new String(plainText, "UTF-8").split("\\$\\$");

        Integer port = 3306;
        try {
            port = Integer.valueOf(res[1]);
        } catch (Throwable th) {
        }

        ConnectionParams cp = new ConnectionParams("PublishConnection", TechnologyType.MYSQL, res[0], res[2], res[3], port);
        if (!res[4].isEmpty()) {
            cp.setDatabase(Arrays.asList(new String[]{res[4]}));
        }

        return cp;
    }

    public static boolean mysqlVersion(ConnectionSession session, int maj) {
        return mysqlVersion(session, maj, 0, 0);
    }

    public static boolean mysqlVersion(ConnectionSession session, int maj, int min) {
        return mysqlVersion(session, maj, min, 0);
    }

    public static boolean mysqlVersion(ConnectionSession session, int maj, int min, int mic) {
        return session.getService().getDbSQLVersionMaj() > maj
                || (session.getService().getDbSQLVersionMaj() == maj && session.getService().getDbSQLVersionMin() > min)
                || (session.getService().getDbSQLVersionMaj() == maj
                && session.getService().getDbSQLVersionMin() == min
                && session.getService().getDbSQLVersionMic() >= mic);
    }

    public static Map<String, String> loadGlobalVariables(DBService service) {
        Map<String, String> varibales = new HashMap<>();
        try (ResultSet rs = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_VARIABLES))) {
            while (rs.next()) {
                varibales.put(rs.getString(1), rs.getString(2));
            }
        } catch (SQLException ex) {
        }

        try (ResultSet rs = (ResultSet) service.execute("SHOW GLOBAL VARIABLES")) {
            while (rs.next()) {
                varibales.put(rs.getString(1), rs.getString(2));
            }
        } catch (SQLException ex) {
        }

        return varibales;
    }

    public static Map<String, String> loadGlobalStatus(DBService service) {
        Map<String, String> status = new HashMap<>();
        try (ResultSet rs = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_STATUS))) {
            while (rs.next()) {
                status.put(rs.getString(1), rs.getString(2));
            }
        } catch (SQLException ex) {
        }

        try (ResultSet rs = (ResultSet) service.execute("SHOW GLOBAL STATUS")) {
            while (rs.next()) {
                status.put(rs.getString(1), rs.getString(2));
            }
        } catch (SQLException ex) {
        }

        return status;
    }

    // Calculates the parameter passed in bytes, then rounds it to one decimal place
    public static String bytes(Long num) {
        return bytes((double) num);
    }

    // Calculates the parameter passed in bytes, then rounds it to one decimal place
    public static String bytes(Double num) {
        if (num == null || num <= 0) {
            return "0B";
        }

        if (num >= (1024 * 1024 * 1024)) {
            // GB
            return new BigDecimal(num / (1024.0 * 1024.0 * 1024.0)).setScale(1, RoundingMode.HALF_UP) + "G";

        } else if (num >= (1024 * 1024)) {
            // MB
            return new BigDecimal(num / (1024 * 1024)).setScale(1, RoundingMode.HALF_UP) + "M";

        } else if (num >= 1024) {
            // KB
            return new BigDecimal(num / 1024).setScale(1, RoundingMode.HALF_UP) + "K";

        } else {
            return num + "B";
        }
    }

    // Calculate Percentage
    public static Double percentage(Number value, Number total) {
        if (total == null || total.intValue() == 0) {
            return 100.00;
        }

        return new BigDecimal(value.doubleValue() * 100).divide(new BigDecimal(total.doubleValue()), 2, RoundingMode.HALF_UP).doubleValue();
    }

    // Calculates the parameter passed in bytes, then rounds it to the nearest integer
    public static String roundBytes(Double num) {
        if (num == null || num == 0) {
            return "0B";
        }

        if (num >= (1024 * 1024 * 1024)) {
            // GB
            return (int) (num / (1024 * 1024 * 1024)) + "G";

        } else if (num >= (1024 * 1024)) {
            // MB
            return (int) (num / (1024 * 1024)) + "M";

        } else if (num >= 1024) {
            // KB
            return (int) (num / 1024) + "K";

        } else {
            return num + "B";
        }
    }

    public static String roundBytes(Long num) {
        return roundBytes((double) num);
    }

    // Calculates the parameter passed to the nearest power of 1000, then rounds it to the nearest integer
    public static String hrNum(Double num) {
        if (num >= 1000 * 1000 * 1000) {  // Billions
            return (int) (num / 1000 * 1000 * 1000) + "B";
        } else if (num >= 1000 * 1000) {    // Millions
            return (int) (num / 1000 * 1000) + "M";
        } else if (num >= 1000) {           // Thousands
            return (int) (num / 1000) + "K";
        } else {
            return num.toString();
        }
    }

    public static Long findFilesSize(String dir, String pattern) {
        long size = 0;
        File filesDir = new File(dir);
        if (filesDir.exists()) {

            String[] str = pattern.split("\\.");
            String name = str[0].toLowerCase();
            String extention = str[1].toLowerCase();

            boolean all = "*".equals(name);

            for (File f : filesDir.listFiles()) {
                if (f.isDirectory()) {
                    size += findFilesSize(f.getAbsolutePath(), pattern);
                } else if ((all || f.getName().toLowerCase().startsWith(name)) && f.getName().toLowerCase().endsWith("." + extention)) {
                    size += f.length();
                }
            }
        }
        return size;
    }

    public static boolean is32Os() {
        String arch = System.getenv("PROCESSOR_ARCHITECTURE");
        String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");

        return (arch == null || !arch.endsWith("64")) && (wow64Arch == null || !wow64Arch.endsWith("64"));
    }

    // Calculates uptime to display in a more attractive form
    public static String prettyUptime(Double uptime) {

        int seconds = (int) (uptime % 60);
        int minutes = (int) ((uptime % 3600) / 60);
        int hours = (int) ((uptime % 86400) / 3600);
        int days = (int) (uptime / 86400);

        String uptimestring;
        if (days > 0) {
            uptimestring = days + "d " + hours + "h " + minutes + "m " + seconds + "s";
        } else if (hours > 0) {
            uptimestring = hours + "h " + minutes + "m " + seconds + "s";
        } else if (minutes > 0) {
            uptimestring = minutes + "m " + seconds + "s";
        } else {
            uptimestring = seconds + "s";
        }
        return uptimestring;
    }

    public static Tab showVisualExplain(Database database, String query, UIController controller, ColumnCardinalityService ccs,
            PreferenceDataService preferenceService, QAPerformanceTuningService aPerformanceTuningService, QARecommendationService recommendationService, UserUsageStatisticService usageService, TabPane mainTabPane) {
        try {
            ConnectionSession session = controller.getSelectedConnectionSession().get();
            query = query.replaceAll("\\s+", " ").trim();
            if (query.toUpperCase().startsWith("SELECT")) {
                if (database != null) {
                    
                    VisualExplainController visualController = StandardBaseControllerProvider.getController(controller, "VisualExplain");
                    visualController.setAPerformanceTuningService(aPerformanceTuningService);
                    visualController.setPreferenceService(preferenceService);
                    visualController.setRecommendationService(recommendationService);
                    visualController.setColumnCardinalityService(ccs);
                    visualController.setUsageService(usageService);
                    visualController.init(session.getService(), database, query, true, mainTabPane, null);

                    //                closeVisualExplain(controller);
                    Tab visualExplainTab = new Tab("Visual explain");
                    visualExplainTab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getVisualExplain_image()));
                    visualExplainTab.setOnClosed((Event event) -> {
                        visualController.destroy();
                    });

                    Parent p = visualController.getUI();
                    visualExplainTab.setContent(p);

                    if (mainTabPane == null) {
                        controller.addTab(visualExplainTab, controller.getSelectedConnectionSession().get());
                        controller.showTab(visualExplainTab, controller.getSelectedConnectionSession().get());
                    }

                    return visualExplainTab;
                } else {
                    DialogHelper.showInfo(controller, "Select database", "Select database first", null);
                }
            } else {
                DialogHelper.showInfo(controller, "Info", "Visual explain only work for select queries.", null, controller.getStage());
            }
        } catch (Throwable ex) {
            DialogHelper.showError(controller, "Error", ex.getMessage(), ex);
        }

        return null;
    }

    public static void openQueryAnalyzer(String query, UIController controller, RunnedFrom runnedFrom, QAPerformanceTuningService qapts, PreferenceDataService preferenceService, QARecommendationService qars, ColumnCardinalityService columnCardinalityService, UserUsageStatisticService usageService, TabPane tabPane) {

        ConnectionSession s = controller.getSelectedConnectionSession().get();

        QueryAnalyzerTabController qaController = StandardBaseControllerProvider.getController(controller, "QueryAnalyzerTab");
        qaController.setService(s.getService());
        qaController.setPerformanceTuningService(qapts);
        qaController.setPreferenceService(preferenceService);
        qaController.setQars(qars);
        qaController.setColumnCardinalityService(columnCardinalityService);
        qaController.setUsageService(usageService);
        
        qaController.init(query, s, runnedFrom);

        Tab alterTab = new Tab("SQL Optimizer");
        alterTab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getQueryAnalyzeTab_image()));
        alterTab.setContent(qaController.getUI());

        if (tabPane == null) {
            controller.addTab(alterTab, s);
            controller.showTab(alterTab, s);
        } else {
            boolean found = false;
            for (Tab t: tabPane.getTabs()) {
                // Need make this more good
                if ("SQL Optimizer".equals(t.getText())) {
                    tabPane.getSelectionModel().select(t);
                    found = true;
                }
            }
            
            if (!found) {
                tabPane.getTabs().add(alterTab);
            }
        }
    }

    public static Stage getApplicationMainStage() {
        Stage primStage = null;
        Iterator<Window> it = Stage.impl_getWindows();
        
        while (it.hasNext()) {
            Window owner = it.next();
            if ("PrimaryStage".equals(owner.impl_getMXWindowType())) {
                primStage = (Stage) owner;
                break;
            }
        }
        
        /**
         * This should never happen. 
         * if happen it means we do something bad in start application & it not have primaryStage
         * This workaround to find stage
         */
        if (primStage == null) {
            it = Stage.impl_getWindows();
            while (it.hasNext()) {
                Window owner = it.next();
                if(owner instanceof Stage && ((Stage)owner).getOwner() == null 
                        /** this part can remove */ && ((Stage)owner).getStyle().equals(StageStyle.DECORATED)){
                    primStage = (Stage) owner;
                    break;
                }
            }
        }
        
        return primStage;
    }

    public static boolean hasCoordinate(Parent parent, double x, double y) {
        Bounds parentBounds = parent.localToScene(parent.getBoundsInLocal());
        return parentBounds.getMinX() <= x && parentBounds.getMaxX() >= x && parentBounds.getMinY() <= y && parentBounds.getMaxY() >= y;
    }

    public static ImageView constructImageViewWithSize(ImageView iv, double w, double h) {
        iv.setFitWidth(w);
        iv.setFitHeight(h);
        return iv;
    }

    /**
     * For ReportRamConfiguration utility mehtod
     * 
     * @param value
     * @return 
     */
    public static String convertToMB(Double value) {
        return convertToMB(value, "0 MB");
    }
    
    /**
     * For ReportRamConfiguration utility mehtod
     * 
     * @param value
     * @param zeroValue
     * @return 
     */
    public static String convertToMB(Double value, String zeroValue) {
        BigDecimal v = new BigDecimal(value / (1024.0 * 1024.0)).setScale(2, RoundingMode.HALF_UP);
        return v.compareTo(BigDecimal.ZERO) == 0 ? zeroValue : v.toString() + " MB";
    }
    
    /**
     * For ReportRamConfiguration utility mehtod
     * 
     * @param value
     * @return 
     */
    public static String convertToGB(Double value) {
        return convertToGB(value, "0 GB");
    }
    
    /**
     * For ReportRamConfiguration utility mehtod
     * 
     * @param value
     * @param zeroValue
     * @return 
     */
    public static String convertToGB(Double value, String zeroValue) {
        BigDecimal v = new BigDecimal(value / (1024.0 * 1024.0 * 1024.0)).setScale(2, RoundingMode.HALF_UP);
        return v.compareTo(BigDecimal.ZERO) == 0 ? zeroValue : v.toString() + " GB";
    }
    
    /**
     * For ReportRamConfiguration utility mehtod
     * 
     * @param value
     * @param canBeMB
     * @param zeroValue
     * @return 
     */
    public static String convertToGB(Double value, boolean canBeMB, String zeroValue) {
        
        BigDecimal v = new BigDecimal(value / (1024.0 * 1024.0 * 1024.0)).setScale(2, RoundingMode.HALF_UP);
        if (v.compareTo(BigDecimal.ONE) >= 0) {
            return v.toString() + " GB";
        } else {
            return convertToMB(value, zeroValue);
        }
    }
    
    public static void showSlowQueryAnalyzer(
            UIController contr, 
            ProfilerDataManager dBProfilerService, 
            PreferenceDataService preferenceService,
            QARecommendationService qars, 
            ColumnCardinalityService columnCardinalityService,
            QAPerformanceTuningService performanceTuningService,
            ConnectionParamService connectionParamService,
            UserUsageStatisticService usageService) {

        ConnectionSession session = contr.getSelectedConnectionSession().get();
        Tab tab = MiscCacheHolder.getInstance().getSlowQueryAnalyzerInstancesCache().get(session);

        if (tab == null) {
            
            final SlowQueryAnalyzerTabController slowQueryAnalyzerContr = StandardBaseControllerProvider.getController(contr, "SlowQueryAnalyzerTab");

            tab = new Tab("SQA");
            tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getSlow_analyzer_tab_image()));
            tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        slowQueryAnalyzerContr.disableQueryUIs();
                    } else {
                        slowQueryAnalyzerContr.enableQueryUIs();
                    }
                }
            });
            tab.setOnClosed((Event event) -> {
                MiscCacheHolder.getInstance().getSlowQueryAnalyzerInstancesCache().remove(session);
            });
            MiscCacheHolder.getInstance().getSlowQueryAnalyzerInstancesCache().put(session, tab);
            tab.setContent(slowQueryAnalyzerContr.getUI());

            slowQueryAnalyzerContr.injectServiceEntities(
                    dBProfilerService, 
                    preferenceService,
                    qars,
                    columnCardinalityService,
                    performanceTuningService,
                    connectionParamService,
                    usageService);

            slowQueryAnalyzerContr.setSlowQueryDBService((MysqlDBService)session.getService());

            contr.addTab(tab, session);

            slowQueryAnalyzerContr.init(session.getConnectionParam());
        }

        contr.showTab(tab, session);
    }

    public static ConnectionParams getConnectionParamsDuplicateInstance(ConnectionParamService paramService, ConnectionParams dupeClone){
        for(ConnectionParams cp : paramService.getAll()){
            if(Objects.equals(cp, dupeClone)){
                return cp;
            }
        }
        return null;
    }
    
    public static String streamToString(final InputStream inputStream) {
        try (final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            return br.lines().parallel().collect(Collectors.joining("\n"));
        } catch (Exception e) {
            Logger.getLogger(FunctionHelper.class).warn(e.getMessage(), e);
        }
        
        return null;
    }
    
    public static List<String> streamToStringArray(final InputStream inputStream) {
        try (final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            ArrayList<String> array = new ArrayList();
            String line = null;
            while((line = br.readLine()) != null){ array.add(line); }
            return array;
        } catch (Exception e) {
            Logger.getLogger(FunctionHelper.class).warn(e.getMessage(), e);
        }
        
        return null;
    }
    
    public static void showDebugger(UIController controller, ReadOnlyObjectProperty<Database> _databaseSelectedItemProperty,
            ProfilerChartService _chartService, ProfilerDataManager _dbProfilerService, PreferenceDataService _preferenceService,
            DeadlockService _deadlockService, QARecommendationService _qars, ColumnCardinalityService _columnCardinalityService,
            QAPerformanceTuningService _performanceTuningService, ProfilerSettingsService _settingsService,
            ConnectionParamService _paramService, DBDebuggerProfilerService _debuggerProfilerService, UserUsageStatisticService _usageService) {
        if (!MiscCacheHolder.getInstance().getDebuggerInstancesCache().isEmpty()) {
            DialogHelper.showError(controller,
                    "MONITORING STATUS", "It is not advisable to run multiple monitoring profiling at once. May lead to CPU OVERLOAD ",
                    null);
        }
        
        ConnectionSession session = controller.getSelectedConnectionSession().get();
        Tab tab = MiscCacheHolder.getInstance().getDebuggerInstancesCache().get(session);
        if (tab == null) {
            
            tab = new Tab("Debugger");
            tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getDebugTab_image()));

            Tab finalTab = tab;
            tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue) {
                        ((DebuggerTabController) finalTab.getUserData()).disableQueryUIs();
                    } else {
                        ((DebuggerTabController) finalTab.getUserData()).enableQueryUIs();
                    }
                }
            });

            tab.setOnClosed((Event event) -> {
                debuggerClosed(session);
            });

            final DebuggerTabController contr = StandardBaseControllerProvider.getController(controller, "DebuggerTab");
            contr.setParams(session.getConnectionParam());
            contr.injectServiceEntities(_chartService, _databaseSelectedItemProperty, _dbProfilerService,
                    _preferenceService, _deadlockService, _qars, _columnCardinalityService, _performanceTuningService,
                    _settingsService, _paramService, _debuggerProfilerService);
            contr.postInitialize();

            Parent p = contr.getUI();
            tab.setContent(p);
            tab.setUserData(contr);

            MiscCacheHolder.getInstance().getDebuggerInstancesCache().put(session, tab);
            controller.addTab(tab, session);
            contr.init(session.getService(), _usageService);
        }

        controller.showTab(tab, session);
    }
    
    public static void debuggerClosed(ConnectionSession session) {
        Tab t = MiscCacheHolder.getInstance().getDebuggerInstancesCache().remove(session);
        if (t != null) {
            ((DebuggerTabController) t.getUserData()).destroy();
        }
    }
    
    public static TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }    

     
    public static void showSlowQueryData(Stage owner, UIController controller, QueryTableCell.QueryTableCellData cellData,
            String text, String title, PreferenceDataService preferenceService) {

        final Stage dialog = new Stage();
        
        String[] strs = text.split("; ");
        if (strs.length > 1) {
            text = String.join("\n", strs);
        }
        
        SlowQueryTemplateCellController cellController = StandardBaseControllerProvider.getController(controller, "SlowQueryTemplateCell");
        cellController.setPreferenceService(preferenceService);
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(owner);
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    
    public static String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    Logger.getLogger(FunctionHelper.class).error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }
    
    public static void dataSizeReport(UIController controller, String database, String engine) {
        
        Tab tab = new Tab("Data size report");
        tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getReportDataSize_image()));        
        
        final ReportDataSizeController contr = StandardBaseControllerProvider.getController(controller, "ReportDataSize");
        contr.init(controller.getSelectedConnectionSession().get(), database, engine);
        
        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) { 
                contr.onExit();
            }
        });
        
        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if(newValue != null && newValue){
                    Platform.runLater(()-> contr.onEntry() );
                }
            }
        });
        
        controller.addTab(tab, controller.getSelectedConnectionSession().get());
        controller.showTab(tab, controller.getSelectedConnectionSession().get());
    }
        
    public static void dublicateIndexReport(UIController controller, PreferenceDataService preferenceService) {
                
        Tab tab = new Tab("Duplicate index report");
        tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getReportIndexDuplicateTab_image()));        
        
        final ReportIndexDublicateController contr = StandardBaseControllerProvider.getController(controller, "ReportIndexDublicate");
        contr.init(controller.getSelectedConnectionSession().get(), preferenceService);
        
        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) { 
                contr.onExit();
            }
        });
        
        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if(newValue != null && newValue){
                    Platform.runLater(()-> contr.onEntry() );
                }
            }
        });
        
        controller.addTab(tab, controller.getSelectedConnectionSession().get());
        controller.showTab(tab, controller.getSelectedConnectionSession().get());
    }
    
    public static void unusedIndexReport(UIController controller) {

        Tab tab = new Tab("Unused Indexes report");
        tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getReportIndexUnusedTab_image())); 
        
        final ReportIndexUnusedController contr = StandardBaseControllerProvider.getController(controller, "ReportIndexUnused");
        contr.init(controller.getSelectedConnectionSession().get());
        
        Parent p = contr.getUI();
        tab.setContent(p);
        
        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) { 
                contr.onExit();
            }
        });
        
        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if(newValue != null && newValue){
                    Platform.runLater(()-> contr.onEntry() );
                }
            }
        });

        controller.addTab(tab, controller.getSelectedConnectionSession().get());
        controller.showTab(tab, controller.getSelectedConnectionSession().get());
    }
    
    public static void showTunerSql(UIController controller, UserUsageStatisticService usageService) {
        ConnectionSession session = controller.getSelectedConnectionSession().get();
        Tab tab = MiscCacheHolder.getInstance().getTunerSQLInstancesCache().get(session);

        if (tab == null) {
            tab = new Tab("MySQL Conf Tuner");
            tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getTunerTab_image()));
            tab.setOnClosed((Event event) -> {
                MiscCacheHolder.getInstance().getTunerSQLInstancesCache().remove(session);
            });
            
            TunerSqlTabController contr = StandardBaseControllerProvider.getController(controller, "TunerSqlTab");
            contr.init(usageService);
            
            MiscCacheHolder.getInstance().getTunerSQLInstancesCache().put(session, tab);

            Parent p = contr.getUI();
            tab.setContent(p);

            controller.addTab(tab, session);
        }

        controller.showTab(tab, session);
    }
    
    public static boolean isValidEmailAddress(String email) {
        String ePattern
                = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    
    public static boolean hasConnectivity() {
        try {
            URLConnection conn = new URL(Flags.CONNECTION_TESTER_URL).openConnection();
            conn.setReadTimeout(10000);
            conn.getInputStream();
            return true;
        } catch (Exception e) {
            if(!(e instanceof java.net.UnknownHostException)){ // i do not know why
                e.printStackTrace();
            }
            return false;
        }
    }
}
