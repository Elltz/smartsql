package np.com.ngopal.smart.sql.structure.utils;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class URLUtils {
    
    public static final String SEARCH__GOOGLE_LINK         = "https://www.google.com/webhp?#q=";
    public static final String SEARCH__STACKOVERFLOW_LINK  = "http://stackoverflow.com/search?q=";
    public static final String SEARCH__YAHOO_LINK          = "https://search.yahoo.com/search?p=";
    public static final String SEARCH__BING_LINK           = "https://www.bing.com/search?q=";

    public static void openUrl(String uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URI(uri));
            } catch (URISyntaxException | IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }
    
    public static void openSearchUrl(String uri, String searchKey) {
        try {
            URLUtils.openUrl(uri + URLEncoder.encode(searchKey != null ? searchKey.replaceAll("\\s", " ") : "", "UTF-8"));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
    
}
