package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import np.com.ngopal.smart.sql.model.Database;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
public class SyncCompareResult {

    private final Database sourceDatabase;
    private final String sourceHost;
    
    private final Database targetDatabase;
    private final String targetHost;
    
    private final List<SyncCompareResultItem> onlyInSourceList = new ArrayList<>();
    private final List<SyncCompareResultItem> onlyInTargetList = new ArrayList<>();
    private final List<SyncCompareResultItem> differentList = new ArrayList<>();
    private final List<SyncCompareResultItem> equalList = new ArrayList<>();
    
    public SyncCompareResult(Database sourceDatabase, String sourceHost, Database targetDatabase, String targetHost) {
        this.sourceDatabase = sourceDatabase;
        this.sourceHost = sourceHost;
        this.targetDatabase = targetDatabase;
        this.targetHost = targetHost;
    }
    
    public void addOnlyInSource(SyncCompareResultItem item) {
        onlyInSourceList.add(item);
    }
    
    public void addOnlyInTarget(SyncCompareResultItem item) {
        onlyInTargetList.add(item);
    }
    
    public void addDifferent(SyncCompareResultItem item) {
        differentList.add(item);
    }
    
    public void addEquals(SyncCompareResultItem item) {
        equalList.add(item);
    }
}
