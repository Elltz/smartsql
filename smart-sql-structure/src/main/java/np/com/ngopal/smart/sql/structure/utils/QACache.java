package np.com.ngopal.smart.sql.structure.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.QARecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.QueryOptimizer;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.QOIndexRecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.providers.UIProvider;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QACache {

    private QACache() {
    }
    
//    @Getter
//    public static class QARecommendationCache {
//        String database;
//        String query;
//        String smartSqlQuery;
//        String optimizedQuery;
//        boolean emptyWithOr = false;
//        boolean hasAnalyzerResults = false;
//        Long rowsMulti;
//        
//        boolean hasCredits = true;
//        
//        boolean error = false;
//        
//        Map<String, List<String>> recommendations = new HashMap<>();
//        Map<String, List<QAIndexRecommendation>> indexes = new HashMap<>();
//        
//        public static QARecommendationCache emptyError(String query, String database) {
//            QARecommendationCache c = new QARecommendationCache();
//            c.database = database;
//            c.query = query;
//            c.recommendations.put(null, Arrays.asList(new String[] {"Unable to explain the query"}));
//            c.error = true;
//            
//            return c;
//        }
//    }
//    
//    @Getter
//    public static class QAIndexRecommendation {
//        String indexName;
//        String indexQuery;
//        String recommendation;
//        Long cachedRows;
//    }
    
    private static final Pattern INDEX_PATTERN = Pattern.compile("`idx_.*?`\\((.*)\\);");
    
    public static QORecommendation getQARecommandation(UIController baseController, QARecommendationService qars, ColumnCardinalityService columnCardinalityService, QAPerformanceTuningService performanceTuningService, Database database, String query, boolean withOutCaching, boolean quite, boolean fullUsage, boolean fullUsageExipred, boolean showExpiredInfo) throws SQLException {
        return getQARecommandation(baseController, qars, columnCardinalityService, performanceTuningService, database, query, withOutCaching, 4, 16, quite, fullUsage, fullUsageExipred, showExpiredInfo);
    }
    
    public static QORecommendation getQARecommandation(UIController contorller, QARecommendationService qars, ColumnCardinalityService columnCardinalityService, QAPerformanceTuningService performanceTuningService, Database database, String query, boolean withOutCaching, int indexMaxNoCount, int indexSubstrLength, boolean quite, boolean fullUsage, boolean fullUsageExipred, boolean showExpiredInfo) throws SQLException {
        ConnectionSession session = contorller.getSelectedConnectionSession().get();

        database = DatabaseCache.getInstance().getDatabase(session.getConnectionParam().cacheKey(), database.getName());
        if (database == null) {
            database = DatabaseCache.getInstance().syncDatabase(database.getName(), session.getConnectionParam().cacheKey(), (AbstractDBService) session.getService());
        }
        
        QORecommendation recommendation = new QORecommendation();
        recommendation.setDatabase(database.getName());
        
        if (!withOutCaching) {
            List<QARecommendation> recommendationList = qars.findRecommendations(session.getConnectionParam().getId(), database.getName(), query);
            if (recommendationList != null && !recommendationList.isEmpty()) {

                boolean needReAnalyze = false;
                // need check if all recommendation actual
                Set<String> processedTables = new HashSet<>();
                for (QARecommendation pt: recommendationList) {
                    
                    recommendation.setOptimizedQuery(pt.getOptimizednQuery());
                    recommendation.setSmartSqlQuery(pt.getRewritenQuery());
                    recommendation.setEmptyWithOr(pt.isEmptyWithOr());
                    recommendation.setHasAnalyzerResults(pt.isHasAnylyzers());
                    recommendation.setQuery(pt.getQuery());
                    recommendation.setRowsMulti(pt.getRowsMulti());
                    
                    if (!processedTables.contains(pt.getDbtable())) {
                        processedTables.add(pt.getDbtable());

                        // No recommendation (no need any index)
                        if (AnalyzerResult.EMPTY_OR.equals(pt.getRecommendation()) || AnalyzerResult.NO_IMPROVEMENTS.equals(pt.getRecommendation())) {

                            List<String> recommendations = recommendation.getRecommendations().get(pt.getDbtable());
                            if (recommendations == null) {
                                recommendations = new ArrayList<>();
                                recommendation.getRecommendations().put(pt.getDbtable(), recommendations);
                            }

                            recommendations.add(pt.getRecommendation());

                        } else {

                            boolean foundIndex = false;

                            if (pt.getQaSqlIndexQuery() != null && !pt.getQaSqlIndexQuery().isEmpty()) {
                                Matcher matcher = INDEX_PATTERN.matcher(pt.getQaSqlIndexQuery());
                                if (matcher != null) {
                                    while (matcher.find()) {
                                        String group = matcher.group(1);    
                                        if (group != null) {
                                            List<String> columns = new ArrayList<>();
                                            String[] strs = group.split(",");
                                            for (String s: strs) {
                                                // remove 'desc'
                                                s = s.trim();
                                                s = s.split("\\s+")[0].trim();
                                                // remove '(16)'
                                                if (s.endsWith("(16)")) {
                                                    s = s.substring(0, s.length() - 4).trim();
                                                }
                                                
                                                if (s.startsWith("`")) {
                                                    s = s.substring(1, s.length());
                                                }
                                                
                                                if (s.endsWith("`")) {
                                                    s = s.substring(0, s.length() - 1);
                                                }

                                                columns.add(s);
                                            }

                                            DBTable table = database.getTable(pt.getDbtable());
                                            if (table != null) {    
                                                for (Index tableIndex: table.getIndexes()) {

                                                    if (tableIndex.getColumns() != null && !tableIndex.getColumns().isEmpty() && columns.size() <= tableIndex.getColumns().size()) {

                                                        boolean f = true;
                                                        for (int i = 0; i < columns.size(); i++) {                        

                                                            String c0 = columns.get(i);
                                                            Column c1 = tableIndex.getColumns().get(i);

                                                            if (!c0.equalsIgnoreCase(c1.getName())) {
                                                                f = false;
                                                                break;
                                                            }
                                                        }

                                                        if (f) {
                                                            foundIndex = true;
                                                            break;
                                                        }
                                                    }
                                                }

                                                if (foundIndex) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (!foundIndex) {
                                if (pt.getSqlIndexQuery() == null && pt.getQaSqlIndexQuery() != null) {
                                    needReAnalyze = true;
                                    break;
                                } else {
                                    List<String> recommendations = recommendation.getRecommendations().get(pt.getDbtable());
                                    if (recommendations == null) {
                                        recommendations = new ArrayList<>();
                                        recommendation.getRecommendations().put(pt.getDbtable(), recommendations);
                                    }
                                    recommendations.add(pt.getQaRecommendation());
                                    
                                    if (pt.getQaSqlIndexQuery() != null) {
                                        List<QOIndexRecommendation> indexes = recommendation.getIndexes().get(pt.getDbtable());
                                        if (indexes == null) {
                                            indexes = new ArrayList<>();
                                            recommendation.getIndexes().put(pt.getDbtable(), indexes);
                                        }

                                        QOIndexRecommendation ir = new QOIndexRecommendation();
                                        ir.setIndexName(pt.getIndexName());
                                        ir.setIndexQuery(pt.getQaSqlIndexQuery());
                                        ir.setRecommendation(pt.getQaRecommendation());

                                        indexes.add(ir);
                                    }
                                }
                            }
                        }
                    }
                }

                if (needReAnalyze) {
                    recommendation = new QORecommendation();
                } else {
                    
                    if (!licenseCheck(recommendation, fullUsage, fullUsageExipred, showExpiredInfo)) {
                        recommendation.setHasCredits(false);
                    }
                    
                    return recommendation;
                }
            }
        }
        
//        QueryAnalyzer qa = new QueryAnalyzer(columnCardinalityService, database, session.getConnectionParam(), session.getService(), query, indexMaxNoCount, indexSubstrLength);
//        boolean stopedAnalyze = !qa.analyze(quite ? null : contorller, !withOutCaching);
        
        boolean hasCredit = hasCredit(contorller, fullUsage, fullUsageExipred, showExpiredInfo);
        
        recommendation = QueryOptimizer.analyzeQuery(
            QODataProvider.create(
                session.getService(), 
                columnCardinalityService, 
                qars, 
                performanceTuningService, 
                session.getConnectionParam(),
                hasCredit), 
            quite ? null : new UIProvider() {
                @Override
                public boolean showConfirm(String message) {
                    DialogResponce dr = DialogHelper.showConfirm(contorller, "SQL Optimizer", message, contorller.getStage(), false);
                    if (dr == DialogResponce.CANCEL || dr == DialogResponce.NO || dr == DialogResponce.NO_TO_ALL) {
                        return false;
                    }
                    
                    return true;
                }

                @Override
                public void showInfo(String message) {
                    DialogHelper.showInfo(contorller, "Info", message, null);
                }
            },
            database.getName(), 
            query, 
            indexMaxNoCount,
            indexSubstrLength,
            !withOutCaching            
        );
//        
//        if (!recommendation.) {            
//            // clear old
//            qars.removeRecommendations(session.getConnectionParam().getId(), database.getName(), query);
//            
//            recommendation.smartSqlQuery = qa.getSmartMySQLRewrittenQuery();
//            recommendation.optimizedQuery = qa.getOptimizerQuery();
//            recommendation.query = query;
//            recommendation.rowsMulti = qa.getRowsMulty();
//
//            List<AnalyzerResult> analyzerResults = new ArrayList<>();
//            if (!hasAnalyzerRows(qa.getAnalyzerResult())) {
//
//                String tableName = "UNKNOWN";
//                List<ExplainResult> qrs = qa.getvQueryExplainResults();
//                if (qrs == null) {
//                    qrs = qa.getVNQueryExplainResults();
//                }
//
//                if (qrs != null && !qrs.isEmpty()) {
//                    ExplainResult er = qrs.get(0);
//                    tableName = er.getTable();
//                }
//
//                recommendation.emptyWithOr = checkEmptyWithOr(qa.getAnalyzerResult());
//                recommendation.hasAnalyzerResults = false;
//                
//                AnalyzerResult ar = recommendation.emptyWithOr ? new AnalyzerResult(database.getName(), tableName, query, AnalyzerResult.EMPTY_OR) : new AnalyzerResult(database.getName(), tableName, query);
//                ar.setQueryAnalyzer(qa);
//                analyzerResults.add(ar);
//                
//            } else {
//                
//                recommendation.emptyWithOr = false;
//                recommendation.hasAnalyzerResults = true;
//
//                analyzerResults.addAll(qa.getAnalyzerResult());
//            }
//            
//            // save static for all recommendations
//            List<AnalyzerResult.RecommendationResult> listRecs = collectAnalyzerRows(analyzerResults);
//
//            if (listRecs != null) {
//                for (AnalyzerResult.RecommendationResult rr: listRecs) {
//                    
//                    if (rr.getDatabaseName() != null) {
//                        if (rr.getRecommendation() != null && !rr.isEmpty()) { 
//                            QAPerformanceTuning qat = performanceTuningService.saveStatistic(
//                                session.getConnectionParam().getId(), 
//                                rr.getDatabaseName(), 
//                                rr.getTableName(), 
//                                query, 
//                                rr.getRecommendation(), 
//                                rr.getSqlCommand(), 
//                                rr.getParent().getCachedRows(rr.getTableName()),
//                                rr.getSqlCommand() != null && hasCredit
//                            );
//
//                            rr.setQAPerformanceTuning(qat);
//                        }
//
//                        qars.save(
//                            new QARecommendation(
//                                session.getConnectionParam().getId(), 
//                                rr.getDatabaseName(), 
//                                query, 
//                                rr.getTableName(), 
//                        
//                                rr.isEmpty() ? null : rr.getRecommendation(), 
//                                rr.getIndexName(),
//                                rr.isEmpty() ? null : rr.getSqlCommand(), 
//
//                                recommendation.smartSqlQuery,
//                                recommendation.optimizedQuery,
//                                recommendation.emptyWithOr,
//                                recommendation.hasAnalyzerResults,
//
//                                rr.getRecommendation(), 
//                                rr.getSqlCommand(),
//                                rr.getParent().getCachedRows(rr.getTableName()),
//                                qa.getRowsMulty()
//                            )
//                        );
//
//                        if (!rr.isEmpty() && rr.getRecommendation() != null && !rr.getRecommendation().isEmpty()) {
//                            List<String> recommendations = recommendation.recommendations.get(rr.getTableName());
//                            if (recommendations == null) {
//                                recommendations = new ArrayList<>();
//                                recommendation.recommendations.put(rr.getTableName(), recommendations);
//                            }
//
//                            recommendations.add(rr.getRecommendation());
//                        }
//
//                        if (!rr.isEmpty() && rr.getSqlCommand() != null && !rr.getSqlCommand().isEmpty()) {
//                            List<QAIndexRecommendation> indexes = recommendation.indexes.get(rr.getTableName());
//                            if (indexes == null) {
//                                indexes = new ArrayList<>();
//                                recommendation.indexes.put(rr.getTableName(), indexes);
//                            }
//
//                            QAIndexRecommendation ir = new QAIndexRecommendation();
//                            ir.indexName = rr.getIndexName();
//                            ir.indexQuery = rr.getSqlCommand();
//                            ir.recommendation = rr.getRecommendation();
//                            ir.cachedRows = rr.getParent().getCachedRows(rr.getTableName());
//                            indexes.add(ir);
//                        }
//                    }
//                }
//            }
//        }
        
        if (!licenseCheck(recommendation, fullUsage, fullUsageExipred, false)) {
            recommendation.setHasCredits(false);
        }
        
        return recommendation;
    }
    
    private static boolean hasCredit(UIController controller, boolean fullUsage, boolean fullUsageExipred, boolean showExpiredInfo) {
        // License usage
        if (fullUsage) {
            if (fullUsageExipred) {
                if (LicenseManager.getInstance().creditsExpired(controller, showExpiredInfo)) {
                    return false;
                } else {                        
                    return true;
                }
            } else {
                return true;
            }
        } else {
            if (LicenseManager.getInstance().creditsExpired(controller, showExpiredInfo)) {
                return false;
            } else {                        
                return true;
            }
        }
    }
    
    private static boolean licenseCheck(QORecommendation recommendation, boolean fullUsage, boolean fullUsageExipred, boolean showExpiredInfo) {
        // License usage
        if (recommendation.getIndexes() != null && !recommendation.getIndexes().isEmpty()) {

            if (fullUsage) {
                if (fullUsageExipred) {
                    if (LicenseManager.getInstance().creditsExpired(null, showExpiredInfo)) {
                        maskRecommendations(recommendation);
                        return false;
                    } else {                        
                        LicenseManager.getInstance().usedCredit();
                    }
                }
            } else {
                if (LicenseManager.getInstance().creditsExpired(null, showExpiredInfo)) {
                    maskRecommendations(recommendation);
                    return false;
                } else {                        
                    LicenseManager.getInstance().usedCredit();
                }
            }
        }
        
        return true;
    }
    
    public static void maskRecommendations(QORecommendation recommendation) {
        for (String table: recommendation.getIndexes().keySet()) {
            List<QOIndexRecommendation> list = recommendation.getIndexes().get(table);
            if (list != null) {
                for (QOIndexRecommendation ir: list) {
                    
                    String oldName = ir.getIndexName();
                    if (ir.getIndexName() != null && ir.getIndexName().length() > 5) {
                        ir.setIndexName(ir.getIndexName().substring(0, 5) + "XXXX");
                    }
                    
                    if (ir.getIndexQuery() != null) {
                        ir.setIndexQuery(ir.getIndexQuery().replace(oldName, ir.getIndexName()));
                        
                        int idx_start = ir.getIndexQuery().indexOf("(");
                        int idx_end = ir.getIndexQuery().lastIndexOf(")");
                        
                        ir.setIndexQuery(ir.getIndexQuery().substring(0, idx_start + 1) + "XXXX" + ir.getIndexQuery().substring(idx_end));
                    }
                    
                    if (ir.getRecommendation() != null && ir.getRecommendation().contains("index on (")) {
                        int idx_start = ir.getRecommendation().indexOf("index on (");
                        int idx_end = ir.getRecommendation().lastIndexOf(")");
                        if (idx_start >= 0  && idx_end >= 0) {
                            ir.setRecommendation(ir.getRecommendation().substring(0, idx_start + 10) + "XXXX" + ir.getRecommendation().substring(idx_end));
                        }
                    }
                }
            }

            if (recommendation.getRecommendations() != null && !recommendation.getRecommendations().isEmpty()) {
                List<String> recs = recommendation.getRecommendations().get(table);
                for (int i = 0; i < recs.size(); i++) {
                    String rec = recs.get(i);
                    if (rec != null && rec.contains("index on (")) {
                        int idx_start = rec.indexOf("index on (");
                        int idx_end = rec.lastIndexOf(")");
                        if (idx_start >= 0  && idx_end >= 0) {
                            recs.set(i, rec.substring(0, idx_start + 10) + "XXXX" + rec.substring(idx_end));
                        }
                    }
                }
            }
        }
    }
    
    public static String maskIndex(String indexName) {
        if (indexName != null && indexName.length() > 5) {
            return indexName.substring(0, 5) + "XXXX";
        }
        
        return indexName;
    }
    
    public static String maskIndexQuery(String indexQuery, String indexName) {
        if (indexQuery != null) {
            indexQuery = indexQuery.replace(indexName, maskIndex(indexName));

            int idx_start = indexQuery.indexOf("(");
            int idx_end = indexQuery.lastIndexOf(")");

            return indexQuery.substring(0, idx_start + 1) + "XXXX" + indexQuery.substring(idx_end);
        }
        
        return indexQuery;
    }
    
    public static String maskRecommendation(String recommendation) {
        if (recommendation != null && recommendation.contains("index on (")) {
            int idx_start = recommendation.indexOf("index on (");
            int idx_end = recommendation.lastIndexOf(")");
            if (idx_start >= 0  && idx_end >= 0) {
                return recommendation.substring(0, idx_start + 10) + "XXXX" + recommendation.substring(idx_end);
            }
        }
        
        return recommendation;
    }    
    
    
    private static List<AnalyzerResult.RecommendationResult> collectAnalyzerRows(List<AnalyzerResult> list) {
        
        List<AnalyzerResult.RecommendationResult> result = new ArrayList<>();
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.getRecomendationsSize() > 0) {

                    for (AnalyzerResult.RecommendationResult rr: t.getRecomendations()) {
                        result.add(rr);
                    }
                }

                if (t.getChildren() != null) {
                    result.addAll(collectAnalyzerRows(t.getChildren()));
                } else {
                    result.add(t.createEmptyRecomendation(t.getQuery()));
                }
            }
        }
        
        return result;
    }
    
    private static boolean checkEmptyWithOr(List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.isRecommendationEmptyWithOR()) {
                    return true;
                }

                if (t.getChildren() != null){
                    if (checkEmptyWithOr(t.getChildren())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    
    private static boolean hasAnalyzerRows(List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.getRecomendationsSize() > 0) {
                    return true;

                }

                if (t.getChildren() != null){
                    if (hasAnalyzerRows(t.getChildren())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
