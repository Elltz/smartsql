/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import java.util.regex.Pattern;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class Flags {

    public static byte CARET_POSITION_FROM_START = 0;
    public static byte CARET_POSITION_FROM_LINE = 1;

    public final static String SMART_SQL_APP = "SMARTSQL-APP";
    public final static String SMART_SQL_APP_ATTR_ENCRYPTION = "appEncryptionKey";
    public final static String SMART_SQL_APP_ATTR_VERSION = "version";
    public final static String SMART_SQL_APP_ATTR_PAID = "paid";
    public final static String SMART_SQL_APP_ATTR_GRACE_PERIOD = "grace-period";
    public final static String SMART_SQL_APP_ATTR_LAUNCH_DATE = "launch-time";
    public final static String SMART_SQL_APP_ATTR_AUTO_RESOTRE_SESSION = "auto-restore";
    public final static String SMART_SQL_APP_ATTR_AUTO_START_SERVER = "auto-start-server";
    
    public final static int SMART_SQL_APP_SOCKET_PORT_ID = 7172;

    public final static String USER_SERIAL_ID = "serialId";
    public final static String USER_EMAIL_ID = "emailId";
    public final static String USER_KEYTYPE = "keyType";
    public final static String USER_KEY = "keye";
    public final static String USER_KEY_DAYS = "keyDays";
    public final static String USER_COUNTRY = "country";
    public final static String USER_FULL_NAME = "name";

    public static final String SMARTSQL_MAIN_APP_JAR_FILENAME = "main_app";
    public static final String SMARTSQL_SCHEDULER_JAR_FILENAME = "ssj";
    public static final String SMARTSQL_UPDATE_UTILITY_JAR_FILENAME = "update_utility";

    public static final String SMARTSQL_PACKAGE_TYPE = "pack_type";
    //public static final String SMARTSQL_UPDATE = "update";
    public static final String SMARTSQL_LOG_FILE = "applogger.log";
    public static final String SMARTSQL_LOG_2_FILE = "updatelogger.log";
    public static final String SMARTSQL_YOUTUBE_LINK_FILE = "youtube_link";
    public static final String SMARTSQL_SPLASH_INDEX = "splash";
    public static final String SMARTSQL_LOCK_FILE = "lock.smartmysql";
    public static final String SMARTSQL_FILE_INFO = "app_info.config";
    public static final String SMARTSQL_PROJECT_BUILD_MODULE = "project_name";
    public static final String SMARTSQL_BUILD_TYPE = "buildType";
    public static final String SMARTSQL_BANNER_TEXT = "bannerText";
    public static final String SMARTSQL_YOUTUBE_TEXT = "youtubeText";
    public static final String SMARTSQL_ONLINE_SERVER = "onlineServer";
    public static final String SMARTSQL_SAVED_DATA_FILE = "data.json";

    public static final String UPDATE_SCRIPT_TASK_NAME = "SmartUpdaterTask";
    //public static final String DATA_STORAGE_DIR = "Saved_Data";
    public static final String DEPENDENCY_UPDATER_JAR_ID = "smart-sql-dependency-updater";

    //http://storage.googleapis.com/[BUCKET_NAME]/[OBJECT_NAME]
    public static final String LINK_QA_VERSION_FILE = "version_qa.json";
    public static final String LINK_PRODUCTION_VERSION_FILE = "version_production.json";

    public static final String TOPDBS_WEBSITE = "http://www.topdbs.in/";//http://www.topdbs.in/UserRegistration.php
    public static final String SMARTMYSQL__REGISTER = "https://www.smartmysql.com/registration";
    public static final String SMARTMYSQL__FORGOT = "https://www.smartmysql.com/resetpassword";
    public static final String SEARCH__GOOGLE_LINK = "https://www.google.com/webhp?#q=";
    public static final String SEARCH__STACKOVERFLOW_LINK = "http://stackoverflow.com/search?q=";
    public static final String SEARCH__YAHOO_LINK = "https://search.yahoo.com/search?p=";
    public static final String SEARCH__BING_LINK = "https://www.bing.com/search?q=";
    public static final String CONNECTION_TESTER_URL = "http://www.google.com";
    public static final String URL_DEV_MYSQL = "http://dev.mysql.com/doc/refman/8.0/en/error-messages-server.html";

    /*
    * The type of build (production / qa)
     */
    public static final byte MODE_QA_BUILD = 2;
    public static final byte MODE_PROD_BUILD = 1;

    /*
    * The package type, if it should recieve updates or not
     */
    public static final byte MODE_UPDATEABLE_PACKAGE = 2;
    public static final byte MODE_STANDALONE_PACKAGE = 1;

    public static final String METRIC_STYLE_CLASS = "black-theme-class";
    public static final String METRIC_INNERS_STYLE_CLASS = "lighter";

    public final static int MAXIMUM_NUMBER_OF_WAITING_CYCLE_COUNT = 15;
    public final static String METRIC_DELIMITER_TAGS = "_METRIC";

    public static final int DEFAULT_SCALE_INDEX = 3; // 1 hour

    public final static byte KILOBYTE = 0;
    public final static byte MEGABYTE = 1;
    public final static byte GIGABYTE = 2;
    public final static byte BYTES = 3;

    public final static byte CORE_APPLICATION_DIR = 1;
    public final static byte FRAGILE_DIR = 2;
    public final static byte IDENTIFICATION_FILE = 3;
    public final static byte FILE_INFO = 4;
    public final static byte DATA_FILE = 5;
    public final static byte LOG_FILE = 6;
    public final static byte LOG_2_FILE = 7;
    public final static byte YOUTUBE_LINK_FILE = 8;
    public final static byte LAST_UPDATE_FILE = 9;
    public final static byte CLEAR_H2_SQ_FILE = 10;
    public final static byte CLEAR_MYSQL_SQ_FILE = 11;
    public final static byte VALID_FILE = 12;
    public final static byte SPLASH_FILE = 13;
    
    //public final static byte SAVED_DATA_DIR = 7;

    public final static int SETTINGS_MYSQL_SERVER_STATUS = 0;
    public final static int SETTINGS_AUTO_START_MYSQL_ON_SYSTEM = 1;
    public final static int SETTINGS_AUTO_RESTORE_LAST_KNOWN_SETTINGS = 2;
    public final static int SETTINGS_EMAIL = 3;
    public final static int SETTINGS_NAME = 4;
    public final static int SETTINGS_KEYTYPE = 5;
    public final static int SETTINGS_MYSQL_SERVER_NAME = 6;
    public final static int SETTINGS_PAYMENT_LEVEL = 7;
    public final static int SETTINGS_SAVE_DURATION_INTERVAL = 8;
    public final static int SETTINGS_RUN_APP_IN_FULL_SCREEN = 9;
    public final static int SETTINGS_APPLICATION_TEXT_FONT_SIZE = 10;
    public final static int SETTINGS_SHOW_AUTO_COMPLETE_IN_QUERY = 11;

    public final static int FOOTER_ELEMENT_PARENT = 0;
    public final static int FOOTER_ELEMENT_NAV_BUTTON = 1;
    public final static int FOOTER_ELEMENT_NOTIFICATION_SECTION = 2;
    public final static int FOOTER_ELEMENT_CONNECTION_SESSIONS_SECTION = 3;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_N0_OF_CON = 4;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA = 5;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_ROWS = 6;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_TOTAL_TIME = 7;
    public final static int FOOTER_ELEMENT_NOTIFICATION_UPDATE_FRONTLINE = 8;

    public final static String CONNECTION_SESSION_ELEMENT_NAME = "SESSION";
    public final static String CONNECTION_SESSION_ATTR_PARAM_ID = "paramID";
    public final static String CONNECTION_SESSION_ATTR_CON_TYPE = "conType";
    public final static String CONNECTION_SESSION_ATTR_OS_NAME = "os-name";//its not needed
    public final static String CONNECTION_SESSION_ATTR_SELECTED_MODULE_CLASSNAME = "slected-module";

    public final static String SCHEMA_DESIGNER_ELEMENT_NAME = "SCHEMA-DESIGNER";
    public final static String QUERY_BUILDER_ELEMENT_NAME = "QUERY-BUILDER";

    public final static String MANDATORY_ATTR_ID = "id";
    public final static String MANDATORY_ATTR_CLASS_HANDLER = "classHandler";

    public final static Byte LEFT_TREE_ANIMATION_TRANSLATE = 2;
    public final static Byte LEFT_TREE_ANIMATION_ONLY_LIGHT = 1;

    /**
     * PROFILER FLAGS & PATTERN VARIABLES
     */
    //-+\nSEMAPHORES\n-+\n(.*\n)+?-+
    public static final Pattern PATTERN__ROW_OPERATIONS = Pattern.compile("-+\\nROW OPERATIONS\\n-+\\n((.*\\n)+?)-+\\n");
    public static final Pattern PATTERN__ROW_OPERATIONS_0 = Pattern.compile("inserted\\s+(\\d+),\\s+updated\\s+(\\d+),\\s+deleted\\s+(\\d+),\\s+read\\s+(\\d+)");
    public static final Pattern PATTERN__ROW_OPERATIONS_1 = Pattern.compile("(\\d+(\\.\\d+)?)\\s+inserts/s,\\s+(\\d+(\\.\\d+)?)\\s+updates/s,\\s+(\\d+(\\.\\d+)?)\\s+deletes/s,\\s+(\\d+(\\.\\d+)?)\\s+reads/s");
    public static final Pattern PATTERN__ROW_OPERATIONS_2 = Pattern.compile("(\\d+)\\s+queries\\s+inside\\s+InnoDB,\\s+(\\d+)\\s+queries\\s+in\\s+queue");
    public static final Pattern PATTERN__ROW_OPERATIONS_3 = Pattern.compile("(\\d+)\\s+read\\s+views\\s+open\\s+inside\\s+InnoDB");
    public static final Pattern PATTERN__ROW_OPERATIONS_4 = Pattern.compile("\\sstate:\\s(.*)");

    public static final Pattern PATTERN__TRANSACTIONS = Pattern.compile("-+\\nTRANSACTIONS\\n-+\\n((.*\\n)+?)-+\\n");
    public static final Pattern PATTERN__TRANSACTIONS_0 = Pattern.compile("LOCK\\s+WAIT\\s+(\\d+)");
    public static final Pattern PATTERN__TRANSACTIONS_1 = Pattern.compile("History\\s+list\\s+length\\s+(\\d+)");

    public static final Pattern PATTERN__FILE_IO = Pattern.compile("-+\\nFILE I\\/O\\n-+\\n((.*\\n)+?)-+\\n");
    public static final Pattern PATTERN__FILE_IO_0 = Pattern.compile("log:\\s+(\\d+);");
    public static final Pattern PATTERN__FILE_IO_1 = Pattern.compile("(\\d+)\\s+OS\\s+file\\s+reads,\\s+(\\d+)\\s+OS\\s+file\\s+writes,\\s+(\\d+)\\s+OS\\s+fsyncs");

    public static final Pattern PATTERN__LOG = Pattern.compile("-+\\nLOG\\n-+\\n((.*\\n)+?)-+\\n");
    public static final Pattern PATTERN__LOG_0 = Pattern.compile("Pages\\s+flushed\\s+up\\s+to\\s+(\\d+)");

    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY = Pattern.compile("-+\\nBUFFER\\s+POOL\\s+AND\\s+MEMORY\\n-+\\n((.*\\n)+?)-+\\n");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_0 = Pattern.compile("Pages\\s+read\\s+(\\d+),\\s+created\\s+(\\d+),\\s+written\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_1 = Pattern.compile("Buffer\\s+pool\\s+size\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_2 = Pattern.compile("Free\\s+buffers\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_3 = Pattern.compile("Database\\s+pages\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_4 = Pattern.compile("Modified\\s+db\\s+pages\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_5 = Pattern.compile("Buffer\\s+pool\\s+hit\\s+rate\\s+(\\d+\\s+/\\s+\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_6 = Pattern.compile("Total\\s+large\\s+memory\\s+allocated\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_7 = Pattern.compile("Additonal\\s+pool\\s+allocated\\s+(\\d+)");
    public static final Pattern PATTERN__BUFFER_POOL_AND_MEMORY_8 = Pattern.compile("(\\d+(\\.\\d+)?)\\s+reads/s,\\s+(\\d+(\\.\\d+)?)\\s+creates/s,\\s+(\\d+(\\.\\d+)?)\\s+writes/s");

    public static final Pattern PATTERN__INSERT_BUFFER = Pattern.compile("-+\\nINSERT\\s+BUFFER\\s+AND\\s+ADAPTIVE\\s+HASH\\s+INDEX\\n-+\\n((.*\\n)+?)-+");
    public static final Pattern PATTERN__INSERT_BUFFER_0 = Pattern.compile("(\\d+)\\s+merges");
    public static final Pattern PATTERN__INSERT_BUFFER_1 = Pattern.compile("merged\\s+operations:\\n\\s+insert\\s+(\\d+),\\s+delete\\s+mark\\s+(\\d+),\\s+delete\\s+(\\d+)");
    public static final Pattern PATTERN__INSERT_BUFFER_2 = Pattern.compile("Ibuf:\\s+size\\s+(\\d+),\\s+free\\s+list\\+len\\s+(\\d+),\\s+seg\\s+size\\s+(\\d+),\\s+(\\d+)\\s+merges");

    public static final Pattern PATTERN__SEMAPHORES = Pattern.compile("-+\\nSEMAPHORES\\n-+\\n((.*\\n)+?)-+");
    public static final Pattern PATTERN__SEMAPHORES_0 = Pattern.compile("RW-shared.*?OS\\s+waits\\s+(\\d+)");
    public static final Pattern PATTERN__SEMAPHORES_1 = Pattern.compile("RW-excl.*?OS\\s+waits\\s+(\\d+)");
    public static final Pattern PATTERN__SEMAPHORES_2 = Pattern.compile("RW-sx.*?OS\\s+waits\\s+(\\d+)");
    public static final Pattern PATTERN__SEMAPHORES_3 = Pattern.compile("RW-shared.*?rounds\\s+(\\d+)");
    public static final Pattern PATTERN__SEMAPHORES_4 = Pattern.compile("RW-excl.*?rounds\\s+(\\d+)");
    public static final Pattern PATTERN__SEMAPHORES_5 = Pattern.compile("RW-sx.*?rounds\\s+(\\d+)");

    public static final Pattern PATTERN__DEADLOCKS = Pattern.compile("-+\\nLATEST\\s+DETECTED\\s+DEADLOCK\\n-+\\n((.*\\n)+?)-+\\n");
    public static final Pattern PATTERN__DEADLOCKS_0 = Pattern.compile("\\*\\*\\*\\s+\\(\\d+\\)\\sTRANSACTION:");
    public static final Pattern PATTERN__DEADLOCKS_1 = Pattern.compile("\\*\\*\\*\\s+\\(1\\)\\sTRANSACTION:");

    public static final String DB_PROFILER__VARIABLE__ID = "id";
    public static final String DB_PROFILER__VARIABLE__USER = "user";
    public static final String DB_PROFILER__VARIABLE__HOST = "host";
    public static final String DB_PROFILER__VARIABLE__DB = "db";
    public static final String DB_PROFILER__VARIABLE__COMMAND = "command";
    public static final String DB_PROFILER__VARIABLE__TIME = "time";
    public static final String DB_PROFILER__VARIABLE__STATE = "state";
    public static final String DB_PROFILER__VARIABLE__INFO = "info";

    public static final Pattern PATTERN__FOREIGN_KEY_ERROR = Pattern.compile("-+\\nLATEST FOREIGN KEY ERROR\\n-+\\n((.*\\n)+?)-+");

    public static final Pattern PATTERN__INNO_TOP__STATE = Pattern.compile("query\\s+id\\s+\\d+\\s+(.+)", Pattern.CASE_INSENSITIVE);
    public static final Pattern PATTERN__INNO_TOP__STATUS = Pattern.compile("\\s+ACTIVE\\s+(\\d+)\\s+sec", Pattern.CASE_INSENSITIVE);
    public static final Pattern PATTERN__INNO_TOP__ID = Pattern.compile("MySQL\\s+thread\\s+id\\s+(\\d+)", Pattern.CASE_INSENSITIVE);
    public static final Pattern PATTERN__INNO_TOP__LOCK_STRUCT = Pattern.compile("(\\d+)\\s+lock\\s+struct\\(s\\)", Pattern.CASE_INSENSITIVE);
    public static final Pattern PATTERN__INNO_TOP__UNDO_LOG = Pattern.compile("undo\\s+log\\s+entries\\s+(\\d+)", Pattern.CASE_INSENSITIVE);
    public static final Pattern PATTERN__INNO_TOP__USER_STATE = Pattern.compile("query\\s+id\\s+\\d+\\s+(.+?)\\s+(.+?)\\s+(.+)", Pattern.CASE_INSENSITIVE);

    public static final Pattern PATTERN__ROLLBACK = Pattern.compile("\\*\\*\\* WE ROLL BACK TRANSACTION \\((\\d+)\\)");
    public static final Pattern DATABASE__ROLLBACK = Pattern.compile("`(.+)`\\.`.+`");

    public static final Pattern PATTERN__TIME0 = Pattern.compile("(\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2})");
    public static final Pattern PATTERN__TIME1 = Pattern.compile("(\\d{6}\\s+\\d+:\\d{2}:\\d{2})");

}
