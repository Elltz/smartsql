package np.com.ngopal.smart.sql.structure.models;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.EngineInnodbStatus;
import np.com.ngopal.smart.sql.model.GlobalStatusStart;
import np.com.ngopal.smart.sql.model.SlaveStatus;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class ProfilerData {

    double upper;
    double lower;
    
    Map<Long, Map<String, GlobalStatusStart>> listChangedGlobalStatus;
    
    Map<Long, SlaveStatus> listChangedSlaveStatus;
    
    Map<Long, Map<String, EngineInnodbStatus>> listChangedEngineInnodbStatus;
    
    
    List<Object[]> topQueries;
    List<Object[]> lockQueries;
    List<Object[]> slowQueries;
    List<Object[]> topTables;
    List<Object[]> topSchemas;
    List<Object[]> topUsers;
    List<Object[]> topHosts;
    List<Object[]> notUsingIndexes;
    List<Object[]> analyzerQueries;
}
