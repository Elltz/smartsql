package np.com.ngopal.smart.sql.structure.models;

import java.util.List;
import java.util.Map;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class SyncCompareResultItem {

    public enum Type {
        TABLE("Table", StandardDefaultImageProvider.getInstance().getTableTab_image()),
        VIEW("View", StandardDefaultImageProvider.getInstance().getViewTab_image()),
        PROCEDURE("Procedure", StandardDefaultImageProvider.getInstance().getProcTab_image()),
        FUNCTION("Function", StandardDefaultImageProvider.getInstance().getFunctionTab_image()),
        TRIGGER("Trigger", StandardDefaultImageProvider.getInstance().getTriggerTab_image()),
        EVENT("Event", StandardDefaultImageProvider.getInstance().getEventTab_image());
        
        private final String name;
        private final Image image;
        Type(String name, Image image) {
            this.name = name;
            this.image = image;
        }

        @Override
        public String toString() {
            return name;
        }
        
        public Image getImage() {
            return image;
        }
    }
    
    public enum Operation {
        NONE("None", StandardDefaultImageProvider.getInstance().getOperation_None_image()),
        DROP("Drop", StandardDefaultImageProvider.getInstance().getOperation_Drop_image()),
        UPDATE("Update", StandardDefaultImageProvider.getInstance().getOperation_Update_image()),
        CREATE("Create", StandardDefaultImageProvider.getInstance().getOperation_Create_image());
        
        private final String name;
        private final Image image;
        Operation(String name, Image image) {
            this.name = name;
            this.image = image;
        }

        @Override
        public String toString() {
            return name;
        }
        
        public Image getImage() {
            return image;
        }
    }
    
    
    private Type type;
    private String source;
    private Object sourceObject;
    
    private boolean selected = true;
    private Operation operation;
    
    private String target;
    private Object targetObject;
    
    private String createSourceQuery;
    private String dropSourceQuery;
    
    private String createTargetQuery;
    private String dropTargetQuery;
    
    private Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diffs;
 
    
    public boolean containsFilterText(String text) {
        text = text.toLowerCase();
        if (type != null && type.toString().toLowerCase().contains(text) ||
            source != null && source.toLowerCase().contains(text) ||
            operation != null && operation.toString().toLowerCase().contains(text) ||
            target != null && target.toLowerCase().contains(text)) {
            return true;
        }
            
        return false;
    }
}
