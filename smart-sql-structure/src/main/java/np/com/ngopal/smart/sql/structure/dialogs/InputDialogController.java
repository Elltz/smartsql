package np.com.ngopal.smart.sql.structure.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
public class InputDialogController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private Label titleLabel;
    @FXML
    private Label inputLabel;
    @FXML
    private TextField inputField;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
    public void init(String titleText, String label) {
        init(titleText, label, "");
    }
    public void init(String titleText, String label, String initText) {
        titleLabel.setText(titleText);
        inputLabel.setText(label);
        inputField.setText(initText);
        inputField.setOnKeyPressed((KeyEvent event) -> {
            
            switch (event.getCode()) {
                
                case ENTER:
                    okAction(null);
                    break;
                
                case ESCAPE:
                    cancelAction(null);
                    break;                    
            }
        });
        
        calculateHeightFlow(titleLabel, 450.0);
    }
        
    @FXML
    public void okAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        responce = DialogResponce.CANCEL;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
    public DialogResponce getResponce() {
        return responce;
    }
    
    public String getInputValue() {
        return inputField.getText();
    }

    @Override
    public void initDialog(Object... params) {
        init((String)params[0], (String)params[1], (String)params[2]);
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }

}
