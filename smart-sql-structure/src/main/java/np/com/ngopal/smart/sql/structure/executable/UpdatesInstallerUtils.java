/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.executable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import np.com.ngopal.smart.sql.model.provider.StorageProvider;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public abstract class UpdatesInstallerUtils {
    
    private ProgressIndicator progressIndicator;    
    private Label titleLabel;
    private Label noticeLabel;
    
    private volatile boolean stopOperation = false;
    
    private final static String MSG_NO_UPDATE       = "You are using the latest version of Smart MYSQL";
    private final static String MSG_CHECK_UPDATE    = "Checking for updates...";
    private final static String MSG_INSTALL_UPDATE    = "Patching application updates... \n application will restart now";
    private final static String MSG_GET_UPDATE    = "Downloading updates...";
    private final static String MSG_GET_UPDATE_DONE    = "UPDATES DOWNLOADED SUCCESSFULLY";
    private final static String MSG_CHECK_ERROR     = "Can't process updates request. Server unavailable";
    private final static String MSG_FOUND_UPDATE    = "New update found - %s";
    private final static String MSG_ILLEGAL_STATE    = "Application file(s) integrity void (version : %s)";

    private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyy-MM-dd");
    
    public UpdatesInstallerUtils(ProgressIndicator progressIndicator, Label titleLabel, Label noticeLabel) {
        this.progressIndicator = progressIndicator;
        this.titleLabel = titleLabel;
        this.noticeLabel = noticeLabel;
    }
    
    public void checkForUpdates(){
        // only one time at the day
        boolean canUpdate = true;
        File f = StandardApplicationManager.getInstance().getSmartDataDirectory(Flags.LAST_UPDATE_FILE);
        if (f.exists()) {
            try {
                String res = new String(Files.readAllBytes(f.toPath()), "UTF-8");
                Date date = DATE_FORMAT.parse(res);
                
                Calendar c0 = Calendar.getInstance();
                c0.setTime(new Date());
                
                Calendar c1 = Calendar.getInstance();
                c1.setTime(date);
                
                if (c0.get(Calendar.YEAR) == c1.get(Calendar.YEAR) && c0.get(Calendar.MONTH) == c1.get(Calendar.MONTH) && c0.get(Calendar.DAY_OF_MONTH) == c1.get(Calendar.DAY_OF_MONTH)) {
                    canUpdate = false;
                } else {
                    try {
                        if (f.exists()) {
                            f.delete();
                        }
                        Files.write(f.toPath(), DATE_FORMAT.format(new Date()).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                    } catch (Throwable ex) {
                        Logger.getLogger(StorageProvider.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            } catch (Throwable ex) {
                Logger.getLogger(UpdatesInstallerUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Files.write(f.toPath(), DATE_FORMAT.format(new Date()).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            } catch (Throwable ex) {
                Logger.getLogger(StorageProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (!canUpdate) {
            return;
        }
        
        // in some reason if config deleted
        StandardApplicationManager.getInstance().downloadConfigIfNeed();
        
        getOperationFlagAndFalsifyIfTrue();
        WorkProc checkUpdatesWorkProc = new WorkProc() {
            boolean[] bools = new boolean[] {false, false};            
            @Override
            public void run() {
                if (FunctionHelper.hasConnectivity()) {
                    try {
                        if (StandardApplicationManager.getInstance().hasUnattendedUpdates()) {
                            bools[0] = false;
                            bools[1] = true;
                        } else {
                            bools = StandardApplicationManager.getInstance().checkForApplicationUpdates();
                        }
                        callUpdate = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Platform.runLater(() -> {
                            progressIndicator.setProgress(0.0);
                            setTextIfLabelNonNull(titleLabel, MSG_CHECK_ERROR);
                            onUpdatesCheckFailed();
                        });
                    }
                }
            }

            @Override
            public void updateUI() {
                super.updateUI();
                progressIndicator.setProgress(1.0);
                String version = StandardApplicationManager.getInstance().getServerVersion();
                if (version == null || version.trim().isEmpty()) {
                    version = StandardApplicationManager.getInstance().getSmartsqlSavedData().getVersion();
                }
                if(bools[0]){
                    setTextIfLabelNonNull(titleLabel, String.format(MSG_FOUND_UPDATE, version));                    
                }else if(bools[1]){
                    setTextIfLabelNonNull(titleLabel, String.format(MSG_ILLEGAL_STATE, version));                    
                }else{
                    setTextIfLabelNonNull(titleLabel, MSG_NO_UPDATE);
                }
                onUpdatesCheckDone(bools[0], bools[1], version);                
            }
            
        };
        setTextIfLabelNonNull(titleLabel, MSG_CHECK_UPDATE);
        
        progressIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        Recycler.addWorkProc(checkUpdatesWorkProc);
        onUpdatesCheckStarted();
    }
    
    public void downloadRequiredUpdates() {
        getOperationFlagAndFalsifyIfTrue();
        setTextIfLabelNonNull(titleLabel, MSG_GET_UPDATE);
        if(StandardApplicationManager.getInstance().hasUnattendedUpdates()){
            setTextIfLabelNonNull(titleLabel, MSG_GET_UPDATE_DONE);
            onDownloadUpdatesDone();
        }else{
            WorkProc updatesDownloaderWorkProc = new WorkProc() {
                @Override
                public void run() {                
                    StringBuilder sb = new StringBuilder(500);
                    for (Map.Entry<String, String> en : StandardApplicationManager.getInstance().getFliteredNeededRemoteDependencies().entrySet()) {

                        if(getOperationFlagAndFalsifyIfTrue()){
                            setTextIfLabelNonNull(noticeLabel, "Cancelled Operation");
                            return;
                        }

                        try {
                            String hashedUpdateEntityName = StandardApplicationManager.getInstance().getMD5String(en.getKey()).substring(0, 10);
                            sb.append(System.lineSeparator())
                                    .append("-> Downloading ")
                                    .append(hashedUpdateEntityName);
                            Platform.runLater(() -> {
                                setTextIfLabelNonNull(noticeLabel, sb.toString());
                            });
                            ProgressIndicatorInterface pii = new ProgressIndicatorInterface() {
                                @Override
                                public void onStart(String what) {
                                }

                                @Override
                                public void onUpdate(long current, long total) {
                                    sb.delete(sb.lastIndexOf(System.lineSeparator()), sb.length())
                                            .append(System.lineSeparator())
                                            .append("-> Downloading ")
                                            .append(hashedUpdateEntityName)
                                            .append(" ( ")
                                            .append(String.valueOf(current))
                                            .append("/")
                                            .append(String.valueOf(total))
                                            .append(" )");
                                    Platform.runLater(() -> {
                                        setTextIfLabelNonNull(noticeLabel, sb.toString());
                                    });
                                }

                                @Override
                                public void onDone(File outfile) {
                                    sb.append(System.lineSeparator()).append("-> Downloaded ").append(hashedUpdateEntityName);
                                    Platform.runLater(() -> {
                                        setTextIfLabelNonNull(noticeLabel, sb.toString());
                                    });
                                }
                            };
                            
                            File parentDir = null;
                            if(en.getKey().equalsIgnoreCase(Flags.SMARTSQL_MAIN_APP_JAR_FILENAME) 
                                    || en.getKey().equalsIgnoreCase(Flags.SMARTSQL_SCHEDULER_JAR_FILENAME)
                                    || en.getKey().equalsIgnoreCase(Flags.SMARTSQL_UPDATE_UTILITY_JAR_FILENAME)){
                                parentDir = StandardApplicationManager.getInstance().getSmartDataDirectory(Flags.CORE_APPLICATION_DIR);
                            }else{
                                parentDir = new File(StandardApplicationManager.getInstance().getSmartDataDirectory(Flags.CORE_APPLICATION_DIR),
                                "lib");
                            }
                            File outFile = new File(parentDir, en.getValue().substring(en.getValue().lastIndexOf('/') + 1));
                            StandardApplicationManager.getInstance().streamDownloader(en.getValue(), outFile, pii);
                        } catch (Exception e) {
                            sb.append(System.lineSeparator()).append(e.getMessage());
                            Platform.runLater(() -> {
                                setTextIfLabelNonNull(noticeLabel, sb.toString());
                            });
                        }

                    }

                    callUpdate = true;
                }

                @Override
                public void updateUI() {
                    super.updateUI();
                    setTextIfLabelNonNull(titleLabel, MSG_GET_UPDATE_DONE);
                    onDownloadUpdatesDone();
                }

            };
            Recycler.addWorkProc(updatesDownloaderWorkProc); 
        }
    }
    
    public void installUpdates() {
        progressIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        setTextIfLabelNonNull(titleLabel, MSG_INSTALL_UPDATE);
        try {
            StandardApplicationManager.getInstance().callUpdaterScript();
            FunctionHelper.getApplicationMainStage().close();
            Platform.exit();
        } catch (Exception e) {
            e.printStackTrace();
            setTextIfLabelNonNull(noticeLabel, "ERROR APPLYING UPDATES");
            onApplyUpdatesFailed();
        }
    }
    
    private void setTextIfLabelNonNull(Label l, String text){
        if(l != null){
            l.setText(text);
        }
    }
    
    public void stopCurrentOperation(){
        synchronized(this){
            stopOperation = true;
        }
    }
    
    private boolean getOperationFlagAndFalsifyIfTrue(){
        boolean a;
        synchronized(this){
            a = stopOperation;
            if(stopOperation){
                stopOperation = false;
            }
        }
        return a;
    }
    
    public abstract void onUpdatesCheckDone(boolean needsUpdates, boolean needRestructuring, String version);
    public abstract void onDownloadUpdatesDone();
    public abstract void onUpdatesCheckFailed();
    public abstract void onUpdatesCheckStarted();
    public abstract void onApplyUpdatesFailed();
}
