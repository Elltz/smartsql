package np.com.ngopal.smart.sql.structure.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.application.Platform;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.LicenseDataService;
import np.com.ngopal.smart.sql.db.LicenseDataUsageService;
import np.com.ngopal.smart.sql.model.LicenseData;
import np.com.ngopal.smart.sql.model.LicenseDataUsage;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;
import org.apache.commons.codec.binary.Hex;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class LicenseManager {

    private static final String AES = "y#JG8cN-buWKL*Y7";
    
    private LicenseDataService licenseDataService;
    private LicenseDataUsageService licenseDataUsageService;
    
    private String email;
    private String license;
    private LicenseData licenseData;
    private LicenseDataUsage licenseDataUsage;
    
    private final SimpleDateFormat LAST_UPDATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private final SimpleDateFormat EXPIRE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private LicenseManager() {
    }
    
    private static LicenseManager instance;
    public static synchronized LicenseManager getInstance() {
        if (instance == null) {
            instance = new LicenseManager();
        }
        
        return instance;
    }
    
    
    public synchronized boolean creditsExpired(UIController controller, boolean showInfo) {
        
        if (licenseData == null) {
            return true;
        } 
        
        Integer type = aes_decrypt_int(licenseData.getType());
        if (type != null) {
            type = 1;
            
            int leftCredits = aes_decrypt_int(licenseData.getCredits()) - aes_decrypt_int(licenseDataUsage.getUsedCredits());
            boolean dateExpired = aes_decrypt(licenseData.getExpired()).compareTo(EXPIRE_FORMAT.format(new Date())) < 0;
            
            switch (type) {
                case 3:
                    return false;
                    
                case 2:
                case 1:
                    if (!dateExpired && leftCredits > 0) {
                        if (leftCredits < 10 && showInfo) {
                            Platform.runLater(() -> {
                               DialogHelper.showInfo(controller, "License info", "Only " + leftCredits + " credits are remaining", null); 
                            });
                        }
                        return false;
                    }
                    break;
                    
                case 0:
                    if (!dateExpired && leftCredits > 0) {                        
                        return false;
                        
                    } else {
                        // TODO Maybe need show
//                        int daysRemain = 0;
//                        
//                        try {
//                            daysRemain = (int) (EXPIRE_FORMAT.parse(aes_decrypt(licenseData.getExpired())).getTime() - new Date().getTime()) / 1000 * 60 * 60 * 24;
//                        } catch (ParseException ex) {
//                            log.error(ex.getMessage(), ex);
//                        }
//                        
//                        int staticDays = daysRemain;
//                        Platform.runLater(() -> {
//                            DialogHelper.showInfo(null, "License info", "You have remain " + leftCredits + " credits.\n" +
//                                "You have used your free day credits. You need to use one more day to use it again. There is " + staticDays + " days remaining", null); 
//                        });
                   }
                   break;
            }
        }
        
        if (showInfo) {
            Platform.runLater(() -> {
                DialogHelper.showInfo(controller, "License info", "Query Optimization credits has been completed. Please renewal from www.smartmysql.com/pricing Please restart App if you already renewal but not reflected.", null); 
            });
        }

        return true;
    }
    
    public synchronized boolean fullUsageExpired(UIController controller) {
        
        if (licenseData == null) {
            return true;
        }
        
        Integer type = aes_decrypt_int(licenseData.getType());
        if (type != null) {
            type = 1;
            int leftFullUsage = aes_decrypt_int(licenseData.getFullUsage()) - aes_decrypt_int(licenseDataUsage.getUsedFullUsage());
            boolean dateExpired = true;
            try {
                String dateData = aes_decrypt(licenseData.getExpired());
                if (dateData != null) {
                    dateExpired = dateData.compareTo(EXPIRE_FORMAT.format(new Date())) < 0;
                }
            } catch (Throwable th) {
                log.error("Error", th);
            }
            
            switch (type) {
                case 3:
                    return false;
                    
                case 2:
                case 1:
                case 0:
                    if (!dateExpired && leftFullUsage > 0) {
                        return false;
                    }
                    break;
            }
        }
        
        return true;
    }
    
    
    public synchronized void usedCredit() {
        
        if (licenseDataUsage == null) {
            return;
        }
        
        Integer value = aes_decrypt_int(licenseDataUsage.getUsedCredits());
        licenseDataUsage.setUsedCredits(aes_encrypt(String.valueOf(value + 1)));
        licenseDataUsage.setLastUpdate(new Timestamp(new Date().getTime()));
        licenseDataUsageService.save(licenseDataUsage);
    }
    
    public synchronized void usedFullUsage() {
        
        if (licenseDataUsage == null) {
            return;
        }
        
        Integer value = aes_decrypt_int(licenseDataUsage.getUsedFullUsage());
        licenseDataUsage.setUsedFullUsage(aes_encrypt(String.valueOf(value + 1)));
        licenseDataUsage.setLastUpdate(new Timestamp(new Date().getTime()));
        licenseDataUsageService.save(licenseDataUsage);
    }
    
    public synchronized void init(LicenseDataService licenseDataService, LicenseDataUsageService licenseDataUsageService, String email, String license) {
        this.licenseDataService = licenseDataService;
        this.licenseDataUsageService = licenseDataUsageService;
        
        this.email = email;    
        init(email, license, false);
    }
    
    public synchronized void refresh() {
        reinit(email, license);
    }
    
    public synchronized void reinit(String email, String license) {
        this.email = email;    
        this.license = license;    
        init(email, license, true);
    }
    
    private void init(String email, String license, boolean force) {
        if (email != null) {
            if (!force) {
                licenseData = licenseDataService.getLicenseData();
                licenseDataUsage = licenseDataUsageService.getLicenseDataUsage();        
            } else {
                licenseData = null;
                licenseDataUsage = null;
            }

            try {
                PublicDataAccess.getInstanсe().userValidation(email, license);
                // @TODO need do something with not valid?
            } catch (PublicDataException ex) {
                log.error(ex.getMessage(), ex);
            }
    
            syncLicenseData();
        }
    }
    
    public synchronized void syncLicenseData() {
        
        if (licenseDataUsage != null && licenseDataUsage.getLastUpdate() != null) {
            
            boolean needUpdatePublic = true;
            try {
                Timestamp result = PublicDataAccess.getInstanсe().lastUpdateUExpire(email, aes_decrypt(licenseData.getType()), aes_decrypt(licenseData.getExpired()));
                if (result != null) {
                    if (!result.before(licenseDataUsage.getLastUpdate())) {
                        needUpdatePublic = false;
                    }
                }
            } catch (PublicDataException th) {
                log.error(th.getMessage(), th);
            }
            
            if (needUpdatePublic) {
                try {
                    PublicDataAccess.getInstanсe().insertUExpire(
                        email, 
                        aes_decrypt_int(licenseData.getType()), 
                        aes_decrypt_int(licenseDataUsage.getUsedCredits()), 
                        aes_decrypt_int(licenseDataUsage.getUsedFullUsage()), 
                        aes_decrypt(licenseData.getExpired()), 
                        LAST_UPDATE_FORMAT.format(licenseDataUsage.getLastUpdate())
                    );

                    licenseDataUsage.setUsedCredits(aes_encrypt("0"));
                    licenseDataUsage.setUsedFullUsage(aes_encrypt("0"));
                    licenseDataUsageService.save(licenseDataUsage);
                
                } catch (PublicDataException th) {
                    log.error(th.getMessage(), th);
                }
            }
        }
        
        try {
            String licenseResult = PublicDataAccess.getInstanсe().userLicense(email);
            if (licenseResult != null) {

                if (licenseData == null) {
                    licenseData = new LicenseData();
                }

                if (licenseDataUsage == null) {
                    licenseDataUsage = new LicenseDataUsage();
                }

                String[] data = licenseResult.split(":");
                if (data.length == 4) {
                    String type = aes_encrypt(data[0]);
                    String credits = aes_encrypt(data[1]);
                    String fullUsage = aes_encrypt(data[2]);
                    String expired = aes_encrypt(data[3]);

                    if (!type.equals(licenseData.getType()) || !expired.equals(licenseData.getExpired())) {

                        licenseDataUsage.setUsedCredits(aes_encrypt("0"));
                        licenseDataUsage.setUsedFullUsage(aes_encrypt("0"));
                        licenseDataUsage.setLastUpdate(new Timestamp(new Date().getTime()));
                    }

                    licenseData.setType(type);
                    licenseData.setCredits(credits);
                    licenseData.setFullUsage(fullUsage);
                    licenseData.setExpired(expired);

                    licenseDataService.save(licenseData);
                } else {
                    licenseData.setType(aes_encrypt("0"));
                    licenseData.setCredits(aes_encrypt("0"));
                    licenseData.setFullUsage(aes_encrypt("0"));
                }

                licenseDataUsageService.save(licenseDataUsage);
                licenseDataService.save(licenseData);
            }
            
        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }
    
    private String aes_encrypt(String string) {
        if (string != null) {
            try {
                SecretKey key = new SecretKeySpec(AES.getBytes("UTF-8"), "AES");

                Cipher aesCipher = Cipher.getInstance("AES");
                aesCipher.init(Cipher.ENCRYPT_MODE, key);

                byte[] byteCipherText = aesCipher.doFinal(string.getBytes("UTF-8"));
                return new String(Hex.encodeHex(byteCipherText));

            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            } 
        }
        
        return null;
    }
    
    private Integer aes_decrypt_int(String string) {
        String v = aes_decrypt(string);
        try {
            return Integer.valueOf(v);
        } catch (Throwable th) {
            return 0;
        }
    }
    
    private String aes_decrypt(String string) {
        if (string != null) {
            try {
                SecretKey key = new SecretKeySpec(AES.getBytes("UTF-8"), "AES");

                Cipher aesCipher = Cipher.getInstance("AES");
                aesCipher.init(Cipher.DECRYPT_MODE, key);

                byte[] byteCipherText = aesCipher.doFinal(Hex.decodeHex(string.toCharArray()));
                return new String(byteCipherText);

            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            } 
        }
        
        return null;
    }

    public String getType() {
        return licenseData != null ? aes_decrypt(licenseData.getType()) : "";
    }

    public String getDateExpire() {
        return licenseData != null ? aes_decrypt(licenseData.getExpired()) : "";
    }

    public String getCredits() {
        return licenseData != null ? String.valueOf(aes_decrypt_int(licenseData.getCredits()) - aes_decrypt_int(licenseDataUsage.getUsedCredits())) : "";
    }

    public String getFullUsage() {
        return licenseData != null ? String.valueOf(aes_decrypt_int(licenseData.getFullUsage()) - aes_decrypt_int(licenseDataUsage.getUsedFullUsage())) : "";
    }
    
}
