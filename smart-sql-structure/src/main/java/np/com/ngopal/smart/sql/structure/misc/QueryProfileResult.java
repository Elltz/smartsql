package np.com.ngopal.smart.sql.structure.misc;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryProfileResult {
        
    private SimpleStringProperty status = new SimpleStringProperty();
    private SimpleObjectProperty<Double> before = new SimpleObjectProperty<>(0.00);
    private SimpleObjectProperty<Double> after = new SimpleObjectProperty<>(0.00);
    private SimpleObjectProperty<Double> improvement = new SimpleObjectProperty<>();
    
    public QueryProfileResult(String status, Double value) {
        this.status.set(status);
        this.before.set(value);
    }
        
    public SimpleStringProperty status() {
        return status;
    }
    
    public SimpleObjectProperty<Double> before() {
        return before;
    }
    
    public SimpleObjectProperty<Double> after() {
        return after;
    }
    
    public SimpleObjectProperty<Double> improvement() {
        return improvement;
    }

}
