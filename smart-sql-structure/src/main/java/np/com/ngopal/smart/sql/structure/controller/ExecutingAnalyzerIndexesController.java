package np.com.ngopal.smart.sql.structure.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.misc.QueryProfileResult;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ExecutingAnalyzerIndexesController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TableView<ImprovementResult> tableView;
    
    @FXML
    private TableView<QueryProfileResult> profilerTableView;
    
    @FXML
    private Button executeButton;
    @FXML
    private Button stopButton;
    @FXML
    private Button dropButton;
    
    @FXML
    private Label beforeRowsField;
    @FXML
    private Label afterRowsField;
    @FXML
    private Label improvementField;
    
    @FXML
    private CheckBox selectAll;
    
    @FXML
    private TableColumn<ImprovementResult, Boolean> selectedColumn;
    @FXML
    private TableColumn<ImprovementResult, ImprovementResult.State> stateColumn;
    @FXML
    private TableColumn<ImprovementResult, String> tableColumn;
    @FXML
    private TableColumn<ImprovementResult, String> indexColumn;
    @FXML
    private TableColumn<ImprovementResult, String> improvementColumn;
    
    @FXML
    private TableColumn<QueryProfileResult, String> statusColumn;
    @FXML
    private TableColumn<QueryProfileResult, String> beforeDurationColumn;
    @FXML
    private TableColumn<QueryProfileResult, String> afterDurationColumn;
    @FXML
    private TableColumn<QueryProfileResult, Double> profilerImprovementColumn;
    
    @FXML
    private CheckBox displayProfiler;
    
    @Setter(AccessLevel.PUBLIC)
    QAPerformanceTuningService qapts;
    
    
    public List<ImprovementResult> getSelectedResults() {
        List<ImprovementResult> list = new ArrayList<>();
        for (ImprovementResult ir: tableView.getItems()) {
            if (ir.selected().get()) {
                list.add(ir);
            }
        }
        
        return list;
    }
    
    public List<ImprovementResult> getExecutedResults() {
        List<ImprovementResult> list = new ArrayList<>();
        for (ImprovementResult ir: tableView.getItems()) {
            if (ir.state().get() == ImprovementResult.State.DONE) {
                list.add(ir);
            }
        }
        
        return list;
    }
    
    @FXML
    public void executeAction(ActionEvent event) {
        
        List<ImprovementResult> list = getSelectedResults();
        
        Thread th = new Thread(() -> {
            // add for last
            
            if (!list.isEmpty()) {

                if (displayProfiler.isSelected()) {
                    ImprovementResult last = list.get(list.size() - 1);


                    for (ImprovementResult ir: list) {
                        ir.state().set(ImprovementResult.State.PROFILER_BEFORE);
                    }

                    List<QueryProfileResult> profileResult = new ArrayList<>();
                    QueryProfileResult total = new QueryProfileResult("total time", 0.00);
                    try {
                        Map<String, Double> showStatusBefore = FunctionHelper.executeShowProfile(getController().getSelectedConnectionSession().get().getService(), last.mainQuery().get(), null);
                        if (showStatusBefore != null) {
                            for (Map.Entry<String, Double> entry: showStatusBefore.entrySet()) {
                                profileResult.add(new QueryProfileResult(entry.getKey(), entry.getValue()));
                                total.before().set(total.before().get() + entry.getValue());
                            }
                        }

                    } catch (SQLException ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                    }

                    profileResult.add(total);

                    Platform.runLater(() -> {
                        profilerTableView.getItems().clear();
                        profilerTableView.getItems().addAll(profileResult);
                    });                
                }
                
                addCheckImprovement(list);

                for (ImprovementResult ir: list) { 
                    IndexPerformanceImprovement.getInstance(getController().getSelectedConnectionSession().get(), qapts).improve(getController(), ir);            
                }
            }
        });
        th.setDaemon(true);
        th.start();
    }
    
    private void addCheckImprovement(List<ImprovementResult> list) {
        
        ImprovementResult last = list.get(list.size() - 1);
        
        last.newRows().addListener(new ChangeListener<Long>() {
            @Override
            public void changed(ObservableValue<? extends Long> observable, Long oldValue, Long newValue) {

                last.newRows().removeListener(this);
                
                if (newValue != null) {

                    Platform.runLater(() -> {
                        afterRowsField.setText(String.valueOf(newValue));
                        improvementField.setText(last.improvement().get());

                        if (((last.oldRows().get() - newValue) / newValue) * 100 >= 100.0) {
                            improvementField.setStyle("-fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: green;");
                        } else {
                            improvementField.setStyle("-fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: black;");
                        }
                    });
                } else {
                    Platform.runLater(() -> {
                        afterRowsField.setText("Unknown");
                        improvementField.setText("Unknown");
                        improvementField.setStyle("-fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: black;");
                    });
                }
                
                Thread th = new Thread(() -> {
                    
                    if (displayProfiler.isSelected()) {
                        
                        List<QueryProfileResult> profileResult = new ArrayList<>();
                        
                        try {
                            
                            for (ImprovementResult ir: list) {
                                ir.state().set(ImprovementResult.State.PROFILER_AFTER);
                            }
                            
                            Map<String, Double> showStatusBefore = FunctionHelper.executeShowProfile(getController().getSelectedConnectionSession().get().getService(), last.mainQuery().get(), null);
                            if (showStatusBefore != null) {
                                for (Map.Entry<String, Double> entry: showStatusBefore.entrySet()) {
                                    profileResult.add(new QueryProfileResult(entry.getKey(), entry.getValue()));
                                }
                            }

                        } catch (SQLException ex) {
                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                        }
                
                        Platform.runLater(() -> {                    

                            for (QueryProfileResult item: profilerTableView.getItems()) {
                                item.after().set(0.0000000);
                            }

                            for (QueryProfileResult r: profileResult) {

                                boolean found = false;
                                for (QueryProfileResult item: profilerTableView.getItems()) {

                                    if (item.status().get().equals(r.status().get())) {
                                        item.after().set(r.before().get());

                                        Double improvement = (item.before().get() - item.after().get()) * 100 / item.after().get();
                                        if (improvement.isNaN()) {
                                            improvement = 0.0;
                                        }
                                        item.improvement().set(improvement);

                                        found = true;
                                        break;
                                    }
                                }

                                if (!found) {
                                    r.after().set(r.before().get());
                                    r.before().set(0.000000);
                                    r.improvement().set(0.0);
                                    profilerTableView.getItems().add(r);
                                }
                            }

                            // calc total
                            QueryProfileResult total = null;
                            Double totalBefore = 0.0;
                            Double totalAfter = 0.0;
                            for (QueryProfileResult item: profilerTableView.getItems()) {
                                if (item.status().get().equals("total time")) {
                                    total = item;
                                } else {
                                    totalBefore += item.before().get();
                                    totalAfter += item.after().get();
                                }
                            }

                            total.before().set(totalBefore);
                            total.after().set(totalAfter);
                            
                            Double totalImprovement = (totalBefore - totalAfter) * 100 / totalAfter;
                            if (totalImprovement.isNaN()) {
                                totalImprovement = 0.0;
                            }
                            
                            total.improvement().set(totalImprovement);
                            
                            List<QueryProfileResult> list0 =  profilerTableView.getItems();
                            list0.sort((QueryProfileResult o0, QueryProfileResult o1) -> {
                                double v0 = o0.before().get() - o0.after().get();
                                double v1 = o1.before().get() - o1.after().get();
                                
                                if (v0 == v1) {
                                    return 0;
                                }
                                
                                return v0 > v1 ? -1 : 1;
                            });

                            profilerTableView.setVisible(true);
                            profilerTableView.setManaged(true);
                            
                            for (ImprovementResult ir: list) {
                                ir.state().set(ImprovementResult.State.DONE);
                            }
                        });
                    } else {
                        for (ImprovementResult ir: list) {
                            ir.state().set(ImprovementResult.State.DONE);
                        } 
                    }
                });
                th.setDaemon(true);
                th.start();                        
            }
        });
    }
    
    
    @FXML
    public void stopAction(ActionEvent event) {
        for (ImprovementResult ir: getSelectedResults()) {
            IndexPerformanceImprovement.getInstance(getController().getSelectedConnectionSession().get(), qapts).stop(ir);
        }
    }
    
    @FXML
    public void dropAction(ActionEvent event) {
        for (ImprovementResult improvementResult: getSelectedResults()) {
            try {
                boolean temporary = improvementResult.checkExtraTemporary();
                boolean filesort = improvementResult.checkExtraFilesort();
                boolean zero = "0.0%".equals(improvementResult.improvement().get());
                boolean usingIndex = !zero || temporary || filesort;

                String recomendation = "";
                if (temporary) {
                    recomendation += "New index removed costly temporary table creation.\n";
                }
                if (filesort) {
                    recomendation += "New index removed costly filesort operation.\n";
                }
                String title = usingIndex ? 
                    recomendation + "The current query is using newly created index and improved " + improvementResult.improvement().get() + " performance.\nWe recommend to keep new index. Would you like to drop index?" :
                    "The New index is not used by the query. It will be useful with  filter condition\nother values. We recommend to keep the new index.\nWould you like to drop index?";

                ConfirmDropIndexDialogController c = StandardBaseControllerProvider.getController(getController(), "ConfirmDropIndexDialog");
                c.init(title);

                DialogHelper.show("Drop Index", c.getUI(), dialog);

                if (c.getResponce() == DialogResponce.OK_YES) {
                    getController().getSelectedConnectionSession().get().getService().dropIndex(improvementResult.database().get(), improvementResult.table().get(), improvementResult.indexName().get());
                    improvementResult.state().set(ImprovementResult.State.DROPED);
                    improvementResult.improvement().set("Unknown");
                    improvementResult.newRows().set(null);
                }

            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, dialog);
            }
        }
        
        List<ImprovementResult> list = getExecutedResults();
        if (!list.isEmpty()) {
            
            addCheckImprovement(list);
            
            for (ImprovementResult ir: list) {            
                addCheckImprovement(list);
                IndexPerformanceImprovement.getInstance(getController().getSelectedConnectionSession().get()).checkImprovement(ir);            
            }
        } else {
            
            afterRowsField.setText("Unknown");
            improvementField.setText("Unknown");
            improvementField.setStyle("-fx-font-weight: bold; -fx-font-size: 14px; -fx-text-fill: black;");
            
            Platform.runLater(() -> {
                for (QueryProfileResult item: profilerTableView.getItems()) {
                    item.after().set(0.000000);
                    item.improvement().set(0.0);
                }

                profilerTableView.setVisible(false);
                profilerTableView.setManaged(false);
            });
        }
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        dialog.close();
    }
       

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    private Stage dialog;

    private final ChangeListener<ImprovementResult.State> rowListener = (ObservableValue<? extends ImprovementResult.State> observable1, ImprovementResult.State oldValue1, ImprovementResult.State newValue1) -> {
        
        boolean canExecute = false;
        boolean canStop = false;
        boolean canDrop = false;
        
        if (!tableView.getItems().isEmpty()) {

            canExecute = true;
            canStop = true;
            canDrop = true;
            
            boolean selectedAtleastOne = false;
            
            for (ImprovementResult ir: tableView.getItems()) {
                if (ir.selected().get()) {
                    selectedAtleastOne = true;
                    
                    if (!ir.canDrop()) {
                        canDrop = false;
                    }
                    if (!ir.canExecute()) {
                        canExecute = false;
                    }
                    if (!ir.canStop()) {
                        canStop = false;
                    }
                }
            }
            
            if (!selectedAtleastOne) {
                canExecute = false;
                canStop = false;
                canDrop = false;
            }
        }
        
        executeButton.setDisable(!canExecute);
        stopButton.setDisable(!canStop);
        dropButton.setDisable(!canDrop);
    };
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
                
        profilerTableView.setVisible(false);
        profilerTableView.setManaged(false);
        
//        displayProfiler.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
//            profilerTableView.setVisible(newValue != null && newValue);
//            profilerTableView.setManaged(newValue != null && newValue);
//        });
        
        selectAll.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (ImprovementResult ir: tableView.getItems()) {
                ir.selected().set(newValue);
            }
        });
        
        
        selectedColumn.setEditable(true);
        selectedColumn.setCellValueFactory((TableColumn.CellDataFeatures<ImprovementResult, Boolean> p) -> ((ImprovementResult)p.getValue()).selected());
        selectedColumn.setCellFactory((TableColumn<ImprovementResult, Boolean> p) -> {
            return new CheckBoxTableCells<>();
        });
        
        stateColumn.setCellValueFactory((TableColumn.CellDataFeatures<ImprovementResult, ImprovementResult.State> p) -> ((ImprovementResult)p.getValue()).state());
        stateColumn.setCellFactory((TableColumn<ImprovementResult, ImprovementResult.State> param) -> {
            TableCell<ImprovementResult, ImprovementResult.State> cell = new TableCell<ImprovementResult, ImprovementResult.State>() {
                                
                @Override
                protected void updateItem(ImprovementResult.State item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                       
                        Label l = new Label();
                        l.setStyle("-fx-background-color: " + item.getBgColor() + "; -fx-text-fill: " + item.getColor() + ";");

                        l.setMaxWidth(Double.MAX_VALUE);
                        l.setText(item.toString());
                        setGraphic(l);
                        setText("");
                        
                    } else {
                        setText("");
                        setStyle("");
                        setGraphic(null);
                    }
                }
                
            };
            
            return cell;
        });
        
        tableColumn.setCellValueFactory((TableColumn.CellDataFeatures<ImprovementResult, String> p) -> ((ImprovementResult)p.getValue()).table());
        indexColumn.setCellValueFactory((TableColumn.CellDataFeatures<ImprovementResult, String> p) -> ((ImprovementResult)p.getValue()).index());
        improvementColumn.setCellValueFactory((TableColumn.CellDataFeatures<ImprovementResult, String> p) -> ((ImprovementResult)p.getValue()).improvement());
        improvementColumn.setCellFactory((TableColumn<ImprovementResult, String> param) -> {
            TableCell<ImprovementResult, String> cell = new TableCell<ImprovementResult, String>() {
                                
                @Override
                protected void updateItem(String item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                       
                        ImprovementResult ir = getTableView().getItems().get(getTableRow().getIndex());
                        if (!ir.improvement().get().equals("Unknown") && ir.newRows().get() != null) {
                            double improvement = (ir.oldRows().get().doubleValue() / ir.newRows().get().doubleValue()) * 100;
                            
                            Label l = new Label();
                            if (improvement > 100.0) {
                                l.setStyle("-fx-background-color: green; -fx-text-fill: white;");
                            } else if (improvement > 0) {
                                l.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
                            }
                            
                            l.setMaxWidth(Double.MAX_VALUE);
                            l.setText(item);
                            setGraphic(l);
                            setText("");
                            
                        } else {
                            setStyle("");
                            setText(item);
                            setGraphic(null);
                        }                        
                        
                    } else {
                        setText("");
                        setStyle("");
                        setGraphic(null);
                    }
                }
                
            };
            
            return cell;
        });
        
        statusColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryProfileResult, String> p) -> ((QueryProfileResult)p.getValue()).status());
        statusColumn.setCellFactory((TableColumn<QueryProfileResult, String> param) -> {
            TableCell<QueryProfileResult, String> cell = new TableCell<QueryProfileResult, String>() {
                                
                @Override
                protected void updateItem(String item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    setText("");
                    setStyle("");
                        
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                       
                        setText(item);
                        if ("total time".equals(item)) {
                            setStyle("-fx-font-weight: bold; -fx-text-fill: blue;");
                        }
                    }
                }
                
            };
            
            return cell;
        });
        
        beforeDurationColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryProfileResult, String> p) -> Bindings.format("%f", ((QueryProfileResult)p.getValue()).before().get()));
        afterDurationColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryProfileResult, String> p) -> Bindings.format("%f", ((QueryProfileResult)p.getValue()).after().get()));
        profilerImprovementColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryProfileResult, Double> p) -> ((QueryProfileResult)p.getValue()).improvement());
        profilerImprovementColumn.setCellFactory((TableColumn<QueryProfileResult, Double> param) -> {
            TableCell<QueryProfileResult, Double> cell = new TableCell<QueryProfileResult, Double>() {
                                
                @Override
                protected void updateItem(Double item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                       
                        QueryProfileResult ir = getTableView().getItems().get(getTableRow().getIndex());
                        if (ir.improvement().get() != null) {
                            Label l = new Label();
                        
                            if (item >= 0.0) {
                                l.setStyle("-fx-background-color: green; -fx-text-fill: white;");
                            } else {
                                l.setStyle("-fx-background-color: red; -fx-text-fill: white;");
                            }
                            
                            
                            l.setMaxWidth(Double.MAX_VALUE);
                            if (item.isNaN()) {
                                l.setText(new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP).toString() + "%");
                            } else if (item.isInfinite()) {
                                l.setText("99999.00%");
                            } else {
                                l.setText(new BigDecimal(item).setScale(2, RoundingMode.HALF_UP).toString() + "%");
                            }
                            
                            setGraphic(l);
                            setText("");
                        } else {
                            setText("Unknown");
                            setStyle("");
                            setGraphic(null);
                        }
                        
                    } else {
                        setText("");
                        setStyle("");
                        setGraphic(null);
                    }
                }
                
            };
            
            return cell;
        });
    }
    
    public void init(Window win, List<ImprovementResult> list) {
        
        List<ImprovementResult> ims = IndexPerformanceImprovement.getInstance(getController().getSelectedConnectionSession().get()).getResults();
        
        for (ImprovementResult ir: list) {
            ir.state().addListener((ObservableValue<? extends ImprovementResult.State> observable, ImprovementResult.State oldValue, ImprovementResult.State newValue) -> {
                rowListener.changed(null, null, null);
            });
            
            int index = ims.indexOf(ir);
            if (index > -1) {
                ImprovementResult ir0 = ims.get(index);
                if (ir0.state().get() == ImprovementResult.State.STOPED || ir0.state().get() == ImprovementResult.State.ERROR || ir0.state().get() == ImprovementResult.State.DONE) {
                    ims.remove(index);
                    tableView.getItems().add(ir);
                }
            } else {
                tableView.getItems().add(ir);
            }
            
            if (ir.state().get() == ImprovementResult.State.NEW) {
                ir.selected().set(true);
            }
            
            beforeRowsField.setText(ir.oldRows().asString().get());
        }
            
        tableView.getItems().addAll(ims);
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();        
    }
 
    
    class CheckBoxTableCells<S, T> extends TableCell<S, T> {

        CheckBox checkBox;

        public CheckBoxTableCells() {
        }

        @Override
        public void updateItem(T item, boolean empty) {

            super.updateItem(item, empty);
            if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setPadding(new Insets(3));
                    this.checkBox.setFocusTraversable(false);
                    this.checkBox.setAlignment(Pos.CENTER);
                    setAlignment(Pos.CENTER);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        
                        //changed column to selectioncolumn
                        ImprovementResult col = tableView.getItems().get(getIndex());
                        col.selected().set(newValue);                        
                        rowListener.changed(null, null, null);
                    });
                }

                checkBox.setSelected(item != null ? (Boolean)item : Boolean.FALSE);                
                this.checkBox.setAlignment(Pos.CENTER);
                setAlignment(Pos.CENTER);
                setGraphic(checkBox);
                
            } else {
                setGraphic(null);
            }
        }

    }
}
