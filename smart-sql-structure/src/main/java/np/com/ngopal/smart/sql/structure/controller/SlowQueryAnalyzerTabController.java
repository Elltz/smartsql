package np.com.ngopal.smart.sql.structure.controller;

import np.com.ngopal.smart.sql.structure.misc.ProfilerInnerControllerWorkProcAuth;
import com.sun.javafx.PlatformUtil;
import com.sun.javafx.application.PlatformImpl;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import liquibase.util.csv.CSVReader;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.GlobalProcessList;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.queryoptimizer.data.QOIndexRecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.misc.RunnedFrom;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.utils.ScriptRunner;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SlowQueryAnalyzerTabController extends BaseController implements Initializable, ProfilerListener {
    
    private ConnectionParamService connectionParamService;
    private PreferenceDataService preferenceService;
    private ColumnCardinalityService columnCardinalityService;
    private ProfilerDataManager dbProfilerService;
    private MysqlDBService slowQueryDBService;    
    private QARecommendationService qars;
    private QAPerformanceTuningService performanceTuningService;
    private UserUsageStatisticService usageService;
    
    // used for - not changing for the same database, because calling of use database taking a time on remote servers
    private String lastUsedDatabase;
    
    @FXML 
    private PaginatorFilterableTableView slowQueryTable;
    @FXML
    private PaginatorFilterableTableView analyzerQueryTable;
    
    @FXML
    private AnchorPane mainUI;
    @FXML
    private HBox dragAndDropPane;
    
    @FXML
    private BorderPane containerPane;
    
    @FXML
    private Tab topQueries;
    @FXML
    private Tab lockedQueries;
    @FXML
    private Tab topTable;
    @FXML
    private Tab topHosts;
    @FXML
    private Tab topUsers;
    @FXML
    private Tab topSchema;
    @FXML
    private Tab analyzerReportQueries;
    
    private boolean analyzerReportQueriesInited = false;
    
    @FXML
    private TabPane mainTabPane;
    
    @FXML
    private TextField filterField;
    @FXML
    private CheckBox regexCheckBox;
    @FXML
    private CheckBox notMatchCheckBox;
    
    @FXML
    private Button importIntoCurrentDatabaseButton;
    @FXML
    private Button exportToXlsButton;
    
    @FXML
    private DateTimePicker fromDatePicker;
    
    @FXML
    private DateTimePicker toDatePicker;
    
    private volatile boolean canCollect = true;
    
    @Getter
    @FXML
    private ChartTableView topTablesTable;
    
    @Getter
    @FXML
    private ChartTableView topSchemasTable;
    
    @Getter
    @FXML
    private ChartTableView topUsersTable;
    
    @Getter
    @FXML
    private ChartTableView topHostsTable;
    
    @Getter
    @FXML
    private PaginatorFilterableTableView lockQueriesTable;
    
    @FXML
    private HBox loadPane;
    
//    @Getter
//    @FXML
//    private PaginatorFilterableTableView notUsingIndexesTable;
    
    
    private List<SlowQuery> slowQueries;
    
    private final Map<Integer, Image> ratingImages = new HashMap<>();
    
    private final Map<Integer, byte[]> ratingImagesBytes = new HashMap<>();
    
    private volatile boolean explainShowed = false;
    
    private static final Pattern SLOW_QUERY__PATTERN = Pattern.compile("(# Time: (?<time>.*)\\n)?# User@Host: (?<userHost>.*)\\n# Query_time: (?<queryTime>.*) Lock_time: (?<lockTime>.*) Rows_sent: (?<rowsSent>.*) Rows_examined: (?<rowsExamined>.*)(\\nuse (?<database>.*);)?\\nSET timestamp=(?<timestamp>.*)\\n(?<query>.*;)");
    
    private final DateTimeFormatter BEGIN_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00.000");
    private final DateTimeFormatter END_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:59.999");
    private final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    private LocalDateTime minDate;
    private LocalDateTime maxDate;
    
    
    private Date currentDate;
    
    private Date lastDateFrom;
    
    private boolean licenseFullUsageExipred = true;
    
    /*
    ***************************
    ***************************
    *       PROFILER PART
    ***************************
    ***************************
    */
    
    private ProfilerTabContentController profilerController;
    private ConnectionParams connectionParams;
    public void initAsProfilerQA(ProfilerTabContentController profilerController, ConnectionParams params) throws SQLException {
        this.profilerController = profilerController;
        
        checkDragAndDrop();
        
        this.connectionParams = params;
        
        usageService.savePUsage(PUsageFeatures.SQA);
        
        removeColumns(slowQueryTable.getTableView(), "StddevResponce Time", "LockTime", "AvgLockTime", "RowsSent", "AvgRowsSent", "AvgRowsExamined");
        
        try { slowQueryDBService.connect(params); }
        catch (SQLException ex) { log.error("Error connect with current connection params", ex); }
        
        loadPane.setVisible(false);
        loadPane.setManaged(false);
        
        // @TODO Need check why it need - didnt see any logic in code
        //connectionForProfiler = getDbService().connect(getConnectionParams());
        
        lockQueriesTable.setRowsPerPage(10);
        slowQueryTable.setRowsPerPage(10);
        analyzerQueryTable.setRowsPerPage(10);
        
        toDatePicker.setDisable(true);
        toDatePicker.setStyle("-fx-opacity: 1");
        toDatePicker.getEditor().setStyle("-fx-opacity: 1");
        
        fromDatePicker.setDisable(true);
        fromDatePicker.setStyle("-fx-opacity: 1");
        fromDatePicker.getEditor().setStyle("-fx-opacity: 1");
    }
    
    private boolean isProfilerMode() {
        return profilerController != null;
    }
    
    public ConnectionParams getConnectionParams() {
        return connectionParams;//getController().getSelectedConnectionSession().get().getConnectionParam();
    }
    
    private DBService getDbService() {
        return getController().getSelectedConnectionSession().get().getService();
    }
    
    //private Connection connectionForProfiler;
    private volatile boolean keepThreadsAliveForProfiler = true;
    //private WorkProc allBackgroundTask;
    private ProfilerInnerControllerWorkProcAuth slowQueryAnalyzerTabControllerWorkProcAuth;
    private final BooleanProperty profilerStarted = new SimpleBooleanProperty(false);
    
    public void destroyForProfiler() {
        
        keepThreadsAliveForProfiler = false;
        
//        try {
//            if (connectionForProfiler != null) {
//                connectionForProfiler.close();
//            }
//        } catch (SQLException ex) {
//            log.error("Eror", ex);
//        }
    }
    
    private ProfilerData profilerData;
    
    public void openData(ProfilerData data) {        
        this.profilerData = profilerData;
    }
    
    public void saveProfillerData(ProfilerData pd) {       
        pd.setLockQueries(collectFilterTableData(lockQueriesTable.getItems()));
        pd.setSlowQueries(collectFilterTableData(slowQueryTable.getItems()));
        pd.setAnalyzerQueries(collectFilterTableData(analyzerQueryTable.getItems()));
        
        pd.setTopTables(collectTopTableData(topTablesTable.getData()));
        pd.setTopSchemas(collectTopTableData(topSchemasTable.getData()));
        pd.setTopUsers(collectTopTableData(topUsersTable.getData()));
        pd.setTopHosts(collectTopTableData(topHostsTable.getData()));
    }
    
    private List<Object[]> collectFilterTableData(List<SlowQueryData> data) {
        List<Object[]> list = new ArrayList<>();
        for (SlowQueryData f : data) {
            list.add(f.getValues());
        }

        return list;
    }
    
    public List<Object[]> collectTopTableData(List<ProfilerTopData> data) {
        List<Object[]> list = new ArrayList<>();
        for (ProfilerTopData f : data) {
            list.add(f.getValues());
        }

        return list;
    }
    
    public void startProfiler(Date startDate) {
        
        profilerStarted.set(true);
        lastDateFrom = null;
        
        createAndInitThread();
    }
    
    public void stopProfiler() {
        profilerStarted.set(false);
    }
    
    public void clearProfilerData() {
        if (lockQueriesTable.getItems() != null) {
            lockQueriesTable.setItems(null);
        }
        
        if (slowQueryTable.getItems() != null) {
            slowQueryTable.setItems(null);
        }
        
        if (analyzerQueryTable.getItems() != null) {
            analyzerQueryTable.setItems(null);
        }
        
        topTablesTable.setItems(Collections.EMPTY_LIST);
        topSchemasTable.setItems(Collections.EMPTY_LIST);
        topUsersTable.setItems(Collections.EMPTY_LIST);
        topHostsTable.setItems(Collections.EMPTY_LIST);
    }
    
    public void showProfilerData(Date dateFrom) {
        this.lastDateFrom = dateFrom;
                
        this.analyzerReportQueriesInited = false;
        
        getController().addBackgroundWork(new WorkProc() {
            @Override
            public void updateUI() { }
            
            @Override
            public void run() { collectQueries(); }
        });
    }
    
    private void createAndInitThread() {
        createThread();
        // THIS NOT WORK WITH THIS BACKGROUND THREADS
        // SOME TIMES IT JUST IGNORING WORKPROC, 
        // ALSO WE HAVE ONLY 3 THREAD - THIS IS NOT ENOUGH
        //Recycler.getLessBusySWP().addWorkProc(allBackgroundTask);
//        
//        Thread th = new Thread(() -> {
//            allBackgroundTask.run();
//        }, "QAProfiling");
//        th.setDaemon(true);
//        th.start();
    }
    
    private void createThread() {
        // generate server hash code
        final String serverHashCode = serverHashCode(getConnectionParams(), "");
            slowQueryAnalyzerTabControllerWorkProcAuth = new ProfilerInnerControllerWorkProcAuth() {
            
            long lastOverallCodeExecutionRun = 0l;
                long lastTimeNotUsingIndexRunned = 0l;
            
            @Override
            public boolean shouldExecuteWorkProc() {
                return keepThreadsAliveForProfiler && profilerStarted.get() 
                        && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastOverallCodeExecutionRun) >= 30;
            }

            @Override
            public void runWork() {
                analyzerReportQueriesInited = false;
                collectQueries();
                lastOverallCodeExecutionRun = System.currentTimeMillis();
            }
        }; 
    }
    
    @FXML
    public void loadFromMysqlSlowQueryLog() {
        makeBusy(true);
        getController().addBackgroundWork(new WorkProc() {
            @Override
            public void updateUI() { }

            @Override
            public void run() {

                log.debug("Start import");
                DBService dbService = getController().getSelectedConnectionSession().get().getService();
                try (ResultSet rs = (ResultSet) dbService.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_SLOW_QUERY_LOG))) {

                    if (rs != null) {

                        String serverHashCode = serverHashCode(getController().getSelectedConnectionSession().get().getConnectionParam(), "");
                        currentDate = new Date();
                        dbProfilerService.clearOldSlowData(currentDate);
                        dbProfilerService.prepareSlowQueryTables(serverHashCode, currentDate);

                        dbProfilerService.startCaching();

                        Date date = null;
                        String database = null;

                        if (slowQueries != null) {
                            slowQueries.clear();
                        }

                        analyzerReportQueriesInited = false;
                        slowQueries = new ArrayList<>();

                        Date min = null;
                        Date max = null;
                        while (rs.next()) {
                            Date timeTemp = rs.getTimestamp("start_time");
                            if (timeTemp != null && (min == null || min.after(timeTemp))) {
                                min = timeTemp;
                            }

                            if (timeTemp != null && (max == null || max.before(timeTemp))) {
                                max = timeTemp;
                            }

                            String userHostTemp = rs.getString("user_host");
                            Integer queryTimeTemp = rs.getInt("query_time");
                            String queryTemp = rs.getString("sql_text");
                            Integer lockTimeTemp = rs.getInt("lock_time");
                            Integer rowsSentTemp = rs.getInt("rows_sent");
                            Integer rowsExaminedTemp = rs.getInt("rows_examined");
                            String dbTemp = rs.getString("db");
                            Integer id = rs.getInt("thread_id");
                            if (dbTemp != null) {
                                database = dbTemp;
                            }

                            Integer rowsSent = rowsSentTemp;
                            Integer rowsExamined = rowsExaminedTemp;

                            String user = null;
                            String host = null;

                            BigDecimal qTime = new BigDecimal(queryTimeTemp);
                            BigDecimal lockTime = new BigDecimal(lockTimeTemp);

                            if (timeTemp != null) {
                                date = timeTemp;
                            }

                            //get user, host
                            int hostIdx = userHostTemp.indexOf("@");

                            if (hostIdx >= 0) {
                                user = userHostTemp.substring(0, hostIdx);
                                if (user.contains("[")) {
                                    String tempUser = user.replaceAll("\\[.*\\]", "").trim();
                                    if (tempUser.isEmpty()) {
                                        user = user.substring(user.indexOf("[") + 1, user.lastIndexOf("]"));
                                    } else {
                                        user = tempUser;
                                    }
                                }

                                host = userHostTemp.substring(hostIdx + 1);
                                if (host.contains("[")) {
                                    String tempHost = host.replaceAll("\\[.*\\]", "").trim();
                                    if (tempHost.isEmpty()) {
                                        host = host.substring(host.indexOf("[") + 1, host.lastIndexOf("]"));
                                    } else {
                                        host = tempHost;
                                    }
                                }
                            }

                            GlobalProcessList gpl = new GlobalProcessList(
                                    id,
                                    user,
                                    host,
                                    database != null ? database : "",
                                    "Query",
                                    qTime,
                                    "",
                                    queryTemp.trim(),
                                    date
                            );

                            SlowQuery sq = new SlowQuery();
                            sq.gpl = gpl;
                            sq.lockTime = lockTime;
                            sq.rowsSent = rowsSent;
                            sq.rowsExamined = rowsExamined;

                            slowQueries.add(sq);
                        }

                        log.debug("collected");

                        int index = 0;

                        double step = 0.7 / slowQueries.size();
                        double progress = 0.0;

                        for (SlowQuery sq : slowQueries) {
                            dbProfilerService.save(sq.gpl, currentDate, true, serverHashCode, sq.lockTime, sq.rowsSent, sq.rowsExamined);

                            index++;
                            if (index % 100000 == 0) {
                                log.debug("imported:  " + index);
                            }

                            progress += step;
                            busyProgress(progress);
                        }

                        if (min != null) {
                            minDate = min.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                            Platform.runLater(() -> {
                                canCollect = false;
                                fromDatePicker.setDateTimeValue(minDate);
                                canCollect = true;
                            });
                        } else {
                            minDate = null;
                        }

                        if (max != null) {
                            maxDate = max.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                            Platform.runLater(() -> {
                                canCollect = false;
                                toDatePicker.setDateTimeValue(maxDate);
                                canCollect = true;
                            });
                        }

                        log.debug("End import");
                        log.debug("Start collect rows data");
                        Platform.runLater(() -> {
                            collectQueries();
                        });
                        log.debug("End collect data");
                    }

                } catch (SQLException ex) {

                    Platform.runLater(() -> DialogHelper.showError(getController(), "Error", ex.getMessage(), getController().getSelectedConnectionSession().get().getConnectionParam(), ex));
                    makeBusy(false);
                } finally {
                    dbProfilerService.stopCaching();
                }
            }
        });
    }
    
    private static final Long MAX_SLOW_LOG_SIZE = 1024 * 1024 * 1024l;
    
    @FXML
    public void osSlowQueryLog(ActionEvent event) {
        makeBusy(true);
        Thread th = new Thread(() -> {
            
            String logPath = null;
            boolean enabled = false;
            QueryResult qr = new QueryResult();
            try {
                
                String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__VALIABLE_SLOW_QUERY_LOG);
                if (!query.endsWith(";")) {
                    query += ";";
                }
                
                query += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__VALIABLE_SLOW_QUERY_LOG_FILE);
                if (!query.endsWith(";")) {
                    query += ";";
                }
                
                query += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_DATADIR_VARIABLE);
                
                getDbService().execute(query, qr);
                if (qr.getResult() != null && !qr.getResult().isEmpty()) {
                    ResultSet rs = (ResultSet) qr.getResult().get(0);
                    if (rs.next()) {
                        enabled = "ON".equals(rs.getString(2));
                    }
                    rs.close();
                    
                    rs = (ResultSet) qr.getResult().get(1);
                    if (rs.next()) {
                        logPath = rs.getString(2);
                    }
                    rs.close();
                    
                    rs = (ResultSet) qr.getResult().get(2);
                    if (rs.next()) {
                        String basedir = rs.getString(2);
                        if (basedir != null && (logPath.startsWith(".\\") || logPath.startsWith("./") || (!getConnectionParams().isRemote() && PlatformUtil.isWindows()))) {
                            
                            if (!basedir.endsWith(String.valueOf(File.separatorChar))) {
                                basedir += File.separatorChar;
                            }
                            
                            if (logPath.startsWith(".\\") || logPath.startsWith("./")) {
                                logPath = logPath.substring(2);
                            }
                                                        
                            logPath = basedir + logPath;
                        }
                    }
                    rs.close();
                }
            } catch (SQLException ex) {
                Platform.runLater(() -> {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                });
                makeBusy(false);
                return;
            }
            
            if (enabled) {

                if (getConnectionParams().isRemote()) {

                    if (getConnectionParams().hasOSDetails()) {

                        OsDownloader downloader = new OsDownloader(getConnectionParams());
                        downloader.connectToRemote();

                        try {
                            Long fileSize = downloader.fileSize(logPath);
                            if (fileSize > MAX_SLOW_LOG_SIZE) {
                                DialogHelper.showInfo(getController(), "Info", "The Slow Query Log File size is more than 1 GB. It causes poor performance. Please flush Slow log on daily base", null);
                                downloader.disconnectFromRemote();
                            } else {

                                String slowLogPath = logPath;

                                Platform.runLater(() -> {
                                    ChooseDayPeriod msController = StandardBaseControllerProvider.getController(getController(), "ChooseDayPeriod");
                                    msController.init(getStage());

                                    if (msController.getDays() != null) {
                                        Thread th0 = new Thread(() -> {
                                            try {
                                                String data = downloader.downloadErrorSlowLogFileLines(slowLogPath, msController.getDays());
                                                parseSlowLogData(Arrays.asList(new String[] {data}), false);
                                            } catch (Throwable ex) {
                                                Platform.runLater(() -> {
                                                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                                                });
                                            }

                                            downloader.disconnectFromRemote();
                                        });
                                        th0.setDaemon(true);
                                        th0.start();
                                    } else {                            
                                        downloader.disconnectFromRemote();
                                        makeBusy(false);
                                    }
                                });
                            }
                        } catch (Throwable ex) {
                            Platform.runLater(() -> {
                                DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                            });
                            downloader.disconnectFromRemote();
                            makeBusy(false);
                        }

                    } else {
                        Platform.runLater(() -> {
                            DialogHelper.showInfo(getController(), "Info", "Please enter OS details", null);

                            OsDetailsController osController = StandardBaseControllerProvider.getController(getController(), "OsDetails");
                            osController.init(getStage(), connectionParamService, getConnectionParams());
                            if (osController.getSelectedConnParam() != null) {
                                connectionParams = osController.getSelectedConnParam();
                            }
                        });

                        makeBusy(false);
                        return;
                    }

                } else {
                    try {
                        File f = new File(logPath);
                        if (f.exists()) {

                            // check file size
                            if (f.length() > MAX_SLOW_LOG_SIZE) {
                                Platform.runLater(() -> {
                                    DialogHelper.showError(getController(), "Error", "The Slow Query Log File size is more than 1 GB. It causes poor performance. Please flush Slow log on daily base");
                                }); 
                                makeBusy(false);
                            } else {
                                if (PlatformUtil.isWindows()) {
                                    try {
                                        String data = new String(Files.readAllBytes(f.toPath()), "UTF-8");
                                        parseSlowLogData(Arrays.asList(new String[] {data}), false);
                                    } catch (Throwable ex) {
                                        Platform.runLater(() -> {
                                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                                        });
                                        makeBusy(false);
                                    }
                                } else {
                                    
                                    OsDownloader downloader = new OsDownloader(getConnectionParams());
                                    try {                                        
                                        Long fileSize = downloader.fileSize(logPath);
                                        if (fileSize > MAX_SLOW_LOG_SIZE) {
                                            DialogHelper.showInfo(getController(), "Info", "The Slow Query Log File size is more than 1 GB. It causes poor performance. Please flush Slow log on daily base", null);
                                            makeBusy(false);
                                        } else {

                                            String slowLogPath = logPath;

                                            Platform.runLater(() -> {
                                                ChooseDayPeriod msController = StandardBaseControllerProvider.getController(getController(), "ChooseDayPeriod");
                                                msController.init(getStage());

                                                if (msController.getDays() != null) {
                                                    Thread th0 = new Thread(() -> {
                                                        try {
                                                            String data = downloader.downloadErrorSlowLogFileLines(slowLogPath, msController.getDays());
                                                            parseSlowLogData(Arrays.asList(new String[] {data}), false);
                                                        } catch (Throwable ex) {
                                                            Platform.runLater(() -> {
                                                                DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                                                            });
                                                        }

                                                        downloader.disconnectFromRemote();
                                                    });
                                                    th0.setDaemon(true);
                                                    th0.start();
                                                } else {                            
                                                    downloader.disconnectFromRemote();
                                                    makeBusy(false);
                                                }
                                            });
                                        }
                                    } catch (Throwable ex) {
                                        Platform.runLater(() -> {
                                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                                        });
                                        makeBusy(false);
                                    }
                                }
                            }
                        } else {
                            String message = "Error Log file \"" + logPath + "\" not found";
                            Platform.runLater(() -> {
                                DialogHelper.showError(getController(), "Error", message);
                            });
                            makeBusy(false);
                        }
                    } catch (Throwable ex) {
                        log.error(ex.getMessage(), ex);
                        makeBusy(false);
                    }
                }
                
            } else {
                Platform.runLater(() -> {
                    DialogHelper.showInfo(getController(), "Info", "Slow Query log is not enable in your system. Please enable it. How to enable slow query log? www.smartmysql.com/how-to-enable-slow-query-log-in-mysql/", null);
                });
                makeBusy(false);
            }
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void clearLog(ActionEvent event) {
        DialogResponce dr = DialogHelper.showConfirm(getController(), "Clear data", "Do you Want to Remove all Content in the Dashboard?");
        if (dr == DialogResponce.OK_YES || dr == DialogResponce.YES_TO_ALL)  {
            parseSlowLogData(new ArrayList<String>(), false);        
        }
    }    
    
    @FXML
    public void selectSlowQueryLog(ActionEvent event) {

        MultiSelectFilesController msController = StandardBaseControllerProvider.getController(getController(), "MultiSelectFiles");
        msController.init(getStage());

        if (msController.getSelectedFiles() != null && !msController.getSelectedFiles().isEmpty()) {
            addSlowQueryLogFiles(msController.getSelectedFiles(), false);
        }
    }
    
    private void addSlowQueryLogFiles(List<File> files, boolean append) {
        long totalLength = 0;
        for (File userFile: files) {
            totalLength += userFile.length();
        }

        if (totalLength / 1024 / 1024 > 1024) {
            DialogHelper.showInfo(getController(), "Can not load data", "files greater than 1GB are not allowed for processing slow query log!", null);
            return;
        }

        List<String> datas = new ArrayList<>();
        for (File userFile: files) {
            try {
                datas.add(new String(Files.readAllBytes(userFile.toPath()), "UTF-8"));
            } catch (Throwable ex) {
                DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
            }
        }

        parseSlowLogData(datas, append);
    }
    
    private void parseSlowLogData(List<String> datas, boolean append) {
        

        makeBusy(true);

        getController().addBackgroundWork(new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                String serverHashCode = serverHashCode(getController().getSelectedConnectionSession().get().getConnectionParam(), "");

                try {

                    if (!append) {
                        currentDate = new Date();
                    }

                    dbProfilerService.clearOldSlowData(currentDate);
                    dbProfilerService.prepareSlowQueryTables(serverHashCode, currentDate);

                    dbProfilerService.startCaching();

                    if (slowQueries != null) {
                        slowQueries.clear();
                    }

                    analyzerReportQueriesInited = false;
                    slowQueries = new ArrayList<>();

                    Date min = null;
                    Date max = null;
                    

                    for (String data: datas) {

                        int currentRows = slowQueries.size();

//                            Pattern.compile(
//                                    "(# Time: (?<time>.*)\\n)?"
//                                    + "# User@Host: (?<userHost>.*)\\n"
//                                    + "# Query_time: (?<queryTime>.*) Lock_time: (?<lockTime>.*) Rows_sent: (?<rowsSent>.*) Rows_examined: (?<rowsExamined>.*)"
//                                    + "(\\nuse (?<database>.*);)?"
//                                    + "\\nSET timestamp=(?<timestamp>.*)"
//                                    + "\\n(?<query>.*;)");

                        Pattern p = Pattern.compile("# Query_time: (?<queryTime>.*) Lock_time: (?<lockTime>.*) Rows_sent: (?<rowsSent>.*) Rows_examined: (?<rowsExamined>.*)");
                        String time = null;
                        String userHost = null;
                        String queryTime = null;
                        String query = null;
                        String lock_time = null;
                        String rows_sent = null;
                        String rows_examined = null;
                        String database = null;

                        Date date = null;

                        for (String line : data.split("\n")) {

                            if (query != null && query.trim().endsWith(";") || line.startsWith("# Time:")) {
                                if (query != null) {

                                    date = processRow(date, queryTime, time, userHost, lock_time, rows_sent, rows_examined, database, query);

                                    if (date != null && (min == null || min.after(date))) {
                                        min = date;
                                    }

                                    if (date != null && (max == null || max.before(date))) {
                                        max = date;
                                    }

                                    time = null;
                                    userHost = null;
                                    queryTime = null;
                                    query = null;
                                    lock_time = null;
                                    rows_sent = null;
                                    rows_examined = null;
                                }
                            }

                            if (line.startsWith("# User@Host:")) {

                                if (query != null) {

                                    date = processRow(date, queryTime, time, userHost, lock_time, rows_sent, rows_examined, database, query);

                                    if (date != null && (min == null || min.after(date))) {
                                        min = date;
                                    }

                                    if (date != null && (max == null || max.before(date))) {
                                        max = date;
                                    }

                                    time = null;
                                    userHost = null;
                                    queryTime = null;
                                    query = null;
                                    lock_time = null;
                                    rows_sent = null;
                                    rows_examined = null;
                                }

                                userHost = line.substring(12).trim();
                            }

                            if (line.startsWith("# Query_time:")) {
                                Matcher m = p.matcher(line);
                                if (m.find()) {
                                    queryTime = m.group("queryTime");
                                    lock_time = m.group("lockTime");
                                    rows_sent = m.group("rowsSent");
                                    rows_examined = m.group("rowsExamined");
                                }
                            }

                            if (line.toLowerCase().startsWith("use ")) {
                                database = line.substring(4).trim();
                                if (database.endsWith(";")) {
                                    database = database.substring(0, database.length() - 1);
                                }
                            }

                            if (line.startsWith("SET timestamp=")) {
                                time = line.substring(14).trim();
                                if (time.endsWith(";")) {
                                    time = time.substring(0, time.length() - 1);
                                }
                            } else if (time != null) {

                                if (query == null) {
                                    query = line;
                                } else {
                                    query += "\n" + line;
                                }
                            }
                        }
                        
                        if (query != null) {

                            date = processRow(date, queryTime, time, userHost, lock_time, rows_sent, rows_examined, database, query);

                            if (date != null && (min == null || min.after(date))) {
                                min = date;
                            }

                            if (date != null && (max == null || max.before(date))) {
                                max = date;
                            }

                            time = null;
                            userHost = null;
                            queryTime = null;
                            query = null;
                            lock_time = null;
                            rows_sent = null;
                            rows_examined = null;
                        }

                        if (currentRows == slowQueries.size()) {
                            // This can be just CSV structure
                            // lets check it
                            try {
                                CSVReader csvReader = new CSVReader(new StringReader(data));

                                date = null;

                                String[] columnsStr = csvReader.readNext();
                                String[] columns = columnsStr[0].split("\t");

                                int queryTimeIndex = -1;
                                int startTimeIndex = -1;
                                int userHostTimeIndex = -1;
                                int lockTimeIndex = -1;
                                int rowSentIndex = -1;
                                int rowsExaminedIndex = -1;
                                int databaseIndex = -1;
                                int sqlTextIndex = -1;

                                int i = 0;
                                for (String c: columns) {
                                    switch (c) {
                                        case "query_time":
                                            queryTimeIndex = i;
                                            break;

                                        case "start_time":
                                            startTimeIndex = i;
                                            break;

                                        case "user_host":
                                            userHostTimeIndex = i;
                                            break;

                                        case "lock_time":
                                            lockTimeIndex = i;
                                            break;

                                        case "rows_sent":
                                            rowSentIndex = i;
                                            break;

                                        case "rows_examined":
                                            rowsExaminedIndex = i;
                                            break;

                                        case "db":
                                            databaseIndex = i;
                                            break;

                                        case "sql_text":
                                            sqlTextIndex = i;
                                            break;
                                    }
                                    i++;
                                }

                                String[] csvDataStr = csvReader.readNext();
                                String[] csvData = csvDataStr[0].split("\t");
                                while(csvData != null) {

                                    date = processRow(
                                            date, 
                                            csvData[queryTimeIndex], 
                                            csvData[startTimeIndex], 
                                            csvData[userHostTimeIndex], 
                                            csvData[lockTimeIndex], 
                                            csvData[rowSentIndex], 
                                            csvData[rowsExaminedIndex], 
                                            csvData[databaseIndex], 
                                            csvData[sqlTextIndex]);

                                    if (date != null && (min == null || min.after(date))) {
                                        min = date;
                                    }

                                    if (date != null && (max == null || max.before(date))) {
                                        max = date;
                                    }

                                    csvDataStr = csvReader.readNext();
                                    csvData = csvDataStr[0].split("\t");
                                }
                            } catch (Throwable th) {
                                log.error("Error", th);
                            }
                        }
                    }


                    if (min != null) {
                        
                        LocalDateTime minLDT = min.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                        
                        if (!append || minDate == null) {
                            minDate = minLDT;
                        } else {
                            if (minLDT.compareTo(minDate) < 0) {
                                minDate = minLDT;
                            }
                        }
                        Platform.runLater(() -> {
                            canCollect = false;
                            fromDatePicker.setDateTimeValue(minDate);
                            canCollect = true;
                        });
                    } else {
                        if (!append) {
                            minDate = null;
                        }
                    }

                    if (max != null) {
                        
                        LocalDateTime maxLDT = max.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                        
                        if (!append || maxDate == null) {
                            maxDate = maxLDT;
                        } else {
                            if (maxLDT.compareTo(maxDate) > 0) {
                                maxDate = maxLDT;
                            }
                        }
                        
                        Platform.runLater(() -> {
                            canCollect = false;
                            toDatePicker.setDateTimeValue(maxDate);
                            canCollect = true;
                        });
                    } else {
                        if (!append) {
                            maxDate = null;
                        }
                    } 

                    int index = 0;

                    double step = 0.7 / slowQueries.size();
                    double progress = 0.0;

                    int count = slowQueries.size();

                    long startTime = new Date().getTime();

                    for (SlowQuery sq: slowQueries) {
                        dbProfilerService.save(sq.gpl, currentDate, true, serverHashCode, sq.lockTime, sq.rowsSent, sq.rowsExamined);

                        index++;
                        if (index % 10000 == 0) {
                            log.debug("imported:  " + index);
                        }

                        progress += step;
                        busyProgress(progress);

                        // draw estimate time - not correct but....
                        if (index % 1000000 == 0) {
                            long currentTime = new Date().getTime();

                            long diff = currentTime - startTime;
                            startTime = currentTime;

                            int left = ((count - index) / 1000000) + 1;

                            diff *= left;

                            String remainText = "";

                            long mins = diff / (1000 * 60);
                            if (mins > 0) {
                                remainText = mins + " mins";
                            } else {

                                long secs = diff / 1000;
                                if (secs > 0) {
                                    remainText = secs + " secs";
                                } else {
                                    remainText = diff + " msecs";
                                }
                            }

                            if (!remainText.isEmpty()) {
                                busyMessage("About " + remainText + " remaining");
                            }
                        }
                    }

                    Platform.runLater(() -> {
                        collectQueries();
                    });

                } catch (Throwable ex) {
                    makeBusy(false);
                } finally {
                    dbProfilerService.stopCaching();
                }
            }
        });
    }
    
    
    private Date processRow(Date date, String queryTime, String time, String userHost, String lock_time, String rows_sent, String rows_examined, String database, String query) {
        Integer id = null;
        BigDecimal lockTime = null;
        Integer rowsSent = null;
        Integer rowsExamined = null;
        String user = null;
        String host = null;

        BigDecimal qTime = BigDecimal.ZERO;

        if (queryTime != null) {
            try {
                qTime = new BigDecimal(queryTime.trim());
            } catch (Throwable th) {
                // maybe its time 00:00:00
                try {
                    String[] s0 = queryTime.trim().split(":");
                    qTime = new BigDecimal(s0[0]).multiply(new BigDecimal(60 * 60)).add(new BigDecimal(s0[1]).multiply(new BigDecimal(60))).add(new BigDecimal(s0[2]));
                } catch (Throwable th0) {}
            }
        }

        if (time != null) {
            time = time.trim();
            try {
                if (time.endsWith(";")) {
                    time = time.substring(0, time.length() - 1);
                }
                date = new Date(Long.valueOf(time) * 1000);
            } catch (Throwable th) {
                try {
                    date = TIME_FORMATTER.parse(time);
                } catch (Throwable th0) {}
            }
        }

        

        //get id, user, host
        int idIdx = userHost.indexOf("Id:");
        int hostIdx = userHost.indexOf("@");

        if (lock_time != null) {
            try {
                lockTime = new BigDecimal(lock_time.trim());
            } catch (Throwable th) {
                // maybe its time 00:00:00
                try {
                    String[] s0 = lock_time.trim().split(":");
                    lockTime = new BigDecimal(s0[0]).multiply(new BigDecimal(60 * 60)).add(new BigDecimal(s0[1]).multiply(new BigDecimal(60))).add(new BigDecimal(s0[2]));
                } catch (Throwable th0) {}
            }
        }

        if (rows_sent != null) {
            try {
                rowsSent = Integer.valueOf(rows_sent.trim());
            } catch (Throwable th) {}
        }

        if (rows_examined != null) {
            try {
                rowsExamined = Integer.valueOf(rows_examined.trim());
            } catch (Throwable th) {}
        }

        if (idIdx >= 0) {
            try {
                id = Integer.valueOf(userHost.substring(idIdx + 3).trim());
            } catch (Throwable th) {}
        }

        if (hostIdx >= 0) {
            user = userHost.substring(0, hostIdx).trim();
            if (user.contains("[")) {
                String tempUser = user.replaceAll("\\[.*\\]", "").trim();
                if (tempUser.isEmpty()) {
                    user = user.substring(user.indexOf("[") + 1, user.lastIndexOf("]"));
                } else {
                    user = tempUser;
                }
            }

            host = idIdx >= 0 ? userHost.substring(hostIdx + 1, idIdx).trim() : userHost.substring(hostIdx + 1).trim();
            if (host.contains("[")) {
                String tempHost = host.replaceAll("\\[.*\\]", "").trim();
                if (tempHost.isEmpty()) {
                    host = host.substring(host.indexOf("[") + 1, host.lastIndexOf("]"));
                } else {
                    host = tempHost;
                }
            }
        }
        
        query = QueryFormat.removeComments(query);
//        try {
//            QueryTemplate qt = QueryTemplateCoding.makeQueryTemplate(query);
//            query = qt.getTemplate();
//        } catch (TemplateException ex) {
//            log.error("Error", ex);
//        }
        
        GlobalProcessList gpl = new GlobalProcessList(
            id,
            user,
            host,
            database != null ? database : "",
            "Query",
            qTime,
            "",
            query.trim(),
            date
        );

        SlowQuery sq = new SlowQuery();
        sq.gpl = gpl;
        sq.lockTime = lockTime;
        sq.rowsSent = rowsSent;
        sq.rowsExamined = rowsExamined;

        slowQueries.add(sq);
        
        return date;
    }
    
    
    public void importIntoCurrentDatabaseSlowQueryLog(ActionEvent event)
    {
        ConnectionSession cs = getController().getSelectedConnectionSession().get();
        if (cs != null)
        {
            try {
                cs.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__CREATE_SQ_TABLE));
            } catch (SQLException ex) {
                // do nothing its ok
            }
            
            try {
                cs.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__CLEAR_SQ_TABLE));
            
                if (slowQueries != null)
                {
                    try {

                        PreparedStatement ps = cs.getService().getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__INSERT_SQ_TABLE));
 
                        for (SlowQuery sq: slowQueries)
                        {
                            ps.setString(1, sq.gpl.getUser());
                            ps.setString(2, sq.gpl.getHost());
                            ps.setString(3, sq.gpl.getDb());
                            ps.setString(4, sq.gpl.getQuerytemp());
                            ps.setBigDecimal(5, sq.gpl.getTime());
                            ps.setDate(6, new java.sql.Date(sq.gpl.getDatetime().getTime()));
                            ps.setBigDecimal(7, sq.lockTime);
                            ps.setInt(8, sq.rowsSent);
                            ps.setInt(9, sq.rowsExamined);

                            ps.execute();                        
                        }
                        
                        DialogHelper.showInfo(getController(), "Done", "Import done", null, getStage());
                    } catch (SQLException ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage());
                    }
                }
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, getController().getSelectedConnectionSession().get().getConnectionParam(),getStage());
            }            
        }
    }
    
    @FXML 
    public void exportToXls(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            byte[] topTablesImage = topTablesTable.getChartImage();
            byte[] topHostsImage = topHostsTable.getChartImage();            
            byte[] topUsersImage = topUsersTable.getChartImage();
            byte[] topSchemasImage = topSchemasTable.getChartImage();
            
            getController().addBackgroundWork(new WorkProc() {
                @Override
                public void updateUI() { }

                @Override
                public void run() {

                    List<String> sheets = new ArrayList<>();
                    List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                    List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                    List<List<Integer>> totalColumnWidths = new ArrayList<>();
                    List<byte[]> images = new ArrayList<>();
                    List<Integer> heights = new ArrayList<>();
                    List<Integer> colspans = new ArrayList<>();

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (SlowQueryData sq : (List<SlowQueryData>) slowQueryTable.getItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            Integer count = calcRating(sq, sq.getQueryRating().doubleValue());
                            if (count != null) {
                                Image img = ratingImages.get(count);
                                if (img == null) {
                                    switch (count) {
                                        case 0:
                                            img = StandardDefaultImageProvider.getInstance().getRating_0_image();
                                            break;

                                        case 1:
                                            img = StandardDefaultImageProvider.getInstance().getRating_1_image();
                                            break;

                                        case 2:
                                            img = StandardDefaultImageProvider.getInstance().getRating_2_image();
                                            break;

                                        case 3:
                                            img = StandardDefaultImageProvider.getInstance().getRating_3_image();
                                            break;

                                        case 4:
                                            img = StandardDefaultImageProvider.getInstance().getRating_4_image();
                                            break;

                                        case 5:
                                            img = StandardDefaultImageProvider.getInstance().getRating_5_image();
                                            break;
                                    }
                                    ratingImages.put(count, img);
                                }

                                byte[] res = ratingImagesBytes.get(count);
                                if (res == null) {
                                    BufferedImage bImage = SwingFXUtils.fromFXImage(img, null);
                                    ByteArrayOutputStream s = new ByteArrayOutputStream();
                                    try {
                                        ImageIO.write(bImage, "png", s);
                                        res = s.toByteArray();
                                    } catch (Throwable e) {
                                        log.error("Error loading image bytes", e);
                                    }
                                }
                                row.add(res != null ? res : "");
                            } else {
                                row.add("");
                            }

                            row.add(sq.getQueryTemplate());
                            row.add(convertToTime(sq.getResponceTime(), true));
                            row.add(sq.getPtc());
                            row.add(sq.getCalls());
                            row.add(convertToTime(sq.getAvgResponceTime(), true));
                            row.add(sq.getDatabase());
                            row.add(sq.getTable());
                            row.add(convertToTime(sq.getMinResponceTime(), true));
                            row.add(convertToTime(sq.getMaxResponceTime(), true));
                            row.add(convertToTime(sq.getStddevResponceTime(), true));
                            row.add(convertToTime(sq.getLockTime(), true));
                            row.add(convertToTime(sq.getAvgLockTime(), true));
                            row.add(sq.getRowsSent());
                            row.add(sq.getAvgRowsSent());
                            row.add(sq.getRowsExamined());
                            row.add(sq.getAvgRowsExamined());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("QueryRating", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("QueryRating").getWidth());

                        columns.put("Query", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("Query").getWidth());

                        columns.put("Responce Time", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("Responce Time").getWidth());

                        columns.put("PCT(%)", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("PCT(%)").getWidth());

                        columns.put("Calls", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("Calls").getWidth());

                        columns.put("AvgResponce Time", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("AvgResponce Time").getWidth());

                        columns.put("Database", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("Database").getWidth());

                        columns.put("Table", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("Table").getWidth());

                        columns.put("MinResponce Time", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("MinResponce Time").getWidth());

                        columns.put("MaxResponce Time", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("MaxResponce Time").getWidth());

                        columns.put("StddevResponce Time", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("StddevResponce Time").getWidth());

                        columns.put("LockTime", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("LockTime").getWidth());

                        columns.put("AvgLockTime", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("AvgLockTime").getWidth());

                        columns.put("RowsSent", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("RowsSent").getWidth());

                        columns.put("AvgRowsSent", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("AvgRowsSent").getWidth());

                        columns.put("RowsExamined", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("RowsExamined").getWidth());

                        columns.put("AvgRowsExamined", true);
                        columnWidths.add((int) slowQueryTable.getTableView().getColumnByName("AvgRowsExamined").getWidth());

                        sheets.add("Top slow queries");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(null);
                        heights.add(null);
                        colspans.add(null);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (SlowQueryData sq : (List<SlowQueryData>) (List) lockQueriesTable.getItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            Integer count = calcRating(sq, sq.getQueryRating().doubleValue());
                            if (count != null) {
                                Image img = ratingImages.get(count);
                                if (img == null) {
                                    img = new Image(getClass().getResource("/np/com/ngopal/smart/sql/ui/images/rating_" + count + ".png").toExternalForm());
                                    ratingImages.put(count, img);
                                }

                                byte[] res = ratingImagesBytes.get(count);
                                if (res == null) {
                                    BufferedImage bImage = SwingFXUtils.fromFXImage(img, null);
                                    ByteArrayOutputStream s = new ByteArrayOutputStream();
                                    try {
                                        ImageIO.write(bImage, "png", s);
                                        res = s.toByteArray();
                                    } catch (Throwable e) {
                                        log.error("Error loading image bytes", e);
                                    }
                                }
                                row.add(res != null ? res : "");
                            } else {
                                row.add("");
                            }

                            row.add(sq.getQueryTemplate());
                            row.add(convertToTime(sq.getResponceTime(), true));
                            row.add(sq.getPtc());
                            row.add(sq.getCalls());
                            row.add(convertToTime(sq.getAvgResponceTime(), true));
                            row.add(sq.getDatabase());
                            row.add(sq.getTable());
                            row.add(convertToTime(sq.getMinResponceTime(), true));
                            row.add(convertToTime(sq.getMaxResponceTime(), true));
                            row.add(convertToTime(sq.getStddevResponceTime(), true));
                            row.add(convertToTime(sq.getLockTime(), true));
                            row.add(convertToTime(sq.getAvgLockTime(), true));
                            row.add(sq.getRowsSent());
                            row.add(sq.getAvgRowsSent());
                            row.add(sq.getRowsExamined());
                            row.add(sq.getAvgRowsExamined());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("QueryRating", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("QueryRating").getWidth());

                        columns.put("Query", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("Query").getWidth());

                        columns.put("Responce Time", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("Responce Time").getWidth());

                        columns.put("PCT(%)", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("PCT(%)").getWidth());

                        columns.put("Calls", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("Calls").getWidth());

                        columns.put("AvgResponce Time", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("AvgResponce Time").getWidth());

                        columns.put("Database", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("Database").getWidth());

                        columns.put("Table", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("Table").getWidth());

                        columns.put("MinResponce Time", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("MinResponce Time").getWidth());

                        columns.put("MaxResponce Time", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("MaxResponce Time").getWidth());

                        columns.put("StddevResponce Time", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("StddevResponce Time").getWidth());

                        columns.put("LockTime", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("LockTime").getWidth());

                        columns.put("AvgLockTime", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("AvgLockTime").getWidth());

                        columns.put("RowsSent", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("RowsSent").getWidth());

                        columns.put("AvgRowsSent", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("AvgRowsSent").getWidth());

                        columns.put("RowsExamined", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("RowsExamined").getWidth());

                        columns.put("AvgRowsExamined", true);
                        columnWidths.add((int) lockQueriesTable.getTableView().getColumnByName("AvgRowsExamined").getWidth());

                        sheets.add("Top locked queries");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(null);
                        heights.add(null);
                        colspans.add(null);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (ProfilerTopData ptd : (List<ProfilerTopData>) (List) topTablesTable.getCopyItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            row.add(ptd.getSource());
                            row.add(ptd.getTime());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("Table", true);
                        columnWidths.add((int) topTablesTable.getColumnByName("Table").getWidth());

                        columns.put("Time", true);
                        columnWidths.add((int) topTablesTable.getColumnByName("Time").getWidth());

                        sheets.add("Top tables");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(topTablesImage);
                        heights.add(30);
                        colspans.add(3);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (ProfilerTopData ptd : (List<ProfilerTopData>) (List) topHostsTable.getCopyItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            row.add(ptd.getSource());
                            row.add(ptd.getTime());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("Host", true);
                        columnWidths.add((int) topHostsTable.getColumnByName("Host").getWidth());

                        columns.put("Time", true);
                        columnWidths.add((int) topHostsTable.getColumnByName("Time").getWidth());

                        sheets.add("Top hosts");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(topHostsImage);
                        heights.add(30);
                        colspans.add(3);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (ProfilerTopData ptd : (List<ProfilerTopData>) (List) topUsersTable.getCopyItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            row.add(ptd.getSource());
                            row.add(ptd.getTime());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("User", true);
                        columnWidths.add((int) topUsersTable.getColumnByName("User").getWidth());

                        columns.put("Time", true);
                        columnWidths.add((int) topUsersTable.getColumnByName("Time").getWidth());

                        sheets.add("Top users");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(topUsersImage);
                        heights.add(30);
                        colspans.add(3);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (ProfilerTopData ptd : (List<ProfilerTopData>) (List) topSchemasTable.getCopyItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            row.add(ptd.getSource());
                            row.add(ptd.getTime());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("Schema", true);
                        columnWidths.add((int) topSchemasTable.getColumnByName("Schema").getWidth());

                        columns.put("Time", true);
                        columnWidths.add((int) topSchemasTable.getColumnByName("Time").getWidth());

                        sheets.add("Top schemas");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(topSchemasImage);
                        heights.add(30);
                        colspans.add(3);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();
                        for (SlowQueryData sq : (List<SlowQueryData>) (List) analyzerQueryTable.getItems()) {
                            ObservableList row = FXCollections.observableArrayList();

                            Integer count = calcRating(sq, sq.getQueryRating().doubleValue());
                            if (count != null) {
                                Image img = ratingImages.get(count);
                                if (img == null) {
                                    img = new Image(getClass().getResource("/np/com/ngopal/smart/sql/ui/images/rating_" + count + ".png").toExternalForm());
                                    ratingImages.put(count, img);
                                }

                                byte[] res = ratingImagesBytes.get(count);
                                if (res == null) {
                                    BufferedImage bImage = SwingFXUtils.fromFXImage(img, null);
                                    ByteArrayOutputStream s = new ByteArrayOutputStream();
                                    try {
                                        ImageIO.write(bImage, "png", s);
                                        res = s.toByteArray();
                                    } catch (Throwable e) {
                                        log.error("Error loading image bytes", e);
                                    }
                                }
                                row.add(res != null ? res : "");
                            } else {
                                row.add("");
                            }

                            row.add(sq.getQueryTemplate());
                            row.add(sq.getCalls());
                            row.add(sq.getDatabase());
                            row.add(sq.getTable());
                            row.add(convertToTime(sq.getResponceTime(), true));
                            row.add(convertToTime(sq.getAvgResponceTime(), true));
                            row.add(sq.getRecommendedIndex());
                            row.add(sq.getRecommendations());

                            rowsData.add(row);
                        }

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        columns.put("QueryRating", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("QueryRating").getWidth());

                        columns.put("Query", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Query").getWidth());

                        columns.put("Calls", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Calls").getWidth());

                        columns.put("Database", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Database").getWidth());

                        columns.put("Table", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Table").getWidth());

                        columns.put("Responce Time", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Responce Time").getWidth());

                        columns.put("AvgResponce Time", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("AvgResponce Time").getWidth());

                        columns.put("Recommended index", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Recommended index").getWidth());

                        columns.put("Recommendations", true);
                        columnWidths.add((int) analyzerQueryTable.getTableView().getColumnByName("Recommendations").getWidth());

                        sheets.add("SQL Optimizer Report");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);
                        images.add(null);
                        heights.add(null);
                        colspans.add(null);
                    }

                    ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                        @Override
                        public void error(String errorMessage, Throwable th) {
                            Platform.runLater(() -> DialogHelper.showError(getController(), "Error", errorMessage, th, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage()));
                        }

                        @Override
                        public void success() {
                            Platform.runLater(() -> DialogHelper.showInfo(getController(), "Success", "Data export seccessfull", "", getStage()));
                        }
                    });
                }
            });
        }
    }
    
    private void initAnalyzerQueryDataTable(FilterableTableView tableView) {
        TableColumn queryRating = createColumn("QueryRating", "queryRating");
        queryRating.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new RatingTableCell(false);
            }
        });
        
        TableColumn queryTemplateColumn = createColumn("Query", "queryTemplate");
        queryTemplateColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Slow query template"));
            }
        });
        
        TableColumn callsColumn = createColumn("Calls", "calls");
        callsColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn tableColumn = createColumn("Table", "table");
        tableColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new ExistTableCell();
            }
        });
        
        TableColumn databaseColumn = createColumn("Database", "database");
        databaseColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new ExistDatabaseCell();
            }
        });
        
        TableColumn responceTimeColumn = createColumn("Responce Time", "responceTime");
        responceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn avgResponceTimeColumn = createColumn("AvgResponce Time", "avgResponceTime");
        avgResponceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn recommendedIndexColumn = createColumn("Recommended index", "recommendedIndex");
        recommendedIndexColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Recommended index"));
            }
        });
        
        TableColumn recommendationsColumn = createColumn("Recommendations", "recommendations");
        recommendationsColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Recommendations"));
            }
        });
        
        tableView.addTableColumn(queryRating, 0.1, 110);
        tableView.addTableColumn(queryTemplateColumn, 0.25);
        tableView.addTableColumn(callsColumn, 0.05);
        tableView.addTableColumn(databaseColumn, 0.1);
        tableView.addTableColumn(tableColumn, 0.15);
        tableView.addTableColumn(responceTimeColumn, 0.1, 100);
        tableView.addTableColumn(avgResponceTimeColumn, 0.05, 120);
        
        tableView.addTableColumn(recommendedIndexColumn, 0.20);
        tableView.addTableColumn(recommendationsColumn, 0.20);
       
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
    private void removeColumns(FilterableTableView tableView, String...columns) {
        for (String col: columns) {
            TableColumn tc = tableView.getColumnByName(col);
            if (tc != null) {
                tableView.getTable().getColumns().remove(tc);
            }
        }
    }
    
    private void initQueryDataTable(FilterableTableView tableView) {
        
        TableColumn ptcColumn = createColumn("PCT(%)", "ptc");
        ptcColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell("%", true);
            }
        });
        
        TableColumn callsColumn = createColumn("Calls", "calls");
        callsColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn avgColumn = createColumn("Avg", "avg");
        avgColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn queryRating = createColumn("QueryRating", "queryRating");
        queryRating.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new RatingTableCell(isProfilerMode());
            }
        });
        
        TableColumn tableColumn = createColumn("Table", "table");
        tableColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new ExistTableCell();
            }
        });
        
        TableColumn databaseColumn = createColumn("Database", "database");
        databaseColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new ExistDatabaseCell();
            }
        });        
        
        TableColumn responceTimeColumn = createColumn("Responce Time", "responceTime");
        responceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn minResponceTimeColumn = createColumn("MinResponce Time", "minResponceTime");
        minResponceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn maxResponceTimeColumn = createColumn("MaxResponce Time", "maxResponceTime");
        maxResponceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn avgResponceTimeColumn = createColumn("AvgResponce Time", "avgResponceTime");
        avgResponceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn stddevResponceTimeColumn = createColumn("StddevResponce Time", "stddevResponceTime");
        stddevResponceTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn lockTimeColumn = createColumn("LockTime", "lockTime");
        lockTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn avgLockTimeColumn = createColumn("AvgLockTime", "avgLockTime");
        avgLockTimeColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new TimeTableCell();
            }
        });
        
        TableColumn rowsSentColumn = createColumn("RowsSent", "rowsSent");
        rowsSentColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn avgRowsSentColumn = createColumn("AvgRowsSent", "avgRowsSent");
        avgRowsSentColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn rowsExaminedColumn = createColumn("RowsExamined", "rowsExamined");
        rowsExaminedColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn avgRowsExaminedColumn = createColumn("AvgRowsExamined", "avgRowsExamined");
        avgRowsExaminedColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, Number>, TableCell<SlowQueryData, Number>>() {
            @Override
            public TableCell<SlowQueryData, Number> call(TableColumn<SlowQueryData, Number> param) {
                return new NumberTableCell();
            }
        });
        
        TableColumn queryTemplateColumn = createColumn("Query", "queryTemplate");
        queryTemplateColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Slow query template"));
            }
        });
        
        TableColumn explainColumn = createColumn("Explain", "explain");
        explainColumn.setCellFactory(new Callback<TableColumn<SlowQueryData, String>, TableCell<SlowQueryData, String>>() {
            @Override
            public TableCell<SlowQueryData, String> call(TableColumn<SlowQueryData, String> param) {
                return new ExplainTableCell();
            }
        });
        
        TableColumn qedtColumn = createColumn("QEDT", "graph1");
        qedtColumn.setCellFactory(new Callback<TableColumn<GraphData, String>, TableCell<GraphData, String>>() {
            @Override
            public TableCell<GraphData, String> call(TableColumn<GraphData, String> param) {
                return new GraphQEDTTableCell(getStage());
            }
        });
        
        TableColumn qetiColumn = createColumn("QETI", "graph2");
        qetiColumn.setCellFactory(new Callback<TableColumn<GraphData, String>, TableCell<GraphData, String>>() {
            @Override
            public TableCell<GraphData, String> call(TableColumn<GraphData, String> param) {
                return new GraphQETITableCell(getStage());
            }
        });
        
        
        tableView.addTableColumn(queryRating, 0.08, 110);
        tableView.addTableColumn(queryTemplateColumn, 0.25);
        tableView.addTableColumn(responceTimeColumn, 0.08);
        tableView.addTableColumn(ptcColumn, 0.04);
        tableView.addTableColumn(callsColumn, 0.04);
        tableView.addTableColumn(avgResponceTimeColumn, 0.1);
        
        tableView.addTableColumn(qedtColumn, 0.04);
        tableView.addTableColumn(qetiColumn, 0.04);
        
        tableView.addTableColumn(databaseColumn, 0.1);
        tableView.addTableColumn(tableColumn, 0.15);      
        tableView.addTableColumn(minResponceTimeColumn, 0.1);
        tableView.addTableColumn(maxResponceTimeColumn, 0.1);
        
        tableView.addTableColumn(stddevResponceTimeColumn, 0.1);
        tableView.addTableColumn(lockTimeColumn, 0.06);
        tableView.addTableColumn(avgLockTimeColumn, 0.07);
        tableView.addTableColumn(rowsSentColumn, 0.05);
        tableView.addTableColumn(avgRowsSentColumn, 0.07);
        tableView.addTableColumn(rowsExaminedColumn, 0.08);
        tableView.addTableColumn(avgRowsExaminedColumn, 0.1);
        tableView.addTableColumn(explainColumn, 0.06);
       
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
         
    private static final String EMPTY_NUM = "0";

    @Override
    public void userChartsChanged() {
    }

    @Override
    public void scrollForward() {
    }

    @Override
    public void scrollBackward() {
    }

    @Override
    public void zoomInAction() {
    }

    @Override
    public void zoomOutAction() {
    }

    @Override
    public void showAllAction() {
    }

    @Override
    public void exportToXls(List<String> sheets, List<ObservableList<ObservableList>> totalData, List<LinkedHashMap<String, Boolean>> totalColumns, List<List<Integer>> totalColumnWidths, List<byte[]> images, List<Integer> heights, List<Integer> colspans) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void saveProfilerData(ProfilerData pd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProfilerInnerControllerWorkProcAuth getInnerControllerWorkProcAuthority() {
        return slowQueryAnalyzerTabControllerWorkProcAuth;
    }
    
    private class NumberTableCell extends TableCell<SlowQueryData, Number> {    
        
        private final String sufix;
        private final boolean fillPercents;
        
        public NumberTableCell() {
            this("", false);
        }
        
        public NumberTableCell(String sufix, boolean fillPercents) {
            this.sufix = sufix;
            this.fillPercents = fillPercents;
        }
        
        @Override
        protected void updateItem(Number item, boolean empty) {
            super.updateItem(item, empty); 
            
            if (fillPercents) {
                setStyle("");
            }
            
            if (!empty) {
                if (item != null) {
                    long v = Math.round(item.doubleValue());
                    setText(String.valueOf(v) + sufix);
                    
                    if (fillPercents) {
                        if (v == 100) {
                            setStyle("-fx-background-insets: 2; -fx-background-color: linear-gradient(from 0% 100% to 100% 100%, blue 0%, white " + v + "%)");
                        } else {
                            setStyle("-fx-background-insets: 2; -fx-background-color: linear-gradient(from 0% 100% to 100% 100%, blue 0%, white " + v + "%)");
                        }
                    }
                } else {
                    setText(EMPTY_NUM);
                }
            } else {
                setText("");
            }
            setAlignment(Pos.CENTER_RIGHT);
        }
    }
    
    private class TimeTableCell extends TableCell<SlowQueryData, Number> {        
        @Override
        protected void updateItem(Number item, boolean empty) {
            super.updateItem(item, empty); 
            if (!empty) {
                this.setText(convertToTime(item, true));
            } else {
                setText("");
            }
            
            this.setAlignment(Pos.CENTER_RIGHT);
        }
    }
    
    private class ExistDatabaseCell extends TableCell<SlowQueryData, String> {        
        
        private static final int DEFAULT_MAX_LENGTH = 50;
        
        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty); 
            
            if (!empty && item != null) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                
                String text = "";
                TextFlow tf = new TextFlow();
                String[] strs = item.split(",");
                
                boolean hasRed = false;
                for (String s: strs) {
                    s = s.trim();
                    if (!s.isEmpty()) {
                        String ss = s;
                        if (!tf.getChildren().isEmpty()) {
                            tf.getChildren().add(new Text(", "));
                            text += ", ";
                        }
                        
                        Text t = new Text(ss);
                        text += ss;
                        if (!DatabaseCache.getInstance().hasDatabase(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), s)) {
                            
                            boolean found = false;
                            for (Database db: getController().getSelectedConnectionSession().get().getConnectionParam().getDatabases()) {
                                if (db.getName().equals(s)) {
                                    found = true;
                                }
                            }
                            
                            if (!found) {
                                t.setFill(Color.RED);
                                hasRed = true;
                            }
                        }
                        tf.getChildren().add(t);                        
                    }
                }
                
                if (text.length() >= DEFAULT_MAX_LENGTH) {
                    
                    String str = text;
                    Button b = new Button(NumberFormater.compactByteLength(text.getBytes().length));
                    b.setPadding(new Insets(1));
                    b.setFocusTraversable(false);
                    b.setMinWidth(40);
                    b.setPrefWidth(40);
                    b.setMaxWidth(40);
                    b.setOnAction((ActionEvent event) -> {
                        final Stage dialog = new Stage();
                        SlowQueryTemplateCellController cellController = StandardBaseControllerProvider.getController(getController(), "SlowQueryTemplateCell");
                        cellController.init(str, false);

                        final Parent p = cellController.getUI();
                        dialog.initStyle(StageStyle.UTILITY);

                        final Scene scene = new Scene(p);
                        dialog.setScene(scene);
                        dialog.initModality(Modality.WINDOW_MODAL);
                        dialog.initOwner(getController().getStage());
                        dialog.setTitle(getTableColumn().getText());

                        dialog.showAndWait();
                    });
                    
                    HBox box = new HBox();
                    box.setPadding(new Insets(5, 0, 5, 0));
                    box.setAlignment(Pos.CENTER_LEFT);
                    HBox b0 = new HBox(new Group(tf));
                    b0.setAlignment(Pos.CENTER_RIGHT);
                    b0.setMaxWidth(Double.MAX_VALUE);
                    box.getChildren().addAll(b0, b);
                    
                    HBox.setHgrow(b0, Priority.ALWAYS);
                    
                    setGraphic(box);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    
                } else {
                    setText(text);
                    setGraphic(new Group(tf));
                }
                
                if (hasRed) {
                    setTooltip(new Tooltip("Database and Table/s does not exist in current connected DB server."));
                } else {
                    setTooltip(null);
                }
                
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
                setText("");
                setTooltip(null);
            }
            
            this.setAlignment(Pos.CENTER_RIGHT);
        }
    }
    
    
    private class ExistTableCell extends TableCell<SlowQueryData, String> { 
        
        private static final int DEFAULT_MAX_LENGTH = 40;
        
        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty); 
            
            if (!empty && item != null) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                
                String text = "";
                TextFlow tf = new TextFlow();
                String[] strs = item.split(",");
                Set<String> exist = new HashSet<>();
                boolean hasRed = false;
                for (String s: strs) {
                    s = s.trim();
                        
                    if (s.contains(".")) {
                        s = s.substring(s.lastIndexOf(".") + 1);
                    }
                    
                    if (s.startsWith("`") && s.endsWith("`")) {
                        s = s.substring(1, s.length() - 1);
                    }

                    if (!s.isEmpty()) {
                        
                        if (!exist.contains(s)) {
                            exist.add(s);
                            
                            String ss = s;
                            
                            if (!tf.getChildren().isEmpty()) {
                                tf.getChildren().add(new Text(", "));
                                text += ", ";
                            }
                            Text t = new Text(ss);
                            text += ss;
//                            if (!DatabaseCache.getInstance().hasTable(controller.getSelectedConnectionSession().get().getConnectionParam(), s)) {
//                                t.setFill(Color.RED);
//                                hasRed = true;
//                            }
                            tf.getChildren().add(t);
                        }
                    }                
                }
                    
                
                if (text.length() >= DEFAULT_MAX_LENGTH) {
                    
                    String str = text;
                    Button b = new Button(NumberFormater.compactByteLength(text.getBytes().length));
                    b.setPadding(new Insets(1));
                    b.setFocusTraversable(false);
                    b.setMinWidth(40);
                    b.setPrefWidth(40);
                    b.setMaxWidth(40);
                    b.setOnAction((ActionEvent event) -> {
                        final Stage dialog = new Stage();
                        SlowQueryTemplateCellController cellController = StandardBaseControllerProvider.getController(getController(), "SlowQueryTemplateCell");
                        cellController.init(str, false);

                        final Parent p = cellController.getUI();
                        dialog.initStyle(StageStyle.UTILITY);

                        final Scene scene = new Scene(p);
                        dialog.setScene(scene);
                        dialog.initModality(Modality.WINDOW_MODAL);
                        dialog.initOwner(getController().getStage());
                        dialog.setTitle(getTableColumn().getText());

                        dialog.showAndWait();
                    });
                    
                    HBox box = new HBox();
                    box.setPadding(new Insets(5, 0, 5, 0));
                    box.setAlignment(Pos.CENTER_LEFT);
                    HBox b0 = new HBox(new Group(tf));
                    b0.setMaxWidth(Double.MAX_VALUE);
                    b0.setAlignment(Pos.CENTER_RIGHT);
                    box.getChildren().addAll(b0, b);
                    
                    HBox.setHgrow(b0, Priority.ALWAYS);
                    
                    setGraphic(box);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    
                } else {
                    setText(text);
                    setGraphic(new Group(tf));
                }
                
                if (hasRed) {
                    setTooltip(new Tooltip("Database and Table/s does not exist in current connected DB server."));
                } else {
                    setTooltip(null);
                }
                
                
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
                setText("");
                setTooltip(null);
            }
            
            this.setAlignment(Pos.CENTER_RIGHT);
        }
    }
    
    private class RatingTableCell extends TableCell<SlowQueryData, Number> {     
        
        private boolean profilerMode = false;
        public RatingTableCell(boolean profilerMode) {
            this.profilerMode = profilerMode;
        }
        
        @Override
        protected void updateItem(Number item, boolean empty) {
            super.updateItem(item, empty); 
            
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                
                HBox box = new HBox();
                if (item != null) {
                    Integer count = profilerMode ? calcProfilerRating(item.doubleValue()) : calcRating((SlowQueryData)getTableRow().getItem(), item.doubleValue());
                    if (count != null) {
                        Image img = ratingImages.get(count);
                        if (img == null) {
                            switch (count) {
                                case 0:
                                    img = StandardDefaultImageProvider.getInstance().getRating_0_image();
                                    break;
                                    
                                case 1:
                                    img = StandardDefaultImageProvider.getInstance().getRating_1_image();
                                    break;
                                    
                                case 2:
                                    img = StandardDefaultImageProvider.getInstance().getRating_2_image();
                                    break;
                                    
                                case 3:
                                    img = StandardDefaultImageProvider.getInstance().getRating_3_image();
                                    break;
                                    
                                case 4:
                                    img = StandardDefaultImageProvider.getInstance().getRating_4_image();
                                    break;
                                    
                                case 5:
                                    img = StandardDefaultImageProvider.getInstance().getRating_5_image();
                                    break;
                            }
                            ratingImages.put(count, img);
                        }
                        ImageView iv = new ImageView(img);
                        iv.setOnMouseClicked((MouseEvent event) -> {
                            if (event.getClickCount() == 2 && !explainShowed) {
                                explainShowed = true;
                                SlowQueryData sqd = (SlowQueryData)getTableRow().getItem();
                                showExplainPopup(sqd, sqd.getExplain(), RatingTableCell.this);
                            }
                        });
                        box.getChildren().add(iv);
                    }
                }
                box.setOnMouseClicked((MouseEvent event) -> {
                    if (event.getClickCount() == 2 && !explainShowed) {
                        explainShowed = true;
                        SlowQueryData sqd = (SlowQueryData)getTableRow().getItem();
                        showExplainPopup(sqd, sqd.getExplain(), RatingTableCell.this);
                    }
                });
                setGraphic(box);
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
                setText("");
            }
        }
    }
    
    private Integer calcProfilerRating(double value) {
//        less than 100 rows then  5 green stars 
//                  1000 rows then 4 green stars
//		    10000 rows then 3 green  stars 
//		    50000 row then 2 green stars 
//                  100000 rows then 1 green start
//                  >=100000 rows then 0 green start 	

        if (value < 100) {
            return 5;
        } else if (value < 100) {
            return 4;
        } else if (value < 10000) {
            return 3;
        } else if (value < 50000) {
            return 2;
        } else if (value < 100000) {
            return 1;
        }
        
        return 0;
    }
    
    
    private Integer calcRating(SlowQueryData data, double value) {
        if (data != null && data.getQueryTemplate() != null) {
            String query = data.getQueryTemplate().toLowerCase();
            if (query.startsWith("select") || query.startsWith("update") || query.startsWith("delete") || query.startsWith("insert")) {

                InsertCommand command = null;
                // Keep all green stars for all INSERTS without selects commands
                if (query.startsWith("insert")) {
                    command = QueryFormat.parseInsertQuery(query);
                    if (command.getSelectClause() == null) {
                        return 5;
                    }
                }
                
                // Keep all green stars if all DELETE / UPDATE/ INSERT(with select) where total calls = Rows examined 
                // Keep all green stars if all DELETE / UPDATE/ INSERT(with select) where total calls =< 100* Rows examined
                // Keep all green stars if all DELETE / UPDATE/ INSERT(with select) where total calls =< 1000*Rows examined
                // Keep all green stars if all DELETE / UPDATE/ INSERT(with select) where total calls =< 10000*Rows examined
                if (command != null || query.startsWith("update") || query.startsWith("delete")) {
                    
                    double calls = data.getCalls() != null ? data.getCalls().doubleValue() : 0.0;
                    double rowsExamined = data.getRowsExamined() != null ? data.getRowsExamined() : 0.0;
                    
                    if (calls <= 10000 * rowsExamined) {                        
                        return 5;
                    } else {
                        return 0;
                    }
                }
                
                if (data.getRowsSent() > 0 || query.startsWith("select")) {
                    if (query.contains(" sum(") || 
                        query.contains(" avg(") || 
                        query.contains(" min(") || 
                        query.contains(" max(") || 
                        query.contains(" count(")) {

                        value *= 10;
                    }

                    int count = 0;
                    if (value > 33 || (data.getRowsExamined() == 0.0 && data.getRowsSent() == 0)) {
                        count = 5;
                    } else if (value >= 20) {
                        count = 4;
                    } else if (value >= 10) {
                        count = 3;
                    } else if (value >= 4) {
                        count = 2;
                    } else if (value >= 1) {
                        count = 1;
                    }

                    return count;
                }
            }
        }
        
        return null;
    }
    
    
    private static final String SEC      = " sec";
    private static final String MILISEC  = " ms";
    private static final String MICROSEC = " μs";
    private static final String EMPTY    = "0 μs";
    
    private String convertToTime(Number item, boolean round) {
        if (item != null) {
            double v = item.doubleValue();
            if ((int)v > 0) {
                return (round ? (int)(v + 0.5) : new BigDecimal(v).setScale(2, RoundingMode.HALF_UP)) + SEC;
                
            } else if ((int)(v * 1000) > 0) {
                return (round ? (int)(v * 1000 + 0.5) : new BigDecimal(v * 1000).setScale(2, RoundingMode.HALF_UP)) + MILISEC;
                
            } else if ((int)(v * 1000000) > 0) {
                return (round ? (int)(v * 1000000 + 0.5) : new BigDecimal(v * 1000000).setScale(2, RoundingMode.HALF_UP)) + MICROSEC;
                
            }
        }
        
        return EMPTY;
    }
    
    private class ExplainTableCell extends TableCell<SlowQueryData, String> {
        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            
            SlowQueryData p = (SlowQueryData)getTableRow().getItem();
            
            if (!empty && p != null) {
                
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                
                if (p.getRowsSum() != null) {
                    setGraphic(createRatingNode(p, p.getRowsSum(), item));
                } else {
                    Hyperlink link = new Hyperlink("EXPLAIN");
                    link.setFocusTraversable(false);
                    link.setOnAction((ActionEvent event) -> {
                        showExplainPopup(p, item, ExplainTableCell.this);
                    });
                    
                    setGraphic(link);
                }
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
        
        
        
        private Node createRatingNode(SlowQueryData p, Long rowsSum, String query) {
            HBox box = new HBox();
            box.setCursor(Cursor.HAND);
            box.setOnMouseClicked((MouseEvent event) -> {
                showExplainPopup(p, query, box);
            });
            
            if (rowsSum < 10) {
                box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_green_image()));
            } else if (rowsSum < 100) {
                box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_blue_image()));
            } else if (rowsSum < 1000) {
                box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_yellow_image()));
            } else if (rowsSum < 10000) {
                box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
            } else if (rowsSum < 20000) {
                box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
            } else if (rowsSum < 30000) {
                box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
            } else if (rowsSum < 50000) {
                box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
            } else {
                box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
            }
            
            return box;
        }
    }
    
    private void showExplainPopup(SlowQueryData p, String query, Node node) {
        
        // full usage exipred
//        if (licenseFullUsageExipred) {
//            return;
//        }
        
        makeBusy(true);
        getController().addBackgroundWork(new WorkProc() {
            @Override
            public void updateUI() { }

            @Override
            public void run() {
                String explainQuery = QueryFormat.convertToExplainQuery("EXPLAIN", query);

                if (explainQuery != null) {

                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    if (session != null) {
                        Database database;
                        String db = p.getDatabase();

                        // get schema from table names if exist
                        Map<String, String> tableNames = QueryFormat.getTableNamesFromQuery(query);
                        if (tableNames != null && !tableNames.isEmpty()) {
                            for (String key : tableNames.keySet()) {
                                if (key.contains(".")) {
                                    db = key.split("\\.")[0];
                                    if (db.startsWith("`")) {
                                        db = db.substring(1);
                                    }

                                    if (db.endsWith("`")) {
                                        db = db.substring(0, db.length() - 1);
                                    }

                                    break;
                                }
                            }
                        }

                        if (db != null) {
                            db = db.split(",")[0].trim();
                            database = DatabaseCache.getInstance().getDatabase(session.getConnectionParam().cacheKey(), db);
                            if (database == null) {
                                try {
                                    database = DatabaseCache.getInstance().syncDatabase(db, session.getConnectionParam().cacheKey(), (AbstractDBService) session.getService());
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            }
                        } else {
                            database = session.getConnectionParam().getSelectedDatabase();
                        }

                        if (database != null) {
                            Database currentDatabase = database;

                            // Visual explain
//                            QueryAnalyzer qa = new QueryAnalyzer(columnCardinalityService, currentDatabase, session.getConnectionParam(), session.getService(), query);
                            try {
                                
                                ((MysqlDBService) session.getService()).setServiceBusy(true);
//                                QORecommendation recommendation = QueryOptimizer.analyzeQuery(
//                                    QODataProvider.create(
//                                        session.getService(), 
//                                        columnCardinalityService, 
//                                        qars, 
//                                        performanceTuningService, 
//                                        session.getConnectionParam()), 
//                                    database.getName(), 
//                                    query, 
//                                    true
//                                );
//                                qa.analyze(null);

                                // SQL Optimizer
                                final QueryAnalyzerTabController analyzerTabController =  StandardBaseControllerProvider.getController(getController(), "QueryAnalyzerTab");
                                analyzerTabController.setColumnCardinalityService(columnCardinalityService);
                                analyzerTabController.setPerformanceTuningService(performanceTuningService);
                                analyzerTabController.setPreferenceService(preferenceService);
                                analyzerTabController.setQars(qars);
                                analyzerTabController.setService(session.getService());
                                analyzerTabController.setUsageService(usageService);
                                analyzerTabController.init(query, session, currentDatabase, true, RunnedFrom.SQAQA);
                                analyzerTabController.setAreaEditable(false);
                                
                                Platform.runLater(() -> {
                                    final Stage stage = new Stage();
                                    stage.initOwner(getStage());
                                    stage.initModality(Modality.APPLICATION_MODAL);

                                    TabPane tabPane = new TabPane();
                                    tabPane.setPrefSize(1000, 600);

                                    Tab explainTab = showVisualExplain(currentDatabase, query, getController(), columnCardinalityService, preferenceService, performanceTuningService, qars, usageService, tabPane);
                                    
                                    

                                    Tab queryAnalyzerTab = new Tab("SQL Optimizer");
                                    queryAnalyzerTab.setClosable(false);
                                    queryAnalyzerTab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getAnalyzer_image(16, 16)));
                                    queryAnalyzerTab.setContent(analyzerTabController.getUI());

                                    tabPane.getTabs().addAll(explainTab, queryAnalyzerTab);
                                    tabPane.getSelectionModel().select(queryAnalyzerTab);
                                    tabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
                                        if (newValue == queryAnalyzerTab) {
                                            SwingUtilities.invokeLater(() -> {
                                                analyzerTabController.analyzeQueryAction(null);
                                            });
                                        }
                                    });

                                    Scene scene = new Scene(new BorderPane(), 1000, 600);
                                    ((BorderPane) scene.getRoot()).setCenter(tabPane);
                                    stage.setScene(scene);

                                    Platform.runLater(() -> {
                                        makeBusy(false);
                                        explainShowed = false;
                                        stage.showAndWait();
                                    });
                                });

                            } catch (Throwable ex) {
                                explainShowed = false;
                                Platform.runLater(() -> {
                                    makeBusy(false);
                                    if (ex instanceof SQLSyntaxErrorException) {
                                        if (((SQLSyntaxErrorException) ex).getSQLState().startsWith("42")) {
                                            DialogHelper.showError(getController(), "Error", "The user don't have privileges for this operation.", null, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage());
                                        } else {
                                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage());
                                        }
                                    } else {
                                        DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage());
                                    }
                                });
                            } finally {
                                ((MysqlDBService) session.getService()).setServiceBusy(false);
                            }
                        } else {
                            explainShowed = false;
                            Platform.runLater(() -> {
                                makeBusy(false);
                                showPopup(new Popup(), new Label("Database and Table/s are not existed in current connected DB server."), node, 700, 150);
                            });
                        }
                    } else {
                        explainShowed = false;
                        Platform.runLater(() -> {
                            makeBusy(false);
                            showPopup(new Popup(), new Label("Database and Table/s does not exist in current connected DB server."), node, 700, 150);
                        });
                    }
                }
            }
        });
    }
    
    private class StringLimitTableCell extends TableCell<SlowQueryData, String> {
        
        private final int limit;
        public StringLimitTableCell(int limit) {
            this.limit = limit;
        }
        
        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty); 
            if (item != null && item.length() >= limit) {
                setTooltip(new Tooltip(item));
                item = item.substring(0, limit) + "..";
            }
            this.setText(item);
        }
    }
    
    
    private void showPopup(Popup popup, Node content, Node node, double w, double h) {
        
        HBox box = new HBox(content);
        HBox.setHgrow(node, Priority.ALWAYS);
        box.setMaxSize(w, h);
        box.getStyleClass().add("profilerPopup");

        popup.setAutoHide(true);
        popup.getContent().addAll(box);

        Point2D windowCoord = new Point2D(node.getScene().getWindow().getX(), node.getScene().getWindow().getY());
        Point2D sceneCoord = new Point2D(node.getScene().getX(), node.getScene().getY());

        Point2D nodeCoord = node.localToScene(0.0, 0.0);

        double clickX = Math.round(windowCoord.getX() + sceneCoord.getY() + nodeCoord.getX());
        double clickY = Math.round(windowCoord.getY() + sceneCoord.getY() + nodeCoord.getY());

        popup.setX(clickX);
        popup.setY(clickY);

        popup.show(getController().getStage());
    }
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    public void init(ConnectionParams params) {
        this.connectionParams = params;
        try { slowQueryDBService.connect(params); }
        catch (SQLException ex) { log.error("Error connect with current connection params", ex); }
        
        usageService.savePUsage(PUsageFeatures.SQA);
        
        if (!LicenseManager.getInstance().fullUsageExpired(getController())) {
            licenseFullUsageExipred = false;
            LicenseManager.getInstance().usedFullUsage();
        }
    }
    
    private void copyData(TableView table, KeyEvent event) {
        if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.C) {

            List<TablePosition> l = table.getSelectionModel().getSelectedCells();
            if (l != null && !l.isEmpty())
            {
                Clipboard clipboard = Clipboard.getSystemClipboard();
                ClipboardContent content = new ClipboardContent();

                TablePosition pos = l.get(0);

                Object item = table.getItems().get(pos.getRow());
                TableColumn col = pos.getTableColumn();
            
                Object data = col.getCellObservableValue(item).getValue();

                content.putString(data != null ? data.toString() : "null");
                clipboard.setContent(content);
            }
        }
    }
    
    public void injectServiceEntities(
            ProfilerDataManager _dbProfilerService, 
            PreferenceDataService _preferenceService,  
            QARecommendationService _qars, 
            ColumnCardinalityService _columnCardinalityService, 
            QAPerformanceTuningService _performanceTuningService,
            ConnectionParamService _connectionParamService,
            UserUsageStatisticService _usageService) {
        
        connectionParamService = _connectionParamService;
        dbProfilerService = _dbProfilerService;
        preferenceService = _preferenceService;
        qars = _qars;
        columnCardinalityService = _columnCardinalityService;
        performanceTuningService = _performanceTuningService;
        usageService = _usageService;
    }
    
    private void checkDragAndDrop() {
        if (!isProfilerMode() && (slowQueryTable == null || slowQueryTable.getItems() == null || slowQueryTable.getItems().isEmpty())) {
            dragAndDropPane.toFront();
        } else {
            dragAndDropPane.toBack();
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        checkDragAndDrop();        
        
        mainUI.setOnDragOver(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                if (!isProfilerMode()) {
                    if (event.getGestureSource() != mainUI && event.getDragboard().hasFiles()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    }
                    event.consume();

                    dragAndDropPane.toFront();
                }
            }
        });
        
        mainUI.setOnDragExited(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                if (!isProfilerMode()) {
                    checkDragAndDrop();
                }
            }
        });
        
        mainUI.setOnDragDone(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                if (!isProfilerMode()) {
                    checkDragAndDrop();
                }
            }
        });

        mainUI.setOnDragDropped(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                if (!isProfilerMode()) {
                    Dragboard db = event.getDragboard();
                    boolean success = false;
                    if (db.hasFiles()) {
                        addSlowQueryLogFiles(db.getFiles(), true);
                        success = true;
                    }
                    /* let the source know whether the string was successfully 
                     * transferred and used */
                    event.setDropCompleted(success);

                    event.consume();

                    checkDragAndDrop();
                }
            }
        });
        
        fromDatePicker.setShowButtons(false);
        toDatePicker.setShowButtons(false);
             
        fromDatePicker.dateTimeValueProperty().addListener((ObservableValue<? extends LocalDateTime> observable, LocalDateTime oldValue, LocalDateTime newValue) -> {
            
            if (newValue != null && minDate != null && maxDate != null && (newValue.isBefore(minDate) || newValue.isAfter(maxDate))) {
                fromDatePicker.setDateTimeValue(minDate);
            }
            
            if (canCollect) {
                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void updateUI() {}
                    @Override
                    public void run() { collectQueries(); }
                });
            }
        });
        
        toDatePicker.dateTimeValueProperty().addListener((ObservableValue<? extends LocalDateTime> observable, LocalDateTime oldValue, LocalDateTime newValue) -> {
            if (newValue != null && minDate != null && maxDate != null && (newValue.isBefore(minDate) || newValue.isAfter(maxDate))) {
                toDatePicker.setDateTimeValue(maxDate);
            }
            
            if (canCollect) {
                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void updateUI() {}
                    @Override
                    public void run() { collectQueries(); }
                });
            }
        });
        
        currentDate = new Date();
        
        slowQueryTable.getTableView().getTextProperty().bind(filterField.textProperty());                                
        slowQueryTable.getTableView().getRegexStageProperty().bind(regexCheckBox.selectedProperty());                                
        slowQueryTable.getTableView().getMatchStateProperty().bind(notMatchCheckBox.selectedProperty());
        
        slowQueryTable.getTableView().getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(slowQueryTable.getTableView().getTable(), event);
        });
        
        initQueryDataTable(slowQueryTable.getTableView());
        
        TableColumn responceTimeColumn = slowQueryTable.getTableView().getColumnByName("Responce Time");
        responceTimeColumn.setSortType(TableColumn.SortType.DESCENDING);
        slowQueryTable.getTableView().addColumnToSortOrder(responceTimeColumn);
        
        
        
        lockQueriesTable.getTableView().getTextProperty().bind(filterField.textProperty());                                
        lockQueriesTable.getTableView().getRegexStageProperty().bind(regexCheckBox.selectedProperty());                                
        lockQueriesTable.getTableView().getMatchStateProperty().bind(notMatchCheckBox.selectedProperty());
        
        lockQueriesTable.getTableView().getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(lockQueriesTable.getTableView().getTable(), event);
        });
        
        initQueryDataTable(lockQueriesTable.getTableView());
        
        TableColumn lockTimeColumn = lockQueriesTable.getTableView().getColumnByName("LockTime");
        lockTimeColumn.setSortType(TableColumn.SortType.DESCENDING);
        lockQueriesTable.getTableView().addColumnToSortOrder(lockTimeColumn);        
        
        
        analyzerQueryTable.getTableView().getTextProperty().bind(filterField.textProperty());                                
        analyzerQueryTable.getTableView().getRegexStageProperty().bind(regexCheckBox.selectedProperty());                                
        analyzerQueryTable.getTableView().getMatchStateProperty().bind(notMatchCheckBox.selectedProperty());
        
        analyzerQueryTable.getTableView().getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(analyzerQueryTable.getTableView().getTable(), event);
        }); 
        
        initAnalyzerQueryDataTable(analyzerQueryTable.getTableView());
        
        TableColumn responceTimeColumn0 = analyzerQueryTable.getTableView().getColumnByName("Responce Time");
        responceTimeColumn0.setSortType(TableColumn.SortType.DESCENDING);
        analyzerQueryTable.getTableView().addColumnToSortOrder(responceTimeColumn0);
        
        
        topTablesTable.init("Top Tables", "Table", "Source", "Threads", "threads", "Time", "Time");
        topTablesTable.getTable().getSelectionModel().setCellSelectionEnabled(true);
        topTablesTable.getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(topTablesTable.getTable(), event);
        });
        
        topSchemasTable.init("Top Schemas", "Schema", "Source", "Threads", "threads", "Time", "Time");
        topSchemasTable.getTable().getSelectionModel().setCellSelectionEnabled(true);
        topSchemasTable.getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(topSchemasTable.getTable(), event);
        });
        
        topUsersTable.init("Top Users", "User", "Source", "Threads", "threads", "Time", "Time");
        topUsersTable.getTable().getSelectionModel().setCellSelectionEnabled(true);
        topUsersTable.getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(topUsersTable.getTable(), event);
        });
        
        topHostsTable.init("Top Hosts", "Host", "Source", "Threads", "threads", "Time", "Time");
        topHostsTable.getTable().getSelectionModel().setCellSelectionEnabled(true);
        topHostsTable.getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(topHostsTable.getTable(), event);
        });
        
        analyzerReportQueries.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue && !analyzerReportQueriesInited) {
                analyzerReportQueriesInited = true;
                
                analyzerQueryTable.getTableView().makeBusy(true);
                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void updateUI() {}

                    @Override
                    public void run() { 
                        
//                    ((MainController)getController()).usedQueryAnalyzerFromSlowQueryAnalyzer();
                    if (analyzerQueryTable.getItems() != null && !analyzerQueryTable.getItems().isEmpty()) {
                        
                        lastUsedDatabase = null;
                        
                        boolean useCache = preferenceService.getPreference().isQaOtherUseCache();
                        
                        double step = 1.0 / analyzerQueryTable.getItems().size();
                        double progress = 0.0;
                        
                        boolean showExpired = true;
                        
                        for (FilterableData fd: (List<FilterableData>)analyzerQueryTable.getItems()) {

                            SlowQueryData ptq = (SlowQueryData)fd;
                            String q = ptq.getQueryTemplate().toLowerCase();
                            if (q.startsWith("select") || q.startsWith("insert") || q.startsWith("update")) {

                                if (ptq.getRowsSent().doubleValue() == ptq.getRowsExamined()) {
                                    log.info("Same rowsSent and rowsExamined for: " + ptq.getQueryTemplate());
                                    progress += step;
                                    continue;
                                }

                                if (q.startsWith("select")) {
                                    SelectCommand sc = QueryFormat.parseSelectQuery(q);
                                    if (sc != null && sc.getFromClause() == null) {
                                        log.info("Without FROM clause: " + ptq.getQueryTemplate());
                                        progress += step;
                                        continue;
                                    }
                                }

                                ConnectionSession session = getController().getSelectedConnectionSession().get();
                                if (ptq.getDatabase() != null && !ptq.getDatabase().isEmpty()) {

                                    Database database = DatabaseCache.getInstance().getDatabase(session.getConnectionParam().cacheKey(), ptq.getDatabase());
                                    if (database == null) {
                                        try {
                                            database = DatabaseCache.getInstance().syncDatabase(ptq.getDatabase(), session.getConnectionParam().cacheKey(), (AbstractDBService) session.getService());
                                        } catch (SQLException ex) {
                                            log.error("Error", ex);
                                        }
                                    }

                                    if (database != null) {
                                        if (ptq.getTable() != null && !ptq.getTable().isEmpty()) {

                                            boolean find = false;
                                            String[] tables = ptq.getTable().split(",");
                                            for (String t: tables) {
                                                t = t.trim();

                                                if (t.contains(".")) {
                                                    t = t.substring(t.lastIndexOf(".") + 1);
                                                }

                                                if (t.startsWith("`") && t.endsWith("`")) {
                                                    t = t.substring(1, t.length() - 1);
                                                }

                                                if (DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), ptq.getDatabase(), t.trim()) == null) {
                                                    ptq.setRecommendedIndex("");
                                                    ptq.setRecommendations("Table not found");   
                                                    find = true;
                                                    break;
                                                }
                                            }

                                            if (find) {
                                                progress += step;
                                                continue;
                                            }

                                        } else {

                                            ptq.setRecommendedIndex("");
                                            ptq.setRecommendations("Table not found");                
                                            progress += step;
                                            continue;
                                        }


                                        boolean hasAnalyzerResults = false;
                                        try {
                                            if (lastUsedDatabase == null || !lastUsedDatabase.equals(database.getName())) {
                                                lastUsedDatabase = database.getName();
                                                slowQueryDBService.changeDatabase(database);
                                            }

                                            boolean stopedAnalyze = false;
                                            String issueError = null;

                                            ScriptRunner sr = new ScriptRunner(slowQueryDBService.getDB(), true);
                                            Map<Integer, List<String>> queries = sr.findQueries(new StringReader(ptq.getExplain()));
                                            if (queries != null) {            
                                                List<QORecommendation> results = new ArrayList<>();
                                                for (List<String> list: queries.values()) {

                                                    if (list != null) {
                                                        //boolean f = false;
                                                        for (String query: list) {

        //                                                    if (!f) {
        //                                                        query = "UPDATE `sakila`.`actor` SET `first_name` = '1' WHERE `actor_id` = 'actor_id';";
        //                                                        f = true;
        //                                                    } else {
        //                                                        query = "DELETE FROM `sakila`.`actor` WHERE `actor_id` = 'actor_id';";
        //                                                    }
                                                            try {
                                                                QORecommendation qaRecommendations = QACache.getQARecommandation(
                                                                        getController(), 
                                                                        qars, 
                                                                        columnCardinalityService,
                                                                        performanceTuningService, 
                                                                        database,
                                                                        query, 
                                                                        !useCache,
                                                                        true,
                                                                        true,
                                                                        licenseFullUsageExipred,
                                                                        showExpired);
                                                                results.add(qaRecommendations);
                                                                
                                                                showExpired = qaRecommendations.isHasCredits();
                                                            } catch (Throwable th) {
                                                                log.error("Error", th);

                                                                if (th instanceof SQLSyntaxErrorException) {
                                                                    if ("42000".equals(((SQLSyntaxErrorException)th).getSQLState())) {
                                                                        issueError = th.getMessage();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                String recommendations = "";
                                                String recommendedIndex = "";
                                                for (QORecommendation ars: results) {
                    
                                                    for (List<QOIndexRecommendation> list: ars.getIndexes().values()) {
                                                        for (QOIndexRecommendation rr: list) {

                                                            if (rr.getRecommendation() != null) {
                                                                if (!recommendations.isEmpty()) {
                                                                    recommendations += "; ";
                                                                }

                                                                recommendations += rr.getRecommendation();
                                                            }

                                                            if (rr.getIndexQuery() != null) {
                                                                if (!recommendedIndex.isEmpty()) {
                                                                    recommendedIndex += "; ";
                                                                }

                                                                recommendedIndex += rr.getIndexQuery().toUpperCase();
                                                            }
                                                            
                                                            hasAnalyzerResults = true;
                                                        }
                                                    }
                                                    
                                                    for (List<String> list: ars.getRecommendations().values()) {
                                                        
                                                        for (String s: list) {
                                                            if (!recommendations.contains(s)) {
                                                                if (!recommendations.isEmpty()) {
                                                                    recommendations += "; ";
                                                                }

                                                                recommendations += s;
                                                            }
                                                            
                                                            hasAnalyzerResults = true;
                                                        }
                                                    }
                                                }



                                                if (!hasAnalyzerResults && !stopedAnalyze && issueError == null) {
                                                    recommendations = "The SmartMySQL could not identified any improvements.";
                                                    recommendedIndex = "";
                                                }

                                                if (issueError != null) {
                                                    recommendations = issueError;
                                                    recommendedIndex = "";
                                                }

                                                ptq.setRecommendations(recommendations);
                                                ptq.setRecommendedIndex(recommendedIndex);
                                            }
                                        } catch (Throwable th) {
                                            Platform.runLater(() -> {
                                               DialogHelper.showError(getController(), "Error on analyzing query", th.getMessage(), th, getController().getSelectedConnectionSession().get().getConnectionParam(), getStage()); 
                                            });
                                            analyzerQueryTable.getTableView().makeBusy(false);
                                            return;
                                        }
                                    } else {
                                        ptq.setRecommendedIndex("");
                                        ptq.setRecommendations("Database not found");
                                    }
                                } else {
                                    ptq.setRecommendedIndex("");
                                    ptq.setRecommendations("Database not found");
                                }
                            }

                            progress += step;
                            analyzerQueryTable.getTableView().busyProgress(progress);
                        }

                        Platform.runLater(() -> analyzerQueryTable.getTableView().refresh());
                    }
                    
                    analyzerQueryTable.getTableView().makeBusy(false);
                    }
                });
            }
        });       
    }
    
    private Double doubleValue(Object obj) {
        return obj != null ? ((Number)obj).doubleValue() : null;
    }
    
    private Long longValue(Object obj) {
        return obj != null ? ((Number)obj).longValue() : null;
    }
    
    Map<String, Long> explainCache = new HashMap<>();
    private final Function<Object[], SlowQueryData> FUNCTION__SLOW_QUERY = (Object[] row) -> {
        Double ptc = doubleValue(row[0]);
        Long calls = longValue(row[1]);
        Double avg = doubleValue(row[2]);
        String database = (String)row[3];
        String table = (String)row[4];

        Double responceTime = doubleValue(row[5]);
        Double minResponceTime = doubleValue(row[6]);
        Double maxResponceTime = doubleValue(row[7]);
        Double avgResponceTime = doubleValue(row[8]);
        Double stddevResponceTime = doubleValue(row[9]);

        Double lockTime = doubleValue(row[10]);
        Double avgLockTime = doubleValue(row[11]);

        Long rowsSent = longValue(row[12]);
        Double avgRowsSent = doubleValue(row[13]);

        Double rowsExamined = doubleValue(row[14]);
        Double avgRowsExamined = doubleValue(row[15]);
        
        String queries = (String)row[16];
        
        String queriesIds = null;
        Object ids = row[17];
        if (ids != null) {
           queriesIds = ids.toString();
        }
        
        String explain = null;
        if (queries.isEmpty()) {
            try {
                Long id = Long.valueOf(queriesIds);
                explain = dbProfilerService.getQueryById(id.intValue());
            } catch (Throwable th) {
                explain = queriesIds;
            }            
        } else {
            // find latest id and query
            String[] queriesArray = queries.split("####", -1);
            String[] idsArray = queriesIds.split("####", -1);

            Integer maxId = 0;

            for (int i = 0; i < idsArray.length; i++) {
                String id = idsArray[i];

                Integer value = Integer.valueOf(id);
                if (value > maxId && queriesArray.length > i) {
                    maxId = value;
                    explain = queriesArray[i];
                }
            }
        }
        
        //String query = explain != null ? explain : (String)row[18];
        
        Long queryRating = longValue(row[19]);

        String graph1 = (String)row[20];
        String graph2 = (String)row[21];
        
        String user = (String)row[22];
        String host = (String)row[23];
        
        if (isProfilerMode()) {
            Long rows = explainCache.get(explain);
            if (rows == null) {
                try {
                    getDbService().execute("USE " + database);
                    List<ExplainResult> result = FunctionHelper.explainQuery(getDbService(), explain, null);
                    if (result != null) {
                        for (ExplainResult er: result) {
                            if (rows == null) {
                                rows = er.getRows();
                            } else if (rows > 0){
                                rows *= er.getRows();
                            }                                                
                        }    
                    }

                    explainCache.put(explain, rows);
                } catch (Throwable th) {
                    log.error(th.getMessage(), th);
                }
            }
            
            rowsExamined = rows.doubleValue();
            queryRating = rows;
        }

        SlowQueryData ptq = new SlowQueryData(
            user,
            host,
            ptc,
            calls,
            avg,
            database,
            table,

            responceTime,
            minResponceTime,
            maxResponceTime,
            avgResponceTime,
            stddevResponceTime,

            lockTime,
            avgLockTime,

            rowsSent,
            avgRowsSent,

            rowsExamined,
            avgRowsExamined,

            explain,
            explain,
            queryRating,
            graph1,
            graph2
        );

        return ptq;
    };
    
        
    private void collectQueries() {
        if (getController() != null && getController().getSelectedConnectionSession() != null && getController().getSelectedConnectionSession().get() != null && getController().getSelectedConnectionSession().get().getConnectionParam() != null) {
            String hashCode = serverHashCode(getController().getSelectedConnectionSession().get().getConnectionParam(), "");
            String tableName = dbProfilerService.getSlowQueryTableName(hashCode, currentDate);

            LocalDateTime dateTimeFrom = fromDatePicker.getDateTimeValue() != null ? fromDatePicker.getDateTimeValue() : LocalDateTime.now();
            LocalDateTime dateTimeTo = toDatePicker.getDateTimeValue() != null ? toDatePicker.getDateTimeValue() : LocalDateTime.now();
            
            String dateFrom = dateTimeFrom.format(BEGIN_DATE_FORMATTER);
            String dateTo = dateTimeTo.format(END_DATE_FORMATTER);
            
            Duration duration = Duration.between(dateTimeFrom, dateTimeTo);
            Long diffTime = duration.getSeconds() > 0 ? duration.getSeconds() : 1;
            if (isProfilerMode()) {

                if (!profilerStarted.get()) {
                    makeBusy(true);
                }

                ChartScale currentScale = ChartScale.values()[profilerController.currentScaleIndex().get()];

                long scale = lastDateFrom == null ? currentScale.getValue() : new Date().getTime() - lastDateFrom.getTime();

                canCollect = false;                
                
                toDatePicker.setDateTimeValue(new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());                
                fromDatePicker.setDateTimeValue(new Date(new Date().getTime() - scale).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());

                canCollect = true;
                
                List<SlowQueryData> slowQueryList = collectQueriesData(dbProfilerService.getProfilerTopData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_SLOW_QUERIES), scale, hashCode), FUNCTION__SLOW_QUERY);
                //slowQueryList = fillWithDates(slowQueryList);

                List<SlowQueryData> lockQueriesList = collectQueriesData(dbProfilerService.getProfilerTopData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_LOCK_QUERIES), scale, "%lock%"), FUNCTION__SLOW_QUERY);
                //lockQueriesList = fillWithDates(lockQueriesList);

                List<SlowQueryData> analyzerQueryList = collectQueriesData(dbProfilerService.getProfilerAnalyzerData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_PF_ANALYZE_QUERIES), scale), FUNCTION__SLOW_QUERY);
                //analyzerQueryList = fillWithDates(analyzerQueryList);

                List<ProfilerTopData> topTablesList = collectTopData(dbProfilerService.getProfilerTopData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_TOP_TABLES), scale));
                //topTablesList = fillWithDates(topTablesList);

                List<ProfilerTopData> topSchemasList = collectTopData(dbProfilerService.getProfilerTopData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_TOP_SCHEMAS), scale));
                //topSchemasList = fillWithDates(topSchemasList);

                List<ProfilerTopData> topUsersList = collectTopData(dbProfilerService.getProfilerTopData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_TOP_USERS), scale));
                //topUsersList = fillWithDates(topUsersList);

                List<ProfilerTopData> topHostsList = collectTopData(dbProfilerService.getProfilerTopData(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_TOP_HOSTS), scale));
                //topHostsList = fillWithDates(topHostsList);

                Platform.runLater(() -> {
                    collectQueries(slowQueryList, lockQueriesList, analyzerQueryList, topTablesList, topSchemasList, topUsersList, topHostsList);
                    importIntoCurrentDatabaseButton.setDisable(slowQueries == null || slowQueries.isEmpty());
                    exportToXlsButton.setDisable(slowQueries == null || slowQueries.isEmpty());

                    if (!profilerStarted.get()) {
                        makeBusy(false);
                    }
                });

            } else {
                makeBusy(true);

                WorkProc collectionWorkProcedure = new WorkProc() {
                    @Override
                    public void updateUI() {}

                    @Override
                    public void run() {
                        List<Object[]> data = dbProfilerService.getSlowQueryData(String.format(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_SQ_QUERIES), tableName, tableName, dateFrom, dateTo));
                        List<SlowQueryData> slowQueryList = collectQueriesData(data, FUNCTION__SLOW_QUERY);
                        busyProgress(0.8);
                        List<SlowQueryData> lockQueriesList = collectQueriesData(data, FUNCTION__SLOW_QUERY);
                        List<SlowQueryData> analyzerQueryList = collectQueriesData(data, FUNCTION__SLOW_QUERY);

                        busyProgress(0.9);
                        List<ProfilerTopData> topTablesList = collectTopData(dbProfilerService.getSlowQueryData(String.format(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_SQ_TOP_TABLES), diffTime, tableName)));
                        List<ProfilerTopData> topSchemasList = collectTopData(dbProfilerService.getSlowQueryData(String.format(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_SQ_TOP_SCHEMAS), diffTime, tableName)));
                        List<ProfilerTopData> topUsersList = collectTopData(dbProfilerService.getSlowQueryData(String.format(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_SQ_TOP_USERS), diffTime, tableName)));
                        List<ProfilerTopData> topHostsList = collectTopData(dbProfilerService.getSlowQueryData(String.format(QueryProvider.getInstance().getQuery(dbProfilerService.getStorageType(), QueryProvider.QUERY__LOAD_SQ_TOP_HOSTS), diffTime, tableName)));

                        busyProgress(1);

                        Platform.runLater(() -> {
                            collectQueries(slowQueryList, lockQueriesList, analyzerQueryList, topTablesList, topSchemasList, topUsersList, topHostsList);
                            importIntoCurrentDatabaseButton.setDisable(slowQueries == null || slowQueries.isEmpty());
                            exportToXlsButton.setDisable(slowQueries == null || slowQueries.isEmpty());
                            // need select them for drawing charts
                            mainTabPane.getSelectionModel().select(topTable);
                            mainTabPane.getSelectionModel().select(topHosts);
                            mainTabPane.getSelectionModel().select(topUsers);
                            mainTabPane.getSelectionModel().select(topSchema);
                            mainTabPane.getSelectionModel().select(topQueries);
                            
                            checkDragAndDrop();
                            makeBusy(false);
                        });
                    }
                };

                if(PlatformImpl.isFxApplicationThread()){
                    Recycler.addWorkProc(collectionWorkProcedure);
                }else{
                    collectionWorkProcedure.run();
                }
            }
        }
    }
    
    private void collectQueries(
        List<SlowQueryData> slowQueryList, 
        List<SlowQueryData> lockQueriesList, 
        List<SlowQueryData> analyzerQueryList,
        
        List<ProfilerTopData> topTablesList,
        List<ProfilerTopData> topSchemasList,
        List<ProfilerTopData> topUsersList,
        List<ProfilerTopData> topHostsList) {
        
        try {
//            slowQueryTable.removeItems(slowQueryTable.getSourceData());
//            lockQueriesTable.removeItems(lockQueriesTable.getSourceData());
//            analyzerQueryTable.removeItems(analyzerQueryTable.getSourceData());
            
            
            slowQueryTable.setItems(FXCollections.observableArrayList(slowQueryList));
            lockQueriesTable.setItems(FXCollections.observableArrayList(lockQueriesList));
            analyzerQueryTable.setItems(FXCollections.observableArrayList(analyzerQueryList));
//            slowQueryTable.refresh();
//            
//            lockQueriesTable.addAndRemoveItems(lockQueriesList, new ArrayList<>());
//            lockQueriesTable.refresh();
//            
//            analyzerQueryTable.addAndRemoveItems(analyzerQueryList, new ArrayList<>());
//            analyzerQueryTable.refresh();
           
            
            topTablesTable.setItems(topTablesList);
            topTablesTable.refresh();
            
            topSchemasTable.setItems(topSchemasList);
            topSchemasTable.refresh();
            
            topUsersTable.setItems(topUsersList);
            topUsersTable.refresh();
            
            topHostsTable.setItems(topHostsList);
            topHostsTable.refresh();
            
            
            checkDragAndDrop();
        } catch (Throwable th) {
            log.error("Error collect slow query data", th);
        }
    }
    
    
    private List<ProfilerTopData> collectTopData(List<Object[]> result) {
        
        List<ProfilerTopData> list = new ArrayList<>();
        
        if (result != null) {
            for (Object[] row: result) {
                                
                ProfilerTopData ptd = new ProfilerTopData(
                    (String) row[0],
                    (BigDecimal) row[1],
                    (BigDecimal) row[2]
                );
                
                list.add(ptd);
            }
        }
        
        return list;
    }
    
    private List<SlowQueryData> collectQueriesData(List<Object[]> result, Function<Object[], SlowQueryData> mapper) {
        
        List<SlowQueryData> newOrOldQueries = new ArrayList<>();
        if (result != null) {
            try {
                for (Object[] row: result) {
                    SlowQueryData sqd = mapper.apply(row);
                    // 2018-03-05 Sudheer's task:
                    // "can you remove this filter"
                    if (sqd != null /*&& sqd.getResponceTime() >= 1*/) {
                        newOrOldQueries.add(sqd);
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        
        return newOrOldQueries;
    }
    
    private class SlowQuery {
        private GlobalProcessList gpl;
        
        private BigDecimal lockTime;
        private Integer rowsSent;
        private Integer rowsExamined;        
    }
    
     public void disableQueryUIs(){
        getController().hideApplicationFooter();
        getController().toggleLeftPane();
    }
    
    public void enableQueryUIs() {
        getController().toggleLeftPane();
        getController().showApplicationFooter();
    }

    public void setSlowQueryDBService(MysqlDBService slowQueryDBService) {
        this.slowQueryDBService = slowQueryDBService;
    }

    public void setColumnCardinalityService(ColumnCardinalityService columnCardinalityService) {
        this.columnCardinalityService = columnCardinalityService;
    }
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {
        FunctionHelper.showSlowQueryData(getStage(), getController(), cellData, text, title, preferenceService);
    }
}
