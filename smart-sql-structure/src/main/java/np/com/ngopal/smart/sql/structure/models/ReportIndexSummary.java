
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportIndexSummary implements FilterableData {
    
    private final SimpleStringProperty database;
    private final SimpleStringProperty tableName;
    private final SimpleStringProperty indexName;
    private final SimpleObjectProperty rowsSelected;
    private final SimpleStringProperty selectLatency;
    private final SimpleObjectProperty rowsInserted;
    private final SimpleStringProperty insertLatency;
    private final SimpleObjectProperty rowsUpdated;
    private final SimpleStringProperty updateLatency;
    private final SimpleObjectProperty rowsDeleted;
    private final SimpleStringProperty deleteLatency;
    
 
    public ReportIndexSummary(
            String database, 
            String tableName, 
            String indexName, 
            Long rowsSelected, 
            String selectLatency, 
            Long rowsInserted, 
            String insertLatency, 
            Long rowsUpdated,
            String updateLatency,
            Long rowsDeleted,
            String deleteLatency) {
        
        this.database = new SimpleStringProperty(database);
        this.tableName = new SimpleStringProperty(tableName);
        this.indexName = new SimpleStringProperty(indexName);
        this.rowsSelected = new SimpleObjectProperty(rowsSelected);
        this.selectLatency = new SimpleStringProperty(selectLatency);
        this.rowsInserted = new SimpleObjectProperty(rowsInserted);
        this.insertLatency = new SimpleStringProperty(insertLatency);
        this.rowsUpdated = new SimpleObjectProperty(rowsUpdated);
        this.updateLatency = new SimpleStringProperty(updateLatency);
        this.rowsDeleted = new SimpleObjectProperty(rowsDeleted);
        this.deleteLatency = new SimpleStringProperty(deleteLatency);
    }

    public String getDatabase() {
        return database.get();
    }

    public String getTableName() {
        return tableName.get();
    }

    public String getIndexName() {
        return indexName.get();
    }

    public Long getRowsSelected() {
        return (Long) rowsSelected.get();
    }

    public String getSelectLatency() {
        return selectLatency.get();
    }

    public Long getRowsInserted() {
        return (Long) rowsInserted.get();
    }

    public String getInsertLatency() {
        return insertLatency.get();
    }

    public Long getRowsUpdated() {
        return (Long) rowsUpdated.get();
    }

    public String getUpdateLatency() {
        return updateLatency.get();
    }

    public Long getRowsDeleted() {
        return (Long) rowsDeleted.get();
    }

    public String getDeleteLatency() {
        return deleteLatency.get();
    }
 
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getDatabase() != null ? getDatabase() : "", 
            getTableName() != null ? getTableName() : "",
            getIndexName() != null ? getIndexName() : "", 
            getRowsSelected() != null ? getRowsSelected().toString() : "",
            getSelectLatency() != null ? getSelectLatency() : "",
            getRowsInserted() != null ? getRowsInserted().toString() : "",
            getInsertLatency() != null ? getInsertLatency() : "",
            getRowsUpdated() != null ? getRowsUpdated().toString() : "",
            getUpdateLatency() != null ? getUpdateLatency() : "",
            getRowsDeleted() != null ? getRowsDeleted().toString() : "",
            getDeleteLatency() != null ? getDeleteLatency() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDatabase(),
            getTableName(),
            getIndexName(),
            getRowsSelected(),
            getSelectLatency(),
            getRowsInserted(),
            getInsertLatency(), 
            getRowsUpdated(),
            getUpdateLatency(),
            getRowsDeleted(),
            getDeleteLatency()
        };
    }
}