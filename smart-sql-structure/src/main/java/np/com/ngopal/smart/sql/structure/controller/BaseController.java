package np.com.ngopal.smart.sql.structure.controller;

import java.util.Date;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public abstract class BaseController {
    
    private UIController controller;
    
    public void setController(UIController controller) {
        this.controller = controller;
    }
    
    public UIController getController() {
        return controller;
    }
    
    private BorderPane busyPane;
    private ProgressIndicator indicator;
    private Label indicatorLabel;
    
    private volatile boolean busy = false;

    public void makeBusy(boolean busy) {
        makeBusy(busy, -1, null);
    }
    
    public void makeBusy(boolean busy, long wait, String message) {
        
        this.busy = busy;
        
        if (wait >= 0 && busy) {
            Thread th = new Thread(() -> {
                Long start = new Date().getTime();
                while (BaseController.this.busy)  {
                    if (new Date().getTime() - start >= wait) {
                        busyMessage(message);
                        break;
                    }
                   
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {}
                }
            });
            th.setDaemon(true);
            th.start();
        }
        
        Platform.runLater(() -> {
            if (indicator == null) {
                indicator = new ProgressIndicator(-1);
                indicator.setPrefSize(80, 80);
                indicator.setMinSize(80, 80);
                indicator.setMaxSize(80, 80);
            }

            if (indicatorLabel == null) {
                indicatorLabel = new Label();
                indicatorLabel.setStyle("-fx-font-size: 14; -fx-font-weight: bold;");
            }
            
            indicator.setProgress(-1);
            indicatorLabel.setText("");
            
            if (busyPane == null) {
                busyPane = new BorderPane();
                busyPane.setCursor(Cursor.WAIT);

                VBox box = new VBox(20, new Group(indicator), indicatorLabel);
                box.setAlignment(Pos.CENTER);
                busyPane.setCenter(box);

                busyPane.setStyle("-fx-background-color: rgba(0, 0, 0, 0.4)");
            }

            if (busy) {
                if (getUI().getChildren().contains(busyPane)) {
                    getUI().getChildren().remove(busyPane);
                }
                getUI().getChildren().add(busyPane);
                AnchorPane.setBottomAnchor(busyPane, 0.0);
                AnchorPane.setTopAnchor(busyPane, 0.0);
                AnchorPane.setLeftAnchor(busyPane, 0.0);
                AnchorPane.setRightAnchor(busyPane, 0.0);
            } else {
                getUI().getChildren().remove(busyPane);
            }
        });
    }

    /**
     * Progress from 0 to 1
     *
     * @param progress
     */
    public void busyProgress(double progress) {
        Platform.runLater(() -> {
            indicator.setProgress(progress);
        });
    }

    public void busyMessage(String message) {
        Platform.runLater(() -> {
            indicatorLabel.setText(message);
        });
    }

    
    public void requestFocus() {
        if (getUI() != null) {
            getUI().requestFocus();
        }
    }
        
    public abstract AnchorPane getUI();
    
    public Stage getStage(){
        return (Stage) getUI().getScene().getWindow();
    }
}
