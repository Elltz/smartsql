package np.com.ngopal.smart.sql.structure.misc;

import com.google.common.base.Objects;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ImprovementResult {

    public enum State {
        NEW("New", "white", "black"),
        IN_QUEUE("In Queue", "blue", "white"),
        EXECUTING("Executing", "orange", "black"),
        PROFILER_BEFORE("Collecting the Query profiler details before index/es create", "#ab8a3c", "white"),
        PROFILER_AFTER("Collecting the Query profiler details after index/es create", "#76830b", "white"),
        ERROR("Error", "red", "white"),
        STOPED("Stoped", "white", "black"),
        DONE("Done", "green", "white"),
        DROPED("Droped", "black", "white");
        
        private final String name;
        private final String bgColor;
        private final String color;
        
        State(String name, String bgColor, String color) {
            this.name = name;
            this.bgColor = bgColor;
            this.color = color;
        }
        
        public String getBgColor() {
            return bgColor;
        }

        public String getColor() {
            return color;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    private SimpleStringProperty mainQuery = new SimpleStringProperty();
    
    private SimpleObjectProperty<State> state = new SimpleObjectProperty<>(State.NEW);
    private SimpleStringProperty database = new SimpleStringProperty();
    private SimpleStringProperty table = new SimpleStringProperty();
    private SimpleStringProperty indexName = new SimpleStringProperty();
    private SimpleStringProperty index = new SimpleStringProperty();
    private SimpleStringProperty recomendation = new SimpleStringProperty();
    private SimpleObjectProperty<Long> oldRows = new SimpleObjectProperty<>();
    private SimpleObjectProperty<Long> newRows = new SimpleObjectProperty<>();
    private SimpleStringProperty oldExtra = new SimpleStringProperty();
    private SimpleStringProperty afterImproveExtra = new SimpleStringProperty();
    private SimpleStringProperty improvement = new SimpleStringProperty("Unknown");
    private SimpleBooleanProperty selected = new SimpleBooleanProperty(false);
    
    @Setter
    @Getter
    private QAPerformanceTuning qAPerformanceTuning;
    
    private AnalyzerResult analyzerResult;
        
    public void setAnalyzerResult(AnalyzerResult analyzerResult) {
        this.analyzerResult = analyzerResult;
    }
        
    public AnalyzerResult getAnalyzerResult() {
        return analyzerResult;
    }
    
    public SimpleBooleanProperty selected() {
        return selected;
    }
    
    public boolean canExecute() {
        return state.get() == State.NEW || state.get() == State.DROPED;
    }
    
    public boolean canStop() {
        return state.get() == State.EXECUTING;
    }
    
    public boolean canDrop() {
        return state.get() == State.DONE;
    }
    
    public SimpleObjectProperty<State> state() {
        return state;
    }
    
    public SimpleObjectProperty<Long> oldRows() {
        return oldRows;
    }
    
    public SimpleObjectProperty<Long> newRows() {
        return newRows;
    }
    
    public SimpleStringProperty oldExtra() {
        return oldExtra;
    }
    
    public SimpleStringProperty afterImproveExtra() {
        return afterImproveExtra;
    }
    
    public SimpleStringProperty mainQuery() {
        return mainQuery;
    }
    
    public SimpleStringProperty recomendation() {
        return recomendation;
    }
    
    public SimpleStringProperty database() {
        return database;
    }
        
    public SimpleStringProperty table() {
        return table;
    }
    
    public SimpleStringProperty indexName() {
        return indexName;
    }
    
    
    public SimpleStringProperty index() {
        return index;
    }
    
    public SimpleStringProperty improvement() {
        return improvement;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ImprovementResult && Objects.equal(index.get(), ((ImprovementResult)obj).index.get());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + java.util.Objects.hashCode(this.index.get());
        return hash;
    }
    
    
    public boolean checkExtraTemporary() {
        String oldExtraStr = oldExtra().get();
        String afterImproveExtraStr = afterImproveExtra().get();
        
        return oldExtraStr != null && (oldExtraStr.toLowerCase().contains("using temporary") && (afterImproveExtraStr == null || !afterImproveExtraStr.toLowerCase().contains("using temporary")));
    }
    
    
    public boolean checkExtraFilesort() {
        String oldExtraStr = oldExtra().get();
        String afterImproveExtraStr = afterImproveExtra().get();
        
        return oldExtraStr != null && (oldExtraStr.toLowerCase().contains("using filesort") && (afterImproveExtraStr == null || !afterImproveExtraStr.toLowerCase().contains("using filesort")));
    }
}
