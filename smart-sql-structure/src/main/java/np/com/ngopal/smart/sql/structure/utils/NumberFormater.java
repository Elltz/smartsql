package np.com.ngopal.smart.sql.structure.utils;

import java.text.DecimalFormat;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class NumberFormater {

    private final static String[] units = new String[] { "B", "K", "M", "G", "T" };
    
    private final static DecimalFormat df = new DecimalFormat("#0");
    
    public static String compactByteLength(double length) {
        if (length > 0) {
            int digitGroups = (int) (Math.log10(length) / Math.log10(1024));
            return df.format(length / Math.pow(1024, digitGroups)) + units[digitGroups];
        } else {
            return "0";
        }
    }
}
