package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerTable implements DesignerData, InvalidationListener {
    
    public enum RowFormat {
        DYNAMIC,
        FIXED,
        COMPRESSED;
    }
    
    private DesignerErrDiagram parent;
    
    private final SimpleStringProperty uuid;
    
    private final SimpleStringProperty name;
    private final SimpleStringProperty schema;
    private final SimpleStringProperty collation;
    private final SimpleStringProperty engine;
    private final SimpleStringProperty comments;
    
    private final SimpleStringProperty description;
    
    private final SimpleStringProperty color;
    
    private final SimpleStringProperty autoInc;
    private final SimpleStringProperty avgRowLength;
    private final SimpleStringProperty maxRows;
    private final SimpleStringProperty minRows;
    private final SimpleObjectProperty<RowFormat> rowFormat;
    private final SimpleObjectProperty<Integer> checksum;
    private final SimpleObjectProperty<Integer> delayKeyWrite;
    
    
    private final SimpleDoubleProperty x;
    private final SimpleDoubleProperty y;
    private final SimpleDoubleProperty width;
    private final SimpleDoubleProperty height;
    
    private final SimpleStringProperty layerUuid;
    
    private final ObservableList<DesignerColumn> columns;    
    private final ObservableList<DesignerIndex> indexes;    
    private final ObservableList<DesignerForeignKey> foreignKeys;
    
    private final SimpleBooleanProperty foreignKeysChanged = new SimpleBooleanProperty(false);
    
    private final SimpleBooleanProperty expanded = new SimpleBooleanProperty(true);
    
    private transient int relationCount = 0;
    
    private final WeakInvalidationListener weakListner = new WeakInvalidationListener(this);
    
    // TODO
    // this constructor used in saving state - but it not correct and no need at all save like JSON, just like id of opened schema
    public DesignerTable(String _name, String _schema, String _collation, String _engine, String _comments,
            List<DesignerColumn> _columns, List<DesignerIndex> _indexes, List<DesignerForeignKey> _foreignKeys) {
        name = new SimpleStringProperty(_name);
        name.addListener(weakListner);
        
        schema = new SimpleStringProperty(_schema);
        schema.addListener(weakListner);
        
        collation = new SimpleStringProperty(_collation);//i do not know if i should change them
        collation.addListener(weakListner);
        
        engine = new SimpleStringProperty(_engine);
        engine.addListener(weakListner);
        
        comments = new SimpleStringProperty(_comments);
        comments.addListener(weakListner);
        
        color = new SimpleStringProperty("");
        color.addListener(weakListner);

        // todo i think no need get them from template
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        
        autoInc = new SimpleStringProperty("");
        autoInc.addListener(weakListner);
        
        avgRowLength = new SimpleStringProperty("");
        avgRowLength.addListener(weakListner);
        
        maxRows = new SimpleStringProperty("");
        maxRows.addListener(weakListner);
        
        minRows = new SimpleStringProperty("");
        minRows.addListener(weakListner);
        
        rowFormat = new SimpleObjectProperty<>();
        rowFormat.addListener(weakListner);
        
        checksum = new SimpleObjectProperty<>(0);
        checksum.addListener(weakListner);
        
        delayKeyWrite = new SimpleObjectProperty<>(0);
        delayKeyWrite.addListener(weakListner);
        
        columns = FXCollections.observableArrayList(_columns);
        columns.addListener(weakListner);
        
        indexes = FXCollections.observableArrayList(_indexes);
        indexes.addListener(weakListner);
        
        foreignKeys = FXCollections.observableArrayList(_foreignKeys);
        foreignKeys.addListener(weakListner);
        
        layerUuid = new SimpleStringProperty();
        layerUuid.addListener(weakListner);
        
        description = new SimpleStringProperty("");
        description.addListener(weakListner);
        
        x = new SimpleDoubleProperty();
        x.addListener(weakListner);
        
        y = new SimpleDoubleProperty();
        y.addListener(weakListner);
        
        width = new SimpleDoubleProperty();
        width.addListener(weakListner);
        
        height = new SimpleDoubleProperty();
        height.addListener(weakListner);
    }
    
    public DesignerTable(){
        this("", "", "Schema Default", "Innodb", "", new ArrayList<>(), new ArrayList<>(), new ArrayList<>());       
    }

    @Override
    public void invalidated(Observable observable) {
        if (parent != null) {
            parent.invalidated(observable);
        }
    }
    
    
    @Override
    public void setParent(DesignerErrDiagram parent) {
        this.parent = parent;
    }
    
    @Override
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    @Override
    public SimpleStringProperty name() {
        return name;
    }
    
    public SimpleStringProperty schema() {
        return schema;
    }
    
    public SimpleStringProperty collation() {
        return collation;
    }
    
    public SimpleStringProperty engine() {
        return engine;
    }
    
    public SimpleStringProperty comments() {
        return comments;
    }
    
    public SimpleStringProperty autoInc() {
        return autoInc;
    }
    
    public SimpleStringProperty avgRowLength() {
        return avgRowLength;
    }
    
    public SimpleStringProperty maxRows() {
        return maxRows;
    }
    
    public SimpleStringProperty minRows() {
        return minRows;
    }
    
    public SimpleObjectProperty<RowFormat> rowFormat() {
        return rowFormat;
    }
    
    public SimpleObjectProperty<Integer> checksum() {
        return checksum;
    }
    
    public SimpleObjectProperty<Integer> delayKeyWrite() {
        return delayKeyWrite;
    }
    
    public SimpleBooleanProperty expanded() {
        return expanded;
    }
    
    @Override
    public SimpleDoubleProperty x() {
        return x;
    }
    
    @Override
    public SimpleDoubleProperty y() {
        return y;
    }
    
    @Override
    public SimpleDoubleProperty width() {
        return width;
    }
    
    @Override
    public SimpleDoubleProperty height() {
        return height;
    }
    
    public SimpleStringProperty color() {
        return color;
    }
    
    public SimpleStringProperty layerUuid() {
        return layerUuid;
    }
    
    public ObservableList<DesignerColumn> columns() {
        return columns;
    }
    
    public ObservableList<DesignerIndex> indexes() {
        return indexes;
    }
    
    public ObservableList<DesignerForeignKey> foreignKeys() {
        return foreignKeys;
    }

    public SimpleBooleanProperty foreignKeysChanged() {
        return foreignKeysChanged;
    }
    
    public void notifyForeignKeysChanged() {
        foreignKeysChanged.set(!foreignKeysChanged.get());
    }

    public void setRelationCount(int relationCount) {
        this.relationCount = relationCount;
    }
    public int getRelationCount() {
        return relationCount;
    }
    
    @Override
    public SimpleStringProperty description() {
        return description;
    }
    
    
    
    @Override
    public String toString() {
        return name.get();
    }
    
    public String getStringRepresentation() {
        return "`" + schema.get() + "`.`" + name.get() + "`";
    }

    @Override
    public DesignerData copyData() {
        
        // without foreignkeys
        
        DesignerTable t = new DesignerTable();
        t.autoInc.set(autoInc.get());
        t.avgRowLength.set(avgRowLength.get());
        t.checksum.set(checksum.get());
        t.collation.set(collation.get());
        t.color.set(color.get());
        
        for (DesignerColumn dc: columns) {
            DesignerColumn dc0 = new DesignerColumn();
            dc0.fromMap(dc.toMap());
            
            t.columns.add(dc0);
        }
        
        t.comments.set(comments.get());
        t.delayKeyWrite.set(delayKeyWrite.get());
        t.engine.set(engine.get());
        t.height.set(height.get());
        
        for (DesignerIndex di: indexes) {
            DesignerIndex di0 = new DesignerIndex();
            di0.fromMap(di.toMap(), t);
            
            t.indexes.add(di0);
        }
        
        t.layerUuid.set(layerUuid.get());
        t.name.set(name.get());
        t.maxRows.set(maxRows.get());
        t.minRows.set(minRows.get());
        t.width.set(width.get());
        t.rowFormat.set(rowFormat.get());
        t.schema.set(schema.get());
        t.x.set(x.get());
        t.y.set(y.get());
        
        t.description.set(description.get());
        
        t.expanded.set(expanded.get());
        
        t.setParent(parent);
        
        return t;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        map.put("schema", schema.get());
        map.put("colation", collation.get());
        map.put("engine", engine.get());
        map.put("comments", comments.get());
        
        map.put("color", color.get());
        
        map.put("autoInc", autoInc.get());
        map.put("avgRowLength", avgRowLength.get());
        map.put("maxRows", maxRows.get());
        map.put("minRows", minRows.get());
        map.put("rowFormat", rowFormat.get() != null ? rowFormat.get().name() : null);
        map.put("checksum", checksum.get());
        map.put("delayKeyWrite", delayKeyWrite.get());
        
        map.put("x", x.get());
        map.put("y", y.get());
        map.put("width", width.get());
        map.put("height", height.get());
        
        map.put("layerUuid", layerUuid.get());
        map.put("description", description.get());
        
        map.put("expanded", expanded.get());
        
        List<Map<String, Object>> listColumns = new ArrayList<>();
        map.put("columns", listColumns);
        
        for (DesignerColumn c: columns) {
            listColumns.add(c.toMap());
        }
        
        List<Map<String, Object>> listIndexes = new ArrayList<>();
        map.put("indexes", listIndexes);
        
        for (DesignerIndex i: indexes) {
            listIndexes.add(i.toMap());
        }
        
        List<Map<String, Object>> listForeignKeys = new ArrayList<>();
        map.put("foreignKeys", listForeignKeys);
        
        for (DesignerForeignKey fk: foreignKeys) {
            listForeignKeys.add(fk.toMap());
        }
        
        return map;
    }

    public void fromMapWithoutIndexesAndFks(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set(getNotNullStringValue(map, "name"));
            schema.set(getNotNullStringValue(map, "schema"));
            
            collation.set(getNotNullStringValue(map, "colation"));
            engine.set(getNotNullStringValue(map, "engine"));
            comments.set(getNotNullStringValue(map, "comments"));
            description.set(getNotNullStringValue(map, "description"));
            
            color.set(getNotNullStringValue(map, "color"));
            
            autoInc.set(getNotNullStringValue(map, "autoInc"));
            avgRowLength.set(getNotNullStringValue(map, "avgRowLength"));
            maxRows.set(getNotNullStringValue(map, "maxRows"));
            minRows.set(getNotNullStringValue(map, "minRows"));
            rowFormat.set(map.get("rowFormat") != null ? RowFormat.valueOf((String) map.get("rowFormat")) : null);
            try {
                checksum.set(map.get("checksum") != null ? Double.valueOf(map.get("checksum").toString()).intValue() : null);
            } catch (NumberFormatException ex) {}
            
            try {
                delayKeyWrite.set(map.get("delayKeyWrite") != null ? Double.valueOf(map.get("delayKeyWrite").toString()).intValue() : null);
            } catch (NumberFormatException ex) {}
            
            Double x0 = (Double)map.get("x");
            x.set(x0 != null ? x0 : -1);
            
            Double y0 = (Double)map.get("y");
            y.set(y0 != null ? y0 : -1);
            
            Double width0 = (Double)map.get("width");
            width.set(width0 != null ? width0 : -1);
            
            Double height0 = (Double)map.get("height");
            height.set(height0 != null ? height0 : -1);
            
            layerUuid.set(getNotNullStringValue(map, "layerUuid"));
            expanded.set(map.get("expanded") != null ? (Boolean) map.get("expanded") : true);
            
            columns.clear();
            
            List<Map<String, Object>> listColumns = (List<Map<String, Object>>) map.get("columns");
            if (listColumns != null) {
                for (Map<String, Object> c: listColumns) {
                    DesignerColumn column = new DesignerColumn();
                    column.fromMap(c);
                    
                    columns.add(column);
                }
            }
        }
    }
    
    
    public void fromMapIndexesAndFks(Map<String, Object> map, Map<String, DesignerTable> tablesByUuid) {
        
        foreignKeys.clear();
        List<Map<String, Object>> listForeignKeys = (List<Map<String, Object>>) map.get("foreignKeys");
        if (listForeignKeys != null) {
            for (Map<String, Object> c: listForeignKeys) {
                DesignerForeignKey fk = new DesignerForeignKey();
                fk.fromMap(c, this, tablesByUuid);

                foreignKeys.add(fk);
            }
        }
        
        indexes.clear();
        List<Map<String, Object>> listIndexes = (List<Map<String, Object>>) map.get("indexes");
        if (listIndexes != null) {
            for (Map<String, Object> c: listIndexes) {
                DesignerIndex index = new DesignerIndex();
                index.fromMap(c, this);

                indexes.add(index);
            }
        }
    }
    
    
    
    public DesignerColumn findColumnByUuid(String uuid) {
        if (uuid != null) {
            for (DesignerColumn c: columns) {
                if (uuid.equals(c.uuid().get())) {
                    return c;
                }
            }
        }
        
        return null;
    }
    
    public DesignerColumn findColumnByName(String name) {
        if (uuid != null) {
            for (DesignerColumn c: columns) {
                if (name.equals(c.name().get())) {
                    return c;
                }
            }
        }
        
        return null;
    }
    
    
    public DesignerForeignKey findForeignKeyByUuid(String uuid) {
        if (uuid != null) {
            for (DesignerForeignKey fk: foreignKeys) {
                if (uuid.equals(fk.uuid().get())) {
                    return fk;
                }
            }
        }
        
        return null;
    }

    

    @Override
    public List<DesignerProperty> getProperties() {
        
        List<DesignerProperty> list = new ArrayList<>();
        list.add(new DesignerProperty("Name", name, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Color", color, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Expanded", expanded, DesignerProperty.PropertyType.BOOLEAN, v -> Boolean.valueOf(v)));
        list.add(new DesignerProperty("X", x, DesignerProperty.PropertyType.DOUBLE, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Y", y, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Height", height, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Width", width, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        
        return list;
    }
    
    private String getNotNullStringValue(Map<String, Object> map, String key) {
        Object v = map.get(key);
        return v != null ? v.toString() : "";
    }
}
