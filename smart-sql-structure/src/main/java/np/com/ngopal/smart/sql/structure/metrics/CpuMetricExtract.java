/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class CpuMetricExtract extends MetricExtract {
    
    private int cpuRowPositionInResponse = 2;
    private final String CLOCK_SPEED = "clockspeed";

    public CpuMetricExtract(MetricTypes type, String allResponseToParse) {
        super(type, allResponseToParse);
    }    

    public CpuMetricExtract(MetricTypes type) {
        super(type);
    }

    @Override
    public void extract() {
        super.extract();
        String[] lines = response.trim().split(System.lineSeparator());
        int index = -1;
        
        index = lines[cpuRowPositionInResponse].indexOf(':');//index = lines[nTimes-1].indexOf(':');
        String[] cpuRow = lines[cpuRowPositionInResponse].substring(index + 1).trim().split(",");//lines = lines[nTimes-1].substring(index+1).trim().split(",");   
        data.put(CLOCK_SPEED, String.valueOf(getClockRateByFactoring(cpuRow)));
        
        index = lines[1].indexOf(':');
        String[] taskRow = lines[1].substring(index + 1).trim().split(",");
        for(String s : taskRow){
            String[] a = s.trim().split(" ");
            data.put(a[1], a[0]);
        }
    }
    
    private double getClockRateByAddition(String[] lines){
        double clockRate = 0.0;
        for (int k = 0; k < lines.length; k++) {
            if (k == 3) {
                continue;
            }
            Double d = Double.valueOf(lines[k].trim().split(" ")[0]);
            clockRate += d;
        }
        return clockRate;
    }
    
    private double getClockRateByFactoring(String[] lines){    
        String idleVal = lines[3].trim().split(" ")[0];
        //System.out.println("IDLE VALUE : ( "+ idleVal + ")");
        return 100.0 - Double.valueOf(idleVal);
    }
    
    public String getTotalUsedRam(){
        return getRemoteVal(CLOCK_SPEED);
    }
    
    public String getTotalTaskAvailable(){
        return getRemoteVal("total");
    }
    
    public String getActiveRunningTask(){
        return getRemoteVal("running");
    }
    
}
