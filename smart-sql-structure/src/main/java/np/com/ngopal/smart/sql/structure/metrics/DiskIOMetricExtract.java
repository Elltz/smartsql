/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class DiskIOMetricExtract extends MetricExtract {

    public DiskIOMetricExtract(MetricTypes type, String allResponseToParse) {
        super(type, allResponseToParse);
    }

    public DiskIOMetricExtract(MetricTypes type) {
        super(type);
    }

    
    @Override
    public void extract() {
        super.extract();
        String[] lines = response.trim().split(System.lineSeparator());
        String[] keys = lines[0].split("\\s+");
        String[] vals = lines[1].split("\\s+");

        for(int i =0; i< vals.length; i++){
            data.put(keys[i], vals[i]);
        }
    }
    
    public String getAvailableSize(){
        return getRemoteVal("Available");
    }

    public String getUsagePercent(){
        return getRemoteVal("Use%");
    }

    public String getUsedSize(){
        return getRemoteVal("Used");
    }

    public String getTotalSize(){
        return getRemoteVal("1K-blocks");
    }

    public String getDriveName(){
        return getRemoteVal("Filesystem");
    }

}
