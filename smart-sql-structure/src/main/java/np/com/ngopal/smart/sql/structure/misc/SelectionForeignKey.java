package np.com.ngopal.smart.sql.structure.misc;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javax.persistence.Transient;
import lombok.*;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ForeignKey;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class SelectionForeignKey  extends ForeignKey {
    
    @Transient
    private SimpleBooleanProperty selectedProperty; 
    
    @Transient
    private SimpleStringProperty referencedTableProperty;
    
    @Transient
    private SimpleStringProperty referenceColumnsProperty;
        
    
    public SimpleBooleanProperty selected() {
        if (selectedProperty == null) {
            selectedProperty = new SimpleBooleanProperty(false);
        }
        
        return selectedProperty;
    }
    
    public SimpleStringProperty referencedTable() {
        if (referencedTableProperty == null) {
            referencedTableProperty = new SimpleStringProperty(getReferenceTable());
            referencedTableProperty.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                setReferenceTable(newValue);
                referenceColumns().set("");
                referenceColumns().set(null);
            });
        }
        
        return referencedTableProperty;
    }
    
    public SimpleStringProperty referenceColumns() {
        if (referenceColumnsProperty == null) {
            referenceColumnsProperty = new SimpleStringProperty(calcReferenceColumns());
        }
        
        return referenceColumnsProperty;
    }
     
    @Override
    public void setReferenceColumns(List<Column> referenceColumns) {
        super.setReferenceColumns(referenceColumns);
        
        referenceColumns().set(calcReferenceColumns());
    }
     
    private String calcReferenceColumns() {
        List<Column> columns = getReferenceColumns();
        String s = "";
        if (columns != null) {
            for (int c = 0; c < columns.size(); c++) {
                s += columns.get(c).getName();
                if (c != columns.size() - 1) {
                    s += ", ";
                }
            }
        }
        
        return s;
    }  
}