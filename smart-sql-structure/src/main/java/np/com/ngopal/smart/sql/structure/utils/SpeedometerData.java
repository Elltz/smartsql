/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class SpeedometerData {
    private final SimpleStringProperty name;
        private final SimpleObjectProperty<Double> value;
        private final SimpleStringProperty valuePct;
        private final SimpleObjectProperty<Double> lastInc;
        private final SimpleStringProperty lastIncPct;
        
        public SpeedometerData(String name, Double value, Double lastInc) {
            this.name = new SimpleStringProperty(name);
            this.value = new SimpleObjectProperty<>(value);
            this.valuePct = new SimpleStringProperty("0.00%");
            this.lastInc = new SimpleObjectProperty<>(0.0);
            this.lastIncPct = new SimpleStringProperty("0.00%");
        }
        
        public SimpleStringProperty name() {
            return name;
        }
        
        public SimpleObjectProperty value() {
            return value;
        }
        
        public SimpleStringProperty valuePct() {
            return valuePct;
        }
        
        public SimpleObjectProperty lastInc() {
            return lastInc;
        }
        
        public SimpleStringProperty lastIncPct() {
            return lastIncPct;
        }
}
