/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import java.util.Date;
import javafx.application.Platform;
import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class SwapMemoryMetric extends Metric {
    
    public SwapMemoryMetric(){
        super("SWAP MEMORY METRIC", "");
    }
    
    @Override
    public StringConverter<Number> yConverter() {
        return new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                return String.valueOf(object.doubleValue()) + " MB";
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string.split(" ")[0]);
            }
        };
    }

    @Override
    public long getMaxYValue() {
        return getSwapMemoryMetricExtract().getTotalMemory() / 1024;
    }

    @Override
    public int getUnitTimeValue() {
        return super.getUnitTimeValue(); 
    }

    @Override
    public XYChart.Series<Number, Number>[] getXYChartSeries() {
        XYChart.Series<Number, Number>[] series = new XYChart.Series[1];
        series[0] = getNewXYChart();
        series[0].setName("Used Memory");
        
//        series[1] = getNewXYChart();
//        series[1].setName("Free Memory");
        
        return series;
    }

    @Override
    public void updateUI(long timeInMillis, MetricExtract extract) {
        super.updateUI(timeInMillis, extract);
        if(extract != null){
            for(XYChart.Series<Number, Number> serie : data){
                 XYChart.Data<Number, Number> myData = new XYChart.Data();
                myData.setXValue(timeInMillis);
//                myData.setYValue(((serie.getName().charAt(0) == 'U') ? getSwapMemoryMetricExtract().getUsedMemory() :
//                        getSwapMemoryMetricExtract().getFreeMemory()) / 1024);
                myData.setYValue(getSwapMemoryMetricExtract().getUsedMemory() / 1024);
                serie.getData().add(myData);
            }
        }
    }
    
    private SwapMemoryMetricExtract getSwapMemoryMetricExtract(){
        return (SwapMemoryMetricExtract) metricExtract;
    }
    
    @Override
    public int getYTickUnitCount() {
        return 5;
    }

    @Override
    public String[] getChartColors() {
        return new String[]{"goldenrod"};
    }
    
}
