package np.com.ngopal.smart.sql.structure.controller;

import com.google.gson.Gson;
import com.sun.javafx.tk.Toolkit;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.queryoptimizer.QueryOptimizer;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.ExplainResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import np.com.ngopal.smart.sql.structure.components.CanvasPane;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.factories.QueryTableCell;
import np.com.ngopal.smart.sql.structure.misc.RunnedFrom;
import np.com.ngopal.smart.sql.structure.models.DebugExplainProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.calcBlobLength;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.showSlowQueryData;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.openQueryAnalyzer;
import np.com.ngopal.smart.sql.structure.utils.QODataProvider;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualFactory;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualGroupBy;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualJoin;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualOrderBy;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualTable;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualTable.TableType;
import np.com.ngopal.smart.sql.structure.visualexplain.VisualUnion;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class VisualExplainController extends BaseController implements Initializable {

    private static final String USING__FILE_SORT = "Using filesort";
    private static final String USING__TEMPORARY = "Using temporary";
    
    private static final String ORDER_BY = "ORDER";
    private static final String GROUP_BY = "GROUP";
    private static final String UNION = "UNION";
    
    private static final String ORDER_BY__FILESORT  = "filesort";
    private static final String GROUP_BY__TMP_TABLE = "tmp table";
    
    private TabPane mainTabPane;
    
    private boolean destoryed = false;
    
    private final EventHandler<KeyEvent> keyPressed = (KeyEvent event) -> {
        if (KeyCode.CONTROL == event.getCode()) {
            controlPushed = true;
        }
    };
    
    private final EventHandler<KeyEvent> keyReleased = (KeyEvent event) -> {
        if (KeyCode.CONTROL == event.getCode()) {
            controlPushed = false;
        }
    };
            
    @Getter
    private final SimpleDoubleProperty scale = new SimpleDoubleProperty(1);
    private final double maxScale = 5.0;
    private final double minScale = 0.1;
    
    @Setter(AccessLevel.PUBLIC)
    private PreferenceDataService preferenceService;
    
    @Setter(AccessLevel.PUBLIC)
    private QARecommendationService recommendationService;
    
    @Setter(AccessLevel.PUBLIC)
    private ColumnCardinalityService columnCardinalityService;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TableView<ExplainResult> gridUI;
    
    @FXML
    private Canvas visualUI;
    
    @FXML
    private CanvasPane parentContainer;
    
    @Getter
    private List<VisualTable> visualTables;
    
    private boolean controlPushed = false;
    
    private double movedX = 0;
    private double movedY = 0;
    
    private double fullWidth;
    private double fullHeight;
    
    private double systemWidth;
    private double systemHeight;
    
    private double constWidth;
    private double constHeight;
    
    private double rangeWidth;
    private double rangeHeight;
    
    private double fulltextWidth;
    private double fulltextHeight;
    
    private double refOrNullWidth;
    private double refOrNullHeight;
    
    private double indexMergeWidth;
    private double indexMergeHeight;
    
    private double uniqueSubqueryWidth;
    private double uniqueSubqueryHeight;
    
    private double indexSubqueryWidth;
    private double indexSubqueryHeight;
    
    private double unknownWidth;
    private double unknownHeight;
    
    private double indexWidth;
    private double indexHeight;

    private double queryWidth;
    private double queryHeight;

    private double nonUniqueWidth;
    private double nonUniqueHeight;

    private double uniqueWidth;
    private double uniqueHeight;
    
    @Getter
    private double joinWidth;
    @Getter
    private double joinHeight;
    
    @Getter
    private double unionHeight;
    
    private double orderByWidth;
    private double orderByHeight;
    
    private double groupByWidth;
    private double groupByHeight;
    
    
    private QORecommendation recommendation;
    
    private String query;
    
    private DBService service;
    
    public static final String JOIN_TEXT_0 = "nested";
    public static final String JOIN_TEXT_1 = "\n  loop";
    
    public static final int BASE_FONT_SIZE = 12;
    
    public static final int REC_SHIFT_X = 15;
    public static final int REC_SHIFT_Y = 10;
    
    public static final int COMPONENT_SHIFT_X = 30;
    public static final int COMPONENT_SHIFT_Y = 30;
    
    private static final int groupOrderShiftX = 26;
    private static final int groupOrderShiftY = 20;
    
    @Setter(AccessLevel.PUBLIC)
    private QAPerformanceTuningService aPerformanceTuningService;
    
    @Setter(AccessLevel.PUBLIC)
    private UserUsageStatisticService usageService;
    
    @FXML
    public void queryAnalyzer(ActionEvent event) {
        openQueryAnalyzer(query != null ? query : "", getController(), RunnedFrom.QA, aPerformanceTuningService, preferenceService, recommendationService, columnCardinalityService, usageService, mainTabPane);
    }
    
    @FXML
    public void visualExplain(ActionEvent event) {
        visualUI.setVisible(true);
        gridUI.setVisible(false);
    }
    
    @FXML
    public void gridExplain(ActionEvent event) {
        visualUI.setVisible(false);
        gridUI.setVisible(true);
    }
    
    @FXML
    public void zoomIn(ActionEvent event) {
        zoom(true);
    }
    
    @FXML
    public void zoomOut(ActionEvent event) {
        zoom(false);
    }
    
    public void init(DBService service, Database database, String query, TabPane tabPane) {
        init(service, database, query, false, tabPane, null);
    }
    
    public void init(DBService service, Database database, String query, boolean multimode, TabPane tabPane, DebugExplainProvider p) {
        
        this.mainTabPane = tabPane;
        this.query = query;
        this.service = service;
        
        if (multimode) {
            visualUI.setVisible(true);
            gridUI.setVisible(true);
        }
        
        GraphicsContext gc = visualUI.getGraphicsContext2D();
        gc.setFont(new Font("Arial", BASE_FONT_SIZE));
        
        double lineHegiht = Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();
        
        fullWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.FULL.toString(), gc.getFont()) + REC_SHIFT_X;
        fullHeight = lineHegiht + REC_SHIFT_Y;
        
        systemWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.SYSTEM.toString(), gc.getFont()) + REC_SHIFT_X;
        systemHeight = lineHegiht + REC_SHIFT_Y;
        
        constWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.CONST.toString(), gc.getFont()) + REC_SHIFT_X;
        constHeight = lineHegiht + REC_SHIFT_Y;
        
        rangeWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.RANGE.toString(), gc.getFont()) + REC_SHIFT_X;
        rangeHeight = lineHegiht + REC_SHIFT_Y;
        
        fulltextWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.FULLTEXT.toString(), gc.getFont()) + REC_SHIFT_X;
        fulltextHeight = lineHegiht + REC_SHIFT_Y;
        
        refOrNullWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.REF_OR_NULL.toString(), gc.getFont()) + REC_SHIFT_X;
        refOrNullHeight = lineHegiht + REC_SHIFT_Y;
        
        indexMergeWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.INDEX_MERGE.toString(), gc.getFont()) + REC_SHIFT_X;
        indexMergeHeight = lineHegiht + REC_SHIFT_Y;
        
        uniqueSubqueryWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.UNIQUE_SUBQUERY.toString(), gc.getFont()) + REC_SHIFT_X;
        uniqueSubqueryHeight = lineHegiht + REC_SHIFT_Y;
        
        indexSubqueryWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.INDEX_SUBQUERY.toString(), gc.getFont()) + REC_SHIFT_X;
        indexSubqueryHeight = lineHegiht + REC_SHIFT_Y;
        
        unknownWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.UNKNOWN.toString(), gc.getFont()) + REC_SHIFT_X;
        unknownHeight = lineHegiht + REC_SHIFT_Y;
        
        indexWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.INDEX.toString(), gc.getFont()) + REC_SHIFT_X;
        indexHeight = lineHegiht + REC_SHIFT_Y;

        queryWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.QUERY.toString() + "####", gc.getFont()) + REC_SHIFT_X;
        queryHeight = lineHegiht + REC_SHIFT_Y;

        nonUniqueWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.NON_UNIQUE.toString(), gc.getFont()) + REC_SHIFT_X;
        nonUniqueHeight = lineHegiht + REC_SHIFT_Y;

        uniqueWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(TableType.UNIQUE.toString(), gc.getFont()) + REC_SHIFT_X;
        uniqueHeight = lineHegiht + REC_SHIFT_Y;
        
        joinWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(JOIN_TEXT_0 + JOIN_TEXT_1, gc.getFont()) + REC_SHIFT_X;
        joinHeight = (lineHegiht + REC_SHIFT_Y) * 2;
        
        orderByWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(ORDER_BY, gc.getFont()) + groupOrderShiftX;
        orderByHeight = lineHegiht + groupOrderShiftY;
        
        groupByWidth = Toolkit.getToolkit().getFontLoader().computeStringWidth(GROUP_BY, gc.getFont()) + groupOrderShiftX;
        groupByHeight = lineHegiht + groupOrderShiftY;
        
        unionHeight = lineHegiht + REC_SHIFT_Y;        
        
        visualExplain(null);
                
        makeBusy(true);
        Thread th = new Thread(() -> {
            
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            try {
                recommendation = QueryOptimizer.analyzeQuery(
                    QODataProvider.create(
                        service, 
                        columnCardinalityService, 
                        recommendationService, 
                        aPerformanceTuningService,
                        session.getConnectionParam(),
                        true), 
                    database.getName(), 
                    query
                );

                if (p != null) {
                    p.setRowsSum(recommendation.getRowsSum());
                }
                
                try {
                    Gson gson = new Gson();
                    Files.write(StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory("analyzerResult.json").toPath(),
                            gson.toJson(recommendation.getAnalyzer().getAnalyzerResult()).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }

                Platform.runLater(() -> {
                    gridUI.setItems(FXCollections.observableArrayList(recommendation.getAnalyzer().getvQueryExplainResults()));

                    convertExplain(service, query, recommendation, gc);
                    
                    parentContainer.draw();
                });
                
                makeBusy(false);
                
            } catch (Throwable ex) {
                Platform.runLater(() -> {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                });
            }
            
        });
        th.setDaemon(true);
        th.start();
    }
    
    
    private void convertExplain(DBService service, String query, QORecommendation queryAnalyzer, GraphicsContext gc) {
        if (queryAnalyzer != null && queryAnalyzer.getAnalyzer().getAnalyzerResult() != null && !queryAnalyzer.getAnalyzer().getAnalyzerResult().isEmpty()) {
            
            if (queryAnalyzer.getAnalyzer().getMaxId() == 1) {
                if (queryAnalyzer.getAnalyzer().getAnalyzerResult() != null && !queryAnalyzer.getAnalyzer().getAnalyzerResult().isEmpty()) {
                    
                    AnalyzerResult ar = queryAnalyzer.getAnalyzer().getAnalyzerResult().get(0);
                    
                    List<ExplainResult> list = queryAnalyzer.getAnalyzer().getVNQueryExplainResults() != null && !queryAnalyzer.getAnalyzer().getVNQueryExplainResults().isEmpty() ? queryAnalyzer.getAnalyzer().getVNQueryExplainResults() : queryAnalyzer.getAnalyzer().getvQueryExplainResults();
                    if (list != null && !list.isEmpty()) {
                        visualTables = convertSimple(ar.getChildren() != null && !ar.getChildren().isEmpty() ? ar.getChildren().get(0) : null, list, gc, 100, 0, true);
                    }
                }
            } else {
                
                // V_NQuery                
                List<AnalyzerResult> results = new ArrayList<>();            
                
                for (AnalyzerResult ar: queryAnalyzer.getAnalyzer().getAnalyzerResult().get(0).getChildren()) {
                    if (!"GOOD".equals(ar.getQuery())) {                    
                        results.add(ar);
                    }
                }
                
                // TODO I think this is wrong
                // need fix this
                visualTables = convertComplex(results.get(0), results.size() == 1 && results.get(0).getChildren() != null ? results.get(0).getChildren() : results, gc);
            }
        }
    }

    
    private List<VisualTable> convertComplex(AnalyzerResult rootAnalyzerResult, List<AnalyzerResult> analyzerResults, GraphicsContext gc) {
        List<VisualTable> tables = new ArrayList<>();
        if (recommendation.getAnalyzer().getAnalyzerResult() != null && !recommendation.getAnalyzer().getAnalyzerResult().isEmpty()) {
                        
            double x = 0;
            double y = 0;
            double width = 0;
            double currentWidth = 0;
            
            double lastX = 0;
            double lastY = 0;
            
            Object prevComponent = null;            
            VisualJoin prevJoin = null;
            
            for (AnalyzerResult child: analyzerResults) {
            
                VisualJoin join = null;
                
                switch (child.getType()) {
                    // TODO If need add for all types
                    case "UNION":
                        width = 0;
                        currentWidth = 0;
                        List<VisualTable> unionTables = new ArrayList<>();

                        List<AnalyzerResult> list = child.getChildren() != null ? new ArrayList<>(child.getChildren()) : new ArrayList<>();
                        Collections.reverse(list);
                        for (AnalyzerResult ar: list) {
                            // find explain result for each analyzer result
                            String queryKey = ar.getVariable();
                            List<ExplainResult> result = recommendation.getAnalyzer().lookupExplainResults(queryKey);

                            // if simple - convert using simple method
                            if (ar.getDegree() == 1 && result != null && !result.isEmpty()) {

                                List<VisualTable> simpleTables = convertSimple(ar, result, gc, x + width, unionHeight + COMPONENT_SHIFT_Y, true);
                                for (VisualTable vt: simpleTables) {
                                    if (vt.type != VisualTable.TableType.QUERY) {
                                        currentWidth += vt.width;
                                        width += vt.width + COMPONENT_SHIFT_X;
                                    }
                                }

                                unionTables.addAll(simpleTables);
                            }
                        }
                        
                        List<ExplainResult> result = recommendation.getAnalyzer().lookupExplainResults(child.getVariable());
                        ExplainResult er = result.get(0);
                        VisualUnion union = VisualFactory.createUnion(x, y, width, unionHeight, er.getTable(), er.getKey(), er.getType(), unionTables);
                        
                        if (prevComponent != null) {
                            join = VisualFactory.createJoin(x + width / 2 - joinWidth / 2, y - joinHeight, prevJoin != null ? prevJoin : prevComponent);
                            prevJoin = join;
                        }
                        union.prevComponent = join;
                        
                        lastX = x;
                        x += width + COMPONENT_SHIFT_X;
                        tables.add(union);
                        prevComponent = union;
                        break;
                        
                    default:
                        width = 0;
                        currentWidth = 0;
                        List<ExplainResult> resultList = recommendation.getAnalyzer().lookupExplainResults(child.getVariable());
                        if (resultList != null && !resultList.isEmpty()) {
                            List<VisualTable> simpleTables = convertSimple(child, resultList, gc, x, y, false);
                            if (simpleTables != null) {

                                VisualTable lastTable = null;
                                for (VisualTable vt: simpleTables) {
                                    width += vt.width + COMPONENT_SHIFT_X;
                                    currentWidth += vt.width;
                                    lastTable = vt;
                                }

                                if (prevComponent != null) {
                                    join = VisualFactory.createJoin(x + currentWidth / 2 - joinWidth / 2, y - joinHeight, prevJoin != null ? prevJoin : prevComponent);
                                    prevJoin = join;
                                }
                                lastTable.prevComponent = join;

                                lastX = x;
                                x += width;
                                tables.addAll(simpleTables);
                                prevComponent = lastTable;
                            }
                        }
                }
                
                lastY = join != null ? join.y - joinHeight / 2 : y;                
            }

            lastX += currentWidth / 2;
            y = lastY - COMPONENT_SHIFT_Y - unionHeight;
            
            VisualGroupBy groupBy = null;
            VisualOrderBy orderBy = null;

            // get first explain result for checking colors of blocks
            List<ExplainResult> result = recommendation.getAnalyzer().lookupExplainResults(analyzerResults.get(0).getVariable());
            ExplainResult er = result != null && !result.isEmpty() ? result.get(0) : null;

            if (rootAnalyzerResult != null) {

                boolean groupByExist = rootAnalyzerResult.getGroupBy() != null && !rootAnalyzerResult.getGroupBy().isEmpty();
                boolean orderByExist = rootAnalyzerResult.getOrderBy() != null && !rootAnalyzerResult.getOrderBy().isEmpty();

                if (groupByExist || orderByExist) {

                    if (groupByExist) {

                        lastX -= groupByWidth / 2;

                        boolean hasTemporary = er != null ? er.getExtra().toUpperCase().contains(USING__TEMPORARY.toUpperCase()) : false;
                        groupBy = VisualFactory.createGroupBy(
                            lastX, y, groupByWidth, groupByHeight, 
                            GROUP_BY, hasTemporary ? GROUP_BY__TMP_TABLE : null,
                            hasTemporary ? Color.RED : Color.GREEN
                        );
                    }

                    // if exists both - than need draw them one by one
                    if (groupByExist && orderByExist) {
                        lastX += groupByWidth + COMPONENT_SHIFT_X;
                    }

                    if (orderByExist) {

                        if (!groupByExist) {
                            lastX -= groupByWidth / 2;
                        }

                        boolean hasFileSort = er.getExtra().toUpperCase().contains(USING__FILE_SORT.toUpperCase());
                        orderBy = VisualFactory.createOrderBy(
                            lastX, y, orderByWidth, orderByHeight, 
                            ORDER_BY, hasFileSort ? ORDER_BY__FILESORT : null,
                            hasFileSort ? Color.RED : Color.GREEN
                        );
                    }

                    lastX += (orderByExist ? orderByWidth / 2 : groupByWidth / 2);                            
                    y -= (orderByExist ? orderByHeight : groupByHeight) + COMPONENT_SHIFT_Y;
                }
            }
            
            tables.add(VisualFactory.createTable(lastX - queryWidth / 2, y, queryWidth, queryHeight, " #1", null, TableType.QUERY, er != null ? er.getExtra() : null, groupBy, orderBy, null, null, prevJoin != null ? prevJoin : prevComponent));
        }
        
        return tables;
    }
    
    
    

    private List<VisualTable> convertSimple(AnalyzerResult ar, List<ExplainResult> explainResults, GraphicsContext gc, double shiftX, double shiftY, boolean withQueryTable) {
        List<VisualTable> tables = new ArrayList<>();
        
        VisualTable prevTable = null;
        VisualJoin prevJoin = null;

        double lastXShift = 0;
        double lastY = 0;

        double x = shiftX;
        double y = shiftY;
        
        Long id = 1l;
        
        for (ExplainResult er: explainResults) {
            
            id = er.getId();
            VisualJoin join = null;
            
            double currentWidth = 0;
            double currentHeight = 0;
            TableType currentTableType = null;
            
            double currentX = x;
            
            if (er.getType() != null) {
                switch(er.getType().toUpperCase()) {
                    case "SYSTEM":
                        currentWidth = systemWidth;
                        currentHeight = systemHeight;
                        currentTableType = TableType.SYSTEM;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "CONST":
                        currentWidth = constWidth;
                        currentHeight = constHeight;
                        currentTableType = TableType.CONST;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "FULLTEXT":
                        currentWidth = fulltextWidth;
                        currentHeight = fulltextHeight;
                        currentTableType = TableType.FULLTEXT;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "REF_OR_NULL":
                        currentWidth = refOrNullWidth;
                        currentHeight = refOrNullHeight;
                        currentTableType = TableType.REF_OR_NULL;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "INDEX_MERGE":
                        currentWidth = indexMergeWidth;
                        currentHeight = indexMergeHeight;
                        currentTableType = TableType.INDEX_MERGE;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "UNIQUE_SUBQUERY":
                        currentWidth = uniqueSubqueryWidth;
                        currentHeight = uniqueSubqueryHeight;
                        currentTableType = TableType.UNIQUE_SUBQUERY;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "INDEX_SUBQUERY":
                        currentWidth = indexSubqueryWidth;
                        currentHeight = indexSubqueryHeight;
                        currentTableType = TableType.INDEX_SUBQUERY;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "REF":
                        currentWidth = nonUniqueWidth;
                        currentHeight = nonUniqueHeight;
                        currentTableType = TableType.NON_UNIQUE;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;

                    case "EQ_REF":
                        currentWidth = uniqueWidth;
                        currentHeight = uniqueHeight;
                        currentTableType = TableType.UNIQUE;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;

                    case "INDEX":
                        currentWidth = indexWidth;
                        currentHeight = indexHeight;
                        currentTableType = TableType.INDEX;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    case "RANGE":
                        currentWidth = rangeWidth;
                        currentHeight = rangeHeight;
                        currentTableType = TableType.RANGE;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;

                    case "ALL":                 
                        currentWidth = fullWidth;
                        currentHeight = fullHeight;
                        currentTableType = TableType.FULL;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                        
                    default:                 
                        currentWidth = unknownWidth;
                        currentHeight = unknownHeight;
                        currentTableType = TableType.UNKNOWN;
                        x += currentWidth + COMPONENT_SHIFT_X;
                        lastXShift = currentWidth;
                        break;
                }
            
            
                if (prevTable != null) {
                    join = VisualFactory.createJoin(currentX + currentWidth / 2 - joinWidth / 2, y - joinHeight, prevJoin != null ? prevJoin : prevTable);
                    prevJoin = join;
                }

                prevTable = VisualFactory.createTable(currentX, y, currentWidth, currentHeight, er.getTable(), er.getKey(), currentTableType, null, null, null, null, er.getRows(), join);


                // TODO check may be table has inside queries
                if (ar.getChildren() != null && !ar.getChildren().isEmpty()) {

                    double newX = prevTable.x;
                    double width = 0;
                    for (AnalyzerResult arTemp: ar.getChildren()) {                    

                        String queryKey = arTemp.getVariable();
                        List<ExplainResult> result = recommendation.getAnalyzer().lookupExplainResults(queryKey);
                        if (result != null && !result.isEmpty()) {

                            List<VisualTable> simpleTables = convertSimple(arTemp, result, gc, newX, y + currentHeight * 2, false);
                            if (prevTable.insideTables != null) {
                                prevTable.insideTables.addAll(simpleTables);
                            } else {
                                prevTable.insideTables = simpleTables;
                            }

                            for (VisualTable vt: simpleTables) {
                                width += vt.width + COMPONENT_SHIFT_X;
                                // just hide them
                                vt.rowsNumber = null;
                            }

                            newX += width + COMPONENT_SHIFT_X;
                        }                    
                    }

                    prevTable.width = width - COMPONENT_SHIFT_X + COMPONENT_SHIFT_X / 2;
                }

                tables.add(prevTable);

                lastY = join != null ? join.y - joinHeight / 2 : y;
            }
        }

        // change X for last block
        //if (x > 0) {
            x -= COMPONENT_SHIFT_X + lastXShift / 2;
        //}
        
        //if (lastY > 0) {
            y = lastY - queryHeight - COMPONENT_SHIFT_Y;
        //}

        VisualGroupBy groupBy = null;
        VisualOrderBy orderBy = null;

        // get first explain result for checking colors of blocks
        ExplainResult er = explainResults.get(0);
        
        if (ar != null) {
                    
            boolean groupByExist = ar.getGroupBy() != null && !ar.getGroupBy().isEmpty();
            boolean orderByExist = ar.getOrderBy() != null && !ar.getOrderBy().isEmpty();

            if (groupByExist || orderByExist) {

                if (groupByExist) {

                    x -= groupByWidth / 2;

                    boolean hasTemporary = er.getExtra().toUpperCase().contains(USING__TEMPORARY.toUpperCase());                                
                    groupBy = VisualFactory.createGroupBy(
                        x, y, groupByWidth, groupByHeight, 
                        GROUP_BY, hasTemporary ? GROUP_BY__TMP_TABLE : null,
                        hasTemporary ? Color.RED : Color.GREEN
                    );
                }

                // if exists both - than need draw them one by one
                if (groupByExist && orderByExist) {
                    x += groupByWidth + COMPONENT_SHIFT_X;
                }

                if (orderByExist) {

                    if (!groupByExist) {
                        x -= orderByWidth / 2;
                    }

                    boolean hasFileSort = er.getExtra().toUpperCase().contains(USING__FILE_SORT.toUpperCase());
                    orderBy = VisualFactory.createOrderBy(
                        x, y, orderByWidth, orderByHeight, 
                        ORDER_BY, hasFileSort ? ORDER_BY__FILESORT : null,
                        hasFileSort ? Color.RED : Color.GREEN
                    );
                }

                x += (orderByExist ? orderByWidth / 2 : groupByWidth / 2);                            
                y -= (orderByExist ? orderByHeight : groupByHeight) + COMPONENT_SHIFT_Y;
            }
        }
        
        x -= queryWidth / 2;

        if (withQueryTable) {
            tables.add(VisualFactory.createTable(x,  y, queryWidth, queryHeight, " #" + id, null, TableType.QUERY, er != null ? er.getExtra() : null, groupBy, orderBy, null, null, prevJoin != null ? prevJoin : prevTable));
        }
        
        return tables;
    }
    
    
    @Override
    public AnchorPane getUI() {        
        return mainUI;
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
                
        ContextMenu menu = new ContextMenu();
        MenuItem resetItem = new MenuItem("Reset", new ImageView(StandardDefaultImageProvider.getInstance().getRefresh_image()));
        resetItem.setOnAction((ActionEvent event) -> {
            parentContainer.setCanvasTranslateX(0);
            parentContainer.setCanvasTranslateY(0);
            
            scale.set(1);
            parentContainer.draw();
        });
        menu.getItems().add(resetItem);
        
        parentContainer.setParentController(this);
        
        visualUI.setOnMouseClicked((MouseEvent event) -> {
            if (event.isPopupTrigger()) {
                menu.show(getController().getStage(), event.getScreenX(), event.getScreenY());
            }
        });        
        
        mainUI.sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
            if (newValue != null) {
                newValue.addEventFilter(KeyEvent.KEY_PRESSED, keyPressed);        
                newValue.addEventFilter(KeyEvent.KEY_RELEASED, keyReleased);
            }
        });
        
        
        visualUI.setOnMouseDragged((MouseEvent event) -> {
            parentContainer.setCanvasTranslateX(parentContainer.getCanvasTranslateX() + event.getX() - movedX);
            parentContainer.setCanvasTranslateY(parentContainer.getCanvasTranslateY() + event.getY() - movedY);
            movedX = event.getX();
            movedY = event.getY();
            
            parentContainer.draw();
        });
        
        visualUI.setOnMousePressed((MouseEvent event) -> {
            movedX = event.getX();
            movedY = event.getY();
        });
        
        
        visualUI.setOnScroll((ScrollEvent event) -> {
            if (controlPushed) {                
                zoom(event.getDeltaY() > 0);
            }
        });
     
        
        TableColumn<ExplainResult, Long> idColum = new TableColumn<>("id");
        idColum.setCellValueFactory(new PropertyValueFactory<>("id"));
        idColum.setStyle( "-fx-alignment: center-right;");
        idColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> selectTypeColum = new TableColumn<>("select_type");
        selectTypeColum.setCellValueFactory(new PropertyValueFactory<>("selectType"));
        selectTypeColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> tableColum = new TableColumn<>("table");
        tableColum.setCellValueFactory(new PropertyValueFactory<>("table"));
        tableColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> typeColum = new TableColumn<>("type");
        typeColum.setCellValueFactory(new PropertyValueFactory<>("type"));
        typeColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> posibleKyesColum = new TableColumn<>("posible_kyes");
        posibleKyesColum.setCellValueFactory(new PropertyValueFactory<>("posibleKyes"));
        posibleKyesColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> keyColum = new TableColumn<>("key");
        keyColum.setCellValueFactory(new PropertyValueFactory<>("key"));
        keyColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, Long> keyLenColum = new TableColumn<>("key_len");
        keyLenColum.setCellValueFactory(new PropertyValueFactory<>("keyLen"));
        keyLenColum.setStyle( "-fx-alignment: center-right;");
        keyLenColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> refColum = new TableColumn<>("ref");
        refColum.setCellValueFactory(new PropertyValueFactory<>("ref"));
        refColum.setCellFactory(new Callback<TableColumn<ExplainResult, String>, TableCell<ExplainResult, String>>() {
            @Override
            public TableCell<ExplainResult, String> call(TableColumn<ExplainResult, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(getStage(), getController(), data, text, "Ref data", preferenceService));
            }
        });
        refColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, Long> rowsColum = new TableColumn<>("rows");
        rowsColum.setCellValueFactory(new PropertyValueFactory<>("rows"));
        rowsColum.setStyle( "-fx-alignment: center-right;");
        rowsColum.setPrefWidth(100);
        
        TableColumn<ExplainResult, String> extraColum = new TableColumn<>("extra");
        extraColum.setCellValueFactory(new PropertyValueFactory<>("extra"));
        extraColum.setPrefWidth(100);      
        
        gridUI.getColumns().addAll(idColum, selectTypeColum, tableColum, typeColum, posibleKyesColum, keyColum, keyLenColum, refColum, rowsColum, extraColum);
    }
    
    private void zoom(boolean in) {
        if (visualUI.isVisible()) {
            double newScale = scale.get();
            if (in) {
                newScale += 0.2;
                if (newScale > maxScale) {
                    newScale = maxScale;
                }
            } else {
                newScale -= 0.2;
                if (newScale < minScale) {
                    newScale = minScale;
                }
            }

            scale.set(newScale);
            parentContainer.draw();
        }
    }
    
    public boolean isDestroyed() {
        return destoryed;
    }
    
    public void destroy() {
        destoryed = true;
        
        if (mainUI != null && mainUI.getScene() != null) {
            mainUI.getScene().removeEventFilter(KeyEvent.KEY_PRESSED, keyPressed);        
            mainUI.getScene().removeEventFilter(KeyEvent.KEY_RELEASED, keyReleased);
        }
    }
        
}
