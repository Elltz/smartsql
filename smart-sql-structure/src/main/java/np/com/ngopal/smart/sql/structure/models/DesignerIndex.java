package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerIndex {

    public enum IndexType {
        INDEX,
        UNIQUE,
        FULLTEXT,
        PRIMARY;
        
        public static IndexType fromString(String value) {
            switch (value.toUpperCase()) {
                case "INDEX":
                    return INDEX;
                    
                case "UNIQUE":
                case "UNIQUE_TEXT":
                    return UNIQUE;
                    
                case "FULLTEXT":
                case "FULL_TEXT":
                    return FULLTEXT;
                    
                case "PRIMARY":
                    return PRIMARY;
                    
                default:
                    return null;
            }
        }
    }
    
    public enum IndexOrder {
        ASC,
        DESC
    }
    
    public enum IndexStorageType {
        BTREE
    }
    private SimpleStringProperty uuid;
    
    private SimpleStringProperty name;
    private SimpleObjectProperty<IndexType> type;
        
    private ObservableList<DesignerColumn> columns;

    private ObservableMap<DesignerColumn, Integer> orderMap;
    private ObservableMap<DesignerColumn, IndexOrder> sortMap;
    private ObservableMap<DesignerColumn, Long> lengthMap;
    
    private SimpleObjectProperty<IndexStorageType> storageType;
    private SimpleStringProperty keyBlockSize;
    private SimpleStringProperty parser;
    private SimpleStringProperty comment;
    
    private SimpleObjectProperty<DesignerForeignKey> foreignKey;
    
    public DesignerIndex() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty();
        type = new SimpleObjectProperty<>(IndexType.INDEX);

        columns = FXCollections.observableArrayList();

        orderMap = FXCollections.observableHashMap();
        sortMap = FXCollections.observableHashMap();
        lengthMap = FXCollections.observableHashMap();

        storageType = new SimpleObjectProperty<>();
        keyBlockSize = new SimpleStringProperty("0");
        parser = new SimpleStringProperty();
        comment = new SimpleStringProperty();

        foreignKey = new SimpleObjectProperty<>();
    }
    
    public DesignerIndex(String _name,IndexType intype,ArrayList<DesignerColumn> col,
            HashMap<DesignerColumn, Integer> ordermap,
            HashMap<DesignerColumn, IndexOrder> sortmap,
            HashMap<DesignerColumn, Long> lengthmap,
            IndexStorageType storagetype,String _keyBlockSize,String _parser,String _comment,
            DesignerForeignKey dfk) {
        name = new SimpleStringProperty(_name);
        type = new SimpleObjectProperty<>(intype);

        columns = FXCollections.observableArrayList(col);

        orderMap = FXCollections.observableHashMap();
        orderMap.putAll(ordermap);
        sortMap = FXCollections.observableHashMap();
        sortMap.putAll(sortmap);
        lengthMap = FXCollections.observableHashMap();
        lengthmap.putAll(lengthmap);
        storageType = new SimpleObjectProperty<>(storagetype);
        keyBlockSize = new SimpleStringProperty(_keyBlockSize);
        parser = new SimpleStringProperty(_parser);
        comment = new SimpleStringProperty(_comment);

        foreignKey = new SimpleObjectProperty<>(dfk);
    }
    
    
    public void removeColumn(DesignerColumn dc) {
        columns().remove(dc);
        
        sortMap().remove(dc);
        orderMap().remove(dc);
        lengthMap().remove(dc);
    }
    
    public void addColumn(DesignerColumn dc) {
        columns.add(dc);
        
        // find current index
        boolean found = false;
        int index = 1;
        while (!found) {
            found = true;
            for (Integer v: orderMap.values()) {
                if (v == index) {
                    found = false;
                    index++;
                    break;
                }
            }
        }
        
        orderMap.put(dc, index);
        sortMap.put(dc, IndexOrder.ASC);
    }
    public boolean isPrimaryKey() {
        return IndexType.PRIMARY.equals(type.get());
    }
    
    public boolean isUniqueKey() {
        return IndexType.UNIQUE.equals(type.get());
    }
    
    
    
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    public SimpleStringProperty name() {
        return name;
    }
    
    public SimpleObjectProperty<IndexType> type() {
        return type;
    }
    
    public ObservableList<DesignerColumn> columns() {
        return columns;
    }
    
    public ObservableMap<DesignerColumn, Integer> orderMap() {
        return orderMap;
    }
    
    public ObservableMap<DesignerColumn, IndexOrder> sortMap() {
        return sortMap;
    }
    
    public ObservableMap<DesignerColumn, Long> lengthMap() {
        return lengthMap;
    }
    
    public SimpleObjectProperty<IndexStorageType> storageType() {
        return storageType;
    }
    
    public SimpleStringProperty keyBlockSize() {
        return keyBlockSize;
    }
    
    public SimpleStringProperty parser() {
        return parser;
    }
    
    public SimpleStringProperty comment() {
        return comment;
    }
 
    public SimpleObjectProperty<DesignerForeignKey> foreignKey() {
        return foreignKey;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        
        if (type.get() != null) {
            map.put("type", type.get().name());
        }
        
        if (storageType.get() != null) {
            map.put("storageType", storageType.get().name());
        }
        
        map.put("keyBlockSize", keyBlockSize.get());
        map.put("parser", parser.get());
        map.put("comment", comment.get());
        
        
        List<String> listColumns = new ArrayList<>();
        map.put("columns", listColumns);
        
        for (DesignerColumn c: columns) {
            listColumns.add(c.uuid().get());
        }
        
        
        Map<String, Integer> orderMap0 = new HashMap<>();
        map.put("orderMap", orderMap0);
        
        for (DesignerColumn c: orderMap.keySet()) {
            orderMap0.put(c.uuid().get(), orderMap.get(c));
        }
        
        
        Map<String, String> sortMap0 = new HashMap<>();
        map.put("sortMap", sortMap0);
        
        for (DesignerColumn c: sortMap.keySet()) {
            if (sortMap.get(c) != null) {
                sortMap0.put(c.uuid().get(), sortMap.get(c).name());
            }
        }
        
        
        Map<String, Long> lengthMap0 = new HashMap<>();
        map.put("lengthMap", lengthMap0);
        
        for (DesignerColumn c: lengthMap.keySet()) {
            if (lengthMap.get(c) != null) {
                lengthMap0.put(c.uuid().get(), lengthMap.get(c));
            }
        }
        
        if (foreignKey.get() != null) {
            map.put("foreignKey", foreignKey.get().uuid().get());
        }
        
        return map;
    }
    
    
    
    public void fromMap(Map<String, Object> map, DesignerTable table) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set((String)map.get("name"));
            
            String typeStr = (String)map.get("type");
            if (typeStr != null) {
                type.set(IndexType.valueOf(typeStr));
            }
            
            String storageTypeStr = (String)map.get("storageType");
            if (storageTypeStr != null) {
                storageType.set(IndexStorageType.valueOf(storageTypeStr));
            }
            
            keyBlockSize.set((String)map.get("keyBlockSize"));
            parser.set((String)map.get("parser"));
            comment.set((String)map.get("comment"));
            
            List<String> listColumns = (List<String>) map.get("columns");
            if (listColumns != null) {
                for (String c: listColumns) {
                    DesignerColumn column = table.findColumnByUuid(c);
                    if (column != null) {
                        columns.add(column);
                    }
                }
            }
            
            Map<String, Object> orderMap0 = (Map<String, Object>) map.get("orderMap");
            if (orderMap0 != null) {
                for (String c: orderMap0.keySet()) {
                    DesignerColumn column = table.findColumnByUuid(c);
                    if (column != null) {
                        orderMap.put(column, Double.valueOf(orderMap0.get(c).toString()).intValue());
                    }
                }
            }
            
            Map<String, String> sortMap0 = (Map<String, String>) map.get("sortMap");
            if (sortMap0 != null) {
                for (String c: sortMap0.keySet()) {
                    DesignerColumn column = table.findColumnByUuid(c);
                    if (column != null) {
                        sortMap.put(column, IndexOrder.valueOf(sortMap0.get(c)));
                    }
                }
            }
            
            Map<String, Object> lengthMap0 = (Map<String, Object>) map.get("orderMap");
            if (lengthMap0 != null) {
                for (String c: lengthMap0.keySet()) {
                    DesignerColumn column = table.findColumnByUuid(c);
                    if (column != null) {
                        lengthMap.put(column, Double.valueOf(lengthMap0.get(c).toString()).longValue());
                    }
                }
            }
            
            String fk = (String) map.get("foreignKey");
            if (fk != null) {
                foreignKey.set(table.findForeignKeyByUuid(fk));
            }
        }
    }
}
