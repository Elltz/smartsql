package np.com.ngopal.smart.sql.structure.queryutils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.db.HistoryQueryService;
import np.com.ngopal.smart.sql.db.HistoryTemplateService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Favourites;
import np.com.ngopal.smart.sql.model.HistoryQuery;
import np.com.ngopal.smart.sql.model.HistoryTemplate;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.DefaultCompletionProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class CustomCompletionProvider extends DefaultCompletionProvider {
        
    public enum CompletionState {
        DEFAULT,
        FEQ,
        HISTORY,
        FAVORITE,
        TAB,
        FULL
    }
    
    public interface DataProvider {
        
        DBTable getTable(String tableName);
        List<DBTable> getTables();
        
    }
    
    private CompletionState completionState = CompletionState.DEFAULT;
    
    private final int MAX_COMPLATION = 25;
    
    @Setter
    private HistoryQueryService historyQueryService;
    @Setter
    private HistoryTemplateService historyTemplateService;
    @Setter
    private FavouritesService favouritesService;
    
    @Setter
    private DataProvider dataProvider;
    
    private Pattern SELECT_0 = Pattern.compile("SELECT .+? FROM ", Pattern.CASE_INSENSITIVE);
    private Pattern SELECT_1 = Pattern.compile("SELECT .+? FROM (.+?) WHERE ", Pattern.CASE_INSENSITIVE);
    
    public CustomCompletionProvider(){
        
    }
    
    public void defaultState() {
        completionState = CompletionState.DEFAULT;
    }
    
    public void fullState() {
        completionState = CompletionState.FULL;
    }
    
    public void feqState() {
        completionState = CompletionState.FEQ;
    }
    
    public void tabState() {
        completionState = CompletionState.TAB;
    }
    
    public void historyState() {
        completionState = CompletionState.HISTORY;
    }
    
    public void favoriteState() {
        completionState = CompletionState.FAVORITE;
    }

    @Override
    protected boolean sameLvl(Completion c, String text) {
        return c.getInputText().contains(".") == text.contains(".");
    }
    
    
    

    //forcing it to obey and autocomplete words like "sakila.actor"
    @Override
    protected boolean isValidChar(char ch) {
        return super.isValidChar(ch) || ch == '.' || ch == '(';
    }

    @Override
    public String getAlreadyEnteredText(JTextComponent comp) {        
        
        switch (completionState) {            
            case FEQ:
            case HISTORY:
            case FAVORITE:
                return getLineEnteredText(comp);
                
            default:
                String s = super.getAlreadyEnteredText(comp);
                int lastIndex = s.lastIndexOf('.');
                int firstIndex = s.indexOf('.');

                if (lastIndex > -1 && firstIndex > -1 && lastIndex != firstIndex) {
                    return s.substring(lastIndex + 1);
                }
                return s;
        }
    }
    
    
    private String getLineEnteredText(JTextComponent comp) {
        Document doc = comp.getDocument();

        int dot = comp.getCaretPosition();
        Element root = doc.getDefaultRootElement();
        int index = root.getElementIndex(dot);
        Element elem = root.getElement(index);
        int start = elem.getStartOffset();
        int len = dot-start;
        try {
            doc.getText(start, len, seg);
        } catch (BadLocationException ble) {
            ble.printStackTrace();
            return EMPTY_STRING;
        }

        return len==0 ? EMPTY_STRING : new String(seg.array, start, len);
    }

    @Override
    public List<Completion> getCompletions(JTextComponent comp) {
        
        switch (completionState) {            
            case FEQ:
                return getFEQCompletion(getLineEnteredText(comp));
                
            case HISTORY:
                return getHistoryCompletion(getLineEnteredText(comp));
                
            case FAVORITE:
                return getFavoriteCompletion(getLineEnteredText(comp));                
                
            case TAB:
                String line = getLineEnteredText(comp);
                line = line.replaceAll("\\s+", " ");
                
                List<Completion> resultList = new ArrayList<>();
                
                String alreadyEnteredText = getAlreadyEnteredText(comp).toLowerCase();
                Matcher m = SELECT_1.matcher(line);
                if (m.find()) {
                    
                    String result = m.group(1);
                    if (result != null) {
                        
                        String[] tables = result.split(",");
                        for (String table: tables) {
                            
                            String[] data = table.trim().split(" ");
                            if (data.length > 0) {
                                String name = data[0];
                                String alias = name;
                                if (data.length > 1) {
                                    alias = data[1];
                                }
                                
                                DBTable t = dataProvider.getTable(name);
                                if (t != null && t.getColumns() != null) {
                                    for (Column c: t.getColumns()) {
                                        if (c.getName().toLowerCase().startsWith(alreadyEnteredText)) {
                                            SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(this, (tables.length > 0 ? alias + "." : "") + c.getName(), c, c.getTable().getDatabase().getName(), RsSyntaxCompletionHelper.columnIcon);
                                            resultList.add(b);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    m = SELECT_0.matcher(line);
                    if (m.find()) {
                        List<DBTable> tables = dataProvider.getTables();
                        if (tables != null) {
                            for (DBTable t: tables) {
                                if (t.getName().toLowerCase().startsWith(alreadyEnteredText)) {
                                    SmartSQLDatabaseReferenceCompletion b = new SmartSQLDatabaseReferenceCompletion(this, t.getName(), t, t.getDatabase().getName(), RsSyntaxCompletionHelper.tableIcon);
                                    resultList.add(b);
                                }
                            }
                        }
                    }
                }
                
                return resultList;
                
            case FULL:
                String text = getAlreadyEnteredText(comp);
                if (text != null && text.contains(".")) {
                    break;
                }
                return completions;
        }
                
        // We add only unique completions
        // TODO Need check this code
        List<Completion> list = new ArrayList<>();
        
        for (Completion c: super.getCompletions(comp)) {
            if (!list.contains(c)) {
                list.add(c);
            }
        }
        return list;
    }
    
    
    private boolean queryContains(String query, String enteredText) {
        query = query.toLowerCase().replaceAll("\\s+", " ").trim();
        enteredText = enteredText.toLowerCase().replaceAll("\\s+", " ").trim();
        
        if (enteredText.split(" ").length == 1 && !query.isEmpty())  {
            for (String s: query.split(" ")) {
                if (s.contains(enteredText)) {
                    return true;
                }
            }
        }
        return !query.isEmpty() && query.startsWith(enteredText);
    }

    
    private List<Completion> getFEQCompletion(String enteredText) {
        List<Completion> result = new ArrayList<>();
        
        int index = 1;
        List<HistoryTemplate> listTemplates = historyTemplateService.selectAllWithOrder(RsSyntaxCompletionHelper.getInstance().getConnectionId());        
        for (HistoryTemplate ht: listTemplates) {        
            if (index < MAX_COMPLATION || enteredText.trim().replace("\\s+", " ").split(" ").length == 1) {
                if (ht.getQuery() != null && queryContains(ht.getQuery(), enteredText)) {
                    result.add(new BasicCompletion(this, ht.getQueryTemplate()));
                    index++;
                }
            } else {
                break;
            }
        }
        
        return result;
    }
    
    private List<Completion> getHistoryCompletion(String enteredText) {
        List<Completion> result = new ArrayList<>();
        
        int index = 1;
        List<HistoryQuery> listHistories = historyQueryService.selectAllOnMonth(RsSyntaxCompletionHelper.getInstance().getConnectionId());
        for (HistoryQuery hq: listHistories) {
            if (index < MAX_COMPLATION) {
                if (hq.getQuery() != null && queryContains(hq.getQuery(), enteredText)) {
                    result.add(new BasicCompletion(this, hq.getQuery()));
                    index++;
                }
            } else {
                break;
            }
        }

        return result;
    }    
    
    private List<Completion> getFavoriteCompletion(String enteredText) {
        List<Completion> result = new ArrayList<>();
        
        int index = 1;
        List<Favourites> list = favouritesService.getAll();
        for (Favourites f: list) {
            if (index < MAX_COMPLATION) {
                if (f.getQuery() != null && queryContains(f.getQuery(), enteredText)) {
                    result.add(new BasicCompletion(this, f.getQuery()));
                    index++;
                }
            } else {
                break;
            }
        }

        return result;
    }
    

}
