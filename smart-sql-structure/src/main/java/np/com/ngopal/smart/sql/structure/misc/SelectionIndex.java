package np.com.ngopal.smart.sql.structure.misc;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javax.persistence.Transient;
import lombok.*;
import np.com.ngopal.smart.sql.model.Index;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class SelectionIndex extends Index {
    
    @Transient
    private SimpleBooleanProperty selectedProperty;
    @Transient
    private SimpleStringProperty nameProperty;    
    
    
    public SimpleStringProperty name() {
        if (nameProperty == null) {
            nameProperty = new SimpleStringProperty(getName());
            nameProperty.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                super.setName(newValue);
            });
        }
        
        return nameProperty;
    }
    
     public SimpleBooleanProperty selected() {
        if (selectedProperty == null) {
            selectedProperty = new SimpleBooleanProperty(false);
        }
        
        return selectedProperty;
    }
    
    @Override
    public void setName(String name) {
        name().set(name);
    }
}
