
package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ProfilerTopData {
    
    private final SimpleStringProperty source;
    private final SimpleObjectProperty time;
    private final SimpleObjectProperty threads;
     
    public ProfilerTopData(String source, BigDecimal time, BigDecimal threads) {
        this.source = new SimpleStringProperty(source);
        this.threads = new SimpleObjectProperty(threads);
        this.time = new SimpleObjectProperty(time);
    }
    
    public String getSource() {
        return source.get();
    }
    public void setSource(String source) {
        this.source.set(source);
    }       
    
    public BigDecimal getThreads() {
        return (BigDecimal) threads.get();
    }
    public void setThreads(BigDecimal threads) {
        this.threads.set(threads);
    }
    
    public BigDecimal getTime() {
        return (BigDecimal) time.get();
    }
    public void setTime(BigDecimal time) {
        this.time.set(time);
    }
    
    
    public Object[] getValues() {
        return new Object[] {
            getSource(),
            getTime(),
            getThreads()
        };
    }
}