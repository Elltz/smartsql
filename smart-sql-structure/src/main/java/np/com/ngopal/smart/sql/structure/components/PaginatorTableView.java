package np.com.ngopal.smart.sql.structure.components;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class PaginatorTableView<T> extends BorderPane {

    private final TableView<T> tableView = new TableView<>();
    
    private final Button homeButton = new Button();
    private final Button prevButton = new Button();
    private final Button nextButton = new Button();
    private final Button endButton = new Button();
    
    private final Label totalLabel = new Label("1 of 1") ;
    
    private int currentPage = 0;
    private int rowsPerPage = 25;
    
    private ObservableList allData = FXCollections.observableArrayList();
        
    private FilteredList<T> filterList;
    private SortedList<T> sortedList;
    
    private Predicate<? super T> predicate;
    
    private HBox paginatorBox;
    
    public PaginatorTableView() {
        
        getStyleClass().add("paginatorTableView");
        
//        tableView.setFixedCellSize(18);
        tableView.setStyle("-fx-font-size: 11;");
        paginatorBox = new HBox(homeButton, prevButton, totalLabel, nextButton, endButton);
        paginatorBox.getStyleClass().add("paginatorButtonHBox");
        
        homeButton.getStyleClass().add("paginatorHomeButton");
        homeButton.setFocusTraversable(false);
        homeButton.setOnAction((ActionEvent event) -> {
            currentPage = 0;
            dataChanged();
        });
        
        prevButton.getStyleClass().add("paginatorPrevButton");
        prevButton.setFocusTraversable(false);
        prevButton.setOnAction((ActionEvent event) -> {
            currentPage--;
            dataChanged();
        });
                
        totalLabel.getStyleClass().add("paginatorTotalLabel");        
        
        nextButton.getStyleClass().add("paginatorNextButton");
        nextButton.setFocusTraversable(false);
        nextButton.setOnAction((ActionEvent event) -> {
            currentPage++;
            dataChanged();
        });
        
        endButton.getStyleClass().add("paginatorEndButton");
        endButton.setFocusTraversable(false);
        endButton.setOnAction((ActionEvent event) -> {
            
            int size = filterList.size() / rowsPerPage + (filterList.size() % rowsPerPage > 0 || filterList.isEmpty() ? 1 : 0);
            currentPage = size - 1;
            
            dataChanged();
        });
        
        setTop(paginatorBox);
        
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        setCenter(tableView);
        
        tableView.comparatorProperty().addListener((ObservableValue<? extends Comparator<T>> observable, Comparator<T> oldValue, Comparator<T> newValue) -> {
            dataChanged();
        });
    }
    
    public TableColumn getColumnByName(String name) {
        if (name != null) {
            for (TableColumn tc : tableView.getColumns()) {
                if (name.equals(tc.getText())) {
                    return tc;
                } else {
                    Node n = tc.getGraphic();
                    if (n instanceof Label && name.equals(((Label) n).getText())) {
                        return tc;
                    }
                }
            }
        }

        return null;
    }
    
    public HBox removePaginator() {
        setTop(null);
        return paginatorBox;
    }
    
    public TableView<T> getTableView() {
        return tableView;
    }
    
    public void dataChanged() {
        
        if (filterList != null) {
            filterList.setPredicate(predicate);

            List<T> list = sortedList;
            if (!sortedList.isEmpty()) {

                int start = currentPage * rowsPerPage;
                int finish = currentPage * rowsPerPage + rowsPerPage;
                if (finish > sortedList.size()) {
                    finish = sortedList.size();
                }

                list = sortedList.subList(start, finish);
            }   

            tableView.getItems().setAll(list);        


            int size = filterList.size() / rowsPerPage + (filterList.size() % rowsPerPage > 0 || filterList.isEmpty() ? 1 : 0);

            homeButton.setDisable(currentPage <= 0);
            prevButton.setDisable(currentPage <= 0);
            nextButton.setDisable(currentPage >= size - 1);
            endButton.setDisable(currentPage >= size - 1);

            totalLabel.setText((currentPage + 1) + " of " + size) ;
        }
    }
    
    /**
     * Add table column and bind width
     *
     * @param tableColumn input table column
     * @param bindingWidth binding value from 0.00 to 1.00
     */
    public void addTableColumn(TableColumn tableColumn, double bindingWidth) {
        tableView.getColumns().add(tableColumn);
        tableColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(bindingWidth));
    }

    public void addTableColumn(int index, TableColumn tableColumn, double bindingWidth) {
        tableView.getColumns().add(index, tableColumn);
        tableColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(bindingWidth));
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth, double minWidth) {
        addTableColumn(tableColumn, bindingWidth, minWidth, true);
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth, double minWidth, boolean inPercent) {
        tableView.getColumns().add(tableColumn);
        if (inPercent) {
            tableColumn.prefWidthProperty().bind(tableView.widthProperty().multiply(bindingWidth));
        } else {
            tableColumn.setPrefWidth(bindingWidth);
        }
        tableColumn.setMinWidth(minWidth);
    }

    public void addColumnToSortOrder(TableColumn tableColumn) {
        tableView.getSortOrder().add(tableColumn);
    }
    
    
    public void setPredicate(Predicate<? super T> predicate) {
        this.predicate = predicate;
        
        currentPage = 0;
        dataChanged();
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
        
        dataChanged();
    }
    
    public void scrollTo(T row) {
        tableView.scrollTo(row);
    }
    
    public ObservableList<TableColumn<T,?>> getColumns() {
        return tableView.getColumns();
    }
    
    public ReadOnlyObjectProperty<Comparator<T>> comparatorProperty() {
        return tableView.comparatorProperty();
    }
    
    public ObservableList<T> getItems() {
        return allData;
    }
    
    public ObservableList<T> getFilteredItems() {
        return filterList;
    }
    
    public List<T> getCopyItems() {
        synchronized (tableView) {
            return new ArrayList<>(tableView.getItems());
        }
    }
    
    public synchronized void setItems(ObservableList<T> list) {
        if (sortedList != null) {
            sortedList.comparatorProperty().unbind();
        }
        
        allData.clear();
        
        if (list != null) {
            
            allData.addAll(list);
            
//            allData.addListener((ListChangeListener.Change c) -> {
//                dataChanged();
//            });
            
            filterList = new FilteredList<>(allData != null ? allData : FXCollections.observableArrayList());
            sortedList = new SortedList<>(filterList);
            
            sortedList.comparatorProperty().bind(tableView.comparatorProperty());
        }
        
        try {
            dataChanged();
        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }
    
    public TableView.TableViewSelectionModel<T> getSelectionModel() {
        return tableView.getSelectionModel();
    }
}
