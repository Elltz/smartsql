
package np.com.ngopal.smart.sql.structure.models;

import java.util.Objects;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DebugInnoTopData implements FilterableData, DebugExplainProvider {
    
    private final SimpleObjectProperty<Long> id;
    private final SimpleStringProperty user;
    private final SimpleStringProperty status;
    private final SimpleStringProperty time;
    private final SimpleObjectProperty<Long> undoLog;
    private final SimpleObjectProperty<Long> lStruct;
    private final SimpleStringProperty state;
    private final SimpleStringProperty qText;
    private final SimpleStringProperty database;
      
 
    private Long rowsSum = null;
    
    public DebugInnoTopData(Long id, String user, String status, String time, Long undoLog, Long lStruct, String state, String qText, String database) {        
        this.id = new SimpleObjectProperty<>(id);
        this.user = new SimpleStringProperty(user);
        this.status = new SimpleStringProperty(status);
        this.time = new SimpleStringProperty(time);
        this.undoLog = new SimpleObjectProperty<>(undoLog);
        this.lStruct = new SimpleObjectProperty<>(lStruct);
        this.state = new SimpleStringProperty(state);
        this.qText = new SimpleStringProperty(qText);
        this.database = new SimpleStringProperty(database);
    }
    
    @Override
    public void setRowsSum(Long rowsSum) {
        this.rowsSum = rowsSum;
    }
    @Override
    public Long getRowsSum() {
        return rowsSum;
    } 
        
    public Long getId() {
        return (Long) id.get();
    }
    public void setId(Long id) {
        this.id.set(id);
    }
    
    public DebugInnoTopData getTimeProvider() {
        return this;
    }
    
    public String getUser() {
        return user.get();
    }
    public void setUser(String user) {
        this.user.set(user);
    }
    
    public String getState() {
        return state.get();
    }
    public void setState(String state) {
        this.state.set(state);
    }
    
    public String getStatus() {
        return status.get();
    }
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    public String getTime() {
        return time.get();
    }
    public void setTime(String time) {
        this.time.set(time);
    }
        
    public Long getUndoLog() {
        return undoLog.get();
    }
    public void setUndoLog(Long undoLog) {
        this.undoLog.set(undoLog);
    }
    
    public Long getLStruct() {
        return lStruct.get();
    }
    public void setLStruct(Long lStruct) {
        this.lStruct.set(lStruct);
    }    
    
    public String getQText() {
        return qText.get();
    }
    public void setQText(String qText) {
        this.qText.set(qText);
    }
    
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {            
            getId() != null ? getId().toString() : "",
            getUser() != null ? getUser() : "",
            getStatus() != null ? getStatus() : "",
            getTime() != null ? getTime() : "",
            getUndoLog() != null ? getUndoLog().toString() : "",
            getLStruct() != null ? getLStruct().toString() : "",
            getQText() != null ? getQText() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getId(),
            getUser(),
            getStatus(),
            getTime(),
            getUndoLog(),
            getLStruct(),
            getQText()
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DebugInnoTopData) {
            DebugInnoTopData d = (DebugInnoTopData) obj;
            
            return Objects.equals(d.getId(), getId()) && Objects.equals(d.getUser(), getUser()) && Objects.equals(d.getQText(), getQText());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        hash = 41 * hash + Objects.hashCode(this.user);
        hash = 41 * hash + Objects.hashCode(this.qText);
        return hash;
    }

    @Override
    public String getDatabase() {
        return database.get();
    }

    
    public static final String INNO_TOP__NOT_STARTED = "Not started";
    public static final String INNO_TOP__ACTIVE = "ACTIVE";
    public static final String INNO_TOP__UNDO = "UNDO";    
    public static final String[] INNO_TOP__STATUSES = new String[] {INNO_TOP__UNDO, INNO_TOP__ACTIVE, INNO_TOP__NOT_STARTED};
    
    public static final String COLOR_RED = "#ff0000";
    public static final String COLOR_RED_LIGHT = "#ff3b00";
    public static final String COLOR_ORANGE = "#ff5400";
    public static final String COLOR_GREEN = "green";
    public static final String COLOR_WHITE = "white";
    public static final String[] COLORS = new String[] {COLOR_RED, COLOR_RED_LIGHT, COLOR_ORANGE, COLOR_GREEN, COLOR_WHITE};
    
    public String getColor() {
        
        String color = COLOR_WHITE;
        
        String[] times = getTime().split(":");
        Long secs = 0l;
        if (times.length > 2) {
            secs = Long.valueOf(times[0]) * 36000 + Long.valueOf(times[1]) * 60 + Long.valueOf(times[2]);
        }
        
        if ("UNDO".equalsIgnoreCase(getStatus())) {
            color = COLOR_RED;
            
        } else {
            if (INNO_TOP__NOT_STARTED.equalsIgnoreCase(getStatus())) {
                color = COLOR_WHITE;
            } else {
                if (secs <= 10) {
                    color = COLOR_GREEN;
                } else if (secs <= 30) {
                    color = COLOR_ORANGE;
                } else if (secs <= 50) {
                    color = COLOR_RED_LIGHT;
                } else {
                    color = COLOR_RED;
                }
            }
        }
        
        return color;
    }
    
}