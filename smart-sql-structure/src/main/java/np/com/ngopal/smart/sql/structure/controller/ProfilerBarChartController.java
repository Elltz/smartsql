package np.com.ngopal.smart.sql.structure.controller;

import com.sun.javafx.charts.Legend;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Popup;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ProfilerBarChartController extends MetricEntityContainerController {

    private GSChart chart;
    private XYChart<String, Number> gsChart;
    private CategoryAxis xAxis;
    private NumberAxis yAxis;
    
    private ProfilerChartControllerEventHandler chartControllerEventHandler;
    
    private double initStart = -1;
    private double initFinish = -1;

    private final SimpleIntegerProperty currentScaleindex
            = new SimpleIntegerProperty(Flags.DEFAULT_SCALE_INDEX);

    private Line mouseLine = new Line();
    private Popup popup = new Popup();
    private Label popupLabel = new Label();
    private Map<XYChart.Series<String, Number>, Label> popupValueLabels = new HashMap<>();
    
    private Map<XYChart.Series<String, Number>, String> seriesColors;
            
    private Label toggledLegend;
    private static final String CSS_TOGGLED_LEGEND = "profilerChartToggledLegend";
    
    private final SimpleDateFormat popupFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    
    
    public static String formatToXValue(long time) {
        return dateFormat.format(new Date(time));
    }
    
    public static Long formatFromXValue(String time) {
        try {
            return dateFormat.parse(time).getTime();
        } catch (ParseException ex) {
            return 0l;
        }
    }
    
    public SimpleIntegerProperty currentScaleIndex() {
        return currentScaleindex;
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        createEntityAsModule();

        xAxis = new CategoryAxis();
        yAxis = new NumberAxis();

        xAxis.setTickLabelRotation(0);
        initDefaultContextItems();
    }

    public void insertGSChart(GSChart chart) {
        if (this.chart != null) {
            return;
        }
        this.chart = chart;
        
        initChart();
        gsChart.setTitle(representableName());
    }

    private void initChart() {
        
        List<XYChart.Series<String, Number>> list = FXCollections.observableArrayList();
        if (gsChart != null) {
            gsChart.setOnMousePressed(null);
            gsChart.setOnMouseReleased(null);
            gsChart.setOnMouseDragged(null);
            list.addAll(gsChart.getData());
            gsChart = null;
        }


        gsChart = new BarChart<>(xAxis, yAxis);
        gsChart.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);
        gsChart.setLegendVisible(false);
        
        gsChart.setMinHeight(250);
        gsChart.setMaxHeight(250);
        gsChart.setPrefHeight(250);
        
        applyColors();
        
        gsChart.getData().addAll(list);  
        
        gsChart.setAnimated(false);

        setContent(null);
        
        AnchorPane pane = new AnchorPane();
        pane.getChildren().add(gsChart);
        
        AnchorPane.setBottomAnchor(gsChart, 0.0);
        AnchorPane.setLeftAnchor(gsChart, 0.0);
        AnchorPane.setTopAnchor(gsChart, 0.0);
        AnchorPane.setRightAnchor(gsChart, 0.0);
        
        setContent(pane);
        
        gsChart.setTitle(representableName());
        
        gsChart.setOnMouseMoved((MouseEvent event) -> {
                        
            if (popup.isShowing()) {
                popup.hide();
            }
            
            Map<XYChart.Series<String, Number>, XYChart.Data<String, Number>> dataForShow = collectDataForShow(event.getSceneX(), event.getSceneY());
            if (dataForShow != null && !dataForShow.isEmpty()) {

                boolean first = true;
                for (Map.Entry<XYChart.Series<String, Number>, XYChart.Data<String, Number>> entry: dataForShow.entrySet()) {

                    XYChart.Series<String, Number> series = entry.getKey();
                    XYChart.Data<String, Number> data = entry.getValue();
                                        
                    if (first) {
                        first = false;
                        popupLabel.setText(data.getXValue());                        
                    }
                    
                    Label valueLabel = popupValueLabels.get(series);
                    if (valueLabel != null) {
                        if (data.getYValue().longValue() < data.getYValue().doubleValue()) {
                            valueLabel.setText(new BigDecimal(data.getYValue().doubleValue()).setScale(2, RoundingMode.HALF_UP).toString());
                        } else {
                            valueLabel.setText(String.valueOf(data.getYValue().longValue()));
                        }
                    }
                }    
                
                if (!gsChart.getData().isEmpty()) {
                    
//                    hoveredX.set(xAxis.getValueForDisplay(event.getX() - yAxis.getWidth() - 16));
//                    hoveredOnChart.set(true);
                }
                     
                popup.show(getController().getStage(), event.getScreenX() + 10, event.getScreenY() + 5);
            }
        });
        
        gsChart.setOnMouseExited((MouseEvent event) -> {
            if (popup.isShowing()) {
                popup.hide();
            }
            
//            hoveredOnChart.set(false);
//            hoveredX.set(0.0);
        });
        
        if (chart.getDescription() != null && !chart.getDescription().isEmpty()) {
            Label info = new Label();
            info.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getInfo_image()));
            
            Tooltip tooltip = new Tooltip(chart.getDescription());
            tooltip.getStyleClass().add("profilerChartInfoTooltip");
            info.setTooltip(tooltip);
            
            pane.getChildren().add(info);
            AnchorPane.setLeftAnchor(info, 0.0);
            AnchorPane.setTopAnchor(info, 0.0);
        }
    }
    
    
    private Map<XYChart.Series<String, Number>, XYChart.Data<String, Number>> collectDataForShow(double mouseX, double mouseY) {
        Map<XYChart.Series<String, Number>, XYChart.Data<String, Number>> result = new LinkedHashMap<>();
        
        Point2D mouseSceneCoords = new Point2D(mouseX, mouseY);
        double x = xAxis.sceneToLocal(mouseSceneCoords).getX();
        
        try {
            String value = xAxis.getValueForDisplay(x);
            if (value != null) {
                Date currentDate = dateFormat.parse(value);

                for (XYChart.Series<String, Number> s: gsChart.getData()) {
                    if (s.getData() != null) {

                        XYChart.Data<String, Number> bestData = null;
                        for (XYChart.Data<String, Number> d: s.getData()) {

                            Date bestDate = bestData != null ? dateFormat.parse(bestData.getXValue()) : null;
                            Date dDate = dateFormat.parse(d.getXValue());

                            if (bestData == null) {
                                bestData = d;                        
                            } else if (Math.abs(dDate.getTime() - currentDate.getTime()) <= Math.abs(bestDate.getTime() - currentDate.getTime())) {
                                bestData = d;
                            }
                        }

                        if (bestData != null) {
                            result.put(s, bestData);
                        }
                    }
                }
            }
        } catch (ParseException ex) {}
        
        return result;
    }
    
    
//    private static String web2Rgba(String colorStr, double alpha) {
//        return "rgba(" +
//                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ) + ", " +
//                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ) + ", " +
//                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) + ", " + alpha + ")";
//    }
    
    private void applyColors() {
        
        if (!gsChart.getData().isEmpty()) {
            for (int i = 0; i < gsChart.getData().size(); i++) {
                String seriesColor = seriesColors.get(gsChart.getData().get(i));
                for(Node n: gsChart.lookupAll(".default-color" + i + ".chart-bar")) {
                    n.setStyle("-fx-bar-fill: " + seriesColor + ";");
                }
            }
        }
//        
//        for (Node n: gsChart.getChildrenUnmodifiable()) {
//            if (n instanceof Legend) {
//                for(Legend.LegendItem legendItem: ((Legend)n).getItems()){
//                   
//                    for (XYChart.Series<String, Number> s: gsChart.getData()) {
//                        if (s.getName().equals(legendItem.getText())) {
//                            String seriesColor = seriesColors.get(s);
//                            legendItem.getSymbol().setStyle("-fx-background-color: " + seriesColor +", white;");
//                        }
//                    }
//                }
//            }
//         }
//
//        for (Node n : gsChart.getChildrenUnmodifiable()) {
//            if (n instanceof Legend) {
//                Legend l = (Legend) n;
//                for (Legend.LegendItem li : l.getItems()) {
//                    for (XYChart.Series<String, Number> s : gsChart.getData()) {
//                        
//                        String seriesColor = seriesColors.get(s);
//                        
//                        if (s.getName().equals(li.getText())) {
//                            Node symbol = l.getChildren().get(0).lookup(".chart-legend-item-symbol");
//                            
//                            if (symbol != null) {
//                                symbol.setStyle("-fx-stroke: " + seriesColor + ";");
//                            }
//
//                        }
//                    }
//                }
//            }
//        }
    }

    public void init(Collection<XYChart.Series<String, Number>> series, Map<XYChart.Series<String, Number>, String> seriesColors) {
        this.seriesColors = seriesColors;
        
        gsChart.getData().addAll(series);
        for (XYChart.Series<String, Number> s: series) {
            s.dataProperty().addListener(new InvalidationListener() {
                @Override
                public void invalidated(Observable observable) {
                    System.out.println("1111");
                }
            });
        }
                
        applyColors();
        
        //((Pane)((Parent)(((Region)gsChart).getChildrenUnmodifiable()).get(1)).getChildrenUnmodifiable().get(0)).getChildren();
        
        if (gsChart.getData().get(0).getNode() != null) {
            ((Group)gsChart.getData().get(0).getNode()).getChildren().add(mouseLine);
        }
        mouseLine.setVisible(false);
        
        popupValueLabels.clear();
        
        popupLabel.getStyleClass().add("profilerChartDataTooltipTitleLabel");
        
        GridPane gp = new GridPane();
        gp.getStyleClass().add("profilerChartDataTooltip");
        gp.add(popupLabel, 0, 0, 2, 1);
        GridPane.setHalignment(popupLabel, HPos.CENTER);
        
        if (series != null) {
            int row = 1;
            for (XYChart.Series<String, Number> s: series) {
                
                Line l = new Line(0, 15, 15, 15);
                l.setStrokeWidth(2);
                l.setStroke(Color.web(seriesColors.get(s)));
                
                Label title = new Label();
                title.getStyleClass().add("profilerChartDataTooltipLabel");
                title.setText(s.getName() + ": ");
                title.setGraphic(l);
                
                Label value = new Label();
                value.getStyleClass().add("profilerChartDataTooltipBoldLabel");
                
                gp.add(title, 0, row);
                GridPane.setHgrow(title, Priority.ALWAYS);
                
                gp.add(value, 1, row);
                GridPane.setHalignment(value, HPos.RIGHT);
                
                popupValueLabels.put(s, value);
                row++;
            }
        }
        
        for (Node n : gsChart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {                
                Legend l = (Legend) n;
                for (int i = 0; i < l.getItems().size(); i++) {
                    Label liNode = (Label) l.getChildren().get(i);
                    liNode.setCursor(Cursor.HAND);                            
                    liNode.setOnMouseClicked(me -> {
                        if (me.getButton() == MouseButton.PRIMARY) {
                            toggleSeries(liNode);
                        }
                    });
                }
            }
        }
        popup.getContent().clear();
        popup.getContent().add(gp);
    }

    private void toggleSeries(Label li) {
        
        boolean visible = false;
        if (toggledLegend == li) {
            visible = true;
            toggledLegend = null;
        } else {
            toggledLegend = li;
        }
        
        for (Node n : gsChart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {                
                Legend l = (Legend) n;
                for (int i = 0; i < l.getItems().size(); i++) {
                    
                    Label liNode = (Label) l.getChildren().get(i);
                    if (liNode == li) {
                        if (visible) {
                            liNode.getStyleClass().remove(CSS_TOGGLED_LEGEND);
                        } else {
                            if (!liNode.getStyleClass().contains(CSS_TOGGLED_LEGEND)) {
                                liNode.getStyleClass().add(CSS_TOGGLED_LEGEND);
                            }
                        }
                        
                    } else {                        
                        liNode.getStyleClass().remove(CSS_TOGGLED_LEGEND);
                    }
                }
            }
        }
        
        for (XYChart.Series<String, Number> s: gsChart.getData()) {
            if (!s.getName().equals(li.getText())) {
                s.getNode().setVisible(visible);
                for (XYChart.Data<String, Number> d : s.getData()) {
                    if (d.getNode() != null) {
                        d.getNode().setVisible(visible);
                    }
                }
            } else {
                s.getNode().setVisible(true);
                for (XYChart.Data<String, Number> d : s.getData()) {
                    if (d.getNode() != null) {
                        d.getNode().setVisible(true);
                    }
                }
            }
        }
    }
    
    public void clear() {
        initStart = -1;
        initFinish = -1;

        for (XYChart.Series<String, Number> series : gsChart.getData()) {
            series.getData().clear();
        }

        gsChart.getData().clear();
    }

    public void updateChart(boolean showAll, SimpleStringProperty zoomCountStage, Long firstRunDate, Long dragedStart, Long dragedFinish) {
        try {
            if (firstRunDate != null) {

                long lower;
                long currentTime = new Date().getTime();
                long upper = currentTime;

                if (initStart == -1 && initFinish == -1) {

                    long runDate = firstRunDate;
                    if (gsChart.getData() != null && !gsChart.getData().isEmpty()) {
                        XYChart.Series<String, Number> s = gsChart.getData().get(0);
                        if (s.getData() != null && !s.getData().isEmpty()) {
                            runDate = dateFormat.parse(s.getData().get(0).getXValue()).getTime();
                            currentTime = dateFormat.parse(s.getData().get(s.getData().size() - 1).getXValue()).getTime();
                        }
                    }

                    if (showAll) {
                        lower = runDate;
                        if (zoomCountStage != null) {
                            zoomCountStage.set("Show All");
                        }

                    } else if (dragedStart != null && dragedFinish != null) {
                        lower = dragedStart;
                        upper = dragedFinish;

                    } else {
                        ChartScale currentScale = ChartScale.values()[currentScaleindex.get()];

                        upper = currentTime;
                        lower = upper - currentScale.getValue();
                        if (zoomCountStage != null) {
                            zoomCountStage.set(currentScale.getName());
                        }
                    }
                } else if (dragedStart != null && dragedFinish != null) {
                    lower = dragedStart;
                    upper = dragedFinish;
                } else if (showAll) {
                    lower = (long) initStart;
                    upper = (long) initFinish;

                    if (zoomCountStage != null) {
                        zoomCountStage.set("Show All");
                    }
                } else {

                    ChartScale currentScale = ChartScale.values()[currentScaleindex.get()];
                    upper = currentTime;
                    lower = upper - currentScale.getValue();
                    
                    if (zoomCountStage != null) {
                        zoomCountStage.set(currentScale.getName());
                    }
                }
            }
        } catch (ParseException ex) {}
    }

    
    public void initXAxis(double upper, double lower) {
        initFinish = upper;
        initStart = lower;
    }

    public GSChart getChart() {
        return chart;
    }
    
//    private final BooleanProperty hoveredOnChart = new SimpleBooleanProperty(false) {
//        @Override
//        protected void invalidated() {
//            super.invalidated();
//            
//            drawLine();
//        }
//        
//    };
//    public BooleanProperty hoveredOnChart() {
//        return hoveredOnChart;
//    }
    
//    private final ObjectProperty<Long> hoveredX = new SimpleObjectProperty<Long>(0l) {        
//        @Override
//        protected void invalidated() {
//            super.invalidated();
//            
//            drawLine();
//        }        
//    };
//    public ObjectProperty<Long> hoveredX() {
//        return hoveredX;
//    }
    
//    private void drawLine() {
//        if (hoveredOnChart.get()) {
//            mouseLine.setStroke(Color.RED);
//            mouseLine.setStrokeWidth(1);
//            mouseLine.setStartX(hoveredX.get());
//            mouseLine.setStartY(0);
//            mouseLine.setEndX(hoveredX.get());
//            mouseLine.setEndY(yAxis.getHeight());
//        }
//
//        mouseLine.setVisible(hoveredOnChart.get());
//    }

    public void setChartControllerEventHandler(ProfilerChartControllerEventHandler chartControllerEventHandler) {
        if(chartControllerEventHandler != null && gsChart.getOnMousePressed() != chartControllerEventHandler){
            // Drag period functionality
            gsChart.setOnMousePressed(chartControllerEventHandler);
            gsChart.setOnMouseReleased(chartControllerEventHandler);
            gsChart.setOnMouseDragged(chartControllerEventHandler);
        }
        
        if(this.chartControllerEventHandler != chartControllerEventHandler && chartControllerEventHandler != null){
            this.chartControllerEventHandler = chartControllerEventHandler;
            this.chartControllerEventHandler.xAxis = xAxis;
            this.chartControllerEventHandler.chart = gsChart;
        }
    }

    @Override
    public String representableName() {
        return chart == null ? "" : chart.getChartName();
    }
    
}
