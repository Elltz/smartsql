/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.executable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.Executors;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.SmartsqlSavedData;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public abstract class AppUserManager {
    
    private SmartsqlSavedData savedData;
    private Connection onlineConnection;
    private Properties props;

    public AppUserManager() {
        savedData = StandardApplicationManager.getInstance().getSmartsqlSavedData();
    }
    
    public abstract void onLicenseValidityResponse(boolean valid);

    public abstract void onUserValidityResponse(boolean valid);

    public boolean isCurrentUserValid() {
        return true;
    }
    
    public void checkUserValidity() {
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() { 
                boolean valid = isCurrentUserValid();
                onUserValidityResponse(valid);
            }
        });
    }

    public boolean isLicenseValid(String email, String providedLicense) {
        try {
            // for @Terah: check maybe we enabled encryption for email and license, 
            // then need add it to this code
            getLog().info("VALIDATING USER LICENSE");
            connectToMysqlDb();
            Statement state = onlineConnection.createStatement();
            StringBuilder query = new StringBuilder(200);
            query.append("SELECT SmartMySQL.user_validation('");
            query.append(email);
            query.append("','");
            query.append(providedLicense);
            query.append("');");
            ResultSet rs = state.executeQuery(query.toString());
            rs.next();
            boolean response = rs.getInt(1) == 1;
            state.close();
            return response;
        } catch (Throwable e) {
            e.printStackTrace();
            getLog().error(e.getMessage(), e);
        }

        return false;
    }

    public void checkLicenseValidity(){
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() { 
                boolean valid = isLicenseValid(savedData.getEmailId(), savedData.getLKey());
                /**
                 * TODO
                 * need get user details from license after succesful validation
                 * need get license key details, duration etc
                 * need update saveData, so changes to be seen by all app
                 */
                onLicenseValidityResponse(valid);
            }
        });
    }
    
    public boolean hasFreewareLicense(){
        return true;
    }
    
    public void connectToMysqlDb() throws Throwable {
        getLog().info("CREATING CONNECTION TO SERVER");
        
        if (onlineConnection != null && !onlineConnection.isClosed()
                && onlineConnection.isValid(100)) { return; }
        
        Properties props = getConnectionProps();

        try { Class.forName("com.mysql.jdbc.Driver"); } catch (Exception e) { e.printStackTrace(); }
        onlineConnection = DriverManager.getConnection("jdbc:mysql://" + props.getProperty("url") + ":" 
                + props.getProperty("port") + "?allowMultiQueries=true", props);
        Statement state = onlineConnection.createStatement();
        state.execute("use " + props.getProperty("database") + ";SET wait_timeout = 100;");
        state.close();
    }
    
    
    public void disConnectionFromMysqlDb() {
        try {
            if (onlineConnection != null && !onlineConnection.isClosed()
                    && onlineConnection.isValid(100)) {
                if (!onlineConnection.getAutoCommit()) {
                    onlineConnection.commit();
                }
                onlineConnection.close();
                onlineConnection.abort(Executors.newFixedThreadPool(1));
            }
            onlineConnection = null;
        } catch (Exception e) { e.printStackTrace(); }
    }
    
    private Logger getLog(){
        return org.slf4j.LoggerFactory.getLogger(getClass());
    }
    
    private Properties getConnectionProps() throws Exception {
        if (props == null) {
            props = new Properties();

            if (savedData.getOnlineServer() == null) {
                throw new Exception("Missing online server details");
            }

            Cipher cipher = Cipher.getInstance("DESede");
            // @TODO Need get this key from some input like webservice
            String encryptKey = savedData.getAppEncryptionKey();
            if (encryptKey == null || encryptKey.isEmpty()) {
                getLog().error("Encryption key for server is missing");
                encryptKey = "ganFITKDEfasjdONUASD&Y*F*";
            }
            DESedeKeySpec ks = new DESedeKeySpec(encryptKey.getBytes("UTF-8"));
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
            cipher.init(Cipher.DECRYPT_MODE, skf.generateSecret(ks));
            byte[] encryptedText = Base64.decodeBase64(savedData.getOnlineServer());
            byte[] plainText = cipher.doFinal(encryptedText);
            String[] res = new String(plainText, "UTF-8").split("\\$\\$");

            props.put("url", res[0]);
            props.put("port", res[1]);
            props.put("user", res[2]);
            props.put("password", res[3]);
            props.put("database", res[4]);
        }
        return props;
    }
    
    public void closeManager(){
        disConnectionFromMysqlDb();
    }
}
