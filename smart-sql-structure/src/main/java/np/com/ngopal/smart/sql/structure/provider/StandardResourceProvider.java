/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.provider;

import java.io.InputStream;
import java.net.URL;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class StandardResourceProvider {
    
    private static StandardResourceProvider provider;    
    private String resourceDirectory;

    public static StandardResourceProvider getInstance(){
        if(provider == null){
            provider = new StandardResourceProvider();
        }
        return provider;
    }
    
    private StandardResourceProvider() {
        this("np/com/ngopal/smart/sql/structure/provider/");
    }
    public StandardResourceProvider(String resourceDirectory) {
        if(resourceDirectory != null && !resourceDirectory.isEmpty()){
            this.resourceDirectory = checkIfhasSeparator(resourceDirectory);
        }
    }
    
    private String checkIfhasSeparator(String dir) {
        if(dir.charAt(0) == '/'){ dir = dir.substring(1); }        
        if (!dir.endsWith("/")) { dir = dir + "/"; }        
        return dir;
    }
    
    /**
     * Derives the path of the resource file you are looking for
     * @param fileFolder
     * The parent sub-folder the file is in. if its directly in the resource directory put NULL
     * @param file
     * the name of the file + extension
     * @return 
     */
    public final InputStream requireFileAsStream(String fileFolder, String file){
        String resDir = checkIfhasSeparator((fileFolder == null ? resourceDirectory : resourceDirectory + fileFolder));                
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(resDir + file);
    }
    
    public final String requireFileAsString(String fileFolder, String file){
        return requireURL(fileFolder, file).toExternalForm();
    }
    
    public final URL requireURL(String fileFolder, String file){
        String resDir = checkIfhasSeparator((fileFolder == null ? resourceDirectory : resourceDirectory + fileFolder));
        return Thread.currentThread().getContextClassLoader().getResource(resDir + file);
    }
    
    public URL getAnalyzerResultMarkUpURL(){
        return requireURL("markups/", "analyzerResult.html");
    }
    
    public URL getProfileMarkUpURL(){
        return requireURL("markups/", "profile.html");
    }
    
    public InputStream getDefaultImportChartScript(){
        return requireFileAsStream("markups/", "importDefaultChartScript.sql");
    }
    
    public InputStream getTunerBasicPasswords(){
        return requireFileAsStream("tuner/", "basic_passwords.txt");
    }
    
    public InputStream getTunerMysql(){
        return requireFileAsStream("tuner/", "mysqltuner.pl");
    }
    
    public InputStream getTunerVulnerabilities(){
        return requireFileAsStream("tuner/", "vulnerabilities.csv");
    }
    
    public InputStream getRsSyntaxTheme(){
        return requireFileAsStream("themes/", "smart-sql-rsyntax-theme.xml");
    }
    
    public String getDependenceStylesheet(){
        return requireFileAsString("styles/", "style.css");
    }
}
