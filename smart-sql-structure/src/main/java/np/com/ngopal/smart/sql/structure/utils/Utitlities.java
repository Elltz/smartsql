package np.com.ngopal.smart.sql.structure.utils;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class Utitlities {

    /**
     * ////////////////////////////////////////////////////////////////////////
     * This code section is for making a node resizeable only when moouse is
     * coming from the outside
     * ////////////////////////////////////////////////////////////////////////
     */
    private final static Object resizableFeatureKey = new Object();

    private static class NodeResizableHandlersHolder {

        EventHandler<MouseEvent> mouseClickedDownHandler, mouseClickedUpHandler, mouseMovedHandler, mouseEnteredHandler,
                mouseExitedHandler, mouseDraggedHandler;

        private boolean enteredState = false;
        private boolean dragState = false;

        private Region nodeInQuestion = null;
        private final double resizeBorderThickness = 20.0;
        private Point2D lastKnownPointerValue = null;
        private final DropShadow dropShadow = new DropShadow();

        public NodeResizableHandlersHolder() {
            this(null);
        }

        public NodeResizableHandlersHolder(Region n) {
            initializeEventHandlers();
            if (n != null) {
                installHandlersOnNode(n);
            }
        }

        private final void initializeEventHandlers() {
            mouseClickedDownHandler = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    Node n = nodeInQuestion;
                    dragState = (n.getCursor() == Cursor.E_RESIZE || n.getCursor() == Cursor.H_RESIZE
                            || n.getCursor() == Cursor.N_RESIZE || n.getCursor() == Cursor.NE_RESIZE
                            || n.getCursor() == Cursor.NW_RESIZE || n.getCursor() == Cursor.S_RESIZE
                            || n.getCursor() == Cursor.SE_RESIZE || n.getCursor() == Cursor.SW_RESIZE
                            || n.getCursor() == Cursor.V_RESIZE || n.getCursor() == Cursor.W_RESIZE) /* && arg0.isPrimaryButtonDown() */;

                    if (dragState) {
                        // System.out.println("is drag state activated");
                    }

                }
            };

            mouseClickedUpHandler = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    // if(dragState){
                    dragState = false;
                    // System.out.println("drag state de-activated");
                    // }
                    //since we need it to be later. as mouseMoved events are still being propagated
                    //Platform.runLater(() -> enteredState = false);
                }
            };

            mouseEnteredHandler = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    nodeInQuestion.setCursor(getCursorFromPoint(arg0.getSceneX(), arg0.getSceneY()));
                    nodeInQuestion.setEffect(dropShadow);
                    //since we need it to be later. as mouseMoved events are still being propagated
                    Platform.runLater(() -> enteredState = true);
                }
            };

            mouseExitedHandler = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    enteredState = false;
                    nodeInQuestion.setEffect(null);
                }
            };

            mouseMovedHandler = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    if (enteredState) {
                        boolean changeCursor = (lastKnownPointerValue.getX() > 0
                                && Math.abs(lastKnownPointerValue.getX() - arg0.getSceneX()) > resizeBorderThickness)
                                || (lastKnownPointerValue.getY() > 0 && Math
                                .abs(lastKnownPointerValue.getY() - arg0.getSceneY()) > resizeBorderThickness);

                        if (changeCursor) {
                            nodeInQuestion.setCursor(Cursor.DEFAULT);
                            enteredState = false;
                            lastKnownPointerValue = null;
                        }
                    }
                }
            };

            mouseDraggedHandler = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    if (dragState) {
                        /*
						 * System.out.println("Moving in dragstate ");
						 * System.out.println("screen x " +
						 * String.valueOf(arg0.getScreenX()) +"  screen y " +
						 * arg0.getScreenY()); System.out.println("sceen x " +
						 * String.valueOf(arg0.getSceneX()) +"  sceen y " +
						 * arg0.getSceneY());
                         */

                        nodeInQuestion.setEffect(null);
                        Point2D p2d = nodeInQuestion.sceneToLocal(arg0.getSceneX(),arg0.getSceneY());
                        double width = nodeInQuestion.getBoundsInLocal().getMinX() + p2d.getX();
                        double height = nodeInQuestion.getBoundsInLocal().getMinY() + p2d.getY();

                        if (nodeInQuestion.getCursor() == Cursor.H_RESIZE) { // only horizontal
                            nodeInQuestion.setPrefWidth(width);
                        } else if (nodeInQuestion.getCursor() == Cursor.V_RESIZE) { // only vertical
                            nodeInQuestion.setPrefHeight(height);
                        } else {
                            nodeInQuestion.setPrefSize(width, height);
                        }
                    }

                }
            };
        }

        public final void installHandlersOnNode(Region n) {
            n.setMinSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
            n.setOnMouseMoved(mouseMovedHandler);
            n.setOnDragDetected(mouseClickedDownHandler);
            n.setOnMouseReleased(mouseClickedUpHandler);
            n.setOnMouseEntered(mouseEnteredHandler);
            n.setOnMouseExited(mouseExitedHandler);
            n.setOnMouseDragged(mouseDraggedHandler);

            n.getProperties().put(resizableFeatureKey, NodeResizableHandlersHolder.this);

            nodeInQuestion = n;
        }

        private final Cursor getCursorFromPoint(double x, double y) {
            Cursor cursor = Cursor.NONE;
            lastKnownPointerValue = nodeInQuestion.sceneToLocal(x,y);

            boolean isRightWidthPref = nodeInQuestion.getBoundsInLocal().getMaxX() <= lastKnownPointerValue.getX()
                    && nodeInQuestion.getBoundsInLocal().getMinX() < lastKnownPointerValue.getX();

            boolean isLeftWidthPref = nodeInQuestion.getBoundsInLocal().getMinX() >= lastKnownPointerValue.getX()
                    && nodeInQuestion.getBoundsInLocal().getMaxX() > lastKnownPointerValue.getX();

            boolean isTopHeightPref = nodeInQuestion.getBoundsInLocal().getMinY() >= lastKnownPointerValue.getY()
                    && nodeInQuestion.getBoundsInLocal().getMaxY() > lastKnownPointerValue.getY();

            boolean isBottomHeightPref = nodeInQuestion.getBoundsInLocal().getMaxY() <= lastKnownPointerValue.getY()
                    && nodeInQuestion.getBoundsInLocal().getMinY() < lastKnownPointerValue.getY();

            if (isRightWidthPref && isBottomHeightPref) {
                cursor = Cursor.SE_RESIZE;
            } else if (isRightWidthPref && isTopHeightPref) {
                cursor = Cursor.NE_RESIZE;
            } else if (isLeftWidthPref && isBottomHeightPref) {
                cursor = Cursor.SW_RESIZE;
            } else if (isLeftWidthPref && isTopHeightPref) {
                cursor = Cursor.NW_RESIZE;
            } else if (isLeftWidthPref || isRightWidthPref) {
                cursor = Cursor.H_RESIZE;
                lastKnownPointerValue = new Point2D(lastKnownPointerValue.getX(), 0);
            } else if (isBottomHeightPref || isTopHeightPref) {
                cursor = Cursor.V_RESIZE;
                lastKnownPointerValue = new Point2D(0, lastKnownPointerValue.getY());
            }

            System.out.println(cursor.toString());

            return cursor;
        }
    }

    public static void addResizableFeatureToNode(Region node) {
        if (!node.getProperties().containsKey(resizableFeatureKey)) {
            NodeResizableHandlersHolder handlersHolder = new NodeResizableHandlersHolder(node);
        }
    }

    /**
     * //////////////////////////////////////////
     * Node resizeable code flow ends here
     * //////////////////////////////////////////
     */  
    
    /**
     * /////////////////////////////////////////////////////
     * FlowPane code control starts here
     * ////////////////////////////////////////////////////
     */
    
    public static void prepFlowPaneDimensionConditionWithParent(FlowPane fp,double flowpaneChildrenWdithCount, boolean calcHeight) {
        
//        fp.sceneProperty().addListener(new ChangeListener<Scene>() {
//            @Override
//            public void changed(ObservableValue<? extends Scene> observable, Scene oldScene, Scene newScene) {
//                if (newScene != null) {
//                    ((Stage)newScene.getWindow()).fullScreenProperty().addListener(new ChangeListener<Boolean>() {
//                        @Override
//                        public void changed(ObservableValue<? extends Boolean> observable,
//                                Boolean oldValue, Boolean newValue) {
//                            resizeFlowPaneChildren(fp,flowpaneChildrenWdithCount,calcHeight);
//                        }
//                    });
//                    newScene.widthProperty().addListener(new ChangeListener<Number>() {
//                        @Override
//                        public void changed(ObservableValue<? extends Number> observable,
//                                Number oldValue, Number newValue) {
//                            if (newValue != null) {
//                                resizeFlowPaneChildren(fp,flowpaneChildrenWdithCount,calcHeight);
//                            }
//                        }
//                    });
//                }else{ fp.sceneProperty().removeListener(this); }
//            }
//        });
        
        fp.prefWrapLengthProperty().bind(fp.widthProperty().subtract(3));      
        
        fp.getChildren().addListener(new ListChangeListener<Node>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends Node> c) {
                while (c.next()) {
                    if (c.wasAdded() || c.wasRemoved()) {
                        resizeFlowPaneChildren(fp,flowpaneChildrenWdithCount,calcHeight);
                    }
                }
            }
        });
        
        fp.needsLayoutProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                //System.out.println("NEEDS LAYOUT PROPS " + String.valueOf(newValue));
                if(newValue){ resizeFlowPaneChildren(fp,flowpaneChildrenWdithCount,calcHeight); }
            }
        });
        
//        fp.layoutBoundsProperty().addListener(new ChangeListener<Bounds>() {
//            @Override
//            public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
//                System.out.println("LAYOUT BOUNDS PROPS " + String.valueOf(newValue));
//            }
//        });
        
        fp.setHgap(8.0);
        fp.setVgap(10.0);
        
    }
    
    private static void resizeFlowPaneChildrenWithChildResizeablity(FlowPane fp, boolean makeChildrenResizeable,
            double flowpaneChildrenWdithCount,boolean calcHeight) {
        Platform.runLater(() -> {
            for (Node n : fp.getChildren()) {
                
                if (makeChildrenResizeable) {
                    addResizableFeatureToNode((Region) n);
                }
                calculate_AddNodeSpecs(fp, n,flowpaneChildrenWdithCount,calcHeight);
                
            }
        });
    }

    private static void resizeFlowPaneChildren(FlowPane fp, double flowpaneChildrenWdithCount,boolean calcHeight) {
        resizeFlowPaneChildrenWithChildResizeablity(fp, false, flowpaneChildrenWdithCount,calcHeight);
    }

    private static void calculate_AddNodeSpecs(FlowPane fp, Node n, double flowpaneChildrenWdithCount, boolean calcHeight) {
        if (!n.getStyleClass().contains(Flags.METRIC_INNERS_STYLE_CLASS)) {
            n.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);
        }
        double dependant = fp.getPrefWrapLength();
        if (dependant > -1) {
            double containerWidth = (dependant / flowpaneChildrenWdithCount) - 8.0;
            //boolean increase = n instanceof ProfilerNodeChart;
            ((Region) n).setPrefSize(containerWidth,
                    (calcHeight ? containerWidth * 0.4 : ((Region) n).getHeight()));
        }
    }
    
    /**
     * /////////////////////////////////////////////////////
     *  FLOWPANE CODE CONTROL ENDS HERE
     * ///////////////////////////////////////////////////
     */
    
}
