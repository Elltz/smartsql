package np.com.ngopal.smart.sql.structure.controller;

import np.com.ngopal.smart.sql.structure.misc.ProfilerChartControllerEventHandler;
import np.com.ngopal.smart.sql.structure.misc.ProfilerInnerControllerWorkProcAuth;
import javafx.fxml.Initializable;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.GSChartVariable;
import np.com.ngopal.smart.sql.model.GlobalStatusStart;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.metrics.Metric;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class MysqlProfilingController extends BaseController implements Initializable, ProfilerListener {

    private ProfilerDataManager dbProfilerService;
    private ProfilerChartService chartService;

    private static final String STATUS__QUERIES = "QUERIES";
    private static final String STATUS__UPTIME = "UPTIME";
    private static final String STATUS__OPENED_FILES = "OPENED_FILES";
    private static final String STATUS__OPENED_TABLES = "OPENED_TABLES";
    private static final String STATUS__OPEN_TABLES = "OPEN_TABLES";

    private final ObjectProperty<Long> hoveredX = new SimpleObjectProperty<>(0l);

    public ObjectProperty<Long> hoveredX() {
        return hoveredX;
    }

    private final BooleanProperty hoveredOnChart = new SimpleBooleanProperty(false);

    public BooleanProperty hoveredOnChart() {
        return hoveredOnChart;
    }

    @Getter
    private ProfilerTabContentController parentController;

    public MysqlProfilingController() {}

    private final Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
    private final Map<XYChart.Series<String, Number>, String> barSeriesColors = new HashMap<>();

    @FXML
    private AnchorPane mainUI;

    @Getter
    @FXML
    private BorderPane chartContainer;

    @FXML
    private VBox flowContainer;

    @FXML
    private ScrollPane scrollContainer;

    @FXML
    private FlowPane topHorizontalFlatFlowPane;

    private TabularBannerNodeController mysqlUptimeBannerController;
    private TabularBannerNodeController currentQPSBannerController;
    private TabularBannerNodeController innoDBBufferSizeBannerController;
    private TabularBannerNodeController bufferPoolSizeBannerController;

    //private WorkProc allBackgroundTask;
    private ProfilerInnerControllerWorkProcAuth mysqlProfilingControllerWorkProcAuth;

    private Long dragedStart;
    private Long dragedFinish;

    private final List<ProfilerChartController> chartTabs = new ArrayList<>();
    private final List<ProfilerBarChartController> barChartTabs = new ArrayList<>();

    private final Map<String, TitledPane> titledPanes = new HashMap<>();

    private Long firstRunDate;
    private Date startDate;

    private Map<String, GlobalStatusStart> changedGlobalStatus = new HashMap<>();

    @Override
    public void userChartsChanged() {
        updateUserCharts();
    }

    public void openData(ProfilerData data) {

        firstRunDate = (long) data.getLower();

        for (ProfilerChartController t : chartTabs) {
            t.clear();
            if (t.isActive()) {
                t.initXAxis(data.getUpper(), data.getLower());
            }
        }

        currentQPSBannerController.initXAxis(data.getUpper(), data.getLower());

        firstRunDate = null;

        Map<String, List<GSChart>> chartsMap = new HashMap<>();
        Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = initSeries(chartsMap);

        initChartData(chartsMap, chartSeriesMap, data.getListChangedGlobalStatus());
    }

    @Override
    public void zoomInAction() {
        clearDraggedData();
        updateCharts();
    }

    public void scrollForward() {
        Long diff = dragedFinish - dragedStart;
        dragedStart += diff;
        dragedFinish += diff;

        updateCharts();
    }

    public void scrollBackward() {
        Long diff = dragedFinish - dragedStart;
        dragedStart -= diff;
        dragedFinish -= diff;

        updateCharts();
    }

    private void clearDraggedData() {
        dragedStart = null;
        dragedFinish = null;
        getParentController().zoomDragged().set(false);
    }

    private void zoomDragedAction(Long start, Long finish) {

        parentController.setShowAll(false);

        dragedStart = start;
        dragedFinish = finish;
        getParentController().zoomDragged().set(true);

        updateCharts();
    }

    @Override
    public void zoomOutAction() {
        clearDraggedData();
        updateCharts();
    }

    @Override
    public void showAllAction() {
        clearDraggedData();
        updateCharts();
    }

    private void updateCharts() {
        if (chartTabs != null) {
            for (ProfilerChartController chartController : chartTabs) {
                if (chartController.isActive()) {
                    chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                }
            }
        }

        if (barChartTabs != null) {
            for (ProfilerBarChartController chartController : barChartTabs) {
                if (chartController.isActive()) {
                    chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                }
            }
        }

        currentQPSBannerController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
    }

    private Map<Long, Map<String, GlobalStatusStart>> getListChangedGlobalStatus() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void saveProfilerData(ProfilerData pd) {
        pd.setListChangedGlobalStatus(getListChangedGlobalStatus());
        pd.setLower(getLower());
        pd.setUpper(getUpper());
    }

    public void startProfiler(Date startDate) {
        this.startDate = startDate;

        startDate = new Date();

        for (ProfilerChartController chartController : chartTabs) {
            chartController.clear();
        }

        for (ProfilerBarChartController chartController : barChartTabs) {
            chartController.clear();
        }

        currentQPSBannerController.clear();
        firstRunDate = null;

        createAndInitChartThread();
    }

    @Override
    public void clearProfilerData() {

        for (ProfilerChartController chartController : chartTabs) {
            chartController.clear();
        }

        for (ProfilerBarChartController chartController : barChartTabs) {
            chartController.clear();
        }

        currentQPSBannerController.clear();
        firstRunDate = null;
    }

    private Map<Long, Map<String, GlobalStatusStart>> convertToHourly(Map<Long, Map<String, GlobalStatusStart>> map, Map<Long, Map<String, Integer>> hourlyCounts) {
        Map<Long, Map<String, GlobalStatusStart>> result = new LinkedHashMap<>();

        Map<Long, Map<String, Integer>> resultCount = new LinkedHashMap<>();

        int hour = 60 * 60 * 1000;
        for (Long time : map.keySet()) {
            Map<String, GlobalStatusStart> m = map.get(time);
            if (m == ProfilerDataManager.EMPTY_STATUSES) {
                continue;
            }

            Long hourlyTime = (time / hour) * hour;

            Map<String, GlobalStatusStart> hourMap = result.get(hourlyTime);
            Map<String, Integer> mCount = resultCount.get(hourlyTime);
            if (hourMap == null) {
                hourMap = new HashMap<>();
                for (String k: m.keySet()) {
                    GlobalStatusStart gss = m.get(k);
                    hourMap.put(k, gss.copyValue());
                }
                
                result.put(hourlyTime, hourMap);

                mCount = new HashMap<>();
                for (String s : m.keySet()) {
                    mCount.put(s, 1);
                }
                resultCount.put(hourlyTime, mCount);
                continue;
            }

            for (String key : m.keySet()) {
                GlobalStatusStart eis0 = m.get(key);
                GlobalStatusStart eis1 = hourMap.get(key);

                if (eis1 != null) {
                    
                    int count = mCount.get(key);
                    eis1.setValue(eis1.getValue() * count + eis0.getValue());
                    mCount.put(key, count + 1); 
                } else {
                    hourMap.put(key, eis0.copyValue());
                    mCount.put(key, 1);
                }
            }
        }

        for (Long time : result.keySet()) {

            Map<String, Integer> mCount = resultCount.get(time);
            Map<String, GlobalStatusStart> hourMap = result.get(time);

            for (String s : hourMap.keySet()) {
                GlobalStatusStart eis = hourMap.get(s);
                Integer count = mCount.get(s);

                eis.setValue(eis.getValue() / (double) count);

                Map<String, Integer> counts = hourlyCounts.get(s);
                if (counts == null) {
                    counts = new HashMap<>();
                    hourlyCounts.put(time, counts);
                }

                counts.put(eis.getName(), count);
            }
        }

        return result;
    }

    private Map<Long, Map<String, GlobalStatusStart>> readHourlyData(Map<Long, Map<String, GlobalStatusStart>> result, Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> chartSeriesData, Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap, Map<Long, Map<String, Integer>> hourlyCounts) {

        result = convertToHourly(result, hourlyCounts);

        for (Long dateTime : result.keySet()) {
            try {

                // collect Chart data
                synchronized (parentController) {

                    Map<String, GlobalStatusStart> data = result.get(dateTime);

                    //log.error("areaChartThread ********************************");
                    for (String s : data.keySet()) {

                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {

                                XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());

                                if (chartSeries != null) {
                                    List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                    if (listSeriesData == null) {
                                        listSeriesData = new ArrayList<>();
                                        chartSeriesData.put(chartSeries, listSeriesData);
                                    }
                                    listSeriesData.add(new XYChart.Data(ProfilerBarChartController.formatToXValue(dateTime), data.get(s).getValue()));
                                }
                            }
                        }
                    }

                    for (GSChart chart : chartSeriesMap.keySet()) {
                        if (chart.getVariables() != null) {
                            for (GSChartVariable v : chart.getVariables()) {

                                if (v != null && v.getVariable() != null && v.getVariable() == 0) {

                                    String[] vnames = v.getVariableName().split("\\s+");

                                    boolean found = false;
                                    if (vnames.length == 1) {
                                        for (String k : data.keySet()) {
                                            if (k.equalsIgnoreCase(v.getVariableName())) {
                                                found = true;
                                            }
                                        }
                                    }

                                    if (!found) {
                                        XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(v.getVariableName().toUpperCase());
                                        if (chartSeries != null) {
                                            List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                            if (listSeriesData == null) {
                                                listSeriesData = new ArrayList<>();
                                                chartSeriesData.put(chartSeries, listSeriesData);
                                            }

                                            listSeriesData.add(new XYChart.Data(ProfilerBarChartController.formatToXValue(dateTime), 0.0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable ex) {
                log.error(ex.getMessage(), ex);
            }
        }

        return result;
    }

    public void showProfilerData(Date dateFrom) {

        firstRunDate = null;

        makeBusy(true);
        Thread th = new Thread(() -> {

            Map<String, List<GSChart>> chartsMap = new HashMap<>();
            Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = initSeries(chartsMap);

            Map<Long, Map<String, GlobalStatusStart>> gsData = dbProfilerService.selectGSDistinction(serverHashCode(getConnectionParams(), ""), dateFrom, ProfilerDataManager.EMPTY_DELAY__GS, null);

            Platform.runLater(() -> {

//                for (XYChart.Series<Number, Number> series: chartSeriesData.keySet()) {
//                    series.getData().addAll(chartSeriesData.get(series));
//                }
                initChartData(chartsMap, chartSeriesMap, gsData);

                for (ProfilerChartController chartController : chartTabs) {
                    if (chartController.isActive()) {
                        chartController.clear();
                        chartController.init(chartSeriesMap.get(chartController.getChart()).values(), seriesColors);
                        chartController.initXAxis(new Date().getTime(), dateFrom.getTime());
                        chartController.updateChart(true, parentController.zoomCountStage(), dateFrom.getTime(), dragedStart, dragedFinish);
                    }
                }

                currentQPSBannerController.clear();
                List<Data<Number, Number>> data = initTabularChart(gsData);
                currentQPSBannerController.initChart(data);
                currentQPSBannerController.initXAxis(new Date().getTime(), dateFrom.getTime());
                currentQPSBannerController.updateChart(true, parentController.zoomCountStage(), dateFrom.getTime(), dragedStart, dragedFinish);

                makeBusy(false);
            });
        });
        th.setDaemon(true);
        th.start();
    }

    private List<Data<Number, Number>> initTabularChart(Map<Long, Map<String, GlobalStatusStart>> result) {
        currentQPSBannerController.getChartData().clear();

        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        currentQPSBannerController.getChartData().add(series);

        List<Data<Number, Number>> data = new ArrayList<>();
        GlobalStatusStart prev = null;
        Long lastTime = null;
        boolean emptyAdded = true;
        for (Long time : result.keySet()) {

            Map<String, GlobalStatusStart> m = result.get(time);
            if (m != null && m.containsKey(STATUS__QUERIES) && m != ProfilerDataManager.EMPTY_STATUSES) {

                if (lastTime != null && time - lastTime > ProfilerDataManager.EMPTY_DELAY__GS) {
                    emptyAdded = false;
                }

                GlobalStatusStart current = m.get(STATUS__QUERIES);

                if (!emptyAdded) {
                    data.add(new Data<>(lastTime + 1000, 0));
                } else if (prev != null) {
                    int queriesPerSec = (int) (current.getValue() - prev.getValue()) / (int) TimeUnit.SECONDS.toSeconds(1);
                    data.add(new Data<>(time, queriesPerSec));
                }

                if (!emptyAdded) {
                    data.add(new Data<>(time - 1000, 0));
                }

                if (!emptyAdded) {
                    emptyAdded = true;
                }

                lastTime = time;
                prev = current;
            }
        }

        return data;
    }

    private Map<GSChart, Map<String, XYChart.Series<String, Number>>> initBarSeries(Map<String, List<GSChart>> chartsMap) {
        barSeriesColors.clear();
        Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap = new HashMap<>();

        List<GSChart> charts = chartService.getAll("Profiling");
        for (GSChart chart : charts) {

            if (chart.isStatus() && chart.getHourly() == 1) {
                Map<String, XYChart.Series<String, Number>> map = new LinkedHashMap<>();
                if (chart.getVariables() != null) {
                    for (GSChartVariable v : chart.getVariables()) {
                        String value = v.getVariableName().toUpperCase();
                        //log.error(" variable: " + value);
                        List<GSChart> list = chartsMap.get(value);
                        if (list == null) {
                            list = new ArrayList<>();
                            chartsMap.put(value, list);
                        }
                        list.add(chart);

                        final XYChart.Series series = new XYChart.Series();
                        series.setName(v.getDisplayName());

                        if (!"Random".equals(v.getDisplayColor())) {
                            barSeriesColors.put(series, v.getDisplayColor());
                        }

                        map.put(value, series);
                    }
                }

                chartSeriesMap.put(chart, map);
            }
        }

        return chartSeriesMap;
    }

    @Override
    public void stopProfiler() {
        startDate = null;
        dbProfilerService.updateLatestGSStartTable(serverHashCode(getConnectionParams(), ""), true);
    }

    @Override
    public void destroy() {
        stopProfiler();
        keepThreadsAlive = false;

        changedGlobalStatus.clear();

        chartService = null;
        chartTabs.clear();
        titledPanes.clear();
    }

    public void createConnection() throws SQLException {
        // clear old data
        dbProfilerService.clearOldGSData(getParentController().getCurrentSettings());

        createChartThread();
        reinitChartTabs();
    }

    public void updateUserCharts() {

        usersChartsWasChanged = true;
        createChartThread();
        reinitChartTabs();
    }

    public ConnectionParams getConnectionParams() {
        return getParentController().getParams();
    }

    private volatile boolean keepThreadsAlive = true;
    private volatile boolean usersChartsWasChanged = false;

    private void initChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap, Map<Long, Map<String, GlobalStatusStart>> data) {
        try {
            //List<XYChart.Series<Number, Number>> seriesList = new ArrayList<>();
            //Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> map = new HashMap<>();

            Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();

            // collect Chart data
            synchronized (parentController) {

                Long lastDateTime = null;
                for (Long time : data.keySet()) {

                    Date date = new Date(time);
                    if (firstRunDate == null) {
                        firstRunDate = date.getTime();
                    }

                    Map<String, GlobalStatusStart> map = data.get(time);

                    boolean needEmpty = lastDateTime != null && time - lastDateTime > ProfilerDataManager.EMPTY_DELAY__GS;

                    //log.error("areaChartThread ********************************");
                    for (String s : map.keySet()) {

                        GlobalStatusStart gs = map.get(s);
                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {
                                if (chart.getHourly() != 1) {
                                    XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }

                                        GSChartVariable v = chart.getVariableByName(gs.getName());

                                        listSeriesData.add(new XYChart.Data(date.getTime(), v.getType() == 1 ? gs.getDifferece() : gs.getValue()));
                                    }
                                }
                            }
                        }
                    }

                    for (GSChart chart : chartSeriesMap.keySet()) {
                        if (chart.getVariables() != null) {
                            for (GSChartVariable v : chart.getVariables()) {
                                if (v.getVariable() != null && v.getVariable() == 1) {

                                    XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(v.getVariableName().toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }

                                        BigDecimal value = BigDecimal.ZERO;
                                        Double val = getParentController().getDoubleGlobalVariable(v.getVariableName().toUpperCase());
                                        if (val != null) {
                                            value = new BigDecimal(val);
                                        }

                                        if (needEmpty) {
                                            listSeriesData.add(new XYChart.Data(lastDateTime + 1000, BigDecimal.ZERO));
                                            listSeriesData.add(new XYChart.Data(time - 1000, BigDecimal.ZERO));
                                        }

                                        listSeriesData.add(new XYChart.Data(date.getTime(), value != null ? value : BigDecimal.ZERO));
                                    }
                                }
                            }
                        }
                    }

                    lastDateTime = time;
                }
            }

            //log.error("areaChartThread ===============================");
            Platform.runLater(() -> {

                for (XYChart.Series<Number, Number> series : chartSeriesData.keySet()) {
                    series.getData().addAll(chartSeriesData.get(series));
                }

                for (ProfilerChartController chartController : chartTabs) {
                    if (chartController.isActive()) {
                        chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                    }
                }

                currentQPSBannerController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
            });

        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }

    private void initBarChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap, Map<Long, Map<String, GlobalStatusStart>> data, Map<Long, Map<String, Integer>> hourlyCounts) {
        try {

            Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> chartSeriesData = new HashMap<>();

            // collect Chart data
            synchronized (parentController) {

                long hour = 60 * 60 * 1000;
                for (Long time : data.keySet()) {

                    Date date = new Date(time);
                    if (firstRunDate == null) {
                        firstRunDate = date.getTime();
                    }

                    Map<String, GlobalStatusStart> map = data.get(time);

                    //log.error("areaChartThread ********************************");
                    for (String s : map.keySet()) {

                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {
                                if (chart.getHourly() == 1) {
                                    XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }
                                        String xValue = ProfilerBarChartController.formatToXValue((long) (date.getTime() / hour) * hour);
                                        listSeriesData.add(new XYChart.Data(xValue, map.get(s).getValue()));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //log.error("areaChartThread ===============================");
            Platform.runLater(() -> {

                for (XYChart.Series<String, Number> series : chartSeriesData.keySet()) {

                    String propertyKey = null;
                    for (Map<String, XYChart.Series<String, Number>> ss : chartSeriesMap.values()) {

                        for (String s : ss.keySet()) {
                            if (series.equals(ss.get(s))) {
                                propertyKey = s;
                                break;
                            }

                            if (propertyKey != null) {
                                break;
                            }
                        }
                    }

                    for (Data<String, Number> d : chartSeriesData.get(series)) {

                        Map<String, Integer> counts = hourlyCounts.get(d.getXValue());
                        if (counts == null) {
                            counts = new HashMap<>();
                            hourlyCounts.put(ProfilerBarChartController.formatFromXValue(d.getXValue()), counts);
                        }

                        boolean found = false;
                        for (Data<String, Number> d0 : series.getData()) {

                            if (d0.getXValue().equals(d.getXValue())) {

                                if (propertyKey != null) {
                                    Integer v = counts.get(propertyKey);
                                    if (v == null) {
                                        v = 0;
                                    }

                                    counts.put(propertyKey, v + 1);

                                    double value = ((d0.getYValue().doubleValue() * v) + d.getYValue().doubleValue()) / (double) (v + 1);
                                    d0.setYValue(value);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found) {
                            counts.put(propertyKey, 1);
                            series.getData().add(d);
                        }
                    }
                }

                for (ProfilerBarChartController chartController : barChartTabs) {
                    if (chartController.isActive()) {
                        chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                    }
                }
            });

        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }

    private Map<GSChart, Map<String, XYChart.Series<Number, Number>>> initSeries(Map<String, List<GSChart>> chartsMap) {
        seriesColors.clear();
        Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = new HashMap<>();

        List<GSChart> charts = chartService.getAll("Profiling");
        for (GSChart chart : charts) {

            if (chart.isStatus()) {
                Map<String, XYChart.Series<Number, Number>> map = new LinkedHashMap<>();
                if (chart.getVariables() != null) {
                    for (GSChartVariable v : chart.getVariables()) {
                        String value = v.getVariableName().toUpperCase();
                        //log.error(" variable: " + value);
                        List<GSChart> list = chartsMap.get(value);
                        if (list == null) {
                            list = new ArrayList<>();
                            chartsMap.put(value, list);
                        }
                        list.add(chart);

                        final XYChart.Series series = new XYChart.Series();
                        series.setName(v.getDisplayName());

                        if (!"Random".equals(v.getDisplayColor())) {
                            seriesColors.put(series, v.getDisplayColor());
                        }

                        map.put(value, series);
                    }
                }

                chartSeriesMap.put(chart, map);
            }
        }

        return chartSeriesMap;
    }

    private void reinitChartTabs() {
        Platform.runLater(() -> {
            for (ProfilerChartController chartController : chartTabs) {
                chartController.clear();
            }

            for (ProfilerBarChartController chartController : barChartTabs) {
                chartController.clear();
            }

            currentQPSBannerController.clear();

            firstRunDate = null;
            //chartsTabPane.getTabs().clear();
            chartTabs.clear();
            barChartTabs.clear();
            titledPanes.clear();

            List<GSChart> charts = chartService.getAll("Profiling");
            VBox lastVbox = null;
            for (GSChart chart : charts) {
                if (chart.isStatus()) {

                    HBox box = null;
                    VBox vbox = null;

                    if (chart.getGroupName() != null && !chart.getGroupName().isEmpty()) {
                        TitledPane tp = titledPanes.get(chart.getGroupName());
                        if (tp == null) {
                            tp = new TitledPane();
                            tp.setFocusTraversable(false);
                            tp.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            tp.setText(chart.getGroupName());

                            VBox vx = new VBox(5);
                            vx.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vx.setPadding(new Insets(-3.0));
                            vx.setAlignment(Pos.CENTER);
                            tp.setContent(vx);

                            flowContainer.getChildren().add(0, tp);

                            titledPanes.put(chart.getGroupName(), tp);
                        }

                        vbox = (VBox) tp.getContent();

                    } else if (lastVbox == null) {
                        vbox = new VBox(5);
                        lastVbox = vbox;
                        flowContainer.getChildren().add(0, vbox);
                    } else {
                        vbox = lastVbox;
                    }

                    if (vbox.getChildren().isEmpty()) {
                        box = new HBox(5);
                        box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                        vbox.getChildren().add(0, box);
                    } else {

                        HBox oldBox = (HBox) vbox.getChildren().get(0);

                        if (chart.getGroupWidth() == 2) {
                            box = new HBox(5);
                            box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vbox.getChildren().add(0, box);
                        } else if (oldBox.getChildren().size() == 2 || ((GSChart) oldBox.getUserData()).getGroupWidth() == 2) {
                            box = new HBox(5);
                            box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vbox.getChildren().add(0, box);
                        }

                        if (box == null) {
                            box = oldBox;
                        } else if (oldBox.getChildren().size() == 1 && ((GSChart) oldBox.getUserData()).getGroupWidth() == 1) {
                            Label empty = new Label();
                            empty.setMaxWidth(Double.MAX_VALUE);
                            oldBox.getChildren().add(empty);
                            HBox.setHgrow(empty, Priority.ALWAYS);
                        }
                    }

                    box.setAlignment(Pos.CENTER);
                    box.setUserData(chart);

                    if (chart.getHourly() == 1) {
                        ProfilerBarChartController pcc = loadProfilerBarChartEntityMetricControllers(chart);
                        box.getChildren().add(0, pcc.getUI());

                        pcc.getUI().setMaxWidth(Double.MAX_VALUE);
                        HBox.setHgrow(pcc.getUI(), Priority.ALWAYS);

                        barChartTabs.add(pcc);
                    } else {
                        ProfilerChartController pcc = loadProfilerChartEntityMetricControllers(chart);
                        box.getChildren().add(0, pcc.getUI());

                        pcc.getUI().setMaxWidth(Double.MAX_VALUE);
                        HBox.setHgrow(pcc.getUI(), Priority.ALWAYS);

                        chartTabs.add(pcc);
                    }
                }
            }
            Platform.runLater(() -> usersChartsWasChanged = false);
        });
    }

    private void createAndInitChartThread() {
        createChartThread();
        // THIS NOT WORK WITH THIS BACKGROUND THREADS
        // SOME TIMES IT JUST IGNORING WORKPROC, 
        // ALSO WE HAVE ONLY 3 THREAD - THIS IS NOT ENOUGH
        //Recycler.getLessBusySWP().addWorkProc(allBackgroundTask);
//        
//        Thread th = new Thread(() -> {
//            allBackgroundTask.run();
//        }, "MYSqlProfiling");
//        th.setDaemon(true);
//        th.start();
    }

    private void createChartThread() {

        // generate server hash code
        final String serverHashCode = serverHashCode(getConnectionParams(), "");
        mysqlProfilingControllerWorkProcAuth = new ProfilerInnerControllerWorkProcAuth() {

            long lastTimeGlobalVariablesRunned = 0l;
            long lastOverallCodeExecutionRun = 0l;
            long lastUpdateExecutionRun = 0l;
            Map<Long, Map<String, GlobalStatusStart>> changedCache;
            Map<String, List<GSChart>> chartsMap;
            Map<String, List<GSChart>> barChartsMap;
            Map<Long, Map<String, Integer>> hourlyCounts;
            Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap;
            boolean first = true;
            Date currentDate;
            Date readDate;
//                    Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();
            Map<Long, Map<String, GlobalStatusStart>> gsData;
//                    Map<Long, Map<String, GlobalStatusStart>> result = readData(gsData, chartSeriesData, chartsMap, chartSeriesMap);
            Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> barChartSeriesData;
            Map<GSChart, Map<String, XYChart.Series<String, Number>>> barChartSeriesMap;
//                    Map<Long, Map<String, GlobalStatusStart>> barResult = readHourlyData(gsData, barChartSeriesData, barChartsMap, barChartSeriesMap, hourlyCounts);
            Map<Long, Map<String, GlobalStatusStart>> barResult;
            Map<String, GSChartVariable> variablesNames;
            Double oldQueries;
            Long lastDateTime;

            @Override
            public boolean shouldExecuteWorkProc() {
                return parentController.isDataLoaded() && getParentController().profilerStarted().get() && keepThreadsAlive && !usersChartsWasChanged
                        && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastOverallCodeExecutionRun) >= 1;
            }

            @Override
            public void runWork() {

                if (chartSeriesMap == null) {

                    firstRunDate = null;
                    currentDate = new Date();
                    changedCache = new LinkedHashMap<>();
                    chartsMap = new HashMap<>();
                    barChartsMap = new HashMap<>();
                    hourlyCounts = new HashMap<>();
                    chartSeriesMap = initSeries(chartsMap);
                    readDate = new Date(currentDate.getTime() - ChartScale.SCALE_8H.getValue());
//                    Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();
                    gsData = dbProfilerService.selectGSDistinction(serverHashCode(getConnectionParams(), ""), readDate, ProfilerDataManager.EMPTY_DELAY__GS, currentDate);

//                    Map<Long, Map<String, GlobalStatusStart>> result = readData(gsData, chartSeriesData, chartsMap, chartSeriesMap);
                    barChartSeriesData = new HashMap<>();
                    barChartSeriesMap = initBarSeries(barChartsMap);

//                    Map<Long, Map<String, GlobalStatusStart>> barResult = readHourlyData(gsData, barChartSeriesData, barChartsMap, barChartSeriesMap, hourlyCounts);
                    barResult = convertToHourly(gsData, hourlyCounts);

                    variablesNames = chartService.getVariablesNames("Profiling");
                    oldQueries = null;
                    lastDateTime = null;

                    // date of execution qeury
                    currentDate = new Date();

                    dbProfilerService.clearGlobalStatusStartTable(serverHashCode);
                    Platform.runLater(() -> {
//                        for (XYChart.Series<Number, Number> series: chartSeriesData.keySet()) {
//                            series.getData().addAll(chartSeriesData.get(series));
//                        }

                        for (ProfilerChartController chartController : chartTabs) {
                            if (chartController.isActive()) {
                                chartController.clear();
                            }
                        }

                        currentQPSBannerController.clear();

                        List<Data<Number, Number>> data = initTabularChart(gsData);
                        currentQPSBannerController.initChart(data);

                        initChartData(chartsMap, chartSeriesMap, gsData);

                        initBarChartData(barChartsMap, barChartSeriesMap, barResult, hourlyCounts);
                    });

                }

                synchronized (parentController) {

                    if (getParentController().hasGlobalStatus("UPTIME") && getParentController().hasGlobalStatus("QUERIES") && getParentController().hasGlobalVariable("INNODB_BUFFER_POOL_SIZE")) {
                        if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastTimeGlobalVariablesRunned) >= 10) {
                            lastTimeGlobalVariablesRunned = System.currentTimeMillis();

                            Double uptime = getParentController().getDoubleGlobalStatus("UPTIME", true);
                            if (uptime == null) {
                                uptime = 0.0;
                            }

                            int uptimeInDays = (int) (uptime / 86400);
                            if (uptimeInDays >= 1) {
                                Platform.runLater(() -> {
                                    mysqlUptimeBannerController.getContentTextProperty().set(String.valueOf(uptimeInDays));
                                    mysqlUptimeBannerController.getContentTextProperty1().set(" DAY");
                                    mysqlUptimeBannerController.getContentTextProperty2().set(uptimeInDays > 1 ? "S" : "");
                                });
                            } else {
                                String uptimeInDur = String.valueOf(TimeUnit.SECONDS.toHours(uptime.intValue()));
                                Platform.runLater(() -> {
                                    mysqlUptimeBannerController.getContentTextProperty().set(uptimeInDur);
                                    mysqlUptimeBannerController.getContentTextProperty1().set(" HOUR(S)");
                                });
                            }

                            Double queries = getParentController().getDoubleGlobalStatus("QUERIES", true);
                            if (queries == null) {
                                queries = 0.0;
                            }

                            if (oldQueries != null) {

                                int queriesPerSec = (queries.intValue() - oldQueries.intValue()) / (int) TimeUnit.SECONDS.toSeconds(30);
                                Platform.runLater(() -> {
                                    currentQPSBannerController.getContentTextProperty().set(String.valueOf(queriesPerSec));
                                });

                                currentQPSBannerController.getChartData().get(0).getData().add(new XYChart.Data(currentDate.getTime(), queriesPerSec));
                            }

                            oldQueries = queries;

                            Double ibps = getParentController().getDoubleGlobalVariable("INNODB_BUFFER_POOL_SIZE");
                            if (ibps == null) {
                                ibps = 0.0;
                            }
                            long bufferSize = Metric.convertToUnitBytes(ibps.longValue(), Flags.MEGABYTE);
                            Platform.runLater(() -> {
                                innoDBBufferSizeBannerController.getContentTextProperty().set(String.valueOf(bufferSize) + " MB");
                            });

                            Long totalMemory = 0l;
                            String address = parentController.getConnectionParams().getAddress();
                            if ("localhost".equals(address) || "127.0.0.1".equals(address)) {
                                totalMemory = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize();

                                //} else if (parentController.getMemoryMetrics() != null) {
                                //    totalMemory = parentController.getMemoryMetrics().getTotalMemory();
                            } else {
                                try {
                                    totalMemory = new BigDecimal(parentController.getParams().getOsMetricRamSize()).multiply(new BigDecimal(1024 * 1024 * 1024)).longValue();
                                } catch (Throwable th) {
                                }
                            }

                            if (totalMemory > 0) {

                                BigDecimal b = new BigDecimal(ibps * 100).divide(new BigDecimal(totalMemory), 0, RoundingMode.HALF_UP);
                                Platform.runLater(() -> {
                                    //if it is more then 60% then need to keep in green color , 50 to 60 light green , 30 to 50 white else red color 
                                    bufferPoolSizeBannerController.getContentTextProperty().set(b.toString() + "%");
                                    if (b.intValue() >= 60) {
                                        bufferPoolSizeBannerController.getContentTextLabel().setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20;");
                                    } else if (b.intValue() >= 50) {
                                        bufferPoolSizeBannerController.getContentTextLabel().setStyle("-fx-text-fill: lightgreen; -fx-font-weight: bold; -fx-font-size: 20;");
                                    } else if (b.intValue() >= 30) {
                                        bufferPoolSizeBannerController.getContentTextLabel().setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
                                    } else {
                                        bufferPoolSizeBannerController.getContentTextLabel().setStyle("-fx-text-fill: red; -fx-font-weight: bold; -fx-font-size: 20;");
                                    }
                                });
                            }
                        }
                    }

                    // prepare table for global status values
                    dbProfilerService.updateStartTimeGSStartTable(serverHashCode, currentDate);

                    Map<Long, Map<String, GlobalStatusStart>> lastGS = dbProfilerService.selectLastGS(serverHashCode, startDate);

                    Long maxTime = startDate.getTime();
                    for (Long time : lastGS.keySet()) {

                        if (time > maxTime) {
                            maxTime = time;
                        }
                        changedGlobalStatus.clear();

                        Map<String, GlobalStatusStart> map = lastGS.get(time);
                        for (String str : map.keySet()) {
//
                            GlobalStatusStart gss = map.get(str);
//                                        GSChartVariable v = variablesNames.get(str);
//                                        if (v != null && v.getType() == 0)  {
//                                            gss.setDifferece(gss.getValue());
//                                        }
                            changedGlobalStatus.put(str, gss);
                        }

                        // need add with 0 all not found
                        for (String key : variablesNames.keySet()) {

                            GSChartVariable v = variablesNames.get(key);
                            if (v != null && v.getVariable() != null && v.getVariable() == 0) {

                                String[] names = v.getVariableName().split("\\s+");

                                boolean found = false;
                                if (names.length == 1) {
                                    for (String k : changedGlobalStatus.keySet()) {
                                        if (k.equalsIgnoreCase(key)) {
                                            found = true;
                                        }
                                    }
                                }

                                if (!found) {
                                    if (names.length >= 2) {
                                        String function = names[0].trim();
                                        String name = names[1].trim().toUpperCase();

                                        double value = 0l;
                                        double diff = 0l;
                                        if ("AVG".equalsIgnoreCase(function)) {

                                            List<GSChart> gsc = chartsMap.get(name);
                                            if (gsc != null && !gsc.isEmpty()) {

                                                Map<String, XYChart.Series<Number, Number>> ss = chartSeriesMap.get(gsc.get(0));
                                                if (ss != null && ss.containsKey(name)) {

                                                    long avg = 0l;
                                                    List<Data<Number, Number>> list = ss.get(name).getData();
                                                    int size = list.size();

                                                    for (Data<Number, Number> data : list) {
                                                        avg += data.getYValue().doubleValue();
                                                    }

                                                    long prevValue = size > 0 ? avg / size : 0l;

                                                    if (changedGlobalStatus.containsKey(name)) {
                                                        avg += changedGlobalStatus.get(name).getValue();
                                                        size++;
                                                    } else if (getParentController().containsGlobalVariable(name)) {
                                                        avg += getParentController().getDoubleGlobalVariable(name);
                                                        size++;
                                                    }

                                                    value = size > 0 ? avg / size : 0;
                                                    diff = value - prevValue;
                                                }
                                            }
                                        } else if ("DIVIDE".equalsIgnoreCase(function) && names.length >= 3) {

                                            String divider = names[2].toUpperCase();

                                            double prevValue = 0l;
                                            double dividerValue = 0l;
                                            double currentValue = 0l;
                                            GlobalStatusStart old = changedGlobalStatus.get(key);
                                            if (old != null) {
                                                prevValue = old.getValue();
                                            }

                                            if (changedGlobalStatus.containsKey(name)) {
                                                currentValue = changedGlobalStatus.get(name).getValue();
                                            } else if (getParentController().containsGlobalVariable(name)) {
                                                currentValue = getParentController().getDoubleGlobalVariable(name);
                                            }

                                            if (changedGlobalStatus.containsKey(divider)) {
                                                dividerValue = changedGlobalStatus.get(divider).getValue();
                                            } else if (getParentController().containsGlobalVariable(divider)) {
                                                dividerValue = getParentController().getDoubleGlobalVariable(divider);
                                            }

                                            if (dividerValue > 0) {
                                                value = currentValue / dividerValue;

                                                if (names.length == 4) {
                                                    try {
                                                        value *= Long.valueOf(names[3]);
                                                    } catch (NumberFormatException ex) {
                                                    }
                                                }

                                                diff = value - prevValue;
                                            }
                                        }

                                        changedGlobalStatus.put(key, new GlobalStatusStart(key, value, v != null && v.getType() == 1 ? diff : value));
                                    } else {
                                        changedGlobalStatus.put(key, new GlobalStatusStart(key, 0.0, 0.0));
                                    }
                                }
                            }
                        }

                        Map<String, GlobalStatusStart> m = new HashMap<>();
                        for (Map.Entry<String, GlobalStatusStart> entry : changedGlobalStatus.entrySet()) {
                            m.put(entry.getKey(), entry.getValue());
                        }

                        if (lastDateTime == null) {
                            changedCache.put(time - 1000, ProfilerDataManager.EMPTY_STATUSES);

                        } else if (time - lastDateTime > ProfilerDataManager.EMPTY_DELAY__GS) {

                            changedCache.put(lastDateTime + 1000, ProfilerDataManager.EMPTY_STATUSES);
                            changedCache.put(time - 1000, ProfilerDataManager.EMPTY_STATUSES);
                        }
                        changedCache.put(time, m);

                        lastDateTime = time;
                    }

                    startDate = new Date(maxTime);
                }

                if (first) {
                    first = false;
                    // add serieas to each chart
                    Platform.runLater(() -> {
                        //log.error("start init");
                        for (ProfilerChartController chartController : chartTabs) {
                            if (chartController.isActive() && chartSeriesMap.get(chartController.getChart()) != null) {
                                chartController.init(chartSeriesMap.get(chartController.getChart()).values(), seriesColors);
                            }
                        }

                        for (ProfilerBarChartController chartController : barChartTabs) {
                            if (chartController.isActive() && barChartSeriesMap.get(chartController.getChart()) != null) {
                                chartController.init(barChartSeriesMap.get(chartController.getChart()).values(), barSeriesColors);
                            }
                        }
                    });
                }

                lastOverallCodeExecutionRun = System.currentTimeMillis();

                if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastUpdateExecutionRun) >= getParentController().getCurrentSettings().getDataRefresh()) {
                    initChartData(chartsMap, chartSeriesMap, changedCache);
                    initBarChartData(barChartsMap, barChartSeriesMap, changedCache, hourlyCounts);
                    changedCache.clear();
                    lastUpdateExecutionRun = System.currentTimeMillis();
                }

            }
        };
        usersChartsWasChanged = false;
    }

    @Override
    public void exportToXls(
            List<String> sheets,
            List<ObservableList<ObservableList>> totalData,
            List<LinkedHashMap<String, Boolean>> totalColumns,
            List<List<Integer>> totalColumnWidths,
            List<byte[]> images,
            List<Integer> heights,
            List<Integer> colspans) {
    }

    public double getUpper() {
        return chartTabs.get(0).getUpper();
    }

    public double getLower() {
        return chartTabs.get(0).getLower();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public void setParentController(ProfilerTabContentController parentController) {
        this.parentController = parentController;

        createAllEntities();

        mysqlUptimeBannerController.getTitleTextProperty().set("MYSQL SERVER UPTIME");
        mysqlUptimeBannerController.getContentTextProperty().set("---");
        mysqlUptimeBannerController.getContentTextLabel().setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20;");
        mysqlUptimeBannerController.getContentTextLabel1().setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20;");

        currentQPSBannerController.getTitleTextProperty().set("CURRENT QPS");
        currentQPSBannerController.getContentTextProperty().set("---");

        innoDBBufferSizeBannerController.getTitleTextProperty().set("INNO BUFFER POOL SIZE");
        innoDBBufferSizeBannerController.getContentTextProperty().set("-- GB");

        bufferPoolSizeBannerController.getTitleTextProperty().set("BUFFER POOL SIZE OF TOTAL RAM");
        bufferPoolSizeBannerController.getContentTextProperty().set("---");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private ProfilerBarChartController loadProfilerBarChartEntityMetricControllers(GSChart chart) {

        ProfilerBarChartController pcc = loadEntityMetricControllers(ProfilerBarChartController.class);
        pcc.insertGSChart(chart);
        pcc.setChartControllerEventHandler(new ProfilerChartControllerEventHandler() {
            @Override
            public void zoomDragedAction(Long start, Long finish) {
                MysqlProfilingController.this.zoomDragedAction(start, finish);
            }
        });
        pcc.currentScaleIndex().bind(parentController.currentScaleIndex());
//        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
//        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }

    private ProfilerChartController loadProfilerChartEntityMetricControllers(GSChart chart) {
        ProfilerChartController pcc = loadEntityMetricControllers(ProfilerChartController.class);
        pcc.insertGSChart(chart);
        pcc.setChartControllerEventHandler(new ProfilerChartControllerEventHandler() {
            @Override
            public void zoomDragedAction(Long start, Long finish) {
                MysqlProfilingController.this.zoomDragedAction(start, finish);
            }
        });
        pcc.currentScaleIndex().bind(parentController.currentScaleIndex());
        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }
    
    private <T extends MetricEntityContainerController> T loadEntityMetricControllers(Class<T> meccClass) {
        try {
            T t = StandardBaseControllerProvider.getController(meccClass, getController(), "MetricEntityContainer");
            t.activateMetric();
            return t;
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private np.com.ngopal.smart.sql.structure.controller.TabularBannerNodeController loadEntityTabularBannerControllers(String description) {
        try {
            np.com.ngopal.smart.sql.structure.controller.TabularBannerNodeController controller = StandardBaseControllerProvider.getController("TabularBannerNode");
            controller.init(description, getParentController());
            return controller;
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void createAllEntities() {

        mysqlUptimeBannerController = loadEntityTabularBannerControllers(
                "    The amount of time since the last restart of\n"
                + "the MySQL server process.");

        currentQPSBannerController = loadEntityTabularBannerControllers(
                "    Based on the queries reported by MySQL's SHOW STATUS command,\n"
                + "it is the number of statements executed by the server within\n"
                + "the last second. This variable includes statements executed\n"
                + "within stored programs, unlike the Questions variable. It\n"
                + "does not count COM_PING or COM_STATISTICS commands.");

        bufferPoolSizeBannerController = loadEntityTabularBannerControllers(
                "    InnoDB maintains a storage area called the buffer pool for\n"
                + "caching data and indexes in memory. Knowing how the InnoDB\n"
                + "buffer pool works, and taking advantage of it to keep\n"
                + "frequently accessed data in memory, is one of the most\n"
                + "important aspects of MySQL tuning. The goal is to keep the\n"
                + "working set in memory. In most cases, this should be between\n"
                + "60%-90% of available memory on a dedicated database host,\n"
                + "but depends on many factors.");

        innoDBBufferSizeBannerController = loadEntityTabularBannerControllers(
                "    InnoDB maintains a storage area called the buffer pool for\n"
                + "caching data and indexes in memory. Knowing how the InnoDB\n"
                + "buffer pool works, and taking advantage of it to keep\n"
                + "frequently accessed data in memory, is one of the most\n"
                + "important aspects of MySQL tuning. The goal is to keep the\n"
                + "working set in memory. In most cases, this should be between\n"
                + "60%-90% of available memory on a dedicated database host,\n"
                + "but depends on many factors.");

        if (getUI().getScene() == null) {
            getUI().sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
                if (newValue != null && oldValue == null) {
                    applyNewUiModifications();
                }
            });
        } else {
            applyNewUiModifications();
        }
    }

    private void applyNewUiModifications() {
        scrollContainer.getStyleClass().add(Flags.METRIC_STYLE_CLASS);

        topHorizontalFlatFlowPane.getStyleClass().add(Flags.METRIC_STYLE_CLASS);

        Utitlities.prepFlowPaneDimensionConditionWithParent(topHorizontalFlatFlowPane, 4.0, false);
        topHorizontalFlatFlowPane.getChildren().addAll(mysqlUptimeBannerController.getUI(), innoDBBufferSizeBannerController.getUI(),
                bufferPoolSizeBannerController.getUI(), currentQPSBannerController.getUI());
    }

    public void finalizeConnectionCreation() {
        getUI().requestLayout();
    }

    @Override
    public ProfilerInnerControllerWorkProcAuth getInnerControllerWorkProcAuthority() {
        return mysqlProfilingControllerWorkProcAuth;
    }

    public void setDbProfilerService(ProfilerDataManager dbProfilerService) {
        this.dbProfilerService = dbProfilerService;
    }

    public void setChartService(ProfilerChartService chartService) {
        this.chartService = chartService;
    }

}
