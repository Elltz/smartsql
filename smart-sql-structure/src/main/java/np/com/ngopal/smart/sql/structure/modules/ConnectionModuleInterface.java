package np.com.ngopal.smart.sql.structure.modules;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public interface ConnectionModuleInterface {
    
    void connectionOpened(ConnectionParams params);
    void connectToConnection(ConnectionParams cp);
    void connectionFailedOpening(String error);
    boolean isNeedOpenInNewTab();
}
