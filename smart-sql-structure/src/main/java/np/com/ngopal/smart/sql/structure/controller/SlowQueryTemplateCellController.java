package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.structure.queryutils.RSyntaxTextAreaBuilder;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import tools.java.pats.models.SqlFormatter;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SlowQueryTemplateCellController extends BaseController implements Initializable {
    
    @Setter(AccessLevel.PUBLIC)
    private PreferenceDataService preferenceService;
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private BorderPane textAreaPane;
    
    private RSyntaxTextArea textArea;
    
    @FXML
    private Label sizeLabel;   

    
    private String data = null;
    
    private RSyntaxTextAreaBuilder builder;
    
    
    @FXML
    void copyAction(ActionEvent event) {
        
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();

        content.putString(textArea.getText());
        clipboard.setContent(content);
    }
    
    @FXML
    void formatAction(ActionEvent event) {
        
        SqlFormatter formatter = new SqlFormatter();
            
        List<PreferenceData> list = preferenceService.getAll();
        if (!list.isEmpty()) {
            textArea.setText(formatter.formatSql(textArea.getText(), list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle()));
        } else {
            textArea.setText(formatter.formatSql(textArea.getText(), "", "2", "block"));
        }
        
        //((MainController)getController()).usedQueryFormat();
        
        textArea.setCaretPosition(0);
    }
    
    @FXML
    void cancelAction(ActionEvent event) {
        data = null;
        ((Stage)mainUI.getScene().getWindow()).close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), getController().getSelectedConnectionSession().get());
        SwingUtilities.invokeLater(() -> {
            builder.init();
            textArea = builder.getTextArea();

            Platform.runLater(() -> {
                textAreaPane.setCenter(builder.getSwingNode());
            });

            textArea.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    updateSize();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    updateSize();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    updateSize();
                }

                private void updateSize() {
                    Platform.runLater(() -> {
                        try {
                            sizeLabel.setText("Size: " + new DecimalFormat("#,###").format(textArea.getText() != null ? textArea.getText().getBytes("UTF-8").length : 0) + " bytes");
                        } catch (Throwable th) {}
                    });                
                }
            });
        });
    }
    
    public void init(String data, boolean editable) {
        SwingUtilities.invokeLater(() -> {
            textArea.setEditable(editable);

            if (!"(NULL)".equals(data)) {

                //SwingUtilities.invokeLater(() -> {
                    textArea.setText(data);
                    textArea.setCaretPosition(0);
                //});
            }
            PreferenceData pd = preferenceService.getPreference();
            builder.changeFont(pd.getFontsBlobViewer());
            builder.changeTabSettings(pd.getTabsSize(), pd.isTabsInsertSpaces());
        });
    }
    
    public String getData() {
        return data;
    }
}
