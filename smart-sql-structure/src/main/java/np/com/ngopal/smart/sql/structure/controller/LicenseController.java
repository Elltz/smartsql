/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.awt.Desktop;
import java.net.URL;
import java.sql.Connection;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.LicenseManager;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.SmartsqlSavedData;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class LicenseController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private HBox licenseParent;
    
    @FXML
    private Text licenseText;
    
    @FXML
    private TextField licenseField;
    
    @FXML
    private TextField emailField;
    
    @FXML
    private Label labelnoticeLicenseField;
    
    @FXML
    private HBox emailParent;
    
    @FXML
    private Text emailText;
    
    @FXML
    private Label labelnoticeEmailField;
    
    @FXML
    private Button verifyButton;
    
    @FXML
    private Button goBackButton;
    
    @FXML
    private Label licenseLabel;
    
    @FXML
    private Hyperlink registerLink;
    
    @FXML
    private Hyperlink forgotLink;
    
//    private final String PASS_EMAIL_ID = "dev@topdbs.in";
//    
//    private final String COMMUNITY_KEY = "SmartMySQLcommunityVersion";
    
    private Connection onlineConnection;
    
    @Getter
    private boolean registrationValid = false;
    
    final SimpleBooleanProperty licenseValidated = new SimpleBooleanProperty(false);
    final SimpleBooleanProperty emailValidated = new SimpleBooleanProperty(false);

    private UserUsageStatisticService usageService;
    private boolean registerInPublic = false;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        emailField.textProperty().addListener(new ChangeListener<String>() {

            private void changeCode(String t, String t1) {
                if (!labelnoticeEmailField.isVisible()) {
                    labelnoticeEmailField.setVisible(true);
                }
                
                int newLen = t1.length();

                if (newLen > 5 && FunctionHelper.isValidEmailAddress(t1)) {
                    setGraphicOnLabel(labelnoticeEmailField, StandardDefaultImageProvider.getInstance().getOk_small_image());
                    labelnoticeEmailField.setText("email good");
                    emailValidated.setValue(Boolean.TRUE);
                    emailField.setTooltip(null);
                } else {
                    emailValidated.setValue(Boolean.FALSE);
                    setGraphicOnLabel(labelnoticeEmailField, StandardDefaultImageProvider.getInstance().getDialogError_image());
                    labelnoticeEmailField.setText("Invalid email. please check");
                    Tooltip tip = emailField.getTooltip();
                    if (tip == null) {
                        tip = new Tooltip();
                        tip.setWrapText(true);
                        emailField.setTooltip(tip);
                    }
                    tip.setText("Invalid email. please check");
                }
            }

            {
                labelnoticeEmailField.tooltipProperty().bind(emailField.tooltipProperty());
                emailField.focusedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> ov,
                            Boolean t, Boolean t1) {
                        if (!t1) {
                            changeCode("", emailField.getText());
                        }
                    }
                });
            }

            @Override
            public void changed(ObservableValue<? extends String> ov,
                    String t, String t1) {
                changeCode(t, t1);
            }
        });
        
        licenseField.textProperty().addListener(new ChangeListener<String>() {

            private void changeCode(String t, String t1) {
                if (!labelnoticeLicenseField.isVisible()) {
                    labelnoticeLicenseField.setVisible(true);
                }
                
                int newLen = t1.length();

                if (newLen > 3 && (!t1.contains(" ") && t1.charAt(newLen - 1) != ' ')) {
                    setGraphicOnLabel(labelnoticeLicenseField, StandardDefaultImageProvider.getInstance().getOk_small_image());
                    labelnoticeLicenseField.setText("license looks good");
                    licenseValidated.set(true);
                    licenseField.setTooltip(null);
                    
                } else {
                    setGraphicOnLabel(labelnoticeLicenseField, StandardDefaultImageProvider.getInstance().getDialogError_image());
                    labelnoticeLicenseField.setText("license looks invalid");
                    licenseValidated.set(false);
                    Tooltip tip = licenseField.getTooltip();
                    if (tip == null) {
                        tip = new Tooltip();
                        tip.setWrapText(true);
                        licenseField.setTooltip(tip);
                    }
                    tip.setText("Please a license if you have");
                }
            }

            {
                labelnoticeLicenseField.tooltipProperty().bind(licenseField.tooltipProperty());
                licenseField.focusedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> ov,
                            Boolean t, Boolean t1) {
                        if (!t1) {
                            changeCode("", licenseField.getText());
                        }
                    }
                });
            }

            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                changeCode(t, t1);
            }
        });
        
        verifyButton.visibleProperty().bind(emailValidated.and(licenseValidated));
        
        EventHandler<ActionEvent> allActionEventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Object source = event.getSource();
                
                if (source == registerLink) {
                    try {
                        Desktop.getDesktop().browse(new URL(Flags.SMARTMYSQL__REGISTER).toURI());
                    } catch (Exception e) { }
                    
                } else if (source == forgotLink) {
                    try {
                        Desktop.getDesktop().browse(new URL(Flags.SMARTMYSQL__FORGOT).toURI());
                    } catch (Exception e) { }
                    
                } else if(source == goBackButton) {
                    getStage().close();                    
                } else if(source == verifyButton) {
                    validateLicense();
                }
            }
        };
        registerLink.setOnAction(allActionEventHandler);
        forgotLink.setOnAction(allActionEventHandler);
        goBackButton.setOnAction(allActionEventHandler);
        verifyButton.setOnAction(allActionEventHandler);       
        
    }
    
    public static void setGraphicOnLabel(Label l,Image im){
        if(l.getGraphic() == null){
            l.setGraphic(new ImageView(im));
        }else{
            ((ImageView)l.getGraphic()).setImage(im);
        }
    }
    
    public void validateLicense() {
        makeBusy(true);
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                try {
                    //connectToMysqlDb();
                    boolean licenseState = PublicDataAccess.getInstanсe().userValidation(emailField.getText(), licenseField.getText());
                    if (licenseState) {
                        Properties props = new Properties();
                        props.setProperty(Flags.USER_KEYTYPE, "paid");
                        props.setProperty(Flags.USER_KEY_DAYS, "365");
                        saveDetailsToSmartsqlSavedData(props);
                        
                        registrationValid = true;
                        
                        if (registerInPublic) {
                            usageService.savePCDetailsAfterRegistration(emailField.getText(), StandardApplicationManager.getInstance().getSmartsqlSavedData().getOnlineServer());
                        }
                        
                        LicenseManager.getInstance().reinit(emailField.getText(), licenseField.getText());
                        
                        PublicDataAccess.getInstanсe().login(emailField.getText(), licenseField.getText());
                        
                        Platform.runLater(() -> {
                            getStage().close();
//                            Platform.runLater(() -> {
//                                DialogHelper.showInfo(getController(), "LICENSE CONFIRMATION",
//                                    "Your license have been activated with 1year(s)..", null, getController().getStage());
//                            });                            
                        });
                    } else {
                        Platform.runLater(()-> DialogHelper.showError(getController(), "LICENSE CONFIRMATION", "License void..", null, getStage()) );
                    }
//                    disConnectionFromMysqlDb();
                } catch (Throwable e) {
                    log.error(e.getMessage(), e);
                    Platform.runLater(()-> updateSubText("Error validating license... Please try again", Color.TOMATO));
                    try { Thread.sleep(5000); } catch (Throwable ke) {}
                    Platform.runLater(()-> updateSubText(null, Color.BLACK));                    
                }finally{
                    makeBusy(false);
                }
            }
        });
    }
    
    private void updateSubText(String text, Color textColor) {
        if (text == null || text.isEmpty()) {
            licenseLabel.setContentDisplay(ContentDisplay.TEXT_ONLY);
        } else {
            licenseLabel.setContentDisplay(ContentDisplay.BOTTOM);
            Label subText = (Label) licenseLabel.getGraphic();
            subText.setText(text);
            if(textColor != null)
                subText.setTextFill(textColor);
        }
    }
    
    public void preInjectFields(){
        SmartsqlSavedData savedData = StandardApplicationManager.getInstance().getSmartsqlSavedData();
        licenseField.setText(savedData.getLKey());
        emailField.setText(savedData.getEmailId());
    }
    
    public void saveDetailsToSmartsqlSavedData(Properties props) {
        if (emailField.getText().length() > 3 && licenseField.getText().length() > 4) {
            if(props == null){
                props = new Properties();
            }
            
            props.setProperty(Flags.USER_EMAIL_ID, emailField.getText());
            props.setProperty(Flags.USER_KEY, licenseField.getText());            
            StandardApplicationManager.getInstance().getSmartsqlSavedData().updateProperties(props);
        }
    }
    
    
//    public void connectToMysqlDb() throws Throwable {
//        log.info("CREATING CONNECTION TO SERVER");
//        
//        if (onlineConnection != null && !onlineConnection.isClosed()
//                && onlineConnection.isValid(100)) { return; }
//        
//        Properties props = new Properties();
//        SmartsqlSavedData savedData = StandardApplicationManager.getInstance().getSmartsqlSavedData();
//                
//        if (savedData.getOnlineServer() == null)  {
//            throw new Exception("Missing online server details");
//        }
//
//
//        Cipher cipher = Cipher.getInstance("DESede");
//        // @TODO Need get this key from some input like webservice
//        String encryptKey = savedData.getAppEncryptionKey();
//        if(encryptKey == null || encryptKey.isEmpty()){
//            log.error("Encryption key for server is missing");
//            encryptKey = "ganFITKDEfasjdONUASD&Y*F*";
//        }
//        DESedeKeySpec ks = new DESedeKeySpec(encryptKey.getBytes("UTF-8"));
//        SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
//        cipher.init(Cipher.DECRYPT_MODE, skf.generateSecret(ks));
//        byte[] encryptedText = Base64.decodeBase64(savedData.getOnlineServer());
//        byte[] plainText = cipher.doFinal(encryptedText);
//        String[] res = new String(plainText, "UTF-8").split("\\$\\$");
//
//        props.put("user", res[2]);
//        props.put("password", res[3]);
//
//        try { Class.forName("com.mysql.jdbc.Driver"); } catch (Exception e) { e.printStackTrace(); }
//        onlineConnection = DriverManager.getConnection("jdbc:mysql://" + res[0] + ":" + res[1] + "?allowMultiQueries=true", props);
//        Statement state = onlineConnection.createStatement();
//        state.execute("use " + res[4] + ";SET wait_timeout = 100;");
//        state.close();
//    }
//    
//    
//    public void disConnectionFromMysqlDb() {
//        try {
//            if (onlineConnection != null && !onlineConnection.isClosed()
//                    && onlineConnection.isValid(100)) {
//                if (!onlineConnection.getAutoCommit()) {
//                    onlineConnection.commit();
//                }
//                onlineConnection.close();
//                onlineConnection.abort(Executors.newFixedThreadPool(1));
//            }
//            onlineConnection = null;
//        } catch (Exception e) { e.printStackTrace(); }
//    }
//    
//    
//    boolean isLicenseValid(String email, String providedLicense) {
//        log.info("VALIDATING USER LICENSE");
//        // for @Terah: check maybe we enabled encryption for email and license, 
//        // then need add it to this code
//        try (Statement state = onlineConnection.createStatement();) {
//            StringBuilder query = new StringBuilder(200);
//            query.append("SELECT SmartMySQL.user_validation('");
//            query.append(email);
//            query.append("','");
//            query.append(providedLicense);
//            query.append("');");
//            ResultSet rs = state.executeQuery(query.toString());
//            rs.next();
//            
//            return rs.getInt(1) == 1;
//        } catch (Exception e) { log.error(e.getMessage(), e); }
//        
//        return false;
//    }
    
    public void prepareLicenseScreen(UserUsageStatisticService usageService, boolean registerInPublic){
        
        this.registerInPublic = registerInPublic;
        this.usageService = usageService;
        
        Stage stage = new Stage();
        stage.setTitle("LICENSE ACTIVATION");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(getController().getStage());
        stage.setScene(new Scene(getUI()));
        stage.getScene().getStylesheets().addAll(getController().getStage().getScene().getStylesheets());
        stage.setResizable(false);
        stage.showAndWait();
    }
}
