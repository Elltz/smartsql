/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class SwapMemoryMetricExtract extends MetricExtract{
    
    private final String MAX_MEMORY = "max";
    private final String FREE_MEMORY = "free";
    private final String AVAI_MEMORY = "avai";
    private final String USED_MEMORY = "used";

    public SwapMemoryMetricExtract(MetricTypes type, String allResponseToParse) {
        super(type, allResponseToParse);
    }

    public SwapMemoryMetricExtract(MetricTypes type) {
        super(type);
    }
    

    @Override
    public void extract() {
        super.extract(); 
        int index = -1;
        String[] lines = response.trim().split(System.lineSeparator());
        index = lines[4].indexOf(':');//index = lines[nTimes-1].indexOf(':');
        lines = lines[4].substring(index + 1).trim().split("[,.]");

        for (int k = 0; k < lines.length; k++) {
            String d = lines[k].trim().split(" ")[0];
            if (k == 0) {
                data.put(MAX_MEMORY, d);
            } else if (k == 1) {
               data.put(FREE_MEMORY, d);
            } else if (k == 2) {
                data.put(USED_MEMORY, d);
            } else if (k == 3) {
                data.put(AVAI_MEMORY, d);
            }
        }
    }
    
    /**
     * returns values in KILOBYTE
     * @return 
     */
    
    public Long getTotalMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(MAX_MEMORY));
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
    /**
     * returns values in KILOBYTE
     * @return 
     */
    
    public Long getFreeMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(FREE_MEMORY));
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
    /**
     * returns values in KILOBYTE
     * @return 
     */
    
    public Long getAvailableMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(AVAI_MEMORY));
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
    /**
     * returns values in KILOBYTE
     * @return 
     */
    
    public Long getUsedMemory() {
        try {
            // return in bytes
            return Long.valueOf(getRemoteVal(USED_MEMORY));
        } catch (Throwable th) {
        }
        
        return 0l;
    }
    
}
