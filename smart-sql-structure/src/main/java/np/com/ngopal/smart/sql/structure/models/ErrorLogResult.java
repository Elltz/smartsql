/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

import java.text.Collator;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class ErrorLogResult implements FilterableData {
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {time, thread, label, errCode, subsystem, message};
    }

    private String time;
    private String thread;
    private String label;
    private String errCode;
    private String subsystem;
    private String message;
    private Integer count = 0;
    private String minTime;
    private String maxTime;

    private Collator COLLATOR = Collator.getInstance();

    public ErrorLogResult(String time, String thread, String label, String errCode, String subsystem, String message) {
        this.time = time;
        this.thread = thread;
        this.label = label;
        this.errCode = errCode;
        this.subsystem = subsystem;
        this.message = message;
    }
    
    public void checkTime(String time) {
        if (this.minTime == null || COLLATOR.compare(this.minTime, time) > 0) {
            this.minTime = time;
        }
        
        if (this.maxTime == null || COLLATOR.compare(this.maxTime, time) < 0) {
            this.maxTime = time;
        }
    }
    
    public void incCount() {
        this.count++;
    }
    
    public ErrorLogResult copuResult() {
        return new ErrorLogResult(time, thread, label, errCode, subsystem, message);
    }
    
    public String groupKey() {
        return label + ":" + errCode;
    }
    

    public boolean isError() {
        return label != null ? label.toLowerCase().contains("error") : false;
    }
    
    public String getTime() {
        return time;
    }

    public String getThread() {
        return thread;
    }        

    public String getLabel() {
        return label;
    }

    public String getErrCode() {
        return errCode;
    }

    public String getSubsystem() {
        return subsystem;
    }

    public String getMessage() {
        return message;
    }
    
    public void setCount(Integer count) {
        this.count = count;
    }    
    public Integer getCount() {
        return count;
    }

    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }
    public String getMinTime() {
        return minTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public String getMaxTime() {
        return maxTime;
    }
    
}
