package np.com.ngopal.smart.sql.structure.components;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class FilterableTableView extends AnchorPane {

    private BorderPane busyPane;
    private ProgressIndicator indicator;

    private Function<FilterableData, Boolean> customFilter;

    public void makeBusy(boolean busy) {
        Platform.runLater(() -> {
            if (indicator == null) {
                indicator = new ProgressIndicator(-1);
                indicator.setPrefSize(80, 80);
                indicator.setMinSize(80, 80);
                indicator.setMaxSize(80, 80);
            }

            indicator.setProgress(-1);

            if (busyPane == null) {
                busyPane = new BorderPane();
                busyPane.setCursor(Cursor.WAIT);
                busyPane.setCenter(new Group(indicator));
                busyPane.getStyleClass().add("busyPane");
            }

            if (busy) {
                getChildren().remove(busyPane);
                getChildren().add(busyPane);
                AnchorPane.setBottomAnchor(busyPane, 0.0);
                AnchorPane.setTopAnchor(busyPane, 0.0);
                AnchorPane.setLeftAnchor(busyPane, 0.0);
                AnchorPane.setRightAnchor(busyPane, 0.0);
            } else {
                getChildren().remove(busyPane);
            }
        });
    }

    public void setCustomFilter(Function<FilterableData, Boolean> customFilter) {
        synchronized (table) {
            this.customFilter = customFilter;

            filterChanged();
        }
    }

    /**
     * Progress from 0 to 1
     *
     * @param progress
     */
    public void busyProgress(double progress) {
        Platform.runLater(() -> {
            indicator.setProgress(progress);
        });
    }

    private final TableView<FilterableData> table = new TableView<>();
    private final TextField searchField = new TextField();
    private final HBox searchBox = new HBox();

    private boolean searchEnabled = false;
    private TableColumn<FilterableData, Number> indexColumn;
    private final List<FilterableData> sourceData = new ArrayList<>();

    private boolean withIndexColumn = true;

    public FilterableTableView() {
        this(true);
    }

    public FilterableTableView(boolean withIndexColumn,boolean useDefaultStyle) {
        this.withIndexColumn = withIndexColumn;

        table.setFixedCellSize(18);
        table.setStyle("-fx-font-size: 11;");
        if (useDefaultStyle) {
            table.setId("filteredTable");
            table.getColumns().addListener(new ListChangeListener<
                TableColumn<FilterableData, ?>>() {

                @Override
                public void onChanged(ListChangeListener.Change<? extends TableColumn<FilterableData, ?>> change) {
                    while (change.next()) {
                        if (change.wasAdded()) {
                            for (TableColumn tc : change.getAddedSubList()) {
                                tc.setId("querybuildercolumn");
                            }
                        }
                    }
                }
            });
        }

        table.setPlaceholder(new Label(""));

        init();        
    }
    
    public FilterableTableView(boolean withIndexColumn) {
        this(withIndexColumn,true);
    }

    public TableColumn getColumnByName(String name) {
        if (name != null) {
            for (TableColumn tc : table.getColumns()) {
                if (name.equals(tc.getText())) {
                    return tc;
                } else {
                    Node n = tc.getGraphic();
                    if (n instanceof Label && name.equals(((Label) n).getText())) {
                        return tc;
                    }
                }
            }
        }

        return null;
    }

    public TableColumn getColumnByIndex(int index) {
        return table.getColumns().get(index);
    }

    public TableView getTable() {
        return table;
    }

    public List<FilterableData> getSourceData() {
        return sourceData;
    }

    public void setSearchEnabled(boolean enabled) {
        this.searchEnabled = enabled;

        if (searchEnabled) {
            getChildren().add(searchBox);

            AnchorPane.setTopAnchor(searchBox, 5.0);
            AnchorPane.setLeftAnchor(searchBox, 5.0);
            AnchorPane.setRightAnchor(searchBox, 5.0);

            AnchorPane.setTopAnchor(table, 40.0);
        } else {
            getChildren().remove(searchBox);

            AnchorPane.setTopAnchor(table, 0.0);
        }
    }

    private void init() {
        table.getSelectionModel().getSelectedCells().addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change c) {
                if (!table.getSelectionModel().getSelectedCells().isEmpty()) {
                    row = table.getSelectionModel().getSelectedCells().get(0).getRow();
                    column = table.getSelectionModel().getSelectedCells().get(0).getColumn();
                } else {
                    row = 0;
                    column = 0;
                }
            }
        });

        searchBox.getChildren().addAll(new Label("Search (regex):"), searchField);
        searchBox.setSpacing(5);
        searchBox.setAlignment(Pos.CENTER_LEFT);

        HBox.setHgrow(searchField, Priority.ALWAYS);

        searchField.setOnAction((ActionEvent event) -> {
            String searchText = searchField.getText();
            if (!searchText.isEmpty()) {
                if (!findNext(searchText)) {
                    restartSearch = true;
                    findNext(searchText);
                }
            }
        });

        getChildren().addAll(table);

        AnchorPane.setTopAnchor(table, 0.0);
        AnchorPane.setLeftAnchor(table, 5.0);
        AnchorPane.setRightAnchor(table, 5.0);
        AnchorPane.setBottomAnchor(table, 5.0);

        if (withIndexColumn) {
            indexColumn = new TableColumn<>("#");
            indexColumn.setSortable(false);
            indexColumn.setCellValueFactory(column
                    -> new ReadOnlyObjectWrapper<>(table.getItems().
                            indexOf(column.getValue()) + 1));

            addTableColumn(indexColumn, 50, 50, false);
        }

        textProps.addListener((ObservableValue<? extends String> observable,
                String oldValue, String newValue) -> filterChanged());
        matchCheckProps.addListener((ObservableValue<? extends Boolean> observable,
                Boolean oldValue, Boolean newValue) -> filterChanged());
        regexCheckProps.addListener((ObservableValue<? extends Boolean> observable,
                Boolean oldValue, Boolean newValue) -> filterChanged());
    }

    protected void filterChanged() {
        synchronized (table) {
            restartSearch = true;

            List<FilterableData> accepted = new ArrayList<>();
            List<FilterableData> notAccepted = new ArrayList<>();

            for (FilterableData row : sourceData) {
                if (acceptFilter(row)) {
                    if (!table.getItems().contains(row)) {
                        accepted.add(row);
                    }
                } else {
                    notAccepted.add(row);
                }
            }

            table.getItems().removeAll(notAccepted);
            table.getItems().addAll(accepted);

            table.sort();
        }
    }

    protected void acceptFilter() {

        restartSearch = true;

        List<FilterableData> list = table.getItems();

        int i = 0;
        while (i < list.size()) {
            if (!acceptFilter(list.get(i))) {
                list.remove(i);
            } else {
                i++;
            }
        }
    }

    public boolean acceptFilter(FilterableData row) {
        String filterCondition = textProps.get();
        if (filterCondition != null && !filterCondition.isEmpty()) {

            try {
                Pattern pattern = regexCheckProps.get() ? Pattern.compile(filterCondition) : null;

                boolean result = false;
                for (String filterValue : row.getValuesFoFilter()) {

                    if (regexCheckProps.get() && pattern != null) {
                        // if regex - need check using regex
                        result = pattern.matcher(filterValue).find();
                    } else {
                        // else just
                        result = filterValue.toLowerCase().contains(filterCondition.toLowerCase());
                    }

                    if (result) {
                        break;
                    }
                }

                if (matchCheckProps.get()) {
                    result = !result;
                }

                if (result && customFilter != null) {
                    return customFilter.apply(row);
                }

                return result;
            } catch (Throwable th) {
                // my be some errors in pattern compile - its ok just ignore and give user write full regex
            }
        }

        if (customFilter != null) {
            return customFilter.apply(row);
        }

        return true;
    }

    public void refresh() {
        indexColumn.setVisible(false);
        indexColumn.setVisible(true);
    }

    public void addAndRemoveItems(List<? extends FilterableData> itemsForAdd, List<? extends FilterableData> itemsForRemove) {
        synchronized (table) {
            if (itemsForAdd != null) {
                for (FilterableData row : itemsForAdd) {
                    if (!sourceData.contains(row)) {
                        sourceData.add(row);
                    }

                    if (!table.getItems().contains(row)) {
                        table.getItems().add(row);
                    }
                }
            }

            if (itemsForRemove != null) {
                // Some time we can as param add sourceData - then it will not work correct
                // so wrap it
                itemsForRemove = new ArrayList<>(itemsForRemove);
                sourceData.removeAll(itemsForRemove);
                table.getItems().removeAll(itemsForRemove);
            }

            acceptFilter();
            table.sort();
        }
    }

    public void addItems(List<? extends FilterableData> itemsForAdd) {
        synchronized (table) {
            if (itemsForAdd != null) {
                sourceData.addAll(itemsForAdd);

                table.getItems().addAll(itemsForAdd);

                acceptFilter();

                table.sort();
            }
        }
    }

    public void removeItems(List<? extends FilterableData> itemsForRemove) {
        synchronized (table) {
            if (itemsForRemove != null) {
                table.getItems().removeAll(itemsForRemove);

                sourceData.removeAll(itemsForRemove);

                acceptFilter();

                table.sort();
            }
        }
    }

    public List<FilterableData> getCopyItems() {
        synchronized (table) {
            return new ArrayList<>(table.getItems());
        }
    }

    /**
     * Add table column and bind width
     *
     * @param tableColumn input table column
     * @param bindingWidth binding value from 0.00 to 1.00
     */
    public void addTableColumn(TableColumn tableColumn, double bindingWidth) {
        table.getColumns().add(tableColumn);
        tableColumn.prefWidthProperty().bind(table.widthProperty().multiply(bindingWidth));
    }

    public void addTableColumn(int index, TableColumn tableColumn, double bindingWidth) {
        table.getColumns().add(index, tableColumn);
        tableColumn.prefWidthProperty().bind(table.widthProperty().multiply(bindingWidth));
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth, double minWidth) {
        addTableColumn(tableColumn, bindingWidth, minWidth, true);
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth, double minWidth, boolean inPercent) {
        table.getColumns().add(tableColumn);
        if (inPercent) {
            tableColumn.prefWidthProperty().bind(table.widthProperty().multiply(bindingWidth));
        } else {
            tableColumn.setPrefWidth(bindingWidth);
        }
        tableColumn.setMinWidth(minWidth);
    }

    public void addColumnToSortOrder(TableColumn tableColumn) {
        table.getSortOrder().add(tableColumn);
    }

    private int row = 0;
    private int column = 1;
    private boolean restartSearch = true;

    public boolean findNext(String text) {
        synchronized (table) {
            if (restartSearch) {
                row = 0;
                column = 0;
                restartSearch = false;
            }

            column++;

            Pattern p = Pattern.compile(text, Pattern.CASE_INSENSITIVE);

            while (row < table.getItems().size()) {
                while (column < table.getColumns().size()) {

                    TableColumn tc = table.getColumns().get(column);
                    Object data = tc.getCellData(row);
                    column++;

                    if (data != null && p.matcher(data.toString()).find()) {
                        int selectedRow = row;
                        Platform.runLater(() -> {
                            table.getSelectionModel().select(selectedRow, tc);
                            // TODO Not sure that this working correct
                            table.scrollToColumn(tc);
                            Platform.runLater(() -> table.scrollTo(selectedRow));
                        });

                        return true;
                    }
                }
                column = 1;
                row++;
            }
        }

        return false;
    }

    private final SimpleStringProperty textProps
            = new SimpleStringProperty();

    public StringProperty getTextProperty() {
        return textProps;
    }

    private final SimpleBooleanProperty regexCheckProps
            = new SimpleBooleanProperty(false),
            matchCheckProps
            = new SimpleBooleanProperty(false);

    public BooleanProperty getRegexStageProperty() {
        return regexCheckProps;
    }

    public BooleanProperty getMatchStateProperty() {
        return matchCheckProps;
    }

    public void unbindAll() {
        textProps.unbind();
        matchCheckProps.unbind();
        regexCheckProps.unbind();
    }

}
