/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class SalveStatusData {
    String key;
        String value;
        
        public SalveStatusData(String key, String value) {
            this.key = key;
            this.value = value;
        }
        
        public String getKey() {
            return key;
        }
        
        public String getValue() {
            return value;
        }
}
