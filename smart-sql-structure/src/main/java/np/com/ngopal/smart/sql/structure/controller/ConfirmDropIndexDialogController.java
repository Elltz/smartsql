package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ConfirmDropIndexDialogController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label titleLabel;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
    public void init(String titleText) {
        titleLabel.setText(titleText); 
        calculateHeightFlow(titleLabel, 400.0);
    }
    
    @FXML
    public void keepIndexAction(ActionEvent event) {
        responce = DialogResponce.CANCEL;
        getStage().close();
    }
    
    @FXML
    public void dropIndexAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    public DialogResponce getResponce() {
        return responce;
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }

    @Override
    public String getInputValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initDialog(Object... params) {
        init((String)params[0]);
        
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }
}
