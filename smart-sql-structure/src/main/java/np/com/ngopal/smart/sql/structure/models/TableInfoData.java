package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class TableInfoData {
     
    private List<ColumnData> columns;
    
    public TableInfoData() {
    }

    public ObservableList<String> getColumnsData() {
        ObservableList<String> list = FXCollections.observableArrayList();
        if (columns != null) {
            for (ColumnData c: columns) {
                list.add(c.getName());
            }
        }
        
        return list;
    }
    
    public ColumnData getAutoIncrementColumn() {
        
        if (columns != null) {
            for (ColumnData cd: columns) {
                if (cd.isAutoincrement()) {
                    return cd;
                }
            }
        }
        
        return null;
    }
    
    public List<String> getTables() {
        List<String> list = new ArrayList<>();
        
        if (columns != null) {
            for (ColumnData cd: columns) {            
                String table = cd.getDisplayTable();

                if (!list.contains(table)) {
                    list.add(table);
                }
            }
        }
        
        return list;
    }
    
    public List<ColumnData> getColumnsByTable(String table) {
        List<ColumnData> list = new ArrayList<>();
        if (columns != null) {
            for (ColumnData cd: columns) {
                if (table.equals(cd.getDisplayTable())) {
                    list.add(cd);
                }
            }
        }
        
        return list;
    }
    
    public String getTableByColumn(String column) { 
        if (columns != null) {
            for (ColumnData cd: columns) {
                if (column.equals(cd.getName())) {
                    return cd.getDisplayTable();
                }
            }
        }
        
        return null;
    }
    
//    // TODO
//    // Need refactor
//    public String getFirstDislayName() {
//        return "`" + columns.get(0).getDatabaseName() + "`.`" + columns.get(0).getTableName() + "`";
//    }
    
    public int getColumnIndex(String columnName) {
        if (columns != null && columnName != null) {
            for (int i = 0; i < columns.size(); i++) {
                if (columnName.equals(columns.get(i).getName())) {
                    return i;
                }
            }
        }
        
        return -1;
    }
    
    public ColumnData getColumnByNameAndDatabaseAndTable(String name, String database, String table) {
        
        if (columns != null) {
            for (ColumnData cd: columns) {
                if (cd.getName().equalsIgnoreCase(name) && cd.getDatabaseName().equals(database) && cd.getTableName().equals(table)) {
                    return cd;
                }
            }
        }
        
        return null;
    }

    public List<ColumnData> getPrimaryKeyColumns(String tableName) {
        List<ColumnData> list = new ArrayList<>();
        if (columns != null) {
            list = columns.stream().filter(cd -> cd.isPrimaryKey() && cd.getDisplayTable().equals(tableName)).collect(Collectors.toList());
            
            // if list is empty - its mean that no primary columns
            // we must add all columns as primary keys
            if (list.isEmpty()) {
                list.addAll(columns);
            }
        }
        
        return list;
    }

    public void addColumn(ColumnData column) {
        if (columns == null) {
            columns = new ArrayList<>();
        }
        columns.add(column);
    }
    
    
    public ColumnData getColumnByIndex(int index) {
        if (columns != null && index >= 0 && index < columns.size()) {
            return columns.get(index);
        }
        
        return null;
    }
    
    
    public void setColumns(List<ColumnData> columns) {
        this.columns = columns;
    }
    public List<ColumnData> getColumns() {
        return columns;
    }
    
    
}
