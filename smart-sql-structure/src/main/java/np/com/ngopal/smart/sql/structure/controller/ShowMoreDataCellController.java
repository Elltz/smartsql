package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ShowMoreDataCellController extends BaseController implements Initializable {
        
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TextArea textArea;
    
    @FXML
    private Label sizeLabel;   

    
    private String data = null;
    
    
    @FXML
    void copyAction(ActionEvent event) {
        
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();

        content.putString(textArea.getText());
        clipboard.setContent(content);
    }
        
    @FXML
    void cancelAction(ActionEvent event) {
        data = null;
        ((Stage)mainUI.getScene().getWindow()).close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {        
               
        textArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateSize();
        });
    }
    
    private void updateSize() {
        Platform.runLater(() -> {
            try {
                sizeLabel.setText("Size: " + new DecimalFormat("#,###").format(textArea.getText() != null ? textArea.getText().getBytes("UTF-8").length : 0) + " bytes");
            } catch (Throwable th) {}
        });                
    }

    public void init(String data, boolean editable) {
        textArea.setText(data);
        textArea.positionCaret(0);
    }
    
    public String getData() {
        return data;
    }
}
