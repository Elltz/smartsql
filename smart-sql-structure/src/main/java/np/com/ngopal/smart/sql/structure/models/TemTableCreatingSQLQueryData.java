/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class TemTableCreatingSQLQueryData implements FilterableData {

    private SimpleStringProperty query;
    private SimpleStringProperty db;
    private SimpleStringProperty exec_count;
    private SimpleStringProperty total_latency;
    private SimpleStringProperty memory_tmp_tables;
    private SimpleStringProperty disk_tmp_tables;
    private SimpleStringProperty avg_tmp_tables_per_query;
    private SimpleStringProperty tmp_tables_to_disk_pct;
    private SimpleStringProperty first_seen;
    private SimpleStringProperty last_seen;
    private SimpleStringProperty digest;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("SSS 'ms'");

    public TemTableCreatingSQLQueryData(String query, String db, String exec_count, String total_latency, String memory_tmp_tables,
            String disk_tmp_tables, String avg_tmp_tables_per_query, String tmp_tables_to_disk_pct, String first_seen,
            String last_seen, String digest) {
        this.query = new SimpleStringProperty(query);
        this.db = new SimpleStringProperty(db);
        this.exec_count = new SimpleStringProperty(exec_count);
        this.total_latency = new SimpleStringProperty( convert(Long.valueOf(total_latency) / 1000000000l) );
        this.memory_tmp_tables = new SimpleStringProperty(memory_tmp_tables);
        this.disk_tmp_tables = new SimpleStringProperty(disk_tmp_tables);
        this.avg_tmp_tables_per_query = new SimpleStringProperty(avg_tmp_tables_per_query);
        this.tmp_tables_to_disk_pct = new SimpleStringProperty(tmp_tables_to_disk_pct);
        this.first_seen = new SimpleStringProperty(first_seen);
        this.last_seen = new SimpleStringProperty(last_seen);
        this.digest = new SimpleStringProperty(digest);
    }

    @Override
    public String[] getValuesFoFilter() {
        return new String[]{
            getQuery(), getDb(), getExec_count(), getTotal_latency(), getMemory_tmp_tables(), getDisk_tmp_tables(),
            getAvg_tmp_tables_per_query(), getTmp_tables_to_disk_pct(), getFirst_seen(), getLast_seen(), getDigest()
        };
    }

    public String getQuery() {
        return query.getValue();
    }

    public String getDb() {
        return db.getValue();
    }

    public String getExec_count() {
        return exec_count.getValue();
    }

    public String getTotal_latency() {
        return total_latency.getValue();
    }

    public String getMemory_tmp_tables() {
        return memory_tmp_tables.getValue();
    }

    public String getDisk_tmp_tables() {
        return disk_tmp_tables.getValue();
    }

    public String getAvg_tmp_tables_per_query() {
        return avg_tmp_tables_per_query.getValue();
    }

    public String getTmp_tables_to_disk_pct() {
        return tmp_tables_to_disk_pct.getValue();
    }

    public String getFirst_seen() {
        return first_seen.getValue();
    }

    public String getLast_seen() {
        return last_seen.getValue();
    }

    public String getDigest() {
        return digest.getValue();
    }

    private String convert(Long val){
        //val is in milliseconds
        TimeUnit unit = TimeUnit.MILLISECONDS;
        if(unit.toHours(val) > 0){
            dateFormat.applyPattern("HH 'h'");
        }else if(unit.toMinutes(val) > 0){
            dateFormat.applyPattern("mm 'm'");
        }else if(unit.toSeconds(val) > 0){
            dateFormat.applyPattern("ss 's'");
        }else {
            dateFormat.applyPattern("SSS 'ms'");
        }        
        return dateFormat.format(new Date(val));
    }
    
}
