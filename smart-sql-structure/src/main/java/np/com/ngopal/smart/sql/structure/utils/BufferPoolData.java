/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class BufferPoolData {
    private final SimpleStringProperty size;
        private final SimpleStringProperty freeBuffs;
        private final SimpleStringProperty pages;
        private final SimpleStringProperty dirtyPages;
        private final SimpleStringProperty hitRate;
        private final SimpleStringProperty memory;
        private final SimpleStringProperty addPool;
        
        public BufferPoolData() {
            this.size = new SimpleStringProperty();
            this.freeBuffs = new SimpleStringProperty();
            this.pages = new SimpleStringProperty();
            this.dirtyPages = new SimpleStringProperty();
            this.hitRate = new SimpleStringProperty();
            this.memory = new SimpleStringProperty();
            this.addPool = new SimpleStringProperty();
        }
        
        public SimpleStringProperty size() {
            return size;
        }
        
        public SimpleStringProperty freeBuffs() {
            return freeBuffs;
        }
        
        public SimpleStringProperty pages() {
            return pages;
        }
        
        public SimpleStringProperty dirtyPages() {
            return dirtyPages;
        }
        
        public SimpleStringProperty hitRate() {
            return hitRate;
        }
        
        public SimpleStringProperty memory() {
            return memory;
        }
        
        public SimpleStringProperty addPool() {
            return addPool;
        }
}
