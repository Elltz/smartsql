/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.misc;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javax.persistence.Transient;
import lombok.*;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.DBTable;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class SelectionColumn extends Column {
    
    @Transient
    private SimpleObjectProperty<DataType> dataTypeProperty;
    @Transient
    private SimpleBooleanProperty selectedProperty;
    @Transient
    private SimpleStringProperty characterSetProperty;
    @Transient
    private SimpleStringProperty collationPorperty;
    
    public SelectionColumn(String name, DBTable table, DataType dataType, String defaults, boolean notNull,
              boolean primaryKey,
              boolean unsigned, boolean autoIncrement, boolean zeroFill, boolean onUpdate, String comment,
              String characterSet, String collation) {
        super(name, table, dataType, defaults, notNull, primaryKey, unsigned, autoIncrement, zeroFill, onUpdate,
                  comment,
                  characterSet, collation);
    }
    
    public SimpleObjectProperty<DataType> dataType() {
        if (dataTypeProperty == null) {
            dataTypeProperty = new SimpleObjectProperty(getDataType());
            dataTypeProperty.addListener((ObservableValue<? extends DataType> observable, DataType oldValue, DataType newValue) -> {
                super.setDataType(newValue);
            });
        }
        
        return dataTypeProperty;
    }
    
    public boolean isEmpty() {
        return getName() == null || getName().trim().isEmpty();
    }
    
    @Override
    public void setDataType(DataType dataType) {
        dataType().set(dataType);
    }
    
    public SimpleStringProperty characterSet() {
        if (characterSetProperty == null) {
            characterSetProperty = new SimpleStringProperty(getCharacterSet());
            characterSetProperty.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                super.setCharacterSet(newValue);
                setCollation("");
                setCollation(null);
            });
        }
        
        return characterSetProperty;
    }
    
    @Override
    public void setCharacterSet(String characterSet) {
        characterSet().set(characterSet);
    }
    
    public SimpleStringProperty collation() {
        if (collationPorperty == null) {
            collationPorperty = new SimpleStringProperty(getCollation());
            collationPorperty.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                super.setCollation(newValue);
            });
        }
        
        return collationPorperty;
    }
    
     public SimpleBooleanProperty selected() {
        if (selectedProperty == null) {
            selectedProperty = new SimpleBooleanProperty(false);
        }
        
        return selectedProperty;
    }
    
    @Override
    public void setCollation(String collation) {
        collation().set(collation);
    }
    
    
    @Override
    public String toString() {
        return "selected=" + selected().get() + ", column=" + getName();
    }
}
