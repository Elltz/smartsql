/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import com.sun.javafx.application.PlatformImpl;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class IntroductionScreenController extends BaseController implements Initializable  {

    @FXML
    private AnchorPane mainUI;
        
    @FXML
    private HBox imageHBox;
    
    @FXML
    private HBox buttonBox;
    
    @FXML
    private ImageView imageView;
        
    @FXML
    private Label notification;
            
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public void checkConnection(ActionEvent event) {
        // TODO What need do?        
    }
    
    public void exitApplicaiton(ActionEvent event) {
        Platform.exit();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {                   
    }
    
    public void destroySplash() {
        stopSplashAnimation();
        setBottomLoaderText("");
        mainUI.getScene().setRoot(null);
        mainUI = null;
        System.gc();        
    }
    
    public void setSplashText(String text){
//        smartLabel.setText(text);
    }
        
    public void setBottomLoaderText(String text){
        notification.setText(text);
    }
    
    public void startSplashAnimation(){           
        
        PlatformImpl.setTaskbarApplication(false);
        
        int imageIdx = 0;
        File splashIndexFile = StandardApplicationManager.getInstance().getSmartDataDirectory(np.com.ngopal.smart.sql.structure.utils.Flags.SPLASH_FILE);
        if (splashIndexFile.exists()) {
            
            try {
                String result = new String(Files.readAllBytes(splashIndexFile.toPath()), "UTF-8");
                imageIdx = Integer.parseInt(result);
            } catch (Throwable ex) {
                log.error(ex.getMessage(), ex);
            }
            
            if (splashIndexFile.exists()) {
                splashIndexFile.delete();
            }
        }
        
        
        Image img = null;
        try {
            img = StandardDefaultImageProvider.getInstance().getSplash_gif(imageIdx, (int)imageHBox.getWidth(), (int)imageHBox.getHeight());
        } catch (Throwable th) {
            imageIdx = 0;
            img = StandardDefaultImageProvider.getInstance().getSplash_gif(imageIdx, (int)imageHBox.getWidth(), (int)imageHBox.getHeight());
        }
        
        if (img != null) {
            imageView.setImage(img);
        }
        
        try {
            Files.write(splashIndexFile.toPath(), String.valueOf(imageIdx + 1).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
    
    public void stopSplashAnimation(){
        PlatformImpl.setTaskbarApplication(true);
    }
    
    public void triggerErrorScreen(String errorTitle, String errorMessage){
        Platform.runLater(()->{
            buttonBox.setVisible(true);
            getStage().setTitle(errorTitle);
            stopSplashAnimation();
            notification.setStyle("-fx-text-fill: white; -fx-font-weight: bold;");
            notification.setText(errorMessage);
            notification.setContentDisplay(ContentDisplay.TEXT_ONLY);
        });
    }
    
}
