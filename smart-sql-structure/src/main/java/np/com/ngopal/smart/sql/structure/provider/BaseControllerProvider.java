/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.provider;

import java.io.IOException;
import java.net.URISyntaxException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.util.Callback;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.UIController;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public abstract class BaseControllerProvider<T extends BaseController> {

    protected String input;
    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PROTECTED)
    protected UIController iController;
    private Class<T> optionalClass;
    protected Callback<Class<?>, Object> controllerFactory;

    public BaseControllerProvider() {
        this(null, null);
    }

    public BaseControllerProvider(Callback<Class<?>, Object> controllerFactory) {
        this(null, null, controllerFactory);
    }

    public BaseControllerProvider(UIController iController) {
        this(iController, null);
    }

    public BaseControllerProvider(Class<T> meccalss) {
        this(null, meccalss);
    }

    public BaseControllerProvider(UIController iController, Class<T> meccalss) {
        this(iController, meccalss, null);
    }

    public BaseControllerProvider(UIController iController, Class<T> meccalss, Callback<Class<?>, Object> controllerFactory) {
        this.iController = iController;
        this.optionalClass = meccalss;
        this.controllerFactory = controllerFactory;
    }

    public T get(String input) {
        this.input = input;
        return get();
    }

    public T get() {
        try {            
            FXMLLoader loader = new FXMLLoader(getClass().getResource(getPreface() + input + ".fxml").toURI().toURL());
            if (optionalClass != null) {
                try {
                    Object o = optionalClass.newInstance();
                    ((BaseController) o).setController(iController);
                    loader.setController(o);
                } catch (Exception e) { e.printStackTrace(); }
            } else {
                loader.setControllerFactory(new Callback<Class<?>, Object>() {
                    @Override
                    public Object call(Class<?> param) {
                        Object o = null;
                        try {
                            if (controllerFactory != null) {
                                o = controllerFactory.call(param);
                            } else {
                                o = param.newInstance();
                            }
                            if (iController != null) {
                                ((BaseController) o).setController(iController);
                            }
                        } catch (Exception e) { e.printStackTrace(); }
                        return o;
                    }
                });
            }
            
            loader.load();
            T controller = loader.getController();

            return controller;
        } catch (URISyntaxException | IOException ex) {
            ex.printStackTrace();
            log.error("Error", ex.getMessage(), ex);
            return null;
        }
    }

    public Parent getUI(String input) {
        return get(input).getUI();
    }

    protected abstract String getPreface();
}
