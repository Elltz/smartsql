package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.GSChartVariable;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ProfilerUserChartsController extends BaseController implements Initializable {
    
    private ProfilerChartService chartService;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TableView<GSChart> chartsTableView;
    @FXML
    private TableColumn<GSChart, Number> chartIndexColumn;
    @FXML
    private TableColumn<GSChart, String> chartNameColumn;
    @FXML
    private TableColumn<GSChart, String> descriptionColumn;
    @FXML
    private TableColumn<GSChart, Boolean> statusColumn;
    
    @FXML
    private TableView<GSChartVariable> variablesTableView;
    @FXML
    private TableColumn<GSChartVariable, Number> variableIndexColumn;
    @FXML
    private TableColumn<GSChartVariable, String> variableNameColumn;
    @FXML
    private TableColumn<GSChartVariable, String> variableColorColumn;
    @FXML
    private TableColumn<GSChartVariable, Boolean> variableDiffColumn;
    
    @FXML
    private Button addButton;
    @FXML
    private Button removeButton;
    @FXML
    private Button upButton;
    @FXML
    private Button downButton;
    
    @FXML
    private Button addVariableButton;
    @FXML
    private Button removeVariableButton;

    private boolean changed = false;
    
    private List<String> variables;
    
    private final List<String> defaultCharts = Arrays.asList(new String[]{"Threads", "Questions", "SQL-Commands", "Thried-Locks"});
    
    public boolean wasChanged() {
        return changed;
    }
    
    public void init(ProfilerChartService chartService) {
        this.chartService = chartService;
        chartsTableView.getItems().addAll(chartService.getUserCharts());
    }
    
    @FXML
    public void addChartAction(ActionEvent event) {        
        GSChart chart = new GSChart();
        // user chart
        chart.setChartType(true);
        
        chartsTableView.getItems().add(chart);
    }
    
    @FXML
    public void removeChartAction(ActionEvent event) {
        DialogResponce responce = DialogHelper.showConfirm(getController(), "Delete selected chart", "Do you really want to delete selected chart?");
        if (responce == DialogResponce.OK_YES) {
            // remove
            chartsTableView.getItems().remove(chartsTableView.getSelectionModel().getSelectedItem());
        }
    }
     
    
    
    @FXML
    public void addVariableAction(ActionEvent event) {
        GSChart chart = chartsTableView.getSelectionModel().getSelectedItem();
        
        GSChartVariable variable = new GSChartVariable();
        variable.setChart(chart);
        if (chart.getVariables() == null) {
            chart.setVariables(new ArrayList<>());
        }
        chart.getVariables().add(variable);
        
        variablesTableView.getItems().add(variable);
    }
    
    @FXML
    public void removeVariableAction(ActionEvent event) {
        DialogResponce responce = DialogHelper.showConfirm(getController(), "Delete selected chart variable", "Do you really want to delete selected chart variable?");
        if (responce == DialogResponce.OK_YES) {
            // remove
            GSChartVariable v = variablesTableView.getItems().remove(variablesTableView.getSelectionModel().getSelectedIndex());
            
            GSChart chart = chartsTableView.getSelectionModel().getSelectedItem();
            chart.getVariables().remove(v);
        }
    }
    
    @FXML
    public void saveUserCharts(ActionEvent event) {

        // check chart names for unique and empty        
        List<String> charts = new ArrayList<>();
        for (GSChart chart: chartsTableView.getItems()) {
            if (chart.getChartName() == null || chart.getChartName().isEmpty()) {
                DialogHelper.showError(getController(), "The chart name is empty", "The chart name is empty!", null, getStage());
                return;
            } else if (defaultCharts.contains(chart.getChartName()) || charts.contains(chart.getChartName())) {
                DialogHelper.showError(getController(), "Non unique chart name", "The chart name '" + chart.getChartName() + "' non unique! Check default charts names or others.", null, getStage());
            return;
            } else {
                charts.add(chart.getChartName());
            }
            
            // check variables for unique
            if (chart.getVariables() == null || chart.getVariables().isEmpty()) {
                DialogHelper.showError(getController(), "Empty chart", "The chart must have variables!", null, getStage());
                return;
            } else {
                List<String> variables = new ArrayList<>();
                for (GSChartVariable variable: chart.getVariables()) {
                    if (variable.getVariableName() == null || variable.getVariableName().isEmpty()) {
                        DialogHelper.showError(getController(), "The chart variable is empty", "The chart '" + chart.getChartName() + "' variable name is empty!", null, getStage());
                        return;
                    } else if (defaultCharts.contains(variable.getVariableName()) || variables.contains(variable.getVariableName())) {
                        DialogHelper.showError(getController(), "Non unique chart variable name", "Th chart '" + chart.getChartName() + "' variable name '" + variable.getVariableName() + "' non unique! Check others names.", null, getStage());
                        return;
                    } else {
                        variables.add(variable.getVariableName());
                    }
                }
            }
        }
        
        for (GSChart chart: chartsTableView.getItems()) {
            chartService.save(chart);
        }
        
        changed = true;
        getStage().close();
    }
    
    @FXML
    public void cancelUserCharts(ActionEvent event) {
        
        changed = false;
        getStage().close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        chartsTableView.setEditable(true);
        
        chartIndexColumn.setCellValueFactory(column-> new ReadOnlyObjectWrapper<>(chartsTableView.getItems().indexOf(column.getValue()) + 1));
        
        chartNameColumn.setCellValueFactory(new PropertyValueFactory<>("chartName"));
        chartNameColumn.setCellFactory(TextFieldTableCell.<GSChart>forTableColumn());
        chartNameColumn.setOnEditCommit((TableColumn.CellEditEvent<GSChart, String> t) -> {
            GSChart chart = (GSChart)t.getTableView().getItems().get(t.getTablePosition().getRow());
            chart.setChartName(t.getNewValue());
        });
        
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        descriptionColumn.setCellFactory(TextFieldTableCell.<GSChart>forTableColumn());
        descriptionColumn.setOnEditCommit((TableColumn.CellEditEvent<GSChart, String> t) -> {
            GSChart chart = (GSChart)t.getTableView().getItems().get(t.getTablePosition().getRow());
            chart.setDescription(t.getNewValue());
        });
        
        statusColumn.setCellValueFactory((CellDataFeatures<GSChart, Boolean> param) -> param.getValue().getStatusProperty());
        statusColumn.setCellFactory(CheckBoxTableCell.<GSChart>forTableColumn(statusColumn));
        statusColumn.setOnEditCommit((TableColumn.CellEditEvent<GSChart, Boolean> t) -> {
            GSChart chart = (GSChart)t.getTableView().getItems().get(t.getTablePosition().getRow());
            chart.setStatus(t.getNewValue());
        });
        
        
        variablesTableView.setEditable(true);
        
        variableIndexColumn.setCellValueFactory(column-> new ReadOnlyObjectWrapper<>(variablesTableView.getItems().indexOf(column.getValue()) + 1));
        
        
        variables = new ArrayList<>();
        DBService service = getController().getSelectedConnectionSession().get().getService();
        try (ResultSet rs = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_STATUS))) {

            while(rs.next()) {
                try {
                    variables.add(rs.getString(1));
                } catch (SQLException ex) {
                    //log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_GLOBAL_STATUS'", ex);
                }  
            }
        } catch (SQLException ex) {
            log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_GLOBAL_STATUS'", ex);
        }
        
        variableNameColumn.setCellValueFactory(new PropertyValueFactory<>("variableName"));
        variableNameColumn.setCellFactory((TableColumn<GSChartVariable, String> param) -> {
            TableCell<GSChartVariable, String> cell = new TableCell<GSChartVariable, String>() {
                private ComboBox<String> box;
                @Override
                protected void updateItem(String item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        if (box == null) {
                            box = new ComboBox(FXCollections.observableArrayList(variables));
                            box.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                                GSChartVariable gsv = variablesTableView.getItems().get(getIndex());
                                gsv.setVariableName(newValue);
                            });
                           
                            new AutoCompleteComboBoxListener<>(box);
                        }
                        
                        box.getSelectionModel().select(item);
                        setGraphic(box);
                    } else {
                        setGraphic(null);
                    }
                }
                
            };
            
            return cell;
        });
        variableNameColumn.setOnEditCommit((TableColumn.CellEditEvent<GSChartVariable, String> t) -> {
            GSChartVariable variable = (GSChartVariable)t.getTableView().getItems().get(t.getTablePosition().getRow());
            variable.setVariableName(t.getNewValue());
        });
        
        variableColorColumn.setCellValueFactory(new PropertyValueFactory<>("displayColor"));
        variableColorColumn.setCellFactory(TextFieldTableCell.<GSChartVariable>forTableColumn());
        variableColorColumn.setOnEditCommit((TableColumn.CellEditEvent<GSChartVariable, String> t) -> {
            GSChartVariable variable = (GSChartVariable)t.getTableView().getItems().get(t.getTablePosition().getRow());
            variable.setDisplayColor(t.getNewValue());
        });
        
        variableDiffColumn.setCellValueFactory((CellDataFeatures<GSChartVariable, Boolean> param) -> param.getValue().getTypeProperty());
        variableDiffColumn.setCellFactory(CheckBoxTableCell.<GSChartVariable>forTableColumn(variableDiffColumn));
        variableDiffColumn.setOnEditCommit((TableColumn.CellEditEvent<GSChartVariable, Boolean> t) -> {
            GSChartVariable variable = (GSChartVariable)t.getTableView().getItems().get(t.getTablePosition().getRow());
            variable.setType(t.getNewValue() ? 1 : 0);
        });
        
        bind();
    } 
    
    
    private void bind() {
        
        chartsTableView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GSChart> observable, GSChart oldValue, GSChart newValue) -> {
            variablesTableView.getItems().clear();
            variablesTableView.getItems().addAll(newValue != null && newValue.getVariables() != null ? newValue.getVariables() : new ArrayList<>());
        });
        
        removeButton.disableProperty().bind(Bindings.isNull(chartsTableView.getSelectionModel().selectedItemProperty()));
        upButton.disableProperty().bind(Bindings.lessThanOrEqual(chartsTableView.getSelectionModel().selectedIndexProperty(), 0));
        downButton.disableProperty().bind(Bindings.equal(chartsTableView.getSelectionModel().selectedIndexProperty(), Bindings.size(chartsTableView.getItems()).subtract(1)).or(Bindings.equal(-1, chartsTableView.getSelectionModel().selectedIndexProperty())));
        
        addVariableButton.disableProperty().bind(Bindings.isNull(chartsTableView.getSelectionModel().selectedItemProperty()));
        removeVariableButton.disableProperty().bind(Bindings.isNull(variablesTableView.getSelectionModel().selectedItemProperty()));
    }
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
