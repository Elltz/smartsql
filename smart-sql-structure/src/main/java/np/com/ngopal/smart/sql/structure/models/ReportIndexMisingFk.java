
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportIndexMisingFk implements FilterableData {
    
    private final SimpleStringProperty databaseName;
    private final SimpleStringProperty mt;
    private final SimpleStringProperty mtColumn;
    private final SimpleStringProperty mtcDataType;
    private final SimpleStringProperty relation;
    private final SimpleStringProperty tt;
    private final SimpleStringProperty ttColumn;
    private final SimpleStringProperty ttcDataType;
    private final SimpleStringProperty ttEngine;
    private final SimpleStringProperty alterSql;
    private final SimpleStringProperty comments;
    
 
    public ReportIndexMisingFk(
            String databaseName, 
            String mt, 
            String mtColumn, 
            String mtcDataType, 
            String relation, 
            String tt, 
            String ttColumn, 
            String ttcDataType, 
            String ttEngine, 
            String alterSql, 
            String comments) {
        this.databaseName = new SimpleStringProperty(databaseName);
        this.mt = new SimpleStringProperty(mt);
        this.mtColumn = new SimpleStringProperty(mtColumn);
        this.mtcDataType = new SimpleStringProperty(mtcDataType);
        this.relation = new SimpleStringProperty(relation);
        this.tt = new SimpleStringProperty(tt);
        this.ttColumn = new SimpleStringProperty(ttColumn);
        this.ttcDataType = new SimpleStringProperty(ttcDataType);
        this.ttEngine = new SimpleStringProperty(ttEngine);
        this.alterSql = new SimpleStringProperty(alterSql);
        this.comments = new SimpleStringProperty(comments);
    }
 
 
    public String getDatabaseName() {
        return databaseName.get();
    }
        
    public String getMt() {
        return mt.get();
    }
    
    public String getMtColumn() {
        return mtColumn.get();
    }
    
    public String getMtcDataType() {
        return mtcDataType.get();
    }
        
    public String getRelation() {
        return relation.get();
    }
    
     public String getTt() {
        return tt.get();
    }
    
    public String getTtColumn() {
        return ttColumn.get();
    }
    
    public String getTtcDataType() {
        return ttcDataType.get();
    }
    
    public String getTtEngine() {
        return ttEngine.get();
    }
    
    public String getAlterSql() {
        return alterSql.get();
    }
    
    public String getComments() {
        return comments.get();
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getDatabaseName() != null ? getDatabaseName() : "", 
            getMt() != null ? getMt() : "",
            getMtColumn() != null ? getMtColumn() : "", 
            getMtcDataType() != null ? getMtcDataType() : "",
            getRelation() != null ? getRelation() : "",
            getTt() != null ? getTt() : "",
            getTtColumn() != null ? getTtColumn() : "",
            getTtcDataType() != null ? getTtcDataType() : "",
            getTtEngine() != null ? getTtEngine() : "",
            getAlterSql() != null ? getAlterSql() : "",
            getComments() != null ? getComments() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDatabaseName(),
            getMt(),
            getMtColumn(),
            getMtcDataType(),
            getRelation(),
            getTt(),
            getTtColumn(),
            getTtcDataType(),
            getTtEngine(),
            getAlterSql(),
            getComments()
        };
    }
}
