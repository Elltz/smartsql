/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class CheckBoxTableCell<S, T> extends TableCell<S, T> {

    private CheckBox checkBox;

    public CheckBoxTableCell() {

//        System.out.println("ob:" + ov);
//        if (check) {
//            System.out.println("is it boolean:" + ov.getValue());
//            String b = (String)ov.getValue();
//            if (b.equalsIgnoreCase("TRUE")) {
//                checkBox.selectedProperty().setValue(true);
//            }
//        }
//        checkBox.setSelected(check);
    }

    @Override public void updateItem(T item, boolean empty) {

        super.updateItem(item, empty);
        //if (item != null) {
        if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
            if (checkBox == null) {
                this.checkBox = new CheckBox();
                this.checkBox.setAlignment(Pos.CENTER);
                setAlignment(Pos.CENTER);

            }
//            System.out.println("item:" + item.toString());
//            System.out.println("empty:" + empty);
            checkBox.setSelected(item != null ? (Boolean)item : Boolean.FALSE);
            setGraphic(checkBox);
        } else {
            //setGraphic(checkBox);
            setGraphic(null);
        }

//        if (empty) {
//
//            setText(null);
//            checkBox.setSelected((Boolean)item);
//            setGraphic(null);
//
//        } else {
//
//            setGraphic(checkBox);
//
//            if (ov instanceof BooleanProperty) {
//
//                checkBox.selectedProperty().unbindBidirectional((BooleanProperty)ov);
//
//            }
//
//            ov = getTableColumn().getCellObservableValue(getIndex());
//
//            if (ov instanceof BooleanProperty) {
//
//                checkBox.selectedProperty().bindBidirectional((BooleanProperty)ov);
//
//            }
//
//        }
    }

}
