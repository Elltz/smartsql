
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportRamUsage implements FilterableData {
    
    private final SimpleStringProperty codeArea;
    private final SimpleStringProperty currentAlloc;
    
 
    public ReportRamUsage(String codeArea, String currentAlloc) {
        this.codeArea = new SimpleStringProperty(codeArea);
        this.currentAlloc = new SimpleStringProperty(currentAlloc);
    }
 
 
    public String getCodeArea() {
        return codeArea.get();
    }
    public void setCodeArea(String codeArea) {
        this.codeArea.set(codeArea);
    }
        
    public String getCurrentAlloc() {
        return currentAlloc.get();
    }
    public void setCurrentAlloc(String currentAlloc) {
        this.currentAlloc.set(currentAlloc);
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getCodeArea() != null ? getCodeArea() : "", 
            getCurrentAlloc() != null ? getCurrentAlloc() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getCodeArea(),
            getCurrentAlloc()
        };
    }
}