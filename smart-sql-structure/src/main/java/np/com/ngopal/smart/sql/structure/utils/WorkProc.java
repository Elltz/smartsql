/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

/**
 *
 * @author Terah laweh (rumorsapp@gmail.com)
 */
public abstract class WorkProc<T> implements Runnable{

    /**
     * Thread.currentThread().isInterruped();
     */
         
    public volatile boolean callUpdate = false;
    private volatile T addData;
    public String workingProcedure = null;
    
    /**
     * Since the run method will run in a background thread, the updateUI will
     * run on the ui thread by Platform.runlater(). i wanted to use a callable
     * to return the final value for proccessing on the ui thread, but that will
     * complicate things since getting reference to objects will be a bit off.
     * so everything starts here and ends here in this class and all the background
     * code does is run the run mehtod when done pass on the updateUI method.
     *
     * Its no longer mandatory
     * 
     */
    public void updateUI(){}
    
    public synchronized T getAddData(){
        return addData;
    }
    
    public synchronized void setAdditionalData(T t){
        addData = t;
    }
    
    public WorkProc(){}
    
    public WorkProc(String name,T t){
        workingProcedure = name;
        addData = t;
    }
    
    public WorkProc(T t){
        addData = t;
    }

}
