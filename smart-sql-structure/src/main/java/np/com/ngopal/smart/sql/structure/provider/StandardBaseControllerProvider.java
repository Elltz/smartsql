/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.provider;

import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.UIController;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class StandardBaseControllerProvider<K extends BaseController> extends BaseControllerProvider<K>{
    
    public StandardBaseControllerProvider() {
    }

    public StandardBaseControllerProvider(UIController iController, Class<K> meccalss) {
        super(iController, meccalss);
    }

    public StandardBaseControllerProvider(UIController iController) {
        super(iController);
    }
           
    public static <K extends BaseController> K getController(String input){
        return getController(null, input);
    }
    
    public static <K extends BaseController> K getController(UIController _standardController, String input){
        return getController(null, _standardController, input);
    }
    
    public static <K extends BaseController> K getController(Class<K> _injectedClass, UIController _standardController, String input){
        StandardBaseControllerProvider<K> scp = new StandardBaseControllerProvider(_standardController, _injectedClass);
        return scp.get(input);
    }

    @Override
    protected String getPreface() {
        return "/np/com/ngopal/smart/sql/structure/ui/";
    }
    
}
