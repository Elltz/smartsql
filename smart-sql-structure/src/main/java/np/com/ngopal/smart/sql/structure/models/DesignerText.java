package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerText implements DesignerData, InvalidationListener {

    private DesignerErrDiagram parent;
    
    private final SimpleStringProperty uuid;
    
    private final SimpleStringProperty name;
    private final SimpleStringProperty text;
    
    private final SimpleStringProperty font;
    private final SimpleStringProperty color;
    private final SimpleStringProperty textColor;
    
    private final SimpleDoubleProperty x;
    private final SimpleDoubleProperty y;
    private final SimpleDoubleProperty width;
    private final SimpleDoubleProperty height;
    
    private final SimpleStringProperty layerUuid;
    
    private final SimpleStringProperty description;
    
    private final WeakInvalidationListener weakListner = new WeakInvalidationListener(this);
    
    public DesignerText() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty("");
        name.addListener(weakListner);
        
        text = new SimpleStringProperty("");
        text.addListener(weakListner);
        
        font = new SimpleStringProperty("");
        font.addListener(weakListner);
        
        color = new SimpleStringProperty("#fefded");
        color.addListener(weakListner);
        
        textColor = new SimpleStringProperty("#ff0000");
        textColor.addListener(weakListner);
        
        x = new SimpleDoubleProperty();
        x.addListener(weakListner);
        
        y = new SimpleDoubleProperty();
        y.addListener(weakListner);
        
        width = new SimpleDoubleProperty();
        width.addListener(weakListner);
        
        height = new SimpleDoubleProperty();
        height.addListener(weakListner);
        
        layerUuid = new SimpleStringProperty();
        layerUuid.addListener(weakListner);
        
        description = new SimpleStringProperty("");
        description.addListener(weakListner);
    }
    
    @Override
    public void invalidated(Observable observable) {
        if (parent != null) {
            parent.invalidated(observable);
        }
    }
    
    @Override
    public void setParent(DesignerErrDiagram parent) {
        this.parent = parent;
    }
    
    @Override
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    @Override
    public SimpleStringProperty name() {
        return name;
    }
    
    @Override
    public SimpleStringProperty description() {
        return description;
    }
    
    public SimpleStringProperty text() {
        return text;
    }
    
    public SimpleStringProperty font() {
        return font;
    }
    
    public SimpleStringProperty textColor() {
        return textColor;
    }
    
    public SimpleStringProperty color() {
        return color;
    }
    
    @Override
    public SimpleDoubleProperty x() {
        return x;
    }
    
    @Override
    public SimpleDoubleProperty y() {
        return y;
    }
    
    @Override
    public SimpleDoubleProperty width() {
        return width;
    }
    
    @Override
    public SimpleDoubleProperty height() {
        return height;
    }
    
    
    public SimpleStringProperty layerUuid() {
        return layerUuid;
    }
    
    @Override
    public String toString() {
        return name.get();
    }

    
    @Override
    public DesignerData copyData() {
        
        DesignerText t = new DesignerText();
        t.color.set(color.get());
        t.font.set(font.get());
        t.height.set(height.get());
        t.layerUuid.set(layerUuid.get());
        t.name.set(name.get());
        t.text.set(text.get());
        t.textColor.set(textColor.get());
        t.width.set(width.get());
        t.x.set(x.get());
        t.y.set(y.get());
        t.description.set(description.get());
        
        return t;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        map.put("text", text.get());
        
        map.put("font", font.get());
        
        map.put("color", color.get());
        map.put("textColor", textColor.get());
                
        map.put("x", x.get());
        map.put("y", y.get());
        map.put("width", width.get());
        map.put("height", height.get());
        
        map.put("layerUuid", layerUuid.get());        
        map.put("description", description.get());

        return map;
    }
    
    
    public void fromMap(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set(getNotNullStringValue(map, "name"));
            text.set(getNotNullStringValue(map, "text"));
            
            font.set(getNotNullStringValue(map, "font"));
            
            color.set(getNotNullStringValue(map, "color"));
            textColor.set(getNotNullStringValue(map, "textColor"));
            
            Double x0 = (Double)map.get("x");
            x.set(x0 != null ? x0 : -1);
            
            Double y0 = (Double)map.get("y");
            y.set(y0 != null ? y0 : -1);
            
            Double width0 = (Double)map.get("width");
            width.set(width0 != null ? width0 : -1);
            
            Double height0 = (Double)map.get("height");
            height.set(height0 != null ? height0 : -1);
            
            layerUuid.set(getNotNullStringValue(map, "layerUuid"));            
            description.set(getNotNullStringValue(map, "description"));
        }
    }

    @Override
    public List<DesignerProperty> getProperties() {
        
        List<DesignerProperty> list = new ArrayList<>();
        list.add(new DesignerProperty("Name", name, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Text", text, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Font", font, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Color", color, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Text Color", textColor, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("X", x, DesignerProperty.PropertyType.DOUBLE, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Y", y, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Height", height, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Width", width, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        
        return list;
    }
    
    private String getNotNullStringValue(Map<String, Object> map, String key) {
        Object v = map.get(key);
        return v != null ? v.toString() : "";
    }
}
