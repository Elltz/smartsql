/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class PagesStatisticData {
    private final SimpleStringProperty reads;
        private final SimpleStringProperty writes;
        private final SimpleStringProperty created;
        private final SimpleStringProperty readsSec;
        private final SimpleStringProperty writesSec;
        private final SimpleStringProperty createdSec;
        
        public PagesStatisticData() {
            this.reads = new SimpleStringProperty();
            this.writes = new SimpleStringProperty();
            this.created = new SimpleStringProperty();
            this.readsSec = new SimpleStringProperty();
            this.writesSec = new SimpleStringProperty();
            this.createdSec = new SimpleStringProperty();
        }
        
        public SimpleStringProperty reads() {
            return reads;
        }
        
        public SimpleStringProperty writes() {
            return writes;
        }
        
        public SimpleStringProperty created() {
            return created;
        }
        
        public SimpleStringProperty readsSec() {
            return readsSec;
        }
        
        public SimpleStringProperty writesSec() {
            return writesSec;
        }
        
        public SimpleStringProperty createdSec() {
            return createdSec;
        }
}
