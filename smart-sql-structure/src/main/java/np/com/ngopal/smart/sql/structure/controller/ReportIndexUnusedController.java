package np.com.ngopal.smart.sql.structure.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportIndexUnusedController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;

    @FXML
    private Button exportButton;
    
    private ConnectionSession session;
    
    @FXML
    public void refreshAction(ActionEvent event) {
        
        makeBusy(true);
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                if (session != null) {
                    try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_SUMMARY))) {
                        if (rs != null) {
                            List<ReportIndexUnused> list = new ArrayList<>();
                            while (rs.next()) {
                                ReportIndexUnused rid = new ReportIndexUnused(
                                        rs.getString(1),
                                        rs.getString(2),
                                        rs.getString(3)
                                );

                                list.add(rid);
                            }

                            tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                        }
                    } catch (SQLException ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), null);
                    }
                }

                Platform.runLater(() -> makeBusy(false));
            }
        });
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            List<String> sheets = new ArrayList<>();
            List<ObservableList<ObservableList>> totalData = new ArrayList<>();
            List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
            List<List<Integer>> totalColumnWidths = new ArrayList<>();
            List<byte[]> images = new ArrayList<>();
            List<Integer> heights = new ArrayList<>();
            List<Integer> colspans = new ArrayList<>();

            {
                ObservableList rowsData = FXCollections.observableArrayList();
                for (ReportIndexUnused rds : (List<ReportIndexUnused>) (List) tableView.getCopyItems()) {
                    ObservableList row = FXCollections.observableArrayList();

                    row.add(rds.getDatabase());
                    row.add(rds.getTableName());
                    row.add(rds.getIndexName());

                    rowsData.add(row);
                }

                List<Integer> columnWidths = new ArrayList<>();
                LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                columns.put("Object Schema", true);
                columnWidths.add(100);

                columns.put("Object name", true);
                columnWidths.add(200);

                columns.put("Index Name", true);
                columnWidths.add(300);

                sheets.add("Unused Indexes Report");
                totalData.add(rowsData);
                totalColumns.add(columns);
                totalColumnWidths.add(columnWidths);
                images.add(null);
                heights.add(null);
                colspans.add(null);
            }

            ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                @Override
                public void error(String errorMessage, Throwable th) {
                    Platform.runLater(() -> DialogHelper.showError(getController(), "Error", errorMessage, th, getStage()));
                }

                @Override
                public void success() {
                    Platform.runLater(() -> DialogHelper.showInfo(getController(), "Success", "Data export seccessfull", null, getStage()));
                }
            });
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        tableView.setSearchEnabled(true);
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        TableColumn databaseColumn = createColumn("Object Schema", "database");        
        TableColumn tableNameColumn = createColumn("Object Name", "tableName");        
        TableColumn indexNameColumn = createColumn("Index Name", "indexName");
        
        tableView.addTableColumn(databaseColumn, 0.23);
        tableView.addTableColumn(tableNameColumn, 0.23);
        tableView.addTableColumn(indexNameColumn, 0.5);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
}
