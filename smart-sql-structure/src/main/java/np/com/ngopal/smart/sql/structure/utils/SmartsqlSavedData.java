/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.Getter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Getter(AccessLevel.PUBLIC)
public class SmartsqlSavedData {

    private String firstTimeStartUpDate;

    private String lastAppOpenDate;

    @Setter(AccessLevel.PUBLIC)
    private String durationOfLastUsage;

    @Setter(AccessLevel.PUBLIC)
    private boolean Paid = false;

    @Setter(AccessLevel.PUBLIC)
    private String mysqlVersion = "";

    @Setter(AccessLevel.PUBLIC)
    private String mysqlServiceName = "";

    private String emailId;
    private String serialId;
    private String fullName;
    private String country;
    private String keyType = "free";
    private String lKey;
    private String keyDays = "-1";
    private String phone;
    private String noOfEmp;
    private String company;
    private String noOfServer;

    @Setter(AccessLevel.PUBLIC)
    private boolean runAppInFullScreen = true;

    /**
     * CONFIGURABLE OPTIONS WITH HOW APP BEHAVES
     */
//    @Setter(AccessLevel.PUBLIC)
//    private boolean autoRestore = true;

//    @Setter(AccessLevel.PUBLIC)
//    private boolean autoStartMysqlWhenDown = true;

    //-1 means never. the figure is in minutes // not used anymore - updater does that
//    @Setter(AccessLevel.PUBLIC)
//    private String checkForUpdateDuration = "20";

//    @Setter(AccessLevel.PUBLIC)
//    private String savingDataDuration = "5";//units in minutes   

    private String version;

    @Setter(AccessLevel.PUBLIC)
    private String logFile;

    private String versionFeatures;

    private int buildType = Flags.MODE_PROD_BUILD;
    private int packageType = Flags.MODE_UPDATEABLE_PACKAGE;

    /**
     * QUERY EDITOR SAVEABLE DATAS
     */
    @Setter(AccessLevel.PUBLIC)
    private double queryEditorSize = 13f;

    @Setter(AccessLevel.PUBLIC)
    private boolean pasteObjectOnDoubleClick = true;

    @Setter(AccessLevel.PUBLIC)
    private boolean notifyUnSavedTab = true;

    @Setter(AccessLevel.PUBLIC)
    private boolean autorebuildTags = true;

    /**
     * IN APP SAVEABLE OPTIONS
     */
    @Setter(AccessLevel.PUBLIC)
    private String fontColor;

    @Setter(AccessLevel.PUBLIC)
    private String backgroundColor;

    @Setter(AccessLevel.PUBLIC)
    private String selectionColor;

    private String appProjectName;

    //configure options
    private long appSize;

    private String appHash;

    private Map<String, String> dependencyHashes, dependencyDirectories;

    private String bannerText;
    private String youtubeText;
    private String appEncryptionKey;
    private String onlineServer;

    //BELOW IS ONLY NEEDED FOR SMARTSQLSAVEDDATA - TAKE NOTE
    private transient String smartSqlDataStringtype;
    private transient boolean forceConvert;
    private transient Gson gson;

    public SmartsqlSavedData() {
        this(null);
    }

    public SmartsqlSavedData(Gson gson) {
        this.gson = gson;
        this.firstTimeStartUpDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
        this.lastAppOpenDate = firstTimeStartUpDate;
    }

    public static SmartsqlSavedData getSmartsqlSavedDataInstance(Gson gson, String configurationWrapperString) {
        SmartsqlSavedData ssd = gson.fromJson(configurationWrapperString, SmartsqlSavedData.class).injectGson(gson);
        ssd.lastAppOpenDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
        return ssd;
    }

    public SmartsqlSavedData injectGson(Gson gson) {
        this.gson = gson;
        return this;
    }

    public void makeDirty() {
        forceConvert = true;
    }

    /**
     * updates crucial fields of Smartsql app;
     *
     * @param inputs
     */
    public final void updateProperties(Properties inputs) {
        serialId = inputs.getProperty(Flags.USER_SERIAL_ID, serialId);
        emailId = inputs.getProperty(Flags.USER_EMAIL_ID, emailId);
        keyType = inputs.getProperty(Flags.USER_KEYTYPE, keyType);
        lKey = inputs.getProperty(Flags.USER_KEY, lKey);
        keyDays = inputs.getProperty(Flags.USER_KEY_DAYS, keyDays);
        country = inputs.getProperty(Flags.USER_COUNTRY, country);

        appEncryptionKey = inputs.getProperty(Flags.SMART_SQL_APP_ATTR_ENCRYPTION, appEncryptionKey);
        version = inputs.getProperty(Flags.SMART_SQL_APP_ATTR_VERSION, version);
        bannerText = inputs.getProperty(Flags.SMARTSQL_BANNER_TEXT, bannerText);
        youtubeText = inputs.getProperty(Flags.SMARTSQL_YOUTUBE_TEXT, youtubeText);
        onlineServer = inputs.getProperty(Flags.SMARTSQL_ONLINE_SERVER, onlineServer);
        appProjectName = inputs.getProperty(Flags.SMARTSQL_PROJECT_BUILD_MODULE, appProjectName);
        logFile = inputs.getProperty(Flags.SMARTSQL_LOG_FILE, logFile);
        try {
            buildType = Integer.valueOf(inputs.getProperty(Flags.SMARTSQL_BUILD_TYPE, String.valueOf(buildType)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            packageType = Integer.valueOf(inputs.getProperty(Flags.SMARTSQL_PACKAGE_TYPE, String.valueOf(packageType)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        makeDirty();
    }

    public boolean isEvaluationTimeOver() {
//        try {
//            if (days < 0) {
//                LocalDate deadTime = LocalDate.parse(getFirstTimeStartUpDate()).plusDays(keyDays),
//                        onTime = LocalDate.parse(getLastAppOpenDate());
//                Period p = onTime.until(deadTime);
//                days = (p.getYears() * 365) + (p.getMonths() * 30) + (p.getDays());
//            }
//        } finally {
//            return days < 1;
//        }
        return false;
    }

    public final AbstractMap.SimpleEntry<Boolean, String> getEvaluationStatus_Message() {

        if (lKey.isEmpty() && Integer.valueOf(keyDays).equals(Integer.valueOf(-1))) {
            return new AbstractMap.SimpleEntry(Boolean.FALSE, "YOU HAVE NO VALID LICENSE KEY...");
        } else if (Integer.valueOf(keyDays).equals(Integer.valueOf(-1))) {
            return new AbstractMap.SimpleEntry(Boolean.TRUE, "UN-FORSEEN ERROR DETECTED... PLEASE WAIT WHILE WE FIX-(INTERNET NEEDED)");
        } else {
            return new AbstractMap.SimpleEntry(Boolean.FALSE, "LOOKS LIKE YOUR TRIAL VERSION IS UP.. CONSIDER BUYING A LICENSE");
        }
    }

    public void setAppSize(long appSize) {
        this.appSize = appSize;
        makeDirty();
    }

    public void setAppHash(String appHash) {
        this.appHash = appHash;
        makeDirty();
    }

    public void setVersionFeatures(String versionFeatures) {
        this.versionFeatures = versionFeatures;
        makeDirty();
    }

    public void setAppVersion(String appVersion) {
        this.version = appVersion;
        makeDirty();
    }

    @Override
    public String toString() {
        if (forceConvert || smartSqlDataStringtype == null || smartSqlDataStringtype.isEmpty()) {
            smartSqlDataStringtype = gson.toJson(SmartsqlSavedData.this, SmartsqlSavedData.class);
            forceConvert = false;
        }
        return smartSqlDataStringtype;
    }

    public boolean isPaid() {
        return "free".equalsIgnoreCase(keyType) || Paid;
    }

    public Map<String, String> getDependencyHashes() {
        if (dependencyHashes == null) {
            dependencyHashes = getHashMap();
        }
        return dependencyHashes;
    }

    public Map<String, String> getDependencyDirectories() {
        if (dependencyDirectories == null) {
            dependencyDirectories = getHashMap();
        }
        return dependencyDirectories;
    }

    private final HashMap<String, String> getHashMap() {
        return new HashMap<String, String>(100) {
            @Override
            public String put(String key, String value) {
                makeDirty();
                return super.put(key, value);
            }

            @Override
            public String putIfAbsent(String key, String value) {
                makeDirty();
                return super.putIfAbsent(key, value);
            }

            @Override
            public void clear() {
                makeDirty();
                super.clear();
            }
        };
    }
}
