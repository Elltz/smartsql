package np.com.ngopal.smart.sql.structure.queryutils;

import javafx.scene.control.TablePosition;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class FindCodeAreaData
{
    public enum FindDirection
    {
        UP,
        DOWN
    };
    
    @Getter
    private final String findText;
    @Getter
    private final FindDirection direction;
    @Getter
    private final boolean matchCase;
    @Getter
    private final boolean matchWholeWord;
    
    @Getter
    @Setter
    private int lastPosition = -1;
    
    @Getter
    @Setter
    private int foundedLength = -1;
    
    @Getter
    @Setter
    private int curretPosition = -1;
    
    @Getter
    private boolean selectingRange = false;
    
    @Getter
    @Setter
    private TablePosition tablePosition;
    
    
    public FindCodeAreaData(String findText, FindDirection direction, boolean matchCase, boolean matchWholeWord, boolean selectingRange, int currentPosition)
    {
        this.findText = findText;
        this.direction = direction;
        this.matchCase = matchCase;
        this.matchWholeWord = matchWholeWord;
        this.selectingRange = selectingRange;
        this.curretPosition = currentPosition;
    }
    
    public FindCodeAreaData(String findText, FindDirection direction, boolean matchCase, boolean matchWholeWord, boolean selectingRange, TablePosition tablePosition)
    {
        this.findText = findText;
        this.direction = direction;
        this.matchCase = matchCase;
        this.matchWholeWord = matchWholeWord;
        this.selectingRange = selectingRange;
        this.tablePosition = tablePosition;
    }
}
