package np.com.ngopal.smart.sql.structure.controller;

import com.sun.javafx.charts.Legend;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Popup;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ProfilerChartController extends MetricEntityContainerController {

    private GSChart chart;
    private XYChart<Number, Number> gsChart;
    private NumberAxis xAxis;
    private NumberAxis yAxis;
    private Rectangle rectangle;
    private ProfilerChartControllerEventHandler chartControllerEventHandler;
    private final int profilerChartXAxisDivisor = 5;
    private double initStart = -1;
    private double initFinish = -1;

    private final SimpleIntegerProperty currentScaleindex
            = new SimpleIntegerProperty(Flags.DEFAULT_SCALE_INDEX);

    private List<Circle> effectedNodes = new ArrayList<>();
    private Line mouseLine = new Line();
    private Popup popup = new Popup();
    private Label popupLabel = new Label();
    private Map<XYChart.Series<Number, Number>, Label> popupValueLabels = new HashMap<>();
    
    private Map<XYChart.Series<Number, Number>, String> seriesColors;
            
    private Label toggledLegend;
    private static final String CSS_TOGGLED_LEGEND = "profilerChartToggledLegend";
    
    private final SimpleDateFormat popupFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public SimpleIntegerProperty currentScaleIndex() {
        return currentScaleindex;
    }
    
    public void minimize() {
        gsChart.setTitle("");
        
        gsChart.setMinHeight(50);
        gsChart.setPrefHeight(50);
        gsChart.setMaxHeight(50);
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        createEntityAsModule();
        this.rectangle = new Rectangle();
        getUI().getChildren().add(rectangle);
        long time = new Date().getTime();
        long value = ChartScale.values()[Flags.DEFAULT_SCALE_INDEX].getValue();

        xAxis = new NumberAxis(time, time + value, value / profilerChartXAxisDivisor);
        yAxis = new NumberAxis();
        yAxis.setUpperBound(200);

        xAxis.setTickLabelFormatter(new StringConverter<Number>() {
            private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

            @Override
            public String toString(Number t) {
                return dateFormat.format(new Date(t.longValue()));
            }

            @Override
            public Number fromString(String string) {
                try {
                    return dateFormat.parse(string).getTime();
                } catch (ParseException ex) {
                    //log.error("Error", ex);
                }

                return 0;
            }
        });
        xAxis.setTickLabelRotation(0);
        initDefaultContextItems();
    }

    public void insertGSChart(GSChart chart) {
        insertGSChart(chart, false);
    }
    
    public void insertGSChart(GSChart chart, boolean mimized) {
        if (this.chart != null) {
            return;
        }
        this.chart = chart;
        
        initChart(mimized);
        gsChart.setTitle(representableName());
    }

    private void initChart(boolean minimized) {
        
        List<XYChart.Series<Number, Number>> list = FXCollections.observableArrayList();
        if (gsChart != null) {
            gsChart.setOnMousePressed(null);
            gsChart.setOnMouseReleased(null);
            gsChart.setOnMouseDragged(null);
            list.addAll(gsChart.getData());
            gsChart = null;
        }

        if (GSChart.CHART_STYLE.AREA.name().equalsIgnoreCase(chart.getChartStyle())) {
            gsChart = new AreaChart<Number, Number>(xAxis, yAxis) {
                @Override
                protected void dataItemAdded(XYChart.Series<Number, Number> series, int itemIndex, XYChart.Data<Number, Number> item) {
                    //super.dataItemAdded(series, itemIndex, item);
                }
                
            };
            ((AreaChart) gsChart).setCreateSymbols(false);

        } else if (GSChart.CHART_STYLE.LINE.name().equalsIgnoreCase(chart.getChartStyle())) {
            gsChart = new LineChart<Number, Number>(xAxis, yAxis) {
                @Override
                protected void dataItemAdded(XYChart.Series<Number, Number> series, int itemIndex, XYChart.Data<Number, Number> item) {
                    //super.dataItemAdded(series, itemIndex, item);
                }
                
            };
            ((LineChart) gsChart).setCreateSymbols(false);
        } else {
            gsChart = new AreaChart<Number, Number>(xAxis, yAxis) {
                @Override
                protected void dataItemAdded(XYChart.Series<Number, Number> series, int itemIndex, XYChart.Data<Number, Number> item) {
                    //super.dataItemAdded(series, itemIndex, item);
                }
                
            };
            ((AreaChart) gsChart).setCreateSymbols(false);
        }
        
        if (!minimized) {
            gsChart.setMinHeight(250);
            gsChart.setMaxHeight(250);
            gsChart.setPrefHeight(250);
        }

        gsChart.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);
        //gsChart.setPadding(new Insets(5));
        applyColors();
        
        gsChart.getData().addAll(list);  
        
        gsChart.setAnimated(false);

        setContent(null);
        
        AnchorPane pane = new AnchorPane();
        pane.getChildren().add(gsChart);
        
        AnchorPane.setBottomAnchor(gsChart, 0.0);
        AnchorPane.setLeftAnchor(gsChart, 0.0);
        AnchorPane.setTopAnchor(gsChart, 0.0);
        AnchorPane.setRightAnchor(gsChart, 0.0);
        
        setContent(pane);
        
        gsChart.setTitle(representableName());
        
        gsChart.setOnMouseMoved((MouseEvent event) -> {
                        
            for (Circle efeNode: effectedNodes) {
                ((Group)efeNode.getParent()).getChildren().remove(efeNode);
            }
                        
            effectedNodes.clear();
            
//            log.error("0-0");
//            if (popup.isShowing()) {
//                popup.hide(); 
//                log.error("0-1");
//            }
            
            Map<XYChart.Series<Number, Number>, XYChart.Data<Number, Number>> dataForShow = collectDataForShow(event.getSceneX(), event.getSceneY());
            if (dataForShow != null && !dataForShow.isEmpty()) {

                boolean first = true;
                for (Map.Entry<XYChart.Series<Number, Number>, XYChart.Data<Number, Number>> entry: dataForShow.entrySet()) {

                    XYChart.Series<Number, Number> series = entry.getKey();
                    XYChart.Data<Number, Number> data = entry.getValue();
                                        
                    if (first) {
                        first = false;
                        popupLabel.setText(popupFormat.format(new Date(data.getXValue().longValue())));                        
                    }
                    
                    Label valueLabel = popupValueLabels.get(series);
                    if (valueLabel != null) {
                        if (data.getYValue().longValue() < data.getYValue().doubleValue()) {
                            Double v = Double.isNaN(data.getYValue().doubleValue()) ? 0.0 : data.getYValue().doubleValue();
                            valueLabel.setText(new BigDecimal(v).setScale(2, RoundingMode.HALF_UP).toString());
                        } else {
                            valueLabel.setText(String.valueOf(data.getYValue().longValue()));
                        }
                    }
                    
                    double x = xAxis.getDisplayPosition(data.getXValue());
                    
                    Circle c = new Circle(
                        x, 
                        yAxis.getDisplayPosition(data.getYValue()), 
                        4
                    );
                    
                    c.setStroke(Color.RED);
                    c.setStrokeWidth(1.5);
                    c.setFill(Color.TRANSPARENT);
                    
                    try {
                        c.setStroke(Color.web(seriesColors.get(series)));
                    } catch (Throwable th) {
                        // TODO: what we will do with random colors?
                    }
                    
                    ((Group)series.getNode()).getChildren().add(c);
                    effectedNodes.add(c);
                }  
                                
                if (!gsChart.getData().isEmpty()) {
                    
                    double x = event.getX() - yAxis.getWidth() - 16;
                    
                    hoveredX.set(xAxis.getValueForDisplay(x).longValue());
                    hoveredOnChart.set(true);
                }
                                     
                popup.show(getController().getStage(), event.getScreenX() + 10, event.getScreenY() + 5);
            }
        });
        
        gsChart.setOnMouseExited((MouseEvent event) -> {
            if (popup.isShowing()) {
                popup.hide();
            }
            
            for (Circle efeNode: effectedNodes) {
                ((Group)efeNode.getParent()).getChildren().remove(efeNode);
            }
            
            hoveredOnChart.set(false);
            hoveredX.set(0l);
            
            effectedNodes.clear();
        });
        
        if (chart.getDescription() != null && !chart.getDescription().isEmpty()) {
            Label info = new Label();
            info.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getInfo_image()));
            
            Tooltip tooltip = new Tooltip(chart.getDescription());
            tooltip.getStyleClass().add("profilerChartInfoTooltip");
            info.setTooltip(tooltip);
            
            pane.getChildren().add(info);
            AnchorPane.setLeftAnchor(info, 0.0);
            AnchorPane.setTopAnchor(info, 0.0);
        }
    }
    
    
    private Map<XYChart.Series<Number, Number>, XYChart.Data<Number, Number>> collectDataForShow(double mouseX, double mouseY) {
        Map<XYChart.Series<Number, Number>, XYChart.Data<Number, Number>> result = new LinkedHashMap<>();
        
        Point2D mouseSceneCoords = new Point2D(mouseX, mouseY);
        double x = xAxis.sceneToLocal(mouseSceneCoords).getX();        
        long xValue = xAxis.getValueForDisplay(x).longValue();
        
        for (XYChart.Series<Number, Number> s: gsChart.getData()) {
            if (s.getData() != null) {
                
                XYChart.Data<Number, Number> bestData = null;
                for (XYChart.Data<Number, Number> d: s.getData()) {
                                        
                    if (bestData == null) {
                        bestData = d;                        
                    } else if (Math.abs(d.getXValue().longValue() - xValue) <= Math.abs(bestData.getXValue().longValue() - xValue)) {
                        bestData = d;
                    }
                }
                
                if (bestData != null) {
                    result.put(s, bestData);
                }
            }
        }
        
        return result;
    }
    
    
    private static String web2Rgba(String colorStr, double alpha) {
        return "rgba(" +
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ) + ", " +
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ) + ", " +
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) + ", " + alpha + ")";
    }

    private void applyColors() {
        for (XYChart.Series<Number, Number> s: gsChart.getData()) {
            String seriesColor = seriesColors.get(s);
            if (seriesColor != null) {
                
                Node fill = s.getNode().lookup(".chart-series-area-fill"); // only for AreaChart
                Node line = s.getNode().lookup(".chart-series-area-line");
                
                if (fill != null) {
                    fill.setStyle("-fx-fill: " + web2Rgba(seriesColor, 0.8) + ";");
                }
                
                if (line != null) {
                    line.setStyle("-fx-stroke: " + seriesColor + ";");
                }
            }
        }
        
        for (Node n: gsChart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {
                for(Legend.LegendItem legendItem: ((Legend)n).getItems()){
                   
                    for (XYChart.Series<Number, Number> s: gsChart.getData()) {
                        if (s.getName().equals(legendItem.getText())) {
                            String seriesColor = seriesColors.get(s);
                            legendItem.getSymbol().setStyle("-fx-background-color: " + seriesColor +", white;");
                        }
                    }
                }
            }
         }

        for (Node n : gsChart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {
                Legend l = (Legend) n;
                for (Legend.LegendItem li : l.getItems()) {
                    for (XYChart.Series<Number, Number> s : gsChart.getData()) {
                        
                        String seriesColor = seriesColors.get(s);
                        
                        if (s.getName().equals(li.getText())) {
                            Node symbol = l.getChildren().get(0).lookup(".chart-legend-item-symbol");
                            
                            if (symbol != null) {
                                symbol.setStyle("-fx-stroke: " + seriesColor + ";");
                            }

                        }
                    }
                }
            }
        }
    }

    public void init(Collection<XYChart.Series<Number, Number>> series, Map<XYChart.Series<Number, Number>, String> seriesColors) {
        this.seriesColors = seriesColors;
        
        gsChart.getData().addAll(series);
        applyColors();
        
        ((Group)gsChart.getData().get(0).getNode()).getChildren().add(mouseLine);
        mouseLine.setVisible(false);
        
        popupValueLabels.clear();
        
        popupLabel.getStyleClass().add("profilerChartDataTooltipTitleLabel");
        
        GridPane gp = new GridPane();
        gp.getStyleClass().add("profilerChartDataTooltip");
        gp.add(popupLabel, 0, 0, 2, 1);
        GridPane.setHalignment(popupLabel, HPos.CENTER);
        
        if (series != null) {
            int row = 1;
            for (XYChart.Series<Number, Number> s: series) {
                
                Line l = new Line(0, 15, 15, 15);
                l.setStrokeWidth(2);
                l.setStroke(Color.web(seriesColors.get(s)));
                
                Label title = new Label();
                title.getStyleClass().add("profilerChartDataTooltipLabel");
                title.setText(s.getName() + ": ");
                title.setGraphic(l);
                
                Label value = new Label();
                value.getStyleClass().add("profilerChartDataTooltipBoldLabel");
                
                gp.add(title, 0, row);
                GridPane.setHgrow(title, Priority.ALWAYS);
                
                gp.add(value, 1, row);
                GridPane.setHalignment(value, HPos.RIGHT);
                
                popupValueLabels.put(s, value);
                row++;
            }
        }
                
        for (Node n : gsChart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {                
                Legend l = (Legend) n;
                for (int i = 0; i < l.getItems().size(); i++) {
                    Label liNode = (Label) l.getChildren().get(i);
                    liNode.setCursor(Cursor.HAND);                            
                    liNode.setOnMouseClicked(me -> {
                        if (me.getButton() == MouseButton.PRIMARY) {
                            toggleSeries(liNode);
                        }
                    });
                }
            }
        }
        popup.getContent().clear();
        popup.getContent().add(gp);
    }

    private void toggleSeries(Label li) {
        
        boolean visible = false;
        if (toggledLegend == li) {
            visible = true;
            toggledLegend = null;
        } else {
            toggledLegend = li;
        }
        
        for (Node n : gsChart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {                
                Legend l = (Legend) n;
                for (int i = 0; i < l.getItems().size(); i++) {
                    
                    Label liNode = (Label) l.getChildren().get(i);
                    if (liNode == li) {
                        if (visible) {
                            liNode.getStyleClass().remove(CSS_TOGGLED_LEGEND);
                        } else {
                            if (!liNode.getStyleClass().contains(CSS_TOGGLED_LEGEND)) {
                                liNode.getStyleClass().add(CSS_TOGGLED_LEGEND);
                            }
                        }
                        
                    } else {                        
                        liNode.getStyleClass().remove(CSS_TOGGLED_LEGEND);
                    }
                }
            }
        }
        
        for (XYChart.Series<Number, Number> s: gsChart.getData()) {
            if (!s.getName().equals(li.getText())) {
                s.getNode().setVisible(visible);
                for (XYChart.Data<Number, Number> d : s.getData()) {
                    if (d.getNode() != null) {
                        d.getNode().setVisible(visible);
                    }
                }
            } else {
                s.getNode().setVisible(true);
                for (XYChart.Data<Number, Number> d : s.getData()) {
                    if (d.getNode() != null) {
                        d.getNode().setVisible(true);
                    }
                }
            }
        }
    }
    
    public void clear() {
        initStart = -1;
        initFinish = -1;

        for (XYChart.Series<Number, Number> series : gsChart.getData()) {
            series.getData().clear();
        }

        gsChart.getData().clear();
    }

    public void updateChart(boolean showAll, SimpleStringProperty zoomCountStage, Long firstRunDate, Long dragedStart, Long dragedFinish) {

        if (firstRunDate != null) {

            long lower;
            long currentTime = new Date().getTime();
            long upper = currentTime;

            if (initStart == -1 && initFinish == -1) {

                long runDate = firstRunDate;
                if (gsChart.getData() != null && !gsChart.getData().isEmpty()) {
                    XYChart.Series<Number, Number> s = gsChart.getData().get(0);
                    if (s.getData() != null && !s.getData().isEmpty()) {
                        runDate = s.getData().get(0).getXValue().longValue();
                        currentTime = s.getData().get(s.getData().size() - 1).getXValue().longValue();
                    }
                }

                if (showAll) {
                    lower = runDate;
                    if (zoomCountStage != null) {
                        zoomCountStage.set("Show All");
                    }

                } else if (dragedStart != null && dragedFinish != null) {
                    lower = dragedStart;
                    upper = dragedFinish;

                } else {
                    ChartScale currentScale = ChartScale.values()[currentScaleindex.get()];
                    
                    upper = currentTime;
                    lower = upper - currentScale.getValue();
                    
                    if (zoomCountStage != null) {
                        zoomCountStage.set(currentScale.getName());
                    }
                }
            } else if (dragedStart != null && dragedFinish != null) {
                lower = dragedStart;
                upper = dragedFinish;
            } else if (showAll) {
                lower = (long) initStart;
                upper = (long) initFinish;

                if (zoomCountStage != null) {
                    zoomCountStage.set("Show All");
                }
            } else {

                ChartScale currentScale = ChartScale.values()[currentScaleindex.get()];
                upper = currentTime;
                lower = upper - currentScale.getValue();
                
                if (zoomCountStage != null) {
                    zoomCountStage.set(currentScale.getName());
                }
            }
            
            xAxis.setLowerBound(lower);
            xAxis.setUpperBound(upper);
            xAxis.setTickUnit((upper - lower) / profilerChartXAxisDivisor);
            
            yAxis.setUpperBound(200);
        }
    }

    public double getLower() {
        return xAxis.getLowerBound();
    }

    public double getUpper() {
        return xAxis.getUpperBound();
    }

    public void initXAxis(double upper, double lower) {
        initFinish = upper;
        initStart = lower;
    }

    public GSChart getChart() {
        return chart;
    }
    
    private final BooleanProperty hoveredOnChart = new SimpleBooleanProperty(false) {
        @Override
        protected void invalidated() {
            super.invalidated();
            
            drawLine();
        }
        
    };
    public BooleanProperty hoveredOnChart() {
        return hoveredOnChart;
    }
    
    private final ObjectProperty<Long> hoveredX = new SimpleObjectProperty<Long>(0l) {        
        @Override
        protected void invalidated() {
            super.invalidated();
            
            drawLine();
        }        
    };
    public ObjectProperty<Long> hoveredX() {
        return hoveredX;
    }
    
    private void drawLine() {
        if (hoveredOnChart.get()) {
            mouseLine.setStroke(Color.RED);
            mouseLine.setStrokeWidth(1);
            mouseLine.setStartX(xAxis.getDisplayPosition(hoveredX.get()));
            mouseLine.setStartY(0);
            mouseLine.setEndX(xAxis.getDisplayPosition(hoveredX.get()));
            mouseLine.setEndY(yAxis.getHeight());
        }

        mouseLine.setVisible(hoveredOnChart.get());
    }

    public void setChartControllerEventHandler(ProfilerChartControllerEventHandler chartControllerEventHandler) {
        if(chartControllerEventHandler != null && gsChart.getOnMousePressed() != chartControllerEventHandler){
            // Drag period functionality
            gsChart.setOnMousePressed(chartControllerEventHandler);
            gsChart.setOnMouseReleased(chartControllerEventHandler);
            gsChart.setOnMouseDragged(chartControllerEventHandler);
        }
        
        if(this.chartControllerEventHandler != chartControllerEventHandler && chartControllerEventHandler != null){
            this.chartControllerEventHandler = chartControllerEventHandler;
            this.chartControllerEventHandler.xAxis = xAxis;
            this.chartControllerEventHandler.rectangle = rectangle;
            this.chartControllerEventHandler.chart = gsChart;
        }
    }

    @Override
    public String representableName() {
        return chart == null ? "" : chart.getChartName();
    }
    
    private ListChangeListener<XYChart.Data<Number, Number>> getSerieListener(XYChart.Series<Number, Number> serie) {
        return new ListChangeListener<XYChart.Data<Number, Number>>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends XYChart.Data<Number, Number>> c) {
                if (c.next() && c.wasAdded()) {
                    Platform.runLater(() -> {
                        Recycler.addWorkProc(new WorkProc() {
                            @Override
                            public void updateUI() {}

                            @Override
                            public void run() {
                                handleDataAdditionAndManipulation(serie);
                            }
                        });
                    });
                }
            }
        };
    }
    
    private void handleDataAdditionAndManipulation(XYChart.Series<Number, Number> serie) {
        long maxTime = 0l;
        long lowerTime = 0l;
        int deleteSuffix = -1;

        if (serie.getData() != null && !serie.getData().isEmpty()) {
            maxTime = serie.getData().get(serie.getData().size() - 1).getXValue().longValue();

            for (int i = 0; i < serie.getData().size(); i++) {
                long temp = serie.getData().get(i).getXValue().longValue();
                long tempDif = maxTime - getTimeUnitSpaceForChart().toMillis(1);
                if (tempDif > temp) {
                    deleteSuffix = i;
                    lowerTime = temp;
                }else{ break; }
            }
        }

        if (deleteSuffix > -1) {
            final int finalDeleteSuffix = deleteSuffix;
            long finalLowerTime = lowerTime;
            Platform.runLater(() -> {
                if (serie.getData().size() >= finalDeleteSuffix) {
                    serie.getData().remove(0, finalDeleteSuffix);
                }
                xAxis.setLowerBound(finalLowerTime);
            });
        }

    }
    
    private TimeUnit getTimeUnitSpaceForChart(){
        return TimeUnit.MINUTES;
    }

}
