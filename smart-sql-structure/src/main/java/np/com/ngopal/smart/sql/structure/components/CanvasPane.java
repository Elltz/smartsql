package np.com.ngopal.smart.sql.structure.components;

import com.sun.javafx.tk.Toolkit;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CanvasPane extends Region {

    @Setter
    private VisualExplainController parentController;
    
    private Canvas delegated;

    @Getter
    @Setter
    private double canvasTranslateX = 0;
    @Getter
    @Setter
    private double canvasTranslateY = 0;

    public CanvasPane() {
        setStyle("-fx-background-color: white");
    }

    public void setDelegated(Canvas delegated) {
        this.delegated = delegated;
        delegated.widthProperty().addListener(observable -> draw());
        delegated.heightProperty().addListener(observable -> draw());
        getChildren().add(delegated);
    }

    public Canvas getDelegated() {
        return delegated;
    }
    

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double width = getWidth();
        final double height = getHeight();
        final Insets insets = getInsets();
        final double contentX = insets.getLeft();
        final double contentY = insets.getTop();
        final double contentWith = Math.max(0, width - (insets.getLeft() + insets.getRight()));
        final double contentHeight = Math.max(0, height - (insets.getTop() + insets.getBottom()));
        delegated.relocate(contentX, contentY);
        delegated.setWidth(contentWith);
        delegated.setHeight(contentHeight);
    }

    public void draw() {
        final double width = delegated.getWidth();
        final double height = delegated.getHeight();

        final GraphicsContext gc = delegated.getGraphicsContext2D();

        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, width, height);

        gc.save();
        
        double scale = parentController.getScale().get();
        gc.scale(scale, scale);
        gc.translate(canvasTranslateX / scale, canvasTranslateY / scale);

        Pair<Double, Double> minMaxX = getMinMaxX(parentController.getVisualTables());

        gc.translate((width / 2 - (minMaxX.getValue() - minMaxX.getKey()) / 2 - 80) / scale, 200 / scale);

        if (parentController.getVisualTables() != null) {
            for (VisualTable t : parentController.getVisualTables()) {
                drawTable(gc, t);
            }
        }
        gc.restore();
    }
    
    private void drawTable(GraphicsContext gc, VisualTable table) {
                
        String text;
        Color textColor;
        switch (table.type) {
            case FULL:
            case INDEX:
                gc.setFill(Color.RED);
                text = table.type.toString();
                textColor = Color.WHITE;
                break;
                
            case QUERY:
                gc.setFill(Color.LIGHTGREY);
                text = table.type.toString().trim() + table.name;
                textColor = Color.BLACK;
                break;
                
            case SYSTEM:
            case CONST:
                gc.setFill(Color.BLUE);
                text = table.type.toString();
                textColor = Color.WHITE;
                break;
                
            case FULLTEXT:
                gc.setFill(Color.YELLOW);
                text = table.type.toString();
                textColor = Color.BLACK;
                break;
                
            case UNIQUE_SUBQUERY:
            case INDEX_SUBQUERY:
            case RANGE:
                gc.setFill(Color.web("ff5223"));
                text = table.type.toString();
                textColor = Color.WHITE;
                break;
                
            case NON_UNIQUE:
            case UNIQUE:
            case REF_OR_NULL:
            case INDEX_MERGE:
                gc.setFill(Color.GREEN);
                text = table.type.toString();
                textColor = Color.WHITE;
                break;
                
            case UNION:
                gc.setFill(Color.LIGHTGREY);
                text = table.type.toString();
                textColor = Color.BLACK;
                break;
                
            default:
                gc.setFill(Color.WHITE);
                text = table.type.toString();
                textColor = Color.BLACK;
                break;
        }
        
        gc.setLineWidth(1);
        gc.setStroke(Color.BLACK);
        
        if (table.insideTables == null || table.insideTables.isEmpty()) {
            // draw rect
            gc.fillRect(table.x, table.y, table.width, table.height);
            gc.strokeRect(table.x, table.y, table.width, table.height);

            // draw rect type
            gc.setFill(textColor);
            gc.setFont(Font.font("Arial", FontWeight.NORMAL, VisualExplainController.BASE_FONT_SIZE));

            double textWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(text, gc.getFont());
            double textHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

            gc.fillText(text, table.x + table.width / 2 - textWidth / 2, table.y + table.height / 2 + textHeight / 2 - 2);

            if (VisualTable.TableType.QUERY != table.type) {         
                // draw table name
                gc.setFill(Color.BLACK);
                gc.setFont(new Font("Arial", 8));

                double nameWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(table.name, gc.getFont());
                double nameHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

                double y = table.y + table.height + nameHeight;
                gc.fillText(table.name, table.x + table.width / 2 - nameWidth / 2, y);

                // draw key name
                gc.setFill(Color.BLACK);
                gc.setFont(Font.font("Arial", FontWeight.BOLD, 8));

                double keyWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(table.key, gc.getFont());
                double keyHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

                gc.fillText(table.key, table.x + table.width / 2 - keyWidth / 2, y + keyHeight);

                if (table.rowsNumber != null) {
                    // draw rows name
                    gc.setFill(Color.BLACK);
                    gc.setFont(new Font("Arial", 8));

                    String rowsText = table.rowsNumber + " rows";

                    double rowsHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

                    gc.fillText(rowsText, table.x + table.width / 2 + 3, table.y - rowsHeight / 2);
                }
            }
            else if (table.extra != null)
            {
                // draw table name
                gc.setFill(Color.BLACK);
                gc.setFont(new Font("Arial", 8));

                double extraWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(table.extra, gc.getFont());
                double extraHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

                double y = table.y + table.height + extraHeight;
                gc.fillText(table.extra, table.x + table.width / 2 - extraWidth / 2, y);
            }
            
        } else {            
            gc.setFill(((Color)gc.getFill()).desaturate());
                        
            // draw rect
            gc.fillRect(table.x, table.y, table.width, table.height);
            gc.strokeRect(table.x, table.y, table.width, table.height);
            
            // draw rect type
            gc.setFill(textColor);
            gc.setFont(Font.font("Arial", FontWeight.NORMAL, VisualExplainController.BASE_FONT_SIZE));

            double textWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(text, gc.getFont());
            double textHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

            gc.fillText(text, table.x + table.width / 2 - textWidth / 2, table.y + table.height / 2 + textHeight / 2 - 2);
            
            
            gc.setFill(Color.LIGHTGREY);
            // draw rect
            gc.fillRect(table.x, table.y + table.height, table.width, table.height);
            
            // draw table name
            gc.setFill(Color.BLACK);
            gc.setFont(Font.font("Arial", FontWeight.NORMAL, VisualExplainController.BASE_FONT_SIZE));

            double nameWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(table.name, gc.getFont());
            double nameHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();

            gc.fillText(table.name, table.x + table.width / 2 - nameWidth / 2, table.y + table.height + table.height / 2 + nameHeight / 2 - 2);
            
            double insideHeight = 0;
            // draw insed table
            for (VisualTable t: table.insideTables) {
                
                gc.save();
                gc.translate(VisualExplainController.COMPONENT_SHIFT_X / 4, 10);
                
                drawTable(gc, t);
                insideHeight = Math.max(insideHeight, t.height);
                
                gc.restore();
            }
            
            gc.setStroke(Color.GREY);
            gc.setLineDashes(5d, 5d);
            gc.strokeRect(table.x, table.y + table.height, table.width, table.y + table.height + insideHeight + VisualExplainController.COMPONENT_SHIFT_Y + 10);
            gc.setLineDashes(null);
        }
            
        
        double lineWidth = 1;
        if (table.rowsNumber != null) {
            if (table.rowsNumber > 1000) {
                lineWidth = 2;
            } else if (table.rowsNumber > 15000) {
                lineWidth = 3;
            }
        }

        if (table.groupBy != null || table.orderBy != null) {
            
            double x = -1;
            double y = -1;
            
            if (table.prevComponent instanceof VisualJoin) {
                VisualJoin vj = (VisualJoin)table.prevComponent;
                x = vj.x + parentController.getJoinWidth() / 2;
                y = vj.y - parentController.getJoinHeight() / 2;
            } else if (table.prevComponent instanceof VisualTable) {
                VisualTable vt = (VisualTable)table.prevComponent;
                x = vt.x + vt.width / 2;
                y = vt.y;
            }
            
            if (table.groupBy != null) {
                drawGroupBy(gc, x, y, table, table.groupBy, table.orderBy);
            }
            
            if (table.orderBy != null) {
                drawOrderBy(gc, x, y, table, table.orderBy, table.groupBy);
            }
            
        } else if (table.prevComponent instanceof VisualJoin) {            
            drawJoin(gc, table, (VisualJoin) table.prevComponent, lineWidth);
        } else if (table.prevComponent instanceof VisualTable) {            
            
            VisualTable vt = (VisualTable)table.prevComponent;
            double x = vt.x + vt.width / 2;
            double y = vt.y;
            drawArrow(gc, x, y, table.x + table.width / 2, table.y + table.height, false);
        }
        
        
        // if union
        if (table instanceof VisualUnion) {
            
            VisualUnion union = (VisualUnion)table;
            
            gc.translate(VisualExplainController.REC_SHIFT_X, parentController.getUnionHeight() + VisualExplainController.COMPONENT_SHIFT_Y);
            for (VisualTable vt: union.tables) {
                drawTable(gc, vt);
                
                drawArrow(gc, vt.x + vt.width / 2, vt.y, vt.x + vt.width / 2, union.y - VisualExplainController.COMPONENT_SHIFT_Y, false);
            }
            gc.translate(- VisualExplainController.REC_SHIFT_X, - (parentController.getUnionHeight() + VisualExplainController.COMPONENT_SHIFT_Y));
        }
    }
    
    
    private void drawOrderBy(GraphicsContext gc, double lineX, double lineY, VisualTable queryTable, VisualOrderBy orderBy, VisualGroupBy groupBy) {        
        gc.setLineWidth(2);
        gc.setStroke(orderBy.color);
        
        // draw rect
        gc.strokeRoundRect(orderBy.x, orderBy.y, orderBy.width, orderBy.height, 8, 8);
        
        // draw rect type0
        gc.setFill(Color.BLACK);
        gc.setFont(new Font("Arial", VisualExplainController.BASE_FONT_SIZE));
        
        double nameWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(orderBy.name, gc.getFont());
        double nameHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();
        
        gc.fillText(orderBy.name, orderBy.x + orderBy.width / 2 - nameWidth / 2, orderBy.y + orderBy.height / 2 + nameHeight / 2 - 2);
        
        if (orderBy.key != null) {
            
            gc.setFont(Font.font("Arial", FontWeight.BOLD, 8));
            
            double keyWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(orderBy.key, gc.getFont());
            double keyHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();
            
            gc.fillText(orderBy.key, orderBy.x + orderBy.width / 2 - keyWidth - 5, orderBy.y + orderBy.height +  keyHeight);
        }
        
        gc.setLineWidth(2);
        gc.setStroke(Color.BLACK);
        
        double x = orderBy.x + orderBy.width / 2;
        double y = orderBy.y + orderBy.height;
        
        // if groupBy no exists draw arrow from last component to ORDER BY
        if (groupBy == null) {
            drawArrow(gc, lineX, lineY - 2, x, y, false);
        } else {
            // else we must draw arrow from groupBy to orderBy
            drawArrow(
                gc, 
                groupBy.x + groupBy.width + 2, groupBy.y + groupBy.height / 2, 
                orderBy.x, orderBy.y + groupBy.height / 2, 
                true
            );
        }
        
        // draw arrow to query table
        drawArrow(gc, x, orderBy.y, queryTable.x + queryTable.width / 2, queryTable.y + queryTable.height, false);
    }
    
    private void drawGroupBy(GraphicsContext gc, double lineX, double lineY, VisualTable queryTable, VisualGroupBy groupBy, VisualOrderBy orderBy) {        
        gc.setLineWidth(2);
        gc.setStroke(groupBy.color);
        
        // draw rect
        gc.strokeRoundRect(groupBy.x, groupBy.y, groupBy.width, groupBy.height, 8, 8);
        
        // draw rect type
        gc.setFill(Color.BLACK);
        gc.setFont(new Font("Arial", VisualExplainController.BASE_FONT_SIZE));
        
        double nameWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(groupBy.name, gc.getFont());
        double nameHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();
        
        gc.fillText(groupBy.name, groupBy.x + groupBy.width / 2 - nameWidth / 2, groupBy.y + groupBy.height / 2 + nameHeight / 2 - 2);
        
        if (groupBy.key != null) {
            
            gc.setFont(Font.font("Arial", FontWeight.BOLD, 8));
            
            double keyWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(groupBy.key, gc.getFont());
            double keyHeight = (int) Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont()).getLineHeight();
            
            gc.fillText(groupBy.key, groupBy.x + groupBy.width / 2 - keyWidth - 5, groupBy.y + groupBy.height + keyHeight);
        }
        
        gc.setLineWidth(2);
        gc.setStroke(Color.BLACK);
        
        double x = groupBy.x + groupBy.width / 2;
        double y = groupBy.y + groupBy.height;
        
        // draw arrow from last component to GROUP BY
        drawArrow(gc, lineX, lineY, x, y, false);
        
        // if orderBy not exist need draw arrow to query table
        if (orderBy == null) {
            drawArrow(gc, x, groupBy.y, queryTable.x + queryTable.width / 2, queryTable.y + queryTable.height, false);
        }
    }
    
    private void drawJoin(GraphicsContext gc, VisualTable visualTable, VisualJoin visualJoin, double lineWidth) {
       
        if (visualTable.type == VisualTable.TableType.QUERY) {
            
            // its last arrow with query table
            double lineX0 = visualTable.x + visualTable.width / 2;
            double lineY0 = visualTable.y + visualTable.height;

            gc.setLineWidth(lineWidth);
            drawArrow(gc, visualJoin.x + parentController.getJoinWidth() / 2, visualJoin.y - parentController.getJoinHeight() / 2, lineX0, lineY0, false);

        } else {
            // draw rhombus
            gc.setLineWidth(2);
            gc.setStroke(Color.GRAY);
            gc.strokePolyline(
                new double[]{visualJoin.x, visualJoin.x + parentController.getJoinWidth() / 2, visualJoin.x + parentController.getJoinWidth(), visualJoin.x + parentController.getJoinWidth() / 2, visualJoin.x},
                new double[]{visualJoin.y, visualJoin.y - parentController.getJoinHeight() / 2, visualJoin.y, visualJoin.y + parentController.getJoinHeight() / 2, visualJoin.y},
                5);

            // draw rhombus text
            gc.setFill(Color.BLACK);
            gc.setStroke(Color.BLACK);
            gc.setFont(new Font("Arial", VisualExplainController.BASE_FONT_SIZE));

            double nameWidth = (int) Toolkit.getToolkit().getFontLoader().computeStringWidth(VisualExplainController.JOIN_TEXT_0, gc.getFont());

            double x = visualJoin.x + parentController.getJoinWidth() / 2 - nameWidth / 2;
            double y = visualJoin.y;

            gc.fillText(VisualExplainController.JOIN_TEXT_0 + VisualExplainController.JOIN_TEXT_1, x, y);

            // draw arrows

            VisualTable table = visualJoin.join instanceof VisualTable ? (VisualTable) visualJoin.join : null;
            VisualJoin join = visualJoin.join instanceof VisualJoin ? (VisualJoin) visualJoin.join : null;

            // first arrow
            double lineX = visualTable.x + visualTable.width / 2;
            double lineY = visualTable.y;

            gc.setLineWidth(lineWidth);
            drawArrow(gc, lineX, lineY, visualJoin.x + parentController.getJoinWidth() / 2, visualJoin.y + parentController.getJoinHeight() / 2, false);

            // second arrow
            if (table != null) {
                double lineX0 = table.x + table.width / 2;
                double lineY0 = table.y;

                double lineX1 = lineX0;
                double lineY1 = visualJoin.y;

                double lineX2 = visualJoin.x;
                double lineY2 = visualJoin.y;
                
                lineWidth = 1;
                if (table.rowsNumber != null) {
                    if (table.rowsNumber > 1000) {
                        lineWidth = 2;
                    } else if (table.rowsNumber > 15000) {
                        lineWidth = 3;
                    }
                }
                
                gc.strokeLine(lineX0, lineY0, lineX1, lineY1);
                drawArrow(gc, lineX1, lineY1, lineX2, lineY2, true);

            } else if (join != null) {
                double lineX0 = join.x + parentController.getJoinWidth();
                double lineY0 = join.y;

                drawArrow(gc, lineX0, lineY0, visualJoin.x, visualJoin.y, true);
            }
        }
    }
    
    
    private void drawArrow(GraphicsContext gc, double x0, double y0, double x1, double y1, boolean horizontal) {
        
        gc.setStroke(Color.BLACK);
        gc.strokeLine(x0, y0, horizontal ? x1 - 3 : x1, horizontal ? y1 : y1 + 3);
        
        gc.setFill(Color.BLACK);
        if (horizontal) {
            gc.fillPolygon(
                new double[]{x1 - 7, x1, x1 - 7, x1 - 7},
                new double[]{y1 - 4, y1, y1 + 4, y1 - 4},
                4);
        } else {
            gc.fillPolygon(
                new double[]{x1 - 4, x1, x1 + 4, x1 - 4},
                new double[]{y1 + 7, y1, y1 + 7, y1 + 7},
                4);
        }
    }

    private Pair<Double, Double> getMinMaxX(List<VisualTable> tables) {

        double maxX = 0;
        double minX = 0;
        if (tables != null) {

            for (VisualTable t : tables) {

                double x0 = t.x;
                if (minX > x0) {
                    minX = x0;
                }

                double x1 = t.x + t.width;
                if (maxX < x1) {
                    maxX = x1;
                }

                if (t.groupBy != null) {
                    x0 = t.groupBy.x;
                    if (minX > x0) {
                        minX = x0;
                    }

                    x1 = t.groupBy.x + t.groupBy.width;
                    if (maxX < x1) {
                        maxX = x1;
                    }
                }

                if (t.orderBy != null) {
                    x0 = t.orderBy.x;
                    if (minX > x0) {
                        minX = x0;
                    }

                    x1 = t.orderBy.x + t.orderBy.width;
                    if (maxX < x1) {
                        maxX = x1;
                    }
                }

                if (t.insideTables != null) {
                    Pair<Double, Double> x = getMinMaxX(t.insideTables);

                    if (minX > x.getKey()) {
                        minX = x.getKey();
                    }

                    if (maxX < x.getValue()) {
                        maxX = x.getValue();
                    }
                }
            }
        }

        return new Pair<>(minX, maxX);
    }

    public double clamp(double value, double min, double max) {

        if (Double.compare(value, min) < 0) {
            return min;
        }

        if (Double.compare(value, max) > 0) {
            return max;
        }

        return value;
    }
}
