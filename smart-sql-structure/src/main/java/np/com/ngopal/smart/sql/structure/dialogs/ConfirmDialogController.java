package np.com.ngopal.smart.sql.structure.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com);
 */
public class ConfirmDialogController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label titleLabel;
    @FXML
    private Button yesToAllButton;
    @FXML
    private Button noToAllButton;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
    public void init(String titleText, boolean hasMultiButtons) {
        titleLabel.setText(titleText); 
        yesToAllButton.setVisible(hasMultiButtons);
        noToAllButton.setVisible(hasMultiButtons);
        yesToAllButton.setManaged(hasMultiButtons);
        noToAllButton.setManaged(hasMultiButtons);
        calculateHeightFlow(titleLabel, 400.0);
    }
    
    @FXML
    public void yesAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
    
    @FXML
    public void yesToAllAction(ActionEvent event) {
        responce = DialogResponce.YES_TO_ALL;
        getStage().close();
    }
    
    @FXML
    public void noAction(ActionEvent event) {
        responce = DialogResponce.NO;
        getStage().close();
    }
    
    @FXML
    public void noToAllAction(ActionEvent event) {
        responce = DialogResponce.NO_TO_ALL;
        getStage().close();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        responce = DialogResponce.CANCEL;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
    public DialogResponce getResponce() {
        return responce;
    }

    @Override
    public void initDialog(Object... params) {
        init((String)params[0], (Boolean)params[1]);
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }

    @Override
    public String getInputValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
