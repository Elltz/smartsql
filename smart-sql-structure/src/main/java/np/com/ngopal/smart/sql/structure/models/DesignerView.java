package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerView implements DesignerData, InvalidationListener {

    private static final Pattern VIEW_PATTERN = Pattern.compile("CREATE\\s+VIEW\\s+(`\\w+`|\\w+)");
    
    private DesignerErrDiagram parent;
    
    private final SimpleStringProperty uuid;
    
    private final SimpleStringProperty name;
    private final SimpleStringProperty comments;
    private final SimpleStringProperty view;
    
    private final SimpleStringProperty color;
    
    private final SimpleDoubleProperty x;
    private final SimpleDoubleProperty y;
    private final SimpleDoubleProperty width;
    private final SimpleDoubleProperty height;
    
    private final SimpleStringProperty layerUuid;
    
    private final SimpleStringProperty description;
    
    private final WeakInvalidationListener weakListner = new WeakInvalidationListener(this);
    
    public DesignerView() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty();
        name.addListener(weakListner);
        
        comments = new SimpleStringProperty("");
        comments.addListener(weakListner);
        
        view = new SimpleStringProperty("");
        view.addListener(weakListner);
        
        view.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null) {
                Matcher m = VIEW_PATTERN.matcher(newValue);
                if (m.find()) {
                    String v = m.group(1);
                    name.set(v.startsWith("`") ? v.substring(1, v.length() - 1) : v);
                }
            }
        });
        
        color = new SimpleStringProperty("#fede58");
        color.addListener(weakListner);
                
        x = new SimpleDoubleProperty();
        x.addListener(weakListner);
        
        y = new SimpleDoubleProperty();
        y.addListener(weakListner);
        
        width = new SimpleDoubleProperty();
        width.addListener(weakListner);
        
        height = new SimpleDoubleProperty();
        height.addListener(weakListner);
        
        layerUuid = new SimpleStringProperty();
        layerUuid.addListener(weakListner);
        
        description = new SimpleStringProperty("");
        description.addListener(weakListner);
    }
    
    @Override
    public void invalidated(Observable observable) {
        if (parent != null) {
            parent.invalidated(observable);
        }
    }
    
    @Override
    public void setParent(DesignerErrDiagram parent) {
        this.parent = parent;
    }
    
    @Override
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    @Override
    public SimpleStringProperty name() {
        return name;
    }
    
    @Override
    public SimpleStringProperty description() {
        return description;
    }
    
    public SimpleStringProperty comments() {
        return comments;
    }
    
    public SimpleStringProperty view() {
        return view;
    }
    
    public SimpleStringProperty color() {
        return color;
    }
    
    @Override
    public SimpleDoubleProperty x() {
        return x;
    }
    
    @Override
    public SimpleDoubleProperty y() {
        return y;
    }
    
    @Override
    public SimpleDoubleProperty width() {
        return width;
    }
    
    @Override
    public SimpleDoubleProperty height() {
        return height;
    }
    
    
    public SimpleStringProperty layerUuid() {
        return layerUuid;
    }
    
    @Override
    public String toString() {
        return name.get();
    }

    @Override
    public DesignerData copyData() {
        
        DesignerView v = new DesignerView();
        v.color.set(color.get());
        v.comments.set(comments.get());
        v.height.set(height.get());
        v.layerUuid.set(layerUuid.get());
        v.name.set(name.get());
        v.view.set(view.get());
        v.width.set(width.get());
        v.x.set(x.get());
        v.y.set(y.get());
        v.description.set(description.get());
        
        return v;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        map.put("view", view.get());
        map.put("comments", comments.get());
                
        map.put("color", color.get());
                
        map.put("x", x.get());
        map.put("y", y.get());
        map.put("width", width.get());
        map.put("height", height.get());
        
        map.put("layerUuid", layerUuid.get());
        map.put("description", description.get());
                
        return map;
    }
    
    
    public void fromMap(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set(getNotNullStringValue(map, "name"));
            view.set(getNotNullStringValue(map, "view"));
            comments.set(getNotNullStringValue(map, "comments"));
            
            color.set(getNotNullStringValue(map, "color"));
            
            Double x0 = (Double)map.get("x");
            x.set(x0 != null ? x0 : -1);
            
            Double y0 = (Double)map.get("y");
            y.set(y0 != null ? y0 : -1);
            
            Double width0 = (Double)map.get("width");
            width.set(width0 != null ? width0 : -1);
            
            Double height0 = (Double)map.get("height");
            height.set(height0 != null ? height0 : -1);
            
            layerUuid.set(getNotNullStringValue(map, "layerUuid"));
            description.set(getNotNullStringValue(map, "description"));
        }
    }
    
    @Override
    public List<DesignerProperty> getProperties() {
        
        List<DesignerProperty> list = new ArrayList<>();
        list.add(new DesignerProperty("Name", name, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("Color", color, DesignerProperty.PropertyType.STRING, v -> v));
        list.add(new DesignerProperty("X", x, DesignerProperty.PropertyType.DOUBLE, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Y", y, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Height", height, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        list.add(new DesignerProperty("Width", width, DesignerProperty.PropertyType.STRING, v -> Double.valueOf(v)));
        
        return list;
    }

    private String getNotNullStringValue(Map<String, Object> map, String key) {
        Object v = map.get(key);
        return v != null ? v.toString() : "";
    }
}
