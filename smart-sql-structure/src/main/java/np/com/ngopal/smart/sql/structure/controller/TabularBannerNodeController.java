/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import lombok.Getter;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class TabularBannerNodeController extends BaseController implements Initializable {
    
    @Getter
    @FXML
    private Label titleLabel;
    
    @Getter
    @FXML
    private Label contentTextLabel;
    
    @Getter
    @FXML
    private Label contentTextLabel1;
    
    @Getter
    @FXML
    private Label contentTextLabel2;
    
    @Getter
    private Label info = new Label();
        
    private AreaChart<Number, Number> chart;
    
    @Getter
    @FXML
    private VBox container;
    
    @FXML
    private AnchorPane mainUI;
    
    private String textColor,backColor;
    
    private NumberAxis xAxis;
    private NumberAxis yAxis;

    private ProfilerTabContentController profilerEntry;
    
    private double initStart = -1;
    private double initFinish = -1;
            
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {  
        titleLabel.setVisible(false);
        contentTextLabel.setVisible(false);
                
        makeLabelVisibleOnText(titleLabel);
        makeLabelVisibleOnText(contentTextLabel);
        
        titleLabel.setText("");
        contentTextLabel.setText("");
        
        mainUI.setMinWidth(200.0);
                
        long time = new Date().getTime();
        long value = ChartScale.values()[Flags.DEFAULT_SCALE_INDEX].getValue();

        xAxis = new NumberAxis(time, time + value, value / 5);
        yAxis = new NumberAxis(0, 5, 1);
        
        chart = new AreaChart<>(xAxis, yAxis);
                
        xAxis.setTickLabelFormatter(new StringConverter<Number>() {
            private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

            @Override
            public String toString(Number t) {
                return dateFormat.format(new Date(t.longValue()));
            }

            @Override
            public Number fromString(String string) {
                try {
                    return dateFormat.parse(string).getTime();
                } catch (ParseException ex) {
                    //log.error("Error", ex);
                }

                return 0;
            }
        });
        
        chart.setVisible(false);
        chart.setLegendVisible(false);
        chart.setCreateSymbols(false);
        chart.setHorizontalGridLinesVisible(false);
        chart.setHorizontalZeroLineVisible(false);
        chart.setVerticalGridLinesVisible(false);
        chart.setVerticalZeroLineVisible(false);
        chart.setMinHeight(70);
        chart.setMaxHeight(70);
        chart.setPrefHeight(70);
        chart.setStyle("-fx-padding: 0 0 -20 0");
        
        mainUI.getChildren().add(chart);
        AnchorPane.setBottomAnchor(chart, 0.0);
        AnchorPane.setLeftAnchor(chart, 0.0);
        AnchorPane.setRightAnchor(chart, 0.0);
        
        chart.toBack();
                
        xAxis.setTickLabelsVisible(false);
        xAxis.setOpacity(0);
        yAxis.setTickLabelsVisible(false);
        yAxis.setOpacity(0);
    }
    
    public void init(String description, ProfilerTabContentController profilerEntry) {
        
        this.profilerEntry = profilerEntry;
        
        if (description != null && !description.isEmpty()) {
            info.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getInfo_image()));
            
            Tooltip tooltip = new Tooltip(description);
            tooltip.getStyleClass().add("profilerChartInfoTooltip");
            info.setTooltip(tooltip);
            
            mainUI.getChildren().add(info);
            AnchorPane.setLeftAnchor(info, 0.0);
            AnchorPane.setTopAnchor(info, 0.0);
        }
    }
    
    private void makeLabelVisibleOnText(Label l){
        l.visibleProperty().bind(l.textProperty().isEmpty().not());
    }
    
    private StringProperty getLabelTextProperty(Label l){
        return l.textProperty();
    }
    
    public StringProperty getTitleTextProperty(){
        return getLabelTextProperty(titleLabel);
    }
    
    public StringProperty getContentTextProperty(){
        return getLabelTextProperty(contentTextLabel);
    }
    
    public StringProperty getContentTextProperty1(){
        return getLabelTextProperty(contentTextLabel1);
    }
    
    public StringProperty getContentTextProperty2(){
        return getLabelTextProperty(contentTextLabel2);
    }
    
    public void initChart(List<XYChart.Data<Number, Number>> data) {
        chart.setVisible(true);
     
        getChartData().get(0).getData().addAll(data);
    }
    
    public void initXAxis(double upper, double lower) {
        initFinish = upper;
        initStart = lower;
    }
    
    public void clear() {
        initStart = -1;
        initFinish = -1;

        for (XYChart.Series<Number, Number> series : chart.getData()) {
            series.getData().clear();
        }

        chart.getData().clear();
    }
    
    public void updateChart(boolean showAll, SimpleStringProperty zoomCountStage, Long firstRunDate, Long dragedStart, Long dragedFinish) {

        if (firstRunDate != null) {

            long lower;
            long currentTime = new Date().getTime();
            long upper = currentTime;

            if (initStart == -1 && initFinish == -1) {

                long runDate = firstRunDate;
                if (chart.getData() != null && !chart.getData().isEmpty()) {
                    XYChart.Series<Number, Number> s = chart.getData().get(0);
                    if (s.getData() != null && !s.getData().isEmpty()) {
                        runDate = s.getData().get(0).getXValue().longValue();
                        currentTime = s.getData().get(s.getData().size() - 1).getXValue().longValue();
                    }
                }

                if (showAll) {
                    lower = runDate;
                    zoomCountStage.set("Show All");

                } else if (dragedStart != null && dragedFinish != null) {
                    lower = dragedStart;
                    upper = dragedFinish;

                } else {
                    ChartScale currentScale = ChartScale.values()[profilerEntry.currentScaleIndex().get()];
                    
                    upper = currentTime;
                    lower = upper - currentScale.getValue();
                    
                    zoomCountStage.set(currentScale.getName());
                }
            } else if (dragedStart != null && dragedFinish != null) {
                lower = dragedStart;
                upper = dragedFinish;
            } else if (showAll) {
                lower = (long) initStart;
                upper = (long) initFinish;

                zoomCountStage.set("Show All");
            } else {

                ChartScale currentScale = ChartScale.values()[profilerEntry.currentScaleIndex().get()];
                upper = currentTime;
                lower = upper - currentScale.getValue();
                
                zoomCountStage.set(currentScale.getName());
            }
            
            xAxis.setLowerBound(lower);
            xAxis.setUpperBound(upper);
            
            yAxis.setAutoRanging(true);
        }
    }
    
    public List<XYChart.Series<Number, Number>> getChartData() {
        return chart.getData();
    }
    
    public void setBackGroundColor(String color){
        backColor = color;
        refreshStyle();
    }
    
    public void setTextColors(String color){
        textColor = color;
        refreshStyle();
    }
    
    private void refreshStyle(){
        boolean backColorBool = backColor != null && !backColor.isEmpty();
        boolean textColorBool = textColor != null && !textColor.isEmpty();
        mainUI.getChildren().get(0).setStyle("-fx-background-color : " + (backColorBool ? backColor : "transparent") + ";");
        if(textColorBool){
            contentTextLabel.setStyle("-fx-text-fill: "+ textColor + ";");
        }
    }

}
