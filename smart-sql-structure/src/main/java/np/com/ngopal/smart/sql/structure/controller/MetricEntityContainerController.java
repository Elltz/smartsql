/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class MetricEntityContainerController<T extends Node> extends BaseController implements Initializable,Runnable {
    
    @FXML
    private AnchorPane parent;
    
    @FXML
    protected HBox footer;
    
    @FXML
    protected Button closeButton;
    
    @FXML
    private ProgressIndicator progress_id;
    
    @FXML
    private ImageView pause_logo;    
    
    private ContextMenu contextMenu;  
    private T containingNode;
    private SimpleBooleanProperty entityModuleCreated = new SimpleBooleanProperty(false);
    protected SimpleBooleanProperty activeState = new SimpleBooleanProperty(false);
    
    public static ArrayList<MetricEntityContainerController> allMetricsController = new ArrayList(60);

    @Override
    public AnchorPane getUI() {
        return parent;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) { 
        allMetricsController.add(this);
        contextMenu = new ContextMenu();
        parent.setPadding(Insets.EMPTY);
        footer.getChildren().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                AnchorPane.setBottomAnchor(containingNode, (footer.getChildren().size() > 0 ? 30.0 : 0.0));
            }
        });
        
        parent.sceneProperty().addListener(new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
                activeState.set(newValue != null);
                if(newValue == null){
                    allMetricsController.remove(MetricEntityContainerController.this);
                }
            }
        });
        
        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) { 
                Pane p = (Pane) getUI().getParent();
                p.getChildren().remove(getUI());
            }
        });
        
        parent.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(event.getButton() == MouseButton.SECONDARY){
                    contextMenu.show(parent, event.getScreenX(), event.getScreenY());
                }else {
                    contextMenu.hide();
                }
            }
        });
    }
        
    public void installContextMenu(boolean removeOldItems, MenuItem... items){
       if(!entityModuleCreated.get()){ return; }
       if(removeOldItems){
           contextMenu.getItems().clear();
       }
       contextMenu.getItems().addAll(items);
    }
    
    public void createEntityAsModule(){
        entityModuleCreated.set(true);
    }
    
    public void applyBlackTheme(){
        parent.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);
    }
    
    public void setContent(T t){
        if(t != null){
            parent.getChildren().add(t);
            AnchorPane.setBottomAnchor(t, (footer.getChildren().size() > 0 ? 30.0 : 0.0));
            AnchorPane.setTopAnchor(t, 0.0);
            AnchorPane.setRightAnchor(t, 0.0);
            AnchorPane.setLeftAnchor(t, 0.0);            
        }
        
        if(containingNode != null){
            parent.getChildren().remove(containingNode);
            AnchorPane.clearConstraints(containingNode);
        }
                
        containingNode = t;
    }
    
    public  T getContentNodeAsType(){
        return containingNode;
    }
    
    public boolean isActive(){
        return activeState.get();
    }
    
    public void activateMetric(){
        //activeState.set(true);
    }
    
    public String representableName(){
        return "";
    }

    @Override
    public void run() { }
    
    protected void initDefaultContextItems(){
//        
//        MenuItem removeMetricFromConsole = new MenuItem("Close screen");
//        removeMetricFromConsole.setGraphic(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/metric/stop_2.png", 16,16, true,true)));
//        removeMetricFromConsole.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent event) {
//                Pane p = (Pane) getUI().getParent();
//                p.getChildren().remove(getUI());
//            }
//        });
//        
//        CheckMenuItem resume_pauseMetricCalc = new CheckMenuItem("Pause monitoring");
//        resume_pauseMetricCalc.setGraphic(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/metric/flat.png", 16,16, true,true)));
//        resume_pauseMetricCalc.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent event) { 
//                activeState.setValue(!activeState.get());
//            }
//        });
//        resume_pauseMetricCalc.setSelected(!activeState.get());
//        
//        installContextMenu(true, resume_pauseMetricCalc, new SeparatorMenuItem(), removeMetricFromConsole);
    }
    
    public boolean isOEntityController(){
        return this instanceof OsMetricComponentsController;
    }
    
    public void setDescription(String description){
        if (description != null && !description.isEmpty()) {
            Label info = new Label();
            info.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getInfo_image()));
            
            Tooltip tooltip = new Tooltip(description);
            tooltip.getStyleClass().add("profilerChartInfoTooltip");
            info.setTooltip(tooltip);
            
            parent.getChildren().add(info);
            AnchorPane.setLeftAnchor(info, 0.0);
            AnchorPane.setTopAnchor(info, 0.0);
        }        
    }
    
}
