
package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportDataSize implements FilterableData {
    
    private final SimpleStringProperty database;
    private final SimpleObjectProperty databaseSize;
    private final SimpleStringProperty databaseSizePercent;
    private final SimpleObjectProperty dataSize;
    private final SimpleStringProperty dataSizePercent;
    private final SimpleObjectProperty indexSize;
    private final SimpleStringProperty indexSizePercent;
    private final SimpleObjectProperty fragmentationSize;
    private final SimpleStringProperty fragmentationSizePercent;
    private final SimpleStringProperty engine;
    
 
    public ReportDataSize(String database, BigDecimal databaseSize, BigDecimal dataSize, BigDecimal indexSize, BigDecimal fragmentationSize, String engine) {
        this.database = new SimpleStringProperty(database);
        this.databaseSize = new SimpleObjectProperty(databaseSize);
        this.dataSize = new SimpleObjectProperty(dataSize);
        this.indexSize = new SimpleObjectProperty(indexSize);
        this.fragmentationSize = new SimpleObjectProperty(fragmentationSize);
        this.engine = new SimpleStringProperty(engine);
        this.databaseSizePercent = new SimpleStringProperty();
        this.dataSizePercent = new SimpleStringProperty();
        this.indexSizePercent = new SimpleStringProperty();
        this.fragmentationSizePercent = new SimpleStringProperty();
    }
 
 
    public String getDatabase() {
        return database.get();
    }
    public void setDatabase(String database) {
        this.database.set(database);
    }
    
    public void setDatabaseSize(BigDecimal databaseSize) {
        this.databaseSize.set(databaseSize);
    }
    public BigDecimal getDatabaseSize() {
        return (BigDecimal) databaseSize.get();
    } 
    
    public void setDatabaseSizePercent(String databaseSizePercent) {
        this.databaseSizePercent.set(databaseSizePercent);
    }
    public String getDatabaseSizePercent() {
        return (String) databaseSizePercent.get();
    }
    
    public void setDataSize(BigDecimal dataSize) {
        this.dataSize.set(dataSize);
    }
    public BigDecimal getDataSize() {
        return (BigDecimal) dataSize.get();
    }
    
    public void setDataSizePercent(String dataSizePercent) {
        this.dataSizePercent.set(dataSizePercent);
    }
    public String getDataSizePercent() {
        return (String) dataSizePercent.get();
    }
    
    public void setIndexSize(BigDecimal indexSize) {
        this.indexSize.set(indexSize);
    }
    public BigDecimal getIndexSize() {
        return (BigDecimal) indexSize.get();
    }
    
    public void setIndexSizePercent(String indexSizePercent) {
        this.indexSizePercent.set(indexSizePercent);
    }
    public String getIndexSizePercent() {
        return (String) indexSizePercent.get();
    }
    
    public void setFragmentationSize(BigDecimal fragmentationSize) {
        this.fragmentationSize.set(fragmentationSize);
    }
    public BigDecimal getFragmentationSize() {
        return (BigDecimal) fragmentationSize.get();
    }
    
    public void setFragmentationSizePercent(String fragmentationSizePercent) {
        this.fragmentationSizePercent.set(fragmentationSizePercent);
    }
    public String getFragmentationSizePercent() {
        return (String) fragmentationSizePercent.get();
    }
    
    public String getEngine() {
        return engine.get();
    }
    public void setEngine(String engine) {
        this.engine.set(engine);
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getDatabase() != null ? getDatabase() : "", 
            getDatabaseSize() != null ? getDatabaseSize().toString() : "", 
            getDatabaseSizePercent() != null ? getDatabaseSizePercent() : "", 
            getDataSize() != null ? getDataSize().toString() : "", 
            getDataSizePercent() != null ? getDataSizePercent() : "", 
            getIndexSize() != null ? getIndexSize().toString() : "",             
            getIndexSizePercent() != null ? getIndexSizePercent() : "",             
            getFragmentationSize() != null ? getFragmentationSize().toString() : "", 
            getFragmentationSizePercent() != null ? getFragmentationSizePercent() : "", 
            getEngine() != null ? getEngine() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getDatabase(),
            getDatabaseSize(),
            getDatabaseSizePercent(),
            getDataSize(),
            getDataSizePercent(),
            getIndexSize(),            
            getIndexSizePercent(),            
            getFragmentationSize(),
            getFragmentationSizePercent(),
            getEngine()
        };
    }
}