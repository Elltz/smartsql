package np.com.ngopal.smart.sql.structure.models;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface DebugExplainProvider {

    public void setRowsSum(Long value);
    public Long getRowsSum();
    
    public String getDatabase();
    
    
}
