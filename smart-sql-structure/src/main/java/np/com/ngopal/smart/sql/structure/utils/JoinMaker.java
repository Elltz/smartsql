package np.com.ngopal.smart.sql.structure.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public final class JoinMaker {
    
    private static final Pattern STRING__PATTERN =  Pattern.compile("(@\\d+)", Pattern.CASE_INSENSITIVE);
    
    private JoinMaker() {
    }
    
    public static JoinMaker getInstance() {
        return JoinMakerHolder.INSTANCE;
    }
    
    private static class JoinMakerHolder {

        private static final JoinMaker INSTANCE = new JoinMaker();
    }

    
    
    public static JoinResult makeJoin(ConnectionSession session, UIController contr, MysqlDBService dbService, String prevDatabase, DBTable dragTable, String query, boolean clearOnCancel) {
        
        query = query.replaceAll("\\s+", " ").trim();
        QueryClause queryClause = QueryFormat.parseQuery(query);
        if (queryClause instanceof SelectCommand) {

            JoinResult joinResult = JoinResult.empty();
            joinResult.selectCommand = (SelectCommand) queryClause;

            // check if from clause exist
            FromClause fromClause = ((SelectCommand)queryClause).getFromClause();
            if (fromClause != null) {

                List<TableReferenceClause> tableRefs = fromClause.getTables();
                if (tableRefs != null) {

                    int subIndex = 0;
                    boolean firstStep = true;
                    for (TableReferenceClause t: tableRefs) {
                        String firstName;
                        String database;
                        
                        if (t instanceof SelectCommand) {
                            firstName = t.getName().trim();
                            database = prevDatabase;
                            subIndex++;
                        } else {
                            String[] names = t.getName().trim().split("\\.");
                            firstName = names[names.length - 1].trim();
                            database = names.length > 1 ? names[0] : prevDatabase;
                        }
                        
                        // database and table can going with ` - need clear them
                        if (database.startsWith("`") && database.endsWith("`")) {
                            database = database.substring(1, database.length() - 1);
                        }
                        
                        if (firstName.startsWith("`") && firstName.endsWith("`")) {
                            firstName = firstName.substring(1, firstName.length() - 1);
                        }
                        
                        DBTable firstTable = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), database, firstName);
                        
                        // @TODO Check this
                        if (firstTable == null) {
                            firstTable = new DBTable();
                            firstTable.setName(firstName);
                                                        
                            Database db = new Database();
                            db.setName(database);
                            
                            firstTable.setDatabase(db);
                        }
                        
                        if (t.getAlias() != null && !t.getAlias().isEmpty()) {
                            firstTable.setAlias(t.getAlias());
                            
                        } else if (t instanceof SelectCommand) {
                            firstTable.setAlias("sub" + subIndex);
                        }
                        
                        joinResult.allTables.add(firstTable);

                        if (firstStep) {
                            firstStep = false;                                
                            joinResult.initBaseTable(firstTable);                                
                        }

                        // check joins
                        List<JoinClause> joins = t.getJoinClauses();
                        if (joins != null && !joins.isEmpty()) {
                            for (JoinClause j: joins) {

                                String[] names = j.getTable().getName().trim().split("\\.");
                                String secondName = names[names.length - 1].trim();
                                database = names.length > 1 ? names[0] : prevDatabase;
                        
                                // database and table can going with ` - need clear them
                                if (database.startsWith("`") && database.endsWith("`")) {
                                    database = database.substring(1, database.length() - 1);
                                }

                                if (secondName.startsWith("`") && secondName.endsWith("`")) {
                                    secondName = secondName.substring(1, secondName.length() - 1);
                                }
                        
                                DBTable secondTable = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), database, secondName);
                                if (j.getTable().getAlias() != null && !j.getTable().getAlias().isEmpty()) {
                                    secondTable.setAlias(j.getTable().getAlias());
                                }
                                // @TODO Check this
                                if (secondTable == null) {
                                    secondTable = new DBTable();
                                    secondTable.setName(firstName);

                                    Database db = new Database();
                                    db.setName(database);

                                    secondTable.setDatabase(db);
                                }
                        
                        
                                joinResult.allTables.add(secondTable);

                                JoinCondition jc = JoinCondition.create(secondTable, secondTable, firstTable);
                                
                                if (j.getCondition() != null) {
                                    String joinCondition = j.getCondition().trim();
                                    if (joinCondition.startsWith("(") && joinCondition.endsWith(")")) {
                                        joinCondition = joinCondition.substring(1, joinCondition.length() - 1);
                                    }

                                    String[] conditions = joinCondition.split("\\bAND\\b|\\band\\b");

                                    for (String str: conditions) {

                                        String first = str;
                                        String condition = null;
                                        String second = null;

                                        String[] res = str.split("<>|=|<|>|LIKE");

                                        if (res.length > 0) {
                                            first = res[0].trim();
                                            int idx = str.indexOf(first);
                                            condition = str.substring(idx + first.length()).trim();

                                            if (res.length > 1) {
                                                second = res[1].trim();
                                                idx = condition.indexOf(second);
                                                condition = condition.substring(0, idx).trim();
                                            }
                                        }

                                        jc.addCondition(Condition.create(columnWithDot(jc, first), condition, columnWithDot(jc, second)));
                                    }
                                }

                                joinResult.addTableWithJoinCondition(secondTable, jc);
                            }
                        } else {
                            if (joinResult.allTables.size() > 1) {
                                joinResult.addWithNonJoinContion(firstTable);
                            }
                        }
                    }
                }
            }

            makeJoin(contr, dbService, dragTable, joinResult, true, clearOnCancel);

            return joinResult;
        } 
        
        return null;
    }
    
    /**
     * Note this method should be runned NOT IN FX thread
     * 
     * @param mainController main controller
     * @param dbService mysql service
     * @param dragTable new added table
     * @param joinResult prev join result
     * @param autoJoin enable / disable auto join, if true - will showing dialog for making condition in FX thread
     * @param clearOnCancel clear on pressed cancel if true
     */
    public static void makeJoin(UIController contr, MysqlDBService dbService, TableProvider dragTable, JoinResult joinResult, boolean autoJoin, boolean clearOnCancel) {
        
        joinResult = joinResult != null ? joinResult : new JoinResult();
        
        // its for generating presentations
        joinResult.toString();
            
        
        // when we drag one table then 1st table is base tables
        if (joinResult.allTables.isEmpty()) {
            
            joinResult.initBaseTable(dragTable);
                    
            joinResult.allTables.add(dragTable);
            joinResult.initTableIndex();
            joinResult.clearPresentation();
            
        } else {
            
            joinResult.allTables.add(dragTable);
                        
            String database = dragTable.getDatabase().getName();
            String tableName = dragTable.getName();
            String tables = "";
            for (TableProvider t: new ArrayList<>(joinResult.allTables)) {
                if (t != dragTable) {
                    if (!tables.isEmpty()) {
                        tables += ", ";
                    }
                    
                    tables += "'" + t.getName() + "'";
                }
            }
            
            Map<String, Map<String, Object>> fks = new LinkedHashMap<>();
            
            try (ResultSet rs = (ResultSet) dbService.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_FOREIGN_KEYS), database, tableName, tables, tables, tableName))) {
                if (rs != null) {
                    while (rs.next()) {
                        String fkName = rs.getString("CONSTRAINT_NAME");
                        Map<String, Object> m = fks.get(fkName);
                        if (m == null) {
                            m = new HashMap<>();
                            fks.put(fkName, m);
                            
                            m.put("table", rs.getString("TABLE_NAME"));
                            m.put("referencedTable", rs.getString("REFERENCED_TABLE_NAME"));
                            m.put("columns", new ArrayList<>());
                            m.put("referencedColumns", new ArrayList<>());
                        }
                        
                        ((List)m.get("columns")).add(rs.getString("COLUMN_NAME"));
                        ((List)m.get("referencedColumns")).add(rs.getString("REFERENCED_COLUMN_NAME"));
                    }
                }
            } catch (SQLException ex) {
                log.error("Error in join maker", ex);
            }
            
            
            JoinCondition joinCondition = null;
            if (!fks.isEmpty()) {
                Map<String, Object> fk = fks.values().stream().findFirst().get();
                if (fk != null) {
                    
                    String name = (String)fk.get("table");
                    List<String> columns = (List<String>) fk.get("columns");
                    List<String> referencedColumns = (List<String>) fk.get("referencedColumns");
                    
                    if (dragTable.getName().equals(name)) {
                        name = (String)fk.get("referencedTable");
                        columns = (List<String>) fk.get("referencedColumns");
                        referencedColumns = (List<String>) fk.get("columns");
                    }
                    
                    TableProvider table = null;
                    if (dragTable.getName().equalsIgnoreCase(name)) {
                        table = dragTable;
                    } else {
                        for (TableProvider tp: joinResult.allTables) {
                            if (tp.getName().equalsIgnoreCase(name)) {
                                table = tp;
                                break;
                            }
                        }
                    }
                    
                    if (table == null) {
                        DatabaseCache.getInstance().getCopyOfTable(contr.getSelectedConnectionSession().get().getConnectionParam().cacheKey(), dragTable.getDatabase().getName(), name);
                    }
                    
                    joinCondition = JoinCondition.create(dragTable, table, dragTable);
                    for (int i = 0; i < columns.size(); i++) {
                        joinCondition.addCondition(Condition.create(conditionTableColumn(referencedColumns.get(i), dragTable), "=", conditionTableColumn(columns.get(i), table)));
                    }
                    
                    // if found add it
                    if (joinCondition != null) {
                        
                        if (joinResult.getTableJoinCondition(joinCondition.joinTable) == null) {
                            joinResult.addTableWithJoinCondition(joinCondition.joinTable, joinCondition);
                            
                        } else if (joinResult.getTableJoinCondition(joinCondition.joinTable == joinCondition.first ? joinCondition.second : joinCondition.first) == null){
                            joinResult.addTableWithJoinCondition(joinCondition.joinTable == joinCondition.first ? joinCondition.second : joinCondition.first, joinCondition);
                        }
                    }
                }
            }
                        
            if (joinCondition == null) {
                                
                for (TableProvider table: joinResult.allTables) {

                    if (table != dragTable) {
                        // try find condition for each table
                        joinCondition = findSameNamesJoinCondition(dbService, table, dragTable);

                        // if found add it and return
                        if (joinCondition != null) {

                            joinResult.addTableWithJoinCondition(joinCondition.joinTable, joinCondition);
                            break;
                        }
                    }
                }
                
                if (joinCondition == null) {
                    
                    if (contr != null) {
                        joinCondition = JoinCondition.create(dragTable, dragTable, joinResult.baseTable);

                        for (Column c: dragTable.getColumns()) {
                            if (c.isPrimaryKey()) {
                                joinCondition.addCondition(Condition.create(columnWithDot(joinCondition, c.getName()), "=", null));
                            }
                        }

                        JoinCondition jc = joinCondition;
                        JoinResult jr = joinResult;

                        if (Platform.isFxApplicationThread()) {
                            if (!showJoinConditionDialog(contr, jr.allTables, jr, jc, false, true, clearOnCancel)) {
                                jr.allTables.remove(dragTable);
                            } else {
                                jr.clearPresentation();
                            }
                        } else {

                            // lock current thread and wait respond from FX thread
                            CountDownLatch lock = new CountDownLatch(1);

                            Platform.runLater(() -> {
                                if (!showJoinConditionDialog(contr, jr.allTables, jr, jc, false, true, clearOnCancel)) {
                                    jr.allTables.remove(dragTable);
                                } else {
                                    jr.clearPresentation();
                                }
                                lock.countDown();
                            });

                            try {
                                lock.await();
                            } catch (InterruptedException ex) {
                                log.error("Error awaing lock", ex);
                            }
                        }
                    }
                } else {
                    joinResult.clearPresentation();
                }
            } else {
                joinResult.clearPresentation();
            }

            joinResult.initTableIndex();   
        }
    }
    
    
    public static boolean showJoinConditionDialog(UIController contr, List<TableProvider> tables, JoinResult joinResult, JoinCondition joinCondition, boolean canChangeFirst, boolean canChangeSecond, boolean clearOnCancel) {
        
        QueryBuilderFilterDialogController filterController = StandardBaseControllerProvider.getController(contr, "QueryBuilderFilterDialog");                
        filterController.init(contr.getSelectedConnectionSession().get().getService(),  joinResult, joinCondition, tables, canChangeFirst, canChangeSecond, clearOnCancel);

        Scene scene = new Scene(filterController.getUI());        
        Stage stage = new Stage();
        stage.setTitle("Join conditions");
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.WINDOW_MODAL);        
        stage.initOwner(getApplicationMainStage());
        stage.setScene(scene);
        stage.sizeToScene();

        stage.showAndWait();
        
        return filterController.isAccepted();
    }
    
    
    
    
    // <editor-fold desc=" Find base table ">

    public static TableProvider findBaseTable(MysqlDBService dbService, List<TableProvider> table) {
        // when we drag second table then we need to calculate when table is
        // using primary key/unique key in the join condition  then is the base table,
        
        TableProvider baseTable = null;
        // find base table
        // base table never join with other tables using his primary key  or unique key
        for (int i = table.size() - 1; i >= 0; i--) {
            
            TableProvider sourceTable = table.get(i);
                    
            boolean baseTableFounded = true;
            
            // find PK and Uniques
            List<Column> pks = null;
            List<List<Column>> uniques = new ArrayList<>();
            
            if (sourceTable instanceof DBTable) {
                
                for (Index idx: ((DBTable)sourceTable).getIndexes()) {

                    if (idx.getType() != null) {
                        switch (idx.getType()) {
                            case PRIMARY:
                                pks = idx.getColumns();
                                break;

                            case UNIQUE_TEXT:
                                uniques.add(idx.getColumns());
                                break;
                        }
                    }
                }

                // try find any table that has FK on source
                for (TableProvider destinationTable: table) {
                    if (!destinationTable.equals(sourceTable)) {

                        if (destinationTable instanceof DBTable) {
                            if (((DBTable)destinationTable).getForeignKeys() != null) {
                                for (ForeignKey fk: ((DBTable)destinationTable).getForeignKeys()) {

                                    // check FKs not in PK and Uniques
                                    if (fk.getReferenceTable().equalsIgnoreCase(sourceTable.getName())) {

                                        boolean fullEqual = true;
                                        // check PK
                                        if (pks != null && pks.size() == fk.getColumns().size()) {
                                            for (Column c: fk.getColumns()) {

                                                boolean contains = false;
                                                // check PK
                                                for (Column pkc: pks) {
                                                    if (pkc.getName().equals(c.getName())) {
                                                        contains = true;
                                                        break;
                                                    }
                                                }

                                                if (!contains) {
                                                    fullEqual = false;
                                                    break;
                                                }
                                            }
                                        } else {
                                            fullEqual = false;
                                        }

                                        if (fullEqual) {
                                            baseTableFounded = false;

                                        } else {

                                            // check Uniques
                                            for (List<Column> unique: uniques) {

                                                boolean foundUnique = false;

                                                if (unique.size() == fk.getColumns().size()) {

                                                    foundUnique = true;
                                                    for (Column c: fk.getColumns()) {

                                                        boolean contains = false;
                                                        // check PK
                                                        for (Column uc: unique) {
                                                            if (uc.getName().equals(c.getName())) {
                                                                contains = true;
                                                                break;
                                                            }
                                                        }

                                                        if (!contains) {
                                                            foundUnique = false;
                                                            break;
                                                        }
                                                    }
                                                }

                                                if (foundUnique) {
                                                    baseTableFounded = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (!baseTableFounded) {
                                        break;
                                    }
                                }
                            }
                        }

                        if (!baseTableFounded) {
                            break;
                        }
                    }
                }
            
                if (baseTableFounded) {
                    baseTable = sourceTable;
                    break;
                }
            }
        }

        // incase both tables not having priory keys / unique keys then you can 
        // get records count and when table has more records then that is the base table
        if (baseTable == null && !table.isEmpty()) {
            
            long maxCount = 0l;
            baseTable = table.get(0);
            
            for (TableProvider sourceTable: table) {
                long count = dbService.getRowCount(sourceTable.getDatabase().getName(), sourceTable.getName());
                if (count > maxCount) {
                    maxCount = count;
                    baseTable = sourceTable;
                }
            }
        }
        
        return baseTable;
    }
    
    // </editor-fold>
    
    
    
    // <editor-fold desc=" Find auto join condition ">
    
    private static JoinCondition findAutoJoinCondition(JoinResult joinResult, TableProvider table, TableProvider dragedTable) {
            
        if (table instanceof DBTable && dragedTable instanceof DBTable && ((DBTable)table).getForeignKeys() != null) {
            
            List<Set<String>> uniques = new ArrayList<>();
            for (Index index: ((DBTable)table).getIndexes()) {
                
                if (index.getType() == IndexType.UNIQUE_TEXT) {
                    
                    Set<String> set = new TreeSet<>();
                    for (Column c: index.getColumns()) {
                        set.add(c.getName());
                    }
            
                    uniques.add(set);
                }
            }

            Set<String> primarys = new TreeSet<>();
            for (Column c: table.getColumns()) {
                if (c.isPrimaryKey()) {
                    primarys.add(c.getName());
                }
            }

            if (((DBTable)dragedTable).getForeignKeys() != null) {
                // if not returned need check for draged table
                for (ForeignKey fk: ((DBTable)dragedTable).getForeignKeys()) {

                    // check database and table name
                    if (Objects.equals(fk.getReferenceDatabase().toLowerCase(), table.getDatabase().getName().toLowerCase()) 
                        && Objects.equals(fk.getReferenceTable().toLowerCase(), table.getName().toLowerCase())) {

                        //if (table == joinResult.baseTable) {
                            
                        boolean changed = false;
                        // check primary
                        if (primarys.size() == fk.getColumns().size()) {
                            boolean found = true;
                            for (Column c: fk.getColumns()) {
                                if (!primarys.contains(c.getName())) {
                                    found = false;
                                    break;
                                }
                            }

                            if (found) {
                                joinResult.baseTable = dragedTable;
                                changed = true;
                            }
                        }                            

                        if (!changed) {

                            for (Set<String> unique: uniques) {
                                if (unique.size() == fk.getColumns().size()) {
                                    boolean found = true;
                                    for (Column c: fk.getColumns()) {
                                        if (!unique.contains(c.getName())) {
                                            found = false;
                                            break;
                                        }
                                    }
                                    if (found) {
                                        joinResult.baseTable = dragedTable;
                                        break;
                                    }
                                }
                            }
                        }
                            
                        //}
                        
                        JoinCondition jc = JoinCondition.create(dragedTable, table, dragedTable);
                        for (int i = 0; i < fk.getColumns().size(); i++) {
                            jc.addCondition(Condition.create(fk.getReferenceColumns().get(i), "=", fk.getColumns().get(i)));
                        }
                        
                        return jc;
                    }
                }
            }
            
            // first check FKs
            for (ForeignKey fk: ((DBTable)table).getForeignKeys()) {

                // check database and table name
                if (Objects.equals(fk.getReferenceDatabase().toLowerCase(), dragedTable.getDatabase().getName().toLowerCase()) 
                    && Objects.equals(fk.getReferenceTable().toLowerCase(), dragedTable.getName().toLowerCase())) {

                    JoinCondition jc = JoinCondition.create(dragedTable, dragedTable, table);
                    for (int i = 0; i < fk.getColumns().size(); i++) {
                        jc.addCondition(Condition.create(fk.getReferenceColumns().get(i), "=", fk.getColumns().get(i)));
                    }

                    return jc;
                }
            }
        }
        
        return null;
    }
    
    private static Column findColumnByName(TableProvider table, String columnName) {
        if (columnName != null && table != null && table.getColumns() != null) {
            for (Column c: table.getColumns()) {
                if (columnName.equalsIgnoreCase(c.getName())) {
                    return c;
                }
            }
        }
        
        return null;
    }
    
    
    public static JoinCondition findSameNamesJoinCondition(DBService service, TableProvider table, TableProvider dragedTable) {
        
        List<Condition> conditions = new ArrayList<>();
        String str = "'" + table.getName() + "', '" + dragedTable + "'";
        
        try (ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_PRIMARY_KEYS), table.getDatabase().getName(), str, str))) {
            if (rs != null) {
                while (rs.next()) {
                    String tableName = rs.getString("TABLE_NAME");
                    String referencedTableName = rs.getString("REFERENCED_TABLE_NAME");
                    String columnName = rs.getString("COLUMN_NAME");
                    String referencedColumnName = rs.getString("REFERENCED_COLUMN_NAME");
                    
                    boolean f = table.getName().equalsIgnoreCase(tableName);
                    Column c0 = f ? findColumnByName(table, columnName) : findColumnByName(table, referencedColumnName);
                    Column c1 = !f ? findColumnByName(dragedTable, columnName) : findColumnByName(dragedTable, referencedColumnName);
                    
                    // check if condition already added
                    boolean found = false;
                    Condition condition = Condition.create(c0, "=", c1);
                    for (Condition c: conditions){
                        if (c.first == condition.first && c.second == condition.second ||
                            c.first == condition.second && c.second == condition.first) {
                            found = true;
                            break;
                        }
                    }
                    
                    if (!found) {
                        conditions.add(condition);
                    }
                }
            }
        } catch (SQLException ex) {
            log.error("Error in join maker", ex);
        }
        
//        // check primary keys
//        List<Column> pk0 = new ArrayList<>();
//        for (Column c: table.getColumns()) {
//            if (c.isPrimaryKey()) {
//                pk0.add(c);
//            }
//        } 
//        
//        List<Column> pk1 = new ArrayList<>();
//        for (Column c: dragedTable.getColumns()) {
//            if (c.isPrimaryKey()) {
//                pk1.add(c);
//            }
//        } 
//        
//        
//        // same pk columns
//        List<Column> samePKs = new ArrayList<>();        
//        samePKs.addAll(pk0);
//        samePKs.addAll(pk1);
//        
//        for (Column c0: pk0) {
//            if (AbstractDBService.isNumber(c0.getDataType().getName())) {
//                for (Column c1: pk1) {
//                    if (AbstractDBService.isNumber(c1.getDataType().getName())) {
//                        if (c0.getName().equalsIgnoreCase(c1.getName())) {
//                            samePKs.add(c0);
//                        }
//                    }
//                }
//            }
//        }
//        
//        if (!samePKs.isEmpty()) {
//            for (Column c0: samePKs) {
//                
//                boolean founded = false;
//                
//                String s0 = c0.getName().replaceAll(table.getName(), "").replaceAll("_|s|es", "");
//                
//                if (c0.getTable().equals(dragedTable))  {
//                    // try find in tables
//                    for (Column c1: table.getColumns()) {
//                        if (!c1.isPrimaryKey()) {
//                            String s1 = c1.getName().toLowerCase().replaceAll(dragedTable.getName(), "").replaceAll("_|s|es", "");
//
//                            if (s0.equalsIgnoreCase(s1)) {
//                                conditions.add(Condition.create(c0, "=", c1));
//                                founded = true;
//                                break;
//                            }
//                        }
//                    }
//                } else {                
//                    // try find in tables
//                    for (Column c1: dragedTable.getColumns()) {
//                        if (!c1.isPrimaryKey()) {
//                            String s1 = c1.getName().toLowerCase().replaceAll(table.getName(), "").replaceAll("_|s|es", "");
//
//                            if (s0.equalsIgnoreCase(s1)) {
//                                conditions.add(Condition.create(c0, "=", c1));
//                                founded = true;
//                                break;
//                            }
//                        }
//                    }
//                }
//                
//                if (founded) {
//                    break;
//                }
//            }
//        }
//        
        
//        Set<Column> tableColumns = collectColumnsForJoin(table);
//        Set<Column> dragedTableColumns = collectDraggedColumnsForJoin(dragedTable);
//        
//        
//        // check first only NUMBER types
//        for (Column c: dragedTableColumns) {
//
//            if (AbstractDBService.isNumber(c.getDataType().getName())) {
//                String name = c.getName().replaceAll(table.getName(), "").replaceAll("_|s|es", "");
//                // try found the same colum names
//                for (Column dc: tableColumns) {
//
//                    if (AbstractDBService.isNumber(dc.getDataType().getName())) {
//                        if (name.equalsIgnoreCase(dc.getName().replaceAll("_|s|es", ""))) {
//                            conditions.add(Condition.create(c.getName(), "=", dc.getName()));
//                        }
//                    }
//                }
//            }
//        }
    
        // if we find conditions - just remove them
        if (!conditions.isEmpty()) {
            return JoinCondition.create(dragedTable, table, dragedTable, conditions);
            
        }
                
        return null;
    }
    
    
    private static Set<Column> collectColumnsForJoin(TableProvider table) {
        Set<Column> tableColumns = new TreeSet<>();
        
        for (Column c: table.getColumns()) {
            if (c.isPrimaryKey()) {
                tableColumns.add(c);
            }
        }         
        
        for (Index index: ((DBTable)table).getIndexes()) {                
            if (index.getType() == IndexType.UNIQUE_TEXT) {
                tableColumns.addAll(index.getColumns());
            }
        }
        
        if (((DBTable)table).getForeignKeys() != null) {
            for (ForeignKey fk: ((DBTable)table).getForeignKeys()) {
                tableColumns.addAll(fk.getColumns());
            }
        }
        
        return tableColumns;
    }
    
    private static Set<Column> collectDraggedColumnsForJoin(TableProvider table) {
        Set<Column> tableColumns = new TreeSet<>();
        
        if (((DBTable)table).getForeignKeys() != null) {
            for (ForeignKey fk: ((DBTable)table).getForeignKeys()) {
                tableColumns.addAll(fk.getColumns());
            }
        }
        
        for (Index index: ((DBTable)table).getIndexes()) {                
            if (index.getType() == IndexType.UNIQUE_TEXT) {
                tableColumns.addAll(index.getColumns());
            }
        }   
        return tableColumns;
    }
    
    // </editor-fold>
    
    
    
    
    private static Column conditionTableColumn(String condition, TableProvider table) {
        if (table != null && table.getColumns() != null) {
            for (Column c: table.getColumns()) {
                if (c.getName().equalsIgnoreCase(condition.trim())) {
                    return c;
                }
            }
        }

        return null;
    }
        
    private static String trimNameWithDot(String name) {
        return name.contains(".") ? name.substring(name.lastIndexOf(".") + 1) : name;
    }
    
    private static Object columnWithDot(JoinCondition jc, String name) {
        Column column = null;
        TableProvider table = null;
        String columnName = name;
        
        if (name.contains(".")) {            
            String[] str = name.split("\\.");
            if (jc.getFirst().getName().equalsIgnoreCase(str[0].trim())) {
                table = jc.getFirst();
                columnName = str[1];
                
            } else if (jc.getSecond().getName().equalsIgnoreCase(str[0].trim())) {
                table = jc.getSecond();
                columnName = str[1];
            }
        }
        
        if (columnName != null) {
            columnName = columnName.trim();
        }
        
        if (table != null) {
            column = conditionTableColumn(columnName, table);
        } else {
            column = conditionTableColumn(columnName, jc.getFirst());
            if (column == null) {
                column = conditionTableColumn(columnName, jc.getSecond());
            }
        }        
//        
        return column != null ? column : name;
    }
    
    
    
    
    @Getter
    @Setter
    public static class JoinResult {
        
        private SelectCommand selectCommand;
        
        private TableProvider baseTable;
        private boolean needChangeBaseTable = false;
        
        private List<TableProvider> allTables = new ArrayList<>();
        
        private Map<TableProvider, List<JoinCondition>> joinTables = new HashMap<>();
        
        private List<TableProvider> tablesWithOutJoins = new ArrayList<>();
        
        private Map<TableProvider, Integer> sameTableIndexes = new HashMap<>();
        private Map<String, Integer> currentSameTableCounts = new HashMap<>();
        
        private String presentation;
        
        public void clearPresentation() {
            presentation = null;
        }
        
        public static JoinResult empty() {
            return new JoinResult();
        }
        
        public void initBaseTable(TableProvider baseTable) {
            this.baseTable = baseTable;
            
            // TODO maybe need some logic
        }
        
        private void clearTableIndex() {
            sameTableIndexes.clear();
            currentSameTableCounts.clear();
        }
                
        
        public void removeTable(MysqlDBService service, TableProvider removeTable) {
            allTables.remove(removeTable);
            tablesWithOutJoins.remove(removeTable);
            sameTableIndexes.remove(removeTable);
            
            Integer i = currentSameTableCounts.get(removeTable.getName());
            if (i != null) {
                if (i == 0) {
                    currentSameTableCounts.remove(removeTable.getName());
                } else {
                    currentSameTableCounts.put(removeTable.getName(), i - 1);
                }
            }
            
            
            if (baseTable == removeTable) {
                initBaseTable(findBaseTable(service, allTables));
            }
            
            joinTables.remove(removeTable);

            List<TableProvider> forRemove = new ArrayList<>();
            // need remove joins with this table            
            for (TableProvider tp: joinTables.keySet()) {
                
                List<JoinCondition> list = joinTables.get(tp);
                for (JoinCondition jc: list) {
                    if (jc.first == removeTable || jc.second == removeTable) {
                        forRemove.add(tp);
                    }
                }
            }
            
            for (TableProvider tp: forRemove) {
                joinTables.remove(tp);
                if (allTables.size() > 1) {
                    tablesWithOutJoins.add(tp);
                }
            }
        }
        
        public void initTableIndex() {
            
            clearTableIndex();
            
            for (TableProvider table: allTables) {
                // get index from map
                Integer count = currentSameTableCounts.get(table.getName().toLowerCase());
                if (count == null) {
                    count = 0;
                }

                // put new index for table
                sameTableIndexes.put(table, count);

                // inc count add put to map
                count++;            
                currentSameTableCounts.put(table.getName().toLowerCase(), count);
            }
        }
        
        
        public void addTableWithJoinCondition(TableProvider table, JoinCondition joinCondition) {  
            List<JoinCondition> list = joinTables.get(table);
            if (list == null) {
                list = new ArrayList<>();
                joinTables.put(table, list);
            }
            
            if (!list.isEmpty()) {
                for (JoinCondition jc: list) {
                    
                    if (jc.getFirst() == joinCondition.getFirst() && jc.getSecond() == joinCondition.getSecond() || 
                            jc.getSecond() == joinCondition.getFirst() && jc.getFirst() == joinCondition.getSecond()) {
                        
                        List<Condition> alreadyExist = new ArrayList<>();
                        
                        for (Condition c0: joinCondition.getConditions()) {                            
                            for (Condition c: jc.getConditions()) {
                                
                                if (c0.getFirst().equals(c.getFirst()) && c0.getSecond().equals(c.getSecond()) ||
                                        c0.getSecond().equals(c.getFirst()) && c0.getFirst().equals(c.getSecond())) {
                                    alreadyExist.add(c0);
                                    break;
                                }
                            }
                        }
                        
                        for (Condition c: alreadyExist) {
                            joinCondition.getConditions().remove(c);
                        }
                        
                        if (joinCondition.getConditions().isEmpty()) {
                            return;
                        }
                    }
                }
                
            }
            
            list.add(joinCondition);            
            tablesWithOutJoins.remove(table);
            tablesWithOutJoins.remove(joinCondition.getFirst());
            
            int oldIndex = allTables.indexOf(table);
            if (joinCondition.getFirst() != table) {
                
                int firstIndex = allTables.indexOf(joinCondition.getFirst());
                if (oldIndex < firstIndex) {
                    
                    oldIndex = firstIndex;
                    
                    allTables.remove(table);
                    allTables.add(firstIndex, table);
                }                
            }
            
            if (joinCondition.getSecond() != table) {
                
                int secondIndex = allTables.indexOf(joinCondition.getSecond());
                if (oldIndex < secondIndex) {
                    
                    allTables.remove(table);
                    allTables.add(secondIndex, table);
                }                
            }
        }
        
        public void addWithNonJoinContion(JoinCondition joinCondition) {
            
            List<JoinCondition> list = joinTables.get(joinCondition.getJoinTable());
            if (list != null) {
                list.remove(joinCondition);
            }
            
            if (list == null || list.isEmpty()) {
                joinTables.remove(joinCondition.getJoinTable());
                tablesWithOutJoins.add(joinCondition.getJoinTable());
            }
        }
        
        public void addWithNonJoinContion(TableProvider table) {
            tablesWithOutJoins.add(table);
            joinTables.remove(table);
        }
        
        public List<JoinCondition> getTableJoinCondition(TableProvider table) {
            return joinTables.get(table);
        }
        
        
        @Override
        public String toString() {
            if (presentation == null) {
                boolean useDatabase = false;
                String databaseName = null;
                for (TableProvider tp: allTables) {                
                    if (databaseName == null) {
                        databaseName = tp.getDatabase().getName();
                    } else if (!databaseName.equals(tp.getDatabase().getName())) {
                        useDatabase = true;
                        break;
                    }
                }

                StringBuilder sb = new StringBuilder("SELECT ");

                String columnsStr = "";
                if (selectCommand != null) {
                    List<ColumnReferenceClause> columns = selectCommand.getColumns();
                    if (columns != null) {

                        for (ColumnReferenceClause c: columns) {
                            if (!columnsStr.isEmpty()) {
                                columnsStr += ", ";
                            }
                            columnsStr += (c.getName() + (c.isAs() ? " AS" : "") + (c.getAlias() != null ? " " + c.getAlias() : "")).trim();
                        }

                        if (allTables.size() > 1) {
                            TableProvider t = allTables.get(allTables.size() - 1);
                            String s = getCurrentTableName(t) + ".*";
                            
                            int i = columnsStr.indexOf(s, 0);
                            boolean found = false;
                            
                            while (i >= 0) {
                                if (i > 0 && (columnsStr.charAt(i - 1) == ' ' || columnsStr.charAt(i - 1) == ',' || columnsStr.charAt(i - 1) == '\n')) {
                                    found = true;
                                    break;
                                }
                                i = columnsStr.indexOf(s, i + 1);
                            }
                            
                            if (!found) {
                                columnsStr += ", " + s;
                            }
                        }
                    }
                } else if (!allTables.isEmpty()) {
                    
                    List<String> alreadyAdded = new ArrayList<>();
                    
                    for (TableProvider t: allTables) {
                        String s = getCurrentTableName(t) + ".*";
                        
                        if (!alreadyAdded.contains(s)) {
                            if (!columnsStr.isEmpty()) {
                                columnsStr += ", ";
                            }
                            columnsStr += s;
                            alreadyAdded.add(s);
                        }
                    }
                }

                sb.append(columnsStr); 

                // start FROM section
                sb.append(" FROM ");

                boolean appendComma = false;
                // add non joins table
                for (int i = 0; i < tablesWithOutJoins.size(); i++) {
                    TableProvider t = tablesWithOutJoins.get(i);

                    if (appendComma) {
                        sb.append(", ");
                    } else {
                        appendComma = true;
                    }

                    if (useDatabase) {
                        sb.append(t.getDatabase()).append(".");
                    }

                    sb.append(t.getName()).append(" ").append(getCurrentTableName(t));              
                }


                if (!allTables.isEmpty()) {

                    if (appendComma) {
                        sb.append(", ");
                    }

                    TableProvider tp = allTables.get(0);        
                    if (useDatabase) {
                        sb.append(tp.getDatabase()).append(".");
                    }
                    sb.append(tp.getName()).append(" ").append(getCurrentTableName(tp));

                    // add joins table
                    for (int i = 1; i < allTables.size(); i++) {

                        TableProvider t = allTables.get(i);

                        List<JoinCondition> list = getTableJoinCondition(t);
                        if (list != null && !list.isEmpty()) {

                            // start JOIN section
                            String joinType = list.get(0).getJoinType();
                            if (joinType != null && !joinType.isEmpty()) {
                                sb.append(" " + joinType);
                            }
                            sb.append(" JOIN ");

                            if (useDatabase) {
                                sb.append(t.getDatabase()).append(".");
                            }
                            // add join table
                            sb.append(t.getName()).append(" ").append(getCurrentTableName(t));

                            processJoinConditions(list, sb);
                        }                
                    }
                    
                    if (selectCommand != null) {
                        WhereClause wc = selectCommand.getWhereClause();
                        if (wc != null) {

                            String result = wc.getCondition();
                            Matcher m = STRING__PATTERN.matcher(result);
                            while (m.find()) {
                                String key = m.group();
                                result = result.replace(key, selectCommand.getString(key));
                            }

                            sb.append(" WHERE ").append(result);
                        }

                    }
                    
                    if (selectCommand != null) {
                        GroupByClause gpc = selectCommand.getGroupByClause();
                        if (gpc != null) {
                            String str = "";
                            List<ColumnReferenceClause> list = gpc.getColumns();
                            if (list != null) {
                                for (ColumnReferenceClause c: list) {
                                    if (!str.isEmpty()) {
                                        str += ", ";
                                    }
                                    str += c.getName().trim();
                                }
                            }
                            sb.append(" GROUP BY ").append(str);
                            if (gpc.isWithRollup()) {
                                sb.append(" WITH ROLLUP");
                            }
                        }
                    }

                    if (selectCommand != null) {
                        HavingClause hc = selectCommand.getHavingClause();
                        if (hc != null) {
                            sb.append(" HAVING ").append(hc.getCondition());
                        }
                    }

                    if (selectCommand != null) {
                        OrderByClause obc = selectCommand.getOrderByClause();
                        if (obc != null) {
                            String str = "";
                            List<ColumnReferenceClause> list = obc.getColumns();
                            if (list != null) {
                                for (ColumnReferenceClause c: list) {
                                    if (!str.isEmpty()) {
                                        str += ", ";
                                    }
                                    str += c.getName().trim();
                                }
                            }
                            sb.append(" ORDER BY ").append(str);
                        }
                    }

                    if (selectCommand != null) {
                        LimitClause lc = selectCommand.getLimitClause();
                        if (lc != null) {
                            sb.append(" LIMIT ").append(lc.getSource());
                        }
                    }
                }
                
                presentation = sb.toString();
            }
            
            return presentation;
        }
        
        
        public void processJoinConditions(List<JoinCondition> list, StringBuilder sb) {
            // add join condition
            sb.append(" ON (");
            
            boolean addAnd = false;
            for (JoinCondition joinCondition: list) {

                for (int j = 0; j < joinCondition.conditionSize(); j++) {

                    if (addAnd) {
                        sb.append(" AND ");
                    }

                    addAnd = true;

                    Condition condition = joinCondition.getCondition(j);
                    if (condition.getFirst() instanceof Column) {

                        if (((Column)condition.getFirst()).getTable() != null) {
                            sb.append(getCurrentTableName(((Column)condition.getFirst()).getTable())).append(".");
                        }

                        sb.append(((Column)condition.getFirst()).getName());
                    } else {
                        if (joinCondition.getFirst() != null) {
                            sb.append(getCurrentTableName(joinCondition.getFirst())).append(".");
                        }
                        sb.append(condition.getFirst() != null ? condition.getFirst() : "");
                    }

                    sb.append(" ").append(condition.getCondition()).append(" ");;

                    if (condition.getSecond() instanceof Column) {

                        if (((Column)condition.getSecond()).getTable() != null) {
                            sb.append(getCurrentTableName(((Column)condition.getSecond()).getTable())).append(".");
                        }

                        sb.append(((Column)condition.getSecond()).getName());
                    } else {
                        if (joinCondition.getSecond() != null) {
                            sb.append(getCurrentTableName(joinCondition.getSecond())).append(".");
                        }
                        sb.append(condition.getSecond() != null ? condition.getSecond() : "");
                    }
                }                            
            }
            
            sb.append(")");
        }
        
        private String getCurrentTableName(TableProvider t) {
            String name = t.getAlias() != null ? t.getAlias() : t.getName();
            if (sameTableIndexes.containsKey(t)) {
                int count = sameTableIndexes.get(t);
                if (count > 0) {
                    name += String.valueOf(count);
                }
            }
            
            return name;
        }
    }
    
    
    
    @Getter
    @Setter
    public static class JoinCondition {
        
        private String joinType;
        
        private TableProvider joinTable;
        
        private TableProvider first;
        private TableProvider second;
        
        private List<Condition> conditions = new ArrayList<>();
        
        public static JoinCondition create(TableProvider joinTable, TableProvider first, TableProvider second) {
            JoinCondition jc = new JoinCondition();
            jc.joinTable = joinTable;
            jc.first = first;
            jc.second = second;
            
            return jc;
        }
        
        public static JoinCondition create(TableProvider joinTable, TableProvider first, TableProvider second, List<Condition> conditions) {
            JoinCondition jc = new JoinCondition();
            jc.joinTable = joinTable;
            jc.first = first;
            jc.second = second;
            jc.conditions = conditions;
            
            return jc;
        }
        
        public void setJoinType(String joinType) {
            this.joinType = joinType;
        }
        public String getJoinType() {
            return joinType;
        }
        
        public void addCondition(Condition c) {
            conditions.add(c);
        }
        public void removeCondition(Condition c) {
            conditions.remove(c);
        }
        
        public void clearConditions() {
            conditions.clear();
        }
        
        public boolean isEmptyConditions() {
            return conditions.isEmpty();
        }
        
        public Condition getCondition(int index) {
            return conditions.get(index);
        }
        
        public int conditionSize() {
            return conditions.size();
        }
    }
    
    @Getter
    @Setter
    public static class Condition {
        private Object first;
        private String condition;
        private Object second;
        
        public static Condition create(Object first, String condition, Object second) {
            Condition c = new Condition();
            c.first = first;
            c.condition = condition;
            c.second = second;
            
            return c;
        }
    }
}
