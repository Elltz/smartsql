package np.com.ngopal.smart.sql.structure.utils;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import org.apache.commons.lang.time.DateFormatUtils;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
@Slf4j
public class OsDownloader {

    private ConnectionParams params;
    
    private JSch jsch;
    private Session session;
    private ChannelExec channel;


    public OsDownloader(ConnectionParams params) {
        this.params = params;        
        createJSCH();        
    }
    
    public synchronized Long fileSize(String file) throws IOException {
        
        InputStream is = openChannel("ls -l " + file);
        
        BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        
        String[] result = br.readLine().split(" ");
        
        br.close();
        
        return Long.valueOf(result[4]);
    }    
    
    
    public synchronized String downloadErrorSlowLogFileLines(String file, Integer lastDays) throws SftpException, IOException {
        
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        
        c.add(Calendar.DAY_OF_YEAR, -lastDays + 1);
        
        String command = "sed -n";
        for (int i = 0; i < lastDays; i++) {
            String dateTo = DateFormatUtils.format(c, "'# Time: 'yyyy-MM-dd");

            command += " -e '/" + dateTo + "/ , //p'";

            c.add(Calendar.DAY_OF_YEAR, 1);
        }

        command += " " + file;            
        
        InputStream is = openChannel(command);
        
        BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        
        StringBuilder builder = new StringBuilder();
        String line;
        boolean first = true;
        while ((line = br.readLine()) != null) {            
            if (line != null) {
                if (!first) {
                    builder.append("\n");
                }
                first = false;
                builder.append(line);
            } else {
                break;
            }
        }
        br.close();
        
        return builder.toString();
    } 
    
    public synchronized List<String> downloadErrorLogFileLines(String file, Integer lastDays, Integer maxLinesSize, Integer skip, boolean needErrors) throws SftpException, IOException {
        
        String command;
        if (lastDays != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DAY_OF_YEAR, -lastDays + 1);
            
            command = "sed -n";
            for (int i = 0; i < lastDays; i++) {
                String dateTo = DateFormatUtils.format(c, "yyyy-MM-dd");
                
                command += " -e '/" + dateTo + "/ , //p'";
                
                c.add(Calendar.DAY_OF_YEAR, 1);
            }
            
            command += " " + file + " | grep \\\\[ERROR\\\\] | tail -" + maxLinesSize + (skip != null && skip > 0 ? " | head -" + skip : "");
            
        } else {
            if (needErrors) {
                command = "grep -i \\\\[ERROR\\\\] " + file + " | tail -" + maxLinesSize + (skip != null && skip > 0 ? " | head -" + skip : "");
            } else {
                command = "tail -" + maxLinesSize + " " + file + (skip != null && skip > 0 ? " | head -" + skip : "");
            }
        }
        
        InputStream is = openChannel(command);
        
        BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        
        List<String> lines = new ArrayList<>(maxLinesSize);        
        String line;
        while ((line = br.readLine()) != null) {            
            if (line != null) {
                lines.add(line);
            } else {
                break;
            }
        }
        br.close();
        
        return lines;
    } 
        
    private final void createJSCH() {
        jsch = new JSch();
        jschPrivateKeySet();
    }

    private final boolean isUsingPrivateKey() {
        return params.getOsPrivateKey() != null && !params.getOsPrivateKey().isEmpty();
    }

    private final void jschPrivateKeySet() {
        try {
            jsch.removeAllIdentity();
            if (isUsingPrivateKey()) {
                jsch.addIdentity(params.getOsPrivateKey(), params.getOsPassword());
            }
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    public void disconnectFromRemote() {

        if (channel != null) {
            channel.disconnect();
            channel = null;
        }

        if (session != null) {
            session.disconnect();
            session = null;
        }
        
        jsch = null;
    }

    public boolean connectToRemote() {
        boolean res = false;
        try {
            session = jsch.getSession(params.getOsUser(), params.getOsHost(), params.getOsPort());
            session.setConfig("StrictHostKeyChecking", "no");
            if (!isUsingPrivateKey()) {
                session.setPassword(params.getOsPassword());
            }
            session.setServerAliveCountMax(3);
            session.setServerAliveInterval((int) TimeUnit.MINUTES.toMillis(1));//1 minute
            session.setDaemonThread(true);
            session.connect();
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private InputStream openChannel(String command) {
        
        if (session == null) {
            try {
                // its local
                // TODO: Refactor this part
                Runtime r = Runtime.getRuntime();
                Process p = r.exec(command);
                p.waitFor();
                return p.getInputStream();
            } catch (Throwable ex) {
                log.error(ex.getMessage(), ex);
            }
        
        } else {
            if (channel != null) {
                channel.disconnect();
                channel = null;
            }

            InputStream in = null;

            try {
                channel = (ChannelExec)session.openChannel("exec");
                channel.setErrStream(System.err);
                in = channel.getInputStream();
                channel.setCommand(command);
                channel.connect();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

            return in;
        }
        
        return null;
    }

    
}
