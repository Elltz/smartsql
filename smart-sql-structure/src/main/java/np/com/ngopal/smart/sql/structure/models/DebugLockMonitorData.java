
package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import java.util.Objects;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import np.com.ngopal.smart.sql.model.PreferenceData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DebugLockMonitorData implements FilterableData, DebugTimeProvider, DebugExplainProvider {
    
    private final SimpleObjectProperty id;
    private final SimpleStringProperty user;
    private final SimpleStringProperty host;
    private final SimpleStringProperty database;
    private final SimpleStringProperty command;
    private final SimpleObjectProperty time;
    private final SimpleStringProperty state;
    private final SimpleStringProperty query;
    
    private final SimpleStringProperty waitingTableLock;
    private final SimpleStringProperty blockingQuery;
    private final SimpleStringProperty blockingTrxId;
    private final SimpleObjectProperty blockingThread;
    private final SimpleStringProperty blockingHost;
    private final SimpleObjectProperty idleInTrx;
    private final SimpleStringProperty lockType;
    private final SimpleStringProperty lockTypeDesc;
      
 
    private Long rowsSum = null;
    
    public DebugLockMonitorData(Long id, String user, String host, String command, String state, BigDecimal time, String query, String database, String waitingTableLock, String blockingQuery, String blockingTrxId, Long blockingThread, String blockingHost, Long idleInTrx, String lockType, String lockTypeDesc) {
        this.id = new SimpleObjectProperty(id);
        this.time = new SimpleObjectProperty(time);
        this.query = new SimpleStringProperty(query);
        this.database = new SimpleStringProperty(database);
        this.user = new SimpleStringProperty(user);
        this.host = new SimpleStringProperty(host);
        this.command = new SimpleStringProperty(command);
        this.state = new SimpleStringProperty(state);
        
        this.waitingTableLock = new SimpleStringProperty(waitingTableLock);
        this.blockingQuery = new SimpleStringProperty(blockingQuery);
        this.blockingTrxId = new SimpleStringProperty(blockingTrxId);
        this.blockingThread = new SimpleObjectProperty(blockingThread);
        this.blockingHost = new SimpleStringProperty(blockingHost);
        this.idleInTrx = new SimpleObjectProperty(idleInTrx);
        this.lockType = new SimpleStringProperty(lockType);
        this.lockTypeDesc = new SimpleStringProperty(lockTypeDesc);
    }
    
    @Override
    public void setRowsSum(Long rowsSum) {
        this.rowsSum = rowsSum;
    }
    @Override
    public Long getRowsSum() {
        return rowsSum;
    } 
    
    @Override
    public BigDecimal getTime() {
        return (BigDecimal) time.get();
    }
    public void setTime(BigDecimal time) {
        this.time.set(time);
    }
    
    public Long getId() {
        return (Long) id.get();
    }
    public void setId(Long id) {
        this.id.set(id);
    }
    
    public String getUser() {
        return user.get();
    }
    public void setUser(String user) {
        this.user.set(user);
    }
    
    public String getHost() {
        return host.get();
    }
    public void setHost(String host) {
        this.host.set(host);
    }
    
    public String getCommand() {
        return command.get();
    }
    public void setCommand(String command) {
        this.command.set(command);
    }
    
    @Override
    public DebugTimeProvider getTimeProvider() {
        return this;
    }
    
    @Override
    public String getState() {
        return state.get();
    }
    public void setState(String state) {
        this.state.set(state);
    }
    
    public String getQuery() {
        return query.get();
    }
    public void setQuery(String query) {
        this.query.set(query);
    }
    
    @Override
    public String getDatabase() {
        return database.get();
    }
    public void setDatabase(String database) {
        this.database.set(database);
    }    
    
    public String getWaitingTableLock() {
        return waitingTableLock.get();
    }
    public void setWaitingTableLock(String waitingTableLock) {
        this.waitingTableLock.set(waitingTableLock);
    }
    
    public String getBlockingQuery() {
        return blockingQuery.get();
    }
    public void setBlockingQuery(String blockingQuery) {
        this.blockingQuery.set(blockingQuery);
    }
    
    public String getBlockingTrxId() {
        return blockingTrxId.get();
    }
    public void setBlockingTrxId(String blockingTrxId) {
        this.blockingTrxId.set(blockingTrxId);
    }
    
    public Long getBlockingThread() {
        return (Long) blockingThread.get();
    }
    public void setBlockingThread(Long blockingThread) {
        this.blockingThread.set(blockingThread);
    }
    
    public String getBlockingHost() {
        return blockingHost.get();
    }
    public void setBlockingHost(String blockingHost) {
        this.blockingHost.set(blockingHost);
    }
    
    public Long getIdleInTrx() {
        return (Long) idleInTrx.get();
    }
    public void setIdleInTrx(Long idleInTrx) {
        this.idleInTrx.set(idleInTrx);
    }
    
    public String getLockType() {
        return lockType.get();
    }
    public void setLockType(String lockType) {
        this.lockType.set(lockType);
    }
    
    public String getLockTypeDesc() {
        return lockTypeDesc.get();
    }
    public void setLockTypeDesc(String lockTypeDesc) {
        this.lockTypeDesc.set(lockTypeDesc);
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getUser() != null ? getUser() : "",
            getHost() != null ? getHost() : "",
            getDatabase() != null ? getDatabase() : "",
            getCommand() != null ? getCommand() : "",
            getTime() != null ? getTime().toString() : "",
            getState() != null ? getState() : "",
            getQuery() != null ? getQuery() : "",
            getWaitingTableLock() != null ? getWaitingTableLock() : "",
            getBlockingQuery() != null ? getBlockingQuery() : "",
            getBlockingTrxId() != null ? getBlockingTrxId() : "",
            getBlockingThread() != null ? getBlockingThread().toString() : "",
            getBlockingHost() != null ? getBlockingHost() : "",
            getIdleInTrx() != null ? getIdleInTrx().toString() : "",
            getLockType() != null ? getLockType() : "",
            getLockTypeDesc() != null ? getLockTypeDesc() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getUser(),
            getHost(),
            getDatabase(),
            getCommand(),
            getTime(),
            getState(),
            getQuery(),
            getWaitingTableLock(),
            getBlockingQuery(),
            getBlockingTrxId(),
            getBlockingThread(),
            getBlockingHost(),
            getIdleInTrx(),
            getLockType(),
            getLockTypeDesc()
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DebugLockMonitorData) {
            DebugLockMonitorData d = (DebugLockMonitorData) obj;
            
            return Objects.equals(d.getId(), getId()) && Objects.equals(d.getQuery(), getQuery()) && Objects.equals(d.getDatabase(), getDatabase());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        hash = 41 * hash + Objects.hashCode(this.query);
        hash = 41 * hash + Objects.hashCode(this.database);
        return hash;
    }

    @Override
    public String getColor(PreferenceData pd) {
        String color = COLORS[4];
        
        if (!"Sleep".equalsIgnoreCase(getState())) {
            
            if (getTime().doubleValue() <= 1) {
                color = COLORS[3];
            } else if (getTime().doubleValue() <= 3) {
                color = COLORS[2];
            } else if (getTime().doubleValue() <= 6) {
                color = COLORS[1];
            } else if (getTime().doubleValue() > 6) {
                color = COLORS[0];
            }
        }
        
        return color;
    }
    
}