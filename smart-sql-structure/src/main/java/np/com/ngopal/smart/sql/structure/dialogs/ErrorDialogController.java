package np.com.ngopal.smart.sql.structure.dialogs;

import java.awt.Desktop;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Window;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.utils.ErrorInfo;
import np.com.ngopal.smart.sql.structure.utils.ErrorInfoProvider;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.URLUtils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class ErrorDialogController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;
    
    private static final String ERROR_NO_SUFIX = "Error No. ";
    
//    private final Image moreDetailImage = new Image("/np/com/ngopal/smart/sql/ui/images/dialog-more-details.png");
//    private final Image fewerDetailImage = new Image("/np/com/ngopal/smart/sql/ui/images/dialog-fewer-details.png");

    @FXML
    private VBox unknownErrorVbox;
    @FXML
    private Hyperlink titleHyperlink;
    @FXML
    private Label unknownTitleLabel;
    
            
    @FXML
    private VBox knownErrorVbox;
    @FXML
    private Label titleLabel;
    @FXML
    private TextArea detailArea;
    @FXML
    private Label rootCauseLabel;
    @FXML
    private TextArea debugArea;
    @FXML
    private TextArea solutionArea;
    @FXML
    private TitledPane debugPane;
    @FXML
    private TitledPane solutionPane;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
    private String errorMessage;
    
    public void init(String titleText, ConnectionParams params) {
        init(titleText, params, null);
    }
    
    public void init(String titleText, ConnectionParams params, Throwable throwable) {
         init(titleText, params, throwable, "");
    }
    
    public void init(String titleText, ConnectionParams params, Throwable throwable, String detail) {
        
        unknownErrorVbox.managedProperty().bind(unknownErrorVbox.visibleProperty());
        knownErrorVbox.managedProperty().bind(knownErrorVbox.visibleProperty());
        
        unknownTitleLabel.managedProperty().bind(unknownTitleLabel.visibleProperty());
        titleHyperlink.managedProperty().bind(titleHyperlink.visibleProperty());
        
        
        // add error number for sql excption only
        if (throwable instanceof SQLException) {
            titleText = ERROR_NO_SUFIX + ((SQLException)throwable).getErrorCode() + "\n" + titleText;            
            errorMessage = throwable.getMessage();
        }
        
        // first try get Error Info
        ErrorInfo errorInfo = ErrorInfoProvider.getErrorInfoByThrowable(params, throwable);
        
        unknownErrorVbox.setVisible(errorInfo == null);
        knownErrorVbox.setVisible(errorInfo != null);
        
        // if errorInfo is null - than show dialog with google link
        if (errorInfo == null) {            
            titleHyperlink.setVisible(false);
            unknownTitleLabel.setText(titleText);

            if (detail != null && !detail.trim().isEmpty()) {
                if (throwable == null) {
                    detailArea.setText(detail);
                } else {
                    detailArea.setText(throwable.getMessage() + "\n" + detail);
                }
            } else {
                detailArea.setVisible(false);
                detailArea.setManaged(false);
            }
        } else {
            
            // if we have ErrorInfo than we must show all needed info
            titleLabel.setText(ERROR_NO_SUFIX + errorInfo.getErrorCode() + "\n" + errorInfo.getErrorMessage());
            if (errorInfo.getRootCause() != null) {
                rootCauseLabel.setText(rootCauseLabel.getText() + "\n" + errorInfo.getRootCause());
            } else {
                rootCauseLabel.setVisible(false);
                rootCauseLabel.setManaged(false);
            }
            
            if (errorInfo.getDebugInfo() != null && !errorInfo.getDebugInfo().isEmpty()) {
                debugArea.setText(errorInfo.getDebugInfo());
            } else {
                debugPane.setVisible(false);
                debugPane.setManaged(false);
            }
            
            if (errorInfo.getSolutionInfo() != null && !errorInfo.getSolutionInfo().isEmpty()) {
                solutionArea.setText(errorInfo.getSolutionInfo());
            } else {
                solutionPane.setVisible(false);
                solutionPane.setManaged(false);
            }
            
        }
        calculateHeightFlow(titleLabel, 400.0);
        calculateHeightFlow(rootCauseLabel, 500.0);
    }
    
    
    private Hyperlink createUsefulHyperLink(String website) {
        Hyperlink h = new Hyperlink(website);
        h.setFocusTraversable(false);
        h.setOnAction((ActionEvent event) -> {
            try {
                openInBrowser(new URL(website).toURI());
            } catch (MalformedURLException | URISyntaxException ex) {
                log.error("Error while opening error link", ex);
            }
        });
        return h;
    }
    
    private void openInBrowser(URI uri) {        
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (IOException e) {
                log.error("Error while opening error link", e);
            }
        }
    }
    
    @FXML
    public void googleAction(ActionEvent event) {
        URLUtils.openSearchUrl(Flags.SEARCH__GOOGLE_LINK, errorMessage);
    }
    
    @FXML
    public void yahooAction(ActionEvent event) {
        URLUtils.openSearchUrl(Flags.SEARCH__YAHOO_LINK, errorMessage);
    }
    
    @FXML
    public void stackoverflowAction(ActionEvent event) {
        URLUtils.openSearchUrl(Flags.SEARCH__STACKOVERFLOW_LINK, errorMessage);
    }
    
    @FXML
    public void bingAction(ActionEvent event) {
        URLUtils.openSearchUrl(Flags.SEARCH__BING_LINK, errorMessage);
    }
    
    
        
    @FXML
    public void okAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainUI.sceneProperty().addListener((ObservableValue<? extends Scene> o, Scene oldV, Scene newV) -> {
            
            if (newV != null) {
                newV.windowProperty().addListener((ObservableValue<? extends Window> o1, Window oldV1, Window newV1) -> {
                    if (newV1 != null) {
                        Platform.runLater(() -> getStage().sizeToScene());
        
                        debugPane.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                            Platform.runLater(() -> getStage().sizeToScene());
                        });
                        solutionPane.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                            Platform.runLater(() -> getStage().sizeToScene());
                        });
                        //onShowAnimationWorks();
                    }
                });
            }
        });
    }
    
    public DialogResponce getResponce() {
        return responce;
    }  

    @Override
    public void initDialog(Object... params) {
        init((String)params[0], (ConnectionParams)params[1], (Throwable)params[2], (String)params[3]);
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }

    @Override
    public String getInputValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
