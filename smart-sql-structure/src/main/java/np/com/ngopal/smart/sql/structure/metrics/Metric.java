/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.metrics;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class Metric {
    
    protected SimpleBooleanProperty skipHandleProperty;
    protected String metricDescription;
    protected String metricName;
    protected MetricExtract metricExtract;
    protected ObservableList<XYChart.Series<Number, Number>> data;    
    protected SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");//yyyy/MM/dd HH:mm:ss
    private TimeUnit timeUnit = TimeUnit.SECONDS;
    private double scaleValue = 1.0;

    public Metric(String metricname, String description) {
        metricDescription = description;
        skipHandleProperty = new SimpleBooleanProperty(false);  
        this.metricName = metricname;
        data = FXCollections.observableArrayList();
    }
       
    public void forceSkip(){
        skipHandleProperty.unbind();
        skipHandleProperty.set(true);
    }
    
    public void undoForceSkip(){
        skipHandleProperty.unbind();
        skipHandleProperty.set(false);
    }
    
    protected static final long generateTimeTick() {
        long currentTimeTick = System.currentTimeMillis();
        return currentTimeTick;
    }
    
    public void reset(){
        data.clear();
    }
    
    public void updateUI(long timeInMillis, MetricExtract extract) {
        this.metricExtract = extract;
        if (extract != null && data.isEmpty()) {
            data.addAll(getXYChartSeries());
        } else {
            long lastInputTime = 0l;
            boolean add = false;
            for (XYChart.Series<Number, Number> serie : data) {
                if (lastInputTime == 0l && !serie.getData().isEmpty()) {
                    lastInputTime = serie.getData().get(serie.getData().size() - 1).getXValue().longValue();
                    add = (timeInMillis - lastInputTime) > getTimeUnitChart().toMillis(4);                    
                }
                if (add) {
                    XYChart.Data<Number, Number> myData = new XYChart.Data();
                    myData.setXValue(lastInputTime + TimeUnit.SECONDS.toMillis(1));
                    myData.setYValue(0);
                    serie.getData().add(myData);

                    myData = new XYChart.Data();
                    myData.setXValue(timeInMillis - TimeUnit.SECONDS.toMillis(1));
                    myData.setYValue(0);
                    serie.getData().add(myData);
                }
            }
        }
    }
    
    public ObservableList<XYChart.Series<Number, Number>> getData(){
        return data;
    }
 
    private void changeListenerFunction(XYChart chart, ListChangeListener.Change<? extends XYChart.Data<Number, Number>> c) {
        Recycler.addWorkProc(new WorkProc() {

            long maxX = c.getAddedSubList().get(0).getXValue().longValue();
            double maxY = c.getAddedSubList().get(0).getYValue().doubleValue();
            double yTickUnit = 0.0;
            int deleteSuffix = -1;
            long lowerTime = 0l;
            long xTickUnit;

            @Override
            public void updateUI() {
                ((NumberAxis) chart.getYAxis()).setTickUnit((int) yTickUnit);
                ((NumberAxis) chart.getYAxis()).setUpperBound(maxY);

                ((NumberAxis) chart.getXAxis()).setTickUnit(xTickUnit);
                ((NumberAxis) chart.getXAxis()).setUpperBound(maxX);
                ((NumberAxis) chart.getXAxis()).setLowerBound(lowerTime);                
            }

            @Override
            public void run() {
                callUpdate = true;

                if (((NumberAxis) chart.getXAxis()).getUpperBound() > maxX) {
                    maxX = (long) ((NumberAxis) chart.getXAxis()).getUpperBound();
                }

                if (((NumberAxis) chart.getYAxis()).getUpperBound() > maxY) {
                    maxY = ((NumberAxis) chart.getYAxis()).getUpperBound();
                }

                yTickUnit = maxY / getYTickUnitCount();                
                xTickUnit = (long)(getTimeUnitSpaceInMillis() * scaleValue);
                lowerTime = maxX - (xTickUnit  * getXTickUnitCount());
            }
        });
    }

    protected final XYChart.Series<Number, Number> getNewXYChart() {
        XYChart.Series<Number, Number> serie = new XYChart.Series();
        serie.getData().addListener(new ListChangeListener<XYChart.Data<Number, Number>>() {
            
            //boolean serieStylerOnetimeCaller = true;
            
            @Override
            public void onChanged(ListChangeListener.Change<? extends XYChart.Data<Number, Number>> c) {
                while (c.next() && c.wasAdded()) {
                    changeListenerFunction(serie.getChart(), c);
                    serieStyler(serie);
                }
            }
        });

        return serie;
    }
    
    private void serieStyler(XYChart.Series serie){
        int index = serie.getChart().getData().indexOf(serie);
        String color = getChartColors()[index];
        
        serie.getChart().setStyle(generateStyleColorForChart());
        
        Node fillNode = serie.getNode().lookup(".chart-series-area-fill");    
        if(fillNode != null){
            fillNode.setStyle("-fx-fill: " + color  + " ;");
        }
        
        /*Node LineNode = serie.getNode().lookup(".chart-series-area-line");        
        if(LineNode != null){
            LineNode.setStyle("-fx-stroke: "+ color + " ;");
        }*/
    }
    
    public XYChart.Series<Number, Number>[] getXYChartSeries(){
        return null;
    }
    
    public final TimeUnit getTimeUnitChart(){
        return timeUnit;
    }
    
    public int getUnitTimeValue(){
        return 600;
    }
    
    public final long getTimeUnitSpaceInMillis(){
        //default is 10 mins
        return getTimeUnitChart().toMillis(getUnitTimeValue());
    }
    
    public static long convertToUnitBytes(long input, Byte to) {
        int divisor = 0;
        switch (to) {
            case Flags.KILOBYTE:
                divisor = 1024;
                break;
            case Flags.MEGABYTE:
                divisor = 1048576;
                break;
            case Flags.GIGABYTE:
                divisor = 1073741824;
                break;
        }        
        input = input / divisor;        
        return input;
    }
    
    public long getMaxYValue(){
        return 0l;
    }
    
    public StringConverter<Number> yConverter(){
        return null;
    }
    
    public StringConverter<Number> xConverter(){
        return new StringConverter<Number>() {
            @Override
            public String toString(Number numb) {
                //reAnalyseDateFormatParser();
                String to = sdf.format(new Date(numb.longValue()));
               return to;
            }

             @Override
            public Number fromString(String string) {
                try {
                    //reAnalyseDateFormatParser();
                    return sdf.parse(string).getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
            
        };
    }
    
    private void reAnalyseDateFormatParser(){
        switch(getTimeUnitChart()){            
            case HOURS: case DAYS:
                sdf.applyPattern("MM/dd HH:mm:ss");
                break;
            default:
                sdf.applyPattern("HH:mm:ss");
                break;
        }
    }
    
    public String getMetricDescription(){
        return metricDescription;
    }

    public String getMetricName() {
        return metricName;
    }
    
    public int getYTickUnitCount(){
        return 10;
    }
    
    public int getXTickUnitCount(){
        return 6;
    }
    
    public String[] getChartColors(){
        return null;
    }
    
    public final String generateStyleColorForChart(){
        StringBuilder sb = new StringBuilder(50);
        
        if(getChartColors() != null){
            int index = 1;
            for(String s : getChartColors()){
                sb.append("CHART_COLOR_");
                sb.append(index);
                sb.append(':');
                sb.append(' ');
                sb.append(s);
                sb.append(';');
                index++;
            }
        }
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) || (obj != null && obj instanceof Metric && obj.getClass().getName().equals(this.getClass().getName()));
    }

    public double getScaleValue() {
        return scaleValue;
    }

    public void setScaleValue(double scaleValue) {
        this.scaleValue = scaleValue;
    }
    
}
