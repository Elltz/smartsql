/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.executable;

import java.io.File;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public interface ProgressIndicatorInterface {
    
    void onStart(String what);
    void onUpdate(long current, long total);
    void onDone(File outfile);
    
}
