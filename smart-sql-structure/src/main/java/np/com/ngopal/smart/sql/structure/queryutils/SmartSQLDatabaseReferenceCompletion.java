package np.com.ngopal.smart.sql.structure.queryutils;

import java.util.Objects;
import javax.swing.Icon;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SmartSQLDatabaseReferenceCompletion extends BasicCompletion {

    private final String referenceDatabase;
    private final Object dependedObject;
    
    public SmartSQLDatabaseReferenceCompletion(CompletionProvider provider, String replacementText, Object dependedObject, String referenceDatabase, Icon icon){
        super(provider, replacementText, null);
        
        this.dependedObject = dependedObject;
        this.referenceDatabase = referenceDatabase;
        
        setIcon(icon);
    }
    

    public String getName() {
        
        int i = getReplacementText().lastIndexOf('.') + 1;
        if(i == -1) {
            return getReplacementText(); 
        }
        
        return getReplacementText().substring(i);
    }  

    @Override
    public String getCompareText() {
        return getCurrentText();
    }
        
    public String getCurrentText() {
        return getName();
    }

    public String getReferenceDatabase() {
        return referenceDatabase;
    }
    
    public Object getDependedObject() {
        return dependedObject;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SmartSQLDatabaseReferenceCompletion) {
            SmartSQLDatabaseReferenceCompletion sc = (SmartSQLDatabaseReferenceCompletion) obj;
            
            return Objects.equals(sc.getCurrentText(), getCurrentText()) && Objects.equals(sc.getIcon(), getIcon());
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(getCurrentText());
        hash = 73 * hash + Objects.hashCode(getIcon());
        return hash;
    }
}
