/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.DBTable;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class SelectionTable extends DBTable {
    private boolean selected;

}
