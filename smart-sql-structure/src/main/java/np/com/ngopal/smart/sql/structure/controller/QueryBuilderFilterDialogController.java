package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.structure.utils.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.utils.JoinMaker;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class QueryBuilderFilterDialogController extends BaseController implements Initializable {
    
    private static final String[] CONDITIONS = new String[]{"=", "<>", ">", "<", "LIKE"};
        
    private static final int DEFAULT_ROWS_SIZE = 4;
    
    @FXML
    private GridPane gridPane;
    
    @FXML
    private ComboBox<String> joinType;
    
    @FXML
    private ComboBox<TableProviderWrapper> firstComboboxField;
    @FXML
    private ComboBox<TableProviderWrapper> secondComboboxField;
    
    
    @FXML
    private ComboBox<String> firstCombobox;
    @FXML
    private ComboBox<String> conditionsCombobox;
    @FXML
    private ComboBox<String> secondCombobox;
    
    @FXML
    private Button filterButton;
    
    
    private List<ComboBox<String>> firstComboboxs;
    private List<ComboBox<String>> conditionsComboboxs;
    private List<ComboBox<String>> secondComboboxs;
    
    private Node focusedComponent;
    
    @FXML
    private AnchorPane mainUI;
    
    private JoinMaker.JoinCondition joinCondition;
    private static JoinMaker.JoinResult joinResult;
    
    private TableProvider firstTable;
    private TableProvider secondTable;
    
    private boolean canChangeFirst = false;
    private boolean canChangeSecond = false;
    private boolean accepted = false;
    private boolean clearOnCancel = true;
    
    private DBService dbService;
        
    public void init(DBService dbService, JoinMaker.JoinResult joinResult, JoinMaker.JoinCondition joinCondition, List<TableProvider> tables, boolean canChangeFirst, boolean canChangeSecond, boolean clearOnCancel) {
        this.dbService = dbService;
        this.joinResult = joinResult;
        this.joinCondition = joinCondition;
        
        if (joinCondition.getJoinType() != null && !joinCondition.getJoinType().isEmpty()) {
            joinType.getSelectionModel().select(joinCondition.getJoinType());
        }
        
        this.canChangeFirst = canChangeFirst;
        this.canChangeSecond = canChangeSecond;
        this.clearOnCancel = clearOnCancel;
        
        firstTable = joinCondition.getFirst();
        secondTable = joinCondition.getSecond();
        
        firstComboboxField.setDisable(!canChangeFirst);
        firstComboboxField.setStyle("-fx-opacity: 1");
        
        secondComboboxField.setDisable(!canChangeSecond);
        secondComboboxField.setStyle("-fx-opacity: 1");
        
        List<String> firstColumns = loadColumns(joinCondition.getFirst());
        List<String> secondColumns = loadColumns(joinCondition.getSecond());
     
        firstCombobox.getItems().setAll(firstColumns);
        secondCombobox.getItems().setAll(secondColumns);
        
        
        firstComboboxs = new ArrayList<>();
        conditionsComboboxs = new ArrayList<>();
        secondComboboxs = new ArrayList<>();             
        
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            filterButton.setDisable(!checkFilters());
        };
        
        firstCombobox.getEditor().textProperty().addListener(listener);
        secondCombobox.getEditor().textProperty().addListener(listener);

        
        int row = 4;
        for (int i = 0; i < DEFAULT_ROWS_SIZE; i++) {
            
            ComboBox<String> columnBox = new ComboBox<>(FXCollections.observableArrayList(firstColumns));
            new AutoCompleteComboBoxListener<>(columnBox);
            columnBox.getEditor().textProperty().addListener(listener);
            firstComboboxs.add(columnBox);
            
            ComboBox<String> conditionsBox = new ComboBox<>(FXCollections.observableArrayList(CONDITIONS));
            conditionsBox.setPrefWidth(150);
            conditionsBox.getSelectionModel().select(0);
            conditionsComboboxs.add(conditionsBox);
            
            ComboBox<String> field = new ComboBox<>(FXCollections.observableArrayList(secondColumns));
            new AutoCompleteComboBoxListener<>(field);
            field.getEditor().textProperty().addListener(listener);
            secondComboboxs.add(field);
           
            gridPane.add(columnBox, 0, row);
            gridPane.add(conditionsBox, 1, row);
            gridPane.add(field, 2, row);
            
            RowConstraints rc = new RowConstraints();
            gridPane.getRowConstraints().add(rc);
            row++;
        }
        
        if (tables != null) {
            firstComboboxField.getItems().addAll(tables.stream().map(t -> TableProviderWrapper.wrap(t)).collect(Collectors.toList()));
        }
        firstComboboxField.getSelectionModel().select(TableProviderWrapper.wrap(joinCondition.getFirst()));
        firstComboboxField.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TableProviderWrapper> observable, TableProviderWrapper oldValue, TableProviderWrapper newValue) -> {
            
            this.firstTable = newValue.table;
            
            joinConditionChanged();
        });
        
        if (tables != null) {
            secondComboboxField.getItems().addAll(tables.stream().map(t -> TableProviderWrapper.wrap(t)).collect(Collectors.toList()));
        }
        secondComboboxField.getSelectionModel().select(TableProviderWrapper.wrap(joinCondition.getSecond()));
        secondComboboxField.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TableProviderWrapper> observable, TableProviderWrapper oldValue, TableProviderWrapper newValue) -> {
            
            this.secondTable = newValue.table;
            
            joinConditionChanged();
        });
        
        joinConditionChanged();
        
        if (focusedComponent == null) {
            focusedComponent = secondCombobox;
        }
    }
     
    
    
    private List<String> loadColumns(TableProvider table) {
        return table != null && table.getColumns() != null ? table.getColumns().stream().map(c -> c.getName()).collect(Collectors.toList()) : new ArrayList<>();
    }
    
    private void joinConditionChanged() {
        
        boolean sameTables = firstTable == secondTable;
        filterButton.setDisable(sameTables);
        
        if (!sameTables){
                        
            if (canChangeSecond && canChangeFirst) {
                this.joinCondition = null;

                for (TableProvider tp: joinResult.getJoinTables().keySet()) {
                    
                    boolean found = false;
                    List<JoinMaker.JoinCondition> list = joinResult.getTableJoinCondition(tp);
                    for (JoinMaker.JoinCondition jc: list) {
                        if ((jc.getFirst() == firstTable || jc.getFirst() == secondTable) && (jc.getSecond() == secondTable || jc.getSecond() == firstTable)) {
                            joinCondition = jc;
                            found = true;
                            break;
                        }
                    }
                    
                    if (found) {
                        break;
                    }
                }


                if (this.joinCondition == null) {
                    this.joinCondition = JoinMaker.findSameNamesJoinCondition(dbService, firstTable, secondTable);

                    if (joinCondition == null) {
                        joinCondition = JoinMaker.JoinCondition.create(null, firstTable, secondTable);
                    } else {
                        joinCondition.setJoinTable(null);
                    }
                }            
            } else {
//                joinCondition.setJoinTable(secondTable);
                joinCondition.setFirst(firstTable);
                joinCondition.setSecond(secondTable);
            }
            
            firstCombobox.getItems().clear();
            secondCombobox.getItems().clear();
            
            List<String> firstColumns = loadColumns(firstTable);
            firstCombobox.getItems().addAll(firstColumns);

            List<String> secondColumns = loadColumns(secondTable);
            secondCombobox.getItems().addAll(secondColumns);



            if (!joinCondition.isEmptyConditions()) {

                JoinMaker.Condition fd = joinCondition.getCondition(0);

                String fv0 = joinCondition.getFirst() == firstTable ? fd.getFirst() == null ? "" : (fd.getFirst() instanceof Column ? ((Column)fd.getFirst()).getName() : fd.getFirst().toString()) : (fd.getSecond() instanceof Column ? ((Column)fd.getSecond()).getName() : fd.getSecond() == null ? "" : fd.getSecond().toString());
                String sv0 = joinCondition.getSecond() == firstTable ? fd.getFirst() == null ? "" : (fd.getFirst() instanceof Column ? ((Column)fd.getFirst()).getName() : fd.getFirst().toString()) : (fd.getSecond() instanceof Column ? ((Column)fd.getSecond()).getName() : fd.getSecond() == null ? "" : fd.getSecond().toString());
                firstCombobox.setValue(fv0);
                conditionsCombobox.getSelectionModel().select(fd.getCondition());
                secondCombobox.setValue(sv0);

            } else {
                firstCombobox.getSelectionModel().clearSelection();
                firstCombobox.setValue("");
                conditionsCombobox.getSelectionModel().select(0);
                secondCombobox.getSelectionModel().clearSelection();
                secondCombobox.setValue("");
            }


            for (int i = 0; i < DEFAULT_ROWS_SIZE; i++) {

                ComboBox<String> first = firstComboboxs.get(i);            
                ComboBox<String> conditionsBox = conditionsComboboxs.get(i);
                ComboBox<String> second = secondComboboxs.get(i);

                second.getItems().clear();
                second.getItems().addAll(secondColumns);

                first.getItems().clear();
                first.getItems().addAll(firstColumns);

                if (i < joinCondition.conditionSize() - 1) {

                    JoinMaker.Condition fd = joinCondition.getCondition(i + 1);

                    String fv = joinCondition.getFirst() == firstTable ? fd.getFirst() == null ? "" : (fd.getFirst() instanceof Column ? ((Column)fd.getFirst()).getName() : fd.getFirst().toString()) : (fd.getSecond() instanceof Column ? ((Column)fd.getSecond()).getName() : fd.getSecond() == null ? "" : fd.getSecond().toString());
                    String sv = joinCondition.getSecond() == firstTable ? fd.getFirst() == null ? "" : (fd.getFirst() instanceof Column ? ((Column)fd.getFirst()).getName() : fd.getFirst().toString()) : (fd.getSecond() instanceof Column ? ((Column)fd.getSecond()).getName() : fd.getSecond() == null ? "" : fd.getSecond().toString());
                
                    first.setValue(fv);     
                    conditionsBox.getSelectionModel().select(fd.getCondition());
                    second.setValue(sv);                

                } else {
                    first.getSelectionModel().clearSelection();
                    first.setValue("");                
                    conditionsBox.getSelectionModel().select(0);
                    second.getSelectionModel().clearSelection();
                    second.setValue("");                
                }
            }
        }
    }
    
    
    @FXML
    void clearAction(ActionEvent event) {
        
        firstCombobox.getSelectionModel().clearSelection();   
        firstCombobox.getEditor().setText("");
        if (firstComboboxs != null) {
            for (ComboBox<String> c: firstComboboxs) {
                c.getSelectionModel().clearSelection();
                c.getEditor().setText("");
            }
        }
        
        conditionsCombobox.getSelectionModel().select(0);
        if (conditionsComboboxs != null) {
            for (ComboBox<String> c: conditionsComboboxs) {
                c.getSelectionModel().select(0);
            }
        }
        
        secondCombobox.getSelectionModel().clearSelection();
        secondCombobox.getEditor().setText("");
        if (secondComboboxs != null) {
            for (ComboBox<String> c: secondComboboxs) {
                c.getSelectionModel().clearSelection();
                c.getEditor().setText("");
            }
        }
    }
    
    @FXML
    void filterAction(ActionEvent event) {
        
        joinCondition.clearConditions();
        joinCondition.setJoinType(joinType.getSelectionModel().getSelectedItem());
        
        JoinMaker.Condition condition = getFilterData(firstCombobox, conditionsCombobox, secondCombobox);
        if (condition != null) {
            joinCondition.addCondition(condition);
        }
        
        if (firstComboboxs != null) {
            for (int i = 0; i < DEFAULT_ROWS_SIZE; i++) {            
                condition = getFilterData(firstComboboxs.get(i), conditionsComboboxs.get(i), secondComboboxs.get(i));
                if (condition != null) {
                    joinCondition.addCondition(condition);
                }
            }
        }
                
        if (joinCondition.getJoinTable() != null) {
            List<JoinMaker.JoinCondition> list = joinResult.getTableJoinCondition(joinCondition.getJoinTable());
            if (list == null) {
                if (!joinCondition.isEmptyConditions()) {
                    joinResult.addTableWithJoinCondition(joinCondition.getJoinTable(), joinCondition);
                }
            } else {
                if (joinCondition.isEmptyConditions()) {
                    joinResult.addWithNonJoinContion(joinCondition);
                }
            }
            
        } else if (!joinCondition.isEmptyConditions()) {
            
            // try find where need add this condition
            for (int i = joinResult.getAllTables().size() - 1; i >= 0; i--) {
                TableProvider tp = joinResult.getAllTables().get(i);
                
                if (tp == joinCondition.getFirst() || tp == joinCondition.getSecond()) {
//                    joinCondition.setJoinTable(joinCondition.getSecond());
                    joinResult.addTableWithJoinCondition(joinCondition.getJoinTable(), joinCondition);
                    break;
                }
            }
        }
        
        accepted = true;
        getStage().close();
    }

    private JoinMaker.Condition getFilterData(ComboBox<String> column, ComboBox<String> condition, ComboBox<String> value) {
        String fr = column.getEditor().getText();
        String resultCondition = condition.getSelectionModel().getSelectedItem();
        String sr = value.getEditor().getText();
        
        if (!fr.trim().isEmpty() && resultCondition != null && !sr.trim().isEmpty()) {       
            
            fr = joinCondition.getFirst() == firstTable ? column.getEditor().getText() : value.getEditor().getText();
            sr = joinCondition.getFirst() == firstTable ? value.getEditor().getText() : column.getEditor().getText();
            
            return JoinMaker.Condition.create(findColumn(joinCondition.getFirst(), fr), resultCondition, findColumn(joinCondition.getSecond(), sr));
        }
        
        return null;
    }
    
    private Object findColumn(TableProvider table, String columnName) {
        if (table.getColumns() != null) {
            for (Column c: table.getColumns()) {
                if (c.getName().equalsIgnoreCase(columnName.trim())) {
                    return c;
                }
            }
        }
        return columnName;
    }
    
    public boolean checkFilters() {
        String fr = firstCombobox.getEditor().getText();
        String sr = secondCombobox.getEditor().getText();
        
        if (fr.trim().isEmpty() && !sr.trim().isEmpty() || !fr.trim().isEmpty() && sr.trim().isEmpty()) {
            return false;
        }
        
        for (int i = 0; i < DEFAULT_ROWS_SIZE; i++) {
            ComboBox fc = firstComboboxs.get(i);
            ComboBox sc = secondComboboxs.get(i);
            
            fr = fc.getEditor().getText();
            sr = sc.getEditor().getText();
            
            if (fr.trim().isEmpty() && !sr.trim().isEmpty() || !fr.trim().isEmpty() && sr.trim().isEmpty()) {
                return false;
            }
        }
        
        return true;
    }
    
    @FXML
    void cancelAction(ActionEvent event) {
        accepted = false;
        if (!canChangeFirst && clearOnCancel) {
            joinCondition.clearConditions();
        }
        getStage().close();
    }

    
    public boolean isAccepted() {
        return accepted;
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        joinType.getItems().addAll("", "INNER", "LEFT", "RIGHT");
        joinType.getSelectionModel().clearSelection();
        
        conditionsCombobox.getItems().setAll(CONDITIONS);
        conditionsCombobox.getSelectionModel().select("=");
        
        mainUI.sceneProperty().addListener((ObservableValue<? extends Scene> ov, Scene t, Scene t1) -> {
            if (focusedComponent != null) {
                focusedComponent.requestFocus();
            }
        });
        
        new AutoCompleteComboBoxListener<>(firstCombobox);
        new AutoCompleteComboBoxListener<>(secondCombobox);        
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
    
    
    private static class TableProviderWrapper {
        private TableProvider table;
        
        public static TableProviderWrapper wrap(TableProvider table) {
            return new TableProviderWrapper(table);
        }
        
        public TableProviderWrapper(TableProvider table) {
            this.table = table;
        }
        
        @Override
        public int hashCode() {
            int hash = 7;
            hash = 29 * hash + Objects.hashCode(this.table);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            return obj instanceof TableProviderWrapper && ((TableProviderWrapper)obj).table == table;
        }
        
        @Override
        public String toString() {
            Integer idx = joinResult.getSameTableIndexes().get(table);
            if (idx == null) {
                idx = 0;
            }

            return table.getName() + (idx > 0 ? idx : "");
        }
    }
}