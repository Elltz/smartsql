/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.models;


import java.util.LinkedList;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;
/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 */
public class RowData implements FilterableData{

    private SimpleBooleanProperty selected = new SimpleBooleanProperty(false);

    private List data;

    private boolean dataRow = true;
    
    public RowData() {
        data = new LinkedList<>();
    }

    public RowData(List rowData) {
        data = rowData;
    }

    public void setDataRow(boolean dataRow) {
        this.dataRow = dataRow;
    }
    public boolean isDataRow() {
        return dataRow;
    }
    
    public SimpleBooleanProperty selected() {
        return selected;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public boolean isNewRow() {
        return false;
    }

    @Override
    public String[] getValuesFoFilter() {
        String[] result = new String[data.size()];
        int i = 0;
        for (Object obj: data) {
            result[i] = obj != null ? obj.toString() : "null";
            i++;
    }
        return result;
    }

}
