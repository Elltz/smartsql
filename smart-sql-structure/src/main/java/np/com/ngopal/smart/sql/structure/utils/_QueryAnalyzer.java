
package np.com.ngopal.smart.sql.structure.utils;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public final class _QueryAnalyzer {
    
//    private final Pattern PATTERN_COUNT = Pattern.compile("count\\(\\s*\\*\\s*\\)", Pattern.CASE_INSENSITIVE);
//    private final Pattern PATTERN_55 = Pattern.compile("<[\\w\\s]*>");
//    private final Pattern PATTERN_56 = Pattern.compile("\\b['\"\\.\\w]+\\b\\s+IN\\s*\\(\\s*SELECT\\s", Pattern.CASE_INSENSITIVE);
//    private final Pattern PATTERN_SUB_SELECT = Pattern.compile("/\\*\\s*SELECT\\s*#\\s*\\d+\\s*\\*/", Pattern.CASE_INSENSITIVE);
//    
//    private static final String RECOMENDATION_1 = "MySQL SERVER could not use index since the filter column is using %s";
//    private static final String RECOMENDATION_2 = "The Table column data types and operand data types are mismatched. MySQL could not use index up to 5.5 version(It applied MySQL 5.5 version)";
//    private static final String RECOMENDATION_3 = "Join condition is weak. Primary / Unique keys are not involved in the join condition/s.";
//    private static final String RECOMENDATION_4 = "A new composite index on (%s) can help the query retrieve records faster";
//    private static final String RECOMENDATION_4_SPATIAL = "A new composite spatial index on (%s) can help the query retrieve records faster";
//    private static final String RECOMENDATION_4_FULLTEXT = "A new full-text spatial index on (%s) can help the query retrieve records faster";
//    private static final String RECOMENDATION_5 = "A new index on (%s) can help the query retrieve records faster";
//    private static final String RECOMENDATION_5_SPATIAL = "A new spatial index on (%s) can help the query retrieve records faster";
//    private static final String RECOMENDATION_5_FULLTEXT = "A new full-text index on (%s) can help the query retrieve records faster";
//    private static final String RECOMENDATION_6 = "The “OR” operator in JOIN condition kills the performance. We recommend to avoid the OR operator in JOIN condition.";
//    private static final String RECOMENDATION_7 = "The JOIN condition column \"%s\" is/are using functions in the qyery. Hence, MySQL optimizer can’t use Index/es";
//    private static final String RECOMENDATION_8 = "the %s Join Condition for table %s is missing";
//    private static final String RECOMENDATION_9 = "The Query is built with %d(N) tables and it should come before %d(N-1) JOIN conditions but the query has 0 JOIN Conditions";
//    private static final String RECOMENDATION_10 = "the JOIN condition is not strong. We recommend building Foreign key constraints between the tables %s";
//    private static final String RECOMENDATION_11 = "Please avoid single quotes for operand of integer data types column \"%s\" in where condition.";
//    private static final String RECOMENDATION_12 = "Please consider 'Not null filter' as a range filter in the query.";
//    private static final String RECOMENDATION_13 = "Please check GROUP BY columns. Order same as ORDER BY columns order.";
//    private static final String RECOMENDATION_14 = "The Query is missing partition column %s in where clause. If the query 'where clause' has Partition column filter, then the query gives significant performance.";
//    private static final String RECOMENDATION_15 = "We advise against function usage on JOIN/WHERE/GROUP BY/ORDER BY clause columns for best performance";
//    private static final String RECOMENDATION_16 = "The Database design needs to be improved based on Normalization.";
//    private static final String RECOMENDATION_17 = "Avoid LIKE expressions with leading wildcards (e.g. '%TERM')";
//    private static final String RECOMENDATION_18 = "We recommend replacing count(*) with count(1) for better performance.";
//    private static final String RECOMENDATION_19 = "MySQL generally cannot use indexes on columns unless the columns are isolated in the query. \"Isolating\" the column means it should not be part of an expression or be inside a function in the query.";
//    private static final String RECOMENDATION_20 = "A SELECT statement without WHERE/GROUP/LIMIT clauses could examine many more rows than intended.";
//    private static final String RECOMENDATION_21 = "Paginating a result set with LIMIT and OFFSET O(n^2) complexity, will cause performance problems as the data grows larger. Pagination techniques such as bookmarked scans are more efficient.";
//    private static final String RECOMENDATION_22 = "ORDER BY RAND() or ORDER BY RANDOM() is a very inefficient way of retrieving a random row from the results because it sorts the entire result and then throws most of it away.";
//    private static final String RECOMENDATION_23 = "GROUP BY or ORDER BY clauses with columns from different tables with different columns order or any column with expressions, will force the use of a temporary table and filesort. This lead to huge performance problem and can consume large amounts of memory and temporary space on disk.";
//    private static final String RECOMENDATION_24 = "GROUP BY or ORDER BY clauses that sort the results in different directions prevent indexes from being used. All expressions in the ORDER BY clause must be ordered either by using ASC or DESC so that MySQL can use an index.";
//    private static final String RECOMENDATION_25 = "Selecting all columns with the * wildcard will cause the query’s meaning and behavior to change if the table’s schema changes. Might also make the query retrieve too much data.";
//    private static final String RECOMENDATION_26 = "Selecting non-grouped columns from a GROUP BY query can cause non-deterministic results.";
//    private static final String RECOMENDATION_27 = "LIMIT without ORDER BY clauses non-deterministic results, depending on the query execution plan.";
//    private static final String RECOMENDATION_28 = "If the JOIN condition (%s) columns are not INTEGER (TINYINT, SMALLINT, INT, MEDIUMINT, BIGINT) data type they may require more resources to join tables. We recommend using integer column join between tables.";
//    private static final String RECOMENDATION_29 = "We advise against using big data types (TINYBLOB / BLOB / MEDIUMBLOB / LONGBLOB / TINYTEXT /TEXT/ MEDIUMTEXT /LONGTEXT) in ORDER BY/ GROUP BY clause of the query";
//    private static final String RECOMENDATION_30 = "The Query's ORDER BY clause is used by AUTO_INCREMENT/ UNIQUE/ PRIMARY KEY columns results to ignore indexes on WHERE Clause filters";
//    private static final String RECOMENDATION_31 = "It is really bad practice to use PRIMARY KEY/UNIQUE Key/AUTO_INCREMENT columns in the Group by(Distinct) clause [for avoid duplicates] in the query.";
//    private static final String RECOMENDATION_32 = "The MySQL 5.7 version come with  generated columns feature , it can help to improve performance for function / column with expression. \nWe recommend to upgrade MySQL 5.7  for more details https://dev.mysql.com/doc/refman/5.7/en/create-table-generated-columns.html \nIf the MySQL 5.7+ then we need to recommend to add generated column + index ";
//    
//    private static final String SQL_1 = "Alter table %s add index `idx__%s`(%s);";
//    private static final String SQL_2 = "ALTER TABLE %s ADD COLUMN %s %s GENERATED ALWAYS AS (%s) STORED, add index `idx__%s`(%s);";
//    private static final String SQL_SPATIAL = "Alter table %s add spatial index `idx__%s`(%s);";
//    private static final String SQL_FULLTEXT = "Alter table %s add fulltext index `idx__%s`(%s);";
//    
//    private static final String FUNCTION__MAX = "MAX";
//    private static final String FUNCTION__MIN = "MIN";
//    
//    private static final String IS_OPERAND          = "is";
//    private static final String IS_NOT_OPERAND      = "is not";
//    private static final String LIKE_OPERAND        = "like";
//    private static final String LIKE_PERCENT_VALUE  = "'%";
//    
//    
//    private static final String NULL_VALUE  = "null";
//    private static final String FALSE_VALUE = "false";
//    private static final String TRUE_VALUE  = "true";
//    
//    
//    private int maxId;
//    
//    private List<WarningsResult> warningsResults;
//    private Map<String, Integer> queriesDegree;
//    private Map<String, String> queries;
//    private Map<String, List<String>> hierarchy;
//    
//    private Map<String, AnalyzerResult> analyzerResultMap;
//    
//    private final List<ExplainResult> vQueryExplainResults = new ArrayList<>();
//    private final List<ExplainResult> vNQueryExplainResults = new ArrayList<>();
//    
//    private List<AnalyzerResult> analyzerResult;
//    
//    private Database database;
//    private String query;
//    private String optimizerQuery;
//    private String smartMySQLRewrittenQuery;
//    
//    private DBService service;
//    private ConnectionParams params;
//    
//    private Map<String, Set<Column>> cardinalityColumns = new HashMap<>();
//    
//    @Getter
//    private Long rowsSum;
//    
//    @Getter
//    private Long rowsMulty = 0l;
//    
//    private int indexMaxNoCount = 4;
//    private int indexSubstrLength = 16;
//    
//    boolean rowsLessThen10 = true;
//    boolean rowsLessThen10NeedStop = false;
//    
//    boolean withCaching = true;
//    
//    private static final List<String> DATATYPE_DEGREE = Arrays.asList(new String[] {"BIGINT", "MEDIUMINT", "INTEGER", "INT", "DECIMAL", "NUMERIC", "FLOAT", "DOUBLE", "DATETIME", "TIMESTAMP", "DATE", "TIME", "YEAR", "SMALLINT", "ENUM", "SET", "CHAR", "VARCHAR", "TINYTEXT", "TEXT", "LONGTEXT", "MEDIUMTEXT", "BINARY", "VARBINARY", "INYBLOB", "BLOB", "MEDIUMBLOB", "LONGBLOB", "TINYINT", "BIT", "BOOLIAN"});
//    private static final List<String> LOWER_DEGREE_NAMES = Arrays.asList(new String[] {"status", "state", "type", "delete", "flag", "enum", "set", "active", "pending", "expired"});
//    
//    private ColumnCardinalityService columnCardinalityService;
//    
//    public _QueryAnalyzer(ColumnCardinalityService columnCardinalityService, Database database, ConnectionParams params, DBService service, String query) {
//        this(columnCardinalityService, database, params, service, query, 4, 16);
//    }
//    
//    public _QueryAnalyzer(ColumnCardinalityService columnCardinalityService, Database database, ConnectionParams params, DBService service, String query, int indexMaxNoCount, int indexSubstrLength) {
//        this.columnCardinalityService = columnCardinalityService;
//        
//        this.database = database;
//        // a hack
//        this.query = QueryFormat.convertToExplainQuery("", query);
//        if (this.query != null) {
//            this.query = this.query.trim();
//        }
//        this.service = service;
//        this.params = params;
//        
//        this.indexMaxNoCount = indexMaxNoCount;
//        this.indexSubstrLength = indexSubstrLength;
//    }
//    
//    public boolean analyze(UIController controller) throws SQLException {
//        return analyze(controller, true);
//    }
//    
//    public boolean analyze(UIController controller, boolean withCaching) throws SQLException {
//        try {
//            queries = new LinkedHashMap<>();
//            hierarchy = new LinkedHashMap<>();
//            queriesDegree = new LinkedHashMap<>();
//
//            rowsLessThen10 = true;
//            rowsLessThen10NeedStop = false;
//            
//            this.withCaching = withCaching;
//
//            if (query == null) {
//                throw new SQLException("Query is wrong!");
//            }
//
//            String vQuery = query.replaceAll("\\s+", " ").trim();        
//
//            //vQuery = convertNQuery_56(vQuery);
//
//            maxId = 1;        
//            Map<String, String> vQueriesTypes = analyzeQuery(database, vQueryExplainResults, vQuery, "Query", true);
//
//            // find optimized query after show warnings
//            String vNQuery = null;
//
//            if (warningsResults != null) {
//                // if warningsResults size is 1, that it containds needed query
//                if (warningsResults.size() == 1) {
//                    vNQuery = warningsResults.get(0).getMessage();
//                } else {
//                    // find with code 1003
//                    for (WarningsResult wr: warningsResults) {
//                        if (wr.getCode() == 1003) {
//                            vNQuery = wr.getMessage();
//                            break;
//                        }
//                    }
//                }
//            }
//            
//            database.clearIndexCache();
//
//            optimizerQuery = vNQuery;
//            
//            Map<String, List<Map<String, String>>> cachePartition = new HashMap<>();
//
//            AnalyzerResult vResult = new AnalyzerResult(null, service, params, database, "V_Query", maxId, null, vQuery, optimizerQuery, false, cachePartition);        
//            vResult.setQueryAnalyzer(this);
//            AnalyzerResult nResult = null;
//            Map<String, String> vNQueriesTypes = new HashMap<>();
//            if (vNQuery != null) {
//
//                //vNQuery = vNQuery.replaceAll("/\\* SELECT#\\d+ \\*/", " ").trim();
//                vNQuery = convertNQuery_SubSelect(vNQuery);
//                String convertedNQuery = convertNQuery_55(vNQuery);
//                // run for it analyzer
//                try {
//                    vNQueriesTypes = analyzeQuery(database, vNQueryExplainResults, convertedNQuery, "NQuery", false);
//
//
//                    int nDegree = 0;
//                    for (int index = 1; index <= maxId; index++) {            
//                        String key = "NQuery" + index;            
//                        String query = queries.get(key);
//                        if (query != null) {
//                            nDegree++;
//                        }
//                    }
//
//                    nResult = new AnalyzerResult(null, service, params, database, "V_NQuery", nDegree, null, convertedNQuery, optimizerQuery, false, cachePartition);
//                    nResult.setQueryAnalyzer(this);
//                } catch (SQLException ex) {
//                    // do nothing
//                }
//            }
//
//
//            analyzerResultMap = new HashMap<>();
//            for (int index = 1; index <= queries.size(); index++) {
//
//                String key = "Query" + index;            
//                String query = queries.get(key);
//                if (query != null) {
//                    AnalyzerResult result = new AnalyzerResult(vResult, service, params, database, key, queriesDegree.get(key), vQueriesTypes.get(key), query, optimizerQuery, true, cachePartition);
//                    result.setQueryAnalyzer(this);
//
//                    analyzerResultMap.put(key, result);
//
//
//                    AnalyzerResult parentResult = null;
//
//                    for (String s: hierarchy.keySet()) {
//                        List<String> keys = hierarchy.get(s);
//                        if (keys != null && keys.contains(key)) {
//                            parentResult = analyzerResultMap.get(s);
//                        }
//                    }
//
//                    if (parentResult == null) {
//                        parentResult = vResult;
//                    }
//                    parentResult.addChild(result);
//                }
//            }
//
//            // in NQuery in query - null, than add GOOD
//            for (int index = 1; index <= queries.size(); index++) {
//
//                if (nResult != null) {
//
//                    String key = "NQuery" + index;
//                    String query = queries.get(key);
//                    if (query != null) {
//
//                        AnalyzerResult result = new AnalyzerResult(nResult, service, params, database, key, queriesDegree.get(key), vNQueriesTypes.get(key), query, optimizerQuery, true, cachePartition);
//                        result.setQueryAnalyzer(this);
//
//                        analyzerResultMap.put(key, result);
//
//
//                        AnalyzerResult parentresult = null;
//
//                        for (String s: hierarchy.keySet()) {
//                            List<String> keys = hierarchy.get(s);
//                            if (keys != null && keys.contains(key)) {
//                                parentresult = analyzerResultMap.get(s);
//                            }
//                        }
//
//                        if (parentresult == null) {
//                            parentresult = nResult;
//                        }
//                        parentresult.addChild(result);
//                    }
//                }
//            }
//
//            CountDownLatch lock = new CountDownLatch(1);
//            if (controller != null && rowsLessThen10) {
//
//                Platform.runLater(() -> {
//                    DialogResponce dr = DialogHelper.showConfirm(controller, "SQL Optimizer", "The query is just scan 10 rows for retrieve result. SmartMySQL\n optimizer may not tune on this data. It may help to improve\n on production/many records tables. Would you like to continue?", controller.getStage(), false);
//                    if (dr == DialogResponce.CANCEL || dr == DialogResponce.NO || dr == DialogResponce.NO_TO_ALL) {
//                        rowsLessThen10NeedStop = true;
//                    }
//
//                    lock.countDown();
//                });
//
//                try {
//                    lock.await();
//                } catch (InterruptedException ex) {
//                    log.error(ex.getMessage(), ex);
//                }
//
//                if (rowsLessThen10NeedStop) {
//                    return false;
//                }
//            } 
//
//            if (service != null) {
//                if (vResult.getChildren() == null && nResult == null) {
//                    checkRecomendations(Arrays.asList(new AnalyzerResult[]{vResult}), new ArrayList<>());
//                } else {
//                    checkRecomendations(vResult.getChildren(), nResult != null ? nResult.getChildren() : new ArrayList<>());
//                }
//            }
//
//            analyzerResult =  Arrays.asList(nResult != null ? new AnalyzerResult[]{nResult, vResult} : new AnalyzerResult[]{vResult});
//
//            return true;
//            
//        } catch (QueryNeverReturnAnyRows ex) {
//            Platform.runLater(() -> {
//                if (controller != null) {
//                    DialogHelper.showInfo(controller, "Info", "The query cannot return any row", null);
//                }
//            });
//            
//            return false;
//        }
//    }
//    
//    private void pushCardinalityColumn(Column c) {
//        if (c != null && c.getTable() != null) {
//            Set<Column> columns = cardinalityColumns.get(c.getTable().getName());
//            if (columns == null) {
//                columns = new HashSet<>();
//                cardinalityColumns.put(c.getTable().getName(), columns);
//            }
//            
//            columns.add(c);
//        }            
//    }
//    
//    
//    public List<ExplainResult> lookupExplainResults(String key) {
//        List<ExplainResult> result = new ArrayList<>();
//        if (key != null) {            
//            
//            Long id = null;
//            List<ExplainResult> list = null;
//            
//            if (key.startsWith("Query")) {
//                id = Long.valueOf(key.substring(5));
//                list = vQueryExplainResults;
//            } else if (key.startsWith("NQuery")) {
//                id = Long.valueOf(key.substring(6));
//                list = vNQueryExplainResults;
//            }
//            
//            if (id != null && list != null) {
//                for (ExplainResult er: list) {
//                    if (er.getId().equals(id)) {
//                        result.add(er);
//                    }
//                }
//            }
//        }
//        
//        return result;
//    }
//    
//    //select * from film where title=1 and description>2 and release_year<3 and length>2 and rating=1
//    private void checkRecomendations(List<AnalyzerResult> vResults, List<AnalyzerResult> nResults) throws SQLException {
//        
//        // nQery give more details that is why we using nResults
//        // ex: select * from ( select * from actor where last_update < now() ) a
//        // optimized: select `sakila`.`actor`.`actor_id` AS `actor_id`,`sakila`.`actor`.`first_name` AS `first_name`,`sakila`.`actor`.`last_name` AS `last_name`,`sakila`.`actor`.`last_update` AS `last_update` from `sakila`.`actor` where (`sakila`.`actor`.`last_update` < <cache>(now()))
//        // current algorithm didnt check subqueries, and eplain return simple 1 query - so we deside that more good using optimized query
//        
//        // size of vResult and Result - the same
//        if (nResults == null || nResults.isEmpty()) {
//            nResults = vResults;
//        }
//        
//        if (nResults != null && !nResults.isEmpty()) {
//            int index = 0;
//                        
//            for (AnalyzerResult r: nResults) {
//
//                final AnalyzerResult analyzerResult;
//                
//                // using for columns from GROUP BY and ORDER BY
//                // sizes for this columns should be ignored
//                List<Column> ignoredForSize = new ArrayList<>();
//                
//                String query = r.getQuery();
//                log.debug("analyze query: " + query);
//                if (query == null || query.isEmpty()) {
//                    analyzerResult = vResults.get(index);
//                } else if ("GOOD".equals(query))  {
//                    // no need check this query
//                    continue;
//                } else {
//                    analyzerResult = r;
//                }
//                
//                boolean hasGroupBy = analyzerResult.isDistinct() || (analyzerResult.getGroupBy() != null && !analyzerResult.getGroupBy().isEmpty());
//                boolean hasOrderBy = analyzerResult.getOrderBy() != null && !analyzerResult.getOrderBy().isEmpty();
//                boolean hasWhere = analyzerResult.getWhere() != null;
//                
//                // Task: "need to ignore group by or order column if any one of the column is in BLOB or TINYTEXT, TEXT, MEDIUMTEXT and LONGTEXT"
//                if (hasGroupBy) {
//                    if (analyzerResult.getGroupBy() != null) {
//                        for (AnalyzerResult.ColumnResult ac: analyzerResult.getGroupBy()) {
//                            
//                            if (!hasGroupBy) {
//                                break;
//                            }
//                            
//                            String columnName = ac.getColumnName();
//                            if (ac.getIsFunction()) {
//                                String[] functionColumns = getFunctionColumns(columnName);
//                                if (functionColumns.length > 0) {
//                                    for (String n: functionColumns) {
//                                        Column c = AnalyzerResult.findColumn(analyzerResult, n);
//                                        pushCardinalityColumn(c);
//                                        if (c != null && c.getDataType() != null && c.getDataType().isLargeString()) {
//                                            hasGroupBy = false;
//                                            break;
//                                        }
//                                    }
//                                }
//                            } else {
//                                Column c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                                pushCardinalityColumn(c);
//                                if (c != null && c.getDataType() != null && c.getDataType().isLargeString()) {
//                                    hasGroupBy = false;
//                                    break;
//                                }
//                            }
//                        }
//                    }
//                    
//                    if (!hasGroupBy) {
//                        analyzerResult.clearGroupBy();
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_29, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                }
//                
//                if (hasOrderBy) {
//                    
//                    boolean hasPkUniqueAutoInc = false;
//                    
//                    if (analyzerResult.getOrderBy() != null) {
//                        for (AnalyzerResult.ColumnResult ac: analyzerResult.getOrderBy()) {
//                            
//                            if (!hasOrderBy) {
//                                break;
//                            }
//                            
//                            String columnName = ac.getColumnName();
//                            if (ac.getIsFunction()) {
//                                String[] functionColumns = getFunctionColumns(columnName);
//                                if (functionColumns.length > 0) {
//                                    for (String n: functionColumns) {
//                                        Column c = AnalyzerResult.findColumn(analyzerResult, n);
//                                        pushCardinalityColumn(c);
//                                        if (c != null && c.getDataType() != null && c.getDataType().isLargeString()) {
//                                            hasOrderBy = false;
//                                            break;
//                                        }
//                                        
//                                        for (Index idx: ((DBTable)c.getTable()).getIndexes()) {
//                                            if (idx.getType() == IndexType.UNIQUE_TEXT) {
//                                                for (Column c0: idx.getColumns()) {
//                                                    if (c0.getName().equals(c.getName())) {
//                                                        hasPkUniqueAutoInc = true;
//                                                        break;
//                                                    }
//                                                }
//                                            }
//                                        }
//                                        
//                                        if (c.isPrimaryKey() || c.isAutoIncrement()) {
//                                            hasPkUniqueAutoInc = true;
//                                        }
//                                        
//                                        if (hasPkUniqueAutoInc) {
//                                            break;
//                                        }
//                                    }
//                                }
//                            } else {
//                                Column c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                                pushCardinalityColumn(c);
//                                if (c != null && c.getDataType() != null && c.getDataType().isLargeString()) {
//                                    hasOrderBy = false;
//                                    break;
//                                }
//                            }
//                        }
//                    }
//                    
//                    // If orderby clause has any table’s auto_incremnt column or Primary key column/s or UNIQUE key column 
//                    // The Query is using.
//                    if (hasPkUniqueAutoInc) {
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_30, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                    
//                    if (!hasOrderBy) {
//                        analyzerResult.clearOrderBy();
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_29, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                }
//                
//                LinkedHashSet<Column> columnForRecomendation = analyzerResult.getDegree() == null || analyzerResult.getDegree() == 1 ? new LinkedHashSet<>() : null;
//                LinkedHashSet<Column> columnFullText = new LinkedHashSet<>();
//                LinkedHashSet<Column> columnSpatial = new LinkedHashSet<>();
//                
//                // TODO Maybe need skiped if flag skiped is true or analyzerResult.getRecomendationsSize() = 0
//                
//                if (analyzerResult.getColumns() != null) {
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getColumns()) {
//                        if (cr.getColumnName().contains("*")) {
//                            analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_25, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                            break;
//                        }
//                    }
//                }
//                
//                
//                boolean whereOnlyOr = false;
//                
//                List<Column> filterColumns = new ArrayList<>();
//                
//                // JOIN checking
//                // if the query has table degree more than 1 - its join query
//                if (analyzerResult.getDegree() != null && analyzerResult.getDegree() > 1 && analyzerResult.getFrom() != null && !analyzerResult.isIgnoreJoin()) {
//                    analyzerJoinResult(analyzerResult, analyzerResult.getFrom(), columnSpatial);
//                    
//                } else {
//                    
//                    String tableName = "";
//                    if (analyzerResult.getFrom() != null && analyzerResult.getFrom().getTables() != null && !analyzerResult.getFrom().getTables().isEmpty()) {
//                        tableName = analyzerResult.getFrom().getTables().get(0).getTableName();
//                    }
//
//                    // If DISTINCT with GROUP BY - ignore
//                    if (analyzerResult.isDistinct() && analyzerResult.getGroupBy() != null && !analyzerResult.getGroupBy().isEmpty()) {
//                        continue;
//                    }
//                    
//                    if (hasWhere && hasGroupBy && hasOrderBy) {
//                        // 1) where + group by  + order by
//
//                        // if group by has any function then only where
//                        if (hasFunctions(analyzerResult.getGroupBy())) {
//                            // if group by has any function then only where 
//                            // WHERE rules
//                            whereOnlyOr = analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, false);
//
//                        } else if (hasFunctions(analyzerResult.getOrderBy())) {                        
//                            // if order by has function then where + group by  
//                            // WHERE + GROUP BY
//                            if (!checkGroupByWithWhere(analyzerResult, filterColumns, columnForRecomendation, ignoredForSize)) {
//                                analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, true);
//                            }
//
//                        } else if (checkGroupByAndOrderByColumns(analyzerResult)) {
//                            // if both are same column then jsut where + gourp by 
//                            if (!checkGroupByWithWhere(analyzerResult, filterColumns, columnForRecomendation, ignoredForSize)) {
//                                analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, true);
//                            }
//
//                        } else {
//                            if (!checkGroupByWithWhere(analyzerResult, filterColumns, columnForRecomendation, ignoredForSize)) {
//                                analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, true);
//                            }
//                            
//                            analyzerResult.addRecomendation(database.getName(), tableName, RECOMENDATION_13, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        }
//
//                    } else if (hasWhere && hasGroupBy) {
//                        // 2) where + group by
//                        if (!checkGroupByWithWhere(analyzerResult, filterColumns, columnForRecomendation, ignoredForSize)) {
//                            whereOnlyOr = analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, true);
//                        }
//
//                    } else if (hasWhere && hasOrderBy) {
//                        // 3) where + order by
//                        analyzerOrderByColumnsResult(analyzerResult, filterColumns, analyzerResult.getOrderBy(), analyzerResult.getGroupBy(), analyzerResult.getWhere(), columnForRecomendation, ignoredForSize);
//
//                        whereOnlyOr = analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, true);
//
//                    } else if (hasWhere) {
//                        // 4) where
//                        whereOnlyOr = analyzerConditionResult(analyzerResult, filterColumns, collectOrderByAndGroupByColumns(analyzerResult), analyzerResult.getWhere(), columnForRecomendation, columnFullText, columnSpatial, true, false);                                
//
//                    } else if (hasGroupBy && hasOrderBy) {
//                        // 5) group by + order by
//                        if (!checkGroupByAndOrderByColumns(analyzerResult)) {
//                            analyzerResult.addRecomendation(database.getName(), tableName, RECOMENDATION_13, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        }
//                    } else if (hasGroupBy) {
//                        // 6) group by
//                        analyzerGroupByWithWhereColumnsResult(analyzerResult, filterColumns, analyzerResult.getGroupBy(), analyzerResult.getColumns(), null, null, columnForRecomendation, ignoredForSize);
//
//                    } else if (hasOrderBy) {
//                        // 7) order by
//                        analyzerOrderByColumnsResult(analyzerResult, filterColumns, analyzerResult.getOrderBy(), analyzerResult.getGroupBy(), analyzerResult.getWhere(), columnForRecomendation, ignoredForSize);
//                    } else {
//                        // check MIN and MAX functions in select
//                        checkMinMaxColumns(analyzerResult);
//                    }
//                    
//                    // Check partitions RULES
//                    checkPartitions(analyzerResult, tableName, columnForRecomendation, null);                    
//                }              
//                
//                
//                // check for count(*)
//                Matcher m = PATTERN_COUNT.matcher(query);
//                if (m.find()) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_18, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//                //  IF any query missing WHERE / GROUP/LIMIT then we need to show the following message.
//                if (analyzerResult.getWhere() == null && analyzerResult.getGroupBy() == null && analyzerResult.getLimit() == null && analyzerResult.getLimit().trim().isEmpty()) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_20, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//                // IF select query has LIMIT and OFFSET in the query then we need to show the following recommendation 
//                if (analyzerResult.getLimit() != null && (analyzerResult.getLimit().toLowerCase().contains("offset") || analyzerResult.getLimit().contains(","))) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_21, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//                // If the query has group by  the columns after SELECT clause are belongs to group by clause columns. IT is only for column it may come grouping function after SELECT. 
//                if (analyzerResult.getGroupBy() != null && analyzerResult.getColumns() != null) {
//                    // check if exist columns in group by that not in select columns
//                    List<String> groupByColumns = new ArrayList<>();
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getGroupBy()) {
//                        if (!cr.getIsFunction()) {
//                            groupByColumns.add(cr.getColumnName());
//                        }
//                    }
//                    
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getColumns()) {
//                        // if select column not in group by - recommendation
//                        boolean found = false;
//                        for (String name: groupByColumns) {
//                            if (name.equalsIgnoreCase(cr.getColumnName())) {
//                                found = true;
//                                break;
//                            }
//                        }
//                        
//                        if (!found) {
//                            analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_26, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        }
//                    }
//                }
//                
//                // if any query  have LIMIT and without ORDER BY then we need to show the following message.
//                if (analyzerResult.getLimit() != null && !analyzerResult.getLimit().isEmpty() && analyzerResult.getOrderBy() == null) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_27, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//                if (columnForRecomendation != null && !columnForRecomendation.isEmpty()) {
//                    
//                    List<Column> forSecondIndxcolumns = new ArrayList<>();
//                    
//                    List<Column> columns = new ArrayList<>();
//                    List<Column> other = new ArrayList<>();
//                    List<Column> ranges = new ArrayList<>();
//                    
//                    DBTable table = null;
//                    for (Column c: columnForRecomendation) {
//                        if (c != null && c.getTable() != null) {
//                            table = (DBTable) c.getTable();
//                        }
//                    }
//                    
//                    // need to ignore PK/UQ/AI in group by clause and functions
//                    List<Column> forIgnore = new ArrayList<>();
//                    TablePkAndUniqueStructure ignoreStructure = TablePkAndUniqueStructure.create(table);
//                    for (Column c: columnForRecomendation) {
//                        if (c != null && c.getGroupBy() != null && c.getGroupBy()) {
//                            if (ignoreAsPk(c)) {
//                                ignoreStructure.removeColumn(c);
//                            }
//                            if (c.isAutoIncrement()) {
//                                forIgnore.add(c);
//                            }
//                        }
//                    }
//                    
//                    if (ignoreStructure.needIgnorePk()) {                        
//                        forIgnore.addAll(ignoreStructure.pkColumns());
//                    }
//                    
//                    if (ignoreStructure.needIgnoreUq()) {
//                        for (List<Column> list: ignoreStructure.uqColumns().values()) {
//                            forIgnore.addAll(list);
//                        }
//                    }
//                    
//                    if (!forIgnore.isEmpty()) {
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_31, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                    
//                    for (Column c: columnForRecomendation) {
//                        if (c != null && !forIgnore.contains(c)) {
//                            if (c.isConstraint()) {
//                                columns.add(c);
//                            } else if (ranges.isEmpty()) {
//                                ranges.add(c);
//                            } 
//
//                            if (c.getOrderBy() != null && c.getOrderBy() || c.getGroupBy() != null && c.getGroupBy()) {
//                                other.add(c);
//                            }                            
//                        }
//                    }
//                    
//                    int size = columns.size();
//                    if (size > indexMaxNoCount - 1 && columns.size() >= indexMaxNoCount - 1) {
//                        columns = columns.subList(0, indexMaxNoCount - 1);
//                    }
//                                        
//                    // we need choose high range or order/group by
//                    // need to get one hight cardinality column from
//                    // where condition rage filters then compare with 
//                    // GROUP BY [if only order by then consider ORDER by all column ] 
//                    // all columns then choose the right index. 
//                    
//                    if (ranges.isEmpty()) {
//                        for (Column c: other) {
//                            if (!columns.contains(c)) {
//                                columns.add(c);
//                            }
//                        }
//                    } else if (other.isEmpty()) {
//                        columns.addAll(ranges);
//                    } else {
//
//                        boolean allRangeInLow = true;
//                        Long cardinality0 = getColumnDegree(analyzerResult, ranges.get(0));//columnCardinalityService.getColumnCardinality(params.cacheKey(), database.getName(), table.getName(), ranges.get(0).getName());
//                        if (cardinality0 == null) {
//                            cardinality0 = 0l;
//                        }
//                        if (!LOWER_DEGREE_NAMES.contains(ranges.get(0).getName())) {
//                            allRangeInLow = false;
//                        }
//
//                        boolean allOrderByInLow = true;
//                        Long cardinality1 = 0l;
//                        for (Column c0: other) {
//                            Long cardinality = getColumnDegree(analyzerResult, c0);//columnCardinalityService.getColumnCardinality(params.cacheKey(), database.getName(), table.getName(), c0.getName());
//                            if (cardinality != null) {
//                                cardinality1 += cardinality;
//                            }
//
//                            if (!LOWER_DEGREE_NAMES.contains(c0.getName())) {
//                                allOrderByInLow = false;
//                            }
//                        }
//
//                        if (cardinality0 == cardinality1) {
//                            if (allRangeInLow && !allOrderByInLow) {
//                                for (Column c: other) {
//                                    if (!columns.contains(c)) {
//                                        columns.add(c);
//                                    }
//                                }
//                            } else if (!allRangeInLow && allOrderByInLow) {
//                                columns.add(ranges.get(0)); 
//                            } else {
//                                
//                                forSecondIndxcolumns.addAll(columns);
//                                forSecondIndxcolumns.add(ranges.get(0));
//                                
//                                for (Column c: other) {
//                                    if (!columns.contains(c)) {
//                                        columns.add(c);
//                                    }
//                                }
//                            }
//                        } else {                  
//
//                            if (cardinality0 > cardinality1) {
//                                columns.add(ranges.get(0)); 
//                            } else {
//                                for (Column c: other) {
//                                    if (!columns.contains(c)) {
//                                        columns.add(c);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    
////                    columns.addAll(ranges);
////                    columns.addAll(other);
//
//                    // table must be only 1
//                        
//                    if (whereOnlyOr) {
//
//                        boolean foundIndex = false;
//                        for (Index tableIndex: table.getIndexes()) {
//
//                            if (tableIndex.getColumns() != null) {
//
//                                if (whereOnlyOr) {
//                                    if (tableIndex.getColumns().size() == 1) {
//                                        Column c0 = tableIndex.getColumns().get(0);
//                                        for (Column c1: columns) {
//                                            if (c0.getName().equalsIgnoreCase(c1.getName())) {
//                                                columns.remove(c1);
//                                                break;
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//
//                        // If not found - need add recommendation
//                        if (!foundIndex) {
//                            for (Column c: columns) {
//                                String name = convertColumnName(c, ignoredForSize);
//
//                                String idxName = c.getName();
//
//                                analyzerResult.addRecomendation(database.getName(), table.getName(), String.format(RECOMENDATION_5, c.getName()), String.format("idx__%s", idxName), String.format(SQL_1, table.getName(), idxName, name), query, Arrays.asList(new Column[]{c}));
//                            }
//                        } else {
//                            for (Column c: columns) {
//                                String name = convertColumnName(c, ignoredForSize);
//                                String idxName = c.getName();
//
//                                analyzerResult.addRecomendation(true, database.getName(), table.getName(), String.format(RECOMENDATION_5, c.getName()), String.format("idx__%s", idxName), String.format(SQL_1, table.getName(), idxName, name), query, Arrays.asList(new Column[]{c}));
//                            }
//                        }
//                    } else {
//                        checkForIndexes(analyzerResult, table, columns, filterColumns);
//                    }
//                    
//                    if (!forSecondIndxcolumns.isEmpty()) {
//                        if (whereOnlyOr) {
//
//                            boolean foundIndex = false;
//                            for (Index tableIndex: table.getIndexes()) {
//
//                                if (tableIndex.getColumns() != null) {
//
//                                    if (whereOnlyOr) {
//                                        if (tableIndex.getColumns().size() == 1) {
//                                            Column c0 = tableIndex.getColumns().get(0);
//                                            for (Column c1: forSecondIndxcolumns) {
//                                                if (c0.getName().equalsIgnoreCase(c1.getName())) {
//                                                    forSecondIndxcolumns.remove(c1);
//                                                    break;
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//
//                            // If not found - need add recommendation
//                            if (!foundIndex) {
//                                for (Column c: forSecondIndxcolumns) {
//                                    String name = convertColumnName(c, ignoredForSize);
//
//                                    String idxName = c.getName();
//
//                                    analyzerResult.addRecomendation(database.getName(), table.getName(), String.format(RECOMENDATION_5, c.getName()), String.format("idx__%s", idxName), String.format(SQL_1, table.getName(), idxName, name), query, Arrays.asList(new Column[]{c}));
//                                }
//                            } else {
//                                for (Column c: forSecondIndxcolumns) {
//                                    String name = convertColumnName(c, ignoredForSize);
//                                    String idxName = c.getName();
//
//                                    analyzerResult.addRecomendation(true, database.getName(), table.getName(), String.format(RECOMENDATION_5, c.getName()), String.format("idx__%s", idxName), String.format(SQL_1, table.getName(), idxName, name), query, Arrays.asList(new Column[]{c}));
//                                }
//                            }
//                        } else {
//                            checkForIndexes(analyzerResult, table, forSecondIndxcolumns, filterColumns);
//                        }
//                    }
//                }
//                
//                if (!columnFullText.isEmpty()) {                    
//                    List<Column> columns = new ArrayList<>(columnFullText);
//                    
//                    // table must be only 1
//                    if (columns.get(0) != null) {                    
//                        DBTable table = (DBTable) columns.get(0).getTable();                        
//                        checkForIndexes(analyzerResult, table, columns, null, IndexType.FULL_TEXT, RECOMENDATION_5_FULLTEXT, RECOMENDATION_4_FULLTEXT, SQL_FULLTEXT);
//                    }
//                }
//                
//                if (!columnSpatial.isEmpty()) {                    
//                    List<Column> columns = new ArrayList<>(columnSpatial);
//                    
//                    // table must be only 1
//                    if (columns.get(0) != null) {                    
//                        DBTable table = (DBTable) columns.get(0).getTable();
//                        checkForIndexes(analyzerResult, table, columns, null, IndexType.SPATIAL, RECOMENDATION_5_SPATIAL, RECOMENDATION_4_SPATIAL, SQL_SPATIAL);
//                    }
//                }
//                
//                // check EXPLAIN
//                // Sudheer: Move to the end, ignore for table that have index
//                analyzerExplain(analyzerResult, columnForRecomendation);
//                
//                index++;
//                
//                checkRecomendations(r.getChildren(), new ArrayList<>());
//            }
//        }
//    }
//    
//    private List<Column> collectOrderByAndGroupByColumns(AnalyzerResult analyzerResult) {
//        List<Column> result = new ArrayList<>();
//        
//        if (analyzerResult.getGroupBy() != null) {
//            for (AnalyzerResult.ColumnResult ac: analyzerResult.getGroupBy()) {
//
//                String columnName = ac.getColumnName();
//                if (ac.getIsFunction()) {
//                    String[] functionColumns = getFunctionColumns(columnName);
//                    if (functionColumns.length > 0) {
//                        for (String n: functionColumns) {
//                            Column c = AnalyzerResult.findColumn(analyzerResult, n);
//                            if (c != null) {
//                                pushCardinalityColumn(c);
//                                result.add(c);
//                            }
//                        }
//                    }
//                } else {
//                    Column c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                    if (c != null) {
//                        c.setGroupBy(true);
//                        pushCardinalityColumn(c);
//                        result.add(c);
//                    }
//                }
//            }
//        }
//        
//        if (analyzerResult.getOrderBy() != null) {
//            for (AnalyzerResult.ColumnResult ac: analyzerResult.getOrderBy()) {
//                
//                Column c = null;
//                
//                String columnName = ac.getColumnName();
//                if (ac.getIsFunction()) {
//                    String[] functionColumns = getFunctionColumns(columnName);
//                    if (functionColumns.length > 0) {
//                        for (String n: functionColumns) {
//                            c = AnalyzerResult.findColumn(analyzerResult, n);
//                            if (c != null) {
//                                pushCardinalityColumn(c);
//                                result.add(c);
//                            }
//                        }
//                    }
//                } else {
//                    c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                    if (c != null) {
//                        pushCardinalityColumn(c);
//                        result.add(c);
//                    }
//                }
//                
//                if (c != null) {
//                    c.setOrderBy(true);
//                    c.setOrderByDesc(ac.getIsOrderDesc());
//                    c.setGroupByDesc(ac.getIsGroupDesc());
//                }
//            }
//        }
//        
//        return result;
//    }
//    
//    private void checkPartitions(AnalyzerResult analyzerResult, String tableName, LinkedHashSet columnForRecomendation, List<TableResult> tables) {
//        // RULE: If the partition type  in List / Key / hash  or Range and the ParitionColumnName 
//        // is not in filter condition (where clause ) then we need to show the recommendation
//        if (analyzerResult.isPartition(tableName)) {
//
//            List<Column> columns = new ArrayList<>();
//            collectFilterColumn(analyzerResult.getWhere(), analyzerResult, columns, true);
//
//            if (tables != null) {
//                for (AnalyzerResult.TableResult t: tables) {                
//                    if (t.getJoins() != null) {
//                        for (AnalyzerResult.JoinResult jr: t.getJoins()) {
//                            collectFilterColumn(jr.getOn(), analyzerResult, columns);
//                        }
//                    }
//                }
//            }
//            
//            List<Column> partitionColumnInWhere = new ArrayList<>();
//            if (!columns.isEmpty()) {
//                for (Column c: columns) {
//                    if (c.getTable() != null && c.getTable().getName().equalsIgnoreCase(tableName) && analyzerResult.isPartitionAny(tableName, c.getName())) {
//                        partitionColumnInWhere.add(c);
//                    }
//                }
//            }
//
//            if (partitionColumnInWhere.isEmpty()) {
//                for (String column: analyzerResult.getPartitionColumns(tableName)) {
//                    analyzerResult.addRecomendation(database.getName(), tableName, String.format(RECOMENDATION_14, column), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//            } else {                        
//
//                // If the partition in List or Hash or key and the query 
//                // where clause has partition column with constant (Ex col = 90) 
//                // condition then we need to keep partition column at first place
//                // at left had side when we recommend composite index (index one more than one column)
//                for (Column c: partitionColumnInWhere) {
//                    if (analyzerResult.isPartitionList(tableName, c.getName()) ||
//                            analyzerResult.isPartitionHash(tableName, c.getName()) ||
//                            analyzerResult.isPartitionKey(tableName, c.getName())) {
//
//                        if (columnForRecomendation != null && !columnForRecomendation.isEmpty()) {
//
//                            List<Column> cs = new ArrayList<>(columnForRecomendation);
//                            for (Column column: cs) {
//                                if (column.getName().equalsIgnoreCase(c.getName())) {
//                                    cs.remove(column);
//                                    cs.add(0, column);
//                                    pushCardinalityColumn(column);
//                                    break;
//                                }
//                            }
//
//                            columnForRecomendation.clear();
//                            columnForRecomendation.addAll(cs);
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    private void checkSpatial(LinkedHashSet<Column> columnForRecomendation, LinkedHashSet<Column> columnSpatial) {
//        
//        List<Column> cs = new ArrayList<>(columnForRecomendation);
//        for (Column c: cs) {
//            if (c != null && c.getDataType() != null && "GEOMETRY".equals(c.getDataType().getName().toUpperCase())) {
//                columnForRecomendation.remove(c);
//                columnSpatial.add(c);
//            }
//        }        
//    }
//    
//    private boolean checkGroupByAndOrderByColumns(AnalyzerResult analyzerResult) {
//        List<AnalyzerResult.ColumnResult> groupBy = analyzerResult.getGroupBy();
//        List<AnalyzerResult.ColumnResult> orderBy = analyzerResult.getOrderBy();
//        
//        List<Column> groupByColumns = new ArrayList<>();
//        for (AnalyzerResult.ColumnResult ac: groupBy) {
//
//            String columnName = ac.getColumnName();
//            if (checkForCalculation(columnName)) {
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_19, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                generatedAlways(analyzerResult, columnName, false);
//                continue;
//            }
//            //Case 3 ) We should not recommend any index if any column in order having functions
//            if (ac.getIsFunction() != null && ac.getIsFunction()) {
//                // if functions - just ignore this query
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                
//                generatedAlways(analyzerResult, ac.getSource(), true);
//                return true;
//            } else {
//                Column c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                if (!groupByColumns.contains(c)) {
//                    groupByColumns.add(c);
//                }
//            }
//        }
//        
//        int asc = 0;
//        int desc = 0;
//        List<Column> orderByByColumns = new ArrayList<>();
//        for (AnalyzerResult.ColumnResult ac: orderBy) {
//
//            if (ac.getIsOrderDesc()) {
//                desc++;
//            } else {
//                asc++;
//            }
//            
//            String columnName = ac.getColumnName();
//            if (checkForCalculation(columnName)) {
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_19, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                generatedAlways(analyzerResult, columnName, false);
//                continue;
//            }
//            
//            //Case 3 ) We should not recommend any index if any column in order having functions
//            if (ac.getIsFunction() != null && ac.getIsFunction()) {
//                // if functions - just ignore this query
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                generatedAlways(analyzerResult, ac.getSource(), true);
//                return false;
//            } else {
//                Column c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                if (!orderByByColumns.contains(c)) {
//                    orderByByColumns.add(c);
//                }
//            }
//        }
//
//        // Columns must be the same
//        if (groupByColumns.size() == orderByByColumns.size()) {                    
//
//            boolean theSame = true;
//            for (Column c0: groupByColumns) {                        
//
//                boolean notFound = true;
//                for (Column c1: orderByByColumns) {
//                    if (c0.getTable().getName().equals(c1.getTable().getName()) && c0.getName().equals(c1.getName())) {
//                        notFound = false;
//                        break;
//                    }
//                }
//
//                if (notFound) {
//                    theSame = false;
//                    break;
//                }
//            }
//            
//            if (!theSame || (asc > 0 && desc > 0)) {
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_24, null, null, analyzerResult.getQuery(), new ArrayList<>());
//            }
//            
//            return theSame;
//        } else {
//            analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_24, null, null, analyzerResult.getQuery(), new ArrayList<>());
//        }
//        
//        return false;
//    }
//    
//    private boolean hasFunctions(List<AnalyzerResult.ColumnResult> columns) {
//        boolean result = false;
//        if (columns != null) {
//            for (AnalyzerResult.ColumnResult cr: columns) {
//                if (cr.getIsFunction() != null && cr.getIsFunction()) {
//                    return true;
//                }
//            }
//        }
//        
//        return result;
//    }
//    
//    
//    private boolean checkGroupByWithWhere(AnalyzerResult analyzerResult, List<Column> filterColumns, Set<Column> columnsForRecomendation, List<Column> ignoredForSize) {
//            
//        // If the query has group by and where with more then one filter then 
//        // First columns always should be group by columns then follow where Condition rules 
//        // Also if the where clause have more then one row range conditions then we have to choose only one based on cardinality. 
//
//        List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//        checkNormalConditionResult(analyzerResult.getWhere(), analyzerResult, list);
//        
//        List<Column> ignoreFilterColumns = collectOrderByAndGroupByColumns(analyzerResult);
//
//        List<Column> contrains = new ArrayList<>();
//        List<Column> ranges = new ArrayList<>();
//        for (AnalyzerResult.ConditionResult cr0: list) {
//            if (!cr0.getIgnore()) {
//                String left = (String) cr0.getLeft();
//                String right = (String) cr0.getRight();
//                
//                boolean isLeft = checkForCalculation(left);
//                boolean isRight = checkForCalculation(right);
//                if (isLeft || isRight) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_19, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    generatedAlways(analyzerResult, isRight ? right : left, false);
//                    continue;
//                }
//
//                Column leftColumn = cr0.getIsLeftColumn() != null && cr0.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
//                Column rightColumn = cr0.getIsRightColumn() != null && cr0.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
//
//                pushCardinalityColumn(leftColumn);
//                pushCardinalityColumn(rightColumn);
//                
//                boolean leftIsFunction = cr0.getIsLeftFunction() != null ? cr0.getIsLeftFunction() : false;
//                boolean rightIsFunction = cr0.getIsRightFunction() != null ? cr0.getIsRightFunction() : false;
//
//                Column column = null;
//
//                boolean ignore = false;
//
//                // Find COLUMN if condition only wtih out column and without function, else - ignore
//                if (leftColumn != null && rightColumn == null && !rightIsFunction) {
//                    column = leftColumn;
//
//                } else if (rightColumn != null && leftColumn == null && !leftIsFunction) {
//                    column = rightColumn;
//
//                } else {
//                    ignore = true;
//                }
//                
//                if (rightIsFunction || leftIsFunction) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    generatedAlways(analyzerResult, rightIsFunction ? right : left, true);
//                }
//                
//                if (!ignore) {
//                    
//                    if (isConstraint(cr0)) {
//                        column.setConstraint(true);
//                        contrains.add(column);
//                        
//                        if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(cr0.getType()) && !ignoreFilterColumns.contains(column)) {
//                            filterColumns.add(column);
//                        }
//
//                    } else if (isRange(cr0) || isRangeSecondPriority(cr0) && (NULL_VALUE.equalsIgnoreCase(left) || NULL_VALUE.equalsIgnoreCase(right))) {
//                        ranges.add(column);
//                        column.setConstraint(false);
//                    }
//                }
//            }
//        }
//        
//        // If query degree 1 and where clause with only one range condition column and which is one of the column in the group by columns
//        // If query degree 1 and where clause contain one or more constraint filters come with “AND”  and shouldn't contain range filter 
//        if (ranges.size() > 1 || !ranges.isEmpty() && !contrains.isEmpty()) {
//            // Other cases we have to ignore group by and follow only where clause
//            return false;
//        } else {
//            return analyzerGroupByWithWhereColumnsResult(analyzerResult, filterColumns, analyzerResult.getGroupBy(), analyzerResult.getColumns(), contrains, ranges, columnsForRecomendation, ignoredForSize);
//        }        
//    }
//    
//    
//    private String convertColumnName(Column column, List<Column> ignoredForSize) {
//        
//        boolean desc = false;
//        if (column.getGroupBy() != null && column.getGroupBy()) {
//            desc = column.getGroupByDesc() != null && column.getGroupByDesc();
//        }
//        
//        if (column.getOrderBy() != null && column.getOrderBy()) {
//            desc = column.getOrderByDesc() != null && column.getOrderByDesc();
//        }
//        
//        if (ignoredForSize == null || !ignoredForSize.contains(column)) {
//            // if column String need check size, max must be 16
//            // From @Sudheer: index sub string need to apply only for where condition column. in any other case it should not work like order by / group by / min / max / distince cases 
//            if ((column.getOrderBy() == null || !column.getOrderBy()) && (column.getGroupBy() == null || !column.getGroupBy()) && isComumnTypeStringWithLegth(column.getDataType()) ) {
//                if (column.getDataType().getLength() == null || column.getDataType().getLength() > indexSubstrLength) {
//                    return "`" + column.getName() + "`(" + indexSubstrLength + ")" + (desc ? " desc" : "");
//                }
//            }
//        }
//        return "`" + column.getName() + "`" + (desc ? " desc" : "");
//    }
//    
//    private boolean analyzerExplain(AnalyzerResult analyzerResult, Set<Column> columnsForRecomendation) throws SQLException {
//        
//        Map<Long, List<ExplainResult>> map = new HashMap<>();
//
//        String queryTmp;
//        if (service.getMySQLVersionMaj() > 5) {
//            queryTmp = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), analyzerResult.getQuery());
//        } else {
//            queryTmp = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN_EXTENDED), analyzerResult.getQuery());
//        }
//        
//        try (ResultSet rs = (ResultSet) service.execute(queryTmp)) {
//            if (rs != null) {
//                while (rs.next()) {
//                    Long id = getLongValue(rs, "id");
//                    if (id > 0) {
//                        ExplainResult er = 
//                            new ExplainResult(
//                                id,  
//                                getStringValue(rs, "select_type"), 
//                                getStringValue(rs, "table"), 
//                                getStringValue(rs, "partitions"), 
//                                getStringValue(rs, "type"), 
//                                getStringValue(rs, "possible_keys"), 
//                                getStringValue(rs, "key"), 
//                                getLongValue(rs, "key_len"), 
//                                getStringValue(rs, "ref"), 
//                                getLongValue(rs, "rows"), 
//                                getLongValue(rs, "filtered"), 
//                                getStringValue(rs, "extra")
//                            );
//                        
////                        if (analyzerResult.getDegree() > 1) {
//                            if (er.getExtra() != null && (er.getExtra().toLowerCase().contains("temporary") || er.getExtra().toLowerCase().contains("filesort"))) {
//                                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_23, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                            }
////                        }
//                        
//                        if ("ALL".equalsIgnoreCase(er.getType()) && er.getRef() == null) {
//                            
//                            List<ExplainResult> results = map.get(er.getId());
//                            if (results == null) {
//                                results = new ArrayList<>();
//                                map.put(er.getId(), results);
//                            }
//                            
//                            results.add(er);
//                        }
//                        
//                        analyzerResult.putCachedRows(er.getTable(), er.getRows());
//                    }
//                }
//            }
//        } catch (SQLException ex) {
//            if (vQueryExplainResults != null) {
//                
//                List<String> tables = new ArrayList<>();
//                
//                List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//                checkNormalConditionResult(analyzerResult.getWhere(), analyzerResult, list);
//                if (analyzerResult.getFrom() != null) {
//                    
//                    for (AnalyzerResult.TableResult t: analyzerResult.getFrom().getTables()) {
//                        tables.add(t.getAliasName() != null && !t.getAliasName().isEmpty() ? t.getAliasName().toLowerCase() : t.getTableName().toLowerCase());
//
//                        if (t.getJoins() != null) {
//                            for (AnalyzerResult.JoinResult jr: t.getJoins()) {
//                                checkNormalConditionResult(jr.getOn(), analyzerResult, list);
//                            }
//                        }
//                    }
//                }
//                
//                for (AnalyzerResult.ConditionResult cr: list) {
//                    String leftTableName = cr.getIsLeftColumn() != null && cr.getIsLeftColumn() ? AnalyzerResult.findColumnTableName(analyzerResult, (String)cr.getLeft()) : null;
//                    String rightTableName = cr.getIsRightColumn() != null && cr.getIsRightColumn() ? AnalyzerResult.findColumnTableName(analyzerResult, (String)cr.getRight()) : null;
//                    
//                    if (leftTableName != null) {
//                        if (!tables.contains(leftTableName.toLowerCase())) {
//                            tables.add(leftTableName.toLowerCase());
//                        }
//                    }
//                    
//                    if (rightTableName != null) {
//                        if (!tables.contains(rightTableName.toLowerCase())) {
//                            tables.add(rightTableName.toLowerCase());
//                        }
//                    }
//                }
//                
//                for (String t: tables) {
//                    for (ExplainResult er: vQueryExplainResults) {
//                        if (t.equalsIgnoreCase(er.getTable())) {
//                            if ("ALL".equalsIgnoreCase(er.getType()) && er.getRef() == null) {
//                            
//                                List<ExplainResult> results = map.get(er.getId());
//                                if (results == null) {
//                                    results = new ArrayList<>();
//                                    map.put(er.getId(), results);
//                                }
//
//                                results.add(er);
//                            }
//
//                            analyzerResult.putCachedRows(er.getTable(), er.getRows());
//                            break;
//                        }
//                    }
//                }
//            }
//        }
//        
//        // first need collect filters from where clause
//        List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//        checkNormalConditionResult(analyzerResult.getWhere(), analyzerResult, list);
//        
//        // DISABLED by task from Sudheer
//        // Rules 2)  Need to verify Explain output Type should not come “ALL” more than 1 and ref column is not null more than one. 
//        // In this validation need to check each table’s join column having index or not. 
//        // If no index then recommend index on those columns. 
//
////        for (List<ExplainResult> results: map.values()) {
////            if (results.size() > 1) {
////                
////                for (ExplainResult er: results) {
////                    Map<DBTable, List<Column>> constraintColumns = new HashMap<>();
////                    Map<DBTable, List<Column>> rangeColumns = new HashMap<>();
////
////                    List<DBTable> resultTables = AnalyzerResult.findTables(analyzerResult, er.getTable());
////                    DBTable resultTable = resultTables != null && !resultTables.isEmpty() ? resultTables.get(0) : null;
////                    if (resultTable != null) {
////                        // find columns
////                        for (AnalyzerResult.ConditionResult cr: list) {
////                            // we mus check only filter joins
////                            if (AnalyzerResult.CONDITION_TYPE__JOIN.equals(cr.getType())) {
////                                String left = (String) cr.getLeft();
////                                String right = (String) cr.getRight();
////
////                                Column leftColumn = AnalyzerResult.findColumn(analyzerResult, left);
////                                Column rightColumn = AnalyzerResult.findColumn(analyzerResult, right);
////
////                                DBTable leftTable = (DBTable)leftColumn.getTable();
////                                DBTable rightTable =(DBTable)rightColumn.getTable();
////
////                                DBTable table = null;
////                                boolean isLeft = true;
////                                if (leftTable.getName().equalsIgnoreCase(resultTable.getName())) {
////                                    table = leftTable;
////                                    isLeft = true;
////                                } else if (rightTable.getName().equalsIgnoreCase(resultTable.getName())) {
////                                    table = rightTable;
////                                    isLeft = false;
////                                }
////
////                                if (table != null) {
////
////                                    List<Column> ranges = rangeColumns.get(table);
////                                    if (ranges == null) {
////                                        ranges = new ArrayList<>();
////                                        rangeColumns.put(table, ranges);
////                                    }
////                                    List<Column> constraints = constraintColumns.get(table);
////                                    if (constraints == null) {
////                                        constraints = new ArrayList<>();
////                                        constraintColumns.put(table, constraints);
////                                    }
////
////                                    if (isConstraint(cr)) {                                    
////                                        constraints.add(isLeft ? leftColumn : rightColumn);
////                                    } else {                                    
////                                        ranges.add(isLeft ? leftColumn : rightColumn);
////                                    }
////                                }
////                            }
////                        }
////
////                        for (DBTable t: constraintColumns.keySet()) {
////                            
////                            // if we already have index for this table - ignore then
////                            if (analyzerResult.hasIndexForTable(t.getName())) {
////                                continue;
////                            }
////                            
////                            List<Column> conditions = constraintColumns.get(t);
////                            List<Column> ranges = rangeColumns.get(t);
////
////                            conditions.sort((Column o1, Column o2) -> {
////                                // need find column degree
////                                // if column hase index get degree from show index
////                                // if not need call select methods
////                                return compareCardinality(analyzerResult, o1, o2);
////                            });
////                            
////                            int size = conditions.size();
////                            if (size > indexMaxNoCount - 1) {
////                                conditions = conditions.subList(0, indexMaxNoCount - 1);
////                            }
////
////                            ranges.sort((Column o1, Column o2) -> {
////                                // need find column degree
////                                // if column hase index get degree from show index
////                                // if not need call select methods
////                                return compareCardinality(analyzerResult, o1, o2);
////                            });
////
////                            conditions.addAll(ranges);
////                            if (!conditions.isEmpty()) {
////                                
////                                // if columnsForRecomendation NOT NULL - need just collect all columns
////                                // and show one composit recomendation
////                                if (columnsForRecomendation != null) {
////                                    columnsForRecomendation.addAll(conditions);
////
////                                } else {                           
////                                    checkForIndexes(analyzerResult, (DBTable)conditions.get(0).getTable(), conditions, null);                                    
////                                }
////                            }
////                        }
////                    }
////                }
////            }
////        }
//        
//        return false;
//    }
//        
//    private static String getStringValue(ResultSet rs, String column) {
//        String result = null;
//        try {
//            result = rs.getString(column);
//        } catch (SQLException ex) {}
//        
//        return result;
//    }
//    
//    private static Long getLongValue(ResultSet rs, String column) {
//        Long result = null;
//        try {
//            result = rs.getLong(column);
//        } catch (SQLException ex) {}
//        
//        return result;
//    }
//    
//    private void checkMinMaxColumns(AnalyzerResult analyzerResult) {
//        
//        if (analyzerResult.getColumns() != null) {
//            Map<TableProvider, List<Column>> columnsMap = new HashMap<>();
//            for (AnalyzerResult.ColumnResult cr: analyzerResult.getColumns()) {
//                String columnName = cr.getColumnName();
//                if (cr.getIsFunction()) {
//                    String[] functionColumns = getFunctionColumns(columnName);
//                    if (functionColumns.length == 1 && 
//                        (columnName.toLowerCase().trim().matches("min\\s*\\(.*\\)") || columnName.toLowerCase().trim().matches("max\\s*\\(.*\\)"))) {
//                        Column c = AnalyzerResult.findColumn(analyzerResult, functionColumns[0].trim());
//                        if (c != null) {
//                            pushCardinalityColumn(c);
//                            List<Column> columns = columnsMap.get(c.getTable());
//                            if (columns == null) {
//                                columns = new ArrayList<>();
//                                columnsMap.put(c.getTable(), columns);
//                            }
//
//                            columns.add(c);
//                        }
//                    }
//                }
//            }
//            
//            for (TableProvider tp: columnsMap.keySet()) {
//                checkForIndexes(analyzerResult, (DBTable)tp, columnsMap.get(tp), null);
//            }
//        }
//    }
//    
//    
//    // select * from t1 as a, test as t, tetete as te where a.c1 = t.id and a.c2 = te.id
//    private boolean analyzerJoinResult(AnalyzerResult analyzerResult, AnalyzerResult.FromResult from, LinkedHashSet<Column> columnSpatial) {
//        List<AnalyzerResult.TableResult> tables = from.getTables();
//        
//        // first need collect filters from where clause
//        List<AnalyzerResult.ConditionResult> whereConditions = new ArrayList<>();
//        List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//        boolean noOrOperand = checkNormalConditionResult(analyzerResult.getWhere(), analyzerResult, whereConditions);
//        
//        Map<Column, List<AnalyzerResult.ConditionResult>> conditionCache = new HashMap<>();
//        
//        list.addAll(whereConditions);
//        
//        // if no OR operands we can check that all join tables have their join conditions
//        if (noOrOperand || !whereConditions.isEmpty()) {     
//            String tablesStr = "";            
//            Map<AnalyzerResult.TableResult, Boolean> joinTables = new HashMap<>();
//            for (AnalyzerResult.TableResult t: tables) {
//                collectJoinTables(t, joinTables);
//                
//                if (t.getJoins() != null) {
//                    for (AnalyzerResult.JoinResult jr: t.getJoins()) {
//                        checkNormalConditionResult(jr.getOn(), analyzerResult, list);
//                    }
//                }
//            }
//            
//            for (AnalyzerResult.TableResult t: joinTables.keySet()) {
//                if (!tablesStr.isEmpty()) {
//                    tablesStr += ", ";
//                }
//                
//                tablesStr += t.getTableName();
//            }
//            
//            // Rules 1)  if the query has table degree = n then it must more than n-1 join conditions. 
//            // if any join missed then we need to spcity join condition missing. 
//            if (analyzerResult.getDegree() > list.size() + 1) {
//                analyzerResult.addRecomendation(database.getName(), null, String.format(RECOMENDATION_9, analyzerResult.getDegree(), analyzerResult.getDegree() - 1), null, null, analyzerResult.getQuery(), new ArrayList<>());
//            }
//            
//            
//            String firstTable = null;
//            String othersTables = "";
//            int countRows = 0;
//            for (AnalyzerResult.TableResult t: joinTables.keySet()) {
//                if (firstTable == null) {
//                    firstTable = t.getTableName();
//                } else {
//                    othersTables += (othersTables.isEmpty() ? "" : ", ") + "'" + t.getTableName() + "'";
//                }
//                Long rows = analyzerResult.getCachedRows(t.getTableName());
//                if (rows != null && rows > 1) {
//                    countRows++;
//                }
//            }
//            
//            if (countRows > 1) {
//                
//                boolean ignore = false;
//                try (ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_FOREIGN_KEYS), database, firstTable, othersTables, othersTables, firstTable))) {
//                    if (rs != null && rs.next()) {
//                        ignore = true;
//                    }
//                } catch (SQLException ex) {
//                    log.error(ex.getMessage(), ex);
//                }
//                
//                if (!ignore) {
//                    analyzerResult.addRecomendation(database.getName(), tablesStr, String.format(RECOMENDATION_10, tablesStr), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//            }
//            
//            Map<DBTable, List<Column>> tableContraintColumns = new HashMap<>();
//            Map<DBTable, List<Column>> tableRangeColumns = new HashMap<>();
//            Set<DBTable> tablesSet = new LinkedHashSet<>();
//            
//            Map<DBTable, List<Column>> moreFilterColumns = new HashMap<>();
//            Map<DBTable, List<Column>> filterColumns = new HashMap<>();
//            
////            Set<DBTable> transactionTablesSet = new LinkedHashSet<>();
////            Set<DBTable> removedTransactionTablesSet = new HashSet<>();
//            
//            Map<DBTable, List<Column>> joinColumns = new HashMap<>();
//            
//            for (AnalyzerResult.ConditionResult cr: list) {
//                
//                String left = (String) cr.getLeft();
//                String right = (String) cr.getRight();
//
//                Column leftColumn = AnalyzerResult.findColumn(analyzerResult, left);
//                Column rightColumn = AnalyzerResult.findColumn(analyzerResult, right);
//                
//                if (whereConditions.contains(cr)) {
//                    if (leftColumn != null) {
//                        List<AnalyzerResult.ConditionResult> l = conditionCache.get(leftColumn);
//                        if (l == null) {
//                            l = new ArrayList<>();
//                            conditionCache.put(leftColumn, l);
//                        }
//
//                        if (!l.contains(cr)) {
//                            l.add(cr);
//                        }
//                        
//                        pushCardinalityColumn(leftColumn);
//                    }
//
//                    if (rightColumn != null) {
//                        List<AnalyzerResult.ConditionResult> l = conditionCache.get(rightColumn);
//                        if (l == null) {
//                            l = new ArrayList<>();
//                            conditionCache.put(rightColumn, l);
//                        }
//
//                        if (!l.contains(cr)) {
//                            l.add(cr);
//                        }
//                        
//                        pushCardinalityColumn(rightColumn);
//                    }
//                }
//                
//                DBTable leftTable = leftColumn != null ? (DBTable)leftColumn.getTable() : null;
//                DBTable rightTable = rightColumn != null ? (DBTable)rightColumn.getTable() : null;
//                
//                boolean foundLeft = false;
//                boolean foundRight = false;
//                
//                for (AnalyzerResult.TableResult tr: joinTables.keySet()) {
//                    
//                    if (leftTable != null && tr.getTableName().equalsIgnoreCase(leftTable.getName())) {
//                        foundLeft = true;
//                    }
//                    
//                    if (rightTable != null && tr.getTableName().equalsIgnoreCase(rightTable.getName())) {
//                        foundRight = true;
//                    }
//                }
//                    
//                if (leftTable != null && foundLeft) {
//                    tablesSet.add(leftTable);
//                }
//                
//                if (rightTable != null && foundRight) {
//                    tablesSet.add(rightTable);
//                }
//                
//                boolean leftFunction = cr.getIsLeftFunction() != null ? cr.getIsLeftFunction() : false;
//                boolean rightFunction = cr.getIsRightFunction() != null ? cr.getIsRightFunction() : false;
//                
//                // NR: if join condition datatypes are other then integer(tinyint, smallint, int, mediumint , bigint) 
//                // then we need to show the following recommendation                     
//                if (leftColumn != null && rightColumn != null && (!leftColumn.getDataType().isInteger() || !rightColumn.getDataType().isInteger())) {
//                    String tableName = leftTable != null ? leftTable.getName() : "";
//                    analyzerResult.addRecomendation(database.getName(), tableName, String.format(RECOMENDATION_28, String.valueOf(cr.getLeft()) + ' ' + cr.getOperand() + ' ' + String.valueOf(cr.getRight())), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//
//
//                if (!whereConditions.contains(cr)) {
//                    
//                    // Rules 3) Need to check if any column having function on join column 
//                    // if one of columns is functions - need add recomendation to avoid functions                      
//                    if (leftFunction) {                 
//                        String tableName = leftTable != null ? leftTable.getName() : "";
//                        analyzerResult.addRecomendation(database.getName(), tableName, String.format(RECOMENDATION_7, cr.getLeft()), null, null, analyzerResult.getQuery(), new ArrayList<>());                    
//                    } 
//
//                    if (rightFunction) {                 
//                        String tableName = rightTable != null ? rightTable.getName() : "";
//                        analyzerResult.addRecomendation(database.getName(), tableName, String.format(RECOMENDATION_7, cr.getRight()), null, null, analyzerResult.getQuery(), new ArrayList<>());                    
//                    }
//
//                    if (leftFunction || rightFunction) {
//                        return false;
//                    }
//                } else {
//                    if (leftFunction || rightFunction) {
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, rightFunction ? right : left, true);
//                    }
//                }
//                
//                if (AnalyzerResult.CONDITION_TYPE__JOIN.equals(cr.getType()) || AnalyzerResult.CONDITION_TYPE__FILTER.equals(cr.getType())) {
//
//                    if (leftColumn != null && rightColumn != null) {
//                        // left and right columns must be in different tables
//                        if (!Objects.equals(leftTable, rightTable)) {
//                            for (AnalyzerResult.TableResult t: joinTables.keySet()) {
//                                if (leftTable != null && leftTable.getName().equalsIgnoreCase(t.getTableName())) {
//                                    joinTables.put(t, true);
//                                }
//
//                                if (rightTable != null && rightTable.getName().equalsIgnoreCase(t.getTableName())) {
//                                    joinTables.put(t, true);
//                                }
//                            }
//                        }
//                    }
//                }
//                
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equals(cr.getType())) {
//                    
//                    Column c = leftColumn != null ? leftColumn : (rightColumn != null ? rightColumn : null);
//                    if (c != null) {
//                        List<Column> columns = filterColumns.get((DBTable)c.getTable());
//                        if (columns == null) {
//                            columns = new ArrayList<>();
//                            filterColumns.put((DBTable)c.getTable(), columns);
//                        }
//                        
//                        columns.add(c);
//                    }
//                }
//                
//                if (AnalyzerResult.CONDITION_TYPE__JOIN.equals(cr.getType())) {
//                    if (leftColumn != null) {
//                        List<Column> l0 = joinColumns.get(leftColumn.getTable());
//                        if (l0 == null) {
//                            l0 = new ArrayList<>();
//                            joinColumns.put((DBTable)leftColumn.getTable(), l0);
//                        }
//                        l0.add(leftColumn);
////                        if (!checkJoinColumn(analyzerResult, leftColumn, leftColumn.getTable().getName(), true)) {
////                            if (!removedTransactionTablesSet.contains((DBTable) leftColumn.getTable())) {
////                                transactionTablesSet.add((DBTable) leftColumn.getTable());
////                            }
////                        } else {
////                            transactionTablesSet.remove((DBTable) leftColumn.getTable());
////                            removedTransactionTablesSet.add((DBTable) leftColumn.getTable());
////                        }
//                    } 
//                    
//                    if (rightColumn != null) {
//                        List<Column> l0 = joinColumns.get(rightColumn.getTable());
//                        if (l0 == null) {
//                            l0 = new ArrayList<>();
//                            joinColumns.put((DBTable)rightColumn.getTable(), l0);
//                        }
//                        l0.add(rightColumn);
////                        if (!checkJoinColumn(analyzerResult, rightColumn, rightColumn.getTable().getName(), true)) {
////                            if (!removedTransactionTablesSet.contains((DBTable) rightColumn.getTable())) {
////                                transactionTablesSet.add((DBTable) rightColumn.getTable());
////                            }
////                        } else {
////                            transactionTablesSet.remove((DBTable) rightColumn.getTable());
////                            removedTransactionTablesSet.add((DBTable) rightColumn.getTable());
////                        }
//                    }
//                    
//                    
//                    if (leftColumn != null) {
//                        // for left
//                        List<Column> tableColumns = moreFilterColumns.get((DBTable) leftColumn.getTable());
//                        if (tableColumns == null) {
//                            tableColumns = new ArrayList<>();
//                            moreFilterColumns.put((DBTable) leftColumn.getTable(), tableColumns);
//                        }
//                        
//                        //if (!checkJoinColumn(null, leftColumn, leftColumn.getTable().getName(), true) && !tableColumns.contains(leftColumn)) {
//                        if (!tableColumns.contains(leftColumn)) {
//                            tableColumns.add(leftColumn);
//                        }
//                        //}
//                    }
//                
//                    if(rightColumn != null) {
//                        // for right
//                        List<Column> tableColumns = moreFilterColumns.get((DBTable) rightColumn.getTable());
//                        if (tableColumns == null) {
//                            tableColumns = new ArrayList<>();
//                            moreFilterColumns.put((DBTable) rightColumn.getTable(), tableColumns);
//                        }
//                        
//                        //if (!checkJoinColumn(null, rightColumn, rightColumn.getTable().getName(), true) && !tableColumns.contains(rightColumn)) {
//                        if (!tableColumns.contains(rightColumn)) {    
//                            tableColumns.add(rightColumn);
//                        }
//                        //}
//                    }
//                }
//                
//                // for JOIN type if columns not pk, unique, without index - then need index for high cardinality column
//                if (leftColumn != null && leftTable != null && rightColumn != null && rightTable != null) {
//                    
//                    // From Sudheer: 26.11.18 19:24 pleae disable it
//                    // It is giving duplicate indexes
////                    Long leftCount = 0l;                    
////                    Boolean leftCheck = checkJoinColumn(analyzerResult, leftColumn, leftTable.getName(), false);
////                    if (leftCheck != null && !leftCheck && !hasIndex(leftColumn)) {
////                        try (ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__TABLE_ROW_COUNT), database, leftTable.getName()))) {
////                            if (rs != null && rs.next()) {
////                                leftCount = rs.getLong(1);
////                            }
////                        } catch (SQLException ex) {
////                            log.error("Error in join maker", ex);
////                        }
////                    }
////                    
////                    Long rightCount = 0l;
////                    if (leftCheck == null || !leftCheck) {                      
////                        Boolean rightCheck = checkJoinColumn(analyzerResult, rightColumn, rightTable.getName(), false);
////                        if (rightCheck != null && !rightCheck && !hasIndex(rightColumn)) {
////
////                            try (ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__TABLE_ROW_COUNT), database, rightTable.getName()))) {
////                                if (rs != null && rs.next()) {
////                                    rightCount = rs.getLong(1);
////                                }
////                            } catch (SQLException ex) {
////                                log.error("Error in join maker", ex);
////                            }
////                        }
////                        
////                        if (rightCheck != null && rightCheck) {
////                            leftCount = 0l;
////                        }
////                    }
////                    
////                    if (leftCount > 0 || rightCount > 0) {
////                        DBTable table = rightTable;
////                        Column column = rightColumn;
////                        if (leftCount >= rightCount) {
////                            table = leftTable;
////                            column = leftColumn;
////                        }
////                        
////                        String idxName = column.getName();
////
////                        analyzerResult.addRecomendation(database.getName(), table.getName(), String.format(RECOMENDATION_3, cr.getSource()), String.format("idx__%s", idxName), String.format(SQL_1, table.getName(), idxName, convertColumnName(column, null)), query, new ArrayList<>());
////                    }
//                    
//                } else {
//                    Column column = leftColumn;
//                    DBTable table = leftTable;
//                    
//                    if (rightColumn != null && rightTable != null) {
//                        column = rightColumn;
//                        table = rightTable;
//                    }
//                    
//                    if (column != null && table != null) {
//                        
//                        if (isConstraint(cr)) {
//                            column.setConstraint(true);
//                            List<Column> l = tableContraintColumns.get(table);
//                            if (l == null) {
//                                l = new ArrayList<>();
//                                tableContraintColumns.put(table, l);
//                            }
//
//                            if (!l.contains(column)) {
//                                l.add(column);
//                            }
//                            
//                        } else if (isRange(cr) || isRangeSecondPriority(cr)) {
//                            column.setConstraint(false);
//                            List<Column> l = tableRangeColumns.get(table);
//                            if (l == null) {
//                                l = new ArrayList<>();
//                                tableRangeColumns.put(table, l);
//                            }
//
//                            if (!l.contains(column)) {
//                                l.add(column);
//                            }
//                        }
//                    
//                        Boolean check = checkJoinColumn(analyzerResult, column, table.getName(), false);
//                        if (check != null && !check) {
//                            
//                            checkForIndexes(analyzerResult, table, Arrays.asList(new Column[]{column}), filterColumns.get(table));
////                            String name = convertColumnName(column, null);
////                            analyzerResult.addRecomendation(database.getName(), table.getName(), String.format(RECOMENDATION_5, column.getName()), String.format("idx__%s", column.getName()), String.format(SQL_1, table.getName(), column.getName(), name), query, Arrays.asList(new Column[]{column}));
//                        }
//                    }
//                }
//            }
//            
//            List<DBTable> transactionTables = new ArrayList<>();
//            
//            // find Transaction tables
//            for (DBTable t: joinColumns.keySet()) {
//                
//                TablePkAndUniqueStructure ignoreStructure = TablePkAndUniqueStructure.create(t);
//                
//                List<Column> jc = joinColumns.get(t);
//                for (Column c: jc) {
//                    if (ignoreAsPk(c)) {
//                        ignoreStructure.removeColumn(c);
//                    }
//                }
//                
//                if (!ignoreStructure.needIgnore()) {
//                    transactionTables.add(t);
//                }
//            }
//            
//            
//            List<String> tableNames = new ArrayList<>();
//            // if all ok - all tables in map must have value - true
//            for (AnalyzerResult.TableResult t: joinTables.keySet()) {
//                if (!joinTables.get(t) && !tableNames.contains(t.getTableName())) {
//                    tableNames.add(t.getTableName());
////                    analyzerResult.addRecomendation(database, t.getTableName(), String.format(RECOMENDATION_8, t.getTableName()), null, null, analyzerResult.getQuery());
//                }
//            }
//            
//            
//            boolean founded = false;
//            String generatedCondition = "";
//            if (tableNames.size() > 1) {
//                for (String name0: tableNames) {
//
//                    String others = "";
//                    for (String name1: tableNames) {
//                        if (!name1.equals(name0)) {
//                            if (!others.isEmpty()) {
//                                others += ", ";
//                            }
//                            others += "'" + name1 + "'";
//                        }
//                    }
//
//                    Map<String, Map<String, Object>> fks = new LinkedHashMap<>();
//
//                    try (ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_FOREIGN_KEYS), database, name0, others, others, name0))) {
//                        if (rs != null) {
//                            while (rs.next()) {
//                                String fkName = rs.getString("CONSTRAINT_NAME");
//                                Map<String, Object> m = fks.get(fkName);
//                                if (m == null) {
//                                    m = new HashMap<>();
//                                    fks.put(fkName, m);
//
//                                    m.put("table", rs.getString("TABLE_NAME"));
//                                    m.put("referencedTable", rs.getString("REFERENCED_TABLE_NAME"));
//                                    m.put("columns", new ArrayList<>());
//                                    m.put("referencedColumns", new ArrayList<>());
//                                }
//
//                                ((List)m.get("columns")).add(rs.getString("COLUMN_NAME"));
//                                ((List)m.get("referencedColumns")).add(rs.getString("REFERENCED_COLUMN_NAME"));
//                            }
//                        }
//                    } catch (SQLException ex) {
//                        log.error("Error in join maker", ex);
//                    }
//
//                    if (!fks.isEmpty()) {
//                        for (Map<String, Object> m: fks.values()) {
//
//                            String tableName = (String)m.get("table");
//                            String refTableName = (String)m.get("referencedTable");
//                            List<String> columns = (List<String>) m.get("columns");
//                            List<String> referencedColumns = (List<String>) m.get("referencedColumns");
//
//                            String condition = "";
//                            for (int i = 0; i < columns.size(); i++) {
//                                if (!condition.isEmpty()) {
//                                    condition += " AND ";
//                                }
//                                condition += "`" + tableName + "`.`" + columns.get(i) + "` = `" + refTableName + "`.`" + referencedColumns.get(i) + "`";
//                                
//                                Column c0 = AnalyzerResult.findColumn(analyzerResult, "`" + tableName + "`.`" + columns.get(i) + "`");
//                                Column c1 = AnalyzerResult.findColumn(analyzerResult, "`" + refTableName + "`.`" + referencedColumns.get(i) + "`");
//                                
//                                if (c0 != null) {
//                                    tablesSet.add((DBTable) c0.getTable());
//                                }
//                                if (c1 != null) {
//                                    tablesSet.add((DBTable) c1.getTable());
//                                }
//                            }
//
//                            if (!generatedCondition.contains(condition)) {
//                                generatedCondition += (generatedCondition.isEmpty() ? " " : " and ") + condition;
//                            }
//
//                            analyzerResult.addRecomendation(database.getName(), tableName, String.format(RECOMENDATION_8, condition, tableName), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        }
//
//                        founded = true;
//                    }
//                }
//            }
//            
//            if (!founded && !tableNames.isEmpty()) {
//                String str = "";
//                for (String name: tableNames) {
//                    if (!str.isEmpty()) {
//                        str += ", ";
//                    }
//                    str += "'" + name + "'";
//                }
//                
//                try (ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_PRIMARY_KEYS), database, str, str))) {
//                    if (rs != null) {
//                        while (rs.next()) {
//                        
//                            String tableName = rs.getString("TABLE_NAME");
//                            String referencedTableName = rs.getString("REFERENCED_TABLE_NAME");
//                            String columnName = rs.getString("COLUMN_NAME");
//                            String referencedColumnName = rs.getString("REFERENCED_COLUMN_NAME");
//                            
//                            String condition = "`" + tableName + "`.`" + columnName + "` = `" + referencedTableName + "`.`" + referencedColumnName + "`";
//                            if (!generatedCondition.contains(condition)) {
//                                generatedCondition += (generatedCondition.isEmpty() ? " " : " AND ") + condition;
//                                
//                                Column c0 = AnalyzerResult.findColumn(analyzerResult, "`" + tableName + "`.`" + columnName + "`");
//                                Column c1 = AnalyzerResult.findColumn(analyzerResult, "`" + referencedTableName + "`.`" + referencedColumnName + "`");
//                                
//                                if (c0 != null) {
//                                    tablesSet.add((DBTable) c0.getTable());
//                                }
//                                if (c1 != null) {
//                                    tablesSet.add((DBTable) c1.getTable());
//                                }
//                            }
//                            
//                            analyzerResult.addRecomendation(database.getName(), tableName, String.format(RECOMENDATION_8, condition, tableName), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        }
//                    }
//                } catch (SQLException ex) {
//                    log.error("Error in join maker", ex);
//                }
//            }
//                   
//            if (!generatedCondition.isEmpty()) {
//                int start = optimizerQuery.toUpperCase().indexOf(" WHERE ");
//                int end = optimizerQuery.toUpperCase().indexOf(" GROUP BY ");
//                if (end == -1) {
//                    end = optimizerQuery.toUpperCase().indexOf(" HAVING ");
//                    if (end == -1) {
//                        end = optimizerQuery.toUpperCase().indexOf(" ORDER BY ");
//                        if (end == -1) {
//                            end = optimizerQuery.toUpperCase().indexOf(" LIMIT ");
//                        }
//                    }
//                }
//                
//                smartMySQLRewrittenQuery = (start == -1 ? optimizerQuery : optimizerQuery.substring(0, start)) + " where (" + generatedCondition + ")" + (end == -1 ? "" : optimizerQuery.substring(end));
//            }
//            
//            if (analyzerResult.getOrderBy() != null) {
//                
//                for (AnalyzerResult.ColumnResult cr: analyzerResult.getOrderBy()) {
//                    
//                    Column c = AnalyzerResult.findColumn(analyzerResult, cr.getColumnName());
//                    if (c != null) {
//                        pushCardinalityColumn(c);
//                    }
//                }
//            }
//            
//            if (analyzerResult.getGroupBy() != null) {
//                
//                for (AnalyzerResult.ColumnResult cr: analyzerResult.getGroupBy()) {
//                    
//                    Column c = AnalyzerResult.findColumn(analyzerResult, cr.getColumnName());
//                    if (c != null) {
//                        pushCardinalityColumn(c);
//                    }
//                }
//            }                        
//            
//            // find "More filter table"
//            // table that have high cardinality in WHERE conditions
//
//            DBTable moreFilterTable = null;
//            long moreFilterCardinality = -1;
//            long moreFilterCount = -1;
//
//            for (DBTable t: tablesSet) {
//                List<Column> constraints = tableContraintColumns.get(t);
//                List<Column> ranges = tableRangeColumns.get(t);
//
//                long cardinality = 0;
//                if (constraints != null) {
//                    for (Column c: constraints) {
//                        cardinality += getColumnDegree(analyzerResult, c);
//                    }
//                }
//
//                if (ranges != null && !ranges.isEmpty()) {
//                    cardinality += getColumnDegree(analyzerResult, ranges.get(0));
//                }
//
//                if (cardinality >= moreFilterCardinality) {
//                    
//                    int size = 0;
//                    List<Column> l = filterColumns.get(t);
//                    if (l != null) {
//                        size = l.size();
//                    }
//                    
//                    if (cardinality > moreFilterCardinality || size > moreFilterCount) {
//                        moreFilterTable = t;
//                        moreFilterCardinality = cardinality;
//                        moreFilterCount = size;
//                    }
//                }
//            }
//            
//            // find Transaction table
//            // table that not have PK/Unique/AI in JOIN conditions
//            // we get first table from transactionTablesSet
//            DBTable transactionTable = null;
//            for (DBTable t: transactionTables) {
//                if (t != moreFilterTable) {
//                    transactionTable = t;
//                    break;
//                }
//            }
//            
//            // check order by columns
//            // If Order by columns are more filter table column and we need to add  
//            // condition order by column as range filter rules. 
//            // If the order by columns are different tables, or if the columns are 
//            // different sorting order then ignore  order by clause columns for index. 
//            // If the table where condition MFT has ranger filter and order by columns 
//            // then we need to pick up high cardinality column of range condition or  order by clause . 
//            
//            boolean ignoreOrderBy = false;
//            List<Column> orderByColumns = new ArrayList<>();
//            Boolean sort = null;
//            if (analyzerResult.getOrderBy() != null) {
//                
//                
//                // If SELECT columns not all from MFT - ignore order by
//                if (analyzerResult.getColumns() != null) {
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getColumns()) {
//                        Column c = AnalyzerResult.findColumn(analyzerResult, cr.getColumnName());
//                        if (c != null) {
//                            if (!c.getTable().equals(moreFilterTable)) {
//                                ignoreOrderBy = true;
//                                break;
//                            }
//                        }
//                    }
//                }
//                
//                if (!ignoreOrderBy) {
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getOrderBy()) {
//
//                        Column c = AnalyzerResult.findColumn(analyzerResult, cr.getColumnName());
//                        if (c != null) {
//                            if (c.getTable().equals(moreFilterTable)) {
//                                orderByColumns.add(c);
//
//                                c.setOrderBy(true);
//                                c.setGroupBy(false);
//
//                                if (sort == null) {
//                                    sort = cr.getIsOrderDesc();
//
//                                } else if (!Objects.equals(sort, cr.getIsOrderDesc())) {
//                                    ignoreOrderBy = true;
//                                    break;
//                                }
//                            } else {
//                                ignoreOrderBy = true;
//                                break;
//                            }
//                        }
//                    }
//                }
//            }
//            
//            if (!ignoreOrderBy && !orderByColumns.isEmpty()) {
//                for (Column c: orderByColumns) {
//                    c.setOrderByDesc(sort);
//                }
//            }
//            
//            int sizeOfNotJoinColumns = 0;
//            
//            // if both same - index on only WHERE conditions
//            // if different - need index on WHERE conditions and on other tables in JOIN
//            if (moreFilterTable != transactionTable) {
//                                
//                for (DBTable t: moreFilterColumns.keySet()) {
//
//                    List<Column> columns = t != moreFilterTable ? new ArrayList<>(moreFilterColumns.get(t)) : new ArrayList<>();
//
//                    columns.sort((Column o1, Column o2) -> {
//                        // need find column degree
//                        // if column hase index get degree from show index
//                        // if not need call select methods
//                        return compareCardinality(analyzerResult, o1, o2);
//                    });
//
//                    if (tableContraintColumns.get(t) != null) {
//                        
//                        List<Column> l = new ArrayList<>(tableContraintColumns.get(t));
//                        l.sort((Column o1, Column o2) -> {
//                            // need find column degree
//                            // if column hase index get degree from show index
//                            // if not need call select methods
//                            return compareCardinality(analyzerResult, o1, o2);
//                        });
//                        
//                        for (Column c: l) {
//                            if (!columns.contains(c)) {
//                                columns.add(c);
//                            }
//                        }
//                    }
//                    
//                    int size = columns.size();
//                    if (size > indexMaxNoCount - 1) {
//                        columns = columns.subList(0, indexMaxNoCount - 1);
//                    }
//                    
//                    List<Column> ls = getOrderByColumnsForMFT(analyzerResult, tableRangeColumns, t, moreFilterTable, ignoreOrderBy, orderByColumns);
//                    ls.sort((Column o1, Column o2) -> {
//                        // need find column degree
//                        // if column hase index get degree from show index
//                        // if not need call select methods
//                        return compareCardinality(analyzerResult, o1, o2);
//                    });
//                    
//                    for (Column c: ls) {
//                        if (!columns.contains(c)) {
//                            columns.add(c);
//                        }
//                    }  
//                    
//                    boolean ignore = false;
//                    
//                    // check for PK
//                    TablePkAndUniqueStructure ignoreStructure = TablePkAndUniqueStructure.create(t);
//                    
//                    for (Column c: columns) {
//                        List<AnalyzerResult.ConditionResult> l = conditionCache.get(c);
//                        if (l != null) {
//                            for (AnalyzerResult.ConditionResult cr: l) {
//                                // ignore if PK but if it from order by - its ok
//                                if (isConstraint(cr) && ignoreAsPk(c) && (c.getOrderBy() == null || !c.getOrderBy())) {
//                                    
//                                    ignoreStructure.removeColumn(c);
//                                    if (ignoreStructure.needIgnore()) {
//                                        ignore = true;
//                                        break;
//                                    }
//                                    
//                                    
//                                } else if (isConstraint(cr) && c.isAutoIncrement() && (c.getOrderBy() == null || !c.getOrderBy())) {
//                                    ignore = true;
//                                    break;
//                                }
//                            }
//                        }
//                        
//                        if (ignore) {
//                            break;
//                        }
//                    }
//                    
//                    if (!ignore) {
//                        // Check partitions RULES
//
//                        LinkedHashSet<Column> columnForRecomendation = new LinkedHashSet<>(columns);
//                        checkPartitions(analyzerResult, t.getName(), columnForRecomendation, tables);
//
//                        checkSpatial(columnForRecomendation, columnSpatial);
//
//                        columns = new ArrayList<>(columnForRecomendation);
//                        
//                        checkForIndexes(analyzerResult, t, columns, filterColumns.get(t));
//                    }
//                }
//            } else {
//                
//                for (DBTable t: moreFilterColumns.keySet()) {
//
//                    List<Column> columns = new ArrayList<>(moreFilterColumns.get(t));
//                    
//                    List<Column> forClear = new ArrayList<>();
//                    for (Column c: columns) {
//                        if (checkJoinColumn(analyzerResult, c, c.getTable().getName(), true)) {
//                            forClear.add(c);
//                        } else {
//                            sizeOfNotJoinColumns++;
//                        }
//                    }
//                    
//                    columns.removeAll(forClear);
//                    
//                    columns.sort((Column o1, Column o2) -> {
//                        // need find column degree
//                        // if column hase index get degree from show index
//                        // if not need call select methods
//                        return compareCardinality(analyzerResult, o1, o2);
//                    });
//
//                    if (tableContraintColumns.get(t) != null) {
//                        
//                        List<Column> l = new ArrayList<>(tableContraintColumns.get(t));
//                        l.sort((Column o1, Column o2) -> {
//                            // need find column degree
//                            // if column hase index get degree from show index
//                            // if not need call select methods
//                            return compareCardinality(analyzerResult, o1, o2);
//                        });
//                        
//                        for (Column c: l) {
//                            if (!columns.contains(c)) {
//                                columns.add(c);
//                            }
//                        }
//                    }
//
//                    int size = columns.size();
//                    if (size > indexMaxNoCount - 1) {
//                        columns = columns.subList(0, indexMaxNoCount - 1);
//                    }                    
//
//                    List<Column> ls = getOrderByColumnsForMFT(analyzerResult, tableRangeColumns, t, moreFilterTable, ignoreOrderBy, orderByColumns);
//                    ls.sort((Column o1, Column o2) -> {
//                        // need find column degree
//                        // if column hase index get degree from show index
//                        // if not need call select methods
//                        return compareCardinality(analyzerResult, o1, o2);
//                    });
//
//                    for (Column c: ls) {
//                        if (!columns.contains(c)) {
//                            columns.add(c);
//                        }
//                    }
//                    
//                    boolean ignore = false;
//                    
//                    // check for PK
//                    TablePkAndUniqueStructure ignoreStructure = TablePkAndUniqueStructure.create(t);
//                    
//                    for (Column c: columns) {
//                        List<AnalyzerResult.ConditionResult> l = conditionCache.get(c);
//                        if (l != null) {
//                            for (AnalyzerResult.ConditionResult cr: l) {
//                                // ignore if PK but if it from order by - its ok
//                                if (isConstraint(cr) && ignoreAsPk(c) && (c.getOrderBy() == null || !c.getOrderBy())) {
//                                    
//                                    ignoreStructure.removeColumn(c);
//                                    if (ignoreStructure.needIgnore()) {
//                                        ignore = true;
//                                        break;
//                                    }
//                                    
//                                    
//                                } else if (isConstraint(cr) && c.isAutoIncrement() && (c.getOrderBy() == null || !c.getOrderBy())) {
//                                    ignore = true;
//                                    break;
//                                }
//                            }
//                        }
//                        
//                        if (ignore) {
//                            break;
//                        }
//                    }
//
//                    if (!ignore) {
//                        LinkedHashSet<Column> columnForRecomendation = new LinkedHashSet<>(columns);
//                        checkPartitions(analyzerResult, t.getName(), columnForRecomendation, tables);
//
//                        checkSpatial(columnForRecomendation, columnSpatial);
//
//                        columns = new ArrayList<>(columnForRecomendation);
//
//                        checkForIndexes(analyzerResult, t, columns, filterColumns.get(t));
//                    }
//                }
//            }
//            
//            if (moreFilterTable != transactionTable && analyzerResult.getRecomendations() != null && !analyzerResult.getRecomendations().isEmpty()) {
//                // The two table has been join with one table’s primary key it is primary key.
//                // In this case we should not recommedn “The Database design need to improve based on normalizations. Else no need”
//                boolean needRecommend = true;
//                for (DBTable t: moreFilterColumns.keySet()) {
//                    List<Column> l = moreFilterColumns.get(t);
//                    if (!l.isEmpty()) {
//                        boolean allPk = true;
//                        for (Column c: l) {
//                            if (!c.isPrimaryKey()) {
//                                allPk = false;
//                                break;
//                            }
//                        }
//
//                        if (allPk) {
//                            needRecommend = false;
//                            break;
//                        }
//                    }
//                }
//                
//                if (needRecommend) {
//                    analyzerResult.addRecomendation(database.getName(), moreFilterTable.getName(), RECOMENDATION_16, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//            }
//            
//            if (sizeOfNotJoinColumns > 1 || transactionTables.contains(moreFilterTable)) {
//                analyzerResult.addRecomendation(database.getName(), moreFilterTable.getName(), RECOMENDATION_3, null, null, analyzerResult.getQuery(), new ArrayList<>());
//            }
//            
//        } else {
//            
//            // If we find OR operands - add recomendation
//            analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_6, null, null, analyzerResult.getQuery(), new ArrayList<>());
//        }
//        
//        return false;
//    }
//    
//    private List<Column> getOrderByColumnsForMFT(AnalyzerResult analyzerResult, Map<DBTable, List<Column>> tableRangeColumns, DBTable t, DBTable moreFilterTable, boolean ignoreOrderBy, List<Column> orderByColumns) {
//        List<Column> ls = new ArrayList<>();
//        List<Column> range = tableRangeColumns.get(t);
//        if (!ignoreOrderBy && !orderByColumns.isEmpty()) {
//
//            // The ORDER BY columns also from same table which are HFT.
//            if (t == moreFilterTable) {
//                
//                if (range == null) {
//                    ls.addAll(orderByColumns);
//
//                } else {
//                    range = new ArrayList<>(range);
//                    range.sort((Column o1, Column o2) -> {
//                        return compareCardinality(analyzerResult, o1, o2);
//                    });
//
//                    orderByColumns.sort((Column o1, Column o2) -> {
//                        return compareCardinality(analyzerResult, o1, o2);
//                    });
//                                        
//                    // If projection column[columns after SELECT and before FROM] are from same table with and it is should be HFT.
//                    TableProvider t0 = null;
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getColumns()) {
//                        Column c0 = AnalyzerResult.findColumn(analyzerResult, cr.getColumnName());
//                        if (c0 != null) {
//                            if (t0 == null) {
//                                t0 = c0.getTable();
//                            }
//
//                            if (t0 != c0.getTable()) {
//                                ignoreOrderBy = true;
//                            }
//                        }
//                    }
//                    
//                    if (ignoreOrderBy) {
//                        if (!range.isEmpty()) {
//                            ls.add(range.get(0));
//                        }
//                        return ls;
//                    }
//
//                    // need add columns that have high cardinality
//                    // if there are more then 1 column in order by then need to multiply each column cardinality and compare with high cardinally rage condition column. 
//                    // ex x>1 and order by y , z 
//                    // x cardinality: 10 and y =2 and z = 4 
//                    // in this case we need to choose x
//                    // x cardinality: 10 and y =2 and z = 5
//                    // in this case if x in list then need to chose order by column , if y and z in list then coose x
//                    // if both are in list then order by is high priority 
//
//                    boolean allRangeInLow = true;
//                    Long cardinality0 = null;
//                    for (Column c0: range) {
//                        Long cardinality = columnCardinalityService.getColumnCardinality(params.cacheKey(), moreFilterTable.getDatabase().getName(), moreFilterTable.getName(), c0.getName());
//                        if (cardinality != null) {
//                            if (cardinality0 == null) {
//                                cardinality0 = cardinality;
//                            } else {
//                                cardinality0 *= cardinality;
//                            }
//                        }
//                        
//                        if (!LOWER_DEGREE_NAMES.contains(c0.getName())) {
//                            allRangeInLow = false;
//                        }
//                    }
//
//                    boolean allOrderByInLow = true;
//                    Long cardinality1 = null;
//                    for (Column c0: orderByColumns) {
//                        Long cardinality = columnCardinalityService.getColumnCardinality(params.cacheKey(), moreFilterTable.getDatabase().getName(), moreFilterTable.getName(), c0.getName());
//                        if (cardinality1 == null) {
//                                cardinality1 = cardinality;
//                            } else {
//                                cardinality1 *= cardinality;
//                            }
//                        
//                        if (!LOWER_DEGREE_NAMES.contains(c0.getName())) {
//                            allOrderByInLow = false;
//                        }
//                    }
//                    
//                    // If the ORDER BY  columsn thse N columns then need to add all columns. 
//                    
//                    // 
//                    
//                    if (cardinality0 == cardinality1) {
//                        if (allRangeInLow && !allOrderByInLow) {
//                            ls.addAll(orderByColumns); 
//                        } else if (!allRangeInLow && allOrderByInLow) {
//                            if (!range.isEmpty()) {
//                                ls.add(range.get(0)); 
//                            }
//                        } else {
//                            ls.addAll(orderByColumns); 
//                        }
//                    } else {                  
//
//                        if (cardinality0 > cardinality1) {
//                            if (!range.isEmpty()) {
//                                ls.add(range.get(0)); 
//                            }
//                        } else {
//                            ls.addAll(orderByColumns); 
//                        }
//                    }
//                }
//            }
//        } else {
//            if (range != null && !range.isEmpty()) {
//                ls.add(range.get(0)); 
//            }
//        }
//        
//        return ls;
//    }
//    
//    private void checkForIndexes(AnalyzerResult analyzerResult, DBTable table, List<Column> columns, List<Column> filterColumns) {
//        checkForIndexes(analyzerResult, table, columns, filterColumns, null, RECOMENDATION_5, RECOMENDATION_4, SQL_1);
//    }
//    
//    private void checkForIndexes(AnalyzerResult analyzerResult, DBTable table, List<Column> columns, List<Column> filterColumns, IndexType indexType, String recommendation, String compositeRecommendation, String sql) {
//        if (columns != null && !columns.isEmpty()) {
//            boolean foundIndex = false;
//            for (Index tableIndex: table.getIndexes()) {
//
//                if (tableIndex.getColumns() != null && (indexType == null || indexType == tableIndex.getType())) {
//
//                    if (tableIndex.getColumns().size() >= columns.size()) {
//                        boolean f = true;
//                        for (int i = 0; i < columns.size(); i++) {                        
//
//                            Column c0 = columns.get(i);
//                            Column c1 = tableIndex.getColumns().get(i);
//
//                            if (!c0.getName().equalsIgnoreCase(c1.getName())) {
//                                f = false;
//                                break;
//                            }
//                        }
//
//                        if (f) {
//                            foundIndex = true;
//                            break;
//                        }
//                    } else if (tableIndex.getType() == IndexType.PRIMARY || tableIndex.getType() == IndexType.UNIQUE_TEXT) {
//                        boolean f = true;
//                        for (int i = 0; i < tableIndex.getColumns().size(); i++) {                        
//
//                            Column c0 = columns.get(i);
//                            Column c1 = tableIndex.getColumns().get(i);
//
//                            if (!c0.getName().equalsIgnoreCase(c1.getName())) {
//                                f = false;
//                                break;
//                            }
//                        }
//
//                        // check maybe this all PK / UNIQUE columns are filters - then they can going in different places
//                        if (!f && filterColumns != null) {
//                            f = true;
//                            for (Column c: tableIndex.getColumns()) {
//                                if (!columns.contains(c) || !filterColumns.contains(c)) {
//                                    f = false;
//                                    break;
//                                }
//                            }
//                        }
//                            
//                        
//                        if (f) {
//                            foundIndex = true;
//                            break;
//                        }
//                    }
//                }
//            }
//
//            // If not found - need add recommendation
//            if (!foundIndex) {
//                List<String> strs = new ArrayList<>();
//                List<String> names = new ArrayList<>();
//                for (Column c: columns) {
//                    strs.add(c.getName());
//                    names.add(convertColumnName(c, null));
//                }
//                
//                String name = String.join("_", strs);
//                
//                if (strs.size() == 1) {
//                    analyzerResult.addRecomendation(database.getName(), table.getName(), String.format(recommendation, String.join(", ", strs)), String.format("idx__%s", name), String.format(sql, table.getName(), name, String.join(", ", names)), analyzerResult.getQuery(), new ArrayList<>(columns));
//                } else {
//                    analyzerResult.addRecomendation(database.getName(), table.getName(), String.format(compositeRecommendation, String.join(", ", strs)), String.format("idx__%s", name), String.format(sql, table.getName(), name, String.join(", ", names)), analyzerResult.getQuery(), new ArrayList<>(columns));
//                }
//            } else {
//                List<String> strs = new ArrayList<>();
//                List<String> names = new ArrayList<>();
//                for (Column c: columns) {
//                    strs.add(c.getName());
//                    names.add(convertColumnName(c, null));
//                }
//                
//                String name = String.join("_", strs);
//                
//                if (strs.size() == 1) {
//                    analyzerResult.addRecomendation(true, database.getName(), table.getName(), String.format(recommendation, String.join(", ", strs)), String.format("idx__%s", name), String.format(sql, table.getName(), name, String.join(", ", names)), analyzerResult.getQuery(), new ArrayList<>(columns));
//                } else {
//                    analyzerResult.addRecomendation(true, database.getName(), table.getName(), String.format(compositeRecommendation, String.join(", ", strs)), String.format("idx__%s", name), String.format(sql, table.getName(), name, String.join(", ", names)), analyzerResult.getQuery(), new ArrayList<>(columns));
//                }
//            }
//        }
//    }
//    
//    
//    private boolean needIgnoreBecauseInWhereClause(AnalyzerResult analyzerResult, AnalyzerResult.ConditionResult where, Column column) {
//                
//        List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//        checkNormalConditionResult(where, analyzerResult, list);
//        
//        for (AnalyzerResult.ConditionResult cr: list) {
//            
//            String left = (String) cr.getLeft();
//            String right = (String) cr.getRight();
//
//            Column leftColumn = cr.getIsLeftColumn() != null && cr.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
//            Column rightColumn = cr.getIsRightColumn() != null && cr.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
//
//            if (leftColumn == null || rightColumn == null) {
//                
//                Column c;
//                if (leftColumn != null) {
//                    c = leftColumn; 
//                    // ignore function
//                    if (cr.getIsLeftFunction() != null && cr.getIsLeftFunction()) {
//                        continue;
//                    }
//                } else {
//                    c = rightColumn;
//                    // ignore function
//                    if (cr.getIsRightFunction() != null && cr.getIsRightFunction()) {
//                        continue;
//                    }
//                }
//                
//                if (c != null && c.getName().equalsIgnoreCase(column.getName()) && c.getTable().getName().equalsIgnoreCase(column.getTable().getName())) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//    
//    private void collectJoinTables(AnalyzerResult.TableResult t, Map<AnalyzerResult.TableResult, Boolean> joinTables) {
//        
//        joinTables.put(t, false);
//        
//        if (t.getJoins() != null) {
//            for (AnalyzerResult.JoinResult j: t.getJoins()) {
//                collectJoinTables(j.getTable(), joinTables);
//            }
//        }
//    }
//    
//    
//    // <editor-fold desc=" Analyzing ORDER BY columns ">
//
//    private boolean analyzerOrderByColumnsResult(AnalyzerResult analyzerResult, List<Column> filterColumns, List<AnalyzerResult.ColumnResult> columnsResult, List<AnalyzerResult.ColumnResult> groupByResult, AnalyzerResult.ConditionResult whereCondition, Set<Column> columnForRecomendation, List<Column> ignoredForSize) {
//        List<Column> columns = new ArrayList<>();
//
//        // Check for ASC ans DESC flags
//        // if exists both - need ignore
//        boolean foundAsc = false;
//        boolean foundDesc = false;
//
//        for (AnalyzerResult.ColumnResult ac: columnsResult) {
//
//            String columnName = ac.getColumnName();
//            if (checkForCalculation(columnName)) {
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_19, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                generatedAlways(analyzerResult, columnName, false);
//                continue;
//            }
//
//            //Case 3 ) We should not recommend any index if any column in order having functions
//            if (ac.getIsFunction()) {
//                // if functions - just ignore this query
//                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                generatedAlways(analyzerResult, ac.getSource(), true);
//                if (columnName.toLowerCase().contains("rand()") || columnName.toLowerCase().contains("random()")) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_22, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                return true;
//            } else {
//                Column c = AnalyzerResult.findColumn(analyzerResult, columnName);
//                if (c != null && !columns.contains(c)) {
//                    columns.add(c);
//                    if (ac.getIsOrderDesc()) {
//                        foundDesc = true;
//                    } else {
//                        foundAsc = true;
//                    }
//                    
//                    c.setOrderBy(true);
//                    c.setOrderByDesc(ac.getIsOrderDesc());
//                }
//            }
//        }
//        
//        columns.sort((Column o1, Column o2) -> {
//            return compareCardinality(analyzerResult, o1, o2);
//        });
//        
//        // All column either ASC order or DESC order should not mix
//        if (foundAsc && foundDesc) {
//            return true;
//        }
//        
//        if (!columns.isEmpty()) {
//        
//            if (whereCondition != null) {
//                // 1. the column which are in Order by could be as range filter or constraint  filter  in where clause 
//                // 2. the column which are not in Order by could be as constraint filters in where clause 
//                
//                List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//                checkNormalConditionResult(whereCondition, analyzerResult, list);
//                
//                List<Column> ignoreFilterColumns = collectOrderByAndGroupByColumns(analyzerResult);
//                // if list of conditions is empty - its mean that no condition with AND operand
//                // so just ignore 
////                if (list.isEmpty()) {
////                    return true;
////                }
//                
//                // must find 
//                // 1. if range filter not in group - than no index
//                // 2. all contraint must be at index
//                
//                List<Column> constraintColumns = new ArrayList<>();
//                int rangeCount = 0;
//                for (AnalyzerResult.ConditionResult cr: list) {
//                    String left = (String) cr.getLeft();
//                    String right = (String) cr.getRight();
//
//                    Column leftColumn = cr.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
//                    Column rightColumn = cr.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
//
//                    boolean leftIsFunction = cr.getIsLeftFunction();
//                    boolean rightIsFunction = cr.getIsRightFunction();
//                    
//                    if (!leftIsFunction && !rightIsFunction) {
//                        
//                        Column column = leftColumn;
//                        if (column != null) {
//                            if (rightColumn != null) {
//                                return true;
//                            }
//                        } else {
//                            column = rightColumn;
//                        }
//                        
//                        // collect constrains columns and range column
//                        if (column != null) {
//                            
//                            if (isConstraint(cr)) {
//                                
//                                column.setConstraint(true);
//                                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(cr.getType()) && !ignoreFilterColumns.contains(column)) {
//                                    filterColumns.add(column);
//                                }
//                                
//                                if (ignoreAsPk(column)) {
//                                    return true;
//                                } else {
//                                    if (!constraintColumns.contains(column)) {
//                                        constraintColumns.add(column);
//                                    }
//                                }
//                            } else if (isRange(cr) || isRangeSecondPriority(cr) && (NULL_VALUE.equals(left) || NULL_VALUE.equals(right))) {
//                                column.setConstraint(false);
//                                rangeCount++;
//                                // THIS IS ONLY FOR GROUP BY
////                                if (!columns.contains(column)) {
////                                    return true;
////                                }
//                                
//                                // If we have more than 1 range - ignore
//                                if (rangeCount > 1) {
//                                    return true;
//                                }
//                            }
//                        }
//                    } else {
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, rightIsFunction ? right : left, true);
//                    }
//                }
//                
//                constraintColumns.sort((Column o1, Column o2) -> {
//                    return compareCardinality(analyzerResult, o1, o2);
//                });
//
//                // Constraint columns must be first
//                for (Column c: columns) {
//                    if (!constraintColumns.contains(c)) {
//                        constraintColumns.add(c);
//                    }
//                }                
//                columns = new ArrayList<>(constraintColumns);                
//            }            
//            
//            return checkFoundedOrderByColumnsForRecommendation(analyzerResult, filterColumns, columns, columnForRecomendation, ignoredForSize);
//        }
//                
//        return false;
//    }
//    
//    private boolean checkFoundedOrderByColumnsForRecommendation(AnalyzerResult analyzerResult, List<Column> filterColumns, List<Column> columns, Set<Column> columnsForRecomendation, List<Column> ignoredForSize) {
//        // Split all columns by tables
//        Map<DBTable, List<Column>> tableColumns = new HashMap<>();
//        for (Column c: columns) {
//            List<Column> list = tableColumns.get((DBTable)c.getTable());
//            if (list == null) {
//                list = new ArrayList<>();
//                tableColumns.put((DBTable)c.getTable(), list);
//            }
//
//            list.add(c);
//        }
//
//        // igone if more than table columns using in order by
//        if (tableColumns.size() > 1) {                
//            return true;
//
//        } else {
//
//            for (DBTable table: tableColumns.keySet()) {
//
//                // if columnsForRecomendation NOT NULL - need just collect all columns
//                // and show one composit recomendation
//                if (columnsForRecomendation != null) {
//                    columnsForRecomendation.addAll(columns);
//                    // TODO Commented, but not sure
//                    //ignoredForSize.addAll(columns);
//
//                } else {
//                    checkForIndexes(analyzerResult, table, columns, filterColumns);
//                }
//            }
//        }
//        
//        return false;
//    }
//    
//    private Boolean checkJoinColumn(AnalyzerResult analyzerResult, Column column, String tableName, boolean ignoreWhere) {
//        
//        if (ignoreWhere || !needIgnoreBecauseInWhereClause(analyzerResult, analyzerResult.getWhere(), column)) {
//            if (column.getTable().getName().equalsIgnoreCase(tableName)) {
//
//                // in this case rule PASS
//                if (column.isPrimaryKey() || column.isAutoIncrement()) {
//                    return true;
//                } else {
//
//                    // need check maybe it UNIQUE
//                    List<Index> indexes = ((DBTable)column.getTable()).getIndexes();
//                    if (indexes != null) {
//                        for (Index index: indexes) {
//                            if (index.getType() == IndexType.UNIQUE_TEXT) {
//                                List<Column> indexColumns = index.getColumns();
//                                if (indexColumns != null) {
//                                    for (Column c: indexColumns) {
//                                        if (c.getName().equalsIgnoreCase(column.getName())) {
//                                            return true;
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            
//            return false;
//        }
//        
//        return null;
//    }
//    
//    // </editor-fold>
//    
//    private boolean hasIndex(Column column) {
//        if (column.isPrimaryKey()) {
//            return true;
//        }
//        
//        List<Index> indexes = ((DBTable)column.getTable()).getIndexes();
//        if (indexes != null) {
//            for (Index index: indexes) {
//                List<Column> indexColumns = index.getColumns();
//                if (indexColumns != null) {
//                    for (Column c: indexColumns) {
//                        if (c.getName().equalsIgnoreCase(column.getName())) {
//                            return true;
//                        }
//                    }
//                }
//            }
//        }
//        
//        return false;
//    }
//    
//    
//    // <editor-fold desc=" Analyzing GROUP BY columns ">
//
//    // select distinct min(actor_id), max(actor_id), first_name from actor
//    private boolean analyzerGroupByWithWhereColumnsResult(AnalyzerResult analyzerResult, List<Column> filterColumns, List<AnalyzerResult.ColumnResult> columnsResult, List<AnalyzerResult.ColumnResult> selectColumnsResult, List<Column> constraintColumns, List<Column> rangeColumns, Set<Column> columnsForRecomendation, List<Column> ignoredForSize) {
//        
//        if (analyzerResult.isDistinct()) {
//            columnsResult = selectColumnsResult;
//            selectColumnsResult = null;
//            rangeColumns = null;
//        }
//        
//        List<Column> columns = new ArrayList<>();
//                            
//        // This is only for query with DEGREE = 1
//        // so get first table
//        DBTable table = null;
//        AnalyzerResult.FromResult fr = analyzerResult.getFrom();
//        if (fr != null && fr.getTables() != null && fr.getTables().size() >= 1) {
//            String tableName = fr.getTables().get(0).getTableName();
//            List<DBTable> tables = AnalyzerResult.findTables(analyzerResult, tableName);
//            if (tables != null && tables.size() == 1) {
//                table = tables.get(0);
//            }
//        }
//               
//        List<Column> selectColumns = new ArrayList<>();
//        
//        if (table != null) {
//            
//            for (AnalyzerResult.ColumnResult ac: columnsResult) {
//
//                String columnName = ac.getColumnName();
//                String name = columnName;
//                if (checkForCalculation(columnName)) {
//                    analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_19, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    generatedAlways(analyzerResult, columnName, false);
//                    continue;
//                }
//
//                if (ac.getIsFunction()) {
//                    String[] functionColumns = getFunctionColumns(columnName);
//                    if (functionColumns.length > 0) {
//                        name = functionColumns[0].trim();
//                    }
//                    
//                    boolean minmax = false;
//                            
//                    if (columnName.toLowerCase().trim().matches("min\\s*\\(.*\\)") || columnName.toLowerCase().trim().matches("max\\s*\\(.*\\)")) {                        
//                        minmax = true;
//                    }
//                    
//                    // if functions - just ignore this query
//                    if (!analyzerResult.isDistinct() && !minmax) {
//                        analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, ac.getSource(), true);
//                        return true;
//                    }
//                }
//                
//                Column c = AnalyzerResult.findColumn(analyzerResult, name);                
//                if (c != null && (analyzerResult.isDistinct() || isFunctionAggreagate(columnName.toLowerCase().trim()))) {
//                    c.setGroupBy(true);
//                }
//                pushCardinalityColumn(c);
//                if (!columns.contains(c)) {
//                    columns.add(c);
//                }
//            }            
//            
//            // Rule: If the query is 1 degree and the table is range or list partition and partition column not in first position of group by columns then ignore group by. 
//            if (analyzerResult.isPartition(table.getName())) {
//                // first column must be partition
//                if (columns.isEmpty() || !analyzerResult.isPartitionRange(table.getName(), columns.get(0).getName()) && 
//                        !analyzerResult.isPartitionList(table.getName(), columns.get(0).getName())) {
//                    return false;
//                }
//            }
//                        
//            if (selectColumnsResult != null) {
//                // look for min/max fucntion
//                Column minColumn = null;
//                Column maxColumn = null;
//                for (AnalyzerResult.ColumnResult ac: selectColumnsResult) {
//
//                    String columnName = ac.getColumnName();
//                    String name = columnName;
//
//                    if (ac.getIsFunction()) {
//                        String[] functionColumns = getFunctionColumns(columnName);
//                        if (functionColumns.length > 0) {
//                            name = functionColumns[0].trim();
//                        }
//
////                        boolean minmax = false;
//                        if (columnName.toLowerCase().trim().matches("min\\s*\\(.*\\)")) {
//                            if (minColumn != null && !minColumn.getName().equalsIgnoreCase(name)) {
//                                minColumn = null;
//                                maxColumn = null;
//                                break;
//                            }
//
//                            minColumn = AnalyzerResult.findColumn(analyzerResult, name);
//                            minColumn.setGroupBy(true);
////                            minmax = true;
//                        }
//
//                        if (columnName.toLowerCase().trim().matches("max\\s*\\(.*\\)")) {
//                            if (maxColumn != null && !maxColumn.getName().equalsIgnoreCase(name)) {
//                                minColumn = null;
//                                maxColumn = null;
//                                break;
//                            }
//
//                            maxColumn = AnalyzerResult.findColumn(analyzerResult, name);
//                            maxColumn.setGroupBy(true);
////                            minmax = true;
//                        }
//                        
//                        // if functions - just ignore this query
////                        if (!analyzerResult.isDistinct() && !minmax) {
////                            return true;
////                        }
//                    }                
//                }
//                
//                if (minColumn != null) {
//                    pushCardinalityColumn(minColumn);
//                    if (!selectColumns.contains(minColumn)) {
//                        selectColumns.add(minColumn);
//                    }
//                } else if (maxColumn != null) {
//                    pushCardinalityColumn(maxColumn);
//                    if (!selectColumns.contains(maxColumn)) {
//                        selectColumns.add(maxColumn);
//                    }
//                }
//            }
//        }
//        
////        columns.sort((Column o1, Column o2) -> {
////            return compareCardinality(analyzerResult, o1, o2);
////        });
//        
//        selectColumns.sort((Column o1, Column o2) -> {
//            return compareCardinality(analyzerResult, o1, o2);
//        });
//        
//        for (Column c: selectColumns) {
//            if (!columns.contains(c)) {
//                columns.add(c);
//            }
//        }
//
//        if (!columns.isEmpty() && table != null) {            
//            
//            if (constraintColumns != null && !constraintColumns.isEmpty()) {
//                constraintColumns.sort((Column o1, Column o2) -> {
//                    return compareCardinality(analyzerResult, o1, o2);
//                });
//                
//                List<Column> list = new ArrayList<>(constraintColumns);
//                int size = list.size();
//                if (size > indexMaxNoCount - 1) {
//                    list = list.subList(0, indexMaxNoCount - 1);
//                }
//                
//                columns.addAll(list);
//            }
//            
//            if (rangeColumns != null && !rangeColumns.isEmpty()) {
//                rangeColumns.sort((Column o1, Column o2) -> {
//                    return compareCardinality(analyzerResult, o1, o2);
//                });
//                
//                if (!columns.contains(rangeColumns.get(0))) {
//                    columns.add(rangeColumns.get(0));
//                }
//            }
//                        
//            
//            // if columnsForRecomendation NOT NULL - need just collect all columns
//            // and show one composit recomendation
//            if (columnsForRecomendation != null) {
//                columnsForRecomendation.addAll(columns);
//                ignoredForSize.addAll(columns);
//                
//            } else {                
//                // need to ignore PK/UQ/AI in group by clause and functions
//                filterIgnoreColumns(analyzerResult, table, columns);
//                // find index
//                checkForIndexes(analyzerResult, table, columns, filterColumns);
//            }
//        }
//        
//        return true;
//    }
//        
//    // </editor-fold>
//    
//    
//    private boolean isFunctionAggreagate(String name) {
//        return name.matches("min\\s*\\(.*\\)") ||
//               name.matches("max\\s*\\(.*\\)") ||
//               name.matches("avg\\s*\\(.*\\)") ||
//               name.matches("bit_and\\s*\\(.*\\)") ||
//               name.matches("bit_or\\s*\\(.*\\)") ||
//               name.matches("bit_xor\\s*\\(.*\\)") ||
//               name.matches("count\\s*\\(.*\\)") ||
//               name.matches("group_concat\\s*\\(.*\\)") ||
//               name.matches("json_arrayagg\\s*\\(.*\\)") ||
//               name.matches("json_objectagg\\s*\\(.*\\)") ||
//               name.matches("std\\s*\\(.*\\)") ||
//               name.matches("stdev\\s*\\(.*\\)") ||
//               name.matches("stddev_pop\\s*\\(.*\\)") ||
//               name.matches("stddev_samp\\s*\\(.*\\)") ||
//               name.matches("sum\\s*\\(.*\\)") ||
//               name.matches("var_pop\\s*\\(.*\\)") ||
//               name.matches("var_samp\\s*\\(.*\\)") ||
//               name.matches("variance\\s*\\(.*\\)");
//    }
//    
//    // <editor-fold desc=" Analyzing CONDITIONS ">
//
//    //select * from actor as a, film_actor as f where a.first_name=2 and f.actor_id=2 and f.film_id>2
//    private boolean analyzerConditionResult(AnalyzerResult analyzerResult, List<Column> filterColumns, List<Column> ignoredFilterColumns, AnalyzerResult.ConditionResult condition, Set<Column> columnsForRecomendation, Set<Column> columnsFullText, Set<Column> columnsSpatial, boolean where, boolean addOthers) throws SQLException {
//       
//        boolean onlyOr = false;
//        // get only if exist not empty condition
//        if (condition != null) {
//            
//            List<AnalyzerResult.ConditionResult> list = new ArrayList<>();
//            boolean withOr = !checkNormalConditionResult(condition, analyzerResult, list);
//            if (list.isEmpty()) {
//                if (!checkOrConditionResult(condition, analyzerResult, list)) {
//                    list.clear();
//                } else {
//                    onlyOr = true;
//                }
//            }
//            
//            /*
//                If any query filter come with  
//
//                Case 1) Select * from Table where column1=’xxxx’ OR (column2=’asdfasf’ and column3=’asdf’)
//                Case 2)  Select * from Table where (column2=’asdfasf’ and column3=’asdf’) OR column1=’xxxx’ 
//
//                Need to recommend index 
//
//                Alter table Table add index Idx_column1(column1(16));
//                Add recommendation :
//                Query rewrite:
//                Select * from Table where column1=’xxxx’
//                Union 
//                Select * from Table where column2=’asdfasf’ and column3=’asdf’
//            */
//            
//            Map<Column, AnalyzerResult.ConditionResult> columns0 = new HashMap<>();
//            Map<Column, AnalyzerResult.ConditionResult> columns1 = new HashMap<>();
//            AnalyzerResult.ConditionResult otherCondition = null;
//            
//            if ("OR".equalsIgnoreCase(condition.getOperand())) {
//                AnalyzerResult.ConditionResult left = (AnalyzerResult.ConditionResult) condition.getLeft();
//                AnalyzerResult.ConditionResult right = (AnalyzerResult.ConditionResult) condition.getRight();
//                
//                List<AnalyzerResult.ConditionResult> list0 = new ArrayList<>();
//                List<AnalyzerResult.ConditionResult> list1 = new ArrayList<>();
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(left.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(left.getType())) {
//                    
//                    list0.add(left);
//                    if (AnalyzerResult.CONDITION_TYPE__CONDITION.equalsIgnoreCase(right.getType())) {
//                        boolean hasOr = !checkNormalConditionResult(right, analyzerResult, list1);
//                        if (hasOr) {
//                            list0.clear();
//                            list1.clear();
//                        }
//                        otherCondition = right;
//                    } else {
//                        list1.add(right);
//                    }
//                } else if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(right.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(right.getType())) {
//                    
//                    list0.add(right);
//                    if (AnalyzerResult.CONDITION_TYPE__CONDITION.equalsIgnoreCase(left.getType())) {
//                        boolean hasOr = !checkNormalConditionResult(left, analyzerResult, list1);
//                        if (hasOr) {
//                            list0.clear();
//                            list1.clear();
//                        }
//                        otherCondition = left;
//                    } else {
//                        list1.add(left);
//                    }
//                }
//                
//                if (!list0.isEmpty() && !list1.isEmpty()) {
//                    if (list0.size() == 1) {
//                        
//                        for (AnalyzerResult.ConditionResult cr: list0) {                    
//
//                            String leftStr = (String) cr.getLeft();
//                            String rightStr = (String) cr.getRight();
//                            
//                            Column leftColumn = cr.getIsLeftColumn() != null && cr.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, leftStr) : null;
//                            Column rightColumn = cr.getIsRightColumn() != null && cr.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, rightStr) : null;                
//
//                            boolean leftIsFunction = cr.getIsLeftFunction() != null ? cr.getIsLeftFunction() : false;
//                            boolean rightIsFunction = cr.getIsRightFunction() != null ? cr.getIsRightFunction() : false;
//                            
//                            pushCardinalityColumn(leftColumn);
//                            pushCardinalityColumn(rightColumn);
//                            if (leftColumn != null && !leftIsFunction || rightColumn != null && !rightIsFunction) {
//                                
//                                if (leftColumn == null || rightColumn == null) {
//                                    Column column = leftColumn;
//                                    if (column == null) {
//                                        column = rightColumn;
//                                    }
//                                    columns0.put(column, cr);
//                                }
//                            }
//                        }
//                    }
//                    
//                    if (list1.size() == 1) {
//                        
//                        for (AnalyzerResult.ConditionResult cr: list1) {                    
//
//                            String leftStr = (String) cr.getLeft();
//                            String rightStr = (String) cr.getRight();
//                            
//                            Column leftColumn = cr.getIsLeftColumn() != null && cr.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, leftStr) : null;
//                            Column rightColumn = cr.getIsRightColumn() != null && cr.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, rightStr) : null;                
//
//                            boolean leftIsFunction = cr.getIsLeftFunction() != null ? cr.getIsLeftFunction() : false;
//                            boolean rightIsFunction = cr.getIsRightFunction() != null ? cr.getIsRightFunction() : false;
//                            
//                            pushCardinalityColumn(leftColumn);
//                            pushCardinalityColumn(rightColumn);
//                            
//                            if (leftColumn != null && !leftIsFunction || rightColumn != null && !rightIsFunction) {
//                                
//                                if (leftColumn == null || rightColumn == null) {
//                                    Column column = leftColumn;
//                                    if (column == null) {
//                                        column = rightColumn;
//                                    }
//                                    columns1.put(column, cr);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//
//            
//            /*
//                If our tool could not give any index recommandation and the outer 
//                logical operator is “OR” and no.of filter conditions are more then 
//                two then we give recommendations as below
//
//                Recommendation: The MySQL optimiser could not use any index because 
//                the outer logical operator is “OR” and having more than two filter 
//                conditions.You may get good performance if you rewrite two query 
//                without “OR” and merge result with “UNION” then check performance 
//                difference.  Please contact to SMS(SmartMySQL) support team if you 
//                have any doubts.             
//            */
//            if (withOr && list.isEmpty()) {
//                List<Column> fcs = new ArrayList<>();
//                collectFilterColumn(condition, analyzerResult, fcs, true);
//                if (fcs.size() > 2) {
//                    analyzerResult.setRecommendationEmptyWithOR(true);
//                }
//            }
//            
//            // get current table names
//            List<String> tableNames = new ArrayList<>();
//            if (analyzerResult.getFrom() != null) {
//                for (AnalyzerResult.TableResult t: analyzerResult.getFrom().getTables()) {
//                    tableNames.add(t.getAliasName() != null ? t.getAliasName().toLowerCase() : t.getTableName().toLowerCase());
//                }
//            }
//
//            // collect constraint and range conditions
//            List<Column> conditions = new ArrayList<>();
//            List<Column> ranges = new ArrayList<>();
//            List<Column> rangesSecond = new ArrayList<>();
//            
//            List<Column> others = new ArrayList<>();
//            if (addOthers) {
//                List<Column> groupBy = new ArrayList<>();
//                List<Column> orderBy = new ArrayList<>();
//                
//                for (Column c: ignoredFilterColumns) {
//                    if (c.getGroupBy() != null && c.getGroupBy()) {
//                        if (!groupBy.contains(c)) {
//                            groupBy.add(c);
//                        }
//                    } 
//                    
//                    if (c.getOrderBy() != null && c.getOrderBy()) {
//                        if (!orderBy.contains(c)) {
//                            orderBy.add(c);
//                        }
//                    }
//                }
//                
//                if (groupBy.isEmpty()) {
//                    for (Column c: orderBy) {
//                        c.setGroupBy(false);
//                    }
//                    others.addAll(orderBy);
//                } else {
//                    for (Column c: groupBy) {
//                        c.setOrderBy(false);
//                    }
//                    others.addAll(groupBy);
//                }
//            }
//
//            for (AnalyzerResult.ConditionResult cr: list) {                    
//                
//                if (cr.getIsFullText()) {
//                    if (cr.getLeft() instanceof String) {
//                        String[] l = cr.getLeft().toString().split(",");
//                        for (String s: l) {
//                            Column c = AnalyzerResult.findColumn(analyzerResult, s.trim());
//                            pushCardinalityColumn(c);
//                            columnsFullText.add(c);
//                        }
//                    }
//                } else {
//
//                    if (!cr.getIgnore()) {
//                        String left = (String) cr.getLeft();
//                        String right = (String) cr.getRight();
//                        
//                        boolean isLeft = checkForCalculation(left);
//                        boolean isRight = checkForCalculation(right);
//                        if (isLeft || isRight) {
//                            analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_19, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                            generatedAlways(analyzerResult, isRight ? right : left, false);
//                            continue;
//                        }
//                        
//                        Column leftColumn = cr.getIsLeftColumn() != null && cr.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
//                        Column rightColumn = cr.getIsRightColumn() != null && cr.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
//
//                        boolean leftIsFunction = cr.getIsLeftFunction() != null ? cr.getIsLeftFunction() : false;
//                        boolean rightIsFunction = cr.getIsRightFunction() != null ? cr.getIsRightFunction() : false;
//                            
//                        pushCardinalityColumn(leftColumn);
//                        pushCardinalityColumn(rightColumn);
//                        
//                        if (leftColumn != null || rightColumn != null) {
//                                                        
//                            // check for spatial columns
//                            boolean spatial = false;
//                            if (leftColumn != null && leftColumn.getDataType() != null && "GEOMETRY".equals(leftColumn.getDataType().getName().toUpperCase())) {
//                                columnsSpatial.add(leftColumn);
//                                spatial = true;
//                            }
//                            
//                            if (rightColumn != null && rightColumn.getDataType() != null && "GEOMETRY".equals(rightColumn.getDataType().getName().toUpperCase())) {
//                                columnsSpatial.add(rightColumn);
//                                spatial = true;
//                            }
//                            
//                            if (spatial) {
//                                continue;
//                            }
//
//                            boolean isFunction = false;
//
//                            // Validation 1 - check for functions
//                            if (leftIsFunction) {
//                                isFunction = checkFunction(analyzerResult, left);
//                            }
//
//                            if (rightIsFunction) {
//                                isFunction = checkFunction(analyzerResult, right);
//                            }
//
//                            boolean columnsOk = leftColumn == null || rightColumn == null;
//                            if (!columnsOk) {
//                                // check maybe of column not from current lvl query
//                                String tableName = getTableName(left);
//                                if (tableName != null && !tableNames.contains(tableName.toLowerCase())) {
//                                    columnsOk = true;
//                                }
//
//                                tableName = getTableName(right);
//                                if (tableName != null && !tableNames.contains(tableName.toLowerCase())) {
//                                    columnsOk = true;
//                                }
//                            }
//
//                            if (!isFunction && columnsOk) {
//                                
//                                Column column = leftColumn != null ? leftColumn : rightColumn;
//                                
//                                Object value = leftColumn != null ? cr.getRight() : cr.getLeft();
//                                
//                                if (isConstraint(cr)) {
//                                    column.setConstraint(true);
//                                    
//                                    if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(cr.getType()) && !ignoredFilterColumns.contains(column)) {
//                                        filterColumns.add(column);
//                                    }
//                                    
//                                    // If the where condition filter have any one of the column is primary key ()and it is constrained filter then no need to consider any new index for such query
//                                    if (where && column != null) {
//                                        if (ignoreAsPk(column)) {
//                                            return onlyOr;
//                                        }
//                                    }
//
//                                    conditions.add(column);
//
//                                } else if (isRange(cr)) {
//                                    column.setConstraint(false);
//                                    ranges.add(column);
//
//                                } else if (isRangeSecondPriority(cr) && (NULL_VALUE.equalsIgnoreCase(left) || NULL_VALUE.equalsIgnoreCase(right))) {
//                                    column.setConstraint(false);
//                                    rangesSecond.add(column);
//                                }
//
//                                
//                                if (leftColumn != null && rightColumn != null) {
//                                    continue;
//                                }
//
//                                if (value != null) {
//                                    String str = value.toString().trim();
//                                    if (str.startsWith("'") && str.endsWith("'") && column != null && isColumnTypeDigit(column.getDataType())) {
//                                        analyzerResult.addRecomendation(database.getName(), column.getTable() != null ? column.getTable().getName() : "", String.format(RECOMENDATION_11, column.getName()), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                                    }
//                                }
//                            } else if (isFunction) {
//                                analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                                generatedAlways(analyzerResult, rightIsFunction ? right : left, true);
//                            }
//                        } else if (leftIsFunction || rightIsFunction) {
//                            analyzerResult.addRecomendation(database.getName(), "", RECOMENDATION_15, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                            generatedAlways(analyzerResult, rightIsFunction ? right : left, true);
//                        }
//                    }
//                }
//            }
//            
//            // if the query  degree is 1 the query has limit and the limit number is less then total total 
//            // no. scan rows and less than 10k rows consider order by column/s also as range filter. 
//            
//            if (analyzerResult.getDegree() == 1 && analyzerResult.getLimit() != null && analyzerResult.getOrderBy() != null) {
//                Long limit = null;
//                try {
//                    if (analyzerResult.getLimit().contains(",")) {
//                        limit = Long.valueOf(analyzerResult.getLimit().split(",")[1].trim());
//                    } else {
//                        limit = Long.valueOf(analyzerResult.getLimit().trim());
//                    }
//                } catch (Throwable th) {
//                }
//                
//                Long rowScan = analyzerResult.getCachedRows(analyzerResult.getFrom().getTables().get(0).getTableName());
//                if (limit != null && limit < 10000 && rowScan != null && limit < rowScan) {
//                    for (AnalyzerResult.ColumnResult cr: analyzerResult.getOrderBy()) {
//                        Column c = AnalyzerResult.findColumn(analyzerResult, cr.getColumnName());
//                        if (c != null) {
//                            pushCardinalityColumn(c);
//                            ranges.add(c);
//                        }
//                    }
//                }
//            }
//
//            conditions.sort((Column o1, Column o2) -> {
//                return compareCardinality(analyzerResult, o1, o2);
//            });
//            
//            if (ranges.isEmpty()) {
//                ranges.addAll(rangesSecond);
//            }
//
//            ranges.sort((Column o1, Column o2) -> {
//                return compareCardinality(analyzerResult, o1, o2);
//            });
//            
//            // check for index all conditions + ranges(0)
//            // collect them for each table
//
//            Map<DBTable, List<Column>> indexes = new HashMap<>();
//            for (Column c: conditions) {
//                if (c != null) {
//                    List<Column> columns = indexes.get((DBTable)c.getTable());
//                    if (columns == null) {
//                        columns = new ArrayList<>();
//                        indexes.put(((DBTable)c.getTable()), columns);
//                    }
//                    
//                    if (!columns.contains(c)) {
//                        columns.add(c);
//                    }
//                }
//            }
//            
//            for (DBTable t: indexes.keySet()) {
//                List<Column> columns = indexes.get(t);
//                if (columns != null) {
//                    int s = columns.size();
//                    if (s > indexMaxNoCount - 1) {
//                        columns = columns.subList(0, indexMaxNoCount - 1);
//                    }
//
//                    indexes.put(t, columns);
//                }
//            }
//            
//
//            List<DBTable> alreadyUsed = new ArrayList<>();
//            for (Column c: ranges) {
//                if (c != null && !alreadyUsed.contains((DBTable)c.getTable())) {
//
//                    alreadyUsed.add(((DBTable)c.getTable()));
//
//                    List<Column> columns = indexes.get((DBTable)c.getTable());
//                    if (columns == null) {
//                        columns = new ArrayList<>();
//                        indexes.put((DBTable)c.getTable(), columns);
//                    }
//
//                    if (!columns.contains(c)) {
//                        columns.add(c);
//                        break;
//                    }
//                }
//            }
//            
//            for (Column c: others) {
//                
//                List<Column> columns = indexes.get((DBTable)c.getTable());
//                if (columns == null) {
//                    columns = new ArrayList<>();
//                    indexes.put((DBTable)c.getTable(), columns);
//                }
//
//                if (!columns.contains(c)) {
//                    columns.add(c);
//                }
//            }
//            
//            
//            List<Column> list0 = new ArrayList<>(columns0.keySet());
//            List<Column> list1 = new ArrayList<>(columns1.keySet());
//            // check tables for founded indexes
//            for (DBTable t: indexes.keySet()) {
//
//                List<Column> columns = indexes.get(t);
//
//                // if columnsForRecomendation NOT NULL - need just collect all columns
//                // and show one composit recomendation
//                if (columnsForRecomendation != null) {
//                    columnsForRecomendation.addAll(columns);
//
//                } else {
//                    // need to ignore PK/UQ/AI in group by clause and functions
//                    filterIgnoreColumns(analyzerResult, t, columns);
//                    
//                    checkForIndexes(analyzerResult, t, columns, filterColumns);
//                }
//                
//                if (!columns0.isEmpty()) {
//                    checkForIndexes(analyzerResult, t, list0, null);
//                    
//                    Column c = list0.get(0);
//                    if (otherCondition != null) {
//                        String recommendationText = "Query rewrite:\n" +
//                            "SELECT * FROM " + c.getTable().getName() + " WHERE " + columns0.get(c).convert() + "\n" + 
//                            "UNION\n" +
//                            "SELECT * FROM " + c.getTable().getName() + " WHERE " + otherCondition.convert();
//                        analyzerResult.addRecomendation(database.getName(), "", recommendationText, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                }
//                
//                if (!columns1.isEmpty()) {
//                    checkForIndexes(analyzerResult, t, list1, null);
//                    
//                    Column c = list1.get(0);
//                    if (otherCondition != null) {
//                        String recommendationText = "Query rewrite:\n" +
//                            "SELECT * FROM " + c.getTable().getName() + " WHERE " + otherCondition.convert() + "\n" + 
//                            "UNION\n" +
//                            "SELECT * FROM " + c.getTable().getName() + " WHERE " + columns1.get(c).convert();
//                            
//                        analyzerResult.addRecomendation(database.getName(), "", recommendationText, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                }
//                
//                if (!list0.isEmpty() && !list1.isEmpty() && otherCondition == null) {
//                    Column c0 = list0.get(0);
//                    Column c1 = list1.get(0);
//                    String recommendationText = "Query rewrite:\n" +
//                        "SELECT * FROM " + c0.getTable().getName() + " WHERE " + columns0.get(c0).convert() + "\n" + 
//                        "UNION\n" +
//                        "SELECT * FROM " + c1.getTable().getName() + " WHERE " + columns1.get(c1).convert();                        
//
//                    analyzerResult.addRecomendation(database.getName(), "", recommendationText, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//                columns0.clear();
//                list0.clear();
//                columns1.clear();
//                list1.clear();
//            }
//            
//            if (!list0.isEmpty()) {
//                
//                Column c = list0.get(0);
//                
//                String name = convertColumnName(c, null);
//
//                String idxName = c.getName();
//                analyzerResult.addRecomendation(database.getName(), c.getTable().getName(), String.format(RECOMENDATION_5, name), String.format("idx__%s", idxName), String.format(SQL_1, c.getTable().getName(), idxName, name), query, Arrays.asList(new Column[]{c}));                
//                
//                if (otherCondition != null) {
//                    String recommendationText = "Query rewrite:\n" +
//                        "SELECT * FROM " + c.getTable().getName() + " WHERE " + columns0.get(c).convert() + "\n" + 
//                        "UNION\n" +
//                        "SELECT * FROM " + c.getTable().getName() + " WHERE " + otherCondition.convert();
//                    analyzerResult.addRecomendation(database.getName(), "", recommendationText, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//            }
//            
//            if (!list1.isEmpty()) {
//                
//                Column c = list1.get(0);
//                
//                
//                String name = convertColumnName(c, null);
//
//                String idxName = c.getName();
//                analyzerResult.addRecomendation(database.getName(), c.getTable().getName(), String.format(RECOMENDATION_5, name), String.format("idx__%s", idxName), String.format(SQL_1, c.getTable().getName(), idxName, name), query, Arrays.asList(new Column[]{c}));
//                if (otherCondition != null) {
//                    String recommendationText = "Query rewrite:\n" +
//                        "SELECT * FROM " + c.getTable().getName() + " WHERE " + otherCondition.convert() + "\n" + 
//                        "UNION\n" +
//                        "SELECT * FROM " + c.getTable().getName() + " WHERE " + columns1.get(c).convert();                        
//                    
//                    analyzerResult.addRecomendation(database.getName(), "", recommendationText, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                }
//                
//            }
//            
//            if (!list0.isEmpty() && !list1.isEmpty() && otherCondition == null) {
//                Column c0 = list0.get(0);
//                Column c1 = list1.get(0);
//                String recommendationText = "Query rewrite:\n" +
//                    "SELECT * FROM " + c0.getTable().getName() + " WHERE " + columns0.get(c0).convert() + "\n" + 
//                    "UNION\n" +
//                    "SELECT * FROM " + c1.getTable().getName() + " WHERE " + columns1.get(c1).convert();                        
//
//                analyzerResult.addRecomendation(database.getName(), "", recommendationText, null, null, analyzerResult.getQuery(), new ArrayList<>());
//            }
//            
//        }
//        
//        return onlyOr;
//    }
//    
//    private void filterIgnoreColumns(AnalyzerResult analyzerResult, DBTable t, List<Column> columns) {
//        List<Column> forIgnore = new ArrayList<>();
//        TablePkAndUniqueStructure ignoreStructure = TablePkAndUniqueStructure.create(t);
//        for (Column c: columns) {
//            if (c != null && c.getGroupBy() != null && c.getGroupBy()) {
//                if (ignoreAsPk(c)) {
//                    ignoreStructure.removeColumn(c);
//                }
//                if (c.isAutoIncrement()) {
//                    forIgnore.add(c);
//                }
//            }
//        }
//
//        if (ignoreStructure.needIgnorePk()) {                        
//            forIgnore.addAll(ignoreStructure.pkColumns());
//        }
//
//        if (ignoreStructure.needIgnoreUq()) {
//            for (List<Column> l: ignoreStructure.uqColumns().values()) {
//                forIgnore.addAll(l);
//            }
//        }
//
//        columns.removeAll(forIgnore);
//        
//        if (!forIgnore.isEmpty()) {
//            analyzerResult.addRecomendation(t.getDatabase().getName(), "", RECOMENDATION_31, null, null, analyzerResult.getQuery(), new ArrayList<>());
//        }
//    }
//    
//    
//    private boolean checkForCalculation(String column) {
//        return column != null && (column.contains("+") || column.contains("-") || column.contains("*") || column.contains("/"));
//    }
//    
//    private boolean ignoreAsPk(Column column) {
//                
//        DBTable table = (DBTable)column.getTable();
//        
//        if (column.isPrimaryKey()) {
//            if (table.getIndexes() != null) {
//                for (Index i: table.getIndexes()) {
//                    if (i.getType() == IndexType.PRIMARY) {
//                        if (i.getColumns() == null || i.getColumns().size() == 1 || (i.containsColumn(column.getName()) && column.isAutoIncrement())) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
//                }
//            }
//            
//        } else {
//            if (table.getIndexes() != null) {
//                for (Index i: table.getIndexes()) {
//                    if (i.getType() == IndexType.UNIQUE_TEXT) {
//                        if (i.getColumns() != null && i.containsColumn(column.getName()) && column.isAutoIncrement()) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
//                }
//            }
//        }
//        
//        return column.isAutoIncrement();
//    }
//    
//    private String getTableName(String name) {
//        String[] result = name.split("\\.");
//        String tableName = null;
//        if (result.length == 2) {
//            tableName = result[0];
//        }  else if (result.length == 3) {
//            tableName = result[1];
//        }
//
//        if (tableName != null) {
//            if (tableName.startsWith("`") && tableName.endsWith("`")) {
//                tableName = tableName.substring(1, tableName.length() - 1);
//            }
//        }
//        
//        return tableName;
//    }
//    
//    // </editor-fold>
//    
//    private int compareCardinality(AnalyzerResult analyzerResult, Column o1, Column o2) {
//        int cardinality = (int) (getColumnDegree(analyzerResult, o2) - getColumnDegree(analyzerResult, o1));
//        if (cardinality == 0) {
//            
//            if (o1.getDataType() == o2.getDataType()) {
//                int o1Length = o1.getDataType() != null && o1.getDataType().getLength() != null ? o1.getDataType().getLength() : 0;
//                int o2Length = o2.getDataType() != null && o2.getDataType().getLength() != null ? o2.getDataType().getLength() : 0;
//
//                cardinality = o1Length - o2Length;
//            }
//            
//            if (cardinality == 0) {
//                // check type cardinality
//                cardinality = DATATYPE_DEGREE.indexOf(o1.getDataType().getName().toUpperCase()) - DATATYPE_DEGREE.indexOf(o2.getDataType().getName().toUpperCase());
//                if (cardinality == 0) {
//                    // check contains low degree names
//                    for (String lowLvl: LOWER_DEGREE_NAMES) {
//                        if (o1.getName().toLowerCase().contains(lowLvl.toLowerCase())) {
//                            return 1;
//                        }
//                        
//                        if (o2.getName().toLowerCase().contains(lowLvl.toLowerCase())) {
//                            return -1;
//                        }
//                    }
//                }
//            }
//        }
//        
//        return cardinality == 0 ? (o1.isPrimaryKey() ? -1 : 1) : cardinality;
//    }
//    
//    private boolean isConstraint(AnalyzerResult.ConditionResult cr) {
//        return "=".equals(cr.getOperand());
//    }
//    
//    private boolean isRange(AnalyzerResult.ConditionResult cr) {
//        return !"=".equals(cr.getOperand()) && !"is".equalsIgnoreCase(cr.getOperand()) && !"is not".equalsIgnoreCase(cr.getOperand());
//    }
//    
//    private boolean isRangeSecondPriority(AnalyzerResult.ConditionResult cr) {
//        return "is".equalsIgnoreCase(cr.getOperand()) || "is not".equalsIgnoreCase(cr.getOperand());
//    }
//    
//    private long getColumnDegree(AnalyzerResult analyzerResult, Column column) {
//        
//        // check if table has columns without cardinality
//        TableProvider table = column.getTable();
////        for (Column c: table.getColumns()) {
//            Long cardinality = columnCardinalityService.getColumnCardinality(params.cacheKey(), table.getDatabase().getName(), table.getName(), column.getName());
//            if (cardinality == null || !withCaching) {
//                                
//                try (ResultSet rs = (ResultSet) service.execute(getFormatedQuery(QueryProvider.QUERY__TABLE_ROW_COUNT, column.getTable().getDatabase().getName(), column.getTable().getName()))) {
//                    if (rs.next()) {
//                        Long rowCount = rs.getLong(1);
//                        
//                        String columnsStr = "";
//                        
//                        List<Column> setColumns = new ArrayList<>();
//                        
//                        Set<Column> sc0 = cardinalityColumns.get(column.getTable().getName());
//                        
//                        if (sc0 != null) {
//                            
//                            if (!sc0.contains(column)) {
//                                setColumns.add(column);
//                            }
//
//
//                            for (Column c: sc0) {
//                                if (!withCaching || columnCardinalityService.getColumnCardinality(params.cacheKey(), table.getDatabase().getName(), table.getName(), c.getName()) == null) {
//                                    setColumns.add(c);
//                                }
//                            }
//                        } else {
//                            pushCardinalityColumn(column);
//                            setColumns.add(column);
//                        }                        
//                        
//                        for (Column c0: setColumns) {
//                            if (!columnsStr.isEmpty()) {
//                                columnsStr += ", ";
//                            }
//                            
//                            String s0 = c0.getName();
//                            if (isComumnTypeCardinalitySubstring(c0.getDataType())) {
//                                s0 = "SUBSTRING(`" + s0 + "`, 1, 32)";
//                            } else {
//                                s0 = "`" + s0 + "`";
//                            }
//                            
//                            columnsStr += "COUNT(DISTINCT " + s0 + ") '" + c0.getName() + "'";
//                        }
//                        
//                        Column autoincAndPrimary = null;
//                        if (column.getTable().getColumns() != null) {
//                            for (Column c0: column.getTable().getColumns()) {
//                                if (c0.isPrimaryKey() && c0.isAutoIncrement()) {
//                                    autoincAndPrimary = c0;
//                                    break;
//                                }
//                            }
//                        }
//                        
////                        log.debug("columns: " + columnsStr);
//                        
//                        if (autoincAndPrimary != null && rowCount > 2500) {
//                            
//                            try (ResultSet rs0 = (ResultSet) service.execute(
//                                    "SELECT " + columnsStr + " FROM " + table.getName() + " WHERE " + autoincAndPrimary.getName() + " > (SELECT MAX(" + autoincAndPrimary.getName() + ") FROM " + table.getName() + ") - 25000")) {
//                                if (rs0.next()) {
//                                    for (Column c0: setColumns) {
//                                        columnCardinalityService.insert(params.cacheKey(), table.getDatabase().getName(), table.getName(), c0.getName(), rs0.getLong(c0.getName()));
//                                    }
//                                }
//                            }
//                            
//                        } else {
//                            try (ResultSet rs0 = (ResultSet) service.execute(
//                                    "SELECT " + columnsStr + " FROM " + table.getName() + " LIMIT 2500")) {
//                                if (rs0.next()) {
//                                    for (Column c0: setColumns) {
//                                        columnCardinalityService.insert(params.cacheKey(), table.getDatabase().getName(), table.getName(), c0.getName(), rs0.getLong(c0.getName()));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                } catch (SQLException ex) {
//                    log.error(analyzerResult.getQuery() + "\n" + ex.getMessage(), ex);
//                    return 0l;
//                }
////                break;
//            }
////        }
//        
//        cardinality = columnCardinalityService.getColumnCardinality(params.cacheKey(), column.getTable().getDatabase().getName(), column.getTable().getName(), column.getName());
//        
//        return cardinality != null ? cardinality : 0l;
//    }
//    
//    
//    private String getQuery(String queryId) {
//        return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, queryId);
//    }
//    
//    private String getFormatedQuery(String queryId, Object...values) {
//        return String.format(getQuery(queryId), values);
//    }
//    
//    private boolean checkFunction(AnalyzerResult analyzerResult, String function) {
//        // Need get function params - and check may be there are table columns                                    
//        String functionParams = function.substring(function.indexOf("(") + 1, function.lastIndexOf(")"));
//        String[] columns = functionParams.split(",");
//        for (String column: columns) {
//            // if table column using in function - add recomendation
//            if (AnalyzerResult.findColumn(analyzerResult, column) != null) {
//                return true;
//            }
//        }
//        
//        return false;
//    }
//    
//    private String getFunctionTables(AnalyzerResult analyzerResult, String function) {
//        String[] functionColumns = getFunctionColumns(function);
//        String tableNames = "";
//        if (functionColumns != null) {
//            for (String c: functionColumns) {
//                Column column = AnalyzerResult.findColumn(analyzerResult, c);
//                if (column != null) {
//                    if (!tableNames.isEmpty()) {
//                        tableNames += ", ";
//                    }
//                    tableNames += column.getTable().getName();
//                }
//            }
//        }
//        
//        return tableNames.isEmpty() ? null : tableNames;
//    }
//    
//    private String getFunctionName(String function) {
//        return function.substring(0, function.indexOf("("));
//    }
//    
//    private String[] getFunctionColumns(String function) {
//        String functionParams = function.substring(function.indexOf("(") + 1, function.lastIndexOf(")"));
//        String[] columns = functionParams.split(",");
//        
//        for (int i = 0; i < columns.length; i++) {
//            String c = columns[i].trim();
//            if (c.toLowerCase().startsWith("distinct ")) {
//                c = c.substring(9);
//            }
//            
//            columns[i] = c;
//        }
//        return columns;
//    }
//
//    
//    private String[] getExpressionColumns(String expression) {
//        
//        String[] columns = expression.split("[-\\+\\*\\\\]");
//        for (int i = 0; i < columns.length; i++) {
//            String c = columns[i].trim();
//            columns[i] = c.replace("(", "").replace(")", "");
//        }
//        return columns;
//    }    
//    
//    private boolean checkNormalConditionResult(AnalyzerResult.ConditionResult conditionResult, AnalyzerResult analyzerResult, List<AnalyzerResult.ConditionResult> list) {
//        if (conditionResult != null && !conditionResult.getIgnore()) {
//            if ("AND".equalsIgnoreCase(conditionResult.getOperand())) {
//                AnalyzerResult.ConditionResult left = (AnalyzerResult.ConditionResult) conditionResult.getLeft();
//                AnalyzerResult.ConditionResult right = (AnalyzerResult.ConditionResult) conditionResult.getRight();
//
//                boolean leftOk = true;
//                boolean rightOk = true;
//
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(left.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(left.getType())) {                    
//                    
//                    boolean ignore = false;
//                    // operator is "like" and value is started from "%" in this case we should ignore it
//                    if (("like".equalsIgnoreCase(left.getOperand()) || "ilike".equalsIgnoreCase(left.getOperand()))) {
//                        if (left.getRight() != null && left.getRight().toString().matches("@\\d+") && left.getCommand() instanceof SelectCommand) {
//                            String value = ((SelectCommand)left.getCommand()).getString(left.getRight().toString());
//                            if (value != null && value.startsWith("'%")) {
//                                ignore = true;
//                            }
//                        }
//                    }
//                    
//                    if (!ignore) {
//                        list.add(left);
//                    } else {
//                        String leftStr = (String) left.getLeft();
//                        Column leftColumn = left.getIsLeftColumn() != null && left.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, leftStr) : null;
//                        analyzerResult.addRecomendation(database.getName(), leftColumn != null ? leftColumn.getTable().getName() : "", RECOMENDATION_17, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                } else {
//                    leftOk = checkNormalConditionResult(left, analyzerResult, list);
//                }
//
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(right.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(right.getType())) {
//                    
//                    // operator is "like" and value is started from "%" in this case we should ignore it
//                    boolean ignore = false;
//                    // operator is "like" and value is started from "%" in this case we should ignore it
//                    if (("like".equalsIgnoreCase(right.getOperand()) || "ilike".equalsIgnoreCase(right.getOperand()))) {
//                        if (right.getRight() != null && right.getRight().toString().matches("@\\d+") && right.getCommand() instanceof SelectCommand) {
//                            String value = ((SelectCommand)right.getCommand()).getString(right.getRight().toString());
//                            if (value != null && value.startsWith("'%")) {
//                                ignore = true;
//                            }
//                        }
//                    }
//                    
//                    if (!ignore) {
//                        list.add(right);
//                    } else {
//                        String leftStr = (String) right.getLeft();
//                        Column leftColumn = right.getIsLeftColumn() != null && right.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, leftStr) : null;
//                        analyzerResult.addRecomendation(database.getName(), leftColumn != null ? leftColumn.getTable().getName() : "", RECOMENDATION_17, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    }
//                } else {
//                    rightOk = checkNormalConditionResult(right, analyzerResult, list);
//                }
//
//                if (!leftOk || !rightOk) {
//                    return false;
//                }
//            } else if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(conditionResult.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(conditionResult.getType())) {
//                list.add(conditionResult);
//                
//                String left = (String) conditionResult.getLeft();
//                String right = (String) conditionResult.getRight();
//                
//                Column leftColumn = conditionResult.getIsLeftColumn() != null && conditionResult.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
//                Column rightColumn = conditionResult.getIsRightColumn() != null && conditionResult.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
//                
//                boolean leftIsFunction = conditionResult.getIsLeftFunction() != null ? conditionResult.getIsLeftFunction() : false;
//                boolean rightIsFunction = conditionResult.getIsRightFunction() != null ? conditionResult.getIsRightFunction() : false;
//                
//                
//                boolean isFunction = false;
//                
//                // Validation 1 - check for functions
//                if (leftIsFunction) {
//                    
//                    String tableNames = getFunctionTables(analyzerResult, left);
//                    if (tableNames != null) {
//                        analyzerResult.addRecomendation(database.getName(), tableNames, String.format(RECOMENDATION_1, convertHideFunctionality(analyzerResult, left)), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, left, true);
//                    }
//                    isFunction = true;
//                }
//                
//                if (rightIsFunction) {
//                    String tableNames = getFunctionTables(analyzerResult, right);
//                    if (tableNames != null) {
//                        analyzerResult.addRecomendation(database.getName(), tableNames, String.format(RECOMENDATION_1, convertHideFunctionality(analyzerResult, right)), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, right, true);
//                    }
//                    isFunction = true;
//                }
//                
//                if (!isFunction) {
//                                     
//                    if (leftColumn != null) {
//                        
//                        // if both columns
//                        if (rightColumn != null) {                            
//                            if (!leftColumn.getDataType().getName().equals(rightColumn.getDataType().getName())) {
//                                
//                                String tableName = leftColumn.getTable().getName();
//                                if (!tableName.equals(rightColumn.getTable().getName())) {
//                                    tableName += ", " + rightColumn.getTable().getName();
//                                }
//                                analyzerResult.addRecomendation(database.getName(), tableName, RECOMENDATION_2, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                            }
//                            
//                        } else {                                                
//                            checkColumnValue(analyzerResult, leftColumn, conditionResult, right);
//                        }
//                        
//                    } else if (rightColumn != null) {                        
//                        checkColumnValue(analyzerResult, rightColumn, conditionResult, left);
//                    }
//                }
//            } else {
//                return false;
//            }
//        }
//        
//        return true;
//    }
//    
//    private void collectFilterColumn(AnalyzerResult.ConditionResult conditionResult, AnalyzerResult analyzerResult, List<Column> list) {
//        collectFilterColumn(conditionResult, analyzerResult, list, false);
//    }
//    
//    private void collectFilterColumn(AnalyzerResult.ConditionResult conditionResult, AnalyzerResult analyzerResult, List<Column> list, boolean ignoreOr) {
//        if (conditionResult != null && !conditionResult.getIgnore()) {
//            if ("AND".equalsIgnoreCase(conditionResult.getOperand()) || (ignoreOr && "OR".equalsIgnoreCase(conditionResult.getOperand()))) {
//                AnalyzerResult.ConditionResult left = (AnalyzerResult.ConditionResult) conditionResult.getLeft();
//                AnalyzerResult.ConditionResult right = (AnalyzerResult.ConditionResult) conditionResult.getRight();
//
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(left.getType())) {
//                    if (left.getIsLeftColumn()) {                        
//                        Column c = AnalyzerResult.findColumn(analyzerResult, (String) left.getLeft());
//                        pushCardinalityColumn(c);
//                        if (c != null) {
//                            list.add(c);
//                        }
//                    } else if (left.getIsRightColumn()) {                        
//                        Column c = AnalyzerResult.findColumn(analyzerResult, (String) left.getRight());
//                        pushCardinalityColumn(c);
//                        if (c != null) {
//                            list.add(c);
//                        }
//                    }
//                } else {
//                    collectFilterColumn(left, analyzerResult, list, ignoreOr);
//                }
//                
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(right.getType())) {
//                    if (right.getIsLeftColumn()) {                        
//                        Column c = AnalyzerResult.findColumn(analyzerResult, (String) right.getLeft());
//                        pushCardinalityColumn(c);
//                        if (c != null) {
//                            list.add(c);
//                        }
//                    } else if (right.getIsRightColumn()) {                        
//                        Column c = AnalyzerResult.findColumn(analyzerResult, (String) right.getRight());
//                        pushCardinalityColumn(c);
//                        if (c != null) {
//                            list.add(c);
//                        }
//                    }
//                } else {
//                    collectFilterColumn(right, analyzerResult, list, ignoreOr);
//                }
//                
//            } else if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(conditionResult.getType())) {
//                if (conditionResult.getIsLeftColumn()) {                        
//                    Column c = AnalyzerResult.findColumn(analyzerResult, (String) conditionResult.getLeft());
//                    pushCardinalityColumn(c);
//                    if (c != null) {
//                        list.add(c);
//                    }
//                } else if (conditionResult.getIsRightColumn()) {                        
//                    Column c = AnalyzerResult.findColumn(analyzerResult, (String) conditionResult.getRight());
//                    pushCardinalityColumn(c);
//                    if (c != null) {
//                        list.add(c);
//                    }
//                }
//            }
//        }
//    }
//    
//    private boolean checkOrConditionResult(AnalyzerResult.ConditionResult conditionResult, AnalyzerResult analyzerResult, List<AnalyzerResult.ConditionResult> list) {
//        if (conditionResult != null && !conditionResult.getIgnore()) {
//            if ("OR".equalsIgnoreCase(conditionResult.getOperand())) {
//                AnalyzerResult.ConditionResult left = (AnalyzerResult.ConditionResult) conditionResult.getLeft();
//                AnalyzerResult.ConditionResult right = (AnalyzerResult.ConditionResult) conditionResult.getRight();
//
//                boolean leftOk = true;
//                boolean rightOk = true;
//
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(left.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(left.getType())) {
//                    list.add(left);
//                } else {
//                    leftOk = checkOrConditionResult(left, analyzerResult, list);
//                }
//
//                if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(right.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(right.getType())) {
//                    list.add(right);
//                } else {
//                    rightOk = checkOrConditionResult(right, analyzerResult, list);
//                }
//
//                if (!leftOk || !rightOk) {
//                    return false;
//                }
//            } else if (AnalyzerResult.CONDITION_TYPE__FILTER.equalsIgnoreCase(conditionResult.getType()) ||
//                        AnalyzerResult.CONDITION_TYPE__JOIN.equalsIgnoreCase(conditionResult.getType())) {
//                list.add(conditionResult);
//                
//                String left = (String) conditionResult.getLeft();
//                String right = (String) conditionResult.getRight();
//                
//                Column leftColumn = conditionResult.getIsLeftColumn() != null && conditionResult.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
//                Column rightColumn = conditionResult.getIsRightColumn() != null && conditionResult.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
//                
//                boolean leftIsFunction = conditionResult.getIsLeftFunction() != null ? conditionResult.getIsLeftFunction() : false;
//                boolean rightIsFunction = conditionResult.getIsRightFunction() != null ? conditionResult.getIsRightFunction() : false;
//                
//                
//                boolean isFunction = false;
//                
//                // Validation 1 - check for functions
//                if (leftIsFunction) {
//                    
//                    String tableNames = getFunctionTables(analyzerResult, left);
//                    if (tableNames != null) {
//                        analyzerResult.addRecomendation(database.getName(), tableNames, String.format(RECOMENDATION_1, convertHideFunctionality(analyzerResult, left)), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, left, true);
//                    }
//                    isFunction = true;
//                }
//                
//                if (rightIsFunction) {
//                    String tableNames = getFunctionTables(analyzerResult, right);
//                    if (tableNames != null) {
//                        analyzerResult.addRecomendation(database.getName(), tableNames, String.format(RECOMENDATION_1, convertHideFunctionality(analyzerResult, right)), null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        generatedAlways(analyzerResult, right, true);
//                    }
//                    isFunction = true;
//                }
//                
//                if (!isFunction) {
//                                     
//                    if (leftColumn != null) {
//                        
//                        // if both columns
//                        if (rightColumn != null) {                            
//                            if (!leftColumn.getDataType().getName().equalsIgnoreCase(rightColumn.getDataType().getName())) {
//                                
//                                String tableName = leftColumn.getTable().getName();
//                                if (!tableName.equalsIgnoreCase(rightColumn.getTable().getName())) {
//                                    tableName += ", " + rightColumn.getTable().getName();
//                                }
//                                analyzerResult.addRecomendation(database.getName(), tableName, RECOMENDATION_2, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                            }
//                            
//                        } else {                                                
//                            checkColumnValue(analyzerResult, leftColumn, conditionResult, right);
//                        }
//                        
//                    } else if (rightColumn != null) {                        
//                        checkColumnValue(analyzerResult, rightColumn, conditionResult, left);
//                    }
//                }
//
//            } else {
//                return false;
//            }
//        }
//        
//        return true;
//    }
//    
////    private void checkSimpleConditionResult(AnalyzerResult.ConditionResult conditionResult, AnalyzerResult analyzerResult) {
////        
////        switch (conditionResult.getType()) {
////            
////            // if condition - than check left and right part using recursion
////            case AnalyzerResult.CONDITION_TYPE__CONDITION:
////                                
////                checkSimpleConditionResult((AnalyzerResult.ConditionResult) conditionResult.getLeft(), analyzerResult);
////                checkSimpleConditionResult((AnalyzerResult.ConditionResult) conditionResult.getRight(), analyzerResult);
////                break;
////            
////            case AnalyzerResult.CONDITION_TYPE__FILTER:
////            case AnalyzerResult.CONDITION_TYPE__JOIN:
////                
////                String left = (String) conditionResult.getLeft();
////                String right = (String) conditionResult.getRight();
////                
////                Column leftColumn = conditionResult.getIsLeftColumn() != null && conditionResult.getIsLeftColumn() ? AnalyzerResult.findColumn(analyzerResult, left) : null;
////                Column rightColumn = conditionResult.getIsRightColumn() != null && conditionResult.getIsRightColumn() ? AnalyzerResult.findColumn(analyzerResult, right) : null;                
////                
////                boolean leftIsFunction = conditionResult.getIsLeftFunction() != null ? conditionResult.getIsLeftFunction() : false;
////                boolean rightIsFunction = conditionResult.getIsRightFunction() != null ? conditionResult.getIsRightFunction() : false;
////                
////                
////                boolean isFunction = false;
////                
////                // Validation 1 - check for functions
////                if (leftIsFunction) {
////                    
////                    String tableNames = getFunctionTables(analyzerResult, left);
////                    if (tableNames != null) {
////                        analyzerResult.addRecomendation(database, tableNames, String.format(RECOMENDATION_1, left), null, null, analyzerResult.getQuery());
////                    }
////                    isFunction = true;
////                }
////                
////                if (rightIsFunction) {
////                    String tableNames = getFunctionTables(analyzerResult, right);
////                    if (tableNames != null) {
////                        analyzerResult.addRecomendation(database, tableNames, String.format(RECOMENDATION_1, right), null, null, analyzerResult.getQuery());
////                    }
////                    isFunction = true;
////                }
////                
////                if (!isFunction) {
////                    
////                    boolean noRecomendations = true;                    
////                    if (leftColumn != null) {
////                        
////                        // if both columns
////                        if (rightColumn != null) {                            
////                            if (!leftColumn.getDataType().getName().equals(rightColumn.getDataType().getName())) {
////                                
////                                String tableName = leftColumn.getTable().getName();
////                                if (!tableName.equals(rightColumn.getTable().getName())) {
////                                    tableName += ", " + rightColumn.getTable().getName();
////                                }
////                                analyzerResult.addRecomendation(database, tableName, RECOMENDATION_2, null, null, analyzerResult.getQuery());
////                                noRecomendations = false;
////                            }
////                        } else {                                                
////                            boolean f = checkColumnValue(analyzerResult, leftColumn, conditionResult.getOperand(), right);
////                            if (f) {
////                                noRecomendations = false;
////                            }
////                        }
////                    } else if (rightColumn != null) {
////                        
////                        boolean f = checkColumnValue(analyzerResult, rightColumn, conditionResult.getOperand(), left);
////                        if (f) {
////                            noRecomendations = false;
////                        }
////                    }
////                    
////                    if (noRecomendations) {                    
////                        // check indexes
////                        if (leftColumn != null) {                            
////                            boolean hasIndexes = AnalyzerResult.checkIndexes(analyzerResult, leftColumn);
////                            if (!hasIndexes) {
////                                analyzerResult.addRecomendation(database, leftColumn.getTable().getName(), String.format(RECOMENDATION_3, leftColumn.getName()), String.format("idx__%s", leftColumn.getTable().getName()), String.format(SQL_1, leftColumn.getTable().getName(), leftColumn.getName(), leftColumn.getName()), analyzerResult.getQuery());
////                            }
////                        }
////
////                        if (rightColumn != null) {                            
////                            boolean hasIndexes = AnalyzerResult.checkIndexes(analyzerResult, rightColumn);
////                            if (!hasIndexes) {
////                                analyzerResult.addRecomendation(database, rightColumn.getTable().getName(), String.format(RECOMENDATION_3, rightColumn.getName()), String.format("idx__%s", rightColumn.getTable().getName()), String.format(SQL_1, rightColumn.getTable().getName(), rightColumn.getName(), rightColumn.getName()), analyzerResult.getQuery());
////                            }
////                        }
////                    }
////                }
////                break;
////        }
////    }
//
//    
//    private boolean checkColumnValue(AnalyzerResult analyzerResult, Column column, AnalyzerResult.ConditionResult conditionResult, String value) {
//        if (value != null && !value.isEmpty()) {
//            
//            String operand = conditionResult.getOperand();
//            if (!value.matches("!\\d+") && !"BETWEEN".equalsIgnoreCase(operand.trim())) {
//                if ((IS_OPERAND.equalsIgnoreCase(operand) || IS_NOT_OPERAND.equalsIgnoreCase(operand) || (isRange(conditionResult)) || isRangeSecondPriority(conditionResult)) && NULL_VALUE.equalsIgnoreCase(value)) {
//                    analyzerResult.addRecomendation(database.getName(), column.getTable().getName(), RECOMENDATION_12, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    return true;
//
//                } else if (isColumnTypeDigit(column.getDataType()) && !"1".equals(value)) {
//                    // if digit, than right must be a digit
//                    if (!NULL_VALUE.equalsIgnoreCase(value) && !value.matches("\\d+")) {
//                        analyzerResult.addRecomendation(database.getName(), column.getTable().getName(), RECOMENDATION_2, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        return true;
//                    }
//
//                } else if (isComumnTypeString(column.getDataType())) {
//                    // check string
//                    if (!NULL_VALUE.equalsIgnoreCase(value) && !value.startsWith("'") && !"1".equals(value) && !value.matches("@\\d+")) {
//                        analyzerResult.addRecomendation(database.getName(), column.getTable().getName(), RECOMENDATION_2, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        return true;
//                    }
//                    else if (LIKE_OPERAND.equalsIgnoreCase(operand) && value.startsWith(LIKE_PERCENT_VALUE)) {
//                        return true;
//                    }
//
//                } else if (isComumnTypeBoolean(column.getDataType())) {
//                    // check boolean
//                    if (!NULL_VALUE.equalsIgnoreCase(value) && 
//                            !FALSE_VALUE.equalsIgnoreCase(value) &&
//                            !TRUE_VALUE.equalsIgnoreCase(value) && !"1".equals(value)) {
//                        analyzerResult.addRecomendation(database.getName(), column.getTable().getName(), RECOMENDATION_2, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                        return true;
//                    }
//
//                } else if (isColumnTypeDateAndWrong(column.getDataType(), value) && !"1".equals(value)) {
//                    analyzerResult.addRecomendation(database.getName(), column.getTable().getName(), RECOMENDATION_2, null, null, analyzerResult.getQuery(), new ArrayList<>());
//                    return true;
//                }
//            }
//        }
//        
//        return false;
//    }
//    
//    private boolean isColumnTypeDateAndWrong(DataType type, String dateValue) {
//        if (!NULL_VALUE.equalsIgnoreCase(dateValue)) {
//            switch (type.getName().toUpperCase()) {
//                 case "DATETIME":
//                 case "TIMESTAMP":
//                     return !(dateValue.matches("'\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}'") || dateValue.matches("'\\d{4}-\\d{2}-\\d{2}'"));
//                 case "TIME":
//                     return !dateValue.matches("'\\d{2}:\\d{2}:\\d{2}'");
//                 case "DATE":
//                     return !dateValue.matches("'\\d{4}-\\d{2}-\\d{2}'");
//                 case "YEAR":
//                     return !dateValue.matches("\\d{4}");
//
//                 default:
//                     return false;
//            } 
//        }
//        return false;
//    }
//        
//    private boolean isComumnTypeBoolean(DataType type) {
//       switch (type.getName().toUpperCase()) {
//            case "BOOL":
//            case "BOOLEAN":
//                return true;
//                
//            default:
//                return false;
//        } 
//    }
//    
//    private boolean isComumnTypeString(DataType type) {
//       switch (type.getName().toUpperCase()) {
//            case "CHAR":
//            case "VARCHAR":
//            case "BINARY":
//            case "VARBINARY":
//            case "BLOB":
//            case "TEXT":
//            case "ENUM":
//            case "SET":
//            case "LONGTEXT":
//            case "LONGBLOB":
//            case "MEDIUMBLOB":
//            case "MEDIUMTEXT":
//            case "TINYBLOB":
//            case "TINYTEXT":
//                return true;
//                
//            default:
//                return false;
//        } 
//    }
//    
//    private boolean isComumnTypeCardinalitySubstring(DataType type) {
//       switch (type.getName().toUpperCase()) {
//            case "CHAR":
//            case "VARCHAR":
//            case "TINYTEXT":
//            case "TEXT":
//            case "MEDIUMTEXT":
//            case "LONGTEXT":
//                return true;
//                
//            default:
//                return false;
//        } 
//    }
//    
//    private boolean isComumnTypeStringWithLegth(DataType type) {
//       switch (type.getName().toUpperCase()) {
//            case "CHAR":
//            case "VARCHAR":
//            case "BLOB":
//            case "TEXT":
////            case "ENUM":
////            case "SET":
//            case "LONGTEXT":
//            case "LONGBLOB":
//            case "MEDIUMBLOB":
//            case "MEDIUMTEXT":
//            case "TINYBLOB":
//                return true;
//                
//            default:
//                return false;
//        } 
//    }
//    
//    private boolean isColumnTypeDigit(DataType type) {
//        switch (type.getName().toUpperCase()) {
//            case "BIGINT":
//            case "DECIMAL":
//            case "DOUBLE":
//            case "FLOAT":
//            case "INT":
//            case "MEDIUMINT":
//            case "NUMERIC":
//            case "REAL":
//            case "SMALLINT":
//            case "BIT":
//            case "TINYINT":
//                return true;
//                
//            default:
//                return false;
//        }
//    }
//        
//    
//    
//    private String convertNQuery_55(String query) {
//        
//        // solution for 5.5
//        Matcher m = PATTERN_55.matcher(query);
//        if (m.find()) {
//            int start = m.start();
//            int end = m.end();
//            
//            query = query.substring(0, start) + "" + query.substring(end);
//            
////            // find '( ... )' block            
////            int brachesDiff = 1;
////            for (int i = end + 1; i < query.length(); i++)
////            {
////                switch (query.charAt(i))
////                {
////                    case '(':
////                        brachesDiff++;
////                        break;
////                        
////                    case ')':
////                        brachesDiff--;
////                        break;
////                }
////                
////                if ()
////                
////                // we find end of select query
////                if (brachesDiff == 0)
////                {
////                    String t = query.substring(end, i + 1);
////                    while (t.startsWith("(") && t.endsWith(")")) {
////                        t = t.substring(1, t.length() - 1).trim();
////                    }
////                            
////                    query = query.substring(0, start) + "" + query.substring(end + 1);
////                    break;
////                }
////            }
//
//            return convertNQuery_55(query);
//        }
//        
//        return query;
//    }
//    
//    
//    private String convertNQuery_56(String query) {
//        
//        // solution for 5.6
//        Matcher m = PATTERN_56.matcher(query);
//        if (m.find()) {
//            int start = m.start();
//            int end = m.end();
//            
//            // find '( ... )' block            
//            int brachesDiff = 1;
//            for (int i = end + 1; i < query.length(); i++)
//            {
//                switch (query.charAt(i))
//                {
//                    case '(':
//                        brachesDiff++;
//                        break;
//                        
//                    case ')':
//                        brachesDiff--;
//                        break;
//                }
//                
//                // we find end of select query
//                if (brachesDiff == 0)
//                {
//                    query = query.substring(0, start) + "1 = 1" + query.substring(i + 1);
//                    break;
//                }
//            }
//
//            return convertNQuery_56(query);
//        }
//        
//        return query;
//    }
//    
//    private String convertNQuery_SubSelect(String query) {
//        
//        Matcher m = PATTERN_SUB_SELECT.matcher(query);
//        if (m.find()) {
//            int start = m.start();
//            int end = m.end();
//            
//            query = query.substring(0, start) + "" + query.substring(end + 1);
//            
//            return convertNQuery_SubSelect(query);
//        }
//        
//        return query;
//    }
//    
//    private Map<String, String> analyzeQuery(Database database, List<ExplainResult> explainResults, String query, String idPrefix, boolean needCheckWarnings) throws SQLException, QueryNeverReturnAnyRows {
//        
//        Map<String, String> queriesTypes = new HashMap<>();
//        
//        if (service == null) {
//            return queriesTypes;
//        }
//        
//        // step 2: Get the reconstarted query using Explain extended & show warnings and save it on V_NQuery
//        // step 3: Save the Explain Extended resuslt set in one object
//        
//        String multiQuery = "SET @old_max_error_count=@@max_error_count; SET max_error_count=10000;";
//        
//        int rsIndex = 2;
//        
////        Connection conn = service.getDB();
//        if (((MysqlDBService)service).getDatabase() == null || !database.getName().equals(((MysqlDBService)service).getDatabase().getName())) {
//            rsIndex++;
//            multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__CHANGE_DATABASE), database.getName());
//            if (!multiQuery.trim().endsWith(";")) {
//                multiQuery += ";";
//            }
//        }
////        Database old = ((MysqlDBService)service).changeDatabase(database);
//        
//        boolean needCalcRowsSum = false;
//        if (rowsSum == null) {
//            rowsSum = 0l;
//            needCalcRowsSum = true;
//        }
//
//        String source = query;
//        
//        if (!query.trim().toLowerCase().startsWith("explain")) {
//            if (service.getMySQLVersionMaj() > 5) {
//                query = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), query);
//            } else {
//                query = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN_EXTENDED), query);
//            }
//        }
//        
//        multiQuery += query;
//        if (!multiQuery.trim().endsWith(";")) {
//            multiQuery += ";";
//        }
//        
//        if (needCheckWarnings) {
//            multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_WARNINGS);
//            if (!multiQuery.trim().endsWith(";")) {
//                multiQuery += ";";
//            }
//        }
//        
//        multiQuery += "SET max_error_count=@old_max_error_count;";
//        
//        try {
//            QueryResult qr = new QueryResult();
//            service.execute(multiQuery, qr);
//            
//            // 0 - ignore USE
//            // 1 - explain
//            rowsMulty = 1l;
//
//            ResultSet rs = (ResultSet) qr.getResult().get(rsIndex);
//            while (rs.next()) {
//
//                Long id = getLongValue(rs, "id");
//                if (id > 0) {
//
//                    Long rows = getLongValue(rs, "rows");
//                    explainResults.add(
//                        new ExplainResult(
//                            id,  
//                            getStringValue(rs, "select_type"), 
//                            getStringValue(rs, "table"), 
//                            getStringValue(rs, "partitions"), 
//                            getStringValue(rs, "type"), 
//                            getStringValue(rs, "possible_keys"), 
//                            getStringValue(rs, "key"), 
//                            getLongValue(rs, "key_len"), 
//                            getStringValue(rs, "ref"), 
//                            rows, 
//                            getLongValue(rs, "filtered"), 
//                            getStringValue(rs, "extra")
//                        )
//                    );
//
//                    rowsMulty *= rows;
//
//                    if (needCalcRowsSum) {
//                        rowsSum += rows;
//                    }
//
//                    if (rowsLessThen10 && rows > 10) {
//                        rowsLessThen10 = false;
//                    }
//                }
//            }
//            
//            rs.close();
//                
//            // 2 - warning
//            if (needCheckWarnings) {
//                
//                rs = (ResultSet) qr.getResult().get(rsIndex + 1);
//                // call show warnings
//                warningsResults = new ArrayList<>();
//                while (rs.next()) {
////                    log.debug("level: " + rs.getString(1) + ", code: " + rs.getLong(2) + ", msg: " + rs.getString(3));
//                    warningsResults.add(
//                        new WarningsResult( 
//                            rs.getString(1),
//                            rs.getLong(2), 
//                            rs.getString(3)
//                        )
//                    );
//                }
//                
//                rs.close();
//                
//            }
//        } catch (SQLException th) {
//            if (th instanceof SQLException &&
//                    ((SQLException)th).getErrorCode() == 1191) {
//                // ignore them:
//                // error code 1191 - Message: Can't find FULLTEXT index matching the column list
//                log.error("Error", th);
//            } else {
//                throw th;
//            }
//        }
//        
//        
////        try (ResultSet rs = conn.prepareStatement(query).executeQuery()) {
////            if (rs != null) {
////                
////                rowsMulty = 1l;
////                
////                while (rs.next()) {
////                    
////                    Long id = getLongValue(rs, "id");
////                    if (id > 0) {
////                        
////                        Long rows = getLongValue(rs, "rows");
////                        explainResults.add(
////                            new ExplainResult(
////                                id,  
////                                getStringValue(rs, "select_type"), 
////                                getStringValue(rs, "table"), 
////                                getStringValue(rs, "partitions"), 
////                                getStringValue(rs, "type"), 
////                                getStringValue(rs, "possible_keys"), 
////                                getStringValue(rs, "key"), 
////                                getLongValue(rs, "key_len"), 
////                                getStringValue(rs, "ref"), 
////                                rows, 
////                                getLongValue(rs, "filtered"), 
////                                getStringValue(rs, "extra")
////                            )
////                        );
////                        
////                        rowsMulty *= rows;
////                        
////                        if (needCalcRowsSum) {
////                            rowsSum += rows;
////                        }
////                        
////                        if (rowsLessThen10 && rows > 10) {
////                            rowsLessThen10 = false;
////                        }
////                    }
////                }
////            }
////        } catch (Throwable th) {
////            if (th instanceof SQLException &&
////                    ((SQLException)th).getErrorCode() == 1191) {
////                // ignore them:
////                // error code 1191 - Message: Can't find FULLTEXT index matching the column list
////                log.error("Error", th);
////            } else {
////                throw th;
////            }
////        }
//                
////        if (needCheckWarnings) {
////            // call show warnings
////            try (ResultSet rs = conn.prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_WARNINGS)).executeQuery()) {
////                if (rs != null) {
////                    warningsResults = new ArrayList<>();
////                    while (rs.next()) {
////                        warningsResults.add(
////                            new WarningsResult( 
////                                rs.getString(1),
////                                rs.getLong(2), 
////                                rs.getString(3)
////                            )
////                        );
////                    }
////                }
////            }
////        }
//        
//        // step 4: Find out number of simple queries (Query degree ) for the complicated query: max(id) in explain extended result set
//        // step 5: Get the each simple query's Table degree: this mean count of each id in explain extended result set
//        if (!explainResults.isEmpty()) {
//            
//            for (ExplainResult er: explainResults) {
//                
//                // @Sudheer: sap report query is not returning recommendtions due to the reason we disable
////                if (er.getTable() == null || "no matching row in const table".equals(er.getExtra())) {
////                    throw new QueryNeverReturnAnyRows();
////                }
//                
//                Long id = er.getId();
//                if (id != null) {
//                    
//                    Integer intId = id.intValue();
//                    
//                    if (intId > maxId) {
//                        maxId = intId;
//                    }
//                
//                    
//                    Integer count = queriesDegree.get(idPrefix + intId);
//                    queriesDegree.put(idPrefix + intId, count == null ? 1 : ++count);
//
//                    queriesTypes.put(idPrefix + intId, er.getSelectType());                
//                }
//            }
//            
//            idIndex = 1;            
//            if (maxId > 1) {                
//                findData(source, idPrefix, queriesTypes, "");                                
//            } else {
//                addToQueries(idPrefix, queriesTypes, source, "");
//            }
//        }
//        
////        ((MysqlDBService)service).changeDatabase(old);
//        
//        return queriesTypes;
//    }
//    
//    int idIndex = 1;
//    private void findData(String query, String idPrefix, Map<String, String> queriesTypes, String parentKey) {
//        String[] splitUnion = SqlUtils.splitByUnion(query);
//
//        // split query by UNION/UNION ALL if exist
//        if (splitUnion.length > 1) {
//
//            String key = idPrefix + idIndex;
//            // first is id 1 query
//            queries.put(key, splitUnion[0].trim());
//            
//            List<String> keys = hierarchy.get(parentKey);
//            if (keys == null) {
//                keys = new ArrayList<>();
//                hierarchy.put(parentKey, keys);
//            }
//            keys.add(key);
//            
//            List<String> list = new ArrayList<>();
//            SqlUtils.findSubQueries(queries.get(key), list, false);
//            for (String s: list) {
//                idIndex++;
//                findData(s, idPrefix, queriesTypes, key);
//            }
//
//            // at this point index must be less then first unoinIndex on 1 
//            for (int i = 1; i < splitUnion.length; i++) {
//
//                // find unioin indexes
//                for (int j = idIndex + 1; j <= maxId; j++) {
//                    if ("UNION".equalsIgnoreCase(queriesTypes.get(idPrefix + j))) {
//                        idIndex = j;
//                        break;
//                    }
//                }
//                
//                findData(splitUnion[i].trim(), idPrefix, queriesTypes, parentKey);
//            }
//
//        } else {
//            // if no union - than its all 0 quer
//            addToQueries(idPrefix, queriesTypes, query, parentKey);
//        }
//    }
//    
//    private void addToQueries(String idPrefix, Map<String, String> queriesTypes, String query, String parentKey) {
//        
//        String key = idPrefix + idIndex;
//        queries.put(key, query);
//        
//        List<String> keys = hierarchy.get(parentKey);
//        if (keys == null) {
//            keys = new ArrayList<>();
//            hierarchy.put(parentKey, keys);
//        }
//        keys.add(key);
//        
//        List<String> list = new ArrayList<>();
//        SqlUtils.findSubQueries(queries.get(key), list, true);
//        for (String s: list) {
//            idIndex++;
//            
//            String[] splitUnion = SqlUtils.splitByUnion(s);
//            if (splitUnion.length > 1) {
//                findData(s, idPrefix, queriesTypes, key);
//            } else {
//                
//                String key0 = idPrefix + idIndex;
//                queries.put(key0, s);
//                
//                List<String> keys0 = hierarchy.get(key);
//                if (keys0 == null) {
//                    keys0 = new ArrayList<>();
//                    hierarchy.put(key, keys0);
//                }
//                keys0.add(key0);
//            }
//        }
//    }
//    
//    private String convertHideFunctionality(AnalyzerResult analyzerResult, String expression) {
//        while (expression.contains("@")) {
//            Matcher m = Pattern.compile("@\\d\\d").matcher(expression);
//            if (m.find()) {
//                int start = m.start();
//                int end = m.end();
//
//                expression = expression.substring(0, start) + ((SelectCommand)analyzerResult.getWhere().getCommand()).getString(m.group()) + expression.substring(end);
//
//            } else {
//                break;
//            }
//        }
//        
//        return expression;
//    }
//    
//    private void generatedAlways(AnalyzerResult analyzerResult, String expression, boolean function) {
////        if (service.getMySQLVersionMaj() < 5 || service.getMySQLVersionMaj() == 5 && service.getMySQLVersionMin() < 7) {
//            expression = expression.trim();
//            
//            expression = convertHideFunctionality(analyzerResult, expression);
//            
//            String[] columns = function ? getFunctionColumns(expression) : getExpressionColumns(expression);
//
//            String table = null;
//            String name = null;
//            // TODO its wrong for default value, but dont know at now what need
//            // for real we always must found some data type
//            String dataType = "INT";
//            int length = 0;
//            
//            if (columns != null && columns.length > 0) {
//                
//                Column col_for_gen = null;
//                for (String col: columns) {
//                    
//                    Column c = AnalyzerResult.findColumn(analyzerResult, col);                    
//                    if (c != null) {
//                        
//                        if (col_for_gen == null) {
//                            col_for_gen = c;
//                            name = col_for_gen.getName() + "_gen1";
//                            table = c.getTable().getName();
//                        }
//                        
//                        if (table == null) {
//                            table = c.getTable().getName();
//                        }
//                        
//                        dataType = c.getDataType().getName();
//                        if (c.getDataType().getLength() != null) {
//                            length += c.getDataType().getLength();
//                        }
//                    }
//                }
//                
//                
//                // TODO ??? this is all cant working, need discuss
//                if (dataType.equalsIgnoreCase("varchar") && length > 0) {
//                    dataType += "(" + length +  ")";
//                }
//                
//            } else {
//                name = "col_gen1";
//            }
//
//            int i = 2;
//            while (AnalyzerResult.findColumn(analyzerResult, name) != null) {
//                name = name.substring(0, name.length() - 2) + i;
//                i++;
//            }
//            
//            analyzerResult.addRecomendation(
//                database.getName(), 
//                table,
//                RECOMENDATION_32, 
//                "idx__" + name,
//                String.format(SQL_2, table, name, dataType, expression, name, name), 
//                analyzerResult.getQuery(), 
//                new ArrayList<>());
////        }
//    }
//    
//    public String getOptimizerQuery() {
//        return optimizerQuery;
//    }
//
//    public String getSmartMySQLRewrittenQuery() {
//        return smartMySQLRewrittenQuery;
//    }
//    
//    public List<AnalyzerResult> getAnalyzerResult() {
//        return analyzerResult;
//    }
//    
//    public List<ExplainResult> getvQueryExplainResults() {
//        return vQueryExplainResults;
//    }
//    
//    public List<ExplainResult> getVNQueryExplainResults() {
//        return vNQueryExplainResults;
//    }
//    
//    public int getMaxId() {
//        return maxId;
//    }
//    
//    class QueryNeverReturnAnyRows extends Exception {
//        
//    }
//    
//    static class TablePkAndUniqueStructure {
//        private boolean hasPk = false;
//        private List<Column> pkColumns = new ArrayList<>();
//        
//        private boolean hasUnique = false;
//        private Map<String, List<Column>> uniques = new HashMap<>();
//        
//        private DBTable table;
//        
//        private TablePkAndUniqueStructure(DBTable table) {
//            this.table = table;
//        }
//        
//        public static TablePkAndUniqueStructure create(DBTable table) {
//            TablePkAndUniqueStructure s = new TablePkAndUniqueStructure(table);
//            if (table.getIndexes() != null) {
//                for (Index idx: table.getIndexes()) {
//                    if (idx.getType() == IndexType.PRIMARY) {
//                        if (idx.getColumns() != null && !idx.getColumns().isEmpty()) {
//                            s.hasPk = true;
//                            s.pkColumns.addAll(idx.getColumns());
//                        }
//                    } else if (idx.getType() == IndexType.UNIQUE_TEXT) {
//                        if (idx.getColumns() != null && !idx.getColumns().isEmpty()) {
//                            s.hasUnique = true;
//                            s.uniques.put(idx.getName(), new ArrayList<>(idx.getColumns()));
//                        }
//                    }
//                }
//            }
//            
//            return s;
//        }
//        
//        // must have same positions, so we always check if 0 is our column
//        public void removeColumn(Column c) {
//            if (hasPk && !pkColumns.isEmpty() && pkColumns.get(0).getName().equals(c.getName())) {
//                pkColumns.remove(0);
//            }
//
//            if (hasUnique) {
//
//                for (List<Column> list: uniques.values()) {
//                    if (!list.isEmpty() && list.get(0).getName().equals(c.getName())) {
//                        list.remove(0);
//                    }
//                }
//            }
//        }
//        
//        public boolean needIgnorePk() {
//            return hasPk && pkColumns.isEmpty();
//        }
//        
//        public List<Column> pkColumns() {
//            List<Column> result = new ArrayList<>();
//            if (table.getIndexes() != null) {
//                for (Index idx: table.getIndexes()) {
//                    if (idx.getType() == IndexType.PRIMARY) {
//                        result.addAll(idx.getColumns());
//                    }
//                }
//            }
//            
//            return result;
//        }
//        
//        public Map<String, List<Column>> uqColumns() {
//            Map<String, List<Column>> result = new HashMap<>();
//            if (table.getIndexes() != null) {
//                for (Index idx: table.getIndexes()) {
//                    if (idx.getType() == IndexType.UNIQUE_TEXT && uniques.get(idx.getName()).isEmpty()) {
//                        if (idx.getColumns() != null && !idx.getColumns().isEmpty()) {
//                            result.put(idx.getName(), new ArrayList<>(idx.getColumns()));
//                        }
//                    }
//                }
//            }
//            
//            return result;
//        }
//        
//        public boolean needIgnoreUq() {
//            if (hasUnique) {
//                for (List<Column> list: uniques.values()) {
//                    if (list.isEmpty()) {
//                        return true;
//                    }
//                }
//            }
//            
//            return false;
//        }
//                
//        public boolean needIgnore() {
//            
//            boolean needIgnore = needIgnorePk();
//            
//            if (!needIgnore) {
//                needIgnore = needIgnoreUq();
//            }
//            
//            return needIgnore;
//        }
//    }
}
