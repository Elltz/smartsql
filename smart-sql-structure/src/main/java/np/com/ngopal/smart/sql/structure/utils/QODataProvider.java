package np.com.ngopal.smart.sql.structure.utils;

import java.sql.SQLException;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;
import np.com.ngopal.smart.sql.model.QARecommendation;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.queryoptimizer.providers.DataProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class QODataProvider {

    public static DataProvider create(DBService service, ColumnCardinalityService columnCardinalityService, QARecommendationService qars, QAPerformanceTuningService performanceTuningService, ConnectionParams params, boolean hasCredit) {
        
        return new DataProvider() {
            
            @Override
            public Object execute(String sql) throws SQLException {
                return service.execute(sql);
            }

            @Override
            public Object execute(String sql, QueryResult qr) throws SQLException {
                return service.execute(sql, qr);
            }

            @Override
            public String getUsedDatabase() {
                return ((MysqlDBService)service).getDatabase() != null ? ((MysqlDBService)service).getDatabase().getName() : null;
            }

            @Override
            public DBTable getCachedTable(String database, String table) {
                return DatabaseCache.getInstance().getCopyOfTable(params.cacheKey(), database, table);
            }

            @Override
            public Database getCachedDatabase(String database) {
                return DatabaseCache.getInstance().getDatabase(params.cacheKey(), database);
            }

            @Override
            public boolean canExplainExtended() {
                return service.getDbSQLVersionMaj() <= 5;
            }

            @Override
            public boolean hasCredits() {
                return hasCredit;
            }
            
            @Override
            public Long getColumnCardinality(String database, String table, String column) {
                return columnCardinalityService.getColumnCardinality(params.cacheKey(), database, table, column);
            }

            @Override
            public void saveCardinality(String database, String table, String column, Long cardinality) {
                columnCardinalityService.insert(params.cacheKey(), database, table, column, cardinality);
            }

            @Override
            public void removeRecommendations(String database, String query) {
                qars.removeRecommendations(params.getId(), database, query);
            }

            @Override
            public void saveRecommendation(String database, String query, String table, String recommendation, String index, String alterQuery, String smartSqlQuery, String optimizedQuery, boolean emptyWithOr, boolean hasAnalyzerResults, String qoRecommentaion, String qoAlterQuery, Long rows, Long rowsMulti) {
                qars.save(
                    new QARecommendation(
                        params.getId(), 
                        database, 
                        query, 
                        table, 

                        recommendation, 
                        index,
                        alterQuery, 

                        smartSqlQuery,
                        optimizedQuery,
                        emptyWithOr,
                        hasAnalyzerResults,

                        qoRecommentaion, 
                        qoAlterQuery,
                        rows,
                        rowsMulti
                    )
                );
            }

            @Override
            public QAPerformanceTuning savePerformanceTuning(String database, String table, String query, String recommendation, String sqlCommand, Long rows) {
                QAPerformanceTuning pt = performanceTuningService.saveStatistic(
                    params.getId(), 
                    database, 
                    table, 
                    query, 
                    recommendation, 
                    sqlCommand, 
                    rows,
                    hasCredits()
                );
                
                return pt;
            }

            @Override
            public void logError(String message, Throwable th) {
                log.error(message, th);
            }

            @Override
            public void logDebug(String message) {
                log.debug(message);
            }
            
        };
    }
}
