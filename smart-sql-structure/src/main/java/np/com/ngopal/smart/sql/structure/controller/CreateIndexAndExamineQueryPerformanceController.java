package np.com.ngopal.smart.sql.structure.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CreateIndexAndExamineQueryPerformanceController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    private boolean agree = false;
    
    @FXML
    public void agreeAction(ActionEvent event) {        
        agree = true;
        dialog.close();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        dialog.close();
    }
    
    @FXML
    public void linkAction(ActionEvent event) {
        URLUtils.openUrl("https://dev.mysql.com/doc/refman/5.7/en/mysql-indexes.html");
    }
    
    
    public boolean isAgree() {
        return agree;
    }

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    private Stage dialog;

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        
    }
    
    public void init(Window win) {        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(550);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Create index an examine query performance");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();        
    }
    
}
