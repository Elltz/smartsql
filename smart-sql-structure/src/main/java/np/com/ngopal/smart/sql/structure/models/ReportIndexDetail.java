
package np.com.ngopal.smart.sql.structure.models;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReportIndexDetail implements FilterableData {
    
    private final SimpleStringProperty type;
    private final SimpleStringProperty database;
    private final SimpleStringProperty tableName;
    private final SimpleStringProperty indexName;
    private final SimpleObjectProperty countStar;
    private final SimpleObjectProperty sumTimerWait;
    private final SimpleObjectProperty minTimerWait;
    private final SimpleObjectProperty avgTimerWait;
    private final SimpleObjectProperty maxTimerWait;
    private final SimpleObjectProperty countRead;
    private final SimpleObjectProperty sumTimerRead;
    private final SimpleObjectProperty minTimerRead;
    private final SimpleObjectProperty avgTimerRead;
    private final SimpleObjectProperty maxTimerRead;
    private final SimpleObjectProperty countWrite;
    private final SimpleObjectProperty sumTimerWrite;
    private final SimpleObjectProperty minTimerWrite;
    private final SimpleObjectProperty avgTimerWrite;
    private final SimpleObjectProperty maxTimerWrite;
    private final SimpleObjectProperty countFetch;
    private final SimpleObjectProperty sumTimerFetch;
    private final SimpleObjectProperty minTimerFetch;
    private final SimpleObjectProperty avgTimerFetch;
    private final SimpleObjectProperty maxTimerFetch;
    private final SimpleObjectProperty countInsert;
    private final SimpleObjectProperty sumTimerInsert;
    private final SimpleObjectProperty minTimerInsert;
    private final SimpleObjectProperty avgTimerInsert;
    private final SimpleObjectProperty maxTimerInsert;
    private final SimpleObjectProperty countUpdate;
    private final SimpleObjectProperty sumTimerUpdate;
    private final SimpleObjectProperty minTimerUpdate;
    private final SimpleObjectProperty avgTimerUpdate;
    private final SimpleObjectProperty maxTimerUpdate;
    private final SimpleObjectProperty countDelete;
    private final SimpleObjectProperty sumTimerDelete;
    private final SimpleObjectProperty minTimerDelete;
    private final SimpleObjectProperty avgTimerDelete;
    private final SimpleObjectProperty maxTimerDelete;
 
    public ReportIndexDetail(
            String type,
            String database,
            String tableName,
            String indexName,
            
            Long countStar,
            Long sumTimerWait,
            Long minTimerWait,
            Long avgTimerWait,
            Long maxTimerWait,
            
            Long countRead,
            Long sumTimerRead,
            Long minTimerRead,
            Long avgTimerRead,
            Long maxTimerRead,
            
            Long countWrite,
            Long sumTimerWrite,
            Long minTimerWrite,
            Long avgTimerWrite,
            Long maxTimerWrite,
            
            Long countFetch,
            Long sumTimerFetch,
            Long minTimerFetch,
            Long avgTimerFetch,
            Long maxTimerFetch,
            
            Long countInsert,
            Long sumTimerInsert,
            Long minTimerInsert,
            Long avgTimerInsert,
            Long maxTimerInsert,
            
            Long countUpdate,
            Long sumTimerUpdate,
            Long minTimerUpdate,
            Long avgTimerUpdate,
            Long maxTimerUpdate,
            
            Long countDelete,
            Long sumTimerDelete,
            Long minTimerDelete,
            Long avgTimerDelete,
            Long maxTimerDeletee) {
        
        this.type = new SimpleStringProperty(type);
        this.database = new SimpleStringProperty(database);
        this.tableName = new SimpleStringProperty(tableName);
        this.indexName = new SimpleStringProperty(indexName);
        
        this.countStar = new SimpleObjectProperty(countStar);
        this.sumTimerWait = new SimpleObjectProperty(sumTimerWait);
        this.minTimerWait = new SimpleObjectProperty(minTimerWait);
        this.avgTimerWait = new SimpleObjectProperty(avgTimerWait);
        this.maxTimerWait = new SimpleObjectProperty(maxTimerWait);
        
        this.countRead = new SimpleObjectProperty(countRead);
        this.sumTimerRead = new SimpleObjectProperty(sumTimerRead);
        this.minTimerRead = new SimpleObjectProperty(minTimerRead);
        this.avgTimerRead = new SimpleObjectProperty(avgTimerRead);
        this.maxTimerRead = new SimpleObjectProperty(maxTimerRead);
        
        this.countWrite = new SimpleObjectProperty(countWrite);
        this.sumTimerWrite = new SimpleObjectProperty(sumTimerWrite);
        this.minTimerWrite = new SimpleObjectProperty(minTimerWrite);
        this.avgTimerWrite = new SimpleObjectProperty(avgTimerWrite);
        this.maxTimerWrite = new SimpleObjectProperty(maxTimerWrite);
        
        this.countFetch = new SimpleObjectProperty(countFetch);
        this.sumTimerFetch = new SimpleObjectProperty(sumTimerFetch);
        this.minTimerFetch = new SimpleObjectProperty(minTimerFetch);
        this.avgTimerFetch = new SimpleObjectProperty(avgTimerFetch);
        this.maxTimerFetch = new SimpleObjectProperty(maxTimerFetch);
        
        this.countInsert = new SimpleObjectProperty(countInsert);
        this.sumTimerInsert = new SimpleObjectProperty(sumTimerInsert);
        this.minTimerInsert = new SimpleObjectProperty(minTimerInsert);
        this.avgTimerInsert = new SimpleObjectProperty(avgTimerInsert);
        this.maxTimerInsert = new SimpleObjectProperty(maxTimerInsert);
        
        this.countUpdate = new SimpleObjectProperty(countUpdate);
        this.sumTimerUpdate = new SimpleObjectProperty(sumTimerUpdate);
        this.minTimerUpdate = new SimpleObjectProperty(minTimerUpdate);
        this.avgTimerUpdate = new SimpleObjectProperty(avgTimerUpdate);
        this.maxTimerUpdate = new SimpleObjectProperty(maxTimerUpdate);
        
        this.countDelete = new SimpleObjectProperty(countDelete);
        this.sumTimerDelete = new SimpleObjectProperty(sumTimerDelete);
        this.minTimerDelete = new SimpleObjectProperty(minTimerDelete);
        this.avgTimerDelete = new SimpleObjectProperty(avgTimerDelete);
        this.maxTimerDelete = new SimpleObjectProperty(maxTimerDeletee);
    }

    public String getType() {
        return type.get();
    }
    
    public String getDatabase() {
        return database.get();
    }

    public String getTableName() {
        return tableName.get();
    }

    public String getIndexName() {
        return indexName.get();
    }

    public Long getCountStar() {
        return (Long) countStar.get();
    }

    public Long getSumTimerWait() {
        return (Long) sumTimerWait.get();
    }
    
    public Long getMinTimerWait() {
        return (Long) minTimerWait.get();
    }
    
    public Long getAvgTimerWait() {
        return (Long) avgTimerWait.get();
    }
    
    public Long getMaxTimerWait() {
        return (Long) maxTimerWait.get();
    }
    
    public Long getCountRead() {
        return (Long) countRead.get();
    }

    public Long getSumTimerRead() {
        return (Long) sumTimerRead.get();
    }
    
    public Long getMinTimerRead() {
        return (Long) minTimerRead.get();
    }
    
    public Long getAvgTimerRead() {
        return (Long) avgTimerRead.get();
    }
    
    public Long getMaxTimerRead() {
        return (Long) maxTimerRead.get();
    }
    
    public Long getCountWrite() {
        return (Long) countWrite.get();
    }

    public Long getSumTimerWrite() {
        return (Long) sumTimerWrite.get();
    }
    
    public Long getMinTimerWrite() {
        return (Long) minTimerWrite.get();
    }
    
    public Long getAvgTimerWrite() {
        return (Long) avgTimerWrite.get();
    }
    
    public Long getMaxTimerWrite() {
        return (Long) maxTimerWrite.get();
    }
 
    public Long getCountFetch() {
        return (Long) countFetch.get();
    }

    public Long getSumTimerFetch() {
        return (Long) sumTimerFetch.get();
    }
    
    public Long getMinTimerFetch() {
        return (Long) minTimerFetch.get();
    }
    
    public Long getAvgTimerFetch() {
        return (Long) avgTimerFetch.get();
    }
    
    public Long getMaxTimerFetch() {
        return (Long) maxTimerFetch.get();
    }
    
    public Long getCountInsert() {
        return (Long) countInsert.get();
    }

    public Long getSumTimerInsert() {
        return (Long) sumTimerInsert.get();
    }
    
    public Long getMinTimerInsert() {
        return (Long) minTimerInsert.get();
    }
    
    public Long getAvgTimerInsert() {
        return (Long) avgTimerInsert.get();
    }
    
    public Long getMaxTimerInsert() {
        return (Long) maxTimerInsert.get();
    }
    
    public Long getCountUpdate() {
        return (Long) countUpdate.get();
    }

    public Long getSumTimerUpdate() {
        return (Long) sumTimerUpdate.get();
    }
    
    public Long getMinTimerUpdate() {
        return (Long) minTimerUpdate.get();
    }
    
    public Long getAvgTimerUpdate() {
        return (Long) avgTimerUpdate.get();
    }
    
    public Long getMaxTimerUpdate() {
        return (Long) maxTimerUpdate.get();
    }
    
    public Long getCountDelete() {
        return (Long) countDelete.get();
    }

    public Long getSumTimerDelete() {
        return (Long) sumTimerDelete.get();
    }
    
    public Long getMinTimerDelete() {
        return (Long) minTimerDelete.get();
    }
    
    public Long getAvgTimerDelete() {
        return (Long) avgTimerDelete.get();
    }
    
    public Long getMaxTimerDelete() {
        return (Long) maxTimerDelete.get();
    }
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getType() != null ? getType() : "",
            getDatabase() != null ? getDatabase() : "", 
            getTableName() != null ? getTableName() : "",
            getIndexName() != null ? getIndexName() : "", 
            getCountStar() != null ? getCountStar().toString() : "",
            getSumTimerWait() != null ? getSumTimerWait().toString() : "",
            getMinTimerWait() != null ? getMinTimerWait().toString() : "",
            getAvgTimerWait() != null ? getAvgTimerWait().toString() : "",
            getMaxTimerWait() != null ? getMaxTimerWait().toString() : "",
            getCountRead() != null ? getCountRead().toString() : "",
            getSumTimerRead() != null ? getSumTimerRead().toString() : "",
            getMinTimerRead() != null ? getMinTimerRead().toString() : "",
            getAvgTimerRead() != null ? getAvgTimerRead().toString() : "",
            getMaxTimerRead() != null ? getMaxTimerRead().toString() : "",
            getCountWrite() != null ? getCountWrite().toString() : "",
            getSumTimerWrite() != null ? getSumTimerWrite().toString() : "",
            getMinTimerWrite() != null ? getMinTimerWrite().toString() : "",
            getAvgTimerWrite() != null ? getAvgTimerWrite().toString() : "",
            getMaxTimerWrite() != null ? getMaxTimerWrite().toString() : "",
            getCountFetch() != null ? getCountFetch().toString() : "",
            getSumTimerFetch() != null ? getSumTimerFetch().toString() : "",
            getMinTimerFetch() != null ? getMinTimerFetch().toString() : "",
            getAvgTimerFetch() != null ? getAvgTimerFetch().toString() : "",
            getMaxTimerFetch() != null ? getMaxTimerFetch().toString() : "",
            getCountInsert() != null ? getCountInsert().toString() : "",
            getSumTimerInsert() != null ? getSumTimerInsert().toString() : "",
            getMinTimerInsert() != null ? getMinTimerInsert().toString() : "",
            getAvgTimerInsert() != null ? getAvgTimerInsert().toString() : "",
            getMaxTimerInsert() != null ? getMaxTimerInsert().toString() : "",
            getCountUpdate() != null ? getCountUpdate().toString() : "",
            getSumTimerUpdate() != null ? getSumTimerUpdate().toString() : "",
            getMinTimerUpdate() != null ? getMinTimerUpdate().toString() : "",
            getAvgTimerUpdate() != null ? getAvgTimerUpdate().toString() : "",
            getMaxTimerUpdate() != null ? getMaxTimerUpdate().toString() : "",
            getCountDelete() != null ? getCountDelete().toString() : "",
            getSumTimerDelete() != null ? getSumTimerDelete().toString() : "",
            getMinTimerDelete() != null ? getMinTimerDelete().toString() : "",
            getAvgTimerDelete() != null ? getAvgTimerDelete().toString() : "",
            getMaxTimerDelete() != null ? getMaxTimerDelete().toString() : "",
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getType(),
            getDatabase(),
            getTableName(),
            getIndexName(),
            getCountStar(),
            getSumTimerWait(),
            getMinTimerWait(),
            getAvgTimerWait(),
            getMaxTimerWait(),
            getCountRead(),
            getSumTimerRead(),
            getMinTimerRead(),
            getAvgTimerRead(),
            getMaxTimerRead(),
            getCountWrite(),
            getSumTimerWrite(),
            getMinTimerWrite(),
            getAvgTimerWrite(),
            getMaxTimerWrite(),
            getCountFetch(),
            getSumTimerFetch(),
            getMinTimerFetch(),
            getAvgTimerFetch(),
            getMaxTimerFetch(),
            getCountInsert(),
            getSumTimerInsert(),
            getMinTimerInsert(),
            getAvgTimerInsert(),
            getMaxTimerInsert(),
            getCountUpdate(),
            getSumTimerUpdate(),
            getMinTimerUpdate(),
            getAvgTimerUpdate(),
            getMaxTimerUpdate(),
            getCountDelete(),
            getSumTimerDelete(),
            getMinTimerDelete(),
            getAvgTimerDelete(),
            getMaxTimerDelete()
        };
    }
}