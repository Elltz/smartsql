package np.com.ngopal.smart.sql.structure.components;

import com.sun.javafx.scene.control.TableColumnComparatorBase;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.models.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 * @param <T>
 */
@Slf4j
public class PaginatorFilterableTableView<T extends FilterableData> extends BorderPane {

    //sir Sudheer wanted black theme so i changed constructor
    private final FilterableTableView tableView = new FilterableTableView(true, false) {
        @Override
        protected void filterChanged() {
            dataChanged();
        }

        @Override
        protected void acceptFilter() {
            // do nothing
        }
        
    };
    
    private final Button homeButton = new Button();
    private final Button prevButton = new Button();
    private final Button nextButton = new Button();
    private final Button endButton = new Button();
    
    private final Label totalLabel = new Label("1 of 1") ;
    
    private int currentPage = 0;
    private int rowsPerPage = 30;
    
    private ObservableList allData;
        
    private FilteredList<T> filterList;
    private SortedList<T> sortedList;
    
    private Predicate<? super T> predicate;
    
    private HBox paginatorBox;
    
    private List<TableColumn> oldComparatorColumns;
    private Map<TableColumn, TableColumn.SortType> oldComparatorSorTypes = new HashMap<>();
    
    public PaginatorFilterableTableView() {
        
        getStyleClass().add("paginatorTableView");
        
        tableView.setStyle("-fx-font-size: 11;");
        paginatorBox = new HBox(homeButton, prevButton, totalLabel, nextButton, endButton);
        paginatorBox.getStyleClass().add("paginatorButtonHBox");
        
        homeButton.getStyleClass().add("paginatorHomeButton");
        homeButton.setFocusTraversable(false);
        homeButton.setDisable(true);
        homeButton.setOnAction((ActionEvent event) -> {
            currentPage = 0;
            dataChanged();
        });
        
        prevButton.getStyleClass().add("paginatorPrevButton");
        prevButton.setFocusTraversable(false);
        prevButton.setDisable(true);
        prevButton.setOnAction((ActionEvent event) -> {
            currentPage--;
            dataChanged();
        });
                
        totalLabel.getStyleClass().add("paginatorTotalLabel");        
        
        nextButton.getStyleClass().add("paginatorNextButton");
        nextButton.setFocusTraversable(false);
        nextButton.setDisable(true);
        nextButton.setOnAction((ActionEvent event) -> {
            currentPage++;
            dataChanged();
        });
        
        endButton.getStyleClass().add("paginatorEndButton");
        endButton.setFocusTraversable(false);
        endButton.setDisable(true);
        endButton.setOnAction((ActionEvent event) -> {
            
            int size = filterList.size() / rowsPerPage + (filterList.size() % rowsPerPage > 0 || filterList.isEmpty() ? 1 : 0);
            currentPage = size - 1;
            
            dataChanged();
        });
        
        setTop(paginatorBox);
        
        setCenter(tableView);
        
        tableView.getTable().comparatorProperty().addListener((Observable observable) -> {
            TableColumnComparatorBase tccb = (TableColumnComparatorBase)((ReadOnlyObjectProperty)observable).getValue();
            if (tccb != null) {
                boolean need = false;
                List<TableColumn> newColumns = tccb.getColumns();
                if (oldComparatorColumns != null && newColumns.size() == oldComparatorColumns.size()) {
                    for (int i = 0; i < newColumns.size(); i++) {
                        TableColumn c0 = newColumns.get(i);
                        TableColumn c1 = oldComparatorColumns.get(i);
                        if (!c0.equals(c1) || c0.getSortType() != oldComparatorSorTypes.get(c0)) {
                            need = true;
                            break;
                        }
                    }
                } else {
                    need = true;
                }
                
                if (oldComparatorColumns != null) {
                    oldComparatorColumns.clear();
                }
                oldComparatorColumns = new ArrayList<>(newColumns);

                oldComparatorSorTypes.clear();
                for (TableColumn tc: newColumns) {
                    oldComparatorSorTypes.put(tc, tc.getSortType());
                }
                
                if (need) {
                    setItems(allData);
                }
            } else {
                if (oldComparatorColumns != null) {
                    oldComparatorColumns = null;
                    oldComparatorSorTypes.clear();
                    
                    setItems(allData);
                }
            }
        });
        
        setPredicate((T t) -> {
            return tableView.acceptFilter(t);
        });
    }
    
    public HBox removePaginator() {
        setTop(null);
        return paginatorBox;
    }
    
    public FilterableTableView getTableView() {
        return tableView;
    }
    
    public void dataChanged() {
        if (filterList != null) {
            filterList.setPredicate(null);
            filterList.setPredicate(predicate);

            List<T> list = sortedList;
            if (!sortedList.isEmpty()) {

                int start = currentPage * rowsPerPage;
                int finish = currentPage * rowsPerPage + rowsPerPage;
                if (finish > sortedList.size()) {
                    finish = sortedList.size();
                }

                list = sortedList.subList(start, finish);
            }   

            tableView.addAndRemoveItems(new ArrayList<>(), new ArrayList<>(tableView.getSourceData()));
            tableView.addItems((List<? extends FilterableData>) list);


            int size = filterList.size() / rowsPerPage + (filterList.size() % rowsPerPage > 0 || filterList.isEmpty() ? 1 : 0);

            homeButton.setDisable(currentPage <= 0);
            prevButton.setDisable(currentPage <= 0);
            nextButton.setDisable(currentPage >= size - 1);
            endButton.setDisable(currentPage >= size - 1);

            totalLabel.setText((currentPage + 1) + " of " + size) ;
        }
    }
    
    
    private void setPredicate(Predicate<? super T> predicate) {
        this.predicate = predicate;
        
        currentPage = 0;
        dataChanged();
    }
    
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
        
        dataChanged();
    }
    
    public void scrollTo(T row) {
        tableView.getTable().scrollTo(row);
    }
    
    public ObservableList<TableColumn<T,?>> getColumns() {
        return tableView.getTable().getColumns();
    }
    
    public ReadOnlyObjectProperty<Comparator<T>> comparatorProperty() {
        return tableView.getTable().comparatorProperty();
    }
    
    public ObservableList<T> getItems() {
        return allData;
    }
    
    public ObservableList<T> getFilteredItems() {
        return filterList;
    }
    
    public synchronized void setItems(ObservableList<T> list) {
        if (list != null) {
            if (sortedList != null) {
                sortedList.comparatorProperty().unbind();
            }

            allData = FXCollections.observableArrayList(list);
//            ((SortedList)allData).comparatorProperty().bind(tableView.getTable().comparatorProperty());
//            if (allData != null) {

    //            allData.addListener((ListChangeListener.Change c) -> {
    //                dataChanged();
    //            });

            filterList = new FilteredList<>(allData != null ? allData : FXCollections.observableArrayList());
            sortedList = new SortedList<>(filterList);

            sortedList.comparatorProperty().bind(tableView.getTable().comparatorProperty());
//            }

            try {
                dataChanged();
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            }
        }
    }
    
    public synchronized void addItems(List<T> list) {
        if (list != null) {
            if (sortedList != null) {
                sortedList.comparatorProperty().unbind();
            }

            if (allData == null || allData.isEmpty()) {
                allData = FXCollections.observableArrayList(list);

                filterList = new FilteredList<>(allData != null ? allData : FXCollections.observableArrayList());
                sortedList = new SortedList<>(filterList);
            } else {
                allData.addAll(list);
            }

            sortedList.comparatorProperty().bind(tableView.getTable().comparatorProperty());

            try {
                dataChanged();
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            }
        }
    }
    
    public TableView.TableViewSelectionModel<T> getSelectionModel() {
        return tableView.getTable().getSelectionModel();
    }
}
