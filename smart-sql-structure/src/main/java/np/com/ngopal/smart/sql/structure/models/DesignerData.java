package np.com.ngopal.smart.sql.structure.models;

import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface DesignerData {

    public SimpleStringProperty uuid();
    
    public SimpleStringProperty name();
    public SimpleStringProperty description();
            
    public SimpleDoubleProperty x();
    public SimpleDoubleProperty y();
    
    public SimpleDoubleProperty width();
    public SimpleDoubleProperty height();
    
    public DesignerData copyData();
    
    public List<DesignerProperty> getProperties();
    
    public void setParent(DesignerErrDiagram parent);
}
