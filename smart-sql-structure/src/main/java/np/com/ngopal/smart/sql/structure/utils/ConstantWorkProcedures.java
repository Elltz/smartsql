package np.com.ngopal.smart.sql.structure.utils;

import java.util.ArrayList;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ConstantWorkProcedures<T> extends WorkProc<T> {
    
    public static ArrayList<ConstantWorkProcedures> cWorkloads = new ArrayList();
    protected static long constantWPStartTime = 0l;

    private boolean inForce;

    @Override
    public void updateUI() {
        inForce = false;        
    }

    @Override
    public void run() {
//        System.out.println("Now Active WorkProc = " +workingProcedure);
    }
    
    public void lastVerifier(){        
        //controller.addWorkToBackground(this);
    }

    public ConstantWorkProcedures() {
        this(null);
    }

    public ConstantWorkProcedures(T t) {
        super(t);
//        if (controller == null) {
//            controller = Guice.createInjector(new UIModule(MainUI.isProductionBuildType())).getInstance(MainController.class);
            constantWPStartTime = System.currentTimeMillis();
//        }
        cWorkloads.add(this);
    }

    public boolean isInForce() {
        return inForce;
    }

    protected void startInitiation() { 
    }
}
