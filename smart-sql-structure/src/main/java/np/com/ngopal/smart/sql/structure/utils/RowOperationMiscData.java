/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.utils;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class RowOperationMiscData {
    private final SimpleStringProperty queriesQueued;
        private final SimpleStringProperty queriesInside;
        private final SimpleStringProperty rdViews;
        private final SimpleStringProperty mainThreadState;
        
        public RowOperationMiscData() {
            this.queriesQueued = new SimpleStringProperty();
            this.queriesInside = new SimpleStringProperty();
            this.rdViews = new SimpleStringProperty();
            this.mainThreadState = new SimpleStringProperty();
        }
        
        public SimpleStringProperty queriesQueued() {
            return queriesQueued;
        }
        
        public SimpleStringProperty queriesInside() {
            return queriesInside;
        }
        
        public SimpleStringProperty rdViews() {
            return rdViews;
        }
        
        public SimpleStringProperty mainThreadState() {
            return mainThreadState;
        }
}
