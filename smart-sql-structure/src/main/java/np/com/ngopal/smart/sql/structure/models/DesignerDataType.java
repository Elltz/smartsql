package np.com.ngopal.smart.sql.structure.models;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerDataType {
        
    public final int order;
    public final String name;
    public final boolean binary;
    public final boolean unsigned;
    public final boolean zeroFill;
    public final boolean autoIncremental;

    public DesignerDataType(String name, int order, boolean binary, boolean unsigned, boolean zeroFill, boolean autoIncremental) {
        this.name = name;
        this.order = order;
        this.binary = binary;
        this.unsigned = unsigned;
        this.zeroFill = zeroFill;
        this.autoIncremental = autoIncremental;
    }

    @Override
    public String toString() {
        return name;
    }
}
