package np.com.ngopal.smart.sql.structure.components;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CustomProgreeBarTableCell<S> extends TableCell<S, Double> {

    public static <S> Callback<TableColumn<S,Double>, TableCell<S,Double>> forTableColumn() {
        return forTableColumn(null);
    }
    
    public static <S> Callback<TableColumn<S,Double>, TableCell<S,Double>> forTableColumn(String progreeBarStyle) {
        return param -> new CustomProgreeBarTableCell<S>(progreeBarStyle);
    }

    private final Text progressBarText;
    private final ProgressBar progressBar;
    private final StackPane container;
    
    private ObservableValue<Double> observable;

    public CustomProgreeBarTableCell() {
        this(null);
    }
    public CustomProgreeBarTableCell(String progreeBarStyle) {
        this.getStyleClass().add("progress-bar-table-cell");
        
        if (progreeBarStyle != null) {
            this.getStyleClass().add(progreeBarStyle);
        }
        this.progressBar = new ProgressBar();
        this.progressBar.setMaxWidth(Double.MAX_VALUE);
        
        this.progressBar.progressProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                progressBarText.setText(new BigDecimal(newValue.doubleValue() * 100).setScale(2, RoundingMode.HALF_UP).toString() + "%");
            }
        });
        
        this.progressBarText = new Text();
        
        this.container = new StackPane(progressBar, progressBarText);
        
    }
    

    @Override 
    public void updateItem(Double item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
        } else {
            progressBar.progressProperty().unbind();

            final TableColumn<S,Double> column = getTableColumn();
            observable = column == null ? null : column.getCellObservableValue(getIndex());

            if (observable != null) {
                progressBar.progressProperty().bind(observable);
            } else if (item != null) {
                progressBar.setProgress(item);
            }

            setGraphic(container);
        }
    }
}
