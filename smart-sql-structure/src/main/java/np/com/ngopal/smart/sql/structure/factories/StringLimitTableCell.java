/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class StringLimitTableCell<T> extends TableCell<T, String> {

    private final int limit;

    public StringLimitTableCell(int limit) {
        this.limit = limit;
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null && item.length() >= limit) {
            setTooltip(new Tooltip(item));
            item = item.substring(0, limit) + "..";
        }
        this.setText(item);
        setStyle((isSelected() ? " -fx-background-color : white; -fx-text-fill : black;"
                : "-fx-background-color : transparent; -fx-text-fill : white;"));
    }
}
