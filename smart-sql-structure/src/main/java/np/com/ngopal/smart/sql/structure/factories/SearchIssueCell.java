/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.factories;

import java.util.function.Function;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.URLUtils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * TODO: Refactor this class
 */
public class SearchIssueCell<T, S> extends TableCell<T, S> {
    
    private final Function<T, Boolean> issueFunction;    
    protected Label label;
    
    public SearchIssueCell(Function<T, Boolean> issueFunction) {        
        this.issueFunction = issueFunction;
        setAlignment(Pos.CENTER);
    }

    @Override
    protected void updateItem(S item, boolean empty) {
        super.updateItem(item, empty); 
        setText("");

        int length = item != null ? item.toString().length(): 0;
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        setStyle("-fx-padding: 0;");

        String itemStr = item != null ? item.toString() : "";
        label = new Label(itemStr.startsWith("x'") && itemStr.endsWith("'") ? "(Binary / Image)" : itemStr.replaceAll("\r", "").replaceAll("\n", ""));
        label.setAlignment(Pos.CENTER_LEFT);
        label.setMaxSize(Double.MAX_VALUE, Label.USE_COMPUTED_SIZE);
        label.styleProperty().bind(styleProperty());

        HBox box = new HBox(label);
        box.setSpacing(2);
        box.getStyleClass().add("table-cell-hbox");
        box.setAlignment(Pos.CENTER_LEFT);
        box.setMaxHeight(USE_PREF_SIZE);            
        HBox.setHgrow(label, Priority.ALWAYS);
        setGraphic(box); 

        if (getTableRow() != null && getTableRow().getItem() != null && issueFunction != null && issueFunction.apply((T) getTableRow().getItem())) {
            
            setStyle("-fx-padding: 0;");
            
            setTooltip(new Tooltip(itemStr));
            
            Label googleLabel = new Label();
            googleLabel.setTooltip(new Tooltip("Google search"));
            googleLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getGoogle_image()));
            googleLabel.setCursor(Cursor.HAND);
            googleLabel.setOnMouseClicked((MouseEvent event) -> {
                URLUtils.openSearchUrl(URLUtils.SEARCH__GOOGLE_LINK, item != null ? item.toString() : "");
            });

            Label stackoverflowLabel = new Label();
            stackoverflowLabel.setTooltip(new Tooltip("Stackoverflow search"));
            stackoverflowLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getStackOverflow_image()));
            stackoverflowLabel.setCursor(Cursor.HAND);
            stackoverflowLabel.setOnMouseClicked((MouseEvent event) -> {
                URLUtils.openSearchUrl(URLUtils.SEARCH__STACKOVERFLOW_LINK, item != null ? item.toString() : "");
            });

            Label yahooLabel = new Label();
            yahooLabel.setTooltip(new Tooltip("Yahoo search"));
            yahooLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getYahoo_image()));
            yahooLabel.setCursor(Cursor.HAND);
            yahooLabel.setOnMouseClicked((MouseEvent event) -> {
                URLUtils.openSearchUrl(URLUtils.SEARCH__YAHOO_LINK, item != null ? item.toString() : "");
            });

            Label bingLabel = new Label();
            bingLabel.setTooltip(new Tooltip("Bing search"));
            bingLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getBing_image()));
            bingLabel.setCursor(Cursor.HAND);
            bingLabel.setOnMouseClicked((MouseEvent event) -> {
                URLUtils.openSearchUrl(URLUtils.SEARCH__BING_LINK, item != null ? item.toString() : "");
            });

            box.getChildren().addAll(googleLabel, stackoverflowLabel, yahooLabel, bingLabel);                           
        }
    }

    @Override
    public void updateSelected(boolean selected) {
        super.updateSelected(selected);
    }   
    
}