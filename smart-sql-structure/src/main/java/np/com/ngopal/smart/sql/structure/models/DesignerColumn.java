package np.com.ngopal.smart.sql.structure.models;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerColumn {

    private SimpleStringProperty uuid;
    
    private SimpleStringProperty name;
    private SimpleStringProperty collation;
    private SimpleStringProperty comments;
    
    private SimpleStringProperty dataType;
    private SimpleStringProperty defaultValue;
    
    private final SimpleBooleanProperty storage;
    private final SimpleBooleanProperty virtual;
    private final SimpleBooleanProperty primaryKey;
    private final SimpleBooleanProperty notNull;
    private final SimpleBooleanProperty unique;
    private final SimpleBooleanProperty binary;
    private final SimpleBooleanProperty unsigned;
    private final SimpleBooleanProperty zeroFill;
    private final SimpleBooleanProperty autoIncrement;
    private final SimpleBooleanProperty generated;
    
    public static final Background DEFAULT_BACKGROUND = new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY));
    
    private transient final SimpleObjectProperty<Background> backgroundColor = new SimpleObjectProperty<>(DEFAULT_BACKGROUND);
    
    
    private final SimpleBooleanProperty selected = new SimpleBooleanProperty(false);
    
    public DesignerColumn() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty();
        collation = new SimpleStringProperty("Table Default");
        comments = new SimpleStringProperty();

        dataType = new SimpleStringProperty();
        defaultValue = new SimpleStringProperty();

        storage = new SimpleBooleanProperty(false);
        virtual = new SimpleBooleanProperty(false);
        primaryKey = new SimpleBooleanProperty(false);
        notNull = new SimpleBooleanProperty(false);
        unique = new SimpleBooleanProperty(false);
        binary = new SimpleBooleanProperty(false);
        unsigned = new SimpleBooleanProperty(false);
        zeroFill = new SimpleBooleanProperty(false);
        autoIncrement = new SimpleBooleanProperty(false);
        generated = new SimpleBooleanProperty(false);
    }
    
    public DesignerColumn(String dc_name, String dc_collation, String dc_comments,
            String dc_datatype, String dc_default, boolean dc_storage, boolean dc_virtual,
            boolean dc_primaryKey, boolean dc_notnull, boolean dc_unique, boolean dc_binary,
            boolean dc_unsigned, boolean dc_zerofill, boolean dc_autin, boolean dc_generated) {
        
         name = new SimpleStringProperty(dc_name);
        collation = new SimpleStringProperty(dc_collation);
        comments = new SimpleStringProperty(dc_comments);

        dataType = new SimpleStringProperty(dc_datatype);
        defaultValue = new SimpleStringProperty(dc_default);

        storage = new SimpleBooleanProperty(dc_storage);
        virtual = new SimpleBooleanProperty(dc_virtual);
        primaryKey = new SimpleBooleanProperty(dc_primaryKey);
        notNull = new SimpleBooleanProperty(dc_notnull);
        unique = new SimpleBooleanProperty(dc_unique);
        binary = new SimpleBooleanProperty(dc_binary);
        unsigned = new SimpleBooleanProperty(dc_unsigned);
        zeroFill = new SimpleBooleanProperty(dc_zerofill);
        autoIncrement = new SimpleBooleanProperty(dc_autin);
        generated = new SimpleBooleanProperty(dc_generated);
        
    }
    
    
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    public SimpleStringProperty name() {
        return name;
    }
    
    public SimpleStringProperty collation() {
        return collation;
    }
        
    public SimpleStringProperty comments() {
        return comments;
    }
    
    public SimpleStringProperty dataType() {
        return dataType;
    }
    
    public SimpleStringProperty defaultValue() {
        return defaultValue;
    }
    
    public SimpleBooleanProperty storage() {
        return storage;
    }
    
    public SimpleBooleanProperty virtual() {
        return virtual;
    }
    
    public SimpleBooleanProperty primaryKey() {
        return primaryKey;
    }
    
    public SimpleBooleanProperty notNull() {
        return notNull;
    }
    
    public SimpleBooleanProperty unique() {
        return unique;
    }
    
    public SimpleBooleanProperty binary() {
        return binary;
    }
    
    public SimpleBooleanProperty unsigned() {
        return unsigned;
    }
    
    public SimpleBooleanProperty zeroFill() {
        return zeroFill;
    }
    
    public SimpleBooleanProperty autoIncrement() {
        return autoIncrement;
    }
    
    public SimpleBooleanProperty generated() {
        return generated;
    }
    
    public SimpleBooleanProperty selected() {
        return selected;
    }
        
    public SimpleObjectProperty<Background> backgroundColor() {
        return backgroundColor;
    }

    @Override
    public String toString() {
        return name.get();
    }
    
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        map.put("colation", collation.get());
        map.put("comments", comments.get());
        map.put("dataType", dataType.get());
        map.put("defaultValue", defaultValue.get());
        
        map.put("storage", storage.get());
        map.put("virtual", virtual.get());
        map.put("primaryKey", primaryKey.get());
        map.put("notNull", notNull.get());
        map.put("unique", unique.get());
        map.put("binary", binary.get());
        map.put("unsigned", unsigned.get());
        map.put("zeroFill", zeroFill.get());
        map.put("autoIncrement", autoIncrement.get());
        map.put("generated", generated.get());
                
        return map;
    }

    public void fromMap(Map<String, Object> map) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set((String)map.get("name"));
            collation.set((String)map.get("colation"));
            comments.set((String)map.get("comments"));
            dataType.set((String)map.get("dataType"));
            defaultValue.set((String)map.get("defaultValue"));
            
            Boolean storage0 = (Boolean)map.get("storage");
            storage.set(storage0 != null ? storage0 : false);
            
            Boolean virtual0 = (Boolean)map.get("virtual");
            virtual.set(virtual0 != null ? virtual0 : false);
            
            Boolean primaryKey0 = (Boolean)map.get("primaryKey");
            primaryKey.set(primaryKey0 != null ? primaryKey0 : false);
            
            Boolean notNull0 = (Boolean)map.get("notNull");
            notNull.set(notNull0 != null ? notNull0 : false);
            
            Boolean unique0 = (Boolean)map.get("unique");
            unique.set(unique0 != null ? unique0 : false);
            
            Boolean binary0 = (Boolean)map.get("binary");
            binary.set(binary0 != null ? binary0 : false);
            
            Boolean unsigned0 = (Boolean)map.get("unsigned");
            unsigned.set(unsigned0 != null ? unsigned0 : false);
            
            Boolean zeroFill0 = (Boolean)map.get("zeroFill");
            zeroFill.set(zeroFill0 != null ? zeroFill0 : false);
            
            Boolean autoIncrement0 = (Boolean)map.get("autoIncrement");
            autoIncrement.set(autoIncrement0 != null ? autoIncrement0 : null);
            
            Boolean generated0 = (Boolean)map.get("generated");
            generated.set(generated0 != null ? generated0 : false);
        }
    }
    
    
}
