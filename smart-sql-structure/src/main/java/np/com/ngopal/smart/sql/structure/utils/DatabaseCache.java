package np.com.ngopal.smart.sql.structure.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Advanced;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DatabaseCache {
    
    private final Map<String, Map<String, Database>> databasesCache = new HashMap<>();
    private final Map<String, Map<String, Map<String, DBTable>>> tablesCache = new HashMap<>();
    private final Map<String, List<String>> showVariables = new HashMap<>();
    private final Map<String, List<String>> showStatus = new HashMap<>();
    
    private final List<Object> keywordsKeys = new ArrayList<>();
    private final List<Object> serverFunctionsKeys = new ArrayList<>();
    private final Map<String, List<Object>> enginesKeys = new HashMap<>();
    private final Map<String, List<Object>> databasesKeys = new HashMap<>();
    private final Map<String, List<Object>> tablesKeys = new HashMap<>();
    private final Map<String, List<Object>> viewsKeys = new HashMap<>();    
    private final Map<String, List<Object>> columnsKeys = new HashMap<>();
    private final Map<String, List<Object>> proceduresKeys = new HashMap<>();
    private final Map<String, List<Object>> functionsKeys = new HashMap<>();
    private final Map<String, List<Object>> triggersKeys = new HashMap<>();    
    private final Map<String, List<Object>> eventsKeys = new HashMap<>();
    
    private static final Pattern PATTERN__FOREIGN_KEY = Pattern.compile("CONSTRAINT `(?<name>.*?)` FOREIGN KEY \\(`(?<columns>.*?)`\\) REFERENCES `(?<refTable>.*?)`\\(`(?<refColumns>.*?)`\\)(?<updatCascade> ON UPDATE CASCADE)?(?<updateRestrict> ON UPDATE RESTRICT)?(?<deleteCascade> ON DELETE CASCADE)?(?<deleteRstrict> ON DELETE RESTRICT)?");
    
    public synchronized List<Object> getKeywordsKeys() {
        return keywordsKeys;
    }
    
    public synchronized List<Object> getServerFunctionsKeys() {
        return serverFunctionsKeys;
    }
    
    public synchronized List<Object> getEnginesKeys(String params) {
        List<Object> list = enginesKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getDatabasesKeys(String params) {
        List<Object> list = databasesKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getTablesKeys(String params) {
        List<Object> list = tablesKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getViewsKeys(String params) {
        List<Object> list = viewsKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized View getView(String params, String viewName) {
        List<Object> list = viewsKeys.get(params);
        if (list != null) {
            for (Object obj: list) {
                if (viewName.equals(((View)obj).getName())) {
                    return (View) obj;
                }
            }
        }
        
        return null;
    }
    
    public synchronized List<Object> getColumnKeys(String params) {
        List<Object> list = columnsKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getProceduresKeys(String params) {
        List<Object> list = proceduresKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getFunctionsKeys(String params) {
        List<Object> list = functionsKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getTriggersKeys(String params) {
        List<Object> list = triggersKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized List<Object> getEventsKeys(String params) {
        List<Object> list = eventsKeys.get(params);
        return list != null ? list : Collections.EMPTY_LIST;
    }
    
    public synchronized boolean hasDatabase(String params, String database) {
        return getDatabase(params, database) != null;
    }
    
    public synchronized boolean hasTable(String params, String tableName) {
        
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);        
        if (m != null) {
            
            for (Map<String, DBTable> tables: m.values()) {
                if (tables.containsKey(tableName)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public synchronized List<DBTable> getTables(String params, String databaseName) {
        List<DBTable> result = new ArrayList<>();
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);        
        if (m != null) {
            
            Map<String, DBTable> map = m.get(databaseName.toLowerCase());            
            if (map != null) {
                for (String t: map.keySet()) {
                    result.add(getCopyOfTable(params, databaseName, t));
                }
            }
        }
        
        return result;
    }
    
    
    public synchronized List<String> getTablePrimaryColumnsNamesLowerCase(String params, String databaseName, String tableName) {
        
        List<String> columns = new ArrayList<>();
        
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);
        
        if (m != null) {
            Map<String, DBTable> map = m.get(databaseName.toLowerCase());
            
            if (map != null && map.containsKey(tableName.toLowerCase())) {
                DBTable t = map.get(tableName.toLowerCase());
                if (t.getColumns() != null) {
                    for (Column c: t.getColumns()) {
                        if (c.isPrimaryKey()) {
                            columns.add(c.getName().toLowerCase());
                        }
                    }
                }
            }
        }
        
        return columns;
    }
    
    
    public synchronized Database getDatabase(String params, String databaseName) {
        Map<String, Database> m = databasesCache.get(params);
        return m != null ? m.get(databaseName.toLowerCase()) : null;
    }
    
    public synchronized DBTable getCopyOfTable(String params, String databaseName, String tableName) {
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);
        
        if (m != null) {
            Map<String, DBTable> map = m.get(databaseName.toLowerCase());
            
            if (map != null && map.containsKey(tableName.toLowerCase())) {
                DBTable t = new DBTable();
                DBTable t0 = map.get(tableName.toLowerCase());
                t.copy(t0);
                if (t0.getColumns() != null) {
                    for (Column c: t0.getColumns()) {
                        Column c0 = new Column();
                        c0.copy(c);
                        t.addColumn(c0);
                    }
                }
                return t;
            }
        }
        
        return null;
    }
    
    public synchronized DBTable getTable(String params, String databaseName, String tableName) {
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);        
        if (m != null) {
            
            Map<String, DBTable> map = m.get(databaseName.toLowerCase());            
            if (map != null && map.containsKey(tableName.toLowerCase())) {
                return map.get(tableName.toLowerCase());
            }
        }
        
        return null;
    }
    
    public synchronized boolean containsTable(String params, String databaseName, String tableName) {
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);        
        if (m != null) {
            
            Map<String, DBTable> map = m.get(databaseName.toLowerCase());            
            if (map != null) {
                return map.containsKey(tableName.toLowerCase());
            }
        }
        
        return false;
    }
    
    public synchronized List<Database> getDatabases(String params) {
        Map<String, Database> m = databasesCache.get(params);
        return m != null ? new ArrayList(m.values()) : Collections.EMPTY_LIST;
    }
    
    public synchronized boolean isColumnAutoincrement(String params, String databaseName, String tableName, String columnName) {
        Map<String, Map<String, DBTable>> databases = tablesCache.get(params);
        if (databases != null) {
            Map<String, DBTable> db = databases.get(databaseName);
            if (db != null) {
                DBTable table = db.get(tableName);
                if (table != null) {
                    Column c = table.getColumnByName(columnName);
                    if (c != null) {
                        return c.isAutoIncrement();
                    }
                }
            }
        }
        
        return false;
    }
    
    public synchronized boolean isColumnEnum(String params, String databaseName, String tableName, String columnName) {
        Map<String, Map<String, DBTable>> databases = tablesCache.get(params);
        if (databases != null) {
            Map<String, DBTable> db = databases.get(databaseName);
            if (db != null) {
                DBTable table = db.get(tableName);
                if (table != null) {
                    Column c = table.getColumnByName(columnName);
                    if (c != null) {
                        return c.getDataType() != null && c.getDataType().isEnum();
                    }
                }
            }
        }
        
        return false;
    }
    
    public synchronized DataType getColumnDataType(String params, String databaseName, String tableName, String columnName) {
        Map<String, Map<String, DBTable>> databases = tablesCache.get(params);
        if (databases != null) {
            Map<String, DBTable> db = databases.get(databaseName);
            if (db != null) {
                DBTable table = db.get(tableName);
                if (table != null) {
                    Column c = table.getColumnByName(columnName);
                    if (c != null) {
                        return c.getDataType();
                    }
                }
            }
        }
        
        return null;
    }
    
    public synchronized boolean isColumnSet(String params, String databaseName, String tableName, String columnName) {
        Map<String, Map<String, DBTable>> databases = tablesCache.get(params);
        if (databases != null) {
            Map<String, DBTable> db = databases.get(databaseName);
            if (db != null) {
                DBTable table = db.get(tableName);
                if (table != null) {
                    Column c = table.getColumnByName(columnName);
                    if (c != null) {
                        return c.getDataType() != null && c.getDataType().isSet();
                    }
                }
            }
        }
        
        return false;
    }
    
    
    public synchronized String getColumnDefaultValue(String params, String databaseName, String tableName, String columnName) {
        Map<String, Map<String, DBTable>> databases = tablesCache.get(params);
        if (databases != null) {
            Map<String, DBTable> db = databases.get(databaseName);
            if (db != null) {
                DBTable table = db.get(tableName);
                if (table != null) {
                    Column c = table.getColumnByName(columnName);
                    if (c != null) {
                        return c.getDefaults();
                    }
                }
            }
        }
        
        return null;
    }
    
    public synchronized List<String> getShowStatus(String params) {
        List<String> result = showStatus.get(params);
        return result != null ? result : new ArrayList<>();
    }
    
    public synchronized List<String> getShowVariables(String params) {
        List<String> result = showVariables.get(params);
        return result != null ? result : new ArrayList<>();
    }
    
    public synchronized Database syncDatabase(String databaseName, String params, AbstractDBService service, boolean force) throws SQLException {
        return loadDatabase(databaseName, service.loadDBStructure(databaseName, force), params, service, force);        
    }
    
    public synchronized Database syncDatabase(String databaseName, String params, AbstractDBService service) throws SQLException {
        return loadDatabase(databaseName, service.loadDBStructure(databaseName, false), params, service, true);        
    }
    
    public synchronized void updateDBTable(String databaseName, DBTable table, String params) throws SQLException {
        Map<String, Map<String, DBTable>> m = tablesCache.get(params);        
        if (m != null) {
            
            Map<String, DBTable> map = m.get(databaseName.toLowerCase());            
            if (map == null) {
                map = new HashMap<>();
                m.put(databaseName.toLowerCase(), map);
            }
            
            map.put(table.getName().toLowerCase(), table.makeCopy());
        }
    }
    
    public synchronized DBTable syncDBTable(String databaseName, String tableName, String params, AbstractDBService service) throws SQLException {
        return syncDBTable(databaseName, tableName, params, service, false);
    }
    public synchronized DBTable syncDBTable(String databaseName, String tableName, String params, AbstractDBService service, boolean force) throws SQLException {
        return loadTable(databaseName, tableName, service.loadDBStructure(databaseName, tableName, force), params, service);
    }
    
    public synchronized List<Database> syncDatabases(Map<String, Map<String, Object>> data, ConnectionParams params, AbstractDBService service) {
        return syncDatabases(data, params, service, false);
    }
    
    public synchronized List<Database> syncDatabases(Map<String, Map<String, Object>> data, ConnectionParams params, AbstractDBService service, boolean onlyDBStructure) {
                
        databasesCache.remove(params);
        tablesCache.remove(params);
        
        List<Database> databases = new ArrayList<>();
        if (data != null) {
            for (String database: data.keySet()) {
                Database db = loadDatabase(database, data.get(database), params.cacheKey(), service, false); 
                if (db != null) {
                    databases.add(db);
                }
            }
        }
        
        if (!onlyDBStructure) {
            updateCompletionData(params.cacheKey(), params.getDatabase(), service, null);
        }
        
        return databases;
    }
    
    public synchronized void updateCompletionData(String params, List<String> databases, AbstractDBService service, Database withUseDatabase) {
        
        showStatus.clear();
        showVariables.clear();
        
        enginesKeys.remove(params);        
        
        String query = "";
        if (withUseDatabase != null) {
            query += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__CHANGE_DATABASE), withUseDatabase.getName());
            if (!query.trim().endsWith(";")) {
                query += ";";
            }
        }
        
        query += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_VARIABLES);
        if (!query.trim().endsWith(";")) {
            query += ";";
        }
        
        query += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_STATUS);
        if (!query.trim().endsWith(";")) {
            query += ";";
        }

        query += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ENGINES);
        if (!query.trim().endsWith(";")) {
            query += ";";
        }

        try {
            QueryResult qr = new QueryResult();
            service.execute(query, qr);
            int index = 0;
            if (withUseDatabase != null) {
                index++;
            }
            {
                ResultSet rs = (ResultSet) qr.getResult().get(index);
                index++;
                while (rs.next()) {        
                    List<String> l = showVariables.get(params);
                    if (l == null) {
                        l = new ArrayList<>();
                        showVariables.put(params, l);
                    }

                    l.add(rs.getString(1));                
                }

                rs.close();
            }
            
            {
                ResultSet rs = (ResultSet) qr.getResult().get(index);
                index++;
                while (rs.next()) {        
                    List<String> l = showStatus.get(params);
                    if (l == null) {
                        l = new ArrayList<>();
                        showStatus.put(params, l);
                    }

                    l.add(rs.getString(1));              
                }

                rs.close();
            }
            
            {
                ResultSet rs = (ResultSet) qr.getResult().get(index);
                index++;
                List<String> engines = new ArrayList();
                while (rs.next()) {
                    engines.add(rs.getString(1));
                }
                
                rs.close();
                
                if (engines != null) {

                    List<Object> l = enginesKeys.get(params);
                    if (l == null) {
                        l = new ArrayList<>();
                        enginesKeys.put(params, l);
                    }

                    l.addAll(engines);
                }  
            }
            
        } catch (SQLException ex) {}
        
//        try (ResultSet rs  = (ResultSet) service.execute()){
//            while (rs.next()) {        
//                List<String> l = showVariables.get(params);
//                if (l == null) {
//                    l = new ArrayList<>();
//                    showVariables.put(params, l);
//                }
//
//                l.add(rs.getString(1));                
//            }
//        } catch (SQLException ex) {}
//
//        try (ResultSet rs  = (ResultSet) service.execute()){
//            while (rs.next()) {                 
//                List<String> l = showStatus.get(params);
//                if (l == null) {
//                    l = new ArrayList<>();
//                    showStatus.put(params, l);
//                }
//
//                l.add(rs.getString(1));
//            }
//        } catch (SQLException ex) {}
//            
//
//        try {
//            List<String> engines = service.getEngine();
//            if (engines != null) {
//
//                List<Object> l = enginesKeys.get(params);
//                if (l == null) {
//                    l = new ArrayList<>();
//                    enginesKeys.put(params, l);
//                }
//
//                l.addAll(engines);
//            }                    
//        }  catch (SQLException ex) {}

        collectAutocompleteData(params, databases);
    }
    
    
    public synchronized void updateViewsCompletionData(String params, Database database) {
        List<Object> listViews = viewsKeys.get(params);
        if (listViews == null) {
            listViews = new ArrayList<>();
            viewsKeys.put(params, listViews);
        }
                
        // found view only for selected database
        List<Database> dbs = getDatabases(params);
        if (dbs != null) {
            for (Database db: dbs) {
                if (db.equals(database)) {
                    
                    List<View> forRemove = new ArrayList<>();
                    // when found - try to clear old and add new
                    for (View v: (List<View>)(List)listViews) {
                        if (database.equals(v.getDatabase())) {
                            forRemove.add(v);
                        }
                    }
                    
                    // remove founded
                    listViews.removeAll(forRemove);
                    
                    // add new views
                    if (database.getViews() != null) {
                        listViews.addAll(database.getViews());
                    }
                }
            }
        }        
    }
    
    public synchronized void updateFunctionsCompletionData(String params, Database database) {
        List<Object> listFunctions = functionsKeys.get(params);
        if (listFunctions == null) {
            listFunctions = new ArrayList<>();
            functionsKeys.put(params, listFunctions);
        }
                
        // found view only for selected database
        List<Database> dbs = getDatabases(params);
        if (dbs != null) {
            for (Database db: dbs) {
                if (db.equals(database)) {
                    
                    List<Function> forRemove = new ArrayList<>();
                    // when found - try to clear old and add new
                    for (Function v: (List<Function>)(List)listFunctions) {
                        if (database.equals(v.getDatabase())) {
                            forRemove.add(v);
                        }
                    }
                    
                    // remove founded
                    listFunctions.removeAll(forRemove);
                    
                    // add new functions
                    if (database.getFunctions() != null) {
                        listFunctions.addAll(database.getFunctions());
                    }
                }
            }
        }        
    }
    
    public synchronized void updateTriggersCompletionData(String params, Database database) {
        List<Object> listTriggers = triggersKeys.get(params);
        if (listTriggers == null) {
            listTriggers = new ArrayList<>();
            triggersKeys.put(params, listTriggers);
        }
                
        // found trigger only for selected database
        List<Database> dbs = getDatabases(params);
        if (dbs != null) {
            for (Database db: dbs) {
                if (db.equals(database)) {
                    
                    List<Trigger> forRemove = new ArrayList<>();
                    // when found - try to clear old and add new
                    for (Trigger v: (List<Trigger>)(List)listTriggers) {
                        if (database.equals(v.getDatabase())) {
                            forRemove.add(v);
                        }
                    }
                    
                    // remove founded
                    listTriggers.removeAll(forRemove);
                    
                    // add new triggers
                    if (database.getTriggers() != null) {
                        listTriggers.addAll(database.getTriggers());
                    }
                }
            }
        }        
    }
    
    public synchronized void updateProceduresCompletionData(String params, Database database) {
        List<Object> listProcedures = proceduresKeys.get(params);
        if (listProcedures == null) {
            listProcedures = new ArrayList<>();
            proceduresKeys.put(params, listProcedures);
        }
                
        // found procedure only for selected database
        List<Database> dbs = getDatabases(params);
        if (dbs != null) {
            for (Database db: dbs) {
                if (db.equals(database)) {
                    
                    List<StoredProc> forRemove = new ArrayList<>();
                    // when found - try to clear old and add new
                    for (StoredProc v: (List<StoredProc>)(List)listProcedures) {
                        if (database.equals(v.getDatabase())) {
                            forRemove.add(v);
                        }
                    }
                    
                    // remove founded
                    listProcedures.removeAll(forRemove);
                    
                    // add new prcedures
                    if (database.getStoredProc() != null) {
                        listProcedures.addAll(database.getStoredProc());
                    }
                }
            }
        }        
    }
    
    public synchronized void updateEventsCompletionData(String params, Database database) {
        List<Object> listEvents = eventsKeys.get(params);
        if (listEvents == null) {
            listEvents = new ArrayList<>();
            eventsKeys.put(params, listEvents);
        }
                
        // found event only for selected database
        List<Database> dbs = getDatabases(params);
        if (dbs != null) {
            for (Database db: dbs) {
                if (db.equals(database)) {
                    
                    List<Event> forRemove = new ArrayList<>();
                    // when found - try to clear old and add new
                    for (Event v: (List<Event>)(List)listEvents) {
                        if (database.equals(v.getDatabase())) {
                            forRemove.add(v);
                        }
                    }
                    
                    // remove founded
                    listEvents.removeAll(forRemove);
                    
                    // add new events
                    if (database.getEvents() != null) {
                        listEvents.addAll(database.getEvents());
                    }
                }
            }
        }        
    }
    
    public Database loadDatabase(String database, Map<String, Object> databaseStructure, String params, AbstractDBService service, boolean readScripts) {
        
        //log.debug("Start caching database");
        if (databaseStructure != null) {
            Database databaseRef =  new Database();
            databaseRef.setName(database);
//            databaseRef.setParams(params);

            Map<String, Database> map = databasesCache.get(params);
            if (map == null) {
                map = new LinkedHashMap<>();
                databasesCache.put(params, map);
            }

            map.put(database.toLowerCase(), databaseRef);

            Map<String, Map<String, List<String>>> tables = (Map<String, Map<String, List<String>>>) databaseStructure.get(DBService.COLUMN_NAME__TABLES);
            if (tables != null) {

                Map<String, Map<String, DBTable>> dbMap = tablesCache.get(params);
                if (dbMap == null) {
                    dbMap = new LinkedHashMap<>();
                    tablesCache.put(params, dbMap);
                }

                Map<String, DBTable> tableMap = dbMap.get(database.toLowerCase());
                if (tableMap == null) {
                    tableMap = new LinkedHashMap<>();
                    dbMap.put(database.toLowerCase(), tableMap);
                }

                List<DBTable> dbtables = new ArrayList<>();
                for (String table: tables.keySet()) {

                    DBTable tableRef = new DBTable();
                    tableRef.setName(table);
                    tableRef.setDatabase(databaseRef);

                    dbtables.add(tableRef);

                    tableMap.put(table.toLowerCase(), tableRef);

                    Map<String, List<String>> tableData = tables.get(table);

                    List<String> infoList = tableData.get(DBService.COLUMN_NAME__INFO);
                    if (infoList != null && !infoList.isEmpty()) {
                        String[] info = infoList.get(0).split("\\$\\$\\$\\$");

                        tableRef.setEngineType(new MySQLEngineTypes().getTypeByName(info[0]));
                        tableRef.setCharacterSet(info[1]);
                        tableRef.setCollation(info[2]);
                        if (tableRef.getAdvanced() == null) {
                            tableRef.setAdvanced(new Advanced());
                        }
                        tableRef.getAdvanced().setAvgRowLength(Integer.valueOf(info[4]));
                    }


                    List<String> columns = tableData.get(DBService.COLUMN_NAME__COLUMNS);
                    if (columns != null) {

                        int i = 0;
                        List<Column> tableColumns = new ArrayList<>();
                        for (String column: columns) {

                            Column columnRef = new Column();
                            columnRef.setTable(tableRef);

                            columnRef.setOrderIndex(i);
                            i++;

                            String[] columnInfo = column.split("\\$\\$\\$\\$");
                            int length = columnInfo.length;

                            if (length > 0) {
                                columnRef.setName(columnInfo[0]);
                                columnRef.setOriginalName(columnInfo[0]);
                            }

                            if (length > 1) {
                                columnRef.setDataType(service.getDatatype(columnInfo[1]));
                                String[] strs = columnInfo[1].split(" ");
                                if (strs.length > 1 && "unsigned".equalsIgnoreCase(strs[strs.length - 1])) {
                                    columnRef.setUnsigned(true);
                                }
                            }

                            if (length > 2) {
                                columnRef.setNotNull("NOT NULL".equalsIgnoreCase(columnInfo[2]));
                            }

                            if (length > 3) {
                                columnRef.setAutoIncrement("auto_increment".equalsIgnoreCase(columnInfo[3].trim()));
                            }

                            if (length > 4) {
                                columnRef.setDefaults(columnInfo[4]);
                            }

                            if (length > 5) {
                                columnRef.setCharacterSet(columnInfo[5]);
                            }

                            if (length > 6) {
                                columnRef.setCollation(columnInfo[6]);
                            }

                            if (length > 7) {
                                columnRef.setComment(columnInfo[7]);
                            }

                            tableColumns.add(columnRef);
                        }

                        tableRef.setColumns(tableColumns);
                    }

                    List<String> indexes = tableData.get(DBService.COLUMN_NAME__INDEXES);
                    if (indexes != null) {

                        List<Index> tableIndexes = new ArrayList<>();
                        for (String index: indexes) {

                            // its just an index
                            if (index.startsWith("BTREE ")) {
                                index = index.substring(6);

                            }

                            boolean primary = index.startsWith("PRIMARY(");

                            Index indexRef = new Index();
                            indexRef.setTable(tableRef);
                            indexRef.setType(primary ? IndexType.PRIMARY : null);

                            if (primary) {

                                indexRef.setName("PRIMARY");

                                String primaryName = index.substring(8, index.length() - 1);
                                String[] cols = primaryName.split(",");
                                Column[] indexColumns = new Column[cols.length];
                                int i = 0;
                                for (String c: cols) {
                                    
                                    if (c.contains("(")) {
                                        String strLength = c.substring(c.indexOf("(") + 1, c.indexOf(")"));
                                        
                                        c = c.substring(0, c.indexOf("(")).trim();
                                        
                                        try {
                                            indexRef.setColumnLength(c, Integer.valueOf(strLength));
                                        } catch (NumberFormatException ex) {
                                            log.error(ex.getMessage(), ex);
                                        }
                                    }
                                    
                                    Column primaryColumn = tableRef.getColumnByName(c.trim());

                                    if (primaryColumn != null) {
                                        primaryColumn.setPrimaryKey(true);
                                    }
                                    indexColumns[i] = primaryColumn;
                                    i++;
                                }

                                indexRef.setColumns(Arrays.asList(indexColumns));

                            } else {

                                if (index.startsWith("FULLTEXT ")) {
                                    indexRef.setType(IndexType.FULL_TEXT);
                                    index = index.substring(9);
                                } else if (index.startsWith("SPATIAL ")) {
                                    indexRef.setType(IndexType.SPATIAL);
                                    index = index.substring(8);
                                } else if (index.startsWith("UNIQUE ")) {
                                    indexRef.setType(IndexType.UNIQUE_TEXT);
                                    index = index.substring(7);
                                }

                                int startIndex = index.indexOf("(");
                                indexRef.setName(index.substring(0, startIndex));


                                String[] idxColumns = index.substring(startIndex + 1, index.length() - 1).split(",");
                                if (idxColumns != null) {

                                    List<Column> columnsList = new ArrayList<>();
                                    for (String idxColumn: idxColumns) {
                                        
                                        if (idxColumn.contains("(")) {
                                            String strLength = idxColumn.substring(idxColumn.indexOf("(") + 1, idxColumn.indexOf(")"));

                                            idxColumn = idxColumn.substring(0, idxColumn.indexOf("(")).trim();

                                            try {
                                                indexRef.setColumnLength(idxColumn, Integer.valueOf(strLength));
                                            } catch (NumberFormatException ex) {
                                                log.error(ex.getMessage(), ex);
                                            }
                                        }
                                        
                                        Column c = tableRef.getColumnByName(idxColumn.trim());
                                        if (c != null) {
                                            columnsList.add(c);
                                        }
                                    }

                                    indexRef.setColumns(columnsList);
                                }
                            }
                            if (primary) {
                                tableIndexes.add(0, indexRef);
                            } else {
                                tableIndexes.add(indexRef);
                            }
                        }

                        tableRef.setIndexes(tableIndexes);
                    }
                }

                // After collecting all tables - need collect foreign keys
                for (String table: tables.keySet()) {

                    Map<String, List<String>> tableData = tables.get(table);

                    DBTable sourceTable = tableMap.get(table.toLowerCase());    

                    List<String> foreignkeys = tableData.get(DBService.COLUMN_NAME__FOREIGNS);
                    if (foreignkeys != null) {

                        List<ForeignKey> tableForeignKeys = new ArrayList<>();
                        for (String foreignkey: foreignkeys) {

                            Matcher m = PATTERN__FOREIGN_KEY.matcher(foreignkey);
                            if (m.find()) {

                                ForeignKey fk = new ForeignKey();
                                fk.setReferenceDatabase(database);
                                fk.setName(m.group("name"));

                                String[] cols = m.group("columns").split(",");
                                Column[] fkColumns = new Column[cols.length];
                                int i = 0;
                                for (String c: cols) {
                                    fkColumns[i] = sourceTable.getColumnByName(c.trim());
                                    i++;
                                }

                                fk.setColumns(Arrays.asList(fkColumns));

                                String refTableName = m.group("refTable").toLowerCase();
                                DBTable refTable = tableMap.get(refTableName);                            
                                fk.setReferenceTable(refTableName);


                                String[] refCols = m.group("refColumns").split(",");
                                Column[] fkRefColumns = new Column[refCols.length];
                                i = 0;
                                for (String c: refCols) {
                                    Column col = refTable != null ? refTable.getColumnByName(c.trim()) : null;
                                    if (col == null) {
                                        col = new Column();
                                        col.setName(c.trim());
                                        col.setOriginalName(c.trim());
                                    }

                                    fkRefColumns[i] = col;
                                    i++;
                                }
                                fk.setReferenceColumns(Arrays.asList(fkRefColumns));

                                tableForeignKeys.add(fk);
                            } else {
                                log.warn("CAN NOT PARSE FOREIGN KEY - need check!!!: " + foreignkey);
                            }                        
                        }

                        sourceTable.setForeignKeys(tableForeignKeys);
                    }
                }




                databaseRef.setTables(dbtables);
            }

            Map<String, Map<String, List<String>>> views = (Map<String, Map<String, List<String>>>) databaseStructure.get(DBService.COLUMN_NAME__VIEWS);
            if (views != null) {
                List<View> viewList = new ArrayList<>();
                for (String view: views.keySet()) {

                    Map<String, List<String>> viewData = views.get(view);
                    List<String> columns = viewData.get(DBService.COLUMN_NAME__COLUMNS);

                    View viewRef = new View(view, null, databaseRef, null, null);

                    // in columns - create script
                    if (columns != null && !columns.isEmpty()) {
                        viewRef.setCreateScript(columns.get(0));
                    }

                    viewList.add(viewRef);
                }

                databaseRef.setViews(viewList);
            }


            Map<String, Map<String, List<String>>> procedures = (Map<String, Map<String, List<String>>>) databaseStructure.get(DBService.COLUMN_NAME__PROCEDURES);
            if (procedures != null) {
                List<StoredProc> procList = new ArrayList<>();
                for (String proc: procedures.keySet()) {

                    Map<String, List<String>> procData = procedures.get(proc);
                    List<String> columns = procData.get(DBService.COLUMN_NAME__COLUMNS);

                    StoredProc procRef = new StoredProc(proc, databaseRef, null);

                    String script = null;
                    // in columns - create script
                    if (columns != null && !columns.isEmpty()) {
                        script = "DELIMITER $$ \n\n" +
                            columns.get(0) + "$$\n\n" +
                            "DELIMITER;";
                    } else if (readScripts) {
                        try (ResultSet rs0 = (ResultSet) service.execute(service.getShowCreateStoredProcedureSQL(database, proc))) {
                            if (rs0.next()) {
                                script = rs0.getString("Create Procedure");
                                if (columns == null) {
                                    columns = new ArrayList<>();
                                    procData.put(DBService.COLUMN_NAME__COLUMNS, columns);
                                }

                                columns.add(script);
                            }
                        } catch (SQLException ex) {
                            log.error("Error", ex);
                        }
                    }

                    procRef.setCreateScript(script);

                    procList.add(procRef);
                }

                databaseRef.setStoredProc(procList);
            }


            Map<String, Map<String, List<String>>> functions = (Map<String, Map<String, List<String>>>) databaseStructure.get(DBService.COLUMN_NAME__FUNCTIONS);
            if (functions != null) {
                List<Function> functionList = new ArrayList<>();
                for (String func: functions.keySet()) {

                    Map<String, List<String>> funcData = functions.get(func);
                    List<String> columns = funcData.get(DBService.COLUMN_NAME__COLUMNS);

                    Function funcRef = new Function(func, databaseRef, null);

                    String script = null;
                    // in columns - create script
                    if (columns != null && !columns.isEmpty()) {

                        script = "DELIMITER $$ \n\n" +
                            columns.get(0) + "$$\n\n" +
                            "DELIMITER;";
                    } else if (readScripts) {

                        try (ResultSet rs0 = (ResultSet) service.execute(service.getShowCreateFunctionSQL(database, func))) {
                            if (rs0.next()) {
                                script = rs0.getString("Create Function");

                                if (columns == null) {
                                    columns = new ArrayList<>();
                                    funcData.put(DBService.COLUMN_NAME__COLUMNS, columns);
                                }

                                columns.add(script);
                            }
                        } catch (SQLException ex) {
                            log.error("Error", ex);
                        }
                    }

                    funcRef.setCreateScript(script);

                    functionList.add(funcRef);
                }

                databaseRef.setFunctions(functionList);
            }


            Map<String, Map<String, List<String>>> triggers = (Map<String, Map<String, List<String>>>) databaseStructure.get(DBService.COLUMN_NAME__TRIGGERS);
            if (triggers != null) {
                List<Trigger> triggersList = new ArrayList<>();
                for (String trigger: triggers.keySet()) {

                    Map<String, List<String>> triggerData = triggers.get(trigger);
                    List<String> columns = triggerData.get(DBService.COLUMN_NAME__COLUMNS);

                    Trigger triggerRef = new Trigger(trigger, databaseRef, null);

                    // in columns - create script
                    if (columns != null && !columns.isEmpty()) {
                        String script = "DELIMITER $$ \n\n" +
                            columns.get(0) + "$$\n\n" +
                            "DELIMITER;";                    

                        triggerRef.setCreateScript(script);
                    }

                    triggersList.add(triggerRef);
                }

                databaseRef.setTriggers(triggersList);
            }


            Map<String, Map<String, List<String>>> events = (Map<String, Map<String, List<String>>>) databaseStructure.get(DBService.COLUMN_NAME__EVENTS);
            if (events != null) {
                List<Event> eventList = new ArrayList<>();
                for (String event: events.keySet()) {

                    Map<String, List<String>> eventData = events.get(event);
                    List<String> columns = eventData.get(DBService.COLUMN_NAME__COLUMNS);

                    Event eventRef = new Event(event, databaseRef, null);

                    // in columns - create script
                    if (columns != null && !columns.isEmpty()) {

                        String script = "DELIMITER $$ \n\n" +
                            columns.get(0) + "$$\n\n" +
                            "DELIMITER;";

                        eventRef.setCreateScript(script);
                    }

                    eventList.add(eventRef);
                }

                databaseRef.setEvents(eventList);
            }
            
            //log.debug("Finish caching database");

            return databaseRef;
        }
        
        //log.debug("Finish caching database");
        return null;
    }
    
    private DBTable loadTable(String database, String table, Map<String, List<String>> tableData, String params, AbstractDBService service) {
        
        Map<String, Map<String, DBTable>> dbMap = tablesCache.get(params);
        if (dbMap == null) {
            dbMap = new LinkedHashMap<>();
            tablesCache.put(params, dbMap);
        }

        Map<String, DBTable> tableMap = dbMap.get(database.toLowerCase());
        if (tableMap == null) {
            tableMap = new LinkedHashMap<>();
            dbMap.put(database.toLowerCase(), tableMap);
        }

        Database databaseRef = getDatabase(params, database);
        
        DBTable tableRef = new DBTable();
        tableRef.setName(table);
        tableRef.setDatabase(databaseRef);

        DBTable t0 = databaseRef.getTable(table);
        if (t0 != null) {
            databaseRef.getTables().remove(t0);
        }        
        databaseRef.addTable(tableRef);
        
        tableMap.put(table.toLowerCase(), tableRef);

        List<String> infoList = tableData.get(DBService.COLUMN_NAME__INFO);
        if (infoList != null && !infoList.isEmpty()) {
            String[] info = infoList.get(0).split("\\$\\$\\$\\$");

            tableRef.setEngineType(new MySQLEngineTypes().getTypeByName(info[0]));
            tableRef.setCharacterSet(info[1]);
            tableRef.setCollation(info[2]);
            if (tableRef.getAdvanced() == null) {
                tableRef.setAdvanced(new Advanced());
            }
            tableRef.getAdvanced().setAvgRowLength(Integer.valueOf(info[4]));
        }


        List<String> columns = tableData.get(DBService.COLUMN_NAME__COLUMNS);
        if (columns != null) {

            int i = 0;
            List<Column> tableColumns = new ArrayList<>();
            for (String column: columns) {

                Column columnRef = new Column();
                columnRef.setTable(tableRef);

                columnRef.setOrderIndex(i);
                columnRef.setUpdateIndex(-1);
                i++;
                
                String[] columnInfo = column.split("\\$\\$\\$\\$");
                int length = columnInfo.length;

                if (length > 0) {
                    columnRef.setName(columnInfo[0]);
                    columnRef.setOriginalName(columnInfo[0]);
                }

                if (length > 1) {
                    columnRef.setDataType(service.getDatatype(columnInfo[1]));
                    String[] strs = columnInfo[1].split(" ");
                    if (strs.length > 1 && "unsigned".equalsIgnoreCase(strs[strs.length - 1])) {
                        columnRef.setUnsigned(true);
                    }
                }

                if (length > 2) {
                    columnRef.setNotNull("NOT NULL".equalsIgnoreCase(columnInfo[2]));
                }

                if (length > 3) {
                    columnRef.setAutoIncrement("auto_increment".equalsIgnoreCase(columnInfo[3].trim()));
                }

                if (length > 4) {
                    columnRef.setDefaults(columnInfo[4]);
                }

                if (length > 5) {
                    columnRef.setCharacterSet(columnInfo[5]);
                }

                if (length > 6) {
                    columnRef.setCollation(columnInfo[6]);
                }

                tableColumns.add(columnRef);
            }

            tableRef.setColumns(tableColumns);
        }

        List<String> indexes = tableData.get(DBService.COLUMN_NAME__INDEXES);
        if (indexes != null) {

            List<Index> tableIndexes = new ArrayList<>();
            for (String index: indexes) {

                // its just an index
                if (index.startsWith("BTREE ")) {
                    index = index.substring(6);

                }

                boolean primary = index.startsWith("PRIMARY(");

                Index indexRef = new Index();
                indexRef.setTable(tableRef);
                indexRef.setType(primary ? IndexType.PRIMARY : null);

                if (primary) {

                    indexRef.setName("PRIMARY");

                    String primaryName = index.substring(8, index.length() - 1);
                    String[] cols = primaryName.split(",");
                    Column[] indexColumns = new Column[cols.length];
                    int i = 0;
                    for (String c: cols) {
                        if (c.contains("(")) {
                            String strLength = c.substring(c.indexOf("(") + 1, c.indexOf(")"));

                            c = c.substring(0, c.indexOf("(")).trim();

                            try {
                                indexRef.setColumnLength(c, Integer.valueOf(strLength));
                            } catch (NumberFormatException ex) {
                                log.error(ex.getMessage(), ex);
                            }
                        }
                        
                        Column primaryColumn = tableRef.getColumnByName(c.trim());

                        if (primaryColumn != null) {
                            primaryColumn.setPrimaryKey(true);
                        }
                        indexColumns[i] = primaryColumn;
                        i++;
                    }

                    indexRef.setColumns(Arrays.asList(indexColumns));

                } else {

                    if (index.startsWith("FULLTEXT ")) {
                        indexRef.setType(IndexType.FULL_TEXT);
                        index = index.substring(9);
                    } else if (index.startsWith("SPATIAL ")) {
                        indexRef.setType(IndexType.SPATIAL);
                        index = index.substring(8);
                    } else if (index.startsWith("UNIQUE ")) {
                        indexRef.setType(IndexType.UNIQUE_TEXT);
                        index = index.substring(7);
                    }

                    int startIndex = index.indexOf("(");
                    indexRef.setName(index.substring(0, startIndex));


                    String[] idxColumns = index.substring(startIndex + 1, index.length() - 1).split(",");
                    if (idxColumns != null) {

                        List<Column> columnsList = new ArrayList<>();
                        for (String idxColumn: idxColumns) {
                            
                            if (idxColumn.contains("(")) {
                                String strLength = idxColumn.substring(idxColumn.indexOf("(") + 1, idxColumn.indexOf(")"));

                                idxColumn = idxColumn.substring(0, idxColumn.indexOf("(")).trim();

                                try {
                                    indexRef.setColumnLength(idxColumn, Integer.valueOf(strLength));
                                } catch (NumberFormatException ex) {
                                    log.error(ex.getMessage(), ex);
                                }
                            }
                            
                            Column c = tableRef.getColumnByName(idxColumn.trim());
                            if (c != null) {
                                columnsList.add(c);
                            }
                        }

                        indexRef.setColumns(columnsList);
                    }
                }
                if (primary) {
                    tableIndexes.add(0, indexRef);
                } else {
                    tableIndexes.add(indexRef);
                }
            }

            tableRef.setIndexes(tableIndexes);
        } 

        List<String> foreignkeys = tableData.get(DBService.COLUMN_NAME__FOREIGNS);
        if (foreignkeys != null) {

            List<ForeignKey> tableForeignKeys = new ArrayList<>();
            for (String foreignkey: foreignkeys) {

                Matcher m = PATTERN__FOREIGN_KEY.matcher(foreignkey);
                if (m.find()) {

                    ForeignKey fk = new ForeignKey();
                    fk.setReferenceDatabase(database);
                    fk.setName(m.group("name"));

                    String[] cols = m.group("columns").split(",");
                    Column[] fkColumns = new Column[cols.length];
                    int i = 0;
                    for (String c: cols) {
                        fkColumns[i] = tableRef.getColumnByName(c.trim());
                        i++;
                    }

                    fk.setColumns(Arrays.asList(fkColumns));

                    String refTableName = m.group("refTable").toLowerCase();
                    DBTable refTable = tableMap.get(refTableName);                            
                    fk.setReferenceTable(refTableName);


                    String[] refCols = m.group("refColumns").split(",");
                    Column[] fkRefColumns = new Column[refCols.length];
                    i = 0;
                    for (String c: refCols) {
                        Column col = refTable != null ? refTable.getColumnByName(c.trim()) : null;
                        if (col == null) {
                            col = new Column();
                            col.setName(c.trim());
                            col.setOriginalName(c.trim());
                        }

                        fkRefColumns[i] = col;
                        i++;
                    }
                    fk.setReferenceColumns(Arrays.asList(fkRefColumns));

                    tableForeignKeys.add(fk);
                } else {
                    log.warn("CAN NOT PARSE FOREIGN KEY - need check!!!: " + foreignkey);
                }                        
            }

            tableRef.setForeignKeys(tableForeignKeys);
        }
        
        return tableRef;
    }
    
    
    private void collectAutocompleteData(String params, List<String> currentDatabases) {
        if (keywordsKeys.isEmpty()) {
            // 1. Keywords from keywords.properties at smart-sql-model
            try {
                Properties p = new Properties();
                p.load(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/model/tech/mysql/keywords.properties"));

                Set<String> keys = (Set)p.keySet();
                if (keys != null) {
                    keywordsKeys.addAll(keys);
                }
            } catch (Throwable ex) {
                log.error("Error while reading keywords from keywords.properties", ex);
            }
        }
        
        if (serverFunctionsKeys.isEmpty()) {
            // 1. Keywords from keywords.properties at smart-sql-model
            try {
                Properties p = new Properties();
                p.load(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/model/tech/mysql/functions.properties"));

                Set<String> keys = (Set)p.keySet();
                if (keys != null) {
                    serverFunctionsKeys.addAll(keys);
                }
            } catch (Throwable ex) {
                log.error("Error while reading serverFunctionsKeys from functions.properties", ex);
            }
        }

        if (params != null) {
            // 2. Table, column, function, procedure, triger names from db
            try {
                
//                Map<String, Map<String, Object>> data = session.getService().loadDBStructure();
                
                List<Database> dbs = DatabaseCache.getInstance().getDatabases(params);//service.syncDatabases(data, session.getConnectionParam(), (AbstractDBService) session.getService());
                
                if (dbs != null) {
                    
                    List<Object> listDatabase = new ArrayList<>();
                    databasesKeys.put(params, listDatabase);
                    
                    List<Object> listTables = new ArrayList<>();
                    tablesKeys.put(params, listTables);
                    
                    List<Object> listColumns = new ArrayList<>();
                    columnsKeys.put(params, listColumns);
                    
                    List<Object> listViews = new ArrayList<>();
                    viewsKeys.put(params, listViews);
                    
                    List<Object> listProcedures = new ArrayList<>();
                    proceduresKeys.put(params, listProcedures);
                    
                    List<Object> listFunctions = new ArrayList<>();
                    functionsKeys.put(params, listFunctions);
                    
                    List<Object> listTriggers = new ArrayList<>();
                    triggersKeys.put(params, listTriggers);
                    
                    List<Object> listEvents = new ArrayList<>();
                    eventsKeys.put(params, listEvents);
                    
                    // find curretn db
                    for (Database db : dbs) {
                        if (currentDatabases == null 
                            || currentDatabases.isEmpty()
                            || currentDatabases.contains(db.getName())) {
                            
                            listDatabase.add(db);

                            // 2.1 read all table names
                            List<DBTable> tables = db.getTables();//session.getService().getTables(db);
                            if (tables != null) {
                                for (DBTable t : tables) {
                                    listTables.add(t);

                                    // read columns
                                    List<Column> columns = t.getColumns();
                                    if (columns != null) {
                                        listColumns.addAll(columns.stream().map(c -> {c.setTable(t); return c;}).collect(Collectors.toSet()));
                                    }
                                }
                            }

                            List<View> views = db.getViews();//session.getService().getViews(db);
                            if (views != null) {
                                for (View v : views) {
                                    listViews.add(v);

//                                    try {
                                        List<Column> columns = v.getColumns();//session.getService().getColumnInformation(db.getName(), v.getName());
                                        // read columns
                                        if (columns != null) {
                                            listColumns.addAll(columns.stream().map((Column c) -> {
                                                c.setView(v);
                                                return c;
                                            }).collect(Collectors.toSet()));
                                        }
//                                    } catch (SQLException ex) {
//                                    }
                                }

                            }

                            // 2.2 read all procedures
                            List<StoredProc> procedures = db.getStoredProc();//session.getService().getStoredProcs(db);
                            if (procedures != null) {
                                listProcedures.addAll(procedures.stream().map(p -> p).collect(Collectors.toSet()));
                            }

                            // 2.3 read all functions
                            List<Function> functions = db.getFunctions();//session.getService().getFunctions(db);
                            if (functions != null) {
                                listFunctions.addAll(functions.stream().map(f -> f).collect(Collectors.toSet()));
                            }

                            // 2.4 read all triggers
                            List<Trigger> triggers = db.getTriggers();//session.getService().getTriggers(db);
                            if (triggers != null) {
                                listTriggers.addAll(triggers.stream().map(t -> t).collect(Collectors.toSet()));
                            }

                            // 2.5 read all events
                            List<Event> events = db.getEvents();//session.getService().getEvents(db);
                            if (events != null) {
                                listEvents.addAll(events.stream().map(t -> t).collect(Collectors.toSet()));
                            }
                        }
                    }
                }
            } catch (Throwable ex) {
                log.error("Error while reading table names from current connection", ex);
            }
        }
    }
    
    
    
    public void columnDroped(String params, Column column) {
        if (column != null && column.getTable() != null && column.getTable().getDatabase() != null) {
            Map<String, Map<String, DBTable>> map = tablesCache.get(params);
            if (map != null) {
                Map<String, DBTable> tables = map.get(column.getTable().getDatabase().getName());
                if (tables != null) {
                    DBTable t = tables.get(column.getTable().getName());
                    if (t != null && t.getColumns() != null) {
                        for (Column c: t.getColumns()) {
                            if (c.getName().equalsIgnoreCase(column.getName())) {
                                t.getColumns().remove(c);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void indexDroped(String params, Index index) {
        if (index != null && index.getTable() != null && index.getTable().getDatabase() != null) {
            Map<String, Map<String, DBTable>> map = tablesCache.get(params);
            if (map != null) {
                Map<String, DBTable> tables = map.get(index.getTable().getDatabase().getName());
                if (tables != null) {
                    DBTable t = tables.get(index.getTable().getName());
                    if (t != null && t.getIndexes() != null) {
                        for (Index i: t.getIndexes()) {
                            if (i.getName().equalsIgnoreCase(index.getName())) {
                                t.getIndexes().remove(i);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void tableDroped(String params, DBTable table) {
        if (table != null && table.getDatabase() != null) {
            Map<String, Map<String, DBTable>> map = tablesCache.get(params);
            if (map != null) {
                Map<String, DBTable> tables = map.get(table.getDatabase().getName());
                if (tables != null) {
                    tables.remove(table.getName());
                }
            }
        }
    }
    
    public void databaseDroped(String params, Database database) {
        if (database != null) {
            Map<String, Map<String, DBTable>> map = tablesCache.get(params);
            if (map != null) {
                map.remove(database.getName());
            }
            
            Map<String, Database> map0 = databasesCache.get(params);
            map0.remove(database.getName());            
        }
    }
    
    public void eventDroped(String params, Event event) {
        if (event != null) {
            List<Object> list = eventsKeys.get(params);
            if (list != null) {
                list.remove(event);
            }     
        }
    }
    
    public void functionDroped(String params, Function function) {
        if (function != null) {
            List<Object> list = functionsKeys.get(params);
            if (list != null) {
                list.remove(function);
            }     
        }
    }
    
    public void storedProcDroped(String params, StoredProc storedProc) {
        if (storedProc != null) {
            List<Object> list = proceduresKeys.get(params);
            if (list != null) {
                list.remove(storedProc);
            }     
        }
    }
    
    public void triggerDroped(String params, Trigger trigger) {
        if (trigger != null) {
            List<Object> list = triggersKeys.get(params);
            if (list != null) {
                list.remove(trigger);
            }     
        }
    }
    
    public void viewDroped(String params, View view) {
        if (view != null) {
            List<Object> list = viewsKeys.get(params);
            if (list != null) {
                list.remove(view);
            }     
        }
    }
    
    
    private DatabaseCache() {
    }
    
    public synchronized static DatabaseCache getInstance() {
        return DatabaseCacheHolder.INSTANCE;
    }
    
    private static class DatabaseCacheHolder {

        private static final DatabaseCache INSTANCE = new DatabaseCache();
    }
}
