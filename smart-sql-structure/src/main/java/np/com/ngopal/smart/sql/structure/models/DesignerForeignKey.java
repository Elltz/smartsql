package np.com.ngopal.smart.sql.structure.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerForeignKey {

    public enum OnAction {
        RESTRICT("RESTRICT"),
        CASCADE("CASCADE"),
        SET_NULL("SET NULL"),
        NO_ACTION("NO ACTION");
        
        private final String name;
        OnAction(String name) {
            this.name = name;
        }
        
        public static OnAction fromString(String name) {
            if (name != null) {
                switch (name) {
                    case "RESTRICT":
                        return RESTRICT;
                    
                    case "CASCADE":
                        return CASCADE;
                        
                    case "SET NULL":
                        return SET_NULL;
                        
                    case "NO ACTION":
                        return NO_ACTION;
                }
            }
            
            return null;
        }
        

        @Override
        public String toString() {
            return name;
        }
    }
    
    private SimpleStringProperty uuid;
    
    private SimpleStringProperty name;
    private SimpleObjectProperty<DesignerTable> referencedTable;
    
    private ObservableList<DesignerColumn> columns;

    private ObservableMap<DesignerColumn, DesignerColumn> referencedColumns;
    
    private SimpleObjectProperty<OnAction> onUpdate;
    private SimpleObjectProperty<OnAction> onDelete;
    private SimpleBooleanProperty skipSqlGeneration;
    private SimpleStringProperty comment;
    
    public DesignerForeignKey() {
        uuid = new SimpleStringProperty(UUID.randomUUID().toString());
        name = new SimpleStringProperty();
        referencedTable = new SimpleObjectProperty<>();
        columns = FXCollections.observableArrayList();
        referencedColumns = FXCollections.observableHashMap();
        onUpdate = new SimpleObjectProperty<>(OnAction.NO_ACTION);
        onDelete = new SimpleObjectProperty<>(OnAction.NO_ACTION);
        skipSqlGeneration = new SimpleBooleanProperty(false);
        comment = new SimpleStringProperty();
    }
    
    public DesignerForeignKey(String dfk_name, String dfk_comment, DesignerTable refTable,
            ArrayList<DesignerColumn> dfk_column, HashMap<DesignerColumn, DesignerColumn> refColumn,
            OnAction dfk_onUpdate, OnAction dfk_onDelete, boolean skipSql,String uuid) {
        name = new SimpleStringProperty(dfk_name);
        referencedTable = new SimpleObjectProperty<>(refTable);
        columns = FXCollections.observableArrayList(dfk_column);
        referencedColumns = FXCollections.observableHashMap();
        referencedColumns.putAll(refColumn);
        onUpdate = new SimpleObjectProperty<>(dfk_onUpdate);
        onDelete = new SimpleObjectProperty<>(dfk_onDelete);
        skipSqlGeneration = new SimpleBooleanProperty(skipSql);
        comment = new SimpleStringProperty(dfk_comment);
        this.uuid = new SimpleStringProperty(uuid);
    }
    
    public SimpleStringProperty uuid() {
        return uuid;
    }
    
    public SimpleStringProperty name() {
        return name;
    }
    
    public SimpleObjectProperty<DesignerTable> referencedTable() {
        return referencedTable;
    }
    
    public ObservableList<DesignerColumn> columns() {
        return columns;
    }
    
    public ObservableMap<DesignerColumn, DesignerColumn> referencedColumns() {
        return referencedColumns;
    }
    
    public SimpleObjectProperty<OnAction> onUpdate() {
        return onUpdate;
    }
    
    public SimpleObjectProperty<OnAction> onDelete() {
        return onDelete;
    }
    
    public SimpleBooleanProperty skipSqlGeneration() {
        return skipSqlGeneration;
    }
    
    public SimpleStringProperty comment() {
        return comment;
    }
    
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid.get());
        map.put("name", name.get());
        
        if (onUpdate.get() != null) {
            map.put("onUpdate", onUpdate.get().name());
        }
        
        if (onDelete.get() != null) {
            map.put("onDelete", onDelete.get().name());
        }
        
        map.put("skipSqlGeneration", skipSqlGeneration.get());
        map.put("comment", comment.get());
        
        if (referencedTable.get() != null) {
            map.put("referencedTable", referencedTable.get().uuid().get());
        }
        
        List<String> listColumns = new ArrayList<>();
        map.put("columns", listColumns);
        
        for (DesignerColumn c: columns) {
            listColumns.add(c.uuid().get());
        }
        
        
        Map<String, String> referencedColumns0 = new HashMap<>();
        map.put("referencedColumns", referencedColumns0);
        
        for (DesignerColumn c: referencedColumns.keySet()) {
            if (referencedColumns.get(c) != null) {
                referencedColumns0.put(c.uuid().get(), referencedColumns.get(c).uuid().get());
            }
        }
                
        return map;
    }
    
    public void fromMap(Map<String, Object> map, DesignerTable table, Map<String, DesignerTable> tables) {
        if (map != null) {
            uuid.set((String)map.get("uuid"));
            name.set((String)map.get("name"));
            
            String onUpd = (String)map.get("onUpdate");
            if (onUpd != null) {
                onUpdate.set(OnAction.valueOf(onUpd));
            }
            
            String onDlt = (String)map.get("onDelete");
            if (onDlt != null) {
                onDelete.set(OnAction.valueOf(onDlt));
            }
            
            skipSqlGeneration.set((Boolean)map.get("skipSqlGeneration"));
            comment.set((String)map.get("comment"));
            
            String refTable = (String) map.get("referencedTable");
            if (refTable != null) {
                
                DesignerTable t = tables.get(refTable);
                referencedTable.set(t);
                
                List<String> listColumns = (List<String>)map.get("columns");
                if (columns != null) {
                    for (String columnUuid: listColumns) {
                        DesignerColumn c = table.findColumnByUuid(columnUuid);
                        if (c != null) {
                            columns.add(c);
                        }
                    }
                }
                
                Map<String, String> referencedColumns0 = (Map<String, String>)map.get("referencedColumns");
                if (referencedColumns0 != null) {
                    for (String key: referencedColumns0.keySet()) {
                        DesignerColumn keyColumn = table.findColumnByUuid(key);
                        DesignerColumn valueColumn = t.findColumnByUuid(referencedColumns0.get(key));
                        
                        // TODO dont know what need to do if some of them null
                        // but this cant be
                        if (keyColumn != null && valueColumn != null) {
                            referencedColumns.put(keyColumn, valueColumn);
                        }
                    }
                }
            }
        }
    }
}
