package np.com.ngopal.smart.sql.structure.utils;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.sql.SQLException;
import java.util.Arrays;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public final class ErrorInfoProvider {
        
    private ErrorInfoProvider() {};
    
    // TODO at now this is a stub
    public static final ErrorInfo getErrorInfoByThrowable(ConnectionParams params, Throwable th) {
        ErrorInfo ei = null;
        
        if (th instanceof CommunicationsException && ((CommunicationsException)th).getErrorCode() == 0) {
            ei = processErrorNo2003(params, new SQLException(null, "SQL:2003", 2003, th));
            
        } else if (th instanceof SQLException) {
            SQLException ex = (SQLException)th;
            
            switch (ex.getErrorCode()) {
                
                // TODO Add cases
                case 1045:
                    ei = processErrorNo1045(params, ex);
                    break;
                
                case 2003:
                    ei = processErrorNo2003(params, ex);
                    break;
                    
                case 1142:
                    ei = processErrorNo1142(params, ex);
                    break;
                    
                // TODO remove default
                default:
//                    ei = new ErrorInfo(th);
//                    ei.setErrorCode(ex.getErrorCode());
//                    ei.setErrorMessage(ex.getMessage());
//                    ei.setRootCause("Stub root cause");
//                    ei.setDebugInfo("Stub debug info");
//                    ei.setUsefulWebsites(
//                        Arrays.asList(
//                            new String[] {
//                                "http://stub.useful.website0", 
//                                "http://stub.useful.website1", 
//                                "http://stub.useful.website2"
//                            }
//                        )
//                    );
                    break;
            }
        } 
        
        return ei;
    }
    
    
    private static ErrorInfo processErrorNo1045(ConnectionParams params, SQLException ex) {
        String msg = ex.getMessage();
                    
        // in this case params null, and we need get info from error message
        String user = msg.substring(msg.indexOf("'") + 1);
        user = user.substring(0, user.indexOf("'"));

        String host = msg.substring(msg.indexOf("@'") + 2);
        host = host.substring(0, host.indexOf("'"));

        ErrorInfo ei = new ErrorInfo(ex);
        ei.setErrorCode(ex.getErrorCode());
        ei.setErrorMessage("Access denied for user '" + user + "'@'" + host + "'\n(Using Password yes)");
        ei.setRootCause(
            "Case 1) Wrong password\n" +
            "Case 2) Wrong User"
        );

        ei.setDebugInfo(
            "Case 1) Debug wrong password\n" + 
            "   Step 1) Login with admin/root\n" +
            "   Step 2) Execute the following two queries\n" +
            "       Query1: select password  from mysql.user where user = '" + user + "' and host = '" + host + "'\n" +
            "       Query2: select password('Please copy the password what you entered');\n" +
            "   Step 3) If Query1 and Query2 return different result set then you entered wrong password\n" +
            "Case 2) Wrong User\n" +
            "   Step 1) Login with admin/root\n" +
            "   Step 2) Execute the following two queries\n" +
            "       Query3) select user  from mysql.user where user = '" + user + "'  and host = '" + host + "'\n" +
            "   Step 3) If Query3 didn't return any record then entered wrong user"
        );

        ei.setSolutionInfo(
            "Case 1) Debug wrong password\n" +
            "   Reset the user password with help of DBA using the following command\n" +
            "       update mysql.user set passwrod=passwrod('New passwrod') where user = '" + user + "' and host = '" + host + "'\n" +
            "Case 2) Wrong User\n" +
            "   Get the right user name or create new DB user with help DBA.\n" +
            "1. Query for find out all user in DB.\n" +
            "   Query1:  Select user, host from mysql.user\n" +
            "2. Create New user\n" +
            "   Query2: grant privileges on database.tables to user@'host' identified by 'passwrord'; Flush privileges;"
        );

        ei.setUsefulWebsites(
            Arrays.asList(
                new String[] {
                    "https://dev.mysql.com/doc/refman/5.7/en/problems-connecting.html",
                    "http://stackoverflow.com/questions/20353402/access-denied-for-user-testlocalhost-using-password-yes-except-root-user",
                    "http://dba.stackexchange.com/questions/10852/mysql-error-access-denied-for-user-alocalhost-using-password-yes",
                    "http://forums.mysql.com/read.php?52,591816,591816"
                }
            )
        );
        
        return ei;
    }
    
    private static ErrorInfo processErrorNo2003(ConnectionParams params, SQLException ex) {
        String msg = ex.getMessage();
                    
        // in this case params null, and we need get info from error message

        String host = "";
        if (params != null) {
            host = params.getAddress();
            if (msg != null){
                host = msg.substring(msg.indexOf("'") + 1);
                host = host.substring(0, host.indexOf("'"));
            }
        }

        ErrorInfo ei = new ErrorInfo(ex);
        ei.setErrorCode(ex.getErrorCode());
        ei.setErrorMessage("Can't connect to MySQL Server on '" + host + "'(0)");
        ei.setRootCause("Simply means that connection is not possible for one of the following (or similar) reasons:\n" +
            "   ● There is no MySQL server running at the specified host.\n" +
            "   ● Connection to the MySQL server is not allowed using" + 
            " TCP/IP. Check the 'skip-networking' setting in the MySQL" + 
            " configuration file (my.ini on Windows, my.cnf on Unix/Linux)." + 
            "  It shall be commented out like '#skip-networking'. If it" + 
            " is not commented out, then do it and restart the MySQL" +
            " server for the change to take effect. MONyog needs to connect" +
            "  using TCP/IP.\n" +
            "   ● When trying to connect to a MySQL server at an ISP this" + 
            " error message often indicates that direct connection to MySQL" +
            " has been blocked. You must then use SSH-tunneling to connect.\n" +
            "   ● Some networking issue prevents connection. It could be a" + 
            " network malconfiguration or a firewall issue. We have" +
            " experienced sometimes that some firewalls (ZoneAlarm in" +
            " particular) is blocking TCP/IP connections even if it claims" +
            " to be disabled. Most often it will help to uninstall and" + 
            "reinstall the firewall."
        );
//
//        ei.setSolutionInfo(
//            "Case1: DB server not accessible\n" +
//            "   Contact to your Network administration and Database administration they can grant required privileges\n" +
//            "Case 2: DB server port not accessible\n" +
//            "   Contact to your Network administration and Database administration they can grant required privileges"
//        );
//
//        ei.setUsefulWebsites(
//            Arrays.asList(
//                new String[] {
//                    "http://dev.mysql.com/doc/refman/5.7/en/can-not-connect-to-server.html",
//                    "http://stackoverflow.com/questions/17426052/error-2003-hy000-cant-connect-to-mysql-server-on-localhost-10061",
//                    "http://stackoverflow.com/questions/119008/cant-connect-to-mysql-server-on-localhost-10061"
//                }
//            )
//        );
        
        return ei;
    }
    
    private static ErrorInfo processErrorNo1142(ConnectionParams params, SQLException ex) {
        
        ErrorInfo ei = new ErrorInfo(ex);
        ei.setErrorCode(ex.getErrorCode());
        ei.setErrorMessage("Access dined; you need (at least on of) th RELOAD privilege(s) for this operation");        
        return ei;
    }
}
