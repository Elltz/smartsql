package np.com.ngopal.smart.sql.structure.components;

import com.sun.javafx.scene.control.skin.ComboBoxPopupControl;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Skin;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DatePicker extends javafx.scene.control.DatePicker {
    
    public static final String DEFAUL_FORMAT                                    = "dd.MM.yyyy";
    
    private final SimpleStringProperty format = new SimpleStringProperty(DEFAUL_FORMAT);
    
    
    public DatePicker()
    {
        this((LocalDate)null);
    }
    
    
    public DatePicker(Date date)
    {
        this(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }
    
    
    public DatePicker(LocalDate localDate)
    {        
        
        getStyleClass().add("GFA-DatePicker");
        
        if (localDate != null)
        {
            setValue(localDate);
        }
        
        
        
        // set custom string converter for show date-time 
        // value string in textfield
        setConverter(createConverter());
        
        
        
        addEventFilter(KeyEvent.KEY_RELEASED, (final KeyEvent event) ->
        {
            if (event.getCode() == KeyCode.SPACE && event.isControlDown())
            {
                show();
            }
        });
        addEventFilter(MouseEvent.MOUSE_CLICKED, (final MouseEvent event) ->
        {
            if (event.getClickCount() == 2 && event.getButton() == MouseButton.PRIMARY)
            {
                show();
            }
        });
        
        
        getEditor().addEventFilter(KeyEvent.KEY_RELEASED, (final KeyEvent event) ->
        {
            if (!event.getText().isEmpty())
            {
                String text = getEditor().getText();
                if (text.trim().isEmpty())
                {
                    clearDateValue();
                }
                else
                {
                    try
                    {
                        getConverter().fromString(text);
                    }
                    catch (DateTimeParseException ex)
                    {
                        // do nothing its ok
                    }
                }
            }
        });
    }

    @Override
    public void requestFocus()
    {
        super.requestFocus();
        
        ((ComboBoxPopupControl.FakeFocusTextField)getEditor()).setFakeFocus(true);
    }
    
    
    
    protected void clearDateValue()
    {
        setValue(null);
    }
    
    
    @Override
    protected Skin<?> createDefaultSkin()
    {
        // return own custom skin
        return new DatePickerSkin(this);
    }
    
    
    
    
    protected StringConverter<LocalDate> createConverter()
    {
        return new DateStringConverter();
    }
    
    
    
    
    public void destroyComponent() {
    }
    
    
    
    
    public final String getFormat()
    {
        return formatProperty().get();
    }
    public final void setFormat(final String format)
    {
        formatProperty().set(format);
    }

    public final StringProperty formatProperty()
    {
        return format;
    }
    
    
    
    
    
    
    
    private class DateStringConverter extends StringConverter<LocalDate>
    {
        private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAUL_FORMAT);
        
        private final ChangeListener<? super String> formatListener;
        
        private DateStringConverter()
        {
            formatListener = (final ObservableValue<? extends String> observable, final String oldValue, final String newValue) ->
            {
                // recreate formatter instance
                try
                {
                    formatter = DateTimeFormatter.ofPattern(newValue);
                }
                catch (final IllegalArgumentException invalid)
                {
                    // if format string invalid - use default format
                    formatter = DateTimeFormatter.ofPattern(DEFAUL_FORMAT);
                }
            };
                        
            formatProperty().addListener(formatListener);
            
        }
        
        
        @Override
        public String toString(final LocalDate object)
        {           
            final LocalDate date = getValue();

            if (date != null)
            {
                return date.format(formatter);
            }
            else
            {
                return "";
            }
        }

        
        @Override
        public LocalDate fromString(final String string)
        {
            LocalDate dateTime = null;

            if (string != null && !string.isEmpty())
            {
                dateTime = LocalDate.parse(string, formatter);
            }


            setValue(dateTime);


            return getValue();
        }
    }
    
    
}
