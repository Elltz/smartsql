package np.com.ngopal.smart.sql.structure.queryutils;

import java.util.Arrays;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import lombok.AccessLevel;
import lombok.Getter;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLFunctions;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLKeywords;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.modes.SQLTokenMaker;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SmartSQLTokenMaker extends SQLTokenMaker {

    private final List<String> KEYWORDS;
    private final List<String> FUNCITONS;
    
    public static SimpleObjectProperty<ConnectionSession> smartSQLTokenMakerConnectionSession = new SimpleObjectProperty();    
    
    public SmartSQLTokenMaker() {
        super();
        KEYWORDS = Arrays.asList(MySQLKeywords.getTypesArray());
        FUNCITONS = Arrays.asList(MySQLFunctions.FUNCTIONS.toArray(new String[0]));
    }
    
    @Override
    public void addToken(char[] array, int start, int end, int tokenType, int startOffset) {
        // TODO Need try check if this our tokens - then need add them as highlighted tockens
        try {
            if (tokenType == Token.IDENTIFIER) {

                String token = new String(array, start, end - start + 1);

                // in this point we can add others conditions 
                // for our keywords for highlighting
                if (KEYWORDS.contains(token.toUpperCase())) {
                    tokenType = Token.RESERVED_WORD;

                } else if (FUNCITONS.contains(token.toUpperCase())) {
                    tokenType = Token.FUNCTION;

                } else {
                    
                    List<String> engines = (List<String>)(List)DatabaseCache.getInstance().getEnginesKeys(
                            smartSQLTokenMakerConnectionSession.get().getConnectionParam().cacheKey());
                    if (engines != null) {
                        for (String e: engines) {
                            if (e.equalsIgnoreCase(token)) {
                                tokenType = Token.RESERVED_WORD;
                                break;
                            }
                        }
                    }
                    
                    if (tokenType == Token.IDENTIFIER) {
                    
                        List<String> greenList = QueryUtils.highlightGreenMap.get(smartSQLTokenMakerConnectionSession.get());
                        if (greenList != null && greenList.contains(token.toLowerCase())) {
                            tokenType = Token.RESERVED_WORD_2;

                        } else {
                            List<String> maroonList = QueryUtils.highlightMaroonMap.get(smartSQLTokenMakerConnectionSession.get());
                            if (maroonList != null && maroonList.contains(token.toLowerCase())) {
                                tokenType = Token.VARIABLE;
                            }
                        }
                    }
                }

                super.addToken(array, start, end, tokenType, startOffset);
            } else {
                super.addToken(array, start, end, tokenType, startOffset);
            }
        } catch (Throwable th) {
        }
    }

    
    
    
}
