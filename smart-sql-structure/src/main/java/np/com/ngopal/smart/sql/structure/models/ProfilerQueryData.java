
package np.com.ngopal.smart.sql.structure.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ProfilerQueryData implements FilterableData, GraphData, DebugExplainProvider {
    
    private final SimpleStringProperty user;
    private final SimpleStringProperty host;
    private final SimpleStringProperty queryTemplate;
    private final SimpleIntegerProperty queryId;
    private final SimpleObjectProperty threads;
    private final SimpleObjectProperty time;
    
    private final SimpleObjectProperty ptc;
    private final SimpleObjectProperty calls;
    private final SimpleObjectProperty avg;
    private final SimpleStringProperty database;
    private final SimpleStringProperty table;
    
    private final SimpleObjectProperty minResponceTime;
    private final SimpleObjectProperty maxResponceTime;
    private final SimpleObjectProperty avgResponceTime;
    private final SimpleObjectProperty stddevResponceTime;
    
    private final SimpleStringProperty graph1;
    private final SimpleStringProperty graph2;
    
    private final SimpleStringProperty recommendedIndex = new SimpleStringProperty();
    private final SimpleStringProperty recommendations = new SimpleStringProperty();
    
    private final SimpleStringProperty query;
    
    private Long rowsSum = null;
 
    public ProfilerQueryData(String user, String host, String queryTemplate, Integer queryId, BigDecimal threads, BigDecimal time, String graph1, String graph2, Double ptc, Long calls, Double avg, String database, String table, Double minResponceTime, Double maxResponceTime, Double avgResponceTime, Double stddevResponceTime, String query) {
        this.user = new SimpleStringProperty(user);
        this.host = new SimpleStringProperty(host);
        this.queryTemplate = new SimpleStringProperty(queryTemplate);
        this.queryId = new SimpleIntegerProperty(queryId);
        this.threads = new SimpleObjectProperty(threads);
        this.time = new SimpleObjectProperty(time);
        
        this.graph1 = new SimpleStringProperty(graph1);
        this.graph2 = new SimpleStringProperty(graph2);
        
        this.ptc = new SimpleObjectProperty(ptc);
        this.calls = new SimpleObjectProperty(calls);
        this.avg = new SimpleObjectProperty(avg);
        this.database = new SimpleStringProperty(database);
        this.table = new SimpleStringProperty(table);
        
        this.minResponceTime = new SimpleObjectProperty(minResponceTime);
        this.maxResponceTime = new SimpleObjectProperty(maxResponceTime);
        this.avgResponceTime = new SimpleObjectProperty(avgResponceTime);
        this.stddevResponceTime = new SimpleObjectProperty(stddevResponceTime);
        
        this.query = new SimpleStringProperty(query);
    }
    
    public String getQuery() {
        return query.get();
    }
    public void setQuery(String query) {
        this.query.set(query);
    }     
 
    @Override
    public String getGraph1() {
        return graph1.get();
    }
    @Override
    public void setGraph1(String graph1) {
        this.graph1.set(graph1);
    }     
    
    @Override
    public String getGraph2() {
        return graph2.get();
    }
    @Override
    public void setGraph2(String graph2) {
        this.graph2.set(graph2);
    }
 
    public void setRowsSum(Long rowsSum) {
        this.rowsSum = rowsSum;
    }
    public Long getRowsSum() {
        return rowsSum;
    }
    
    public String getUser() {
        return user.get();
    }
    public void setUser(String user) {
        this.user.set(user);
    }
        
    public String getHost() {
        return host.get();
    }
    public void setHost(String host) {
        this.host.set(host);
    }
    
    public String getQueryTemplate() {
        return queryTemplate.get();
    }
    public void setQueryTemplate(String queryTemplate) {
        this.queryTemplate.set(queryTemplate);
    }
    
    public Integer getQueryId() {
        return queryId.get();
    }
    public void setQueryId(Integer queryId) {
        this.queryId.set(queryId);
    }
    
    public BigDecimal getThreads() {
        return (BigDecimal) threads.get();
    }
    public void setThreads(BigDecimal threads) {
        this.threads.set(threads);
    }
    
    public BigDecimal getTime() {
        return (BigDecimal) time.get();
    }
    public void setTime(BigDecimal time) {
        this.time.set(time);
    }
    
    public Double getPtc() {
        return (Double) ptc.get();
    }
    public void setPtc(Double ptc) {
        this.ptc.set(ptc);
    }
    
    public Long getCalls() {
        return (Long) calls.get();
    }
    public void setCalls(Long calls) {
        this.calls.set(calls);
    }
    
    public String getDatabase() {
        return database.get();
    }
    public void setDatabase(String database) {
        this.database.set(database);
    }
    
    public String getTable() {
        return table.get();
    }
    public void setTable(String table) {
        this.table.set(table);
    }
    
    public Double getMinResponceTime() {
        return (Double) minResponceTime.get();
    }
    public void setMinResponceTime(Double minResponceTime) {
        this.minResponceTime.set(minResponceTime);
    }
    
    public Double getMaxResponceTime() {
        return (Double) maxResponceTime.get();
    }
    public void setMaxResponceTime(Double maxResponceTime) {
        this.maxResponceTime.set(maxResponceTime);
    }
    
    public Double getAvgResponceTime() {
        return (Double) avgResponceTime.get();
    }
    public void setAvgResponceTime(Double avgResponceTime) {
        this.avgResponceTime.set(avgResponceTime);
    }
    
    public Double getStddevResponceTime() {
        return (Double) stddevResponceTime.get();
    }
    public void setStddevResponceTime(Double stddevResponceTime) {
        this.stddevResponceTime.set(stddevResponceTime);
    }
    
    public Double getAvg() {
        return (Double) avg.get();
    }
    public void setAvg(Double avg) {
        this.avg.set(avg);
    }
    
    public String getRecommendedIndex() {
        return recommendedIndex.get();
    }
    public void setRecommendedIndex(String recommendedIndex) {
        this.recommendedIndex.set(recommendedIndex);
    } 
    
    public String getRecommendations() {
        return recommendations.get();
    }
    public void setRecommendations(String recommendations) {
        this.recommendations.set(recommendations);
    } 
    
    @Override
    public String[] getValuesFoFilter() {
        return new String[] {
            getUser() != null ? getUser() : "", 
            getHost() != null ? getHost() : "", 
            getQueryTemplate() != null ? getQueryTemplate() : "",
            getThreads() != null ? getThreads().setScale(3, RoundingMode.HALF_UP).toString() : "",
            getTime() != null ? getTime().toString() : "",
            
            getPtc() != null ? getPtc().toString() : "", 
            getCalls() != null ? getCalls().toString() : "", 
            getAvg() != null ? getAvg().toString() : "", 
            getTable() != null ? getTable() : "", 
             
            getMinResponceTime() != null ? getMinResponceTime().toString() : "", 
            getMaxResponceTime() != null ? getMaxResponceTime().toString() : "", 
            getAvgResponceTime() != null ? getAvgResponceTime().toString() : "", 
            getStddevResponceTime() != null ? getStddevResponceTime().toString() : ""
        };
    }
    
    public Object[] getValues() {
        return new Object[] {
            getUser(),
            getHost(),
            getQueryTemplate(),
            getQueryId(),
            getThreads(),
            getTime(),
            
            getPtc(),
            getCalls(),
            getAvg(),
            getTable(),
            
            getMinResponceTime(),
            getMaxResponceTime(),
            getAvgResponceTime(),
            getStddevResponceTime()
        };
    }
}