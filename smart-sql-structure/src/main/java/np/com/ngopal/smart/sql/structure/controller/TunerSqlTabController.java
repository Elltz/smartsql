package np.com.ngopal.smart.sql.structure.controller;

import java.awt.Desktop;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import static np.com.ngopal.smart.sql.structure.utils.MySQLTunnerUtil.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class TunerSqlTabController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    @FXML
    private TabPane tunerTabPane;
    
    private List<Tab> postLoadedTab;
    
    private Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>> nameCellFactory;
    private Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>> valueCellFactory;
    private Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>> messageCellFactory;
    private Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>> detailsCellFactory;
    
    private Map<String, FilterableTableView> performanceMetricsTables;
    
    private Tab performanceMetrics;
    private Tab securityRecommendations;
    private Tab replicationMetrics;
    private Tab performanceSchema;
    
    private List<Tab> specialTabs;
    
    private String[] NAMES = new String[] {"Performance Metrics", "Security Recommendations", "Replication Metrics", "Performance schema"};
    
    private MySQLTunnerUtil tunnerUtil;
    
    @FXML 
    public void exportTunerData() {
        
        Tab selectedTab = tunerTabPane.getSelectionModel().getSelectedItem();
        if (selectedTab != null) {
            
            FileChooser fc = new FileChooser();
            fc.setInitialFileName(selectedTab.getText());
            fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XLS files", "*.xls"));
            File xlsFile = fc.showSaveDialog(getStage());
            if (xlsFile != null) {

                String filename = xlsFile.getAbsolutePath();

                ExportUtils.ExportStatusCallback exportCallback = new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        DialogHelper.showError(getController(), "Error", errorMessage, th);
                    }

                    @Override
                    public void success() {
                        DialogResponce resp = DialogHelper.showConfirm(getController(), "Information",
                            "Data exported Successfully, Would you like to open file?");

                        if (resp == DialogResponce.OK_YES) {
                            try {
                                Desktop.getDesktop().open(new File(filename));
                            } catch (IOException ex) {
                                Platform.runLater(() -> {
                                    DialogHelper.showError(getController(), "Error", "There is an error while opening exported file", ex);
                                });
                            }
                        }
                    }
                };
                
                // @TODO Need refactor
                if (specialTabs.contains(selectedTab)) {
                    exportXLS(filename);
                    
                } else {
                    
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
                    columns.put("No", true);
                    columns.put("Status", true);
                    columns.put("Message", true);

                    List<Integer> widths = new ArrayList<>();
                    widths.add(50);
                    widths.add(25);
                    widths.add(250);

                    byte[] good;
                    try {
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        ImageIO.write(SwingFXUtils.fromFXImage(getStatusImage(TunerResult.Status.GOOD), null), "png", bos);

                        good = bos.toByteArray();
                    } catch (Throwable e) {
                        log.error("Error", e);
                        good = null;                
                    }

                    byte[] neutral;
                    try {
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        ImageIO.write(SwingFXUtils.fromFXImage(getStatusImage(TunerResult.Status.NEUTRAL), null), "png", bos);

                        neutral = bos.toByteArray();
                    } catch (Throwable e) {
                        log.error("Error", e);
                        neutral = null;                
                    }

                    byte[] bad;
                    try {
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        ImageIO.write(SwingFXUtils.fromFXImage(getStatusImage(TunerResult.Status.BAD), null), "png", bos);

                        bad = bos.toByteArray();
                    } catch (Throwable e) {
                        log.error("Error", e);
                        bad = null;                
                    }
                
                    ObservableList<ObservableList> rowsData = FXCollections.observableArrayList();
                    int i = 1;
                    for (TunerResult tr: (List<TunerResult>)((FilterableTableView)selectedTab.getContent()).getTable().getItems()) {

                        ObservableList row = FXCollections.observableArrayList();
                        row.add(i);

                        if (tr.getStatus() != null) {
                            switch (tr.getStatus()) {
                                case BAD:
                                    row.add(bad);
                                    break;

                                case NEUTRAL:
                                    row.add(neutral);
                                    break;

                                case GOOD:
                                    row.add(good);
                                    break;

                                default:
                                    row.add("");
                            }
                        } else {
                            row.add("");
                        }

                        row.add(tr.getMessage());

                        rowsData.add(row);
                        i++;
                    }            

                    ExportUtils.exportXLS(filename, rowsData, columns, widths, null, null, "UTF-8", exportCallback);
                }
            }
        }
    }
    
    @FXML 
    public void refreshTunerData() {        
        makeBusy(true);        
        tunerTabPane.getTabs().clear();        
        postLoadedTab = new ArrayList<>();
        specialTabs = new ArrayList<>();
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                try {
                    Map<String, List<TunerResult>> result = tunnerUtil.runTuner();
                    Platform.runLater(() -> {
                        // static positions for tabs
                        performanceMetrics = addFullTab("Performance Metrics", result.remove("Performance Metrics"));
                        securityRecommendations = addWithDetailsTab("Security Recommendations", result.remove("Security Recommendations"));
                        replicationMetrics = addWithDetailsTab("Replication Metrics", result.remove("Replication Metrics"));
                        performanceSchema = addTab("Performance schema", result.remove("Performance schema"));

                        specialTabs.add(performanceMetrics);
                        specialTabs.add(securityRecommendations);
                        specialTabs.add(replicationMetrics);
                        specialTabs.add(performanceMetrics);

                        Tab calculationTab = null;
                        for (String tabName : result.keySet()) {
                            Tab t = new Tab(tabName);

                            FilterableTableView table = new FilterableTableView();
                            table.getTable().setFixedCellSize(30.0);

                            initBaseTunerTable(table);

                            new TableViewCellCopyHandler<TunerResult>(table.getTable()) {
                                @Override
                                protected String copyCell(TunerResult row, TableColumn column) {
                                    switch (((Label) column.getGraphic()).getText()) {
                                        case "#":
                                            return "" + table.getTable().getSelectionModel().getSelectedIndex() + 1;
                                        case "Name":
                                            return row.getName();
                                        case "Value":
                                            return row.getValue();
                                        case "Status":
                                            return "";
                                        case "Message":
                                            return row.getMessage();
                                        case "Details":
                                            return row.getDetails();
                                    }
                                    return "";
                                }
                            };

                            List<TunerResult> list = result.get(tabName);
                            if (list != null) {
                                table.addItems(list);
                            } else {
                                // add listen tab
                                postLoadedTab.add(t);
                            }

                            t.setContent(table);

                            if ("Calculations".equals(tabName)) {
                                if (!table.getSourceData().isEmpty()) {
                                    calculationTab = t;
                                }
                            } else {
                                tunerTabPane.getTabs().add(t);
                            }
                        }

                        if (calculationTab != null) {
                            tunerTabPane.getTabs().add(calculationTab);
                        }

                        tunerTabPane.getSelectionModel().select(0);

                        makeBusy(false);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    makeBusy(false);
                    Platform.runLater(()-> DialogHelper.showError(getController(), "MySQL Tuner", "Failed proccessing your request",
                            null, getStage()));
                }
            }
        });
        
    }
    
    private Tab addFullTab(String name, List<TunerResult> list) {
        Tab t = new Tab(name);
                    
        Map<String, List<TunerResult>> map = new LinkedHashMap<>();
        
        int index = 1;
        for (TunerResult tr: list) {

            List<TunerResult> l = map.get(tr.getGroup());
            if (l == null) {
                l = FXCollections.observableArrayList();
                map.put(tr.getGroup(), l);
            }

            if (tr.getStatus() != null) {
                tr.setIndex(String.valueOf(index));
                index++;
                l.add(tr);
            } else {
                tr.setIndex("");
                index = 1;
            }
        }
        
        VBox vbox = new VBox(10);
        vbox.setFillWidth(true);
        
        performanceMetricsTables = new LinkedHashMap<>();
        
        for (String group: map.keySet()) {
                        
            VBox vb = new VBox(5);
            vb.setFillWidth(true);
            vb.setAlignment(Pos.CENTER);
            
            if (group != null) {
                Label title = new Label(group);
                title.setTextAlignment(TextAlignment.CENTER);
                title.setStyle("-fx-font-size: 18; -fx-font-weight: bold;");
                
                vb.getChildren().add(title);
            }
            
            FilterableTableView table = new FilterableTableView(false);
            table.setMaxHeight(30 * (map.get(group).size() + 1) + 10);
            initFullTunerTable(table);
            
            performanceMetricsTables.put(group, table);
            
            vb.getChildren().add(table);

            vbox.getChildren().add(vb);
            
            table.getTable().setFixedCellSize(30.0);

            new TableViewCellCopyHandler<TunerResult>(table.getTable()) {
                @Override
                protected String copyCell(TunerResult row, TableColumn column) {
                    switch (((Label)column.getGraphic()).getText()) {
                        case "#":
                            return "" + row.getIndex();
                        case "Name":
                            return row.getName();
                        case "Value":
                            return row.getValue();
                        case "Status":
                            return "";
                        case "Message":
                            return row.getMessage();
                        case "Details":
                            return row.getDetails();
                    }
                    return "";
                }            
            };
            
            table.addItems((ObservableList) map.get(group));
        }
        
        ScrollPane sp = new ScrollPane(vbox);
        sp.setFitToWidth(true);
        
        t.setContent(sp);
        tunerTabPane.getTabs().add(t);
        
        return t;
    }
    
    private Tab addTab(String name, List<TunerResult> list) {
        Tab t = new Tab(name);
                    
        FilterableTableView table = new FilterableTableView();
        initBaseTunerTable(table);
        
        table.getTable().setFixedCellSize(30.0);
        
        new TableViewCellCopyHandler<TunerResult>(table.getTable()) {
            @Override
            protected String copyCell(TunerResult row, TableColumn column) {
                switch (((Label)column.getGraphic()).getText()) {
                    case "#":
                        return "" + table.getTable().getSelectionModel().getSelectedIndex() + 1;
                    case "Name":
                        return row.getName();
                    case "Value":
                        return row.getValue();
                    case "Status":
                        return "";
                    case "Message":
                        return row.getMessage();
                    case "Details":
                        return row.getDetails();
                }
                return "";
            }            
        };

        if (list != null) {            
            table.addItems(list);
        } else {
            // add listen tab
            postLoadedTab.add(t);
        }

        t.setContent(table);
        tunerTabPane.getTabs().add(t);
        
        return t;
    }
    
    private Tab addWithDetailsTab(String name, List<TunerResult> list) {
        Tab t = new Tab(name);
                    
        FilterableTableView table = new FilterableTableView();
        initSecurityTunerTable(table);
        
        table.getTable().setFixedCellSize(30.0);
        
        new TableViewCellCopyHandler<TunerResult>(table.getTable()) {
            @Override
            protected String copyCell(TunerResult row, TableColumn column) {
                switch (((Label)column.getGraphic()).getText()) {
                    case "#":
                        return "" + table.getTable().getSelectionModel().getSelectedIndex() + 1;
                    case "Name":
                        return row.getName();
                    case "Value":
                        return row.getValue();
                    case "Status":
                        return "";
                    case "Message":
                        return row.getMessage();
                    case "Details":
                        return row.getDetails();
                }
                return "";
            }            
        };

        if (list != null) {
            table.addItems(list);
        } else {
            // add listen tab
            postLoadedTab.add(t);
        }

        t.setContent(table);
        tunerTabPane.getTabs().add(t);
        
        return t;
    }
    
    private void initFullTunerTable(FilterableTableView tableView) {
        TableColumn indexColumn = createColumn("#", "index");
        
        TableColumn nameColumn = createColumn("Name", "name");
        nameColumn.setCellFactory(nameCellFactory);
        
        TableColumn valueColumn = createColumn("Value", "value");
        valueColumn.setCellFactory(valueCellFactory);
        
        TableColumn typeColumn = createColumn("Status", "status");
        typeColumn.setCellFactory(new Callback<TableColumn<TunerResult, TunerResult.Status>, TableCell<TunerResult, TunerResult.Status>>() {
            @Override
            public TableCell<TunerResult, TunerResult.Status> call(TableColumn<TunerResult, TunerResult.Status> param) {
                return new TunerStatusCell();
            }
        });
        
        TableColumn messageColumn = createColumn("Message", "message");
        messageColumn.setCellFactory(messageCellFactory);
        
        TableColumn detailsColumn = createColumn("Details", "details");
        detailsColumn.setCellFactory(detailsCellFactory);
        
        tableView.addTableColumn(indexColumn, 50, 50, false); 
        tableView.addTableColumn(nameColumn, 250, 250, false);
        tableView.addTableColumn(valueColumn, 0.15);
        tableView.addTableColumn(typeColumn, 0.05, 50);
        tableView.addTableColumn(messageColumn, 0.28);
        tableView.addTableColumn(detailsColumn, 0.28);
       
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
    private void initSecurityTunerTable(FilterableTableView tableView) {
                
        TableColumn typeColumn = createColumn("Status", "status");
        typeColumn.setCellFactory(new Callback<TableColumn<TunerResult, TunerResult.Status>, TableCell<TunerResult, TunerResult.Status>>() {
            @Override
            public TableCell<TunerResult, TunerResult.Status> call(TableColumn<TunerResult, TunerResult.Status> param) {
                return new TunerStatusCell();
            }
        });
        
        TableColumn messageColumn = createColumn("Message", "message");
        messageColumn.setCellFactory(messageCellFactory);
        
        TableColumn detailsColumn = createColumn("Details", "details");
        detailsColumn.setCellFactory(detailsCellFactory);
        
        tableView.addTableColumn(typeColumn, 0.05, 50);
        tableView.addTableColumn(messageColumn, 0.45);
        tableView.addTableColumn(detailsColumn, 0.45);
       
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
    private void initBaseTunerTable(FilterableTableView tableView) {

        TableColumn typeColumn = createColumn("Status", "status");
        typeColumn.setCellFactory(new Callback<TableColumn<TunerResult, TunerResult.Status>, TableCell<TunerResult, TunerResult.Status>>() {
            @Override
            public TableCell<TunerResult, TunerResult.Status> call(TableColumn<TunerResult, TunerResult.Status> param) {
                return new TunerStatusCell();
            }
        });
        
        TableColumn messageColumn = createColumn("Message", "message");
        messageColumn.setCellFactory(messageCellFactory);
        
        tableView.addTableColumn(typeColumn, 0.05, 50);
        tableView.addTableColumn(messageColumn, 0.9);
       
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
      
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    public void init(UserUsageStatisticService usageService) {   
        
        usageService.savePUsage(PUsageFeatures.MST);
        
        tunnerUtil = new MySQLTunnerUtil(getController().getSelectedConnectionSession().get());
        refreshTunerData();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        nameCellFactory = new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(30, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setTooltip(new Tooltip(item));                        
                        setStyle("-fx-alignment: baseline-left; -fx-font-size: 16; -fx-font-family: serif; -fx-font-weight: normal");
                    }                    
                };
            }
        };
        
        valueCellFactory = new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(25, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setTooltip(new Tooltip(item));              
                        setStyle("-fx-alignment: baseline-left; -fx-font-size: 16; -fx-font-family: serif; -fx-font-weight: normal");
                    }                    
                };
            }
        };
        
        messageCellFactory = new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(55, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setTooltip(new Tooltip(item));                
                        setStyle("-fx-alignment: baseline-left; -fx-font-size: 16; -fx-font-family: serif; -fx-font-weight: normal");
                    }                    
                };
            }
        };
        
        detailsCellFactory = new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(55, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setTooltip(new Tooltip(item));                 
                        setStyle("-fx-alignment: baseline-left; -fx-font-size: 16; -fx-font-family: serif; -fx-font-weight: normal");
                    }                    
                };
            }
        };
        
        tunerTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue != null && postLoadedTab != null && postLoadedTab.contains(newValue)) {
                
                makeBusy(true);
                postLoadedTab.remove(newValue);
                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void run() {
                        Map<String, List<TunerResult>> result = new HashMap<>();
                        switch (newValue.getText()) {
                            case "Database Metrics":
                                tunnerUtil.mysqlDatabases(new ArrayList<>(), new HashMap<>(), result);
                                break;

                            case "Table Column Metrics":
                                tunnerUtil.mysqlTables(new ArrayList<>(), new HashMap<>(), result);
                                break;

                            case "Indexes Metrics":
                                tunnerUtil.mysqlIndexes(new ArrayList<>(), new HashMap<>(), result);
                                break;
                        }

                        Platform.runLater(() -> {
                            ((FilterableTableView) newValue.getContent()).addItems(result.get(newValue.getText()));
                            makeBusy(false);
                        });
                    }
                });
            }
        });
    }
    
    
    private void showMoreData(QueryTableCell.QueryTableCellData cellData, String text, String title) {
        final Stage dialog = new Stage();

        ShowMoreDataCellController cellController = StandardBaseControllerProvider.getController(getController(), "ShowMoreDataCell");
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    private String calcBlobLength(String text) {        
        int size = text != null ? text.getBytes().length : 0;         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }
    
    public void exportXLS(String excelFileName) {
        
        // create sub dirs
        File f = new File(excelFileName);
        if (f.getParentFile() != null) {
            f.getParentFile().mkdirs();
        }

        HSSFWorkbook wb = new HSSFWorkbook();

        HSSFFont defaultFont = createDefaultFont(wb);
        HSSFFont titleFont = createTitleFont(wb);

        CellStyle timestampStyle = wb.createCellStyle();
        CellStyle dateStyle = wb.createCellStyle();
        CellStyle timeStyle = wb.createCellStyle();


        CreationHelper createHelper = wb.getCreationHelper();
        timestampStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
        timeStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));


        byte[] good;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(SwingFXUtils.fromFXImage(getStatusImage(TunerResult.Status.GOOD), null), "png", bos);

            good = bos.toByteArray();
        } catch (Throwable e) {
            log.error("Error", e);
            good = null;                
        }

        byte[] neutral;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(SwingFXUtils.fromFXImage(getStatusImage(TunerResult.Status.NEUTRAL), null), "png", bos);

            neutral = bos.toByteArray();
        } catch (Throwable e) {
            log.error("Error", e);
            neutral = null;                
        }

        byte[] bad;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(SwingFXUtils.fromFXImage(getStatusImage(TunerResult.Status.BAD), null), "png", bos);

            bad = bos.toByteArray();
        } catch (Throwable e) {
            log.error("Error", e);
            bad = null;                
        }

        {   
            // export performanceMetrics    
            List<String> columns = new ArrayList<>();
            columns.add("No");
            columns.add("Name");
            columns.add("Value");
            columns.add("Status");
            columns.add("Message");
            columns.add("Details");

            List<Integer> widths = new ArrayList<>();
            widths.add(50);
            widths.add(300);
            widths.add(200);
            widths.add(25);
            widths.add(300);
            widths.add(300);            

            HSSFSheet sheet = wb.createSheet(performanceMetrics.getText());

            short rowIndex = 0;
            for (String group: performanceMetricsTables.keySet()) {

                rowIndex = drawHeader(wb, sheet, defaultFont, titleFont, rowIndex, group, 6, columns);

                FilterableTableView table = performanceMetricsTables.get(group);

                List<Object[]> data = new ArrayList<>();
                if (table.getSourceData() != null) {
                    for (TunerResult tr: (List<TunerResult>)(List)table.getSourceData()) {
                        Object[] obj = new Object[] {
                            tr.getIndex(),
                            tr.getName(),
                            tr.getValue(),
                            tr.getStatus() == TunerResult.Status.BAD ? bad : 
                                (tr.getStatus() == TunerResult.Status.GOOD ? good : 
                                    (tr.getStatus() == TunerResult.Status.NEUTRAL ? neutral : null)),
                            tr.getMessage(),
                            tr.getDetails()
                        };

                        data.add(obj);
                    }
                }

                rowIndex = drawData(wb, sheet, createHelper, timestampStyle, dateStyle, timeStyle, rowIndex, data);

                fixColumnsWidth(sheet, widths);
            }
        }

        {
            // export securityRecommendations    
            List<String> columns = new ArrayList<>();
            columns.add("No");
            columns.add("Status");
            columns.add("Message");
            columns.add("Details");

            List<Integer> widths = new ArrayList<>();
            widths.add(50);
            widths.add(25);
            widths.add(300);
            widths.add(300);     

            // export securityRecommendations            
            HSSFSheet sheet = wb.createSheet(securityRecommendations.getText());

            short rowIndex = drawHeader(wb, sheet, defaultFont, titleFont, (short) 0, null, 0, columns);

            FilterableTableView table = (FilterableTableView) securityRecommendations.getContent();

            List<Object[]> data = new ArrayList<>();
            if (table.getSourceData() != null) {
                int index = 1;
                for (TunerResult tr: (List<TunerResult>)(List)table.getSourceData()) {
                    Object[] obj = new Object[] {
                        index,
                        tr.getStatus() == TunerResult.Status.BAD ? bad : 
                            (tr.getStatus() == TunerResult.Status.GOOD ? good : 
                                (tr.getStatus() == TunerResult.Status.NEUTRAL ? neutral : null)),
                        tr.getMessage(),
                        tr.getDetails()
                    };

                    data.add(obj);
                    index++;
                }
            }

            drawData(wb, sheet, createHelper, timestampStyle, dateStyle, timeStyle, rowIndex, data);

            fixColumnsWidth(sheet, widths);
        }
        
        {
            // export replicationMetrics    
            List<String> columns = new ArrayList<>();
            columns.add("No");
            columns.add("Status");
            columns.add("Message");
            columns.add("Details");

            List<Integer> widths = new ArrayList<>();
            widths.add(50);
            widths.add(25);
            widths.add(300);
            widths.add(300);     

            // export replicationMetrics            
            HSSFSheet sheet = wb.createSheet(replicationMetrics.getText());

            short rowIndex = drawHeader(wb, sheet, defaultFont, titleFont, (short) 0, null, 0, columns);

            FilterableTableView table = (FilterableTableView) replicationMetrics.getContent();

            List<Object[]> data = new ArrayList<>();
            if (table.getSourceData() != null) {
                int index = 1;
                for (TunerResult tr: (List<TunerResult>)(List)table.getSourceData()) {
                    Object[] obj = new Object[] {
                        index,
                        tr.getStatus() == TunerResult.Status.BAD ? bad : 
                            (tr.getStatus() == TunerResult.Status.GOOD ? good : 
                                (tr.getStatus() == TunerResult.Status.NEUTRAL ? neutral : null)),
                        tr.getMessage(),
                        tr.getDetails()
                    };

                    data.add(obj);
                    index++;
                }
            }

            drawData(wb, sheet, createHelper, timestampStyle, dateStyle, timeStyle, rowIndex, data);

            fixColumnsWidth(sheet, widths);
        }
                
        {
            // export performanceSchema    
            List<String> columns = new ArrayList<>();
            columns.add("No");
            columns.add("Status");
            columns.add("Message");

            List<Integer> widths = new ArrayList<>();
            widths.add(50);
            widths.add(25);
            widths.add(300);    

            // export performanceSchema            
            HSSFSheet sheet = wb.createSheet(performanceSchema.getText());

            short rowIndex = drawHeader(wb, sheet, defaultFont, titleFont, (short) 0, null, 0, columns);

            FilterableTableView table = (FilterableTableView) performanceSchema.getContent();

            List<Object[]> data = new ArrayList<>();
            if (table.getSourceData() != null) {
                int index = 1;
                for (TunerResult tr: (List<TunerResult>)(List)table.getSourceData()) {
                    Object[] obj = new Object[] {
                        index,
                        tr.getStatus() == TunerResult.Status.BAD ? bad : 
                            (tr.getStatus() == TunerResult.Status.GOOD ? good : 
                                (tr.getStatus() == TunerResult.Status.NEUTRAL ? neutral : null)),
                        tr.getMessage(),
                        tr.getDetails()
                    };

                    data.add(obj);
                    index++;
                }
            }

            drawData(wb, sheet, createHelper, timestampStyle, dateStyle, timeStyle, rowIndex, data);

            fixColumnsWidth(sheet, widths);
        }


        try {            
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            wb.write(bo);
            Files.write(f.toPath(), bo.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (Throwable ex) {
            return;
        }
        
        DialogResponce resp = DialogHelper.showConfirm(getController(), "Information",
            "Data exported Successfully, Would you like to open file?");

        if (resp == DialogResponce.OK_YES) {
            try {
                Desktop.getDesktop().open(f);
            } catch (IOException ex) {
                Platform.runLater(() -> {
                    DialogHelper.showError(getController(), "Error", "There is an error while opening exported file", ex);
                });
            }
        }
    }
    
    private HSSFFont createDefaultFont(HSSFWorkbook wb) {
        HSSFFont defaultFont = wb.createFont();
        defaultFont.setFontHeightInPoints((short)11);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLUE.getIndex());
        defaultFont.setBold(true);
        
        return defaultFont;
    }
    
    private HSSFFont createTitleFont(HSSFWorkbook wb) {
        HSSFFont defaultFont = wb.createFont();
        defaultFont.setFontHeightInPoints((short)13);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(true);
        
        return defaultFont;
    }
    
    private short drawHeader(HSSFWorkbook wb, HSSFSheet sheet, HSSFFont defaultFont, HSSFFont titleFont, short rowIndex, String group, int groupSpan, List<String> columns) {
        HSSFRow headerRow = sheet.createRow(rowIndex);
        
        if (group != null) {
            HSSFCell headerCell = headerRow.createCell(0);
            headerCell.setCellValue(group);
            
            CellStyle titleStyle = wb.createCellStyle();
            titleStyle.setFont(titleFont);
            titleStyle.setAlignment(HorizontalAlignment.CENTER);
            
            headerCell.setCellStyle(titleStyle);
            
            if (groupSpan > 1) {                
                sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, groupSpan - 1));
            }
            
            rowIndex++;
            headerRow = sheet.createRow(rowIndex);
        }
        
        CellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFont(defaultFont);

        int columnIndex = 0;
        for (String column: columns) {
            HSSFCell headerCell = headerRow.createCell(columnIndex);
            headerCell.setCellValue(column.trim());
            headerCell.setCellStyle(headerStyle);
            
            columnIndex++;
        }
        
        return ++rowIndex;
    }
    
    private short drawData(HSSFWorkbook wb, HSSFSheet sheet, CreationHelper createHelper, CellStyle timestampStyle, CellStyle dateStyle, CellStyle timeStyle, short rowIndex, List<Object[]> data) {
     
        for (int r = 0; r < data.size(); r++) {
            HSSFRow row = sheet.createRow(rowIndex);
            rowIndex++;
            
            int columnIndex = 0;
            for (Object value: data.get(r)) {               
            
                HSSFCell cell = row.createCell(columnIndex);

                if (value instanceof Number) {
                    cell.setCellValue(((Number)value).doubleValue());
                } else if (value instanceof Date) {

                    cell.setCellValue((Date)value);

                    if (value instanceof Time) {
                        cell.setCellStyle(timeStyle);
                    } else if (value instanceof Timestamp) {
                        cell.setCellStyle(timestampStyle);
                    } else {
                        cell.setCellStyle(dateStyle);
                    }
                } else if (value instanceof Boolean) {
                    cell.setCellType(CellType.NUMERIC);
                    cell.setCellValue((Boolean)value);

                } else if (value instanceof byte[]) {

                    Drawing drawing = sheet.createDrawingPatriarch();
                    ClientAnchor anchor = createHelper.createClientAnchor();

                    int pictureIndex = wb.addPicture((byte[]) value, Workbook.PICTURE_TYPE_PNG);

                    anchor.setCol1(columnIndex);
                    anchor.setRow1(rowIndex - 1);
                    anchor.setCol2(columnIndex);
                    anchor.setRow2(rowIndex - 1);                            

                    Picture p = drawing.createPicture(anchor, pictureIndex);
                    p.resize(1.0, 1.0);

                } else {

                    if (value == null) {
                        value = "";
                    } else if (value instanceof byte[]){
                        value = "x'" + Hex.encodeHexString((byte[])value) + "'";
                    } else {
                        value = value.toString();
                    }

                    cell.setCellValue(new HSSFRichTextString((String)value));
                }

                columnIndex++;
            }
        }
        
        return ++rowIndex;
    }
    
    private void fixColumnsWidth(HSSFSheet sheet, List<Integer> columnWidths) {
    
        if (columnWidths != null) {
            for (int i = 0; i < columnWidths.size(); i++) {
                Integer width = columnWidths.get(i) / 7;
                if (width > 255) {
                    width = 255;
                }
                sheet.setColumnWidth(i, width * 256);
            }
        }
    }
    
}
