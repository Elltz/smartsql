package np.com.ngopal.smart.sql.structure.controller;

import com.sun.javafx.PlatformUtil;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import java.io.File;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import lombok.AccessLevel;
import javafx.util.Duration;
import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import static np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService.CACHE_TIME;
import np.com.ngopal.smart.sql.db.DeadlockService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DeadlockData;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.GSChartVariable;
import np.com.ngopal.smart.sql.model.GlobalProcessList;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.QOIndexRecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;

/**
 * @TODO THIS CLASS NEED TO BE REFACTORED !!!
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DebuggerTabController extends BaseController implements Initializable {

    private static final Map<String, String> GENERAL_STATES = new HashMap<>();
    private static final Map<String, String> GENERAL_COMMANDS = new HashMap<>();

    @Setter
    @Getter
    private ConnectionParams params;
    
    @Setter(AccessLevel.PUBLIC)
    private ProfilerChartService chartService; 
    
    @Setter(AccessLevel.PUBLIC)
    private ReadOnlyObjectProperty<Database> databaseSelectedItemProperty;
    
    @Setter(AccessLevel.PUBLIC)
    private ProfilerDataManager profilerService;
    
    @Setter(AccessLevel.PUBLIC)
    private ProfilerSettingsService settingsService;
    
    private DBService service;
    private UserUsageStatisticService usageService;
    
    @Setter(AccessLevel.PUBLIC)
    private PreferenceDataService preferenceService;
    
    @Setter(AccessLevel.PUBLIC)
    private DeadlockService deadlockService;
    
    @Setter(AccessLevel.PUBLIC)
    private QARecommendationService qars;
    
    @Setter(AccessLevel.PUBLIC)
    private ColumnCardinalityService columnCardinalityService;
    
    @Setter(AccessLevel.PUBLIC)
    private QAPerformanceTuningService performanceTuningService;
    
    @Setter(AccessLevel.PUBLIC)
    private ConnectionParamService paramsService;

    private final ObjectProperty<Long> hoveredX = new SimpleObjectProperty<>(0l);

    public ObjectProperty<Long> hoveredX() {
        return hoveredX;
    }

    private final BooleanProperty hoveredOnChart = new SimpleBooleanProperty(false);

    public BooleanProperty hoveredOnChart() {
        return hoveredOnChart;
    }
    
    public ProfilerSettings getCurrentSettings() {
        return settingsService.getProfilerSettings();
    }
    
    @FXML
    private Tab errorLogTab;
    private boolean errorLogInited = false;
    @FXML
    private VBox errorLogContainer;
    @FXML
    private HBox errorLogButtonsContainer;
    @FXML
    private ComboBox<String> dayLogComboBox;
    @FXML
    private PaginatorFilterableTableView<ErrorLogResult> errorLogTable;
    @FXML
    private CheckBox displayOnlyErrors;
    @FXML
    private Button elRefreshButton;
    @FXML
    private Button elGetOldButton;
    @FXML
    private TextField elFilterField;
    @FXML
    private CheckBox elRegexCheckBox;
    @FXML
    private CheckBox elNotMatchCheckBox;
    
    @FXML
    private Label runningQueriesButton;
    
    @FXML
    private Text rqText_0_1;
    @FXML
    private Text rqText_1_6;
    @FXML
    private Text rqText_6;
    
    @FXML
    private Text rqLabel_0_1;
    @FXML
    private Text rqLabel_1_6;
    @FXML
    private Text rqLabel_6;
    
    @FXML
    private Label transactionsButton;
    
    @FXML
    private Text trText_undo;
    @FXML
    private Text trText_active;
    @FXML
    private Text trText_notStarted;
    
    @FXML
    private Label deadLocksButton;
    
    @FXML
    private Text dlText;
    
    @FXML
    private Label queriesLockButton;
    
    @FXML
    private Text qlText;
    
    @FXML
    private Label replicationLaggingButton;
    
    @FXML
    private Text rlText;
    @FXML
    private Text rlLabel;
    
    @FXML
    private Label replicationMonitorButton;
    @FXML
    private Text rmLabel;
    
    @FXML
    private Label openTablesButton;
    
    @FXML
    private Text otText_openTables;
    @FXML
    private Text otText_tableOpenCache;
    
    @FXML
    private Label dbLoadButton;
    
    @FXML
    private Text dbText_total;
    @FXML
    private Text dbText_write;
    @FXML
    private Text dbText_select;
    @FXML
    private Text dbText_insert;
    @FXML
    private Text dbText_update;
    @FXML
    private Text dbText_delete;
    @FXML
    private Text dbText_BytesReceived;
    @FXML
    private Text dbText_BytesSent;
    
    @FXML
    private Label configurationButton;
    
    @FXML
    private Text configurationText;
    
    @FXML
    private Label unusedIndexesButton;
    
    @FXML
    private Text unusedIndexesText;
    
    @FXML
    private Label duplicateIndexesButton;
    
    @FXML
    private Text duplicateIndexesText;
    
    @FXML
    private Label dbSizeButton;
    
    @FXML
    private Text dbsText_total;
    @FXML
    private Text dbsText_data;
    @FXML
    private Text dbsText_index;
    @FXML
    private Text dbsText_disk;
    
    private boolean reportRunned = false;
    
    @FXML
    private Label connectionsButton;
    
    @FXML
    private Text conText_maxConnections;
    @FXML
    private Text conText_threads;
    @FXML
    private Text conText_maxUsed;
    
    @FXML
    private Label bufferPoolButton;
    
    @FXML
    private Text bpText_size;
    @FXML
    private Text bpText_dirtySize;
    @FXML
    private Text bpText_freeSize;
    
    @FXML
    private Label foreignKeyErrorsButton;
    @FXML
    private Text foreignKeyErrorsText;
    
    @FXML
    private VBox deadlockSplitVContainer;

    @FXML
    private FilterableTableView runningQueriesTable;
    
    @FXML
    private PaginatorTableView<DebugInnoTopData> innoTopTable;
    
    @FXML
    private PaginatorFilterableTableView<DeadlockMonitorQueryData> deadlockMonitorTable;
    
    @FXML
    private FilterableTableView lockMonitorTable;

    @FXML
    private FilterableTableView configurationDebuggerTable;
    
    @FXML
    private Tab configurationDebuggerTab;
    
    private boolean initConfigurationDebugger = false;

    @FXML
    private HBox deadlockChartBox;

    @FXML
    private HBox replicationDelayChartBox;
    
    @FXML
    private HBox replicationSyncChartBox;
    
    @FXML
    private HBox relayLogSpaceChartBox;
    
    @FXML
    private HBox relayLogWrittenChartBox;

    @FXML
    private TextField rqFilterField;
    
    @FXML
    private CheckBox rqRegexCheckBox;
    
    @FXML
    private CheckBox rqNotMatchCheckBox;

    @FXML
    private TextField itFilterField;
    
    @FXML
    private CheckBox itRegexCheckBox;
    
    @FXML
    private CheckBox itNotMatchCheckBox;

    @FXML
    private TextField dmFilterField;
    
    @FXML
    private CheckBox dmRegexCheckBox;
    
    @FXML
    private CheckBox dmNotMatchCheckBox;

    @FXML
    private TextField lmFilterField;
    
    @FXML
    private CheckBox lmRegexCheckBox;
    
    @FXML
    private CheckBox lmNotMatchCheckBox;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private TextField rqUpdateStepField;
    
    @FXML
    private TextField itUpdateStepField;
    
    @FXML
    private TextField dbUpdateStepField;
    
    @FXML
    private TextField dmUpdateStepField;
    
    @FXML
    private TextField lmUpdateStepField;
    
    @FXML
    private TextField rlUpdateStepField;

    @FXML
    private TabPane bottomTabPane;
    
    @FXML
    private Tab autodetectTab;
    
    @FXML
    private Tab runningQueriesTab;
    
    @FXML
    private Tab transactionsTab;
    
    @FXML
    private Tab deadlockMonitorTab;
    
    @FXML
    private Tab lockMonitorTab;
    
    @FXML
    private Tab speedometerTab;
    
    private Tab dbLoadTab;
    
    @FXML
    private Tab dbConnectionsAnalyzerTab;
//    @FXML
//    private Tab dbSizeTab;
//    @FXML
//    private Tab unusedIndexesTab;    
//    @FXML
//    private Tab duplicateIndexesTab;
    
    @FXML
    private ScrollPane autodetectContainer;
    
    private List<SalveStatusData> salveStatusData = new ArrayList<>();

    private static final String Seconds_Behind_Master = "Seconds_Behind_Master";
    private static final String Slave_IO_Running = "Slave_IO_Running";
    private static final String Slave_SQL_Running = "Slave_SQL_Running";
    private static final String Last_SQL_Errno = "Last_SQL_Errno";
    private static final String Last_SQL_Error = "Last_SQL_Error";
    private static final String Last_IO_Errno = "Last_IO_Errno";
    private static final String Last_IO_Error = "Last_IO_Error";

    private static final List<String> REPLICATION_STATUS_COLUMNS = Arrays.asList(new String[]{
        Seconds_Behind_Master,
        Slave_IO_Running,
        Slave_SQL_Running,
        Last_SQL_Errno,
        Last_SQL_Error,
        Last_IO_Errno,
        Last_IO_Error
    });

    @FXML
    private Button rqStartButton;
    @FXML
    private Button rqPauseButton;
    @FXML
    private Button rqRefreshButton;
    @FXML
    private Button rqKillButton;
    @FXML
    private Button rqKillAllButton;

    @FXML
    private Button rlStartButton;
    @FXML
    private Button rlPauseButton;
    @FXML
    private Button rlRefreshButton;

    @FXML
    private Button itStartButton;
    @FXML
    private Button itPauseButton;
    @FXML
    private Button itRefreshButton;
    @FXML
    private Button itKillButton;
    @FXML
    private Button itKillAllButton;

    @FXML
    private Button dmStartButton;
    @FXML
    private Button dmPauseButton;
    @FXML
    private Button dmPasteButton;
    @FXML
    private Button dmRefreshButton;
    @FXML
    private Button dmClearButton;

    @FXML
    private Button dbStartButton;
    @FXML
    private Button dbPauseButton;
    @FXML
    private Button dbRefreshButton;

    @FXML
    private Button lmStartButton;
    @FXML
    private Button lmPauseButton;
    @FXML
    private Button lmRefreshButton;
    @FXML
    private Button lmKillButton;
    @FXML
    private Button lmKillAllButton;

    @FXML
    private ToggleButton rqAllButton;
    @FXML
    private ToggleButton rqTimeGreater0Button;
    @FXML
    private ToggleButton rqAvoidSleepButton;

    @FXML
    private ToggleButton dmAllButton;
    @FXML
    private ToggleButton dmTodayButton;
    @FXML
    private ToggleButton dmOneHourButton;

    @FXML
    private ToggleButton lmAllButton;
    @FXML
    private ToggleButton lmTimeGreater0Button;
    @FXML
    private ToggleButton lmTXLockButton;

    @FXML
    private ComboBox<String> rqTimeCombobox;
    @FXML
    private TextField rqTimeField;

    @FXML
    private ComboBox<String> lmTimeCombobox;
    @FXML
    private TextField lmTimeField;

    @FXML
    private AnchorPane sqlSpeedometerPane;

    @FXML
    private StackPane stackofgaugeContainer;

    private ToggleButton rqLastButton;
    private ToggleButton dmLastButton;
    private ToggleButton lmLastButton;

    private boolean rqShowed = false;
    private boolean itShowed = false;
    private boolean dmShowed = false;
    private boolean lmShowed = false;
    
    private boolean rqRed = false;
    private boolean trRed = false;
    private boolean qlRed = false;
    
    private boolean rqGreen = false;
    private boolean trGreen = false;
    private boolean qlGreen = false;



    private WorkProc backgroundWorkProcessor;
    private volatile boolean needStop = false;
    
    private volatile boolean blinkEnabled = true;

    private volatile int rqWaitInSec = 15;
    private volatile int itWaitInSec = 15;
    private volatile int dmWaitInSec = 15;
    private volatile int dbWaitInSec = 30;
    private volatile int lmWaitInSec = 15;
    private volatile int speedometerWaitInSec = 1;
    private volatile int replWaitInSec = 15;
    private volatile int chartsWaitInSec = 10;

    private volatile Long rqLast = 0l;
    private volatile Long itLast = 0l;
    private volatile Long dmLast = 0l;
    private volatile Long dbLast = 0l;
    private volatile Long lmLast = 0l;
    private volatile Long speedometerLast = 0l;
    private volatile Long replLast = 0l;

    private volatile Long chartsLast = 0l;

    private volatile boolean rqPaused = false;
    private volatile boolean itPaused = false;
    private volatile boolean dbPaused = false;
    private volatile boolean dmPaused = false;
    private volatile boolean lmPaused = false;
    private volatile boolean replPaused = false;

    private boolean dmFirstRun = true;

    private boolean rqForceRun = false;
    private boolean itForceRun = false;
    private boolean dbForceRun = false;
    private boolean dmForceRun = false;
    private boolean lmForceRun = false;
    private boolean rlForceRun = false;
    private boolean speedForceRun = false;
    private boolean chartsForceRun = false;

    private final String PIE_PARENT_INDEX_IN_CONTAINER_FIELD = "field";
    private final String PIE_PARENT_HEIGHT = "h";
    private final String PIE_TOTAL_SUM_IN_UNITS = "units";
    private final String PIE_NODE_PERCENTILE = "cent";
    private final String PIE_NODE_NAME = "pie";
    
    private static final SimpleDateFormat DEADLOCK_DATE_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final SimpleDateFormat dateFormat0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    
    private final SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyMMdd HH:mm:ss");
    
    private Double speedometerTotalValue = 0.0;
    private Double speedometerTotalLastInc = 0.0;

    @FXML
    private TableView<SpeedometerData> speedometerTable;
    
    @FXML
    private TableColumn<SpeedometerData, String> speedometerNameColumn;
    
    @FXML
    private TableColumn<SpeedometerData, Double> speedometerValueColumn;
    
    @FXML
    private TableColumn<SpeedometerData, String> speedometerValuePctColumn;
    
    @FXML
    private TableColumn<SpeedometerData, Double> speedometerLastIncColumn;
    
    @FXML
    private TableColumn<SpeedometerData, String> speedometerLastIncPctColumn;

    private static final String DB_CONNECTIONS__APPLICATIONS = "Top DB Connections Consumed Applications";
    private static final String DB_CONNECTIONS__USER_WITH_HOSTS = "Top DB Connections Consumed By Users with Hosts";
    private static final String DB_CONNECTIONS__USER_WITH_DBS_HOSTS = "Top DB Connections Consumed By Users with DBs & Hosts";
    private static final String DB_CONNECTIONS__USER_WITH_HOSTS_STATE = "Top DB Connections Consumed By Users with Hosts & State";
    private static final String DB_CONNECTIONS__USERS = "Top DB Connections Consumed By DB Users";
    private static final String DB_CONNECTIONS__DATABASES = "Top DB Connections Consumed By Databases";
    private static final String DB_CONNECTIONS__QUERY_STATE = "Top DB Connections Consumed By Query State";
    private static final String DB_CONNECTIONS__USERS_DBS_QUERY_STATE = "Top DB Connections Consumed By DB Users, DBs & Query state";
    private static final String DB_CONNECTIONS__USERS_QUERY_STATE = "Top DB Connections Consumed By DB Users & Query state";

    @FXML
    private ComboBox<String> dbConnectionsComboBox;
    
    @FXML
    private TableView<DBConnectionInfo> wiseGrid;

    @FXML
    private Label minMaxGridButtonLabel;
    
    @FXML
    private Label minMaxBarButtonLabel;
    
    @FXML
    private Label minMaxPieButtonLabel;

    @FXML
    private TableColumn nameColumn;
    
    @FXML
    private TableColumn connsColumn;
    
    @FXML
    private TableColumn activeConnsColumn;
    
    @FXML
    private TableColumn sleepConnsColumn;
    
    @FXML
    private TableColumn activePercentColumn;
    
    @FXML
    private TableColumn sleepPercentColumn;
    
    @FXML
    private TableColumn percentColumn;

    @FXML
    private PieChart wisePie;
    
    @FXML
    private AnchorPane wiseBarContainer;

    private XYChart.Series activeSeries;
    private XYChart.Series sleepSeries;

    private StackedBarChart wiseBar;
    
    @FXML
    private Tab replicationMonitorTab;
    @FXML
    private Tab foreignKeyErrorsTab;
    @FXML
    private Tab openTablesTab;
    @FXML
    private Tab bufferPoolTab;
    @FXML
    private Tab innoDBRowsTab;
    
    @FXML
    private Tab replicationLagging;

    @FXML
    private Label notValidLabel;

    @FXML
    private VBox replicationLaggingPane;

    @FXML
    private Label sbmLabel;

    @FXML
    private FilterableTableView analyzerQueryTable;

    @FXML
    private StackPane stackofpieContainer;

    @FXML
    private HBox innoTopButtonsContainer;

    @FXML
    private HBox deadlockTopButtonsContainer;

    private Thread replicationLaggingThread;

    private volatile boolean needStopLagging = true;

    @FXML
    private TableView<BufferPoolData> bufferPoolTable;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPSizeColumn;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPFreeBuffsColumn;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPPagesColumn;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPDirtyPagesColumn;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPHitRateColumn;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPMemoryColumn;
    
    @FXML
    private TableColumn<BufferPoolData, String> bufferPoolBPAddPoolColumn;

    @FXML
    private TableView<PagesStatisticData> pageStatisticTable;
    
    @FXML
    private TableColumn<PagesStatisticData, String> bufferPoolPSReadsColumn;
    
    @FXML
    private TableColumn<PagesStatisticData, String> bufferPoolPSWritesColumn;
    
    @FXML
    private TableColumn<PagesStatisticData, String> bufferPoolPSCreatedColumn;
    
    @FXML
    private TableColumn<PagesStatisticData, String> bufferPoolPSReadsSecColumn;
    
    @FXML
    private TableColumn<PagesStatisticData, String> bufferPoolPSWritesSecColumn;
    
    @FXML
    private TableColumn<PagesStatisticData, String> bufferPoolPSCreatesSecColumn;

    @FXML
    private TextArea insertBufferAndAdaptiveHashIndexArea;
//    
//    @FXML
//    private TableView<InsertBuffersData> insertBuffersTable;
//    @FXML
//    private TableColumn<InsertBuffersData, String> bufferPoolIBInsertsColumn;
//    @FXML
//    private TableColumn<InsertBuffersData, String> bufferPoolIBMergedRecsColumn;
//    @FXML
//    private TableColumn<InsertBuffersData, String> bufferPoolIBMergesColumn;
//    @FXML
//    private TableColumn<InsertBuffersData, String> bufferPoolIBSizeColumn;
//    @FXML
//    private TableColumn<InsertBuffersData, String> bufferPoolIBFreeListLenColumn;
//    @FXML
//    private TableColumn<InsertBuffersData, String> bufferPoolIBSegSizeColumn;
//    
//    @FXML
//    private TableView<AdaptiveHashIndexData> adaptiveHashIndexTable;
//    @FXML
//    private TableColumn<AdaptiveHashIndexData, String> bufferPoolAHISizeColumn;
//    @FXML
//    private TableColumn<AdaptiveHashIndexData, String> bufferPoolAHICellsUsedColumn;
//    @FXML
//    private TableColumn<AdaptiveHashIndexData, String> bufferPoolAHINodeHeapBuffsColumn;
//    @FXML
//    private TableColumn<AdaptiveHashIndexData, String> bufferPoolAHIHashSecColumn;
//    @FXML
//    private TableColumn<AdaptiveHashIndexData, String> bufferPoolAHINonHashSecColumn;    

    @FXML
    private TableView<InnodbRowOperationsData> innodbRowOperationsTable;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIROInsColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIROUpdColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIROIReadColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIRODelColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIROInsSecColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIROUpdSecColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIROReadSecColumn;
    @FXML
    private TableColumn<InnodbRowOperationsData, String> rowOperationIRODelSecColumn;

    @FXML
    private TableView<RowOperationMiscData> rowOperationMiscTable;
    @FXML
    private TableColumn<RowOperationMiscData, String> bufferPoolROMQueriesQueuedColumn;
    @FXML
    private TableColumn<RowOperationMiscData, String> bufferPoolROMQueriesInsideColumn;
    @FXML
    private TableColumn<RowOperationMiscData, String> bufferPoolROMRdViewsColumn;
    @FXML
    private TableColumn<RowOperationMiscData, String> bufferPoolROMMainThreadStateColumn;

    @FXML
    private TextArea semaphoresArea;

    @FXML
    private TextArea foreignKeyErrorsArea;

    @FXML
    private TableView<OpenTablesData> openTablesTable;

    @FXML
    private TableColumn<OpenTablesData, String> openTablesDatabaseColumn;

    @FXML
    private TableColumn<OpenTablesData, String> openTablesTableColumn;

    @FXML
    private TableColumn<OpenTablesData, Integer> openTablesInUseColumn;

    @FXML
    private TableColumn<OpenTablesData, Integer> openTablesNameLockedColumn;

    private static final String PROCESSLIST__VARIABLE__ID = "id";
    private static final String PROCESSLIST__VARIABLE__USER = "user";
    private static final String PROCESSLIST__VARIABLE__HOST = "host";
    private static final String PROCESSLIST__VARIABLE__DB = "db";
    private static final String PROCESSLIST__VARIABLE__COMMAND = "command";
    private static final String PROCESSLIST__VARIABLE__TIME = "time";
    private static final String PROCESSLIST__VARIABLE__STATE = "state";
    private static final String PROCESSLIST__VARIABLE__INFO = "info";
    
    boolean rqPausedOld = false;
    boolean itPausedOld = false;
    boolean dbPausedOld = false;
    boolean dmPausedOld = false;
    boolean lmPausedOld = false;
    boolean replPausedOld = false;

    private Gauge totalQueriesPerSec;
    private Label totalQueriesPerMin;

    private Gauge writeQueriesPerSec;
    private Label writeQueriesPerMin;

    private Gauge selectPerSec;
    private Label selectPerMin;

    private Gauge insertPerSec;
    private Label insertPerMin;

    private Gauge updatePerSec;
    private Label updatePerMin;

    private Gauge deletePerSec;
    private Label deletePerMin;

    private Gauge commitPerSec;
    private Label commitPerMin;

    private Gauge beginPerSec;
    private Label beginPerMin;

    private final Long MAX_DEFAULT = 25000l;

    private final double X_SEC_DEFAULT = 100.0;

    private Long maxTotalQueries = 25000l;
    private Long maxWriteQueries = 25000l;
    private Long maxSelect = 25000l;
    private Long maxInsert = 25000l;
    private Long maxUpdate = 25000l;
    private Long maxDelete = 25000l;
    private Long maxCommit = 25000l;
    private Long maxBegin = 25000l;

    private Long totalQueriesOld = null;
//    private Long queriesOld = null;
    private Long selectOld = null;
    private Long insertOld = null;
    private Long updateOld = null;
    private Long deleteOld = null;
    private Long commitOld = null;
    private Long beginOld = null;
    private Long replaceOld = null;

    private Map<PieChart, Double> chartSums = new HashMap<>();

    private ProfilerChartController deadlockChartPane;
    
    private XYChart.Series<Number, Number> deadlockSeries;
    
    private ProfilerChartController replicationDelayChartPane;
    private XYChart.Series<Number, Number> secondBehindMasterSeries;
    
    private ProfilerChartController replicationSyncChartPane;
    private XYChart.Series<Number, Number> execMasterLogPosSeries;
    private XYChart.Series<Number, Number> readMasterLogPosSeries;
    
    private ProfilerChartController relayLogSpaceChartPane;
    private XYChart.Series<Number, Number> relayLogSpaceSeries;
    
    private ProfilerBarChartController relayLogWrittenChartPane;
    private XYChart.Series<String, Number> relayLogWrittenSeries;
    
    private int countRelayLog = 0;

    private MySQLTunnerUtil tunnerUtil;
    
    private boolean licenseFullUsageExipred = true;

    public void threadsAction(boolean pause) {
        if (pause) {
            rqPausedOld = rqPaused;
            itPausedOld = itPaused;
            dbPausedOld = dbPaused;
            dmPausedOld = dmPaused;
            lmPausedOld = lmPaused;
            replPausedOld = replPaused;

            rqPaused = pause;
            itPaused = pause;
            dbPaused = pause;
            dmPaused = pause;
            lmPaused = pause;
            replPaused = pause;
        } else {
            rqPaused = rqPausedOld;
            itPaused = itPausedOld;
            dbPaused = dbPausedOld;
            dmPaused = dmPausedOld;
            lmPaused = lmPausedOld;
            replPaused = replPausedOld;
        }
    }

    public void dbConnectionsAnalyzerCodeBranch(long timesRunned) {
        if (dbForceRun || !dbPaused && timeAllowanceOnCodeBranches(timesRunned - dbLast, dbWaitInSec)) {
            dbForceRun = false;
            dbLast = timesRunned;
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {
                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController) {
                    String multiQuery = "SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';";

                    String type = dbConnectionsComboBox.getSelectionModel().getSelectedItem();
                    if (type != null)
                    {
                        switch (type) {
                            case DB_CONNECTIONS__APPLICATIONS:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__APPLICATION_WISE_HOST);
                                break;

                            case DB_CONNECTIONS__USER_WITH_HOSTS:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DB_USER_HOST_WISE);
                                break;

                            case DB_CONNECTIONS__USER_WITH_DBS_HOSTS:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DB_USER_DBS_HOST_WISE);
                                break;  

                            case DB_CONNECTIONS__USER_WITH_HOSTS_STATE:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DB_USER_HOST_STATE);
                                break;  

                            case DB_CONNECTIONS__USERS:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__USERS_WISE);
                                break;  

                            case DB_CONNECTIONS__DATABASES:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DATABASE_WISE);
                                break;

                            case DB_CONNECTIONS__QUERY_STATE:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__QUERY_STATE_WISE);
                                break;

                            case DB_CONNECTIONS__USERS_DBS_QUERY_STATE:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__USERS_DBS_QUERY_STATE_WISE);
                                break; 

                            case DB_CONNECTIONS__USERS_QUERY_STATE:
                                multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__USERS_QUERY_STATE_WISE);
                                break; 

                        }

                        if (!multiQuery.trim().endsWith(";")) {
                            multiQuery += ";";
                        }
                        
                        multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_STATUS);

                        if (!multiQuery.trim().endsWith(";")) {
                            multiQuery += ";";
                        }
                        
                        multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_VARIABLES);
                        
                        if (!multiQuery.trim().endsWith(";")) {
                            multiQuery += ";";
                        }
                    }

                    multiQuery += "SET SQL_MODE=@OLD_SQL_MODE;";

                    try {
                        QueryResult qr = new QueryResult();
                        service.execute(multiQuery, qr);

                        List<PieChart.Data> pieData = new ArrayList<>();
                        List<XYChart.Data> activeData = new ArrayList<>();
                        List<XYChart.Data> sleepData = new ArrayList<>();
                        List<DBConnectionInfo> gridData = new ArrayList<>();

                        ResultSet rs = (ResultSet) qr.getResult().get(2);
                        while (rs.next()) {

                            DBConnectionInfo ci = new DBConnectionInfo(
                                rs.getString(1),
                                rs.getInt(2),
                                rs.getInt(3),
                                rs.getInt(4),
                                rs.getDouble(5) / 100,
                                rs.getDouble(6) / 100,
                                rs.getDouble(7) / 100
                            );

                            gridData.add(ci);

                            pieData.add(new PieChart.Data(ci.getName() != null ? ci.getName() : "", ci.getActiveConns()));

                            activeData.add(new XYChart.Data(ci.getName() != null ? ci.getName() : "", ci.getActiveConns()));
                            sleepData.add(new XYChart.Data(ci.getName() != null ? ci.getName() : "", ci.getSleepConns()));
                        }
                        rs.close();
                        
                        int threadsConnected = 0;
                        int maxUsedConnections = 0;
                        rs = (ResultSet) qr.getResult().get(3);
                        while (rs.next()) {
                            if ("Threads_connected".equals(rs.getString(1))) {
                                threadsConnected = rs.getInt(2);
                            } else if ("Max_used_connections".equals(rs.getString(1))) {
                                maxUsedConnections = rs.getInt(2);
                            }
                        }
                        rs.close();
                        
                        int maxConnections = 0;
                        rs = (ResultSet) qr.getResult().get(4);
                        while (rs.next()) {
                            if ("max_connections".equals(rs.getString(1))) {
                                maxConnections = rs.getInt(2);
                            }
                        }
                        rs.close();
                        
                        int threadsConnectedStatic = threadsConnected;
                        int maxUsedConnectionsStatic = maxUsedConnections;
                        int maxConnectionsStatic = maxConnections;
                        
                        Platform.runLater(() -> {

                            // Max_connections:N  (from show global variables)
                            // Threads_connected:N (from show global status)
                            // Max_used_connections:N (from show global status)
                            // Keep full red color if Max_used_connections/Max_connections > 0.8 
                            // Keep right red color if Max_used_connections/Max_connections > 0.7 and <=0.8 
                            // Keep green color if Max_used_connections/Max_connections < 0.5  
                            // Jut black color for other case. 
                                
                            conText_maxConnections.setText(String.valueOf(maxConnectionsStatic));
                            conText_threads.setText(String.valueOf(threadsConnectedStatic));
                            conText_maxUsed.setText(String.valueOf(maxUsedConnectionsStatic));
                            
                            if (preferenceService.getPreference().isDebuggerAlertConn()) {
                                double v = maxConnectionsStatic > 0 ? (double)maxUsedConnectionsStatic / (double)maxConnectionsStatic : 0.0;
                                if (v > 0.8) {
                                    connectionsButton.setId("debugerButton_red");
                                    addBlink(connectionsButton);
                                } else if (v > 0.7) {
                                    connectionsButton.setId("debugerButton_light_red");
                                    addBlink(connectionsButton);
                                } else if (v < 0.5) {
                                    connectionsButton.setId("debugerButton_green");
                                    removeBlink(connectionsButton);
                                } else {
                                    connectionsButton.setId("debugerButton_default");
                                    removeBlink(connectionsButton);
                                }
                            } else {
                                connectionsButton.setId("debugerButton_default");
                                removeBlink(connectionsButton);
                            }
                            
                            chartSums.clear();

                            wisePie.getData().clear();
                            wisePie.getData().addAll(pieData);

                            activeSeries = new XYChart.Series();
                            activeSeries.setName("ActiveConns");   

                            sleepSeries = new XYChart.Series();
                            sleepSeries.setName("SleepConns");

                            if (wiseBar != null) {
                                wiseBarContainer.getChildren().remove(wiseBar);
                            }
                            wiseBar = new StackedBarChart(new CategoryAxis(), new NumberAxis());
                            wiseBar.getData().addAll(activeSeries, sleepSeries);

                            activeSeries.getData().addAll(activeData);
                            sleepSeries.getData().addAll(sleepData);

                            wiseBarContainer.getChildren().add(wiseBar);
                            wiseBar.toBack();

                            AnchorPane.setBottomAnchor(wiseBar, 0.0);
                            AnchorPane.setTopAnchor(wiseBar, 0.0);
                            AnchorPane.setLeftAnchor(wiseBar, 0.0);
                            AnchorPane.setRightAnchor(wiseBar, 0.0);

                            int max = 0;
                            for (int i = 0; i < activeData.size(); i++) {

                                Data<String, Number> activeValue = activeData.get(i);
                                Data<String, Number> sleepValue = sleepData.get(i);

                                int active = 0;
                                try {
                                    active = activeValue.getYValue().intValue();
                                } catch (Throwable th) {
                                }

                                int sleep = 0;
                                try {
                                    sleep = sleepValue.getYValue().intValue();
                                } catch (Throwable th) {
                                }

                                int total = active + sleep;

                                String msg = "Active: " + active + "\nSleep: " + sleep + "\nTotal: " + total;

                                {
                                    Tooltip tooltip = new Tooltip();
                                    tooltip.setText(msg);
                                    Tooltip.install(activeValue.getNode(), tooltip);
                                }

                                {
                                    Tooltip tooltip = new Tooltip();
                                    tooltip.setText(msg);
                                    Tooltip.install(sleepValue.getNode(), tooltip);
                                }

                                if (total > max) {
                                    max = total;
                                }
                            }

                            wiseGrid.getItems().clear();
                            wiseGrid.getItems().addAll(gridData);

                        });
                    } catch (Throwable ex) {
                        log.error("Error", ex);
                    }
                }
            }
        }
    }

    private void initBufferPoolTables() {

        bufferPoolBPSizeColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).size());
        bufferPoolBPFreeBuffsColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).freeBuffs());
        bufferPoolBPPagesColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).pages());
        bufferPoolBPDirtyPagesColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).dirtyPages());
        bufferPoolBPHitRateColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).hitRate());
        bufferPoolBPMemoryColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).memory());
        bufferPoolBPAddPoolColumn.setCellValueFactory(cellData -> ((BufferPoolData) cellData.getValue()).addPool());

        bufferPoolPSReadsColumn.setCellValueFactory(cellData -> ((PagesStatisticData) cellData.getValue()).reads());
        bufferPoolPSWritesColumn.setCellValueFactory(cellData -> ((PagesStatisticData) cellData.getValue()).writes());
        bufferPoolPSCreatedColumn.setCellValueFactory(cellData -> ((PagesStatisticData) cellData.getValue()).created());
        bufferPoolPSReadsSecColumn.setCellValueFactory(cellData -> ((PagesStatisticData) cellData.getValue()).readsSec());
        bufferPoolPSWritesSecColumn.setCellValueFactory(cellData -> ((PagesStatisticData) cellData.getValue()).writesSec());
        bufferPoolPSCreatesSecColumn.setCellValueFactory(cellData -> ((PagesStatisticData) cellData.getValue()).createdSec());

//        bufferPoolIBInsertsColumn.setCellValueFactory(cellData -> ((InsertBuffersData)cellData.getValue()).inserts());
//        bufferPoolIBMergedRecsColumn.setCellValueFactory(cellData -> ((InsertBuffersData)cellData.getValue()).mergedRecs());
//        bufferPoolIBMergesColumn.setCellValueFactory(cellData -> ((InsertBuffersData)cellData.getValue()).merges());
//        bufferPoolIBSizeColumn.setCellValueFactory(cellData -> ((InsertBuffersData)cellData.getValue()).size());
//        bufferPoolIBFreeListLenColumn.setCellValueFactory(cellData -> ((InsertBuffersData)cellData.getValue()).freeListLen());
//        bufferPoolIBSegSizeColumn.setCellValueFactory(cellData -> ((InsertBuffersData)cellData.getValue()).segSize());
//        
//        bufferPoolAHISizeColumn.setCellValueFactory(cellData -> ((AdaptiveHashIndexData)cellData.getValue()).size());
//        bufferPoolAHICellsUsedColumn.setCellValueFactory(cellData -> ((AdaptiveHashIndexData)cellData.getValue()).cellsUsed());
//        bufferPoolAHINodeHeapBuffsColumn.setCellValueFactory(cellData -> ((AdaptiveHashIndexData)cellData.getValue()).nodeHeapBuffs());
//        bufferPoolAHIHashSecColumn.setCellValueFactory(cellData -> ((AdaptiveHashIndexData)cellData.getValue()).hashSec());
//        bufferPoolAHINonHashSecColumn.setCellValueFactory(cellData -> ((AdaptiveHashIndexData)cellData.getValue()).nonHashSec());
    }

    private void initRowOperationsTables() {

        rowOperationIROInsColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).ins());
        rowOperationIROUpdColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).upd());
        rowOperationIROIReadColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).read());
        rowOperationIRODelColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).del());
        rowOperationIROInsSecColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).insSec());
        rowOperationIROUpdSecColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).updSec());
        rowOperationIROReadSecColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).readSec());
        rowOperationIRODelSecColumn.setCellValueFactory(cellData -> ((InnodbRowOperationsData) cellData.getValue()).delSec());

        bufferPoolROMQueriesQueuedColumn.setCellValueFactory(cellData -> ((RowOperationMiscData) cellData.getValue()).queriesQueued());
        bufferPoolROMQueriesInsideColumn.setCellValueFactory(cellData -> ((RowOperationMiscData) cellData.getValue()).queriesInside());
        bufferPoolROMRdViewsColumn.setCellValueFactory(cellData -> ((RowOperationMiscData) cellData.getValue()).rdViews());
        bufferPoolROMMainThreadStateColumn.setCellValueFactory(cellData -> ((RowOperationMiscData) cellData.getValue()).mainThreadState());
    }

    private void initOpenTablesTable() {
        openTablesDatabaseColumn.setCellValueFactory(cellData -> ((OpenTablesData) cellData.getValue()).database());
        openTablesTableColumn.setCellValueFactory(cellData -> ((OpenTablesData) cellData.getValue()).table());
        openTablesInUseColumn.setCellValueFactory(cellData -> ((OpenTablesData) cellData.getValue()).inUse());
        openTablesNameLockedColumn.setCellValueFactory(cellData -> ((OpenTablesData) cellData.getValue()).nameLocked());
    }

    private void initAnalyzerQueryTable() {

        final HashMap<Integer, String> rowStyleProperties = new HashMap(10);

        TableColumn userColumn = new TableColumn("User");
        userColumn.setCellValueFactory(new PropertyValueFactory<>("user"));
        userColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, String>, TableCell<ProfilerQueryData, String>>() {
            @Override
            public TableCell<ProfilerQueryData, String> call(TableColumn<ProfilerQueryData, String> param) {
                return new StringLimitTableCell(16);
            }
        });

        TableColumn hostColumn = new TableColumn("Host");
        hostColumn.setCellValueFactory(new PropertyValueFactory<>("host"));
        hostColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, String>, TableCell<ProfilerQueryData, String>>() {
            @Override
            public TableCell<ProfilerQueryData, String> call(TableColumn<ProfilerQueryData, String> param) {
                return new StringLimitTableCell(16);
            }
        });

        TableColumn queryTemplateColumn = new TableColumn("Query");
        queryTemplateColumn.setCellValueFactory(new PropertyValueFactory<>("queryTemplate"));
        queryTemplateColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, String>, TableCell<ProfilerQueryData, String>>() {
            @Override
            public TableCell<ProfilerQueryData, String> call(TableColumn<ProfilerQueryData, String> param) {
                return new QueryTableCell<>(10, (item) -> new String("" + (item != null ? item.toString().length() : 0)), (data, item) -> showPopup(new Label(item), data.getButton()));
            }
        });

        TableColumn explainColumn = new TableColumn("Explain");
        explainColumn.setCellValueFactory(new PropertyValueFactory<>("query"));
//        explainColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, Integer>, TableCell<ProfilerQueryData, Integer>>() {
//            @Override
//            public TableCell<ProfilerQueryData, Integer> call(TableColumn<ProfilerQueryData, Integer> param) {
//                return new ExplainTableCell();
//                    }                    
//        });

        explainColumn.setCellFactory(new Callback<TableColumn<DebugExplainProvider, String>, TableCell<DebugExplainProvider, String>>() {
            @Override
            public TableCell<DebugExplainProvider, String> call(TableColumn<DebugExplainProvider, String> param) {
                return new DebuggerExplainTableCell() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn threadsColumn = new TableColumn("Threads");
        threadsColumn.setCellValueFactory(new PropertyValueFactory<>("threads"));
        threadsColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, BigDecimal>, TableCell<ProfilerQueryData, BigDecimal>>() {
            @Override
            public TableCell<ProfilerQueryData, BigDecimal> call(TableColumn<ProfilerQueryData, BigDecimal> param) {
                return new ThreadsTableCell<>();
            }
        });

        TableColumn timeColumn = new TableColumn("Time");
        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
        timeColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, Number>, TableCell<ProfilerQueryData, Number>>() {
            @Override
            public TableCell<ProfilerQueryData, Number> call(TableColumn<ProfilerQueryData, Number> param) {
                return new TimeTableCell();
            }
        });

        TableColumn qedtColumn = new TableColumn("QEDT");
        qedtColumn.setCellValueFactory(new PropertyValueFactory<>("graph1"));
        qedtColumn.setCellFactory(new Callback<TableColumn<GraphData, String>, TableCell<GraphData, String>>() {
            @Override
            public TableCell<GraphData, String> call(TableColumn<GraphData, String> param) {
                return new GraphQEDTTableCell(DebuggerTabController.this.getStage());
            }
        });

        TableColumn qetiColumn = new TableColumn("QETI");
        qetiColumn.setCellValueFactory(new PropertyValueFactory<>("graph2"));
        qetiColumn.setCellFactory(new Callback<TableColumn<GraphData, String>, TableCell<GraphData, String>>() {
            @Override
            public TableCell<GraphData, String> call(TableColumn<GraphData, String> param) {
                return new GraphQETITableCell(DebuggerTabController.this.getStage());
            }
        });

        analyzerQueryTable.addTableColumn(userColumn, 0.07);
        analyzerQueryTable.addTableColumn(hostColumn, 0.07);
        analyzerQueryTable.addTableColumn(queryTemplateColumn, 0.41);
        analyzerQueryTable.addTableColumn(explainColumn, 0.09);
        analyzerQueryTable.addTableColumn(threadsColumn, 0.09);
        analyzerQueryTable.addTableColumn(timeColumn, 0.09);
        analyzerQueryTable.addTableColumn(qedtColumn, 0.04);
        analyzerQueryTable.addTableColumn(qetiColumn, 0.04);

        // bind column size
        timeColumn.setSortType(TableColumn.SortType.DESCENDING);
        analyzerQueryTable.addColumnToSortOrder(timeColumn);

        // add for slow and lock queries tables SQL Optimizer button
        TableColumn analyzeColumn = new TableColumn("Analyze");
        analyzeColumn.setCellValueFactory(new PropertyValueFactory<>("queryId"));
        analyzeColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, Integer>, TableCell<ProfilerQueryData, Integer>>() {
            @Override
            public TableCell<ProfilerQueryData, Integer> call(TableColumn<ProfilerQueryData, Integer> param) {
                return new AnalyzeButtonCell("", StandardDefaultImageProvider.getInstance().getAnalyzer_image());
            }
        });

        analyzerQueryTable.addTableColumn(analyzeColumn, 0.05);
    }

    private void initConfigurationDebuggerTable() {

        configurationDebuggerTable.getTable().setFixedCellSize(30.0);

        TableColumn nameColumn = createColumn("Name", "name");
        nameColumn.setCellFactory(new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(30, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setTooltip(new Tooltip(item));
                        //setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
                    }
                };
            }
        });

        TableColumn valueColumn = createColumn("Value", "value");
        valueColumn.setCellFactory(new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(25, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setTooltip(new Tooltip(item));
                        //setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
                    }
                };
            }
        });

        TableColumn typeColumn = createColumn("Status", "status");
        typeColumn.setCellFactory(new Callback<TableColumn<TunerResult, TunerResult.Status>, TableCell<TunerResult, TunerResult.Status>>() {
            @Override
            public TableCell<TunerResult, TunerResult.Status> call(TableColumn<TunerResult, TunerResult.Status> param) {
                return new TunerStatusCell();
            }
        });

        TableColumn messageColumn = createColumn("Message", "message");
        messageColumn.setCellFactory(new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(55, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setTooltip(new Tooltip(item));
                        //setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
                    }
                };
            }
        });

        TableColumn detailsColumn = createColumn("Details", "details");
        detailsColumn.setCellFactory(new Callback<TableColumn<FilterableData, String>, TableCell<FilterableData, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new QueryTableCell<FilterableData, String>(55, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showMoreData(data, text, "More")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setTooltip(new Tooltip(item));
                        //setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
                    }
                };
            }
        });

        configurationDebuggerTable.addTableColumn(nameColumn, 250, 250, false);
        configurationDebuggerTable.addTableColumn(valueColumn, 0.15);
        configurationDebuggerTable.addTableColumn(typeColumn, 0.05, 50);
        configurationDebuggerTable.addTableColumn(messageColumn, 0.28);
        configurationDebuggerTable.addTableColumn(detailsColumn, 0.28);

        configurationDebuggerTable.getTable().getSelectionModel().setCellSelectionEnabled(true);

        new TableViewCellCopyHandler<TunerResult>(configurationDebuggerTable.getTable()) {
            @Override
            protected String copyCell(TunerResult row, TableColumn column) {
                switch (((Label) column.getGraphic()).getText()) {
                    case "#":
                        return "" + configurationDebuggerTable.getTable().getSelectionModel().getSelectedIndex() + 1;
                    case "Name":
                        return row.getName();
                    case "Value":
                        return row.getValue();
                    case "Status":
                        return "";
                    case "Message":
                        return row.getMessage();
                    case "Details":
                        return row.getDetails();
                }
                return "";
            }
        };
    }
    
    
    private void initErrorLogTable(PaginatorFilterableTableView<ErrorLogResult> errorLogTable) {

        errorLogTable.getTableView().getTable().setFixedCellSize(30.0);

        TableColumn timeColumn = createColumn("Time", "time");
        timeColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
//        TableColumn threadColumn = createColumn("Thread", "thread");
//        threadColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
//            @Override
//            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
//                return new SimpleErrorLogTableCell();
//            }
//        });
        
        TableColumn labelColumn = createColumn("Label", "label");
        labelColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn errorCodeColumn = createColumn("Error code", "errCode");
        errorCodeColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn subsystemColumn = createColumn("Subsystem", "subsystem");
        subsystemColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn messageColumn = createColumn("Message", "message");
        messageColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SearchIssueCell<>(ErrorLogResult :: isError);
            }
        });

        errorLogTable.getTableView().addTableColumn(timeColumn, 0.2, 250, false);
//        errorLogTable.getTableView().addTableColumn(threadColumn, 0.05);
        errorLogTable.getTableView().addTableColumn(labelColumn, 0.15, 50);
        if (service.getDbSQLVersionMaj() == 8) {
            errorLogTable.getTableView().addTableColumn(errorCodeColumn, 0.1);
            errorLogTable.getTableView().addTableColumn(subsystemColumn, 0.1);
            errorLogTable.getTableView().addTableColumn(messageColumn, 0.4);
        } else {
            errorLogTable.getTableView().addTableColumn(messageColumn, 0.6);
        }
        

        errorLogTable.getTableView().getTable().getSelectionModel().setCellSelectionEnabled(true);

        new TableViewCellCopyHandler<ErrorLogResult>(errorLogTable.getTableView().getTable()) {
            @Override
            protected String copyCell(ErrorLogResult row, TableColumn column) {
                switch (((Label) column.getGraphic()).getText()) {
                    case "#":
                        return "" + configurationDebuggerTable.getTable().getSelectionModel().getSelectedIndex() + 1;
                    case "Time":
                        return row.getTime();
                    case "Thread":
                        return row.getThread();
                    case "Label":
                        return row.getLabel();
                    case "Error code":
                        return row.getErrCode();
                    case "Subsystem":
                        return row.getSubsystem();
                    case "Message":
                        return row.getMessage();
                }
                return "";
            }
        };
    }
    
    private void initErrorLogTable2(PaginatorFilterableTableView<ErrorLogResult> errorLogTable) {

        errorLogTable.getTableView().getTable().setFixedCellSize(30.0);

        TableColumn labelColumn = createColumn("Label", "label");
        labelColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn errorCodeColumn = createColumn("Error code", "errCode");
        errorCodeColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn countColumn = createColumn("Count", "count");
        countColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, Integer>, TableCell<ErrorLogResult, Integer>>() {
            @Override
            public TableCell<ErrorLogResult, Integer> call(TableColumn<ErrorLogResult, Integer> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn startTimeColumn = createColumn("Start Time", "minTime");
        startTimeColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });
        
        TableColumn endTimeColumn = createColumn("End Time", "maxTime");
        endTimeColumn.setCellFactory(new Callback<TableColumn<ErrorLogResult, String>, TableCell<ErrorLogResult, String>>() {
            @Override
            public TableCell<ErrorLogResult, String> call(TableColumn<ErrorLogResult, String> param) {
                return new SimpleErrorLogTableCell<>();
            }
        });

        
//        errorLogTable.getTableView().addTableColumn(threadColumn, 0.05);
        errorLogTable.getTableView().addTableColumn(labelColumn, 0.25, 50);
        if (service.getDbSQLVersionMaj() == 8) {
            errorLogTable.getTableView().addTableColumn(errorCodeColumn, 0.15);
            errorLogTable.getTableView().addTableColumn(countColumn, 0.15);
            errorLogTable.getTableView().addTableColumn(startTimeColumn, 0.2);
            errorLogTable.getTableView().addTableColumn(endTimeColumn, 0.2);
        } else {
            errorLogTable.getTableView().addTableColumn(countColumn, 0.15);
            errorLogTable.getTableView().addTableColumn(startTimeColumn, 0.25);
            errorLogTable.getTableView().addTableColumn(endTimeColumn, 0.25);
        }
        

        errorLogTable.getTableView().getTable().getSelectionModel().setCellSelectionEnabled(true);

        new TableViewCellCopyHandler<ErrorLogResult>(errorLogTable.getTableView().getTable()) {
            @Override
            protected String copyCell(ErrorLogResult row, TableColumn column) {
                switch (((Label) column.getGraphic()).getText()) {
                    case "#":
                        return "" + configurationDebuggerTable.getTable().getSelectionModel().getSelectedIndex() + 1;
                    case "Time":
                        return row.getTime();
                    case "Label":
                        return row.getLabel();
                    case "Error code":
                        return row.getErrCode();
                    case "Count":
                        return "" + row.getCount();
                }
                return "";
            }
        };
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public class AnalyzeButtonCell extends TableCell<ProfilerQueryData, Integer> {

        final Button cellButton;

        AnalyzeButtonCell(String text, Image image) {
            cellButton = new Button(text, new ImageView(image));
            cellButton.setOnAction((ActionEvent event) -> {
                try {

                    String query = profilerService.getQueryById(getItem());

                    Database database = DatabaseCache.getInstance().getDatabase(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), getDatabaseName());
                    if (database == null) {
                        database = DatabaseCache.getInstance().syncDatabase(getDatabaseName(), getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), (AbstractDBService) service);
                    }
                    if (database != null) {

                        ProfilerQueryData p = (ProfilerQueryData) getTableRow().getItem();

                        usageService.savePUsage(PUsageFeatures.DQA);
                        
                        showExplainPopup(p, query, AnalyzeButtonCell.this, false);
                        
                        
//                        boolean useCache = preferenceService.getPreference().isQaOtherUseCache();
//                        
//                        controller.usedQueryAnalyzerFromProfiler();
//                        QACache.QARecommendationCache qaRecommendations = QACache.getQARecommandation(controller, DebuggerTabController.this, controller.getSelectedConnectionSession().get(), database, query, !useCache);
////                        
////                        QueryAnalyzer qa = new QueryAnalyzer(database, controller.getSelectedConnectionSession().get().getConnectionParam(), service, query);
////                        qa.analyze(null);
//
//                        TableView<AnalyzerResult.RecommendationResult> analyzerResultTable = createTable();
//                        for (String table: qaRecommendations.getIndexes().keySet()) {
//                            List<QACache.QAIndexRecommendation> indexes = qaRecommendations.getIndexes().get(table);
//                            if (indexes != null) {
//                                for (QACache.QAIndexRecommendation rec: indexes) {
//                                    analyzerResultTable.getItems().add(AnalyzerResult.createRecomendation(
//                                        null, 
//                                        false, 
//                                        qaRecommendations.getDatabase(), 
//                                        table, 
//                                        rec.getRecommendation(), 
//                                        rec.getIndexName(), 
//                                        rec.getIndexQuery(), 
//                                        "", 
//                                        qaRecommendations.getOptimizedQuery(), 
//                                        "",  
//                                        null));
//                                }
//                            }
//                        }
////                        prepareAnalyzerRows(analyzerResultTable, qa.getAnalyzerResult());
//
//                        threadsAction(true);
//                        Popup popup = showPopup(analyzerResultTable, cellButton);
//                        popup.setOnCloseRequest((WindowEvent event1) -> {
//                            threadsAction(false);
//                        });
                    }

                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", "Error analyzing", ex);
                }
            });
            cellButton.setFocusTraversable(false);
        }

        @Override
        protected void updateItem(Integer t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
                setAlignment(Pos.CENTER);
            } else {
                setGraphic(null);
            }
        }
        
        private String getDatabaseName() {
        int row = getTableRow().getIndex();
        if (row >= 0 && row < getTableView().getItems().size()) {
            ProfilerQueryData pqd = getTableView().getItems().get(row);
            if (pqd != null && pqd.getDatabase() != null && !pqd.getDatabase().isEmpty()) {
                return pqd.getDatabase();
            }
        }

        if (getController().getSelectedConnectionSession().get().getConnectionParam().getSelectedDatabase() != null) {
            return getController().getSelectedConnectionSession().get().getConnectionParam().getSelectedDatabase().getName();
        }

        return null;
    }
        
    }

    private TableView<AnalyzerResult.RecommendationResult> createTable() {
        TableView<AnalyzerResult.RecommendationResult> analyzerResultTable = new TableView<>();
        analyzerResultTable.setPrefSize(700, 150);
        analyzerResultTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<AnalyzerResult.RecommendationResult, Number> indexColumn = new TableColumn<>("SNo");
        indexColumn.setSortable(false);
        indexColumn.setMaxWidth(50);
        indexColumn.setMinWidth(50);
        indexColumn.setCellValueFactory(column -> new ReadOnlyObjectWrapper<>(analyzerResultTable.getItems().indexOf(column.getValue()) + 1));

        //select * from actor where first_name=1
        TableColumn<AnalyzerResult.RecommendationResult, String> tableNameColumn = new TableColumn<>("Table name");
        tableNameColumn.setCellValueFactory(new PropertyValueFactory<>("tableName"));

        TableColumn<AnalyzerResult.RecommendationResult, String> recommendationColumn = new TableColumn<>("Recommendation");
        recommendationColumn.setCellValueFactory(new PropertyValueFactory<>("recommendation"));

        TableColumn<AnalyzerResult.RecommendationResult, String> sqlCommandColumn = new TableColumn<>("SQL command");
        sqlCommandColumn.setCellValueFactory(new PropertyValueFactory<>("sqlCommand"));

        TableColumn<AnalyzerResult.RecommendationResult, Boolean> executionButtonColumn = new TableColumn<>("Execution button");
        executionButtonColumn.setCellValueFactory(new PropertyValueFactory<>("executed"));
        executionButtonColumn.setCellFactory((TableColumn<AnalyzerResult.RecommendationResult, Boolean> p) -> new ExecuteButtonCell());
        executionButtonColumn.setSortable(false);

        analyzerResultTable.getColumns().addAll(indexColumn, tableNameColumn, recommendationColumn, sqlCommandColumn, executionButtonColumn);

        return analyzerResultTable;
    }

    private void prepareAnalyzerRows(TableView<AnalyzerResult.RecommendationResult> analyzerResultTable, List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t : list) {
                if (t.getRecomendationsSize() > 0) {
                    for (AnalyzerResult.RecommendationResult rr : t.getRecomendations()) {
                        analyzerResultTable.getItems().add(rr);
                    }
                }

                if (t.getChildren() != null) {
                    prepareAnalyzerRows(analyzerResultTable, t.getChildren());
                }
            }
        }
    }
    
    private class ExecuteButtonCell extends TableCell<AnalyzerResult.RecommendationResult, Boolean> {

        final Button cellButton = new Button("Execute");

        ExecuteButtonCell() {
            cellButton.setOnAction((ActionEvent t) -> {
                AnalyzerResult.RecommendationResult rr = (AnalyzerResult.RecommendationResult) getTableRow().getItem();
                if (rr != null && rr.getSqlCommand() != null && !rr.getSqlCommand().isEmpty()) {
                    rr.setExecuted(Boolean.TRUE);
                    cellButton.setDisable(true);

                    mainUI.setDisable(true);
                    Recycler.addWorkProc(new WorkProc() {
                        @Override
                        public void updateUI() {
                            mainUI.setDisable(false);
                        }

                        @Override
                        public void run() {
                            callUpdate = true;
                            try {
                                service.execute(rr.getSqlCommand());
                            } catch (SQLException ex) {
                                DialogHelper.showError(getController(), "Error", "Error on execution recommendation command", ex);
                            }
                        }
                    });
                }
            });

            cellButton.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getExecute_image()));
            cellButton.setFocusTraversable(false);
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                cellButton.setDisable(t != null ? t : false);
                setGraphic(cellButton);
                setAlignment(Pos.CENTER);
            } else {
                setGraphic(null);
            }
        }
    }
    
    @FXML
    public void elSummary(ActionEvent event) {
        
        List<ErrorLogResult> list0 = new ArrayList<>();
        Map<String, ErrorLogResult> map = new HashMap<>();
        
        List<ErrorLogResult> list = errorLogTable.getItems();
        for (ErrorLogResult elr: list) {
            ErrorLogResult elr0 = map.get(elr.groupKey());
            if (elr0 == null) {
                elr0 = elr.copuResult();
                map.put(elr.groupKey(), elr0);
                list0.add(elr0);
            }
            
            elr0.incCount();
            elr0.checkTime(elr.getTime());            
        }
        
        
        VBox bp = new VBox();
        bp.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);
        bp.setPrefSize(800, 500);
        PaginatorFilterableTableView<ErrorLogResult> errorLogTable0 = new PaginatorFilterableTableView<>();
        initErrorLogTable2(errorLogTable0);
        
        errorLogTable0.setItems(FXCollections.observableArrayList(list0));
        
        bp.getChildren().add(errorLogTable0);
        
        bp.getStylesheets().addAll(getController().getStage().getScene().getStylesheets());
        
        DialogHelper.show("Summary", bp, getStage());
    }

    
    @FXML
    public void uploadLogAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            forceErrorLogPath = file.getAbsolutePath();            
            elRefreshAction(null);
        }
    }

    @FXML
    public void rqRefreshAction(ActionEvent event) {
        rqForceRun = true;
    }

    @FXML
    public void itRefreshAction(ActionEvent event) {
        itForceRun = true;
    }

    @FXML
    public void dmRefreshAction(ActionEvent event) {
        dmForceRun = true;
    }

    @FXML
    public void dmClearAction(ActionEvent event) {
        deadlockService.deleteAll(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey());
        deadlockMonitorTable.setItems(FXCollections.observableArrayList());
    }

    private class ExplainTableCell extends TableCell<ProfilerQueryData, Integer> {

        @Override
        protected void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);

            ProfilerQueryData p = (ProfilerQueryData) getTableRow().getItem();

            if (!empty && p != null) {

                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

                if (p.getRowsSum() != null) {
                    setGraphic(createRatingNode(p, p.getRowsSum(), item, this));
                } else {
                    Hyperlink link = new Hyperlink("EXPLAIN");
                    link.setFocusTraversable(false);
                    link.setOnAction((ActionEvent event) -> {
                        showExplainPopup(p, item, ExplainTableCell.this, ExplainTableCell.this);
                    });

                    setGraphic(link);
                }
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
    }
    
    @FXML
    public void lmRefreshAction(ActionEvent event) {
        lmForceRun = true;
    }

    @FXML
    public void rqStartAction(ActionEvent event) {
        rqPaused = false;
        rqStartButton.setDisable(true);
        rqPauseButton.setDisable(false);
        rqRefreshButton.setDisable(false);
    }

    @FXML
    public void rqPauseAction(ActionEvent event) {
        rqPaused = true;
        rqStartButton.setDisable(false);
        rqPauseButton.setDisable(true);
        rqRefreshButton.setDisable(true);
    }

    @FXML
    public void itStartAction(ActionEvent event) {
        itPaused = false;
        itStartButton.setDisable(true);
        itPauseButton.setDisable(false);
        itRefreshButton.setDisable(false);
    }

    @FXML
    public void itPauseAction(ActionEvent event) {
        itPaused = true;
        itStartButton.setDisable(false);
        itPauseButton.setDisable(true);
        itRefreshButton.setDisable(true);
    }

    @FXML
    public void dbStartAction(ActionEvent event) {
        dbPaused = false;
        dbStartButton.setDisable(true);
        dbPauseButton.setDisable(false);
        dbRefreshButton.setDisable(false);
    }

    @FXML
    public void dbPauseAction(ActionEvent event) {
        dbPaused = true;
        dbStartButton.setDisable(false);
        dbPauseButton.setDisable(true);
        dbRefreshButton.setDisable(true);
    }
    
    @FXML
    public void dbRefreshAction(ActionEvent event) {
        dbForceRun = true;
    }

    @FXML
    public void rqKillAction(ActionEvent event) {
        DialogResponce rs = DialogHelper.showConfirm(getController(), "Kill query", "Are you sure what kill selected query?");
        if (rs == DialogResponce.OK_YES) {
            DebugRunningQueryData item = (DebugRunningQueryData) runningQueriesTable.getTable().getSelectionModel().getSelectedItem();

            kill(item.getId());
        }
    }

    @FXML
    public void rqKillAllAction(ActionEvent event) {
        DialogResponce rs = DialogHelper.showConfirm(getController(), "Kill all query", "Are you sure what kill all queries?");
        if (rs == DialogResponce.OK_YES) {
            for (Object row : runningQueriesTable.getCopyItems()) {
                kill(((DebugRunningQueryData) row).getId());
            }
        }
    }

    @FXML
    public void itKillAction(ActionEvent event) {
        DialogResponce rs = DialogHelper.showConfirm(getController(), "Kill query", "Are you sure what kill selected Transaction?");
        if (rs == DialogResponce.OK_YES) {
            DebugInnoTopData item = (DebugInnoTopData) innoTopTable.getSelectionModel().getSelectedItem();

            if (item.getId() != null && item.getId() > 0) {
                kill(item.getId());
            }
        }
    }

    @FXML
    public void itKillAllAction(ActionEvent event) {
        DialogResponce rs = DialogHelper.showConfirm(getController(), "Kill all query", "Are you sure what kill all Transactions?");
        if (rs == DialogResponce.OK_YES) {
            for (Object row : innoTopTable.getCopyItems()) {

                DebugInnoTopData item = (DebugInnoTopData) row;
                if (item.getId() != null && item.getId() > 0) {
                    kill(item.getId());
                }
            }
        }
    }

    @FXML
    public void lmStartAction(ActionEvent event) {
        lmPaused = false;
        lmStartButton.setDisable(true);
        lmPauseButton.setDisable(false);
        lmRefreshButton.setDisable(false);
    }

    @FXML
    public void lmPauseAction(ActionEvent event) {
        lmPaused = true;
        lmStartButton.setDisable(false);
        lmPauseButton.setDisable(true);
        lmRefreshButton.setDisable(true);
    }

    @FXML
    public void lmKillAction(ActionEvent event) {
        DialogResponce rs = DialogHelper.showConfirm(getController(), "Kill query", "Are you sure what kill selected query?");
        if (rs == DialogResponce.OK_YES) {
            DebugLockMonitorData item = (DebugLockMonitorData) lockMonitorTable.getTable().getSelectionModel().getSelectedItem();

            kill(item.getId());
        }
    }

    @FXML
    public void lmKillAllAction(ActionEvent event) {
        DialogResponce rs = DialogHelper.showConfirm(getController(), "Kill all query", "Are you sure what kill all queries?");
        if (rs == DialogResponce.OK_YES) {
            for (Object row : lockMonitorTable.getCopyItems()) {
                kill(((DebugLockMonitorData) row).getId());
            }
        }
    }

    private void kill(Long id) {
        try {
            service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__KILL_QUERY), id));
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", ex.getMessage(), null, getStage());
        }
    }

    @FXML
    public void rqQueryAnalyzeAction(ActionEvent event) {
        List<DebugRunningQueryData> items = (List<DebugRunningQueryData>) (List) runningQueriesTable.getCopyItems();

        String queries = "";
        for (DebugRunningQueryData item : items) {
            if (!queries.isEmpty()) {
                if (!queries.trim().endsWith(";")) {
                    queries += ";";
                }

                queries += "\n";
            }
            queries += item.getQuery();
        }

        showExplain(queries);
    }

    @FXML
    public void itQueryAnalyzeAction(ActionEvent event) {
        List<DebugInnoTopData> items = (List<DebugInnoTopData>) (List) innoTopTable.getCopyItems();

        String queries = "";
        for (DebugInnoTopData item : items) {
            if (!queries.isEmpty()) {
                if (!queries.trim().endsWith(";")) {
                    queries += ";";
                }

                queries += "\n";
            }
            queries += item.getQText();
        }

        showExplain(queries);
    }

    @FXML
    public void lmQueryAnalyzeAction(ActionEvent event) {
        List<DebugLockMonitorData> items = (List<DebugLockMonitorData>) (List) lockMonitorTable.getCopyItems();

        String queries = "";
        for (DebugLockMonitorData item : items) {
            if (!queries.isEmpty()) {
                if (!queries.trim().endsWith(";")) {
                    queries += ";";
                }

                queries += "\n";
            }
            queries += item.getQuery();
        }

        showExplain(queries);
    }

    @FXML
    public void dmStartAction(ActionEvent event) {
        dmPaused = false;
        dmStartButton.setDisable(true);
        dmPauseButton.setDisable(false);
        dmRefreshButton.setDisable(false);
    }

    @FXML
    public void dmPauseAction(ActionEvent event) {
        dmPaused = true;
        dmStartButton.setDisable(false);
        dmPauseButton.setDisable(true);
        dmRefreshButton.setDisable(true);
    }

    @FXML
    public void dmPasteAction(ActionEvent event) {
        final Stage dialog = new Stage();

        InputAreaDialogController cellController = StandardBaseControllerProvider.getController(getController(), "InputAreaDialog");
        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle("Deadlock custom text");

        dialog.showAndWait();

        if (cellController.getData() != null) {

            List<DeadlockMonitorQueryData> list = new ArrayList<>();

            readDeadLock(cellController.getData(), list);

            Platform.runLater(() -> {
                deadlockMonitorTable.setItems(FXCollections.observableArrayList(list));
            });
        }
    }

    @FXML
    public void dmQueryAnalyzeAction(ActionEvent event) {
        List<DeadlockMonitorQueryData> items = new ArrayList<>((List<DeadlockMonitorQueryData>) (List) deadlockMonitorTable.getItems());

        String queries = "";
        for (DeadlockMonitorQueryData item : items) {
            if (item.getTransaction1() != null && !item.getTransaction1().isEmpty()) {
                if (!queries.isEmpty()) {
                    if (!queries.trim().endsWith(";")) {
                        queries += ";";
                    }

                    queries += "\n";
                }

                queries += item.getTransaction1();
            }

            if (item.getTransaction2() != null && !item.getTransaction2().isEmpty()) {
                if (!queries.isEmpty()) {
                    if (!queries.trim().endsWith(";")) {
                        queries += ";";
                    }

                    queries += "\n";
                }

                queries += item.getTransaction2();
            }
        }

        showExplain(queries);
    }

    @FXML
    public void rlRefreshAction(ActionEvent event) {
        rlForceRun = true;
    }

    @FXML
    public void rlStartAction(ActionEvent event) {
        replPaused = false;
        rlStartButton.setDisable(true);
        rlPauseButton.setDisable(false);
        rlRefreshButton.setDisable(false);
    }

    @FXML
    public void rlPauseAction(ActionEvent event) {
        replPaused = true;
        rlStartButton.setDisable(false);
        rlPauseButton.setDisable(true);
        rlRefreshButton.setDisable(true);
    }

    private void showExplain(String text) {

        if (text != null) {

            // full usage exipred
            if (licenseFullUsageExipred) {
                return;
            }

            makeBusy(true);
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    if (session != null) {
                        Database database = session.getConnectionParam().getSelectedDatabase();
                        if (database != null) {
                            try {
                                Platform.runLater(() -> {
                                    try {
                                        threadsAction(true);

                                        Stage stage = new Stage();
                                        stage.initOwner(getStage());
                                        stage.initModality(Modality.APPLICATION_MODAL);
                                        stage.setOnCloseRequest((WindowEvent event) -> {
                                            threadsAction(false);
                                        });

                                        // SQL Optimizer
                                        QueryAnalyzerTabController controller = StandardBaseControllerProvider.getController(getController(), "QueryAnalyzerTab");
                                        controller.setService(session.getService());
                                        controller.setPerformanceTuningService(performanceTuningService);
                                        controller.setPreferenceService(preferenceService);
                                        controller.setQars(qars);
                                        controller.setColumnCardinalityService(columnCardinalityService);
                                        controller.setUsageService(usageService);
                                        controller.init(text, session, database, true, RunnedFrom.DQA);

                                        controller.setAreaEditable(false);

                                        Tab queryAnalyzerTab = new Tab("SQL Optimizer");
                                        queryAnalyzerTab.setClosable(false);
                                        queryAnalyzerTab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getQueryAnalyzeTab_image()));

                                        queryAnalyzerTab.setContent(controller.getUI());

                                        Scene scene = new Scene(new BorderPane(), 1000, 600);
                                        ((BorderPane) scene.getRoot()).setCenter(controller.getUI());

                                        stage.setScene(scene);

                                        stage.show();
                                    } finally {
                                        makeBusy(false);
                                    }
                                });
                            } catch (Throwable ex) {
                                Platform.runLater(() -> {
                                    makeBusy(false);
                                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, getStage());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                makeBusy(false);
                                DialogHelper.showInfo(getController(), "SQL Optimizer", "Database and Table/s are not existed in current connected DB server.", null);
                            });
                        }
                    }
                }
            });
        } else {
            Platform.runLater(() -> {
                makeBusy(false);
                DialogHelper.showInfo(getController(), "SQL Optimizer", "Can't call EXPLAIN query", null);
            });
        }
    }

    private boolean timeAllowanceOnCodeBranches(long diffLastUpdate, int entityWaitInSec) {
        return diffLastUpdate >= entityWaitInSec * 1000;
    }

    private void runningQueriesCodeBranch(long timesRunned) {

        if (rqForceRun || !rqPaused && timeAllowanceOnCodeBranches(timesRunned - rqLast, rqWaitInSec)) {
            
            rqLast = timesRunned;
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {
                
                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController && (rqForceRun || bottomTabPane.getSelectionModel().getSelectedItem() == runningQueriesTab)) {

                    rqForceRun = false;
                    
                    String query = QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL,
                            QueryProvider.QUERY__DEBUG_RUNNING_QUERIES
                    );

                    boolean all = false;

                    if (rqTimeGreater0Button.isSelected()) {
                        query = String.format(QueryProvider.getInstance().getQuery(
                                QueryProvider.DB__MYSQL,
                                QueryProvider.QUERY__DEBUG_RUNNING_QUERIES_WITH_TIME),
                                rqTimeCombobox.getSelectionModel().getSelectedItem(), rqTimeField.getText());

                    } else if (rqAvoidSleepButton.isSelected()) {
                        query = QueryProvider.getInstance().getQuery(
                                QueryProvider.DB__MYSQL,
                                QueryProvider.QUERY__DEBUG_RUNNING_QUERIES_AVOID_SLEEP
                        );
                    } else {
                        all = true;
                    }

                    List<DebugRunningQueryData> list = new ArrayList<>();
                    try (ResultSet rs = (ResultSet) service.execute(query)) {

                        if (rs != null) {
                            while (rs.next()) {
//                                for (int i = 0; i < 100; i++) {
                                Double v = rs.getDouble("time");
                                BigDecimal time = v != null ? new BigDecimal(v).setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO;
                                DebugRunningQueryData d = new DebugRunningQueryData(rs.getLong("id"), rs.getString("user"), rs.getString("host"), rs.getString("command"), rs.getString("state"), time, rs.getString("info"), rs.getString("db"));
                                list.add(d);
//                                }
                            }
                        }
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                    
                    Platform.runLater(() -> {

                        PreferenceData pd = preferenceService.getPreference();
                        rqLabel_0_1.setText("B/W " + pd.getDebuggerRQNormal() + " to " + pd.getDebuggerRQMedium() + " sec");
                        rqLabel_1_6.setText("B/W " + pd.getDebuggerRQMedium() + " to " + pd.getDebuggerRQSlow()+ " sec");
                        rqLabel_6.setText("B/W " + pd.getDebuggerRQSlow() + "+ sec");
                        
                        TableColumn timeColumn = runningQueriesTable.getColumnByName("Time");

                        List<DebugRunningQueryData> forRemove = new ArrayList<>();
                        List<DebugRunningQueryData> forAdd = new ArrayList<>();

                        List<DebugRunningQueryData> l = (List<DebugRunningQueryData>) (List) runningQueriesTable.getCopyItems();
                        for (DebugRunningQueryData d : list) {
                            int index = l.indexOf(d);
                            if (index == -1) {
                                forAdd.add(d);
                            } else {
                                l.get(index).setTime(d.getTime());
                            }
                        }

                        for (DebugRunningQueryData d : l) {
                            if (!list.contains(d)) {
                                forRemove.add(d);
                            }
                        }
                        
                        runningQueriesTable.addAndRemoveItems(forAdd, forRemove);
                        
                        Map<String, Integer> colorCounts = new HashMap<>();
                        for (DebugRunningQueryData item: (List<DebugRunningQueryData>)(List)runningQueriesTable.getSourceData()) {
                            String color = item.getColor(preferenceService.getPreference());
                            Integer count = colorCounts.get(color);
                            if (count == null) {
                                count = 0;
                            }
                            
                            colorCounts.put(color, count + 1);
                        }
                        timeColumn.setVisible(false);
                        timeColumn.setVisible(true);
                        
//                        runningQueriesTextFlow.getChildren().clear();
                        String highColor = null;
                        
                        Integer greenCount = colorCounts.get(DebugTimeProvider.COLOR_GREEN);
                        rqText_0_1.setText(String.valueOf(greenCount != null ? greenCount : 0));                        
                                                
                        Integer lightRedCount0 = colorCounts.get(DebugTimeProvider.COLOR_RED_MORE_LIGHT);
                        Integer lightRedCount1 = colorCounts.get(DebugTimeProvider.COLOR_RED_LIGHT);
                        if (lightRedCount0 == null) {
                            lightRedCount0 = 0;
                        }
                        
                        if (lightRedCount1 != null) {
                            lightRedCount0 += lightRedCount1;
                        }
                        
                        rqText_1_6.setText(String.valueOf(lightRedCount0));
                        
                        Integer redCount = colorCounts.get(DebugTimeProvider.COLOR_RED);
                        rqText_6.setText(String.valueOf(redCount != null ? redCount : 0));
                        
                        if (!colorCounts.isEmpty()) {
                                                       
                            for (String c: DebugTimeProvider.COLORS) {
                                if (c != DebugTimeProvider.COLOR_WHITE) {
                                    Integer count = colorCounts.get(c);
                                    if (count != null) {

                                        if (highColor == null) {
                                            highColor = c;
                                        }                                       
                                    }
                                }
                            }
                        }
                        
                        if (preferenceService.getPreference().isDebuggerAlertRQ()) {
                            if (highColor == null) {
                                runningQueriesButton.setId("debugerButton_default");
                                removeBlink(runningQueriesButton);
                                rqRed = false;
                                rqGreen = false;
                            } else {
                                switch (highColor) {
                                    case DebugTimeProvider.COLOR_GREEN:
                                        runningQueriesButton.setId("debugerButton_green");
                                        removeBlink(runningQueriesButton);
                                        rqRed = false;
                                        rqGreen = true;
                                        break;

                                    case DebugTimeProvider.COLOR_RED:
                                        runningQueriesButton.setId("debugerButton_red");
                                        addBlink(runningQueriesButton);
                                        rqRed = true;
                                        rqGreen = false;
                                        break;

                                    case DebugTimeProvider.COLOR_RED_LIGHT:
                                    case DebugTimeProvider.COLOR_RED_MORE_LIGHT:
                                        runningQueriesButton.setId("debugerButton_light_red");
                                        addBlink(runningQueriesButton);
                                        rqRed = true;
                                        rqGreen = false;
                                        break;
                                }
                            }
                        } else {
                            runningQueriesButton.setId("debugerButton_default");
                            removeBlink(runningQueriesButton);
                            rqRed = false;
                            rqGreen = false;
                        }
                    });

                    if (all && list.isEmpty() && !rqShowed) {
                        rqShowed = true;
                        Platform.runLater(() -> DialogHelper.showInfo(getController(), "Running queries", "The server doesn't has issue with long running queries", null, getStage()));
                    }
                }
            }
        }
    }
    
    private Map<Label, FadeTransition> blinksMap = new HashMap<>();
    private synchronized void addBlink(Label label) {

        FadeTransition fadeTransition = blinksMap.get(label);
        if (fadeTransition == null) {
            fadeTransition = new FadeTransition(Duration.seconds(1), label);
            fadeTransition.setFromValue(1.0);
            fadeTransition.setToValue(0.4);
            fadeTransition.setCycleCount(Animation.INDEFINITE);
            if (blinkEnabled) {
                fadeTransition.play();
            }

            blinksMap.put(label, fadeTransition);
        }
    }
    
    private synchronized void removeBlink(Label label) {
        FadeTransition fadeTransition = blinksMap.remove(label);
        if (fadeTransition != null) {
            fadeTransition.stop();
        }
    }
    
    private Map<String, Double> GLOBAL_VARIABLES = new HashMap<>();
    private Map<String, Double[]> GLOBAL_STATUS = new HashMap<>();
    private Double oldPagesFlushed = null;
    
    private long serverTimeDiff = 0l;
    
    @FXML
    private Tab innodbStatusTab;

    @FXML
    private Tab myISAMTab;

    // REFACTOR THIS!!!!
    // ALL THIS CODE!!!!
    private Date dateInit = new Date();
    private synchronized void chartsCodeBranch(long timesRunned) {
        
        if (bottomTabPane.getSelectionModel().getSelectedItem() == innodbStatusTab && innodbMatrixController.getStartDate() == null || 
                bottomTabPane.getSelectionModel().getSelectedItem() == myISAMTab && myISAMController.getStartDate() == null) {
            
            innodbMatrixController.startProfiler(dateInit);
            myISAMController.startProfiler(dateInit);
            
            chartsThread = new Thread(() -> {
               while (myISAMController.getStartDate() != null && innodbMatrixController.getStartDate() != null)  {
                   
                   if (myISAMController.getInnerControllerWorkProcAuthority().shouldExecuteWorkProc()) {
                       myISAMController.getInnerControllerWorkProcAuthority().runWork();
                       try{ Thread.sleep(100); }catch(Exception e){}
                   }
                   
                   if (innodbMatrixController.getInnerControllerWorkProcAuthority().shouldExecuteWorkProc()) {
                       innodbMatrixController.getInnerControllerWorkProcAuthority().runWork();
                       try{ Thread.sleep(100); }catch(Exception e){}
                   }
                   
                   try{ Thread.sleep(250); }catch(Exception e){}
               }
            });
            chartsThread.setDaemon(true);
            chartsThread.start();
        }
        
        
        if (chartsForceRun || timeAllowanceOnCodeBranches(timesRunned - chartsLast, chartsWaitInSec)) {
            
            chartsForceRun = false;
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {

                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController) {
            
                    chartsLast = timesRunned;

                    String globalVariablesQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_VARIABLES);
                    String globalStatusQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_STATUS);
                    String eisQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ENGINE_INNODB_STATUS);
                    String openTablesQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_OPEN_TABLES);
                    String openTablesQuery0 = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_OPEN_TABLES_0);
                    String openTablesQuery1 = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_OPEN_TABLES_1);
                    String unusedIndexesQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_SUMMARY);
                    String duplicateIndexesQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_DUBLICATE);
                    String dataSizeQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASES_SIZE);

                    String query = "SELECT now();";
                            
                    query += globalVariablesQuery;
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }

                    query += globalStatusQuery;
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }

                    query += eisQuery;
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }

                    query += openTablesQuery;
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }

                    query += openTablesQuery0;
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }

                    query += openTablesQuery1;
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }
                    
                    if (!reportRunned) {
                        query += unusedIndexesQuery;
                        if (!query.trim().endsWith(";")) {
                            query += ";";
                        }

                        query += duplicateIndexesQuery;
                        if (!query.trim().endsWith(";")) {
                            query += ";";
                        }
                    
                        query += dataSizeQuery;
                        if (!query.trim().endsWith(";")) {
                            query += ";";
                        }
                    }
                    
                    QueryResult qr = new QueryResult();
                    try {
                        service.execute(query, qr);

                        ResultSet rs = (ResultSet)qr.getResult().get(0);
                        if (rs != null && rs.next()) {
                            serverTimeDiff = new Date().getTime() - rs.getTimestamp(1).getTime();
                        }
                        rs.close();
                        
                        rs = (ResultSet)qr.getResult().get(1);
                        while (rs.next()) {
                            Object obj = rs.getObject(2);
                            Double result = 0.0;
                            try {
                                result = new BigDecimal(obj.toString()).doubleValue();
                            } catch (Throwable th) {            
                            }
                            GLOBAL_VARIABLES.put(rs.getString(1), result);
                        }
                        rs.close();

                        rs = (ResultSet)qr.getResult().get(2);                
                        while (rs.next()) {
                            String key = rs.getString(1);
                            Object obj = rs.getObject(2);
                            Double result = 0.0;
                            try {
                                result = new BigDecimal(obj.toString()).doubleValue();
                            } catch (Throwable th) {            
                            }

                            Double[] old = GLOBAL_STATUS.get(key);
                            if (old != null) {
                                old[1] = result - old[0];
                                old[0] = result;
                            } else {                    
                                GLOBAL_STATUS.put(key, new Double[] {result, 0.0});
                            }
                            debuggerProfilerService.addGlobalStatus(timesRunned, key, result);
                        }
                        rs.close();
                        
                        Platform.runLater(() -> {
                        
                            // IBP size: Innodb_buffer_pool_pages_total*Innodb_page_size
                            {
                                Double[] v0 = GLOBAL_STATUS.get("Innodb_buffer_pool_pages_total");
                                Double[] v1 = GLOBAL_STATUS.get("Innodb_page_size");

                                Double value = 0.0;
                                if (v0 != null && v1 != null) {
                                    value = v0[0] * v1[0];
                                }
                                
                                bpText_size.setText(convertToGB(value, true, "0 MB"));
                            }

                            // IBP dirty size: Innodb_buffer_pool_bytes_dirty
                            {
                                Double[] v0 = GLOBAL_STATUS.get("Innodb_buffer_pool_bytes_dirty");

                                Double value = v0[0];
                                
                                bpText_dirtySize.setText(convertToGB(value, true, "0 MB"));
                            }

                            // IBP free size: Innodb_buffer_pool_pages_free *Innodb_page_size
                            {
                                Double[] v0 = GLOBAL_STATUS.get("Innodb_buffer_pool_pages_free");
                                Double[] v1 = GLOBAL_STATUS.get("Innodb_page_size");

                                Double value = 0.0;
                                if (v0 != null && v1 != null) {
                                    value = v0[0] * v1[0];
                                }
                               
                                bpText_freeSize.setText(convertToGB(value, true, "0 MB"));
                            }
                        });

                        BufferPoolData bufferPoolData = bufferPoolTable.getItems().isEmpty() ? null : bufferPoolTable.getItems().get(0);
                        if (bufferPoolData == null) {
                            bufferPoolData = new BufferPoolData();
                            BufferPoolData staticBufferPoolData = bufferPoolData;
                            Platform.runLater(() -> {
                                bufferPoolTable.getItems().add(staticBufferPoolData);
                            });
                        }

                        PagesStatisticData pagesStatisticData = pageStatisticTable.getItems().isEmpty() ? null : pageStatisticTable.getItems().get(0);
                        if (pagesStatisticData == null) {
                            pagesStatisticData = new PagesStatisticData();
                            PagesStatisticData staticPagesStatisticData = pagesStatisticData;
                            Platform.runLater(() -> {
                                pageStatisticTable.getItems().add(staticPagesStatisticData);
                            });
                        }

        //                InsertBuffersData insertBuffersData = insertBuffersTable.getItems().isEmpty() ? null : insertBuffersTable.getItems().get(0);
        //                if (insertBuffersData == null) {
        //                    insertBuffersData = new InsertBuffersData();
        //                    insertBuffersTable.getItems().add(insertBuffersData);
        //                }

        //                AdaptiveHashIndexData adaptiveHashIndexData = adaptiveHashIndexTable.getItems().isEmpty() ? null : adaptiveHashIndexTable.getItems().get(0);
        //                if (adaptiveHashIndexData == null) {
        //                    adaptiveHashIndexData = new AdaptiveHashIndexData();
        //                    adaptiveHashIndexTable.getItems().add(adaptiveHashIndexData);
        //                }

                        InnodbRowOperationsData innodbRowOperationsData = innodbRowOperationsTable.getItems().isEmpty() ? null : innodbRowOperationsTable.getItems().get(0);
                        if (innodbRowOperationsData == null) {
                            innodbRowOperationsData = new InnodbRowOperationsData();
                            InnodbRowOperationsData staticInnodbRowOperationsData = innodbRowOperationsData;
                            Platform.runLater(() -> {
                                innodbRowOperationsTable.getItems().add(staticInnodbRowOperationsData);
                            });
                        }

                        RowOperationMiscData rowOperationMiscData = rowOperationMiscTable.getItems().isEmpty() ? null : rowOperationMiscTable.getItems().get(0);
                        if (rowOperationMiscData == null) {
                            rowOperationMiscData = new RowOperationMiscData();
                            RowOperationMiscData staticRowOperationMiscData = rowOperationMiscData;
                            Platform.runLater(() -> {
                                rowOperationMiscTable.getItems().add(staticRowOperationMiscData);
                            });
                        }

                        rs = (ResultSet)qr.getResult().get(3);
                        while (rs.next()) {

                            Double innodbPageSize = getDoubleGlobalStatus("Innodb_page_size", true);
                            Double innodbBufferPoolPagesTotal = getDoubleGlobalStatus("Innodb_buffer_pool_pages_total", true);
                            Double innodbBufferPoolPagesDirty = getDoubleGlobalStatus("Innodb_buffer_pool_pages_dirty", true);

                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL__DATA_TOTAL", innodbBufferPoolPagesTotal * innodbPageSize);
                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL__DATA_DIRTY", innodbBufferPoolPagesDirty * innodbPageSize);

                            Double innodbBufferPoolReadRequests = getDoubleGlobalStatus("Innodb_buffer_pool_read_requests", true);
                            Double innodbBufferPoolReads = getDoubleGlobalStatus("Innodb_buffer_pool_reads", true);

                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_IO__HIT_RATIO", (innodbBufferPoolReadRequests / (innodbBufferPoolReadRequests + innodbBufferPoolReads)) * 100);

                            String text = rs.getString(3).replace("\r", "");

                            boolean done0 = false;
                            boolean done1 = false;
                            boolean done2 = false;
                            boolean done3 = false;
                            boolean done4 = false;
                            boolean done5 = false;

                            Matcher m = Flags.PATTERN__ROW_OPERATIONS.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                                    m = Flags.PATTERN__ROW_OPERATIONS_0.matcher(data);
                                    if (m != null && m.find()) {

                                        innodbRowOperationsData.ins().set(m.group(1));
                                        innodbRowOperationsData.upd().set(m.group(2));
                                        innodbRowOperationsData.del().set(m.group(3));
                                        innodbRowOperationsData.read().set(m.group(4));

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__INSERTED", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__INSERTED", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__UPDATED", Double.valueOf(m.group(2)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__UPDATED", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__DELETED", Double.valueOf(m.group(3)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__DELETED", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__READ", Double.valueOf(m.group(4)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__READ", 0.0);
                                        }

                                        done0 = true;
                                    } else {
                                        innodbRowOperationsData.ins().set("0");
                                        innodbRowOperationsData.upd().set("0");
                                        innodbRowOperationsData.del().set("0");
                                        innodbRowOperationsData.read().set("0");
                                    }

                                    m = Flags.PATTERN__ROW_OPERATIONS_1.matcher(data);
                                    if (m != null && m.find()) {                                
                                        innodbRowOperationsData.insSec().set(m.group(1));
                                        innodbRowOperationsData.updSec().set(m.group(3));
                                        innodbRowOperationsData.delSec().set(m.group(5));
                                        innodbRowOperationsData.readSec().set(m.group(7));
                                    } else {
                                        innodbRowOperationsData.insSec().set("0");
                                        innodbRowOperationsData.updSec().set("0");
                                        innodbRowOperationsData.delSec().set("0");
                                        innodbRowOperationsData.readSec().set("0");
                                    }

                                    m = Flags.PATTERN__ROW_OPERATIONS_2.matcher(data);
                                    if (m != null && m.find()) {                                
                                        rowOperationMiscData.queriesInside().set(m.group(1));
                                        rowOperationMiscData.queriesQueued().set(m.group(2));
                                    } else {
                                        rowOperationMiscData.queriesInside().set("0");
                                        rowOperationMiscData.queriesQueued().set("0");
                                    }                            

                                    m = Flags.PATTERN__ROW_OPERATIONS_3.matcher(data);
                                    if (m != null && m.find()) {                                
                                        rowOperationMiscData.rdViews().set(m.group(1));
                                    } else {
                                        rowOperationMiscData.rdViews().set("0");
                                    }

                                    m = Flags.PATTERN__ROW_OPERATIONS_4.matcher(data);
                                    if (m != null && m.find()) {                                
                                        rowOperationMiscData.mainThreadState().set(m.group(1));
                                    } else {
                                        rowOperationMiscData.mainThreadState().set("");
                                    }
                                }
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__INSERTED", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__UPDATED", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__DELETED", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "ROW_OPERATIONS__READ", 0.0);
                            }

                            done0 = false;
                            done1 = false;
                            m = Flags.PATTERN__TRANSACTIONS.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                                    m = Flags.PATTERN__TRANSACTIONS_0.matcher(data);
                                    if (m != null) {
                                        Double lockWait = 0.0;
                                        Integer count = 0;
                                        while (m.find()) {
                                            String lockWaitText = m.group(1);
                                            if (lockWaitText != null) {
                                                try {
                                                    lockWait += Double.valueOf(lockWaitText);
                                                    count++;
                                                } catch (Throwable ex) {
                                                }
                                            }
                                        }

                                        debuggerProfilerService.addEngineStatus(timesRunned, "TRANSACTIONS__LOCK_WAIT", lockWait);
                                        debuggerProfilerService.addEngineStatus(timesRunned, "TRANSACTIONS__AVG__LOCK_WAIT", count > 0 ? lockWait / count.intValue() : 0.0);

                                        done0 = true;
                                    }

                                    m = Flags.PATTERN__TRANSACTIONS_1.matcher(data);
                                    if (m != null) {
                                        Double historyLength = 0.0;
                                        if (m.find()) {
                                            String historyLengthText = m.group(1);
                                            if (historyLengthText != null) {
                                                try {
                                                    historyLength += Double.valueOf(historyLengthText);
                                                } catch (Throwable ex) {
                                                }
                                            }
                                        }

                                        debuggerProfilerService.addEngineStatus(timesRunned, "TRANSACTIONS__HISTORY_LENGTH", historyLength);
                                        done1 = true;
                                    }
                                }
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "TRANSACTIONS__LOCK_WAIT", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "TRANSACTIONS__AVG__LOCK_WAIT", 0.0);
                            }

                            if (!done1) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "TRANSACTIONS__HISTORY_LENGTH", 0.0);
                            }

                            done0 = false;
                            done1 = false;
                            m = Flags.PATTERN__FILE_IO.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                                    m = Flags.PATTERN__FILE_IO_0.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__LOG", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__LOG", 0.0);
                                        }

                                        done0 = true;
                                    }

                                    m = Flags.PATTERN__FILE_IO_1.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__READS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__READS", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__WRITES", Double.valueOf(m.group(2)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__WRITES", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__FSYNCS", Double.valueOf(m.group(3)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__FSYNCS", 0.0);
                                        }

                                        done1 = true;
                                    }
                                }
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__LOG", 0.0);
                            }

                            if (!done1) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__READS", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__WRITES", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "FILE_IO__FSYNCS", 0.0);
                            }

                            done0 = false;
                            m = Flags.PATTERN__LOG.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                                    m = Flags.PATTERN__LOG_0.matcher(data);
                                    if (m != null && m.find()) {

                                        try {

                                            Double pagesFlushed = Double.valueOf(m.group(1));

                                            debuggerProfilerService.addEngineStatus(timesRunned, "LOG__PAGES_FLUSHED", pagesFlushed);

                                            if (oldPagesFlushed != null) {

                                                Double innodbLogFileSize = getDoubleGlobalVariable("innodb_log_file_size");
                                                Double innodbBufferSize = getDoubleGlobalVariable("innodb_log_buffer_size");

                                                Double v;
                                                if (oldPagesFlushed > pagesFlushed) {
                                                    v = innodbLogFileSize - oldPagesFlushed + pagesFlushed;
                                                } else {
                                                    v = pagesFlushed - oldPagesFlushed;
                                                }

                                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__HOURLY__PAGES_FLUSHED", v);

                                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__INNODB_LOG_FILE_SIZE__TIME", v > 0 ? innodbLogFileSize / v : 0.0);
                                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__INNODB_LOG_BUFFER_SIZE__TIME", v > 0 ? innodbBufferSize / v : 0.0);
                                            } else {
                                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__HOURLY__PAGES_FLUSHED", 0.0);

                                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__INNODB_LOG_FILE_SIZE__TIME", 0.0);
                                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__INNODB_LOG_BUFFER_SIZE__TIME", 0.0);
                                            }

                                            oldPagesFlushed = pagesFlushed;
                                            done0 = true;
                                        } catch (Throwable ex) {
                                        }
                                    }
                                }
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__HOURLY__PAGES_FLUSHED", 0.0);

                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__INNODB_LOG_FILE_SIZE__TIME", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "LOG__INNODB_LOG_BUFFER_SIZE__TIME", 0.0);
                            }

                            done0 = false;
                            m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_1.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.size().set(m.group(1));
                                    } else {
                                        bufferPoolData.size().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_2.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.freeBuffs().set(m.group(1));
                                    } else {
                                        bufferPoolData.freeBuffs().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_3.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.pages().set(m.group(1));
                                    } else {
                                        bufferPoolData.pages().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_4.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.dirtyPages().set(m.group(1));
                                    } else {
                                        bufferPoolData.dirtyPages().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_5.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.hitRate().set(m.group(1));
                                    } else {
                                        bufferPoolData.hitRate().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_6.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.memory().set(m.group(1));
                                    } else {
                                        bufferPoolData.memory().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_7.matcher(data);
                                    if (m != null && m.find()) {
                                        bufferPoolData.addPool().set(m.group(1));
                                    } else {
                                        bufferPoolData.addPool().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_0.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            pagesStatisticData.reads().set(m.group(1));
                                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__READ", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__READ", 0.0);
                                        }

                                        try {
                                            pagesStatisticData.created().set(m.group(2));
                                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__CREATED", Double.valueOf(m.group(2)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__CREATED", 0.0);
                                        }

                                        try {
                                            pagesStatisticData.writes().set(m.group(3));
                                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__WRITTEN", Double.valueOf(m.group(3)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__WRITTEN", 0.0);
                                        }

                                        done0 = true;
                                    } else {
                                        pagesStatisticData.reads().set("0");
                                        pagesStatisticData.created().set("0");
                                        pagesStatisticData.writes().set("0");
                                    }

                                    m = Flags.PATTERN__BUFFER_POOL_AND_MEMORY_8.matcher(data);
                                    if (m != null && m.find()) {
                                        pagesStatisticData.readsSec().set(m.group(1));
                                        pagesStatisticData.createdSec().set(m.group(3));
                                        pagesStatisticData.writesSec().set(m.group(5));
                                    } else {
                                        pagesStatisticData.readsSec().set("0");
                                        pagesStatisticData.createdSec().set("0");
                                        pagesStatisticData.writesSec().set("0");
                                    }
                                }
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__READ", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__CREATED", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "BUFFER_POOL_AND_MEMORY__WRITTEN", 0.0);
                            }

                            done0 = false;
                            done1 = false;
                            m = Flags.PATTERN__INSERT_BUFFER.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {

                                    Platform.runLater(() -> {
                                       insertBufferAndAdaptiveHashIndexArea.setText(data); 
                                    });

        //                            m = Flags.PATTERN__INSERT_BUFFER_2.matcher(data);
        //                            if (m != null && m.find()) {
        //                                insertBuffersData.size.set(m.group(1));
        //                                insertBuffersData.freeListLen.set(m.group(2));
        //                                insertBuffersData.segSize.set(m.group(3));
        //                                insertBuffersData.merges.set(m.group(4));
        //                            }

                                    m = Flags.PATTERN__INSERT_BUFFER_0.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGES", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGES", 0.0);
                                        }

                                        done0 = true;
                                    }

                                    m = Flags.PATTERN__INSERT_BUFFER_1.matcher(data);
                                    if (m != null && m.find()) {

        //                                insertBuffersData.inserts.set(m.group(1));

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_INSERT", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_INSERT", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_DELETE_MARK", Double.valueOf(m.group(2)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_DELETE_MARK", 0.0);
                                        }

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_DELETE", Double.valueOf(m.group(3)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_DELETE", 0.0);
                                        }

                                        done1 = true;
                                    }
                                } else {
                                    Platform.runLater(() -> {
                                       insertBufferAndAdaptiveHashIndexArea.setText(""); 
                                    });
                                }
                            } else {
                                Platform.runLater(() -> {
                                    insertBufferAndAdaptiveHashIndexArea.setText(""); 
                                });
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGES", 0.0);
                            }

                            if (!done1) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_INSERT", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_DELETE_MARK", 0.0);
                                debuggerProfilerService.addEngineStatus(timesRunned, "INSERT_BUFFER__MERGED_DELETE", 0.0);
                            }

                            done0 = false;
                            done1 = false;
                            done2 = false;
                            done3 = false;
                            done4 = false;
                            done5 = false;
                            m = Flags.PATTERN__SEMAPHORES.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                                    Platform.runLater(() -> {
                                        semaphoresArea.setText(data);
                                    });

                                    m = Flags.PATTERN__SEMAPHORES_0.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_S_WAITS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_S_WAITS", 0.0);
                                        }

                                        done0 = true;
                                    }

                                    m = Flags.PATTERN__SEMAPHORES_1.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_X_WAITS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_X_WAITS", 0.0);
                                        }

                                        done1 = true;
                                    }

                                    m = Flags.PATTERN__SEMAPHORES_2.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_SX_WAITS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_SX_WAITS", 0.0);
                                        }

                                        done2 = true;
                                    }

                                    m = Flags.PATTERN__SEMAPHORES_3.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_S_ROUNDS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_S_ROUNDS", 0.0);
                                        }

                                        done3 = true;
                                    }

                                    m = Flags.PATTERN__SEMAPHORES_4.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_X_ROUNDS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_X_ROUNDS", 0.0);
                                        }

                                        done4 = true;
                                    }

                                    m = Flags.PATTERN__SEMAPHORES_5.matcher(data);
                                    if (m != null && m.find()) {

                                        try {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_SX_ROUNDS", Double.valueOf(m.group(1)));
                                        } catch (Throwable ex) {
                                            debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_SX_ROUNDS", 0.0);
                                        }

                                        done5 = true;
                                    }
                                } else {
                                    Platform.runLater(() -> {
                                        semaphoresArea.setText("");
                                    });
                                }
                            } else {
                                Platform.runLater(() -> {
                                    semaphoresArea.setText("");
                                });
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_S_WAITS", 0.0);
                            }

                            if (!done1) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_X_WAITS", 0.0);
                            }

                            if (!done2) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_SX_WAITS", 0.0);
                            }

                            if (!done3) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_S_ROUNDS", 0.0);
                            }

                            if (!done4) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_X_ROUNDS", 0.0);
                            }

                            if (!done5) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "SEMAPHORES__RW_LOCKS_SX_ROUNDS", 0.0);
                            }

                            done0 = false;
                            m = Flags.PATTERN__DEADLOCKS.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                                    Double count = 0.0;
                                    m = Flags.PATTERN__DEADLOCKS_0.matcher(data);
                                    while (m != null && m.find()) {
                                        count++;
                                    }

                                    try {
                                        debuggerProfilerService.addEngineStatus(timesRunned, "DEADLOCKS__DEADLOCKS", count);
                                    } catch (Throwable ex) {
                                        debuggerProfilerService.addEngineStatus(timesRunned, "DEADLOCKS__DEADLOCKS", 0.0);
                                    }

                                    done0 = true;
                                }
                            }

                            if (!done0) {
                                debuggerProfilerService.addEngineStatus(timesRunned, "DEADLOCKS__DEADLOCKS", 0.0);
                            }
                            
                            m = PATTERN__FOREIGN_KEY_ERROR.matcher(text);
                            if (m != null && m.find()) {
                                String data = m.group(1);
                                if (data != null) {
                            
                                    String[] lines = data.split("\\n");
                                    if (lines.length > 0) {
                                        String[] line = lines[0].split("\\s+");
                                        if (line.length > 2) {
                                            
                                            try {
                                                Date date = null;
                                                String timeStr = line[0] + " " + line[1];
                                                Matcher timeMatcher = PATTERN__TIME0.matcher(timeStr);
                                                if (timeMatcher.find()) {
                                                    date = dateFormat0.parse(timeStr);
                                                } else {
                                                    timeMatcher = PATTERN__TIME1.matcher(timeStr);
                                                    if (timeMatcher.find()) {
                                                        date = dateFormat1.parse(timeStr);
                                                    }
                                                }
                                                
                                                Date dateStatic = new Date(date.getTime() + serverTimeDiff);

                                                long diff = new Date().getTime() - date.getTime();
                                                if (diff < 15 * 60 * 1000) {
                                                    Platform.runLater(() -> {
                                                        // foreignKeyErrorsButton.setStyle("-fx-text-fill: red"); 
                                                        if (preferenceService.getPreference().isDebuggerAlertFK()) {
                                                            foreignKeyErrorsButton.setId("debugerButton_red");
                                                            addBlink(foreignKeyErrorsButton);
                                                        } else {
                                                            foreignKeyErrorsButton.setId("debugerButton_default");
                                                            removeBlink(foreignKeyErrorsButton);
                                                        }

                                                        foreignKeyErrorsText.setText("There is FK error at \"" + dateFormat0.format(dateStatic) + "\"");
                                                    });
                                                } else if (diff < 60 * 60 * 1000) {
                                                    Platform.runLater(() -> {
                                                        // foreignKeyErrorsButton.setStyle("-fx-text-fill: #c44444"); 
                                                        if (preferenceService.getPreference().isDebuggerAlertFK()) {
                                                            foreignKeyErrorsButton.setId("debugerButton_light_red");
                                                            addBlink(foreignKeyErrorsButton);
                                                        } else {
                                                            foreignKeyErrorsButton.setId("debugerButton_default");
                                                            removeBlink(foreignKeyErrorsButton);
                                                        }

                                                        foreignKeyErrorsText.setText("There is FK error at \"" + dateFormat0.format(dateStatic) + "\"");
                                                    });
                                                } else {
                                                    Platform.runLater(() -> {
                                                        // foreignKeyErrorsButton.setStyle(""); 
                                                        foreignKeyErrorsButton.setId("debugerButton_default");
                                                        removeBlink(foreignKeyErrorsButton);

                                                        foreignKeyErrorsText.setText("There is FK error at \"" + dateFormat0.format(dateStatic) + "\"");
                                                    });
                                                }
                                                
                                            } catch (ParseException ex) {
                                                log.error("Error", ex);
                                            }                                            
                                        }
                                    }
                                    
                                    Platform.runLater(() -> {
                                        foreignKeyErrorsArea.setText(data);
                                    });
                                } else {
                                    
                                    Platform.runLater(() -> {
                                        foreignKeyErrorsArea.setText("");                                       
                                        if (preferenceService.getPreference().isDebuggerAlertFK()) {
                                            foreignKeyErrorsButton.setId("debugerButton_green");
                                        } else {
                                            foreignKeyErrorsButton.setId("debugerButton_default");
                                        }
                                        
                                        removeBlink(foreignKeyErrorsButton);

                                        foreignKeyErrorsText.setText("There is no FK errors");
                                    });
                                }
                            } else {
                                Platform.runLater(() -> {
                                    foreignKeyErrorsArea.setText("");
                                    if (preferenceService.getPreference().isDebuggerAlertFK()) {
                                        foreignKeyErrorsButton.setId("debugerButton_green");
                                    } else {
                                        foreignKeyErrorsButton.setId("debugerButton_default");
                                    }
                                    
                                    removeBlink(foreignKeyErrorsButton);

                                    foreignKeyErrorsText.setText("There is no FK errors");
                                });
                            }
                        }
                        rs.close();

                        Platform.runLater(() -> {
                            openTablesTable.getItems().clear();
                        });

                        rs = (ResultSet)qr.getResult().get(4);                
                        while (rs.next()) {
                            String database = rs.getString(1);
                            String table = rs.getString(2);
                            Integer inUse = rs.getInt(3);
                            Integer nameLocked = rs.getInt(4);

                            if (inUse > 0 || nameLocked > 0) {
                                OpenTablesData ot = new OpenTablesData();
                                ot.database().set(database);
                                ot.table().set(table);
                                ot.inUse().set(inUse);
                                ot.nameLocked().set(nameLocked);

                                Platform.runLater(() -> {
                                    openTablesTable.getItems().add(ot);
                                });
                            }
                        }
                        rs.close();
                        
                        Long openTables;
                        rs = (ResultSet)qr.getResult().get(5);
                        if (rs.next()) {
                            openTables = rs.getLong(2);
                        } else {
                            openTables = 0l;
                        }
                        rs.close();
                        
                        Long openTablesCache;
                        rs = (ResultSet)qr.getResult().get(6);
                        if (rs.next()) {
                            openTablesCache = rs.getLong(1);
                        } else {
                            openTablesCache = 0l;
                        }
                        rs.close();
                        
                        String labelId = "debugerButton_default";
                        if (preferenceService.getPreference().isDebuggerAlertOT()) {
                            if (openTablesCache > 0) {
                                Long value = openTables * 100 / openTablesCache;
                                if (value > 70) {
                                    labelId = "debugerButton_red";                                
                                } else if (value >= 50) {
                                    labelId = "debugerButton_light_red";
                                } else {
                                    labelId = "debugerButton_green";
                                }
                            }
                        }
                        
                        String staticLabelId = labelId;
                        
                        Platform.runLater(() ->  {
                            openTablesButton.setId(staticLabelId);
                            
                            if ("debugerButton_red".equals(staticLabelId) || "debugerButton_light_red".equals(staticLabelId)) {
                                addBlink(openTablesButton);
                            } else {
                                removeBlink(openTablesButton);
                            }
                                                        
                            otText_openTables.setText(String.valueOf(openTables));
                            otText_tableOpenCache.setText(String.valueOf(openTablesCache));
                        });
                        
                        if (!reportRunned) {
                            
                            Double[] uptimeValue = GLOBAL_STATUS.get("Uptime");
                            boolean canBeRed = false;
                            if (uptimeValue != null && uptimeValue[0] / (60 * 60 * 24) > 1.0) {
                                canBeRed = true;
                            }
                            
                            reportRunned = true;
                            
                            rs = (ResultSet)qr.getResult().get(7);
                            int unusedIndexesCount = 0;
                            while (rs.next()) {
                                unusedIndexesCount++;
                            }
                            rs.close();

                            int unusedIndexesCountStatic = unusedIndexesCount;
                            boolean canBeRedStatic = canBeRed;
                            Platform.runLater(() ->  {
                                unusedIndexesText.setText(String.valueOf(unusedIndexesCountStatic));
                                
                                if (preferenceService.getPreference().isDebuggerAlertUI()) {
                                    if (canBeRedStatic && unusedIndexesCountStatic > 25) {
                                        // unusedIndexesButton.setStyle("-fx-text-fill: red");
                                        unusedIndexesButton.setId("debugerButton_red");
                                        addBlink(unusedIndexesButton);
                                    } else if (canBeRedStatic && unusedIndexesCountStatic >= 10) {
                                        // unusedIndexesButton.setStyle("-fx-text-fill: #c44444");
                                        unusedIndexesButton.setId("debugerButton_light_red");
                                        addBlink(unusedIndexesButton);
                                    } else {
                                        // unusedIndexesButton.setStyle("-fx-text-fill: green");
                                        unusedIndexesButton.setId("debugerButton_green");
                                        removeBlink(unusedIndexesButton);                                    
                                    }                                
                                } else {
                                    unusedIndexesButton.setId("debugerButton_default");
                                    removeBlink(unusedIndexesButton);    
                                }
                            });

                            rs = (ResultSet)qr.getResult().get(8);
                            int duplicateIndexesCount = 0;
                            while (rs.next()) {
                                duplicateIndexesCount++;
                            }
                            rs.close();

                            int duplicateIndexesCountStatic = duplicateIndexesCount;
                            Platform.runLater(() ->  {
                                duplicateIndexesText.setText(String.valueOf(duplicateIndexesCountStatic));
                                
                                if (preferenceService.getPreference().isDebuggerAlertDI()) {
                                    if (duplicateIndexesCountStatic > 10) {
                                        // duplicateIndexesButton.setStyle("-fx-text-fill: red");
                                        duplicateIndexesButton.setId("debugerButton_red");
                                        addBlink(duplicateIndexesButton);
                                    } else if (duplicateIndexesCountStatic >= 1) {
                                        // duplicateIndexesButton.setStyle("-fx-text-fill: #c44444");
                                        duplicateIndexesButton.setId("debugerButton_light_red");
                                        addBlink(duplicateIndexesButton);
                                    } else {
                                        // duplicateIndexesButton.setStyle("-fx-text-fill: green");
                                        duplicateIndexesButton.setId("debugerButton_green");
                                        removeBlink(duplicateIndexesButton);
                                    }
                                } else {
                                    duplicateIndexesButton.setId("debugerButton_default");
                                    removeBlink(duplicateIndexesButton);
                                }
                            });                        
                            
                            rs = (ResultSet)qr.getResult().get(9);
                            Double dbSize = 0.0;
                            Double dataSize = 0.0;
                            Double indexSize = 0.0;
                            while (rs.next()) {
                                dbSize += rs.getDouble(2);
                                dataSize += rs.getDouble(3);
                                indexSize += rs.getDouble(4);
                            }
                            rs.close();

                            // Check for DB size
                            ConnectionParams cp = getController().getSelectedConnectionSession().get().getConnectionParam();
                            if (cp != null) {
                                if (cp.getOsMetricDiskSize() != null && !cp.getOsMetricDiskSize().trim().isEmpty()) {
                                    try {
                                        Double diskSize = new BigDecimal(cp.getOsMetricDiskSize().trim()).multiply(new BigDecimal(1024 * 1024 * 1024)).doubleValue();
                                        checkDbSizeColor(diskSize, dbSize, dataSize, indexSize);
                                    } catch (Throwable ex) {
                                        log.error("Error", ex);
                                        checkDbSizeColor(0.0, dbSize, dataSize, indexSize);
                                    }
                                } else if (cp.isRemote() && cp.hasOSDetails()) {

                                    Double staticDbSize = dbSize;
                                    Double staticDataSize = dataSize;
                                    Double staticIndexSize = indexSize;

                                    try {
                                        // before remote metrics executed - we showing others data
                                        checkDbSizeColor(0.0, dbSize, dataSize, indexSize);
                                        
                                        RemoteMetricExtractor remoteMetricExtractor = new RemoteMetricExtractor(cp, MetricTypes.DISK_IO_METRIC);
                                        remoteMetricExtractor.setMetricListener(new MetricListener() {
                                            @Override
                                            public void onReceiveRemoteMetrics(MetricTypes mt, MetricExtract extract) {
                                                Double diskSize = new BigDecimal(((DiskIOMetricExtract) extract).getTotalSize()).doubleValue();
                                                checkDbSizeColor(diskSize, staticDbSize, staticDataSize, staticIndexSize);
                                                remoteMetricExtractor.stop();
                                            }

                                            @Override
                                            public void onDone() {}
                                        });
                                        remoteMetricExtractor.connectAndExecuteServer();
                                    } catch (Throwable th) {
                                        log.warn("Error", th);
                                        checkDbSizeColor(0.0, dbSize, dataSize, indexSize);
                                    }
                                } else {
                                    checkDbSizeColor(0.0, dbSize, dataSize, indexSize);
                                }
                            }
                        }
                        
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }      
                }
            }
        }
    }

    private void checkDbSizeColor(Double diskSize, Double dbSize, Double dataSize, Double indexSize) {
        Platform.runLater(() -> {
            
            if (preferenceService.getPreference().isDebuggerAlertDBSize()) {
                if (diskSize > 0) {
                    double percent = dataSize * 100.0 / diskSize;

                    if (percent > 90) {
                        dbSizeButton.setId("debugerButton_red");
                        addBlink(dbSizeButton);
                    } else if (percent > 80) {
                        dbSizeButton.setId("debugerButton_light_red");
                        addBlink(dbSizeButton);
                    } else if (percent > 50) {
                        dbSizeButton.setId("debugerButton_green");
                        removeBlink(dbSizeButton);
                    } else {
                        dbSizeButton.setId("debugerButton_default");
                        removeBlink(dbSizeButton);
                    }
                } else {
                    dbSizeButton.setId("debugerButton_red");
                    removeBlink(dbSizeButton);
                }
            } else {
                dbSizeButton.setId("debugerButton_default");
                removeBlink(dbSizeButton);
            }
            
            dbsText_total.setText(convertToGB(dbSize, true, "0 MB"));            
            dbsText_data.setText(convertToGB(dataSize, true, "0 MB"));            
            dbsText_index.setText(convertToGB(indexSize, true, "0 MB"));            
            dbsText_disk.setText(convertToGB(diskSize, true, "NA"));
        });
    }
    
    private void innoTopCodeBranch(long timesRunned) {
        if (itForceRun || !itPaused && timeAllowanceOnCodeBranches(timesRunned - itLast, itWaitInSec)) {
            
            itLast = timesRunned;

            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {

                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController && (itForceRun || bottomTabPane.getSelectionModel().getSelectedItem() == transactionsTab)) {
                    itForceRun = false;
                    
                    log.info("Debuger: innoTopCodeBranch");
                    String query = QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL,
                            QueryProvider.QUERY__SHOW_ENGINE_INNODB_STATUS
                    );

                    ObservableList<DebugInnoTopData> list = FXCollections.observableArrayList();
                    try (ResultSet rs = (ResultSet) service.execute(query)) {

                        if (rs != null) {
                            while (rs.next()) {
                                String text = rs.getString("Status");

                                int i = text.indexOf("---TRANSACTION");
                                while (i > 0) {
                                    text = text.substring(i + 14);

                                    String[] strs = text.split("\\n");

                                    int length = strs.length;

                                    // 0 - Id & Status & Time
                                    String status = DebuggerInnoTopTimeTableCell.INNO_TOP__NOT_STARTED;
                                    String time = "00:00:00";
                                    Long id = 0l;
                                    Long lStruct = 0l;
                                    Long undoLog = 0l;
                                    String user = "";
                                    String state = "";
                                    String qtext = "";

                                    if (length > 3) {
                                        Matcher m = PATTERN__INNO_TOP__ID.matcher(strs[2]);
                                        if (m.find()) {
                                            try {
                                                id = Long.valueOf(m.group(1));
                                            } catch (Throwable th) {
                                            }
                                        } else {
                                            m = PATTERN__INNO_TOP__ID.matcher(strs[3]);
                                            if (m.find()) {
                                                try {
                                                    id = Long.valueOf(m.group(1));
                                                } catch (Throwable th) {
                                                }
                                            }
                                        }
                                    }

                                    if (id > 0) {
                                        Matcher m = PATTERN__INNO_TOP__STATUS.matcher(strs[0]);
                                        if (m.find()) {
                                            status = "ACTIVE";
                                            try {
                                                Long secs = Long.valueOf(m.group(1));
                                                Long hours = secs / 3600;
                                                Long mins = secs / 60;
                                                secs = secs - hours * 3600 - mins * 60;

                                                time = String.format("%02d:%02d:%02d", hours, mins, secs);
                                            } catch (Throwable th) {
                                            }
                                        }

                                        if (length > 2) {
                                            // 2 - Lock Struct & undo log
                                            m = PATTERN__INNO_TOP__LOCK_STRUCT.matcher(strs[1]);
                                            if (m.find()) {
                                                try {
                                                    lStruct = Long.valueOf(m.group(1));
                                                } catch (Throwable th) {
                                                }
                                            } else {
                                                m = PATTERN__INNO_TOP__LOCK_STRUCT.matcher(strs[2]);
                                                if (m.find()) {
                                                    try {
                                                        lStruct = Long.valueOf(m.group(1));
                                                    } catch (Throwable th) {
                                                    }
                                                }
                                            }

                                            m = PATTERN__INNO_TOP__UNDO_LOG.matcher(strs[1]);
                                            if (m.find()) {
                                                try {
                                                    undoLog = Long.valueOf(m.group(1));
                                                } catch (Throwable th) {
                                                }
                                            } else {
                                                m = PATTERN__INNO_TOP__UNDO_LOG.matcher(strs[2]);
                                                if (m.find()) {
                                                    try {
                                                        undoLog = Long.valueOf(m.group(1));
                                                    } catch (Throwable th) {
                                                    }
                                                }
                                            }

                                            if (length > 3 && !strs[3].startsWith("---")) {
                                                // 3 - User & State                                            
                                                m = PATTERN__INNO_TOP__USER_STATE.matcher(strs[3]);
                                                int index = 3;
                                                if (m.find()) {
                                                    user = m.group(2);
                                                    state = m.group(3);
                                                    index = 4;
                                                } else {
                                                    m = PATTERN__INNO_TOP__STATE.matcher(strs[3]);
                                                    if (m.find()) {
                                                        state = m.group(1);
                                                        index = 4;
                                                    }
                                                }

                                                if (length > index) {
                                                    // 4+ query
                                                    for (int j = index; j < length; j++) {
                                                        if (strs[j].startsWith("Trx read view will") || strs[j].startsWith("---")) {
                                                            break;
                                                        } else {
                                                            qtext += strs[j];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ("ACTIVE".equals(status) && qtext.isEmpty()) {
                                        status = "UNDO";
                                    }

                                    list.add(new DebugInnoTopData(id, user, status, time, undoLog, lStruct, state, qtext, ""));

                                    i = text.indexOf("---TRANSACTION");
                                }
                            }
                        }
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }

                    Platform.runLater(() -> {

                        TableColumn idColumn = innoTopTable.getColumnByName("Thread ID");

                        innoTopTable.setItems(list);

                        idColumn.setVisible(false);
                        idColumn.setVisible(true);
                                                
                        boolean hasRed = false;
                        Map<String, Integer> statusCounts = new HashMap<>();
                        for (DebugInnoTopData item: list) {
                            String color = item.getColor();
                            if (color == DebugInnoTopData.COLOR_RED || color == DebugInnoTopData.COLOR_RED_LIGHT) {
                                hasRed = true;
                            }
                            
                            Integer count = statusCounts.get(item.getStatus());
                            if (count == null) {
                                count = 0;
                            }
                            
                            statusCounts.put(item.getStatus(), count + 1);
                        }
                        
                        Integer undo = statusCounts.get(DebugInnoTopData.INNO_TOP__UNDO);
                        trText_undo.setText(String.valueOf(undo != null ? undo : 0));
                        
                        Integer active = statusCounts.get(DebugInnoTopData.INNO_TOP__ACTIVE);
                        trText_active.setText(String.valueOf(active != null ? active : 0));
                        
                        Integer notStarted = statusCounts.get(DebugInnoTopData.INNO_TOP__NOT_STARTED);
                        trText_notStarted.setText(String.valueOf(notStarted != null ? notStarted : 0));
                        
                        if (preferenceService.getPreference().isDebuggerAlertTR()) {
                            if (hasRed) {
                                // transactionsButton.setStyle("-fx-text-fill: red;");
                                transactionsButton.setId("debugerButton_red");
                                addBlink(transactionsButton);
                                trRed = true;
                                trGreen = false;
                            } else {
                                // transactionsButton.setStyle("-fx-text-fill: green;");
                                transactionsButton.setId("debugerButton_green");
                                removeBlink(transactionsButton);
                                trRed = false;
                                trGreen = true;
                            }                        
                        } else {
                            transactionsButton.setId("debugerButton_default");
                            removeBlink(transactionsButton);
                            trRed = false;
                            trGreen = false;
                        }
                    });
                }
            }
        }
    }

    private XYChart.Data<Number, Number> parseDeadlocks(String text, Date datetime) {

        Double count = 0.0;

        Matcher m = Flags.PATTERN__DEADLOCKS.matcher(text);
        if (m != null && m.find()) {
            String data = m.group(1);
            if (data != null) {
                m = Flags.PATTERN__DEADLOCKS_1.matcher(data);
                while (m != null && m.find()) {
                    count++;
                }
            }
        }

        return new XYChart.Data<>(datetime.getTime(), count);
    }

    private List<String> usedDeadlocks = new ArrayList<>();
    
    private void deadlockMonitorCodeBranch(long timesRunned) {
        if (dmForceRun || !dmPaused && timeAllowanceOnCodeBranches(timesRunned - dmLast, dmWaitInSec)) {
            dmLast = timesRunned;
                    
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {
                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController && (dmForceRun || bottomTabPane.getSelectionModel().getSelectedItem() == deadlockMonitorTab)) {
                    
                    dmForceRun = false;
                    
                    List<DeadlockMonitorQueryData> list = new ArrayList<>();
                    List<DeadlockMonitorQueryData> firstData = new ArrayList<>();
                    if (dmFirstRun) {
                        dmFirstRun = false;
                        
                        DeadlockData prev = null;
                        int days = 7;
                        try {
                            days = Integer.valueOf(preferenceService.getPreference().getDebuggerDeadlockHistory());
                        } catch (Throwable th) {}
                                
                        for (DeadlockData d: deadlockService.getAll(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), days)) {
                            firstData.add(new DeadlockMonitorQueryData(d));
                            
                            try {
                                XYChart.Data<Number, Number> data = parseDeadlocks(d.getDetails(), dateFormat0.parse(d.getDeadlockTime()));
                                
                                if (prev != null) {
                                    try {
                                        Long d0 = dateFormat0.parse(d.getDeadlockTime()).getTime();
                                        Long d1 = dateFormat0.parse(prev.getDeadlockTime()).getTime();
                                        if (d0 - d1 > 10 * 60 * 1000) {
                                            Platform.runLater(() -> {
                                                try {
                                                    deadlockSeries.getData().add(new XYChart.Data<>(d1 + 1000, 0));
                                                    deadlockSeries.getData().add(new XYChart.Data<>(d0 - 1000, 0));
                                                } catch (Throwable th) {
                                                    log.error("Error", th);
                                                }
                                            });
                                        }
                                    } catch (Throwable th) {
                                        log.error("Need to fix, its for debug", th);
                                    }
                                }
                                
                                prev = d;
                                
                                Platform.runLater(() -> {
                                    if (!usedDeadlocks.contains(d.getDeadlockTime())) {
                                        deadlockSeries.getData().add(data);
                                        usedDeadlocks.add(d.getDeadlockTime());
                                    }
                                });
                            } catch (ParseException ex) {
                                log.error("Error", ex);
                            }                    
                        }
                    }
                    
                    try (ResultSet rs = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DEBUG_DEADLOCK_MONITOR))) {

                        String currentDate = null;
                        if (rs != null) {
                            while (rs.next()) {
                                currentDate = readDeadLock(rs.getString("Status"), list);
                            }
                        }
                        
                        Long currentTime = new Date().getTime();
                        if (currentDate != null) {
                            try {
                                currentTime = dateFormat0.parse(currentDate).getTime();
                            } catch (Throwable th) {
                                log.error("Need to fix, its for debug", th);
                            }
                        }

                        Long staticTime = currentTime;
                        if (list.isEmpty()) {
                            Platform.runLater(() -> {
                                XYChart.Data<Number, Number> data = new XYChart.Data<>(staticTime, 0);
                                deadlockSeries.getData().add(data);
                            });
                        } else {
                            
                            // save deadlock to db
                            for (DeadlockMonitorQueryData d: list) {
                                deadlockService.save(d.toDeadlockData(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey()));                        

                                XYChart.Data<Number, Number> prev = deadlockSeries.getData().isEmpty() ? null : (XYChart.Data<Number, Number>)deadlockSeries.getData().get(deadlockSeries.getData().size() - 1);
                                
                                try {
                                    XYChart.Data<Number, Number> data = parseDeadlocks(d.getDetails(), dateFormat0.parse(d.getDeadlockTime()));

                                    if (!usedDeadlocks.contains(d.getDeadlockTime())) {
                                        if (prev != null) {
                                            try {
                                                Long d0 = dateFormat0.parse(d.getDeadlockTime()).getTime();
                                                if (d0 - prev.getXValue().longValue() > 10 * 60 * 1000) {
                                                    Platform.runLater(() -> {
                                                        try {
                                                            deadlockSeries.getData().add(new XYChart.Data<>(prev.getXValue().longValue() + 1000, 0));
                                                            deadlockSeries.getData().add(new XYChart.Data<>(d0 - 1000, 0));
                                                        } catch (Throwable th) {
                                                            log.error("Error", th);
                                                        }
                                                    });
                                                }
                                            } catch (Throwable th) {
                                                log.error("Need to fix, its for debug", th);
                                            }
                                        }

                                        deadlockSeries.getData().add(data);
                                        usedDeadlocks.add(d.getDeadlockTime());
                                    }else {
                                        try {
                                            if (prev != null) {
                                                if (staticTime - prev.getXValue().longValue() > 10 * 60 * 1000) {
                                                    Platform.runLater(() -> {
                                                        try {
                                                            deadlockSeries.getData().add(new XYChart.Data<>(prev.getXValue().longValue() + 1000, 0));
                                                            deadlockSeries.getData().add(new XYChart.Data<>(staticTime - 1000, 0));
                                                        } catch (Throwable th) {
                                                            log.error("Error", th);
                                                        }
                                                    });
                                                }
                                            }
                                        } catch (Throwable th) {
                                            log.error("Need to fix, its for debug", th);
                                        }
                                        
                                        data = new XYChart.Data<>(staticTime, 0);
                                        deadlockSeries.getData().add(data);
                                    }       
                       
                                } catch (Throwable th) {
                                    log.error("Need to fix, its for debug", th);
                                }
                            }
                        }
                        
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                    
                    if (!firstData.isEmpty()) {
                        list.addAll(0, firstData);
                    }
                    
                    if (!deadlockSeries.getData().isEmpty()) {

                        try {
                            Long lower = ((XYChart.Data<Number, Number>)deadlockSeries.getData().get(0)).getXValue().longValue();
                            Long upper = ((XYChart.Data<Number, Number>)deadlockSeries.getData().get(deadlockSeries.getData().size() - 1)).getXValue().longValue();
                            
//                            if (!firstData.isEmpty()) {
                            deadlockChartPane.initXAxis(upper + 30 * 60 * 100, lower);
//                            }

                            deadlockChartPane.updateChart(true, null, lower, null, null);
                        } catch (Throwable th) {
                            log.error("Error", th);
                        }
                    }
                    
                    Platform.runLater(() -> {
                        
                        List<DeadlockMonitorQueryData> forRemove = new ArrayList<>();
                        if (deadlockMonitorTable.getItems() != null) {
                            for (DeadlockMonitorQueryData d: (List<DeadlockMonitorQueryData>)(List)deadlockMonitorTable.getItems()) {

                                for (DeadlockMonitorQueryData d0: list) {
                                    if (d.getDeadlockTime().equals(d0.getDeadlockTime()) && d.getTransaction1().equals(d0.getTransaction1()) && d.getTransaction2().equals(d0.getTransaction2())) {
                                        forRemove.add(d);
                                        deadlockService.delete(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), d.getDeadlockTime(), d.getTransaction1(), d.getTransaction2());
                                    }
                                }
                            }
                            
                            deadlockMonitorTable.getItems().removeAll(forRemove);
                        }
                        
                        if (deadlockMonitorTable.getItems() != null) {
                            deadlockMonitorTable.getItems().addAll(list);
                            deadlockMonitorTable.dataChanged();
                        } else {
                            deadlockMonitorTable.setItems(FXCollections.observableArrayList(list));
                        }
                        
                        int value = 5;
                        try {
                            value = Integer.valueOf(preferenceService.getPreference().getDebuggerDeadlockAlert());
                        } catch (Throwable th) {}
                        
                        long green = 4 * 60 * 60 * 1000;
                        long lightRed = 60 * 60 * 1000;
                        long red = value * 60 * 1000;
                        
                        boolean hasRed = false;
                        boolean hasLightRed = false;
                        boolean hasGreen = true;
                        
                        long current = new Date().getTime();
                        long maxDate = 0l;
                        
                        for (XYChart.Data<Number, Number> data: deadlockSeries.getData()) {
                            
                            long v = data.getXValue().longValue();
                            
                            if (maxDate < v) {
                                maxDate = v;
                            }
                            
                            if (!hasRed) {
                                if (v > current - green && data.getYValue().longValue() > 0) {
                                    hasGreen = false;
                                }

                                if (v > current - lightRed && data.getYValue().longValue() > 0) {
                                    hasLightRed = true;
                                }

                                if (v > current - red && data.getYValue().longValue() > 0) {
                                    hasRed = true;
                                }
                            }
                        }
                        
                        
                        if (preferenceService.getPreference().isDebuggerAlertDL()) {
                            if (hasRed) {                            
                                deadLocksButton.setId("debugerButton_red");
                                addBlink(deadLocksButton);
                            } else if (hasLightRed){
                                deadLocksButton.setId("debugerButton_light_red");
                                addBlink(deadLocksButton);
                            } else if (hasGreen){
                                deadLocksButton.setId("debugerButton_green");
                                removeBlink(deadLocksButton);
                            } else {
                                deadLocksButton.setId("debugerButton_default");
                                removeBlink(deadLocksButton);
                            }    
                        } else {
                            deadLocksButton.setId("debugerButton_default");
                            removeBlink(deadLocksButton);
                        }
                        
                        if (list.isEmpty() && !hasLightRed && !hasRed) {
                            dlText.setText("No Dead lock found");
                        } else {
                            if (maxDate > 0) {
                                dlText.setText("Last DL at: " + dateFormat0.format(new Date(maxDate)));
                            } else {
                                dlText.setText("");
                            }
                        }
                    });

                    if (list.isEmpty() && !dmShowed) {

                        Calendar c = GregorianCalendar.getInstance();
                        c.setTime(new Date());

                        c.add(Calendar.HOUR_OF_DAY, -1);

                        int count = 0;
                        for (DeadlockMonitorQueryData d : list) {
                            try {
                                Date date = DEADLOCK_DATE_FORMATER.parse(d.getDeadlockTime());
                                if (c.getTime().before(date)) {
                                    count++;
                                }
                            } catch (Throwable ex) {
                                log.error("Error", ex);
                            }
                        }

                        if (count == 0) {
                            dmShowed = true;
//                            Platform.runLater(() -> showInfo("Dead locks", "The server doesn't has issue with \"DEAD LOCK\"", null, getStage()));
                        }
                    }
                }
            }
        }
    }
    
    
    private void sqlSpeedometerBranch(long timesRunned) {  
        
        if (speedForceRun || timeAllowanceOnCodeBranches(timesRunned - speedometerLast, speedometerWaitInSec)) {
            speedometerLast = timesRunned;
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {
                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController && (speedForceRun || bottomTabPane.getSelectionModel().getSelectedItem() == dbLoadTab)) {
                
                    speedForceRun = false;
                    //log.info("Debuger: sqlSpeedometerBranch");
                    String query = QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL,
                            QueryProvider.QUERY__SHOW_GLOBAL_STATUS
                    );
                    
                    if (!query.trim().endsWith(";")) {
                        query += ";";
                    }
                    
                    query += "Show global status like 'Com%';";

                    Long totalQueries = null;
//                    Long queries = null;
                    Long select = null;
                    Long insert = null;
                    Long update = null;
                    Long delete = null;
                    Long commit = null;
                    Long begin = null;
                    Long replace = null;
                    Double bytesReceived = null;
                    Double bytesSent = null;

                    QueryResult qr = new QueryResult();
                    
                    try {
                        
                        service.execute(query, qr);
                        
                        ResultSet rs = (ResultSet) qr.getResult().get(0);
                        if (rs != null) {
                            while (rs.next()) {
                                switch (rs.getString(1)) {
                                    case "Com_insert":
                                        insert = rs.getLong(2);
                                        break;

                                    case "Com_select":
                                        select = rs.getLong(2);
                                        break;

                                    case "Com_update":
                                        update = rs.getLong(2);
                                        break;

                                    case "Com_delete":
                                        delete = rs.getLong(2);
                                        break;

                                    case "Com_commit":
                                        commit = rs.getLong(2);
                                        break;

                                    case "Com_begin":
                                        begin = rs.getLong(2);
                                        break;

                                    case "Queries":
                                        totalQueries = rs.getLong(2);
                                        break;
                                        
                                    case "Com_replace":
                                        replace = rs.getLong(2);
                                        break;
                                        
                                    case "Bytes_received":
                                        bytesReceived = rs.getDouble(2);
                                        break;
                                        
                                    case "Bytes_sent":
                                        bytesSent = rs.getDouble(2);
                                        break;

                                    default:
                                        // TODO Not sure about this, but
                                        // show GLOBAL status like '%Queries%'
//                                        if (rs.getString(1).toLowerCase().contains("queries")) {
//                                            if (queries == null) {
//                                                queries = 0l;
//                                            }
//                                            queries += rs.getLong(2);
//                                        }
                                }
                            }
                            
                            rs.close();
                            
                            
                            Map<String, SpeedometerData> old = new HashMap<>();
                            for (SpeedometerData sd: speedometerTable.getItems()) {
                                old.put(sd.name().get(), sd);
                            }
                            
                            speedometerTotalValue = 0.0;
                            speedometerTotalLastInc = 0.0;
    
                            rs = (ResultSet) qr.getResult().get(1);
                            if (rs != null) {
                                while (rs.next()) {
                                    String name = rs.getString(1);
                                    Double value = 0.0;
                                    try {
                                        value = rs.getDouble(2);
                                    } catch (Throwable th) {
                                        log.error(th.getMessage(), th);
                                    }
                                    
                                    speedometerTotalValue += value;
                                    
                                    SpeedometerData sd = old.remove(name);
                                    if (sd == null) {
                                        sd = new SpeedometerData(name, value, value);
                                        speedometerTable.getItems().add(sd);
                                        
                                        speedometerTotalLastInc += value;
                                    } else {
                                        
                                        sd.lastInc().set(value - (Double)sd.value().get());
                                        sd.value().set(value);
                                        
                                        speedometerTotalLastInc += (Double)sd.lastInc().get();
                                    }
                                }
                            }
                            
                            // clear old not used
                            speedometerTable.getItems().removeAll(old.values());
                            
                            // calc pct
                            for (SpeedometerData sd: speedometerTable.getItems()) {
                                
                                if (speedometerTotalValue > 0) {
                                    sd.valuePct().set(new BigDecimal(((Double)sd.value().get() / speedometerTotalValue) * 100).setScale(2, RoundingMode.HALF_UP).toString() + "%");
                                } else {
                                    sd.valuePct().set("0.00%");
                                }
                                
                                if (speedometerTotalLastInc > 0) {
                                    sd.lastIncPct().set(new BigDecimal(((Double)sd.lastInc().get() / speedometerTotalLastInc) * 100).setScale(2, RoundingMode.HALF_UP).toString() + "%");
                                } else {
                                    sd.lastIncPct().set("0.00%");
                                }
                            }
                            
                            Platform.runLater(() -> {
                                speedometerTable.getSortOrder().clear();
                                speedometerTable.getSortOrder().add(speedometerValueColumn); 
                            });
                        }
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }

                    if (totalQueries != null && totalQueriesOld == null) {
                        totalQueriesOld = totalQueries;
                    }
                    
                    if (replace != null && replaceOld == null) {
                        replaceOld = replace;
                    }

//                    if (queries != null && queriesOld == null) {
//                        queriesOld = queries;
//                    }

                    if (select != null && selectOld == null) {
                        selectOld = select;
                    }

                    if (insert != null && insertOld == null) {
                        insertOld = insert;
                    }

                    if (update != null && updateOld == null) {
                        updateOld = update;
                    }

                    if (delete != null && deleteOld == null) {
                        deleteOld = delete;
                    }

                    if (commit != null && commitOld == null) {
                        commitOld = commit;
                    }

                    if (begin != null && beginOld == null) {
                        beginOld = begin;
                    }

                    Long totalQueriesSec = totalQueries != null && totalQueriesOld != null ? (totalQueries - totalQueriesOld) / speedometerWaitInSec : 0l;
                    Long totalQueriesMinute = totalQueriesSec * 60;

                    Long selectSec = select != null && selectOld != null ? (select - selectOld) / speedometerWaitInSec : 0l;
                    Long selectMinute = selectSec * 60;

                    Long insertSec = insert != null && insertOld != null ? (insert - insertOld) / speedometerWaitInSec : 0l;
                    Long insertMinute = insertSec * 60;

                    Long updateSec = update != null && updateOld != null ? (update - updateOld) / speedometerWaitInSec : 0l;
                    Long updateMinute = updateSec * 60;

                    Long deleteSec = delete != null && deleteOld != null ? (delete - deleteOld) / speedometerWaitInSec : 0l;
                    Long deleteMinute = deleteSec * 60;
                    
                    Long replaceSec = replace != null && replaceOld != null ? (replace - replaceOld) / speedometerWaitInSec : 0l;
                    Long replaceMinute = replaceSec * 60;

                    Long commitSec = commit != null && commitOld != null ? (commit - commitOld) / speedometerWaitInSec : 0l;
                    Long commitMinute = commitSec * 60;

                    Long beginSec = begin != null && beginOld != null ? (begin - beginOld) / speedometerWaitInSec : 0l;
                    Long beginMinute = beginSec * 60;
                    
                    Long writeQueriesSec = insertSec + updateSec + deleteSec + replaceSec;
                    Long writeQueriesMinute = insertMinute + updateMinute + deleteMinute + replaceMinute;
                    
                    Double bytesReceivedStatic = bytesReceived;
                    Double bytesSentStatic = bytesSent;
                    
                    Platform.runLater(() -> {
                        dbText_total.setText(String.valueOf(totalQueriesSec));
                        dbText_write.setText(String.valueOf(writeQueriesSec));
                        dbText_select.setText(String.valueOf(selectSec));
                        dbText_insert.setText(String.valueOf(insertSec));
                        dbText_update.setText(String.valueOf(updateSec));
                        dbText_delete.setText(String.valueOf(deleteSec));
                        
                        dbText_BytesReceived.setText(convertToGB(bytesReceivedStatic, true, "0 MB"));
                        dbText_BytesSent.setText(convertToGB(bytesSentStatic, true, "0 MB"));
                        
                        if (preferenceService.getPreference().isDebuggerAlertDBLoad()) {
                            Double v = (bytesReceivedStatic + bytesSentStatic) / (1024 * 1024);

                            if (v < 1 && rqGreen && trGreen && qlGreen) {
                                dbLoadButton.setId("debugerButton_green");
                                removeBlink(dbLoadButton);

                            } else if (v >= 1 && v < 5 && rqGreen && trGreen && qlGreen) {
                                dbLoadButton.setId("debugerButton_default");
                                removeBlink(dbLoadButton);

                            } else if (v >= 1 && v < 5 && (rqRed || trRed || qlRed)) {
                                dbLoadButton.setId("debugerButton_light_red");
                                addBlink(dbLoadButton);

                            } else if (v >= 5 && (rqRed || trRed || qlRed)) {
                                dbLoadButton.setId("debugerButton_red");
                                addBlink(dbLoadButton);

                            } else {
                                dbLoadButton.setId("debugerButton_default");
                                removeBlink(dbLoadButton);
                            }
                        } else {
                            dbLoadButton.setId("debugerButton_default");
                            removeBlink(dbLoadButton);
                        }
                        
                    });
                    
                    
                    totalQueriesOld = totalQueries;
//                    queriesOld = queries;
                    selectOld = select;
                    insertOld = insert;
                    updateOld = update;
                    deleteOld = delete;
                    commitOld = commit;
                    beginOld = begin;
                    replaceOld = replace;

                    while (writeQueriesSec > maxWriteQueries) {
                        maxWriteQueries += MAX_DEFAULT;
                    }

                    while (totalQueriesSec > maxTotalQueries) {
                        maxTotalQueries += MAX_DEFAULT;
                    }

                    while (selectSec > maxSelect) {
                        maxSelect += MAX_DEFAULT;
                    }

                    while (insertSec > maxInsert) {
                        maxInsert += MAX_DEFAULT;
                    }

                    while (updateSec > maxUpdate) {
                        maxUpdate += MAX_DEFAULT;
                    }

                    while (deleteSec > maxDelete) {
                        maxDelete += MAX_DEFAULT;
                    }

                    while (commitSec > maxCommit) {
                        maxCommit += MAX_DEFAULT;
                    }

                    while (beginSec > maxBegin) {
                        maxBegin += MAX_DEFAULT;
                    }

                    Platform.runLater(() -> {
                        totalQueriesPerSec.setValue(totalQueriesSec / X_SEC_DEFAULT);
                        totalQueriesPerSec.setMaxValue(maxTotalQueries / X_SEC_DEFAULT);
                        totalQueriesPerMin.setText(totalQueriesSec + " per sec / " + totalQueriesMinute + " per min");

                        writeQueriesPerSec.setValue(writeQueriesSec / X_SEC_DEFAULT);
                        writeQueriesPerSec.setMaxValue(maxWriteQueries / X_SEC_DEFAULT);
                        writeQueriesPerMin.setText(writeQueriesSec + " per sec / " + writeQueriesMinute + " per min");

                        selectPerSec.setValue(selectSec / X_SEC_DEFAULT);
                        selectPerSec.setMaxValue(maxSelect / X_SEC_DEFAULT);
                        selectPerMin.setText(selectSec + " per sec / " + selectMinute + " per min");

                        insertPerSec.setValue(insertSec / X_SEC_DEFAULT);
                        insertPerSec.setMaxValue(maxInsert / X_SEC_DEFAULT);
                        insertPerMin.setText(insertSec + " per sec / " + insertMinute + " per min");

                        updatePerSec.setValue(updateSec / X_SEC_DEFAULT);
                        updatePerSec.setMaxValue(maxUpdate / X_SEC_DEFAULT);
                        updatePerMin.setText(updateSec + " per sec / " + updateMinute + " per min");

                        deletePerSec.setValue(deleteSec / X_SEC_DEFAULT);
                        deletePerSec.setMaxValue(maxDelete / X_SEC_DEFAULT);
                        deletePerMin.setText(deleteSec + " per sec / " + deleteMinute + " per min");

                        commitPerSec.setValue(commitSec / X_SEC_DEFAULT);
                        commitPerSec.setMaxValue(maxCommit / X_SEC_DEFAULT);
                        commitPerMin.setText(commitSec + " per sec / " + commitMinute + " per min");

                        beginPerSec.setValue(beginSec / X_SEC_DEFAULT);
                        beginPerSec.setMaxValue(maxBegin / X_SEC_DEFAULT);
                        beginPerMin.setText(beginSec + " per sec / " + beginMinute + " per min");
                    });
                }
            }
        }
    }
    
    
    private void lockMonitorCodeBranch(long timesRunned) {
        if (lmForceRun || !lmPaused && timeAllowanceOnCodeBranches(timesRunned - lmLast, lmWaitInSec)) {
            lmLast = timesRunned;
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {

                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController && (lmForceRun || bottomTabPane.getSelectionModel().getSelectedItem() == lockMonitorTab)) {

                    lmForceRun = false;
                    
                    log.info("Debuger: lockMonitorCodeBranch");
                    boolean all = false;

                    String query = QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL,
                            service.getDbSQLVersionMaj() == 8 ? QueryProvider.QUERY__LOCK_MONITOR_8 : QueryProvider.QUERY__LOCK_MONITOR
                    );

                    if (lmTimeGreater0Button.isSelected()) {
                        query = String.format(QueryProvider.getInstance().getQuery(
                                QueryProvider.DB__MYSQL,
                                service.getDbSQLVersionMaj() == 8 ? QueryProvider.QUERY__LOCK_MONITOR_WITH_TIME_8 : QueryProvider.QUERY__LOCK_MONITOR_WITH_TIME),
                                lmTimeCombobox.getSelectionModel().getSelectedItem(), lmTimeField.getText());

                    } else if (lmTXLockButton.isSelected()) {
                        query = QueryProvider.getInstance().getQuery(
                                QueryProvider.DB__MYSQL,
                                service.getDbSQLVersionMaj() == 8 ? QueryProvider.QUERY__LOCK_MONITOR_TX_LOCK_8 : QueryProvider.QUERY__LOCK_MONITOR_TX_LOCK
                        );
                    } else {
                        all = true;
                    }
                    
                    // Check this...
                    int txLockCount = 0;
                    try (ResultSet rs = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(
                                QueryProvider.DB__MYSQL,
                                service.getDbSQLVersionMaj() == 8 ? QueryProvider.QUERY__LOCK_MONITOR_8 : QueryProvider.QUERY__LOCK_MONITOR
                        ))) {
                        
                        if (rs != null) {
                            while(rs.next()) {
                                txLockCount++;
                            }
                        }                        
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }

                    List<DebugLockMonitorData> list = new ArrayList<>();
                    try (ResultSet rs = (ResultSet) service.execute(query)) {

                        if (rs != null) {
                                                        
                            while (rs.next()) {
                                //Random r = new Random();
                                //for (int i = 0; i < 100; i++) {
                                    
                                String state = rs.getString("WT_state");
//                                    
//                                    state = r.nextBoolean() ? "sleep" : state;
                                    
                                Double v = rs.getDouble("wait_time");
                                BigDecimal time = v != null ? new BigDecimal(v).setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO;
                                
                                    
                                DebugLockMonitorData d = new DebugLockMonitorData(
                                        rs.getLong("id"),
                                        rs.getString("user"),
                                        rs.getString("host"),
                                        rs.getString("command"),
                                        state,
                                        //(double)new Random().nextInt(10),
                                        time,
                                        rs.getString("waiting_query"),
                                        rs.getString("DB"),
                                        rs.getString("waiting_table_lock"),
                                        rs.getString("Blocking_query"),
                                        rs.getString("blocking_trx_id"),
                                        rs.getLong("blocking_thread"),
                                        rs.getString("blocking_host"),
                                        rs.getLong("idle_in_trx"),
                                        rs.getString("Lock_type"),
                                        rs.getString("Lock_type_desc")
                                );

                                list.add(d);
                            }                            
                            //}
                        }
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }

                    int txLockCountStatic = txLockCount;
                    Platform.runLater(() -> {

                        TableColumn timeColumn = lockMonitorTable.getColumnByName("Wait time");

                        List<DebugLockMonitorData> forRemove = new ArrayList<>();
                        List<DebugLockMonitorData> forAdd = new ArrayList<>();

                        List<DebugLockMonitorData> l = (List<DebugLockMonitorData>) (List) lockMonitorTable.getCopyItems();
                        for (DebugLockMonitorData d : list) {
                            int index = l.indexOf(d);
                            if (index == -1) {
                                forAdd.add(d);
                            } else {
                                l.get(index).setTime(d.getTime());
                            }
                        }

                        for (DebugLockMonitorData d : l) {
                            if (!list.contains(d)) {
                                forRemove.add(d);
                            }
                        }

                        lockMonitorTable.addAndRemoveItems(forAdd, forRemove);
                        
                        timeColumn.setVisible(false);
                        timeColumn.setVisible(true);
                        
                        boolean hasRed = false;
                        boolean hasLightRed = false;
                        boolean hasGreen = true;
                                                
                        for (DebugLockMonitorData data: (List<DebugLockMonitorData>) (List) lockMonitorTable.getSourceData()) {
                            String color = data.getColor(preferenceService.getPreference());
                            if (color == DebugTimeProvider.COLOR_GREEN) {
                                hasGreen = false;
                                
                            } else if (color == DebugTimeProvider.COLOR_RED_LIGHT || color == DebugTimeProvider.COLOR_RED_MORE_LIGHT) {
                                hasLightRed = true;
                                
                            } else if (color == DebugTimeProvider.COLOR_RED) {
                                hasRed = true;
                            }
                            
                            if (hasRed) {
                                break;
                            }
                        }
                        
                        qlText.setText(String.valueOf(txLockCountStatic));
                        
                        if (preferenceService.getPreference().isDebuggerAlertQL()) {
                        
                            if (hasRed) {                            
                                queriesLockButton.setId("debugerButton_red");
                                addBlink(queriesLockButton);
                                qlRed = true;
                                qlGreen = false;
                            } else if (hasLightRed){
                                // queriesLockButton.setStyle("-fx-text-fill: #c44444;");
                                queriesLockButton.setId("debugerButton_light_red");
                                addBlink(queriesLockButton);
                                qlRed = true;
                                qlGreen = false;
                            } else {
                                // queriesLockButton.setStyle("-fx-text-fill: green;");
                                queriesLockButton.setId("debugerButton_green");
                                removeBlink(queriesLockButton);
                                qlRed = false;
                                qlGreen = true;
                            }            
                        } else {
                            queriesLockButton.setId("debugerButton_default");
                            removeBlink(queriesLockButton);
                            qlRed = false;
                            qlGreen = false;
                        }
                    });

                    if (all && list.isEmpty() && !lmShowed) {
                        lmShowed = true;
//                        Platform.runLater(() -> showInfo("Lock monitor", "The server doesn't has issue with Table/Transaction Locks", null, getStage()));
                    }
                }
            }
        }
    }
    
    private void replicationLaggingCodeBranch(long timesRunned, String globalVariablesQuery, String globalStatusQuery,
            String fullProcessQuery, Map<String, ProfilerQueryData> analyzeQueries) {
        if (rlForceRun || !replPaused && timeAllowanceOnCodeBranches(timesRunned - replLast, replWaitInSec)) {
            replLast = timesRunned;
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {

                Tab t = tcc.getSelectedTab().get();
                if (t != null && t.getUserData() instanceof DebuggerTabController) {

                    rlForceRun = false;
                    
                    log.info("Debuger: replicationLaggingCodeBranch");
                    /**
                    * *************************************************
                    * SHOW FULL PROCESSLIST STUFF
                    * ************************************************
                    */
                    // date of execution qeury
                    String multiQuery = fullProcessQuery;
                    if (!multiQuery.trim().endsWith(";")) {
                        multiQuery += ";";
                    }

                    multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_SLAVE_STATUS);
                    if (!multiQuery.trim().endsWith(";")) {
                        multiQuery += ";";
                    }

                    Date currentDate = new Date();
                    try {
                        QueryResult qr = new QueryResult();
                        service.execute(multiQuery, qr);

                        ResultSet rs = (ResultSet) qr.getResult().get(0);
                        while (rs.next()) {
                            // save only with not null info
                            String info = rs.getString(PROCESSLIST__VARIABLE__INFO);
                            String command = rs.getString(PROCESSLIST__VARIABLE__COMMAND);
                            String user = rs.getString(PROCESSLIST__VARIABLE__USER);

                            // no need save root commands
                            if (info != null
                                    && !info.equalsIgnoreCase(globalVariablesQuery)
                                    && !info.equalsIgnoreCase(globalStatusQuery)
                                    && !info.equalsIgnoreCase(fullProcessQuery)
                                    && !"sleep".equalsIgnoreCase(command)
                                    /*&& !"system user".equalsIgnoreCase(user)*/) {

                                String host = rs.getString(PROCESSLIST__VARIABLE__HOST);
                                if (host.contains(":")) {
                                    host = host.substring(0, host.indexOf(":"));
                                }
                                
                                profilerService.save(
                                        new GlobalProcessList(
                                                rs.getInt(PROCESSLIST__VARIABLE__ID),
                                                user,
                                                host,
                                                rs.getString(PROCESSLIST__VARIABLE__DB),
                                                command,
                                                rs.getBigDecimal(PROCESSLIST__VARIABLE__TIME),
                                                rs.getString(PROCESSLIST__VARIABLE__STATE),
                                                info,
                                                currentDate
                                        )
                                );
                            }
                        }

                        rs.close();
                        
                        boolean needGreen = false;
                        boolean needLightGreen = false;
                        boolean needRed = false;
                        
                        String colorId = null;
                        String secondsBehindMasterStr = null;
                        rs = (ResultSet) qr.getResult().get(1);
                        synchronized (DebuggerTabController.this) {
                            if (rs != null && rs.next()) {
                                Long secondsBehindMaster = rs.getLong("Seconds_Behind_Master");
                                Long execMasterLogPos = rs.getLong("Exec_Master_Log_Pos");
                                Long readMasterLogPos = rs.getLong("Read_Master_Log_Pos");
                                Long relayLogSpace = rs.getLong("Relay_Log_Space");
                                
                                secondsBehindMasterStr = String.valueOf(secondsBehindMaster);

                                salveStatusData.clear();
                                
                                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                    salveStatusData.add(new SalveStatusData(rs.getMetaData().getColumnName(i), rs.getString(i)));
                                }

                                if (secondsBehindMaster < 1) {
                                    sbmLabel.setStyle("-fx-text-fill: green;");
                                    needGreen = true;
                                    
                                } else if (secondsBehindMaster <= 10) {
                                    sbmLabel.setStyle("-fx-text-fill: blue;");
                                    needLightGreen = true;
                                    
                                } else if (secondsBehindMaster <= 100) {
                                    sbmLabel.setStyle("-fx-text-fill: orange;");
                                    needRed = true;
                                            
                                } else {
                                    sbmLabel.setStyle("-fx-text-fill: red;");
                                    needRed = true;
                                }
                                                                
                                if (needRed) {
                                    colorId = "debugerButton_red";                                    
                                } else if (needLightGreen) {
                                    colorId = "debugerButton_light_green";                                    
                                } else {
                                    colorId = "debugerButton_green";
                                }
                                
                                Platform.runLater(() -> {
                                    sbmLabel.setText("Seconds_behind_master: " + String.valueOf(secondsBehindMaster));
                                });
                                

                                // add Data for charts                            
                                Long dateTime = new Date().getTime();
                                Long maxTime = dateTime - CACHE_TIME;
                                Platform.runLater(() -> {

                                    {
                                        XYChart.Data<Number, Number> data = new XYChart.Data<>(dateTime, secondsBehindMaster);
                                        if (!secondBehindMasterSeries.getData().contains(data)) {
                                            secondBehindMasterSeries.getData().add(data);
                                        }
                                        
                                        for (XYChart.Data<Number, Number> d: new ArrayList<>(secondBehindMasterSeries.getData())) {
                                            if (d.getXValue().longValue() < maxTime) {
                                                secondBehindMasterSeries.getData().remove(d);
                                            }
                                        }
                                    }

                                    {
                                        XYChart.Data<Number, Number> data0 = new XYChart.Data<>(dateTime, execMasterLogPos);
                                        if (!execMasterLogPosSeries.getData().contains(data0)) {
                                            execMasterLogPosSeries.getData().add(data0);
                                        }
                                        
                                        for (XYChart.Data<Number, Number> d: new ArrayList<>(execMasterLogPosSeries.getData())) {
                                            if (d.getXValue().longValue() < maxTime) {
                                                execMasterLogPosSeries.getData().remove(d);
                                            }
                                        }

                                        XYChart.Data<Number, Number> data1 = new XYChart.Data<>(dateTime, readMasterLogPos);
                                        if (!readMasterLogPosSeries.getData().contains(data1)) {
                                            readMasterLogPosSeries.getData().add(data1);
                                        }
                                        
                                        for (XYChart.Data<Number, Number> d: new ArrayList<>(readMasterLogPosSeries.getData())) {
                                            if (d.getXValue().longValue() < maxTime) {
                                                readMasterLogPosSeries.getData().remove(d);
                                            }
                                        }
                                    }

                                    {
                                        XYChart.Data<Number, Number> data = new XYChart.Data<>(dateTime, relayLogSpace);
                                        if (!relayLogSpaceSeries.getData().contains(data)) {
                                            relayLogSpaceSeries.getData().add(data);
                                        }
                                        
                                        for (XYChart.Data<Number, Number> d: new ArrayList<>(relayLogSpaceSeries.getData())) {
                                            if (d.getXValue().longValue() < maxTime) {
                                                relayLogSpaceSeries.getData().remove(d);
                                            }
                                        }
                                    }

                                    {
                                        if (relayLogWrittenSeries.getData().isEmpty()) {
                                            XYChart.Data<String, Number> data = new XYChart.Data<>(ProfilerBarChartController.formatToXValue(dateTime), relayLogSpace);
                                            relayLogWrittenSeries.getData().add(data);
                                            countRelayLog++;

                                        } else {
                                            XYChart.Data<String, Number> data = (XYChart.Data<String, Number>) relayLogWrittenSeries.getData().get(relayLogWrittenSeries.getData().size() - 1);
                                            String xValue = ProfilerBarChartController.formatToXValue(dateTime);

                                            String v0 = xValue.substring(0, xValue.lastIndexOf(":"));
                                            String v1 = data.getXValue().substring(0, xValue.lastIndexOf(":"));

                                            if (v0.equals(v1)) {
                                                data.setYValue((data.getYValue().longValue() * countRelayLog + relayLogSpace) / (countRelayLog + 1));
                                            } else {
                                                data = new XYChart.Data<>(ProfilerBarChartController.formatToXValue(dateTime), relayLogSpace);
                                                relayLogWrittenSeries.getData().add(data);
                                                countRelayLog = 0;
                                            }
                                            
                                            countRelayLog++;
                                        }
                                                                                
                                        for (XYChart.Data<String, Number> d: new ArrayList<>(relayLogWrittenSeries.getData())) {
                                            if (ProfilerBarChartController.formatFromXValue(d.getXValue()) < maxTime) {
                                                relayLogWrittenSeries.getData().remove(d);
                                            }
                                        }
                                    }                             
                                });
                            } else {
                                sbmLabel.setStyle("-fx-text-fill: green;");
                                Platform.runLater(() -> {
                                    sbmLabel.setText("Seconds_behind_master: -");
                                });
                            }
                        }
                        
                        String staticColorId = colorId;
                        String secondsBehindMasterStrStatic = secondsBehindMasterStr;
                        Platform.runLater(() -> {
                            
                            rlText.setText(secondsBehindMasterStrStatic);
                            
                            if (preferenceService.getPreference().isDebuggerAlertRL()) {
                                if (staticColorId != null) {
                                    replicationLaggingButton.setId(staticColorId);
                                    if ("debugerButton_red".equals(staticColorId)) {
                                        addBlink(replicationLaggingButton);
                                    } else {
                                        removeBlink(replicationLaggingButton);
                                    }
                                } else {
                                    replicationLaggingButton.setId("debugerButton_default");
                                    removeBlink(replicationLaggingButton);
                                }
                            } else {
                                replicationLaggingButton.setId("debugerButton_default");
                                removeBlink(replicationLaggingButton);
                            }
                        });

                    } catch (SQLException ex) {
                       log.error("Error while runnig 'DB_PROFILER__COMMAND__SHOW_FULL_PROCESSLIST'", ex);
                    }

                    List<Object[]> result = profilerService.getProfilerAnalyzerData(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__LOAD_ANALYZE_QUERIES), ChartScale.SCALE_2H.getValue());

                    List<ProfilerQueryData> allQueries = new ArrayList<>(analyzeQueries.values());
                    if (result != null) {

                        List<ProfilerQueryData> newOrOldQueries = new ArrayList<>();

                        for (Object[] row : result) {

                            String user = (String) row[0];
                            String host = (String) row[1];
                            Double ptc = ((BigDecimal) row[2]).doubleValue();
                            Long calls = (Long) row[3];
                            Double avg = ((BigDecimal) row[4]).doubleValue();
                            String database = (String) row[5];
                            String table = (String) row[6];

                            BigDecimal responceTime = (BigDecimal) row[7];
                            responceTime.setScale(2, RoundingMode.HALF_UP);
                            Double minResponceTime = ((BigDecimal) row[8]).doubleValue();
                            Double maxResponceTime = ((BigDecimal) row[9]).doubleValue();
                            Double avgResponceTime = ((BigDecimal) row[10]).doubleValue();
                            Double stddevResponceTime = (Double) row[11];

                            String queries = (String) row[12];
                            String queriesIds = (String) row[13];

                            // find latest id and query
                            String[] idsArray = queriesIds.split("####");
                            String[] queriesArray = queries.split("####");

                            Integer maxId = 0;
                            String explain = null;
                            for (int i = 0; i < idsArray.length; i++) {
                                String id = idsArray[i];

                                Integer value = Integer.valueOf(id);
                                if (value > maxId) {
                                    maxId = value;
                                    explain = queriesArray[i];
                                }
                            }

                            String template = (String) row[14];
                            String query = explain != null ? explain : template;

                            String graph1 = (String) row[15];
                            String graph2 = (String) row[16];

                            ProfilerQueryData ptq = analyzeQueries.get(query);
                            if (ptq == null) {
                                ptq = new ProfilerQueryData(
                                        user,
                                        host,
                                        template,
                                        maxId,
                                        responceTime.divide(new BigDecimal(ChartScale.SCALE_2H.getValue() / 1000), 2, RoundingMode.HALF_UP),
                                        responceTime,
                                        graph1,
                                        graph2,
                                        ptc,
                                        calls,
                                        avg,
                                        database,
                                        table,
                                        minResponceTime,
                                        maxResponceTime,
                                        avgResponceTime,
                                        stddevResponceTime,
                                        query
                                );

                                analyzeQueries.put(query, ptq);
                            } else {
                                ptq.setQueryTemplate(template);
                                ptq.setQueryId(maxId);
                                ptq.setTime(responceTime);
                                ptq.setThreads(responceTime.divide(new BigDecimal(ChartScale.SCALE_2H.getValue() / 1000), 2, RoundingMode.HALF_UP));
                                ptq.setGraph1(graph1);
                                ptq.setGraph2(graph2);
                                ptq.setPtc(ptc);
                                ptq.setCalls(calls);
                                ptq.setDatabase(database);
                                ptq.setTable(table);
                                ptq.setMinResponceTime(minResponceTime);
                                ptq.setMaxResponceTime(maxResponceTime);
                                ptq.setAvgResponceTime(avgResponceTime);
                                ptq.setStddevResponceTime(stddevResponceTime);

                                allQueries.remove(ptq);
                            }

                            newOrOldQueries.add(ptq);
                        }

                        Platform.runLater(() -> {

                            synchronized (analyzerQueryTable) {
                                // remove that left in allQueries
                                analyzerQueryTable.addAndRemoveItems(newOrOldQueries, allQueries);

                                for (ProfilerQueryData p : allQueries) {
                                    analyzeQueries.remove(p.getQueryTemplate());
                                }

                                analyzerQueryTable.refresh();
                            }
                        });
                    }
                }
            }
        }
    }
    
    private void forceAutodetect() {
        rqForceRun = true;
        itForceRun = true;
        dmForceRun = true;
        lmForceRun = true;
        rlForceRun = true;
        speedForceRun = true;
        chartsForceRun = true;
    }
    
    private ChangeListener<Database> databaseListener = (ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
        forceAutodetect();
    };

    public void init(DBService service, UserUsageStatisticService usageService) {
        
        if (!LicenseManager.getInstance().fullUsageExpired(getController())) {
            licenseFullUsageExipred = false;
            LicenseManager.getInstance().usedFullUsage();
        }
        
        this.service = service;
        this.usageService = usageService;

        usageService.savePUsage(PUsageFeatures.DEBUGGER);
        
        rqAvoidSleepButton.setSelected(true);
        dmAllButton.setSelected(true);
        lmAllButton.setSelected(true);
        
        if(databaseSelectedItemProperty != null){
            databaseSelectedItemProperty.addListener(databaseListener);
        }

        if (backgroundWorkProcessor == null) {
            backgroundWorkProcessor = new WorkProc() {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {

                    boolean slaveRunning = false;
                    boolean hasSlaveRunning = false;
                    try (ResultSet rs = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_SLAVE_STATUS))) {
                        if (rs != null && rs.next()) {
                            hasSlaveRunning = true;
                            slaveRunning = "Yes".equals(rs.getString("Slave_IO_Running")) && "Yes".equals(rs.getString("Slave_SQL_Running"));
                        }
                    } catch (Throwable ex) {
                        log.error("Error", ex);
                    }

                    String globalVariablesQuery = null,
                            globalStatusQuery = null,
                            fullProcessQuery = null;
                    Map<String, ProfilerQueryData> analyzeQueries = new HashMap<>();

                    // for testing
                    boolean hasSlaveRunningStatic = hasSlaveRunning;
                    if (slaveRunning) {
                        Platform.runLater(() -> {
                            notValidLabel.setVisible(false);
                            replicationLaggingPane.setVisible(true);
                        });
                        globalVariablesQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_VARIABLES);
                        globalStatusQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_GLOBAL_STATUS);
                        fullProcessQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__SHOW_FULL_PROCESS_LIST);
                    } else {
                        Platform.runLater(() -> {
                            notValidLabel.setVisible(true);
                            replicationLaggingPane.setVisible(false);
                           
                            if (preferenceService.getPreference().isDebuggerAlertRL()) {
                                if (hasSlaveRunningStatic) {
                                    replicationLaggingButton.setId("debugerButton_red");
                                }
                            } else {
                                replicationLaggingButton.setId("debugerButton_default");
                            }
                            
                            rlLabel.setText("Replication has broken");
                            rlText.setText("");
                            
                            if (preferenceService.getPreference().isDebuggerAlertRM()) {
                                if (hasSlaveRunningStatic) {
                                    replicationMonitorButton.setId("debugerButton_red");
                                }
                            } else {
                                replicationMonitorButton.setId("debugerButton_default");
                            }
                            
                            rmLabel.setText("Replication has broken");
                        });
                    }

                    initConfigurationDebugger(null);

                    long autodetectForce = 0l;

                    long lastTimeRunned = 0l;
                    while (!needStop) {
//                        System.out.println("whiling");
                        lastTimeRunned = System.currentTimeMillis();
                        
                        TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
                        if (tcc != null) {

                            Tab t = tcc.getSelectedTab().get();
                            if (t != null && t.getUserData() instanceof DebuggerTabController && bottomTabPane.getSelectionModel().getSelectedItem() == autodetectTab) {

                                if (autodetectForce == 0l || lastTimeRunned - autodetectForce >= 15000) {
                                    
                                    // update blink option
                                    boolean oldBlink = blinkEnabled;
                                    blinkEnabled = preferenceService.getPreference().isDebuggerEnableBlink();
                                    
                                    if (oldBlink != blinkEnabled) {
                                        synchronized (DebuggerTabController.this) {
                                            for (FadeTransition fd: blinksMap.values()) {
                                                if (blinkEnabled) {
                                                    fd.play();
                                                } else {
                                                    fd.stop();
                                                }
                                            }
                                        }
                                    }
                                    
                                    forceAutodetect();
                                    autodetectForce = lastTimeRunned;
                                }
                            }
                        }
                        
                        chartsCodeBranch(lastTimeRunned);
                        
                        runningQueriesCodeBranch(lastTimeRunned);
                        innoTopCodeBranch(lastTimeRunned);
                        deadlockMonitorCodeBranch(lastTimeRunned);
                        lockMonitorCodeBranch(lastTimeRunned);
                        sqlSpeedometerBranch(lastTimeRunned);
                        dbConnectionsAnalyzerCodeBranch(lastTimeRunned);

                        if (slaveRunning) {
                            Platform.runLater(() -> {
                               replicationLagging.setText("Replication Lagging");
                            });
                            
                            replicationLaggingCodeBranch(lastTimeRunned, globalVariablesQuery, globalStatusQuery, fullProcessQuery, analyzeQueries);
                        } else {
                            Platform.runLater(() -> {
                               replicationLagging.setText("Replication Lagging"); 
                            });
                        }

                        chartsCodeBranch(lastTimeRunned);
                        /**
                         * sleep for 1 second This is to ensure this while loops
                         * sleeps for exactly 1 second. If true, then
                         * timesRunned will always be number of seconds while
                         * loop runned
                         */
                        long sleeperTime = TimeUnit.SECONDS.toMillis(1) - (System.currentTimeMillis() - lastTimeRunned);
                        if (sleeperTime > -1) {
                            try {
                                Thread.sleep(sleeperTime);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            };
            SynchorniseWorkProccessor swp = Recycler.getLessBusySWP();
            if (swp == null) {                
                Recycler.addWorkProc(backgroundWorkProcessor);
            } else {
                swp.addWorkProc(backgroundWorkProcessor);
            }
        }

        rqPaused = false;
        rqStartButton.setDisable(true);
        rqPauseButton.setDisable(false);

        itPaused = false;
        itStartButton.setDisable(true);
        itPauseButton.setDisable(false);

        dbPaused = false;
        dbStartButton.setDisable(true);
        dbPauseButton.setDisable(false);

        dmPaused = false;
        dmStartButton.setDisable(true);
        dmPauseButton.setDisable(false);

        lmPaused = false;
        lmStartButton.setDisable(true);
        lmPauseButton.setDisable(false);

        replPaused = false;
        rlStartButton.setDisable(true);
        rlPauseButton.setDisable(false);

//        dbConnectionsAnalyzerCodeBranch();
    }

    private String readDeadLock(String detail, List<DeadlockMonitorQueryData> list) {

        String serverDate = null;
        try {
            String[] rows = detail.split("\n");
            if (rows.length > 1) {
                String[] str = rows[2].split(" ");
                serverDate = str[0] + " " + str[1];
            }
        } catch (Throwable th) {
            log.error("Need to fix, its for debug", th);
        }

        int i = detail.indexOf("LATEST DETECTED DEADLOCK\n------------------------");
        if (i > -1) {
            String str = detail.substring(i + 50);
            if (str.contains("\n------")) {
                str = str.substring(0, str.indexOf("\n------"));
            }

            boolean showExpiredInfo = true;
            String[] data = str.split("\n");
            for (i = 0; i < data.length; i++) {

                String time = null;
                String query1 = null;
                String query2 = null;

                String database1 = null;
                String database2 = null;

                String timeStr = data[i];

                Matcher timeMatcher = PATTERN__TIME0.matcher(timeStr);
                if (timeMatcher.find()) {
                    try {
                        Date d0 = dateFormat0.parse(timeMatcher.group());
                        time = dateFormat0.format(new Date(d0.getTime() + serverTimeDiff));
                    } catch (ParseException ex) {
                        log.error("Error", ex);
                    }                
                }

                if (time == null) {
                    timeMatcher = PATTERN__TIME1.matcher(timeStr);
                    if (timeMatcher.find()) {
                        time = timeMatcher.group();
                    }

                    // convert to default
                    if (time != null) {
                        try {
                            Date d0 = dateFormat1.parse(time);
                            time = dateFormat0.format(d0.getTime() + serverTimeDiff);
                        } catch (ParseException ex) {
                            log.error("Error", ex);
                        }
                    }
                }

                if (time != null) {

                    boolean firstTransactionBegin = false;
                    boolean firstTransactionEnded = false;

                    boolean secondTransactionBegin = false;
                    boolean secondTransactionEnded = false;

                    String query = "";
                    for (int j = i; j < data.length; j++) {

                        i = j;

                        String s = data[j];

                        // need find *** (1)
                        if (!firstTransactionEnded && s.startsWith("*** (1)")) {

                            if (firstTransactionBegin) {
                                firstTransactionBegin = false;
                                firstTransactionEnded = true;
                                query1 = query;
                                query = "";
                            } else {
                                firstTransactionBegin = true;
                            }
                            continue;
                        }

                        // need find  *** (2)
                        if (!secondTransactionEnded && s.startsWith("*** (2)")) {

                            if (secondTransactionBegin) {
                                secondTransactionBegin = false;
                                secondTransactionEnded = true;
                                query2 = query;
                                query = "";
                            } else {
                                secondTransactionBegin = true;
                            }
                            continue;
                        }

                        // search lock queries
                        // select / insert / update / delete
                        String s0 = s.trim().toLowerCase();
                        if (!query.isEmpty()
                                || s0.startsWith("select")
                                || s0.startsWith("insert")
                                || s0.startsWith("update")
                                || s0.startsWith("delete")) {

                            if (!query.isEmpty()) {
                                query += "\n";
                            }
                            query += s;
                        }

                        if (s0.startsWith("record locks")) {
                            Matcher m = DATABASE__ROLLBACK.matcher(s0);
                            if (m.find()) {
                                if (secondTransactionEnded) {
                                    database2 = m.group(1);
                                } else {
                                    database1 = m.group(1);
                                }
                            }
                        }

                        // if both ended - break it
                        if (firstTransactionEnded && secondTransactionEnded) {
                            break;
                        }
                    }

                    String rollback = null;

                    for (int j = i; i < data.length; j++) {
                        if (data[j].indexOf("*** WE ROLL BACK TRANSACTION") > -1) {
                            Matcher m = PATTERN__ROLLBACK.matcher(data[j]);
                            if (m.find()) {
                                rollback = "TRANSACTION " + m.group(1);
                            }
                            break;
                        }
                    }

                    String db = null;
                    Map<String, String> m1 = QueryFormat.getTableNamesFromQuery(query1);
                    if (m1 != null && !m1.isEmpty()) {
                        for (String k : m1.keySet()) {
                            if (k.contains(".")) {
                                db = k.split("\\.")[0];
                                if (db.startsWith("`")) {
                                    db = db.substring(1);
                                }

                                if (db.endsWith("`")) {
                                    db = db.substring(0, db.length() - 1);
                                }
                            }

                            if (db != null) {
                                break;
                            }
                        }
                    }

                    if (db == null) {
                        Map<String, String> m2 = QueryFormat.getTableNamesFromQuery(query2);
                        if (m2 != null && !m2.isEmpty()) {
                            for (String k : m2.keySet()) {
                                if (k.contains(".")) {
                                    db = k.split("\\.")[0];
                                    if (db.startsWith("`")) {
                                        db = db.substring(1);
                                    }

                                    if (db.endsWith("`")) {
                                        db = db.substring(0, db.length() - 1);
                                    }
                                }

                                if (db != null) {
                                    break;
                                }
                            }
                        }
                    }

                    db = db != null ? db : (database2 != null ? database2 : database1);

                    Database database = db != null ? DatabaseCache.getInstance().getDatabase(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), db) : null;
                    if (database == null && db != null) {
                        try {
                            database = DatabaseCache.getInstance().syncDatabase(db, getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), (AbstractDBService) getController().getSelectedConnectionSession().get().getService());
                        } catch (SQLException ex) {
                            log.error("Error", ex);
                            database = null;
                        }
                    }

                    Pair<String[], Boolean> res1 = explain(query1, database, showExpiredInfo);
                    Pair<String[], Boolean> res2 = explain(query2, database, showExpiredInfo);

                    String[] explain1 = res1.getKey();
                    String[] explain2 = res2.getKey();
                    if (!res1.getValue() || !res2.getValue()) {
                        showExpiredInfo = false;
                    }
                    
                    String status = explain1 != null && !explain1[0].isEmpty() ? explain1[0] : "Not able to fix";
                    if (!"Fixed".equals(status) && !"Error".equals(status)) {
                        status = explain2 != null && !explain2[0].isEmpty() ? explain2[0] : "Not able to fix";
                    }

                    try {
                        QueryClause clause1 = QueryFormat.parseQuery(query1);
                        QueryClause clause2 = QueryFormat.parseQuery(query2);

                        if (clause1 instanceof InsertCommand && clause2 instanceof InsertCommand) {
                            if (((InsertCommand) clause1).getIntoTable().getName().equalsIgnoreCase(((InsertCommand) clause2).getIntoTable().getName())) {
                                status = "The INSERT commands cases deadlock if you try to insert more then 90% identical data from both transactions or the table " + ((InsertCommand) clause2).getIntoTable().getName() + " (need to replace table) may have too many indexes";
                            }
                        }
                    } catch (Throwable th) {
                    }

                    DeadlockMonitorQueryData d = new DeadlockMonitorQueryData(
                            detail,
                            status,
                            time,
                            query1,
                            query2,
                            explain1 != null ? explain1[1] : "Please select database",
                            explain1 != null ? explain1[2] : "Please select database",
                            explain2 != null ? explain2[1] : "Please select database",
                            explain2 != null ? explain2[2] : "Please select database",
                            db,
                            rollback
                    );
                    list.add(d);
                }
            }
        }

        return serverDate;
    }

    public Pair<String[], Boolean> explain(String sourceQuery, Database db, boolean showExpiredInfo) {

        String status = "";
        String recommendations = "";
        String recommendedIndex = "";

        usageService.savePUsage(PUsageFeatures.DQA);
        try {

            ConnectionSession cs = getController().getSelectedConnectionSession().get();
            Database database = db != null ? db : cs.getConnectionParam().getSelectedDatabase();
            if (database == null) {
                return null;
            }

            ScriptRunner sr = new ScriptRunner(cs.getService().getDB(), true);
            Map<Integer, List<String>> queries = sr.findQueries(new StringReader(sourceQuery));
            if (queries != null) {

                List<QORecommendation> results = new ArrayList<>();
                for (List<String> list : queries.values()) {

                    if (list != null) {
                        for (String query : list) {
                            try {
                                boolean useCache = preferenceService.getPreference().isQaOtherUseCache();
                                QORecommendation qaRecommendations = QACache.getQARecommandation(
                                    getController(), 
                                    qars, 
                                    columnCardinalityService, 
                                    performanceTuningService, 
                                    database, 
                                    query, 
                                    !useCache, 
                                    true,
                                    true,
                                    licenseFullUsageExipred,
                                    showExpiredInfo
                                );
                                results.add(qaRecommendations);
                                
                                showExpiredInfo = qaRecommendations.isHasCredits();
                            } catch (Throwable th) {
                                results.add(QORecommendation.emptyError(query, database.getName()));
                            }
                        }
                    }
                }

                for (QORecommendation ars : results) {
                    if (ars.isError()) {
                        for (List<String> rr : ars.getRecommendations().values()) {

                            for (String r : rr) {
                                if (!recommendations.isEmpty()) {
                                    recommendations += "; ";
                                }

                                recommendations += r;
                                status = "Error";
                            }
                        }
                    } else {
                        for (List<QOIndexRecommendation> list : ars.getIndexes().values()) {
                            for (QOIndexRecommendation rr : list) {

                                if (rr.getRecommendation() != null) {
                                    if (!recommendations.isEmpty()) {
                                        recommendations += "; ";
                                    }

                                    recommendations += rr.getRecommendation();
                                    status = "Fixed";
                                }

                                if (rr.getIndexQuery() != null) {
                                    if (!recommendedIndex.isEmpty()) {
                                        recommendedIndex += "; ";
                                    }

                                    recommendedIndex += rr.getIndexQuery().toUpperCase();
                                    status = "Fixed";
                                }
                            }
                        }
                    }
                }

                if (results.isEmpty()) {
                    status = "Not able to fix";
                    recommendations = "The SmartMySQL could not identified any improvements.";
                    recommendedIndex = "The SmartMySQL could not identified any improvements.";
                }
            }
        } catch (Throwable th) {
            Platform.runLater(() -> {
                DialogHelper.showError(getController(), "Error on analyzing query", th.getMessage(), th, getStage());
            });
        }

        return new Pair(new String[]{status, recommendedIndex, recommendations}, showExpiredInfo);
    }

    public void destroy() {
        needStop = true;
        if (innodbMatrixController != null) {
            innodbMatrixController.destroy();
        }
        
        if (myISAMController != null) {
            myISAMController.destroy();
        }
        
        databaseSelectedItemProperty.removeListener(databaseListener);
    }

    private void addEffectToUI() {
//        Utitlities.prepFlowPaneDimensionConditionWithParent(chartContainer, 2, false);
//        chartContainer.setHgap(10);
//        chartContainer.setVgap(10);

        deadlockSplitVContainer.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);
        deadlockChartBox.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);

        for (Tab t : bottomTabPane.getTabs()) {
            if (t.getText().charAt(1) == 'B') {
                continue;
            }
            t.getContent().getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);
            if (t.getContent() instanceof VBox) {
                VBox.setMargin(((VBox) t.getContent()).getChildren().get(1), new Insets(-3, 0, 0, 0));
                //((VBox)t.getContent()).getChildren().get(1).getStyleClass().add("debugger");
                t.getContent().getStyleClass().add("debugger");
            }
        }

        mainUI.sceneProperty().addListener(new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> observable,
                    Scene oldValue, Scene newValue) {
                mainUI.sceneProperty().removeListener(this);
                Platform.runLater(() -> {
                    if (!((Stage) newValue.getWindow()).isFullScreen()) {
                        newValue.getWindow().setWidth(newValue.getWindow().getWidth() + 0.1);
                    }
                });
            }
        });

        wisePie.setStyle("-fx-label-line-length: 20; -fx-pie-label-visible : true;");
        wisePie.getStyleClass().add("debugger");
        wisePie.setLegendVisible(false);
        wisePie.prefHeightProperty().bind(wiseBarContainer.heightProperty());
        wisePie.getData().addListener(new ListChangeListener<PieChart.Data>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends PieChart.Data> c) {
                boolean reLoop = false;

                Double totalSumInUnits = chartSums.get(wisePie);
                if (totalSumInUnits == null) {
                    totalSumInUnits = 0.0;
                }

                while (c.next()) {
                    if (c.wasAdded()) {
                        reLoop = true;
                        for (PieChart.Data pd : c.getAddedSubList()) {
                            totalSumInUnits += pd.getPieValue();
                            wisePie.getProperties().put(PIE_TOTAL_SUM_IN_UNITS, totalSumInUnits);
                        }
                    }
                }

                chartSums.put(wisePie, totalSumInUnits);

                if (reLoop) {
                    Platform.runLater(() -> {
                        for (PieChart.Data pd : wisePie.getData()) {
                            pd.setName(calcStringOnPieData(pd, 0));
                            installLabelInPieSpace(pd);
                            if (PlatformUtil.isMac()) {
                                Platform.runLater(() -> pd.getChart().layout());
                            }
                        }
                    });
                }
            }
        });

        Node chartPieLine = wisePie.lookup(".chart-pie-label-line");
        if (chartPieLine != null) {
            chartPieLine.setOpacity(0.0);
        }

//        /**
//         * Additional effects
//         */
//        
//        ArrayList<Node> al = new ArrayList(chartContainer.getChildren());
//        chartContainer.getChildren().clear();
//        chartContainer.getStyleClass().remove(Flags.METRIC_INNERS_STYLE_CLASS);
//        for (Node n : al) {
//            AnchorPane ap = new AnchorPane();
//            
//            Button expandButton = new Button();
//            ImageView iv = new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/profiler_zoomIn.png"));
//            iv.setFitHeight(30);
//            iv.setFitWidth(30);
//            expandButton.setGraphic(iv);
//            expandButton.setStyle("-fx-background-color: transparent;");
//            expandButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            
//            Button closeButton = new Button();
//            iv = new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/dialog-error.png"));
//            iv.setFitHeight(30);
//            iv.setFitWidth(30);
//            closeButton.setGraphic(iv);
//            closeButton.setStyle("-fx-background-color: transparent;");
//            closeButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            n.getStyleClass().remove(Flags.METRIC_INNERS_STYLE_CLASS);
//            ap.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);
//                       
//            closeButton.setOnAction(new EventHandler<ActionEvent>() {
//                @Override
//                public void handle(ActionEvent event) {
//                    closeButton.toBack();expandButton.toFront();
//                    stackofpieContainer.getChildren().remove(ap);
//                    ap.setPrefHeight((Double) stackofpieContainer.getProperties().get(PIE_PARENT_HEIGHT));
//                    ap.setMaxHeight(ap.getPrefHeight());
//                    chartContainer.getChildren().add(
//                            (Integer) ap.getProperties().get(PIE_PARENT_INDEX_IN_CONTAINER_FIELD)
//                            ,ap);
//                     ap.setPadding(Insets.EMPTY);
//                }
//            });
//            expandButton.setOnAction(new EventHandler<ActionEvent>() {
//                @Override
//                public void handle(ActionEvent event) {
//                    if(stackofpieContainer.getChildren().contains(ap)){ return; }
//                    
//                    if (!stackofpieContainer.getProperties().containsKey(PIE_PARENT_HEIGHT)) {
//                        stackofpieContainer.getProperties().put(PIE_PARENT_HEIGHT, ap.getHeight());
//                    }
//                    chartContainer.getChildren().remove(ap);
//                    ap.setMaxHeight(AnchorPane.USE_COMPUTED_SIZE);
//                    stackofpieContainer.getChildren().add(ap);
//                    closeButton.toFront();expandButton.toBack();
//                    ap.setPadding(new Insets(0, 50,0,0));
//                }
//            });
//                        
//            AnchorPane.setBottomAnchor(n, 0.0);
//            AnchorPane.setRightAnchor(n, 0.0);
//            AnchorPane.setLeftAnchor(n, 0.0);
//            AnchorPane.setTopAnchor(n, 0.0);
//            
//            AnchorPane.setLeftAnchor(closeButton, 25.0);
//            AnchorPane.setTopAnchor(closeButton, 5.0);
//            
//            AnchorPane.setRightAnchor(expandButton, 15.0);
//            AnchorPane.setTopAnchor(expandButton, 3.0);
//            
//            ap.getChildren().addAll(closeButton,n,expandButton);
//            chartContainer.getChildren().add(ap);
//            ap.getProperties().put(PIE_PARENT_INDEX_IN_CONTAINER_FIELD, chartContainer.getChildren().size() -1);
//        }    
    }

    /**
     *
     * 0 = all; 1 = all without percent 2 = only name 3 = percent 4> = ratio
     */
    private String calcStringOnPieData(PieChart.Data pd, int who) {
        double ratio = pd.getPieValue() / ((Double) pd.getChart().getProperties().get(PIE_TOTAL_SUM_IN_UNITS));
        String name = (String) pd.getNode().getProperties().get(PIE_NODE_NAME);

        if (name == null || name.isEmpty()) {
            name = pd.getName();
            pd.getNode().getProperties().put(PIE_NODE_NAME, name);
        }

        String temp = String.valueOf(ratio * 100);
        int indexOfDot = temp.indexOf('.');
        if (indexOfDot > -1) {
            String afterDot = temp.substring(indexOfDot, indexOfDot + 2);
            temp = temp.substring(0, indexOfDot) + afterDot;
        }
        pd.getNode().getProperties().put(PIE_NODE_PERCENTILE, temp + "%");

        return (who == 0 ? name + " : " + pd.getPieValue() + " - " + (String) pd.getNode().getProperties().get(PIE_NODE_PERCENTILE)
                : who == 1 ? name + " : " + pd.getPieValue()
                        : who == 2 ? name
                                : who == 3 ? (String) pd.getNode().getProperties().get(PIE_NODE_PERCENTILE)
                                        : String.valueOf(ratio));
    }

    private void installLabelInPieSpace(PieChart.Data pd) {
        Tooltip.install(pd.getNode(), new Tooltip(calcStringOnPieData(pd, 1)));
        //the text label on the text
        try {
            Field pieDataTextNodeField = pd.getClass().getDeclaredField("textNode");
            pieDataTextNodeField.setAccessible(true);
            Text text = (Text) pieDataTextNodeField.get(pd);
            if (text != null) {
                text.setText(calcStringOnPieData(pd, 3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private AnchorPane getTopicBox(String text0, String text1, Color color, Gauge gauge, Label perHour, GridPane parent) {

        String bcolor = "#" + Integer.toHexString(color.hashCode());

        Label label0 = new Label(text0);
        label0.setStyle("-fx-font-size: 16; -fx-text-fill: " + bcolor);
        label0.setTextFill(color);
        label0.setAlignment(Pos.CENTER);

        Label label1 = new Label(text1);
        label1.setStyle("-fx-font-size: 16;");
        label1.setTextFill(color);
        label1.setAlignment(Pos.CENTER);

        gauge.setBarColor(color);
        gauge.setBarBackgroundColor(Color.rgb(39, 44, 50));
        gauge.setAnimated(true);

        perHour.setTextFill(color);
        perHour.setStyle("-fx-font-size: 20; -fx-font-weight: normal; -fx-padding: -30 0 0 0");
        perHour.setAlignment(Pos.CENTER);

        HBox hb = new HBox(label0, label1);
        hb.setAlignment(Pos.CENTER);

        VBox vBox = new VBox(hb, gauge, perHour);
        vBox.setSpacing(3);
        vBox.setAlignment(Pos.CENTER);
        vBox.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);

        /**/
        AnchorPane ap = new AnchorPane();
        ap.setStyle("-fx-border-color: " + bcolor + "; -fx-border-width: 2px; -fx-padding: 3");

        Button expandButton = new Button();
        ImageView iv = new ImageView(StandardDefaultImageProvider.getInstance().getProfilerZoomIn_image());
        iv.setFitHeight(20);
        iv.setFitWidth(20);
        expandButton.setGraphic(iv);
        expandButton.setStyle("-fx-background-color: transparent;");
        expandButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

        Button closeButton = new Button();
        iv = new ImageView(StandardDefaultImageProvider.getInstance().getDialogError_image());
        iv.setFitHeight(30);
        iv.setFitWidth(30);
        closeButton.setGraphic(iv);
        closeButton.setStyle("-fx-background-color: transparent;");
        closeButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        ap.getStyleClass().add(Flags.METRIC_INNERS_STYLE_CLASS);

        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                closeButton.toBack();
                expandButton.toFront();
                stackofgaugeContainer.getChildren().remove(ap);
                ap.setPrefHeight((Double) stackofgaugeContainer.getProperties().get(PIE_PARENT_HEIGHT));
                ap.setMaxHeight(ap.getPrefHeight());

                String[] d = ((String) stackofgaugeContainer.getProperties().get(PIE_PARENT_INDEX_IN_CONTAINER_FIELD)).split("-");
                parent.add(ap, Integer.valueOf(d[0]), Integer.valueOf(d[1]));

                ap.setPadding(Insets.EMPTY);
            }
        });
        expandButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (stackofgaugeContainer.getChildren().contains(ap)) {
                    return;
                }
                if (!stackofgaugeContainer.getProperties().containsKey(PIE_PARENT_HEIGHT)) {
                    stackofgaugeContainer.getProperties().put(PIE_PARENT_HEIGHT, ap.getHeight());
                }

                String d = String.valueOf(GridPane.getColumnIndex(ap)) + "-" + String.valueOf(GridPane.getRowIndex(ap));
                stackofgaugeContainer.getProperties().put(PIE_PARENT_INDEX_IN_CONTAINER_FIELD, d);
                parent.getChildren().remove(ap);

                ap.setMaxHeight(AnchorPane.USE_COMPUTED_SIZE);
                stackofgaugeContainer.getChildren().add(ap);
                closeButton.toFront();
                expandButton.toBack();
                ap.setPadding(new Insets(0, 50, 0, 0));
            }
        });

        AnchorPane.setBottomAnchor(vBox, 0.0);
        AnchorPane.setRightAnchor(vBox, 0.0);
        AnchorPane.setLeftAnchor(vBox, 0.0);
        AnchorPane.setTopAnchor(vBox, 0.0);

        AnchorPane.setLeftAnchor(closeButton, 25.0);
        AnchorPane.setTopAnchor(closeButton, 5.0);

        AnchorPane.setRightAnchor(expandButton, 5.0);
        AnchorPane.setTopAnchor(expandButton, 2.0);

        ap.getChildren().addAll(closeButton, vBox, expandButton);

        /**/
        return ap;
    }

    private void initSqlSpeedometerPane() {

        double gaugePrefDimension = 500;

        totalQueriesPerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxTotalQueries / X_SEC_DEFAULT)
                .title("Total Queries")
                .unit("x100 per sec")
                .animated(true)
                .build();

        writeQueriesPerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxWriteQueries / X_SEC_DEFAULT)
                .title("Write Queries")
                .unit("x100 per sec")
                .animated(true)
                .build();

        selectPerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxSelect / X_SEC_DEFAULT)
                .title("Select")
                .unit("x100 per sec")
                .animated(true)
                .build();

        insertPerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxInsert / X_SEC_DEFAULT)
                .title("Insert")
                .unit("x100 per sec")
                .animated(true)
                .build();

        updatePerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxUpdate / X_SEC_DEFAULT)
                .title("Update")
                .unit("x100 per sec")
                .animated(true)
                .build();

        deletePerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxDelete / X_SEC_DEFAULT)
                .title("Delete")
                .unit("x100 per sec")
                .animated(true)
                .build();

        commitPerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxCommit / X_SEC_DEFAULT)
                .title("Commit")
                .unit("x100 per sec")
                .animated(true)
                .build();

        beginPerSec = GaugeBuilder.create()
                .prefSize(gaugePrefDimension, gaugePrefDimension)
                .knobPosition(Pos.BOTTOM_CENTER)
                .angleRange(270)
                .foregroundBaseColor(Color.WHITE)
                .decimals(2)
                .maxValue(maxCommit / X_SEC_DEFAULT)
                .title("Begin")
                .unit("x100 per sec")
                .animated(true)
                .build();

        totalQueriesPerMin = new Label("0 per sec / 0 per min");
        writeQueriesPerMin = new Label("0 per sec / 0 per min");
        selectPerMin = new Label("0 per sec / 0 per min");
        insertPerMin = new Label("0 per sec / 0 per min");
        updatePerMin = new Label("0 per sec / 0 per min");
        deletePerMin = new Label("0 per sec / 0 per min");
        commitPerMin = new Label("0 per sec / 0 per min");
        beginPerMin = new Label("0 per sec / 0 per min");

        GridPane pane = new GridPane();
        pane.setPadding(new Insets(20));
        pane.setHgap(10);
        pane.setVgap(15);
        pane.setBackground(new Background(new BackgroundFill(Color.web("#141414"), CornerRadii.EMPTY, Insets.EMPTY)));

        AnchorPane totalQueriesPane = getTopicBox("Total", " Queries per sec & min", Color.rgb(77, 208, 225), totalQueriesPerSec, totalQueriesPerMin, pane);
        AnchorPane writeQueriesPane = getTopicBox("Write", " Queries per sec & min", Color.rgb(255, 79, 79), writeQueriesPerSec, writeQueriesPerMin, pane);
        AnchorPane selectPane = getTopicBox("Read", "(select) Queries per sec & min", Color.rgb(255, 183, 77), selectPerSec, selectPerMin, pane);
        AnchorPane insertPane = getTopicBox("Insert", " Queries per sec & min", Color.rgb(129, 199, 132), insertPerSec, insertPerMin, pane);
        AnchorPane updatePane = getTopicBox("Update", " Queries per sec & min", Color.rgb(149, 117, 205), updatePerSec, updatePerMin, pane);
        AnchorPane deletePane = getTopicBox("Delete", " Queries per sec & min", Color.rgb(186, 104, 200), deletePerSec, deletePerMin, pane);
        AnchorPane commitPane = getTopicBox("Commits", " per sec & min", Color.rgb(229, 115, 115), commitPerSec, commitPerMin, pane);
        AnchorPane beginPane = getTopicBox("Start", " Transactions per sec & min", Color.rgb(129, 15, 115), beginPerSec, beginPerMin, pane);

        pane.add(totalQueriesPane, 0, 0);
        pane.add(writeQueriesPane, 1, 0);
        pane.add(selectPane, 2, 0);
        pane.add(insertPane, 3, 0);
        pane.add(updatePane, 0, 1);
        pane.add(deletePane, 1, 1);
        pane.add(commitPane, 2, 1);
        pane.add(beginPane, 3, 1);

        sqlSpeedometerPane.getChildren().add(pane);
        AnchorPane.setTopAnchor(pane, 5.0);
        AnchorPane.setRightAnchor(pane, 5.0);
        AnchorPane.setBottomAnchor(pane, 5.0);
        AnchorPane.setLeftAnchor(pane, 5.0);

        speedometerNameColumn.setCellValueFactory(cellData -> ((SpeedometerData) cellData.getValue()).name());
        speedometerValueColumn.setCellValueFactory(cellData -> ((SpeedometerData) cellData.getValue()).value());
        speedometerValuePctColumn.setCellValueFactory(cellData -> ((SpeedometerData) cellData.getValue()).valuePct());
        speedometerLastIncColumn.setCellValueFactory(cellData -> ((SpeedometerData) cellData.getValue()).lastInc());
        speedometerLastIncPctColumn.setCellValueFactory(cellData -> ((SpeedometerData) cellData.getValue()).lastIncPct());
    }

    private ProfilerChartController loadChartControllers(GSChart chart, boolean minimized) {
        ProfilerChartController pcc = loadEntityMetricControllers(ProfilerChartController.class);
        pcc.insertGSChart(chart, minimized);
        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }

    private ProfilerBarChartController loadBarChartControllers(GSChart chart) {

        ProfilerBarChartController pcc = loadEntityMetricControllers(ProfilerBarChartController.class);
        pcc.insertGSChart(chart);
        return pcc;
    }

    private <T extends MetricEntityContainerController> T loadEntityMetricControllers(Class<T> meccClass) {
        T t = StandardBaseControllerProvider.getController(meccClass, getController(), "MetricEntityContainer");
        t.activateMetric();
        return t;
    }

    private Thread chartsThread;
    
    @FXML
    private ScrollPane innodbStatusContainer;
    private DebuggerInnodbMatrixController innodbMatrixController;
    
    private DBDebuggerProfilerService debuggerProfilerService;

    // @TODO Check it
    private void initInnoDBStatus() {
        innodbMatrixController = StandardBaseControllerProvider.getController(getController(), "DebuggerInnodbMatrix");

        innodbMatrixController.init(DebuggerTabController.this, debuggerProfilerService, chartService);
        innodbStatusContainer.setContent(innodbMatrixController.getUI());
    }

    @FXML
    private ScrollPane myISAMContainer;
    private DebuggerMyISAMController myISAMController;

    // @TODO Check it
    private void initMyISAMStatus() {
        myISAMController = StandardBaseControllerProvider.getController(getController(), "DebuggerMyISAM");
        myISAMController.init(DebuggerTabController.this, debuggerProfilerService, chartService);
        myISAMContainer.setContent(myISAMController.getUI());
    }
    
    
//    @FXML
//    private VBox dbSizeContainer;
//    private DebuggerMyISAMController myISAMController;
//        
//    // @TODO Check it
//    private void initMyISAMStatus() {
//        controller.getProvider().setInput("DebuggerMyISAM");
//
//        myISAMController = (DebuggerMyISAMController) controller.getProvider().get();
//        
//        myISAMController.init(this, debuggerProfilerService);
//        
//        myISAMContainer.setContent(myISAMController.getUI());
//    }
//    
//    @FXML
//    private VBox unusedIndexesContainer;
//    private DebuggerMyISAMController myISAMController;
//        
//    // @TODO Check it
//    private void initMyISAMStatus() {
//        controller.getProvider().setInput("DebuggerMyISAM");
//
//        myISAMController = (DebuggerMyISAMController) controller.getProvider().get();
//        
//        myISAMController.init(this, debuggerProfilerService);
//        
//        myISAMContainer.setContent(myISAMController.getUI());
//    }
//    
//    @FXML
//    private VBox duplicateIndexesTab;
//    private DebuggerMyISAMController myISAMController;
//        
//    // @TODO Check it
//    private void initMyISAMStatus() {
//        controller.getProvider().setInput("DebuggerMyISAM");
//
//        myISAMController = (DebuggerMyISAMController) controller.getProvider().get();
//        
//        myISAMController.init(this, debuggerProfilerService);
//        
//        myISAMContainer.setContent(myISAMController.getUI());
//    }

    public Double getDoubleGlobalVariable(String key) {
        Double result = GLOBAL_VARIABLES.get(key);
        return result != null ? result : 0.0;
    }

    public boolean containsGlobalVariable(String key) {
        return GLOBAL_VARIABLES.containsKey(key);
    }

    public boolean hasGlobalStatus(String key) {
        return GLOBAL_STATUS.containsKey(key);
    }

    public Double getDoubleGlobalStatus(String key, boolean value) {
        Double[] result = GLOBAL_STATUS.get(key);
        return result != null ? (value ? result[0] : result[1]) : 0.0;
    }

    @FXML
    private SplitPane deadlockSplitPane;

    private void initDeadlockCharts() {
        GSChart gsChart = chartService.get(35l);
        GSChartVariable v = gsChart.getVariableByName("DEADLOCKS__DEADLOCKS");

        deadlockChartPane = loadChartControllers(gsChart, true);
        deadlockChartPane.minimize();

        AnchorPane chartPane = deadlockChartPane.getUI();
        chartPane.setMaxWidth(Double.MAX_VALUE);
        chartPane.setMaxHeight(Double.MAX_VALUE);
        deadlockChartBox.getChildren().add(chartPane);

        deadlockSeries = new XYChart.Series();
        deadlockSeries.setName(v.getDisplayName());

        List<XYChart.Series<Number, Number>> listSeries = new ArrayList<>();
        listSeries.add(deadlockSeries);

        Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
        seriesColors.put(deadlockSeries, v.getDisplayColor());

        deadlockChartPane.init(listSeries, seriesColors);

        HBox.setHgrow(chartPane, Priority.ALWAYS);

        Platform.runLater(() -> deadlockSplitPane.setDividerPosition(0, 0.76));
    }


    private void initReplicationCharts() {
        {
            GSChart gsChart = chartService.get(25l);
            GSChartVariable v = gsChart.getVariableByName("Seconds_Behind_Master");

            replicationDelayChartPane = loadChartControllers(gsChart, false);
            AnchorPane chartPane = replicationDelayChartPane.getUI();
            chartPane.setMaxWidth(Double.MAX_VALUE);
            replicationDelayChartBox.getChildren().add(chartPane);

            secondBehindMasterSeries = new XYChart.Series();
            secondBehindMasterSeries.setName(v.getDisplayName());

            List<XYChart.Series<Number, Number>> listSeries = new ArrayList<>();
            listSeries.add(secondBehindMasterSeries);

            Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
            seriesColors.put(secondBehindMasterSeries, v.getDisplayColor());

            replicationDelayChartPane.init(listSeries, seriesColors);
            
            HBox.setHgrow(chartPane, Priority.ALWAYS);
        }
        
        {
            GSChart gsChart = chartService.get(26l);
            GSChartVariable v0 = gsChart.getVariableByName("Exec_Master_Log_Pos");
            GSChartVariable v1 = gsChart.getVariableByName("Read_Master_Log_Pos");

            replicationSyncChartPane = loadChartControllers(gsChart, false);
            AnchorPane chartPane = replicationSyncChartPane.getUI();
            chartPane.setMaxWidth(Double.MAX_VALUE);
            replicationSyncChartBox.getChildren().add(chartPane);

            execMasterLogPosSeries = new XYChart.Series();
            execMasterLogPosSeries.setName(v0.getDisplayName());
            
            readMasterLogPosSeries = new XYChart.Series();
            readMasterLogPosSeries.setName(v1.getDisplayName());

            List<XYChart.Series<Number, Number>> listSeries = new ArrayList<>();
            listSeries.add(execMasterLogPosSeries);
            listSeries.add(readMasterLogPosSeries);

            Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
            seriesColors.put(execMasterLogPosSeries, v0.getDisplayColor());
            seriesColors.put(readMasterLogPosSeries, v1.getDisplayColor());

            replicationSyncChartPane.init(listSeries, seriesColors);
            
            HBox.setHgrow(chartPane, Priority.ALWAYS);
        }
        
        {
            GSChart gsChart = chartService.get(27l);
            GSChartVariable v0 = gsChart.getVariableByName("Relay_Log_Space");

            relayLogSpaceChartPane = loadChartControllers(gsChart, false);
            AnchorPane chartPane = relayLogSpaceChartPane.getUI();
            chartPane.setMaxWidth(Double.MAX_VALUE);
            relayLogSpaceChartBox.getChildren().add(chartPane);

            relayLogSpaceSeries = new XYChart.Series();
            relayLogSpaceSeries.setName(v0.getDisplayName());
            
            List<XYChart.Series<Number, Number>> listSeries = new ArrayList<>();
            listSeries.add(relayLogSpaceSeries);

            Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
            seriesColors.put(relayLogSpaceSeries, v0.getDisplayColor());

            relayLogSpaceChartPane.init(listSeries, seriesColors);
            
            HBox.setHgrow(chartPane, Priority.ALWAYS);
        }
        
        {
            GSChart gsChart = chartService.get(28l);
            GSChartVariable v0 = gsChart.getVariableByName("Relay_Log_Space");

            relayLogWrittenChartPane = loadBarChartControllers(gsChart);
            AnchorPane chartPane = relayLogWrittenChartPane.getUI();
            chartPane.setMaxWidth(Double.MAX_VALUE);
            relayLogWrittenChartBox.getChildren().add(chartPane);

            relayLogWrittenSeries = new XYChart.Series();
            relayLogWrittenSeries.setName(v0.getDisplayName());
            
            List<XYChart.Series<String, Number>> listSeries = new ArrayList<>();
            listSeries.add(relayLogWrittenSeries);

            Map<XYChart.Series<String, Number>, String> seriesColors = new HashMap<>();
            seriesColors.put(relayLogWrittenSeries, v0.getDisplayColor());

            relayLogWrittenChartPane.init(listSeries, seriesColors);
            
            HBox.setHgrow(chartPane, Priority.ALWAYS);
        }
    }
    
    public void injectServiceEntities(ProfilerChartService _chartService, ReadOnlyObjectProperty<Database> _databaseSelectedItemProperty,
            ProfilerDataManager _dbProfilerService, PreferenceDataService _preferenceService, DeadlockService _deadlockService, 
            QARecommendationService _qars, ColumnCardinalityService _columnCardinalityService, QAPerformanceTuningService _performanceTuningService,
            ProfilerSettingsService _settingsService, ConnectionParamService _paramsService, DBDebuggerProfilerService _debuggerProfilerService){
        chartService = _chartService;
        databaseSelectedItemProperty = _databaseSelectedItemProperty;
        profilerService = _dbProfilerService;
        preferenceService = _preferenceService;
        deadlockService = _deadlockService;
        qars = _qars;
        columnCardinalityService = _columnCardinalityService;
        performanceTuningService = _performanceTuningService;
        service = getController().getSelectedConnectionSession().get().getService();
        settingsService = _settingsService;
        paramsService = _paramsService;
        debuggerProfilerService = _debuggerProfilerService;
    }
    
    public void postInitialize(){
        
        initDeadlockCharts();        
        initReplicationCharts();
        
        initSqlSpeedometerPane();        
        initAnalyzerQueryTable();
        initConfigurationDebuggerTable();
        
        initInnoDBStatus();
        initMyISAMStatus();
        
        initBufferPoolTables();
        initRowOperationsTables();        
        initOpenTablesTable();        

        initRunningQueriesTable(runningQueriesTable);
        initInnoTopTable(innoTopTable);        
        initLockMonitorTable(lockMonitorTable);
        initDeadlockMonitorQueriesTable(deadlockMonitorTable.getTableView());
        
        initErrorLogTable(errorLogTable);
        
        String updateTime = preferenceService.getPreference().getDebuggerRefreshInterval();
        rqUpdateStepField.setText(updateTime);
        itUpdateStepField.setText(updateTime);
        dmUpdateStepField.setText(updateTime);
        lmUpdateStepField.setText(updateTime);
    }
    
    
    private static final String ERROR_LOG__1_DAY = "1 day log";
    private static final String ERROR_LOG__2_DAY = "2 day log";
    private static final String ERROR_LOG__3_DAY = "3 day log";
    private static final String ERROR_LOG__4_DAY = "4 day log";
    private static final String ERROR_LOG__5_DAY = "5 day log";
    private static final String ERROR_LOG__6_DAY = "6 day log";
    private static final String ERROR_LOG__7_DAY = "7 day log";
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        displayOnlyErrors.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            displayOnlyErrors(newValue);
        });
        
        dayLogComboBox.getItems().addAll(new String[] {"", ERROR_LOG__1_DAY, ERROR_LOG__2_DAY, ERROR_LOG__3_DAY, ERROR_LOG__4_DAY, ERROR_LOG__5_DAY, ERROR_LOG__6_DAY, ERROR_LOG__7_DAY});
        dayLogComboBox.getSelectionModel().selectFirst();
        
        dayLogComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null && !newValue.isEmpty()) {
                displayOnlyErrors.setDisable(true);
                displayOnlyErrors.setSelected(true);
            } else {
                displayOnlyErrors.setDisable(false);
                displayOnlyErrors.setSelected(false);
            }
            
            elRefreshAction(null);
        });
        
        errorLogButtonsContainer.getChildren().add(errorLogTable.removePaginator());
        
        errorLogTable.setRowsPerPage(100);
        errorLogTable.getTableView().getTextProperty().bind(elFilterField.textProperty());
        errorLogTable.getTableView().getRegexStageProperty().bind(elRegexCheckBox.selectedProperty());
        errorLogTable.getTableView().getMatchStateProperty().bind(elNotMatchCheckBox.selectedProperty());

        errorLogTable.getTableView().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(errorLogTable.getTableView().getTable(), event);
        });
                
        //notValidLabel.setStyle("-fx-font-size: 22");
        rqKillButton.disableProperty().bind(Bindings.isNull(runningQueriesTable.getTable().getSelectionModel().selectedItemProperty()));
        rqKillAllButton.disableProperty().bind(Bindings.isEmpty(runningQueriesTable.getTable().getItems()));

        lmKillButton.disableProperty().bind(Bindings.isNull(lockMonitorTable.getTable().getSelectionModel().selectedItemProperty()));
        lmKillAllButton.disableProperty().bind(Bindings.isEmpty(lockMonitorTable.getTable().getItems()));
        
        itKillButton.disableProperty().bind(Bindings.isNull(innoTopTable.getSelectionModel().selectedItemProperty()));
        itKillAllButton.disableProperty().bind(Bindings.isEmpty(innoTopTable.getItems()));

        dmPasteButton.disableProperty().bind(dmPauseButton.disableProperty().not());

        runningQueriesTable.getTextProperty().bind(rqFilterField.textProperty());
        runningQueriesTable.getRegexStageProperty().bind(rqRegexCheckBox.selectedProperty());
        runningQueriesTable.getMatchStateProperty().bind(rqNotMatchCheckBox.selectedProperty());

        runningQueriesTable.getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(runningQueriesTable.getTable(), event);
        });

        rqUpdateStepField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null && !newValue.trim().isEmpty()) {
                try {
                    rqWaitInSec = Integer.valueOf(newValue.trim());
                } catch (Throwable th) {
                    rqUpdateStepField.setText(String.valueOf(rqWaitInSec));
                }
            }
        });
        
        itFilterField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            innoTopTable.setPredicate(this::acceptFilter);
        });
        
        itRegexCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            innoTopTable.setPredicate(this::acceptFilter);
        });
        
        itNotMatchCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            innoTopTable.setPredicate(this::acceptFilter);
        });
        
        innoTopTable.getTableView().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(innoTopTable.getTableView(), event);
        });

        itUpdateStepField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null && !newValue.trim().isEmpty()) {
                try {
                    itWaitInSec = Integer.valueOf(newValue.trim());
                } catch (Throwable th) {
                    itUpdateStepField.setText(String.valueOf(itWaitInSec));
                }
            }
        });

        lockMonitorTable.getTextProperty().bind(lmFilterField.textProperty());
        lockMonitorTable.getRegexStageProperty().bind(lmRegexCheckBox.selectedProperty());
        lockMonitorTable.getMatchStateProperty().bind(lmNotMatchCheckBox.selectedProperty());

        lockMonitorTable.getTable().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(lockMonitorTable.getTable(), event);
        });

        lmUpdateStepField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null && !newValue.trim().isEmpty()) {
                try {
                    lmWaitInSec = Integer.valueOf(newValue.trim());
                } catch (Throwable th) {
                    lmUpdateStepField.setText(String.valueOf(lmWaitInSec));
                }
            }
        });

        deadlockMonitorTable.setRowsPerPage(25);
        deadlockMonitorTable.getTableView().getTextProperty().bind(dmFilterField.textProperty());
        deadlockMonitorTable.getTableView().getRegexStageProperty().bind(dmRegexCheckBox.selectedProperty());
        deadlockMonitorTable.getTableView().getMatchStateProperty().bind(dmNotMatchCheckBox.selectedProperty());

        deadlockMonitorTable.getTableView().addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            copyData(deadlockMonitorTable.getTableView().getTable(), event);
        });

        dmUpdateStepField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null && !newValue.trim().isEmpty()) {
                try {
                    dmWaitInSec = Integer.valueOf(newValue.trim());
                } catch (Throwable th) {
                    dmUpdateStepField.setText(String.valueOf(dmWaitInSec));
                }
            }
        });

        rqTimeCombobox.getItems().addAll(">", "<", "=");
        rqTimeCombobox.getSelectionModel().select(0);
        rqTimeCombobox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                rqTimeGreater0Button.setSelected(true);
            }
        });

        rqTimeField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                rqTimeGreater0Button.setSelected(true);
            }
        });

        rqTimeField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            rqTimeGreater0Button.setSelected(true);
        });

        EventHandler rqHandler = (EventHandler) (Event event) -> {
            if (rqAllButton.isSelected()) {
                rqLastButton = rqAllButton;

            } else if (rqTimeGreater0Button.isSelected()) {
                rqLastButton = rqTimeGreater0Button;

            } else if (rqAvoidSleepButton.isSelected()) {
                rqLastButton = rqAvoidSleepButton;

            } else {
                rqLastButton.setSelected(true);
            }

            rqRefreshAction(null);
        };

        EventHandler dmHandler = (EventHandler) (Event event) -> {
            if (dmAllButton.isSelected()) {
                deadlockMonitorTable.getTableView().setCustomFilter(null);
                dmLastButton = dmAllButton;

            } else if (dmTodayButton.isSelected()) {
                deadlockMonitorTable.getTableView().setCustomFilter((FilterableData t) -> {

                    Calendar c = GregorianCalendar.getInstance();
                    c.setTime(new Date());

                    c.set(Calendar.HOUR_OF_DAY, 0);
                    c.set(Calendar.MINUTE, 0);
                    c.set(Calendar.SECOND, 0);
                    c.set(Calendar.MILLISECOND, 0);

                    try {
                        Date date = DEADLOCK_DATE_FORMATER.parse(((DeadlockMonitorQueryData) t).getDeadlockTime());

                        return c.getTime().before(date);
                    } catch (ParseException ex) {
                        log.error("Error", ex);
                    }

                    return true;
                });
                dmLastButton = dmTodayButton;

            } else if (dmOneHourButton.isSelected()) {
                deadlockMonitorTable.getTableView().setCustomFilter((FilterableData t) -> {

                    Calendar c = GregorianCalendar.getInstance();
                    c.setTime(new Date());

                    c.add(Calendar.HOUR_OF_DAY, -1);

                    try {
                        Date date = DEADLOCK_DATE_FORMATER.parse(((DeadlockMonitorQueryData) t).getDeadlockTime());

                        return c.getTime().before(date);
                    } catch (ParseException ex) {
                        log.error("Error", ex);
                    }

                    return true;
                });

                dmLastButton = dmOneHourButton;
            } else {
                dmLastButton.setSelected(true);
            }
        };

        lmTimeCombobox.getItems().addAll(">", "<", "=");
        lmTimeCombobox.getSelectionModel().select(0);
        lmTimeCombobox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                lmTimeGreater0Button.setSelected(true);
            }
        });

        lmTimeField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                lmTimeGreater0Button.setSelected(true);
            }
        });

        lmTimeField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            lmTimeGreater0Button.setSelected(true);
        });
        EventHandler lmHandler = (EventHandler) (Event event) -> {
            if (lmAllButton.isSelected()) {
                lmLastButton = lmAllButton;

            } else if (lmTimeGreater0Button.isSelected()) {
                lmLastButton = lmTimeGreater0Button;

            } else if (lmTXLockButton.isSelected()) {
                lmLastButton = lmTXLockButton;
            } else {
                lmLastButton.setSelected(true);
            }

            lmRefreshAction(null);
        };

        rqAllButton.setOnAction(rqHandler);
        rqTimeGreater0Button.setOnAction(rqHandler);
        rqAvoidSleepButton.setOnAction(rqHandler);
        dmAllButton.setOnAction(dmHandler);
        dmTodayButton.setOnAction(dmHandler);
        dmOneHourButton.setOnAction(dmHandler);
        lmAllButton.setOnAction(lmHandler);
        lmTimeGreater0Button.setOnAction(lmHandler);
        lmTXLockButton.setOnAction(lmHandler);

        GENERAL_STATES.put("After create", "This occurs when the thread creates a table (including internal temporary tables), at the end of the function that creates the table. This state is used even if the table could not be created due to some error.");
        GENERAL_STATES.put("Analyzing", "The thread is calculating a MyISAM table key distributions (for example, for ANALYZE TABLE).");
        GENERAL_STATES.put("checking permissions", "The thread is checking whether the server has the required privileges to execute the statement.");
        GENERAL_STATES.put("Checking table", "The thread is performing a table check operation.");
        GENERAL_STATES.put("cleaning up", "The thread has processed one command and is preparing to free memory and reset certain state variables.");
        GENERAL_STATES.put("closing tables", "The thread is flushing the changed table data to disk and closing the used tables. This should be a fast operation. If not, verify that you do not have a full disk and that the disk is not in very heavy use.");
        GENERAL_STATES.put("converting HEAP to MyISAM", "The thread is converting an internal temporary table from a MEMORY table to an on-disk MyISAM table.");
        GENERAL_STATES.put("copy to tmp table", "The thread is processing an ALTER TABLE statement. This state occurs after the table with the new structure has been created but before rows are copied into it.\n"
                + "For a thread in this state, the Performance Schema can be used to obtain about the progress of the copy operation. See Section 25.11.5, “Performance Schema Stage Event Tables”.");
        GENERAL_STATES.put("Copying to group table", "If a statement has different ORDER BY and GROUP BY criteria, the rows are sorted by group and copied to a temporary table.");
        GENERAL_STATES.put("Copying to tmp table", "The server is copying to a temporary table in memory.");
        GENERAL_STATES.put("altering table", "The server is in the process of executing an in-place ALTER TABLE.");
        GENERAL_STATES.put("Copying to tmp table on disk", "The server is copying to a temporary table on disk. The temporary result set has become too large (see Section 8.4.4, “Internal Temporary Table Use in MySQL”). Consequently, the thread is changing the temporary table from in-memory to disk-based format to save memory.");
        GENERAL_STATES.put("Creating index", "The thread is processing ALTER TABLE ... ENABLE KEYS for a MyISAM table.");
        GENERAL_STATES.put("Creating sort index", "The thread is processing a SELECT that is resolved using an internal temporary table.");
        GENERAL_STATES.put("creating table", "The thread is creating a table. This includes creation of temporary tables.");
        GENERAL_STATES.put("Creating tmp table", "The thread is creating a temporary table in memory or on disk. If the table is created in memory but later is converted to an on-disk table, the state during that operation will be Copying to tmp table on disk.");
        GENERAL_STATES.put("committing alter table to storage engine", "The server has finished an in-place ALTER TABLE and is committing the result.");
        GENERAL_STATES.put("deleting from main table", "The server is executing the first part of a multiple-table delete. It is deleting only from the first table, and saving columns and offsets to be used for deleting from the other (reference) tables.");
        GENERAL_STATES.put("deleting from reference tables", "The server is executing the second part of a multiple-table delete and deleting the matched rows from the other tables.");
        GENERAL_STATES.put("discard_or_import_tablespace", "The thread is processing an ALTER TABLE ... DISCARD TABLESPACE or ALTER TABLE ... IMPORT TABLESPACE statement.");
        GENERAL_STATES.put("end", "This occurs at the end but before the cleanup of ALTER TABLE, CREATE VIEW, DELETE, INSERT, SELECT, or UPDATE statements.");
        GENERAL_STATES.put("executing", "The thread has begun executing a statement.");
        GENERAL_STATES.put("Execution of init_command", "The thread is executing statements in the value of the init_command system variable.");
        GENERAL_STATES.put("freeing items", "The thread has executed a command. Some freeing of items done during this state involves the query cache. This state is usually followed by cleaning up.");
        GENERAL_STATES.put("FULLTEXT initialization", "The server is preparing to perform a natural-language full-text search.");
        GENERAL_STATES.put("init", "This occurs before the initialization of ALTER TABLE, DELETE, INSERT, SELECT, or UPDATE statements. Actions taken by the server in this state include flushing the binary log, the InnoDB log, and some query cache cleanup operations.\n"
                + "For the end state, the following operations could be happening:\n"
                + "• Removing query cache entries after data in a table is changed\n"
                + "• Writing an event to the binary log\n"
                + "• Freeing memory buffers, including for blobs");
        GENERAL_STATES.put("Killed", "Someone has sent a KILL statement to the thread and it should abort next time it checks the kill flag. The flag is checked in each major loop in MySQL, but in some cases it might still take a short time for the thread to die. If the thread is locked by some other thread, the kill takes effect as soon as the other thread releases its lock.");
        GENERAL_STATES.put("logging slow query", "The thread is writing a statement to the slow-query log.");
        GENERAL_STATES.put("login", "The initial state for a connection thread until the client has been authenticated successfully.");
        GENERAL_STATES.put("manage keys", "The server is enabling or disabling a table index.");
        GENERAL_STATES.put("NULL", "This state is used for the SHOW PROCESSLIST state.");
        GENERAL_STATES.put("Opening tables", "The thread is trying to open a table. This is should be very fast procedure, unless something prevents opening. For example, an ALTER TABLE or a LOCK TABLE statement can prevent opening a table until the statement is finished. It is also worth checking that your table_open_cache value is large enough.");
        GENERAL_STATES.put("optimizing", "The server is performing initial optimizations for a query.");
        GENERAL_STATES.put("preparing", "This state occurs during query optimization.");
        GENERAL_STATES.put("Purging old relay logs", "The thread is removing unneeded relay log files.");
        GENERAL_STATES.put("query end", "This state occurs after processing a query but before the freeing items state.");
        GENERAL_STATES.put("Receiving from client", "he server is reading a packet from the client. This state is called Reading from net prior to MySQL 5.7.8.");
        GENERAL_STATES.put("Removing duplicates", "The query was using SELECT DISTINCT in such a way that MySQL could not optimize away the distinct operation at an early stage. Because of this, MySQL requires an extra stage to remove all duplicated rows before sending the result to the client.");
        GENERAL_STATES.put("removing tmp table", "The thread is removing an internal temporary table after processing a SELECT statement. This state is not used if no temporary table was created.");
        GENERAL_STATES.put("rename", "The thread is renaming a table.");
        GENERAL_STATES.put("rename result table", "The thread is processing an ALTER TABLE statement, has created the new table, and is renaming it to replace the original table.");
        GENERAL_STATES.put("Reopen tables", "The thread got a lock for the table, but noticed after getting the lock that the underlying table structure changed. It has freed the lock, closed the table, and is trying to reopen it.");
        GENERAL_STATES.put("Repair by sorting", "The repair code is using a sort to create indexes.");
        GENERAL_STATES.put("preparing for alter table", "The server is preparing to execute an in-place ALTER TABLE.");
        GENERAL_STATES.put("Repair done", "The thread has completed a multi-threaded repair for a MyISAM table.");
        GENERAL_STATES.put("Repair with keycache", "The repair code is using creating keys one by one through the key cache. This is much slower than Repair by sorting.");
        GENERAL_STATES.put("Rolling back", "The thread is rolling back a transaction.");
        GENERAL_STATES.put("Saving state", "For MyISAM table operations such as repair or analysis, the thread is saving the new table state to the .MYI file header. State includes information such as number of rows, the AUTO_INCREMENT counter, and key distributions.");
        GENERAL_STATES.put("Searching rows for update", "The thread is doing a first phase to find all matching rows before updating them. This has to be done if the UPDATE is changing the index that is used to find the involved rows.");
        GENERAL_STATES.put("Sending data", "The thread is reading and processing rows for a SELECT statement, and sending data to the client. Because operations occurring during this state tend to perform large amounts of disk access (reads), it is often the longest-running state over the lifetime of a given query.");
        GENERAL_STATES.put("Sending to client", "The server is writing a packet to the client. This state is called Writing to net prior to MySQL 5.7.8.");
        GENERAL_STATES.put("setup", "The thread is beginning an ALTER TABLE operation.");
        GENERAL_STATES.put("Sorting for group", "The thread is doing a sort to satisfy a GROUP BY.");
        GENERAL_STATES.put("Sorting for order", "The thread is doing a sort to satisfy an ORDER BY.");
        GENERAL_STATES.put("Sorting index", "The thread is sorting index pages for more efficient access during a MyISAM table optimization operation.");
        GENERAL_STATES.put("Sorting result", "For a SELECT statement, this is similar to Creating sort index, but for nontemporary tables.");
        GENERAL_STATES.put("statistics", "The server is calculating statistics to develop a query execution plan. If a thread is in this state for a long time, the server is probably disk-bound performing other work.");
        GENERAL_STATES.put("System lock", "The thread has called mysql_lock_tables() and the thread state has not been updated since. This is a very general state that can occur for many reasons.\n"
                + "For example, the thread is going to request or is waiting for an internal or external system lock for the table. This can occur when InnoDB waits for a table-level lock during execution of LOCK TABLES. If this state is being caused by requests for external locks and you are not using multiple mysqld servers that are accessing the same MyISAM tables, you can disable external system locks with the --skip-external-locking option. However, external locking is disabled by default, so it is likely that this option will have no effect. For SHOW PROFILE, this state means the thread is requesting the lock (not waiting for it).");
        GENERAL_STATES.put("update", "The thread is getting ready to start updating the table.");
        GENERAL_STATES.put("Updating", "The thread is searching for rows to update and is updating them.");
        GENERAL_STATES.put("updating main table", "The server is executing the first part of a multiple-table update. It is updating only the first table, and saving columns and offsets to be used for updating the other (reference) tables.");
        GENERAL_STATES.put("updating reference tables", "The server is executing the second part of a multiple-table update and updating the matched rows from the other tables.");
        GENERAL_STATES.put("User lock", "The thread is going to request or is waiting for an advisory lock requested with a GET_LOCK() call. For SHOW PROFILE, this state means the thread is requesting the lock (not waiting for it).");
        GENERAL_STATES.put("User sleep", "The thread has invoked a SLEEP() call.");
        GENERAL_STATES.put("Waiting for commit lock", "FLUSH TABLES WITH READ LOCK is waiting for a commit lock.");
        GENERAL_STATES.put("Waiting for global read lock", "FLUSH TABLES WITH READ LOCK is waiting for a global read lock or the global read_only system variable is being set.");
        GENERAL_STATES.put("Waiting for tables", "The thread got a notification that the underlying structure for a table has changed and it needs to reopen the table to get the new structure. However, to reopen the table, it must wait until all other threads have closed the table in question.\n"
                + "This notification takes place if another thread has used FLUSH TABLES or one of the following statements on the table in question: FLUSH TABLES tbl_name, ALTER TABLE, RENAME TABLE, REPAIR TABLE, ANALYZE TABLE, or OPTIMIZE TABLE.");
        GENERAL_STATES.put("Waiting for table flush", "The thread is executing FLUSH TABLES and is waiting for all threads to close their tables, or the thread got a notification that the underlying structure for a table has changed and it needs to reopen the table to get the new structure. However, to reopen the table, it must wait until all other threads have closed the table in question.\n"
                + "This notification takes place if another thread has used FLUSH TABLES or one of the following statements on the table in question: FLUSH TABLES tbl_name, ALTER TABLE, RENAME TABLE, REPAIR TABLE, ANALYZE TABLE, or OPTIMIZE TABLE.");
        GENERAL_STATES.put("Waiting for lock_type lock", "The server is waiting to acquire a THR_LOCK lock or a lock from the metadata locking subsystem, where lock_type indicates the type of lock.\n"
                + "This state indicates a wait for a THR_LOCK:\n"
                + "• Waiting for table level lock\n"
                + "These states indicate a wait for a metadata lock:\n"
                + "• Waiting for event metadata lock\n"
                + "• Waiting for global read lock\n"
                + "• Waiting for schema metadata lock\n"
                + "• Waiting for stored function metadata lock\n"
                + "• Waiting for stored procedure metadata lock\n"
                + "• Waiting for table metadata lock\n"
                + "• Waiting for trigger metadata lock\n"
                + "For information about table lock indicators, see Section 8.11.1, “Internal Locking Methods”. For information about metadata locking, see Section 8.11.4, “Metadata Locking”. To see which locks are blocking lock requests, use the Performance Schema lock tables described at Section 25.11.12, “Performance Schema Lock Tables”.");
        GENERAL_STATES.put("Waiting on cond", "A generic state in which the thread is waiting for a condition to become true. No specific state information is available.");
        GENERAL_STATES.put("Writing to net", "The server is writing a packet to the network. This state is called Sending to client as of MySQL 5.7.8.");

        GENERAL_COMMANDS.put("Binlog Dump", "This is a thread on a master server for sending binary log contents to a slave server.");
        GENERAL_COMMANDS.put("Change user", "The thread is executing a change-user operation.");
        GENERAL_COMMANDS.put("Close stmt", "The thread is closing a prepared statement.");
        GENERAL_COMMANDS.put("Connect", "A replication slave is connected to its master.");
        GENERAL_COMMANDS.put("Connect Out", "A replication slave is connecting to its master.");
        GENERAL_COMMANDS.put("Create DB", "The thread is executing a create-database operation.");
        GENERAL_COMMANDS.put("Daemon", "This thread is internal to the server, not a thread that services a client connection.");
        GENERAL_COMMANDS.put("Debug", "The thread is generating debugging information.");
        GENERAL_COMMANDS.put("Delayed insert", "The thread is a delayed-insert handler.");
        GENERAL_COMMANDS.put("Drop DB", "The thread is executing a drop-database operation.");
        GENERAL_COMMANDS.put("Execute", "The thread is executing a prepared statement.");
        GENERAL_COMMANDS.put("Fetch", "The thread is fetching the results from executing a prepared statement.");
        GENERAL_COMMANDS.put("Field List", "The thread is retrieving information for table columns.");
        GENERAL_COMMANDS.put("Init DB", "The thread is selecting a default database.");
        GENERAL_COMMANDS.put("Kill", "The thread is killing another thread.");
        GENERAL_COMMANDS.put("Long Data", "The thread is retrieving long data in the result of executing a prepared statement.");
        GENERAL_COMMANDS.put("Ping", "The thread is handling a server-ping request.");
        GENERAL_COMMANDS.put("Prepare", "The thread is preparing a prepared statement.");
        GENERAL_COMMANDS.put("Processlist", "The thread is producing information about server threads.");
        GENERAL_COMMANDS.put("Query", "The thread is executing a statement.");
        GENERAL_COMMANDS.put("Quit", "The thread is terminating.");
        GENERAL_COMMANDS.put("Refresh", "The thread is flushing table, logs, or caches, or resetting status variable or replication server information.");
        GENERAL_COMMANDS.put("Register Slave", "The thread is registering a slave server.");
        GENERAL_COMMANDS.put("Reset stmt", "The thread is resetting a prepared statement.");
        GENERAL_COMMANDS.put("Set option", "The thread is setting or resetting a client statement-execution option.");
        GENERAL_COMMANDS.put("Shutdown", "The thread is shutting down the server.");
        GENERAL_COMMANDS.put("Sleep", "The thread is waiting for the client to send a new statement to it.");
        GENERAL_COMMANDS.put("Statistics", "The thread is producing server-status information.");
        GENERAL_COMMANDS.put("Table Dump", "The thread is sending table contents to a slave server.");
        GENERAL_COMMANDS.put("Time", "Unused.");

        bottomTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue == configurationDebuggerTab && !initConfigurationDebugger) {
                initConfigurationDebugger(null);
            } else if (newValue == runningQueriesTab) {
                rqForceRun = true;
            } else if (newValue == transactionsTab) {
                itForceRun = true;
            } else if (newValue == deadlockMonitorTab) {
                dmForceRun = true;
            } else if (newValue == lockMonitorTab) {
                lmForceRun = true;
            } else if (newValue == dbLoadTab) {
                speedForceRun = true;
            } else  if (newValue == autodetectTab) {
                forceAutodetect();
            } else  if (newValue == errorLogTab) {
                if (!errorLogInited) {
                    errorLogInited = true;
                    elRefreshAction(null);
                }
            }
        });
        
        bottomTabPane.getSelectionModel().clearSelection();
        bottomTabPane.getSelectionModel().select(autodetectTab);
        
        innoTopButtonsContainer.getChildren().add(innoTopTable.removePaginator());        
        
        rlUpdateStepField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null && !newValue.trim().isEmpty()) {
                try {
                    replWaitInSec = Integer.valueOf(newValue.trim());
                } catch (Throwable th) {
                    rlUpdateStepField.setText(String.valueOf(replWaitInSec));
                }
            }
        });
        
        // DB Connections
        {
        
            dbUpdateStepField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (newValue != null && !newValue.trim().isEmpty()) {
                    try {
                        dbWaitInSec = Integer.valueOf(newValue.trim());
                    } catch (Throwable th) {
                        dbUpdateStepField.setText(String.valueOf(dbWaitInSec));
                    }
                }
            });
        
            deadlockTopButtonsContainer.getChildren().add(deadlockMonitorTable.removePaginator());

            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__APPLICATIONS);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__USER_WITH_HOSTS);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__USER_WITH_DBS_HOSTS);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__USER_WITH_HOSTS_STATE);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__USERS);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__DATABASES);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__QUERY_STATE);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__USERS_DBS_QUERY_STATE);
            dbConnectionsComboBox.getItems().add(DB_CONNECTIONS__USERS_QUERY_STATE);

            dbConnectionsComboBox.getSelectionModel().select(0);
            
            dbConnectionsComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                dbForceRun = true;
            });

            nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
            connsColumn.setCellValueFactory(new PropertyValueFactory<>("conns"));
            activeConnsColumn.setCellValueFactory(new PropertyValueFactory<>("activeConns"));
            sleepConnsColumn.setCellValueFactory(new PropertyValueFactory<>("sleepConns"));
            
            activePercentColumn.setCellValueFactory(new PropertyValueFactory<>("activePercent"));
            activePercentColumn.setCellFactory(CustomProgreeBarTableCell.forTableColumn("red-bar"));
            
            sleepPercentColumn.setCellValueFactory(new PropertyValueFactory<>("sleepPercent"));
            sleepPercentColumn.setCellFactory(CustomProgreeBarTableCell.forTableColumn("red-bar"));
            
            percentColumn.setCellValueFactory(new PropertyValueFactory<>("percentage"));
            percentColumn.setCellFactory(CustomProgreeBarTableCell.forTableColumn("red-bar"));
        }
        
        // APPLY CSS
        addEffectToUI();
    }

    private TableView<SalveStatusData> replicationStatusTable;

    @FXML
    public void showReplicationStatus(ActionEvent event) {

        if (replicationStatusTable == null) {
            replicationStatusTable = new TableView<>();

            TableColumn<SalveStatusData, String> replicationStatusKeyColumn = new TableColumn<>("Key");
            TableColumn<SalveStatusData, String> replicationStatusValueColumn = new TableColumn<>("Value");

            replicationStatusKeyColumn.setCellValueFactory(new PropertyValueFactory<>("key"));
            replicationStatusKeyColumn.setCellFactory((TableColumn<SalveStatusData, String> param) -> {
                TableCell<SalveStatusData, String> cell = new TableCell<SalveStatusData, String>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {

                        super.updateItem(item, empty);
                        if (REPLICATION_STATUS_COLUMNS.contains(item)) {
                            setStyle("-fx-text-fill: blue; -fx-font-weight: bold");
                        } else {
                            setStyle("");
                        }

                        if (item != null) {
                            setText(item);
                        } else {
                            setText("");
                        }
                    }

                };

                return cell;
            });

            replicationStatusValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
            replicationStatusValueColumn.setCellFactory((TableColumn<SalveStatusData, String> param) -> {
                TableCell<SalveStatusData, String> cell = new TableCell<SalveStatusData, String>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {

                        super.updateItem(item, empty);

                        setStyle("");
                        if (item != null && getTableRow() != null) {

                            int row = getTableRow().getIndex();
                            if (row >= 0 && row < replicationStatusTable.getItems().size()) {
                                SalveStatusData ssd = replicationStatusTable.getItems().get(row);
                                switch (ssd.getKey()) {
                                    case Seconds_Behind_Master:
                                        String color = "green";
                                        try {
                                            int value = Integer.valueOf(item);
                                            if (value == 0) {
                                                color = "green";
                                            } else if (value <= 10) {
                                                color = "#B05A5A";
                                            } else if (value <= 100) {
                                                color = "#D52E2E";
                                            } else {
                                                color = "#FF0404";
                                            }
                                        } catch (Throwable th) {
                                        }

                                        setStyle("-fx-font-weight: bold; -fx-text-fill: " + color);
                                        break;

                                    case Slave_IO_Running:
                                    case Slave_SQL_Running:
                                        color = "blue";
                                        try {
                                            if ("NO".equals(item.toUpperCase())) {
                                                color = "red";
                                            } else {
                                                color = "green";
                                            }
                                        } catch (Throwable th) {
                                        }

                                        setStyle("-fx-font-weight: bold; -fx-background-color: " + color);
                                        break;

                                    case Last_IO_Errno:
                                    case Last_IO_Error:
                                    case Last_SQL_Errno:
                                    case Last_SQL_Error:
                                        color = "red";
                                        try {
                                            if (!"0".equals(item) && !item.isEmpty() && !"null".equalsIgnoreCase(item)) {
                                                color = "red";
                                            } else {
                                                color = "green";
                                            }
                                        } catch (Throwable th) {
                                        }

                                        setStyle("-fx-font-weight: bold; -fx-text-fill: " + color);
                                        break;
                                }
                            }

                            setText(item);
                        } else {
                            setText("");
                        }
                    }

                };

                return cell;
            });

            replicationStatusTable.getColumns().add(replicationStatusKeyColumn);
            replicationStatusTable.getColumns().add(replicationStatusValueColumn);

            replicationStatusTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            replicationStatusTable.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);
        }

        synchronized (this) {
            replicationStatusTable.getItems().clear();
            replicationStatusTable.getItems().addAll(salveStatusData);
        }

        Stage dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle("Replication Status");

        VBox vbox = new VBox(replicationStatusTable);
        replicationStatusTable.setMaxHeight(Double.MAX_VALUE);
        VBox.setVgrow(replicationStatusTable, Priority.ALWAYS);
        vbox.getStyleClass().addAll(Flags.METRIC_STYLE_CLASS, Flags.METRIC_INNERS_STYLE_CLASS);

        Scene scene = new Scene(vbox, 550, getController().getStage().getHeight());
        dialog.setScene(scene);

        scene.getStylesheets().addAll(mainUI.getStylesheets());
        dialog.showAndWait();
    }

    private boolean acceptFilter(DebugInnoTopData row) {
        String filterCondition = itFilterField.getText();
        if (!filterCondition.isEmpty()) {

            try {
                Pattern pattern = itRegexCheckBox.isSelected() ? Pattern.compile(filterCondition) : null;

                boolean result = false;

                for (String filterValue : row.getValuesFoFilter()) {

                    if (itRegexCheckBox.isSelected() && pattern != null) {
                        // if regex - need check using regex
                        result = pattern.matcher(filterValue).find();
                    } else {
                        // else just
                        result = filterValue.toLowerCase().contains(filterCondition.toLowerCase());
                    }

                    if (result) {
                        break;
                    }
                }

                if (itNotMatchCheckBox.isSelected()) {
                    result = !result;
                }

                return result;
            } catch (Throwable th) {
                // my be some errors in pattern compile - its ok just ignore and give user write full regex
            }
        }

        return true;
    }

    public void disableQueryUIs() {
        getController().hideApplicationFooter();
        getController().toggleLeftPane();
    }

    public void enableQueryUIs() {
        getController().toggleLeftPane();
        getController().showApplicationFooter();
    }

    @FXML
    private SplitPane applicationHostWisePane;
    @FXML
    private SplitPane bottomSplitPane;

    @FXML
    private ImageView minMaxGridImageView;
    @FXML
    private ImageView minMaxBarImageView;
    @FXML
    private ImageView minMaxPieImageView;

    private boolean maxMinGrid = false;
    private boolean maxMinBar = false;
    private boolean maxMinPie = false;

    @FXML
    public void maxMinGrid(MouseEvent event) {
        if (maxMinGrid) {
            minMaxGridImageView.setImage(StandardDefaultImageProvider.getInstance().getMaximum_image());
            applicationHostWisePane.setDividerPositions(0.5);
        } else {
            minMaxGridImageView.setImage(StandardDefaultImageProvider.getInstance().getMinimum_image());
            applicationHostWisePane.setDividerPositions(1.0);
        }

        maxMinGrid = !maxMinGrid;
    }

    @FXML
    public void maxMinPie(MouseEvent event) {
        if (maxMinPie) {
            minMaxPieImageView.setImage(StandardDefaultImageProvider.getInstance().getMaximum_image());
            applicationHostWisePane.setDividerPositions(0.5);
            bottomSplitPane.setDividerPositions(0.7);
        } else {
            minMaxPieImageView.setImage(StandardDefaultImageProvider.getInstance().getMinimum_image());
            applicationHostWisePane.setDividerPositions(0.0);
            bottomSplitPane.setDividerPositions(0.0);
        }

        maxMinPie = !maxMinPie;
    }

    @FXML
    public void maxMinBar(MouseEvent event) {
        if (maxMinBar) {
            minMaxBarImageView.setImage(StandardDefaultImageProvider.getInstance().getMaximum_image());
            applicationHostWisePane.setDividerPositions(0.5);
            bottomSplitPane.setDividerPositions(0.7);
        } else {
            minMaxBarImageView.setImage(StandardDefaultImageProvider.getInstance().getMinimum_image());
            applicationHostWisePane.setDividerPositions(0.0);
            bottomSplitPane.setDividerPositions(1.0);
        }

        maxMinBar = !maxMinBar;
    }

    @FXML
    private ImageView minMaxSpeedometerGridImageView;
    @FXML
    private SplitPane speedomenterSplitPane;
    @FXML
    private AnchorPane speedometerGridContainer;

    private boolean maxMinSpeedometerGrid = false;

    @FXML
    public void maxMinSpeedometerGrid(MouseEvent event) {
        if (maxMinSpeedometerGrid) {
            minMaxSpeedometerGridImageView.setImage(StandardDefaultImageProvider.getInstance().getMaximum_image());
            stackofgaugeContainer.getChildren().remove(speedometerGridContainer);
            speedomenterSplitPane.getItems().add(speedometerGridContainer);
            speedomenterSplitPane.setDividerPositions(0.8);
        } else {
            minMaxSpeedometerGridImageView.setImage(StandardDefaultImageProvider.getInstance().getMinimum_image());
            speedomenterSplitPane.getItems().remove(speedometerGridContainer);
            stackofgaugeContainer.getChildren().add(speedometerGridContainer);
        }

        maxMinSpeedometerGrid = !maxMinSpeedometerGrid;
    }

    @FXML
    public synchronized void initConfigurationDebugger(ActionEvent event) {
//        makeBusy(true);
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                if (tunnerUtil == null) {
                    tunnerUtil = new MySQLTunnerUtil(getController().getSelectedConnectionSession().get());
                }
                List<TunerResult> list = tunnerUtil.getRecommendations();
                Platform.runLater(() -> {
                    configurationDebuggerTable.removeItems(configurationDebuggerTable.getSourceData());
                    configurationDebuggerTable.addItems(list);
                    initConfigurationDebugger = true;
                    
                    int size = list.size();
                    if (!list.isEmpty()) {
                        configurationText.setText(String.valueOf(size));
                    }
                    
                    if (preferenceService.getPreference().isDebuggerAlertConfig()) {
                        if (size > 1) {
                            configurationButton.setId("debugerButton_red");
                            addBlink(configurationButton);
                        } else {
                            configurationButton.setId("debugerButton_default");
                            removeBlink(configurationButton);
                        }
                    } else {
                        configurationButton.setId("debugerButton_default");
                        removeBlink(configurationButton);
                    }
                    
//                    makeBusy(false);
                });
            }
        });
    }

    private void initInnoTopTable(PaginatorTableView<DebugInnoTopData> tableView) {

        final HashMap<Integer, String> rowStyleProperties = new HashMap(10);

        TableColumn idColumn = createColumn("Thread ID", "id");
        TableColumn userColumn = createColumn("User", "user");
        TableColumn statusColumn = createColumn("Status", "status");
        TableColumn timeColumn = createColumn("Time", "timeProvider");
        TableColumn undoLogColumn = createColumn("Undo Log", "undoLog");
        TableColumn lStructColumn = createColumn("Lock Struct", "lStruct");
        TableColumn stateColumn = createColumn("State", "state");
        TableColumn qTextColumn = createColumn("Query", "qText");

        for (TableColumn tc : Arrays.asList(idColumn, userColumn, stateColumn, statusColumn, undoLogColumn, lStructColumn)) {
            tc.setCellFactory(new Callback() {
                @Override
                public Object call(Object param) {
                    return new TableCell<DebugInnoTopData, Object>() {
                        @Override
                        protected void updateItem(Object item, boolean empty) {
                            super.updateItem(item, empty);
                            Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                            setText(empty ? "" : String.valueOf(item));
                            
                            if (getTableColumn() == statusColumn && item != null && !empty) {
                                if (DebugInnoTopData.INNO_TOP__UNDO.equals(item)) {
                                    setTooltip(new Tooltip("This UNDO DB transaction has been started but not committed. It is an ideal state and causes DB internal transaction lock issue with other threads. Recommend to kill it if it is more than 50 seconds ideal."));
                                    
                                } else if (DebugInnoTopData.INNO_TOP__ACTIVE.equals(item)) {
                                    setTooltip(new Tooltip("This Transaction is Active."));
                                    
                                } else if (DebugInnoTopData.INNO_TOP__NOT_STARTED.equals(item)) {
                                    setTooltip(new Tooltip("This Transaction is not started."));
                                    
                                } else {
                                    setTooltip(null);
                                }
                            } else {
                                setTooltip(null);
                            }
                        }
                    };
                }
            });
        }

        timeColumn.setCellFactory(new Callback<TableColumn<DebugInnoTopData, DebugInnoTopData>, TableCell<DebugInnoTopData, DebugInnoTopData>>() {
            @Override
            public TableCell<DebugInnoTopData, DebugInnoTopData> call(TableColumn<DebugInnoTopData, DebugInnoTopData> param) {
                return new DebuggerInnoTopTimeTableCell() {
                    {
                        styleProperty().addListener(new ChangeListener<String>() {
                            @Override
                            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                if (getIndex() < 0 || getItem() == null) {
                                    return;
                                }
                                rowStyleProperties.put(getIndex(), newValue);
                            }
                        });
                    }
                };
            }
        });

        qTextColumn.setCellFactory(new Callback<TableColumn<DebugInnoTopData, String>, TableCell<DebugInnoTopData, String>>() {
            @Override
            public TableCell<DebugInnoTopData, String> call(TableColumn<DebugInnoTopData, String> param) {
                return new QueryTableCell<DebugInnoTopData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Query Text")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn explainColumn = createColumn("Explain", "qText");
        explainColumn.setCellFactory(new Callback<TableColumn<DebugExplainProvider, String>, TableCell<DebugExplainProvider, String>>() {
            @Override
            public TableCell<DebugExplainProvider, String> call(TableColumn<DebugExplainProvider, String> param) {
                return new DebuggerExplainTableCell() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });        

        tableView.addTableColumn(idColumn, 0.05);
        tableView.addTableColumn(userColumn, 0.1);
        tableView.addTableColumn(statusColumn, 0.1);
        tableView.addTableColumn(timeColumn, 0.1);
        tableView.addTableColumn(undoLogColumn, 0.1);
        tableView.addTableColumn(lStructColumn, 0.1);
        tableView.addTableColumn(qTextColumn, 0.1, 350);
        tableView.addTableColumn(explainColumn, 0.1, 50);
        tableView.addTableColumn(stateColumn, 0.21);

        timeColumn.setSortType(TableColumn.SortType.DESCENDING);
        timeColumn.setComparator(new Comparator<DebugInnoTopData>() {
            @Override
            public int compare(DebugInnoTopData o1, DebugInnoTopData o2) {
                boolean notStarted1 = DebuggerInnoTopTimeTableCell.INNO_TOP__NOT_STARTED.equalsIgnoreCase(o1.getStatus());
                boolean notStarted2 = DebuggerInnoTopTimeTableCell.INNO_TOP__NOT_STARTED.equalsIgnoreCase(o2.getStatus());
                
                String[] s1 = o1.getTime().split(":");
                String[] s2 = o2.getTime().split(":");
                Long time1 = 0l;
                if (s1.length > 2) {
                    time1 = Long.valueOf(s1[0]) * 3600 + Long.valueOf(s1[1]) * 60 + Long.valueOf(s1[2]);
                }

                Long time2 = 0l;
                if (s2.length > 2) {
                    time2 = Long.valueOf(s2[0]) * 3600 + Long.valueOf(s2[1]) * 60 + Long.valueOf(s2[2]);
                }

                if (notStarted1 && notStarted2) {
                    return (int) (time1 - time2);
                } else if (notStarted1) {
                    return -1;
                } else if (notStarted2) {
                    return 1;
                }

                return (int) (time1 - time2);
            }
        });
        tableView.addColumnToSortOrder(timeColumn);

        tableView.getTableView().getSelectionModel().setCellSelectionEnabled(true);
    }

    private void initRunningQueriesTable(FilterableTableView tableView) {

        final HashMap<Integer, String> rowStyleProperties = new HashMap(10);

        TableColumn idColumn = createColumn("Id", "id");
        TableColumn userColumn = createColumn("User", "user");
        TableColumn hostColumn = createColumn("Host", "host");
        TableColumn databaseColumn = createColumn("Database", "database");

        for (TableColumn tc : Arrays.asList(idColumn, userColumn, databaseColumn, hostColumn)) {
            tc.setCellFactory(new Callback() {
                @Override
                public Object call(Object param) {
                    return new TableCell<DebugRunningQueryData, Object>() {
                        @Override
                        protected void updateItem(Object item, boolean empty) {
                            super.updateItem(item, empty);
                            Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                            setText(empty ? "" : String.valueOf(item));
                        }
                    };
                }
            });
        }

        TableColumn commandColumn = createColumn("Command", "command");
        commandColumn.setCellFactory(column -> {

            return new TableCell<DebugRunningQueryData, String>() {

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    String tooltipText = GENERAL_COMMANDS.get(item);
                    if (tooltipText != null) {
                        setTooltip(new Tooltip(tooltipText));
                    } else {
                        setTooltip(null);
                    }
                }
            };
        });

        TableColumn stateColumn = createColumn("State", "state");
        stateColumn.setCellFactory(column -> {

            return new TableCell<DebugRunningQueryData, String>() {

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    String tooltipText = GENERAL_STATES.get(item);
                    if (tooltipText != null) {
                        setTooltip(new Tooltip(tooltipText));
                    } else {
                        setTooltip(null);
                    }
                }
            };
        });

        TableColumn timeColumn = createColumn("Time", "timeProvider");
        timeColumn.setCellFactory(new Callback<TableColumn<DebugTimeProvider, DebugTimeProvider>, TableCell<DebugTimeProvider, DebugTimeProvider>>() {
            @Override
            public TableCell<DebugTimeProvider, DebugTimeProvider> call(TableColumn<DebugTimeProvider, DebugTimeProvider> param) {
                return new DebuggerTimeTableCell() {
                    {
                        styleProperty().addListener(new ChangeListener<String>() {
                            @Override
                            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                if (getIndex() < 0 || getItem() == null) {
                                    return;
                                }
                                rowStyleProperties.put(getIndex(), newValue);
                            }
                        });
                    }
                };
            }
        });

        TableColumn queryColumn = createColumn("Query", "query");
        queryColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<DebugRunningQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn explainColumn = createColumn("Explain", "query");
        explainColumn.setCellFactory(new Callback<TableColumn<DebugExplainProvider, String>, TableCell<DebugExplainProvider, String>>() {
            @Override
            public TableCell<DebugExplainProvider, String> call(TableColumn<DebugExplainProvider, String> param) {
                return new DebuggerExplainTableCell() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        tableView.addTableColumn(idColumn, 0.05);
        tableView.addTableColumn(userColumn, 0.1);
        tableView.addTableColumn(hostColumn, 0.1);
        tableView.addTableColumn(databaseColumn, 0.1);
        tableView.addTableColumn(commandColumn, 0.1);
        tableView.addTableColumn(timeColumn, 0.1);
        tableView.addTableColumn(stateColumn, 0.1);
        tableView.addTableColumn(queryColumn, 0.21);
        tableView.addTableColumn(explainColumn, 0.1, 50);

        timeColumn.setSortType(TableColumn.SortType.DESCENDING);
        timeColumn.setComparator(new Comparator<DebugRunningQueryData>() {
            @Override
            public int compare(DebugRunningQueryData o1, DebugRunningQueryData o2) {

                boolean sleep1 = "sleep".equalsIgnoreCase(o1.getState());
                boolean sleep2 = "sleep".equalsIgnoreCase(o2.getState());
                if (sleep1 && sleep2) {
                    return (int) (o1.getTime().doubleValue() - o2.getTime().doubleValue());
                } else if (sleep1) {
                    return -1;
                } else if (sleep2) {
                    return 1;
                }

                return (int) (o1.getTime().doubleValue() - o2.getTime().doubleValue());
            }
        });
        tableView.addColumnToSortOrder(timeColumn);

        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }

    private void initLockMonitorTable(FilterableTableView tableView) {

        final HashMap<Integer, String> rowStyleProperties = new HashMap(10);

        TableColumn idColumn = createColumn("Id", "id");
        TableColumn userColumn = createColumn("User", "user");
        TableColumn hostColumn = createColumn("Host", "host");
        TableColumn databaseColumn = createColumn("Database", "database");

        TableColumn commandColumn = createColumn("Command", "command");
        commandColumn.setCellFactory(column -> {

            return new TableCell<DebugRunningQueryData, String>() {

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    String tooltipText = GENERAL_COMMANDS.get(item);
                    if (tooltipText != null) {
                        setTooltip(new Tooltip(tooltipText));
                    } else {
                        setTooltip(null);
                    }
                }
            };
        });

        TableColumn stateColumn = createColumn("Wait state", "state");
        stateColumn.setCellFactory(column -> {

            return new TableCell<DebugRunningQueryData, String>() {

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    String tooltipText = GENERAL_STATES.get(item);
                    if (tooltipText != null) {
                        setTooltip(new Tooltip(tooltipText));
                    } else {
                        setTooltip(null);
                    }
                }
            };
        });

        TableColumn timeColumn = createColumn("Wait time", "timeProvider");
        timeColumn.setCellFactory(new Callback<TableColumn<DebugTimeProvider, DebugTimeProvider>, TableCell<DebugTimeProvider, DebugTimeProvider>>() {
            @Override
            public TableCell<DebugTimeProvider, DebugTimeProvider> call(TableColumn<DebugTimeProvider, DebugTimeProvider> param) {
                return new DebuggerTimeTableCell() {
                    {
                        styleProperty().addListener(new ChangeListener<String>() {
                            @Override
                            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                if (getIndex() < 0 || getItem() == null) {
                                    return;
                                }
                                rowStyleProperties.put(getIndex(), newValue);
                            }
                        });
                    }
                };
            }
        });

        TableColumn queryColumn = createColumn("Waiting query", "query");
        queryColumn.setCellFactory(new Callback<TableColumn<DebugLockMonitorData, String>, TableCell<DebugLockMonitorData, String>>() {
            @Override
            public TableCell<DebugLockMonitorData, String> call(TableColumn<DebugLockMonitorData, String> param) {
                return new QueryTableCell<DebugLockMonitorData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn waitingTableLockColumn = createColumn("Waiting table lock", "waitingTableLock");
        waitingTableLockColumn.setCellFactory(new Callback<TableColumn<DebugLockMonitorData, String>, TableCell<DebugLockMonitorData, String>>() {
            @Override
            public TableCell<DebugLockMonitorData, String> call(TableColumn<DebugLockMonitorData, String> param) {
                return new QueryTableCell<DebugLockMonitorData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn blockingQueryColumn = createColumn("Blocking query", "blockingQuery");
        blockingQueryColumn.setCellFactory(new Callback<TableColumn<DebugLockMonitorData, String>, TableCell<DebugLockMonitorData, String>>() {
            @Override
            public TableCell<DebugLockMonitorData, String> call(TableColumn<DebugLockMonitorData, String> param) {
                return new QueryTableCell<DebugLockMonitorData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn blockingTrxIdColumn = createColumn("Blocking trx id", "blockingTrxId");
        TableColumn blockingThreadColumn = createColumn("Blocking thread", "blockingThread");
        TableColumn blockingHostColumn = createColumn("Blocking host", "blockingHost");
        TableColumn idleInTrxColumn = createColumn("Idle in trx", "idleInTrx");
        TableColumn lockTypeColumn = createColumn("Lock type", "lockType");

        for (TableColumn tc : Arrays.asList(idColumn, userColumn, databaseColumn, hostColumn, blockingThreadColumn, blockingTrxIdColumn, blockingHostColumn, idleInTrxColumn, lockTypeColumn)) {
            tc.setCellFactory(new Callback() {
                @Override
                public Object call(Object param) {
                    return new TableCell<DebugRunningQueryData, Object>() {
                        @Override
                        protected void updateItem(Object item, boolean empty) {
                            super.updateItem(item, empty);
                            Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                            setText(empty ? "" : String.valueOf(item));
                        }
                    };
                }
            });
        }

        TableColumn lockTypeDescColumn = createColumn("Lock type desc", "lockTypeDesc");
        lockTypeDescColumn.setCellFactory(new Callback<TableColumn<DebugLockMonitorData, String>, TableCell<DebugLockMonitorData, String>>() {
            @Override
            public TableCell<DebugLockMonitorData, String> call(TableColumn<DebugLockMonitorData, String> param) {
                return new QueryTableCell<DebugLockMonitorData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn explainColumn = createColumn("Explain", "query");
        explainColumn.setCellFactory(new Callback<TableColumn<DebugExplainProvider, String>, TableCell<DebugExplainProvider, String>>() {
            @Override
            public TableCell<DebugExplainProvider, String> call(TableColumn<DebugExplainProvider, String> param) {
                return new DebuggerExplainTableCell();
            }
        });

        tableView.addTableColumn(idColumn, 0.05);
        tableView.addTableColumn(userColumn, 0.1);
        tableView.addTableColumn(hostColumn, 0.1);
        tableView.addTableColumn(databaseColumn, 0.1);
        tableView.addTableColumn(commandColumn, 0.1);
        tableView.addTableColumn(timeColumn, 0.1);
        tableView.addTableColumn(stateColumn, 0.1);
        tableView.addTableColumn(queryColumn, 0.21);
        tableView.addTableColumn(waitingTableLockColumn, 0.1);
        tableView.addTableColumn(blockingQueryColumn, 0.21);
        tableView.addTableColumn(blockingTrxIdColumn, 0.1);
        tableView.addTableColumn(blockingThreadColumn, 0.1);
        tableView.addTableColumn(blockingHostColumn, 0.1);
        tableView.addTableColumn(idleInTrxColumn, 0.1);
        tableView.addTableColumn(lockTypeColumn, 0.1);
        tableView.addTableColumn(lockTypeDescColumn, 0.15);
        tableView.addTableColumn(explainColumn, 0.1, 50);

        timeColumn.setSortType(TableColumn.SortType.DESCENDING);
        timeColumn.setComparator(new Comparator<DebugLockMonitorData>() {
            @Override
            public int compare(DebugLockMonitorData o1, DebugLockMonitorData o2) {

                boolean sleep1 = "sleep".equalsIgnoreCase(o1.getState());
                boolean sleep2 = "sleep".equalsIgnoreCase(o2.getState());
                if (sleep1 && sleep2) {
                    return (int) (o1.getTime().doubleValue() - o2.getTime().doubleValue());
                } else if (sleep1) {
                    return -1;
                } else if (sleep2) {
                    return 1;
                }

                return (int) (o1.getTime().doubleValue() - o2.getTime().doubleValue());
            }
        });
        tableView.addColumnToSortOrder(timeColumn);

        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }

    private void initDeadlockMonitorQueriesTable(FilterableTableView tableView) {

        final HashMap<Integer, String> rowStyleProperties = new HashMap(10);

        TableColumn detailsColumn = createColumn("Details", "details");
        detailsColumn.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Details")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        setStyle("");
                        DeadlockMonitorQueryData p = (DeadlockMonitorQueryData) getTableRow().getItem();
                        if (p != null) {
                            try {
                                Date d = dateFormat0.parse(p.getDeadlockTime());
                                if (d != null && d.getTime() > new Date().getTime() - 3600000) {
                                    setStyle("-fx-text-fill: red;");
                                }
                            } catch (ParseException ex) {
                                log.error("Error", ex);
                            }
                        }

                        rowStyleProperties.put(getIndex(), getStyle());
                    }
                };
            }
        });

        TableColumn statusColumn = createColumn("Status", "status");
        statusColumn.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Status")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {

                        String bkColor;
                        if ("Fixed".equals(item)) {
                            item = "Please check Recommendation Trans 1 & 2 and Query tuning for Trans 1 & 2 and implement";
                            bkColor = "green";
                        } else {
                            bkColor = "none";
                        }

                        super.updateItem(item, empty);

                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "") + "-fx-background-color: " + bkColor));
                    }
                };
            }
        });
        TableColumn deadlockTimeColumn = createColumn("Dead Lock Time", "deadlockTime");
        deadlockTimeColumn.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new TableCell<DeadlockMonitorQueryData, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn transaction1Column = createColumn("Transaction1", "transaction1");
        transaction1Column.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Transaction1")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn explain1Column = createColumn("Explain1", "transaction1");
        explain1Column.setCellFactory(new Callback<TableColumn<DebugExplainProvider, String>, TableCell<DebugExplainProvider, String>>() {
            @Override
            public TableCell<DebugExplainProvider, String> call(TableColumn<DebugExplainProvider, String> param) {
                return new DebuggerExplainTableCell();
            }
        });

        TableColumn transaction2Column = createColumn("Transaction2", "transaction2");
        transaction2Column.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Transaction2")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn explain2Column = createColumn("Explain2", "transaction2");
        explain2Column.setCellFactory(new Callback<TableColumn<DebugExplainProvider, String>, TableCell<DebugExplainProvider, String>>() {
            @Override
            public TableCell<DebugExplainProvider, String> call(TableColumn<DebugExplainProvider, String> param) {
                return new DebuggerExplainTableCell();
            }
        });

        TableColumn queryTuningTransaction1Column = createColumn("Query Tuning for Trans1", "queryTuningTransaction1");
        queryTuningTransaction1Column.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Query Tuning for Trans1")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn recommendationsTransaction1Column = createColumn("Recommendations for Trans1", "recommendationsTransaction1");
        recommendationsTransaction1Column.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Recommendations for Trans1")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn queryTuningTransaction2Column = createColumn("Query Tuning for Trans2", "queryTuningTransaction2");
        queryTuningTransaction2Column.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Query Tuning for Trans2")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn recommendationsTransaction2Column = createColumn("Recommendations for Trans2", "recommendationsTransaction2");
        recommendationsTransaction2Column.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Recommendations for Trans2")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        TableColumn rollbackColumn = createColumn("Rollback Transaction", "rollback");
        rollbackColumn.setCellFactory(new Callback<TableColumn<DeadlockMonitorQueryData, String>, TableCell<DeadlockMonitorQueryData, String>>() {
            @Override
            public TableCell<DeadlockMonitorQueryData, String> call(TableColumn<DeadlockMonitorQueryData, String> param) {
                return new QueryTableCell<DeadlockMonitorQueryData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Transaction1")) {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        Platform.runLater(() -> setStyle(rowStyleProperties.getOrDefault(getIndex(), "")));
                    }
                };
            }
        });

        tableView.addTableColumn(detailsColumn, 0.1);
        tableView.addTableColumn(statusColumn, 0.08, 175);
        tableView.addTableColumn(deadlockTimeColumn, 0.08, 160.0);
        tableView.addTableColumn(transaction1Column, 0.15, 350);
        tableView.addTableColumn(explain1Column, 0.08);
        tableView.addTableColumn(transaction2Column, 0.15, 350);
        tableView.addTableColumn(explain2Column, 0.08);
        tableView.addTableColumn(queryTuningTransaction1Column, 0.1, 350);
        tableView.addTableColumn(recommendationsTransaction1Column, 0.1, 350);
        tableView.addTableColumn(queryTuningTransaction2Column, 0.1, 350);
        tableView.addTableColumn(recommendationsTransaction2Column, 0.1, 350);
        tableView.addTableColumn(rollbackColumn, 0.1, 175);

        deadlockTimeColumn.setSortType(TableColumn.SortType.DESCENDING);
        tableView.addColumnToSortOrder(deadlockTimeColumn);

        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }

    private void copyData(TableView table, KeyEvent event) {
        if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.C) {

            List<TablePosition> l = table.getSelectionModel().getSelectedCells();
            if (l != null && !l.isEmpty()) {
                Clipboard clipboard = Clipboard.getSystemClipboard();
                ClipboardContent content = new ClipboardContent();

                TablePosition pos = l.get(0);

                Object item = table.getItems().get(pos.getRow());
                TableColumn col = pos.getTableColumn();

                Object data = col.getCellObservableValue(item).getValue();

                content.putString(data != null ? data.toString() : "null");
                clipboard.setContent(content);
            }
        }
    }
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title){
        FunctionHelper.showSlowQueryData(getStage(), getController(), cellData, text, title, preferenceService);
    }
    
    private void showMoreData(QueryTableCell.QueryTableCellData cellData, String text, String title) {
        final Stage dialog = new Stage();
        ShowMoreDataCellController cellController = StandardBaseControllerProvider.getController(getController(), "ShowMoreDataCell");
        cellController.init(text, false);
        
        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);
        
        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }

//    public class InsertBuffersData {
//        private final SimpleStringProperty inserts;
//        private final SimpleStringProperty mergedRecs;
//        private final SimpleStringProperty merges;
//        private final SimpleStringProperty size;
//        private final SimpleStringProperty freeListLen;
//        private final SimpleStringProperty segSize;
//        
//        public InsertBuffersData() {
//            this.inserts = new SimpleStringProperty();
//            this.mergedRecs = new SimpleStringProperty();
//            this.merges = new SimpleStringProperty();
//            this.size = new SimpleStringProperty();
//            this.freeListLen = new SimpleStringProperty();
//            this.segSize = new SimpleStringProperty();
//        }
//        
//        public SimpleStringProperty inserts() {
//            return inserts;
//        }
//        
//        public SimpleStringProperty mergedRecs() {
//            return mergedRecs;
//        }
//        
//        public SimpleStringProperty merges() {
//            return merges;
//        }
//        
//        public SimpleStringProperty size() {
//            return size;
//        }
//        
//        public SimpleStringProperty freeListLen() {
//            return freeListLen;
//        }
//        
//        public SimpleStringProperty segSize() {
//            return segSize;
//        }
//    }
//    
//    public class AdaptiveHashIndexData {
//        private final SimpleStringProperty size;
//        private final SimpleStringProperty cellsUsed;
//        private final SimpleStringProperty nodeHeapBuffs;
//        private final SimpleStringProperty hashSec;
//        private final SimpleStringProperty nonHashSec;
//        
//        public AdaptiveHashIndexData() {
//            this.size = new SimpleStringProperty();
//            this.cellsUsed = new SimpleStringProperty();
//            this.nodeHeapBuffs = new SimpleStringProperty();
//            this.hashSec = new SimpleStringProperty();
//            this.nonHashSec = new SimpleStringProperty();
//        }
//        
//        public SimpleStringProperty size() {
//            return size;
//        }
//        
//        public SimpleStringProperty cellsUsed() {
//            return cellsUsed;
//        }
//        
//        public SimpleStringProperty nodeHeapBuffs() {
//            return nodeHeapBuffs;
//        }
//        
//        public SimpleStringProperty hashSec() {
//            return hashSec;
//        }
//        
//        public SimpleStringProperty nonHashSec() {
//            return nonHashSec;
//        }
//        
//    }
    private class DebuggerExplainTableCell extends TableCell<DebugExplainProvider, String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            DebugExplainProvider p = (DebugExplainProvider) getTableRow().getItem();

            if (!empty && p != null && item != null) {

                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

                if (p.getRowsSum() != null) {
                    setGraphic(createRatingNode(p, p.getRowsSum(), item));
                } else {
                    String query = item.toLowerCase();
                    if (query.startsWith("select") || query.startsWith("insert") || query.startsWith("update") || query.startsWith("delete")) {
                        Hyperlink link = new Hyperlink("EXPLAIN");
                        link.setStyle("-fx-font-weight: bold;");
                        link.setFocusTraversable(false);
                        link.setOnAction((ActionEvent event) -> {
                            showExplainPopup(p, item, DebuggerExplainTableCell.this, true);
                        });

                        setGraphic(link);
                    } else {
                        setGraphic(null);
                    }
                }
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
                setText("");
            }
        }
    }

    private Node createRatingNode(DebugExplainProvider p, Long rowsSum, String query) {
        HBox box = new HBox();
        box.setCursor(Cursor.HAND);
        box.setOnMouseClicked((MouseEvent event) -> {
            showExplainPopup(p, query, box, true);
        });

        if (rowsSum < 10) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_green_image()));
        } else if (rowsSum < 100) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_blue_image()));
        } else if (rowsSum < 1000) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_yellow_image()));
        } else if (rowsSum < 10000) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else if (rowsSum < 20000) {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else if (rowsSum < 30000) {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else if (rowsSum < 50000) {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        }

        return box;
    }

    private Node createRatingNode(ProfilerQueryData p, Long rowsSum, Integer item, TableCell cell) {
        HBox box = new HBox();
        box.setCursor(Cursor.HAND);
        box.setOnMouseClicked((MouseEvent event) -> {
            showExplainPopup(p, item, box, cell);
        });

        if (rowsSum < 10) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_green_image()));
        } else if (rowsSum < 100) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_blue_image()));
        } else if (rowsSum < 1000) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_yellow_image()));
        } else if (rowsSum < 10000) {
            box.getChildren().add(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else if (rowsSum < 20000) {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else if (rowsSum < 30000) {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else if (rowsSum < 50000) {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        } else {
            box.getChildren().addAll(new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()), new ImageView(StandardDefaultImageProvider.getInstance().getRating_red_image()));
        }

        return box;
    }

    private void showExplainPopup(ProfilerQueryData p, Integer item, Node node, TableCell cell) {

        String query = profilerService.getQueryById(item);
        String explainQuery = QueryFormat.convertToExplainQuery("EXPLAIN", query);

        if (explainQuery != null) {
            Long rowsSum = 0l;
            List<ExplainResult> result = new ArrayList<>();
            QueryResult qr = null;
            if (p.getDatabase() != null && !p.getDatabase().isEmpty()) {
                explainQuery = "USE " + p.getDatabase() + "; " + explainQuery;
                qr = new QueryResult();
            }

            try {
                ResultSet rs = null;
                if (qr != null) {
                    service.execute(explainQuery, qr);
                    rs = (ResultSet) qr.getResult().get(1);
                } else {
                    rs = service.getDB().prepareStatement(explainQuery).executeQuery();
                }

                while (rs.next()) {

                    Long rows = rs.getLong(10);
                    result.add(new ExplainResult(
                            rs.getLong(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(7),
                            rs.getLong(8),
                            rs.getString(9),
                            rows,
                            rs.getLong(11),
                            rs.getString(12))
                    );

                    rowsSum += rows;
                }

                rs.close();
            } catch (Throwable th) {
                DialogHelper.showError(getController(), "Error", "Error in explain", th);
            }

            TableView<ExplainResult> table = new TableView<>();
            table.setFocusTraversable(false);
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            TableColumn idColumn = new TableColumn("Id");
            idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

            TableColumn selectTypeColumn = new TableColumn("SelectType");
            selectTypeColumn.setCellValueFactory(new PropertyValueFactory<>("selectType"));

            TableColumn tableColumn = new TableColumn("Table");
            tableColumn.setCellValueFactory(new PropertyValueFactory<>("table"));

            TableColumn typeColumn = new TableColumn("Type");
            typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));

            TableColumn posibleKyesColumn = new TableColumn("PosibleKyes");
            posibleKyesColumn.setCellValueFactory(new PropertyValueFactory<>("posibleKyes"));

            TableColumn keyColumn = new TableColumn("Key");
            keyColumn.setCellValueFactory(new PropertyValueFactory<>("key"));

            TableColumn keyLenColumn = new TableColumn("KeyLen");
            keyLenColumn.setCellValueFactory(new PropertyValueFactory<>("keyLen"));

            TableColumn refColumn = new TableColumn("Ref");
            refColumn.setCellValueFactory(new PropertyValueFactory<>("ref"));

            TableColumn rowsColumn = new TableColumn("Rows");
            rowsColumn.setCellValueFactory(new PropertyValueFactory<>("rows"));

            TableColumn extraColumn = new TableColumn("Extra");
            extraColumn.setCellValueFactory(new PropertyValueFactory<>("extra"));

            table.getColumns().addAll(idColumn, selectTypeColumn, tableColumn, typeColumn, posibleKyesColumn, keyColumn, keyLenColumn, refColumn, rowsColumn, extraColumn);

            table.getItems().addAll(result);
            table.getSelectionModel().clearSelection();

            if (node instanceof ExplainTableCell) {
                cell.setGraphic(createRatingNode(p, rowsSum, item, cell));
                p.setRowsSum(rowsSum);
            }

            threadsAction(true);
            Popup popup = showPopup(table, node);

            popup.setOnCloseRequest((WindowEvent event) -> {
                threadsAction(false);
            });

        } else {
            showPopup(new Label("Can't call EXPLAIN query"), node);
        }
    }

    private Popup showPopup(Node content, Node node) {

        HBox box = new HBox(content);
        HBox.setHgrow(node, Priority.ALWAYS);
        box.setMaxSize(700, 150);
        box.getStyleClass().add("profilerPopup");

        Popup popup = new Popup();
        popup.setAutoHide(true);
        popup.getContent().addAll(box);

        Point2D windowCoord = new Point2D(node.getScene().getWindow().getX(), node.getScene().getWindow().getY());
        Point2D sceneCoord = new Point2D(node.getScene().getX(), node.getScene().getY());

        Point2D nodeCoord = node.localToScene(0.0, 0.0);

        double clickX = Math.round(windowCoord.getX() + sceneCoord.getY() + nodeCoord.getX());
        double clickY = Math.round(windowCoord.getY() + sceneCoord.getY() + nodeCoord.getY());

        popup.setX(clickX);
        popup.setY(clickY);

        popup.show(getController().getStage());

        return popup;
    }

    private void showExplainPopup(DebugExplainProvider p, String query, Node node, boolean visial) {
        
        // full usage exipred
        if (licenseFullUsageExipred) {
            return;
        }
        
        makeBusy(true);
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                String explainQuery = QueryFormat.convertToExplainQuery("EXPLAIN", query);

                if (explainQuery != null) {
                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    if (session != null) {
                        Database database;
                        if (p.getDatabase() != null) {
                            database = DatabaseCache.getInstance().getDatabase(session.getConnectionParam().cacheKey(), p.getDatabase());
                            if (database == null) {
                                try {
                                    database = DatabaseCache.getInstance().syncDatabase(p.getDatabase(), session.getConnectionParam().cacheKey(), (AbstractDBService) service);
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            }
                        } else {
                            database = session.getConnectionParam().getSelectedDatabase();
                        }

                        Database currentDatabase = database;
                        if (database != null) {
                            // Visual explain
                            
//                            QueryAnalyzer qa = new QueryAnalyzer(columnCardinalityService, currentDatabase, session.getConnectionParam(), session.getService(), query);
                            try {
//                                qa.analyze(null);
                                
                                Platform.runLater(() -> {

                                    if (node instanceof ExplainTableCell) {
                                        ((ExplainTableCell) node).setGraphic(createRatingNode(p, p.getRowsSum(), query));
                                    }

                                    try {

                                        threadsAction(true);

                                        Stage stage = new Stage();
                                        stage.initOwner(getStage());
                                        stage.initModality(Modality.APPLICATION_MODAL);
                                        stage.setOnCloseRequest((WindowEvent event) -> {
                                            threadsAction(false);
                                        });

                                        TabPane tabPane = new TabPane();
                                        tabPane.setPrefSize(1000, 600);

                                        VisualExplainController visualController = StandardBaseControllerProvider.getController(getController(), "SlowVisualExplain");
                                        visualController.setAPerformanceTuningService(performanceTuningService);
                                        visualController.setPreferenceService(preferenceService);
                                        visualController.setRecommendationService(qars);
                                        visualController.setColumnCardinalityService(columnCardinalityService);
                                        visualController.setUsageService(usageService);
                                        visualController.init(session.getService(), currentDatabase, query, true, tabPane, p);

                                        Tab explainTab = new Tab("Explain");
                                        explainTab.setClosable(false);
                                        explainTab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getVisualExplain_image()));
                                        explainTab.setOnClosed((Event event) -> {
                                            visualController.destroy();
                                        });

                                        explainTab.setContent(visualController.getUI());

                                        // SQL Optimizer
                                        QueryAnalyzerTabController controller = StandardBaseControllerProvider.getController(getController(), "QueryAnalyzerTab");
                                        controller.setService(session.getService());
                                        controller.setPerformanceTuningService(performanceTuningService);
                                        controller.setPreferenceService(preferenceService);
                                        controller.setQars(qars);
                                        controller.setColumnCardinalityService(columnCardinalityService);
                                        controller.setUsageService(usageService);
                                        controller.init(query, session, currentDatabase, false, RunnedFrom.DQA);

                                        controller.setAreaEditable(false);

                                        Tab queryAnalyzerTab = new Tab("SQL Optimizer");
                                        queryAnalyzerTab.setClosable(false);
                                        queryAnalyzerTab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getQueryAnalyzeTab_image()));

                                        queryAnalyzerTab.setContent(controller.getUI());

                                        tabPane.getTabs().addAll(explainTab, queryAnalyzerTab);

                                        tabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
                                            if (newValue == queryAnalyzerTab) {
                                                controller.analyzeQueryAction(null);
                                            }
                                        });

                                        if (visial) {
                                            tabPane.getSelectionModel().select(explainTab);
                                        } else {
                                            tabPane.getSelectionModel().select(queryAnalyzerTab);
                                        }

                                        Scene scene = new Scene(new BorderPane(), 1000, 600);
                                        ((BorderPane) scene.getRoot()).setCenter(tabPane);

                                        stage.setScene(scene);

                                        stage.show();
                                    } finally {
                                        makeBusy(false);
                                    }
                                });

                            } catch (Throwable ex) {
                                Platform.runLater(() -> {
                                    makeBusy(false);
                                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex, getStage());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                makeBusy(false);
                                showPopup(new Popup(), new Label("Please select database!"), node, 700, 150);
                            });
                        }
                    }
                } else {
                    Platform.runLater(() -> {
                        makeBusy(false);
                        showPopup(new Popup(), new Label("Can't call EXPLAIN query"), node, 700, 150);
                    });
                }
            }
        });
    }

    private void showPopup(Popup popup, Node content, Node node, double w, double h) {

        HBox box = new HBox(content);
        HBox.setHgrow(node, Priority.ALWAYS);
        box.setMaxSize(w, h);
        box.getStyleClass().add("profilerPopup");

        popup.setAutoHide(true);
        popup.getContent().addAll(box);

        Point2D windowCoord = new Point2D(node.getScene().getWindow().getX(), node.getScene().getWindow().getY());
        Point2D sceneCoord = new Point2D(node.getScene().getX(), node.getScene().getY());

        Point2D nodeCoord = node.localToScene(0.0, 0.0);

        double clickX = Math.round(windowCoord.getX() + sceneCoord.getY() + nodeCoord.getX());
        double clickY = Math.round(windowCoord.getY() + sceneCoord.getY() + nodeCoord.getY());

        popup.setX(clickX);
        popup.setY(clickY);

        popup.show(getController().getStage());
    }

    @FXML
    public void showRunningQueriesTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(runningQueriesTab);
    }
    
    @FXML
    public void showTransactionsTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(transactionsTab);
    }
    
    @FXML
    public void showDeadLocksTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(deadlockMonitorTab);
    }
    
    @FXML
    public void showQueriesLocksTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(lockMonitorTab);
    }
    
    @FXML
    public void showConnectionsTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(dbConnectionsAnalyzerTab);
    }
    
    @FXML
    public void showDBLoadTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(dbLoadTab);
    }
    
    @FXML
    public void showConfigurationTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(configurationDebuggerTab);
    }
    
    @FXML
    public void showReplicationLaggingTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(replicationLagging);
    }
    
    @FXML
    public void showReplicationMonitoringTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(replicationMonitorTab);
    }
    
    @FXML
    public void showForeignKeyErrorsTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(foreignKeyErrorsTab);
    }            
    
    @FXML
    public void showOpenTablesTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(openTablesTab);
    }            
    
    @FXML
    public void showBufferPoolTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(bufferPoolTab);
    }
    
    @FXML
    public void showInnoDBRowsTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(innoDBRowsTab);
    }
    
    @FXML
    public void showInnoDBStatusTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(innodbStatusTab);
    }
    
    @FXML
    public void showMyISAMStatusTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(myISAMTab);
    }
        
    @FXML
    public void showDBSizeTab(MouseEvent event) {
        //bottomTabPane.getSelectionModel().select(dbSizeTab);
        dataSizeReport(getController(), ReportDataSizeController.DATABASE_ITEM__EMPTY, ReportDataSizeController.ENGINE_ITEM__EMPTY);
    }
    
    @FXML
    public void showUnusedIndexesTab(MouseEvent event) {
        //bottomTabPane.getSelectionModel().select(unusedIndexesTab);
        unusedIndexReport(getController());
    }
    
    @FXML
    public void showDuplicateIndexesTab(MouseEvent event) {
        //bottomTabPane.getSelectionModel().select(duplicateIndexesTab);
        dublicateIndexReport(getController(), preferenceService);
    }
    
    @FXML
    public void showSlowQueryAnalizerTab(MouseEvent event) {
        //bottomTabPane.getSelectionModel().select(slowQueryAnalizerTab);
        showSlowQueryAnalyzer(getController(), profilerService, preferenceService, qars, columnCardinalityService, performanceTuningService, paramsService, usageService);
    }
    
    @FXML
    public void showErrorLogTab(MouseEvent event) {
        bottomTabPane.getSelectionModel().select(errorLogTab);
    }
    
    private void displayOnlyErrors(Boolean value) {
        
        if (params.isRemote()) {

            if (params.hasOSDetails()) {
                elRefreshAction(null);
            }
        } else {
            if (value != null && value) {
                errorLogTable.getTableView().setCustomFilter((FilterableData t) -> {
                    return ((ErrorLogResult)t).getLabel().toLowerCase().contains("error");
                });
            } else {
                errorLogTable.getTableView().setCustomFilter(null);
            }
        }
    }
    
    private List<String> errorLogData;
    private static long MAX_ERROR_LOG_SIZE = 50 * 1024 * 1024;
    
    private int currentErrorSize = 0;
    private static int ERROR_STEP = 500;
    
    private String errorLogPath = null;
    
    private String forceErrorLogPath = null;
            
    @FXML
    public void elRefreshAction(ActionEvent event) {
        
        makeBusy(true);
        
        errorLogTable.setItems(FXCollections.observableArrayList());
        
        Thread th = new Thread(() -> {

            errorLogData = new ArrayList<>();
            currentErrorSize = 0;
            
            if (forceErrorLogPath != null) {
                errorLogPath = forceErrorLogPath;
                forceErrorLogPath = null;
            } else {

                QueryResult qr = new QueryResult();
                try {

                    String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_LOG_ERROR_VARIABLE);
                    if (!query.endsWith(";")) {
                        query += ";";
                    }

                    query += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_DATADIR_VARIABLE);

                    service.execute(query, qr);
                    if (qr.getResult() != null && !qr.getResult().isEmpty()) {
                        ResultSet rs = (ResultSet) qr.getResult().get(0);
                        if (rs.next()) {
                            errorLogPath = rs.getString(2);
                        }
                        rs.close();

                        rs = (ResultSet) qr.getResult().get(1);
                        if (rs.next()) {
                            String basedir = rs.getString(2);
                            if (basedir != null && (errorLogPath.startsWith(".\\") || errorLogPath.startsWith("./") || (!params.isRemote() && PlatformUtil.isWindows()))) {

                                if (!basedir.endsWith(String.valueOf(File.separatorChar))) {
                                    basedir += File.separatorChar;
                                }

                                if (errorLogPath.startsWith(".\\") || errorLogPath.startsWith("./")) {
                                    errorLogPath = errorLogPath.substring(2);
                                }

                                errorLogPath = basedir + errorLogPath;
                            }
                        }
                        rs.close();
                    }
                } catch (SQLException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }

            if (params.isRemote()) {

                if (params.hasOSDetails()) {
                    
                    OsDownloader downloader = new OsDownloader(params);
                    downloader.connectToRemote();
                    
                    try {
                        log.info("Start remote");
                                                
                        errorLogData = downloader.downloadErrorLogFileLines(errorLogPath, getErrorLogDays(), ERROR_STEP, 0, displayOnlyErrors.isSelected());
                        log.info("Finish remote");
                    } catch (Throwable ex) {
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                        });
                    }
                    
                    downloader.disconnectFromRemote();
                    
                } else {
                    Platform.runLater(() -> {
                        DialogHelper.showInfo(getController(), "Info", "Please enter OS details", null);
                        
                        OsDetailsController osController = StandardBaseControllerProvider.getController(getController(), "OsDetails");
                        osController.init(getStage(), paramsService, params);
                        if (osController.getSelectedConnParam() != null) {
                            params = osController.getSelectedConnParam();
                            elRefreshAction(null);
                        }
                    });
                    
                    makeBusy(false);
                    return;
                }

            } else {
                try {
                    File f = new File(errorLogPath);
                    if (f.exists()) {
                        
                        if (PlatformUtil.isWindows()) {
                            // check file size
                            if (f.length() > MAX_ERROR_LOG_SIZE) {
                                Platform.runLater(() -> {
                                    DialogHelper.showError(getController(), "Error", "Fixe size is more than 50MB not support in Windows. Please flush error log");
                                });                            
                            } else {
                                errorLogData = Files.readAllLines(f.toPath());
                            }
                        } else {
                            try {
                                log.info("Start remote");
                                OsDownloader downloader = new OsDownloader(params);
                                errorLogData = downloader.downloadErrorLogFileLines(errorLogPath, getErrorLogDays(), ERROR_STEP, 0, displayOnlyErrors.isSelected());
                                log.info("Finish remote");
                            } catch (Throwable ex) {
                                Platform.runLater(() -> {
                                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                                });
                            }
                        }
                    } else {
                        String message =  "Error Log file \"" + errorLogPath + "\" not found";
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", message);
                        });
                    }
                } catch (Throwable ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
            
            Collections.sort(errorLogData, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    int v = o1.compareTo(o2);
                    if (v > 0) {
                        return -1;
                    } else if (v < 0) {
                        return 1;
                    }
                    return 0;
                }
            });
            
            // must start at least with digit (improve in future)
            // clear all not matched
            int i = 0;
            while (true) {
                if (errorLogData.size() > i) {
                    String text = errorLogData.get(i).toLowerCase();
                    if (!text.matches("\\d.*")) {
                        errorLogData.remove(i);
                    } else {
                        
                        // must contains [ERROR] [WARNING] [NOTE] or subsystem  error code
                        if (text.contains("[warning]") || text.contains("[error]") || text.contains("[note]") || text.contains("[system]")) {
                            i++;
                        } else {
                            errorLogData.remove(i);
                        }
                    }
                } else {
                    break;
                }
            }
            
            addErrorResults(0, ERROR_STEP);
            
        });
        th.setDaemon(true);
        th.start();
    }
    
    private void addErrorResults(int from, int to) {
        List<ErrorLogResult> itemsForAdd = new ArrayList<>();
        for (int i = from; i < to; i++) {
            if (i >= errorLogData.size()) {
                break;
            }
            
            String row = errorLogData.get(i);
            String[] cells = row.split(" ");
            
            try {
                ErrorLogResult er;
                if (service.getDbSQLVersionMaj() == 8) {
                    er = new ErrorLogResult(
                        cells[0], 
                        cells[1], 
                        cells[2], 
                        cells[3], 
                        cells[4], 
                        String.join(" ", Arrays.copyOfRange(cells, 5, cells.length))
                    );
                } else {
                    er = new ErrorLogResult(
                        cells[0], 
                        cells[1], 
                        cells[2], 
                        "", 
                        "", 
                        String.join(" ", Arrays.copyOfRange(cells, 3, cells.length))
                    );
                }
                
                itemsForAdd.add(er);
                
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            }
        }
        
        Platform.runLater(() -> {
           errorLogTable.addItems(itemsForAdd); 
           
           makeBusy(false);
        });
    }
    
    private Integer getErrorLogDays() {
        String dayLog = dayLogComboBox.getSelectionModel().getSelectedItem();
        if (dayLog != null && !dayLog.isEmpty()) {
            switch (dayLog) {
                case ERROR_LOG__1_DAY:
                    return 1;
                case ERROR_LOG__2_DAY:
                    return 2;
                case ERROR_LOG__3_DAY:
                    return 3;
                case ERROR_LOG__4_DAY:
                    return 4;
                case ERROR_LOG__5_DAY:
                    return 5;
                case ERROR_LOG__6_DAY:
                    return 6;
                case ERROR_LOG__7_DAY:
                    return 7;
            }
        }
        
        return null;
    }
    
    @FXML
    public void elGetOldAction(ActionEvent event) {
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            
            int old = currentErrorSize;
            currentErrorSize += ERROR_STEP;
            
            if (params.isRemote()) {

                if (params.hasOSDetails()) {
                    
                    OsDownloader downloader = new OsDownloader(params);
                    downloader.connectToRemote();
                    
                    try {
                        log.info("Start remote");
                        errorLogData = downloader.downloadErrorLogFileLines(errorLogPath, getErrorLogDays(), currentErrorSize + ERROR_STEP, ERROR_STEP, displayOnlyErrors.isSelected());
                        log.info("Finish remote");
                    } catch (Throwable ex) {
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);                        
                        });
                    }
                    
                    downloader.disconnectFromRemote();
                    
                    Collections.sort(errorLogData, new Comparator<String>() {
                        @Override
                        public int compare(String o1, String o2) {
                            int v = o1.compareTo(o2);
                            if (v > 0) {
                                return -1;
                            } else if (v < 0) {
                                return 1;
                            }
                            return 0;
                        }
                    });

                    // must start at least with digit (improve in future)
                    // clear all not matched
                    int i = 0;
                    while (true) {
                        if (errorLogData.size() > i) {
                            if (!errorLogData.get(i).matches("\\d.*")) {
                                errorLogData.remove(i);
                            } else {
                                i++;
                            }
                        } else {
                            break;
                        }
                    }

                    addErrorResults(0, ERROR_STEP);
            
                }
            } else {                
                addErrorResults(old, currentErrorSize);
            }
            
            
        });
        th.setDaemon(true);
        th.start();
    }
    
    public class SimpleErrorLogTableCell<T> extends TableCell<ErrorLogResult, T> {

        @Override
        protected void updateItem(T item, boolean empty) {

            super.updateItem(item, empty);
            
            ErrorLogResult er = (ErrorLogResult)getTableRow().getItem();
            
            String name = null;
            Node n = getTableColumn().getGraphic();
            if (n instanceof Label) {
                name = ((Label)n).getText();
            }
            if (er != null && er.isError() && (name != null && (name.equals("Error Code") || name.equals("Label")))) {
                setStyle("-fx-text-fill: red");
            } else {
                setStyle("");
            }

            if (item != null) {
                setText(item.toString());
            } else {
                setText("");
            }
        }
    }
}
