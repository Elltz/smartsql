package np.com.ngopal.smart.sql.structure.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringPropertyBase;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SSHConnection {

    private Process proccess;

    private volatile boolean connected = false;

    private final StringPropertyBase resultProperty = new SimpleStringProperty();

    private String connectCommand;

    private final String plinkPath;

    public SSHConnection(final String plinkPath) {
        this.plinkPath = plinkPath;
    }

    public StringPropertyBase resultProperty() {
        return resultProperty;
    }

    public synchronized void disconnect() {
        if (proccess != null)
        {
            proccess.destroy();
        }

        connected = false;
        proccess = null;
    }

    public synchronized void connect(final ConnectionParams params) {
        
        disconnect();
        
        try {
            connectCommand = plinkPath + " ";

            connectCommand += "-t -P " + params.getOsPort() + " ";

            connectCommand += "-pw " + params.getOsPassword() + " ";

            if (params.getOsUser() != null && !params.getOsUser().isEmpty()) {
                connectCommand += params.getOsUser() + "@";
            }

            connectCommand += params.getOsHost();

            proccess = Runtime.getRuntime().exec(connectCommand);

            connected = true;

            InputStream std = proccess.getInputStream();
            InputStream err = proccess.getErrorStream();

            readFromStreamInThread(std, "SSHInput");
            readFromStreamInThread(err, "SSHError");
        } catch (Exception e) {
            log.error("Error while ssh connect", e);
        }
    }

    private void readFromStreamInThread(final InputStream is, final String threadName) {
        final Thread t = new Thread(() -> {
            while (connected) {
                try {
                    Thread.sleep(200);

                    if (connected) {
                        InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                        BufferedReader br = new BufferedReader(isr);

                        final StringBuilder s = new StringBuilder();
                        while (connected && br.ready()) {
                            s.append((char) br.read());
                        }

                        if (s.length() > 0) {
                            resultChanged(s.toString());
                        }
                    }
                } catch (IOException | InterruptedException ex) {
                    log.error("Error while reading from ssh stream", ex);
                }
            }
        }, threadName);

        t.setDaemon(true);
        t.start();
    }

    public void sendCommand(final String command) {
        try {
            proccess.getOutputStream().write((command + "\n").getBytes());
            proccess.getOutputStream().flush();
        } catch (IOException ex) {
            log.error("Error sending command", ex);
        }
    }

    private synchronized void resultChanged(String value) {
        resultProperty.set(null);
        resultProperty.set(value);
    }

}
