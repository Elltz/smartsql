package np.com.ngopal.smart.sql.structure.provider;

import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.EngineInnodbStatus;
import np.com.ngopal.smart.sql.model.GlobalStatusStart;
import np.com.ngopal.smart.sql.model.ProfilerSettings;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
@Singleton
public class DBDebuggerProfilerService extends DefaultProfilerDataManager
{
    public static final Long CACHE_TIME = 30 * 1000l; //2 * 60 * 60 * 1000l;
    
    private Map<Long, List<Object[]>> EIS = new LinkedHashMap<>();
    private Map<Long, List<Object[]>> GS = new LinkedHashMap<>();
    
    public DBDebuggerProfilerService(){
    }    
    
    public void addGlobalStatus(Long timesRunned, String key, Object value) {
        synchronized(storage) {
            List<Object[]> l = GS.get(timesRunned);
            if (l == null) {
                l = new ArrayList<>();
                GS.put(timesRunned, l);
            }

            l.add(new Object[] {key, value});

            Long minTime = new Date().getTime() - CACHE_TIME;
            for (Long time: new ArrayList<>(GS.keySet())) {
                if (time < minTime) {
                    GS.remove(time);
                } else {
                    break;
                }
            } 
        }
    }
    
    public void addEngineStatus(Long timesRunned, String key, Object value) {
        synchronized(storage) {
            List<Object[]> l = EIS.get(timesRunned);
            if (l == null) {
                l = new ArrayList<>();
                EIS.put(timesRunned, l);
            }

            l.add(new Object[] {key, value});

            Long minTime = new Date().getTime() - CACHE_TIME;
            for (Long time: new ArrayList<>(EIS.keySet())) {
                if (time < minTime) {
                    EIS.remove(time);
                } else {
                    break;
                }
            } 
        }
    }
    
    @Override
    public Map<Long, Map<String, EngineInnodbStatus>> selectEISDistinction(String serverHashCode, Date dateFrom, long timeTick, Date nextDate) {
        synchronized (storage) {
            Map<Long, Map<String, EngineInnodbStatus>> result = new LinkedHashMap<>();

            Long lastTime = null;

            Map<String, EngineInnodbStatus> latest = new HashMap<>();

            EMPTY_ENGINE_INNODB_STATUSES.clear();
            boolean emptyAdded = true;

            for (Long dateTime: EIS.keySet()) {
                
                for (Object[] objs: EIS.get(dateTime)) {

                    String name = (String) objs[0];
                    Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : null;
                    if (Double.isNaN(value)) {
                        value = 0.0;
                    }

                    if (lastTime != null && dateTime - lastTime >= timeTick) {

                        Map<String, EngineInnodbStatus> m = result.get(lastTime);
                        if (m != null) {
                            emptyAdded = false;                                            
                            for (String key: m.keySet()) {
                                EMPTY_ENGINE_INNODB_STATUSES.put(key, new EngineInnodbStatus(key, 0.0, 0.0));
                            }
                        }
                    }

                    if (!emptyAdded) {
                        result.put(lastTime + 1000, EMPTY_ENGINE_INNODB_STATUSES);
                        result.put(dateTime - 1000, EMPTY_ENGINE_INNODB_STATUSES);
                        emptyAdded = true;
                    }

                    Map<String, EngineInnodbStatus> map = result.get(dateTime);

                    if (map == null) {
                        map = new HashMap<>();

                        result.put(dateTime, map);
                    }

                    EngineInnodbStatus old = latest.get(name);
                    map.put(name, new EngineInnodbStatus(name, value.doubleValue(), old != null ? value.doubleValue() - old.getValue() : 0.0));
                    latest.put(name, new EngineInnodbStatus(name, value, 0.0));

                    lastTime = dateTime;
                }
            }

            if (nextDate != null && lastTime != null && nextDate.getTime() - lastTime > 120000) {
                result.put(lastTime + 1000, EMPTY_ENGINE_INNODB_STATUSES);
                result.put(nextDate.getTime() - 1000, EMPTY_ENGINE_INNODB_STATUSES);
            }

            return result;
        }
    }
    
    public Map<Long, Map<String, GlobalStatusStart>> selectGSDistinction(String serverHashCode, Date dateFrom, long timeTick, Date nextDate) {
        synchronized (storage) {
            Map<Long, Map<String, GlobalStatusStart>> result = new LinkedHashMap<>();


            Map<String, GlobalStatusStart> latest = new HashMap<>();

            Long lastTime = null;

            EMPTY_STATUSES.clear();
            boolean emptyAdded = true;

            for (Long dateTime: GS.keySet()) {

                for (Object[] objs: GS.get(dateTime)) {

                    String name = (String) objs[0];
                    Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : null;

                    // TODO Need check performance, this is just for first time
                    if (lastTime != null && dateTime - lastTime >= timeTick) {

                        Map<String, GlobalStatusStart> m = result.get(lastTime);
                        if (m != null) {
                            emptyAdded = false;                                            
                            for (String key: m.keySet()) {
                                EMPTY_STATUSES.put(key, new GlobalStatusStart(key, 0.0, 0.0));
                            }
                        }
                    }

                    if (!emptyAdded) {
                        result.put(lastTime + 1000, EMPTY_STATUSES);
                        result.put(dateTime - 1000, EMPTY_STATUSES);
                        emptyAdded = true;
                    }

                    Map<String, GlobalStatusStart> map = result.get(dateTime);

                    if (map == null) {
                        map = new HashMap<>();

                        result.put(dateTime, map);
                    }

                    GlobalStatusStart old = latest.get(name.toUpperCase());

                    map.put(name.toUpperCase(), new GlobalStatusStart(name, value.doubleValue(), old != null ? value.doubleValue() - old.getValue() : 0.0));

                    latest.put(name.toUpperCase(), new GlobalStatusStart(name, value, 0.0));


                    lastTime = dateTime;
                }
            }

            if (nextDate != null && lastTime != null && nextDate.getTime() - lastTime > 120000) {
                result.put(lastTime + 1000, EMPTY_STATUSES);
                result.put(nextDate.getTime() - 1000, EMPTY_STATUSES);
            }

            return result;
        }
    }
    
    public Map<Long, Map<String, EngineInnodbStatus>> selectLastEIS(String serverHashCode, Date lastDateUpdate) {
        synchronized (storage) {
            
            Map<Long, Map<String, EngineInnodbStatus>> result = new LinkedHashMap<>();
            
            Map<String, EngineInnodbStatus> latest = new HashMap<>();

            for (Long dateTime: EIS.keySet()) {

                for (Object[] objs: EIS.get(dateTime)) {

                    String name = (String) objs[0];
                    Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : 0.0;
                    if (Double.isNaN(value)) {
                        value = 0.0;
                    }
                    if (dateTime >= lastDateUpdate.getTime()) {

                        Map<String, EngineInnodbStatus> map = result.get(dateTime);
                        if (map == null) {
                            map = new HashMap<>();
                            result.put(dateTime, map);
                        }

                        EngineInnodbStatus old = latest.get(name.toUpperCase());
                        EngineInnodbStatus gs = map.get(name.toUpperCase());
                        if (gs == null) {
                            gs = new EngineInnodbStatus(name, value, old != null ? value - old.getValue() : 0.0);
                        } else {
                            gs.setValue(value);
                        }

                        map.put(name, gs);
                    }

                    latest.put(name, new EngineInnodbStatus(name, value, 0.0));
                }
            }
            
            return result;
        }
    }
    
    public Map<Long, Map<String, GlobalStatusStart>> selectLastGS(String serverHashCode, Date lastDateUpdate) {
        synchronized (storage) {
            
            Map<Long, Map<String, GlobalStatusStart>> result = new LinkedHashMap<>();
            Map<String, GlobalStatusStart> latest = new HashMap<>();
            
            for (Long dateTime: GS.keySet()) {

                for (Object[] objs: GS.get(dateTime)) {

                    String name = (String) objs[0];
                    Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : null;

                    if (dateTime >= lastDateUpdate.getTime()) {

                        Map<String, GlobalStatusStart> map = result.get(dateTime);
                        if (map == null) {
                            map = new HashMap<>();
                            result.put(dateTime, map);
                        }

                        GlobalStatusStart old = latest.get(name.toUpperCase());
                        GlobalStatusStart gs = map.get(name.toUpperCase());
                        if (gs == null) {
                            gs = new GlobalStatusStart(name, value, old != null ? value - old.getValue() : 0.0);
                        } else {
                            gs.setDifferece(old != null ? value - old.getValue() : 0.0);
                            gs.setValue(value);
                        }

                        map.put(name, gs);
                    }

                    latest.put(name.toUpperCase(), new GlobalStatusStart(name, value, 0.0));                    
                }
            }
            
            return result;
        }
    }
    
    
    public void clearOldGSData(ProfilerSettings settings)
    {
        // not used
    }
    
}
