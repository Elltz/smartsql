package np.com.ngopal.smart.sql.structure.components;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.factories.ThreadsTableCell;
import np.com.ngopal.smart.sql.structure.models.ProfilerTopData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class ChartTableView extends AnchorPane {

    private PieChart chart;
    private TableView<ProfilerTopData> table;
    private DropShadow ds;
    private StackPane pieContainer;
    private boolean black;
    
    private static final String[] COLORS = new String[] {"turquoise", "aquamarine", "cornflowerblue", "blue", "cadetblue", "navy", "deepskyblue", "cyan", "steelblue", "teal", "royalblue", "dodgerblue"};

    public ChartTableView() {

        pieContainer = new StackPane((chart = new PieChart()));
        table = new TableView<>();
        ds = new DropShadow(10, Color.BLACK);
        getChildren().addAll(table, pieContainer);
        heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                    Number oldValue, Number newValue) {
                if (newValue != null) {
                    adjustHeightState(newValue.doubleValue());
                }
            }
        });

        AnchorPane.setRightAnchor(table, 0.0);
        AnchorPane.setLeftAnchor(table, 0.0);
        AnchorPane.setBottomAnchor(table, 0.0);

        AnchorPane.setTopAnchor(pieContainer, 0.0);
        AnchorPane.setRightAnchor(pieContainer, 0.0);
        AnchorPane.setLeftAnchor(pieContainer, 0.0);

        ds.setOffsetX(1.0);
        ds.setOffsetY(1.0);
        table.setFixedCellSize(20);
        table.setStyle("-fx-font-size: 11;");
        table.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

        table.setMinSize(TableView.USE_COMPUTED_SIZE, TableView.USE_PREF_SIZE);
        table.setPrefSize(TableView.USE_COMPUTED_SIZE, TableView.USE_COMPUTED_SIZE);
        table.setMaxSize(TableView.USE_COMPUTED_SIZE, TableView.USE_PREF_SIZE);
        table.setPlaceholder(new Label(""));

        chart.setPadding(new Insets(-1));
        chart.setPrefSize(TableView.USE_COMPUTED_SIZE, TableView.USE_COMPUTED_SIZE);
    }

    public interface ChartTableData {
    }

    private TableColumn<ProfilerTopData, Number> indexColumn;
    private TableColumn<ProfilerTopData, String> sourceColumn;
    private TableColumn<ProfilerTopData, BigDecimal> threadsColumn;
    private TableColumn<ProfilerTopData, BigDecimal> timeColumn;

    private byte[] chartImage;

    public byte[] getChartImage() {
        chartImage = null;

        if (Platform.isFxApplicationThread()) {
            chartImage = getImage();

        } else {
            CountDownLatch mutex = new CountDownLatch(1);

            Platform.runLater(() -> {
                try {
                    chartImage = getImage();
                } finally {
                    mutex.countDown();
                }
            });

            try {
                mutex.await();
            } catch (InterruptedException ex) {
                log.error("Error", ex);
            }
        }

        return chartImage;
    }

    private byte[] getImage() {
        try {
            WritableImage image = chart.snapshot(new SnapshotParameters(), null);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

            return bos.toByteArray();
        } catch (Throwable e) {
            log.error("Error", e);
        }

        return null;
    }

    public TableView<ProfilerTopData> getTable() {
        return table;
    }

    public List<ProfilerTopData> getCopyItems() {
        synchronized (table) {
            return new ArrayList<>(table.getItems());
        }
    }

    public List<ProfilerTopData> getData() {
        return table.getItems();
    }

    public TableColumn getColumnByName(String name) {
        if (name != null) {
            for (TableColumn tc : table.getColumns()) {
                if (name.equals(tc.getText())) {
                    return tc;
                } else {
                    Node n = tc.getGraphic();
                    if (n instanceof Label && name.equals(((Label) n).getText())) {
                        return tc;
                    }
                }
            }
        }

        return null;
    }

    public void init(String chartTitle, String sourceName, String sourceField, String threadsName, String threadsField, String timeName, String timeField) {

        chart.setTitle(chartTitle);
        indexColumn = new TableColumn<>("#");
        indexColumn.setSortable(false);
        indexColumn.setCellValueFactory(column -> new ReadOnlyObjectWrapper<>(
                table.getItems().indexOf(column.getValue()) + 1));
        indexColumn.setCellFactory(new Callback<TableColumn<ProfilerTopData, Number>, TableCell<ProfilerTopData, Number>>() {
            @Override
            public TableCell<ProfilerTopData, Number> call(TableColumn<ProfilerTopData, Number> param) {
                return new TableCell<ProfilerTopData, Number>() {
                    @Override
                    protected void updateItem(Number item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setStyle("-fx-background-color: transparent;");
                        }
                    }

                    @Override
                    public void updateIndex(int i) {
                        super.updateIndex(i);
                        if (i > -1 && i < chart.getData().size()) {
                            PieChart.Data data = chart.getData().get(i);
                            
                            String style = data.getNode().getStyle();
                            String color = style.replace("-fx-pie-color: ", "");
                            
                            setStyle("-fx-background-color: " + color + "; -fx-background-insets: 3 3 3 20");
                            setText(String.valueOf(i + 1));
                        }
                    }

                };
            }
        });

        sourceColumn = new TableColumn<>(sourceName);
        sourceColumn.setCellValueFactory(new PropertyValueFactory<>(sourceField));

        if (threadsField != null) {
            threadsColumn = new TableColumn<>(threadsName);
            threadsColumn.setSortType(TableColumn.SortType.DESCENDING);
            threadsColumn.setCellValueFactory(new PropertyValueFactory<>(threadsField));
            threadsColumn.setCellFactory(new Callback<TableColumn<ProfilerTopData, BigDecimal>, TableCell<ProfilerTopData, BigDecimal>>() {
                @Override
                public TableCell<ProfilerTopData, BigDecimal> call(TableColumn<ProfilerTopData, BigDecimal> param) {
                    return new ThreadsTableCell<>();
                }
            });

            addColumnToSortOrder(threadsColumn);
        }
        
        if (timeField != null) {
            timeColumn = new TableColumn<>(timeName);
            timeColumn.setSortType(TableColumn.SortType.DESCENDING);
            timeColumn.setCellValueFactory(new PropertyValueFactory<>(timeField));
            timeColumn.setCellFactory(new Callback<TableColumn<ProfilerTopData, BigDecimal>, TableCell<ProfilerTopData, BigDecimal>>() {
                @Override
                public TableCell<ProfilerTopData, BigDecimal> call(TableColumn<ProfilerTopData, BigDecimal> param) {
                    return new TableCell<ProfilerTopData, BigDecimal>() {
                        @Override
                        protected void updateItem(BigDecimal item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (item != null) {
                                setText(item.setScale(3, RoundingMode.HALF_UP).toString() + " s");
                            } else {
                                setText("");
                            }
                        }
                    };
                }
            });
            
            addColumnToSortOrder(timeColumn);
        }

        addTableColumn(indexColumn, 0.07, 70, false);
        if (threadsField != null) {
            addTableColumn(sourceColumn, 0.53, 250, false);
            addTableColumn(threadsColumn, 0.40, 250, false);
        } else {
            addTableColumn(sourceColumn, 0.93, 250, false);
        }
        
        if (timeField != null) {
            addTableColumn(timeColumn, 0.53, 100, false);
        }

        if (black) {
            chart.setId("blackPieChart");
        } else {
            chart.setId("mypie");
            table.getColumns().addListener(new ListChangeListener<
                TableColumn<ProfilerTopData, ?>>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends TableColumn<ProfilerTopData, ?>> change) {
                    while (change.next()) {
                        if (change.wasAdded()) {
                            for (TableColumn tc : change.getAddedSubList()) {
                                tc.setId("querybuildercolumn");
                            }
                        }
                    }
                }
            });
        }

    }

    public void setItems(List<ProfilerTopData> data) {
        synchronized (table) {
            ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
            int i = 0;
            for (ProfilerTopData p : data) {
                
                if (i == COLORS.length) {
                    i = 0;
                }
                String color = COLORS[i];
                i++;
                
                PieChart.Data pd = new PieChart.Data(p.getSource(), p.getThreads().doubleValue());
                if (pd.getNode() != null) {
                    pd.getNode().setStyle("-fx-pie-color: " + color);
                } else {
                    pd.nodeProperty().addListener(new ChangeListener<Node>() {
                        @Override
                        public void changed(ObservableValue<? extends Node> observable, Node oldValue, Node newValue) {
                            if (newValue != null) {
                                newValue.setStyle("-fx-pie-color: " + color);
                            }
                        }
                    });
                }
                
                pieChartData.add(pd);
            }
            chart.getData().clear();
            chart.setData(pieChartData);

            table.getItems().clear();
            table.getItems().addAll(data);
        }
    }

    public void refresh() {
        table.refresh();
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth) {
        tableColumn.prefWidthProperty().bind(table.widthProperty().multiply(bindingWidth));
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth, double minWidth) {
        addTableColumn(tableColumn, bindingWidth, minWidth, true);
    }

    public void addTableColumn(TableColumn tableColumn, double bindingWidth, double minWidth, boolean inPercent) {
        table.getColumns().add(tableColumn);
        if (inPercent) {
            tableColumn.prefWidthProperty().bind(table.widthProperty().multiply(bindingWidth));
        } else {
            tableColumn.setPrefWidth(bindingWidth);
        }
        tableColumn.setMinWidth(minWidth);
    }

    private void addColumnToSortOrder(TableColumn tableColumn) {
        table.getSortOrder().add(tableColumn);
    }

    private void adjustHeightState(double h) {
        pieContainer.setPrefHeight(h * 0.65);
        table.setPrefHeight(h * 0.35);
    }

    public PieChart getChart() {
        return chart;
    }

    public void makeItBlacken(boolean black) {
        this.black = black;
    }
}
