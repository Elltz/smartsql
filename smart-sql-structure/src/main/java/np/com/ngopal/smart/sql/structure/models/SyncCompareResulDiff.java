package np.com.ngopal.smart.sql.structure.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SyncCompareResulDiff {
    
    public enum Type {
        ENGINE,
        AVG_ROW_LENGTH,
        CHARSET,
        COLLATE,
        COLUMN,
        INDEX,
        FOREIGN,
        FULL
    }
    
    private Type type;
    private Object sourceObject;
    private Object targetObject;
    
    
}
