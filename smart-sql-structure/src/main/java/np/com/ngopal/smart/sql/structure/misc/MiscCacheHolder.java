/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.misc;

import java.util.HashMap;
import java.util.Map;
import javafx.scene.control.Tab;
import np.com.ngopal.smart.sql.ConnectionSession;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class MiscCacheHolder {
    
    private static MiscCacheHolder cacheHolder;
    
    public static MiscCacheHolder getInstance(){
        if(cacheHolder == null){
            cacheHolder = new MiscCacheHolder();
        }
        return cacheHolder;
    }
    
    private Map<ConnectionSession, Tab> slowQueryAnalyzerInstancesCache;    
    private Map<ConnectionSession, Tab> debuggerInstancesCache;
    private Map<ConnectionSession, Tab> tunerSQLInstancesCache;

    public Map<ConnectionSession, Tab> getDebuggerInstancesCache() {
        if(debuggerInstancesCache == null){
            debuggerInstancesCache = new HashMap<>(10);
        }        
        return debuggerInstancesCache;
    }
    
    public Map<ConnectionSession, Tab> getSlowQueryAnalyzerInstancesCache() {
        if(slowQueryAnalyzerInstancesCache == null){
            slowQueryAnalyzerInstancesCache = new HashMap<>(10);
        }        
        return slowQueryAnalyzerInstancesCache;
    }
    
    public Map<ConnectionSession, Tab> getTunerSQLInstancesCache() {
        if(tunerSQLInstancesCache == null){
            tunerSQLInstancesCache = new HashMap<>(10);
        }        
        return tunerSQLInstancesCache;
    }
    
}
