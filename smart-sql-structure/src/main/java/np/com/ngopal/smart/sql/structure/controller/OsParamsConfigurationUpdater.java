/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import com.google.common.base.Objects;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class OsParamsConfigurationUpdater extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Label label;

    @FXML
    private TextField osHost;

    @FXML
    private TextField osPort;

    @FXML
    private TextField osUser;

    @FXML
    private PasswordField osPassword;

    @FXML
    private CheckBox osSavePasswordCheckBox;

    @FXML
    private CheckBox osUsePasswordCheckbox;

    @FXML
    private CheckBox osUsePrivateKeyCheckbox;

    @FXML
    private TextField osPrivateKeyField;

    @FXML
    private Button osOpenPrivateKeyButton;

    @FXML
    private TextField osDiskSizeField;

    @FXML
    private TextField osRamSizeField;

    @FXML
    private Button reconnect;

    private WorkProc authenticationCodeInterface;
    
    @Getter
    @Setter(AccessLevel.PUBLIC)
    private ConnectionParams params;
    
    @Setter(AccessLevel.PUBLIC)
    private ConnectionParamService paramService;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        osHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (getParams() != null && !Objects.equal(getParams().getOsHost(), newValue)) {
                        getParams().setOsHost(newValue);
                    }
                });

        osPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (newValue.matches("\\d*")) {
                        try {
                            int port = Integer.parseInt(newValue);
                            if (getParams() != null && !Objects.equal(getParams().getOsPort(), port)) {
                                getParams().setOsPort(port);
                            }
                        } catch (NumberFormatException ex) {
                            if (getParams() != null && !Objects.equal(getParams().getOsPort(), 0)) {
                                getParams().setOsPort(0);
                            }
                        }
                    }
                });

        osUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (getParams() != null && !Objects.equal(getParams().getOsUser(), newValue)) {
                        getParams().setOsUser(newValue);
                    }
                });

        osDiskSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (getParams() != null && !Objects.equal(getParams().getOsMetricDiskSize(), newValue)) {
                        getParams().setOsMetricDiskSize(newValue);
                    }
                });

        osRamSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (getParams() != null && !Objects.equal(getParams().getOsMetricRamSize(), newValue)) {
                        getParams().setOsMetricRamSize(newValue);
                    }
                });

        osPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (getParams() != null && !Objects.equal(getParams().getOsPassword(), newValue)) {
                        getParams().setOsPassword(newValue);
                    }
                    //osSavePasswordCheckBox.setSelected(pass != null);
                });

        osUsePasswordCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePrivateKeyCheckbox.setSelected(false);
                }
                if (getParams() != null && newValue != null) {
                    getParams().setOsUsePassword(newValue);
                }
            }
        });

        osUsePrivateKeyCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePasswordCheckbox.setSelected(false);
                }
            }
        });

        osPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (getParams() != null && !Objects.equal(getParams().getOsPrivateKey(), newValue)) {
                        getParams().setOsPrivateKey(newValue);
                    }
                });

        reconnect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                refresConfigurationParamsWithOrigin();
                if (authenticationCodeInterface != null) {
                    Recycler.addWorkProc(authenticationCodeInterface);
                }
            }
        });
        
        osPrivateKeyField.disableProperty().bind(osUsePrivateKeyCheckbox.selectedProperty().not());
        osOpenPrivateKeyButton.disableProperty().bind(osUsePrivateKeyCheckbox.selectedProperty().not());
        osPassword.disableProperty().bind(osUsePasswordCheckbox.selectedProperty().not());

        updateFieldsBaseOnParams(getParams());

    }
    
    @FXML
    void openPrivateKey(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PuTTY Private Key Files", "*.ppk"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            osPrivateKeyField.setText(file.getAbsolutePath());
        }
    }

    private void updateFieldsBaseOnParams(ConnectionParams params) {
        // OS part
        osHost.setText(params != null ? params.getOsHost() : "");
        osPort.setText(params != null ? params.getOsPort() + "" : "");
        osUser.setText(params != null ? params.getOsUser() : "");
        osPassword.setText(params != null ? params.getOsPassword() : "");
        osUsePasswordCheckbox.setSelected(params != null && params.isOsUsePassword());
        osUsePrivateKeyCheckbox.setSelected(params != null && !params.isOsUsePassword());
        osPrivateKeyField.setText(params != null ? params.getOsPrivateKey() : "");

        osRamSizeField.setText(params != null ? params.getOsMetricRamSize() : "");
        osDiskSizeField.setText(params != null ? params.getOsMetricDiskSize() : "");
    }

    private ConnectionParams saveCurrentParams() {
        return paramService.update(getParams());
    }

    private void refresConfigurationParamsWithOrigin() {
        setParams(saveCurrentParams());
        getController().getSelectedConnectionSession().get().setConnectionParam(getParams());
    }

    public void setAuthenticationCodeInterface(WorkProc wp) {
        authenticationCodeInterface = wp;
    }

}
