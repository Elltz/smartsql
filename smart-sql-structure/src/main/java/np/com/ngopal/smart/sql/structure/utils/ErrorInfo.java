package np.com.ngopal.smart.sql.structure.utils;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class ErrorInfo {
    
    // TODO maybe only for sql exception
    private Throwable sourceThrowable;
    
    private int errorCode;
    private String errorMessage;
    private String rootCause;
    private String debugInfo;
    private String solutionInfo;
    private List<String> usefulWebsites;
    
    public ErrorInfo(Throwable sourceThrowable) {
        this.sourceThrowable = sourceThrowable;
    }
}
