package np.com.ngopal.smart.sql.structure.controller;

import np.com.ngopal.smart.sql.structure.misc.ProfilerChartControllerEventHandler;
import np.com.ngopal.smart.sql.structure.misc.ProfilerInnerControllerWorkProcAuth;
import javafx.fxml.Initializable;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.GSChartVariable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import np.com.ngopal.smart.sql.model.OsServerSpecs;
import np.com.ngopal.smart.sql.model.SlaveStatus;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class MysqlReplicationController extends BaseController implements Initializable, ProfilerListener {

    private ProfilerDataManager dbProfilerService;
    private ProfilerChartService chartService;

    private Long firstRunDate;

    private final ObjectProperty<Long> hoveredX = new SimpleObjectProperty<>(0l);

    public ObjectProperty<Long> hoveredX() {
        return hoveredX;
    }

    private final BooleanProperty hoveredOnChart = new SimpleBooleanProperty(false);

    public BooleanProperty hoveredOnChart() {
        return hoveredOnChart;
    }

    @Getter
    private ProfilerTabContentController parentController;

    public MysqlReplicationController() {
    }

    private SlaveStatus changedSlaveStatus = new SlaveStatus();

    private final Map<XYChart.Series<Number, Number>, String> seriesColors = new HashMap<>();
    private final Map<XYChart.Series<String, Number>, String> barSeriesColors = new HashMap<>();

    @FXML
    private AnchorPane mainUI;

    @Getter
    @FXML
    private BorderPane chartContainer;

    @FXML
    private VBox flowContainer;

    @FXML
    private ScrollPane scrollContainer;

    @FXML
    private FlowPane topHorizontalFlatFlowPane;

    private TabularBannerNodeController ioThreadRunningBannerController;
    private TabularBannerNodeController sqlThreadRunningBannerController;
    private TabularBannerNodeController replictionErrorNoBannerController;
    private TabularBannerNodeController readOnlyBannerController;

    //private WorkProc allBackgroundTask;
    private ProfilerInnerControllerWorkProcAuth mysqlReplicationControllerworkProcAuth;

    private Long dragedStart;
    private Long dragedFinish;

    private final List<ProfilerChartController> chartTabs = new ArrayList<>();
    private final List<ProfilerBarChartController> barChartTabs = new ArrayList<>();

    private final Map<String, TitledPane> titledPanes = new HashMap<>();

    private Date startDate;

    public void openData(ProfilerData data) {

        firstRunDate = (long) data.getLower();

        for (ProfilerChartController t : chartTabs) {
            t.clear();
            if (t.isActive()) {
                t.initXAxis(data.getUpper(), data.getLower());
            }
        }

        for (ProfilerBarChartController t : barChartTabs) {
            t.clear();
            if (t.isActive()) {
                t.initXAxis(data.getUpper(), data.getLower());
            }
        }

        firstRunDate = null;

        Map<String, List<GSChart>> chartsMap = new HashMap<>();
        Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = initSeries(chartsMap);

        initChartData(chartsMap, chartSeriesMap, data.getListChangedSlaveStatus());
    }

    @Override
    public void userChartsChanged() {
    }

    @Override
    public void zoomInAction() {

        clearDraggedData();
        updateCharts();
    }

    @Override
    public void scrollForward() {

        Long diff = dragedFinish - dragedStart;
        dragedStart += diff;
        dragedFinish += diff;

        updateCharts();
    }

    @Override
    public void scrollBackward() {

        Long diff = dragedFinish - dragedStart;
        dragedStart -= diff;
        dragedFinish -= diff;

        updateCharts();
    }

    public void clearDraggedData() {
        dragedStart = null;
        dragedFinish = null;
    }

    public void zoomDragedAction(Long start, Long finish) {

        parentController.setShowAll(false);

        dragedStart = start;
        dragedFinish = finish;

        updateCharts();
    }

    @Override
    public void zoomOutAction() {

        clearDraggedData();
        updateCharts();
    }

    @Override
    public void showAllAction() {

        clearDraggedData();
        updateCharts();
    }

    public void updateCharts() {
        if (chartTabs != null) {
            for (ProfilerChartController chartController : chartTabs) {
                if (chartController.isActive()) {
                    chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                }
            }
        }

        if (barChartTabs != null) {
            for (ProfilerBarChartController chartController : barChartTabs) {
                if (chartController.isActive()) {
                    chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                }
            }
        }

        sqlThreadRunningBannerController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
    }

    public void saveProfilerData(ProfilerData pd) {
    }

    @Override
    public void startProfiler(Date startDate) {
        this.startDate = startDate;

        for (ProfilerChartController chartController : chartTabs) {
            chartController.clear();
        }

        for (ProfilerBarChartController chartController : barChartTabs) {
            chartController.clear();
        }

        sqlThreadRunningBannerController.clear();

        firstRunDate = null;

        createAndInitChartThread();
    }

    @Override
    public void clearProfilerData() {

        for (ProfilerChartController chartController : chartTabs) {
            chartController.clear();
        }

        for (ProfilerBarChartController chartController : barChartTabs) {
            chartController.clear();
        }

        sqlThreadRunningBannerController.clear();

        firstRunDate = null;
    }

    private Map<Long, SlaveStatus> readData(Map<Long, SlaveStatus> result, Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData, Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap) {

        // 3000 - 3 sec, we thinking that if dif time more then 3 sec - then we add empty zone 
        for (Long dateTime : result.keySet()) {
            try {

                // collect Chart data
                synchronized (parentController) {

                    SlaveStatus data = result.get(dateTime);

                    //log.error("areaChartThread ********************************");
//                    for (String s: data.keySet()) {
//
//                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
//                        if (chartList != null) {
//                            for (GSChart chart : chartList) {
//
//                                XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
//
//                                if (chartSeries != null) {
//                                    List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
//                                    if (listSeriesData == null) {
//                                        listSeriesData = new ArrayList<>();
//                                        chartSeriesData.put(chartSeries, listSeriesData);
//                                    }
//                                    listSeriesData.add(new XYChart.Data(dateTime, data.getValue(s)));
//                                }
//                            }
//                        }
//                    }
                    for (GSChart chart : chartSeriesMap.keySet()) {
                        if (chart.getVariables() != null) {
                            for (GSChartVariable v : chart.getVariables()) {

                                if (v != null && v.getVariable() != null && v.getVariable() == 0) {

                                    String[] vnames = v.getVariableName().split("\\s+");

                                    boolean found = false;
                                    if (vnames.length == 1) {
                                        for (String k : data.keySet()) {
                                            if (k.equalsIgnoreCase(v.getVariableName())) {
                                                found = true;
                                            }
                                        }
                                    }

                                    if (!found) {
                                        XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(v.getVariableName().toUpperCase());
                                        if (chartSeries != null) {
                                            List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                            if (listSeriesData == null) {
                                                listSeriesData = new ArrayList<>();
                                                chartSeriesData.put(chartSeries, listSeriesData);
                                            }

                                            if (vnames.length >= 2) {

                                                String function = vnames[0].trim();
                                                String name = vnames[1].trim().toUpperCase();

                                                double value = 0l;
                                                double diff = 0l;
                                                if ("AVG".equalsIgnoreCase(function)) {
                                                    List<GSChart> gsc = chartsMap.get(name);
                                                    if (gsc != null && !gsc.isEmpty()) {
                                                        Map<String, XYChart.Series<Number, Number>> ss = chartSeriesMap.get(gsc.get(0));
                                                        if (ss != null && ss.containsKey(name)) {

                                                            long avg = 0l;
                                                            List<Data<Number, Number>> list = ss.get(name).getData();
                                                            int size = list.size();

                                                            for (Data<Number, Number> d : list) {
                                                                avg += d.getYValue().longValue();
                                                            }

                                                            long prevValue = size > 0 ? avg / size : 0l;

                                                            if (data.containsKey(name)) {
                                                                avg += (Long) data.getValue(name);
                                                                size++;
                                                            }

                                                            value = avg / size;
                                                            diff = value - prevValue;
                                                        }
                                                    }
                                                } else if ("DIVIDE".equalsIgnoreCase(function) && vnames.length >= 3) {

                                                    String divider = vnames[2].toUpperCase();

                                                    double prevValue = 0l;
                                                    double dividerValue = 0l;
                                                    double currentValue = 0l;
                                                    SlaveStatus old = data;
                                                    if (old != null) {
                                                        prevValue = (Double) old.getValue(name);
                                                    }

                                                    if (data.containsKey(name)) {
                                                        currentValue = (Double) data.getValue(name);
                                                    }

                                                    if (data.containsKey(divider)) {
                                                        dividerValue = (Double) data.getValue(divider);
                                                    }

                                                    if (dividerValue > 0) {
                                                        value = currentValue / dividerValue;

                                                        if (vnames.length == 4) {
                                                            try {
                                                                value *= Long.valueOf(vnames[3]);
                                                            } catch (NumberFormatException ex) {
                                                            }
                                                        }

                                                        diff = value - prevValue;
                                                    }
                                                }

                                                listSeriesData.add(new XYChart.Data(dateTime, v.getVariable() != null && v.getVariable() == 1 ? diff : value));
                                            } else {
                                                listSeriesData.add(new XYChart.Data(dateTime, 0.0));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable ex) {
                log.error(ex.getMessage(), ex);
            }
        }

        return result;
    }

    private Map<Long, SlaveStatus> convertToHourly(Map<Long, SlaveStatus> map, Map<Long, Integer> hourlyCounts) {
        Map<Long, SlaveStatus> result = new LinkedHashMap<>();

        Map<Long, Integer> resultCount = new LinkedHashMap<>();

        int hour = 60 * 60 * 1000;
        for (Long time : map.keySet()) {
            SlaveStatus m = map.get(time);
            if (m == ProfilerDataManager.EMPTY_ENGINE_INNODB_STATUSES) {
                continue;
            }

            Long hourlyTime = (time / hour) * hour;

            SlaveStatus hourMap = result.get(hourlyTime);
            Integer mCount = resultCount.get(hourlyTime);
            if (hourMap == null) {
                result.put(hourlyTime, m.copyValue());
                resultCount.put(hourlyTime, 1);
                continue;
            }
            
            hourMap.setMaster_Port(hourMap.getMaster_Port() * mCount + m.getMaster_Port());
            hourMap.setConnect_Retry(hourMap.getConnect_Retry() * mCount + m.getConnect_Retry());
            hourMap.setRead_Master_Log_Pos(hourMap.getRead_Master_Log_Pos() * mCount + m.getRead_Master_Log_Pos());
            hourMap.setRelay_Log_Pos(hourMap.getRelay_Log_Pos() * mCount + m.getRelay_Log_Pos());
            hourMap.setLast_Errno(hourMap.getLast_Errno() * mCount + m.getLast_Errno());
            hourMap.setSkip_Counter(hourMap.getSkip_Counter() * mCount + m.getSkip_Counter());
            hourMap.setExec_Master_Log_Pos(hourMap.getExec_Master_Log_Pos() * mCount + m.getExec_Master_Log_Pos());
            hourMap.setRelay_Log_Space(hourMap.getRelay_Log_Space() * mCount + m.getRelay_Log_Space());
            hourMap.setUntil_Log_Pos(hourMap.getUntil_Log_Pos() * mCount + m.getUntil_Log_Pos());
            hourMap.setSeconds_Behind_Master(hourMap.getSeconds_Behind_Master() * mCount + m.getSeconds_Behind_Master());
            hourMap.setLast_IO_Errno(hourMap.getLast_IO_Errno() * mCount + m.getLast_IO_Errno());
            hourMap.setLast_SQL_Errno(hourMap.getLast_SQL_Errno() * mCount + m.getLast_SQL_Errno());
            hourMap.setSQL_Delay(hourMap.getSQL_Delay() * mCount + m.getSQL_Delay());
            hourMap.setSQL_Remaining_Delay(hourMap.getSQL_Remaining_Delay() * mCount + m.getSQL_Remaining_Delay());
            hourMap.setMaster_Retry_Count(hourMap.getMaster_Retry_Count() * mCount + m.getMaster_Retry_Count());
            hourMap.setAuto_Position(hourMap.getAuto_Position() * mCount + m.getAuto_Position());
            resultCount.put(hourlyTime, mCount + 1);
        }

        for (Long time : result.keySet()) {

            Integer count = resultCount.get(time);
            SlaveStatus hourMap = result.get(time);

            hourMap.setMaster_Port(hourMap.getMaster_Port() / count);
            hourMap.setConnect_Retry(hourMap.getConnect_Retry() / count);
            hourMap.setRead_Master_Log_Pos(hourMap.getRead_Master_Log_Pos() / count);
            hourMap.setRelay_Log_Pos(hourMap.getRelay_Log_Pos() / count);
            hourMap.setLast_Errno(hourMap.getLast_Errno() / count);
            hourMap.setSkip_Counter(hourMap.getSkip_Counter() / count);
            hourMap.setExec_Master_Log_Pos(hourMap.getExec_Master_Log_Pos() / count);
            hourMap.setRelay_Log_Space(hourMap.getRelay_Log_Space() / count);
            hourMap.setUntil_Log_Pos(hourMap.getUntil_Log_Pos() / count);
            hourMap.setSeconds_Behind_Master(hourMap.getSeconds_Behind_Master() / count);
            hourMap.setLast_IO_Errno(hourMap.getLast_IO_Errno() / count);
            hourMap.setLast_SQL_Errno(hourMap.getLast_SQL_Errno() / count);
            hourMap.setSQL_Delay(hourMap.getSQL_Delay() / count);
            hourMap.setSQL_Remaining_Delay(hourMap.getSQL_Remaining_Delay() / count);
            hourMap.setMaster_Retry_Count(hourMap.getMaster_Retry_Count() / count);
            hourMap.setAuto_Position(hourMap.getAuto_Position() / count);

            hourlyCounts.put(time, count);
        }

        return result;
    }

    private Map<Long, SlaveStatus> readHourlyData(Map<Long, SlaveStatus> ssData, Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> chartSeriesData, Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap, Map<Long, Integer> hourlyCounts) {

        ssData = convertToHourly(ssData, hourlyCounts);

        for (Long dateTime : ssData.keySet()) {
            try {

                // collect Chart data
                synchronized (parentController) {

                    SlaveStatus data = ssData.get(dateTime);

                    //log.error("areaChartThread ********************************");
//                    for (String s: data.keySet()) {
//
//                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
//                        if (chartList != null) {
//                            for (GSChart chart : chartList) {
//
//                                XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
//
//                                if (chartSeries != null) {
//                                    List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
//                                    if (listSeriesData == null) {
//                                        listSeriesData = new ArrayList<>();
//                                        chartSeriesData.put(chartSeries, listSeriesData);
//                                    }
//                                    listSeriesData.add(new XYChart.Data(ProfilerBarChartController.formatToXValue(dateTime), data.getValue(s)));
//                                }
//                            }
//                        }
//                    }
                    for (GSChart chart : chartSeriesMap.keySet()) {
                        if (chart.getVariables() != null) {
                            for (GSChartVariable v : chart.getVariables()) {

                                if (v != null && v.getVariable() != null && v.getVariable() == 0) {

                                    String[] vnames = v.getVariableName().split("\\s+");

                                    boolean found = false;
                                    if (vnames.length == 1) {
                                        for (String k : data.keySet()) {
                                            if (k.equalsIgnoreCase(v.getVariableName())) {
                                                found = true;
                                            }
                                        }
                                    }

                                    if (!found) {
                                        XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(v.getVariableName().toUpperCase());
                                        if (chartSeries != null) {
                                            List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                            if (listSeriesData == null) {
                                                listSeriesData = new ArrayList<>();
                                                chartSeriesData.put(chartSeries, listSeriesData);
                                            }

                                            listSeriesData.add(new XYChart.Data(ProfilerBarChartController.formatToXValue(dateTime), 0.0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable ex) {
                log.error(ex.getMessage(), ex);
            }
        }

        return ssData;
    }

    public void showProfilerData(Date dateFrom) {

        firstRunDate = null;

        makeBusy(true);
        getController().addBackgroundWork(new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                Map<String, List<GSChart>> chartsMap = new HashMap<>();
                Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = initSeries(chartsMap);
                Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();
                Platform.runLater(() -> {

                    for (XYChart.Series<Number, Number> series : chartSeriesData.keySet()) {
                        series.getData().addAll(chartSeriesData.get(series));
                    }

                    for (ProfilerChartController chartController : chartTabs) {
                        if (chartController.isActive()) {
                            chartController.clear();
                            chartController.init(chartSeriesMap.get(chartController.getChart()).values(), seriesColors);
                            chartController.initXAxis(new Date().getTime(), dateFrom.getTime());
                            chartController.updateChart(true, parentController.zoomCountStage(), dateFrom.getTime(), dragedStart, dragedFinish);
                        }
                    }
                    makeBusy(false);
                });
            }
        });
    }

    @Override
    public void stopProfiler() {
        startDate = null;
    }

    @Override
    public void destroy() {
        stopProfiler();
        System.out.println("destroyController() called");
        keepThreadsAlive = false;
        changedSlaveStatus = null;

        chartService = null;
        chartTabs.clear();
        titledPanes.clear();
    }

    public void createConnection() throws SQLException {
        // clear old data
        dbProfilerService.clearOldSSData(getParentController().getCurrentSettings());

        createChartThread();
        reinitChartTabs();
    }

    public void updateUserCharts() {
        usersChartsWasChanged = true;
        createChartThread();
        reinitChartTabs();
    }

    public ConnectionParams getConnectionParams() {
        return getParentController().getParams();
    }

    private volatile boolean keepThreadsAlive = true;
    private volatile boolean usersChartsWasChanged = false;

    private void initChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap, Map<Long, SlaveStatus> data) {
        try {
            //List<XYChart.Series<Number, Number>> seriesList = new ArrayList<>();
            //Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> map = new HashMap<>();

            Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData = new HashMap<>();

            // collect Chart data
            synchronized (parentController) {

                for (Long time : data.keySet()) {

                    Date date = new Date(time);
                    if (firstRunDate == null) {
                        firstRunDate = date.getTime();
                    }

                    SlaveStatus map = data.get(time);

                    //log.error("areaChartThread ********************************");
                    for (String s : map.keySet()) {

                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {
                                if (chart.getHourly() != 1) {
                                    XYChart.Series<Number, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<Number, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }
                                        listSeriesData.add(new XYChart.Data(date.getTime(), map.getValue(s)));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //log.error("areaChartThread ===============================");
            Platform.runLater(() -> {

                for (XYChart.Series<Number, Number> series : chartSeriesData.keySet()) {
                    series.getData().addAll(chartSeriesData.get(series));
                }

                for (ProfilerChartController chartController : chartTabs) {
                    if (chartController.isActive()) {
                        chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                    }
                }
            });

        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }

    private void initBarChartData(Map<String, List<GSChart>> chartsMap, Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap, Map<Long, SlaveStatus> data, Map<Long, Integer> hourlyCounts) {
        try {

            Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> chartSeriesData = new HashMap<>();

            // collect Chart data
            synchronized (parentController) {

                long hour = 60 * 60 * 1000;
                for (Long time : data.keySet()) {

                    Date date = new Date(time);
                    if (firstRunDate == null) {
                        firstRunDate = date.getTime();
                    }

                    SlaveStatus map = data.get(time);

                    //log.error("areaChartThread ********************************");
                    for (String s : map.keySet()) {

                        List<GSChart> chartList = chartsMap.get(s.toUpperCase());
                        if (chartList != null) {
                            for (GSChart chart : chartList) {
                                if (chart.getHourly() == 1) {
                                    XYChart.Series<String, Number> chartSeries = chartSeriesMap.get(chart).get(s.toUpperCase());
                                    if (chartSeries != null) {
                                        List<XYChart.Data<String, Number>> listSeriesData = chartSeriesData.get(chartSeries);
                                        if (listSeriesData == null) {
                                            listSeriesData = new ArrayList<>();
                                            chartSeriesData.put(chartSeries, listSeriesData);
                                        }
                                        String xValue = ProfilerBarChartController.formatToXValue((long) (date.getTime() / hour) * hour);

                                        listSeriesData.add(new XYChart.Data(xValue, map.getValue(s)));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //log.error("areaChartThread ===============================");
            Platform.runLater(() -> {

                for (XYChart.Series<String, Number> series : chartSeriesData.keySet()) {

                    String propertyKey = null;
                    for (Map<String, XYChart.Series<String, Number>> ss : chartSeriesMap.values()) {

                        for (String s : ss.keySet()) {
                            if (series.equals(ss.get(s))) {
                                propertyKey = s;
                                break;
                            }

                            if (propertyKey != null) {
                                break;
                            }
                        }
                    }

                    for (Data<String, Number> d : chartSeriesData.get(series)) {

                        Integer v = hourlyCounts.get(ProfilerBarChartController.formatFromXValue(d.getXValue()));

                        boolean found = false;
                        for (Data<String, Number> d0 : series.getData()) {

                            if (d0.getXValue().equals(d.getXValue())) {

                                if (propertyKey != null) {
                                    if (v == null) {
                                        v = 0;
                                    }

                                    hourlyCounts.put(ProfilerBarChartController.formatFromXValue(d.getXValue()), v + 1);

                                    double value = ((d0.getYValue().doubleValue() * v) + d.getYValue().doubleValue()) / (double) (v + 1);
                                    d0.setYValue(value);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found) {
                            if (v == null) {
                                v = 1;
                            }
                            hourlyCounts.put(ProfilerBarChartController.formatFromXValue(d.getXValue()), v);
                            series.getData().add(d);
                        }
                    }
                }

                for (ProfilerBarChartController chartController : barChartTabs) {
                    if (chartController.isActive()) {
                        chartController.updateChart(parentController.isShowAll(), parentController.zoomCountStage(), firstRunDate, dragedStart, dragedFinish);
                    }
                }
            });

        } catch (Throwable th) {
            log.error(th.getMessage(), th);
        }
    }

    private Map<GSChart, Map<String, XYChart.Series<Number, Number>>> initSeries(Map<String, List<GSChart>> chartsMap) {
        seriesColors.clear();
        Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap = new HashMap<>();

        List<GSChart> charts = chartService.getAll("Replication");
        for (GSChart chart : charts) {

            if (chart.isStatus() && chart.getHourly() != 1) {
                Map<String, XYChart.Series<Number, Number>> map = new LinkedHashMap<>();
                if (chart.getVariables() != null) {
                    for (GSChartVariable v : chart.getVariables()) {
                        String value = v.getVariableName().toUpperCase();
                        //log.error(" variable: " + value);
                        List<GSChart> list = chartsMap.get(value);
                        if (list == null) {
                            list = new ArrayList<>();
                            chartsMap.put(value, list);
                        }
                        list.add(chart);

                        final XYChart.Series series = new XYChart.Series();
                        series.setName(v.getDisplayName());

                        if (!"Random".equals(v.getDisplayColor())) {
                            seriesColors.put(series, v.getDisplayColor());
                        }

                        map.put(value, series);
                    }
                }

                chartSeriesMap.put(chart, map);
            }
        }

        return chartSeriesMap;
    }

    private Map<GSChart, Map<String, XYChart.Series<String, Number>>> initBarSeries(Map<String, List<GSChart>> chartsMap) {
        barSeriesColors.clear();
        Map<GSChart, Map<String, XYChart.Series<String, Number>>> chartSeriesMap = new HashMap<>();

        List<GSChart> charts = chartService.getAll("Replication");
        for (GSChart chart : charts) {

            if (chart.isStatus() && chart.getHourly() == 1) {
                Map<String, XYChart.Series<String, Number>> map = new LinkedHashMap<>();
                if (chart.getVariables() != null) {
                    for (GSChartVariable v : chart.getVariables()) {
                        String value = v.getVariableName().toUpperCase();
                        //log.error(" variable: " + value);
                        List<GSChart> list = chartsMap.get(value);
                        if (list == null) {
                            list = new ArrayList<>();
                            chartsMap.put(value, list);
                        }
                        list.add(chart);

                        final XYChart.Series series = new XYChart.Series();
                        series.setName(v.getDisplayName());

                        if (!"Random".equals(v.getDisplayColor())) {
                            barSeriesColors.put(series, v.getDisplayColor());
                        }

                        map.put(value, series);
                    }
                }

                chartSeriesMap.put(chart, map);
            }
        }

        return chartSeriesMap;
    }

    public OsServerSpecs getCurrentServerSpecs() {
        return null;
    }

    private void reinitChartTabs() {
        Platform.runLater(() -> {
            for (ProfilerChartController chartController : chartTabs) {
                chartController.clear();
            }

            for (ProfilerBarChartController chartController : barChartTabs) {
                chartController.clear();
            }

            sqlThreadRunningBannerController.clear();

            firstRunDate = null;
            //chartsTabPane.getTabs().clear();
            chartTabs.clear();
            barChartTabs.clear();
            titledPanes.clear();

//            GSChart defaultChart = new GSChart();
//            defaultChart.setChartName("Default chart");
//            ProfilerChartController pcc = loadProfilerChartEntityMetricControllers(defaultChart);
//            flowContainer.getChildren().add(0, pcc.getUI());
//            chartTabs.add(pcc);
//            Utitlities.prepFlowPaneDimensionConditionWithParent(flowContainer, 1.0, true);
            List<GSChart> charts = chartService.getAll("Replication");

            VBox lastVbox = null;
            for (GSChart chart : charts) {
                if (chart.isStatus()) {

                    HBox box = null;
                    VBox vbox = null;

                    if (chart.getGroupName() != null && !chart.getGroupName().isEmpty()) {
                        TitledPane tp = titledPanes.get(chart.getGroupName());
                        if (tp == null) {
                            tp = new TitledPane();
                            tp.setFocusTraversable(false);
                            tp.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            tp.setText(chart.getGroupName());

                            VBox vx = new VBox(5);
                            vx.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vx.setPadding(new Insets(-3.0));
                            vx.setAlignment(Pos.CENTER);
                            tp.setContent(vx);

                            flowContainer.getChildren().add(0, tp);

                            titledPanes.put(chart.getGroupName(), tp);
                        }

                        vbox = (VBox) tp.getContent();

                    } else if (lastVbox == null) {
                        vbox = new VBox(5);
                        lastVbox = vbox;
                        flowContainer.getChildren().add(0, vbox);
                    } else {
                        vbox = lastVbox;
                    }

                    if (vbox.getChildren().isEmpty()) {
                        box = new HBox(5);
                        box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                        vbox.getChildren().add(0, box);
                    } else {

                        HBox oldBox = (HBox) vbox.getChildren().get(0);

                        if (chart.getGroupWidth() == 2) {
                            box = new HBox(5);
                            box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vbox.getChildren().add(0, box);
                        } else if (oldBox.getChildren().size() == 2 || ((GSChart) oldBox.getUserData()).getGroupWidth() == 2) {
                            box = new HBox(5);
                            box.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
                            vbox.getChildren().add(0, box);
                        }

                        if (box == null) {
                            box = oldBox;
                        } else if (oldBox.getChildren().size() == 1 && ((GSChart) oldBox.getUserData()).getGroupWidth() == 1) {
                            Label empty = new Label();
                            empty.setMaxWidth(Double.MAX_VALUE);
                            oldBox.getChildren().add(empty);
                            HBox.setHgrow(empty, Priority.ALWAYS);
                        }
                    }

                    box.setAlignment(Pos.CENTER);
                    box.setUserData(chart);

                    if (chart.getHourly() == 1) {
                        ProfilerBarChartController pcc = loadProfilerBarChartEntityMetricControllers(chart);
                        box.getChildren().add(0, pcc.getUI());

                        pcc.getUI().setMaxWidth(Double.MAX_VALUE);
                        HBox.setHgrow(pcc.getUI(), Priority.ALWAYS);

                        barChartTabs.add(pcc);
                    } else {
                        ProfilerChartController pcc = loadProfilerChartEntityMetricControllers(chart);
                        box.getChildren().add(0, pcc.getUI());

                        pcc.getUI().setMaxWidth(Double.MAX_VALUE);
                        HBox.setHgrow(pcc.getUI(), Priority.ALWAYS);

                        chartTabs.add(pcc);
                    }
                }
            }
            Platform.runLater(() -> usersChartsWasChanged = false);
        });
    }

    private void createAndInitChartThread() {
        createChartThread();
        // THIS NOT WORK WITH THIS BACKGROUND THREADS
        // SOME TIMES IT JUST IGNORING WORKPROC, 
        // ALSO WE HAVE ONLY 3 THREAD - THIS IS NOT ENOUGH
        //Recycler.getLessBusySWP().addWorkProc(allBackgroundTask);
//        
//        Thread th = new Thread(() -> {
//            allBackgroundTask.run();
//        }, "MYSqlProfiling");
//        th.setDaemon(true);
//        th.start();
    }

    private void createChartThread() {

        changedSlaveStatus = new SlaveStatus();
        mysqlReplicationControllerworkProcAuth = new ProfilerInnerControllerWorkProcAuth() {

            // generate server hash code
            final String serverHashCode = serverHashCode(getConnectionParams(), "");
            final String slaveStatusQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_SLAVE_STATUS);
            long lastOverallCodeExecutionRun = 0l;
            long lastUpdateExecutionRun = 0l;

            Map<Long, SlaveStatus> changedCache;
            Map<String, List<GSChart>> chartsMap;
            Map<String, List<GSChart>> barChartsMap;
            Map<Long, Integer> hourlyCounts;
            boolean first = true;
            Date currentDate;
            Date readDate;
            Map<Long, SlaveStatus> ssData;
            Map<XYChart.Series<Number, Number>, List<XYChart.Data<Number, Number>>> chartSeriesData;
            Map<GSChart, Map<String, XYChart.Series<Number, Number>>> chartSeriesMap;
            Map<Long, SlaveStatus> result;
            Map<XYChart.Series<String, Number>, List<XYChart.Data<String, Number>>> barChartSeriesData;
            Map<GSChart, Map<String, XYChart.Series<String, Number>>> barChartSeriesMap;
            Map<Long, SlaveStatus> barResult;
            Map<String, GSChartVariable> variablesNames;
            Long lastDateTime;
            String readOnly;

            @Override
            public boolean shouldExecuteWorkProc() {
                return parentController.isDataLoaded() && getParentController().profilerStarted().get() && keepThreadsAlive && !usersChartsWasChanged
                        && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastOverallCodeExecutionRun) >= 1;
            }

            @Override
            public void runWork() {
                if (chartSeriesMap == null) {
                    changedCache = new LinkedHashMap<>();
                    chartsMap = new HashMap<>();
                    barChartsMap = new HashMap<>();
                    hourlyCounts = new HashMap<>();
                    currentDate = new Date();
                    readDate = new Date(currentDate.getTime() - ChartScale.SCALE_8H.getValue());
                    ssData = dbProfilerService.selectSSDistinction(serverHashCode(getConnectionParams(), ""), readDate, ProfilerDataManager.EMPTY_DELAY__SS, currentDate);
                    chartSeriesData = new HashMap<>();
                    chartSeriesMap = initSeries(chartsMap);
                    result = readData(ssData, chartSeriesData, chartsMap, chartSeriesMap);
                    barChartSeriesData = new HashMap<>();
                    barChartSeriesMap = initBarSeries(barChartsMap);
                    barResult = readHourlyData(ssData, barChartSeriesData, barChartsMap, barChartSeriesMap, hourlyCounts);
                    variablesNames = chartService.getVariablesNames("Replication");
                    lastDateTime = null;
                    readOnly = getParentController().getStringGlobalVariable("READ_ONLY");
                    firstRunDate = null;

                    Platform.runLater(() -> {
//                        for (XYChart.Series<Number, Number> series: chartSeriesData.keySet()) {
//                            series.getData().addAll(chartSeriesData.get(series));
//                        }

//                        for (XYChart.Series<String, Number> series: barChartSeriesData.keySet()) {
//                            series.getData().addAll(barChartSeriesData.get(series));
//                        }
                        for (ProfilerChartController chartController : chartTabs) {
                            if (chartController.isActive()) {
                                chartController.clear();
                            }
                        }

                        for (ProfilerBarChartController chartController : barChartTabs) {
                            if (chartController.isActive()) {
                                chartController.clear();
                            }
                        }

                        initChartData(chartsMap, chartSeriesMap, result);
                        initBarChartData(barChartsMap, barChartSeriesMap, barResult, hourlyCounts);
                    });

                    if ("ON".equalsIgnoreCase(readOnly)) {
                        Platform.runLater(() -> {
                            readOnlyBannerController.getContentTextProperty().set("On");
                            readOnlyBannerController.getContentTextLabel().setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20;");
                        });

                    } else {
                        Platform.runLater(() -> {
                            readOnlyBannerController.getContentTextProperty().set("No");
                            readOnlyBannerController.getContentTextLabel().setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
                        });
                    }
                }

                synchronized (parentController) {
                    // prepare table for global status values
                    dbProfilerService.prepareSlaveStatusTables(serverHashCode, currentDate);

                    Map<Long, SlaveStatus> lastSS = dbProfilerService.selectLastSS(serverHashCode, startDate);
                    Long maxTime = startDate != null ? startDate.getTime() : 0;
                    for (Long time : lastSS.keySet()) {
                        if (time > maxTime) {
                            maxTime = time;
                        }

                        changedSlaveStatus = lastSS.get(time);
                        Platform.runLater(() -> {
                            resetBanners();

                            if (changedSlaveStatus != null) {

                                if (changedSlaveStatus.getSlave_IO_Running() != null && "Yes".equalsIgnoreCase(changedSlaveStatus.getSlave_IO_Running())) {
                                    ioThreadRunningBannerController.getContentTextProperty().set("Yes");
                                    ioThreadRunningBannerController.getContainer().setStyle("-fx-background-color: green;");
                                    ioThreadRunningBannerController.getTitleLabel().setStyle("-fx-background-color: green;");
                                    ioThreadRunningBannerController.getInfo().setStyle("-fx-background-color: green;");
                                    ioThreadRunningBannerController.getContentTextLabel().setStyle("-fx-background-color: green; -fx-font-weight: bold; -fx-font-size: 20;");
                                } else {
                                    ioThreadRunningBannerController.getContentTextProperty().set(changedSlaveStatus.getSlave_IO_Running());
                                }

                                sqlThreadRunningBannerController.getContentTextProperty().set("Yes");
                                sqlThreadRunningBannerController.getContainer().setStyle("-fx-background-color: green;");
                                sqlThreadRunningBannerController.getTitleLabel().setStyle("-fx-background-color: green;");
                                sqlThreadRunningBannerController.getInfo().setStyle("-fx-background-color: green;");
                                sqlThreadRunningBannerController.getContentTextLabel().setStyle("-fx-background-color: green; -fx-font-weight: bold; -fx-font-size: 20;");
                            }
                        });

                        if (changedSlaveStatus.getLast_SQL_Errno() == null || changedSlaveStatus.getLast_SQL_Errno() == 0) {
                            Platform.runLater(() -> {
                                replictionErrorNoBannerController.getContentTextProperty().set("N/A");
                                replictionErrorNoBannerController.getContentTextLabel().setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20;");
                            });
                        } else {
                            Platform.runLater(() -> {
                                replictionErrorNoBannerController.getContentTextProperty().set("" + changedSlaveStatus.getLast_SQL_Errno());
                                replictionErrorNoBannerController.getContentTextLabel().setStyle("-fx-text-fill: red; -fx-font-weight: bold; -fx-font-size: 20;");
                            });
                        }

                        // need add with 0 all not found
                        for (String key : variablesNames.keySet()) {

                            GSChartVariable v = variablesNames.get(key);
                            if (v != null && v.getVariable() != null && v.getVariable() == 0) {

                                String[] names = v.getVariableName().split("\\s+");

                                boolean found = false;
                                if (names.length == 1) {
                                    for (String k : changedSlaveStatus.keySet()) {
                                        if (k.equalsIgnoreCase(key)) {
                                            found = true;
                                        }
                                    }
                                }

                                if (!found) {
                                    if (names.length >= 2) {
                                        String function = names[0].trim();
                                        String name = names[1].trim().toUpperCase();

                                        double value = 0l;
                                        if ("AVG".equalsIgnoreCase(function)) {
                                            List<GSChart> gsc = chartsMap.get(name);
                                            if (gsc != null && !gsc.isEmpty()) {
                                                Map<String, XYChart.Series<Number, Number>> ss = chartSeriesMap.get(gsc.get(0));
                                                if (ss != null && ss.containsKey(name)) {

                                                    long avg = 0l;
                                                    List<Data<Number, Number>> list = ss.get(name).getData();
                                                    int size = list.size();

                                                    for (Data<Number, Number> data : list) {
                                                        avg += data.getYValue().longValue();
                                                    }

                                                    if (changedSlaveStatus.containsKey(name)) {
                                                        avg += (Double) changedSlaveStatus.getValue(name);
                                                        size++;
                                                    }

                                                    value = avg / size;
                                                }
                                            }
                                        } else if ("DIVIDE".equalsIgnoreCase(function) && names.length >= 3) {

                                            String divider = names[2].toUpperCase();

                                            double dividerValue = 0l;
                                            double currentValue = 0l;

                                            if (changedSlaveStatus.containsKey(name)) {
                                                currentValue = (Double) changedSlaveStatus.getValue(name);
                                            }

                                            if (changedSlaveStatus.containsKey(divider)) {
                                                dividerValue = (Double) changedSlaveStatus.getValue(divider);
                                            }

                                            if (dividerValue > 0) {
                                                value = currentValue / dividerValue;

                                                if (names.length == 4) {
                                                    try {
                                                        value *= Long.valueOf(names[3]);
                                                    } catch (NumberFormatException ex) {
                                                    }
                                                }
                                            }
                                        }

                                        changedSlaveStatus.putAdditionValue(key, value);

                                    } else {
                                        changedSlaveStatus = new SlaveStatus();
                                    }
                                }
                            }
                        }

                        if (lastDateTime == null) {
                            changedCache.put(time - 1000, ProfilerDataManager.EMPTY_SLAVE_STATUS);

                        } else if (time - lastDateTime > ProfilerDataManager.EMPTY_DELAY__SS) {
                            changedCache.put(lastDateTime + 1000, ProfilerDataManager.EMPTY_SLAVE_STATUS);
                            changedCache.put(time - 1000, ProfilerDataManager.EMPTY_SLAVE_STATUS);
                        }

                        changedCache.put(time, changedSlaveStatus);
                        lastDateTime = time;
                    }

                    startDate = new Date(maxTime);
                }

                if (first) {
                    first = false;
                    // add serieas to each chart
                    Platform.runLater(() -> {
                        //log.error("start init");
                        for (ProfilerChartController chartController : chartTabs) {
                            if (chartController.isActive() && chartSeriesMap.get(chartController.getChart()) != null) {
                                chartController.init(chartSeriesMap.get(chartController.getChart()).values(), seriesColors);
                            }
                        }

                        for (ProfilerBarChartController chartController : barChartTabs) {
                            if (chartController.isActive() && barChartSeriesMap.get(chartController.getChart()) != null) {
                                chartController.init(barChartSeriesMap.get(chartController.getChart()).values(), barSeriesColors);
                            }
                        }
                    });
                }

                lastOverallCodeExecutionRun = System.currentTimeMillis();

                if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastUpdateExecutionRun) >= getParentController().getCurrentSettings().getDataRefresh()) {
                    Platform.runLater(() -> {
                        initChartData(chartsMap, chartSeriesMap, changedCache);
                        initBarChartData(barChartsMap, barChartSeriesMap, changedCache, hourlyCounts);

                        changedCache.clear();
                    });
                    lastUpdateExecutionRun = System.currentTimeMillis();
                }
            }
        };
        usersChartsWasChanged = false;
    }

    public void exportToXls(
            List<String> sheets,
            List<ObservableList<ObservableList>> totalData,
            List<LinkedHashMap<String, Boolean>> totalColumns,
            List<List<Integer>> totalColumnWidths,
            List<byte[]> images,
            List<Integer> heights,
            List<Integer> colspans) {
    }

    public double getUpper() {
        return chartTabs.get(0).getUpper();
    }

    public double getLower() {
        return chartTabs.get(0).getLower();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public void setParentController(ProfilerTabContentController parentController) {
        this.parentController = parentController;

        createAllEntities();

        ioThreadRunningBannerController.getTitleTextProperty().set("IO Thread running");
        sqlThreadRunningBannerController.getTitleTextProperty().set("SQL Thread Running");
        replictionErrorNoBannerController.getTitleTextProperty().set("Repliction Error No");
        readOnlyBannerController.getTitleTextProperty().set("Read Only");

        resetBanners();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private void resetBanners() {
        ioThreadRunningBannerController.getContentTextProperty().set("No");
        ioThreadRunningBannerController.getContainer().setStyle("-fx-background-color: red;");
        ioThreadRunningBannerController.getTitleLabel().setStyle("-fx-background-color: red;");
        ioThreadRunningBannerController.getInfo().setStyle("-fx-background-color: red;");
        ioThreadRunningBannerController.getContentTextLabel().setStyle("-fx-background-color: red; -fx-font-weight: bold; -fx-font-size: 20;");

        sqlThreadRunningBannerController.getContentTextProperty().set("No");
        sqlThreadRunningBannerController.getContainer().setStyle("-fx-background-color: red;");
        sqlThreadRunningBannerController.getTitleLabel().setStyle("-fx-background-color: red;");
        sqlThreadRunningBannerController.getInfo().setStyle("-fx-background-color: red;");
        sqlThreadRunningBannerController.getContentTextLabel().setStyle("-fx-background-color: red; -fx-font-weight: bold; -fx-font-size: 20;");

        replictionErrorNoBannerController.getContentTextProperty().set("N/A");
        replictionErrorNoBannerController.getContentTextLabel().setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20;");

        readOnlyBannerController.getContentTextProperty().set("No");
    }

    private ProfilerBarChartController loadProfilerBarChartEntityMetricControllers(GSChart chart) {

        ProfilerBarChartController pcc = loadEntityMetricControllers(ProfilerBarChartController.class);
        pcc.insertGSChart(chart);
        pcc.setChartControllerEventHandler(new ProfilerChartControllerEventHandler() {
            @Override
            public void zoomDragedAction(Long start, Long finish) {
                MysqlReplicationController.this.zoomDragedAction(start, finish);
            }
        });
        pcc.currentScaleIndex().bind(parentController.currentScaleIndex());
//        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
//        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }

    private ProfilerChartController loadProfilerChartEntityMetricControllers(GSChart chart) {
        ProfilerChartController pcc = loadEntityMetricControllers(ProfilerChartController.class);
        pcc.insertGSChart(chart);
        pcc.setChartControllerEventHandler(new ProfilerChartControllerEventHandler() {
            @Override
            public void zoomDragedAction(Long start, Long finish) {
                MysqlReplicationController.this.zoomDragedAction(start, finish);
            }
        });
        pcc.currentScaleIndex().bind(parentController.currentScaleIndex());
        pcc.hoveredOnChart().bindBidirectional(hoveredOnChart);
        pcc.hoveredX().bindBidirectional(hoveredX);

        return pcc;
    }

    private <T extends MetricEntityContainerController> T loadEntityMetricControllers(Class<T> meccClass) {
        try {
            T t = StandardBaseControllerProvider.getController(meccClass, getController(), "MetricEntityContainer");
            t.activateMetric();
            return t;
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private TabularBannerNodeController loadEntityTabularBannerControllers(String description) {
        try {
            TabularBannerNodeController controller = StandardBaseControllerProvider.getController("TabularBannerNode");
            controller.init(description, getParentController());
            return controller;
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void createAllEntities() {

        ioThreadRunningBannerController = loadEntityTabularBannerControllers(null);

        sqlThreadRunningBannerController = loadEntityTabularBannerControllers(null);

        readOnlyBannerController = loadEntityTabularBannerControllers(null);

        replictionErrorNoBannerController = loadEntityTabularBannerControllers(null);

        if (getUI().getScene() == null) {
            getUI().sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
                if (newValue != null && oldValue == null) {
                    applyNewUiModifications();
                }
            });
        } else {
            applyNewUiModifications();
        }
    }

    private void applyNewUiModifications() {
        scrollContainer.getStyleClass().add(Flags.METRIC_STYLE_CLASS);

        topHorizontalFlatFlowPane.getStyleClass().add(Flags.METRIC_STYLE_CLASS);

        Utitlities.prepFlowPaneDimensionConditionWithParent(topHorizontalFlatFlowPane, 4.0, false);
        topHorizontalFlatFlowPane.getChildren().addAll(ioThreadRunningBannerController.getUI(), sqlThreadRunningBannerController.getUI(), replictionErrorNoBannerController.getUI(),
                readOnlyBannerController.getUI());
    }

    public void finalizeConnectionCreation() {
        getUI().requestLayout();
    }

    @Override
    public ProfilerInnerControllerWorkProcAuth getInnerControllerWorkProcAuthority() {
        return mysqlReplicationControllerworkProcAuth;
    }

    public void setDbProfilerService(ProfilerDataManager dbProfilerService) {
        this.dbProfilerService = dbProfilerService;
    }

    public void setChartService(ProfilerChartService chartService) {
        this.chartService = chartService;
    }

}
