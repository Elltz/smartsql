
package np.com.ngopal.smart.sql.structure.models;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface GraphData {
     
    public String getGraph1();
    public void setGraph1(String graph1);
    
    public String getGraph2();
    public void setGraph2(String graph2);        
}