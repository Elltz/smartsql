package np.com.ngopal.smart.sql.structure.queryutils;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class AutoCompleteQueryListener implements EventHandler<KeyEvent> {

    @Override
    public void handle(KeyEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
//    private ConnectionParamService service;
//
//    private final Window owner;
//
//    private ConnectionSession session;
//
//    private final CodeArea area;
//
//    private final Popup autoCompletePopup;
//
//    private final ListView<Label> resultListView;
//
//    private String autocompleteFilterText;
//
//    
//    
//    private final Image databaseImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/database_tab.png");
//    private final Image tableImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/table_tab.png");    
//    private final Image viewImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/view_tab.png");
//    private final Image columnImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/column_tab.png");
//    private final Image functionImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/function_tab.png");
//    private final Image triggerImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/trigger_tab.png");
//    private final Image procedureImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/storedprocs_tab.png");
//    private final Image keywordsImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/keyword_tab.png");    
//    private final Image eventImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/event_tab.png");    
//    private final Image showVariableImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/show_variable.png");
//    private final Image showStatusImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/show_status.png");
//
//    private final List<String> separateChars = Arrays.asList(",", ")", "(", "+", "-", "\\", "*", "=", "!", "&", "|");
//    
//    private static final String DOT = ".";
//    private static final String OBJECT_ESCAPER = "`";
//    private static final String EMPTY = "";
//
//    private final Map<ConnectionSession, List<String>> highlightGreenMap;
//    private final Map<ConnectionSession, List<String>> highlightMaroonMap;
//    
//    public AutoCompleteQueryListener(Window owner, ConnectionParamService service, CodeArea area, ConnectionSession session, Map<ConnectionSession, List<String>> highlightGreenMap, Map<ConnectionSession, List<String>> highlightMaroonMap) {
//        this.session = session;
//        this.service = service;
//        this.autoCompletePopup = new Popup();
//        autoCompletePopup.setAutoHide(true);
//        
//        this.highlightGreenMap = highlightGreenMap;
//        this.highlightMaroonMap = highlightMaroonMap;
//
//        this.resultListView = new ListView<>();
//        resultListView.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
//            if (event.getCode() == KeyCode.ENTER) {
//                Label selectedValue = resultListView.getSelectionModel().getSelectedItem();
//                if (selectedValue != null) {
//                    applyAutocomplete(selectedValue.getText() + 
//                       (selectedValue.getUserData() instanceof View || 
//                        selectedValue.getUserData() instanceof DBTable || 
//                        selectedValue.getUserData() instanceof Database ? "" : " "), (ResultLabel) selectedValue);
//                }
//            } else if (event.getCode() == KeyCode.ESCAPE) {
//                area.getPopupWindow().hide();
//            }
//        });
//        
//        resultListView.setMaxHeight(155);
//
//        this.owner = owner;
//        this.area = area;
//
//        area.setPopupWindow(autoCompletePopup);
//        autoCompletePopup.getContent().add(resultListView);
//    }
//
//    
//    private volatile boolean keyPressed = false;
//    private volatile boolean handleThreadRunned = false;
//    
//    private volatile WorkProc proc;
//    private final Object lock = new Object();
//    
//    @Override
//    public void handle(KeyEvent event) {        
//        
//        keyPressed = true;       
//        synchronized (lock) {
//            proc = new WorkProc<KeyEvent>("Auto-Complete_WorkProc", event) {
//                int positionIndex;
//                int pos;
//                int count = -1;
//                boolean all;
//                Paragraph<Collection<String>> paragraph;
//                private List filteredData;
//
//                @Override
//                public void updateUI() {
//                    
//                    log.debug("set itesm: " + filteredData.size());
//                    resultListView.getItems().setAll(filteredData);
//
//                    String autocomleteText = paragraph.fullText().substring(pos, positionIndex);
//
//                    boolean canShow = true;
//                    if (!resultListView.getItems().isEmpty() && count == 1) {
//                        canShow = !resultListView.getItems().get(0).getText().equalsIgnoreCase(autocomleteText);
//                    }
//
//                    if (count > 0 && canShow) {
//                        resultListView.setPrefHeight(count * 24);
//
//                        area.getPopupWindow().hide();
//                        area.getPopupWindow().show(owner);
//
//                        resultListView.getSelectionModel().select(0);
//
//                        log.debug("showed");
//                    } else {
//                        area.getPopupWindow().hide();
//                    }
//                    setAdditionalData(null);
//                }
//
//                @Override
//                public void run() {
//                    filteredData = new ArrayList();
//                    all = getAddData().isControlDown()
//                            && (getAddData().getCode() == KeyCode.SPACE || getAddData().getText().equals(" "));
//
//                    if (all) {
//                        getAddData().consume();
//                        return;
//                    }
//
//                    if ((all || checkKeyEvent(getAddData())) && getAddData().getCode() != KeyCode.ENTER) {
//
//                        if (getAddData().getEventType() == KeyEvent.KEY_RELEASED) {
//                            final int parahraphIndex = area.getCurrentParagraph();
//                            positionIndex = area.getCaretColumn();
//
//                            paragraph = area.getParagraph(parahraphIndex);
//
//                            //String paragraphString = paragraph.toString();
//                            pos = positionIndex;
//
//                            while (pos > 0) {
//                                //char prevChar = paragraphString.charAt(pos - 1);
//                                char prevChar = paragraph.fullText().charAt(pos - 1);
//                                String prefStr = String.valueOf(prevChar);
//                                if (Character.isWhitespace(prevChar) || separateChars.contains(prefStr)) {
//                                    break;
//                                }
//
//                                pos--;
//                            }
//
//                            String autocomleteText = paragraph.fullText().substring(pos, positionIndex);
//                            count = filterAutocompleteData(autocomleteText, all, filteredData);
//
//                            callUpdate = true;
//                        }
//                    }
//                }
//            };
//        }
//
//        if (!handleThreadRunned) {
//
//            Thread th = new Thread(() -> {
//
//                handleThreadRunned = true;
//                while (keyPressed) {
//                    keyPressed = false;
//                    try {
//                        Thread.sleep(320);
//                    } catch (Throwable ex) {}
//                }
//                
//                synchronized (lock) {
//                    handleThreadRunned = false;
//                    Recycler.addWorkProc(proc);          
//                }
//            });
//            th.setDaemon(true);
//            th.start();
//
//        }
//    }
//
//    private boolean checkKeyEvent(KeyEvent event) {
//        return (event.getText() != null && !event.getText().isEmpty() && !event.isControlDown() && !event.isAltDown())
//                  //|| event.getCode() == KeyCode.LEFT
//                  //|| event.getCode() == KeyCode.RIGHT
//                  || event.getCode() == KeyCode.END
//                  || event.getCode() == KeyCode.HOME
//                  || event.getCode() == KeyCode.BACK_SPACE
//                  || event.getCode() == KeyCode.INSERT;
//    }
//
//    private void applyAutocomplete(String text, ResultLabel label) {
//        if (autocompleteFilterText != null) {
//            
//            Object data = label.obj;
//            
//            // add to green map
//            if (data instanceof Database || data instanceof DBTable || data instanceof View) {
//                List<String> names = highlightGreenMap.get(session);
//                if (names == null) {
//                    names = new ArrayList<>();
//                    highlightGreenMap.put(session, names);
//                }
//                
//                if (!names.contains(text.trim().toLowerCase())) {
//                    names.add(text.trim().toLowerCase());
//                }
//                
//            } else if (data instanceof StoredProc || data instanceof Function || data instanceof Event || data instanceof Trigger) {
//                List<String> names = highlightMaroonMap.get(session);
//                if (names == null) {
//                    names = new ArrayList<>();
//                    highlightMaroonMap.put(session, names);
//                }
//                
//                if (!names.contains(text.trim().toLowerCase())) {
//                    names.add(text.trim().toLowerCase());
//                }
//                
//            }
//            
//            if (autocompleteFilterText.contains(DOT)) {
//                
//                int pos = area.getCaretPosition();
//                while (pos > 0) {
//                    //char prevChar = paragraphString.charAt(pos - 1);
//                    char prevChar = area.getText().charAt(pos - 1);
//                    String prefStr = String.valueOf(prevChar);
//                    if (DOT.equals(prefStr)) {
//                        break;
//                    }
//
//                    pos--;
//                }
//            
//                area.replaceText(pos, area.getCaretPosition(), EMPTY);
//                
//            } else {
//                area.replaceText(area.getCaretPosition() - autocompleteFilterText.length(), area.getCaretPosition(), EMPTY);
//            }
//            
//            area.insertText(area.getCaretPosition(), text);
//            
//            
//            autocompleteFilterText = null;
//            if (autoCompletePopup.isShowing()) {
//                autoCompletePopup.hide();
//            }
//        }
//    }
//
//    private int filterAutocompleteData(final String autocomleteText, boolean all, List filteredData) {
//        
//        Map<String, String> tablesAlias = new HashMap<>();
//        try {
//            ScriptRunner sr = new ScriptRunner(session.getService().getDB(), false, false);
//            Pair<Integer, String> result = sr.findQueryAtPosition(area.getText(), area.getCaretPosition());
//            if (result != null) {
//                SelectCommand command = QueryFormat.parseSelectQuery(result.getValue());
//                if (command != null) {
//                    FromClause fc = command.getFromClause();
//                    if (fc != null && fc.getTables() != null) {
//                        for (TableReferenceClause t: fc.getTables()) {
//                            if (t.getAlias() != null && !t.getAlias().isEmpty()) {
//                                tablesAlias.put(t.getName().toLowerCase(), t.getAlias().toLowerCase());
//                            }
//                            
//                            if (t.getJoinClauses() != null) {
//                                for (JoinClause jc: t.getJoinClauses()) {
//                                    if (jc.getTable() != null && jc.getTable().getAlias() != null && !jc.getTable().getAlias().isEmpty()) {
//                                        tablesAlias.put(jc.getTable().getName().toLowerCase(), jc.getTable().getAlias().toLowerCase());
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (SQLException ex) {
//            log.error(ex.getMessage(), ex);
//        }
//        
//        autocompleteFilterText = autocomleteText;
//
//        if (!autocomleteText.isEmpty() || all) {
//            
//            if (autocomleteText.contains(DOT)) {
//                String[] list = autocomleteText.split("\\.", -1);
//                
//                String preLast = EMPTY;
//                String last = EMPTY;
//                int i = 0;
//                for (String str: list) {
//                    if (str.startsWith(OBJECT_ESCAPER) && str.endsWith(OBJECT_ESCAPER)) {
//                        if (str.length() == 2) {
//                            str = EMPTY;
//                        } else {
//                            str = str.substring(1, str.length() - 1);
//                        }
//                    }
//
//                    if (i == list.length - 2) {
//                        preLast = str;
//                    }
//                    
//                    if (i == list.length - 1) {
//                        last = str;
//                    }
//                    
//                    i++;
//                }
//                
//                String lastStr = last;
//                int result = 0;
//                
//                for (Object db: DatabaseCache.getInstance().getDatabasesKeys(session.getConnectionParam())) {                    
//                    // if last is database
//                    if (db.toString().toLowerCase().equals(preLast.toLowerCase())) {
//                        
//                        final Predicate<Object> filter = (Object s) -> {
//                            
//                            if (((DatabaseChildren)s).getDatabase().getName().equalsIgnoreCase(db.toString())) {
//                                return lastStr.isEmpty() || s.toString().toLowerCase().startsWith(lastStr.toLowerCase());
//                            }
//                            return false;
//                        };
//                        
//                        // need add all tables
//                        result += filterSetData(filteredData, DatabaseCache.getInstance().getTablesKeys(session.getConnectionParam()), filter, tableImage);
//                        
//                        // need add all views
//                        result += filterSetData(filteredData, DatabaseCache.getInstance().getViewsKeys(session.getConnectionParam()), filter, viewImage);
//                    }
//                }
//                
//                for (Object t: DatabaseCache.getInstance().getTablesKeys(session.getConnectionParam())) {                    
//                    // if last is table
//                    String name = t.toString().toLowerCase();
//                    String alias = tablesAlias.get(name);
//                    
//                    if (name.equals(preLast.toLowerCase()) || alias != null && alias.equals(preLast.toLowerCase())) {
//                        
//                        final Predicate<Object> filter = (Object s) -> {
//                            Column col = (Column)s;
//                            if (col.getTable() != null && col.getTable().getName().equalsIgnoreCase(t.toString())) {
//                                return lastStr.isEmpty() || s.toString().toLowerCase().startsWith(lastStr.toLowerCase());
//                            }
//                            return false;
//                        };
//                        
//                        // need add all columns
//                        result += filterSetData(filteredData, DatabaseCache.getInstance().getColumnKeys(session.getConnectionParam()), filter, columnImage);
//                    }
//                }
//                
//                
//                for (Object v: DatabaseCache.getInstance().getViewsKeys(session.getConnectionParam())) {                    
//                    // if last is table
//                    String name = v.toString().toLowerCase();
//                    String alias = tablesAlias.get(name);
//                    
//                    if (name.equals(preLast.toLowerCase()) || alias != null && alias.equals(preLast.toLowerCase())) {
//                        
//                        final Predicate<Object> filter = (Object s) -> {
//                            
//                            Column col = (Column)s;
//                            if (col.getView() != null && col.getView().getName().equalsIgnoreCase(v.toString())) {
//                                return lastStr.isEmpty() || s.toString().toLowerCase().startsWith(lastStr.toLowerCase());
//                            }
//                            return false;
//                        };
//                        
//                        // need add all columns
//                        result += filterSetData(filteredData, DatabaseCache.getInstance().getColumnKeys(session.getConnectionParam()), filter, columnImage);
//                    }
//                }
//                
//                return result;
//                
//            } else {
//
//                String text = autocomleteText.replaceAll(OBJECT_ESCAPER, EMPTY);
//                
//                final Predicate<Object> filter = (Object s) -> all || s.toString().toLowerCase().startsWith(
//                          text.toLowerCase());
//
//                int result = filterSetData(filteredData, DatabaseCache.getInstance().getKeywordsKeys(), filter, keywordsImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getServerFunctionsKeys(), filter, keywordsImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getDatabasesKeys(session.getConnectionParam()), filter, databaseImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getTablesKeys(session.getConnectionParam()), filter, tableImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getViewsKeys(session.getConnectionParam()), filter, viewImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getColumnKeys(session.getConnectionParam()), filter, columnImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getProceduresKeys(session.getConnectionParam()), filter, procedureImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getFunctionsKeys(session.getConnectionParam()), filter, functionImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getTriggersKeys(session.getConnectionParam()), filter, triggerImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getEventsKeys(session.getConnectionParam()), filter, eventImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getShowVariables(), filter, showVariableImage);
//                result += filterSetData(filteredData, DatabaseCache.getInstance().getShowStatus(), filter, showStatusImage);
//                
//                return result;
//            }
//        }
//
//        return 0;
//    }
//
//    private int filterSetData(List filteredData, final List set, final Predicate<Object> filter, final Image img) {
//        List<Object> filteredList = (List<Object>) set.stream().filter(filter).collect(Collectors.toList());
//        
//        for (Object s: filteredList) {
//            ResultLabel l = new ResultLabel(s, img);
//            
//            if (!filteredData.contains(l)) {
//                l.setUserData(s);
//                filteredData.add(l);
//            }
//        }
//
//        return filteredList.size();
//    }
//    
//    
//    private class ResultLabel extends Label {
//        
//        private final Object obj;
//        public ResultLabel(Object obj, Image img) {
//            super(obj instanceof Column ? ((Column)obj).getName() : obj.toString(), new ImageView(img));
//            this.obj = obj;
//        }
//        
//        @Override
//        public boolean equals(Object obj) {
//            return obj instanceof ResultLabel ? theSame((ResultLabel)obj) : false;
//        }
//        
//        
//        public boolean theSame(ResultLabel rl) {
//            if (rl.obj.getClass().getName().equals(obj.getClass().getName())) {
//                if (rl.obj instanceof Column) {
//                    return ((Column)rl.obj).getName().equalsIgnoreCase(((Column)obj).getName());
//                } else {
//                    return rl.obj.toString().equalsIgnoreCase(obj.toString());
//                }
//            }
//            
//            return false;
//        }
//    }
}
