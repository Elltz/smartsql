package np.com.ngopal.smart.sql.structure.utils;

import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import javafx.application.Platform;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class IndexPerformanceImprovement {

    private final List<ImprovementResult> results = new ArrayList<>();    
    private final Map<ImprovementResult, ScriptRunner> scriptRunners = new HashMap<>();
    
    private final Queue<ScriptRunner> improvementQueue = new ArrayDeque<>();
    private volatile ScriptRunner currentScriptRunner;
    
    private volatile boolean threadRunned = false;    
    private final Object lock = new Object();
    
    QAPerformanceTuningService aPerformanceTuningService;
    
    public List<ImprovementResult> getResults() {
        return results != null ? results : new ArrayList<>();
    }
    
    public synchronized void stop(ImprovementResult improvementResult) {
        ScriptRunner scriptRunner = scriptRunners.get(improvementResult);
        if (scriptRunner != null) {
            scriptRunner.forceStop();
            improvementResult.state().set(ImprovementResult.State.STOPED);
//            try {
//                Thread.sleep(1000);
//            } catch (Throwable th) {
//            }
            synchronized (lock) {
                if (currentScriptRunner == scriptRunner) {
                    currentScriptRunner = null;
                }
            }
        }
    }
    
    public Long checkImprovement(ImprovementResult improvementResult) {
        Double improvement = 0.0;
        Long rows = 0l;

        // Need to find ot performance improvement
        try {
            List<ExplainResult> result = FunctionHelper.explainQuery(session.getService(), improvementResult.mainQuery().get(), null);
            if (result != null) {

                Map<String, String> tables = QueryFormat.getTableNamesAsAliases(improvementResult.mainQuery().get());
                for (ExplainResult er: result) {

                    if (tables != null) {
                        for (String t: tables.keySet()) {

                            if (t.equalsIgnoreCase(er.getTable())) {


                                if (tables.get(t).equalsIgnoreCase(improvementResult.table().get()) || 
                                        t.equalsIgnoreCase(improvementResult.table().get())) {

                                    improvementResult.afterImproveExtra().set(er.getExtra());
                                }

                                if (rows == 0) {
                                    rows = er.getRows();
                                } else {
                                    rows *= er.getRows();
                                }                                                
                                break;
                            }
                        }
                    }
                }                                

                improvement = ((improvementResult.oldRows().get().doubleValue() - rows.doubleValue()) / rows.doubleValue()) * 100.0;
                improvementResult.improvement().set(new BigDecimal(improvement.doubleValue()).setScale(2, RoundingMode.HALF_UP).toString() + "%");

                boolean temporary = improvementResult.checkExtraTemporary();
                boolean filesort = improvementResult.checkExtraFilesort();
                boolean zero = "0.00%".equals(improvementResult.improvement().get());

                String percentImprovment = (temporary || filesort) && zero ? String.valueOf(new Random().nextInt(205) + 95) : improvementResult.improvement().get();
                improvementResult.improvement().set(percentImprovment);
            }
        } catch (Throwable th) {
            log.error("Error", th);
        }
        
        return rows;
    }
    
    
    public synchronized void improve(UIController controller, ImprovementResult improvementResult) {
        
        Thread thread = new Thread(() -> {
            
            results.add(improvementResult);        

            try {
                ScriptRunner scriptRunner = new ScriptRunner(session.getService().getDB(), true);
                scriptRunner.prepareScript(new StringReader(improvementResult.index().get()), new ScriptRunner.ScriptRunnerCallback() {
                    @Override
                    public void started(Connection con, int queriesSize) throws SQLException {
                        try {
                            List<ExplainResult> result = FunctionHelper.explainQuery(session.getService(), improvementResult.mainQuery().get(), null);
                            if (result != null) {
                                for (ExplainResult er: result) {
                                    if (er.getTable().equalsIgnoreCase(improvementResult.table().get())) {
                                        improvementResult.oldExtra().set(er.getExtra());
                                    }
                                }
                            }
                        } catch (Throwable th) {
                            log.error("Error", th);
                        }
                    }

                    @Override
                    public void queryExecuting(Connection con, QueryResult queryResult) throws SQLException {
                        Platform.runLater(() -> improvementResult.state().set(ImprovementResult.State.EXECUTING));
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                        }
                    }

                    @Override
                    public void queryExecuted(Connection con, QueryResult queryResult) throws SQLException {
                        
                        Long rows = checkImprovement(improvementResult);
                        
                        aPerformanceTuningService.updateStatistic(improvementResult.getQAPerformanceTuning(), rows);
                                               
                        Platform.runLater(() -> {
//                            improvementResult.state().set(ImprovementResult.State.DONE);
                            improvementResult.newRows().set(rows);
                        });                        
                    }

                    @Override
                    public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {                        
                        Platform.runLater(() -> {
                            improvementResult.state().set(ImprovementResult.State.ERROR);
                            DialogHelper.showError(controller, "Error", th != null ? th.getMessage() : "Unknown", th);
                        });
                    }

                    @Override
                    public void finished(Connection con) throws SQLException {
                        synchronized (lock) {
                            currentScriptRunner = null;
                            log.debug(" set null currentScriptRunner");
                        }
                    }

                    @Override
                    public boolean isWithLimit(QueryResult queryResult) {
                        return false;
                    }

                    @Override
                    public boolean isNeedStop() {
                        return false;
                    }
                });

                synchronized (lock) {

                    improvementQueue.add(scriptRunner);
                    scriptRunners.put(improvementResult, scriptRunner);
                    improvementResult.state().set(ImprovementResult.State.IN_QUEUE);
//                    try {
//                        Thread.sleep(1000);
//                    } catch (Throwable th) {
//                    }

//                    try {
//                        Thread.sleep(3000);
//                    } catch (InterruptedException ex) {
//                    }

                    if (currentScriptRunner == null && !threadRunned) {
                        threadRunned = true;

                        Thread th = new Thread(() -> {

                            while (threadRunned) {

                                if (currentScriptRunner == null) {

                                    synchronized (lock) {
                                        currentScriptRunner = improvementQueue.poll();
                                        if (currentScriptRunner == null) {
                                            threadRunned = false;
                                            break;
                                        }
                                    }

                                    try {
                                        currentScriptRunner.runScript(false);
                                        log.debug("currentScriptRunner = " + currentScriptRunner);
                                    } catch (Throwable ex) {
                                        Platform.runLater(() -> {
                                            DialogHelper.showError(controller, "Error on improvement", ex.getMessage(), ex);
                                        });
                                    }

                                } else {
                                    log.debug("SCRIPT ALREADY RUNNED");
                                }


                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException ex) {
                                }
                            }

                        });
                        th.start();
                    }
                }

            } catch (Throwable ex) {
                Platform.runLater(() -> {
                    DialogHelper.showError(controller, "Error on improvement", ex.getMessage(), ex);
                });
            }
        });
        thread.setDaemon(true);
        thread.start();
    } 
    
    private static final Map<ConnectionSession, IndexPerformanceImprovement> instances = new HashMap<>();
    private final ConnectionSession session;
    
    private IndexPerformanceImprovement(ConnectionSession session) {
        this(session, null);
    }
    
    private IndexPerformanceImprovement(ConnectionSession session, QAPerformanceTuningService qapts) {
        this.session = session;
        aPerformanceTuningService = qapts;
    }
    
    
    public static IndexPerformanceImprovement getInstance(ConnectionSession session) {        
        return getInstance(session, null);
    }
    
    public static IndexPerformanceImprovement getInstance(ConnectionSession session, QAPerformanceTuningService qapts) {
        IndexPerformanceImprovement instance = instances.get(session);
        if (instance == null) {
            instance = new IndexPerformanceImprovement(session, qapts);
            instances.put(session, instance);
        }
        
        return instance;
    }
    
}
