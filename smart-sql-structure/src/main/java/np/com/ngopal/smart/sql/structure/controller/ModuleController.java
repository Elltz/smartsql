/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.structure.controller;

import java.util.Map;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.Module;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 * This is an abstract ModuleController which have some pre-condition for
 * extending a module.
 * <p>
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Slf4j
public abstract class ModuleController implements Module{

    private int order = 0;

    @Getter(AccessLevel.PROTECTED)
    private int connectionId;
    protected Exception exception;
    private final BooleanProperty selectedConnectionBusy = new SimpleBooleanProperty(false);
    
    protected ChangeListener<Boolean> serviceListener = (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
        serviceBusyChanged(newValue);
    };
    
    @Getter(AccessLevel.PUBLIC)
    @Setter
    protected UIController controller;

    protected BooleanProperty selectedConnectionBusy() {
        return selectedConnectionBusy;
    }
    
    protected void serviceBusyChanged(Boolean newValue) {
        selectedConnectionBusy.set(newValue);
    }

    public Class getModuleClass() {
        return this.getClass();
    }

    public boolean isConnection() {
        return false;
    }

    public abstract Class getDependencyModuleClass();

    /**
     * This method gets called on the background thread.
     *
     * @param obj The selected TreeItem. This obj is never null.
     */
    public abstract boolean selectedTreeItemChanged(TreeItem item);

    @Override
    public boolean install() {
        /**
         * 1 THIS CODE IS BUGGY ------------------ When the user opens only one
         * connection it does not get triggered hence the listeners are not
         * attach to trigger selectedTreeItemValueChanged(newValue != null ?
         * newValue.getValue() : null);
         *
         * and gets called way too much, maybe it is be
         *
         * 2 IT GETS CALLED 4 TIMES FOR 2 CONNECTION SESSION AND TWICE FOR 1
         * ------------------------------------------------------------------ I
         * do not know if this was the original behaviour but, i think it is
         * because of the code i added in for the comboBox, to synchronise with
         * the selectedTreeItemValue. ChangeListener<TreeItem> itemListener =
         * new ChangeListener<TreeItem>() {
         *
         * @Override public void changed(ObservableValue<? extends TreeItem>
         * observable, TreeItem oldValue, TreeItem newValue) {
         * selectedTreeItemValueChanged(newValue != null ? newValue.getValue() :
         * null); } };
         *
         * getController().getSelectedConnectionSession().addListener(new
         * ChangeListener<ConnectionSession>() {
         * @Override public void
         * changed(ObservableValue<? extends ConnectionSession> observable,
         * ConnectionSession oldValue, ConnectionSession newValue) { if
         * (oldValue != null) { ReadOnlyObjectProperty<TreeItem> oldItem =
         * getController().getSelectedTreeItem(oldValue);
         *
         * if (oldItem != null) { oldItem.removeListener(itemListener); } }
         *
         * if (newValue != null) { ReadOnlyObjectProperty<TreeItem> item =
         * getController().getSelectedTreeItem(newValue); if (item != null) {
         * item.addListener(itemListener); }
         *
         * selectedTreeItemValueChanged(item != null && item.get() != null ?
         * item.get().getValue() : null); } else {
         * selectedTreeItemValueChanged(null); } } });
         *
         * ------------------------------------------------- I HAVE COMMENTED
         * THE CODE BECAUSE OF THIS, AND ADDED A TEMP CODE IN MAIN CONTROLLER
         *
         */
        return true;
    }

    @Override
    public ConnectionParams saveConnection(ConnectionParams param) throws Exception {
        return param;
    }

    /**
     * When any connection selection has been changed then this will get
     * triggered
     * <p>
     * @param service ConnectionSession
     * @param tab Tab
     * @throws Exception
     */
    @Override
    public ConnectionParams connectionChanged(ConnectionParams param) throws Exception {
        return param;
    }

    /**
     * When any connection gets opened with NewConnection this will get executed
     * on appropriate module.
     * <p>
     * @param service ConnectionSession
     * @param tab Tab
     * @throws Exception
     */
    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {

        if (service != null) {
            // get not connection Id, dont know why it named connectionId
            connectionId = service.getId();
        }

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            Label tabLabel;
            String backColor, foregColor;

            {
                tabLabel = (Label) tab.getGraphic();
                if (tab.isSelected()) {

                    backColor = getBackgroundColor(service.getConnectionParam());
                    foregColor = getForegroundColor(service.getConnectionParam());

                    if (foregColor != null) {
                        tabLabel.setTextFill(Color.web(foregColor));
                    }

                    if (backColor != null && !backColor.equals("null")) {
                        tab.setStyle("-fx-background-color:" + backColor + ";");
                    }
                } else {
                    tabLabel.setTextFill(Color.web("#293955"));
                }
            }

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    try {
                        if (backColor != null) {
                            tab.setStyle("-fx-background-color:" + backColor + ";");
                        }

                        if (foregColor != null) {
                            tabLabel.setTextFill(Color.web(foregColor));
                        }

                    } catch (NullPointerException npe) {
                        tab.setStyle(".tab{}");
                    }
                } else {
                    tab.setStyle(".tab{}");
                    tabLabel.setTextFill(Color.web("#293955"));
                }
            }
        });

    }

    private String getBackgroundColor(ConnectionParams params) {
        String backColor = params.getBackgroundColor();
        if (backColor != null) {
            if (!backColor.startsWith("#")) {
                backColor = "#" + backColor;
            }
        }

        return backColor;
    }

    private String getForegroundColor(ConnectionParams params) {
        String foregColor = params.getForegroundColor();
        if (foregColor != null) {
            if (!foregColor.startsWith("#")) {
                foregColor = "#" + foregColor;
            }
        }

        return foregColor;
    }

    /**
     * Some of module controller may denid open connection, for example, the
     * same connection and throw exception
     * <p>
     * @param params ConnectionParams
     * @return boolean indicate can open connection or no with input params
     */
    public boolean canOpenConnection(ConnectionParams params) {
        return true;
    }

    public boolean canReinitConnection(ConnectionParams params) {
        return false;
    }
    
    public synchronized void reinitConnection(ConnectionParams params) {
    }

    /**
     * Left Pane of the main application tab panel.
     * <p>
     * @param t Tab
     * @return AnchorPane
     */
    public AnchorPane getLeftPane(Tab t) {
        Node n = t.getContent().lookup("#leftPane");
        return n != null ? (AnchorPane) n : null;
    }

    /**
     * Right-centered Pane of the main application tab panel.
     * <p>
     * @param t Tab
     * @return AnchorPane
     */
    public AnchorPane getCenterPane(Tab t) {
        Node n = t.getContent().lookup("#centerPane");
        return n != null ? (AnchorPane) n : null;
    }

    /**
     * Right-bottomed Pane of the main application tab panel.
     * <p>
     * @param t Tab
     * @return AnchorPane
     */
    public AnchorPane getBottomPane(Tab t) {
        Node n = t.getContent().lookup("#bottomPane");
        return n != null ? (AnchorPane) n : null;
    }

    /**
     * Provides the name of Module with respective to the class name removing
     * "Module" string from the class name.
     * <p>
     * @return class name of Module with excluded "Module" string.
     */
    public String getName() {
        String name = getClass().getName();

        return name.substring(Math.max(name.lastIndexOf(".") + 1, 0)).replace("Module", "");
    }

    /**
     * Provides the Map object containing number of MenuItem which is to be
     * hooked in Right panel of the main UI.
     * <p>
     * @param param ConnectionSession
     * @return null
     */
    public Map<HookKey, Hookable> getHooks(ConnectionSession param) {
        return null;
    }
}
