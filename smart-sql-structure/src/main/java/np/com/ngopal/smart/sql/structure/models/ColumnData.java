
package np.com.ngopal.smart.sql.structure.models;

import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.DataType;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Setter
@Getter
public class ColumnData {

    private String alias;
    private String name;
    private boolean primaryKey = false;
    private boolean autoincrement = false;
    private boolean nullable = true;
    private String defaultValue = "(NULL)";
    private boolean number = false;
    private boolean date = false;
    
    private boolean enumType = false;
    private boolean setType = false;
    
    private DataType dataType;
    
    private String tableName;
    private String databaseName;
    
    private String displayTable;
    
    public ColumnData(String alias) {
        this.alias = alias;
    }
}
