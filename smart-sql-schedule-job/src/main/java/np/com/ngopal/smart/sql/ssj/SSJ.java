package np.com.ngopal.smart.sql.ssj;

import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Injector;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Schedule;
import np.com.ngopal.smart.sql.model.ScheduleParams;
import np.com.ngopal.smart.sql.model.ScheduleReport;
import np.com.ngopal.smart.sql.utils.DumpUtils;
import np.com.ngopal.smart.sql.utils.EmailUtils;
import np.com.ngopal.smart.sql.utils.ReportUtils;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public class SSJ {
    
    private static final SimpleDateFormat DATETIME_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
    private static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
    
    public static final void main(String[] args) {
        new SSJ().runJob(args[0]);
        //new SSJ().runJob("G:\\work\\GIT\\smart-sql\\smart-sql-updater\\data\\test.ssj");
    }
 
    
    private void runJob(String fileName) {
        try {
                        
            Gson gson = new Gson();
            Map<String, Object> map = gson.fromJson(new FileReader(fileName), Map.class);
            if (map != null) {
                String scheduleType = (String) map.get("scheduleType");
                
                Injector injector = Guice.createInjector(new SSJModule());
                DBService dbService = injector.getInstance(DBService.class);
                if (scheduleType != null && scheduleType.equals("report")) {                    
                    runReport(dbService, gson.fromJson(new FileReader(fileName), ScheduleReport.class));                    
                } else {                    
                    runBackup(dbService, gson.fromJson(new FileReader(fileName), ScheduleParams.class));                    
                }
            }
        } catch (FileNotFoundException | SQLException ex) {
            log.error("Error", ex);
        }
    }
    
    private void runReport(DBService dbService, ScheduleReport report) {
        ReportUtils.doReport(dbService, report, new ReportUtils.ReportStatusCallback() {
            @Override
            public void error(String errorMessage, Throwable th) {
                log.error(errorMessage, th);

                if (report.isNotificationEmail()) {

                    StringWriter sw = new StringWriter();
                    sw.append(errorMessage).append("\n\n");
                    if (th != null) {
                        th.printStackTrace(new PrintWriter(sw));
                    }

                    sendEmail(report, "Report failed - " + report.getName() + " at " +  DATETIME_FORMATER.format(new Date()), sw.toString().replaceAll("\n", "<br>"));
                }
            }

            @Override
            public void success(List<String> messages) {
                if (messages != null && !messages.isEmpty()) {
                    String subject = report.getUserDefined();
                    if (subject == null) {
                        subject = report.isDefinedWithTime() ? DATETIME_FORMATER.format(new Date()) : DATE_FORMATER.format(new Date());
                    }

                    sendEmail(report, subject, String.join("<br><br><br>", messages));
                }
            }
        });
    }
    
    private void runBackup(DBService dbService, ScheduleParams params) throws SQLException {
        if (params != null) {

            dbService.connect(params.toConnectionParams());

            if (!params.isSingleFileForAll()) {
                DumpUtils.dumpPerObjectDatabase(dbService, params, new DumpUtils.DumpStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        log.error(errorMessage, th);

                        if (params.isNotificationEmail()) {

                            StringWriter sw = new StringWriter();
                            sw.append(errorMessage).append("\n\n");
                            th.printStackTrace(new PrintWriter(sw));

                            sendEmail(params, "Bakup failed - " + params.getName() + " at " +  DATETIME_FORMATER.format(new Date()), sw.toString().replaceAll("\n", "<br>"));
                        }
                    }

                    @Override
                    public void success() {
                        if (params.isNotificationEmail() && !params.isOnlyError()) {
                            sendEmail(params, "Bakup completed - " + params.getName() + " at " +  DATETIME_FORMATER.format(new Date()), "");
                        }
                    }                   

                    @Override
                    public void progress(int progress, String objectLevel, String currentDumpedObject) { }
                    
                });
            } else {
                DumpUtils.dumpAllDatabase(dbService, params, new DumpUtils.DumpStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        log.error(errorMessage, th);

                        if (params.isNotificationEmail()) {

                            StringWriter sw = new StringWriter();
                            sw.append(errorMessage).append("\n\n");
                            th.printStackTrace(new PrintWriter(sw));

                            sendEmail(params, "Bakup failed - " + params.getName() + " at " +  DATETIME_FORMATER.format(new Date()), sw.toString());
                        }
                    }

                    @Override
                    public void success() {
                        if (params.isNotificationEmail() && !params.isOnlyError()) {
                            sendEmail(params, "Bakup completed - " + params.getName() + " at " +  DATETIME_FORMATER.format(new Date()), "");
                        }
                    }

                    @Override
                    public void progress(int progress, String objectLevel, String currentDumpedObject) { }
                });
            }
        }
    }
    
    
    // May be need move this some utility
    private void sendEmail(Schedule schedule, String subject, String text) {
        try {
            EmailUtils.sendEmail(schedule.getEmailFromName(), schedule.getEmailFromEmail(), schedule.getEmailReplyEmail(),
                    schedule.getEmailToEmail(), schedule.getEmailCc(), schedule.getEmailBcc(), 
                    schedule.getEmailHost(), ""+schedule.getEmailPort(), schedule.getEmailEncryptior(), 
                    schedule.getEmailAccountName(), schedule.getEmailPasswd(),
                    subject, text);
        } catch (Throwable e) {
            log.error("Error sending message", e);
        }
    }
}
