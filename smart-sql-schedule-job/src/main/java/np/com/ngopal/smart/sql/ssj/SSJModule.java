package np.com.ngopal.smart.sql.ssj;

import com.google.inject.AbstractModule;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;


/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SSJModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(DBService.class).to(MysqlDBService.class);
        bind(SQLLogger.class).toInstance(provideSQLLogger());
    }
    
    public SQLLogger provideSQLLogger() {
        return new SQLLogger();
    }
}
