/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.dependency.updater;

import com.google.gson.Gson;
import com.sun.javafx.PlatformUtil;
import java.awt.Desktop;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class AppUpdatesUtility {

    private static PrintStream oldSysOut;
    
    /**
     * Responsible keys
     */
    
    private final static String KEY_UPDATE_NOTICE = "newUpdateNotice";
    private final static String KEY_VERSION = "version";
    private final static String KEY_BANNER_TEXT = "bannerText";
    private final static String KEY_YOUTUBE_TEXT = "youtubeText";
    private final static String KEY_ONLINE_SERVER = "onlineServer";
    private final static String KEY_APP_SIZE = "appSize";
    private final static String KEY_APP_HASH = "appHash";
    private final static String KEY_IN_APP_ENCRYPTOR_KEY = "appEncryptionKey";
    
    /**
     * Main Entry for Updates Utility
     *
     * @param args Arguments required for Updates Utility to run...
     *
     * [0] = DIRECTORY FOR SOURCE OF UPDATES FOLDER 
     * [1] = DIRECTORY FOR TARGET OF UPDATES FOLDER 
     * [2] = DIRECTORY FOR AFTER-CODE-EXECUTION PROGRAMME 
     * [3] = DIRECTORY FOR [THIS] APPLICATION LOG FILE 
     * [4] = DIRECTORY FOR LAST KNOWN SERVER CONFIG FILE
     * [5] = BOOLEAN FLAG FOR deletion of source folder/file 
     * [6] =
     */
    private static File updatesSourceDirectory;
    private static File updatesTargetDirectory;
    private static File afterExecutionProgramFile;
    private static File applicationLogFile;
    private static File serverConfigFile;
    private static Boolean deleteSourceFolderWhenDone;

    public static void main(String[] args) {
        try {
            if (args != null && args.length > 0) {
                for (int i = 0; i < args.length; i++) {
                    try {
                        switch (i) {
                            case 0:
                                updatesSourceDirectory = new File(args[i]);
                                break;
                            case 1:
                                updatesTargetDirectory = new File(args[i]);
                                break;
                            case 2:
                                afterExecutionProgramFile = new File(args[i]);
                                break;
                            case 3:
                                applicationLogFile = new File(args[i]);
                                break;
                            case 4:
                                serverConfigFile = new File(args[i]);
                                break;
                            case 5:
                                deleteSourceFolderWhenDone = Boolean.valueOf(args[i]);
                                break;
                            default:
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                setStreamsLocation();
//                printOnStreams(String.join(" ", args), true, true);
                printOnStreams("STARTED SMART-UPDATER ON " + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()), true, true);
                Thread.sleep(5000);
                printOnStreams("PATCHING UPDATES...", true, true);
                copyUpdates();
                updateSmartDataVersionInfo();
                printOnStreams(">>> DONE PATCHING UPDATES...", true, true);
                System.gc();
                try { Thread.sleep(2000); } catch (Exception e) { }

                miscCode();
            }
        } catch (Exception e) { e.printStackTrace(); }

        try { Thread.sleep(3000); } catch (Exception e) { }
        System.exit(0);
    }
    
    private static void printOnStreams(String s, boolean fileLogger, boolean commandPromptConsole){
        if(fileLogger)
            System.out.println(s);
        if(commandPromptConsole)
            oldSysOut.println(s);
    }

    private static void setStreamsLocation() throws Exception {
        if (applicationLogFile != null) {
            oldSysOut = System.out;
            PrintStream streamFile = new PrintStream(applicationLogFile);            
            System.setOut(streamFile);
            System.setErr(streamFile);
        }
    }

    private static void copyUpdates() {
        printOnStreams("TRANSFERING UPDATES FILES", true, false);
        printOnStreams("VALIDATING UPDATES FILES", false, true);
        File[] updateSourceFiles = updatesSourceDirectory.listFiles();
        if (updateSourceFiles != null) {
            for (File updateSourceFile : updateSourceFiles) {
                try {
                    copy(updateSourceFile, updatesTargetDirectory);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void deleteOrVoid(File file) throws Exception {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    deleteOrVoid(f);
                }
            }
        }
        file.delete();
    }

    private static void copy(File sourceFile, File destinationFolder) throws Exception {
        printOnStreams("COPYING FILE FROM " + sourceFile.getPath() + " TO " + destinationFolder.getPath(), true, false);
        printOnStreams("PATCHING FILE " + getCipheredString(sourceFile.getPath()) + " TO " + getCipheredString(destinationFolder.getPath()),
                false, true);

        File target = new File(destinationFolder, sourceFile.getName());
        if (sourceFile.isDirectory()) {
            if (sourceFile.exists()) {
                File[] files = sourceFile.listFiles();
                if (files != null) {
                    for (File f : files) {
                        copy(f, target);
                    }
                }
            } else {
                target.mkdirs();
            }
        } else {
//            printOnStreams("writable: " + Files.isWritable(target.toPath()), true, true);            
            
            Files.copy(sourceFile.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }
    
    private static void miscCode() {
        if (deleteSourceFolderWhenDone) {
            try {
                printOnStreams("FLUSHING UPDATE SOURCE FILES", true, true);
                deleteOrVoid(updatesSourceDirectory);
            } catch (Exception e) { e.printStackTrace(); }
        }
        
        printOnStreams("RE-STARTING APPLICATION NOW " + afterExecutionProgramFile.getAbsolutePath(), true, true);
        if (PlatformUtil.isMac()) {
            try {
                printOnStreams("OPEN " + afterExecutionProgramFile.getAbsolutePath(), true, true);
                Desktop.getDesktop().open(afterExecutionProgramFile);
            } catch (IOException ex) {
                printOnStreams("Error: " + ex.getMessage(), true, true);
            }
        } else {
            if(afterExecutionProgramFile != null && afterExecutionProgramFile.exists() && afterExecutionProgramFile.isFile()){
                try{
                    printOnStreams("RE-STARTING APPLICATION NOW...", true, true);
                    System.out.println();
                    Runtime.getRuntime().exec(afterExecutionProgramFile.getPath());
                } catch (Exception e) { 
                    printOnStreams("Error: " + e.getMessage(), true, true);
                }
            }
        }
    }
    
    private static void updateSmartDataVersionInfo() throws Exception {
        String protocolJsonString = new String(Files.readAllBytes(serverConfigFile.toPath()));
        HashMap<String, String> serverConfigMap = new Gson().fromJson(protocolJsonString, HashMap.class);
        printOnStreams("", true, false);
        printOnStreams("SERVER CONFIG FILE", true, false);
        printOnStreams(protocolJsonString, true, false);
        
        File localConfigFile = new File(applicationLogFile.getParentFile(),"data.json");
        protocolJsonString = new String(Files.readAllBytes(localConfigFile.toPath()));
        HashMap<String, String> localConfigMap = new Gson().fromJson(protocolJsonString, HashMap.class);
        printOnStreams("", true, false);
        printOnStreams("DATA FILE", true, false);
        printOnStreams(protocolJsonString, true, false);
        protocolJsonString = null;
        
        localConfigMap.put(KEY_UPDATE_NOTICE, serverConfigMap.getOrDefault(KEY_UPDATE_NOTICE, localConfigMap.get(KEY_UPDATE_NOTICE)));
        localConfigMap.put(KEY_APP_HASH, serverConfigMap.getOrDefault(KEY_APP_HASH, localConfigMap.get(KEY_APP_HASH)));
        localConfigMap.put(KEY_APP_SIZE, serverConfigMap.getOrDefault(KEY_APP_SIZE, localConfigMap.get(KEY_APP_SIZE)));
        localConfigMap.put(KEY_BANNER_TEXT, serverConfigMap.getOrDefault(KEY_BANNER_TEXT, localConfigMap.get(KEY_BANNER_TEXT)));
        localConfigMap.put(KEY_YOUTUBE_TEXT, serverConfigMap.getOrDefault(KEY_YOUTUBE_TEXT, localConfigMap.get(KEY_YOUTUBE_TEXT)));
        localConfigMap.put(KEY_ONLINE_SERVER, serverConfigMap.getOrDefault(KEY_ONLINE_SERVER, localConfigMap.get(KEY_ONLINE_SERVER)));
        
        String old = localConfigMap.get(KEY_VERSION);
        printOnStreams("OLD VERSION " + old, true, false);
        String server = serverConfigMap.get(KEY_VERSION);
        printOnStreams("SERVER VERSION " + server, true, false);
        localConfigMap.put(KEY_VERSION, serverConfigMap.getOrDefault(KEY_VERSION, localConfigMap.get(KEY_VERSION)));
        printOnStreams("NEW: " + localConfigMap.get(KEY_VERSION), true, false);
        localConfigMap.put(KEY_IN_APP_ENCRYPTOR_KEY, serverConfigMap.getOrDefault(KEY_IN_APP_ENCRYPTOR_KEY, localConfigMap.get(KEY_IN_APP_ENCRYPTOR_KEY)));
        
        String localConfigStringData = new Gson().toJson(localConfigMap, HashMap.class);
        printOnStreams("", true, false);
        printOnStreams("LOCAL CONFIG MERGED WITH SERVER CONFIG", true, false);
        printOnStreams(localConfigStringData, true, false);
        printOnStreams("SAVE TO " + localConfigFile.toPath(), true, false);
        Files.write(localConfigFile.toPath(), localConfigStringData.getBytes("UTF-8"), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.SYNC);
        
        /********************************************
         * APPLICATION CFG FILE MANIPULATION
         */
        printOnStreams("ACCOMULATING CONTENTS OF CFG FILE", true, false);
        
        
        if (PlatformUtil.isWindows()) {
            findVersionPosition_Windows(serverConfigMap, localConfigMap);
            
        } else if (PlatformUtil.isMac()) {
            findVersionPosition_Windows(serverConfigMap, localConfigMap);
            findVersionPosition_Mac(serverConfigMap, localConfigMap);
            
        } else if (PlatformUtil.isLinux()) {
            // TODO Need add !!!!
        }        
    }
        
    private static void findVersionPosition_Windows(HashMap<String, String> serverConfigMap, HashMap<String, String> localConfigMap) throws IOException {
        FileFilter cfgFilter = (File pathname) -> pathname.isFile() && pathname.getName().endsWith(".cfg");
        
        File cfgFile = updatesTargetDirectory.listFiles(cfgFilter)[0];
        List<String> lines = Files.readAllLines(cfgFile.toPath(), Charset.forName("UTF-8"));
        
        int argOptionsLineNumber = -1;
        //reverse loop; starting from bottom
        for (int i = lines.size() -1; i >= 0; i--){
            String line = lines.get(i);
            if(line.charAt(0) == '[' && line.equalsIgnoreCase("[ArgOptions]")) {
                argOptionsLineNumber = i;
                break;
            } 
        }
        
        if(argOptionsLineNumber > -1){
            printOnStreams("WRITING CONTENTS OF CFG FILE", true, false);
            /**
             * We are trying to replace the version number in line number 'N'.
             * so we first add the new version number string at line number 'N'
             * after we remove line number 'N + 1', 
             * thus we replace version number
             */
            String versionNumber = serverConfigMap.getOrDefault(KEY_VERSION, localConfigMap.get(KEY_VERSION)).trim();
            int versionLineNumber = argOptionsLineNumber + 5;
            lines.add(versionLineNumber, versionNumber);
            lines.remove(versionLineNumber + 1);
            
            //Here we are chaging the "app.version" entry for application manifest
            versionLineNumber = 3;
            lines.add(versionLineNumber, versionNumber);
            lines.remove(versionLineNumber + 1);
            
            Files.write(cfgFile.toPath(), lines, StandardOpenOption.TRUNCATE_EXISTING);
        }
    }
    
    private static void findVersionPosition_Mac(HashMap<String, String> serverConfigMap, HashMap<String, String> localConfigMap) throws IOException {
        FileFilter cfgFilter = (File pathname) -> pathname.isFile() && pathname.getName().endsWith(".plist");
        
        for (File f: updatesTargetDirectory.getParentFile().listFiles()) {
            printOnStreams("file: " + f.getAbsolutePath(), true, false);
        }
        
        File cfgFile = updatesTargetDirectory.getParentFile().listFiles(cfgFilter)[0];
        List<String> lines = Files.readAllLines(cfgFile.toPath(), Charset.forName("UTF-8"));
        
        int cFBundleShortVersionString = -1;
        int cFBundleVersion = -1;
        //reverse loop; starting from bottom
        for (int i = lines.size() -1; i >= 0; i--){
            String line = lines.get(i).trim();
            if (line.equalsIgnoreCase("<key>CFBundleShortVersionString</key>")) {
                cFBundleShortVersionString = i + 1;
            } else if (line.equalsIgnoreCase("<key>CFBundleVersion</key>")) {
                cFBundleVersion = i + 1;
            }
        }
        
        printOnStreams("WRITING CONTENTS OF CFG FILE", true, false);
        
        String versionNumber = serverConfigMap.getOrDefault(KEY_VERSION, localConfigMap.get(KEY_VERSION)).trim();
        
        if (cFBundleShortVersionString > -1) {
            int versionLineNumber = cFBundleShortVersionString;
            lines.add(versionLineNumber, "  <string>" + versionNumber + "</string>");
            lines.remove(versionLineNumber + 1);
        }
        
        if (cFBundleVersion > -1) {
            int versionLineNumber = cFBundleVersion;
            lines.add(versionLineNumber, "  <string>" + versionNumber + "</string>");
            lines.remove(versionLineNumber + 1);
        }
        
        Files.write(cfgFile.toPath(), lines, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }
    
    private static MessageDigest md;
    private static StringBuilder sb = new StringBuilder();

    private static String getCipheredString(String s) {
        if (md == null) {
            try { md = MessageDigest.getInstance("MD5"); } catch (Exception e) {}
        }
        sb.delete(0, sb.length());
        if (s != null) {
            md.update(s.getBytes());
            byte[] mdbytes = md.digest();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff)
                        + 0x100, 16).substring(1));
            }
        }
        return sb.toString();            
    }
}
