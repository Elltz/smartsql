
package np.com.ngopal.smart.sql.query.clauses;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class OrderByClause implements QueryClause
{
    private List<ColumnReferenceClause> columns;
    
    private String source;
    public OrderByClause(String source)
    {
        this.source = source;
    }
    
    public String getSource() 
    {
        return source;
    }

    public List<ColumnReferenceClause> getColumns()
    {
        return columns;
    }
    public void setColumns(List<ColumnReferenceClause> columns)
    {
        this.columns = columns;
    }
    
    
}
