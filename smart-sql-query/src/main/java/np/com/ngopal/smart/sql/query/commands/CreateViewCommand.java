package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class CreateViewCommand extends AbstractCommand {

    private final String source;
    
    
    public CreateViewCommand(String source) {
        this.source = source;
    }
  

    public String getSource() {
        return source;
    }
    
    
}
