
package np.com.ngopal.smart.sql.query.clauses;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface TableReferenceClause extends QueryClause
{
    String getName();
    
    void setAlias(String alias);
    String getAlias();
    
    void setAs(boolean as);
    boolean isAs();
    
    List<JoinClause> getJoinClauses();
    void setJoinClauses(List<JoinClause> joinClauses);
}
