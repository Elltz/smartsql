package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CallCommand extends AbstractCommand {
    
    private final String source;
    private String definition;
    
    public CallCommand(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    
    
}