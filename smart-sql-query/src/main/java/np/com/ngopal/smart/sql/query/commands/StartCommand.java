
package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class StartCommand extends AbstractCommand
{
    private String definition;
    private boolean slave = false;
    private boolean transaction = false;
    private final String source;
    
    public StartCommand(String source) {
        this.source = source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public boolean isSlave() {
        return slave;
    }
    public void setSlave(boolean slave) {
        this.slave = slave;
    }

    public boolean isTransaction() {
        return transaction;
    }
    public void setTransaction(boolean transaction) {
        this.transaction = transaction;
    }
    
    
    
    public String getSource() {
        return source;
    }
}
