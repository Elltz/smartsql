package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class AlterTableKey {
    
    private String constraint;
    private boolean primaryKey = false;
    private boolean uniqueKey = false;
    private boolean foreignKey = false;
    private String key;
    
    private String column;
    
    
    
    private String index;
    private String checkExpresion;
    
    public AlterTableKey() {
    }

    
    public boolean isEmpty() {
        return constraint == null && key == null && (column == null || column.trim().isEmpty()) && index == null && checkExpresion == null && !primaryKey && !uniqueKey && !foreignKey;
    }
    
    public void setCheckExpresion(String checkExpresion) {
        this.checkExpresion = checkExpresion;
    }

    public String getCheckExpresion() {
        return checkExpresion;
    }
    
    
    
    

    public void setIndex(String index) {
        this.index = index;
    }
    public String getIndex() {
        return index;
    }

    
    
    public void setConstraint(String constraint) {
        this.constraint = constraint;
    }
    public String getConstraint() {
        return constraint;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }
    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setUniqueKey(boolean uniqueKey) {
        this.uniqueKey = uniqueKey;
    }
    public boolean isUniqueKey() {
        return uniqueKey;
    }

    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }
    public boolean isForeignKey() {
        return foreignKey;
    }

    public void setKey(String key) {
        this.key = key;
    }
    public String getKey() {
        return key;
    }

    public void setColumn(String column) {
        this.column = column;
    }
    public String getColumn() {
        return column;
    }
    
    
}
