
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class WhereClause implements QueryClause
{
    private String condition;
    private String source;
    public WhereClause(String source)
    {
        this.source = source;
    }
    
    public String getSource() 
    {
        return source;
    }

    public String getCondition()
    {
        return condition;
    }
    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    
    
}
