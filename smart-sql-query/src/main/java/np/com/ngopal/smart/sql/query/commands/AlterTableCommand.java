
package np.com.ngopal.smart.sql.query.commands;

import java.util.List;
import np.com.ngopal.smart.sql.query.clauses.AlterTableSpecification;


/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class AlterTableCommand extends AbstractCommand
{
    private boolean ignore;
    private String tableName;
    private List<AlterTableSpecification> alterSpecifications;
    
    private final String alterSource;
    
    public AlterTableCommand(final String alterSource)
    {
        this.alterSource = alterSource;
    }
    public String getSource() 
    {
        return alterSource;
    }

    public boolean isIgnore() {
        return ignore;
    }
    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getTableName() {
        return tableName;
    }

    public void setAlterSpecifications(List<AlterTableSpecification> alterSpecifications) {
        this.alterSpecifications = alterSpecifications;
    }
    public List<AlterTableSpecification> getAlterSpecifications() {
        return alterSpecifications;
    }

    
    
}
