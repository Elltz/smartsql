package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ShowCommand extends AbstractCommand {
    
    private final String source;
    private String definition;
    
    public ShowCommand(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    
    
}
