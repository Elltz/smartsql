
package np.com.ngopal.smart.sql.query.clauses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ColumnResult {
    
    private final Map<Integer, String> subQueries = new HashMap<>();
    private final Map<Integer, String> functions = new HashMap<>();
    private final Map<Integer, String> quotes = new HashMap<>();
    
    private final List<String> columns = new ArrayList<>();
    
    private Integer queryIndex = 0;
    private Integer functionIndex = 0;
    private Integer quoteIndex = 0;
    
    
    public Integer nextQueryIndex() {
        return queryIndex++;
    }
    
    public Integer nextFunctionIndex() {
        return functionIndex++;
    }
    
    public Integer nextQuoteIndex() {
        return quoteIndex++;
    }
     
    
    public void putSubQuery(Integer index, String query)  {
        subQueries.put(index, query);
    }
    public String getSubQuery(Integer index) {
        return subQueries.get(index);
    }
    
    public void putFunction(Integer index, String function)  {
        functions.put(index, function);
    }
    public String getFunction(Integer index) {
        return functions.get(index);
    }
    
    public void putQuote(Integer index, String query)  {
        quotes.put(index, query);
    }
    public String getQuote(Integer index) {
        return quotes.get(index);
    }

    public void addColumn(String column) {
        columns.add(column);
    }
    
    public List<String> getColumns() {
        return columns;
    }
}
