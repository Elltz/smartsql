
package np.com.ngopal.smart.sql.query.commands;

import java.util.List;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.clauses.SetClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class UpdateCommand extends AbstractCommand implements WhereClauseProvider
{
    private List<TableReferenceClause> tableClauses;
    private SetClause setClause;
    private WhereClause whereClause;
    private OrderByClause orderByClause;
    private LimitClause limitClause;
    
    private final String updateSource;
    
    private boolean lowPriority = false;
    private boolean ignore = false;
    
    public UpdateCommand(final String updateSource)
    {
        this.updateSource = updateSource;
    }

    public String getName() 
    {
        return updateSource;
    }

    public boolean isLowPriority() {
        return lowPriority;
    }
    public void setLowPriority(boolean lowPriority) {
        this.lowPriority = lowPriority;
    }

    public boolean isIgnore() {
        return ignore;
    }
    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    
    
    public void setSetClause(SetClause setClause)
    {
        this.setClause = setClause;
    }

    public SetClause getSetClause()
    {
        return setClause;
    }
    

    public void setTableClauses(List<TableReferenceClause> tableClauses)
    {
        this.tableClauses = tableClauses;
    }

    public List<TableReferenceClause> getTableClauses()
    {
        return tableClauses;
    }

    public void setWhereClause(WhereClause whereClause)
    {
        this.whereClause = whereClause;
    }

    @Override
    public WhereClause getWhereClause()
    {
        return whereClause;
    }


    public OrderByClause getOrderByClause()
    {
        return orderByClause;
    }

    public void setOrderByClause(OrderByClause orderByClause)
    {
        this.orderByClause = orderByClause;
    }
    
    public void setLimitClause(LimitClause limitClause)
    {
        this.limitClause = limitClause;
    }

    public LimitClause getLimitClause()
    {
        return limitClause;
    }
    
}
