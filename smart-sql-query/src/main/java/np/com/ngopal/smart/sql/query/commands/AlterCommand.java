package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class AlterCommand extends AbstractCommand
{
    private String definition;
    private final String source;
        
    public AlterCommand(String source) {
        this.source = source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    
    
    public String getSource() {
        return source;
    }
}
