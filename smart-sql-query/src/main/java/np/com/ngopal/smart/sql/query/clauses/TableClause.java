
package np.com.ngopal.smart.sql.query.clauses;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class TableClause implements TableReferenceClause
{
    private final String source;
    
    private final String name;
    private String alias;
    private boolean as = false;
    
    private String others;
    
    private List<JoinClause> joinClauses;
    
    public TableClause(final String source, final String name, final String alias, String others)
    {
        this.name = name;
        this.alias = alias;
        this.source = source;
        this.others = others;
    }

    @Override
    public boolean isAs() {
        return as;
    }
    @Override
    public void setAs(boolean as) {
        this.as = as;
    }

    
    
    @Override
    public String getAlias()
    {
        return alias;
    }
    @Override
    public void setAlias(String alias)
    {
        this.alias = alias;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public List<JoinClause> getJoinClauses() 
    {
        return joinClauses;
    }

    @Override
    public void setJoinClauses(List<JoinClause> joinClauses) 
    {
        this.joinClauses = joinClauses;
    }

    @Override
    public String toString() {
        return source;
    }
    
    
}
