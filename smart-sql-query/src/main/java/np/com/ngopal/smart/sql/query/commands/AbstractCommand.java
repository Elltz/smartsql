package np.com.ngopal.smart.sql.query.commands;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class AbstractCommand implements QueryClause {
    
    private final Map<String, String> subQueries = new HashMap<>();
    private final Map<String, String> querySources = new HashMap<>();
    private final Map<String, Integer[]> subQueriesIdx = new HashMap<>();
    private final Map<String, String> strings = new HashMap<>();
    private final Map<String, String> functions = new HashMap<>();
    private final Map<String, SelectCommand> subUnions = new HashMap<>();
    
    public Collection<String> getQuerySources() {
        return querySources.values();
    }
    
    public String getSubQuery(String key) {
        return subQueries.get(key);
    }
    public void putSubQuery(String key, String value) {
        subQueries.put(key, value);
    }
    
    public String getQuerySource(String key) {
        return querySources.get(key);
    }
    public void putQuerySource(String key, String value) {
        querySources.put(key, value);
    }
    
    public String getString(String key) {
        return strings.get(key);
    }
    public void putString(String key, String value) {
        strings.put(key, value);
    }
    
    public String getFunction(String key) {
        return functions.get(key);
    }
    public void putFunction(String key, String value) {
        functions.put(key, value);
    }
    
    public Integer[] getSubQueriesIdx(String key) {
        return subQueriesIdx.get(key);
    }
    public void putSubQueriesIdx(String key, Integer[] value) {
        subQueriesIdx.put(key, value);
    }

    public Iterable<String> getQuerySourcesValues() {
        return querySources.values();
    }

    public void removeSubQuery(String key) {
        subQueries.remove(key);
    }

    public Iterable<Map.Entry<String, String>> getSubQueriesEntrySet() {
        return subQueries.entrySet();
    }
    
    public SelectCommand getSubUnion(String key) {
        return subUnions.get(key);
    }
    public void putSubUnion(String key, SelectCommand value) {
        subUnions.put(key, value);
    }
}
