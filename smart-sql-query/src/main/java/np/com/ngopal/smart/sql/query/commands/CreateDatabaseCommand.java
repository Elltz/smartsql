package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class CreateDatabaseCommand extends AbstractCommand {

    private final String source;
    
    private String databeName;
        
    
    public CreateDatabaseCommand(String source) {
        this.source = source;
    }

    public String getDatabaseName() {
        return databeName;
    }
    public void setDatabaseName(String databeName) {
        this.databeName = databeName;
    }
      
    
    
    
    

    public String getSource() {
        return source;
    }
    
    
}
