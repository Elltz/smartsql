package np.com.ngopal.smart.sql.query.commands;

import np.com.ngopal.smart.sql.query.clauses.FromClause;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface FromProvider {

    public FromClause getFromClause();
    
}
