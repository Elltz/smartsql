package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class GrantCommand extends AbstractCommand {

    private final String source;
    
    private String definition;
    
    public GrantCommand(String source) {
        this.source = source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    
    

    public String getSource() {
        return source;
    }
    
    
}
