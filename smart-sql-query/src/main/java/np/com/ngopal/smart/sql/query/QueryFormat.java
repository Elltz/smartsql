package np.com.ngopal.smart.sql.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.query.clauses.AlterTableKey;
import np.com.ngopal.smart.sql.query.clauses.AlterTableSpecification;
import np.com.ngopal.smart.sql.query.clauses.ColumnClause;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.ColumnResult;
import np.com.ngopal.smart.sql.query.clauses.ConditionResult;
import np.com.ngopal.smart.sql.query.clauses.CreateTableColumnDefinition;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.clauses.IntoTableClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OnDuplicateKeyUpdateClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.RenameTableClause;
import np.com.ngopal.smart.sql.query.clauses.SetClause;
import np.com.ngopal.smart.sql.query.clauses.TableClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.ValuesClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;
import np.com.ngopal.smart.sql.query.commands.AbstractCommand;
import np.com.ngopal.smart.sql.query.commands.AlterCommand;
import np.com.ngopal.smart.sql.query.commands.AlterTableCommand;
import np.com.ngopal.smart.sql.query.commands.AlterTableSpaceCommand;
import np.com.ngopal.smart.sql.query.commands.CallCommand;
import np.com.ngopal.smart.sql.query.commands.CheckTableCommand;
import np.com.ngopal.smart.sql.query.commands.CreateDatabaseCommand;
import np.com.ngopal.smart.sql.query.commands.CreateTableCommand;
import np.com.ngopal.smart.sql.query.commands.CreateUserCommand;
import np.com.ngopal.smart.sql.query.commands.CreateViewCommand;
import np.com.ngopal.smart.sql.query.commands.DeclareCommand;
import np.com.ngopal.smart.sql.query.commands.DeleteCommand;
import np.com.ngopal.smart.sql.query.commands.DropCommand;
import np.com.ngopal.smart.sql.query.commands.FlushCommand;
import np.com.ngopal.smart.sql.query.commands.GrantCommand;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.query.commands.LoadDataCommand;
import np.com.ngopal.smart.sql.query.commands.LockTableCommand;
import np.com.ngopal.smart.sql.query.commands.RenameCommand;
import np.com.ngopal.smart.sql.query.commands.ReplaceCommand;
import np.com.ngopal.smart.sql.query.commands.ReturnCommand;
import np.com.ngopal.smart.sql.query.commands.RevokeCommand;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.query.commands.SetCommand;
import np.com.ngopal.smart.sql.query.commands.ShowCommand;
import np.com.ngopal.smart.sql.query.commands.StartCommand;
import np.com.ngopal.smart.sql.query.commands.TruncateCommand;
import np.com.ngopal.smart.sql.query.commands.UpdateCommand;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public final class QueryFormat {
    
    
    // <editor-fold desc=" Contants and Patterns ">
    
    private static final Pattern STRING__PATTERN0        =  Pattern.compile("(\".*?[^\\\\]\")", Pattern.CASE_INSENSITIVE);
    private static final Pattern STRING__PATTERN1        =  Pattern.compile("(\'.*?[^\\\\]\')", Pattern.CASE_INSENSITIVE);
    
    private static final String COMMENT__START  = "/*";
    private static final String COMMENT__END    = "*/";
    
    protected static final String SELECT                = "\\bselect\\b";
    protected static final String UPDATE                = "\\bupdate\\b";
    protected static final String INSERT                = "\\binsert\\b";
    protected static final String DELETE                = "\\bdelete\\b";
    protected static final String REPLACE               = "\\breplace\\b";
    protected static final String ALTER                 = "\\balter\\b";    
    protected static final String START                 = "\\bstart\\b";    
    protected static final String RENAME                = "\\brename\\b";
    protected static final String DROP                  = "\\bdrop\\b";
    protected static final String TRUNCATE              = "\\btruncate\\b";
    protected static final String CREATE                = "\\bcreate\\b";
    protected static final String SHOW                  = "\\bshow\\b";
    protected static final String REVOKE                = "\\brevoke\\b";
    protected static final String CALL                  = "\\bcall\\b";
    
    protected static final String FROM                  = "\\bfrom\\b";
    protected static final String WHERE                 = "\\bwhere\\b";
    protected static final String GROUP                 = "\\bgroup\\b";
    protected static final String HAVING                = "\\bhaving\\b";
    protected static final String ORDER                 = "\\border\\b";
    protected static final String BY                    = "\\bby\\b";
    protected static final String LIMIT                 = "\\blimit\\b";
    protected static final String JOIN                  = "\\bjoin\\b";
    protected static final String CROSS                 = "\\bcross\\b\\s+";
    protected static final String OUTER                 = "\\bouter\\b\\s+";
    protected static final String CASE                  = "\\bcase\\b";
    protected static final String END                   = "\\bend\\b";
    protected static final String USING                 = "\\busing\\b";
    protected static final String LEFT                  = "\\bleft\\b\\s+";
    protected static final String RIGHT                 = "\\bright\\b\\s+";
    protected static final String INNER                 = "\\binner\\b\\s+";
    protected static final String NATURAL               = "\\bnatural\\b\\s+";
    protected static final String JOIN_CI               = "\\b(?i)join\\b";
    protected static final String LEFT_CI               = "\\b(?i)left\\b\\s+";
    protected static final String RIGHT_CI              = "\\b(?i)right\\b\\s+";
    protected static final String INNER_CI              = "\\b(?i)inner\\b\\s+";
    protected static final String NATURAL_CI            = "\\b(?i)natural\\b\\s+";
    protected static final String OUTER_CI              = "\\b(?i)outer\\b\\s+";
    protected static final String CROSS_CI              = "\\b(?i)cross\\b\\s+";
    protected static final String ON                    = "\\bon\\b";
    protected static final String AS                    = "\\bas\\b";
    protected static final String ASC                   = "\\basc\\b";
    protected static final String DESC                  = "\\bdesc\\b";    
    protected static final String SET                   = "\\bset\\b";
    protected static final String RETURN                = "\\breturn\\b";
    protected static final String DECLARE               = "\\bdeclare\\b";
    protected static final String INTO                  = "\\binto\\b";
    protected static final String VALUES                = "\\bvalues\\b";
    protected static final String VALUE                 = "\\bvalue\\b";
    protected static final String TABLE                 = "\\btable\\b";
    protected static final String PARTITION_BY          = "\\bpartition\\b\\s+\\bby\\b";
    protected static final String ADD                   = "\\badd\\b";
    protected static final String CHANGE                = "\\bchange\\b";
    protected static final String MODIFY                = "\\bmodify\\b";
    protected static final String TO                    = "\\bto\\b";
    protected static final String IF                    = "\\bif\\b";
    protected static final String NOT                   = "\\bnot\\b";
    protected static final String EXISTS                = "\\bexists\\b";
    protected static final String DATABASE              = "\\bdatabase\\b";
    protected static final String SCHEMA                = "\\bschema\\b";
    protected static final String FUNCTION              = "\\bfunction\\b";
    protected static final String EVENT                 = "\\bevent\\b";
    protected static final String PROCEDURE             = "\\bprocedure\\b";
    protected static final String TRIGGER               = "\\btrigger\\b";
    protected static final String SERVER                = "\\bserver\\b";
    protected static final String TABLESPACE            = "\\btablespace\\b";
    protected static final String LOGFILE_GROUP         = "\\blogfile\\s+group\\b";
    protected static final String INDEX                 = "\\bindex\\b";
    protected static final String VIEW                  = "\\bview\\b";
    protected static final String TEMPORARY             = "\\btemporary\\b";
    protected static final String IGNORE                = "\\bignore\\b";
    protected static final String LIKE                  = "\\blike\\b";
    protected static final String CONSTRAINT            = "\\bconstraint\\b";
    protected static final String PRIMARY               = "\\bprimary\\b";
    protected static final String KEY                   = "\\bkey\\b";
    protected static final String BTREE                 = "\\bbtree\\b";
    protected static final String HASH                  = "\\bhash\\b";
    protected static final String UNIQUE                = "\\bunique\\b";
    protected static final String FULLTEXT              = "\\bfulltext\\b";
    protected static final String SPATIAL               = "\\bspatial\\b";
    protected static final String FOREIGN               = "\\bforeign\\b";
    protected static final String CHECK                 = "\\bcheck\\b";
    protected static final String DATAFILE              = "\\bdatafile\\b";
    protected static final String OUTFILE               = "\\boutfile\\b";
    protected static final String ALL                   = "\\ball\\b";
    protected static final String DISTINCT              = "\\bdistinct\\b";
    protected static final String DISTINCTROW           = "\\bdistinctrow\\b";
    protected static final String HIGH_PRIORITY         = "\\bhigh_priority\\b";
    protected static final String LOW_PRIORITY          = "\\blow_priority\\b";
    protected static final String DELAYED               = "\\bdelayed\\b";
    protected static final String MAX_STAT_TIME         = "\\bmax_statement_time\\b";
    protected static final String STRAIGHT_JOIN         = "\\bstraight_join\\b";
    protected static final String SQL_SMALL_RESULT      = "\\bsql_small_result\\b";
    protected static final String SQL_BIG_RESULT        = "\\bsql_big_result\\b";
    protected static final String SQL_BUFFER_RESULT     = "\\bsql_buffer_result\\b";
    protected static final String SQL_CALC_FOUND_ROWS   = "\\bsql_calc_found_rows\\b";
    protected static final String SQL_NO_CACHE          = "\\bsql_no_cache\\b";
    protected static final String SQL_CACHE             = "\\bsql_cache\\b";
    protected static final String PARTITION             = "\\bpartition\\b";
    protected static final String OFFSET                = "\\boffset\\b";
    protected static final String QUICK                 = "\\bquick\\b";
    protected static final String TRANSACTION           = "\\btransaction\\b";
    protected static final String SLAVE                 = "\\bslave\\b";
    protected static final String USER                  = "\\buser\\b";
    protected static final String GRANT                 = "\\bgrant\\b";
    protected static final String FLUSH                 = "\\bflush\\b";
    protected static final String LOAD                  = "\\bload\\b";
    protected static final String DATA                  = "\\bdata\\b";
    protected static final String LOCK                  = "\\block\\b";
            
    
    
    protected static final String TEMPORARY_TABLE           = "\\btemporary\\s+table\\b";        
    protected static final String ON_DUPLICATE_KEY_UPDATE   = "\\bon\\s+duplicate\\s+key\\s+update\\b";
    protected static final String FOR_UPDATE                = "\\bfor\\s+update\\b";
    protected static final String LOCK_IN_SHARE_MODE        = "\\block\\s+in\\s+share\\s+mode\\b";
    protected static final String WITH_ROLLUP               = "\\bwith\\s+rollup\\b";
    
    protected static final String DROP_TARGET               = 
        "(" + TABLE + "|" + SCHEMA + "|" + TEMPORARY_TABLE + "|" + DATABASE + "|" + FUNCTION + "|" + EVENT + "|" + PROCEDURE + "|" +
        TRIGGER + "|" + SERVER + "|" + TABLESPACE + "|(" + LOGFILE_GROUP + ")|" + INDEX + "|" + VIEW + ")";
    
    protected static final Pattern SELECT_QUERY__PATTERN = Pattern.compile("^" + SELECT, Pattern.CASE_INSENSITIVE);
    protected static final Pattern UPDATE_QUERY__PATTERN = Pattern.compile("^" + UPDATE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern INSERT_QUERY__PATTERN = Pattern.compile("^" + INSERT, Pattern.CASE_INSENSITIVE);
    protected static final Pattern DELETE_QUERY__PATTERN = Pattern.compile("^" + DELETE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern REPLACE_QUERY__PATTERN = Pattern.compile("^" + REPLACE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern ALTER_QUERY__PATTERN = Pattern.compile("^" + ALTER, Pattern.CASE_INSENSITIVE);
    protected static final Pattern START_QUERY__PATTERN = Pattern.compile("^" + START, Pattern.CASE_INSENSITIVE);
    protected static final Pattern RENAME_QUERY__PATTERN = Pattern.compile("^" + RENAME, Pattern.CASE_INSENSITIVE);
    protected static final Pattern DROP_QUERY__PATTERN = Pattern.compile("^" + DROP, Pattern.CASE_INSENSITIVE);
    protected static final Pattern TRUNCATE_QUERY__PATTERN = Pattern.compile("^" + TRUNCATE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern SHOW_QUERY__PATTERN = Pattern.compile("^" + SHOW, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CREATE_QUERY__PATTERN = Pattern.compile("^" + CREATE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CREATE_TABLE_QUERY__PATTERN = Pattern.compile("^" + CREATE + "\\s+(" + TEMPORARY + "\\s+)?" + TABLE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CREATE_USER_QUERY__PATTERN = Pattern.compile("^" + CREATE + "\\s+" + USER, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CREATE_DATABASE_QUERY__PATTERN = Pattern.compile("^" + CREATE + "\\s+" + DATABASE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CREATE_VIEW_QUERY__PATTERN = Pattern.compile("^" + CREATE + "\\s+" + VIEW, Pattern.CASE_INSENSITIVE);
    protected static final Pattern SET_QUERY__PATTERN = Pattern.compile("^" + SET, Pattern.CASE_INSENSITIVE);
    protected static final Pattern RETURN_QUERY__PATTERN = Pattern.compile("^" + RETURN, Pattern.CASE_INSENSITIVE);
    protected static final Pattern DECLARE_QUERY__PATTERN = Pattern.compile("^" + DECLARE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern GRANT_QUERY__PATTERN = Pattern.compile("^" + GRANT, Pattern.CASE_INSENSITIVE);
    protected static final Pattern FLUSH_QUERY__PATTERN = Pattern.compile("^" + FLUSH, Pattern.CASE_INSENSITIVE);
    protected static final Pattern REVOKE_QUERY__PATTERN = Pattern.compile("^" + REVOKE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CALL_QUERY__PATTERN = Pattern.compile("^" + CALL, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CHECK_QUERY__PATTERN = Pattern.compile("^" + CHECK, Pattern.CASE_INSENSITIVE);
    protected static final Pattern CHECK_TABLE_QUERY__PATTERN = Pattern.compile("^" + CHECK + "\\s+" + TABLE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern LOCK_TABLE_QUERY__PATTERN = Pattern.compile("^" + LOCK + "\\s+" + TABLE, Pattern.CASE_INSENSITIVE);
    protected static final Pattern LOAD_DATA_QUERY__PATTERN = Pattern.compile("^" + LOAD + "\\s+" + DATA, Pattern.CASE_INSENSITIVE);
    
    
    protected static final String ELEMENT = "[\\?_@'`\"\\#\\w\\d\\>\\<\\=\\+\\-\\*\\/\\s\\.]*";
    
    protected static final Pattern SELECT__PATTERN = Pattern.compile(
        "^" + SELECT + "\\s*"
                + "(?<distinctType>(" + ALL + "|" + DISTINCT + "|" + DISTINCTROW + ")\\s+)?"
                + "(?<highPriority>" + HIGH_PRIORITY + "\\s+)?"
                + "(" + MAX_STAT_TIME + "\\s*=\\s*(?<maxStatTime>\\d+)\\s+)?"
                + "(?<straightJoin>" + STRAIGHT_JOIN + "\\s+)?"
                + "(?<sqlSmallResult>" + SQL_SMALL_RESULT + "\\s+)?"
                + "(?<sqlBigResult>" + SQL_BIG_RESULT + "\\s+)?"
                + "(?<sqlBufferResult>" + SQL_BUFFER_RESULT + "\\s+)?"
                + "(?<sqlType>(" + SQL_CACHE + "|" + SQL_NO_CACHE + ")\\s+)?"
                + "(?<sqlCalcFoundRows>" + SQL_CALC_FOUND_ROWS + "\\s+)?"
            + "(?<columnsSection>(.*?))"
            + "(" + INTO + "(?<intoSelectSection>(.*?)))?"
            + "(" + FROM + "(?<fromSection>(.*?))"
            + "(" + PARTITION + "(?<partitionSection>(.*?)))?"
            + "(" + WHERE + "(?<whereSection>(.*?)))?"
            + "(" + GROUP + "\\s+" + BY + "(?<groupBySection>(.*?))(?<withRollup>" + WITH_ROLLUP + "\\s*)?)?"
            + "(" + HAVING + "(?<havingSection>(.*?)))?"
            + "(" + ORDER + "\\s+" + BY + "(?<orderBySection>(.*?)))?"
            + "(" + LIMIT + "(?<limitSection>(.*?)))?"
            + "(" + PROCEDURE + "(?<procedureSection>(.*?)))?"
            + "(" + INTO + "(?<intoSection>(.*?)))?"
            + "((?<forUpdate>" + FOR_UPDATE + ")|(?<lockIn>" + LOCK_IN_SHARE_MODE + "))?\\s*)?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern UPDATE__PATTERN = Pattern.compile(
        "^" + UPDATE 
            + "(?<lowpriority>\\s+" + LOW_PRIORITY + ")?"
            + "(?<ignore>\\s+" + IGNORE + ")?"
            + "\\s+(?<tableSection>(.*?))"
            + "(" + SET + "\\s+(?<setSection>(.*?)))?"
            + "(" + WHERE + "\\s+(?<whereSection>(.*?)))?"
            + "(" + ORDER + "\\s+" + BY + "\\s+(?<orderBySection>(.*?)))?"
            + "(" + LIMIT + "\\s+(?<limitSection>(.*?)))?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern DELETE__PATTERN = Pattern.compile(
        "^" + DELETE 
            + "(?<lowpriority>\\s+" + LOW_PRIORITY + ")?" 
            + "(?<quick>\\s+" + QUICK + ")?" 
            + "(?<ignore>\\s+" + IGNORE + ")?" 
            + "\\s+(?<tablesSection>(.*?))"
            + "(" + FROM + "\\s+(?<fromSection>(.*?)))?"
            + "(" + PARTITION + "(?<partition>\\s+\\(.*?\\)))?"
            + "(" + USING + "\\s+(?<usingSection>(.*?)))?"
            + "(" + WHERE + "\\s+(?<whereSection>(.*?)))?"
            + "(" + ORDER + "\\s+" + BY + "(?<orderBySection>(.*?)))?"
            + "(" + LIMIT + "\\s+(?<limitSection>(.*?)))?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern INSERT__PATTERN = Pattern.compile(
        "^" + INSERT + "\\s*"
                + "(?<priority>(" + LOW_PRIORITY + "|" + DELAYED + "|" + HIGH_PRIORITY + ")\\s*)?"
                + "(?<ignore>" + IGNORE + "\\s*)?"
            + "(?<intoClause>\\s+" + INTO + ")?"
            + "\\s+(?<tableSection>(.*?))?"
            + "(" + PARTITION + "(?<partition>\\s+\\(.*?\\)))?"
            + "(\\s*\\((?<columnsSection>.*?)\\)\\s*)?"
            + "(((?<values>" + VALUES + ")|(?<value>" + VALUE + "))\\s*(?<valuesSection>(.*?)))?"
            + "(" + SET + "\\s+(?<setSection>(.*?)))?"
            + "(" + SELECT + "\\s+(?<selectSection>(.*?)))?"
            + "(" + ON_DUPLICATE_KEY_UPDATE + "\\s+(?<onDuplicateKeyUpdateSection>(.*?)))?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern REPLACE__PATTERN = Pattern.compile(
        "^" + REPLACE + "\\s*"
                + "(?<priority>(" + LOW_PRIORITY + "|" + DELAYED + "|" + HIGH_PRIORITY + ")\\s*)?"
                + "(?<ignore>" + IGNORE + "\\s*)?"
            + "(?<intoClause>\\s+" + INTO + ")?"
            + "\\s+(?<tableSection>(.*?))?"
            + "(" + PARTITION + "(?<partition>\\s+\\(.*?\\)))?"
            + "(\\s*\\((?<columnsSection>.*?)\\)\\s*)?"
            + "(((?<values>" + VALUES + ")|(?<value>" + VALUE + "))\\s*(?<valuesSection>(.*?)))?"
            + "(" + SET + "\\s+(?<setSection>(.*?)))?"
            + "(" + SELECT + "\\s+(?<selectSection>(.*?)))?"
            + "(" + ON_DUPLICATE_KEY_UPDATE + "\\s+(?<onDuplicateKeyUpdateSection>(.*?)))?"
        , Pattern.CASE_INSENSITIVE);
    
   
    protected static final Pattern SHOW__PATTERN = Pattern.compile(
        "^" + SHOW + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern SET__PATTERN = Pattern.compile(
        "^" + SET + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern RETURN__PATTERN = Pattern.compile(
        "^" + RETURN + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern DECLARE__PATTERN = Pattern.compile(
        "^" + DECLARE + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern GRANT__PATTERN = Pattern.compile(
        "^" + GRANT + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern FLUSH__PATTERN = Pattern.compile(
        "^" + FLUSH + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern ALTER_TABLE__PATTERN = Pattern.compile(
        "^" + ALTER + "\\s+(?<ignore>" + IGNORE + "\\s+)?" + TABLE + "\\s+(?<tableName>([0-9a-zA-Z$_\\.`])+)" + "(?<definition>\\s+.*)?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern START__PATTERN = Pattern.compile(
        "^" + START + "\\s+((?<transaction>" + TRANSACTION + ")|(?<slave>" + SLAVE + "))"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern ALTER_TABLESPACE__PATTERN = Pattern.compile(
        "^" + ALTER + "\\s+" + TABLESPACE + "\\s+(?<tableSpaceName>([0-9a-zA-Z$_\\.`])+)\\s+" +
        "((?<add>" + ADD + ")|(?<drop>" + DROP + "))\\s+" + DATAFILE + "\\s+'(?<fileName>.*?)'\\s*" +
        "(?<definition>.*)?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern ALTER__PATTERN = Pattern.compile(
        "^" + ALTER + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern RENAME__PATTERN = Pattern.compile(
        "^" + RENAME + "\\s+" + TABLE + "(?<tableSection>(.*?))"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern RENAME_TABLE__PATTERN = Pattern.compile(
        "(?<newTableSection>.*)" + TO + "(?<oldTableSection>.*)"
        , Pattern.CASE_INSENSITIVE);
    
    
    protected static final Pattern DROP__PATTERN = Pattern.compile(
        "^" + DROP + "(?<dropTargetSection>\\s+" + DROP_TARGET + ")?(?<ifExistsSection>\\s+" + IF + "\\s+" + EXISTS + ")?(?<tableSection>(.*?))?"
        , Pattern.CASE_INSENSITIVE);
    
    
    protected static final Pattern TRUNCATE__PATTERN = Pattern.compile(
        "^" + TRUNCATE + "\\s+(?<hasTableSection>" + TABLE + ")?(?<tableSection>(.*?))?"
        , Pattern.CASE_INSENSITIVE);
    
    
    protected static final Pattern CREATE_TABLE__PATTERN = Pattern.compile(
        "^" + CREATE + "\\s+(?<temporarySection>" + TEMPORARY + "\\s+)?" + TABLE + "\\s+(?<ifNotExistsSection>" + IF + "\\s+" + NOT + "\\s+" + EXISTS + "\\s+)?" + "(?<tableSection>.*?)" + "(\\s+" + PARTITION_BY + ".*)?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern CREATE_USER__PATTERN = Pattern.compile(
        "^" + CREATE + "\\s+" + USER + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern CREATE_DATABASE__PATTERN = Pattern.compile(
        "^" + CREATE + "\\s+" + DATABASE + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern CHECK_TABLE__PATTERN = Pattern.compile(
        "^" + CHECK + "\\s+" + TABLE + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern LOCK_TABLE__PATTERN = Pattern.compile(
        "^" + LOCK + "\\s+" + TABLE + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern LOAD_DATA__PATTERN = Pattern.compile(
        "^" + LOAD + "\\s+" + DATA + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern CREATE_TABLE__SELECT__PATTERN = Pattern.compile(
        "((?<ignoreSection>" + IGNORE + "\\s+)|(?<replaceSection>" + REPLACE + "\\s+))?" + "(?<asSection>" + AS + "\\s+)?" + SELECT + "(?<selectSection>(.*))?"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern CREATE_TABLE__LIKE__PATTERN = Pattern.compile(
        "(" + LIKE  + "\\s+(?<likeTableName0>.*))|(\\(\\s*" + LIKE  + "\\s+(?<likeTableName1>.*)\\))"
        , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern REVOKE__PATTERN = Pattern.compile(
        "^" + REVOKE + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern CALL__PATTERN = Pattern.compile(
        "^" + CALL + "\\s+" + "(?<definition>.*)?", Pattern.CASE_INSENSITIVE);
    
//    protected static final Pattern CREATE_TABLE__TABLE__PATTERN = Pattern.compile(
//        "(?<tableName>.*?)\\s*" + "(\\((?<createDefinition>.*)\\))?"
//        , Pattern.CASE_INSENSITIVE);
//    
    
        
    // TODO Need more tests 
    // TODO Or may be change list of columns or tables parsing
    protected static final Pattern COLUMNS_AND_TABLES__PATTERN = Pattern.compile(
            "(?<columnsAndTablesSection>(\\(?" + ELEMENT + "(\\([\\?_@'`\"\\#\\w\\d\\>\\<\\=\\+\\-\\*\\/\\s,\\.]*?\\)(" + ELEMENT + ")?)?\\)?)+),?"
            , Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern COLUMN__PATTERN = Pattern.compile(
            "((?<caseSection>\\(?" + CASE + ".*?" + END + "\\s*\\)?\\s*)?|(?<columnSection>.*?\\s*)?)(?<as>" + AS + "\\s+)?(?<aliasSection>.*?)?((?<asc>" + ASC + ")|(?<desc>" + DESC + "))?", Pattern.CASE_INSENSITIVE);
//    [)\\s+]
    
    protected static final Pattern COLUMN_FUNCTION__PATTERN = Pattern.compile(
            "(?<functionSection>\\w+\\((.*)\\))(\\s+(as\\s+)?(?<aliasSection>.*))?", Pattern.CASE_INSENSITIVE);
//    [)\\s+]
    
    protected static final Pattern SUB_UNION_PATTER = Pattern.compile("\\((\\s*#\\d+(\\s+union\\s+all\\s+#\\d+\\s*)+)\\)", Pattern.CASE_INSENSITIVE);
    
    private static final String JOIN_PART = "(" + LEFT + "(" + OUTER +")?)|(" + RIGHT + "(" + OUTER +")?)|" + INNER + "|" + CROSS + "|(" + NATURAL + "((" + LEFT + "(" + OUTER +")?)|(" + RIGHT + "(" + OUTER +")?))?)";
    
    private static final String JOIN_PART_CI = "(" + LEFT_CI + "(" + OUTER_CI +")?)|(" + RIGHT_CI + "(" + OUTER_CI +")?)|" + INNER_CI + "|" + CROSS_CI + "|(" + NATURAL_CI + "((" + LEFT_CI + "(" + OUTER_CI +")?)|(" + RIGHT_CI + "(" + OUTER_CI +")?))?)";
    // TODO Need more tests 
    protected static final Pattern JOIN__PATTERN = Pattern.compile(
            "(?<joinType>(" + JOIN_PART + ")?)" + JOIN + "(?<joinSection>.*?)((" + USING + "\\s*(?<usingSection>.*))|(" + ON + "\\s*(?<onSection>.*)))?", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern TABLE__PATTERN = Pattern.compile(
            "(?<tableSection>.*?\\s*)?(?<as>" + AS + "\\s+)?(?<aliasSection>.*?)?(" + JOIN__PATTERN + ")*", Pattern.CASE_INSENSITIVE);
   

    protected static final Pattern SUB_SELECT__PATTERN =  Pattern.compile("(\\(\\s*" + SELECT + "\\s*)", Pattern.CASE_INSENSITIVE);
 
 
    protected static final Pattern CONDITION_ANDOR_PATTER = Pattern.compile("(.*?)(\\band\\b|\\bor\\b)", Pattern.CASE_INSENSITIVE);
    protected static final Pattern CONDITION_BETWEEN_PATTER = Pattern.compile("(\\bNOT\\b\\s+)?\\bBETWEEN\\b\\s+.*?\\s+\\bAND\\b\\s+.*?\\s+", Pattern.CASE_INSENSITIVE);
    protected static final Pattern SUB_CONDITION_PATTER = Pattern.compile("#(\\d+)", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern FUNCTION_PATTERN = Pattern.compile("(\\!(\\d+))");
    protected static final Pattern SUBQUERY0_PATTERN = Pattern.compile("(\\^(\\d+))");    
    protected static final Pattern SUBQUERY_PATTERN = Pattern.compile("(\\#(\\d+))");    
    protected static final Pattern QUOTE_PATTERN = Pattern.compile("(\\&(\\d+))");
    
    private static final String SIMPLE_CONDITION__PATTERN   =  "\\<>|>=|<=|\\!=|=|>|<|\\bis\\s+not\\b|\\bis\\b";
    
    
    // col_name column_definition [CONSTRAINT [symbol]] PRIMARY KEY [index_type] (index_col_name,...) [index_option] ...
    protected static final Pattern CREATE_TABLE__PRIMARY_KEY__PATTER = Pattern.compile(
        "(?<columnDefinition>.*?)?" + 
        "(?<constraint>" + CONSTRAINT + "\\s+(?<constraintName>`?\\w+`?\\s+)?)?" +
        PRIMARY + "\\s+" + KEY + 
        "(?<using>\\s+" + USING + "(?<usingType>\\s+" + "(" + BTREE + "|" + HASH + ")" + ")?" + ")?" +
        "\\s*(?<columns>\\(.*?\\))?(?<option>\\s+.*)?", Pattern.CASE_INSENSITIVE);
    
    // {INDEX|KEY} [index_name] [index_type] (index_col_name,...) [index_option] ...
    protected static final Pattern CREATE_TABLE__INDEX_KEY__PATTER = Pattern.compile(
        "((?<index>" + INDEX + ")|(?<key>" + KEY + "))" + "(?<indexName>\\s+`?\\w+`?)?" +
        "(?<using>\\s+" + USING + "(?<usingType>\\s+" + "(" + BTREE + "|" + HASH + ")" + ")?" + ")?" +
        "\\s*(?<columns>\\(.*?\\))(?<option>\\s+.*)?", Pattern.CASE_INSENSITIVE);
    
    // [CONSTRAINT [symbol]] UNIQUE [INDEX|KEY] [index_name] [index_type] (index_col_name,...) [index_option] ...
    protected static final Pattern CREATE_TABLE__UNIQUE__PATTER = Pattern.compile(
        "(?<constraint>" + CONSTRAINT + "\\s+(?<constraintName>`?\\w+`?\\s+)?)?" +
        UNIQUE + 
        "(\\s+((?<index>" + INDEX + ")|(?<key>" + KEY + ")))?" + "(?<indexName>\\s+`?\\w+`?\\s+)?" +
        "(?<using>" + USING + "(?<usingType>\\s+" + "(" + BTREE + "|" + HASH + ")" + ")?" + ")?" +
        "\\s*(?<columns>\\(.*?\\))(?<option>\\s+.*)?", Pattern.CASE_INSENSITIVE);
    
    // {FULLTEXT|SPATIAL} [INDEX|KEY] [index_name] (index_col_name,...) [index_option] ...
    protected static final Pattern CREATE_TABLE__FULLTEXT_SPATIAL__PATTER = Pattern.compile(
        "((?<fulltext>" + FULLTEXT + ")|(?<spatial>" + SPATIAL + "))" +
        "(\\s+((?<index>" + INDEX + ")|(?<key>" + KEY + ")))?" + "(?<indexName>\\s+`?\\w+`?\\s*)?" +
        "\\s*(?<columns>\\(.*?\\))(?<option>\\s+.*)?", Pattern.CASE_INSENSITIVE);
    
    // [CONSTRAINT [symbol]] FOREIGN KEY [index_name] (index_col_name,...) reference_definition
    protected static final Pattern CREATE_TABLE__FOREIGN_KEY__PATTER = Pattern.compile(
        "(?<constraint>" + CONSTRAINT + "\\s+(?<constraintName>`?\\w+`?\\s+)?)?" +
        FOREIGN + "\\s+" + KEY + "(?<indexName>\\s+`?\\w+`?\\s*)?" +
        "\\s*(?<columns>\\(.*?\\))(?<option>\\s+.*)?", Pattern.CASE_INSENSITIVE);
    
    // CHECK (expr)
    protected static final Pattern CREATE_TABLE__CHECK__PATTER = Pattern.compile(
        CHECK + "\\s*(?<expr>\\(.*?\\))", Pattern.CASE_INSENSITIVE);
    
    protected static final Pattern INSERT__BULK_VALUES__PATTER = Pattern.compile(
        "\\(.*\\)(,\\(.*\\))+", Pattern.CASE_INSENSITIVE);
    
    
    private static final Pattern FUNCTION__PATTERN =  Pattern.compile("(!\\d+)", Pattern.CASE_INSENSITIVE);
    
    
    
    // </editor-fold>
    
    private QueryFormat() {}
            
    
    
    // <editor-fold desc=" Parse methods ">
    /**
     *  Parse query as SELECT clauses
     * 
     * @param query source query for parsing
     * 
     * @return SELECT clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.SelectCommand
     */
    public static SelectCommand parseSelectQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final SelectCommand selectCommand = new SelectCommand(query, null);
            helper.parseSelect(selectCommand, selectCommand, helper.parseSubSelectQueries(selectCommand, query, 0));
            
            return selectCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as UPDATE clauses
     * 
     * @param query source query for parsing
     * 
     * @return UPDATE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.SelectCommand
     */
    public static UpdateCommand parseUpdateQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            
            query = query.trim().toLowerCase();
            
            final UpdateCommand updateCommand = new UpdateCommand(query);
            helper.parseUpdate(updateCommand, helper.parseSubSelectQueries(updateCommand, query, 0));
            
            return updateCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as DELETE clauses
     * 
     * @param query source query for parsing
     * 
     * @return DELETE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.DeleteCommand
     */
    public static DeleteCommand parseDeleteQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            
            query = query.trim().toLowerCase();
            
            final DeleteCommand deleteCommand = new DeleteCommand(query);
            helper.parseDelete(deleteCommand, helper.parseSubSelectQueries(deleteCommand, query, 0));
            
            return deleteCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as INSERT clauses
     * 
     * @param query source query for parsing
     * 
     * @return INSERT clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.InsertCommand
     */
    public static InsertCommand parseInsertQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final InsertCommand insertCommand = new InsertCommand(query);
            helper.parseInsert(insertCommand, helper.parseSubSelectQueries(insertCommand, query, 0));
            
            return insertCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as REPLACE clauses
     * 
     * @param query source query for parsing
     * 
     * @return REPLACE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.ReplaceCommand
     */
    public static ReplaceCommand parseReplaceQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final ReplaceCommand replaceCommand = new ReplaceCommand(query);
            helper.parseReplace(replaceCommand, helper.parseSubSelectQueries(replaceCommand, query, 0));
            
            return replaceCommand;
        }
        
        return null;
    }
    
    
    /**
     *  Parse query as ALTER TABLE clauses
     * 
     * @param query source query for parsing
     * 
     * @return ALTER clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.AlterTableCommand
     */
    public static AlterTableCommand parseAlterTableQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final AlterTableCommand alterCommand = new AlterTableCommand(query);
            helper.parseAlterTable(alterCommand, helper.parseSubSelectQueries(alterCommand, query, 0));
            
            return alterCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as START clauses
     * 
     * @param query source query for parsing
     * 
     * @return START clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.StartCommand
     */
    public static StartCommand parseStartQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final StartCommand startCommand = new StartCommand(query);
            helper.parseStart(startCommand, helper.parseSubSelectQueries(startCommand, query, 0));
            
            return startCommand;
        }
        
        return null;
    }
    
    
    /**
     *  Parse query as ALTER clauses
     * 
     * @param query source query for parsing
     * 
     * @return ALTER clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.AlterCommand
     */
    public static AlterCommand parseAlterQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final AlterCommand alterCommand = new AlterCommand(query);
            helper.parseAlter(alterCommand, helper.parseSubSelectQueries(alterCommand, query, 0));
            
            return alterCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as ALTER TABLESPACE clauses
     * 
     * @param query source query for parsing
     * 
     * @return ALTER TABLESPACE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.AlterTableSpaceCommand
     */
    public static AlterTableSpaceCommand parseAlterTableSpaceQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final AlterTableSpaceCommand alterCommand = new AlterTableSpaceCommand(query);
            helper.parseAlterTableSpace(alterCommand, helper.parseSubSelectQueries(alterCommand, query, 0, false));
            
            return alterCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as RENAME clauses
     * 
     * @param query source query for parsing
     * 
     * @return RENAME clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.RenameCommand
     */
    public static RenameCommand parseRenameQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final RenameCommand renameCommand = new RenameCommand(query);
            helper.parseRename(renameCommand, helper.parseSubSelectQueries(renameCommand, query, 0));
            
            return renameCommand;
        }
        
        return null;
    }
    
    
     /**
     *  Parse query as DROP clauses
     * 
     * @param query source query for parsing
     * 
     * @return DROP clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.DropCommand
     */
    public static DropCommand parseDropQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final DropCommand dropCommand = new DropCommand(query);
            helper.parseDrop(dropCommand, helper.parseSubSelectQueries(dropCommand, query, 0));
            
            return dropCommand;
        }
        
        return null;
    }
    
    
    /**
     *  Parse query as TRUNCATE clauses
     * 
     * @param query source query for parsing
     * 
     * @return TRUNCATE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.TruncateCommand
     */
    public static TruncateCommand parseTruncatepQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final TruncateCommand truncateCommand = new TruncateCommand(query);
            helper.parseTruncate(truncateCommand, helper.parseSubSelectQueries(truncateCommand, query, 0));
            
            return truncateCommand;
        }
        
        return null;
    }
    
    
    /**
     *  Parse query as CREATE TABLE clauses
     * 
     * @param query source query for parsing
     * 
     * @return CREATE TABLE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.CreateTableCommand
     */
    public static CreateTableCommand parseCreateTableQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final CreateTableCommand createTableCommand = new CreateTableCommand(query);
            helper.parseCreateTable(createTableCommand, helper.parseSubSelectQueries(createTableCommand, query, 0));
            
            return createTableCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as CREATE DATABASE clauses
     * 
     * @param query source query for parsing
     * 
     * @return CREATE DATABASE clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.CreateDatabaseCommand
     */
    public static CreateDatabaseCommand parseCreateDatabaseQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final CreateDatabaseCommand createDatabaseCommand = new CreateDatabaseCommand(query);
            helper.parseCreateDatabase(createDatabaseCommand, helper.parseSubSelectQueries(createDatabaseCommand, query, 0));
            
            return createDatabaseCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as SHOW clauses
     * 
     * @param query source query for parsing
     * 
     * @return SHOW clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.ShowCommand
     */
    public static ShowCommand parseShowQuery(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final ShowCommand showCommand = new ShowCommand(query);
            helper.parseShow(showCommand, helper.parseSubSelectQueries(showCommand, query, 0));
            
            return showCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as SHOW clauses
     * 
     * @param query source query for parsing
     * 
     * @return SHOW clause
     * 
     * @see np.com.ngopal.smart.sql.query.commands.ShowCommand
     */
    public static StartCommand parseStart(String query)
    {
        if (query != null)
        {
            final QueryFormat helper = new QueryFormat();
            query = query.trim().toLowerCase();
            
            final StartCommand startCommand = new StartCommand(query);
            helper.parseStart(startCommand, helper.parseSubSelectQueries(startCommand, query, 0));
            
            return startCommand;
        }
        
        return null;
    }
    
    /**
     *  Parse query as clauses
     * 
     * @param query source query for parsing
     * 
     * @return query clause, can be null
     * 
     * @see np.com.ngopal.smart.sql.query.commands.AlterCommand
     * @see np.com.ngopal.smart.sql.query.commands.AlterTableCommand
     * @see np.com.ngopal.smart.sql.query.commands.AlterTableSpaceCommand
     * @see np.com.ngopal.smart.sql.query.commands.CreateTableCommand
     * @see np.com.ngopal.smart.sql.query.commands.DeleteCommand
     * @see np.com.ngopal.smart.sql.query.commands.DropCommand
     * @see np.com.ngopal.smart.sql.query.commands.InsertCommand
     * @see np.com.ngopal.smart.sql.query.commands.RenameCommand
     * @see np.com.ngopal.smart.sql.query.commands.ReplaceCommand
     * @see np.com.ngopal.smart.sql.query.commands.SelectCommand
     * @see np.com.ngopal.smart.sql.query.commands.ShowCommand
     * @see np.com.ngopal.smart.sql.query.commands.TruncateCommand
     * @see np.com.ngopal.smart.sql.query.commands.UpdateCommand
     */
    public static QueryClause parseQuery(String query)
    {
        if (query != null)
        {
            query = removeComments(query);
            query = query.trim();
            
            if (!query.isEmpty()) {
                
                try {
                    final QueryFormat helper = new QueryFormat();

                    if (query.endsWith(";")) {
                        query = query.substring(0, query.length() - 1).trim();
                    }

                    if (query.toLowerCase().startsWith("explain")) {
                        query = query.substring(7).trim();

                        if (query.toLowerCase().startsWith("extended")) {
                            query = query.substring(8).trim();

                        } else if (query.toLowerCase().startsWith("partitions")) {
                            query = query.substring(10).trim();
                        }                     
                    }

                    AbstractCommand queryCommand = null;
                    if (SELECT_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new SelectCommand(query, null);
                        String subQuery = helper.parseSubSelectQueries(queryCommand, query, 0);
                        // check maybe union is subquery
                        Matcher subUnionMatcher = SUB_UNION_PATTER.matcher(subQuery);
                        int subUnion = 0;
                        while (subUnionMatcher.find()) 
                        {
                            String q = subUnionMatcher.group();
                            int start = subUnionMatcher.start();
                            int end = subUnionMatcher.end();

                            String key = "?" + subUnion;
                            SelectCommand qc = new SelectCommand(q, null);
                            helper.parseSelect((SelectCommand) queryCommand, qc, q);
                            queryCommand.putSubUnion(key, qc);
                            subQuery = subQuery.substring(0, start) + key + subQuery.substring(end);
                            subUnion++;
                        }

                        helper.parseSelect((SelectCommand) queryCommand, (SelectCommand) queryCommand, subQuery);
                    }
                    else if (UPDATE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new UpdateCommand(query);
                        helper.parseUpdate((UpdateCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                    }
                    else if (DELETE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new DeleteCommand(query);
                        helper.parseDelete((DeleteCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                    }
                    else if (INSERT_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new InsertCommand(query);
                        helper.parseInsert((InsertCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                    }
                    else if (REPLACE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new ReplaceCommand(query);
                        helper.parseReplace((ReplaceCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                    }
                    else if (ALTER_QUERY__PATTERN.matcher(query).find())
                    {
                        Matcher matcher = ALTER_TABLE__PATTERN.matcher(query);
                        if (matcher.matches())
                        { 
                            queryCommand = new AlterTableCommand(query);
                            helper.parseAlterTable((AlterTableCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                        } else {
                            matcher = ALTER_TABLESPACE__PATTERN.matcher(query);
                            if (matcher.matches())
                            {
                                queryCommand = new AlterTableSpaceCommand(query);
                                helper.parseAlterTableSpace((AlterTableSpaceCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                            } else {
                                queryCommand = new AlterCommand(query);
                                helper.parseAlter((AlterCommand) queryCommand, helper.parseSubSelectQueries(queryCommand, query, 0));
                            }
                        }                
                    }
                    else if (RENAME_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new RenameCommand(query);
                        helper.parseRename((RenameCommand) queryCommand, query);
                    }
                    else if (DROP_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new DropCommand(query);
                        helper.parseDrop((DropCommand) queryCommand, query);
                    }
                    else if (TRUNCATE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new TruncateCommand(query);
                        helper.parseTruncate((TruncateCommand) queryCommand, query);
                    }
                    else if (START_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new StartCommand(query);
                        helper.parseStart((StartCommand) queryCommand, query);
                    }
                    else if (CREATE_QUERY__PATTERN.matcher(query).find())
                    {
                        if (CREATE_TABLE_QUERY__PATTERN.matcher(query).find()) {
                            queryCommand = new CreateTableCommand(query);
                            helper.parseCreateTable((CreateTableCommand) queryCommand, query);

                        } else if (CREATE_USER_QUERY__PATTERN.matcher(query).find()) {
                            queryCommand = new CreateUserCommand(query);
                            helper.parseCreateUser((CreateUserCommand) queryCommand, query);

                        } else if (CREATE_DATABASE_QUERY__PATTERN.matcher(query).find()) {
                            queryCommand = new CreateDatabaseCommand(query);
                            helper.parseCreateDatabase((CreateDatabaseCommand) queryCommand, query);

                        } else if (CREATE_VIEW_QUERY__PATTERN.matcher(query).find()) {
                            queryCommand = new CreateViewCommand(query);

                        } else {
                            if (Pattern.compile(TABLE, Pattern.CASE_INSENSITIVE).matcher(query).find()) {
                                queryCommand = new CreateTableCommand(query);
                            }
                        }
                    }
                    else if (SHOW__PATTERN.matcher(query).find())
                    {
                        queryCommand = new ShowCommand(query);
                        helper.parseShow((ShowCommand) queryCommand, query);
                    }
                    else if (SET_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new SetCommand(query);
                        helper.parseSet((SetCommand) queryCommand, query);
                    }
                    else if (RETURN_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new ReturnCommand(query);
                        helper.parseReturn((ReturnCommand) queryCommand, query);
                    }
                    else if (DECLARE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new DeclareCommand(query);
                        helper.parseDeclare((DeclareCommand) queryCommand, query);
                    }
                    else if (GRANT_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new GrantCommand(query);
                        helper.parseGrant((GrantCommand) queryCommand, query);
                    }
                    else if (FLUSH_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new FlushCommand(query);
                        helper.parseFlush((FlushCommand) queryCommand, query);
                    }
                    else if (REVOKE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new RevokeCommand(query);
                        helper.parseRevoke((RevokeCommand) queryCommand, query);
                    }
                    else if (CALL_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new CallCommand(query);
                        helper.parseCall((CallCommand) queryCommand, query);
                    }
                    else if (CHECK_QUERY__PATTERN.matcher(query).find())
                    {
                        if (CHECK_TABLE_QUERY__PATTERN.matcher(query).find()) {
                            queryCommand = new CheckTableCommand(query);
                            helper.parseCheckTable((CheckTableCommand) queryCommand, query);
                        }
                    }
                    else if (LOCK_TABLE_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new LockTableCommand(query);
                        helper.parseLockTable((LockTableCommand) queryCommand, query);
                    }
                    else if (LOAD_DATA_QUERY__PATTERN.matcher(query).find())
                    {
                        queryCommand = new LoadDataCommand(query);
                        helper.parseLoadData((LoadDataCommand) queryCommand, query);
                    }

                    return queryCommand;
                } catch (Throwable th) {
                    log.error(th.getMessage(), th);
                    th.printStackTrace();
                }
            }
        }
        
        return null;
    }
    
    /**
     *  Return map of table names like keys and
     * queries like values. Each table name can have and schema part,
     * for example key can be `example`.`table`. So if you need get 
     * scheme - you must parse keys
     * 
     * @param query source query
     * @return map of table name as key and queries as values
     */
    public static Map<String, String> getTableNamesFromQuery(String query) {
        Map<String, String> tableNames = new HashMap<>();
        
        QueryClause clause = parseQuery(query);
        if (clause instanceof InsertCommand) {
            InsertCommand ic = (InsertCommand)clause;
            if (ic.getIntoTable() != null) {
                tableNames.put(ic.getIntoTable().getName(), ic.getName());
            }
        } else if (clause instanceof ReplaceCommand) {
            ReplaceCommand rc = (ReplaceCommand)clause;
            if (rc.getIntoTable() != null) {
                tableNames.put(rc.getIntoTable().getName(), rc.getName());
            }
        } else if (clause instanceof AlterTableCommand) {
            AlterTableCommand ac = (AlterTableCommand)clause;
            if (ac.getTableName() != null) {
                tableNames.put(ac.getTableName(), ac.getSource());
            }
        } else if (clause instanceof DeleteCommand) {
            DeleteCommand dc = (DeleteCommand)clause;
            if (dc.getTables() != null) {
                for (TableReferenceClause t: dc.getTables()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getName(), dc.getName());
                    }
                }
            } else if (dc.getFromClause() != null && dc.getFromClause().getTables() != null) {
                for (TableReferenceClause t: dc.getFromClause().getTables()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getName(), dc.getName());
                    }
                }
            }
        } else if (clause instanceof UpdateCommand) {
            UpdateCommand uc = (UpdateCommand)clause;
            if (uc.getTableClauses() != null) {
                for (TableReferenceClause t: uc.getTableClauses()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getName(), uc.getName());
                    }
                }
            }
        } else if (clause instanceof SelectCommand) {
            SelectCommand sc = (SelectCommand)clause;
            if (sc.getFromClause() != null && sc.getFromClause().getTables() != null) {
                for (TableReferenceClause t: sc.getFromClause().getTables()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getName(), sc.getName());
                    }
                    
                    if (t.getJoinClauses() != null) {
                        for (JoinClause join: t.getJoinClauses()) {
                            if (join.getTable() != null && !(join.getTable() instanceof SelectCommand)) {
                                tableNames.put(join.getTable().getName(), sc.getName());
                            }
                        }
                    }
                }
            }
        }
        
        if (clause instanceof AbstractCommand) {
            AbstractCommand ac = (AbstractCommand)clause;
            for (String q: ac.getQuerySourcesValues()) {
                if (q.startsWith("(") && q.endsWith(")")) {
                    q = q.substring(1, q.length() - 1);
                }
                tableNames.putAll(getTableNamesFromQuery(q));
            }
        }
        
        return tableNames;
    }
    
    public static Map<String, String> getTableNamesAsAliases(String query) {
        Map<String, String> tableNames = new HashMap<>();
        
        QueryClause clause = parseQuery(query);
        if (clause instanceof InsertCommand) {
            InsertCommand ic = (InsertCommand)clause;
            if (ic.getIntoTable() != null) {
                tableNames.put(ic.getIntoTable().getName(), ic.getIntoTable().getName());
            }
        } else if (clause instanceof ReplaceCommand) {
            ReplaceCommand rc = (ReplaceCommand)clause;
            if (rc.getIntoTable() != null) {
                tableNames.put(rc.getIntoTable().getName(), rc.getIntoTable().getName());
            }
        } else if (clause instanceof AlterTableCommand) {
            AlterTableCommand ac = (AlterTableCommand)clause;
            if (ac.getTableName() != null) {
                tableNames.put(ac.getTableName(), ac.getTableName());
            }
        } else if (clause instanceof DeleteCommand) {
            DeleteCommand dc = (DeleteCommand)clause;
            if (dc.getTables() != null) {
                for (TableReferenceClause t: dc.getTables()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getAlias(), t.getName());
                    }
                }
            } else if (dc.getFromClause() != null && dc.getFromClause().getTables() != null) {
                for (TableReferenceClause t: dc.getFromClause().getTables()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getAlias() != null ? t.getAlias() : t.getName(), t.getName());
                    }
                }
            }
        } else if (clause instanceof UpdateCommand) {
            UpdateCommand uc = (UpdateCommand)clause;
            if (uc.getTableClauses() != null) {
                for (TableReferenceClause t: uc.getTableClauses()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getAlias() != null ? t.getAlias() : t.getName(), t.getName());
                    }
                }
            }
        } else if (clause instanceof SelectCommand) {
            SelectCommand sc = (SelectCommand)clause;
            if (sc.getFromClause() != null && sc.getFromClause().getTables() != null) {
                for (TableReferenceClause t: sc.getFromClause().getTables()) {
                    if (!(t instanceof SelectCommand)) {
                        tableNames.put(t.getAlias() != null ? t.getAlias() : t.getName(), t.getName());
                    }
                    
                    if (t.getJoinClauses() != null) {
                        for (JoinClause join: t.getJoinClauses()) {
                            if (join.getTable() != null && !(join.getTable() instanceof SelectCommand)) {
                                tableNames.put(join.getTable().getAlias() != null ? join.getTable().getAlias() : join.getTable().getName(), join.getTable().getName());
                            }
                        }
                    }
                }
            }
        }
        
        if (clause instanceof AbstractCommand) {
            AbstractCommand ac = (AbstractCommand)clause;
            for (String q: ac.getQuerySourcesValues()) {
                if (q.startsWith("(") && q.endsWith(")")) {
                    q = q.substring(1, q.length() - 1);
                }
                tableNames.putAll(getTableNamesFromQuery(q));
            }
        }
        
        return tableNames;
    }
    
    /**
     *  Convert query to explain query
     * @param explainPrefix can be EXPLAIN, EXPLAIN EXTENDED
     * @param query query for converting (if insert will be NULL)
     * @return converted to explain query
     */
    public static String convertToExplainQuery(String explainPrefix, String query) {
        QueryClause queryClause = parseQuery(query);
        
        // if select just add explain text
        if (queryClause instanceof SelectCommand) {
            query = ((SelectCommand)queryClause).getName();
            if (((SelectCommand) queryClause).getIntoSelect()!= null) {
                int i = query.indexOf(((SelectCommand) queryClause).getIntoSelect());
                
                String s0 = query.substring(0, i).trim();
                // substring into = 4
                s0 = s0.substring(0, s0.length() - 4);
                
                query = s0 + query.substring(i + ((SelectCommand) queryClause).getIntoSelect().length());
            }
            return explainPrefix + " " + query;
            
        } else if (queryClause instanceof UpdateCommand) {
            // if update or delete - need get only 'table' and 'where' part and make select query                
            UpdateCommand updateCommand = (UpdateCommand) queryClause;
            query = updateCommand.getName();
            
            String tableText = "";
            if (updateCommand.getTableClauses() != null) {
                for (TableReferenceClause t: updateCommand.getTableClauses()) {
                    if (!tableText.isEmpty()) {
                        tableText += ", ";
                    }
                
                    tableText += t.getName() + (t.getAlias() != null ? " " + t.getAlias() : "");
                }
            }
            int index = query.toLowerCase().indexOf(" where");
            String whereText = index > -1 ? query.substring(index + 6) : "";//updateCommand.getWhereClause() != null ? updateCommand.getWhereClause().getCondition() : "";

            return explainPrefix + " SELECT * FROM " + tableText + (whereText.isEmpty() ? "" : " WHERE" + whereText);
            
        } else if (queryClause instanceof DeleteCommand) {
            // if update or delete - need get only 'table' and 'where' part and make select query                
            DeleteCommand deleteCommand = (DeleteCommand) queryClause;
            
            query = deleteCommand.getName();

            FromClause fromClause = deleteCommand.getFromClause();
            List<TableReferenceClause> tables = fromClause != null ? fromClause.getTables() : null;
            String tableText = "";
            if (tables != null) {
                for (TableReferenceClause t: tables) {
                    if (!tableText.isEmpty()) {
                        tableText += ", ";
                    }
                
                    tableText += t.getName() + (t.getAlias() != null ? " " + t.getAlias() : "");
                }
            }
            
            int index = query.toLowerCase().indexOf(" where");
            String whereText = index > -1 ? query.substring(index + 6) : "";//deleteCommand.getWhereClause() != null ? deleteCommand.getWhereClause().getCondition() : "";

            return explainPrefix + " SELECT * FROM " + tableText + (whereText.isEmpty() ? "" : " WHERE" + whereText);
        }
        
        return null;
    }
    
    public static ConditionResult parseConditionFilters(String query) {
        
        ConditionResult result = new ConditionResult();
        parseConditionFilters(null, query.trim(), result);
        
        return result;
    }
    
    public static void parseConditionFilters(String lvl, String query, ConditionResult conditionResult) {
                
        String resultQuery = parseConditions(query, conditionResult);
        Matcher matcher = CONDITION_ANDOR_PATTER.matcher(resultQuery);
        
        int end = 0;
        while (matcher.find()) {
            
            String group1 = matcher.group(1);
            // BETWEEN also have inside AND            
            if (group1 == null || !group1.toUpperCase().contains(" BETWEEN ")) {            
                String condition = group1 != null ? group1.trim() : null;
                conditionResult.addCondition(lvl, condition);
                conditionResult.addCondition(lvl, matcher.group(2));
                end = matcher.end();
            }
        }
        
        query = resultQuery.substring(end).trim();
        if (!query.isEmpty()) {
            conditionResult.addCondition(lvl, query);
        }
        
        List<String> conditions = conditionResult.getCondition(lvl);
        if (conditions != null) {
            for (int i = 0; i < conditions.size(); i += 2) {
                
                String condition = conditions.get(i);
                
                Matcher m = SUB_CONDITION_PATTER.matcher(condition);
                while (m.find()) {
                    String subCondition = conditionResult.getSubCondition(Integer.valueOf(m.group(1)));
                    parseConditionFilters(m.group(), subCondition, conditionResult);
                }
            }
        }
        
    }
    
    
    public static ColumnResult parseColumnsResult(String query) {
        
        if (query != null) {
            ColumnResult result = new ColumnResult();
            parseColumnsResult(query.trim(), result);

            return result;
        }
        
        return null;
    }
    
    public static void parseColumnsResult(String query, ColumnResult result) {
                
        String resultQuery = parseColumns(query, result);
        String[] columns = resultQuery.split(",", -1);
        
        for (String column: columns) {
            Matcher m = QUOTE_PATTERN.matcher(column);
            if (m.find()) {
                int index = Integer.valueOf(m.group(2));
                column = column.replaceAll("&" + index, result.getQuote(index).trim());
            }
            
            String res[] = column.split(SIMPLE_CONDITION__PATTERN);
            if (res.length > 1) {
                if (!res[0].trim().matches("\\d.*")) {
                    result.addColumn(res[0].trim());
                    
                } else if (!res[1].trim().matches("\\d.*")) {
                    result.addColumn(res[1].trim());
                }
            } else {
                result.addColumn(column);
            }
        }
    }
    
    // </editor-fold>
    
    
    // <editor-fold desc=" Private parsing methods ">
    
    private void parseSelect(AbstractCommand parentCommand, final SelectCommand selectCommand, String query)
    {
        // check mayby this is UnionCommand
        query = query.replaceAll("\\s+", " ");
        int unionIndex = query.toLowerCase().indexOf("union");
        if (unionIndex > -1) {
                            
            List<SelectCommand> unionSelectCommands = new ArrayList<>();
            selectCommand.setUnionSelectCommands(unionSelectCommands);
            
            Boolean prevIsAll = null;
            while (unionIndex > -1) {
                int unionAllIndex = query.toLowerCase().indexOf("union all");
                
                String selectPart = query.substring(0, unionIndex);
                
                selectPart = selectPart.trim();
                if (selectPart.startsWith("(")) {
                    selectPart = selectPart.substring(1);
                    selectPart = selectPart.trim();
                }
                
                if (selectPart.endsWith(")")) {
                    selectPart = selectPart.substring(0, selectPart.length() - 1);
                    selectPart = selectPart.trim();
                }
                
                String q = parentCommand != null && parentCommand.getQuerySource(selectPart) != null ? parentCommand.getQuerySource(selectPart) : selectPart;
                if (q.startsWith("(")) {
                    q = q.substring(1);
                    q = q.trim();
                }
                
                if (q.endsWith(")")) {
                    q = q.substring(0, q.length() - 1);
                    q = q.trim();
                }
                
                SelectCommand sc = new SelectCommand(selectPart, selectCommand);
                parseSelect(parentCommand, sc, convertFunctions(parentCommand, q));
                sc.setUnionAll(prevIsAll != null ? prevIsAll : unionIndex == unionAllIndex);
                
                unionSelectCommands.add(sc);
                
                prevIsAll = unionIndex == unionAllIndex;
                
                query = query.substring(unionIndex + (prevIsAll ? 9 : 5));
                unionIndex = query.toLowerCase().indexOf("union");
            }
            
            if (query.endsWith(")")) {
                query = query.substring(0, query.length() - 1);
                query = query.trim();
            }
            
            query = parentCommand != null && parentCommand.getQuerySource(query) != null ? parentCommand.getQuerySource(query) : query;
            
            if (query.startsWith("(")) {
                query = query.substring(1);
                query = query.trim();
            }

            if (query.endsWith(")")) {
                query = query.substring(0, query.length() - 1);
                query = query.trim();
            }
            
            // parse last select command
            SelectCommand sc = new SelectCommand(query, selectCommand);
            parseSelect(parentCommand, sc, convertFunctions(parentCommand, query.trim()));
            sc.setUnionAll(prevIsAll);
                
            unionSelectCommands.add(sc);
            
            
        } else {        
            final Matcher matcher = SELECT__PATTERN.matcher(query);
            if (matcher.matches())
            {
                selectCommand.setSimpleSource(convertFunctions(selectCommand, query));

                String distinctType = matcher.group("distinctType");
                selectCommand.setDistinctType(distinctType != null ? distinctType.trim() : null);
                selectCommand.setHighPriority(matcher.group("highPriority") != null);

                String maxStatTime = matcher.group("maxStatTime");
                selectCommand.setMaxStatTime(maxStatTime != null ? maxStatTime.trim() : null);

                selectCommand.setStraightJoin(matcher.group("straightJoin") != null);
                selectCommand.setSqlSmallResult(matcher.group("sqlSmallResult") != null);
                selectCommand.setSqlBigResult(matcher.group("sqlBigResult") != null);
                selectCommand.setSqlBufferResult(matcher.group("sqlBufferResult") != null);

                String sqlType = matcher.group("sqlType");
                selectCommand.setSqlType(sqlType != null ? sqlType.trim() : null);

                String sqlCalcFoundRows = matcher.group("sqlCalcFoundRows");
                selectCommand.setSqlCalcFoundRows(sqlCalcFoundRows != null ? sqlCalcFoundRows.trim() : null);
                        
                String columns = matcher.group("columnsSection");
                selectCommand.setColumnSource(columns);
                selectCommand.setColumns(parseColumns(selectCommand, columns));
                String intoSelectSection = matcher.group("intoSelectSection");
                if (intoSelectSection != null && !intoSelectSection.trim().isEmpty()) {
                    selectCommand.setIntoSelect(convertFunctions(selectCommand, intoSelectSection != null ? intoSelectSection.trim() : null));
                }
                selectCommand.setFromClause(parseFrom(selectCommand, convertFunctions(selectCommand, matcher.group("fromSection"))));

                String partitionSection = matcher.group("partitionSection");
                selectCommand.setPartition(partitionSection != null ? partitionSection.trim() : null);

                selectCommand.setWhereClause(parseWhere(selectCommand, convertFunctions(selectCommand, matcher.group("whereSection"))));
                selectCommand.setGroupByClause(parseGroupBy(selectCommand, convertFunctions(selectCommand, matcher.group("groupBySection")), matcher.group("withRollup") != null));
                selectCommand.setHavingClause(parseHaving(selectCommand, convertFunctions(selectCommand, matcher.group("havingSection"))));
                selectCommand.setOrderByClause(parseOrderBy(selectCommand, convertFunctions(selectCommand, matcher.group("orderBySection"))));
                selectCommand.setLimitClause(parseLimit(matcher.group("limitSection")));

                String procedureSection = matcher.group("procedureSection");
                selectCommand.setProcedure(convertFunctions(selectCommand, procedureSection != null ? procedureSection.trim() : null));

                String intoSection = matcher.group("intoSection");
                selectCommand.setInto(convertFunctions(selectCommand, intoSection != null ? intoSection.trim() : null));

                selectCommand.setForUpdate(matcher.group("forUpdate") != null);
                selectCommand.setLockIn(matcher.group("lockIn") != null);
            }
        }
    }
    
    private String convertFunctions(AbstractCommand command, String text) {
        if (text != null) {
            Matcher m = FUNCTION__PATTERN.matcher(text);
            while (m.find()) {
                String key = m.group();
                try {
                    text = text.replaceFirst(key, command.getFunction(key));
                } catch (Throwable th) {
                    log.warn(th.getMessage());
                }
            }
        }
        
        return text;
    }
    
    private void parseUpdate(final UpdateCommand updateCommand, String query)
    {        
        query = convertFunctions(updateCommand, query);
        final Matcher matcher = UPDATE__PATTERN.matcher(query);
        if (matcher.matches())
        {            
            updateCommand.setLowPriority(matcher.group("lowpriority") != null);
            updateCommand.setIgnore(matcher.group("ignore") != null);
            
            updateCommand.setTableClauses(parseTables(updateCommand, matcher.group("tableSection")));
            updateCommand.setSetClause(parseSet(updateCommand, matcher.group("setSection")));
            updateCommand.setWhereClause(parseWhere(updateCommand, matcher.group("whereSection")));
            updateCommand.setOrderByClause(parseOrderBy(updateCommand, matcher.group("orderBySection")));
            updateCommand.setLimitClause(parseLimit(matcher.group("limitSection")));
        }
    }
    
    private void parseDelete(final DeleteCommand deleteCommand, String query)
    {        
        query = convertFunctions(deleteCommand, query);
        final Matcher matcher = DELETE__PATTERN.matcher(query);
        if (matcher.matches())
        {            
            deleteCommand.setLowPriority(matcher.group("lowpriority") != null);
            deleteCommand.setQuick(matcher.group("quick") != null);
            deleteCommand.setIgnore(matcher.group("ignore") != null);
            
            String partitionSection = matcher.group("partition");
            deleteCommand.setPartition(partitionSection != null ? partitionSection.trim() : null);
                
            deleteCommand.setTables(parseTables(deleteCommand, matcher.group("tablesSection")));
            deleteCommand.setFromClause(parseFrom(deleteCommand, matcher.group("fromSection")));
            deleteCommand.setUsing(parseTables(deleteCommand, matcher.group("usingSection")));
            deleteCommand.setWhereClause(parseWhere(deleteCommand, matcher.group("whereSection")));
            deleteCommand.setOrderByClause(parseOrderBy(deleteCommand, matcher.group("orderBySection")));
            deleteCommand.setLimitClause(parseLimit(matcher.group("limitSection")));
        }
    }
    
    
    private void parseInsert(final InsertCommand insertCommand, String query)
    {
        query = convertFunctions(insertCommand, query);
        final Matcher matcher = INSERT__PATTERN.matcher(query);
        if (matcher.matches())
        {
            String priority = matcher.group("priority");
            insertCommand.setPriority(priority != null ? priority.trim() : null);
            insertCommand.setIgnore(matcher.group("ignore") != null);

            insertCommand.setValue(matcher.group("value") != null);
            insertCommand.setValues(matcher.group("values") != null);
            
            String partition = matcher.group("partition");
            insertCommand.setPartitionData(partition != null ? partition.trim() : null);
            insertCommand.setIntoTable(parseIntoTable(matcher.group("tableSection"), matcher.group("columnsSection"), matcher.group("intoClause") != null));
            insertCommand.setValuesClause(parseValues(matcher.group("valuesSection")));
            insertCommand.setSetClause(parseSet(insertCommand, matcher.group("setSection")));
            String selectQuery = matcher.group("selectSection");
            if (selectQuery != null) {
                insertCommand.setSelectClause(parseSelectQuery("SELECT " + selectQuery));
            }
            insertCommand.setOnDuplicateKeyUpdateClause(parseOnDuplicateKeyUpdateClause(insertCommand, matcher.group("onDuplicateKeyUpdateSection")));
        }
    }
    
    private void parseReplace(final ReplaceCommand replaceCommand,  String query)
    {
        query = convertFunctions(replaceCommand, query);
        final Matcher matcher = REPLACE__PATTERN.matcher(query);
        if (matcher.matches())
        {
            replaceCommand.setIntoTable(parseIntoTable(matcher.group("tableSection"), matcher.group("columnsSection"), matcher.group("intoClause") != null));
            replaceCommand.setValuesClause(parseValues(matcher.group("valuesSection")));
        }
    }
    
    private void parseAlterTable(final AlterTableCommand alterCommand, String query)
    {        
        query = convertFunctions(alterCommand, query);
        final Matcher matcher = ALTER_TABLE__PATTERN.matcher(query);
        if (matcher.matches())
        {            
            alterCommand.setIgnore(matcher.group("ignore") != null);
            String tableName = matcher.group("tableName");
            alterCommand.setTableName(tableName != null ? tableName.trim() : null);
            alterCommand.setAlterSpecifications(parseAlterTableSpecifications(alterCommand, matcher.group("definition")));
        }
    }
    
    private void parseStart(final StartCommand startCommand, String query)
    {        
        query = convertFunctions(startCommand, query);
        final Matcher matcher = START__PATTERN.matcher(query);
        if (matcher.matches())
        {            
            startCommand.setTransaction(matcher.group("transaction") != null);
            startCommand.setSlave(matcher.group("slave") != null);
        }
    }
    
    private void parseAlterTableSpace(final AlterTableSpaceCommand alterCommand, String query)
    {        
        query = convertFunctions(alterCommand, query);
        final Matcher matcher = ALTER_TABLESPACE__PATTERN.matcher(query);
        if (matcher.matches())
        {            
            alterCommand.setAdd(matcher.group("add") != null);
            alterCommand.setDrop(matcher.group("drop") != null);
            
            String tableSpaceName = matcher.group("tableSpaceName");
            alterCommand.setTableSpaceName(tableSpaceName != null ? tableSpaceName.trim() : null);
            
            String fileName = matcher.group("fileName");
            alterCommand.setFileName(fileName != null ? fileName.trim() : null);
            
            String definition = matcher.group("definition");
            alterCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseAlter(final AlterCommand alterCommand, String query)
    {        
        query = convertFunctions(alterCommand, query);
        final Matcher matcher = ALTER__PATTERN.matcher(query);
        if (matcher.matches())
        {            
            String definition = matcher.group("definition");
            alterCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseAlterTableAdd(AlterTableSpecification als, String[] result) {
        if (result.length > 0) {
            String addType = result[0].toUpperCase();
            
            AlterTableKey atk = new AlterTableKey();
            als.setAlterTableKey(atk);
            
            switch (addType) {
                case "INDEX":
                case "KEY":                    
                    atk.setIndex(String.join(" ", Arrays.copyOfRange(result, 1, result.length)));
                    break;
                    
                case "FULLTEXT":
                case "SPATIAL":
                    if (result.length > 1) {
                        switch (result[1].toUpperCase()) {
                            case "INDEX":
                            case "KEY":
                                atk.setIndex(String.join(" ", Arrays.copyOfRange(result, 2, result.length)));
                                break;
                            default:
                                atk.setIndex(String.join(" ", Arrays.copyOfRange(result, 1, result.length)));
                        }
                    }
                    break;
                    
                case "CHECK":
                    atk.setCheckExpresion(String.join(" ", Arrays.copyOfRange(result, 1, result.length)));
                    break;
                    
                default:
                    // COLUMN or CONSTRAINT or PRIMARY or UNIQUE or FOREIGN 
                    if (result.length > 0) {
                        
                        switch (result[0].toUpperCase()) {
                            case "COLUMN":
                                String definitions = String.join(" ", Arrays.copyOfRange(result, 1, result.length));                                
                                atk.setColumn(definitions);
                                break;
                                
                            case "CONSTRAINT" :
                                // [CONSTRAINT [symbol]] PRIMARY KEY ...
                                atk = new AlterTableKey();
                                if (result.length > 3) {                                    
                                    atk.setConstraint(result[1]);
                                    
                                    String s = result[2].toUpperCase();
                                    switch (s) {
                                        case "PRIMARY":
                                            if ("KEY".equals(result[3].toUpperCase())) {
                                                atk.setPrimaryKey(true);
                                                
                                                if (result.length > 4) {
                                                    atk.setKey(String.join(" ", Arrays.copyOfRange(result, 4, result.length)));
                                                }
                                            }
                                            break;

                                        case "UNIQUE":
                                            if ("KEY".equals(result[3].toUpperCase())) {
                                                atk.setUniqueKey(true);
                                                
                                                if (result.length > 4) {
                                                    atk.setKey(String.join(" ", Arrays.copyOfRange(result, 4, result.length)));
                                                }
                                            }
                                            break;

                                        case "FOREIGN":
                                            if ("KEY".equals(result[3].toUpperCase())) {
                                                atk.setForeignKey(true);
                                                
                                                if (result.length > 4) {
                                                    atk.setKey(String.join(" ", Arrays.copyOfRange(result, 4, result.length)));
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;
                                
                            case "PRIMARY":
                                if (result.length > 1) {  
                                    atk = new AlterTableKey();
                                    if ("KEY".equals(result[1].toUpperCase())) {
                                        atk.setPrimaryKey(true);

                                        if (result.length > 2) {
                                            atk.setKey(String.join(" ", Arrays.copyOfRange(result, 2, result.length)));
                                        }
                                    }
                                }
                                break;
                                
                            case "UNIQUE":
                                if (result.length > 1) {  
                                    atk = new AlterTableKey();
                                    if ("KEY".equals(result[1].toUpperCase())) {
                                        atk.setUniqueKey(true);

                                        if (result.length > 2) {
                                            atk.setKey(String.join(" ", Arrays.copyOfRange(result, 2, result.length)));
                                        }
                                    }
                                }
                                break;
                                
                            case "FOREIGN":
                                if (result.length > 1) {  
                                    atk = new AlterTableKey();
                                    if ("KEY".equals(result[1].toUpperCase())) {
                                        atk.setForeignKey(true);

                                        if (result.length > 2) {
                                            atk.setKey(String.join(" ", Arrays.copyOfRange(result, 2, result.length)));
                                        }
                                    }
                                }
                                break;
                        }
                    }
            }
        }
        
    }
    
    private List<AlterTableSpecification> parseAlterTableSpecifications(AlterTableCommand alterCommand, String definitions) {
        
        if (definitions != null) {
            definitions = definitions.replaceAll("\\s+", " ");
            
            ColumnResult cr = parseColumnsResult(definitions);
            if (cr != null) {
                List<AlterTableSpecification> list = new ArrayList<>();
                // need check for ORDER BY, because column will be joined with ','
                String groupBy = "";
                boolean hasGroupBy = false;
                List<String> listColumns = new ArrayList<>();
                for (String column: cr.getColumns()) {
                    if (column.trim().toUpperCase().startsWith("GROUP BY")) {
                        hasGroupBy = true;
                    }

                    if (hasGroupBy) {
                        if (!groupBy.isEmpty()) {
                            groupBy += ", ";
                        }
                        groupBy += column.trim();
                    } else {
                        listColumns.add(column.trim());
                    }
                }

                if (hasGroupBy) {
                    listColumns.add(groupBy);
                }

                for (String column: listColumns) {

                    Matcher m = SUBQUERY0_PATTERN.matcher(column);
                    while (m.find()) {
                        int index = Integer.valueOf(m.group(2));
                        column = column.replaceAll("\\^" + index, "(" + cr.getSubQuery(index).trim() + ")");
                    }

                    m = FUNCTION_PATTERN.matcher(column);
                    while (m.find()) {
                        int index = Integer.valueOf(m.group(2));
                        column = column.replaceAll("\\$" + index, cr.getFunction(index).trim());
                    }

                    AlterTableSpecification als = new AlterTableSpecification();
                    als.setSpecification(column);
                    String[] result = column.split(" ");
            
                    String type = result[0].toUpperCase();
                    als.setType(type);
                    switch (type) {
                        case "ADD":
                            parseAlterTableAdd(als, Arrays.copyOfRange(result, 1, result.length));
                            break;

                        case "ALGORITHM":
                            break;

                        case "ALTER":
                            break;

                        case "CHANGE":
                            break;

                        case "DEFAULT":
                        case "CHARACTER":
                            break;

                        case "CONVERT":
                            break;

                        case "DISABLE":
                        case "ENABLE":
                            break;

                        case "DISCARD":
                        case "IMPORT":
                            break;

                        case "DROP":
                            break;

                        case "FORCE":
                            break;

                        case "LOCK":
                            break;

                        case "MODIFY":
                            break;

                        case "ORDER":
                            break;

                        case "RENAME":
                            break;

                        case "WITHOUT":
                        case "WITH":
                            break;

                        case "TRUNCATE":
                            break;

                        case "COALESCE":
                            break;

                        case "REORGANIZE":
                            break;

                        case "EXCHANGE":
                            break;

                        case "ANALYZE":
                            break;

                        case "CHECK":
                            break;

                        case "OPTIMIZE":
                            break;

                        case "REBUILD":
                            break;

                        case "REPAIR":
                            break;

                        case "REMOVE":
                            break;

                        case "UPGRADE":
                            break;
                    }
                    list.add(als);
                }
                
                return list;
            }
        }
        return null;
    }
    
    private void parseRename(final RenameCommand renameCommand, String query)
    {        
        query = convertFunctions(renameCommand, query);
        final Matcher matcher = RENAME__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String tableSection = matcher.group("tableSection");
            if (tableSection != null && !tableSection.trim().isEmpty()) {
                String[] tables = tableSection.split(",");
                
                List<RenameTableClause> renameTableClauses = new ArrayList<>();
                for (String table: tables) {                    
                    
                    final Matcher tableMatcher = RENAME_TABLE__PATTERN.matcher(table);
                    RenameTableClause renameTable = new RenameTableClause(table);
                    if (tableMatcher.matches()) {
                        
                        String oldTableSection = tableMatcher.group("oldTableSection");
                        String newTableSection = tableMatcher.group("newTableSection");
                        
                        renameTable.setOldTable(oldTableSection != null ? oldTableSection.trim() : null);
                        renameTable.setNewTable(newTableSection != null ? newTableSection.trim() : null);
                    }

                    renameTableClauses.add(renameTable);
                }
                
                renameCommand.setRenameTables(renameTableClauses);
            }            
        }
    }
    
    private void parseDrop(final DropCommand dropCommand, String query)
    {        
        query = convertFunctions(dropCommand, query);
        final Matcher matcher = DROP__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String dropTarget = matcher.group("dropTargetSection");
            dropCommand.setDropTarget(dropTarget != null ? dropTarget.trim() : null);
            
            String ifExists = matcher.group("ifExistsSection");
            dropCommand.setIfExists(ifExists != null && !ifExists.trim().isEmpty());
            
            String tableSection = matcher.group("tableSection");
            dropCommand.setTableSection(tableSection != null ? tableSection.trim() : null);
        }
    }
    
    private void parseTruncate(final TruncateCommand truncateCommand, String query)
    {        
        query = convertFunctions(truncateCommand, query);
        final Matcher matcher = TRUNCATE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String hasTableSection = matcher.group("hasTableSection");
            truncateCommand.setHasTable(hasTableSection != null);
                        
            String tableSection = matcher.group("tableSection");
            truncateCommand.setTableSection(tableSection != null ? tableSection.trim() : null);
        }
    }
    
    private void parseShow(final ShowCommand showCommand, String query)
    {        
        query = convertFunctions(showCommand, query);
        final Matcher matcher = SHOW__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            showCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseSet(final SetCommand setCommand, String query)
    {
        query = convertFunctions(setCommand, query);
        final Matcher matcher = SET__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            setCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseReturn(final ReturnCommand returnCommand, String query)
    {
        query = convertFunctions(returnCommand, query);
        final Matcher matcher = RETURN__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            returnCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseDeclare(final DeclareCommand declareCommand, String query)
    {
        query = convertFunctions(declareCommand, query);
        final Matcher matcher = DECLARE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            declareCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseFlush(final FlushCommand flushCommand, String query)
    {
        query = convertFunctions(flushCommand, query);
        final Matcher matcher = FLUSH__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            flushCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseRevoke(final RevokeCommand revokeCommand, String query)
    {
        query = convertFunctions(revokeCommand, query);
        final Matcher matcher = REVOKE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            revokeCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseCall(final CallCommand callCommand, String query)
    {
        query = convertFunctions(callCommand, query);
        final Matcher matcher = CALL__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            callCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseCheckTable(final CheckTableCommand callCommand, String query)
    {
        query = convertFunctions(callCommand, query);
        final Matcher matcher = CHECK_TABLE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            callCommand.setTableName(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseLockTable(final LockTableCommand callCommand, String query)
    {
        query = convertFunctions(callCommand, query);
        final Matcher matcher = LOCK_TABLE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            callCommand.setTableName(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseLoadData(final LoadDataCommand command, String query)
    {
        query = convertFunctions(command, query);
        final Matcher matcher = LOAD_DATA__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            command.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseGrant(final GrantCommand grantCommand, String query)
    {
        query = convertFunctions(grantCommand, query);
        final Matcher matcher = GRANT__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            grantCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseCreateUser(final CreateUserCommand createUserCommand, String query)
    {
        query = convertFunctions(createUserCommand, query);
        final Matcher matcher = CREATE_USER__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            createUserCommand.setDefinition(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseCreateDatabase(final CreateDatabaseCommand createDatabaseCommand, String query)
    {
        query = convertFunctions(createDatabaseCommand, query);
        final Matcher matcher = CREATE_DATABASE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            String definition = matcher.group("definition");
            createDatabaseCommand.setDatabaseName(definition != null ? definition.trim() : null);
        }
    }
    
    private void parseCreateTable(final CreateTableCommand createTableCommand, String query)
    {        
        query = convertFunctions(createTableCommand, query);
        query = query.replaceAll("\\s+", " ");
        final Matcher matcher = CREATE_TABLE__PATTERN.matcher(query);
        if (matcher.matches())
        {   
            createTableCommand.setTemporary(matcher.group("temporarySection") != null);                        
            createTableCommand.setIfNotExists(matcher.group("ifNotExistsSection") != null);
            
            // we can have 3 cases
            String tableSection = matcher.group("tableSection");
            if (tableSection != null) {
                
                tableSection = tableSection.trim();
                //CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name
                //  { LIKE old_tbl_name | (LIKE old_tbl_name) }
                Matcher likeMatcher = CREATE_TABLE__LIKE__PATTERN.matcher(tableSection);
                if (likeMatcher.find()) {
                    String likeTableName = likeMatcher.group("likeTableName0");
                    if (likeTableName == null) {
                        likeTableName = likeMatcher.group("likeTableName1");
                    }
                    
                    createTableCommand.setLikeTableName(likeTableName != null ? likeTableName.trim() : null);                    
                    createTableCommand.setTableName(tableSection.substring(0, likeMatcher.start()).trim());
                } else {
                    //CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name
                    //  [(create_definition,...)]
                    //  [table_options]
                    //  [partition_options]
                    //  select_statement
                    Matcher selectMatcher = CREATE_TABLE__SELECT__PATTERN.matcher(tableSection);
                    if (selectMatcher.find()) {

                        String groupText = selectMatcher.group();
                        createTableCommand.setSelect(groupText.substring(groupText.indexOf("select")).trim());

                        createTableCommand.setSelectIgnore(selectMatcher.group("ignoreSection") != null);
                        createTableCommand.setSelectReplace(selectMatcher.group("replaceSection") != null);
                        createTableCommand.setSelectAs(selectMatcher.group("asSection") != null);

                        tableSection = tableSection.substring(0, selectMatcher.start());
                    } else {
                        //CREATE [TEMPORARY] TABLE [IF NOT EXISTS] tbl_name
                        //  (create_definition,...)
                        //  [table_options]
                        //  [partition_options]
                        
                        // no need parse - just get 'tableSection'
                    }
                    
                    int indexStart = tableSection.indexOf("(");
                    int indexFinish = tableSection.lastIndexOf(")");
                    if (indexStart > -1) {
                        String tableName = tableSection.substring(0, indexStart);
                        createTableCommand.setTableName(tableName != null ? tableName.trim() : null);
                        createTableCommand.setColumns(parseCreateTableColumns(tableSection.substring(indexStart + 1, indexFinish)));
                    } else {
                        createTableCommand.setTableName(tableSection.trim());
                    }
                }
            }
        }
    }
    
    private List<CreateTableColumnDefinition> parseCreateTableColumns(String columnsDefinition) {
        
        if (columnsDefinition != null) {
            columnsDefinition = columnsDefinition.replaceAll("\\s+", " ");

            ColumnResult result = parseColumnsResult(columnsDefinition);
            if (result != null) {
                List<CreateTableColumnDefinition> list = new ArrayList<>();
                              
                for (String column: result.getColumns()) {
                    
                    Matcher m = SUBQUERY0_PATTERN.matcher(column);
                    while (m.find()) {
                        try {
                            int index = Integer.valueOf(m.group(2));
                            column = column.replaceAll("\\^" + index, "(" + result.getSubQuery(index).trim() + ")");
                        } catch (Throwable th) {
                        }
                    }
                    
                    m = FUNCTION_PATTERN.matcher(column);
                    while (m.find()) {
                        try {
                            int index = Integer.valueOf(m.group(2));
                            column = column.replaceAll("\\!" + index, result.getFunction(index).trim());
                        } catch (Throwable th) {
                        }
                    }
                    
                    m = QUOTE_PATTERN.matcher(column);
                    while (m.find()) {
                        try {
                            int index = Integer.valueOf(m.group(2));
                            column = column.replaceAll("\\&" + index, result.getQuote(index).trim());
                        } catch (Throwable th) {
                        }
                    }
                    
                    list.add(parseCreateTableColumnDefinition(column));
                }

                return list;
            }
        }
        return null;
    }
    
    private CreateTableColumnDefinition parseCreateTableColumnDefinition(String columnDefinition) {
        CreateTableColumnDefinition result = new CreateTableColumnDefinition();
        
        Matcher matcher = CREATE_TABLE__PRIMARY_KEY__PATTER.matcher(columnDefinition.trim());
        if (matcher.matches()) {
            
            result.setType(CreateTableColumnDefinition.Type.PRIMARY_KEY); 
            
            result.setConstraint(matcher.group("constraint") != null);            
            String symbol = matcher.group("constraintName");
            result.setConstraintSymbol(symbol != null ? symbol.trim() : null);
            
            result.setUsing(matcher.group("using") != null);
            String usingType = matcher.group("usingType");            
            result.setUsingType(usingType != null ? usingType.trim() : null);
            
            String option = matcher.group("option");
            result.setIndexOption(option != null ? option.trim() : null);
            
            String columns = matcher.group("columns");
            if (columns != null){
                columns = columns.trim();
                if (columns.startsWith("(") && columns.endsWith(")")) {
                    columns = columns.substring(1, columns.length() - 1);
                }
            }
            result.setColumns(columns);
            
            String columnDef = matcher.group("columnDefinition");
            if (columnDef != null && !columnDef.isEmpty()) {
                String data[] = columnDef.trim().split(" ");
                //first its column name
                if (data != null && data.length > 0) {
                    result.setName(data[0]);

                    String definition = "";
                    for (int i = 1; i < data.length; i++) {
                        if (data[i].trim().length() > 0) {
                            definition += data[i].trim() + " ";
                        }
                    }
                    result.setDefinition(definition.trim());
                }
            }
            
            return result;
        }
        
        matcher = CREATE_TABLE__FULLTEXT_SPATIAL__PATTER.matcher(columnDefinition.trim());
        if (matcher.matches()) {
            
            result.setType(matcher.group("fulltext") != null ? CreateTableColumnDefinition.Type.FULLTEXT : CreateTableColumnDefinition.Type.SPATIAL);
                        
            result.setIndex(matcher.group("index") != null);  
            result.setKey(matcher.group("key") != null);  
            
            String indexName = matcher.group("indexName");       
            result.setName(indexName != null ? indexName.trim() : null);
            
            String option = matcher.group("option");
            result.setIndexOption(option != null ? option.trim() : null);
            
            String columns = matcher.group("columns");
            if (columns != null){
                columns = columns.trim();
                if (columns.startsWith("(") && columns.endsWith(")")) {
                    columns = columns.substring(1, columns.length() - 1);
                }
            }
            result.setColumns(columns);
            
            return result;
        }
        
        matcher = CREATE_TABLE__UNIQUE__PATTER.matcher(columnDefinition.trim());
        if (matcher.matches()) {
            
            result.setType(CreateTableColumnDefinition.Type.UNIQUE); 
            
            result.setConstraint(matcher.group("constraint") != null);            
            String symbol = matcher.group("constraintName");
            result.setConstraintSymbol(symbol != null ? symbol.trim() : null);
            
            String indexName = matcher.group("indexName");       
            result.setName(indexName != null ? indexName.trim() : null);
            
            result.setIndex(matcher.group("index") != null);  
            result.setKey(matcher.group("key") != null);  
            
            result.setUsing(matcher.group("using") != null);
            String usingType = matcher.group("usingType");            
            result.setUsingType(usingType != null ? usingType.trim() : null);
            
            String option = matcher.group("option");
            result.setIndexOption(option != null ? option.trim() : null);
            
            String columns = matcher.group("columns");
            if (columns != null){
                columns = columns.trim();
                if (columns.startsWith("(") && columns.endsWith(")")) {
                    columns = columns.substring(1, columns.length() - 1);
                }
            }
            result.setColumns(columns);
            
            return result;
        }
        
        matcher = CREATE_TABLE__INDEX_KEY__PATTER.matcher(columnDefinition.trim());
        if (matcher.matches()) {            
            result.setType(matcher.group("index") != null ? CreateTableColumnDefinition.Type.INDEX : CreateTableColumnDefinition.Type.KEY); 
            
            String indexName = matcher.group("indexName");       
            result.setName(indexName != null ? indexName.trim() : null);
            
            result.setUsing(matcher.group("using") != null);
            String usingType = matcher.group("usingType");            
            result.setUsingType(usingType != null ? usingType.trim() : null);
            
            String option = matcher.group("option");
            result.setIndexOption(option != null ? option.trim() : null);
            
            String columns = matcher.group("columns");
            if (columns != null){
                columns = columns.trim();
                if (columns.startsWith("(") && columns.endsWith(")")) {
                    columns = columns.substring(1, columns.length() - 1);
                }
            }
            result.setColumns(columns);
            
            return result;            
        }
        
        matcher = CREATE_TABLE__FOREIGN_KEY__PATTER.matcher(columnDefinition.trim());
        if (matcher.matches()) {
            
            result.setType(CreateTableColumnDefinition.Type.FOREIGN_KEY); 
            
            result.setConstraint(matcher.group("constraint") != null);            
            String symbol = matcher.group("constraintName");
            result.setConstraintSymbol(symbol != null ? symbol.trim() : null);
            
            String indexName = matcher.group("indexName");       
            result.setName(indexName != null ? indexName.trim() : null);
            
            String option = matcher.group("option");
            result.setIndexOption(option != null ? option.trim() : null);
            
            String columns = matcher.group("columns");
            if (columns != null){
                columns = columns.trim();
                if (columns.startsWith("(") && columns.endsWith(")")) {
                    columns = columns.substring(1, columns.length() - 1);
                }
            }
            result.setColumns(columns);
            
            return result;
        }
        
        matcher = CREATE_TABLE__CHECK__PATTER.matcher(columnDefinition.trim());
        if (matcher.matches()) {
            
            result.setType(CreateTableColumnDefinition.Type.CHECK);
            
            String expr = matcher.group("expr");
            if (expr != null){
                expr = expr.trim();
                if (expr.startsWith("(") && expr.endsWith(")")) {
                    expr = expr.substring(1, expr.length() - 1);
                }
            }
            result.setDefinition(expr);
            
            return result;
        }
                
        // if not any others matches - than its column
        result.setType(CreateTableColumnDefinition.Type.COLUMN);
            
        String data[] = columnDefinition.trim().split(" ");
        //first its column name
        if (data != null && data.length > 0) {
            result.setName(data[0]);

            String definition = "";
            for (int i = 1; i < data.length; i++) {
                if (data[i].trim().length() > 0) {
                    definition += data[i].trim() + " ";
                }
            }
            result.setDefinition(definition.trim());
        }

        return result;
    }
    
        
    private IntoTableClause parseIntoTable(String table, String columns, final boolean hasInto)
    {
        if (table != null) {
            ValuesClause vc = null;
            if (columns != null) {
                vc = new ValuesClause(Arrays.asList(columns.trim()));
            }

            return new IntoTableClause(table.trim(), hasInto, vc);
        }
        
        return null;
    }
    
    private OnDuplicateKeyUpdateClause parseOnDuplicateKeyUpdateClause(final AbstractCommand command, final String query)
    {
        if (query != null)
        {
            final OnDuplicateKeyUpdateClause onDuplicateClause = new OnDuplicateKeyUpdateClause();
            onDuplicateClause.setCondition(parseConditions(command, query));

            return onDuplicateClause;
        }
        
        return null;
    }
    
    
    private ValuesClause parseValues(String query)
    {
        if (query != null)
        {
            try {
                // check maybe this is buld insert
                List<String> values = new ArrayList<>();
                
                int from = query.indexOf("(");
                int to = query.lastIndexOf(")");
                query = query.substring(from + 1, to).trim();
                
                from = 0;
                to = 0;
                while (from >= 0 && to >= 0) {
                    from = query.indexOf("(", from + 1);
                    to = query.indexOf(")", to + 1);
                    if (to < from) {
                        
                        values.add(query.substring(0, to));
                        query = query.substring(from + 1);
                        from = 0;
                        to = 0;
                    }
                }
                
                if (values.isEmpty()) {
                    values.add(query);
                }
                
                
                return new ValuesClause(values);
            } catch (Throwable th) {
                log.error("Error", th);
            }
        }
        
        return null;
    }
    
    private SetClause parseSet(final AbstractCommand command, final String query)
    {
        if (query != null)
        {
            final SetClause setClause = new SetClause(query);
            setClause.setCondition(parseConditions(command, query));

            return setClause;
        }
        
        return null;
    }
        
    private LimitClause parseLimit(final String query)
    {
        return query != null ? new LimitClause(query.trim()) : null;
    }
    
    private HavingClause parseHaving(final AbstractCommand command, final String query)
    {
        if (query != null)
        {
            final HavingClause havingClause = new HavingClause(query);
            havingClause.setCondition(parseConditions(command, query));

            return havingClause;
        }
        
        return null;
    }
    
    private OrderByClause parseOrderBy(final AbstractCommand command, final String query)
    {
        if (query != null)
        {
            final OrderByClause orderByClause = new OrderByClause(query);
            orderByClause.setColumns(parseColumns(command, query));

            return orderByClause;
        }
        
        return null;
    }
    
    private GroupByClause parseGroupBy(final AbstractCommand command, final String query, boolean withRollup)
    {
        if (query != null)
        {
            final GroupByClause groupByClause = new GroupByClause(query);
            groupByClause.setWithRollup(withRollup);
            groupByClause.setColumns(parseColumns(command, query));

            return groupByClause;
        }
        
        return null;
    }
    
    private WhereClause parseWhere(final AbstractCommand command, final String query)
    {
        if (query != null)
        {
            final WhereClause whereClause = new WhereClause(query);
            whereClause.setCondition(parseConditions(command, query));
            
            return whereClause;
        }
        
        return null;
    }
    
    
    private FromClause parseFrom(final AbstractCommand command, final String query)
    {
        if(query != null)
        {
            final FromClause fromClause = new FromClause(query);
            fromClause.setTables(parseTables(command, query));
            
            return fromClause;
        }

        return null;
    }
    
    private List<TableReferenceClause> parseTables(final AbstractCommand command, String query)
    {
        if (query != null)
        {
            query = query.trim();
            if (query.startsWith("(") && query.endsWith(")")) {
                query = query.substring(1, query.length() - 1).trim();
            }
            
            final List<TableReferenceClause> tables = new ArrayList<>();

            String replaceBrackets = replaceBracket(query);
            
            String[] tablesStr = replaceBrackets.split(",");
            
            int length = 0;
            for (String str: tablesStr) {
                
                String currentStr = query.substring(length, length + str.length());
                
                final Matcher mathcer = COLUMNS_AND_TABLES__PATTERN.matcher(str);
                if (mathcer.find()) {
                    final TableReferenceClause table = parseTable(command, currentStr);
                    if (table != null) {
                        tables.add(table);
                    }
                } else {
                    tables.add(new TableClause(currentStr, null, null, null));
                }
                
                length += str.length() + 1;
            }
            
            return tables;
        }
        
        return null;
    }
    
    private List<JoinClause> parseJoinClauses(final AbstractCommand command, String query) {
        
        List<JoinClause> joinClauses = null;
        
        if (query.toUpperCase().contains("JOIN")) {
            String[] joins = query.split("(" + JOIN_PART_CI + ")?" + "(?i)" + JOIN_CI);

            query = query.substring(joins[0].length());

            if (joins.length > 1)
            {
                for (int i = 1; i < joins.length; i++) {

                    int index = query.indexOf(joins[i]) + joins[i].length();
                    String joinQuery = query.substring(0, index);

                    query = query.substring(index);

                    Matcher matcher = JOIN__PATTERN.matcher(joinQuery);
                    if (matcher.matches()) {

                        final String join = matcher.group("joinSection");
                        final String joinType = matcher.group("joinType");
                        final String on = matcher.group("onSection");
                        final String using = matcher.group("usingSection");

                        if (joinClauses == null) {
                            joinClauses = new ArrayList<>();
                        }

                        joinClauses.add(parseJoin(command, join, joinType, on, using));
                    }
                }
            } else {

                if (joinClauses == null) {
                    joinClauses = new ArrayList<>();
                }

                joinClauses.add(new JoinClause(null));
            }
        }
        
        return joinClauses;
    }
    
    private TableReferenceClause parseTable(final AbstractCommand command, final String query)
    {
        if (query != null && !query.isEmpty())
        {
            final Matcher matcher = TABLE__PATTERN.matcher(query.trim());
            if (matcher.matches())
            {
                String name = matcher.group("tableSection");
                String alias = matcher.group("aliasSection");
                boolean as = matcher.group("as") != null;
                
                String others = null;
                
                List<JoinClause> joinClauses = parseJoinClauses(command, query);
                
                // TODO Need change this in future
                if ((name == null || name.isEmpty()) && alias != null)
                {
                    name = alias;
                    alias = null;
                }
                
                
                if (name != null) {
                    Matcher m = Pattern.compile("\\bas\\b", Pattern.CASE_INSENSITIVE).matcher(name);
                    if (m.find()) {
                        alias = alias != null ? name.substring(m.start() + 2) + alias : name.substring(m.start() + 2);
                        name = name.substring(0, m.start());
                        
                        as = true;
                    } else {
                        String regex = "(?<=[-+/()=\\s><])|(?=[-+/()=\\s><])";
                        final String[] splitName = name.trim().split(regex);
                        if (splitName.length > 1 && !splitName[splitName.length - 1].trim().matches("[-+/()=><]"))
                        {
                            String preLastWord = splitName[splitName.length - 2];
                            if (preLastWord.trim().isEmpty() && splitName.length > 2) {
                                preLastWord = splitName[splitName.length - 3];
                            }
                            
                            if (!preLastWord.trim().substring(preLastWord.length() - 1).matches("[-+/()=><]")) {
                                alias = splitName[splitName.length - 1];
                                name = splitName[0];
                                for (int i = 1; i < splitName.length - 1; i++) {
                                    name += splitName[i];
                                }
                            }
                        }
                    }
                }
                
                
                if (name != null && name.startsWith("#"))
                {
                    // its sub query
                    final SelectCommand subSelect = new SelectCommand(command.getQuerySource(name.trim()), command);
                    parseSelect(command, subSelect, command.getSubQuery(name.trim()));
                    command.removeSubQuery(name.trim());

                    subSelect.setAlias(alias != null ? alias.trim() : null);
                    subSelect.setJoinClauses(joinClauses);

                    return subSelect;
                } 
                else if (name != null && name.startsWith("?"))
                {
                    // its sub query
                    final SelectCommand subSelect = command.getSubUnion(name.trim());
                    subSelect.setAlias(alias != null ? alias.trim() : null);
                    subSelect.setJoinClauses(joinClauses);

                    return subSelect;
                }
                
                if (alias == null && name != null) {
                    String[] strs = name.split("\\s+");
                    if (strs.length > 1) {
                        name = strs[0];
                        alias = strs[1];
                        
                        if (strs.length > 2) {
                            for (int i = 2; i < strs.length; i++) {
                                if (others == null) {
                                    others = strs[i];
                                } else {
                                    others += " " + strs[i];
                                }
                            }
                        }
                    }
                }
                
                final TableReferenceClause tableClause = new TableClause(matcher.group(), name != null ? name.trim() : null, alias != null ? alias.trim() : null, others);
                tableClause.setJoinClauses(joinClauses);
                tableClause.setAs(as);
                return tableClause;
            }
        }
        
        return null;
    }
    
    private JoinClause parseJoin(final AbstractCommand command, final String joinQuery, final String joinType, final String onQuery, final String usingColumns)
    {
        if (joinQuery != null)
        {
            final JoinClause joinClause = new JoinClause(joinQuery + (onQuery != null ? " ON " + onQuery : ""));

            joinClause.setTable(parseTable(command, joinQuery));
            joinClause.setConditions(parseConditions(command, onQuery));
            joinClause.setType(JoinClause.JoinType.parseType(joinType != null ? joinType.trim() : null));

            joinClause.setUsingColumns(usingColumns);
            
            return joinClause;
        }
        
        return null;
    }
    
    private String parseConditions(final AbstractCommand command, final String query)
    {
        if (query != null && !query.isEmpty())
        {
            // TODO Now just for validation, bu when add parsing conditions - add select to condition
            if (query.startsWith("#"))
            {
                final int index = query.indexOf(" ");
                final String queryKey = query.substring(0, index >= 0 ? index : query.length());

                // its sub query
                final SelectCommand selectCommand = new SelectCommand(command.getQuerySource(queryKey.trim()), command);
                parseSelect(command, selectCommand, command.getSubQuery(queryKey.trim()));
                command.removeSubQuery(queryKey.trim());
            }
        }
        
        // TODO Add condition parsing
        return query != null? query.trim() : null;
    }
    
    public static Integer findFunctionIndex(String filter) {
        Matcher m = FUNCTION_PATTERN.matcher(filter);
        if (m.find()) {
            return Integer.valueOf(m.group(2));
        }
        
        return null;
    }
    
    public static Integer findSubQuery0Index(String filter) {
        Matcher m = SUBQUERY0_PATTERN.matcher(filter);
        if (m.find()) {
            return Integer.valueOf(m.group(2));
        }
        
        return null;
    }
    
    public static String findSubQueryIndex(String filter) {
        Matcher m = SUBQUERY_PATTERN.matcher(filter);
        if (m.find()) {
            return m.group(2);
        }
        
        return null;
    }
    
    public static Integer findQuoteIndex(String filter) {
        Matcher m = QUOTE_PATTERN.matcher(filter);
        if (m.find()) {
            return Integer.valueOf(m.group(2));
        }
        
        return null;
    }
    
    private List<ColumnReferenceClause> parseColumns(final AbstractCommand command, final String query)
    {
        if (query != null && !query.isEmpty())
        {
            final List<ColumnReferenceClause> columns = new ArrayList<>();
            
            ColumnResult columnResult = parseColumnsResult(convertFunctions(command, query));
            for (String s: columnResult.getColumns()) {
                Integer subQuery = findSubQuery0Index(s);
                while (subQuery != null) {
                    s = s.replace("^" + subQuery, "(" + columnResult.getSubQuery(subQuery) + ")");
                    subQuery = findSubQuery0Index(s);
                }
                
                subQuery = findFunctionIndex(s);
                while (subQuery != null) {
                    s = s.replace("!" + subQuery, columnResult.getFunction(subQuery));
                    subQuery = findFunctionIndex(s);
                }
                
                subQuery = findQuoteIndex(s);
                while (subQuery != null) {
                    s = s.replace("&" + subQuery, columnResult.getQuote(subQuery));
                    subQuery = findQuoteIndex(s);
                }
                
//                String keyQuery = findSubQueryIndex(s);
//                while (keyQuery != null) {
//                    s = s.replace("#" + keyQuery, "(" + command.getSubQuery("#" + keyQuery) + ")");
//                    keyQuery = findSubQueryIndex(s);
//                }
                
                final ColumnReferenceClause column = parseColumn(command, s);
                if (column != null)
                {
                    columns.add(column);
                }
            }
            
            return columns;
        }
        
        return null;
    }
    
    private ColumnReferenceClause parseColumn(final AbstractCommand command, final String query)
    {
        if (query != null)
        {
            final Matcher mathcer = COLUMN__PATTERN.matcher(query.trim());
            if (mathcer.matches())
            {
                String caseText = mathcer.group("caseSection");
                String name = mathcer.group("columnSection");
                String alias = mathcer.group("aliasSection");

                boolean asc = mathcer.group("asc") != null;
                boolean desc = mathcer.group("desc") != null;
                boolean as = mathcer.group("as") != null;
                
                if (caseText == null && (name == null || name.isEmpty()) && alias != null)
                {
                    name = alias;
                    alias = null;
                }
                
                if (name != null) {
                    // check if 
                    int ind = name.toLowerCase().lastIndexOf(" as ");
                    if (ind > -1) {
                        alias = name.substring(ind + 4);
                        name = name.substring(0, ind);
                        
                        as = true;
                    } else {
                        String regex = "(?<=[-+/()=\\s><])|(?=[-+/()=\\s><])";
                        final String[] splitName = name.trim().split(regex);
                        if (splitName.length > 1 && !splitName[splitName.length - 1].trim().matches("[-+/()=><]"))
                        {
                            String preLastWord = splitName[splitName.length - 2];
                            if (preLastWord.trim().isEmpty() && splitName.length > 2) {
                                preLastWord = splitName[splitName.length - 3];
                            }
                            
                            if (!preLastWord.trim().substring(preLastWord.length() - 1).matches("[-+/()=><]")) {
                                alias = splitName[splitName.length - 1];
                                name = splitName[0];
                                for (int i = 1; i < splitName.length - 1; i++) {
                                    name += splitName[i];
                                }
                            }
                        }
                    }
                }
                
                
                if (name != null && name.startsWith("#"))
                {
                    final int index = name.indexOf(" ");
                    final String queryKey = name.substring(0, index >= 0 ? index : name.length());

                    // its sub query                    
                    final SelectCommand selectCommand = new SelectCommand(command.getQuerySource(queryKey.trim()), command);
                    parseSelect(command, selectCommand, convertFunctions(command, command.getSubQuery(queryKey.trim())));
                    command.removeSubQuery(queryKey.trim());

                    selectCommand.setAlias(alias != null ? alias.trim() : null);
                    selectCommand.setAsc(asc);
                    selectCommand.setAs(as);
                    return selectCommand;
                }
                
                if (name != null) {
                    // maybe this is a function
                    Matcher m = COLUMN_FUNCTION__PATTERN.matcher(name);
                    if (m.find()) {
                        name = m.group("functionSection");
                        String tempAlias = m.group("aliasSection");
                        if (tempAlias != null && !tempAlias.isEmpty()) {
                            alias = tempAlias;
                        }
                    }
                }
                
 
                ColumnClause c = new ColumnClause(query, convertFunctions(command, name != null && !name.trim().isEmpty() ? name.trim() : caseText), alias != null && !alias.trim().isEmpty() ? alias.trim() : null);
                c.setAsc(asc);
                c.setSimple(caseText == null);
                c.setHasAsc(asc || desc);
                c.setAs(as);
                return c;
            } else {
                return new ColumnClause(query, "", "");
            }
        }
        
        return null;
    }
    
    
    private String parseSubSelectQueries(final AbstractCommand command, String sqlQuery, final int lvl) {
        return parseSubSelectQueries(command, sqlQuery, lvl, true);
    }   
    
    private String parseSubSelectQueries(final AbstractCommand command, String sqlQuery, final int lvl, boolean replaceStrings)
    {        
        // need remove all commended lines
        // trim line must start with --
        String[] lines = sqlQuery.split("\n");
        sqlQuery = "";
        for (String l: lines) {
            if (!l.trim().startsWith("--")) {
                if (!sqlQuery.isEmpty()) {
                    sqlQuery += " ";
                }
                sqlQuery += l;
            }
        }
        
        if (replaceStrings) {
            // TODO Need check this loop
            int index = 0;
            while (true) {

                int start = sqlQuery.indexOf("'");
                int end = -1;
                if (start > -1) {
                    end = sqlQuery.indexOf("'", start + 1);
                }

                if (start > -1) {

                    if (end > -1) {
                        String forReplace = sqlQuery.substring(start, end + 1);

                        final String key = "@" + lvl + "" + index;
                        sqlQuery = sqlQuery.substring(0, start) + key + sqlQuery.substring(end + 1);

                        command.putString(key.trim(), forReplace);
                        index++;
                    } else {
                        String forReplace = sqlQuery.substring(start);

                        final String key = "@" + lvl + "" + index;
                        sqlQuery = sqlQuery.substring(0, start) + key;

                        command.putString(key.trim(), forReplace);
                        index++;
                    }
                } else {
                    break;
                }
            }

            while (true) {

                int start = sqlQuery.indexOf("\"");
                int end = -1;
                if (start > -1) {
                    end = sqlQuery.indexOf("\"", start + 1);
                }

                if (start > -1) {
                    
                    if (end > -1) {
                        String forReplace = sqlQuery.substring(start, end + 1);

                        final String key = "@" + lvl + "" + index;
                        sqlQuery = sqlQuery.substring(0, start) + key + sqlQuery.substring(end + 1);

                        command.putString(key.trim(), forReplace);
                        index++;
                    } else {
                        String forReplace = sqlQuery.substring(start);

                        final String key = "@" + lvl + "" + index;
                        sqlQuery = sqlQuery.substring(0, start) + key;

                        command.putString(key.trim(), forReplace);
                        index++;
                    }
                } else {
                    break;
                }
            }
        }
        
        Pattern p = Pattern.compile("(\\w+\\()");
        Matcher m0 = p.matcher(sqlQuery);
        
        int functionIndex = 0;
        while (m0.find()) {
            int start = m0.start();
            int end = m0.end();
            
            int diff = 1;
            for (int i = end; i < sqlQuery.length(); i++) {
                char c = sqlQuery.charAt(i);
                if (c == '(') {
                    diff++;
                } else if (c == ')') {
                    diff--;
                }
                
                // this is end of function
                if (diff == 0) {
                    end = i;
                    break;
                }
            }
            
            if (diff != 0) {
                end = sqlQuery.length() - 1;
            }
            
            String function = sqlQuery.substring(start, end + 1);
            if (!function.toLowerCase().startsWith("select(") &&
                    !function.toLowerCase().startsWith("table(") &&
                    !function.toLowerCase().startsWith("values(") &&
                    !function.toLowerCase().startsWith("and(") &&
                    !function.toLowerCase().startsWith("or(") &&
                    !function.toLowerCase().startsWith("all(") &&
                    !function.toLowerCase().startsWith("union(") &&
                    !function.toLowerCase().startsWith("by(") &&
                    !function.toLowerCase().startsWith("having(") &&
                    !function.toLowerCase().startsWith("join(") &&
                    !function.toLowerCase().startsWith("limit(") &&
                    !function.toLowerCase().startsWith("where(") &&
                    !function.toLowerCase().startsWith("on(") &&
                    !function.toLowerCase().startsWith("into(") &&
                    !function.toLowerCase().startsWith("from(")) {
                String key = "!" + lvl + "" + functionIndex;
                sqlQuery = sqlQuery.substring(0, start) + key + sqlQuery.substring(end + 1);

                command.putFunction(key.trim(), function);
                m0 = p.matcher(sqlQuery);

                functionIndex++;
            }
        }
        
        String query = sqlQuery.trim();
        
        int subLvl = 0;
        while (true)
        {
            Matcher m = SUB_SELECT__PATTERN.matcher(query);
            if (!m.find()) {
                break;
            }
            
            int start = m.start();            
            int finish = query.length();
            
            final String text = query.substring(start + 1);
            
            String result = text;
            
            int brachesDiff = 1;
            for (int i = start + 1; i < query.length(); i++)
            {
                switch (query.charAt(i))
                {
                    case '(':
                        brachesDiff++;
                        break;
                        
                    case ')':
                        brachesDiff--;
                        break;
                }
                
                // we find end of select query
                if (brachesDiff == 0)
                {
                    result = query.substring(start, i + 1);
                    finish = i;
                    break;
                }
            }
            
            if (brachesDiff > 0) {
                result = query.substring(start);
                finish = query.length();
            }
            

            final String key = " #" + lvl + "" + subLvl + " ";
            int end = finish + 1;
            query = query.substring(0, start) + key + (end > query.length() ? "" : query.substring(end));
            subLvl++;
            
            result = convertFunctions(command, result);
            
            command.putQuerySource(key.trim(), result);
            command.putSubQueriesIdx(key.trim(), new Integer[]{start + 1, finish});
            command.putSubQuery(key.trim(), convertFunctions(command, parseSubSelectQueries(command, result.substring(1, result.length() - 1), lvl + 1)));
        }
        
        return query;
    }
    
    private static final Pattern LAST_FUNCTION_PATTERN = Pattern.compile("\\s*(\\b\\w*)$");    
    private static String parseConditions(String query, ConditionResult result) {
        
        // find '( ... )' block            
        int brachesDiff = 0;
        int start = -1;
        String prefText = "";
        boolean function = false;
        for (int i = 0; i < query.length(); i++)
        {
            switch (query.charAt(i))
            {
                case '(':
                    if (start == -1) {
                        start = i;
                        
                        if (i > 0 && !prefText.endsWith(" ") && !prefText.toUpperCase().endsWith(" AND") && !prefText.toUpperCase().endsWith(")AND") &&
                                !prefText.toUpperCase().endsWith(" OR") && !prefText.toUpperCase().endsWith(")OR")) {
                            
                            Matcher m = LAST_FUNCTION_PATTERN.matcher(prefText);
                            if (m.find()) {
                                //start = prefText.lastIndexOf(" ");
                                start = m.start(1);
                                if (start == -1) {
                                    start = 0;
                                }
                                
                                function = true;
                            }
                        }
                    }
                    
                    brachesDiff++;
                    break;

                case ')':
                    brachesDiff--;
                    break;
            }

            // we find end of select query
            if (brachesDiff == 0 && start > -1) {
                if (function) {
                    
                    int functionIndex = result.nextFunctionIndex();
                    result.putFunction(functionIndex, query.substring(start, i + 1).trim());
                    
                    query = query.substring(0, start) + " !" + functionIndex + query.substring(i + 1) + " ";
                } else {
                    
                    int conditionIndex = result.nextConditionIndex();
                    
                    String r0 = parseConditions(query.substring(start + 1, i), result);
                    if (r0.trim().startsWith("!") && r0.split(" ").length == 1) {
                        query = query.substring(0, start) + r0 + query.substring(i + 1) + " ";
                        
                    } else {                    
                        result.putSubCondition(conditionIndex, r0);
                        query = query.substring(0, start) + " #" + conditionIndex + query.substring(i + 1) + " ";
                    }
                }
                
                query = parseConditions(query, result);
                break;
            }
            
            prefText += query.charAt(i);
        }
        
        
        return query;
    }
    
    private static final Pattern LAST_COLUMN_FUNCTION_PATTERN = Pattern.compile(",?\\s*\\b(\\w*)$");    
    private static String parseColumns(String query, ColumnResult result) {
        
        try {
            // find all quote
            boolean singleQuote = false;
            boolean doubleQuote = false;
            int start = -1;
            int lastIndex = 0;

            char prevChar = ' ';
            String queryWithoutQuote = "";
            for (int i = 0; i < query.length(); i++)
            {
                switch (query.charAt(i))
                {
                    case '\'':
                        if (prevChar != '\\' && !doubleQuote) {
                            singleQuote = !singleQuote;
                            if (singleQuote) {
                                start = i;
                            }
                        }
                        break;

                    case '"':
                        if (prevChar != '\\' && !singleQuote) {
                            doubleQuote = !doubleQuote;
                            if (doubleQuote) {
                                start = i;
                            }
                        }
                        break;

                }

                prevChar = query.charAt(i);

                // we find end of select query
                if (!doubleQuote && !singleQuote && start > -1) {

                    int quoteIndex = result.nextFunctionIndex();
                    result.putQuote(quoteIndex, query.substring(start, i + 1).trim());

                    queryWithoutQuote += query.substring(lastIndex, start) + "&" + quoteIndex + " ";
                    lastIndex = i + 1;
                    start = -1;
                }
            }

            queryWithoutQuote +=  query.substring(lastIndex, query.length());

            // find '( ... )' block            
            int brachesDiff = 0;
            start = -1;
            String prefText = "";
            boolean function = false;

            query = queryWithoutQuote.isEmpty() ? query : queryWithoutQuote;

            for (int i = 0; i < query.length(); i++)
            {
                switch (query.charAt(i))
                {
                    case '(':
                        if (start == -1) {
                            start = i;

                            if (i > 0 && !prefText.endsWith(" ") && !prefText.toUpperCase().endsWith(",")) {

                                Matcher m = LAST_COLUMN_FUNCTION_PATTERN.matcher(prefText);
                                if (m.find()) {
                                    start = m.start(1);
                                    if (start == -1) {
                                        start = 0;
                                    }

                                    function = true;
                                }
                            }
                        }

                        brachesDiff++;
                        break;

                    case ')':
                        brachesDiff--;
                        break;
                }

                // we find end of select query
                if (brachesDiff == 0 && start > -1) {
                    if (function) {

                        int functionIndex = result.nextFunctionIndex();
                        result.putFunction(functionIndex, query.substring(start, i + 1).trim());

                        query = query.substring(0, start) + "!" + functionIndex + query.substring(i + 1) + " ";
                    } else {

                        int queryIndex = result.nextQueryIndex();
                        result.putSubQuery(queryIndex, parseColumns(query.substring(start + 1, i), result));

                        query = query.substring(0, start) + "^" + queryIndex + query.substring(i + 1) + " ";
                    }

                    query = parseColumns(query, result);
                    break;
                }

                prefText += query.charAt(i);
            }
        } catch (Throwable th) {
        }
        
        return query;
    }
    
    private String replaceBracket(String query) {
        String s = "";
        int bracket = 0;
        
        for (int i = 0; i < query.length(); i++) {
            char c = query.charAt(i);
            
            switch (c) {
                case '(':
                    bracket++;
                    s += '+';
                    break;
                    
                case ')':
                    bracket--;
                    s += '+';
                    break;
                    
                default:
                    s += bracket > 0 ? '+' : c;                
                    break;
            }
        }
        
        return s;
    }
    
    public static String removeComments(String text) {
        Pattern commentPattern = Pattern.compile("/\\*.*?\\*/", Pattern.DOTALL);
        text = commentPattern.matcher(text).replaceAll("");
        
        // remove all '--' comments
        String[] lines = text.split("\n");
        StringBuilder buffer = new StringBuilder();
        for (String l: lines) {
            int index = l.indexOf("--");
            String s;
            
            if (index > 0) {
                s = l.substring(0, index);
            } else {
                s = l;
            }
            
            buffer.append(s).append("\n");
        }
        
        return buffer.toString().trim();
    }
    
    public static String replaceComment(String text) {
        StringBuilder buffer = new StringBuilder(replaceString(text));
        
        int start = buffer.indexOf(COMMENT__START);
        int end = buffer.indexOf(COMMENT__END);
        
        while(start > -1 && end > -1) {
            buffer = buffer.delete(start, end + 2);
            
            start = buffer.indexOf(COMMENT__START);
            end = buffer.indexOf(COMMENT__END);
        }
        
        if (start > -1) {
            buffer = buffer.delete(start, buffer.length());
            end = buffer.indexOf(COMMENT__END);
        }
        
        if (end > 0) {
            buffer = buffer.delete(0, end + 2);
        }
        
        // remove all '--' comments
        String[] lines = buffer.toString().split("\n");
        buffer = new StringBuilder();
        for (String l: lines) {
            int index = l.indexOf("--");
            String s;
            boolean hasComment = false;
            if (index > 0) {
                s = l.substring(0, index).trim();
                hasComment = true;
            } else {
                s = l.trim();
            }
            if (buffer.length() > 0) {
                if (s.startsWith("(") || s.startsWith(")") || s.startsWith(",") || s.startsWith(".") || s.startsWith("`")) {
                    // nothing
                    if (!hasComment) {
                        buffer.append(" ");
                    }
                } else {
                    buffer.append(" ");
                }
            }
            buffer.append(s);
        }
        
        return buffer.toString();
    }
        
    
    public static String replaceString(String text) {
        text = text.replaceAll("\\\\'", "");
        StringBuilder sb = new StringBuilder(text);
        
        int start = sb.indexOf("'");
        int end = sb.indexOf("'", start + 1);
        
        while(start > -1 && end > -1) {            
            sb = sb.replace(start, end + 1, "''");
            start = sb.indexOf("'", start + 2);
            end = start > -1 ? sb.indexOf("'", start + 1) : -1;
        }
        
        if (start > -1) {
            sb = sb.replace(start, sb.length(), "'");
            end = sb.indexOf("'");
        }
        
        if (end > 0) {
            sb = sb.replace(0, end + 1, "'");
        }
        
        return sb.toString();
    }
    // </editor-fold>
}
