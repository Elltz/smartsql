
package np.com.ngopal.smart.sql.query.commands;

import java.util.List;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DeleteCommand extends AbstractCommand implements WhereClauseProvider, FromProvider
{
    private boolean lowPriority = false;
    private boolean quick = false;
    private boolean ignore = false;
    
    private List<TableReferenceClause> tables;
    
    private String partition;
    
    private FromClause fromClause;
    private WhereClause whereClause;
    private OrderByClause orderByClause;
    private LimitClause limitClause;
    
    private List<TableReferenceClause> using;
    
    private final String deleteSource;
        
    public DeleteCommand(final String selectSource)
    {
        this.deleteSource = selectSource;
    }

    public String getPartition() {
        return partition;
    }
    public void setPartition(String partition) {
        this.partition = partition;
    }

    
    public boolean isLowPriority() {
        return lowPriority;
    }
    public void setLowPriority(boolean lowPriority) {
        this.lowPriority = lowPriority;
    }

    public boolean isQuick() {
        return quick;
    }
    public void setQuick(boolean quick) {
        this.quick = quick;
    }

    public boolean isIgnore() {
        return ignore;
    }
    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public OrderByClause getOrderByClause() {
        return orderByClause;
    }
    public void setOrderByClause(OrderByClause orderByClause) {
        this.orderByClause = orderByClause;
    }

    public List<TableReferenceClause> getUsing() {
        return using;
    }
    public void setUsing(List<TableReferenceClause> using) {
        this.using = using;
    }

    

    public String getName() 
    {
        return deleteSource;
    }

    public void setLimitClause(LimitClause limitClause)
    {
        this.limitClause = limitClause;
    }
    public LimitClause getLimitClause()
    {
        return limitClause;
    }
    
    public void setTables(List<TableReferenceClause> tables)
    {
        this.tables = tables;
    }
    public List<TableReferenceClause> getTables()
    {
        return tables;
    }

    public void setFromClause(FromClause fromClause)
    {
        this.fromClause = fromClause;
    }
    
    @Override
    public FromClause getFromClause()
    {
        return fromClause;
    }

    public void setWhereClause(WhereClause whereClause)
    {
        this.whereClause = whereClause;
    }
    @Override
    public WhereClause getWhereClause()
    {
        return whereClause;
    }
    
}
