
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class LimitClause implements QueryClause
{
    private final String value;
    
    public LimitClause(final String value)
    {
        this.value = value;
    }

    public String getSource()
    {
        return value;
    }
    
    
}
