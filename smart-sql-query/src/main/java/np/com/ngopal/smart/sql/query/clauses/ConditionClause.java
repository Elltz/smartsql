
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ConditionClause implements ConditionElementReferenceClause
{
    public enum ConditionExpression
    {
        AND,
        OR,
        IN,
        NOTIN,
        EQUAL,
        NOTEQUAL,
        GREATER,
        GREATEREQUAL,
        LESS,
        LESSEQUAL,
        IS,
        ISNOT,
        NONE,
        UNKNOWN;        
        
        public static ConditionExpression valueOfString(final String type)
        {
            if (type != null)
            {
                switch(type.toLowerCase())
                {
                    case "and":
                        return AND;
                    case "or":
                        return OR;
                    case "in":
                        return IN;
                    case "not in":
                        return NOTIN;
                    case "!=":
                        return NOTEQUAL;
                    case ">=":
                        return GREATEREQUAL;
                    case "=":
                        return EQUAL;                    
                    case "<=":
                        return LESSEQUAL;    
                    case ">":
                        return GREATER;                    
                    case "<":
                        return LESS;                    
                    case "is":
                        return IS;
                    case "is not":
                        return ISNOT;
                        
                    default:
                        return UNKNOWN;
                }
            }
            
            return NONE;
        }
    }
    
    
    private ConditionElementReferenceClause leftCondition;
    private ConditionElementReferenceClause rightCondition;
    
    private ConditionExpression expression = ConditionExpression.NONE;
    
    
    private final String source;
    
    public ConditionClause(final String source)
    {
        this.source = source;
    }

    public String getSource()
    {
        return source;
    }

    
    public ConditionElementReferenceClause getLeftCondition()
    {
        return leftCondition;
    }
    public void setLeftCondition(ConditionElementReferenceClause leftCondition)
    {
        this.leftCondition = leftCondition;
    }

    public ConditionElementReferenceClause getRightCondition()
    {
        return rightCondition;
    }
    public void setRightCondition(ConditionElementReferenceClause rightCondition)
    {
        this.rightCondition = rightCondition;
    }

    public ConditionExpression getExpression()
    {
        return expression;
    }
    public void setExpression(ConditionExpression expression)
    {
        this.expression = expression;
    }
    
    
    
}
