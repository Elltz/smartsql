
package np.com.ngopal.smart.sql.query.clauses;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class FromClause implements QueryClause
{
    private List<TableReferenceClause> tables;
    private String source;
    
    public FromClause(String source)
    {
        this.source = source;
    }

    public List<TableReferenceClause> getTables()
    {
        return tables;
    }
    public void setTables(List<TableReferenceClause> tables)
    {
        this.tables = tables;
    }
    
    public String getSource() 
    {
        return source;
    }
    
}
