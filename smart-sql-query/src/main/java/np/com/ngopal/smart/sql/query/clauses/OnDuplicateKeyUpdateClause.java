
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class OnDuplicateKeyUpdateClause implements QueryClause
{
    private String condition;
    
    public OnDuplicateKeyUpdateClause()
    {
    }

    public String getCondition()
    {
        return condition;
    }
    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    
    
}
