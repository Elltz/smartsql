package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class LoadDataCommand extends AbstractCommand {

    private final String source;
    private String definition;
    
    public LoadDataCommand(String source) {
        this.source = source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }    

    public String getSource() {
        return source;
    }
    
    
}
