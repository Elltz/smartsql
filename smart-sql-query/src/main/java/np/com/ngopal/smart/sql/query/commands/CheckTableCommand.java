package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class CheckTableCommand extends AbstractCommand {

    private final String source;
    
    private String tableName;
    
    
    public CheckTableCommand(String source) {
        this.source = source;
    }

    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }  
    
    
    

    public String getSource() {
        return source;
    }
    
    
}
