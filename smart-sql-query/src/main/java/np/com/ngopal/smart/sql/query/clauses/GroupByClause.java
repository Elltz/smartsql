
package np.com.ngopal.smart.sql.query.clauses;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class GroupByClause implements QueryClause
{
    private List<ColumnReferenceClause> columns;
    
    private boolean withRollup = false;
    private String source;
    public GroupByClause(String source)
    {
        this.source = source;
    }
    
    public String getSource() 
    {
        return source;
    }

    public boolean isWithRollup() {
        return withRollup;
    }
    public void setWithRollup(boolean withRollup) {
        this.withRollup = withRollup;
    }    
    

    public List<ColumnReferenceClause> getColumns()
    {
        return columns;
    }
    public void setColumns(List<ColumnReferenceClause> columns)
    {
        this.columns = columns;
    }
    
    
}
