package np.com.ngopal.smart.sql.query.exceptions;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class TemplateException extends Exception
{
    public TemplateException(final String message)
    {
        super(message);
    }
    
    public TemplateException(final String message, final Throwable ex)
    {
        super(message, ex);
    }
}
