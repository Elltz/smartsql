/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.query.templates;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryTemplate
{
    private final String template;
    private final String md5Sum;
    
    public QueryTemplate(final String template, final String md5sum)
    {
        this.template = template;
        this.md5Sum = md5sum;
    }
    
    public String getTemplate()
    {
        return template;
    }

    public String getMd5Sum() 
    {
        return md5Sum;
    }
}
