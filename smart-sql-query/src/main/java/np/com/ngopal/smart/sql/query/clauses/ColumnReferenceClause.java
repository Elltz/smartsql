
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface ColumnReferenceClause extends QueryClause
{
    String getAlias();
    void setAlias(String alias);
    
    boolean isAs();
    void setAs(boolean as);
    
    boolean isAsc();
    void setAsc(boolean asc);
    
    void setHasAsc(boolean hasAsc);
    boolean isHasAsc();
    
    String getName();
    
    boolean isSimple();
}
