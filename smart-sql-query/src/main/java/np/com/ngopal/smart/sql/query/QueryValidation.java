package np.com.ngopal.smart.sql.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import np.com.ngopal.smart.sql.query.clauses.AlterTableKey;
import np.com.ngopal.smart.sql.query.clauses.AlterTableSpecification;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.CreateTableColumnDefinition;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.clauses.IntoTableClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.exceptions.ValidationException;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.RenameTableClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;
import np.com.ngopal.smart.sql.query.commands.AbstractCommand;
import np.com.ngopal.smart.sql.query.commands.AlterCommand;
import np.com.ngopal.smart.sql.query.commands.AlterTableCommand;
import np.com.ngopal.smart.sql.query.commands.AlterTableSpaceCommand;
import np.com.ngopal.smart.sql.query.commands.CallCommand;
import np.com.ngopal.smart.sql.query.commands.CheckTableCommand;
import np.com.ngopal.smart.sql.query.commands.CreateDatabaseCommand;
import np.com.ngopal.smart.sql.query.commands.CreateTableCommand;
import np.com.ngopal.smart.sql.query.commands.CreateUserCommand;
import np.com.ngopal.smart.sql.query.commands.CreateViewCommand;
import np.com.ngopal.smart.sql.query.commands.DeclareCommand;
import np.com.ngopal.smart.sql.query.commands.DeleteCommand;
import np.com.ngopal.smart.sql.query.commands.DropCommand;
import np.com.ngopal.smart.sql.query.commands.FlushCommand;
import np.com.ngopal.smart.sql.query.commands.GrantCommand;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.query.commands.LoadDataCommand;
import np.com.ngopal.smart.sql.query.commands.LockTableCommand;
import np.com.ngopal.smart.sql.query.commands.RenameCommand;
import np.com.ngopal.smart.sql.query.commands.ReplaceCommand;
import np.com.ngopal.smart.sql.query.commands.ReturnCommand;
import np.com.ngopal.smart.sql.query.commands.RevokeCommand;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.query.commands.SetCommand;
import np.com.ngopal.smart.sql.query.commands.ShowCommand;
import np.com.ngopal.smart.sql.query.commands.StartCommand;
import np.com.ngopal.smart.sql.query.commands.TruncateCommand;
import np.com.ngopal.smart.sql.query.commands.UpdateCommand;
import np.com.ngopal.smart.sql.query.utils.Parser;
import org.apache.commons.lang.StringUtils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryValidation {
    
    private static final char CHAR_SPACE        = ' ';
    private static final char CHAR_COLUMN_QUOTE = '`';
    private static final char CHAR_QUOTE        = '\'';
    
    private static final Pattern SUB_SELECT__PATTERN        =  Pattern.compile("(\\(\\s*\\bSELECT\\b.*\\s*\\))", Pattern.CASE_INSENSITIVE);
    
    private static final String DATA_TYPE__BIT              =  "(\\bBIT\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__BOOLEAN          =  "(\\bBOOLEAN\\b)";
    private static final String DATA_TYPE__BOOL             =  "(\\bBOOL\\b)";
    
    private static final String SIMPLE_CONDITION__PATTERN   =  "\\<>|>=|<=|\\!=|=|>|<|\\bis\\s+not\\b|\\bis\\b";
    
    
    private static final String DATA_TYPE__TINYINT          =  "(\\bTINYINT\\b(\\s*\\(\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__SMALLINT         =  "(\\bSMALLINT\\b(\\s*\\(\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__MEDIUMINT        =  "(\\bMEDIUMINT\\b(\\s*\\(\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__INT              =  "(\\bINT\\b(\\s*\\(\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__INTEGER          =  "(\\bINTEGER\\b(\\s*\\(\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__BIGINT           =  "(\\bBIGINT\\b(\\s*\\(\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    
    private static final String DATA_TYPE__REAL             =  "(\\bREAL\\b(\\s*\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__DOUBLE           =  "(\\bDOUBLE\\b(\\s*\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__FLOAT            =  "(\\bFLOAT\\b(\\s*\\(\\s*\\d+\\s*,\\s*\\d+\\s*\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    
    private static final String DATA_TYPE__DECIMAL          =  "(\\bDECIMAL\\b(\\s*\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    private static final String DATA_TYPE__NUMERIC          =  "(\\bNUMERIC\\b(\\s*\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?(((\\s+\\bUNSIGNED\\b)?(\\s+\\bZEROFILL\\b)?)|((\\s+\\bZEROFILL\\b)?(\\s+\\bUNSIGNED\\b)?)))";
    
    private static final String DATA_TYPE__DATE             =  "(\\bDATE\\b)";
    private static final String DATA_TYPE__YEAR             =  "(\\bYEAR\\b(\\s*\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\))?)";
    
    private static final String DATA_TYPE__TIME             =  "(\\bTIME\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__TIMESTAMP        =  "(\\bTIMESTAMP\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__DATETIME         =  "(\\bDATETIME\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    
    private static final String DATA_TYPE__CHAR             =  "(\\bCHAR\\b(\\s*\\(\\s*\\d+\\s*\\))?(\\s+\\bBINARY\\b)?(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    private static final String DATA_TYPE__VARCHAR          =  "(\\bVARCHAR\\b\\s*\\(\\s*\\d+\\s*\\)(\\s+\\bBINARY\\b)?(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    
    private static final String DATA_TYPE__BINARY           =  "(\\bBINARY\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__VARBINARY        =  "(\\bVARBINARY\\b\\s*\\(\\s*\\d+\\s*\\))";
    
    private static final String DATA_TYPE__TINYBLOB         =  "(\\bTINYBLOB\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__BLOB             =  "(\\bBLOB\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__MEDIUMBLOB       =  "(\\bMEDIUMBLOB\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    private static final String DATA_TYPE__LONGBLOB         =  "(\\bLONGBLOB\\b(\\s*\\(\\s*\\d+\\s*\\))?)";
    
    private static final String DATA_TYPE__TINYTEXT         =  "(\\bTINYTEXT\\b(\\s*\\(\\s*\\d+\\s*\\))?(\\s+\\bBINARY\\b)?(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    private static final String DATA_TYPE__TEXT             =  "(\\bTEXT\\b(\\s*\\(\\s*\\d+\\s*\\))?(\\s+\\bBINARY\\b)?(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    private static final String DATA_TYPE__MEDIUMTEXT       =  "(\\bMEDIUMTEXT\\b(\\s*\\(\\s*\\d+\\s*\\))?(\\s+\\bBINARY\\b)?(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    private static final String DATA_TYPE__LONGTEXT         =  "(\\bLONGTEXT\\b(\\s*\\(\\s*\\d+\\s*\\))?(\\s+\\bBINARY\\b)?(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
 
    private static final String DATA_TYPE__ENUM             =  "(\\bENUM\\b\\s*\\(\\s*([\\w\\.]+|('.*?')|(\".*?\"))\\s*(,\\s*([\\w\\.]+|('.*?')|(\".*?\"))\\s*)*\\)(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    private static final String DATA_TYPE__SET              =  "(\\bSET\\b\\s*\\(\\s*([\\w\\.]+|('.*?')|(\".*?\"))\\s*(,\\s*([\\w\\.]+|('.*?')|(\".*?\"))\\s*)*\\)(\\s+\\bCHARACTER\\b\\s+\\bSET\\b\\s+\\w+)?(\\s+\\bCOLLATE\\b\\s+\\w+)?)";
    
    private static final String DATA_TYPE__GEOMETRY         =  "(\\bGEOMETRY\\b)";
    private static final String DATA_TYPE__POINT            =  "(\\bPOINT\\b)";
    private static final String DATA_TYPE__LINESTRING       =  "(\\bLINESTRING\\b)";
    private static final String DATA_TYPE__POLYGON          =  "(\\bPOLYGON\\b)";
    
    private static final String DATA_TYPE__MULTIPOINT       =  "(\\bMULTIPOINT\\b)";
    private static final String DATA_TYPE__MULTILINESTRING  =  "(\\bMULTILINESTRING\\b)";
    private static final String DATA_TYPE__MULTIPOLYGON     =  "(\\bMULTIPOLYGON\\b)";
    private static final String DATA_TYPE__GEOMETRYCOLLECTION =  "(\\bGEOMETRYCOLLECTION\\b)";
    
    private static final String DATA_TYPE__JSON             =  "(\\bJSON\\b)";
    
//    [NOT NULL | NULL] [DEFAULT default_value]
//      [AUTO_INCREMENT] [UNIQUE [KEY] | [PRIMARY] KEY]
//      [COMMENT 'string']
//      [COLUMN_FORMAT {FIXED|DYNAMIC|DEFAULT}]
//      [STORAGE {DISK|MEMORY|DEFAULT}]
//      [reference_definition]
    
//    REFERENCES tbl_name (index_col_name,...)
//      [MATCH FULL | MATCH PARTIAL | MATCH SIMPLE]
//      [ON DELETE reference_option]
//      [ON UPDATE reference_option]
    
//    RESTRICT | CASCADE | SET NULL | NO ACTION
    
    private static final String ON_DELETE = "\\s*\\bON\\b\\s+\\bDELETE\\b\\s+((\\bRESTRICT\\b)|(\\bCASCADE\\b)|(\\bSET\\b\\s+\\bNULL\\b)|(\\bNO\\b\\s+\\bACTION\\b)|(.*))";
    private static final String ON_UPDATE = "\\s*\\bON\\b\\s+\\bUPDATE\\b\\s+((\\bRESTRICT\\b)|(\\bCASCADE\\b)|(\\bSET\\b\\s+\\bNULL\\b)|(\\bNO\\b\\s+\\bACTION\\b)|(.*))";
    
    private static final String DATA_TYPE_DEFINITION_0      =  
            "\\s*((\\bNOT\\b\\s+)?\\bNULL\\b)?"
            + "(\\s*\\bDEFAULT\\b\\s+([\\w\\.,\\)\\(\\+\\-]+|('.*?')|(\".*?\")))?"
            + "(\\s*\\bAUTO_INCREMENT\\b)?"
            + "((\\s*\\bUNIQUE\\b(\\s+\\bKEY\\b)?)|(\\s*(\\bPRIMARY\\b\\s+)?\\bKEY\\b))?"
            + "(\\s*\\bCOMMENT\\b\\s+(('.*?')|(\".*?\")))?"
            + "(\\s*\\bCOLUMN_FORMAT\\b\\s+((\\bFIXED\\b)|(\\bDYNAMIC\\b)|(\\bDEFAULT\\b)))?"
            + "(\\s*\\bSTORAGE\\b\\s+((\\bDISK\\b)|(\\bMEMORY\\b)|(\\bDEFAULT\\b)))?"
            + "(\\s*\\bREFERENCES\\b\\s+([\\w\\.]+|('.*?')|(\".*?\"))\\s*\\(.*?\\)(\\s*\\bMATCH\\b\\s+((\\bFULL\\b)|(\\bPARTIAL\\b)|(\\bSIMPLE\\b)))?"
            + "((" + ON_DELETE + ")?(" + ON_UPDATE + ")?)|((" + ON_UPDATE + ")?(" + ON_DELETE + ")?))";
      
//    [GENERATED ALWAYS] AS (expression)
//      [VIRTUAL | STORED] [UNIQUE [KEY]] [COMMENT comment]
//      [NOT NULL | NULL] [[PRIMARY] KEY]
    private static final String DATA_TYPE_DEFINITION_1      = 
            "\\s*(\\bGENERATED\\b\\s+\\bALWAYS\\b\\s+)?" +
            "\\bAS\\b\\s*\\(.+?\\)" +
            "((\\s*\\bVIRTUAL\\b)|(\\s*\\bSTORED\\b))?" +
            "(\\s*\\bUNIQUE\\b(\\s*\\bKEY\\b)?)?" +
            "(\\s*\\bCOMMENT\\b\\s+(('.*?')|(\".*?\")))?" +
            "(\\s*(\\bNOT\\b\\s+)?\\bNULL\\b)?" +
            "(\\s*(\\bPRIMARY\\b\\s+)?\\bKEY\\b)?";
            
    private static final Pattern DATA_TYPE__PATTERN         =  Pattern.compile(
            "(" + 
            DATA_TYPE__BIT + "|" +
            DATA_TYPE__BOOLEAN + "|" +
            DATA_TYPE__BOOL + "|" +
                    
            DATA_TYPE__TINYINT + "|" +
            DATA_TYPE__SMALLINT + "|" +
            DATA_TYPE__MEDIUMINT + "|" +
            DATA_TYPE__INT + "|" +
            DATA_TYPE__INTEGER + "|" +
            DATA_TYPE__BIGINT + "|" +
                    
            DATA_TYPE__REAL + "|" +
            DATA_TYPE__DOUBLE + "|" +
            DATA_TYPE__FLOAT + "|" +
                    
            DATA_TYPE__DECIMAL + "|" +
            DATA_TYPE__NUMERIC + "|" +
                    
            DATA_TYPE__DATE + "|" +
            DATA_TYPE__YEAR + "|" +
            
            DATA_TYPE__TIME + "|" +
            DATA_TYPE__TIMESTAMP + "|" +
            DATA_TYPE__DATETIME + "|" +
                    
            DATA_TYPE__CHAR + "|" +                    
            DATA_TYPE__VARCHAR + "|" +
                    
            DATA_TYPE__BINARY + "|" + 
            DATA_TYPE__VARBINARY + "|" + 
                    
            DATA_TYPE__TINYBLOB + "|" + 
            DATA_TYPE__BLOB + "|" + 
            DATA_TYPE__MEDIUMBLOB + "|" + 
            DATA_TYPE__LONGBLOB + "|" + 
                    
            DATA_TYPE__TINYTEXT + "|" + 
            DATA_TYPE__TEXT + "|" + 
            DATA_TYPE__MEDIUMTEXT + "|" + 
            DATA_TYPE__LONGTEXT + "|" +
                    
            DATA_TYPE__ENUM + "|" + 
            DATA_TYPE__SET + "|" + 
                    
            DATA_TYPE__GEOMETRY + "|" +
            DATA_TYPE__POINT + "|" +
            DATA_TYPE__LINESTRING + "|" +
            DATA_TYPE__POLYGON + "|" +
                                
            DATA_TYPE__MULTIPOINT + "|" +
            DATA_TYPE__MULTILINESTRING + "|" +
            DATA_TYPE__MULTIPOLYGON + "|" +
            DATA_TYPE__GEOMETRYCOLLECTION + "|" +
                    
            DATA_TYPE__JSON + ")" +
                    
            "((" + DATA_TYPE_DEFINITION_0 + ")|(" + DATA_TYPE_DEFINITION_1 + "))(\\s+(\\bNOT\\b\\s+)?\\bNULL\\b)?"
                    
            , Pattern.CASE_INSENSITIVE);
    
    public final static String ERROR_MEESAGE__R0            = "ErrorMessage: Syntax error ‘%s’ (identifier) is not a valid input at this position.";
    public final static String ERROR_MEESAGE__JOIN_R0       = "ErrorMessage: Syntax error (Unexpected end of the query)";
    public final static String ERROR_MEESAGE__JOIN_R1       = "ErrorMessage: Syntax error (Join condition missed)";
    public final static String ERROR_MEESAGE__JOIN_R2       = "ErrorMessage: Syntax error (Join condition wrong)";
    public final static String ERROR_MEESAGE__SELECT_R1     = "ErrorMessage: Syntax error.";
    public final static String ERROR_MEESAGE__SELECT_R1_0   = "ErrorMessage: Syntax error (unexpected start)";
    public final static String ERROR_MEESAGE__SELECT_R2     = "ErrorMessage: The query missing \"FROM\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R3     = "ErrorMessage: The query have only \"HAVING\" clause without \"GROUP BY\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R5     = "ErrorMessage: The \"%s\" clause should come before the \"%s\" clause (%d < %d). (Need to replace with clause name on \"%s\" and \"%s\".)";
    public final static String ERROR_MEESAGE__SELECT_R6     = "ErrorMessage: The Query missed columns after \"SELECT\" command and before \"FROM\" clause.";    
    public final static String ERROR_MEESAGE__SELECT_R7     = "ErrorMessage: Incorrect usage of \"%s\" and \"%s\".";
    public final static String ERROR_MEESAGE__SELECT_R8     = "ErrorMessage: The Query missed tables after \"FROM\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R9     = "ErrorMessage: The Query missed condition after \"WHERE\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R10    = "ErrorMessage: The Query missed columns after \"GROUP BY\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R11    = "ErrorMessage: The Query missed condition after \"HAVING\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R12    = "ErrorMessage: The Query missed columns after \"ORDER BY\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R13    = "ErrorMessage: Incorrect usage of \"LIMIT\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R14    = "ErrorMessage: Incorrect usage of \"PROCEDURE\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R15    = "ErrorMessage: Incorrect usage of \"INTO\" clause.";
    public final static String ERROR_MEESAGE__SELECT_R16    = "ErrorMessage: Unexpected %s (identifier)";
    public final static String ERROR_MEESAGE__SELECT_R17    = "ErrorMessage: Syntax error : '%s' (identifier) is not a valid input in this position";

    public final static String ERROR_MEESAGE__UPDATE_R1     = "ErrorMessage: Error query miss the SET clause.";
    public final static String ERROR_MEESAGE__UPDATE_R2     = "ErrorMessage: The \"%s\" clause come after \"%s\" clause.";
    public final static String ERROR_MEESAGE__UPDATE_R3     = "ErrorMessage: LIMIT clause should come only with single tables updates.";
    
    public final static String ERROR_MEESAGE__INSERT_R1     = "ErrorMessage: INSERT command syntax error.";
    public final static String ERROR_MEESAGE__INSERT_R2     = "ErrorMessage: Column count does not match a row value count.";
    public final static String ERROR_MEESAGE__INSERT_R3     = "ErrorMessage: Incorrect usage of \"%s\" and \"%s\".";
    public final static String ERROR_MEESAGE__INSERT_R4     = "ErrorMessage: Syntax error: the \"%s\" keyword(s) is invalid in the INSERT command.";
    
    public final static String ERROR_MEESAGE__REPLACE_R1    = "ErrorMessage: REPLACE command syntax error.";
    public final static String ERROR_MEESAGE__REPLACE_R2    = "ErrorMessage: Column count does not match a row value count.";
    public final static String ERROR_MEESAGE__REPLACE_R3    = "ErrorMessage: Incorrect usage of \"%s\" and \"%s\".";
    public final static String ERROR_MEESAGE__REPLACE_R4    = "ErrorMessage: Syntax error: the \"%s\" keyword(s) is invalid in the REPLACE command.";    
    
    public final static String ERROR_MEESAGE__DELETE_R1     = "ErrorMessage: DELETE missed the from clause.";
    public final static String ERROR_MEESAGE__DELETE_R2     = "ErrorMessage: The \"%s\" clause should before \"%s\" clause.";
    public final static String ERROR_MEESAGE__DELETE_R3     = "ErrorMessage: LIMIT clause should come only with single tables delete.";
    public final static String ERROR_MEESAGE__DELETE_R4     = "ErrorMessage: DELETE command syntax error.";
    
    public final static String ERROR_MEESAGE__RENAME_R1     = "ErrorMessage: Missed keyword %s in the RENAME.";
    public final static String ERROR_MEESAGE__RENAME_R2     = "ErrorMessage: The RENAME %s should come before %s.";
    
    public final static String ERROR_MEESAGE__DROP_R1       = "ErrorMessage: Syntax error in DROP command.";
    
    public final static String ERROR_MEESAGE__TRUNCATE_R1   = "ErrorMessage: Syntax error in TRUNCATE command.";
    
    public final static String ERROR_MEESAGE__CREATE_T_R1   = "ErrorMessage: Syntax error in CREATE TABLE / CREATE TEMPORARY TABLE.";
    public final static String ERROR_MEESAGE__CREATE_T_R2_1 = "ErrorMessage: Table name is not valid.";
    public final static String ERROR_MEESAGE__CREATE_T_R2_2 = "ErrorMessage: Column name '%s' is not valid.";
    public final static String ERROR_MEESAGE__CREATE_T_R3   = "ErrorMessage: Syntax error at column '%s' definition.";
    
    public final static String ERROR_MEESAGE__ALTER_T_R1    = "ErrorMessage: Syntax error in ALTER command.";
    
    public final static String ERROR_MEESAGE__ALTER_R1      = "ErrorMessage: Syntax error in ALTER command.";
    
    public final static String ERROR_MEESAGE__SHOW_R1       = "ErrorMessage: Syntax error in SHOW command.";
    
    public final static String ERROR_MEESAGE__START_R0      = "ErrorMessage: Syntax error (Unexpected start of the query using START).";
    
    public final static String ERROR_MEESAGE__SET_R0        = "ErrorMessage: Syntax error SET clause.";
    
        
    private static final List<String> SELECT_KEYWORDS = Arrays.asList(new String[] {
        "ALL",
        "DISTINCT",
        "DISTINCTROW",
        "HIGH_PRIORITY",
        "MAX_STATEMENT_TIME",
        "STRAIGHT_JOIN",
        "SQL_SMALL_RESULT",
        "SQL_BIG_RESULT",
        "SQL_BUFFER_RESULT",
        "SQL_CACHE",
        "SQL_NO_CACHE",
        "SQL_CALC_FOUND_ROWS"
    });
    
    /**
     *  At now validating only select query
     * @NOTE Queries with length greater then 5000 will ignored
     * for performance reason
     * @TODO Fix this
     * @param query query for validate
     * @return empty query
     * @throws ValidationException exception in validation fail
     */
    public static boolean validateQuery(String query) throws ValidationException
    {
        if (query != null && query.length() < 5000) {
            query = QueryFormat.replaceComment(query);            
            query = query.trim().toLowerCase();

            if (!query.isEmpty() && !";".equals(query)) {
            
                try {
                    final QueryClause queryCommand = QueryFormat.parseQuery(query);
                    if (queryCommand instanceof SelectCommand)
                    {
                        validateSelect((SelectCommand)queryCommand);
                    }
                    else if (queryCommand instanceof UpdateCommand)
                    {
                        validateUpdate((UpdateCommand)queryCommand);
                    }
                    else if (queryCommand instanceof InsertCommand)
                    {
                        validateInsert((InsertCommand)queryCommand);
                    }
                    else if (queryCommand instanceof ReplaceCommand)
                    {
                        validateReplace((ReplaceCommand)queryCommand);
                    }
                    else if (queryCommand instanceof DeleteCommand)
                    {
                        validateDelete((DeleteCommand)queryCommand);
                    }
                    else if (queryCommand instanceof RenameCommand)
                    {
                        validateRename((RenameCommand)queryCommand);
                    }
                    else if (queryCommand instanceof DropCommand)
                    {
                        validateDrop((DropCommand)queryCommand);
                    }
                    else if (queryCommand instanceof TruncateCommand)
                    {
                        validateTruncate((TruncateCommand)queryCommand);
                    }
                    else if (queryCommand instanceof CreateTableCommand)
                    {
                        validateCreateTable((CreateTableCommand)queryCommand);
                    }
                    else if (queryCommand instanceof AlterTableCommand)
                    {
                        validateAlterTable((AlterTableCommand)queryCommand);
                    }
                    else if (queryCommand instanceof AlterTableSpaceCommand)
                    {
                        validateAlterTableSpace((AlterTableSpaceCommand)queryCommand);
                    }
                    else if (queryCommand instanceof AlterCommand)
                    {
                        validateAlter((AlterCommand)queryCommand);
                    }
                    else if (queryCommand instanceof StartCommand)
                    {
                        validateStart((StartCommand)queryCommand);
                    }
                    else if (queryCommand instanceof ShowCommand)
                    {
                        validateShow((ShowCommand)queryCommand);
                    }
                    else if (queryCommand instanceof SetCommand)
                    {
                        validateSet((SetCommand)queryCommand);
                    }
                    else if (queryCommand instanceof ReturnCommand)
                    {
                        validateReturn((ReturnCommand)queryCommand);
                    }
                    else if (queryCommand instanceof DeclareCommand)
                    {
                        validateDeclare((DeclareCommand)queryCommand);
                    }
                    else if (queryCommand instanceof CallCommand)
                    {
                        validateCall((CallCommand)queryCommand);
                    }
                    else if (queryCommand instanceof CreateUserCommand)
                    {
                        validateCreateUser((CreateUserCommand)queryCommand);
                    }
                    else if (queryCommand instanceof CreateDatabaseCommand)
                    {
                        validateCreateDatabase((CreateDatabaseCommand)queryCommand);
                    }
                    else if (queryCommand instanceof CreateViewCommand)
                    {
                        validateCreateView((CreateViewCommand)queryCommand);
                    }
                    else if (queryCommand instanceof GrantCommand)
                    {
                        validateGrant((GrantCommand)queryCommand);
                    }
                    else if (queryCommand instanceof FlushCommand)
                    {
                        validateFlush((FlushCommand)queryCommand);
                    }
                    else if (queryCommand instanceof RevokeCommand)
                    {
                        validateRevoke((RevokeCommand)queryCommand);
                    }
                    else if (queryCommand instanceof CheckTableCommand)
                    {
                        validateCheckTable((CheckTableCommand)queryCommand);
                    }
                    else if (queryCommand instanceof LockTableCommand)
                    {
                        validateLockTable((LockTableCommand)queryCommand);
                    }
                    else if (queryCommand instanceof LoadDataCommand)
                    {
                        validateLoadData((LoadDataCommand)queryCommand);
                    }
                    else
                    {
                        throw new ValidationException(String.format(ERROR_MEESAGE__R0, query.trim().split(" ")[0]));
                    }
                } catch (Throwable th) {
                    if (th instanceof ValidationException) {
                        throw th;
                    } else {
                        throw new ValidationException(String.format(ERROR_MEESAGE__R0, query.trim().split(" ")[0]));
                    }
                }
                
                return false;
            }
        }
        
        return true;
    }
    
    /**
     *  At now validating only select query
     * @param selectCommand select command for validate
     * @throws ValidationException exception in validation fail
     */
    public static void validateQuery(final SelectCommand selectCommand) throws ValidationException
    {
        validateSelect(selectCommand);
    }
    
    
    private static void validateSelect(final SelectCommand selectCommand) throws ValidationException
    {
        if (selectCommand.getUnionSelectCommands() != null && !selectCommand.getUnionSelectCommands().isEmpty()) {
            for (SelectCommand s: selectCommand.getUnionSelectCommands()) {
                validateSelect(s);
            }
        } else {
            validateSelectR5(selectCommand.getSimpleSource());

            validateSelectColumns(selectCommand);
            
            validateSelectR6(selectCommand);            
            validateSelectR2(selectCommand);
            validateSelectR3(selectCommand);

            try {
                validateFrom(selectCommand.getFromClause());
            } catch (ValidationException ex) {
                
                if (ERROR_MEESAGE__JOIN_R0.equals(ex.getMessage()) && 
                    (selectCommand.getWhereClause() != null ||
                        selectCommand.getGroupByClause() != null ||
                        selectCommand.getHavingClause() != null ||
                        selectCommand.getOrderByClause() != null ||
                        selectCommand.getLimitClause() != null)
                    ) {
                    throw new ValidationException(ERROR_MEESAGE__JOIN_R1);
                    
                } else {
                    throw ex;
                }
            }
            validateWhere(selectCommand.getWhereClause());
            validateGroupBy(selectCommand.getGroupByClause());
            validateHaving(selectCommand.getHavingClause());
            validateOrderBy(selectCommand.getOrderByClause());
            
            validateLimit(selectCommand.getLimitClause());
            
            if (selectCommand.getProcedure() != null && !selectCommand.getProcedure().toLowerCase().trim().matches("\\w+\\(.*?\\)")) {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R14);
            }
            
            if (selectCommand.getInto() != null && 
                (selectCommand.getInto().trim().isEmpty() ||
                !selectCommand.getInto().toLowerCase().trim().matches("\\boutfile\\b\\s+@\\d\\d.*") &&
                !selectCommand.getInto().toLowerCase().trim().matches("\\bdumpfile\\b\\s+@\\d\\d") && 
                (!selectCommand.getInto().toLowerCase().trim().matches("\\w+(\\s*,\\s*\\w+)?") || 
                    indexOf(selectCommand.getInto(), "outfile") > -1 ||
                    indexOf(selectCommand.getInto(), "dumpfile") > -1 ))) {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R15);
            }
        
            
            checkAllSubQueries(selectCommand);
        }
    }
    
    private static void validateUpdate(final UpdateCommand updateCommand) throws ValidationException
    {
        validateUpdateR2(updateCommand.getName());
        
        validateUpdateR1(updateCommand);
        validateUpdateR3(updateCommand);

        validateLimitCount(updateCommand.getLimitClause());
        
        checkAllSubQueries(updateCommand);
    }
    
    private static void validateInsert(final InsertCommand insertCommand) throws ValidationException
    {
        int type = -1;
        if (insertCommand.getValuesClause() != null && insertCommand.getSetClause() == null && insertCommand.getSelectClause() == null) {
            type = 0;
        } else if (insertCommand.getValuesClause() == null && insertCommand.getSetClause() == null && insertCommand.getSelectClause() != null) {
            type = 1;
        } else if (insertCommand.getValuesClause() == null && insertCommand.getSetClause() != null && insertCommand.getSelectClause() == null) {
            type = 2;
        } 
        
        if (type == -1) {
            throw new ValidationException(ERROR_MEESAGE__INSERT_R1);
        }
        
        validateInsertR1(insertCommand, type);
        
        validateInsertR2(insertCommand, type);
        if (insertCommand.getSelectClause() != null) {
            validateSelect(insertCommand.getSelectClause());
        }
    }
    
    private static void validateReplace(final ReplaceCommand replaceCommand) throws ValidationException
    {        
        validateReplacetR1(replaceCommand, 0);        
        validateReplaceR2(replaceCommand, 0);
    }
    
    private static void validateDelete(final DeleteCommand deleteCommand) throws ValidationException
    {
        validateDeleteR2(deleteCommand.getName());
        
        validateDeleteR1(deleteCommand);
        validateDeleteR3(deleteCommand);
        
        validateDeleteR4(deleteCommand);

        validateLimitCount(deleteCommand.getLimitClause());
        validateOrderBy(deleteCommand.getOrderByClause());
        validateFrom(deleteCommand.getFromClause());
        
        List<TableReferenceClause> using = deleteCommand.getUsing();
        if (using != null) {
            for (TableReferenceClause t: using) {
                validateTable(t);
            }
        }
        checkAllSubQueries(deleteCommand);
    }
    
    
    private static void validateRename(final RenameCommand renameCommand) throws ValidationException
    {
        validateRenameR1(renameCommand);
        validateRenameR2(renameCommand.getSource());
    }
    
    
    private static void validateDrop(final DropCommand dropCommand) throws ValidationException
    {
        validateDropR1(dropCommand);
    }
    
    
    private static void validateTruncate(final TruncateCommand truncateCommand) throws ValidationException
    {
        validateTruncateR1(truncateCommand);
    }
    
    
    private static void validateCreateTable(final CreateTableCommand createTableCommand) throws ValidationException
    {
        validateCreateTableR1(createTableCommand.getSource());
        validateCreateTableR2(createTableCommand);
    }
    
    private static void validateAlterTable(final AlterTableCommand alterTableCommand) throws ValidationException
    {
        validateAlterTableR1(alterTableCommand);
    }
    
    private static void validateAlterTableSpace(final AlterTableSpaceCommand alterCommand) throws ValidationException
    {
        // now nothing
    }
    
    private static void validateAlter(final AlterCommand alterCommand) throws ValidationException
    {
        validateAlterR1(alterCommand);
    }
    
    private static void validateShow(final ShowCommand showCommand) throws ValidationException
    {
        validateShowR1(showCommand);
    }
    
    private static void validateSet(final SetCommand setCommand) throws ValidationException
    {
        validateSetR0(setCommand);
    }
    
    private static void validateReturn(final ReturnCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateDeclare(final DeclareCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateCall(final CallCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateCreateUser(final CreateUserCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateCreateDatabase(final CreateDatabaseCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateCreateView(final CreateViewCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateGrant(final GrantCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateFlush(final FlushCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateRevoke(final RevokeCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateCheckTable(final CheckTableCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateLockTable(final LockTableCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateLoadData(final LoadDataCommand command) throws ValidationException
    {
        // @TODO
    }
    
    private static void validateStart(final StartCommand startCommand) throws ValidationException
    {
        validateStartR0(startCommand);
    }
    
    
    private static void validateShowR1(final ShowCommand showCommand) throws ValidationException
    {
        String definition = showCommand.getDefinition();
        if (definition != null) {
            
            definition = definition.trim().replaceAll("\\s+", " ").toUpperCase();
            String data[] = definition.split(" ");
            switch (data[0]) {
                // SHOW {BINARY | MASTER} LOGS
                case "BINARY":
                    if (data.length != 2 || !"LOGS".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                // SHOW {BINARY | MASTER} LOGS
                // SHOW MASTER STATUS
                case "MASTER":
                    if (data.length != 2 || (!"LOGS".equals(data[1]) && !"STATUS".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;

                // SHOW BINLOG EVENTS [IN 'log_name'] [FROM pos] [LIMIT [offset,] row_count]
                case "BINLOG":
                    if (data.length < 2 || !"EVENTS".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    break;
                    
                // SHOW CHARACTER SET [like_or_where]
                case "CHARACTER":
                    if (data.length < 2 || !"SET".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW [FULL] COLUMNS FROM tbl_name [FROM db_name] [like_or_where]
                // SHOW [FULL] PROCESSLIST
                // SHOW [FULL] TABLES [FROM db_name] [like_or_where]
                case "FULL":
                    
                    if (data.length < 2 || (!"PROCESSLIST".equals(data[1]) && !"TABLES".equals(data[1]) && !"COLUMNS".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    if ("PROCESSLIST".equals(data[1]) && data.length != 2) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                                        
                    if ("COLUMNS".equals(data[1]) && (data.length < 4 || !"FROM".equals(data[2]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                case "COLUMNS":
                    if (data.length < 3 || !"FROM".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW CREATE DATABASE db_name
                // SHOW CREATE EVENT event_name
                // SHOW CREATE FUNCTION func_name
                // SHOW CREATE PROCEDURE proc_name
                // SHOW CREATE TABLE tbl_name
                // SHOW CREATE TRIGGER trigger_name
                // SHOW CREATE VIEW view_name
                case "CREATE":
                    if (data.length != 3 || 
                        (!"DATABASE".equals(data[1]) &&
                        !"EVENT".equals(data[1]) &&
                        !"FUNCTION".equals(data[1]) &&
                        !"PROCEDURE".equals(data[1]) &&
                        !"TABLE".equals(data[1]) &&
                        !"TRIGGER".equals(data[1]) &&
                        !"VIEW".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW ENGINE engine_name {STATUS | MUTEX}
                case "ENGINE":
                    if (data.length != 3 || (!"STATUS".equals(data[2]) && !"MUTEX".equals(data[2]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break; 
                    
                // SHOW [STORAGE] ENGINES
                case "STORAGE":
                    if (data.length != 1 || !"ENGINES".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW FUNCTION CODE func_name
                // SHOW FUNCTION STATUS [like_or_where]
                case "FUNCTION":
                    if (data.length < 2 || (!"CODE".equals(data[1]) && !"STATUS".equals(data[1]))) {                        
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    if ("CODE".equals(data[1]) && data.length != 3) {                        
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    break;
                    
                // SHOW GRANTS FOR user
                case "GRANTS":
                    if (data.length != 3 || !"FOR".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW INDEX FROM tbl_name [FROM db_name]
                case "INDEX":
                    if (data.length < 3 || !"FROM".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                
                // SHOW OPEN TABLES [FROM db_name] [like_or_where]
                case "OPEN":
                    if (data.length < 2 || !"TABLES".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                
                // SHOW PROCEDURE CODE proc_name
                // SHOW PROCEDURE STATUS [like_or_where]
                case "PROCEDURE":
                    
                    if (data.length < 2 || (!"STATUS".equals(data[1]) && !"CODE".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    if ("CODE".equals(data[1]) && data.length != 3) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    break;
                    
                // SHOW RELAYLOG EVENTS [IN 'log_name'] [FROM pos] [LIMIT [offset,] row_count]
                case "RELAYLOG":
                    if (data.length < 2 || !"EVENTS".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW SLAVE HOSTS
                // SHOW SLAVE STATUS [NONBLOCKING]
                case "SLAVE":
                    if (data.length < 2 || (!"HOSTS".equals(data[1]) && !"STATUS".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    
                    if ("HOSTS".equals(data[1]) && data.length != 2) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW TABLE STATUS [FROM db_name] [like_or_where]
                case "TABLE":
                    if (data.length < 2 || !"STATUS".equals(data[1])) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW [GLOBAL | SESSION] STATUS [like_or_where]
                // SHOW [GLOBAL | SESSION] VARIABLES [like_or_where]
                case "GLOBAL":
                    if (data.length < 2 || (!"VARIABLES".equals(data[1]) && !"STATUS".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW [GLOBAL | SESSION] STATUS [like_or_where]
                // SHOW [GLOBAL | SESSION] VARIABLES [like_or_where]
                case "SESSION":
                    if (data.length < 2 || (!"VARIABLES".equals(data[1]) && !"STATUS".equals(data[1]))) {
                        throw new ValidationException(ERROR_MEESAGE__SHOW_R1);
                    }
                    break;
                    
                // SHOW [FULL] TABLES [FROM db_name] [like_or_where]
                case "TABLES":
                // SHOW COLLATION [like_or_where]
                case "COLLATION":
                // SHOW DATABASES [like_or_where]
                case "DATABASES":
                // SHOW [STORAGE] ENGINES
                case "ENGINES":                
                // SHOW ERRORS [LIMIT [offset,] row_count]
                case "ERRORS":
                // SHOW EVENTS
                case "EVENTS":
                // SHOW PLUGINS
                case "PLUGINS":
                // SHOW PRIVILEGES
                case "PRIVILEGES":
                // SHOW PROFILES
                case "PROFILES":
                // SHOW WARNINGS [LIMIT [offset,] row_count]
                case "WARNINGS":
                // SHOW [FULL] PROCESSLIST
                case "PROCESSLIST":
                // SHOW PROFILE [types] [FOR QUERY n] [OFFSET n] [LIMIT n]
                case "PROFILE":
                // SHOW [GLOBAL | SESSION] STATUS [like_or_where]
                case "STATUS":
                // SHOW TRIGGERS [FROM db_name] [like_or_where]
                case "TRIGGERS":
                // SHOW [GLOBAL | SESSION] VARIABLES [like_or_where]
                case "VARIABLES":
                    // all ok
                    break;
                
                default: 
                    throw new ValidationException(ERROR_MEESAGE__SHOW_R1);

            }
        }
    }
    
    
    private static void validateAlterR1(final AlterCommand alterCommand) throws ValidationException
    {
        // need chaeck may be this is alter table or alter tabespace
        if (alterCommand.getDefinition() != null)
        {
            String definition = alterCommand.getDefinition().trim().replaceAll("\\s+", " ").toUpperCase();
            if (indexOf(definition, "TABLESPACE") < 0 && indexOf(definition, "TABLE") < 0 && indexOf(definition, "IGNORE TABLE") < 0
                    && indexOf(definition, "DATABASE") < 0 && indexOf(definition, "EVENT") < 0 && indexOf(definition, "FUNCTION") < 0
                    && indexOf(definition, "INSTANCE") < 0 && indexOf(definition, "LOGFILE") < 0 && indexOf(definition, "PROCEDURE") < 0
                    && indexOf(definition, "SERVER") < 0 && indexOf(definition, "VIEW") < 0) {
                throw new ValidationException(ERROR_MEESAGE__ALTER_R1);
            }
        }
    }
    
    private static void validateAlterTableR1(final AlterTableCommand alterTableCommand) throws ValidationException
    {
        if (alterTableCommand.getTableName() == null)
        {
            throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
        }
        
        if (alterTableCommand.getAlterSpecifications() != null) {
            for (AlterTableSpecification ats: alterTableCommand.getAlterSpecifications()) {
                AlterTableKey atk = ats.getAlterTableKey();
                String[] data = ats.getSpecification().trim().toUpperCase().split(" ");
                switch (ats.getType()) {
                    case "ADD":
                        if (atk.isEmpty()) {
                            throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                        }
                        break;
                    case "DROP":
                    case "CHANGE":
                    case "ALTER":
                    case "ALGORITHM":
                    case "LOCK":
                    case "MODIFY":
                    case "RENAME":                
                    case "CONVERT":                
                    case "DISCARD": 
                    case "IMPORT":
                    case "FORCE":
                    case "TRUNCATE":
                    case "COALESCE":
                    case "REORGANIZE":
                    case "EXCHANGE":
                    case "ANALYZE":
                    case "CHECK":
                    case "OPTIMIZE":
                    case "REBUILD":
                    case "REPAIR":
                    case "REMOVE":
                    case "UPGRADE":
                        // all ok
                        break;

                    case "DISABLE":
                    case "ENABLE":
                        if (data.length < 2 || !data[1].equals("KEYS")) {
                            throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                        }
                        break;

                    case "ORDER":
                        if (data.length < 2 || !data[1].equals("BY")) {
                            throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                        }
                        break;

                    case "CHARACTER":
                        if (data.length < 2 || !data[1].equals("SET")) {
                            throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                        }
                        break;
                    case "DEFAULT":
                        if (data.length < 3 || (!data[1].equals("CHARACTER") && !data[2].equals("SET"))) {
                            throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                        }
                        break;

                    default:
                        throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                }

                if (data.length == 1) {
                    throw new ValidationException(ERROR_MEESAGE__ALTER_T_R1);
                }
            }
        }
    }
    
    private static void validateCreateTableR1(String query) throws ValidationException
    {        
        query = query.replaceAll("\\s+", " ");
        
        final int temporaryIndex = query.indexOf("temporary");
        final int tableIndex = query.indexOf("table");
        final int ifNotExistsIndex = query.indexOf("if not exists");

        final Integer[] indexes = new Integer[]{temporaryIndex, tableIndex, ifNotExistsIndex};

        int index = temporaryIndex;
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                }
                else
                {
                    throw new ValidationException(ERROR_MEESAGE__CREATE_T_R1);
                }
            }
        }
    }
    
    private static void validateCreateTableR2(final CreateTableCommand createTableCommand) throws ValidationException
    {
        String tableName = createTableCommand.getTableName();
        if (tableName != null) {
            // table_name is any alphanumeric name with out starting with numeric letters and not more than 64 characters length. 
            // [0-9,a-z,A-Z$_] (basic Latin letters, digits 0-9, dollar, underscore).
            // Database, table, and column names cannot end with space characters.
            // The identifier quote character is the backtick (“`”) 
            
            checkName(tableName, ERROR_MEESAGE__CREATE_T_R2_1);
            
            // check columns
            List<CreateTableColumnDefinition> columns = createTableCommand.getColumns();
            if (columns != null && !columns.isEmpty()) {
                
                for (CreateTableColumnDefinition column: columns) {                    
                    switch (column.getType()) {
                        
                        case COLUMN:
                            checkName(column.getName(), String.format(ERROR_MEESAGE__CREATE_T_R2_2, column.getName()));
                            String dataType = column.getDefinition();
                            if (dataType != null) {
                                
                                Matcher typeMatcher = DATA_TYPE__PATTERN.matcher(dataType);
                                if (!typeMatcher.matches()) {
                                    throw new ValidationException(String.format(ERROR_MEESAGE__CREATE_T_R3, column.getName()));
                                }
                                
//                                //first word this must be a valid type
//                                String info[] = dataType.split(" ");
//                                checkDataType(info[0]);
//                                
                                
                            } else {
                                throw new ValidationException(ERROR_MEESAGE__CREATE_T_R3);
                            }
                            break;
                    }
                }
            }
        }
        else
        {
            throw new ValidationException(ERROR_MEESAGE__CREATE_T_R1);
        }
    }
    
    private static boolean checkDataType(String dataType) throws ValidationException {
        
        String length = "";
        // type can have size with "("
        int index = dataType.indexOf("(");
        if (index > -1) {
            length = dataType.substring(index);
            dataType = dataType.substring(0, index);
        }
        
        switch (dataType.trim().toUpperCase()) {
            case "BIT":
            case "BOOLEAN":
            case "TINYINT":
            case "SMALLINT":
            case "MEDIUMINT":
            case "INT":
            case "INTEGER":
            case "BIGINT":
            case "REAL":
            case "DOUBLE":
            case "FLOAT":
            case "DECIMAL":
            case "NUMERIC":
            case "DATE":
            case "TIME":
            case "TIMESTAMP":
            case "DATETIME":
            case "YEAR":
            case "CHAR":
            case "VARCHAR":
            case "BINARY":
            case "VARBINARY":
            case "TINYBLOB":
            case "BLOB":
            case "MEDIUMBLOB":
            case "LONGBLOB":
            case "TINYTEXT":
            case "TEXT":
            case "MEDIUMTEXT":
            case "LONGTEXT":
            case "ENUM":
            case "SET":
            case "JSON":
                return length.isEmpty();
                
            default:
                throw new ValidationException(ERROR_MEESAGE__CREATE_T_R3);
        }
    }
    
    private static void checkName(String name, String error) throws ValidationException {
        if (name.contains(".")) {
            String result[] = name.split("\\.");

            for (int i = 0; i < result.length - 1; i++) {
                String s = result[i].trim();
                
                boolean start = s.startsWith("`");
                boolean finish = s.endsWith("`");                
                if (start != finish) {
                    throw new ValidationException(error);
                }

                if (s.matches("\\d.*") || s.length() > 64) {
                    throw new ValidationException(error);
                }
            }

            // last is table
            name = result[result.length - 1].trim();

            boolean start = name.startsWith("`");
            boolean finish = name.endsWith("`");                
            if (start != finish) {
                throw new ValidationException(error);
            }

            name = name.replaceAll("`", "");
        } else {
            boolean start = name.startsWith("`");
            boolean finish = name.endsWith("`");                
            if (start != finish) {
                throw new ValidationException(error);
            }

            name = name.replaceAll("`", "");
        }


        if (name.startsWith(" ") || name.endsWith(" ") || name.matches("\\d.*") || name.length() > 64 || name.contains(" ")) {
            throw new ValidationException(error);
        }
    }
    
    
    private static void validateTruncateR1(final TruncateCommand truncateCommand) throws ValidationException
    {
        if (truncateCommand.getTableSection() != null && truncateCommand.getTableSection().trim().split(" ").length > 1)
        {
            throw new ValidationException(ERROR_MEESAGE__TRUNCATE_R1);
        }
    }
    
    
    private static void validateDropR1(DropCommand dropCommand) throws ValidationException
    {   
        String dropTarget = dropCommand.getDropTarget();
        
        if (dropTarget == null) {
            throw new ValidationException(ERROR_MEESAGE__DROP_R1);
        }
        
        dropTarget = dropTarget.toUpperCase();
        if (dropTarget.contains(" ")) {
            dropTarget = dropTarget.replaceAll("\\s+", " ");
        }        
        
        switch (dropTarget) {
            case "TABLESPACE":
            case "INDEX":
            case "LOGFILE GROUP":
                if (dropCommand.isIfExists()) {
                    throw new ValidationException(ERROR_MEESAGE__DROP_R1);
                }
        }
    }
    
    private static void validateRenameR1(RenameCommand renameCommand) throws ValidationException
    {        
        if (indexOf(renameCommand.getSource(), "table") == -1) {
            throw new ValidationException(String.format(ERROR_MEESAGE__RENAME_R1, "TABLE"));
        }
        
        if (renameCommand.getRenameTables() != null) {
            for (RenameTableClause t: renameCommand.getRenameTables()) {
                if (indexOf(renameCommand.getSource(), "to") == -1) {
                    throw new ValidationException(String.format(ERROR_MEESAGE__RENAME_R1, "TO"));
                }
            }
        }
    }
    
    private static void validateRenameR2(String query) throws ValidationException
    {        
        final int tableIndex = indexOf(query, "table");
        final int toIndex = indexOf(query, "to");

        final Integer[] indexes = new Integer[]{tableIndex, toIndex};
        final String[] clauses = new String[]{"TABLE", "TO"};

        int index = tableIndex;
        
        String lastClause = "TABLE";
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                    lastClause = clauses[i];
                }
                else
                {
                    throw new ValidationException(String.format(ERROR_MEESAGE__RENAME_R2, lastClause, clauses[i]));
                }
            }
        }
    }
    
    
    private static void validateDeleteR1(final DeleteCommand deleteCommand) throws ValidationException
    {
        if (deleteCommand.getFromClause() == null)
        {
            throw new ValidationException(ERROR_MEESAGE__DELETE_R1);
        }
    }
    
    private static void validateDeleteR3(final DeleteCommand deleteCommand) throws ValidationException
    {
        if (deleteCommand.getFromClause() != null && deleteCommand.getFromClause().getTables() != null && deleteCommand.getFromClause().getTables().size() > 1 && deleteCommand.getLimitClause() != null)
        {
            throw new ValidationException(ERROR_MEESAGE__DELETE_R3);
        }
    }
    
    private static void validateDeleteR4(final DeleteCommand deleteCommand) throws ValidationException
    {
        if (deleteCommand.getFromClause() != null && deleteCommand.getTables() != null) {
            for (TableReferenceClause t: deleteCommand.getTables()) {
                validateTable(t);
                if (t.getAlias() != null && !t.getAlias().trim().isEmpty()) {
                    throw new ValidationException(ERROR_MEESAGE__DELETE_R4);
                }
            }
        }
        
        if (deleteCommand.getFromClause() != null && deleteCommand.getFromClause().getTables() != null && deleteCommand.getUsing()!= null) {
            for (TableReferenceClause t: deleteCommand.getFromClause().getTables()) {
                validateTable(t);
                if (t.getAlias() != null && !t.getAlias().trim().isEmpty()) {
                    throw new ValidationException(ERROR_MEESAGE__DELETE_R4);
                }
            }
        }
    }
    
    private static void validateDeleteR2(String query) throws ValidationException
    {
        query = replaceSubQueries(query, "1");
        
        
        final int lowPriorityIndex = indexOf(query, "low_priority");
        final int quickIndex = indexOf(query, "quick");
        final int ignoreIndex = indexOf(query, "ignore");
        final int fromIndex = indexOf(query, "from");
        final int partitionIndex = indexOf(query, "partition");
        final int usingIndex = indexOf(query, "using");
        final int whereIndex = indexOf(query, "where");
        final int orderByIndex = indexOf(query, "order by");
        final int limitIndex = indexOf(query, "limit");


        final Integer[] indexes = new Integer[]{lowPriorityIndex, quickIndex, ignoreIndex, fromIndex, partitionIndex, usingIndex, whereIndex, orderByIndex, limitIndex};
        final String[] clauses = new String[]{"LOW_PRIORITY", "QUICK", "IGNORE", "FROM", "PARTITION", "USING", "WHERE", "ORDER BY", "LIMIT"};

        int index = lowPriorityIndex;
        
        String lastClause = "LOW_PRIORITY";
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                    lastClause = clauses[i];
                }
                else
                {
                    throw new ValidationException(String.format(ERROR_MEESAGE__DELETE_R2, lastClause.toUpperCase(), clauses[i].toUpperCase()));
                }
            }
        }
    }
    
    
    private static void validateInsertR1(InsertCommand insertCommand, int type) throws ValidationException
    {
        String query = replaceSubQueries(insertCommand.getName(), "1");
        
        IntoTableClause into = insertCommand.getIntoTable();
        if (into == null 
                || into.getName() == null 
                || (type == 0 && insertCommand.getValuesClause() == null)
                || (type == 2 && (insertCommand.getSetClause().getCondition() == null || insertCommand.getSetClause().getCondition().trim().isEmpty()))) {
            throw new ValidationException(ERROR_MEESAGE__INSERT_R1);
        }
        
        String[] names = into.getName().split(" ");
        if (names.length > 1) {
            throw new ValidationException(String.format(ERROR_MEESAGE__INSERT_R4, String.join(" ", Arrays.copyOf(names, names.length - 1)).toUpperCase()));
        }
        
        final int insertIndex = indexOf(query, "insert");
        final int lowPiorityIndex = indexOf(query, "low_priority");
        final int delayedIndex = indexOf(query, "delayed");
        final int highPiorityIndex = indexOf(query, "high_priority");
        final int ignoreIndex = indexOf(query, "ignore");
        final int intoIndex = indexOf(query, "into");
        final int partitionIndex = indexOf(query, "partition");
        final int valuesIndex = indexOf(query, "values");
        final int setIndex = indexOf(query, "set");
        final int selectIndex = indexOf(query, "select");

        
        int priorityIndex = lowPiorityIndex;
        String priorityText = "LOW_PRIORITY";
        if (delayedIndex > -1) {
            if (priorityIndex > -1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__INSERT_R3, "LOW_PRIORITY", "DELAYED"));
            }
            
            priorityIndex = delayedIndex;
            priorityText = "DELAYED";
        }
        
        if (priorityIndex == -1) {
            priorityIndex = highPiorityIndex;
        } else if (highPiorityIndex > -1) {
            throw new ValidationException(String.format(ERROR_MEESAGE__INSERT_R3, priorityText.toUpperCase(), "HIGH_PRIORITY"));
        }
        

        Integer[] indexes = null;
        
        switch (type) {
            case 0:
                indexes = new Integer[]{insertIndex, priorityIndex, ignoreIndex, intoIndex, partitionIndex, valuesIndex};
                break;            
            
            case 1:
                if (delayedIndex > - 1) {
                    throw new ValidationException(ERROR_MEESAGE__INSERT_R1);
                }
                indexes = new Integer[]{insertIndex, priorityIndex, ignoreIndex, intoIndex, partitionIndex, selectIndex};
                break;
                
            case 2:
                indexes = new Integer[]{insertIndex, priorityIndex, ignoreIndex, intoIndex, partitionIndex, setIndex};
                break;
                
            default:
                throw new ValidationException(ERROR_MEESAGE__INSERT_R1);
        }
        
        int index = insertIndex;
        
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                }
                else
                {
                    throw new ValidationException(ERROR_MEESAGE__INSERT_R1);
                }
            }
        }
    }
    
    private static void validateReplacetR1(ReplaceCommand reaplceCommand, int type) throws ValidationException
    {
        String query = replaceSubQueries(reaplceCommand.getName(), "1");
        
        IntoTableClause into = reaplceCommand.getIntoTable();
        if (into == null 
                || into.getName() == null 
                || (type == 0 && reaplceCommand.getValuesClause() == null)) {
            throw new ValidationException(ERROR_MEESAGE__REPLACE_R1);
        }
        
        String[] names = into.getName().split(" ");
        if (names.length > 1) {
            throw new ValidationException(String.format(ERROR_MEESAGE__REPLACE_R4, String.join(" ", Arrays.copyOf(names, names.length - 1)).toUpperCase()));
        }
        
        final int replaceIndex = indexOf(query, "replace");
        final int lowPiorityIndex = indexOf(query, "low_priority");
        final int delayedIndex = indexOf(query, "delayed");
        final int highPiorityIndex = indexOf(query, "high_priority");
        final int ignoreIndex = indexOf(query, "ignore");
        final int intoIndex = indexOf(query, "into");
        final int partitionIndex = indexOf(query, "partition");
        final int valuesIndex = indexOf(query, "values");
        final int setIndex = indexOf(query, "set");
        final int selectIndex = indexOf(query, "select");

        
        int priorityIndex = lowPiorityIndex;
        String priorityText = "LOW_PRIORITY";
        if (delayedIndex > -1) {
            if (priorityIndex > -1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__REPLACE_R3, "LOW_PRIORITY", "DELAYED"));
            }
            
            priorityIndex = delayedIndex;
            priorityText = "DELAYED";
        }
        
        if (priorityIndex == -1) {
            priorityIndex = highPiorityIndex;
        } else if (highPiorityIndex > -1) {
            throw new ValidationException(String.format(ERROR_MEESAGE__REPLACE_R3, priorityText.toUpperCase(), "HIGH_PRIORITY"));
        }
        

        Integer[] indexes = null;
        
        switch (type) {
            case 0:
                indexes = new Integer[]{replaceIndex, priorityIndex, ignoreIndex, intoIndex, partitionIndex, valuesIndex};
                break;            
            
            case 1:
                if (delayedIndex > - 1) {
                    throw new ValidationException(ERROR_MEESAGE__REPLACE_R1);
                }
                indexes = new Integer[]{replaceIndex, priorityIndex, ignoreIndex, intoIndex, partitionIndex, selectIndex};
                break;
                
            case 2:
                indexes = new Integer[]{replaceIndex, priorityIndex, ignoreIndex, intoIndex, partitionIndex, setIndex};
                break;
                
            default:
                throw new ValidationException(ERROR_MEESAGE__REPLACE_R1);
        }
        
        int index = replaceIndex;
        
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                }
                else
                {
                    throw new ValidationException(ERROR_MEESAGE__REPLACE_R1);
                }
            }
        }
    }
    
    private static int countOfValue(List<String> list) {
        int count = 0;
        if (list != null)
        {
            for (String s: list) {
                int v = s.split(",").length;
                if (v > count) {
                    count = v;
                }
            }
        }
        
        return count;
    }
    
    private static void validateInsertR2(InsertCommand insertCommand, int type) throws ValidationException
    {
        int valuesCount = 0;
        int columnsCount = 0;
        
        List<String> values = insertCommand.getValuesClause() != null ? insertCommand.getValuesClause().getValue() : null;
        List<String> intos = insertCommand.getIntoTable() != null && insertCommand.getIntoTable().getValuesClause() != null ? insertCommand.getIntoTable().getValuesClause().getValue() : null;
        
        switch (type) {
            case 0:                
                valuesCount = countOfValue(values);
                columnsCount = countOfValue(intos);
                break;            
            
            case 1:
                valuesCount = 
                    insertCommand.getSelectClause().getColumns() != null ? 
                        insertCommand.getSelectClause().getColumns().size() : 0;
                columnsCount = countOfValue(intos);            
                break;
                
            case 2:
                valuesCount = 
                    insertCommand.getSetClause().getCondition() != null &&
                    !insertCommand.getSetClause().getCondition().isEmpty() ? 
                        insertCommand.getSetClause().getCondition().split(",").length : 0;
                columnsCount = countOfValue(intos); 
                break;
                
            default:
                throw new ValidationException(ERROR_MEESAGE__INSERT_R1);
        }
        
        if (columnsCount > 0 && valuesCount != columnsCount) {
            throw new ValidationException(ERROR_MEESAGE__INSERT_R2);
        }
    }
    
    private static void validateReplaceR2(ReplaceCommand replaceCommand, int type) throws ValidationException
    {
        int valuesCount = 0;
        int columnsCount = 0;
        switch (type) {
            case 0:
                valuesCount = 
                    replaceCommand.getValuesClause().getValue() != null &&
                    !replaceCommand.getValuesClause().getValue().isEmpty()? 
                        countOfValue(replaceCommand.getValuesClause().getValue()) : 0;
                columnsCount = 
                    replaceCommand.getIntoTable() != null && 
                    replaceCommand.getIntoTable().getValuesClause() != null && 
                    replaceCommand.getIntoTable().getValuesClause().getValue() != null && 
                    !replaceCommand.getIntoTable().getValuesClause().getValue().isEmpty()? 
                        countOfValue(replaceCommand.getIntoTable().getValuesClause().getValue()) : 0;
                break; 
                
            default:
                throw new ValidationException(ERROR_MEESAGE__REPLACE_R1);
        }
        
        if (columnsCount > 0 && valuesCount != columnsCount) {
            throw new ValidationException(ERROR_MEESAGE__REPLACE_R2);
        }
    }
    
    
    private static void validateUpdateR1(final UpdateCommand updateCommand) throws ValidationException
    {
        if (updateCommand.getSetClause() == null)
        {
            throw new ValidationException(ERROR_MEESAGE__UPDATE_R1);
        }
    }
    
    private static void validateUpdateR3(final UpdateCommand updateCommand) throws ValidationException
    {
        if (updateCommand.getTableClauses() != null && updateCommand.getTableClauses().size() > 1 && updateCommand.getLimitClause() != null)
        {
            throw new ValidationException(ERROR_MEESAGE__UPDATE_R3);
        }
    }
    
    private static void validateUpdateR2(String query) throws ValidationException
    {
        query = replaceSubQueries(query, "1");
        
        final int lowPriorityIndex = indexOf(query, "low_priority");
        final int ignoreIndex = indexOf(query, "ignore");
        
        final int setIndex = indexOf(query, "set");
        final int whereIndex = indexOf(query, "where");
        final int orderByIndex = indexOf(query, "order by");
        final int limitIndex = indexOf(query, "limit");


        final Integer[] indexes = new Integer[]{lowPriorityIndex, ignoreIndex, setIndex, whereIndex, orderByIndex, limitIndex};
        final String[] clauses = new String[]{"LOW_PRIORITY", "IGNORE", "SET", "WHERE", "ORDER BY", "LIMIT"};

        int index = lowPriorityIndex;
        
        String lastClause = "LOW_PRIORITY";
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                    lastClause = clauses[i];
                }
                else
                {
                    throw new ValidationException(String.format(ERROR_MEESAGE__UPDATE_R2, clauses[i].toUpperCase(), lastClause.toUpperCase()));
                }
            }
        }
    }
    
    
     private static void validateSelectR2(final SelectCommand selectCommand) throws ValidationException
    {
        if (selectCommand.getFromClause() == null && 
            (selectCommand.getGroupByClause() != null || selectCommand.getWhereClause() != null ||
             selectCommand.getOrderByClause() != null || selectCommand.getHavingClause() != null))
        {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R2);
        }
    }
    
    private static void validateSelectR3(final SelectCommand selectCommand) throws ValidationException
    {
        if (selectCommand.getHavingClause() != null && selectCommand.getGroupByClause() == null)
        {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R3);
        }
    }
    
    private static void validateFrom(final FromClause fromClause) throws ValidationException
    {
        if (fromClause != null) {
            List<TableReferenceClause> tables = fromClause.getTables();
            if (tables != null && !tables.isEmpty()) {
                for (TableReferenceClause t: tables) {
                    validateTable(t);
                }
            } else {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R8);
            }
        }
    }
    
    private static void validateGroupBy(final GroupByClause groupByClause) throws ValidationException
    {
        if (groupByClause != null) {
            List<ColumnReferenceClause> columns = groupByClause.getColumns();
            if (columns != null && !columns.isEmpty()) {
                // TODO a now disable this validation
//                for (ColumnReferenceClause c: columns) {
//                    validateColumn(c, false);
//                }
            } else {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R10);
            }
        }
    }
    
    private static void validateOrderBy(final OrderByClause orderByClause) throws ValidationException
    {
        if (orderByClause != null) {
            List<ColumnReferenceClause> columns = orderByClause.getColumns();
            if (columns != null && !columns.isEmpty()) {
                for (ColumnReferenceClause c: columns) {
                    validateColumn(c, false);
                }
            } else {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R12);
            }
        }
    }
    
    private static void validateLimit(final LimitClause limitByClause) throws ValidationException
    {
        if (limitByClause != null && !limitByClause.getSource().toLowerCase().matches("((\\d+,\\s*)?\\d+)|(\\d+\\s+\\boffset\\b\\s+\\d+)")) {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R13);
        }
    }
    
    private static void validateLimitCount(final LimitClause limitByClause) throws ValidationException
    {
        if (limitByClause != null && !limitByClause.getSource().toLowerCase().matches("\\d+")) {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R13);
        }
    }
    
    private static void validateColumn(final ColumnReferenceClause column, boolean hasAlias) throws ValidationException {
        
        if (column != null && column.isSimple()) {
            
            String name = column.getName() != null ? column.getName().trim() : "";
            if (name.contains("(") && name.contains(")")) {
                name = name.substring(0, name.indexOf("(")) + "<column-name>" + name.substring(name.lastIndexOf(")") + 1);
            }
            
            if (name.isEmpty() || name.split(" ").length > 1 || "asc".equalsIgnoreCase(name) || "desc".equalsIgnoreCase(name)) {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
            }
            
            if (hasAlias) {
                String alias = column.getAlias() != null ? column.getAlias().trim() : "";
                if (alias.isEmpty() || alias.split(" ").length > 1 || "asc".equalsIgnoreCase(alias) || "desc".equalsIgnoreCase(alias)) {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
                }
            } else if (column.getAlias() != null) {
                throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
            }
        }
    }
    
    private static void validateWhere(final WhereClause whereClause) throws ValidationException
    {
        if (whereClause != null && (whereClause.getCondition() == null || whereClause.getCondition().trim().isEmpty())) {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R9);
        }
        
        if (whereClause != null) {
            String[] orCoditions = whereClause.getCondition().toLowerCase().trim().split("\\bor\\b");
            for (String or: orCoditions) {
                
                String[] andCoditions = or.trim().split("\\band\\b");
                for (String and: andCoditions) {
                    
                    and = replaceQuotesContent(and);                    
                    String simpleConditions[] = and.trim().split(SIMPLE_CONDITION__PATTERN);
                    
                    // for simple condition we can split each element and split by space
                    // length must be 1 on both side, later need improve this
                    // @TODO if need more validation - then need improve this part
//                    if (simpleConditions.length > 1) {
//                        for (String s: simpleConditions) {
//                            String[] elems = s.trim().split("\\s+");
//                            if (elems.length > 1) {
//                                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R17, elems[1]));
//                            }
//                        }
//                    }
                }
            }
        }        
    }
    
    private static void validateHaving(final HavingClause havingClause) throws ValidationException
    {
        if (havingClause != null && (havingClause.getCondition() == null || havingClause.getCondition().trim().isEmpty())) {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R11);
        }
    }
    
    private static void validateTable(final TableReferenceClause table) throws ValidationException {
        if (table != null && !(table instanceof SelectCommand) &&
            (table.getName() == null || table.getName().trim().isEmpty() || table.getName().trim().split(" ").length > 1 ||
                table.getAlias()!= null && table.getAlias().trim().split(" ").length > 1)) {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
        }
        
        if (table != null && table.getJoinClauses() != null) {
            for (JoinClause jc: table.getJoinClauses()) {
                if (jc.toString() == null) {
                    throw new ValidationException(ERROR_MEESAGE__JOIN_R0);
                } else {
                    validateCondition(jc.getCondition());
                }
            }
        }
    }
    
    private static void validateCondition(String condition) throws ValidationException {
        if (condition != null && !condition.trim().isEmpty()) {
            try {
                if (!condition.startsWith("(") && !condition.endsWith(")")) {
                    condition = "(" + condition + ")";
                }
                Parser.Node node = Parser.parse(condition);
                if (node != null) {

                }
            } catch (Throwable ex) {
                throw new ValidationException(ERROR_MEESAGE__JOIN_R2);
            }
        }
    }
    
    private static void validateSelectColumns(final SelectCommand selectCommand) throws ValidationException
    {
        // must be at least 1 select column
        
        if (selectCommand.getColumns() != null && !selectCommand.getColumns().isEmpty()) {
            
            int allCount = 0;
            //int index = selectCommand.toString().indexOf(selectCommand.getColumns().get(0).toString());
            for (ColumnReferenceClause c: selectCommand.getColumns()) {
                // - alias must have single word
                // TODO Maybe need add checking name (but it may have some expresion)
                if (c.getAlias() != null && splitColumnBySpace(c.getAlias()).size() > 1) {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
                }
                
                if (c.getAlias() == null && c.getName() == null) {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1_0);
                }
                
                // - name and alias cant start with KEYWORDS
                // if alias exist than need check name
                // alias must check in any variants
                if (c.getAlias() != null && isSelectKeyWord(c.getName()) || isSelectKeyWord(c.getAlias())) {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
                }
                
                if ("*".equals(c.getName())) {
                    allCount++;
                }
                
                // - alias cant be *
                if ("*".equals(c.getAlias()) || allCount > 1) {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1_0);//, index + c.getName().length() + (c.isAs() ? 4 : 1));
                }
                
                // - in name *, than alis must be null
                if ("*".equals(c.getName()) && c.getAlias() != null) {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
                }
                
                if (c.getName() != null) {
                    String[] strs = c.getName().split(" ");
                    if (c.getName() != null && strs.length > 1 && c.getAlias() != null) {

                        int count = 0;
                        if (c.getName().trim().startsWith("(") && c.getName().trim().endsWith(")")) {
                            //all ok its difficul name
                            
                        } else if (c.getName().trim().endsWith(")")) {
                            // its function
                            // check at least columns
                            try {
                                String functionColumnData = c.getName().substring(c.getName().indexOf("(") + 1, c.getName().lastIndexOf(")"));
                                String[] functionColumns = functionColumnData.split(",");
                                for (String fc: functionColumns) {
                                    if (fc.trim().isEmpty()) {
                                        throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R16, c.getName().trim()));
                                    }
                                }
                            } catch (Throwable th) {
                                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R16, c.getName().trim()));
                            }                            
                        } else {
                            for (String s: strs) {
                                if (s.matches("\\w+")) {
                                    count++;
                                } else {
                                    count = 0;
                                }

                                if (count == 3) {
                                    throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R16, s));
                                }
                            }
                        }

    //                    if (count == 2) {
    //                        throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R16, c.getAlias()));
    //                    }
                    }
                }
            }
        } else {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R6);
        }
    }
    
    private static void validateSelectR5(String query) throws ValidationException
    {
        query = replaceSubQueries(query, "1");
        
        query = replaceFunctions(query);
        
        // Check global clauses
        final int selectIndex = indexOf(query, "select");
        final int fromIndex = indexOf(query, "from");
        final int whereIndex = indexOf(query, "where");
        final int groupByIndex = indexOf(query, "group by");
        final int havingIndex = indexOf(query, "having");
        final int orderByIndex = indexOf(query, "order by");
        final int limitByIndex = indexOf(query, "limit");
        final int procedureByIndex = indexOf(query, "procedure");
        
        int intoOutFileByIndex = indexOf(query, "into\\s+outfile");
        int intoDumpFileByIndex = indexOf(query, "into\\s+dumpfile");
        int intoOneByIndex = indexOf(query, "into");
        
        int intoByIndex = intoOutFileByIndex;
        String intoText = "INTO OUTFILE";
        if (intoDumpFileByIndex > -1) {
            if (intoByIndex > -1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, "INTO OUTFILE", "INTO DUMPFILE"));
            }
            
            intoByIndex = intoDumpFileByIndex;
            intoText = "INTO DUMPFILE";
        }
        
        if (intoByIndex == -1) {
            intoByIndex = intoOneByIndex;
            intoText = "INTO";
        } else {
            int intoCount = countOf(query, "into");
            if (intoCount > 1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, "INTO", intoText));
            }
        }
        
        
        
        
        int forUpdateByIndex = indexOf(query, "for\\s+update");
        int lockInByIndex = indexOf(query, "lock\\s+in\\s+share\\s+mode");
        
        String optionText = "FOR UPDATE";        
        int optionByIndex = forUpdateByIndex;        
        if (lockInByIndex >  -1) {
            if (optionByIndex > -1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, "FOR UPDATE", "LOCK IN SHARE MODE"));
            }
            
            optionByIndex = lockInByIndex;
            intoText = "LOCK IN SHARE MODE";
        }
        
        
        
        Integer[] indexes = new Integer[] {
            selectIndex, 
            fromIndex, 
            whereIndex, 
            groupByIndex, 
            havingIndex, 
            orderByIndex, 
            limitByIndex,
            procedureByIndex,
//            intoByIndex,
            optionByIndex
        };
        String[] clauses = new String[] {
            "SELECT", 
            "FROM",
            "WHERE", 
            "GROUP BY",
            "HAVING",
            "ORDER BY", 
            "LIMIT",
            "PROCEDURE",
//            intoText,
            optionText
        };

        int index = selectIndex;
        
        String lastClause = "SELECT";
        int lastI = -1;
        for (int i = 1; i < indexes.length; i++)
        {    
            // maybe need remove
            // we check that we write more than 1 clause
            if (countOf(query, lastClause) > 1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, lastClause.toUpperCase(), lastClause.toUpperCase()));
            }
            
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                    lastClause = clauses[i];
                    lastI = i;
                }
                else
                {
                    throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R5, clauses[i].toUpperCase(), lastClause.toUpperCase(), i, lastI, clauses[i].toUpperCase(), lastClause.toUpperCase()));
                }
            }
        }
        
        // Check global select columns options
        int allByIndex = indexOf(query, "all");
        int distinctByIndex = indexOf(query, "distinct");
        int distinctrowIndex = indexOf(query, "distinctrow");
        
        int selectOptionByIndex = allByIndex;
        String selectOptionText = "ALL";
        if (distinctByIndex > -1) {
            if (selectOptionByIndex > -1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, "ALL", "DISTINCT"));
            }
            
            selectOptionByIndex = distinctByIndex;
            selectOptionText = "DISTINCT";
        }
        
        if (selectOptionByIndex == -1) {
            selectOptionByIndex = distinctrowIndex;
            selectOptionText = "DISTINCTROW";
        } else if (distinctrowIndex > -1) {
            throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, selectOptionText.toUpperCase(), "DISTINCTROW"));
        }
        
        
        
        int sqlCacheByIndex = indexOf(query, "sql_cache");
        int sqlNoCacheByIndex = indexOf(query, "sql_no_cache");
        
        String sqlText = "SQL_CACHE";
        int sqlIndex = sqlCacheByIndex;        
        if (sqlNoCacheByIndex >  -1) {
            if (sqlIndex > -1) {
                throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R7, "SQL_CACHE", "SQL_NO_CACHE"));
            }
            
            sqlIndex = sqlNoCacheByIndex;
            sqlText = "SQL_NO_CACHE";
        }
        
        indexes = new Integer[] {
            selectOptionByIndex, 
            indexOf(query, "high_priority"), 
            indexOf(query, "max_statement_time"), 
            indexOf(query, "straight_join"), 
            indexOf(query, "sql_small_result"), 
            indexOf(query, "sql_big_result"), 
            indexOf(query, "sql_buffer_result"), 
            sqlIndex, 
            indexOf(query, "sql_calc_found_rows"), 
        };
        clauses = new String[] {
            selectOptionText, 
            "HIGH_PRIORITY", 
            "MAX_STATEMENT_TIME", 
            "STRAIGHT_JOIN", 
            "SQL_SMALL_RESULT", 
            "SQL_BIG_RESULT", 
            "SQL_BUFFER_RESULT",
            sqlText,
            "SQL_CALC_FOUND_ROWS"
        };

        index = selectOptionByIndex;
        
        lastClause = selectOptionText;
        lastI = -1;
        for (int i = 1; i < indexes.length; i++)
        {           
            final Integer ind = indexes[i];
            if (ind > -1)
            {
                if (index < ind)
                {
                    index = ind;
                    lastClause = clauses[i];
                    lastI = i;
                }
                else
                {
                    throw new ValidationException(String.format(ERROR_MEESAGE__SELECT_R5, clauses[i].toUpperCase(), lastClause.toUpperCase(), i, lastI, clauses[i].toUpperCase(), lastClause.toUpperCase()));
                }
            }
        }
    }
        
    private static void validateSelectR6(final SelectCommand selectCommand) throws ValidationException
    {        
        if (selectCommand.getColumns() == null || selectCommand.getColumns().isEmpty())
        {
            throw new ValidationException(ERROR_MEESAGE__SELECT_R6);
        }
    }
    
    
    private static void validateStartR0(final StartCommand startCommand) throws ValidationException
    {
        if (!startCommand.isSlave() && !startCommand.isTransaction() || startCommand.isSlave() && startCommand.isTransaction())
        {
            throw new ValidationException(ERROR_MEESAGE__START_R0);
        }
    }
    
    private static void validateSetR0(final SetCommand setCommand) throws ValidationException
    {
        if (setCommand.getDefinition() != null) 
        {
            String[] definitions = replaceFunctions(setCommand.getDefinition()).split(",");
            for (String def: definitions) 
            {
                def = replaceQuotesContent(def);
                String s[] = def.split("=");
                if (s.length != 2 || (s.length == 2 && (s[0].trim().isEmpty() || s[1].trim().isEmpty()))) 
                {
                    throw new ValidationException(ERROR_MEESAGE__SET_R0);
                }
            }
        }
        else
        {
            throw new ValidationException(ERROR_MEESAGE__SET_R0);
        }
    }
    
    private static void throwSubqueryException(final AbstractCommand command, final ValidationException ex, final String queryKey) throws ValidationException
    {
        final Integer[] region = command.getSubQueriesIdx(queryKey);
        int start = -1;
        int finish = -1;
        if (region != null)
        {
            start = region[0];
            finish = region[1];
        }

        throw new ValidationException(ex.getMessage() + "\nstart: " + start + "\nfinish:" + finish);
    }
    
    private static void checkAllSubQueries(final AbstractCommand command) throws ValidationException
    {
        for (final Map.Entry<String, String> subQuery: command.getSubQueriesEntrySet())
        {
            try
            {
                QueryClause queryCommand = QueryFormat.parseQuery(subQuery.getValue());
                if (queryCommand instanceof SelectCommand)
                {
                    validateSelect((SelectCommand)queryCommand);
                }
                else
                {
                    throw new ValidationException(ERROR_MEESAGE__SELECT_R1);
                }
            }
            catch (final ValidationException ex)
            {
                throwSubqueryException(command, new ValidationException(ex.getMessage()), subQuery.getKey());
            }
        }
    }
    
    
    private static String replaceSubQueries(String sqlQuery, String replaceText) {
        
        // replace all string data ".*" to "<string>"
        String query = sqlQuery.replaceAll("\".*?\"", "\"<string>\"");
        
        
        final Matcher m = SUB_SELECT__PATTERN.matcher(sqlQuery);
        
        if (m.find())
        {
            int start = m.start();
            
            int brachesDiff = 1;
            for (int i = start + 1; i < query.length(); i++)
            {
                switch (query.charAt(i))
                {
                    case '(':
                        brachesDiff++;
                        break;
                        
                    case ')':
                        brachesDiff--;
                        break;
                }
                
                // we find end of select query
                if (brachesDiff == 0)
                {
                    query = query.substring(0, start) + replaceText + query.substring(i + 1);
                    break;
                }
            }
            
            return replaceSubQueries(query, replaceText);
        }
        
        return query;
    }
    
    private static int findIndexOfQuote(String text, int from) {
        int idx = text.indexOf("'", from);
        int ignore = text.indexOf("\\'", from);
        
        if (idx != ignore + 1) {
            return idx;
            
        } else {
            return findIndexOfQuote(text, idx + 1);
        }
    }
    
    // Need check and refactor if need this methods
    public static String replaceQuotesContent(String text) {
        
        StringBuilder sb = new StringBuilder(text.length());
        
        while (true) {
            int start = findIndexOfQuote(text, 0);
            int end = findIndexOfQuote(text, start + 1);

            if (start > -1 && end > -1) {
                sb.append(text.substring(0, start + 1));
                sb.append(StringUtils.repeat("_", end - start - 1));
                sb.append("'");
                
                text = text.substring(end + 1);
                
            } else if (start > -1) {
                sb.append(text.substring(0, start + 1));
                sb.append(StringUtils.repeat("_", text.length() - start));
                
                break;
                
            } else {
                break;
            }
        }
        
        return sb.length() > 0 ? sb.toString() : text;
    }
    
    
    private static String replaceFunctions(String sqlQuery) {
        
        int last = -1;
        for (int i = 0; i < sqlQuery.length(); i++)
        {
            char c = sqlQuery.charAt(i);
            if (c == '(') {
                last = i;
                
            } else if (c == ')') {
                return replaceFunctions(sqlQuery.substring(0, last) + "<function>" + sqlQuery.substring(i + 1));
            }            
        }
        
        return sqlQuery;
    }
    
    private static int indexOf(String source, String findText) {
        Matcher m = Pattern.compile("\\b" + findText + "\\b", Pattern.CASE_INSENSITIVE).matcher(source);
        if (m.find()) {
            return m.start();
        }
        
        return -1;
    }
    
    private static int countOf(String source, String findText) {
        Matcher m = Pattern.compile("\\b" + findText + "\\b", Pattern.CASE_INSENSITIVE).matcher(source);
        int count = 0;
        while (m.find()) {
            count++;
        }
        
        return count;
    }
    
    
    private static List<String> splitColumnBySpace(String text) {
        List<String> list = new ArrayList<>();
        
        boolean columnQuote = false;
        boolean quote = false;
        String s = "";
        for (int i = 0; i < text.length(); i++)
        {
            char c = text.charAt(i);
            if (c == CHAR_SPACE && !columnQuote && !quote) {
                list.add(s);
                
            } else if (c == CHAR_COLUMN_QUOTE && !quote) {
                columnQuote = !columnQuote;
                
            } else if (c == CHAR_QUOTE) {
                quote = !quote;
                
            } else {
                s += c;
            }
        }
        
        if (!s.trim().isEmpty()) {
            list.add(s);
        }
        
        return list;
    }
    
    
    private static boolean isSelectKeyWord(String source) {
        return source != null ? SELECT_KEYWORDS.contains(source.toUpperCase()) : false;
    }
}
