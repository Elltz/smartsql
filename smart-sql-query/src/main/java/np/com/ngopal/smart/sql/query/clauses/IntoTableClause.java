
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class IntoTableClause implements QueryClause
{   
    private final String name;
    private final boolean hasIntoClause;
    
    private final ValuesClause valuesClause;
    
    public IntoTableClause(final String name, final boolean hasIntoClause, final ValuesClause valuesClause)
    {
        this.name = name;
        this.hasIntoClause = hasIntoClause;
        this.valuesClause = valuesClause;
    }

    public String getName() 
    {
        return name;
    }

    public boolean isHasIntoClause() 
    {
        return hasIntoClause;
    }
    
    public ValuesClause getValuesClause() {
        return valuesClause;
    }
    
}
