package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class RenameTableClause {
    
    private String oldTable;
    private String newTable;
    
    private final String source;
    
    public RenameTableClause(String source) {
        this.source = source;
    }
    
    public String getSource() {
        return source;
    }
    
    public void setOldTable(String oldTable) {
        this.oldTable = oldTable;
    }
    public String getOldTable() {
        return oldTable;
    }
    
    public void setNewTable(String newTable) {
        this.newTable = newTable;
    }
    public String getNewTable() {
        return newTable;
    }
}
