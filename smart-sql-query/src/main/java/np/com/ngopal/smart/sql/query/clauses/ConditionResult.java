
package np.com.ngopal.smart.sql.query.clauses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ConditionResult {
    
    private final Map<Integer, String> subConditions = new HashMap<>();
    private final Map<Integer, String> functions = new HashMap<>();
    private Integer conditionIndex = 0;
    private Integer functionIndex = 0;
    
    private Map<String, List<String>> conditions = new HashMap<>();
    
    public Integer nextConditionIndex() {
        return conditionIndex++;
    }
    
    public Integer nextFunctionIndex() {
        return functionIndex++;
    }
    
    public void putSubCondition(Integer index, String condition)  {
        subConditions.put(index, condition);
    }
    public String getSubCondition(Integer index) {
        return subConditions.get(index);
    }
    
    public void putFunction(Integer index, String function)  {
        functions.put(index, function);
    }
    public String getFunction(Integer index) {
        return functions.get(index);
    }
    
    public void addCondition(String lvl, String condition) {
        List<String> list = conditions.get(lvl);
        if (list == null) {
            list = new ArrayList<>();
            conditions.put(lvl, list);
        }
        
        list.add(condition);
    }
    
    public List<String> getCondition(String lvl) {
        return conditions.get(lvl);
    }
    
}
