package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class AlterTableSpecification {
    private String specification;
    private String type;
    private AlterTableKey alterTableKey;
    
    public AlterTableSpecification() {
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }
    
    public String getSpecification() {
        return specification;
    }
    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public void setAlterTableKey(AlterTableKey alterTableKey) {
        this.alterTableKey = alterTableKey;
    }
    public AlterTableKey getAlterTableKey() {
        return alterTableKey;
    }
    
    
}
