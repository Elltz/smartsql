package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class CreateTableColumnDefinition {
    
    public enum Type {
        COLUMN,
        PRIMARY_KEY,
        INDEX,
        KEY,
        UNIQUE,
        FULLTEXT,
        SPATIAL,
        FOREIGN_KEY,
        CHECK
    }
    
    private Type type;
    
    private String name;
    private String definition;
    
    private boolean constraint = false;
    private String constraintSymbol;
    
    private boolean index = false;
    private boolean key = false;
    
    private boolean using = false;
    private String usingType;
    
    private String indexType;
    private String columns;
    private String indexOption;

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public boolean isConstraint() {
        return constraint;
    }
    public void setConstraint(boolean constraint) {
        this.constraint = constraint;
    }

    public String getConstraintSymbol() {
        return constraintSymbol;
    }
    public void setConstraintSymbol(String constraintSymbol) {
        this.constraintSymbol = constraintSymbol;
    }

    public boolean isUsing() {
        return using;
    }
    public void setUsing(boolean using) {
        this.using = using;
    }

    public String getUsingType() {
        return usingType;
    }
    public void setUsingType(String usingType) {
        this.usingType = usingType;
    }

    public String getIndexType() {
        return indexType;
    }
    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public String getColumns() {
        return columns;
    }
    public void setColumns(String columns) {
        this.columns = columns;
    }

    public String getIndexOption() {
        return indexOption;
    }
    public void setIndexOption(String indexOption) {
        this.indexOption = indexOption;
    }

    public boolean isIndex() {
        return index;
    }
    public void setIndex(boolean index) {
        this.index = index;
    }

    public boolean isKey() {
        return key;
    }
    public void setKey(boolean key) {
        this.key = key;
    }
    
}
