
package np.com.ngopal.smart.sql.query.commands;

import java.util.List;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.ConditionElementReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class SelectCommand extends AbstractCommand implements ColumnReferenceClause, TableReferenceClause, ConditionElementReferenceClause, WhereClauseProvider, FromProvider
{
    private String distinctType;
    private boolean highPriority = false;
    private String maxStatTime;
    private boolean straightJoin = false;
    private boolean sqlSmallResult = false;
    private boolean sqlBigResult = false;
    private boolean sqlBufferResult = false;
    private String sqlType;
    
    private String partition;
    
    private boolean unionAll = false;
    
    private boolean hasAsc = false;
    private boolean asc = false;
    private boolean as = false;
    
    private String intoSelect;
    private String columnsSource;
    private List<ColumnReferenceClause> columns;    
    
    private FromClause fromClause;
    private WhereClause whereClause;
    private OrderByClause orderByClause;
    private GroupByClause groupByClause;
    private HavingClause havingClause;
    private LimitClause limitClause;
    
    private final String selectSource;
    private String simpleQuery;
    
    private String alias;
    private List<JoinClause> joinClauses;
    
    private String procedure;
    private String into;
    private boolean forUpdate = false;
    private boolean lockIn = false;
    
    private String sqlCalcFoundRows;
    
    private List<SelectCommand> unionSelectCommands;
    
    private AbstractCommand parentCommand;
    public SelectCommand(String selectSource, AbstractCommand parentCommand)
    {
        this.selectSource = selectSource;
        this.parentCommand = parentCommand;
    }
    
    public void setColumnSource(String columnsSource)
    {
        this.columnsSource = columnsSource;
    }
    
    public String getColumnSource()
    {
        return columnsSource;
    }
    
    @Override
    public String getSubQuery(String key) {
        String value = super.getSubQuery(key);
        return value == null && parentCommand != null ? parentCommand.getSubQuery(key) : value;
    }
    
    @Override
    public String getQuerySource(String key) {
        String value = super.getQuerySource(key);
        return value == null && parentCommand != null ? parentCommand.getQuerySource(key) : value;
    }
    
    @Override
    public Integer[] getSubQueriesIdx(String key) {
        Integer[] value = super.getSubQueriesIdx(key);
        return value == null && parentCommand != null ? parentCommand.getSubQueriesIdx(key) : value;
    }

    public void setUnionSelectCommands(List<SelectCommand> unionSelectCommands) {
        this.unionSelectCommands = unionSelectCommands;
    }
    public List<SelectCommand> getUnionSelectCommands() {
        return unionSelectCommands;
    }

    public String getSqlCalcFoundRows() {
        return sqlCalcFoundRows;
    }

    public void setSqlCalcFoundRows(String sqlCalcFoundRows) {
        this.sqlCalcFoundRows = sqlCalcFoundRows;
    }
    
    @Override
    public void setHasAsc(boolean hasAsc) {
        this.hasAsc = hasAsc;
    }
    @Override
    public boolean isHasAsc() {
        return hasAsc;
    }
    
    
    public boolean isUnionAll() {
        return unionAll;
    }
    public void setUnionAll(boolean unionAll) {
        this.unionAll = unionAll;
    }

    public String getProcedure() {
        return procedure;
    }
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getInto() {
        return into;
    }
    public void setInto(String into) {
        this.into = into;
    }
    
    public String getIntoSelect() {
        return intoSelect;
    }
    public void setIntoSelect(String intoSelect) {
        this.intoSelect = intoSelect;
    }

    public boolean isForUpdate() {
        return forUpdate;
    }
    public void setForUpdate(boolean forUpdate) {
        this.forUpdate = forUpdate;
    }
    
    public boolean isLockIn() {
        return lockIn;
    }
    public void setLockIn(boolean lockIn) {
        this.lockIn = lockIn;
    }
        
    @Override
    public boolean isAsc() {
        return asc;
    }
    @Override
    public void setAsc(boolean asc) {
        this.asc = asc;
    }
    
    @Override
    public boolean isAs() {
        return as;
    }
    @Override
    public void setAs(boolean as) {
        this.as = as;
    }
    
    public String getDistinctType() {
        return distinctType;
    }
    public void setDistinctType(String distinctType) {
        this.distinctType = distinctType;
    }   

    public boolean isHighPriority() {
        return highPriority;
    }
    public void setHighPriority(boolean highPriority) {
        this.highPriority = highPriority;
    }

    public String getMaxStatTime() {
        return maxStatTime;
    }
    public void setMaxStatTime(String maxStatTime) {
        this.maxStatTime = maxStatTime;
    }

    public boolean isStraightJoin() {
        return straightJoin;
    }
    public void setStraightJoin(boolean straightJoin) {
        this.straightJoin = straightJoin;
    }

    public boolean isSqlSmallResult() {
        return sqlSmallResult;
    }
    public void setSqlSmallResult(boolean sqlSmallResult) {
        this.sqlSmallResult = sqlSmallResult;
    }

    public boolean isSqlBigResult() {
        return sqlBigResult;
    }
    public void setSqlBigResult(boolean sqlBigResult) {
        this.sqlBigResult = sqlBigResult;
    }

    public boolean isSqlBufferResult() {
        return sqlBufferResult;
    }
    public void setSqlBufferResult(boolean sqlBufferResult) {
        this.sqlBufferResult = sqlBufferResult;
    }

    public String getSqlType() {
        return sqlType;
    }
    public void setSqlType(String sqlType) {
        this.sqlType = sqlType;
    }

    public String getSimpleQuery() {
        return simpleQuery;
    }
    public void setSimpleQuery(String simpleQuery) {
        this.simpleQuery = simpleQuery;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }
    public String getPartition() {
        return partition;
    }
    
    
    
    public String getSimpleSource()
    {
        return simpleQuery;
    }
    public void setSimpleSource(String simpleSource)
    {
        this.simpleQuery = simpleSource;
    }
    
    @Override
    public void setAlias(String alias)
    {
        this.alias = alias;
    }
    @Override
    public String getAlias() 
    {
        return alias;
    }

    @Override
    public String getName() 
    {
        return selectSource;
    }

    public void setLimitClause(LimitClause limitClause)
    {
        this.limitClause = limitClause;
    }

    public LimitClause getLimitClause()
    {
        return limitClause;
    }
    
    
    

    public void setHavingClause(HavingClause havingClause)
    {
        this.havingClause = havingClause;
    }

    public HavingClause getHavingClause()
    {
        return havingClause;
    }
    
    
    
    
    @Override
    public List<JoinClause> getJoinClauses() 
    {
        return joinClauses;
    }

    @Override
    public void setJoinClauses(List<JoinClause> joinClauses) 
    {
        this.joinClauses = joinClauses;
    }

    public void setColumns(List<ColumnReferenceClause> columns)
    {
        this.columns = columns;
    }
    public List<ColumnReferenceClause> getColumns()
    {
        return columns;
    }

    public void setFromClause(FromClause fromClause)
    {
        this.fromClause = fromClause;
    }

    @Override
    public FromClause getFromClause()
    {
        return fromClause;
    }

    public void setWhereClause(WhereClause whereClause)
    {
        this.whereClause = whereClause;
    }

    @Override
    public WhereClause getWhereClause()
    {
        return whereClause;
    }

    public GroupByClause getGroupByClause()
    {
        return groupByClause;
    }

    public void setGroupByClause(GroupByClause groupByClause)
    {
        this.groupByClause = groupByClause;
    }

    public OrderByClause getOrderByClause()
    {
        return orderByClause;
    }

    public void setOrderByClause(OrderByClause orderByClause)
    {
        this.orderByClause = orderByClause;
    }

    @Override
    public String toString() {
        return selectSource;
    }

    @Override
    public boolean isSimple() {
        return false;
    }
    
}
