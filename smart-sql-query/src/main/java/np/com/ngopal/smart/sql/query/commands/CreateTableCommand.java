package np.com.ngopal.smart.sql.query.commands;

import java.util.List;
import np.com.ngopal.smart.sql.query.clauses.CreateTableColumnDefinition;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class CreateTableCommand extends AbstractCommand {

    private final String source;
    
    private boolean temporary = false;
    private boolean ifNotExists = false;
    
    private String tableName;
    private String likeTableName;
    
    private List<CreateTableColumnDefinition> columns;
    
    
    private String select;
    private boolean selectIgnore = false;
    private boolean selectReplace = false;
    private boolean selectAs = false;
    
    
    public CreateTableCommand(String source) {
        this.source = source;
    }

    public boolean isTemporary() {
        return temporary;
    }
    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    public boolean isIfNotExists() {
        return ifNotExists;
    }
    public void setIfNotExists(boolean ifNotExists) {
        this.ifNotExists = ifNotExists;
    }

    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public boolean isSelectIgnore() {
        return selectIgnore;
    }
    public void setSelectIgnore(boolean selectIgnore) {
        this.selectIgnore = selectIgnore;
    }

    public boolean isSelectReplace() {
        return selectReplace;
    }
    public void setSelectReplace(boolean selectReplace) {
        this.selectReplace = selectReplace;
    }

    public boolean isSelectAs() {
        return selectAs;
    }
    public void setSelectAs(boolean selectAs) {
        this.selectAs = selectAs;
    }

    public void setSelect(String select) {
        this.select = select;
    }
    public String getSelect() {
        return select;
    }

    public void setLikeTableName(String likeTableName) {
        this.likeTableName = likeTableName;
    }
    public String getLikeTableName() {
        return likeTableName;
    }

    public void setColumns(List<CreateTableColumnDefinition> columns) {
        this.columns = columns;
    }
    public List<CreateTableColumnDefinition> getColumns() {
        return columns;
    }
    
    
    
    
    
    

    public String getSource() {
        return source;
    }
    
    
}
