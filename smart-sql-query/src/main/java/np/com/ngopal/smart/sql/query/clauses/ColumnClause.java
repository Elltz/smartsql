
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ColumnClause implements ColumnReferenceClause, ConditionElementReferenceClause
{    
    private String name;
    private String alias;
    
    private boolean hasAsc = false;
    private boolean asc = false;
    private boolean as = false;
    
    private boolean simple = true;
    
    private String source;
    
    public ColumnClause(final String source, final String name, final String alias)
    {
        this.source = source;
        this.name = name;
        this.alias = alias;
    }
    
    public ColumnClause(final String name)
    {
        this.name = name;
    }

    @Override
    public void setHasAsc(boolean hasAsc) {
        this.hasAsc = hasAsc;
    }
    @Override
    public boolean isHasAsc() {
        return hasAsc;
    }

    public void setSimple(boolean simple) {
        this.simple = simple;
    }
    @Override
    public boolean isSimple() {
        return simple;
    }
    
    @Override
    public boolean isAsc() {
        return asc;
    }
    @Override
    public void setAsc(boolean asc) {
        this.asc = asc;
    }
    
    @Override
    public boolean isAs() {
        return as;
    }
    @Override
    public void setAs(boolean as) {
        this.as = as;
    }
    
    public void setName(final String name)
    {
        this.name = name;
    }
    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void setAlias(String alias) 
    {
        this.alias = alias;
    }
    @Override
    public String getAlias()
    {
        return alias;
    }

    @Override
    public String toString() {
        return source;
    }
    
    
}
