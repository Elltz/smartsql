package np.com.ngopal.smart.sql.query.commands;

import java.util.List;
import np.com.ngopal.smart.sql.query.clauses.RenameTableClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class RenameCommand extends AbstractCommand {
    
    private List<RenameTableClause> renameTables;
    
    private final String source;
    
    public RenameCommand(String source) {
        this.source = source;
    }
    
    public String getSource() {
        return source;
    }
    
    public void setRenameTables(List<RenameTableClause> renameTables) {
        this.renameTables = renameTables;
    }
    public List<RenameTableClause> getRenameTables() {
        return renameTables;
    }
}
