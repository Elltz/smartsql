package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DropCommand extends AbstractCommand {

    private final String source;
    
    private String dropTarget;
    private boolean ifExists = false;
    
    private String tableSection;
    
    public DropCommand(String source) {
        this.source = source;
    }

    public void setDropTarget(String dropTarget) {
        this.dropTarget = dropTarget;
    }
    public String getDropTarget() {
        return dropTarget;
    }

    public boolean isIfExists() {
        return ifExists;
    }
    public void setIfExists(boolean ifExists) {
        this.ifExists = ifExists;
    }

    public String getTableSection() {
        return tableSection;
    }
    public void setTableSection(String tableSection) {
        this.tableSection = tableSection;
    }

    
    public String getSource() {
        return source;
    }
    
}
