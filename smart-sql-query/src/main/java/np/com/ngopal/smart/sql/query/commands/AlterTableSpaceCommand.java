package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class AlterTableSpaceCommand extends AbstractCommand
{
    private boolean add = false;
    private boolean drop = false;
    private String tableSpaceName;
    private String fileName;
    private String definition;
    
    private final String alterSource;
        
    public AlterTableSpaceCommand(final String alterSource)
    {
        this.alterSource = alterSource;
    }
    public String getSource() 
    {
        return alterSource;
    }

    public boolean isAdd() {
        return add;
    }
    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDrop() {
        return drop;
    }
    public void setDrop(boolean drop) {
        this.drop = drop;
    }

    public String getTableSpaceName() {
        return tableSpaceName;
    }
    public void setTableSpaceName(String tableSpaceName) {
        this.tableSpaceName = tableSpaceName;
    }

    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    
    

}
