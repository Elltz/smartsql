
package np.com.ngopal.smart.sql.query;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import np.com.ngopal.smart.sql.query.clauses.ColumnClause;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.commands.DeleteCommand;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.query.clauses.IntoTableClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OnDuplicateKeyUpdateClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.commands.ReplaceCommand;
import np.com.ngopal.smart.sql.query.clauses.SetClause;
import np.com.ngopal.smart.sql.query.clauses.TableClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.commands.UpdateCommand;
import np.com.ngopal.smart.sql.query.clauses.ValuesClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.commands.AbstractCommand;
import np.com.ngopal.smart.sql.query.commands.CallCommand;
import np.com.ngopal.smart.sql.query.exceptions.TemplateException;
import np.com.ngopal.smart.sql.query.templates.QueryTemplate;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryTemplateCoding
{    
    private static final Pattern TEXT_REPLACE__PATTERN =  Pattern.compile("('.*?')", Pattern.CASE_INSENSITIVE);
    private static final Pattern TEXT2_REPLACE__PATTERN =  Pattern.compile("(\".*?\")", Pattern.CASE_INSENSITIVE);
    private static final Pattern NUMBER_REPLACE__PATTERN =  Pattern.compile("\\b(\\d+\\.\\d+|\\.\\d+|\\d+)\\b", Pattern.CASE_INSENSITIVE);
    private static final Pattern NUMBER_REPLACE_0__PATTERN =  Pattern.compile("('\\d+\\.\\d+'|'\\.\\d+'|'\\d+')", Pattern.CASE_INSENSITIVE);
    private static final Pattern NUMBER_REPLACE_1__PATTERN =  Pattern.compile("(\"\\d+\\.\\d+\"|\"\\.\\d+\"|\"\\d+\")", Pattern.CASE_INSENSITIVE);
    
    private static final Pattern STRING__PATTERN =  Pattern.compile("(@\\d+)", Pattern.CASE_INSENSITIVE);
    
    private QueryTemplateCoding()
    {
    }
    
    
    
    
    
    // <editor-fold desc=" Make query tempate methods ">

    /**
     *  Make template from string
     * @param query string query
     * 
     * @return template
     * @see np.com.ngopal.smart.sql.query.templates.QueryTemplate
     * 
     * @throws TemplateException
     */    
    public static QueryTemplate makeQueryTemplate(final String query) throws TemplateException
    {
        QueryClause queryCommand = QueryFormat.parseQuery(query);
        // need to do to work and queryCommand never must be null
        return queryCommand != null ? makeQueryTemplate(queryCommand, query) : new QueryTemplate(query, templateHash(query));
    }
    
    /**
     *  Make template from query command
     * @param queryCommand QueryCommand object
     * @param source
     * 
     * @return template
     * @see np.com.ngopal.smart.sql.query.clauses.QueryClause
     * @see np.com.ngopal.smart.sql.query.templates.QueryTemplate
     * 
     * @throws TemplateException
     */ 
    public static QueryTemplate makeQueryTemplate(final QueryClause queryCommand, String source) throws TemplateException
    {
        String template = calculateTemplate(queryCommand, source);
        template = template.trim();
        
        if (template.endsWith(";"))
        {
            template = template.substring(0, template.length() - 1);
        }
        
        return new QueryTemplate(template, templateHash(template));
    }
    
    /**
     *  Generate MD5 hash for string
     * 
     * @param template
     * @return string md5 hash 
     * 
     * @throws TemplateException
     */
    public static String templateHash(final String template) throws TemplateException
    {
        try 
        {
            final MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(template.getBytes());
            
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i)
            {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            
            return sb.toString();
        } 
        catch (NoSuchAlgorithmException ex) 
        {
            throw new TemplateException("ErrorMessage: Calculation hash sum using MD5 algoritm", ex);
        }
    }    
    
    // </editor-fold>
    
    
    
   
    
    
    
    // <editor-fold desc=" Private query tempate methods ">
    private static String calculateTemplate(final QueryClause queryCommand, String source)
    {
        final StringBuffer templateBuffer = new StringBuffer();
        
        if (queryCommand instanceof SelectCommand)
        {
            calculateSelectTemplate((SelectCommand)queryCommand, (SelectCommand)queryCommand, templateBuffer, false, false);
        }
        else if (queryCommand instanceof UpdateCommand)
        {
            calculateUpdateTemlate((UpdateCommand)queryCommand, templateBuffer);
        }
        else if (queryCommand instanceof DeleteCommand)
        {
            calculateDeleteTemlate((DeleteCommand)queryCommand, templateBuffer);
        }
        else if (queryCommand instanceof InsertCommand)
        {
            calculateInsertTemlate((InsertCommand)queryCommand, templateBuffer);
        }
        else if (queryCommand instanceof ReplaceCommand)
        {
            calculateReplaceTemlate((ReplaceCommand)queryCommand, templateBuffer);
        }
        else if (queryCommand instanceof CallCommand)
        {
            calculateCallTemlate((CallCommand)queryCommand, templateBuffer);
        }
        else
        {
            templateBuffer.append(source);
        }
        
        return templateBuffer.toString().trim();
    }
    
    
    private static void appendToTemplate(final QueryClause mainClause, final StringBuffer buffer, final String text, final boolean needWhiteSpace)
    {
        if (text != null)
        {
            if (needWhiteSpace)
            {
                buffer.append(" ");
            }
            
            String result = text;
            
            
            if (mainClause instanceof AbstractCommand) {
                
                String str = "";
                int index = 0;
                
                Matcher m = STRING__PATTERN.matcher(text);
                while (m.find()) {
                    
                    String key = m.group();
                    
                    int start = m.start();
                    int end = m.end();
                    
                    str += text.substring(index, start);
                    str += ((AbstractCommand)mainClause).getString(key);
                    
                    index = end;
                }
                
                if (text.length() > index) {
                    str += text.substring(index);
                }
                
                result = str;
            }
            
            buffer.append(result.trim());
        }
    }
    
    private static void appendToTemplate(final QueryClause mainClause, final StringBuffer buffer, final String text)
    {
        appendToTemplate(mainClause, buffer, text, true);
    }
    
    private static void calculateReplaceTemlate(final ReplaceCommand replaceCommand, final StringBuffer templateBuffer)
    {
        appendToTemplate(replaceCommand, templateBuffer, "INSERT");
        
        if (replaceCommand.getIntoTable() != null)
        {
            calculateIntoTableTemplate(replaceCommand, replaceCommand.getIntoTable(), templateBuffer);
        }
        
        if (replaceCommand.getValuesClause() != null)
        {
            calculateValuesTemplate(replaceCommand, replaceCommand.getValuesClause(), templateBuffer);
        }
    }
    
    private static void calculateCallTemlate(final CallCommand callCommand, final StringBuffer templateBuffer)
    {
        appendToTemplate(callCommand, templateBuffer, "CALL");
        
        if (callCommand.getDefinition() != null)
        {
            templateBuffer.append(" ");
            appendToTemplate(callCommand, templateBuffer, replaceDataInTemaple((AbstractCommand) callCommand, callCommand.getDefinition()), false);
        }
    }
    
    
    private static void calculateInsertTemlate(final InsertCommand insertCommand, final StringBuffer templateBuffer)
    {
        appendToTemplate(insertCommand, templateBuffer, "INSERT");
        
        if (insertCommand.getIntoTable() != null)
        {
            calculateIntoTableTemplate(insertCommand, insertCommand.getIntoTable(), templateBuffer);
        }
        
        if (insertCommand.getValuesClause() != null)
        {
            calculateValuesTemplate(insertCommand, insertCommand.getValuesClause(), templateBuffer);
        }
        
        if (insertCommand.getOnDuplicateKeyUpdateClause()!= null)
        {
            calculateOnDuplicateKeyUpdateClause(insertCommand, insertCommand.getOnDuplicateKeyUpdateClause(), templateBuffer);
        }
             
        if (insertCommand.getSetClause() != null)
        {
            calculateSetTemplate(insertCommand, insertCommand.getSetClause(), templateBuffer);
        }
        
        if (insertCommand.getSelectClause() != null)
        {
            calculateSelectTemplate(null, insertCommand.getSelectClause(), templateBuffer, true, false);
        }
    }
    
    private static void calculateDeleteTemlate(final DeleteCommand deleteCommand, final StringBuffer templateBuffer)
    {
        appendToTemplate(deleteCommand, templateBuffer, "DELETE");
        
        if (deleteCommand.getTables() != null)
        {
            calculateTablesTemplate(deleteCommand, deleteCommand.getTables(), templateBuffer);
        }
        
        if (deleteCommand.getFromClause() != null)
        {
            calculateFromTemplate(deleteCommand, deleteCommand.getFromClause(), templateBuffer);
        }
        
        if (deleteCommand.getWhereClause() != null)
        {
            calculateWhereTemplate(deleteCommand, deleteCommand.getWhereClause(), templateBuffer);
        }
             
        if (deleteCommand.getLimitClause() != null)
        {
            calculateLimitTemplate(deleteCommand, deleteCommand.getLimitClause(), templateBuffer);
        }
    }
    
    
    private static void calculateUpdateTemlate(final UpdateCommand updateCommand, final StringBuffer templateBuffer)
    {
        appendToTemplate(updateCommand, templateBuffer, "UPDATE");
        
        if (updateCommand.getTableClauses() != null)
        {
            calculateTablesTemplate(updateCommand, updateCommand.getTableClauses(), templateBuffer);
        }
        
        if (updateCommand.getSetClause() != null)
        {
            calculateSetTemplate(updateCommand, updateCommand.getSetClause(), templateBuffer);
        }
        
        if (updateCommand.getWhereClause() != null)
        {
            calculateWhereTemplate(updateCommand, updateCommand.getWhereClause(), templateBuffer);
        }
        
        if (updateCommand.getOrderByClause()!= null)
        {
            calculateOrderByTemplate(updateCommand, updateCommand.getOrderByClause(), templateBuffer);
        }
             
        if (updateCommand.getLimitClause() != null)
        {
            calculateLimitTemplate(updateCommand, updateCommand.getLimitClause(), templateBuffer);
        }
    }
    
    
    private static void calculateSelectTemplate(final SelectCommand mainSelectCommand, final SelectCommand selectCommand, final StringBuffer templateBuffer, boolean needWhiteSpace, boolean union)
    {
        if (!union) {
            appendToTemplate(mainSelectCommand, templateBuffer, "SELECT", needWhiteSpace);
        }
        
        final List<ColumnReferenceClause> columns = selectCommand.getColumns();
        if (columns != null)
        {
            final int size = columns.size();
            for (int i = 0; i < size; i++)
            {
                final ColumnReferenceClause column = columns.get(i);
                
                if (column instanceof SelectCommand)
                {
                    appendToTemplate(mainSelectCommand, templateBuffer, "(");
            
                    calculateSelectTemplate(mainSelectCommand, (SelectCommand) column, templateBuffer, false, ((SelectCommand) column).getUnionSelectCommands() != null);

                    appendToTemplate(mainSelectCommand, templateBuffer, ")", false);

                    if (column.getAlias() != null)
                    {
                        appendToTemplate(mainSelectCommand, templateBuffer, "AS");
                        appendToTemplate(mainSelectCommand, templateBuffer, column.getAlias());
                    }
                }
                else
                {
                    calculateColumnTemplate(mainSelectCommand, (ColumnClause)column, templateBuffer);
                }
                
                if (i < size - 1)
                {
                    appendToTemplate(mainSelectCommand, templateBuffer, ",", false);
                }
            }
        }
        
        if (selectCommand.getFromClause() != null)
        {
            calculateFromTemplate(mainSelectCommand, selectCommand.getFromClause(), templateBuffer);
        }
        
        if (selectCommand.getUnionSelectCommands() != null) {
            
            final int size = selectCommand.getUnionSelectCommands().size();
            for (int i = 0; i < size; i++) {                
                
                appendToTemplate(mainSelectCommand, templateBuffer, "(", false);
                
                SelectCommand sc = selectCommand.getUnionSelectCommands().get(i);
                calculateSelectTemplate(mainSelectCommand, sc, templateBuffer, false, false);
                
                appendToTemplate(mainSelectCommand, templateBuffer, ")", false);
                
                if (i < size - 1)
                {
                    appendToTemplate(mainSelectCommand, templateBuffer, "UNION " + (sc.isUnionAll() ? "ALL" : ""), true);
                    appendToTemplate(mainSelectCommand, templateBuffer, "", true);
                }
            }
        }
        
        if (selectCommand.getWhereClause() != null)
        {
            calculateWhereTemplate(mainSelectCommand, selectCommand.getWhereClause(), templateBuffer);
        }
        
        if (selectCommand.getGroupByClause() != null)
        {
            calculateGroupByTemplate(mainSelectCommand, selectCommand.getGroupByClause(), templateBuffer);
        }
        
        if (selectCommand.getHavingClause() != null)
        {
            calculateHavingTemplate(mainSelectCommand, selectCommand.getHavingClause(), templateBuffer);
        }
        
        if (selectCommand.getOrderByClause() != null)
        {
            calculateOrderByTemplate(mainSelectCommand, selectCommand.getOrderByClause(), templateBuffer);
        }
        
        if (selectCommand.getLimitClause() != null)
        {
            calculateLimitTemplate(mainSelectCommand, selectCommand.getLimitClause(), templateBuffer);
        }
        

    }
    
    
    private static void calculateOnDuplicateKeyUpdateClause(final QueryClause mainClause, final OnDuplicateKeyUpdateClause onDuplicateClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "ON DUPLICATE KEY UPDATE");
        appendToTemplate(mainClause, templateBuffer, onDuplicateClause.getCondition());
    }
            
    private static void calculateValuesTemplate(final QueryClause mainClause, final ValuesClause valuesClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "VALUES (");
        appendToTemplate(mainClause, templateBuffer, replaceDataInTemaple((AbstractCommand) mainClause, String.join("),(", valuesClause.getValue())), false);
        appendToTemplate(mainClause, templateBuffer, ")", false);
    }
    
    private static void calculateIntoTableTemplate(final QueryClause mainClause, final IntoTableClause intoTableClause, final StringBuffer templateBuffer)
    {
        if (intoTableClause.isHasIntoClause())
        {
            appendToTemplate(mainClause, templateBuffer, "INTO");
        }
        
        appendToTemplate(mainClause, templateBuffer, intoTableClause.getName());
        if (intoTableClause.getValuesClause() != null) {
            appendToTemplate(mainClause, templateBuffer, "(");
            appendToTemplate(mainClause, templateBuffer, String.join("),(", intoTableClause.getValuesClause().getValue()), false);
            appendToTemplate(mainClause, templateBuffer, ")", false);
        }
    }
    
    private static void calculateSetTemplate(final QueryClause mainClause, final SetClause setClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "SET");        
        appendToTemplate(mainClause, templateBuffer, replaceDataInTemaple((AbstractCommand) mainClause, setClause.getCondition()));
    }
    
    private static void calculateLimitTemplate(final QueryClause mainClause, final LimitClause limitClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "LIMIT");        
        appendToTemplate(mainClause, templateBuffer, replaceDataInTemaple((AbstractCommand) mainClause, limitClause.getSource()));
    }
    
    private static void calculateOrderByTemplate(final QueryClause mainClause, final OrderByClause orderByClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "ORDER BY");
        
        final List<ColumnReferenceClause> columns = orderByClause.getColumns();
        if (columns != null)
        {
            final int size = columns.size();
            for (int i = 0; i < size; i++)
            {
                final ColumnReferenceClause column = columns.get(i);
                
                calculateColumnTemplate(mainClause, (ColumnClause)column, templateBuffer);
                
                if (i < size - 1)
                {
                    appendToTemplate(mainClause, templateBuffer, ",", false);
                }
            }
        }
    }
    
    private static void calculateHavingTemplate(final QueryClause mainClause, final HavingClause havingClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "HAVING");        
        appendToTemplate(mainClause, templateBuffer, replaceDataInTemaple((AbstractCommand) mainClause, havingClause.getCondition()));
    }
    
    private static void calculateGroupByTemplate(final QueryClause mainClause, final GroupByClause groupByClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "GROUP BY");
        
        final List<ColumnReferenceClause> columns = groupByClause.getColumns();
        if (columns != null)
        {
            final int size = columns.size();
            for (int i = 0; i < size; i++)
            {
                final ColumnReferenceClause column = columns.get(i);
                
                calculateColumnTemplate(mainClause, (ColumnClause)column, templateBuffer);
                
                if (i < size - 1)
                {
                    appendToTemplate(mainClause, templateBuffer, ",", false);
                }
            }
        }
    }
    
    private static void calculateWhereTemplate(final QueryClause mainClause, final WhereClause whereClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "WHERE");        
        appendToTemplate(mainClause, templateBuffer, replaceDataInTemaple((AbstractCommand) mainClause, whereClause.getCondition()));
    }
    
    private static String replaceDataInTemaple(AbstractCommand command, String template)
    {
        if (template != null)
        {
            template = template.trim();
            
            String str = "";
            int ind = 0;
            
            Matcher m = STRING__PATTERN.matcher(template);
            while (m.find()) {
                String key = m.group();
                
                int start = m.start();
                int end = m.end();
                
                str += template.substring(ind, start);
                str += command.getString(key);
                    
                ind = end;
            }
            
            if (template.length() > ind) {
                str += template.substring(ind);
            }
            
            template = str;
            
            int shift = 0;
            m = NUMBER_REPLACE_0__PATTERN.matcher(template);
            while(m.find())
            {
                String text = m.group(1);
                final int index = m.start(1) + shift;

                try
                {
                    final BigDecimal value = new BigDecimal(text.substring(1, text.length() -1));
                    if (value.scale() != 0)
                    {
                        template = template.substring(0, index) + "DDD.DD" + template.substring(index + text.length());
                        shift += 6 - text.length();
                    }
                    else
                    {
                        template = template.substring(0, index) + "NNN" + template.substring(index + text.length());
                        shift += 3 - text.length();
                    }
                }
                catch (NumberFormatException ex)
                {
                }
            }
            
            shift = 0;
            m = NUMBER_REPLACE_1__PATTERN.matcher(template);
            while(m.find())
            {
                String text = m.group(1);
                final int index = m.start(1) + shift;

                try
                {
                    final BigDecimal value = new BigDecimal(text.substring(1, text.length() -1));
                    if (value.scale() != 0)
                    {
                        template = template.substring(0, index) + "DDD.DD" + template.substring(index + text.length());
                        shift += 6 - text.length();
                    }
                    else
                    {
                        template = template.substring(0, index) + "NNN" + template.substring(index + text.length());
                        shift += 3 - text.length();
                    }
                }
                catch (NumberFormatException ex)
                {
                }
            }
                        
            m = TEXT_REPLACE__PATTERN.matcher(template);

            shift = 0;
            while(m.find())
            {
                String text = m.group(1);
                int index = m.start(1) + shift;

                template = template.substring(0, index) + "'XXXXXX'"+ template.substring(index + text.length());
                shift += 8 - text.length();                 
            }

            shift = 0;
            m = TEXT2_REPLACE__PATTERN.matcher(template);
            while(m.find())
            {
                String text = m.group(1);
                int index = m.start(1) + shift;
                
                template = template.substring(0, index) + "\"XXXXXX\"" + template.substring(index + text.length());
                shift += 8 - text.length();
            }
            
            shift = 0;
            m = NUMBER_REPLACE__PATTERN.matcher(template);
            while(m.find())
            {
                String text = m.group(1);
                final int index = m.start(1) + shift;

                try
                {
                    final BigDecimal value = new BigDecimal(text);
                    if (value.scale() != 0)
                    {
                        template = template.substring(0, index) + "DDD.DD" + template.substring(index + text.length());
                        shift += 6 - text.length();
                    }
                    else
                    {
                        template = template.substring(0, index) + "NNN" + template.substring(index + text.length());
                        shift += 3 - text.length();
                    }
                }
                catch (NumberFormatException ex)
                {
                }
            }
        }
        return template;
    }
    
    
    private static void calculateFromTemplate(final QueryClause mainClause, final FromClause fromClause, final StringBuffer templateBuffer)
    {
        appendToTemplate(mainClause, templateBuffer, "FROM");        
        calculateTablesTemplate(mainClause, fromClause.getTables(), templateBuffer);
    }
    
    
    private static void calculateTablesTemplate(final QueryClause mainClause, final List<TableReferenceClause> tables, final StringBuffer templateBuffer)
    {
        if (tables != null)
        {
            final int size = tables.size();
            for (int i = 0; i < size; i++)
            {
                calculateTableReferenceTemplate(mainClause, tables.get(i), templateBuffer);
                
                if (i < size - 1)
                {
                    appendToTemplate(mainClause, templateBuffer, ",", false);
                }
            }
        }
    }
    
    private static void calculateTableReferenceTemplate(final QueryClause mainClause, final TableReferenceClause table, final StringBuffer templateBuffer)
    {
        if (table instanceof SelectCommand)
        {
            appendToTemplate(mainClause, templateBuffer, "(");
            
            calculateSelectTemplate(mainClause instanceof SelectCommand ? (SelectCommand)mainClause : (SelectCommand) table, (SelectCommand) table, templateBuffer, false, ((SelectCommand) table).getUnionSelectCommands() != null);
            
            appendToTemplate(mainClause, templateBuffer, ")", false);
            
            if (table.getAlias() != null)
            {
                appendToTemplate(mainClause, templateBuffer, "AS");
                appendToTemplate(mainClause, templateBuffer, table.getAlias());
            }
            
            if (table.getJoinClauses() != null) {
                for (JoinClause join: table.getJoinClauses()) {
                    calculateJoinTemplate(mainClause, join, templateBuffer);
                }
            }
        }
        else
        {
            calculateTableTemplate(mainClause, (TableClause)table, templateBuffer);
        }
    }
    
    private static void calculateTableTemplate(final QueryClause mainClause, final TableClause tableClause, final StringBuffer templateBuffer)
    {
        if (tableClause != null)
        {
            final String name = tableClause.getName();
            final String alias = tableClause.getAlias();
            
            appendToTemplate(mainClause, templateBuffer, name);
            
            if (alias != null)
            {
                appendToTemplate(mainClause, templateBuffer, "AS");
                appendToTemplate(mainClause, templateBuffer, alias);
            }
            
            if (tableClause.getJoinClauses() != null) {
                for (JoinClause join: tableClause.getJoinClauses()) {
                    calculateJoinTemplate(mainClause, join, templateBuffer);
                }
            }
        }
    }
    
    private static void calculateJoinTemplate(final QueryClause mainClause, final JoinClause joinClause, final StringBuffer templateBuffer)
    {
        if (joinClause != null)
        {
            if (joinClause.getType() != JoinClause.JoinType.NONE)
            {
                appendToTemplate(mainClause, templateBuffer, joinClause.getType().name().toUpperCase());
            }
            
            appendToTemplate(mainClause, templateBuffer, "JOIN");
            
            final TableReferenceClause table = joinClause.getTable();
            if (table != null)
            {
                calculateTableReferenceTemplate(mainClause, table, templateBuffer);
            }
            
            final String condition = joinClause.getCondition();
            if (condition != null)
            {
                appendToTemplate(mainClause, templateBuffer, "ON");
                appendToTemplate(mainClause, templateBuffer, condition.trim());
            }
        }
    }
    
    
    private static void calculateColumnTemplate(final QueryClause mainClause, final ColumnClause columnClause, final StringBuffer templateBuffer)
    {
        if (columnClause != null)
        {
            final String name = columnClause.getName();
            final String alias = columnClause.getAlias();
            
            appendToTemplate(mainClause, templateBuffer, name);
            
            if (alias != null)
            {
                appendToTemplate(mainClause, templateBuffer, "AS");
                appendToTemplate(mainClause, templateBuffer, alias);
            }
        }
    }
    
    
    // </ editor-fold>
    
    
}
