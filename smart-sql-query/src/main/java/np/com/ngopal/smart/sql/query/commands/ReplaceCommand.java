
package np.com.ngopal.smart.sql.query.commands;

import np.com.ngopal.smart.sql.query.clauses.IntoTableClause;
import np.com.ngopal.smart.sql.query.clauses.ValuesClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ReplaceCommand extends AbstractCommand
{
    private IntoTableClause intoTable;
    
    private ValuesClause valuesClause;
    
    private final String replaceSource;
        
    public ReplaceCommand(final String replaceSource)
    {
        this.replaceSource = replaceSource;
    }


    public String getName() 
    {
        return replaceSource;
    }

    
    public void setIntoTable(IntoTableClause intoTable)
    {
        this.intoTable = intoTable;
    }
    public IntoTableClause getIntoTable()
    {
        return intoTable;
    }

    public void setValuesClause(ValuesClause valuesClause)
    {
        this.valuesClause = valuesClause;
    }
    public ValuesClause getValuesClause()
    {
        return valuesClause;
    }

    
}
