package np.com.ngopal.smart.sql.query.commands;

import np.com.ngopal.smart.sql.query.clauses.WhereClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface WhereClauseProvider {
    
    WhereClause getWhereClause();
}
