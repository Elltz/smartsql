package np.com.ngopal.smart.sql.query.exceptions;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ValidationException extends Exception
{
    
    private int errorIndex = -1;
    
    public ValidationException(final String message)
    {
        super(message);
    }
    
    public ValidationException(final String message, int errorIndex)
    {
        super(message);
        this.errorIndex = errorIndex;
    }
    
    public ValidationException(final String message, final Throwable ex)
    {
        super(message, ex);
    }
    
    public int getErrorIndex() {
        return errorIndex;
    }
}
