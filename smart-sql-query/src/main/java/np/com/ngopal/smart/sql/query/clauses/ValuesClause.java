
package np.com.ngopal.smart.sql.query.clauses;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ValuesClause implements QueryClause
{
    private final List<String> value;
    
    public ValuesClause(final List<String> value)
    {
        this.value = value;
    }

    public List<String> getValue()
    {
        return value;
    }
    
}
