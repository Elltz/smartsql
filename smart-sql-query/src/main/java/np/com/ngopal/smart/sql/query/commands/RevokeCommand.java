package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class RevokeCommand extends AbstractCommand {

    private final String source;
    
    private String definition;
    
    public RevokeCommand(String source) {
        this.source = source;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    
    

    public String getSource() {
        return source;
    }
    
    
}