
package np.com.ngopal.smart.sql.query.commands;

import np.com.ngopal.smart.sql.query.clauses.OnDuplicateKeyUpdateClause;
import np.com.ngopal.smart.sql.query.clauses.SetClause;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class InsertCommand extends ReplaceCommand
{
    private String priority;
    private boolean ignore = false;
    
    private boolean values = false;
    private boolean value = false;
    
    private String partitionData;
        
    private OnDuplicateKeyUpdateClause onDuplicateKeyUpdateClause;
    private SetClause setClause;
    private SelectCommand selectClause;
    
        
    public InsertCommand(final String insertSource)
    {
        super(insertSource);
    }
    
    
    public String getPartitionData() {
        return partitionData;
    }

    public void setPartitionData(String partitionData) {
        this.partitionData = partitionData;
    }

    
    public String getPriority() {
        return priority;
    }
    public void setPriority(String priority) {
        this.priority = priority;
    }

    public boolean isIgnore() {
        return ignore;
    }
    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public boolean isValues() {
        return values;
    }
    public void setValues(boolean values) {
        this.values = values;
    }

    public boolean isValue() {
        return value;
    }
    public void setValue(boolean value) {
        this.value = value;
    }
   
    
    public void setOnDuplicateKeyUpdateClause(OnDuplicateKeyUpdateClause onDuplicateKeyUpdateClause)
    {
        this.onDuplicateKeyUpdateClause = onDuplicateKeyUpdateClause;
    }
    public OnDuplicateKeyUpdateClause getOnDuplicateKeyUpdateClause()
    {
        return onDuplicateKeyUpdateClause;
    }

    public SetClause getSetClause() 
    {
        return setClause;
    }
    public void setSetClause(SetClause setClause) 
    {
        this.setClause = setClause;
    }
    
    public SelectCommand getSelectClause() 
    {
        return selectClause;
    }
    public void setSelectClause(SelectCommand selectClause) 
    {
        this.selectClause = selectClause;
    }
    
    
}
