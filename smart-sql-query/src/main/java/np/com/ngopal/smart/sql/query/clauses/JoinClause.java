
package np.com.ngopal.smart.sql.query.clauses;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class JoinClause implements QueryClause
{
    public enum JoinType
    {
        INNER,
        LEFT,
        RIGHT,
        NATURAL,
        NONE;
        
        public static JoinType parseType(String type) {
            if (type != null) {
                switch (type.toLowerCase()) {
                    case "left":
                        return LEFT;
                        
                    case "right":
                        return RIGHT;
                        
                    case "inner":
                        return INNER;
                        
                    case "natural":
                        return NATURAL;
                }
            }
            
            return NONE;
        }
    }
 
    private JoinType type = JoinType.NONE;
    
    private TableReferenceClause table;
    private String condition;
    private String usingColumns;
    
    private String source;
    
    public JoinClause(String source)
    {
        this.source = source;
    }
    
    

    public JoinType getType()
    {
        return type;
    }
    public void setType(JoinType type)
    {
        this.type = type;
    }

    public TableReferenceClause getTable() 
    {
        return table;
    }
    public void setTable(TableReferenceClause table) 
    {
        this.table = table;
    }
    
    public String getUsingColumns() {
        return usingColumns;
    }
    public void setUsingColumns(String usingColumns) {
        this.usingColumns = usingColumns;
    }

    public String getCondition()
    {
        return condition;
    }
    public void setConditions(String condition)
    {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return source;
    }
    
    
}
