package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class TruncateCommand extends AbstractCommand {

    private final String source;
    
    private boolean hasTable = false;    
    private String tableSection;
    
    public TruncateCommand(String source) {
        this.source = source;
    }

    public boolean isHasTable() {
        return hasTable;
    }
    public void setHasTable(boolean hasTable) {
        this.hasTable = hasTable;
    }

    public String getTableSection() {
        return tableSection;
    }
    public void setTableSection(String tableSection) {
        this.tableSection = tableSection;
    }

    public String getSource() {
        return source;
    }
    
}
