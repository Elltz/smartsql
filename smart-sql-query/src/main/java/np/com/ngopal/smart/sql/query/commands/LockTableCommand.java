package np.com.ngopal.smart.sql.query.commands;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class LockTableCommand extends AbstractCommand {

    private final String source;
    
    private String tableName;
    
    
    public LockTableCommand(String source) {
        this.source = source;
    }

    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }  
    
    
    

    public String getSource() {
        return source;
    }
    
    
}
