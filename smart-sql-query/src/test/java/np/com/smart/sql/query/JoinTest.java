package np.com.smart.sql.query;

import java.util.List;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class JoinTest {
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }
    
    
    private void testBaseSelect(final QueryClause queryClause0) {
        
    }
    
    @Test
    public void parseLeftJoinQuery()
    {
        final QueryClause queryClause0 = QueryFormat.parseSelectQuery("SELECT a as a FROM a as a LEFT JOIN b as b ON a.a1 = b.b1");
        
        Assert.assertNotNull(queryClause0);
        
        Assert.assertTrue(queryClause0 instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause0;
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        FromClause fromClause = selectClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        List<TableReferenceClause> tables = fromClause.getTables();
        Assert.assertEquals(1, tables.size());
        
        TableReferenceClause table = tables.get(0);
        Assert.assertEquals("a", table.getName());
        Assert.assertTrue(table.isAs());
        Assert.assertEquals("a", table.getAlias());
        
        List<JoinClause> joins = table.getJoinClauses();
        Assert.assertNotNull(joins);
        
        TableReferenceClause tableJoin = joins.get(0).getTable();
        Assert.assertEquals("b", tableJoin.getName());
        Assert.assertEquals("b", tableJoin.getAlias());
        
        Assert.assertEquals(JoinClause.JoinType.LEFT, joins.get(0).getType());
        
        String on = joins.get(0).getCondition();
        Assert.assertNotNull(on);
        
        Assert.assertEquals("a.a1 = b.b1", on);
        
        String usingColumn = joins.get(0).getUsingColumns();
        Assert.assertNull(usingColumn);
    }
    
    @Test
    public void parseRightJoinQuery()
    {
        final QueryClause queryClause0 = QueryFormat.parseSelectQuery("SELECT * FROM a as a RIGHT JOIN b as b ON a.a1 = b.b1");
        
        Assert.assertNotNull(queryClause0);
        
        Assert.assertTrue(queryClause0 instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause0;
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        FromClause fromClause = selectClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        List<TableReferenceClause> tables = fromClause.getTables();
        Assert.assertEquals(1, tables.size());
        
        TableReferenceClause table = tables.get(0);
        Assert.assertEquals("a", table.getName());
        Assert.assertEquals("a", table.getAlias());
        
        List<JoinClause> joins = table.getJoinClauses();
        Assert.assertNotNull(joins);
        
        TableReferenceClause tableJoin = joins.get(0).getTable();
        Assert.assertEquals("b", tableJoin.getName());
        Assert.assertEquals("b", tableJoin.getAlias());
        
        Assert.assertEquals(JoinClause.JoinType.RIGHT, joins.get(0).getType());
        
        String on = joins.get(0).getCondition();
        Assert.assertNotNull(on);
        
        Assert.assertEquals("a.a1 = b.b1", on);
        
        String usingColumn = joins.get(0).getUsingColumns();
        Assert.assertNull(usingColumn);
    }
    
    @Test
    public void parseNaturalJoinQuery()
    {
        final QueryClause queryClause0 = QueryFormat.parseSelectQuery("SELECT * FROM a as a NATURAL JOIN b as b ON a.a1 = b.b1");
        
        Assert.assertNotNull(queryClause0);
        
        Assert.assertTrue(queryClause0 instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause0;
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        FromClause fromClause = selectClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        List<TableReferenceClause> tables = fromClause.getTables();
        Assert.assertEquals(1, tables.size());
        
        TableReferenceClause table = tables.get(0);
        Assert.assertEquals("a", table.getName());
        Assert.assertEquals("a", table.getAlias());
        
        List<JoinClause> joins = table.getJoinClauses();
        Assert.assertNotNull(joins);
        
        TableReferenceClause tableJoin = joins.get(0).getTable();
        Assert.assertEquals("b", tableJoin.getName());
        Assert.assertEquals("b", tableJoin.getAlias());
        
        Assert.assertEquals(JoinClause.JoinType.NATURAL, joins.get(0).getType());
        
        String on = joins.get(0).getCondition();
        Assert.assertNotNull(on);
        
        Assert.assertEquals("a.a1 = b.b1", on);
        
        String usingColumn = joins.get(0).getUsingColumns();
        Assert.assertNull(usingColumn);
    }
    
    @Test
    public void parseInnerJoinQuery()
    {
        final QueryClause queryClause0 = QueryFormat.parseSelectQuery("SELECT * FROM a as a INNER JOIN b as b ON a.a1 = b.b1");
        
        Assert.assertNotNull(queryClause0);
        
        Assert.assertTrue(queryClause0 instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause0;
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        FromClause fromClause = selectClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        List<TableReferenceClause> tables = fromClause.getTables();
        Assert.assertEquals(1, tables.size());
        
        TableReferenceClause table = tables.get(0);
        Assert.assertEquals("a", table.getName());
        Assert.assertEquals("a", table.getAlias());
        
        List<JoinClause> joins = table.getJoinClauses();
        Assert.assertNotNull(joins);
        
        TableReferenceClause tableJoin = joins.get(0).getTable();
        Assert.assertEquals("b", tableJoin.getName());
        Assert.assertEquals("b", tableJoin.getAlias());
        
        Assert.assertEquals(JoinClause.JoinType.INNER, joins.get(0).getType());
        
        String on = joins.get(0).getCondition();
        Assert.assertNotNull(on);
        
        Assert.assertEquals("a.a1 = b.b1", on);
        
        String usingColumn = joins.get(0).getUsingColumns();
        Assert.assertNull(usingColumn);
    }
    
    @Test
    public void parseNoneJoinQuery()
    {
        final QueryClause queryClause0 = QueryFormat.parseSelectQuery("SELECT * FROM a as a JOIN b as b ON a.a1 = b.b1");
        
        Assert.assertNotNull(queryClause0);
        
        Assert.assertTrue(queryClause0 instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause0;
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        FromClause fromClause = selectClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        List<TableReferenceClause> tables = fromClause.getTables();
        Assert.assertEquals(1, tables.size());
        
        TableReferenceClause table = tables.get(0);
        Assert.assertEquals("a", table.getName());
        Assert.assertEquals("a", table.getAlias());
        
        List<JoinClause> joins = table.getJoinClauses();
        Assert.assertNotNull(joins);
        
        TableReferenceClause tableJoin = joins.get(0).getTable();
        Assert.assertEquals("b", tableJoin.getName());
        Assert.assertEquals("b", tableJoin.getAlias());
        
        Assert.assertEquals(JoinClause.JoinType.NONE, joins.get(0).getType());
        
        String on = joins.get(0).getCondition();
        Assert.assertNotNull(on);
        
        Assert.assertEquals("a.a1 = b.b1", on);
        
        String usingColumn = joins.get(0).getUsingColumns();
        Assert.assertNull(usingColumn);
    }
    
    @Test
    public void parseUsingJoinQuery()
    {
        final QueryClause queryClause0 = QueryFormat.parseSelectQuery("SELECT * FROM a as a JOIN b as b using (a1, b1)");
        
        Assert.assertNotNull(queryClause0);
        
        Assert.assertTrue(queryClause0 instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause0;
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        FromClause fromClause = selectClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        List<TableReferenceClause> tables = fromClause.getTables();
        Assert.assertEquals(1, tables.size());
        
        TableReferenceClause table = tables.get(0);
        Assert.assertEquals("a", table.getName());
        Assert.assertEquals("a", table.getAlias());
        
        List<JoinClause> joins = table.getJoinClauses();
        Assert.assertNotNull(joins);
        
        TableReferenceClause tableJoin = joins.get(0).getTable();
        Assert.assertEquals("b", tableJoin.getName());
        Assert.assertEquals("b", tableJoin.getAlias());
        
        Assert.assertEquals(JoinClause.JoinType.NONE, joins.get(0).getType());
        
        String on = joins.get(0).getCondition();
        Assert.assertNull(on);
        
        String usingColumn = joins.get(0).getUsingColumns();
        Assert.assertNotNull(usingColumn);
        Assert.assertEquals("(a1, b1)", usingColumn);
    }
}
