package np.com.smart.sql.query;

import np.com.ngopal.smart.sql.query.QueryTemplateCoding;
import np.com.ngopal.smart.sql.query.QueryValidation;
import np.com.ngopal.smart.sql.query.exceptions.TemplateException;
import np.com.ngopal.smart.sql.query.exceptions.ValidationException;
import np.com.ngopal.smart.sql.query.templates.QueryTemplate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ValidationTest {
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }    
    
    @Test
    public void validateBulkInsert() {
        try {        
            QueryValidation.validateQuery("insert into triangle(sidea,sideb) values(10,1),(10,2),(10,3),(10,4),(10,5),(10,6),(10,7),(10,8),(10,9),(10,10)");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateInsert() {
        try {        
            QueryValidation.validateQuery("insert into triangle(sidea,sideb) values(10,(1))");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateSelect0() {
        try {        
            QueryValidation.validateQuery("SELECT ALL * FROM a as a LEFT JOIN b as b ON a.a1 = b.b1");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateSelect1() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM a INTO DUMPFILE INTO OUTFILE");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"INTO OUTFILE\" and \"INTO DUMPFILE\".".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect2() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM a INTO DUMPFILE INTO");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"INTO\" and \"INTO DUMPFILE\".".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect3() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM a INTO OUTFILE INTO");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"INTO\" and \"INTO OUTFILE\".".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect4() {
        try {        
            QueryValidation.validateQuery("SELECT * WHERE  FROM");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: The \"WHERE\" clause should come before the \"FROM\" clause (2 < 1). (Need to replace with clause name on \"WHERE\" and \"FROM\".)".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect5() {
        try {        
            QueryValidation.validateQuery("SELECT *   FROM WHERE PROCEDURE LIMIT");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: The \"PROCEDURE\" clause should come before the \"LIMIT\" clause (7 < 6). (Need to replace with clause name on \"PROCEDURE\" and \"LIMIT\".)".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect6() {
        try {        
            QueryValidation.validateQuery("SELECT *   FROM WHERE FOR UPDATE LIMIT");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: The \"FOR UPDATE\" clause should come before the \"LIMIT\" clause (8 < 6). (Need to replace with clause name on \"FOR UPDATE\" and \"LIMIT\".)".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect7() {
        try {        
            QueryValidation.validateQuery("SELECT *   FROM WHERE FOR   UPDATE  LOCK IN   SHARE  MODE");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"FOR UPDATE\" and \"LOCK IN SHARE MODE\".".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect8() {
        try {        
            QueryValidation.validateQuery("SELECT *   FROM WHERE FROM");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"FROM\" and \"FROM\".".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect9() {
        try {        
            QueryValidation.validateQuery("SELECT ALL DISTINCT * FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"ALL\" and \"DISTINCT\".".equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT DISTINCT * FROM a");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT DISTINCTROW DISTINCT * FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"DISTINCT\" and \"DISTINCTROW\".".equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelect10() {
        try {        
            QueryValidation.validateQuery("SELECT * SQL_CACHE SQL_NO_CACHE FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Incorrect usage of \"SQL_CACHE\" and \"SQL_NO_CACHE\".".equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT DISTINCT * SQL_NO_CACHE FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateSelectColumns0() {
        try {        
            QueryValidation.validateQuery("SELECT SQL_CACHE FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R6.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT SQL_CACH * FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1_0.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT  * SQL_CACH FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT ALL * SQL_CACHE FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * as b FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT id + 5 b, c f  FROM a");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT id as b j, c + 1 FROM a");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateFrom0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R8.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM a b");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM a b c");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateWhere0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s where  ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R9.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM a b wheRE b = 1");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateGroupBy0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s group by  ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R10.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM a b group by a, v");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
//        try {        
//            QueryValidation.validateQuery("SELECT * FROM s group by asc ");
//            Assert.fail();
//        } catch (ValidationException ex) {
//            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
//        }
//        
//        try {        
//            QueryValidation.validateQuery("SELECT * FROM s group by asc desc ");
//            Assert.fail();
//        } catch (ValidationException ex) {
//            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
//        }
//        
//        try {        
//            QueryValidation.validateQuery("SELECT * FROM s group by a asc, v v ");
//            Assert.fail();
//        } catch (ValidationException ex) {
//            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
//        }
    }
    
    
    @Test
    public void validateHaving0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s group by a having  ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R11.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM a b group by a having b = 1");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    
    @Test
    public void validateOrderBy0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s order by  ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R12.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM a b order by a, v");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s order by asc ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
        
//        try {        
//            QueryValidation.validateQuery("SELECT * FROM s group by asc desc ");
//            Assert.fail();
//        } catch (ValidationException ex) {
//            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
//        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s order by a asc, v v ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R1.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateLimity0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s limit ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R13.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s limit 1 1");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R13.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s limit 1");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s limit 1,  2");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s limit 1 offset  2");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s limit 1 offset ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R13.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateProcedure0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s procedure ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R14.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s procedure ()");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R14.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s procedure a()");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s procedure asd(fdf");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R14.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateInto0() {
        try {        
            QueryValidation.validateQuery("SELECT * FROM s INTO ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R15.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s INTO s");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s INTO OUTFILE ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R15.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s INTO OUTFILE 's'");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s INTO DUMPFILE 'ss ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue("ErrorMessage: Syntax error ‘'’ (identifier) is not a valid input at this position.".equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("SELECT * FROM s INTO DUMPFILE 's'");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateInsert0() {
        try {        
            QueryValidation.validateQuery("insert LOW_PRIORI table(a) values(b) ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(String.format(QueryValidation.ERROR_MEESAGE__INSERT_R4, "LOW_PRIORI").equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("insert LOW_PRIORITY table(a) values(b) ");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("insert ignore table(a) values(b) ");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateInsert1() {
        try {        
            QueryValidation.validateQuery("INSERT INTO ACTOR  VALUES  (NULL,'NAME','LASTNAME',now());");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateInsert2() {
        try {        
            QueryValidation.validateQuery("INSERT INT ACTOR(actor_id,first_name, last_name,last_update)  VALUES  (NULL,'NAME','LASTNAME',now());");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(String.format(QueryValidation.ERROR_MEESAGE__INSERT_R4, "INT").equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateInsert3() {
        try {        
            QueryValidation.validateQuery("INSERT INTO   actor set actor_id=NULL , first_name= 'asdf',last_name = 'lsdfa', last_update= now();");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateDelete0() {
        try {        
            QueryValidation.validateQuery("delete LOW_PRIORI a from s ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__DELETE_R4.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("delete LOW_PRIORI from s ");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("delete ignore from s limit 1, 2");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R13.equals(ex.getMessage()));
        }
    }
    
    @Test
    public void validateUpdate0() {
        try {        
            QueryValidation.validateQuery("update LOW_PRIORI a SET ");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__UPDATE_R1.equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("update a SET b=1 LOW_PRIORITY");
        } catch (ValidationException ex) {  
            Assert.assertTrue("ErrorMessage: The \"SET\" clause come after \"LOW_PRIORITY\" clause.".equals(ex.getMessage()));
        }
        
        try {        
            QueryValidation.validateQuery("update ignore s set a = 1 limit 1, 2");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertTrue(QueryValidation.ERROR_MEESAGE__SELECT_R13.equals(ex.getMessage()));
        }
    }
    
    
    @Test
    public void validateCreateTable0() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t (c CHAR(20) CHARACTER SET utf8 COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable1() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t (c int CHARACTER SET utf8 COLLATE utf8_bin);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c"), ex.getMessage());
        }
    }
    
    @Test
    public void validateCreateTable2() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t (c VARCHAR(200) );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable3() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t1 (c int , key name(c) ) ENGINE =innodb;");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable4() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t1 (c int ,index name(c) ) ENGINE =innodb;");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable5() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t1 ( c2 BOOLEAN  ) ENGINE =innodb;");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable6() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE `ZTEMP_visitor_192480496` (\n" +
                "  `ID` int(25) NOT NULL AUTO_INCREMENT,\n" +
                "  `ipadress` bigint(25) DEFAULT NULL,\n" +
                "  `cross_domain` int(15) DEFAULT NULL,\n" +
                "  `visitorID` varchar(40) NOT NULL,\n" +
                "  `variationID` int(11) DEFAULT NULL,\n" +
                "  `testid` int(11) DEFAULT NULL,\n" +
                "  `clientID` int(11) DEFAULT NULL,\n" +
                "  `projectID` int(11) DEFAULT NULL,\n" +
                "  `clickedElement` varchar(500) DEFAULT NULL,\n" +
                "  `timeanddate` datetime NOT NULL,\n" +
                "  `urlpath` varchar(760) NOT NULL,\n" +
                "  `mobile_browser` tinyint(1) DEFAULT NULL,\n" +
                "  `referrer` varchar(200) NOT NULL,\n" +
                "  `countrycode` varchar(5) NOT NULL,\n" +
                "  `country` varchar(35) NOT NULL,\n" +
                "  `screenwidth` int(11) DEFAULT NULL,\n" +
                "  `contentGoal` varchar(255) DEFAULT NULL,\n" +
                "  `vstTest` int(3) DEFAULT NULL,\n" +
                "  `transID` int(10) NOT NULL,\n" +
                "  `transTotValue` int(10) NOT NULL,\n" +
                "  `itemName` varchar(350) NOT NULL,\n" +
                "  `itemID` int(10) NOT NULL,\n" +
                "  `amount` int(10) NOT NULL,\n" +
                "  `valExVat` int(10) NOT NULL,\n" +
                "  `category` varchar(1500) NOT NULL,\n" +
                "  `trans` tinyint(1) NOT NULL DEFAULT '0',\n" +
                "  `websiteID` bigint(20) DEFAULT NULL,\n" +
                "  `uid` bigint(20) DEFAULT NULL,\n" +
                "  `PageviewID` bigint(20) DEFAULT NULL,\n" +
                "  `page_url_crc32` int(10) unsigned DEFAULT NULL,\n" +
                "  `page_url` varchar(760) DEFAULT NULL,\n" +
                "  `categoryid` bigint(20) unsigned DEFAULT NULL,\n" +
                "  `eventid` bigint(20) unsigned DEFAULT NULL,\n" +
                "  `productid` bigint(20) unsigned DEFAULT NULL,\n" +
                "  `sessionid` bigint(20) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`ID`),\n" +
                "  KEY `cross_domain` (`cross_domain`),\n" +
                "  KEY `testID` (`testid`),\n" +
                "  KEY `projectID` (`projectID`),\n" +
                "  KEY `variationID` (`variationID`),\n" +
                "  KEY `visitorID` (`visitorID`),\n" +
                "  KEY `timeanddate` (`timeanddate`),\n" +
                "  KEY `transID` (`transID`),\n" +
                "  KEY `itemID` (`itemID`),\n" +
                "  KEY `trans` (`trans`),\n" +
                "  KEY `clientID` (`clientID`,`country`),\n" +
                "  KEY `visitorID_2` (`visitorID`,`country`),\n" +
                "  KEY `page_url_crc32` (`page_url_crc32`,`websiteID`),\n" +
                "  KEY `PageviewID` (`PageviewID`,`variationID`,`timeanddate`)\n" +
                ") ENGINE=TokuDB AUTO_INCREMENT=215650107 DEFAULT CHARSET=latin1;");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable7() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE `transactions` (\n" +
"  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,\n" +
"  `pageviewid` BIGINT(20) NOT NULL,\n" +
"  `transid` BIGINT(20) UNSIGNED NOT NULL,\n" +
"  `productid` BIGINT(20) UNSIGNED NOT NULL,\n" +
"  `timeanddate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n" +
"  `uid` BIGINT(20) UNSIGNED DEFAULT NULL,\n" +
"  `sessionid` BIGINT(20) DEFAULT NULL,\n" +
"  `val` INT(11) DEFAULT NULL,\n" +
"  `valExVat` INT(11) DEFAULT NULL,\n" +
"  PRIMARY KEY (`id`),\n" +
"  KEY `productid` (`productid`),\n" +
"  KEY `timeanddate` (`timeanddate`),\n" +
"  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`productid`) REFERENCES `products` (`id`)\n" +
") ENGINE=INNODB AUTO_INCREMENT=215650107 DEFAULT CHARSET=latin1");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateCreateTable8() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t (c int primary );");
            Assert.fail();
        } catch (ValidationException ex) {
        }
    }
    
    @Test
    public void validateCreateTable10() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t (c varchar CHARACTER SET utf8 COLLATE utf8_bin);");
            Assert.fail();
        } catch (ValidationException ex) {
        }
    }
    
    @Test
    public void validateCreateTable11() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t1 (c int  name(c) ) ENGINE =innodb;");
            Assert.fail();
        } catch (ValidationException ex) {
        }
    }
    
    @Test
    public void validateCreateTable12() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 ( c2 enum   ) ENGINE =innodb;");
            Assert.fail();
        } catch (ValidationException ex) {
        }
    }
    
    @Test
    public void validateCreateTable13() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE abc t2 ( c2 enum('a','abc')   ) ENGINE =innodb");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(QueryValidation.ERROR_MEESAGE__CREATE_T_R2_1, ex.getMessage());
        }
    }
    
    @Test
    public void validateBITDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BIT);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BIT (2));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BIT (2);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    @Test
    public void validateBOOLEANDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BOOLEAN);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BOOLEAN (2));");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BOOLEAN (2);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    @Test
    public void validateBOOLDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BOOL);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BOOL (2));");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BOOL (2);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    @Test
    public void validateTINYINTDataType() {
        testLengthDataType("TINYINT");
    }
    
    @Test
    public void validateSMALLINTDataType() {
        testLengthDataType("SMALLINT");
    }
    
    @Test
    public void validateMEDIUMINTDataType() {
        testLengthDataType("MEDIUMINT");
    }
    
    @Test
    public void validateINTDataType() {
        testLengthDataType("INT");
    }
    
    @Test
    public void validateINTEGERDataType() {
        testLengthDataType("INTEGER");
    }
    
    @Test
    public void validateBIGINTDataType() {
        testLengthDataType("BIGINT");
    }
    
    
    @Test
    public void validateREALDataType() {
        testDecimalsDataType("REAL");
    }
    
    @Test
    public void validateDOUBLEDataType() {
        testDecimalsDataType("DOUBLE");
    }
    
    @Test
    public void validateFLOATDataType() {
        testDecimalsDataType("FLOAT");
    }
    
    
    @Test
    public void validateDECIMALDataType() {
        testLengthOrDecimalsDataType("DECIMAL");
    }
    
    @Test
    public void validateNUMERICDataType() {
        testLengthOrDecimalsDataType("NUMERIC");
    }
    
    
    @Test
    public void validateDATEDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 DATE ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 DATE(10));");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 DATE);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateYEARDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 YEAR ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 YEAR(10));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 YEAR );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validateTIMEDataType() {
        testDateDataType("TIME");
    }
    
    
    @Test
    public void validateTIMESTAMPDataType() {
        testDateDataType("TIMESTAMP");
    }
    
    @Test
    public void validateDATETIMEDataType() {
        testDateDataType("DATETIME");
    }
    
    @Test
    public void validateCHAREDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char(2) binary);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char(2) binar);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char(2) CHARACTER SET utf8 COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char(2) COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 char(2) COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    @Test
    public void validateVARCHAREDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar(2) binary);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar(2) binar);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar(2) CHARACTER SET utf8 COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar(2) COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 varchar(2) COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    @Test
    public void validateBINARYDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY(2));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY(2) binar);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY(2) CHARACTER SET utf8 COLLATE utf8_bin);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY(2) COLLATE utf8_bin);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BINARY(2) COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    @Test
    public void validatVARBINARYDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY(2));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY(2) binar);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY(2) CHARACTER SET utf8 COLLATE utf8_bin);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY(2) COLLATE utf8_bin);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 VARBINARY(2) COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    @Test
    public void validatTINYBLOBDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 TINYBLOB ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 TINYBLOB);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validatBLOBDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BLOB ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 BLOB);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validatMEDIUMBLOBDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 MEDIUMBLOB ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 MEDIUMBLOB);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validatLONGBLOBDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 LONGBLOB ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 LONGBLOB);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void validatTINYTEXTDataType() {
        testTextDataType("TINYTEXT");
    }
    
    @Test
    public void validatTEXTDataType() {
        testTextDataType("TEXT");
    }
    
    @Test
    public void validatMEDIUMTEXTDataType() {
        testTextDataType("MEDIUMTEXT");
    }
    
    @Test
    public void validatLONGTEXTDataType() {
        testTextDataType("LONGTEXT");
    }
    
    
    @Test
    public void validatENUMDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM CHARACTER);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM(2, 3, 4));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM(2));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM(\"2\", '3') CHARACTER SET utf8 COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM(2) COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 ENUM(2) COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    @Test
    public void validatSETDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET CHARACTER);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET(2, 3, 4));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET(2.1));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET(\"2\", '3 3') CHARACTER SET utf8 COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET(2) COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 SET(2) COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    @Test
    public void validatJSONDataType() {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 JSON);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 JSON(2));");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 JSON BINARY);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    public void testTextDataType(String type) {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + ");");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + "(2));");
        } catch (ValidationException ex) {            
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + "(2) binary);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " CHARACTER SET utf8 COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " COLLATE utf8_bin);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " COLLATE utf8_bin binary);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
    }
    
    
    
    
    public void testDateDataType(String type) {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + "(10));");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    
    @Test
    public void testBug0() {
        
        try {        
            QueryValidation.validateQuery("select group_concat(distinct concat(`c`.`name`,': ',(select group_concat(`f`.`title` order by `f`.`title` ASC separator ', ') from `film` `f` join `film_category` `fc` on((`f`.`film_id` = `fc`.`film_id`)) join `film_actor` `fa` on((`f`.`film_id` = `fa`.`film_id`)) where ((`fc`.`category_id` = `c`.`category_id`) and (`fa`.`actor_id` = `a`.`actor_id`)))) order by `c`.`name` ASC separator '; ') AS `film_info` from `actor` `a` left join `film_actor` `fa` on((`a`.`actor_id` = `fa`.`actor_id`)) left join `film_category` `fc` on((`fa`.`film_id` = `fc`.`film_id`)) left join `category` `c` on((`fc`.`category_id` = `c`.`category_id`)) group by `a`.`actor_id`,`a`.`first_name`,`a`.`last_name`;");
        } catch (ValidationException ex) {
            Assert.fail();
        }

    }
    
    @Test
    public void testBug1() {
        
        try {        
            QueryValidation.validateQuery("select `cu`.`customer_id` AS `ID`,concat(`cu`.`first_name`,_utf8' ',`cu`.`last_name`) AS `name`,`a`.`address` AS `address`,`a`.`postal_code` AS `zip code`,`a`.`phone` AS `phone`,`city`.`city` AS `city`,`country`.`country` AS `country`,if(`cu`.`active`,_utf8'active',_utf8'') AS `notes`,`cu`.`store_id` AS `SID` from `customer` `cu` join `address` `a` on((`cu`.`address_id` = `a`.`address_id`)) join `city` on((`a`.`city_id` = `city`.`city_id`)) join `country` on((`city`.`country_id` = `country`.`country_id`))");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }

    }
    
    
    @Test
    public void testBug2() {
        
        try {        
            QueryValidation.validateQuery("select `film`.`film_id` AS `FID`,`film`.`title` AS `title`,`film`.`description` AS `description`,`category`.`name` AS `category`,`film`.`rental_rate` AS `price`,`film`.`length` AS `length`,`film`.`rating` AS `rating`,group_concat(concat(`actor`.`first_name`,_utf8' ',`actor`.`last_name`) separator ', ') AS `actors` from `category` left join `film_category` on(`category`.`category_id` = `film_category`.`category_id`) left join `film` on(`film_category`.`film_id` = `film`.`film_id`) join `film_actor` on(`film`.`film_id` = `film_actor`.`film_id`) join `actor` on(`film_actor`.`actor_id` = `actor`.`actor_id`) group by `film`.`film_id`,`category`.`name`");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }

    }
    
    
    @Test
    public void testBug3() {
        
        try {        
            QueryValidation.validateQuery("select `c`.`name` AS `category`,sum(`p`.`amount`) AS `total_sales` from `payment` `p` join `rental` `r` on((`p`.`rental_id` = `r`.`rental_id`)) join `inventory` `i` on((`r`.`inventory_id` = `i`.`inventory_id`)) join `film` `f` on((`i`.`film_id` = `f`.`film_id`)) join `film_category` `fc` on((`f`.`film_id` = `fc`.`film_id`)) join `category` `c` on((`fc`.`category_id` = `c`.`category_id`)) group by `c`.`name` order by `total_sales` desc");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }

    }
    
    
    @Test
    public void testBug4() {
        
        try {        
            QueryValidation.validateQuery("select concat(`c`.`city`,_utf8',',`cy`.`country`) AS `store`,concat(`m`.`first_name`,_utf8' ',`m`.`last_name`) AS `manager`,sum(`p`.`amount`) AS `total_sales` from `payment` `p` join `rental` `r` on((`p`.`rental_id` = `r`.`rental_id`)) join `inventory` `i` on((`r`.`inventory_id` = `i`.`inventory_id`)) join `store` `s` on((`i`.`store_id` = `s`.`store_id`)) join `address` `a` on((`s`.`address_id` = `a`.`address_id`)) join `city` `c` on((`a`.`city_id` = `c`.`city_id`)) join `country` `cy` on((`c`.`country_id` = `cy`.`country_id`)) join `staff` `m` on((`s`.`manager_staff_id` = `m`.`staff_id`)) group by `s`.`store_id` order by `cy`.`country`,`c`.`city`");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }

    }
    
    
    @Test
    public void testBug5() {
        
        try {        
            QueryValidation.validateQuery("select `s`.`staff_id` AS `ID`,concat(`s`.`first_name`,_utf8' ',`s`.`last_name`) AS `name`,`a`.`address` AS `address`,`a`.`postal_code` AS `zip code`,`a`.`phone` AS `phone`,`city`.`city` AS `city`,`country`.`country` AS `country`,`s`.`store_id` AS `SID` from `staff` `s` join `address` `a` on((`s`.`address_id` = `a`.`address_id`)) join `city` on((`a`.`city_id` = `city`.`city_id`)) join `country` on((`city`.`country_id` = `country`.`country_id`))");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }
    
    
    @Test
    public void testBug6() {
        
        try {        
            QueryValidation.validateQuery("SELECT sum(d.Amount) as DispatchAmount, sum(d.ConfirmedAmount) as DispatchConfirmedAmount, (select sum(d.CarrierTotalAmount) from dispatch d where  d.Company_Id = companyId and d.PickupAppointment >= fromDate and d.PickupDeliveryAppointment <= toDate )as CarriertotalAmount, sum((d.CarrierTotalAmount * d.DispatchPCT) / 100) as DispatchFee, (select sum(mt.Amount ) from  maintenance mt where (mt.Truck_Id IN (select t.Id from truck t where t.Company_Id = companyId)) and mt.MaintenanceDate >= fromDate and mt.MaintenanceDate <= toDate) as TruckMaintenance, (select sum(mt.Amount ) from  maintenance mt where (mt.Trailer_Id IN (select t.Id from trailer t where t.Company_Id = companyId)) and mt.MaintenanceDate >= fromDate and mt.MaintenanceDate <= toDate) as TrailerMaintenance,  	(select sum(p.PaidAmount) from payroll p where (p.Driver_Id IN (select dr.Id from driver dr where dr.Company_Id = companyId)) and p.PaidDate >= fromDate and p.PaidDate <= toDate) as PayrollAmount,  	(select sum(ex.Amount) from expenses ex where (ex.Dispatch_Id IN (select d.Id from dispatch where d.Company_Id = companyId and d.PickupAppointment >= fromDate and d.PickupDeliveryAppointment <= toDate )) and ex.PaidDate >= fromDate and ex.PaidDate <= toDate)  as Expenses,  	(select sum(fl.TotalAmount) from fuellog fl where fl.Company_Id = companyId and fl.FilledDate >= fromDate and fl.FilledDate <= toDate) as FuelAmount,  	sum(d.Miles) as TotalMiles,  	sum(d.LoadMiles) as LoadedMiles,  	sum(d.DeadAheadMiles) as DeadHeadedMiles,  	(select sum(da.advanceAmount) as ce from driveradvance da where (da.Driver_Id IN (select dr.Id from driver dr where dr.Company_Id = companyId)) and da.PaidDate >= fromDate and da.PaidDate <= toDate) as DriverAdvances,  	(select sum(ex.Amount) from expenses ex where (ex.Dispatch_Id IN (select d.Id from dispatch where d.Company_Id = companyId and d.PickupAppointment >= fromDate and d.PickupDeliveryAppointment <= toDate )) and ex.PaidDate >= fromDate and ex.PaidDate <= toDate and ex.PaidByDriver_Id IS NOT NULL and ex.Payroll_Id IS NOT NULL)  as ExpensesPaidbyDriver,  	(select sum(ce.Amount) from companyexpenses ce where (ce.Company_Id = companyId and ce.ExpenseDate >= fromDate and ce.ExpenseDate <= toDate)) as CompanyExpenses FROM dispatch d WHERE d.Company_Id = companyId and d.PickupAppointment >= fromDate and d.PickupDeliveryAppointment <= toDate;");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }
    
    @Test
    public void testBug7() {
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE `film_text_1` (`film_id` smallint(6) NOT NULL, `title` varchar(255) NOT NULL, `description` text(14), test blob(10), test1 DATETIME(5), PRIMARY KEY (`film_id`), FULLTEXT KEY `idx_title_description` (`title`,`description`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        } catch (ValidationException ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }
    
    
    @Test
    public void testStart0() {
        
        try {        
            QueryValidation.validateQuery("start * from emp");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(QueryValidation.ERROR_MEESAGE__START_R0, ex.getMessage());
        }
    }
    
    @Test
    public void testStart1() {
        
        try {        
            QueryValidation.validateQuery("start transaction");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    
    // @TODO
    //@Test
    public void testSelect0() {
        
        try {        
            QueryValidation.validateQuery("select * from   city   where ab cd=''  ; ");
            Assert.fail();
        } catch (ValidationException ex) { 
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__SELECT_R17, "cd"), ex.getMessage());
        }
    }
    
    
    @Test
    public void testSelect1() {
        
        try {        
            QueryValidation.validateQuery("select *,* from emp;");
            Assert.fail();
        } catch (ValidationException ex) { 
            Assert.assertEquals(QueryValidation.ERROR_MEESAGE__SELECT_R1_0, ex.getMessage());
        }
    }
    
    @Test
    public void testCreateUser() {
        try {        
            QueryValidation.validateQuery("CREATE USER `test`@`null` IDENTIFIED BY 'test' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 PASSWORD EXPIRE NEVER;");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    @Test
    public void testExtends() {
        
        try {        
            QueryValidation.validateQuery("explain select * from emp;");
        } catch (ValidationException ex) { 
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("explain  EXTENDED  select * from emp;");
        } catch (ValidationException ex) { 
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("explain PARTITIONS   select * from emp;");
        } catch (ValidationException ex) { 
            Assert.fail();
        }
    }
    
    @Test
    public void testGrant() {
        
        try {        
            QueryValidation.validateQuery("grant all on `smartMySQLSchemaDesign`.* to sudheer@'%';");
        } catch (ValidationException ex) { 
            Assert.fail();
        }
    }
    
    @Test
    public void testFlush() {
        
        try {        
            QueryValidation.validateQuery("flush PRIVILEGES ;");
        } catch (ValidationException ex) { 
            Assert.fail();
        }
    }
    
    
    @Test
    public void testStartTransaction() {
        
        try {        
            QueryValidation.validateQuery("start TRANSACTION ;");
        } catch (ValidationException ex) { 
            Assert.fail();
        }
    }
    
    
    @Test
    public void testCallTemplate() {
        
        try {        
            QueryTemplate template = QueryTemplateCoding.makeQueryTemplate("call functionName(1, 2)");
            Assert.assertNotNull(template);
            Assert.assertEquals("CALL functionName(NNN, NNN)", template.getTemplate());
        } catch (TemplateException ex) { 
            Assert.fail();
        }
        
        try {        
            QueryTemplate template = QueryTemplateCoding.makeQueryTemplate("call functionName(1, '2')");
            Assert.assertNotNull(template);
            Assert.assertEquals("CALL functionName(NNN, NNN)", template.getTemplate());
        } catch (TemplateException ex) { 
            Assert.fail();
        }
        
        try {        
            QueryTemplate template = QueryTemplateCoding.makeQueryTemplate("call functionName(1, 'asdasd')");
            Assert.assertNotNull(template);
            Assert.assertEquals("CALL functionName(NNN, 'XXXXXX')", template.getTemplate());
        } catch (TemplateException ex) { 
            Assert.fail();
        }
        
               
    }
    
    @Test
    public void testSubQueryTemplate() {
        try {        
            QueryTemplate template = QueryTemplateCoding.makeQueryTemplate("insert into payment select null, customer_id, staff_id, rental_id, amount, payment_date, last_update from payment");
            Assert.assertNotNull(template);
            Assert.assertEquals("INSERT INTO payment SELECT null, customer_id, staff_id, rental_id, amount, payment_date, last_update FROM payment", template.getTemplate());
        } catch (TemplateException ex) { 
            Assert.fail();
        } 
        
        try {        
            QueryTemplate template = QueryTemplateCoding.makeQueryTemplate("select id, (select * from a where a > 10), b from c");
            Assert.assertNotNull(template);
            Assert.assertEquals("SELECT id, (SELECT * FROM a WHERE a > NNN), b FROM c", template.getTemplate());
        } catch (TemplateException ex) { 
            Assert.fail();
        } 
        
        
        try {        
            QueryTemplate template = QueryTemplateCoding.makeQueryTemplate(
                "SELECT * FROM ((SELECT information_schema.`COLUMNS`.table_schema `db_name`, information_schema.`COLUMNS`.table_name `table_name` ,information_schema.TABLES.ENGINE ENGINE , CONCAT(        column_name,        '$$$$',        column_type ,                 '$$$$' , if(information_schema.COLUMNS.IS_NULLABLE,'no', 'NOT NULL'), '$$$$', IF(information_schema.COLUMNS.`EXTRA`='auto_increment', 'auto_increment', ''), '$$$$' , IFNULL(information_schema.COLUMNS.COLUMN_DEFAULT,'')  ,        '$$$$' , IFNULL(information_schema.COLUMNS.CHARACTER_SET_NAME,'') ,        '$$$$' , IFNULL(information_schema.COLUMNS.COLLATION_NAME,'')                ) `column_name`, 1 `type`     FROM      information_schema.`COLUMNS`     JOIN information_schema.`TABLES`       ON information_schema.`COLUMNS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME`         AND information_schema.`COLUMNS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA`   WHERE information_schema.`TABLES`.`TABLE_TYPE` !='VIEW' ORDER BY information_schema.`COLUMNS`.`ORDINAL_POSITION`) UNION ALL(SELECT information_schema.`STATISTICS`.table_schema `db_name`, information_schema.`STATISTICS`.table_name `table_name`, '' ENGINE, CONCAT(INDEX_TYPE, ' ', index_name, '(', GROUP_CONCAT(column_name ORDER BY seq_in_index ), ')' ) `column_name`, 2 `type` FROM information_schema.`STATISTICS` JOIN information_schema.`TABLES` ON information_schema.`STATISTICS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME` AND information_schema.`STATISTICS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA` WHERE information_schema.`TABLES`.`TABLE_TYPE` !='VIEW' GROUP BY information_schema.`STATISTICS`.table_schema, information_schema.`STATISTICS`.table_name, index_name) UNION ALL (SELECT  table_schema `db_name`, table_name `table_name` , '' ENGINE, CONCAT('CREATE\n" +
                "VIEW ', table_name, '\n" +
                "AS\n" +
                "', `VIEW_DEFINITION`) `column_name`, 3 `type` FROM  information_schema.`VIEWS`) UNION ALL (SELECT  routine_schema `db_name`, routine_name `table_name` , '' ENGINE, '' `column_name`, IF(    Routine_type='PROCEDURE',    4,    5  ) `type` FROM  information_schema.`ROUTINES`) UNION ALL (SELECT  trigger_schema `db_name`, trigger_name `table_name`,'' ENGINE,CONCAT('CREATE TRIGGER ', trigger_name, '\n" +
                "  ', action_timing, ' ', event_manipulation, '\n" +
                "  ON ', event_object_table, '\n" +
                "  FOR EACH ROW\n" +
                "', action_statement) `column_name`, 6 `type` FROM  information_schema.`TRIGGERS`) UNION ALL(SELECT  event_schema `db_name`, event_name `table_name`,'' ENGINE,CONCAT('CREATE EVENT ', `EVENT_NAME`, '\n" +
                "  ON SCHEDULE ', IF(`EVENT_TYPE` = 'ONE TIME', CONCAT('AT ''', `EXECUTE_AT`, '''\n" +
                "  '), CONCAT('EVERY ''', `INTERVAL_VALUE`, ''' ', `INTERVAL_FIELD`, '\n" +
                "  STARTS ''', `STARTS`, '''\n" +
                "  ', IF (`ENDS` is not null, CONCAT('ENDS ''', `ENDS`, '''\n" +
                "  '), ''))), 'DO\n" +
                "', `EVENT_DEFINITION`) `column_name`, 7 `type` FROM  information_schema.`EVENTS`) UNION ALL(SELECT  information_schema.`SCHEMATA`.`SCHEMA_NAME` `db_name`,'' `table_name`,'' ENGINE,'' `column_name`, 0 TYPE FROM  information_schema.`SCHEMATA`) UNION ALL (SELECT information_schema.`KEY_COLUMN_USAGE`.CONSTRAINT_SCHEMA db_name, information_schema.`KEY_COLUMN_USAGE`.TABLE_NAME table_name, '' ENGINE, CONCAT('CONSTRAINT `', information_schema.KEY_COLUMN_USAGE.CONSTRAINT_NAME, '` FOREIGN KEY (`', GROUP_CONCAT(information_schema.KEY_COLUMN_USAGE.COLUMN_NAME ORDER BY information_schema.`KEY_COLUMN_USAGE`.`ORDINAL_POSITION` ), '`) REFERENCES `', information_schema.KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME, '`(`', information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME, '`)' , CONCAT( IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='CASCADE',' ON UPDATE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='RESTRICT',' ON UPDATE RESTRICT', '')), IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='CASCADE',' ON DELETE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='RESTRICT',' ON DELETE RESTRICT', '')))) column_name, 8 TYPE FROM information_schema.KEY_COLUMN_USAGE LEFT JOIN information_schema.`REFERENTIAL_CONSTRAINTS` ON information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_NAME` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_NAME` AND information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_SCHEMA` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_SCHEMA` WHERE information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME IS NOT NULL GROUP BY information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_SCHEMA`, information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`) UNION ALL (SELECT T.table_schema `db_name`, T.table_name `table_name`, '' ENGINE, CONCAT( IFNULL(T.engine,''), '$$$$', IFNULL(CCSA.CHARACTER_SET_NAME,''), '$$$$', IFNULL(T.TABLE_COLLATION,''), '$$$$', IFNULL(T.`AUTO_INCREMENT`, ''), '$$$$', IFNULL(T.`AVG_ROW_LENGTH`,'')) `column_name`, 9 TYPE FROM information_schema.`TABLES` T JOIN information_schema.COLLATION_CHARACTER_SET_APPLICABILITY AS CCSA  ON (T.TABLE_COLLATION = CCSA.COLLATION_NAME) )) a ORDER BY 1, 2;");
            Assert.assertNotNull(template);
            Assert.assertEquals(
                "SELECT * FROM ((SELECT information_schema.`COLUMNS`.table_schema AS `db_name`, information_schema.`COLUMNS`.table_name AS `table_name`, information_schema.TABLES.ENGINE AS ENGINE, CONCAT( column_name, '$$$$', column_type , '$$$$' , if(information_schema.COLUMNS.IS_NULLABLE,'no', 'NOT NULL'), '$$$$', IF(information_schema.COLUMNS.`EXTRA`='auto_increment', 'auto_increment', ''), '$$$$' , IFNULL(information_schema.COLUMNS.COLUMN_DEFAULT,'') , '$$$$' , IFNULL(information_schema.COLUMNS.CHARACTER_SET_NAME,'') , '$$$$' , IFNULL(information_schema.COLUMNS.COLLATION_NAME,'') ) AS `column_name`, 1 AS `type` FROM information_schema.`COLUMNS` JOIN information_schema.`TABLES` ON information_schema.`COLUMNS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME` AND information_schema.`COLUMNS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA` WHERE information_schema.`TABLES`.`TABLE_TYPE` !='XXXXXX' ORDER BY information_schema.`COLUMNS`.`ORDINAL_POSITION`) UNION ALL (SELECT information_schema.`STATISTICS`.table_schema AS `db_name`, information_schema.`STATISTICS`.table_name AS `table_name`, '' AS ENGINE, CONCAT(INDEX_TYPE, ' ', index_name, '(', GROUP_CONCAT(column_name ORDER BY seq_in_index ), ')' ) AS `column_name`, 2 AS `type` FROM information_schema.`STATISTICS` JOIN information_schema.`TABLES` ON information_schema.`STATISTICS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME` AND information_schema.`STATISTICS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA` WHERE information_schema.`TABLES`.`TABLE_TYPE` !='XXXXXX' GROUP BY information_schema.`STATISTICS`.table_schema, information_schema.`STATISTICS`.table_name, index_name) UNION ALL (SELECT table_schema AS `db_name`, table_name AS `table_name`, '' AS ENGINE, CONCAT('CREATE " +
                "VIEW ', table_name, ' " +
                "AS " +
                "', `VIEW_DEFINITION`) AS `column_name`, 3 AS `type` FROM information_schema.`VIEWS`) UNION ALL (SELECT routine_schema AS `db_name`, routine_name AS `table_name`, '' AS ENGINE, '' AS `column_name`, IF( Routine_type='PROCEDURE', 4, 5 ) AS `type` FROM information_schema.`ROUTINES`) UNION ALL (SELECT trigger_schema AS `db_name`, trigger_name AS `table_name`, '' AS ENGINE, CONCAT('CREATE TRIGGER ', trigger_name, ' " +
                "  ', action_timing, ' ', event_manipulation, ' " +
                "  ON ', event_object_table, ' " +
                "  FOR EACH ROW " +
                "', action_statement) AS `column_name`, 6 AS `type` FROM information_schema.`TRIGGERS`) UNION ALL (SELECT event_schema AS `db_name`, event_name AS `table_name`, '' AS ENGINE, CONCAT('CREATE EVENT ', `EVENT_NAME`, ' " +
                "  ON SCHEDULE ', IF(`EVENT_TYPE` = 'ONE TIME', CONCAT('AT ''', `EXECUTE_AT`, ''' " +
                "  '), CONCAT('EVERY ''', `INTERVAL_VALUE`, ''' ', `INTERVAL_FIELD`, ' " +
                "  STARTS ''', `STARTS`, ''' " +
                "  ', IF (`ENDS` is not null, CONCAT('ENDS ''', `ENDS`, ''' " +
                "  '), ''))), 'DO " +
                "', `EVENT_DEFINITION`) AS `column_name`, 7 AS `type` FROM information_schema.`EVENTS`) UNION ALL (SELECT information_schema.`SCHEMATA`.`SCHEMA_NAME` AS `db_name`, '' AS `table_name`, '' AS ENGINE, '' AS `column_name`, 0 AS TYPE FROM information_schema.`SCHEMATA`) UNION ALL (SELECT information_schema.`KEY_COLUMN_USAGE`.CONSTRAINT_SCHEMA AS db_name, information_schema.`KEY_COLUMN_USAGE`.TABLE_NAME AS table_name, '' AS ENGINE, CONCAT('CONSTRAINT `', information_schema.KEY_COLUMN_USAGE.CONSTRAINT_NAME, '` FOREIGN KEY (`', GROUP_CONCAT(information_schema.KEY_COLUMN_USAGE.COLUMN_NAME ORDER BY information_schema.`KEY_COLUMN_USAGE`.`ORDINAL_POSITION` ), '`) REFERENCES `', information_schema.KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME, '`(`', information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME, '`)' , CONCAT( IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='CASCADE',' ON UPDATE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='RESTRICT',' ON UPDATE RESTRICT', '')), IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='CASCADE',' ON DELETE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='RESTRICT',' ON DELETE RESTRICT', '')))) AS column_name, 8 AS TYPE FROM information_schema.KEY_COLUMN_USAGE LEFT JOIN information_schema.`REFERENTIAL_CONSTRAINTS` ON information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_NAME` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_NAME` AND information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_SCHEMA` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_SCHEMA` WHERE information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME IS NOT NULL GROUP BY information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_SCHEMA`, information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`) UNION ALL (SELECT T.table_schema AS `db_name`, T.table_name AS `table_name`, '' AS ENGINE, CONCAT( IFNULL(T.engine,''), '$$$$', IFNULL(CCSA.CHARACTER_SET_NAME,''), '$$$$', IFNULL(T.TABLE_COLLATION,''), '$$$$', IFNULL(T.`AUTO_INCREMENT`, ''), '$$$$', IFNULL(T.`AVG_ROW_LENGTH`,'')) AS `column_name`, 9 AS TYPE FROM information_schema.`TABLES` AS T JOIN information_schema.COLLATION_CHARACTER_SET_APPLICABILITY AS CCSA ON (T.TABLE_COLLATION = CCSA.COLLATION_NAME))) AS a ORDER BY 1, 2", template.getTemplate());
        } catch (TemplateException ex) { 
            Assert.fail();
        }
    }
    
    
    @Test
    public void validateQueryWithInsideFrom() {
        try {        
            QueryValidation.validateQuery("select      table_name\n" +
                ",           index_type\n" +
                ",           min(column_names)      column_names\n" +
                ",           trim(',' FROM \n" +
                "                case index_type\n" +
                "                    when 'BTREE' then\n" +
                "                        replace(\n" +
                "                            -- report all but the last one\n" +
                "                            -- (the last one is the longest one)\n" +
                "                            substring_index(\n" +
                "                                group_concat(\n" +
                "                                    '`',index_name,'`'\n" +
                "                                    order by    column_count asc\n" +
                "                                    ,           non_unique   asc\n" +
                "                                    ,           index_name   desc\n" +
                "                                    separator   ',' \n" +
                "                                )\n" +
                "                            ,   ','\n" +
                "                            ,   count(*) - 1\n" +
                "                            )\n" +
                "                            -- get the first one\n" +
                "                            -- (the first one is the smallest unique one)\n" +
                "                        ,   concat(\n" +
                "                                '`'\n" +
                "                            ,   substring_index(\n" +
                "                                    group_concat(\n" +
                "                                        if( non_unique = 0\n" +
                "                                        ,   index_name\n" +
                "                                        ,   ''\n" +
                "                                        )\n" +
                "                                        order by    non_unique   asc\n" +
                "                                        ,           column_count asc\n" +
                "                                        ,           index_name   asc\n" +
                "                                        separator   ',' \n" +
                "                                    )\n" +
                "                                ,   ','\n" +
                "                                ,   1\n" +
                "                                )\n" +
                "                            ,   '`'\n" +
                "                            )\n" +
                "                        ,   ''\n" +
                "                        )\n" +
                "                    when 'HASH' then\n" +
                "                        substring_index(\n" +
                "                            group_concat(\n" +
                "                                '`',index_name,'`'\n" +
                "                                order by    non_unique   asc\n" +
                "                                ,           index_name   asc\n" +
                "                                separator   ',' \n" +
                "                            )\n" +
                "                        ,   ','\n" +
                "                        ,   1 - count(*)\n" +
                "                        )\n" +
                "                    when 'SPATIAL' then\n" +
                "                        substring_index(\n" +
                "                            group_concat(\n" +
                "                                '`',index_name,'`'\n" +
                "                                order by    index_name  asc\n" +
                "                                separator ',' \n" +
                "                            )\n" +
                "                        ,   ','\n" +
                "                        ,   1 - count(*) \n" +
                "                        )\n" +
                "                    else 'unexpected type - not implemented'\n" +
                "                end         \n" +
                "            )           redundant_indexes\n" +
                "from        (\n" +
                "            select      table_name\n" +
                "            ,           index_name\n" +
                "            ,           index_type\n" +
                "            ,           non_unique\n" +
                "            ,           count(seq_in_index) as column_count\n" +
                "            ,           group_concat(\n" +
                "                            if(seq_in_index=1,column_name,'')\n" +
                "                            separator ''\n" +
                "                        )                   as column_name\n" +
                "            ,           group_concat(\n" +
                "                            column_name\n" +
                "                            order by seq_in_index\n" +
                "                            separator ','\n" +
                "                        )                   as column_names\n" +
                "            from        information_schema.statistics s\n" +
                "            where       s.table_schema = schema()\n" +
                "            and         s.index_type  != 'FULLTEXT'\n" +
                "            group by    table_name\n" +
                "            ,           index_name\n" +
                "            ,           index_type\n" +
                "            ,           non_unique\n" +
                "            )           as s\n" +
                "group by    table_name  \n" +
                ",           index_type\n" +
                ",           if(index_type='HASH',column_names,column_name)\n" +
                "having      redundant_indexes != ''");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    private void testLengthDataType(String type) {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL    );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " (2) UNSIGNED);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " UNSIGNDE);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " UNSIGNED ZEROFILL);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL UNSIGNED );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    private void testDecimalsDataType(String type) {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " (  2 , 1) UNSIGNED);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " UNSIGNDE);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + "(2));");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " UNSIGNED ZEROFILL);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL UNSIGNED );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    private void testLengthOrDecimalsDataType(String type) {
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL   );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " (  2 , 1) UNSIGNED);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " UNSIGNDE);");
            Assert.fail();
        } catch (ValidationException ex) {
            Assert.assertEquals(String.format(QueryValidation.ERROR_MEESAGE__CREATE_T_R3, "c2"), ex.getMessage());
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + "(2  ));");            
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " UNSIGNED ZEROFILL);");
        } catch (ValidationException ex) {
            Assert.fail();
        }
        
        try {        
            QueryValidation.validateQuery("CREATE TABLE t2 (c2 " + type + " ZEROFILL UNSIGNED );");
        } catch (ValidationException ex) {
            Assert.fail();
        }
    }
    
    
}
