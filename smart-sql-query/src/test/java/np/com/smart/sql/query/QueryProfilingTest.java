package np.com.smart.sql.query;

import np.com.ngopal.smart.sql.query.QueryTemplateCoding;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.QueryValidation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import np.com.ngopal.smart.sql.query.commands.AlterTableCommand;
import np.com.ngopal.smart.sql.query.clauses.AlterTableSpecification;
import np.com.ngopal.smart.sql.query.clauses.ColumnReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.CreateTableColumnDefinition;
import np.com.ngopal.smart.sql.query.commands.DeleteCommand;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.query.clauses.IntoTableClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OnDuplicateKeyUpdateClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.exceptions.ValidationException;
import np.com.ngopal.smart.sql.query.commands.ReplaceCommand;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.query.clauses.SetClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.commands.UpdateCommand;
import np.com.ngopal.smart.sql.query.clauses.ValuesClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;
import np.com.ngopal.smart.sql.query.templates.QueryTemplate;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.commands.AlterCommand;
import np.com.ngopal.smart.sql.query.commands.AlterTableSpaceCommand;
import np.com.ngopal.smart.sql.query.commands.CreateTableCommand;
import np.com.ngopal.smart.sql.query.commands.ShowCommand;
import np.com.ngopal.smart.sql.query.exceptions.TemplateException;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryProfilingTest
{
    
    public QueryProfilingTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void parseSelectQuery()
    {
        final QueryClause queryClause = QueryFormat.parseSelectQuery(
            "SELECT "
                    + "ALL "
                    + "high_priORITY   "
                    + "MAX_STATEMENT_TIME = 10 "
                    + "STRAIGHT_JOIN  "
                    + "SQL_SMALL_RESULT  "
                    + "SQL_BUFFER_RESULT "
                    + "SQL_CACHE  "
                + "(id), (select count(*)   as b from w as b join t on (b=1) and asd(1,2) > 0) as test "
            + "from a "
                    + "partition "
                    + "asc "
            + "where b in (1,2,3) "
            + "group by s ASC, i DESC "
                    + "with  rollup "
            + "having i=1 "
            + "order by ai as f ASC "
            + "limit 2 "
            + "procedure max(i) "
            + "FOR   UPDATE ");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof SelectCommand);
        
        final SelectCommand selectClause = (SelectCommand)queryClause;
        
        Assert.assertEquals("all", selectClause.getDistinctType());
        Assert.assertTrue(selectClause.isHighPriority());
        
        Assert.assertEquals("10", selectClause.getMaxStatTime());
        Assert.assertTrue(selectClause.isStraightJoin());
        Assert.assertFalse(selectClause.isSqlBigResult());
        Assert.assertTrue(selectClause.isSqlBufferResult());
        Assert.assertTrue(selectClause.isSqlSmallResult());
        
        Assert.assertEquals("sql_cache", selectClause.getSqlType());
        
        Assert.assertEquals("max(i)", selectClause.getProcedure());
        
        Assert.assertEquals("asc", selectClause.getPartition());
        
        Assert.assertTrue(selectClause.isForUpdate());
        Assert.assertFalse(selectClause.isLockIn());
        
        final List<ColumnReferenceClause> columns = selectClause.getColumns();
        
        Assert.assertNotNull(columns);
        Assert.assertEquals(2, columns.size());
        Assert.assertNull(selectClause.getAlias());
        Assert.assertNotNull(selectClause.getFromClause());
        
        final WhereClause whereClause = selectClause.getWhereClause();
        Assert.assertNotNull(whereClause);
        
        final String whereConditionClause = whereClause.getCondition();
        Assert.assertNotNull(whereConditionClause);
        Assert.assertEquals("b in (1,2,3)", whereConditionClause);
        
        final GroupByClause groupByClause = selectClause.getGroupByClause();
        Assert.assertNotNull(groupByClause);
        Assert.assertTrue(groupByClause.isWithRollup());
        
        final List<ColumnReferenceClause> groupByColumns = groupByClause.getColumns();
        Assert.assertNotNull(groupByColumns);
        Assert.assertEquals(2, groupByColumns.size());
        
        Assert.assertEquals("s", groupByColumns.get(0).getName());
        Assert.assertNull(groupByColumns.get(0).getAlias());
        Assert.assertTrue(groupByColumns.get(0).isAsc());
        Assert.assertFalse(groupByColumns.get(0).isAs());
        Assert.assertEquals("i", groupByColumns.get(1).getName());
        Assert.assertNull(groupByColumns.get(1).getAlias());
        Assert.assertFalse(groupByColumns.get(1).isAsc());
        
        final HavingClause havingClause = selectClause.getHavingClause();
        Assert.assertNotNull(havingClause);
        
        final String havingConditionClause = havingClause.getCondition();
        Assert.assertNotNull(havingConditionClause);
        Assert.assertEquals("i=1", havingConditionClause);
        
        final OrderByClause orderByClause = selectClause.getOrderByClause();
        Assert.assertNotNull(orderByClause);
        
        final List<ColumnReferenceClause> orderByColumns = orderByClause.getColumns();
        Assert.assertNotNull(orderByColumns);
        Assert.assertEquals(1, orderByColumns.size());
        
        Assert.assertEquals("ai", orderByColumns.get(0).getName());
        Assert.assertEquals("f", orderByColumns.get(0).getAlias());
        Assert.assertTrue(orderByColumns.get(0).isAsc());
        Assert.assertTrue(orderByColumns.get(0).isAs());
        
        final LimitClause limitClause = selectClause.getLimitClause();
        Assert.assertNotNull(limitClause);
        Assert.assertEquals("2", limitClause.getSource());
        
        
        Assert.assertEquals("(id)", columns.get(0).getName());
        Assert.assertNull(columns.get(0).getAlias());

        Assert.assertTrue(columns.get(1) instanceof SelectCommand);

        final SelectCommand subClause = (SelectCommand) columns.get(1);
        Assert.assertNotNull(subClause);
        
        final List<ColumnReferenceClause> subColumns = subClause.getColumns();

        Assert.assertNotNull(subColumns);
        Assert.assertEquals(1, subColumns.size());
        
        Assert.assertEquals("count(*)", subColumns.get(0).getName());
        Assert.assertEquals("b", subColumns.get(0).getAlias());
        Assert.assertTrue(subColumns.get(0).isAs());
        
        Assert.assertEquals("test", subClause.getAlias());
        
        final FromClause subFromClause = subClause.getFromClause();
        Assert.assertNotNull(subFromClause);
        
        final List<TableReferenceClause> subTables = subFromClause.getTables();
        Assert.assertNotNull(subTables);
        Assert.assertEquals(1, subTables.size());
        
        final TableReferenceClause table = subTables.get(0);
        Assert.assertNotNull(table);
        Assert.assertEquals("w", table.getName());
        Assert.assertEquals("b", table.getAlias());
        
        final List<JoinClause> joins = table.getJoinClauses();
        final JoinClause join = joins.get(0);
        
        Assert.assertNotNull(join);
        Assert.assertEquals(JoinClause.JoinType.NONE, join.getType());
        
        final TableReferenceClause joinTable = join.getTable();
        Assert.assertNotNull(joinTable);
        Assert.assertEquals("t", joinTable.getName());
        Assert.assertNull(joinTable.getAlias());
        
        final String joinCondition = join.getCondition();
        Assert.assertNotNull(joinCondition);
        Assert.assertEquals("(b=1) and asd(1,2) > 0", joinCondition);
        
        Assert.assertNull(subClause.getWhereClause());
        Assert.assertNull(subClause.getGroupByClause());
        Assert.assertNull(subClause.getOrderByClause());
    }
    
    @Test
    public void parseUpdateQuery()
    {
        final QueryClause queryClause = QueryFormat.parseUpdateQuery("Update ignore table set s = 1, a=3 where s=0 order by a limit 10");
        
        Assert.assertNotNull(queryClause);
        Assert.assertTrue(queryClause instanceof UpdateCommand);
        
        final UpdateCommand updateClause = (UpdateCommand)queryClause;
        
        Assert.assertTrue(updateClause.isIgnore());
        Assert.assertFalse(updateClause.isLowPriority());
        
        final List<TableReferenceClause> tableClauses = updateClause.getTableClauses();
        Assert.assertNotNull(tableClauses);
        Assert.assertEquals(1, tableClauses.size());
        Assert.assertEquals("table", tableClauses.get(0).getName());
        Assert.assertNull(tableClauses.get(0).getAlias());
        
        final SetClause setClause = updateClause.getSetClause();
        Assert.assertNotNull(setClause);
        
        final String setCondition = setClause.getCondition();
        Assert.assertNotNull(setCondition);
        Assert.assertEquals("s = 1, a=3", setCondition);
        
        final WhereClause whereClause = updateClause.getWhereClause();
        Assert.assertNotNull(whereClause);
        
        final String whereConditionClause = whereClause.getCondition();
        Assert.assertNotNull(whereConditionClause);
        Assert.assertEquals("s=0", whereConditionClause);
        
        final OrderByClause orderByClause = updateClause.getOrderByClause();
        Assert.assertNotNull(orderByClause);
        
        final List<ColumnReferenceClause> orderByColumns = orderByClause.getColumns();
        Assert.assertNotNull(orderByColumns);
        Assert.assertEquals(1, orderByColumns.size());
        
        Assert.assertEquals("a", orderByColumns.get(0).getName());
        Assert.assertNull(orderByColumns.get(0).getAlias());
        
        final LimitClause limitClause = updateClause.getLimitClause();
        Assert.assertNotNull(limitClause);
        Assert.assertEquals("10", limitClause.getSource());
    }
    
    @Test
    public void parseDeleteQuery()
    {
        final QueryClause queryClause = QueryFormat.parseDeleteQuery("DELETE a, b FROM asd as a, bdr as b join cds as s on b.id = s.id WHERE a.id = 1 LIMIT 100");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof DeleteCommand);
        
        final DeleteCommand deleteClause = (DeleteCommand)queryClause;
        
        final List<TableReferenceClause> tables = deleteClause.getTables();
        
        Assert.assertNotNull(tables);
        Assert.assertEquals(2, tables.size());
        
        Assert.assertEquals("a", tables.get(0).getName());
        Assert.assertNull(tables.get(0).getAlias());

        Assert.assertEquals("b", tables.get(1).getName());
        Assert.assertNull(tables.get(1).getAlias());
       
        final FromClause fromClause = deleteClause.getFromClause();
        Assert.assertNotNull(fromClause);
        
        final List<TableReferenceClause> subTables = fromClause.getTables();
        Assert.assertNotNull(subTables);
        Assert.assertEquals(2, subTables.size());
        
        final TableReferenceClause table0 = subTables.get(0);
        Assert.assertNotNull(table0);
        Assert.assertEquals("asd", table0.getName());
        Assert.assertEquals("a", table0.getAlias());
        
        final TableReferenceClause table1 = subTables.get(1);
        Assert.assertNotNull(table1);
        Assert.assertEquals("bdr", table1.getName());
        Assert.assertEquals("b", table1.getAlias());
        
        final List<JoinClause> joins = table1.getJoinClauses();
        final JoinClause join = joins.get(0);
        Assert.assertNotNull(join);
        Assert.assertEquals(JoinClause.JoinType.NONE, join.getType());
        
        final TableReferenceClause joinTable = join.getTable();
        Assert.assertNotNull(joinTable);
        Assert.assertEquals("cds", joinTable.getName());
        Assert.assertEquals("s", joinTable.getAlias());
        
        final String joinCondition = join.getCondition();
        Assert.assertNotNull(joinCondition);
        Assert.assertEquals("b.id = s.id", joinCondition);
        
        final WhereClause whereClause = deleteClause.getWhereClause();
        Assert.assertNotNull(whereClause);
        
        final String whereConditionClause = whereClause.getCondition();
        Assert.assertNotNull(whereConditionClause);
        Assert.assertEquals("a.id = 1", whereConditionClause);
        
        final LimitClause limitClause = deleteClause.getLimitClause();
        Assert.assertNotNull(limitClause);
        Assert.assertEquals("100", limitClause.getSource());
    }
    
    @Test
    public void parseDeleteQuery0()
    {
        final QueryClause queryClause = QueryFormat.parseDeleteQuery("DELETE IGNORE FROM asd as a");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof DeleteCommand);
        
        final DeleteCommand deleteClause = (DeleteCommand)queryClause;
        
        Assert.assertTrue(deleteClause.isIgnore());
        Assert.assertFalse(deleteClause.isLowPriority());
        Assert.assertFalse(deleteClause.isQuick());
    }
    
    @Test
    public void parseDeleteQuery1()
    {
        final QueryClause queryClause = QueryFormat.parseDeleteQuery("DELETE LOW_PRIORITY QUICK FROM asd as a");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof DeleteCommand);
        
        final DeleteCommand deleteClause = (DeleteCommand)queryClause;
        
        Assert.assertFalse(deleteClause.isIgnore());
        Assert.assertTrue(deleteClause.isLowPriority());
        Assert.assertTrue(deleteClause.isQuick());
    }
    
    @Test
    public void parseDeleteQuery2()
    {
        final QueryClause queryClause = QueryFormat.parseDeleteQuery("DELETE LOW_PRIORITY QUICK FROM asd as a PARTITION (aa)");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof DeleteCommand);
        
        final DeleteCommand deleteClause = (DeleteCommand)queryClause;
        
        Assert.assertFalse(deleteClause.isIgnore());
        Assert.assertTrue(deleteClause.isLowPriority());
        Assert.assertTrue(deleteClause.isQuick());
        
        Assert.assertEquals("(aa)", deleteClause.getPartition());
    }
    
    @Test
    public void parseDeleteQuery3()
    {
        final QueryClause queryClause = QueryFormat.parseDeleteQuery("DELETE FROM asd as s order by a limit 1, 2");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof DeleteCommand);
        
        final DeleteCommand deleteClause = (DeleteCommand)queryClause;
        
        OrderByClause orderBy = deleteClause.getOrderByClause();
        
        Assert.assertNotNull(orderBy);
        Assert.assertNotNull(orderBy.getColumns());
        Assert.assertEquals(1, orderBy.getColumns().size());
        Assert.assertEquals("a", orderBy.getColumns().get(0).getName());
        
        LimitClause limit = deleteClause.getLimitClause();
        Assert.assertNotNull(limit);
        Assert.assertNotNull(limit.getSource());
        Assert.assertEquals("1, 2", limit.getSource());
    }
    
    @Test
    public void parseInsertQuerySimple()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("INSERT table(a, s, v) values(1, 2,3)");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertFalse(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a, s, v", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("1, 2,3", String.join("),(", valuesClause.getValue()));
        
        Assert.assertNull(insertClause.getOnDuplicateKeyUpdateClause());
        Assert.assertNull(insertClause.getSetClause());
    }
    
    @Test
    public void parseInsert0()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("INSERT  LOW_PRIORITY  IGNORE  table PARTITION (partition_name) (a, s, v) values (1, 2,3)");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        Assert.assertEquals("low_priority", insertClause.getPriority());
        Assert.assertTrue(insertClause.isIgnore());
        Assert.assertEquals("(partition_name)", insertClause.getPartitionData());
        
        Assert.assertFalse(insertClause.isValue());
        Assert.assertTrue(insertClause.isValues());
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertFalse(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a, s, v", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("1, 2,3", String.join("),(", valuesClause.getValue()));
        
        Assert.assertNull(insertClause.getOnDuplicateKeyUpdateClause());
        Assert.assertNull(insertClause.getSetClause());
    }
    
    @Test
    public void parseInsert1()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("INSERT  IGNORE  table  (a, s, v) value(1, 2,3)");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        Assert.assertNull(insertClause.getPriority());
        Assert.assertTrue(insertClause.isIgnore());
        Assert.assertNull(insertClause.getPartitionData());
        Assert.assertTrue(insertClause.isValue());
        Assert.assertFalse(insertClause.isValues());
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertFalse(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a, s, v", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("1, 2,3", String.join("),(", valuesClause.getValue()));
        
        Assert.assertNull(insertClause.getOnDuplicateKeyUpdateClause());
        Assert.assertNull(insertClause.getSetClause());
    }
    
    @Test
    public void parseInsert2()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("insert LOW_PRIORITY table(a) values(b) ");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        Assert.assertEquals("low_priority", insertClause.getPriority());
        Assert.assertFalse(insertClause.isIgnore());
        
        Assert.assertFalse(insertClause.isValue());
        Assert.assertTrue(insertClause.isValues());
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertFalse(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("b", String.join("),(", valuesClause.getValue()));
        
        Assert.assertNull(insertClause.getOnDuplicateKeyUpdateClause());
        Assert.assertNull(insertClause.getSetClause());
    }
    
    @Test
    public void parseInsertQueryWithInto()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("INSERT into table(a, s, v) values(1, 2,3)");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertTrue(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a, s, v", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("1, 2,3", String.join("),(", valuesClause.getValue()));
        
        Assert.assertNull(insertClause.getOnDuplicateKeyUpdateClause());
        Assert.assertNull(insertClause.getSetClause());
    }
    
    @Test
    public void parseInsertQueryWithSet()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("INSERT into table set a=1, b=2, c=3");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertTrue(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNull(valuesClause);
        
        final SetClause setClause = insertClause.getSetClause();
        Assert.assertNotNull(setClause);
        Assert.assertEquals("a=1, b=2, c=3", setClause.getCondition());
        
        Assert.assertNull(insertClause.getOnDuplicateKeyUpdateClause());        
    }
    
    @Test
    public void parseInsertQueryWithOnDuplicate()
    {
        final QueryClause queryClause = QueryFormat.parseInsertQuery("INSERT into table(a, s, v) values(1, 2,3) ON DUPLICATE KEY UPDATE a=1, b=2, c=3");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof InsertCommand);
        
        final InsertCommand insertClause = (InsertCommand)queryClause;
        
        final IntoTableClause intoTableClause = insertClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertTrue(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a, s, v", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = insertClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("1, 2,3", String.join("),(", valuesClause.getValue()));
        
        final OnDuplicateKeyUpdateClause onDuplicateClause = insertClause.getOnDuplicateKeyUpdateClause();
        Assert.assertNotNull(onDuplicateClause);
        Assert.assertEquals("a=1, b=2, c=3", onDuplicateClause.getCondition());
        
        Assert.assertNull(insertClause.getSetClause());
    }
    
    @Test
    public void parseReplaceQuery()
    {
        final QueryClause queryClause = QueryFormat.parseReplaceQuery("REPLACE into table(a, s, v) values(1, 2,3)");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof ReplaceCommand);
        
        final ReplaceCommand replaceClause = (ReplaceCommand)queryClause;
        
        final IntoTableClause intoTableClause = replaceClause.getIntoTable();
        Assert.assertNotNull(intoTableClause);
        Assert.assertTrue(intoTableClause.isHasIntoClause());
        Assert.assertEquals("table", intoTableClause.getName());
        Assert.assertNotNull(intoTableClause.getValuesClause());
        Assert.assertEquals("a, s, v", String.join("),(", intoTableClause.getValuesClause().getValue()));
        
        final ValuesClause valuesClause = replaceClause.getValuesClause();
        Assert.assertNotNull(valuesClause);
        Assert.assertEquals("1, 2,3", String.join("),(", valuesClause.getValue()));
    }
    
    
    @Test
    public void parseAlterQueryAdd()
    {
        final QueryClause queryClause = QueryFormat.parseAlterTableQuery("ALTER TABLE b ADD column a text");
        
        Assert.assertNotNull(queryClause);
        
        Assert.assertTrue(queryClause instanceof AlterTableCommand);
        
        final AlterTableCommand alterClause = (AlterTableCommand)queryClause;
        Assert.assertEquals("b", alterClause.getTableName());
        
        List<AlterTableSpecification> list = alterClause.getAlterSpecifications();
        Assert.assertNotNull(list);
        Assert.assertEquals(1, list.size());
        Assert.assertEquals("add column a text", list.get(0).getSpecification());
        
    }
    
    @Test
    public void makeSelectQueryTemplate() throws TemplateException
    {
        final String query = "SELECT(id), (select count(*) as b from w as b join t on (b=1) and asd(1,2) > 0) as test from a where b in ('a1','12.3',1,'2',3.2,\"asd\") group by s, i having i=1 order by ai limit 2";
        final QueryTemplate queryTemplate = QueryTemplateCoding.makeQueryTemplate(query);
        
        System.out.println(query);
        System.out.println(queryTemplate.getTemplate());
        System.out.println(queryTemplate.getMd5Sum());
        
//        Assert.assertEquals("SELECT (id), (SELECT count(*) AS b FROM w AS b JOIN t ON (b=1) and asd(1,2) > 0) AS test FROM a WHERE b in ('XXXXXX',DDD.DD,NNN,NNN,DDD.DD,\"XXXXXX\") GROUP BY s, i HAVING i=NNN ORDER BY ai LIMIT 2", queryTemplate.getTemplate());
//        Assert.assertEquals("285afd471a233f3384f53fe46cc68283", queryTemplate.getMd5Sum());
    }
    
    
    @Test
    public void makeUpdateQueryTemplate() throws TemplateException
    {
        final String query = "Update table set s = 1, a=3 where s=0 order by a limit 10";
        final QueryTemplate queryTemplate = QueryTemplateCoding.makeQueryTemplate(query);
        
        System.out.println(query);
        System.out.println(queryTemplate.getTemplate());
        System.out.println(queryTemplate.getMd5Sum());
        
//        Assert.assertEquals("UPDATE table SET s = NNN, a=NNN WHERE s=NNN ORDER BY a LIMIT 10", queryTemplate.getTemplate());
//        Assert.assertEquals("680ee678057d71df37fb110d1cace8a6", QueryTemplateCoding.templateHash(queryTemplate.getTemplate()));
    }
    
    @Test
    public void makeDeleteQueryTemplate() throws TemplateException
    {
        final String query = "DELETE a, b FROM asd as a, bdr as b join cds as s on b.id = s.id WHERE a.id = 1 and b = 'asd1' or s = 1.23 LIMIT 100";
        final QueryTemplate queryTemplate = QueryTemplateCoding.makeQueryTemplate(query);
        
        System.out.println(query);
        System.out.println(queryTemplate.getTemplate());
        System.out.println(queryTemplate.getMd5Sum());
        
//        Assert.assertEquals("DELETE a, b FROM asd AS a, bdr AS b JOIN cds AS s ON b.id = s.id WHERE a.id = NNN and b = 'XXXXXX' or s = DDD.DD LIMIT 100", queryTemplate.getTemplate());
//        Assert.assertEquals("05499f01669333318fef434e0d6ae153", queryTemplate.getMd5Sum());
    }
    
    @Test
    public void makeInsertQueryWithOnDuplicateTemplate() throws TemplateException
    {
        final String query = "INSERT into table(a, s, v) values(1, 2,3) ON DUPLICATE KEY UPDATE a=1, b=2, c=3";
        final QueryTemplate queryTemplate = QueryTemplateCoding.makeQueryTemplate(query);
        
        System.out.println(query);
        System.out.println(queryTemplate.getTemplate());
        System.out.println(queryTemplate.getMd5Sum());
        
        Assert.assertEquals("INSERT INTO table (a, s, v) VALUES (NNN, NNN,NNN) ON DUPLICATE KEY UPDATE a=1, b=2, c=3", queryTemplate.getTemplate());
        Assert.assertEquals("f0e33b9b8d83a83f2c1aebdeeb73cc6e", queryTemplate.getMd5Sum());
    }
    
    @Test
    public void makeReplacetQueryTemplate() throws TemplateException
    {
        final String query = "INSERT into table(a, s, v, q) values(1, 2.2, '3', \"asd\")";
        final QueryTemplate queryTemplate = QueryTemplateCoding.makeQueryTemplate(query);
        
        System.out.println(query);
        System.out.println(queryTemplate.getTemplate());
        System.out.println(queryTemplate.getMd5Sum());
        
        Assert.assertEquals("INSERT INTO table (a, s, v, q) VALUES (NNN, DDD.DD, NNN, \"XXXXXX\")", queryTemplate.getTemplate());
        Assert.assertEquals("82ab3a5fada12b38dd11b9ef82bb4249", queryTemplate.getMd5Sum());
    }
    
    @Test
    public void selectTemplate() throws TemplateException
    {
        String query0 = "select * from `questions_logger` where `folder_id` is null and `question_id` = 7573 and `user_id` = 24 and `test_logger_id` is null";
        QueryTemplate queryTemplate0 = QueryTemplateCoding.makeQueryTemplate(query0);
        
        String query1 = "select * from `questions_logger` where `folder_id` is null and `question_id` = '7520' and `user_id` = 14 and `test_logger_id` is null";
        QueryTemplate queryTemplate1 = QueryTemplateCoding.makeQueryTemplate(query1);
        
        System.out.println(queryTemplate0.getTemplate());
        System.out.println(queryTemplate0.getMd5Sum());
        
        System.out.println(queryTemplate1.getTemplate());
        System.out.println(queryTemplate1.getMd5Sum());
        
        Assert.assertEquals(queryTemplate0.getTemplate(), queryTemplate1.getTemplate());
    }
    
    
    
    @Test
    public void validateQ1()
    {
        try
        {
            QueryValidation.validateQuery("Select from EMP");
        }
        catch (final ValidationException ex)
        {
            Assert.assertEquals(QueryValidation.ERROR_MEESAGE__SELECT_R6, ex.getMessage());
        }
    }
    
    @Test
    public void validateQ2() throws ValidationException
    {
        QueryValidation.validateQuery("Select \"TopDBs\"");
    }
    
    @Test
    public void validateQ3() throws ValidationException
    {
        QueryValidation.validateQuery("select * from EMP order by empname;");
    }

    @Test
    public void validateQ4() throws ValidationException
    {
        try
        {
            QueryValidation.validateQuery("select * from EMP order by empname group by empname");
        }
        catch (final ValidationException ex)
        {
            Assert.assertEquals("ErrorMessage: The \"ORDER BY\" clause should come before the \"GROUP BY\" clause (5 < 3). (Need to replace with clause name on \"ORDER BY\" and \"GROUP BY\".)", ex.getMessage());
        }
    }
    
    @Test
    public void validateQ5() throws ValidationException
    {
        try
        {
            QueryValidation.validateQuery("select * from EMP having count(1) > 10");
        }
        catch (final ValidationException ex)
        {
            Assert.assertEquals(QueryValidation.ERROR_MEESAGE__SELECT_R3, ex.getMessage());
        }
    }
    
    @Test
    public void validateQ6() throws ValidationException
    {
        QueryValidation.validateQuery("select * from EMP where sal<(select max(sal) from EMP )");
    }
    
    @Test
    public void validateQ7() throws ValidationException
    {
        try
        {
            QueryValidation.validateQuery("select empname from EMP where sal in (select avg(sal) from EMP group by DEP order by sal having avg(sal)>2000 )");
        }
        catch (final ValidationException ex)
        {
            Assert.assertEquals("ErrorMessage: The \"ORDER BY\" clause should come before the \"HAVING\" clause (5 < 4). (Need to replace with clause name on \"ORDER BY\" and \"HAVING\".)\nstart: 38\nfinish:100", ex.getMessage());
        }
    }
    
    
    @Test
    public void getTableNamesFromQuery()
    {
        Map<String, String> result0 = QueryFormat.getTableNamesFromQuery("SELECT(id), (select count(*) as b from w as b join t on (b=1) and asd(1,2) > 0) as test from a where b in (1,2,3) group by s, i having i=1 order by ai limit 2");
        Assert.assertEquals(3, result0.size());
        Assert.assertTrue(result0.containsKey("w"));
        Assert.assertTrue(result0.containsKey("t"));
        Assert.assertTrue(result0.containsKey("a"));
        
        Map<String, String> result1 = QueryFormat.getTableNamesFromQuery("Update table set s = 1, a=3 where s=0 order by a limit 10");
        Assert.assertEquals(1, result1.size());
        Assert.assertTrue(result1.containsKey("table"));
        
        Map<String, String> result2 = QueryFormat.getTableNamesFromQuery("DELETE a, b FROM asd as a, bdr as b join cds as s on b.id = s.id WHERE a.id = 1 LIMIT 100");
        Assert.assertEquals(2, result2.size());
        Assert.assertTrue(result2.containsKey("a"));
        Assert.assertTrue(result2.containsKey("b"));
        
        
        Map<String, String> result3 = QueryFormat.getTableNamesFromQuery("INSERT table(a, s, v) values(1, 2,3)");
        Assert.assertEquals(1, result3.size());
        Assert.assertTrue(result3.containsKey("table"));
        
        Map<String, String> result4= QueryFormat.getTableNamesFromQuery("ALTER TABLE b ADD column a text");
        Assert.assertEquals(1, result4.size());
        Assert.assertTrue(result4.containsKey("b"));
    }
    
    @Test
    public void convertToExplainQuery()
    {
        String result0 = QueryFormat.convertToExplainQuery("EXPLAIN", "select * from ware");
        Assert.assertEquals("EXPLAIN select * from ware", result0);
        
        String result1 = QueryFormat.convertToExplainQuery("EXPLAIN", "insert into ware values(1)");
        Assert.assertNull(result1);
        
        String result2 = QueryFormat.convertToExplainQuery("EXPLAIN", "update ware set s = 1 where id = 0");
        Assert.assertEquals("EXPLAIN SELECT * FROM ware WHERE id = 0", result2);
        
        String result3 = QueryFormat.convertToExplainQuery("EXPLAIN", "delete from ware where id = 0 and id = 1");
        Assert.assertEquals("EXPLAIN SELECT * FROM ware WHERE id = 0 and id = 1", result3);
    }
    
    @Test
    public void testRegex()
    {
        Pattern p = Pattern.compile("(and)|(or)");
        Matcher m = p.matcher("s=1 and b>2 or c<3");
        while(m.find()) {
            System.out.println(m.group());
        }
        System.out.println("testRegex");
    }
    
    @Test
    public void parseCreateTableQuery0()
    {
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE   TABLE s");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
    }

    @Test
    public void parseCreateTableQuery1()
    {
        
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TEMPORARY   TABLE s");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        Assert.assertTrue(command.isTemporary());        
        Assert.assertFalse(command.isIfNotExists());
    }

    @Test
    public void parseCreateTableQuery2()
    {    
        
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TEMPORARY   TABLE    IF  NOT EXISTS s");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        Assert.assertTrue(command.isTemporary());        
        Assert.assertTrue(command.isIfNotExists());
        
    }

    @Test
    public void parseCreateTableQuery3()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TABLE  s IGNORE AS Select");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        
        Assert.assertTrue(command.isSelectIgnore());
        Assert.assertFalse(command.isSelectReplace());
        Assert.assertTrue(command.isSelectAs());
        
    }

    @Test
    public void parseCreateTableQuery4()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TEMPORARY  TABLE IF  NOT EXISTS s  REPLACE   Select");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        
        Assert.assertTrue(command.isTemporary());        
        Assert.assertTrue(command.isIfNotExists());
        
        Assert.assertEquals("select", command.getSelect());
        Assert.assertFalse(command.isSelectIgnore());
        Assert.assertTrue(command.isSelectReplace());
        Assert.assertFalse(command.isSelectAs());
        
        Assert.assertEquals("s", command.getTableName());
        
    }

    @Test
    public void parseCreateTableQuery5()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TEMPORARY  TABLE s like t");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        
        Assert.assertTrue(command.isTemporary());
                
        Assert.assertEquals("s", command.getTableName());
        Assert.assertEquals("t", command.getLikeTableName());
        
    }

    @Test
    public void parseCreateTableQuery6()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TEMPORARY  TABLE s ( like t   )");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        
        Assert.assertTrue(command.isTemporary());
                
        Assert.assertEquals("s", command.getTableName());
        Assert.assertEquals("t", command.getLikeTableName());
        
    }

    @Test
    public void parseCreateTableQuery7()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE TEMPORARY  TABLE IF  NOT EXISTS `s`.`d` (PRIMARY KEY (a) COMMENT 'string', CONSTRAINT PRIMARY KEY USING (a), CONSTRAINT sss PRIMARY KEY USING HASH (a), a int, b int, c text default 'test, \\\" data') REPLACE   Select");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
        
        Assert.assertTrue(command.isTemporary());        
        Assert.assertTrue(command.isIfNotExists());
        
        Assert.assertEquals("select", command.getSelect());
        Assert.assertFalse(command.isSelectIgnore());
        Assert.assertTrue(command.isSelectReplace());
        Assert.assertFalse(command.isSelectAs());
        
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(6, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.PRIMARY_KEY, columns.get(0).getType());
        Assert.assertNull(columns.get(0).getName());
        Assert.assertEquals("a", columns.get(0).getColumns());
        Assert.assertFalse(columns.get(0).isConstraint());
        Assert.assertEquals("comment @00", columns.get(0).getIndexOption());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.PRIMARY_KEY, columns.get(1).getType());
        Assert.assertEquals("a", columns.get(1).getColumns());
        Assert.assertTrue(columns.get(1).isConstraint());
        Assert.assertTrue(columns.get(2).isUsing());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.PRIMARY_KEY, columns.get(2).getType());
        Assert.assertEquals("a", columns.get(2).getColumns());
        Assert.assertTrue(columns.get(2).isConstraint());
        Assert.assertEquals("sss", columns.get(2).getConstraintSymbol());
        Assert.assertTrue(columns.get(2).isUsing());
        Assert.assertEquals("hash", columns.get(2).getUsingType());
                
        Assert.assertEquals(CreateTableColumnDefinition.Type.COLUMN, columns.get(3).getType());
        Assert.assertEquals("a", columns.get(3).getName());
        Assert.assertEquals("int", columns.get(3).getDefinition());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.COLUMN, columns.get(4).getType());
        Assert.assertEquals("b", columns.get(4).getName());
        Assert.assertEquals("int", columns.get(4).getDefinition());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.COLUMN, columns.get(5).getType());
        Assert.assertEquals("c", columns.get(5).getName());
        Assert.assertEquals("text default @01", columns.get(5).getDefinition());
        
    }
    
    @Test
    public void parseCreateTableQuery8()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (INDEX index_0 USING HASH (a, b) COMMENT 'string')");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.INDEX, columns.get(0).getType());
        Assert.assertEquals("index_0", columns.get(0).getName());
        Assert.assertTrue(columns.get(0).isUsing());
        Assert.assertEquals("hash", columns.get(0).getUsingType());
        Assert.assertEquals("a, b", columns.get(0).getColumns());
        Assert.assertEquals("comment @00", columns.get(0).getIndexOption());
    }
    
    @Test
    public void parseCreateTableQuery9()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (KEY USING BTREE (a, b, c) )");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.KEY, columns.get(0).getType());
        Assert.assertNull(columns.get(0).getName());
        Assert.assertTrue(columns.get(0).isUsing());
        Assert.assertEquals("btree", columns.get(0).getUsingType());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
        Assert.assertNull(columns.get(0).getIndexOption());
    }
    
    @Test
    public void parseCreateTableQuery10()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (CONSTRAINT s UNIQUE INDEX ttt USING BTREE (a, b, c) )");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.UNIQUE, columns.get(0).getType());
        Assert.assertTrue(columns.get(0).isConstraint());
        Assert.assertEquals("s", columns.get(0).getConstraintSymbol());
        Assert.assertTrue(columns.get(0).isIndex());
        Assert.assertEquals("ttt", columns.get(0).getName());
        Assert.assertTrue(columns.get(0).isUsing());
        Assert.assertEquals("btree", columns.get(0).getUsingType());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
        Assert.assertNull(columns.get(0).getIndexOption());
    }
    
    @Test
    public void parseCreateTableQuery11()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (CONSTRAINT s UNIQUE KEY ttt (a, b, c) )");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.UNIQUE, columns.get(0).getType());
        Assert.assertTrue(columns.get(0).isConstraint());
        Assert.assertEquals("s", columns.get(0).getConstraintSymbol());
        Assert.assertTrue(columns.get(0).isKey());
        Assert.assertEquals("ttt", columns.get(0).getName());
        Assert.assertFalse(columns.get(0).isUsing());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
        Assert.assertNull(columns.get(0).getIndexOption());
    }
    
    @Test
    public void parseCreateTableQuery12()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (FULLTEXT  KEY ttt (a, b, c) WITH PARSER parser_name)");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.FULLTEXT, columns.get(0).getType());
        Assert.assertTrue(columns.get(0).isKey());
        Assert.assertEquals("ttt", columns.get(0).getName());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
        Assert.assertEquals("with parser parser_name", columns.get(0).getIndexOption());
    }
    
    @Test
    public void parseCreateTableQuery13()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (SPATIAL  INDEX ttt (a, b, c) )");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.SPATIAL, columns.get(0).getType());
        Assert.assertTrue(columns.get(0).isIndex());
        Assert.assertEquals("ttt", columns.get(0).getName());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
    }
    
    @Test
    public void parseCreateTableQuery14()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (SPATIAL ttt (a, b, c) )");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.SPATIAL, columns.get(0).getType());
        Assert.assertEquals("ttt", columns.get(0).getName());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
    }
    
    @Test
    public void parseCreateTableQuery15()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (CONSTRAINT ss FOREIGN    KEY ttt (a, b, c) REFERENCES tbl_name (a, b, c))");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.FOREIGN_KEY, columns.get(0).getType());
        Assert.assertTrue(columns.get(0).isConstraint());
        Assert.assertEquals("ss", columns.get(0).getConstraintSymbol());
        Assert.assertEquals("ttt", columns.get(0).getName());
        Assert.assertEquals("a, b, c", columns.get(0).getColumns());
        Assert.assertEquals("references tbl_name (a, b, c)", columns.get(0).getIndexOption());
    }
    
    @Test
    public void parseCreateTableQuery16()
    {    
        QueryClause queryClause = QueryFormat.parseCreateTableQuery("CREATE  TABLE `s`.`d` (CHECK   (a, b, c))");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof CreateTableCommand);
        
        CreateTableCommand command = (CreateTableCommand)queryClause;
                
        Assert.assertEquals("`s`.`d`", command.getTableName());
        
        List<CreateTableColumnDefinition> columns = command.getColumns();
        Assert.assertNotNull(columns);
        Assert.assertEquals(1, columns.size());
        
        Assert.assertEquals(CreateTableColumnDefinition.Type.CHECK, columns.get(0).getType());
        Assert.assertEquals("a, b, c", columns.get(0).getDefinition());
    }
    
    @Test
    public void parseAlterTableQuery0()
    {    
        QueryClause queryClause = QueryFormat.parseAlterTableQuery("Alter table table_name add column c1 , Drop column c2, group   by a, b, c");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof AlterTableCommand);
        
        AlterTableCommand command = (AlterTableCommand)queryClause;
                
        Assert.assertEquals("table_name", command.getTableName());
        
        List<AlterTableSpecification> columns = command.getAlterSpecifications();
        Assert.assertNotNull(columns);
        Assert.assertEquals(3, columns.size());
        
        Assert.assertEquals("add column c1", columns.get(0).getSpecification());
        Assert.assertEquals("drop column c2", columns.get(1).getSpecification());
        Assert.assertEquals("group by a, b, c", columns.get(2).getSpecification());
    }
    
    @Test
    public void parseAlterTableSpaceQuery0()
    {    
        QueryClause queryClause = QueryFormat.parseAlterTableSpaceQuery("ALTER TABLESPACE tablespace_name ADD DATAFILE 'file_name'");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof AlterTableSpaceCommand);
        
        AlterTableSpaceCommand command = (AlterTableSpaceCommand)queryClause;
                
        Assert.assertEquals("tablespace_name", command.getTableSpaceName());
        
        Assert.assertTrue(command.isAdd());
        Assert.assertEquals("file_name", command.getFileName());
    }
    
    @Test
    public void parseAlterQuery0()
    {    
        QueryClause queryClause = QueryFormat.parseAlterQuery("ALTER DATABASE s");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof AlterCommand);
        
        AlterCommand command = (AlterCommand)queryClause;
                
        Assert.assertEquals("database s", command.getDefinition());
    }
    
    @Test
    public void parseAlterQuery1()
    {    
        QueryClause queryClause = QueryFormat.parseAlterQuery("ALTER TABLESPACE s");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof AlterCommand);
        
        AlterCommand command = (AlterCommand)queryClause;
                
        Assert.assertEquals("tablespace s", command.getDefinition());
    }
    
    @Test
    public void parseShowDefinition()
    {    
        QueryClause queryClause = QueryFormat.parseShowQuery("SHOW PRIVILEGES");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof ShowCommand);
        
        ShowCommand command = (ShowCommand)queryClause;
                
        Assert.assertEquals("privileges", command.getDefinition());
    }
    
    @Test
    public void parseUnion() {
        QueryClause queryClause = QueryFormat.parseSelectQuery(
            "select a0, count(b0) from t0 "
        + "UNION "
          + "select max(a1), b1 from t1 "
        + "UNION ALL "
          + "select a2, abs(b2) from t2 ");
        
        Assert.assertNotNull(queryClause);        
        Assert.assertTrue(queryClause instanceof SelectCommand);
        
        SelectCommand command = (SelectCommand)queryClause;
                
        Assert.assertNotNull(command.getUnionSelectCommands());
        Assert.assertEquals(3, command.getUnionSelectCommands().size());
        
        Assert.assertFalse(command.getUnionSelectCommands().get(0).isUnionAll());
        Assert.assertNotNull(command.getUnionSelectCommands().get(0).getColumns());
        Assert.assertEquals(2, command.getUnionSelectCommands().get(0).getColumns().size());
        Assert.assertEquals("a0", command.getUnionSelectCommands().get(0).getColumns().get(0).getName());
        Assert.assertEquals("count(b0)", command.getUnionSelectCommands().get(0).getColumns().get(1).getName());
        Assert.assertNotNull(command.getUnionSelectCommands().get(0).getFromClause());
        Assert.assertNotNull(command.getUnionSelectCommands().get(0).getFromClause().getTables());
        Assert.assertEquals(1, command.getUnionSelectCommands().get(0).getFromClause().getTables().size());
        Assert.assertEquals("t0", command.getUnionSelectCommands().get(0).getFromClause().getTables().get(0).getName());
        
        Assert.assertFalse(command.getUnionSelectCommands().get(1).isUnionAll());
        Assert.assertNotNull(command.getUnionSelectCommands().get(1).getColumns());
        Assert.assertEquals(2, command.getUnionSelectCommands().get(1).getColumns().size());
        Assert.assertEquals("max(a1)", command.getUnionSelectCommands().get(1).getColumns().get(0).getName());
        Assert.assertEquals("b1", command.getUnionSelectCommands().get(1).getColumns().get(1).getName());
        Assert.assertNotNull(command.getUnionSelectCommands().get(1).getFromClause());
        Assert.assertNotNull(command.getUnionSelectCommands().get(1).getFromClause().getTables());
        Assert.assertEquals(1, command.getUnionSelectCommands().get(1).getFromClause().getTables().size());
        Assert.assertEquals("t1", command.getUnionSelectCommands().get(1).getFromClause().getTables().get(0).getName());
        
        Assert.assertTrue(command.getUnionSelectCommands().get(2).isUnionAll());
        Assert.assertNotNull(command.getUnionSelectCommands().get(2).getColumns());
        Assert.assertEquals(2, command.getUnionSelectCommands().get(2).getColumns().size());
        Assert.assertEquals("a2", command.getUnionSelectCommands().get(2).getColumns().get(0).getName());
        Assert.assertEquals("abs(b2)", command.getUnionSelectCommands().get(2).getColumns().get(1).getName());
        Assert.assertNotNull(command.getUnionSelectCommands().get(2).getFromClause());
        Assert.assertNotNull(command.getUnionSelectCommands().get(2).getFromClause().getTables());
        Assert.assertEquals(1, command.getUnionSelectCommands().get(2).getFromClause().getTables().size());
        Assert.assertEquals("t2", command.getUnionSelectCommands().get(2).getFromClause().getTables().get(0).getName());
    }
    
    
    @Test
    public void testQueryTemplates() throws TemplateException
    {
        String query0 = "SELECT COUNT(un.id) as count FROM user_notifications as un JOIN users as u ON u.id = un.user_id JOIN user_notification_pipes as unp ON un.id = unp.user_notification_id  WHERE u.identifier='null' AND u.account_id='478784' AND u.service_id=5 AND unp.pipe=5 AND un.read_at IS NULL;";
        QueryTemplate queryTemplate0 = QueryTemplateCoding.makeQueryTemplate(query0);
        
        String query1 = "SELECT COUNT(un.id) as count FROM user_notifications as un JOIN users as u ON u.id = un.user_id JOIN user_notification_pipes as unp ON un.id = unp.user_notification_id  WHERE u.identifier='null' AND u.account_id='478784' AND u.service_id=5 AND unp.pipe=5 AND un.read_at IS NULL;";
        QueryTemplate queryTemplate1 = QueryTemplateCoding.makeQueryTemplate(query1);
        
        String query2 = "SELECT COUNT(un.id) as count FROM user_notifications as un JOIN users as u ON u.id = un.user_id JOIN user_notification_pipes as unp ON un.id = unp.user_notification_id WHERE u.identifier='6048' AND u.account_id='2952' AND u.service_id=2 AND unp.pipe=1 AND un.seen_at IS NULL;";
        QueryTemplate queryTemplate2 = QueryTemplateCoding.makeQueryTemplate(query2);
        
        String query3 = "SELECT COUNT(un.id) as count FROM user_notifications as un JOIN users as u ON u.id = un.user_id JOIN user_notification_pipes as unp ON un.id = unp.user_notification_id WHERE u.identifier='19002047034' AND u.account_id='435165' AND u.service_id=5 AND unp.pipe=1 AND un.read_at IS NULL";
        QueryTemplate queryTemplate3 = QueryTemplateCoding.makeQueryTemplate(query3);
                
        Assert.assertEquals(queryTemplate0.getTemplate(), queryTemplate1.getTemplate());
        Assert.assertTrue(!queryTemplate0.getTemplate().equals(queryTemplate2.getTemplate()));
        Assert.assertTrue(!queryTemplate0.getTemplate().equals(queryTemplate3.getTemplate()));
    }
}