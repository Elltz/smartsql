package np.com.smart.sql.query;

import java.util.Map;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryFormatTest {
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }
    
    
    private void testBaseSelect(final QueryClause queryClause0) {
        
    }
    
    @Test
    public void tableNamesFromQuery()
    {
        Map<String, String> map = QueryFormat.getTableNamesFromQuery("select start_time as start_time, user_host as user_host, TIME_TO_SEC(query_time) as query_time, TIME_TO_SEC(lock_time) as lock_time, rows_sent as rows_sent, rows_examined as rows_examined, db as db, sql_text as sql_text, thread_id as thread_id from  mysql.slow_log where sql_text like 'select%' or sql_text like 'insert%' or sql_text like 'update%' or sql_text like 'delete%' or sql_text like 'call%' or sql_text like 'execute%'");
        Assert.assertNotNull(map);
    }
}
