package np.com.ngopal.smart.sql.db;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QAPerformanceTuningService extends AbstractSmartSqlService<QAPerformanceTuning>
{
    public QAPerformanceTuningService(String persistanceUnitName) {
        super(persistanceUnitName);
    }

    public QAPerformanceTuning findUnique(Long connectionParamsId, String database, String query, String recommendation) {        
        TypedQuery<QAPerformanceTuning> uniqueQuery = em.createNamedQuery("qaperformancetuning.findUnique", QAPerformanceTuning.class);
        uniqueQuery.setParameter("connectionParamsId", connectionParamsId);
        uniqueQuery.setParameter("database", database);
        uniqueQuery.setParameter("query", query);
        uniqueQuery.setParameter("recommendation", recommendation);
        
        List<QAPerformanceTuning> result = uniqueQuery.getResultList();
        return result.isEmpty() ? null : result.get(0);
    }
    
    
    public synchronized QAPerformanceTuning saveStatistic(Long connectionParamsId, String database, String dbtable, String query, String recommendation, String recommendedIndex, Long rowsScanBefore, boolean usedCredit) {
        
        Date currentDate = new Date();
        
        QAPerformanceTuning qa = findUnique(connectionParamsId, database, query, recommendation);
        if (qa == null) {
            qa = new QAPerformanceTuning();
            qa.setConnectionId(connectionParamsId);
            qa.setDatabase(database);
            qa.setDbtable(dbtable);
            qa.setQuery(query);            
            qa.setSqlIndexQuery(recommendedIndex);
            qa.setRowsScanBefore(rowsScanBefore);
            qa.setCreateDate(new Timestamp(currentDate.getTime()));
        }

        qa.setExecutingDateTime(new Timestamp(currentDate.getTime()));
        qa.setRowsScanAfter(null);
        qa.setRecommendation(recommendation);
        
        qa.setPerformanceImprovement(null);
        qa.setImprovementPercent(null);
        qa.setStatus("Note at created");
        qa.setUsedCredit(usedCredit);
        
        return save(qa);
    }
    
    public synchronized void updateStatistic(QAPerformanceTuning qa, Long rowsScanAfter) {
        
        qa.setRowsScanAfter(rowsScanAfter);
        
        BigDecimal improvment = new BigDecimal(qa.getRowsScanAfter() > 0 ? (double)(qa.getRowsScanBefore() - qa.getRowsScanAfter()) / (double)qa.getRowsScanAfter() : 0);
        
        qa.setPerformanceImprovement(improvment.setScale(2, RoundingMode.HALF_UP).toString() + " X times");
        qa.setImprovementPercent(improvment.multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).toString() + "%");
        
        qa.setExecutingDateTime(new Timestamp(new Date().getTime()));
        
        if (improvment.compareTo(BigDecimal.ZERO) >= 0) {
            qa.setStatus("good");
        } else {
            qa.setStatus("nutal");
        }
        
        save(qa);
    }
    
    public Map<String, Object> getReportDataByConnectionParamsId(Long connectionParamsId) {
        
        Map<String, Object> resultMap = new HashMap<>();
        
        TypedQuery<QAPerformanceTuning> uniqueQuery = em.createNamedQuery("qaperformancetuning.findByConnectionParamsId", QAPerformanceTuning.class);
        uniqueQuery.setParameter("connectionParamsId", connectionParamsId);
        
        List<QAPerformanceTuning> result = uniqueQuery.getResultList();
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        
        List<String[]> data = new ArrayList<>();
        for (QAPerformanceTuning q: result) {
            String[] d = new String[13];
            d[0] = sdf.format(new Date(q.getCreateDate().getTime()));
            d[1] = sdf.format(new Date(q.getExecutingDateTime().getTime()));
            d[2] = q.getDatabase();
            d[3] = q.getDbtable();
            d[4] = q.getQuery();
            d[5] = q.getRecommendation();
            d[6] = q.getSqlIndexQuery();
            d[7] = q.getRowsScanBefore() != null ? q.getRowsScanBefore().toString() : "";
            d[8] = q.getRowsScanAfter() != null ? q.getRowsScanAfter().toString() : "";
            d[9] = q.getPerformanceImprovement();
            d[10] = q.getImprovementPercent();
            d[11] = q.getStatus();
            d[12] = q.isUsedCredit() ? "1" : "0";
            
            data.add(d);
        }
        
        List<String> columns = new ArrayList<>();
        columns.add("Create Date");
        columns.add("Update Date");
        columns.add("Database");
        columns.add("Table");
        columns.add("Query");
        columns.add("Recommendation");
        columns.add("Recommendation Index");
        columns.add("Row Scan Before");
        columns.add("Row Scan After");
        columns.add("Performance Improvement");
        columns.add("Performance Improvement %");
        columns.add("Status");
        columns.add("Credit used");
        
        resultMap.put("columns", columns);
        resultMap.put("data", data);
        
        return resultMap;
    }
}