/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.FindReplaceHistory;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class FindReplaceHistoryService extends AbstractSmartSqlService<FindReplaceHistory> {
    
    private static final int MAX_SHOWED = 15;
    
    public FindReplaceHistoryService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public boolean isNeedSave(String name) {
        if (name != null) {
            
            List<FindReplaceHistory> files = getLastNames();
            if (files != null) {
                for (FindReplaceHistory rf: files) {
                    if (name.equalsIgnoreCase(rf.getHistory())) {
                        return false;
                    }
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    public List<FindReplaceHistory> getLastNames() {
        
        List<FindReplaceHistory> list = getAll();
        if (list != null && list.size() > MAX_SHOWED) {
            return list.subList(list.size() - MAX_SHOWED, list.size());
        } else {
            return list;
        }
    }
}
