/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.util.ArrayList;
import java.util.List;
import np.com.ngopal.smart.sql.model.RemoteMetricModel;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class RemoteMetricModelService extends AbstractSmartSqlService<RemoteMetricModel> {
        
    public RemoteMetricModelService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
   
    public List<RemoteMetricModel> getModelsByParamIdentifier(long id){
        ArrayList<RemoteMetricModel> models = new ArrayList();
        for(RemoteMetricModel model : getAll()){
            if(model.getParamIdentifier() == id){
                models.add(model);
            }
        }
        System.gc();
        return models;
    }

    @Override
    public List<RemoteMetricModel> getAll() {
        List<RemoteMetricModel> models = super.getAll();
        System.out.println("OVERALL REMOTE METRIC MODEL IN DATABASE =  "+ String.valueOf(models.size()));
        return models;
    }
}
