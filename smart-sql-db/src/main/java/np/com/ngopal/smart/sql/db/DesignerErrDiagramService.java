package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.ErrDiagram;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerErrDiagramService extends AbstractSmartSqlService<ErrDiagram> {
    
    public DesignerErrDiagramService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public ErrDiagram loadByHostAndDatabase(String host, String database) {
        if (host != null && database != null) {
            
            List<ErrDiagram> list = super.getAll();
            if (list != null) {

                for (ErrDiagram ed: list) {
                    if (host.equalsIgnoreCase(ed.getHost()) && database.equalsIgnoreCase(ed.getDatabase())) {
                        return ed;
                    }
                }
            }
        }
        
        return null;
    }
}
