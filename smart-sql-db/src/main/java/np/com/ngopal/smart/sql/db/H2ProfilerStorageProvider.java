package np.com.ngopal.smart.sql.db;

import com.google.inject.Singleton;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.provider.StorageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
@Singleton
public class H2ProfilerStorageProvider extends AbstractSmartSqlService implements StorageProvider {
        
    public H2ProfilerStorageProvider(String persistUnitName) {
        super(persistUnitName); 
    }    

    @Override
    public String getProviderType() {
        return QueryProvider.DB__H2;
    }
    
    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void executeQuery(String query, Object... params) {
        createQuery(query, params).executeUpdate();
    }

    @Override
    public List getResultList(String query, Object... params) {
        try {
            return createQuery(query, params).getResultList();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public Object getSingleResult(String query, Object... params) {
        try {
            return createQuery(query, params).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    private Query createQuery(String query, Object... params) {
        Query nativeQuery = em.createNativeQuery(query);
        
        if (params.length > 0) {
            for (int i = 1; i <= params.length; i++) {
                nativeQuery.setParameter(i, params[i-1]);
            }
        }
        
        return nativeQuery;
    }
}
