package np.com.ngopal.smart.sql.db;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.GSChart;
import np.com.ngopal.smart.sql.model.GSChartVariable;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ProfilerChartService extends AbstractSmartSqlService<GSChart> {
    
    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PUBLIC)
    private String defImportChartScript;
    
    public ProfilerChartService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
//    public ProfilerChartService() {
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("profiler-persistence-unit");
//        em = emf.createEntityManager();   
//    }

    public List<GSChart> getAll(String tab) {
        List<GSChart> list = super.getAll();

        // if charts empty try import default data        
        if (list == null || list.isEmpty()) {
            String query = getDefImportChartScript();
            if (query != null) {

                em.getTransaction().begin();
                em.createNativeQuery(query).executeUpdate();
                em.getTransaction().commit();

                list = super.getAll();
            }
        }
        
        if (tab != null && list != null) {
            // get first base than user
            List<GSChart> baseCharts = new ArrayList<>();
            List<GSChart> userCharts = new ArrayList<>();

            for (GSChart chart: list) {
                if (tab.equals(chart.getTab())) {                
                    if (chart.isChartType()) {
                        userCharts.add(chart);
                    } else {
                        baseCharts.add(chart);
                    }
                }
            }

            baseCharts.addAll(userCharts);
            baseCharts.sort(new Comparator<GSChart>() {
                @Override
                public int compare(GSChart o1, GSChart o2) {
                    return o2.getChartOrder() - o1.getChartOrder();
                }
            });
            return baseCharts;
        } else {
            return new ArrayList<>();
        }
    }
    
    public List<GSChart> getUserCharts() {
        List<GSChart> list = super.getAll();
        if (list != null) {
            return list.stream().filter(chart -> chart.isChartType()).collect(Collectors.toList());
        }
        
        return new ArrayList<>();
    }
    
    public Map<String, GSChartVariable> getVariablesNames(String tab) {
        
        Map<String, GSChartVariable> map = new HashMap<>();
        
        List<GSChart> list = getAll();
        if (list != null) {
            for (GSChart chart: list) {
                if (tab.equals(chart.getTab())) {           
                    if (chart.getVariables() != null) {
                        for (GSChartVariable v: chart.getVariables()) {
                            map.put(v.getVariableName().toUpperCase(), v);
                        }
                    }
                }
            }
        }
        
        return map;
    }
    
    public Map<String, GSChartVariable> getVariablesNames() {
        
        Map<String, GSChartVariable> map = new HashMap<>();
        
        List<GSChart> list = getAll();
        if (list != null) {
            for (GSChart chart: list) {
                if (chart.getVariables() != null) {
                    for (GSChartVariable v: chart.getVariables()) {
                        map.put(v.getVariableName().toUpperCase(), v);
                    }
                }
            }
        }
        
        return map;
    }
}