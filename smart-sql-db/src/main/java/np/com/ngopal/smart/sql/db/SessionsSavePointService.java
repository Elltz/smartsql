/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.SessionsSavePoint;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
// Hack - Move persisting data to file system, in future remove this class
@Slf4j
public class SessionsSavePointService {// extends AbstractSmartSqlService<SessionsSavePoint> {
    
    private final Comparator<SessionsSavePoint> COMPARATOR = (SessionsSavePoint o1, SessionsSavePoint o2) -> {
        if (o1.getLastUpdate() == null) {
            return -1;
        }
        
        if (o2.getLastUpdate() == null) {
            return -1;
        }
        
        return o2.getLastUpdate().compareTo(o1.getLastUpdate());
    };
    
    private static final String SESSIONS_DIR = "sessions";
    private static final String SESSIONS_SUFIX = "_sessions.json";
    
    public SessionsSavePointService(String persistanceUnitName) {
        //super(persistanceUnitName);
    }
    
    public SessionsSavePoint getLast() {
        List<SessionsSavePoint> list = getAll();
        
        if (list != null && !list.isEmpty()) {
            list.sort(COMPARATOR);
            return list.get(0);
        }
        
        return null;
    }
    
    public List<SessionsSavePoint> getAll() {
        List<SessionsSavePoint> result = new ArrayList<>();
        File dir = new File(SESSIONS_DIR);
        if (dir.exists()) {
            
            Gson gson = new Gson();
            for (File f: dir.listFiles()) {
                if (f.getName().endsWith(SESSIONS_SUFIX)) {
                    FileReader fr = null;
                    try {
                        fr = new FileReader(f);
                        result.add(gson.fromJson(fr, SessionsSavePoint.class));
                    } catch (FileNotFoundException ex) {
                        log.error("Error", ex);
                    } finally {
                        if (fr != null) {
                            try {
                                fr.close();
                            } catch (IOException ex) {
                                log.error("Error", ex);
                            }
                        }
                    }
                }
            }
        }
        
        return result;
    }
    
    private Long getLatestId() {
        List<SessionsSavePoint> list = getAll();
        Long maxId = 0l;
        for (SessionsSavePoint s: list) {
            if (s.getId() > maxId) {
                maxId = s.getId();
            }
        }
        
        return maxId + 1;
    }
    
    public SessionsSavePoint get(Long id) {
        File f = new File(SESSIONS_DIR + File.separatorChar + id + SESSIONS_SUFIX);
        if (f.exists()) {
            FileReader fr = null;
            try {
                fr = new FileReader(f);
                return new Gson().fromJson(fr, SessionsSavePoint.class);
            } catch (FileNotFoundException ex) {
                log.error("Error", ex);
            } finally {
                if (fr != null) {
                    try {
                        fr.close();
                    } catch (IOException ex) {
                        log.error("Error", ex);
                    }
                }
            }
        }
        
        return null;
    }
    
    public void update(SessionsSavePoint sessions) {
        save(sessions);
    }
    
    public void save(SessionsSavePoint sessions) {
        if (sessions != null) {
            
            try {
                Long id = sessions.getId();
                if (id == null) {
                    id = getLatestId();
                    sessions.setId(id);
                }
                
                File f = new File(SESSIONS_DIR + File.separatorChar + id + SESSIONS_SUFIX);
                new File(SESSIONS_DIR).mkdirs();
                
                Files.deleteIfExists(f.toPath()); 
                
                Files.write(f.toPath(), new Gson().toJson(sessions).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            } catch (IOException ex) {
                log.error("Error", ex);
            }
        }
    }
}
