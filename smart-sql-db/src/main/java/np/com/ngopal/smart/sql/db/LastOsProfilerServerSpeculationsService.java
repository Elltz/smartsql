/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import javax.persistence.PersistenceException;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.OsServerSpecs;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class LastOsProfilerServerSpeculationsService extends AbstractSmartSqlService<OsServerSpecs> {
    
    public LastOsProfilerServerSpeculationsService(String persistanceUnitName) {
        super(persistanceUnitName);
    }

    @Override
    public OsServerSpecs save(OsServerSpecs t) throws PersistenceException {
        if(getAll() == null || getAll().isEmpty()){
            return super.save(t);            
        }else{
            return super.update(t);
        }
    }
    
}
