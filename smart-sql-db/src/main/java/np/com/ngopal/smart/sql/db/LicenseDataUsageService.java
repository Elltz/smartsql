package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.LicenseDataUsage;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class LicenseDataUsageService extends AbstractSmartSqlService<LicenseDataUsage>
{    
    public LicenseDataUsageService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public LicenseDataUsage getLicenseDataUsage() {
        List<LicenseDataUsage> list = getAll();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}