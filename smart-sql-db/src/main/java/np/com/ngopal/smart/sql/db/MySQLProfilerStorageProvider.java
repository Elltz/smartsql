package np.com.ngopal.smart.sql.db;

import com.google.inject.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.provider.StorageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
@Singleton
public class MySQLProfilerStorageProvider extends MysqlDBService implements StorageProvider {
       
    private boolean inited = false;
    
    public MySQLProfilerStorageProvider() {
        super(null);        
    }
    
    public void init(ConnectionParams params, String database) throws SQLException {
                
        params.setMultiQueries(true);
        connect(params);
        
        inited = true;
        
        execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__CHANGE_DATABASE), database));
        
        // create all related tables
        if (getSingleResult("SELECT count(*) FROM DIGEST_QUERY") == null) {
            executeQuery("CREATE TABLE DIGEST_QUERY(ID INT NOT NULL AUTO_INCREMENT, DIGEST_ID BIGINT, SQL_TEXT LONGTEXT, COLLECTED_AT TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (ID), KEY(COLLECTED_AT), KEY(DIGEST_ID, COLLECTED_AT))", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM DIGEST_TEXT") == null) {
            executeQuery("CREATE TABLE DIGEST_TEXT(ID INT NOT NULL AUTO_INCREMENT, CONNECTION_ID BIGINT, SCHEMA_ID BIGINT, DIGEST VARCHAR(32), DIGEST_TEXT LONGTEXT, PRIMARY KEY (ID), UNIQUE(CONNECTION_ID, SCHEMA_ID, DIGEST))", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM EIS_TABLES") == null) {
            executeQuery("CREATE TABLE EIS_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE)", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM GS_START_TABLES") == null) {
            executeQuery("CREATE TABLE GS_START_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE, START_DATETIME TIMESTAMP, END_DATETIME TIMESTAMP)", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM GS_TABLES") == null) {
            executeQuery("CREATE TABLE GS_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE)", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM SLOWQUERIES_TABLES") == null) {
            executeQuery("CREATE TABLE SLOWQUERIES_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE)", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM SS_TABLES") == null) {
            executeQuery("CREATE TABLE SS_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE)", new Object[]{});;
        }
        
        if (getSingleResult("SELECT count(*) FROM STATEMENTS_DIGEST_TABLES") == null) {
            executeQuery("CREATE TABLE STATEMENTS_DIGEST_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE)", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM GLOBALVARIABLES_TABLES") == null) {
            executeQuery("CREATE TABLE GLOBALVARIABLES_TABLES(TABLE_NAME VARCHAR(255) NOT NULL UNIQUE)", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM PQDB") == null) {
            executeQuery("CREATE TABLE PQDB(ID INT NOT NULL AUTO_INCREMENT, DB VARCHAR(64), PRIMARY KEY (ID), INDEX(DB))", new Object[]{});
        }  
        
        if (getSingleResult("SELECT count(*) FROM PQTABLE") == null) {
            executeQuery("CREATE TABLE PQTABLE(ID INT NOT NULL AUTO_INCREMENT, TABLE_NAME VARCHAR(64), DBID INT NOT NULL, FOREIGN KEY PQTABLE_DB__FK(DBID) REFERENCES PQDB(ID), PRIMARY KEY (ID), INDEX(TABLE_NAME), INDEX(TABLE_NAME, DBID))", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM PQUSERHOST") == null) {
            executeQuery("CREATE TABLE PQUSERHOST(ID INT NOT NULL AUTO_INCREMENT, USER VARCHAR(64), HOST VARCHAR(255), PRIMARY KEY (ID), INDEX(USER, HOST))", new Object[]{});
        }        
        
        if (getSingleResult("SELECT count(*) FROM PQUERYTEMPLATE") == null) {
            executeQuery("CREATE TABLE PQUERYTEMPLATE(ID INT NOT NULL AUTO_INCREMENT, QUERY_TEMPLATE TEXT, QUERY TEXT, QUERY_ENCRYPT_NUMBER BIGINT, PRIMARY KEY (ID), INDEX(QUERY_ENCRYPT_NUMBER))", new Object[]{});
        } 
        
        if (getSingleResult("SELECT count(*) FROM PQUERYTEMPLATE_PQTABLE") == null) {
            executeQuery("CREATE TABLE PQUERYTEMPLATE_PQTABLE(PQUERYTEMPLATE_ID INT NOT NULL, FOREIGN KEY PQUERYTEMPLATE__FK(PQUERYTEMPLATE_ID) REFERENCES PQUERYTEMPLATE(ID), PQTABLE_ID INT NOT NULL, FOREIGN KEY PQTABLE__FK(PQTABLE_ID) REFERENCES PQTABLE(ID), INDEX(PQUERYTEMPLATE_ID, PQTABLE_ID))", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM PQUERY") == null) {
            executeQuery("CREATE TABLE PQUERY(ID INT NOT NULL AUTO_INCREMENT, QUERY_TEMPLATE_ID INT NOT NULL, FOREIGN KEY PQUERY_PQUERYTEMPLATE__FK(QUERY_TEMPLATE_ID) REFERENCES PQUERYTEMPLATE(ID), QUERY TEXT, QUERY_ENCRYPT_NUMBER BIGINT, PRIMARY KEY (ID), INDEX(QUERY_ENCRYPT_NUMBER))", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM PQSTATE") == null) {
            executeQuery("CREATE TABLE PQSTATE(ID INT NOT NULL AUTO_INCREMENT, STATE VARCHAR(255), PRIMARY KEY (ID))", new Object[]{});
        }        
        
        if (getSingleResult("SELECT count(*) FROM PQUERYSTATS") == null) {
            executeQuery("CREATE TABLE PQUERYSTATS(ID INT NOT NULL AUTO_INCREMENT, QUERY_TEMPLATE_ID INT NOT NULL, FOREIGN KEY PQUERYSTATS_PQUERYTEMPLATE__FK(QUERY_TEMPLATE_ID) REFERENCES PQUERYTEMPLATE(ID), QUERY_ID INT NOT NULL, FOREIGN KEY PQUERYSTATS_PQUERY__FK(QUERY_TEMPLATE_ID) REFERENCES PQUERY(ID), USERHOST_ID INT NOT NULL, FOREIGN KEY PQUERYSTATS_PQUSERHOST__FK(QUERY_TEMPLATE_ID) REFERENCES PQUSERHOST(ID), TIME DECIMAL(20,6), DATETIME TIMESTAMP, DB_ID INT NOT NULL, FOREIGN KEY PQUERYSTATS_PQDB__FK(DB_ID) REFERENCES PQDB(ID), PRIMARY KEY (ID), INDEX(QUERY_TEMPLATE_ID, QUERY_ID, USERHOST_ID, DB_ID), INDEX(DATETIME, QUERY_TEMPLATE_ID))", new Object[]{});
        }
        
        if (getSingleResult("SELECT count(*) FROM PQSTATESTAT") == null) {
            executeQuery("CREATE TABLE PQSTATESTAT(ID INT NOT NULL AUTO_INCREMENT, STATE_ID INT NOT NULL, FOREIGN KEY PQSTATESTAT_PQSTATE__FK(STATE_ID) REFERENCES PQSTATE(ID), TIME DECIMAL(20,6), DATETIME TIMESTAMP, QUERYSTATS_ID INT(11) NOT NULL, FOREIGN KEY PQSTATESTAT_QUERYSTATS__FK(QUERYSTATS_ID) REFERENCES PQUERYSTATS(ID), PRIMARY KEY (ID), INDEX(STATE_ID, QUERYSTATS_ID))", new Object[]{});
        }
    }

    @Override
    public String getProviderType() {
        return QueryProvider.DB__MYSQL;
    }
    
    
    @Override
    protected ResultSet executeQuery(String sql) throws SQLException {
        this.executeQuery(sql, new Object[]{});
        return null;
    }
    

    @Override
    public void beginTransaction() {
        if (inited) {
            try {
                getDB().setAutoCommit(false);
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void commitTransaction() {
        if (inited) {
            try {
                getDB().commit();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void rollbackTransaction() {
        if (inited) {
            try {
                getDB().rollback();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void executeQuery(String query, Object... params) {
        if (inited) {
            try {            
                createQuery(query, params).execute();
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public List getResultList(String query, Object... params) {
        
        List list = new ArrayList<>();
        
        if (inited) {
            try (ResultSet rs = createQuery(query, params).executeQuery()){

                if (rs != null) {

                    ResultSetMetaData rmd = rs.getMetaData();

                    while (rs.next()) {
                        if (rmd.getColumnCount() == 1) {
                            list.add(rs.getObject(1));
                        } else {
                            Object[] data = new Object[rmd.getColumnCount()];
                            for (int i = 0; i < rmd.getColumnCount(); i++) {
                                data[i] = rs.getObject(i + 1);
                            }

                            list.add(data);
                        }
                    }
                }
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        
        return list;
    }

    @Override
    public Object getSingleResult(String query, Object... params) {
        if (inited) {
            try (ResultSet rs = createQuery(query, params).executeQuery()){

                ResultSetMetaData rmd = rs.getMetaData();
                if (rmd.getColumnCount() == 1) {
                    if (rs != null && rs.next()) {
                        return rs.getObject(1);
                    }
                } else {
                    if (rs != null && rs.next()) {
                        Object[] data = new Object[rmd.getColumnCount()];
                        for (int i = 0; i < rmd.getColumnCount(); i++) {
                            data[i] = rs.getObject(i + 1);
                        }

                        return data;
                    }
                }
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return null;
    }
    
    private PreparedStatement createQuery(String query, Object... params) {
        if (inited) {
            try {
                PreparedStatement ps = getDB().prepareStatement(query);

                if (params.length > 0) {
                    for (int i = 1; i <= params.length; i++) {
                        ps.setObject(i, params[i-1]);
                    }
                }

                return ps;
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        
        return null;
    }
}
