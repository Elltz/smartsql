package np.com.ngopal.smart.sql.db;

import com.google.inject.Singleton;
import np.com.ngopal.smart.sql.model.ScheduleParams;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Singleton
public class ScheduleParamsService extends AbstractSmartSqlService<ScheduleParams>
{
    public ScheduleParamsService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
}
