package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.ProfilerSettings;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ProfilerSettingsService extends AbstractSmartSqlService<ProfilerSettings>
{

    public ProfilerSettingsService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
//    public ProfilerSettingsService() {
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("profiler-persistence-unit");
//        em = emf.createEntityManager();   
//    }
    
    public ProfilerSettings getProfilerSettings() {
        final List<ProfilerSettings> settings = getAll();
        
        // we use only one object
        if (settings != null && !settings.isEmpty()) {
            return settings.get(0);
        }
        
        return new ProfilerSettings();
    }
    
}