package np.com.ngopal.smart.sql.db;

import np.com.ngopal.smart.sql.model.SSHHotKeys;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SSHHotKeyService extends AbstractSmartSqlService<SSHHotKeys>
{
    public SSHHotKeyService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
}