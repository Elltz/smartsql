package np.com.ngopal.smart.sql.db;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import np.com.ngopal.smart.sql.model.HistoryQuery;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class HistoryQueryService extends AbstractSmartSqlService<HistoryQuery>
{

    public HistoryQueryService(String persistanceUnitName) {
        super(persistanceUnitName);
        
        // If total queries more than 1000 then delete older than three months queries
        List<HistoryQuery> list = getAll();
        if (list != null && list.size() > 1000) {
            Calendar threeMonthEarly = Calendar.getInstance();
            threeMonthEarly.setTime(new Date());
            threeMonthEarly.add(Calendar.MONTH, -3);
            
            Date eralyDate = threeMonthEarly.getTime();
            for (HistoryQuery hq: list) {
                if (hq.getExecuted().before(eralyDate)) {
                    delete(hq);
                }
            }
        }
    }
    
    @Override
    public synchronized HistoryQuery save(HistoryQuery t) throws PersistenceException {
        return super.save(t);
    }
    
    public synchronized List<HistoryQuery> selectAllOnMonth(Long connectionId) {
        
        TypedQuery<HistoryQuery> columnQuery = em.createNamedQuery("historyQuery.selectByConnectionIdWithOrder", HistoryQuery.class);
        columnQuery.setParameter("connectionId", connectionId);
        
        List<HistoryQuery> list = columnQuery.getResultList();
        
        // get only last month
        Date date = new Date();
        list = list.stream().filter(t -> notOldThanMonth(t.getExecuted(), date)).collect(Collectors.toList());
        
        for (int i = 0; i < list.size(); i++) {
            HistoryQuery hq = list.get(i);
            hq.setNumber(i + 1);
        }
        
        return list;
    }
    
    private boolean notOldThanMonth(Date d0, Date today) {
        Calendar c0 = new GregorianCalendar();
        c0.setTime(d0);
        
        Calendar c1 = new GregorianCalendar();
        c1.setTime(today);
        
        c0.add(Calendar.MONTH, 1);
        
        return c0.after(c1);
    }
}