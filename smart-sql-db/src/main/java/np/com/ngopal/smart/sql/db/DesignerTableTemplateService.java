/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.util.List;
import javax.persistence.PersistenceException;
import np.com.ngopal.smart.sql.model.DesignerTableTemplate;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class DesignerTableTemplateService extends AbstractSmartSqlService<DesignerTableTemplate> {

    public DesignerTableTemplateService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    @Override
    public List<DesignerTableTemplate> getAll() {
        return super.getAll();
    }

    @Override
    public DesignerTableTemplate save(DesignerTableTemplate t) throws PersistenceException {
        boolean contains = false;
        DesignerTableTemplate xx =null;
        for(DesignerTableTemplate x : getAll()){
            if(x.getName().equals(t.getName())){
                contains = true;
                xx= x;
                break;
            }
        }
        if (contains) {
            xx.setData(t.getData());
            return update(xx);
        } else {
            return super.save(t);
        }
    }

}
