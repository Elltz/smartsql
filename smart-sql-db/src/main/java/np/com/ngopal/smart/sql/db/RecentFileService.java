/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.RecentFiles;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class RecentFileService extends AbstractSmartSqlService<RecentFiles> {
    
    private static final int MAX_SHOWED_FILES = 9;
    
    public RecentFileService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public boolean isNeedSave(String path) {
        if (path != null) {
            
            List<RecentFiles> files = getLastFiles();
            if (files != null) {
                for (RecentFiles rf: files) {
                    if (path.equalsIgnoreCase(rf.getRecentFile())) {
                        return false;
                    }
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    public List<RecentFiles> getLastFiles() {
        
        List<RecentFiles> list = getAll();
        if (list != null && list.size() > MAX_SHOWED_FILES) {
            return list.subList(list.size() - MAX_SHOWED_FILES, list.size());
        } else {
            return list;
        }
    }
}
