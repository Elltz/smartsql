package np.com.ngopal.smart.sql.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import javax.persistence.Persistence;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.model.core.BaseModel;
import np.com.ngopal.services.core.AbstractService;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @param <T>
 */
@Slf4j
public abstract class AbstractSmartSqlService<T extends BaseModel> extends AbstractService<T> {

    @Getter
    private String persistanceUnitName;
    
    public AbstractSmartSqlService(String persistanceUnitName) {
        this.persistanceUnitName = persistanceUnitName;
        
        em = Persistence.createEntityManagerFactory(persistanceUnitName).createEntityManager();
    }
    
    public String streamToString(final InputStream inputStream) {
        try (
            final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))
        ) {
            return br.lines().parallel().collect(Collectors.joining("\n"));
        } catch (final IOException e) {
            log.warn(e.getMessage(), e);
        }
        
        return null;
    }
}
