package np.com.ngopal.smart.sql.db;

import com.sun.javafx.PlatformUtil;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Query;
import lombok.extern.slf4j.Slf4j;
//import np.com.ngopal.smart.sql.utils.PublicServerUtil;
import org.apache.commons.codec.binary.Hex;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class UserUsageStatisticService extends AbstractSmartSqlService {

    private final String[] REMOTE_SERVICE = new String[] {
        "http://checkip.amazonaws.com/",
        "http://icanhazip.com/",
        "http://www.trackip.net/ip",
        "http://myexternalip.com/raw",
        "http://ipecho.net/plain",
        "http://bot.whatismyipaddress.com"
    };

    private static final String AES = "y#JG8cN-buWKL*Y7";
    
    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    
    private final Date openDate = new Date();
    
    public UserUsageStatisticService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    
    public void savePUsage(String feature) {
        synchronized (em) {
            em.getTransaction().begin();
            try {            
                Query q = em.createNativeQuery("INSERT INTO PUSAGE(feature, status) VALUES(?, ?)");
                q.setParameter(1, feature);
                q.setParameter(2, 1);
                q.executeUpdate();

                em.getTransaction().commit();
            } catch (Throwable th) {
                log.error("Error", th);
                em.getTransaction().rollback();
            }
        }
    }
        
    public void savePCDetailsAfterRegistration(String userEmailId, String serverData) {
        
        String os = "Others";
        if (PlatformUtil.isMac()) {
            os = "MAC";
        } else if (PlatformUtil.isWindows()) {
            os = "Windows";
        }
        
        String ipAddress = getPublicIP();
        String macAddress = "";
        try {

            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);

            byte[] mac = network.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            macAddress = sb.toString();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        
        try {
            PublicDataAccess.getInstanсe().userInstallUpdates(userEmailId, os, macAddress, ipAddress, df.format(new Date()));
        } catch (PublicDataException ex) {
            // its ok
            log.warn("Warn", ex);
        }
    }
    
    public void savePCDetailsAfterClosing(String userEmailId, String serverData) {
        
        String os = "Others";
        if (PlatformUtil.isMac()) {
            os = "MAC";
        } else if (PlatformUtil.isWindows()) {
            os = "Windows";
        }
        
        String ipAddress = getPublicIP();
        String macAddress = "";
        try {

            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);

            byte[] mac = network.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            macAddress = sb.toString();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        try {
            PublicDataAccess.getInstanсe().userCloseApp(userEmailId, os, macAddress, ipAddress, df.format(openDate), df.format(new Date()));
        } catch (PublicDataException ex) {
            // its ok
            log.warn("Warn", ex);
        }
    }
    
    public boolean syncWithPublic(int productId, String userEmailId, String license, String serverData) throws PublicDataException {

        synchronized (em) {
            try {
                if (!PublicDataAccess.getInstanсe().userValidation(userEmailId, license)) {
                    return false;
                }
            } catch (PublicDataException ex) {
                log.error("Error", ex);
                throw ex;
            }

            String localIP = "";
            String globalIP = "";
            try {
                InetAddress local = InetAddress.getLocalHost();
                localIP = local.getHostAddress();

                globalIP = getPublicIP();
            } catch (Throwable th) {
                log.error("Error", th);
            }

            Long userSystemId = null;
            try {
                userSystemId = PublicDataAccess.getInstanсe().selectUserSystemId(userEmailId, localIP, globalIP);
            } catch (PublicDataException ex) {
                log.error("Error", ex);
            }

            if (userSystemId == null) {
                // insert new yser system
                try {
                    userSystemId = PublicDataAccess.getInstanсe().insertUserSystem(userEmailId, localIP, globalIP);
                } catch (PublicDataException ex) {
                    log.error("Error", ex);
                }
            }

            if (userSystemId != null) {

                List<Map<String, Object>> data = new ArrayList<>();
                Query query = em.createNativeQuery("SELECT feature, status, createat FROM PUSAGE");
                List<Object[]> result = query.getResultList();
                for (Object[] row: result) {
                    Map<String, Object> d = new HashMap<>();
                    d.put("userSystemId", userSystemId);
                    d.put("feature", row[0]);
                    d.put("status", row[1]);
                    d.put("createAt", df.format((Timestamp)row[2]));

                    data.add(d);
                }



                try {            
                    if (PublicDataAccess.getInstanсe().syncUserUsageStatistic(productId, data)) {
                        // need remove all
                        em.getTransaction().begin();
                        try {
                            em.createNativeQuery("DELETE FROM PUSAGE").executeUpdate();
                            em.getTransaction().commit();
                        } catch (Throwable th) {
                            em.getTransaction().rollback();
                        }
                    }
                } catch (PublicDataException ex) {
                    log.error("Error on sync with public server", ex);
                }
            }
        }
        
        return true;
    }
    
    
    

    private String getPublicIP(){
        for (String url: REMOTE_SERVICE) {
            try {
                URL whatismyip = new URL(url);
                BufferedReader in = null;
                try {
                    in = new BufferedReader(new InputStreamReader(
                            whatismyip.openStream()));
                    
                    return in.readLine();
                    
                } finally {
                    if (in != null) {
                        in.close();
                    }
                }
            } catch (Throwable th) {
                if (!UnknownHostException.class.isInstance(th)) {
                    log.error("Error", th);
                }
            }
        }
        
        return "";
    }
    
    public static String aes_encrypt(String string) {
        if (string != null) {
            try {
                SecretKey key = new SecretKeySpec(AES.getBytes("UTF-8"), "AES");

                Cipher aesCipher = Cipher.getInstance("AES");
                aesCipher.init(Cipher.ENCRYPT_MODE, key);

                byte[] byteCipherText = aesCipher.doFinal(string.getBytes("UTF-8"));
                return new String(Hex.encodeHex(byteCipherText));

            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            } 
        }
        
        return null;
    }
}
