package np.com.ngopal.smart.sql.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.TypedQuery;
import np.com.ngopal.smart.sql.model.DeadlockData;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DeadlockService extends AbstractSmartSqlService<DeadlockData>
{
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public DeadlockService(String persistanceUnitName) {
        super(persistanceUnitName);
    }

    
    public List<DeadlockData> getAll(String paramsKey, int days) {
        
        // Remove all 7 days old
        List<DeadlockData> forDelete = new ArrayList<>();
        
        List<DeadlockData> list = getAll();
        Date today = new Date();

        Long dayTime = 1000 * 60 * 60 * 24l;
        
        for (DeadlockData d: list) {
            try {
                Date date = DATE_FORMAT.parse(d.getDeadlockTime());
                if ((today.getTime() - date.getTime()) / dayTime >= days) {
                    forDelete.add(d);
                }
            } catch (Throwable th) {
            }
        }
        
        for (DeadlockData d: forDelete) {
            delete(d);
        }
        
        TypedQuery<DeadlockData> childrenQuery = em.createNamedQuery("deadlockdata.findByParamsKey", DeadlockData.class);
        childrenQuery.setParameter("paramsKey", paramsKey);
        
        return childrenQuery.getResultList();
    }
    
    
    public void deleteAll(String paramsKey) {
        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM DEADLOCK_DATA WHERE PARAMS_KEY = '" + paramsKey + "'").executeUpdate();
        em.getTransaction().commit();
    }

    public void delete(String paramsKey, String deadlockTime, String transaction1, String transaction2) {
        em.getTransaction().begin();
        TypedQuery<DeadlockData> childrenQuery = em.createNamedQuery("deadlockdata.delete", DeadlockData.class);
        childrenQuery.setParameter("paramsKey", paramsKey);
        childrenQuery.setParameter("deadlockTime", deadlockTime);
        childrenQuery.setParameter("transaction1", transaction1);
        childrenQuery.setParameter("transaction2", transaction2);
        childrenQuery.executeUpdate();
        em.getTransaction().commit();
    }
        
    
}