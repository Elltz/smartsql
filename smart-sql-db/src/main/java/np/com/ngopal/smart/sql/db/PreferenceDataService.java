package np.com.ngopal.smart.sql.db;

import com.google.inject.Singleton;
import java.util.List;
import np.com.ngopal.smart.sql.model.PreferenceData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Singleton
public class PreferenceDataService extends AbstractSmartSqlService<PreferenceData>
{

    public PreferenceDataService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    @Override
    public List<PreferenceData> getAll() {
        List<PreferenceData> originalList = super.getAll();        
        if(originalList.isEmpty()){
            PreferenceData prefData = new PreferenceData();
            prefData.restoreAllDefaults();
            originalList.add(save(prefData));
        }        
        return originalList; 
    }
    
    public PreferenceData getPreference() {
        return getAll().get(0);
    }

}