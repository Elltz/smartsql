/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.PersistenceException;
import javax.xml.parsers.ParserConfigurationException;
import np.com.ngopal.smart.sql.model.TableTemplate;
import org.xml.sax.SAXException;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class TableTemplateService extends AbstractSmartSqlService<TableTemplate> {

    public TableTemplateService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    @Override
    public List<TableTemplate> getAll() {
        List<TableTemplate> list =  super.getAll(); 
        for(TableTemplate tt : list){
            try {
                if(tt.table == null || tt.table.getColumns() == null){
                    tt.initTable();
                }
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                Logger.getLogger(TableTemplateService.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(tt.runs == null){
                tt.runs = new Runnable() {

                    @Override
                    public void run() {
                        TableTemplateService.this.save(tt);
                    }
                };
            }
        }
        return list;
    }

    @Override
    public TableTemplate save(TableTemplate t) throws PersistenceException {
        if(super.getAll().contains(t)){
            return update(t);
        }else{
            return super.save(t);
        }
    }

    @Override
    public boolean delete(TableTemplate t) {
        if (super.getAll().contains(t)) {
            return super.delete(t);
        }
        return false;
    }

    
    
}
