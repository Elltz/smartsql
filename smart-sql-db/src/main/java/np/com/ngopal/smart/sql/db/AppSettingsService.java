package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.AppSettings;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class AppSettingsService extends AbstractSmartSqlService<AppSettings>
{
    public static final String SETTING_NAME__LAST_OPENED_CONNECTION = "lastOpenedConnection";
    
    public AppSettingsService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public String getSettingValue(String name) {
        List<AppSettings> list = getAll();
        if (list != null) {
            for (AppSettings as: list) {
                if (as.getName() != null && as.getName().equals(name)) {
                    return as.getValue();
                }
            }
        }
        
        return null;
    }
    
    public void putSettingValue(String name, String value) {
        List<AppSettings> list = getAll();
        
        AppSettings forSave = null;
        if (list != null) {
            for (AppSettings as: list) {
                if (as.getName() != null && as.getName().equals(name)) {
                    as.setValue(value);
                    forSave = as;
                    break;
                }
            }
        }
        
        if (forSave == null) {
            forSave = new AppSettings(name, value);
        }
        
        save(forSave);
    }
}