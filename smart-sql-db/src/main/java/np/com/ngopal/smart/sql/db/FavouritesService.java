package np.com.ngopal.smart.sql.db;

import java.util.List;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.Favourites;

/**
 * @author Terah Laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class FavouritesService extends AbstractSmartSqlService<Favourites> {

    private static final String DEFAULT_FOLDER = "SmartMySQL";
    
    public FavouritesService(String persistanceUnitName) {
        super(persistanceUnitName);
        
        checkDefaults();
    }
    
    private void checkDefaults() {
        
        Favourites defaultFavor = find(null, DEFAULT_FOLDER, true);
        if (defaultFavor == null) {
            String query = streamToString(getClass().getResourceAsStream("/META-INF/scripts/favorites__importDefaultFolder.sql"));
            if (query != null) {
                em.getTransaction().begin();
                em.createNativeQuery(query).executeUpdate();
                em.getTransaction().commit();
                
                defaultFavor = find(null, DEFAULT_FOLDER, true);
            }
        }
        
        // clear old
        em.getTransaction().begin();
        int deletedCount = em.createNativeQuery("DELETE FROM FAVOURITES WHERE PREDEFINED is TRUE").executeUpdate();
        em.getTransaction().commit();
        
        if (deletedCount == 0) {
            em.getTransaction().begin();
            em.createNativeQuery("DELETE FROM FAVOURITES WHERE PARENT = " + defaultFavor.getId()).executeUpdate();
            em.getTransaction().commit();
        }

        
        if (defaultFavor != null) {
            List<Favourites> list = findChildren(defaultFavor.getId());
            if (list == null || list.isEmpty() || deletedCount > 0) {
                String query = streamToString(getClass().getResourceAsStream("/META-INF/scripts/favorites__importDefaultQueries.sql"));
                if (query != null) {
                    // replace parent id
                    query = query.replaceAll("\\$\\$PARENT\\$\\$", "" + defaultFavor.getId());

                    em.getTransaction().begin();
                    em.createNativeQuery(query).executeUpdate();
                    em.getTransaction().commit();
                }
            }
        }        
    }
    
    
    
    public List<Favourites> findChildren(Long parent) {        
        TypedQuery<Favourites> childrenQuery = em.createNamedQuery("favourites.findByParent", Favourites.class);
        childrenQuery.setParameter("parent", parent);
        
        // TODO Refactor this
        List<Favourites> list = childrenQuery.getResultList();
        if (parent == null) {
            for (Favourites f: list) {
                if (DEFAULT_FOLDER.equals(f.getName())) {
                    list.remove(f);
                    list.add(0, f);
                    break;
                }
            }
        }
        
        return list;
    }
    
    public Favourites find(Long parent, String name, boolean folder) {
        List<Favourites> list = getAll();
        if (list != null) {
            for (Favourites f: list) {
                if (f.isFolder() == folder) {
                    if ((parent == null && f.getParent() == null || parent != null && parent.equals(f.getParent())) && name.equals(f.getName())) {
                        return f;
                    }
                }
            }
        }
        
        return null;
    }
}