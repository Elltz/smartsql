/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.util.List;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class ConnectionParamService extends AbstractSmartSqlService<ConnectionParams> {

        
    public ConnectionParamService(String persistanceUnitName) {
        super(persistanceUnitName);
        
        // TODO This is fix of one of bug
        // we need remove all dublicated objects
        
//        TypedQuery<ConnectionParams> paramsQuery = em.createNamedQuery("connectionParams.selectOrderedById", ConnectionParams.class);
//        List<ConnectionParams> list = paramsQuery.getResultList();
//        
//        try {
//            em.getTransaction().begin();
//            if (list != null) {
//                Map<String, Long> map = new HashMap<>();
//                for (ConnectionParams cp: list) {
//                    if (map.get(cp.getName()) == null) {
//                        map.put(cp.getName(), cp.getId());
//                    } else {
//                        // need remove and maybe do some update
//                        Query q = em.createNamedQuery("connectionParams.deleteById");
//                        q.setParameter("id", cp.getId());
//                        q.executeUpdate();
//                    }
//                }
//            }
//            em.getTransaction().commit();
//        } catch (Throwable th) {
//            em.getTransaction().rollback();
//            th.printStackTrace();
//        }
    }
    
    public boolean checkConnectionParamsName(String name) {
        TypedQuery<ConnectionParams> paramsQuery = em.createNamedQuery("connectionParams.findByName", ConnectionParams.class);
        paramsQuery.setParameter("name", name);
        List<ConnectionParams> list = paramsQuery.getResultList();
        ConnectionParams c = list != null && !list.isEmpty() ? list.get(0) : null;
        return c != null;
    }
    
    public ConnectionParams findConenctionParamsByName(String name) {
        TypedQuery<ConnectionParams> paramsQuery = em.createNamedQuery("connectionParams.findByName", ConnectionParams.class);
        paramsQuery.setParameter("name", name);
        List<ConnectionParams> list = paramsQuery.getResultList();
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public List<ConnectionParams> getAll() {
        // Clear for loading new
        em.clear();
        return super.getAll(); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    @Override
    public synchronized ConnectionParams save(ConnectionParams t) throws PersistenceException {
        
//        if (t != null && t.getDatabases() != null) {
//            for (Database d: t.getDatabases()) {
//                syncDatabase(d);
//            }
//        }
        ConnectionParams result = super.save(t);
        return result;
    }
   
    
    

}
