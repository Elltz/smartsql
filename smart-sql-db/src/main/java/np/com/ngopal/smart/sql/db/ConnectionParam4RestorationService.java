/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.util.List;
import javax.persistence.PersistenceException;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ConnectionParam4RestorationService extends AbstractSmartSqlService<ConnectionParams> {

    public ConnectionParam4RestorationService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    @Override
    public ConnectionParams save(ConnectionParams t) throws PersistenceException {
        List<ConnectionParams> ls = getAll();
        if(ls.contains(t)){ return update(t); }
        
        for(ConnectionParams cp : ls){
            if(cp.equals(t)){
                return t;
            }
        }
        return super.save(t);
    }
    
}
