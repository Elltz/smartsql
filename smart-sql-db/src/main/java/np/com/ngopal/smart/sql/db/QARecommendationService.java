package np.com.ngopal.smart.sql.db;

import java.util.List;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.QARecommendation;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class QARecommendationService extends AbstractSmartSqlService<QARecommendation>
{
    public QARecommendationService(String persistanceUnitName) {
        super(persistanceUnitName);
    }

    public List<QARecommendation> findRecommendations(Long connectionParamsId, String database, String query) {
        synchronized (em) {
            TypedQuery<QARecommendation> findQuery = em.createNamedQuery("qarecommendation.findRecommendations", QARecommendation.class);
            findQuery.setParameter("connectionParamsId", connectionParamsId);
            findQuery.setParameter("database", database);
            findQuery.setParameter("query", query);

            return findQuery.getResultList();
        }
    }

    @Override
    public QARecommendation save(QARecommendation t) throws PersistenceException {
        synchronized (em) {
            return super.save(t);
        }
    }
   
    
    public void removeRecommendations(Long connectionParamsId, String database, String query) {
        synchronized (em) {
            em.getTransaction().begin();
            try {
                Query removeQuery = em.createNamedQuery("qarecommendation.removeRecommendations");
                removeQuery.setParameter("connectionParamsId", connectionParamsId);
                removeQuery.setParameter("database", database);
                removeQuery.setParameter("query", query);

                removeQuery.executeUpdate();
                em.getTransaction().commit();
            } catch (Throwable th) {
                log.error("Error", th);
                em.getTransaction().rollback();
            }
        }
    }
}