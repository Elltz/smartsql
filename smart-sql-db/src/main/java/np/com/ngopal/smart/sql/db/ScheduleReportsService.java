package np.com.ngopal.smart.sql.db;

import com.google.inject.Singleton;
import np.com.ngopal.smart.sql.model.ScheduleReport;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Singleton
public class ScheduleReportsService extends AbstractSmartSqlService<ScheduleReport>
{
    public ScheduleReportsService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
}
