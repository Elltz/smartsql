package np.com.ngopal.smart.sql.db;

import java.util.List;
import np.com.ngopal.smart.sql.model.LicenseData;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class LicenseDataService extends AbstractSmartSqlService<LicenseData>
{    
    public LicenseDataService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public LicenseData getLicenseData() {
        List<LicenseData> list = getAll();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}