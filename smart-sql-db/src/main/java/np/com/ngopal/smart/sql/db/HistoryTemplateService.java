package np.com.ngopal.smart.sql.db;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import np.com.ngopal.smart.sql.model.HistoryTemplate;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class HistoryTemplateService extends AbstractSmartSqlService<HistoryTemplate>
{
    public HistoryTemplateService(String persistanceUnitName) {
        super(persistanceUnitName);
        
        // If total queries more than 1000 then delete older than three months queries
        List<HistoryTemplate> list = getAll();
        if (list != null && list.size() > 1000) {
            Calendar threeMonthEarly = Calendar.getInstance();
            threeMonthEarly.setTime(new Date());
            threeMonthEarly.add(Calendar.MONTH, -3);
            
            Date eralyDate = threeMonthEarly.getTime();
            for (HistoryTemplate hq: list) {
                if (hq.getRecentlyExecuted().before(eralyDate)) {
                    delete(hq);
                }
            }
        }
    }

    @Override
    public synchronized HistoryTemplate save(HistoryTemplate t) throws PersistenceException {
        return super.save(t);
    }
    
    

    public synchronized HistoryTemplate findHistoryTemplateByTemplate(String template, Long connectionId) {
        
        TypedQuery<HistoryTemplate> columnQuery = em.createNamedQuery("history.findByTemplateAndConnectionId", HistoryTemplate.class);
        columnQuery.setParameter("queryTemplate", template);
        columnQuery.setParameter("connectionId", connectionId);
        
        List<HistoryTemplate> list = columnQuery.getResultList();
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }
    
    public synchronized List<HistoryTemplate> selectAllWithOrder(Long connectionId) {
        
        TypedQuery<HistoryTemplate> columnQuery = em.createNamedQuery("history.selectByConnectionIdWithOrder", HistoryTemplate.class);
        columnQuery.setParameter("connectionId", connectionId);
        
        List<HistoryTemplate> list = columnQuery.getResultList();
        
        // get only last month
        Date date = new Date();
        list = list.stream().filter(t -> notOldThanMonth(t.getRecentlyExecuted(), date)).collect(Collectors.toList());
        
        int rank = 1;
        for (HistoryTemplate ht: list) {
            ht.setRank(rank++);
        }
        return list;
    }
    
    private boolean notOldThanMonth(Date d0, Date today) {
        Calendar c0 = new GregorianCalendar();
        c0.setTime(d0);
        
        Calendar c1 = new GregorianCalendar();
        c1.setTime(today);
        
        c0.add(Calendar.MONTH, 1);
        
        return c0.after(c1);
    }
}