package np.com.ngopal.smart.sql.db;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.TypedQuery;
import np.com.ngopal.smart.sql.model.ColumnCardinality;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ColumnCardinalityService extends AbstractSmartSqlService<ColumnCardinality>
{
    private static final Map<String, Map<String, Map<String, Map<String, Long>>>> CARDINALITY_CACHE = new HashMap<>();
    
    public ColumnCardinalityService(String persistanceUnitName) {
        super(persistanceUnitName);
    }
    
    public Long getColumnCardinality(String paramsKey, String database, String table, String column) {
        
        synchronized (em) {
            Map<String, Map<String, Map<String, Long>>> result = CARDINALITY_CACHE.get(paramsKey);
            if (result == null) {

                TypedQuery<ColumnCardinality> childrenQuery = em.createNamedQuery("columncardinality.find", ColumnCardinality.class);
                childrenQuery.setParameter("paramsKey", paramsKey);

                result = new HashMap<>();
                for (ColumnCardinality cc: childrenQuery.getResultList()) {
                    Map<String, Map<String, Long>> dbMap = result.get(cc.getDatabase());
                    if (dbMap == null) {
                        dbMap = new HashMap<>();
                        result.put(cc.getDatabase(), dbMap);
                    }

                    Map<String, Long> tableMap = dbMap.get(cc.getDbtable());
                    if (tableMap == null) {
                        tableMap = new HashMap<>();
                        dbMap.put(cc.getDbtable(), tableMap);
                    }

                    tableMap.put(cc.getDbcolumn(), cc.getCardinality());
                }

                CARDINALITY_CACHE.put(paramsKey, result);
            }

            Map<String, Map<String, Long>> dbMap = result.get(database);
            if (dbMap != null) {
                Map<String, Long> tableMap = dbMap.get(table);
                if (tableMap != null) {
                    return tableMap.get(column);
                }
            }

            return null;
        }
    }
    
    
    public void deleteAll(String paramsKey) {
        synchronized (em) {
            em.getTransaction().begin();
            em.createNativeQuery("DELETE FROM COLUMN_CARDINALITY WHERE PARAMS_KEY = '" + paramsKey + "'").executeUpdate();
            em.getTransaction().commit();
        }
    }

    public void insert(String paramsKey, String database, String table, String column, Long cardinality) {
        synchronized (em) {
            ColumnCardinality cc = new ColumnCardinality(paramsKey, database, table, column, cardinality);
            save(cc);

            Map<String, Map<String, Map<String, Long>>> result = CARDINALITY_CACHE.get(paramsKey);
            if (result == null) {
                result = new HashMap<>();
                CARDINALITY_CACHE.put(paramsKey, result);
            }

            Map<String, Map<String, Long>> dbMap = result.get(database);
            if (dbMap == null) {
                dbMap = new HashMap<>();
                result.put(database, dbMap);
            }

            Map<String, Long> tableMap = dbMap.get(table);
            if (tableMap == null) {
                tableMap = new HashMap<>();
                dbMap.put(table, tableMap);
            }

            tableMap.put(column, cardinality);
        }
    }
        
    
}