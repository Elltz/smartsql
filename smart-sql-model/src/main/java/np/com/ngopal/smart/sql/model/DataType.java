/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.Set;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public interface DataType extends Displayable, Comparable<DataType>, NamedType {

    public boolean isEnumOrSet();
    
    public boolean isEnum();
    
    public boolean isSet();
    
    public boolean isInteger();
    
    public boolean hasPrecisionInteger();    
    
    public boolean isLargeString();

    public boolean hasLength();

    public Integer getLength();

    public Set<String> getList();

    public void setList(Set list);

    public Integer getPrecision();

    public void setPrecision(Integer i);

    public void setLength(Integer i);

    public String getName();

    public TechnologyType getTech();

    public DataType copy(DataType type);
    
    
    public default String calcDataTypeStr() {
        String length = "";
        if (hasLength() && getLength() != null && getLength() >= 0) {
            length = " (" + getLength() + (getPrecision() != null ? ", " + getPrecision() : "") + ")";
        } else if (getList() != null && !getList().isEmpty()) {
            length = " (";
            for (String s: getList()) {
                length += "'" + s + "', ";
            }
            length = length.substring(0, length.length() - 2);
            length += ")";
        }
        
        return getName() + length;
    }
}
