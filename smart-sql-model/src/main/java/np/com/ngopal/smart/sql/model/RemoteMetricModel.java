/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "REMOTE_METRIC_MODEL")
public class RemoteMetricModel extends BaseModel {
    
    @javax.persistence.Column(name = "LOG_TIME")
    private long timeStamp;
    
    @javax.persistence.Column(name = "LOGS")
    private String value;
    
    @javax.persistence.Column(name = "METRICTYPE")
    private String metricType;
    
    @javax.persistence.Column(name = "PARAMTOKEN")
    private long paramIdentifier;
    
    public boolean isMutualOnProps(RemoteMetricModel other){
        return other != null 
                && paramIdentifier == other.paramIdentifier 
                && timeStamp == other.timeStamp 
                && Objects.equals(metricType, other.metricType);
    }
}
