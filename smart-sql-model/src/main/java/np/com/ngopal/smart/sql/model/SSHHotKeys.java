/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SSH_HOTKEYS")
public class SSHHotKeys extends BaseModel {

    private String nextServerTab;

    private String prevServerTab;

    private String addServer;

    private String removeServer;

    private String connectToSelected;

    private String propertiesOfSelected;

    private String importTree;

    private String exportTree;

    private String connectTo;

    @Column(name = "DUPLICATE_CONNECTION")
    private String duplicateConnection;

    private String remapHotkeys;

}
