package np.com.ngopal.smart.sql.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "OS_SERVER_SPECS")

public class OsServerSpecs extends BaseModel {

    private static final String OS__WINDOWS = "Windows";
    private static final String OS__UBUNTU = "Ubuntu";
    private static final String OS__MAC = "Mac";
    private static final String OS__REDHAT = "RedHat / CentOS";
    private static final String OS__DEBIAN = "Debian";
    private static final String OS__SUSE = "SUSE Linux / Open Suse";
    private static final String OS__OTHERS = "Other Linux distributions";
    private static final String OS__AMAZON_S3 = "Aws S3";
    private static final String OS__GOOGLE_CLOUD = "google cloud";

    
    
    /**
     * If you need any new variable or field please add it here, if i see i will 
     * implement the setter.
     */
    @javax.persistence.Column(name = "ram_size")
    private String ramSize;
    
    @javax.persistence.Column(name = "server_os")
    private String serverOs;
    
    @javax.persistence.Column(name = "disk_size")
    private String diskSize;
    
    @javax.persistence.Column(name = "server_arch")
    private String serverArch;
    
    @javax.persistence.Column(name = "ram_cores_number")
    private String numberOfRamCore;    
        
    @javax.persistence.Column(name = "server_platform")
    private String platformType = OS__UBUNTU;

    @javax.persistence.Column(name = "current_usage")
    private boolean isCurrentlyInUse;    
    
    
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) || (obj instanceof OsServerSpecs) &&
                    Objects.equals(this.diskSize, ((OsServerSpecs)obj).diskSize) &&
                    Objects.equals(this.serverArch, ((OsServerSpecs)obj).serverArch) &&
                    Objects.equals(this.ramSize, ((OsServerSpecs)obj).ramSize) &&
                    Objects.equals(this.platformType, ((OsServerSpecs)obj).platformType) &&                   
                    Objects.equals(this.serverOs, ((OsServerSpecs)obj).serverOs) &&                   
                    Objects.equals(this.numberOfRamCore, ((OsServerSpecs)obj).numberOfRamCore);
    }
        
}
