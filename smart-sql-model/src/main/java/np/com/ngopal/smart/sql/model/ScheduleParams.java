package np.com.ngopal.smart.sql.model;

import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@Table(name = "SCHEDULE_PARAMS")
public class ScheduleParams extends Schedule  {       

    @Column(name = "CHARSET")
    private String charset;
    
    @Column(name = "PREFIX_WITH_TIMESTAMP")
    private boolean prefixWithTimestamp = false;
    @Column(name = "SINGLE_TRANSACTION")
    private boolean singleTransaction = false;
    @Column(name = "LOCK_TABLES")
    private boolean lockTables = false;
    @Column(name = "FLUSH_LOGS")
    private boolean flushLogs = false;
    @Column(name = "EXPORT_ONLY_DATA")
    private boolean exportOnlyData = false;
    @Column(name = "SET_FOREIGN_KEY_CHEKS")
    private boolean setForeignKeyChecks = false;
    @Column(name = "INCLUDE_CREATE_DATABASE")
    private boolean includeCreateDatabase = false;
    @Column(name = "INCLUDE_USE_DATABASE")
    private boolean includeUseDatabase = false;
    @Column(name = "INCLUDE_DROP")
    private boolean includeDrop = false;
    @Column(name = "EXPORT_ONLY_STRUCTURE")
    private boolean exportOnlyStructure = false;
    @Column(name = "ADD_LOCK_ARROUND_INSERT")
    private boolean addLockArroundInsert = false;
    @Column(name = "CREATE_BULK_INSERT")
    private boolean createBulkInsert = false;
    @Column(name = "IGNORE_DEFINER")
    private boolean ignoreDefiner = false;
    @Column(name = "ONE_ROW_PRE_LINE")
    private boolean oneRowPerLine = false;
    
    @Column(name = "ZIPPING")
    private boolean ziping = false;
    @Column(name = "SUB_FOLDER_WITH_TIMESTAMP")
    private boolean subFolderWithTimestamp = false;
    @Column(name = "FILES_PREFIX_TIMESTAMP_OVERRIDE")
    private boolean filesPrefixTimestampOverride = true;
    @Column(name = "CREATE_COMPLETE_INSERT")
    private boolean createCompleteInsert = false;
    @Column(name = "CREATE_DELAYED_INSERT")
    private boolean createDelayedInsert = false;
    @Column(name = "FLUSH_MASTER_LOG")
    private boolean flushMasterLog = false;
    @Column(name = "FLUSH_SLAVE_LOG")
    private boolean flushSlaveLog = false;
    @Column(name = "AUTO_COMMIT")
    private boolean autocommit = false;
    @Column(name = "KEYS_ARROUND_INSERT")
    private boolean keysArroundInsert = false;
    @Column(name = "INCLUDE_VERSION_INFORAMTION")
    private boolean includeVersionInformation = false;
    
    @Column(name = "SERVER_DEFAULT_SIZE")
    private boolean serverDefaultSize = true;
    
    @Column(name = "ABORT_ON_ERROR")
    private boolean abortOnError = false;
        
    @Column(name = "ONLY_ERROR")
    private boolean onlyError = true;
       
    
    @Column(name = "SINGLE_FILE_FOR_ALL")
    private boolean singleFileForAll = false;
    @Column(name = "FILES_FILE_FIELD")
    private String filesFileField;
        
    @ElementCollection
    @ManyToMany(cascade = CascadeType.ALL)
    private Map<String, String> selectedData;
    
    @Transient
    private boolean stopExport = false;
    
    @Transient
    private String scheduleType = "backup";

    public ScheduleParams() {
    }

    public ScheduleParams(ConnectionParams params) {
        super(params);
    }
}