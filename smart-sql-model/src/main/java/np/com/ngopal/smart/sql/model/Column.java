package np.com.ngopal.smart.sql.model;

import java.util.Objects;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataTypes;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@NoArgsConstructor
@Getter
@Setter
@Slf4j
@AllArgsConstructor
public class Column implements Comparable<Column>, DBElement {

    private String originalName;
    private String name;

    private int orderIndex = -1;

    private int updateIndex = -1;

    private TableProvider table;

    private DataType dataType;

    private String defaults;

    private boolean notNull;

    private boolean primaryKey;

    private boolean unsigned;

    private boolean autoIncrement;

    private boolean zeroFill;

    private boolean onUpdate;

    private String comment;

    private String characterSet;

    private String collation;
    
    private String key;
    
    private String extra;
    
    private String privileges;
    
    private View view;

    private Long degree;
    
    private Boolean orderByDesc;
    private Boolean groupByDesc;
    
    private Boolean orderBy;
    private Boolean groupBy;
    
    // TODO: NEED CHECK THIS, MAYBE SOME PROBLEMS AFTER CHANGED VALUE TO FALSE
    private boolean constraint = false;
    
    public Column(String name, DBTable table, DataType dataType, String defaults, boolean notNull,
              boolean primaryKey,
              boolean unsigned, boolean autoIncrement, boolean zeroFill, boolean onUpdate, String comment,
              String characterSet, String collation) {
//        setId(id);
        this.originalName = name;
        this.name = name;
        this.table = table;
        this.dataType = dataType;
        this.defaults = defaults;
        this.notNull = notNull;
        this.primaryKey = primaryKey;
        this.unsigned = unsigned;
        this.autoIncrement = autoIncrement;
        this.zeroFill = zeroFill;
        this.onUpdate = onUpdate;
        this.comment = comment;
        this.characterSet = characterSet;
        this.collation = collation;
        this.orderIndex = -1;
        this.updateIndex = -1;
    }

    public void copy(Column c) {
//        setId(c.getId());
        this.originalName = c.originalName;
        this.name = c.name;
        this.table = c.table;
        this.dataType = MySQLDataTypes.getEmptyDataType();
        this.dataType.copy(c.dataType);
        this.defaults = c.defaults;
        this.notNull = c.notNull;
        this.primaryKey = c.primaryKey;
        this.unsigned = c.unsigned;
        this.autoIncrement = c.autoIncrement;
        this.zeroFill = c.zeroFill;
        this.onUpdate = c.onUpdate;
        this.comment = c.comment;
        setCharacterSet(c.characterSet);
        setCollation(c.collation);
        this.orderIndex = -1;
        this.updateIndex = -1;
        this.key = c.key;
        this.extra = c.extra;
        this.privileges = c.privileges;
        this.degree = c.degree;
        this.orderByDesc = c.orderByDesc;
        this.groupByDesc = c.groupByDesc;
        this.orderBy = c.orderBy;
        this.groupBy = c.groupBy;
    }

    @Override
    public String toString() {
        return name + (dataType != null ? " " + calcDataTypeStr() : "");
    }

    public String calcDataTypeStr() {
        String length = "";
        if (dataType.hasLength() && dataType.getLength() != null && dataType.getLength() >= 0) {
            length = " (" + dataType.getLength() + (dataType.getPrecision() != null ? ", " + dataType.getPrecision() : "") + ")";
        } else if (dataType.getList() != null && !dataType.getList().isEmpty()) {
            length = " (";
            for (String s: dataType.getList()) {
                length += "'" + s + "', ";
            }
            length = length.substring(0, length.length() - 2);
            length += ")";
        }
        
        return getDataType().getName() + length;
    }
    @Override
    public int compareTo(Column o) {
        return Objects.equals(o, this) ? 0 : 1;
    }

    
    public boolean isComumnNeedIgnoreForQA() {
       switch (dataType.getName().toUpperCase()) {
            case "TINYBLOB":
            case "BLOB":
            case "MEDIUMBLOB":
            case "LONGBLOB":
            case "JSON":
            case "GEOMETRY":
            case "POINT":
            case "LINESTRING":
            case "POLYGON":
            case "MULTIPOINT":
            case "MULTILINESTRING":
            case "MULTIPOLYGON":
            case "GEOMETRYCOLLECTION":
                return true;
                
            default:
                return false;
        } 
    }
}
