package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DigestQuery extends BaseModel {
    
    private Integer connectionId;
    private String schemaName;
    private String digest;
    private String digestText;
    private String sqlText;
}