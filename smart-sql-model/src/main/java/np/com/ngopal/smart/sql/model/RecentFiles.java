
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Table;
import javax.persistence.Column;
import lombok.NoArgsConstructor;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RecentFiles")
public class RecentFiles extends BaseModel {
    
    @Column(name = "FILENAME")  
    private String recentFile = "";

}
