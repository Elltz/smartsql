/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.tech.mysql;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.DynamicType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class MySQLDataTypes extends DynamicType<DataType> {

    public static final Set<DataType> TYPES;
    private static Properties properties;
    
    public static DataType getEmptyDataType() {
        return new MySQLDataType("", false) {

        };
    }

    static {

        TYPES = new TreeSet<>();
        try {
            properties = new Properties();
            properties.load(MySQLDataType.class.getResourceAsStream("datatypes.properties"));
            log.debug("Loaded");
            for (Object kObj : properties.keySet()) {
                final String key = kObj.toString();
                final boolean value = Boolean.parseBoolean(properties.get(key).toString());
                TYPES.add(new MySQLDataType(key, value) {
                });
            }
        } catch (IOException ex) {
            Logger.getLogger(MySQLDataType.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Set<DataType> getCopyOfTypes() {
        // need return always new object
        // because they used in many comboxes in alter/create table
        // and if its not ne object all columns with equal
        // type - will have equal values of datatype
        
        TreeSet set = new TreeSet<>();
        for (Object kObj : properties.keySet()) {
            final String key = kObj.toString();
            final boolean value = Boolean.parseBoolean(properties.get(key).toString());
            set.add(new MySQLDataType(key, value) {});
        }
        
        return set;
    }

    @Override
    public Set<DataType> getTypes() {
        return TYPES;
    }

    @Override
    public DataType getTypeByName(String name) {

        DataType type = super.getTypeByName(name);
        return type == null ? type : getEmptyDataType().copy(type);
    }

}
