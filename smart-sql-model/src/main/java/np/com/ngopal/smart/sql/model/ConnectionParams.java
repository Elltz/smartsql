/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.*;
import javax.persistence.*;
import lombok.*;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
@Entity
@NamedQueries({
    @NamedQuery(name = "connectionParams.findByName", query = "SELECT cp FROM ConnectionParams cp where cp.name = :name"),
    @NamedQuery(name = "connectionParams.selectOrderedById", query = "SELECT cp FROM ConnectionParams cp ORDER BY cp.id"),
    @NamedQuery(name = "connectionParams.deleteById", query = "DELETE FROM ConnectionParams cp WHERE cp.id = :id"),
})
@AllArgsConstructor
public class ConnectionParams extends BaseModel {

    private String name;

    private TechnologyType type;

    private String address;

    private String username;

    private String password;
    private boolean savePassword = false;

    private int port = 3306;

    private String backgroundColor;

    private String foregroundColor;

    private List<String> database;

    private int limitResult = 1000;

    @Transient
    private String enteredPassword;
    
    @Transient
    private String latestQuery;
    @Transient
    private boolean withLimit = true;
    
    @Transient
    private String newSchemaName;
    @Transient
    private boolean makeBackwardEngine = false;
    
    private int sessionIdleTimeout = 28800;
        
    @javax.persistence.Column(name = "USE_COMPRESSED_PROTOCOL")
    private boolean useCompressedProtocol = true;
    
    @Setter(AccessLevel.NONE)
//    @OneToMany(mappedBy = "params", orphanRemoval = true, cascade = CascadeType.ALL)
    @Transient
    private List<Database> databases;

    @javax.persistence.Column(name = "SELECTED_MODULE_CLASSNAME")
    private String selectedModuleClassName;
    
    @javax.persistence.Column(name = "CONNECTION_TYPE")
    private String connectionType;
    @javax.persistence.Column(name = "OS_NAME")
    private String osName;

    @javax.persistence.Column(name = "OS_HOST")
    private String osHost;
    @javax.persistence.Column(name = "OS_PORT")
    private int osPort = 22;
    @javax.persistence.Column(name = "OS_USER")
    private String osUser;
    @javax.persistence.Column(name = "OS_PASSWORD")
    private String osPassword;
    @javax.persistence.Column(name = "OS_USE_PASSWORD")
    private boolean osUsePassword;
    @javax.persistence.Column(name = "OS_PRIVATE_KEY")
    private String osPrivateKey;
    @javax.persistence.Column(name = "OS_METRIC_DISK_SIZE")
    private String osMetricDiskSize;
    @javax.persistence.Column(name = "OS_METRIC_RAM_SIZE")
    private String osMetricRamSize;
    
    @javax.persistence.Column(name = "USE_SSH_TUNNELING")
    private boolean useSshTunneling = false;
    
    private String sshHost;

    // default ssh port is 22
    private int sshPort = 22;

    private String sshUser;

    private String sshPassword;
    
    @javax.persistence.Column(name = "USE_PRIVATE_KEY")
    private boolean useSshPrivateKey = false;
    @javax.persistence.Column(name = "SSH_PRIVATE_KEY")
    private String sshPrivateKey;

    @javax.persistence.Column(name = "ENABLED_CUSTOM_SQL_MODE")
    private boolean enabledCustomSqlMode = false;
    @javax.persistence.Column(name = "CUSTOM_SQL_MODE")
    private String customSqlMode;
    @javax.persistence.Column(name = "INIT_COMMANDS")
    private String initCommands;
    
    @ElementCollection
    @ManyToMany(cascade = CascadeType.ALL)
    private Map<String, Map<String, String>> moduleParams;
    
    @Setter
    @Getter
    @Transient
    private boolean multiQueries = false;

    @Transient
    private transient Set<String> keywords;

//    @JoinColumn(name = "SELECTEDDATABASE_ID")
//    @ManyToOne
    @Setter(AccessLevel.NONE)
    @Getter
    @Transient
    private Database selectedDatabase;
    
    @Setter
    @Getter
    @Transient
    private ErrDiagram errDiagram;
    
    @Setter
    @Getter
    @Transient
    private boolean needReinit = false;
    
    private transient Boolean selected = false;
    
    @Setter
    @Getter
    @javax.persistence.Column(name = "LAST_USED_DATABASES")
    private String lastUsedDatabases;
    
    @Transient
    public static final List<String> BASE_DBS = Arrays.asList(new String[] {"mysql", "information_schema", "performance_schema", "sys"});
    
    @Transient
    private transient int pgSchemaId;
    
    public ConnectionParams() {
        
    }
    public ConnectionParams(String name, TechnologyType type,
              String address, String username, String password, int port) {
        this.name = name;
        this.type = type;
        this.address = address;
        this.username = username;
        this.password = password;
        this.port = port;
    }
    
    public String cacheKey() {
        return address.replace(".", "_") + "_" + port;
    }

    public Database selectDatabase(String dbName) {
        if (databases != null) {
            for (Database db : databases) {
                if (db.getName().trim().equalsIgnoreCase(dbName.trim())) {
                    selectedDatabase = db;

                    return selectedDatabase;
                }
            }
        }
        return null;
    }
    
    public boolean isRemote() {
        return address != null && !"localhost".equalsIgnoreCase(address) && !"127.0.0.1".equalsIgnoreCase(address);
    }

    public boolean hasOSDetails() {
        return osHost != null && !osHost.isEmpty() && osUser != null && !osUser.isEmpty() && osPassword != null && !osPassword.isEmpty();
    }
    
    public synchronized String[] getListOfLastUsedDatabases() {
        
        if (lastUsedDatabases != null && !lastUsedDatabases.isEmpty()) {
            return lastUsedDatabases.split("\\|");
        } 
        
        return null;
    }
    
    public synchronized void addUsedDatabase(String database) {
        
        if (!BASE_DBS.contains(database)) {
            
            String[] usedDatabases = getListOfLastUsedDatabases();
            if (usedDatabases != null) {
                
                List<String> list = new ArrayList<>(Arrays.asList(usedDatabases));
                list.remove(database);
                list.add(database);
                
                int size = list.size();
                if (size > 5) {
                    list = list.subList(size - 5, size);
                }
                
                lastUsedDatabases = String.join("|", list);
            } else {
                lastUsedDatabases = database;
            }
        }
    }
    
    public void setDatabases(List<Database> dbs) {
        this.databases = dbs;
    }

    @Override
    public String toString() {
        return username + "@" + address;
    }

    public Set<String> getKeywords() {
        if (keywords == null) {
            keywords = new HashSet<>();
        }
        for (Database d : databases) {
            keywords.add(d.getName());
            for (DBTable table : d.getTables()) {
                keywords.add(table.getName());
                for (Column column : table.getColumns()) {
                    keywords.add(column.getName());
                }
            }
        }
        return keywords;
    }

    // TODO
    // This method always remove all dbs expect first
    // addDatabe and only first has id - others null
    // ???
//    public void addDatabase(Database d) {
//        if (databases == null) {
//            databases = new ArrayList<>();
//        }
//        Iterator<Database> it = databases.iterator();
//        while (it.hasNext()) {
//            Database db = it.next();
//            if (db.getName().equalsIgnoreCase(d.getName())) {
//                d.setId(db.getId());
//                db.setParams(null);
//                it.remove();
//            } else if (db.getParams() == null) {
//                it.remove();
//            }
//        }
//
//        d.setParams(this);
//        databases.add(d);
//    }
    
    public void addDatabase(Database d) {
        if (databases == null) {
            databases = new ArrayList<>();
        }
        
        d.setParams(this);
        databases.add(d);
    }
    
    public void clearDatabases() {
        if (databases != null) {
            databases.clear();
        }
    }

    public Map<String, String> getModuleParams(String moduleName) {
        return moduleParams.get(moduleName);
    }

    public ConnectionParams copyOnlyParams() {
        ConnectionParams p = new ConnectionParams();
        p.name = name;
        p.type = type;
        p.address = address;
        p.username = username;
        p.password = password;
        p.port = port;
        p.backgroundColor = backgroundColor;
        p.foregroundColor = foregroundColor;
        p.database = database;
        p.limitResult = limitResult;
        p.sessionIdleTimeout = sessionIdleTimeout;
        p.useCompressedProtocol = useCompressedProtocol;
        p.selectedModuleClassName = selectedModuleClassName;
        p.connectionType = connectionType;
        p.osName = osName;
        p.osHost = osHost;
        p.osPort = osPort;
        p.osUser = osUser;
        p.osPassword = osPassword;
        p.osPrivateKey = osPrivateKey;
        p.osUsePassword = osUsePassword;
        p.osMetricDiskSize = osMetricDiskSize;
        p.osMetricRamSize = osMetricRamSize;
        p.useSshTunneling = useSshTunneling;
        p.sshHost = sshHost;
        p.sshPort = sshPort;
        p.sshUser = sshUser;
        p.sshPassword = sshPassword;
        p.useSshPrivateKey = useSshPrivateKey;
        p.sshPrivateKey = sshPrivateKey;
        p.enabledCustomSqlMode = enabledCustomSqlMode;
        p.initCommands = initCommands;        
        p.keywords = keywords;        
        p.moduleParams = moduleParams;
        p.lastUsedDatabases = lastUsedDatabases;
        
        p.makeBackwardEngine = makeBackwardEngine;
        p.needReinit = needReinit;
        p.pgSchemaId = pgSchemaId;
        return p;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        
        if (obj instanceof ConnectionParams) {
            return getId() == null && ((ConnectionParams)obj).getId() == null ? Objects.equals(getName(), ((ConnectionParams)obj).getName()) : Objects.equals(getId(), ((ConnectionParams)obj).getId());
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.name);
//        hash = 93 * hash + Objects.hashCode(this.getId());
        return hash;
    }
    
    public boolean isLocalToMachine(){
        return getAddress().equalsIgnoreCase("localhost")
                            || getAddress().startsWith("127");
    }
    
}
