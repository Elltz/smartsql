/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Table;
import javax.persistence.Column;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "QueryData")
public class QueryData extends BaseModel{
    
}
