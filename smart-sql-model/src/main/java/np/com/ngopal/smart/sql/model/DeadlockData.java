
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQueries({
    @NamedQuery(name = "deadlockdata.findByParamsKey", query = "SELECT d FROM DeadlockData d where d.paramsKey = :paramsKey"),
    @NamedQuery(name = "deadlockdata.delete", query = "DELETE FROM DeadlockData d where d.paramsKey = :paramsKey AND d.deadlockTime = :deadlockTime AND d.transaction1 = :transaction1 AND d.transaction2 = :transaction2"),
})
@Table(name = "DEADLOCK_DATA")
public class DeadlockData extends BaseModel {
    
    @javax.persistence.Column(name = "PARAMS_KEY")
    private String paramsKey;
    @javax.persistence.Column(name = "DETAILS")
    private String details;
    @javax.persistence.Column(name = "STATUS")
    private String status;
    @javax.persistence.Column(name = "DEADLOCK_TIME")
    private String deadlockTime;
    @javax.persistence.Column(name = "TRANSACTION1")
    private String transaction1;
    @javax.persistence.Column(name = "TRANSACTION2")
    private String transaction2;
    @javax.persistence.Column(name = "QUERY_TUNING_TRANSACTION1")
    private String queryTuningTransaction1;
    @javax.persistence.Column(name = "RECOMMENDATIONS_TRANSACTION1")
    private String recommendationsTransaction1;    
    @javax.persistence.Column(name = "QUERY_TUNING_TRANSACTION2")
    private String queryTuningTransaction2;
    @javax.persistence.Column(name = "RECOMMENDATIONS_TRANSACTION2")
    private String recommendationsTransaction2;
    @javax.persistence.Column(name = "DATABASE")
    private String database;
    @javax.persistence.Column(name = "ROLLBACK")
    private String rollback;   
    
}