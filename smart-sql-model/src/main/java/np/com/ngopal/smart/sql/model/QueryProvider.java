
package np.com.ngopal.smart.sql.model;

import com.google.gson.Gson;
import java.io.InputStreamReader;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public final class QueryProvider {
    
    public static final String DB__POSTGRESQL = "postgresql";
    public static final String DB__MYSQL = "mysql";
    public static final String DB__H2 = "h2";
    public static final String DB__PUBLIC = "public";
            
    public static final String QUERY__ENABLE_PROFILING                  = "enableProfiling";
    public static final String QUERY__DISABLE_PROFILING                 = "disableProfiling";
    public static final String QUERY__SHOW_PROFILES                     = "showProfiles";
    public static final String QUERY__SHOW_ENGINE_INNODB_STATUS         = "showEngineInnodbStatus";
    public static final String QUERY__SHOW_OPEN_TABLES                  = "showOpenTables";
    public static final String QUERY__SHOW_OPEN_TABLES_0                = "showOpenTables0";
    public static final String QUERY__SHOW_OPEN_TABLES_1                = "showOpenTables1";
    
    public static final String QUERY__VALIABLE_SLOW_QUERY_LOG           = "variableSlowQueryLog";
    public static final String QUERY__VALIABLE_SLOW_QUERY_LOG_FILE      = "variableSlowQueryLogFile";
    
    public static final String QUERY__SHOW_LOG_ERROR_VARIABLE           = "showLogErrorVariable";
    public static final String QUERY__SHOW_DATADIR_VARIABLE             = "showDatadirVariable";
    
    public static final String QUERY__SELECT_SLOW_QUERY_LOG             = "selectSlowQueryLog";
            
    public static final String QUERY__CLEAN_FOREIGN_KEY_CHECKS          = "cleanForeignKeyChecks";
    
    public static final String QUERY__SHOW_ENGINES                      = "showEngines";
    
    public static final String QUERY__EXPLAIN                           = "explain";
    public static final String QUERY__EXPLAIN_EXTENDED                  = "explainExtended";
    public static final String QUERY__SHOW_WARNINGS                     = "showWarnings";
    public static final String QUERY__SHOW_STATUS                       = "showStatus";
    public static final String QUERY__SHOW_VARIABLES                    = "showVariables";
    
    public static final String QUERY__SHOW_CHARSET                      = "showCharset";
    public static final String QUERY__SHOW_COLLATION_FOR_CHARSET        = "showCollationForCharset";
    public static final String QUERY__DEFAULT_CHARSET_AND_COLLATION     = "defaultCharsetAndCollation";
    
    public static final String QUERY__SHOW_DATABASES                    = "showDatabases";
    public static final String QUERY__SHOW_CREATE_DATABASE              = "showCreateDatabase";
    public static final String QUERY__CHANGE_DATABASE                   = "changeDatabase";
    public static final String QUERY__DROP_DATABASE                     = "dropDatabase";
    public static final String QUERY__CREATE_DATABASE                   = "createDatabase";
    public static final String QUERY__ALTER_DATABASE                    = "alterDatabase";
    
    public static final String QUERY__SHOW_FULL_TABLES                  = "showFullTables";
    public static final String QUERY__SHOW_TABLE_STATUS                 = "showTableStatus";
    public static final String QUERY__SHOW_ALL_TABLES_STATUS            = "showAllTablesStatus";
    public static final String QUERY__SHOW_FULL_FIELDS                  = "showFullFields";
    public static final String QUERY__SHOW_CREATE_TABLE                 = "showCreateTable";
    public static final String QUERY__RENAME_TABLE                      = "renameTable";
    public static final String QUERY__CREATE_TABLE                      = "createTable";
    public static final String QUERY__DROP_TABLE                        = "dropTable";
    public static final String QUERY__TRUNCATE_TABLE                    = "truncateTable";
    public static final String QUERY__CHANGE_TABLE_TYPE                 = "changeTableType";
    
    public static final String QUERY__TABLE_ROW_COUNT                   = "tableRowCount";
//    public static final String QUERY__TABLE_CARDINALITY                 = "tableCardinality";
//    public static final String QUERY__TABLE_CARDINALITY_LIMIT           = "tableCardinalityLimit";
    
    public static final String QUERY__INSERT_INTO_TABLE                 = "insertIntoTable";
    public static final String QUERY__SELECT_FROM_TABLE                 = "selectFromTable";
    public static final String QUERY__UPDATE_IN_TABLE                   = "updateInTable";
    public static final String QUERY__DELETE_FROM_TABLE                 = "deleteFromTable";
    
    public static final String QUERY__SHOW_KEYS_FOR_TABLE               = "showKeysForTable";
    public static final String QUERY__SHOW_INDEX_FOR_TABLE              = "showIndexForTable";
    public static final String QUERY__SHOW_RECORDS_COUNT_FOR_TABLE      = "showRecordsCountForTable";
    
    public static final String QUERY__GET_FOREIGN_KEYS_FOR_TABLE        = "getForeignKeysForTable";
    
    public static final String QUERY__DESCRIBE_COLUMN                   = "describeColumn";
    
    public static final String QUERY__KILL_QUERY                        = "killQuery";
    
    public static final String QUERY__SELECT_PARTITION                  = "selectPartitions";
    
    public static final String QUERY__SHOW_ALL_VIEWS_STATUS             = "showAllViewsStatus";
    public static final String QUERY__SHOW_VIEWS                        = "showViews";
    public static final String QUERY__SHOW_VIEW_DEFINITION              = "showViewDefinition";
    public static final String QUERY__CREATE_VIEW                       = "createView";
    public static final String QUERY__ALTER_VIEW_0                      = "alterView0";
    public static final String QUERY__ALTER_VIEW_1                      = "alterView1";
    public static final String QUERY__SELECT_VIEW_DEFINITION            = "selectViewDefinition";
    public static final String QUERY__DROP_VIEW                         = "dropView";
    public static final String QUERY__SHOW_CRAETE_VIEW                  = "showCreateView";
    public static final String QUERY__CRAETE_VIEW_TEMPLATE              = "createViewTemplate";
    public static final String QUERY__SHOW_VIEW_FIELDS                  = "showViewFields";
    
    public static final String QUERY__SHOW_TRIGGERS                     = "showTriggers";
    public static final String QUERY__SHOW_TRIGGERS_FOR_TABLE           = "showTriggersForTable";
    public static final String QUERY__SHOW_TRIGGERS_BY_NAME             = "showTriggerByName";
    public static final String QUERY__DROP_TRIGGER                      = "dropTrigger";
    public static final String QUERY__CREATE_TRIGGER                    = "createTrigger";
    public static final String QUERY__ALTER_TRIGGER                     = "alterTrigger";
    public static final String QUERY__CREATE_TRIGGER_TEMPLATE           = "createTriggerTemplate";
    public static final String QUERY__CREATE_AUDIT_TRIGGER_TEMPLATE     = "createAuditTriggerTemplate";
    
    public static final String QUERY__SHOW_FULL_FUNCTIONS               = "showFullFunctions";
    public static final String QUERY__SHOW_FUNCTIONS                    = "showFunctions";
    public static final String QUERY__DROP_FUNCTION                     = "dropFunction";
    public static final String QUERY__CREATE_FUNCTIONS                  = "createFunction";
    public static final String QUERY__ALTER_FUNCTIONS                   = "alterFunction";
    public static final String QUERY__CREATE_FUNCTIONS_TEMPLATE         = "createFunctionTemplate";
    
    public static final String QUERY__SHOW_FULL_EVENTS                  = "showFullEvents";
    public static final String QUERY__SHOW_EVENTS                       = "showEvents";
    public static final String QUERY__ALTER_EVENT                       = "alterEvent";
    public static final String QUERY__DROP_EVENT                        = "dropEvent";
    public static final String QUERY__CREATE_EVENT                      = "createEvent";
    public static final String QUERY__RENAME_EVENT                      = "renameEvent";
    public static final String QUERY__CREATE_EVENT_TEMPLATE             = "createEventTemplate";
    
    public static final String QUERY__SHOW_FULL_STORED_PROCEDURES       = "showFullStoredProcedures";
    public static final String QUERY__SHOW_STORED_PROCEDURES            = "showStoredProcedures";
    public static final String QUERY__DROP_STORED_PROCEDURE             = "dropStoredProcedure";
    public static final String QUERY__CREATE_STORED_PROCEDURE           = "createStoredProcedure";
    public static final String QUERY__ALTER_STORED_PROCEDURE            = "alterStoredProcedure";
    public static final String QUERY__CREATE_STORED_PROCEDURE_TEMPLATE  = "createStoredProcedureTemplate";
    
    public static final String QUERY__CREATE_STORED_PROCEDURE_WITH_CURSOR_TEMPLATE  = "createStoredProcedureWithCursorTemplate";
    
    public static final String QUERY__SET_SESSION_TRANSACTION           = "setSessionTransaction";
    public static final String QUERY__START_TRANSACTION                 = "startTransaction";
    
    public static final String QUERY__RESET_MASTER                      = "resetMaster";
    public static final String QUERY__RESET_SLAVE                       = "resetSlave";
    public static final String QUERY__FLUSH_LOGS                        = "flushLogs";
    public static final String QUERY__UNLOCK_TABLES                     = "unlockTables";
    
    public static final String QUERY__SHOW_COLUMNS                      = "showColumns";
    public static final String QUERY__SELECT_COUNT                      = "selectCount";
    
    public static final String QUERY__DEBUG_RUNNING_QUERIES             = "debugRunningQueries";
    public static final String QUERY__DEBUG_RUNNING_QUERIES_WITH_TIME   = "debugRunningQueriesWithTime";
    public static final String QUERY__DEBUG_RUNNING_QUERIES_AVOID_SLEEP = "debugRunningQueriesAvoidSleep";
    
    public static final String QUERY__DEBUG_DEADLOCK_MONITOR            = "deadlockMonitorQueries";
    
    public static final String QUERY__LOCK_MONITOR                      = "lockMonitor";
    public static final String QUERY__LOCK_MONITOR_WITH_TIME            = "lockMonitorWithTime";
    public static final String QUERY__LOCK_MONITOR_TX_LOCK              = "lockMonitorTXLock";
    
    public static final String QUERY__LOCK_MONITOR_8                    = "lockMonitor_8";
    public static final String QUERY__LOCK_MONITOR_WITH_TIME_8          = "lockMonitorWithTime_8";
    public static final String QUERY__LOCK_MONITOR_TX_LOCK_8            = "lockMonitorTXLock_8";
    
    
    public static final String QUERY__SHOW_GLOBAL_VARIABLES             = "showGlobalVariables";
    public static final String QUERY__SHOW_GLOBAL_STATUS                = "showGlobalStatus";
    public static final String QUERY__SHOW_FULL_PROCESS_LIST            = "showFullProcessList";
        
    public static final String QUERY__SELECT_ALL                        = "selectAll";
    
    public static final String QUERY__INSERT_GLOBAL_VARIABLES           = "insertGlobalVariables";
    public static final String QUERY__DELETE_GV_BY_CONNECTION_ID        = "deleteGVByConnectionId";
    
    public static final String QUERY__SELECT_GV_TABLES                  = "selectGVTables";
    public static final String QUERY__FIND_GV_TABLES_BY_TABLENAME       = "findGVTablesByTableName";
    public static final String QUERY__CREATE_GV_TABLE                   = "createGVTable";
    public static final String QUERY__INSERT_GV_TABLES                  = "insertGVTables";
    public static final String QUERY__INSERT_GV                         = "insertGV";
    public static final String QUERY__REMOVE_GV                         = "removeGV";
    
    public static final String QUERY__SELECT_GS_START_TABLES            = "selectGSStartTables";
    public static final String QUERY__FIND_GS_START_TABLES_BY_TABLENAME = "findGSStartTablesByTableName";
    public static final String QUERY__CREATE_GS_START_TABLE             = "createGSStartTable";
    public static final String QUERY__INSERT_GS_START_TABLES            = "insertGSStartTables";
    public static final String QUERY__UPDATE_GS_START_TABLES            = "updateGSStartTables";
    public static final String QUERY__UPDATE_START_TIME_GS_START_TABLES = "updateStartTimeGSStartTables";
    public static final String QUERY__INSERT_GS_START                   = "insertGSStart";
    public static final String QUERY__REMOVE_GS_START                   = "removeGSStart";
    
    public static final String QUERY__SELECT_SQ_TABLES                  = "selectSQTables";
    public static final String QUERY__FIND_SQ_TABLES_BY_TABLE_NAME      = "findSQTablesByTableName";
    public static final String QUERY__INSERT_SQ_TABLES                  = "insertSQTables";
    public static final String QUERY__CREATE_SQ_TABLES                  = "createSQTables";
    public static final String QUERY__CREATE_SQ_TABLE                   = "createSQTable";
    public static final String QUERY__INSERT_SQ_TABLE                   = "insertSQTable";
    public static final String QUERY__CLEAR_SQ_TABLE                    = "clearSQTable";
    public static final String QUERY__REMOVE_SLOW_TABLE_NAME            = "removeSlowTableName";
        
    public static final String QUERY__SELECT_GS_TABLES                  = "selectGSTables";
    public static final String QUERY__REMOVE_FROM_GS_TABLES             = "removeFromGSTables";
    public static final String QUERY__FIND_GS_TABLES_BY_TABLENAME       = "findGSTablesByTableName";
    public static final String QUERY__CREATE_GS_TABLE                   = "createGSTable";
    public static final String QUERY__INSERT_GS_TABLES                  = "insertGSTables";
    public static final String QUERY__INSERT_GS                         = "insertGS";
    public static final String QUERY__SELECT_GS                         = "selectGS";
        
    public static final String QUERY__SELECT_SS_TABLES                  = "selectSSTables";
    public static final String QUERY__REMOVE_FROM_SS_TABLES             = "removeFromSSTables";
    public static final String QUERY__FIND_SS_TABLES_BY_TABLENAME       = "findSSTablesByTableName";
    public static final String QUERY__CREATE_SS_TABLE                   = "createSSTable";
    public static final String QUERY__INSERT_SS_TABLES                  = "insertSSTables";
    public static final String QUERY__INSERT_SS                         = "insertSS";
    public static final String QUERY__SELECT_SS                         = "selectSS";
    
    public static final String QUERY__SELECT_EIS_TABLES                 = "selectEISTables";
    public static final String QUERY__REMOVE_FROM_EIS_TABLES            = "removeFromEISTables";
    public static final String QUERY__FIND_EIS_TABLES_BY_TABLENAME      = "findEISTablesByTableName";
    public static final String QUERY__CREATE_EIS_TABLE                  = "createEISTable";
    public static final String QUERY__INSERT_EIS_TABLES                 = "insertEISTables";
    public static final String QUERY__INSERT_EIS                        = "insertEIS";
    public static final String QUERY__SELECT_EIS                        = "selectEIS";
    
    public static final String QUERY__LOAD_SQ_QUERIES                   = "loadSQQueries";
    public static final String QUERY__LOAD_SQ_TOP_QUERIES               = "loadSQTopQueries";
    public static final String QUERY__LOAD_SQ_TOP_TABLES                = "loadSQTopTables";
    public static final String QUERY__LOAD_SQ_TOP_USERS                 = "loadSQTopUsers";
    public static final String QUERY__LOAD_SQ_TOP_HOSTS                 = "loadSQTopHosts";
    public static final String QUERY__LOAD_SQ_TOP_SCHEMAS               = "loadSQTopSchemas";
            
    public static final String QUERY__LOAD_PQ_STATE                     = "loadPQState";
    public static final String QUERY__LOAD_PQUERY_IDS                   = "loadPQueryIds";
    public static final String QUERY__INSERT_PQUERY                     = "insertPQuery";
    public static final String QUERY__LOAD_PQUERY_TEMPLATE_ID           = "loadPQueryTemplateId";
    public static final String QUERY__LOAD_PQUERY_QUERY_BY_ID           = "loadPQueryQueryById";
    public static final String QUERY__INSERT_PQUERY_TEMPLATE            = "insertPQueryTemplate";
    public static final String QUERY__LOAD_PQ_DB                        = "loadPQDB";
    public static final String QUERY__INSERT_PQ_DB                      = "insertPQDB";
    public static final String QUERY__LOAD_PQ_USER_HOST                 = "loadPQUserHost";
    public static final String QUERY__INSERT_PQ_USER_HOST               = "insertPQUserHost";
    public static final String QUERY__LOAD_PQ_TABLE                     = "loadPQTable";
    public static final String QUERY__INSERT_PQ_TABLE                   = "insertPQTable";
    public static final String QUERY__LOAD_PQUERY_STATS                 = "loadPQueryStats";
    public static final String QUERY__UPDATE_PQUERY_STATS               = "updatePQueryStats";
    public static final String QUERY__INSERT_PQUERY_STATS               = "insertPQueryStats";
    public static final String QUERY__INSERT_PQ_STATE                   = "insertPQState";
    public static final String QUERY__LOAD_PQ_STATE_STAT                = "loadPQStateStat";
    public static final String QUERY__UPDATE_PQ_STATE_STAT              = "updatePQStateStat";
    public static final String QUERY__INSERT_PQ_STATE_STAT              = "insertPQStateStat";
    public static final String QUERY__LOAD_TEMPLATE_TABLE_RELATION      = "loadTemplateTableRelation";
    public static final String QUERY__INSERT_TEMPLATE_TABLE_RELATION    = "insertTemplateTableRelation";
    public static final String QUERY__MYSQL_VERSION                     = "mysqlVersion";
    
    
    public static final String QUERY__SELECT_SD_TABLES                  = "selectSDTables";
    public static final String QUERY__FIND_SD_TABLES_BY_TABLENAME       = "findSDTablesByTableName";
    public static final String QUERY__REMOVE_FROM_SD_TABLES             = "removeFromSDTables";        
    public static final String QUERY__INSERT_SD_TABLES                  = "insertSDTables";
    public static final String QUERY__CREATE_SD_TABLE                   = "createSDTable";
    public static final String QUERY__INSERT_SD                         = "insertSDTable";
    public static final String QUERY__INSERT_DIGEST_TEXT                = "insertDigestText";
    public static final String QUERY__LOAD_DIGEST_TEXT                  = "loadDigestText";
    public static final String QUERY__SELECT_DIGEST_TEXT_ID             = "selectDigestTextId";    
    public static final String QUERY__INSERT_DIGEST_QUERY               = "insertDigestQuery";
    public static final String QUERY__SELECT_DIGEST_QUERY               = "selectDigestQuery";
    public static final String QUERY__SELECT_STATEMENT_HISTORY          = "selectStatementsHistory";
    
    public static final String QUERY__SELECT_STATEMENTS_BY_DIGEST_0     = "selectStatementsByDigest0";
    public static final String QUERY__SELECT_STATEMENTS_BY_DIGEST_1     = "selectStatementsByDigest1";
    
    
    public static final String QUERY__USERS_ACCOUNTS                    = "serverUsers";
    public static final String QUERY__GLOBAL_USER_PRIV                  = "userGlobalPriv";
    public static final String QUERY__DELETE_USER                       = "deleteUser";
    public static final String QUERY__SHOW_GRANTS                       = "showGrants";
    
    
    public static final String QUERY__LOAD_TOP_QUERIES                  = "loadTopQueries";
    public static final String QUERY__LOAD_TOP_TABLES                   = "loadTopTables";
    public static final String QUERY__LOAD_TOP_SCHEMAS                  = "loadTopSchemas";
    public static final String QUERY__LOAD_TOP_USERS                    = "loadTopUsers";
    public static final String QUERY__LOAD_TOP_HOSTS                    = "loadTopHosts";
    public static final String QUERY__LOAD_SLOW_QUERIES                 = "loadSlowQueries";
    public static final String QUERY__LOAD_LOCK_QUERIES                 = "loadLockQueries";
    public static final String QUERY__LOAD_ANALYZE_QUERIES              = "loadAnalyzeQueries";
    public static final String QUERY__LOAD_PF_ANALYZE_QUERIES           = "loadPFAnalyzeQueries";
    
    
    
    public static final String QUERY__DELETE_PQUERY_TEMPLATE_PQTABLE    = "deletePQueryTemplatePQTable";
    public static final String QUERY__DELETE_PQSTATE_STAT               = "deletePQStateStat";
    public static final String QUERY__DELETE_PQUERY_STATS               = "deletePQueryStats";
    public static final String QUERY__DELETE_PQSTATE                    = "deletePQState";
    public static final String QUERY__DELETE_PQTABLE                    = "deletePQTable";
    public static final String QUERY__DELETE_PQDB                       = "deletePQDB";
    public static final String QUERY__DELETE_PQUERY                     = "deletePQuery";
    public static final String QUERY__DELETE_PQUERY_TEMPLATE            = "deletePQueryTemplate";
    public static final String QUERY__DELETE_PQUSER_HOST                = "deletePQUserHost";
    
    
    public static final String QUERY__DROP_INDEX                        = "dropIndex";
    public static final String QUERY__DROP_COLUMN                       = "dropColumn";
    
    public static final String QUERY__SELECT_DB_INFO                    = "selectDBInfo_new";//"selectDBInfo";
    public static final String QUERY__SELECT_DB_INFO_5_0                = "selectDBInfo_new_5.0";//"selectDBInfo_5.0";
    public static final String QUERY__SELECT_FOREIGN_KEYS               = "selectForeignKeys";
    public static final String QUERY__SELECT_PRIMARY_KEYS               = "selectPrimaryKeys";
    public static final String QUERY__SELECT_DATABASE_INFO              = "selectDatabaseInfo_new";//"selectDatabaseInfo";
    public static final String QUERY__SELECT_DATABASE_INFO_5_0          = "selectDatabaseInfo_new_5.0"; //"selectDatabaseInfo_5.0";
    
    public static final String QUERY__FIND_REDUNDANT_INDEXES            = "findRedundantIndexes";
    public static final String QUERY__FIND_REDUNDANT_INDEXES_BY_TABLE   = "findRedundantIndexesByTable";
    
    
    public static final String QUERY__SELECT_DATABASES_SIZE             = "selectDatabasesSize";
    
    public static final String QUERY__REPORT_DATABASE_SIZE_0            = "reportDatabaseSize0";
    public static final String QUERY__REPORT_DATABASE_SIZE_1            = "reportDatabaseSize1";
    public static final String QUERY__REPORT_DATABASE_SIZE_2            = "reportDatabaseSize2";
    public static final String QUERY__REPORT_DATABASE_SIZE_3            = "reportDatabaseSize3";
    public static final String QUERY__REPORT_DATABASE_SIZE_4            = "reportDatabaseSize4";
    public static final String QUERY__REPORT_FRAGMENTATION_0            = "reportFragmentation0";
    public static final String QUERY__REPORT_FRAGMENTATION_1            = "reportFragmentation1";
    public static final String QUERY__REPORT_RAM_CONFIGURATION_0        = "reportRamConfiguration0";
    public static final String QUERY__REPORT_RAM_CONFIGURATION_1        = "reportRamConfiguration1";
    public static final String QUERY__REPORT_RAM_USAGE                  = "reportRamUsage";
    public static final String QUERY__REPORT_RAM_ALLOCATION_BY_CLIENT   = "reportRamAllocationByClient";
    public static final String QUERY__REPORT_RAM_ALLOCATION_BY_THREAD   = "reportRamAllocationByThread";
    public static final String QUERY__REPORT_RAM_ALLOCATION_BY_USER     = "reportRamAllocationByUser";
    public static final String QUERY__REPORT_RAM_ALLOCATION_DETAILS     = "reportRamAllocationDetails";
    public static final String QUERY__REPORT_RAM_TOTAL_ALLOCATION       = "reportRamTotalAllocation";
    public static final String QUERY__REPORT_RAM_SCHEMA_WISE_BUF_USAGE  = "reportRamSchemaWiseBufferUsage";
    public static final String QUERY__REPORT_RAM_TABLE_WISE_BUF_USAGE   = "reportRamTableWiseBufferUsage";
    public static final String QUERY__REPORT_INDEX_DUBLICATE            = "reportIndexDublicate";
    public static final String QUERY__REPORT_INDEX_MISING_FK            = "reportIndexMisingFk";
    public static final String QUERY__REPORT_INDEX_SUMMARY              = "reportIndexSummary";
    public static final String QUERY__REPORT_INDEX_DETAIL               = "reportIndexDetail";
    public static final String QUERY__REPORT_SQL_COMMANDS_HISTORY_0     = "reportSqlCommandsHistory0";
    public static final String QUERY__REPORT_SQL_COMMANDS_HISTORY_1     = "reportSqlCommandsHistory1";
    public static final String QUERY__REPORT_SQL_COMMANDS_HISTORY_2     = "reportSqlCommandsHistory2";
    public static final String QUERY__REPORT_MISCELLANIOUCE_EVENTS      = "reportMiscellaneouseEvents";
    public static final String QUERY__REPORT_FILE_IO_TABLE_WISE_0       = "reportFileIoTableWise0";
    public static final String QUERY__REPORT_FILE_IO_TABLE_WISE_1       = "reportFileIoTableWise1";
    public static final String QUERY__REPORT_FILE_IO_TABLE_LATEN_WISE_0 = "reportFileIoTableLatencyWise0";
    public static final String QUERY__REPORT_FILE_IO_TABLE_LATEN_WISE_1 = "reportFileIoTableLatencyWise1";
    public static final String QUERY__REPORT_FILE_IO_THREAD_WISE        = "reportFileIoThreadWise";
    public static final String QUERY__REPORT_FILE_IO_USER_WISE          = "reportFileIoUserWise";
    public static final String QUERY__REPORT_FILE_IO_EVENT_BYTE_WISE    = "reportFileIoEventBytesWise";
    public static final String QUERY__REPORT_FILE_IO_EVENT_WISE         = "reportFileIoEventWise";
    
    public static final String QUERY__APPLICATION_WISE_HOST             = "applicationHostWise";
    public static final String QUERY__DB_USER_HOST_WISE                 = "dbUserHostWise";
    public static final String QUERY__DB_USER_DBS_HOST_WISE             = "dbUserDbsHostWise";
    public static final String QUERY__DB_USER_HOST_STATE                = "dbUserHostState";
    public static final String QUERY__USERS_WISE                        = "dbUsersWise";
    public static final String QUERY__DATABASE_WISE                     = "dbDatabaseWise";
    public static final String QUERY__QUERY_STATE_WISE                  = "queryStateWise";
    public static final String QUERY__USERS_DBS_QUERY_STATE_WISE        = "usersDbsQueryStateWise";
    public static final String QUERY__USERS_QUERY_STATE_WISE            = "usersQueryStateWise";
    
    
    
    
    public static final String QUERY__IS_SLAVE_RUNNING                  = "isSlaveRunning";
    public static final String QUERY__SHOW_SLAVE_STATUS                 = "showSlaveStatus";
    
    public static final String QUERY__PUBLIC__LOAD_DOMAINS              = "loadDomains";
    public static final String QUERY__PUBLIC__LOAD_SUBDOMAINS           = "loadSubdomains";
    public static final String QUERY__PUBLIC__LOAD_ERR_DIAGRAMS         = "loadErrDiagrams";
    public static final String QUERY__PUBLIC__LOAD_ALL_ERR_DIAGRAMS     = "loadAllErrDiagrams";
    public static final String QUERY__PUBLIC__GET_ID_BY_DOMAIN          = "getIdByDomain";
    public static final String QUERY__PUBLIC__GET_ID_BY_SUBDOMAIN       = "getIdBySubdomain";
    public static final String QUERY__PUBLIC__INSERT_DOMAIN             = "insertDomain";
    public static final String QUERY__PUBLIC__INSERT_SUBDOMAIN          = "insertSubdomain";
    public static final String QUERY__PUBLIC__INSERT_ERR_DIAGRAM        = "insertErrDiagram";
    public static final String QUERY__PUBLIC__UPDATE_ERR_DIAGRAM        = "updateErrDiagram";
    public static final String QUERY__PUBLIC__DELETE_ERR_DIAGRAM        = "deleteErrDiagram";
    public static final String QUERY__PUBLIC__SEARCH_ERR_DIAGRAM        = "searchErrDiagram";
    
    public static final String QUERY__PUBLIC__SELECT_USER_SYSTEM_ID     = "selectUserSystemId";
    public static final String QUERY__PUBLIC__SELECT_USER_ID            = "selectUserId";
    public static final String QUERY__PUBLIC__INSERT_USER_SYSTEM        = "insertUserSystem";
    
    public static final String QUERY__PUBLIC__INSERT_PUSAGE             = "insertPUsage";
    public static final String QUERY__PUBLIC__SELECT_FEATURE_ID         = "selectFeatureId";
    public static final String QUERY__PUBLIC__INSERT_FEATURE            = "insertFeature";
    
    /**
     * POSTGRESQL FLAGS
     */
    
    public static final String QUERY__POSTGRESQL_VERSION                 = "postgresqlVersion";
    public static final String QUERY__POSTGRESQL_SCHEMA_KIT                 = "schemaKit";
    
    private final Map<String, Map<String, String>> queries;
    
    public String getQuery(String db, String query) {
        String queryResult = queries.get(db).get(query);
        if (queryResult == null) {
            log.warn(db + "." + query + " not found");
        }
        return queryResult != null ? StringEscapeUtils.unescapeJava(queryResult) : null;
    }
    
            
            
            
    private static QueryProvider instance;
    public static QueryProvider getInstance() {
        if (instance == null) {
            instance = new QueryProvider();
        }
        
        return instance;
    }
            
    private QueryProvider() {
        Gson gson = new Gson();
        queries = gson.fromJson(new InputStreamReader(getClass().getResourceAsStream("db.queries.json")), Map.class);
    }
}
