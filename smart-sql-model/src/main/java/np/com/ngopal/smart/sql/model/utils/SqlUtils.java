package np.com.ngopal.smart.sql.model.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlUtils {

    private static final Pattern QUERY_ONE_LINE__PATTERN = Pattern.compile("\\s+");
    private static final Pattern QUERY_WITH_NEW_LINE__PATTERN = Pattern.compile("[^\\S\\r\\n]+");
    private static final Pattern SUB_SELECT__PATTERN =  Pattern.compile("(\\(\\s*\\bSELECT\\b.*\\s*\\))", Pattern.CASE_INSENSITIVE);
    private static final Pattern SUB_UNION__PATTERN =  Pattern.compile("(\\(\\s*1\\s+\\bUNION\\b\\s+(\\bALL\\b\\s+)?.*\\s*\\))", Pattern.CASE_INSENSITIVE);
    
    public static String makeQueryOneLine(String query) {
        return makeQueryOneLine(query, true);
    }
    
    public static String makeQueryOneLine(String query, boolean withEnter) {
        if (withEnter) {
            return query != null ? QUERY_ONE_LINE__PATTERN.matcher(query).replaceAll(" ").trim() : null;
        } else {
            return query != null ? QUERY_WITH_NEW_LINE__PATTERN.matcher(query).replaceAll(" ").trim() : null;
        }
    }
    
    private static final Pattern UNION_ALL_PATTER = Pattern.compile("union(\\s+all)?", Pattern.CASE_INSENSITIVE);
    
    private static String[] split(String query) {
        List<String> result = new ArrayList<>();
        // need check that union not inside "()"
        String str = SqlUtils.replaceSubQueries(query.trim(), " 1 ").toUpperCase();
        Matcher m = UNION_ALL_PATTER.matcher(str);
        if (m.find()) {
            m = UNION_ALL_PATTER.matcher(query);
            int from = 0;
            while (m.find()) {
                String s = query.substring(from, m.start());
                from = m.end();

                result.add(s.trim());
            }

            if (from < query.length()) {
                result.add(query.substring(from).trim());
            }

            return result.toArray(new String[0]);
        } else {
            return new String[]{query};
        }
    }
    
    public static final String[] splitByUnion(String query) {
        
        List<String> list = new ArrayList<>();
        findSubQueries(query, list);
        
        // no subqueries
        if (list.isEmpty()) {            
            return split(query);
            
        } else {
            
            String q = query;
            int i = 0;
            for (String s: list) {
                q = q.replace(s, "#" + i);
                i++;
            }
            
            String[] splitResult = split(query);
            if (splitResult.length > 1) {
                
                String[] result = new String[splitResult.length];
                int index = 0;
                for (String s: splitResult) {
                    
                    String newQuery = s;
                    for (int j = 0; j < i; j++) {
                        newQuery = newQuery.replace("#" + j, list.get(j));
                    }
                    
                    result[index] = newQuery;
                    index++;
                }
                
                return result;
            }
            
            return new String[]{query};
        }
    }
    
    public static final void findSubQueries(String sqlQuery, List<String> queries) {
        findSubQueries(sqlQuery, queries, true);
    }
    
    public static final void findSubQueries(String sqlQuery, List<String> queries, boolean withSubQueries) {
        String query = sqlQuery;
        
        final Matcher m = SUB_SELECT__PATTERN.matcher(sqlQuery);
        
        int startIndex = 0;
        while (m.find(startIndex))
        {
            int start = m.start();
            
            final String text = query.substring(start + 1);
            
            String result = text;
            
            int brachesDiff = 1;
            for (int i = start + 1; i < query.length(); i++)
            {
                switch (query.charAt(i))
                {
                    case '(':
                        brachesDiff++;
                        break;
                        
                    case ')':
                        brachesDiff--;
                        break;
                }
                
                // we find end of select query
                if (brachesDiff == 0)
                {
                    result = query.substring(start, i + 1);
                    startIndex = i + 1;
                    break;
                }
            }

            result = result.substring(1, result.length() - 1);
            queries.add(result);
            
            if (withSubQueries) {
                findSubQueries(result, queries);
            }
        }
    }
    
    public static final String replaceSubQueries(String sqlQuery, String replaceText) {
        String query = sqlQuery;
        
        Matcher m = SUB_SELECT__PATTERN.matcher(sqlQuery);
        if (m.find())
        {
            int start = m.start();
            
            int brachesDiff = 1;
            for (int i = start + 1; i < query.length(); i++)
            {
                switch (query.charAt(i))
                {
                    case '(':
                        brachesDiff++;
                        break;
                        
                    case ')':
                        brachesDiff--;
                        break;
                }
                
                // we find end of select query
                if (brachesDiff == 0)
                {
                    query = query.substring(0, start) + replaceText + query.substring(i + 1);
                    break;
                }
            }
            
            return replaceSubQueries(query, replaceText);
            
        } else {
            m = SUB_UNION__PATTERN.matcher(sqlQuery);
            if (m.find()) {
                query = query.substring(0, m.start()) + replaceText + query.substring(m.end() + 1);
            }
        }
        return query;
    }
    
    public static Map<String, Object> getQueryInsideParents(String query, int leftPosition, int rightPosition) {
        
        boolean inside = false;
        boolean end = false;
        int leftIndex = leftPosition;
        int count = 0;
        while (leftIndex > 0) {
            leftIndex--;
            
            String s = query.substring(leftIndex, leftIndex + 1);
            if ("(".equals(s)) {
                if (count == 0) {
                    leftIndex++;
                    inside = true;
                    break;
                } else {
                    count--;
                }
            } else if (")".equals(s)) {
                count++;
            } else if (";".equals(s)) {
                leftIndex++;
                end = true;
                break;
            } else if ("\n".equals(s) && query.substring(leftIndex).trim().toLowerCase().startsWith("select")) {
                leftIndex++;
                end = true;
                break;
            }
        }
        
        int rightIndex = rightPosition;
        count = 0;
        while (rightIndex < query.length()) {
            
            String s = query.substring(rightIndex, rightIndex + 1);
            if (")".equals(s)) {
                if (count == 0) {
                    inside = true;
                    break;
                } else {
                    count--;
                }
            } else if ("(".equals(s)) {
                count++;
            } else if (";".equals(s)) {
                end = true;
                break;
            }
            
            rightIndex++;
        }
        
        String newQuery = query.substring(leftIndex, rightIndex);
        
        if (end || newQuery.trim().isEmpty() || checkQuery(newQuery) || newQuery.trim().toLowerCase().startsWith("select") || leftIndex == 0 && rightIndex == query.length()) {
            Map<String, Object> m = new HashMap<>();
            m.put("from", leftIndex + newQuery.toLowerCase().indexOf("select"));
            m.put("to", rightIndex);
            m.put("query", newQuery);
            m.put("inside", inside);
            return m;
        } else {
            return getQueryInsideParents(query, leftIndex - 1, rightIndex < query.length() ? rightIndex + 1 : rightIndex);
        }
    }
    
    public static boolean checkQuery(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return checkQuerySelect(query) ||checkQueryUpdate(query) || checkQueryInsert(query) || checkQueryDelete(query) || checkQueryDrop(query) || checkQueryTruncate(query);
        }
        
        return false;
    }
    
    public static boolean checkQuerySelect(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return "select".startsWith(query);
        }
        
        return false;
    }
    
    public static boolean checkQueryUpdate(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return "update".startsWith(query);
        }
        
        return false;
    }
    
    public static boolean checkQueryInsert(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return "insert".startsWith(query);
        }
        
        return false;
    }
    
    public static boolean checkQueryDelete(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return "delete".startsWith(query);
        }
        
        return false;
    }
    
    public static boolean checkQueryDrop(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return "drop".startsWith(query);
        }
        
        return false;
    }
    
    public static boolean checkQueryTruncate(String query) {
        if (query != null) {
            query = query.toLowerCase().trim();
            return "truncate".startsWith(query);
        }
        
        return false;
    }
}