package np.com.ngopal.smart.sql.model.provider;

import java.util.List;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface StorageProvider {

    public static final String LAST_PQ_CLEAR_PATH = "2019-06-06";;
        
    public String getProviderType();
            
    public void beginTransaction();    
    public void commitTransaction();    
    public void rollbackTransaction();
    
    public void executeQuery(String query, Object...params);            
    public List getResultList(String query, Object...params);
    public Object getSingleResult(String query, Object...params);
    
}
