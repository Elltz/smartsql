package np.com.ngopal.smart.sql.model;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Getter
@Setter
@AllArgsConstructor
public class PgDBTable extends DBTable {
    
}
