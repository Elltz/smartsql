/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.text.Collator;

/**
 *
 * @author NGM
 */
public interface DatabaseChildren extends Comparable<DatabaseChildren> {

    public void setDatabase(Database database);

    public Database getDatabase();

    public void setName(String name);

    public String getName();

    @Override
    default int compareTo(DatabaseChildren children)
    {
        return Collator.getInstance().compare(getName(), children.getName());
    }
}
