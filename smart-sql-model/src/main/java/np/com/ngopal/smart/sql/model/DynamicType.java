/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.Collection;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public abstract class DynamicType<T extends NamedType> {

    public abstract Collection<T> getTypes();

    public T getTypeByName(String name) {
        for (T TYPES1 : getTypes()) {
            if (TYPES1.getName().equalsIgnoreCase(name)) {
                return TYPES1;
            }
        }
        return null;
    }
}
