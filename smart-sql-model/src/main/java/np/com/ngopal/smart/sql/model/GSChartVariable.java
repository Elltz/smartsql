
package np.com.ngopal.smart.sql.model;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.BooleanProperty;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "GS_CHART_VARIABLE")
public class GSChartVariable extends BaseModel {
    
    @ManyToOne
    @JoinColumn(name="CHART_ID")
    private GSChart chart; // foreign key relation with GS_CHAT table
    
    @Column(name = "GS_NAME")
    private String variableName; // variable name from GS_START table
    
    @Column(name = "DISPLAY_NAME")
    private String displayName;
    
    @Column(name = "DISPLAYCOLOR")
    private String displayColor = "Random"; // if this column have other then ramdom then chat line/area come with same color

    @Column(name = "TYPE")
    private Integer type = 1; // 'if the it is 1 then substract variable value from previour value mean vale=(199th value - 200th value). if it is 0 then need to save what you collect from DB

    @Column(name = "VARIABLE")
    private Integer variable = 0; // 0 - status, 1 variable

    
    @Transient
    private BooleanProperty typeProperty = new SimpleBooleanProperty(type == 1) {
        @Override
        protected void invalidated() {
            type = typeProperty.get() ? 1 : 0;
        }
        
    };
    
    public void setType(Integer type) {
        typeProperty.set(type == 1);
    }
}
