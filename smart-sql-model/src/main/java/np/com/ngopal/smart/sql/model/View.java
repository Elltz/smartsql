package np.com.ngopal.smart.sql.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class View implements DatabaseChildren, TableProvider, DBElement {

    private String name;
    private String alias;

    private Database database;

    private List<Column> columns;
    
    private String createScript;
    
    @Override
    public String toString() {
        return name;
    }

}
