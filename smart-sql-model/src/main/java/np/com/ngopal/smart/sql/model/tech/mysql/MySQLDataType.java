/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.tech.mysql;

import java.text.Collator;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
public abstract class MySQLDataType implements DataType {

    private Integer length;

    private Set list;

    private Integer precision;

    protected String name;

    private boolean hasLength;
    
    private Collator collator = Collator.getInstance();

    protected MySQLDataType(String name, boolean hasLength) {
        this.name = name;
        this.hasLength = hasLength;
    }
    
    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayValue() {
        return name;
    }

    @Override
    public TechnologyType getTech() {
        return TechnologyType.MYSQL;
    }

    @Override
    public int compareTo(DataType o) {
        return collator.compare(getName(), o.getName());
    }

    @Override
    public String toString() {
//        String format = getLength() != null ? "%s (%d)" : "%s";
//        String data = getLength() != null ? String.format(format, getName(), getLength()) : String.format(format,
//                  getName());
        return getName();
    }

    @Override
    public boolean hasLength() {
        return this.hasLength;
    }

    @Override
    public boolean isEnumOrSet() {
        return getName().equals("ENUM") || getName().equals("SET");
    }
    
    @Override
    public boolean isEnum() {
        return getName().equals("ENUM");
    }
    
    @Override
    public boolean isSet() {
        return getName().equals("SET");
    }

    @Override
    public boolean isInteger() {
        return getName().equalsIgnoreCase("INTEGER") || getName().equalsIgnoreCase("INT") || getName().equalsIgnoreCase("TINYINT") || getName().equalsIgnoreCase("SMALLINT") || getName().equalsIgnoreCase("MEDIUMINT") || getName().equalsIgnoreCase("BIGINT");
    }
    
    @Override
    public boolean hasPrecisionInteger() {
        return getName().equalsIgnoreCase("DECIMAL") || getName().equalsIgnoreCase("FLOAT");
    }
    
    @Override
    public boolean isLargeString() {
        return getName().equalsIgnoreCase("TINYBLOB") || 
                getName().equalsIgnoreCase("BLOB") || 
                getName().equalsIgnoreCase("MEDIUMBLOB") || 
                getName().equalsIgnoreCase("LONGBLOB") || 
                getName().equalsIgnoreCase("TINYTEXT") || 
                getName().equalsIgnoreCase("TEXT") || 
                getName().equalsIgnoreCase("MEDIUMTEXT") || 
                getName().equalsIgnoreCase("LONGTEXT") || 
                ((getName().equalsIgnoreCase("CHAR") || getName().equalsIgnoreCase("VARCHAR")) && getLength() > 255);
    }
    
    @Override
    public DataType copy(DataType type) {
        if (type != null) {
            setLength(type.getLength());
            setList(type.getList());
            setPrecision(type.getPrecision());
            name = type.getName();
            hasLength = type.hasLength();
        }
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof DataType)) {
            return false;
        }
        DataType type = (DataType) obj;
        return Objects.equals(name, type.getName()) && Objects.equals(getLength(), type.getLength()) && Arrays.equals(getList() != null ? getList().toArray() : null, type.getList() != null ? type.getList().toArray() : null) && Objects.equals(getPrecision(), type.getPrecision()); //To change body of generated methods, choose Tools | Templates.
    }

}
