/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Table;
import javax.persistence.Column;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "SessionsSavePoint")
public class SessionsSavePoint extends BaseModel{
        
    public SessionsSavePoint(){}
    
    @Column(name = "STATUS")
    private byte status = 2;//whether it was interrupted or not
    
    @Column(name = "CONTAINEES")
    private String containers = "";//what it contains, this is what should be
    //exposed to the class to restore the items
    
    @Column(name = "NAME")
    private String name = ""; //whole application session by name
    
    @Column(name = "OTHERS")
    private String others = "0.01";//incase i want to add something :)
    
    @Column(name = "LAST_UPDATE")
    private Timestamp lastUpdate;
    
     @Override
    public int hashCode() {
       return super.hashCode();
    }
    
     @Override
    public boolean equals(Object obj) {
        if(obj instanceof SessionsSavePoint){
            SessionsSavePoint ssp = (SessionsSavePoint)obj;
            if(name.equals(ssp.getName()) && 
                    status == ssp.getStatus() &&
                    hashCode() == ssp.hashCode()){
                return true;
            }
        }
        return false;
    }    
    
    @Override
    public String toString() { 
        return name +" with " + status + " for { " 
                + containers + " }";
    }
    
}
