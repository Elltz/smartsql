
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LICENSE_DATA")
public class LicenseData extends BaseModel {
    
    @Column(name = "TYPE")
    private String type;
    
    @Column(name = "CREDITS")
    private String credits;
    
    @Column(name = "FULL_USAGE")
    private String fullUsage;
    
    @Column(name = "EXPIRED")
    private String expired;
}
