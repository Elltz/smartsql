package np.com.ngopal.smart.sql.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SlaveStatus extends BaseModel {
    
    private final Set<String> KEYS = new HashSet<>(Arrays.asList(new String[] {
        "Slave_IO_State",
        "Master_Host",
        "Master_User",
        "Master_Port",
        "Connect_Retry",
        "Master_Log_File",
        "Read_Master_Log_Pos",
        "Relay_Log_File",
        "Relay_Log_Pos",
        "Relay_Master_Log_File",
        "Slave_IO_Running",
        "Slave_SQL_Running",
        "Replicate_Do_DB",
        "Replicate_Ignore_DB",
        "Replicate_Do_Table",
        "Replicate_Ignore_Table",
        "Replicate_Wild_Do_Table",
        "Replicate_Wild_Ignore_Table",
        "Last_Errno",
        "Last_Error",
        "Skip_Counter",
        "Exec_Master_Log_Pos",
        "Relay_Log_Space",
        "Until_Condition",
        "Until_Log_File",
        "Until_Log_Pos",
        "Master_SSL_Allowed",
        "Master_SSL_CA_File",
        "Master_SSL_CA_Path",
        "Master_SSL_Cert",
        "Master_SSL_Cipher",
        "Master_SSL_Key",
        "Seconds_Behind_Master",
        "Master_SSL_Verify_Server_Cert",
        "Last_IO_Errno",
        "Last_IO_Error",
        "Last_SQL_Errno",
        "Last_SQL_Error",
        "Replicate_Ignore_Server_Ids",
        "Master_Server_Id",
        "Master_UUID",
        "Master_Info_File",
        "SQL_Delay",
        "SQL_Remaining_Delay",
        "Slave_SQL_Running_State",
        "Master_Retry_Count",
        "Master_Bind",
        "Last_IO_Error_Timestamp",
        "Last_SQL_Error_Timestamp",
        "Master_SSL_Crl",
        "Master_SSL_Crlpath",
        "Retrieved_Gtid_Set",
        "Executed_Gtid_Set",
        "Auto_Position"
    }));

    private String Slave_IO_State;
    private String Master_Host;
    private String Master_User;
    private Integer Master_Port = 0;
    private Integer Connect_Retry = 0;
    private String Master_Log_File;
    private Integer Read_Master_Log_Pos = 0;
    private String Relay_Log_File;
    private Integer Relay_Log_Pos = 0;
    private String Relay_Master_Log_File;
    private String Slave_IO_Running;
    private String Slave_SQL_Running;
    private String Replicate_Do_DB;
    private String Replicate_Ignore_DB;
    private String Replicate_Do_Table;
    private String Replicate_Ignore_Table;
    private String Replicate_Wild_Do_Table;
    private String Replicate_Wild_Ignore_Table;
    private Integer Last_Errno = 0;
    private String Last_Error;
    private Integer Skip_Counter = 0;
    private Integer Exec_Master_Log_Pos = 0;
    private Integer Relay_Log_Space = 0;
    private String Until_Condition;
    private String Until_Log_File;
    private Integer Until_Log_Pos = 0;
    private String Master_SSL_Allowed;
    private String Master_SSL_CA_File;
    private String Master_SSL_CA_Path;
    private String Master_SSL_Cert;
    private String Master_SSL_Cipher;
    private String Master_SSL_Key;
    private Integer Seconds_Behind_Master = 0;
    private String Master_SSL_Verify_Server_Cert;
    private Integer Last_IO_Errno = 0;
    private String Last_IO_Error;
    private Integer Last_SQL_Errno = 0;
    private String Last_SQL_Error;
    private String Replicate_Ignore_Server_Ids;
    private String Master_Server_Id;
    private String Master_UUID;
    private String Master_Info_File;
    private Integer SQL_Delay = 0;
    private Integer SQL_Remaining_Delay = 0;
    private String Slave_SQL_Running_State;
    private Integer Master_Retry_Count = 0;
    private String Master_Bind;
    private String Last_IO_Error_Timestamp;
    private String Last_SQL_Error_Timestamp;
    private String Master_SSL_Crl;
    private String Master_SSL_Crlpath;
    private String Retrieved_Gtid_Set;
    private String Executed_Gtid_Set;
    private Integer Auto_Position = 0;
    
    private final Map<String, Object> additionValues = new HashMap<>();
    public void putAdditionValue(String key, Object value) {
        additionValues.put(key, value);
    }
    public Object getAdditionValue(String key) {
        return additionValues.get(key);
    }
    
    // TODO Remove it - just for test
    public static SlaveStatus random() {
        SlaveStatus ss = new SlaveStatus();
        Random r = new Random();
        
        ss.Master_Port = r.nextInt(40);
        ss.Connect_Retry = r.nextInt(40);
        ss.Read_Master_Log_Pos = r.nextInt(40);
        ss.Relay_Log_Pos = r.nextInt(40);
        ss.Last_Errno = r.nextInt(40);
        ss.Skip_Counter = r.nextInt(40);
        ss.Exec_Master_Log_Pos = r.nextInt(40);
        ss.Relay_Log_Space = r.nextInt(40);
        ss.Until_Log_Pos = r.nextInt(40);
        ss.Seconds_Behind_Master = r.nextInt(40);
        ss.Last_IO_Errno = r.nextInt(40);
        ss.Last_SQL_Errno = r.nextInt(40);
        ss.SQL_Delay = r.nextInt(40);
        ss.SQL_Remaining_Delay = r.nextInt(40);
        ss.Master_Retry_Count =r.nextInt(40);
        ss.Auto_Position = r.nextInt(40);
        
        return ss;
    }
    
    public static SlaveStatus empty() {
        SlaveStatus ss = new SlaveStatus();
        ss.Master_Port = 0;
        ss.Connect_Retry = 0;
        ss.Read_Master_Log_Pos = 0;
        ss.Relay_Log_Pos = 0;
        ss.Last_Errno = 0;
        ss.Skip_Counter = 0;
        ss.Exec_Master_Log_Pos = 0;
        ss.Relay_Log_Space = 0;
        ss.Until_Log_Pos = 0;
        ss.Seconds_Behind_Master = 0;
        ss.Last_IO_Errno = 0;
        ss.Last_SQL_Errno = 0;
        ss.SQL_Delay = 0;
        ss.SQL_Remaining_Delay = 0;
        ss.Master_Retry_Count = 0;
        ss.Auto_Position = 0;
        
        return ss;
    }
    
    // @TODO This is bad
    public Object getValue(String key) {
        
        switch (key) {
            case "Slave_IO_State":
                return this.Slave_IO_State;
                
            case "Master_Host":
                return this.Master_Host;
                
            case "Master_User":
                return this.Master_User;
                
            case "Master_Port":
                return this.Master_Port;
                
            case "Connect_Retry":
                return this.Connect_Retry;
                
            case "Master_Log_File":
                return this.Master_Log_File;
                
            case "Read_Master_Log_Pos":
                return this.Read_Master_Log_Pos;
                
            case "Relay_Log_File":
                return this.Relay_Log_File;
                
            case "Relay_Log_Pos":
                return this.Relay_Log_Pos;
                
            case "Relay_Master_Log_File":
                return this.Relay_Master_Log_File;
                
            case "Slave_IO_Running":
                return this.Slave_IO_Running;
                
            case "Slave_SQL_Running":
                return this.Slave_SQL_Running;
                
            case "Replicate_Do_DB":
                return this.Replicate_Do_DB;
                
            case "Replicate_Ignore_DB":
                return this.Replicate_Ignore_DB;
                
            case "Replicate_Do_Table":
                return this.Replicate_Do_Table;
                
            case "Replicate_Ignore_Table":
                return this.Replicate_Ignore_Table;
                
            case "Replicate_Wild_Do_Table":
                return this.Replicate_Wild_Do_Table;
                
            case "Replicate_Wild_Ignore_Table":
                return this.Replicate_Wild_Ignore_Table;
                
            case "Last_Errno":
                return this.Last_Errno;
                
            case "Last_Error":
                return this.Last_Error;
                
            case "Skip_Counter":
                return this.Skip_Counter;
                
            case "Exec_Master_Log_Pos":
                return this.Exec_Master_Log_Pos;
                
            case "Relay_Log_Space":
                return this.Relay_Log_Space;
                
            case "Until_Condition":
                return this.Until_Condition;
                
            case "Until_Log_File":
                return this.Until_Log_File;
                
            case "Until_Log_Pos":
                return this.Until_Log_Pos;
                
            case "Master_SSL_Allowed":
                return this.Master_SSL_Allowed;
                
            case "Master_SSL_CA_File":
                return this.Master_SSL_CA_File;
                
            case "Master_SSL_CA_Path":
                return this.Master_SSL_CA_Path;
                
            case "Master_SSL_Cert":
                return this.Master_SSL_Cert;
                
            case "Master_SSL_Cipher":
                return this.Master_SSL_Cipher;
                
            case "Master_SSL_Key":
                return this.Master_SSL_Key;
                
            case "Seconds_Behind_Master":
                return this.Seconds_Behind_Master;
                
            case "Master_SSL_Verify_Server_Cert":
                return this.Master_SSL_Verify_Server_Cert;
                
            case "Last_IO_Errno":
                return this.Last_IO_Errno;
                
            case "Last_IO_Error":
                return this.Last_IO_Error;
                
            case "Last_SQL_Errno":
                return this.Last_SQL_Errno;
                
            case "Last_SQL_Error":
                return this.Last_SQL_Error;
                
            case "Replicate_Ignore_Server_Ids":
                return this.Replicate_Ignore_Server_Ids;
                
            case "Master_Server_Id":
                return this.Master_Server_Id;
                
            case "Master_UUID":
                return this.Master_UUID;
                
            case "Master_Info_File":
                return this.Master_Info_File;
                
            case "SQL_Delay":
                return this.SQL_Delay;
                
            case "SQL_Remaining_Delay":
                return this.SQL_Remaining_Delay;
                
            case "Slave_SQL_Running_State":
                return this.Slave_SQL_Running_State;
                
            case "Master_Retry_Count":
                return this.Master_Retry_Count;
                
            case "Master_Bind":
                return this.Master_Bind;
                
            case "Last_IO_Error_Timestamp":
                return this.Last_IO_Error_Timestamp;
                
            case "Last_SQL_Error_Timestamp":
                return this.Last_SQL_Error_Timestamp;
                
            case "Master_SSL_Crl":
                return this.Master_SSL_Crl;
                
            case "Master_SSL_Crlpath":
                return this.Master_SSL_Crlpath;
                
            case "Retrieved_Gtid_Set":
                return this.Retrieved_Gtid_Set;
                
            case "Executed_Gtid_Set":
                return this.Executed_Gtid_Set;
                
            case "Auto_Position":
                return this.Auto_Position;
        }
        
        return null;
    }
    
    public void setValue(String key, ResultSet rs) throws SQLException {
        
        switch (key) {
            case "Slave_IO_State":
                this.Slave_IO_State = rs.getString(key);
                break;
                
            case "Master_Host":
                this.Master_Host = rs.getString(key);
                break;
                
            case "Master_User":
                this.Master_User = rs.getString(key);
                break;
                
            case "Master_Port":
                this.Master_Port = rs.getInt(key);
                break;
                
            case "Connect_Retry":
                this.Connect_Retry = rs.getInt(key);
                break;
                
            case "Master_Log_File":
                this.Master_Log_File = rs.getString(key);
                break;
                
            case "Read_Master_Log_Pos":
                this.Read_Master_Log_Pos = rs.getInt(key);
                break;
                
            case "Relay_Log_File":
                this.Relay_Log_File = rs.getString(key);
                break;
                
            case "Relay_Log_Pos":
                this.Relay_Log_Pos = rs.getInt(key);
                break;
                
            case "Relay_Master_Log_File":
                this.Relay_Master_Log_File = rs.getString(key);
                break;
                
            case "Slave_IO_Running":
                this.Slave_IO_Running = rs.getString(key);
                break;
                
            case "Slave_SQL_Running":
                this.Slave_SQL_Running = rs.getString(key);
                break;
                
            case "Replicate_Do_DB":
                this.Replicate_Do_DB = rs.getString(key);
                break;
                
            case "Replicate_Ignore_DB":
                this.Replicate_Ignore_DB = rs.getString(key);
                break;
                
            case "Replicate_Do_Table":
                this.Replicate_Do_Table = rs.getString(key);
                break;
                
            case "Replicate_Ignore_Table":
                this.Replicate_Ignore_Table = rs.getString(key);
                break;
                
            case "Replicate_Wild_Do_Table":
                this.Replicate_Wild_Do_Table = rs.getString(key);
                break;
                
            case "Replicate_Wild_Ignore_Table":
                this.Replicate_Wild_Ignore_Table = rs.getString(key);
                break;
                
            case "Last_Errno":
                this.Last_Errno = rs.getInt(key);
                break;
                
            case "Last_Error":
                this.Last_Error = rs.getString(key);
                break;
                
            case "Skip_Counter":
                this.Skip_Counter = rs.getInt(key);
                break;
                
            case "Exec_Master_Log_Pos":
                this.Exec_Master_Log_Pos = rs.getInt(key);
                break;
                
            case "Relay_Log_Space":
                this.Relay_Log_Space = rs.getInt(key);
                break;
                
            case "Until_Condition":
                this.Until_Condition = rs.getString(key);
                break;
                
            case "Until_Log_File":
                this.Until_Log_File = rs.getString(key);
                break;
                
            case "Until_Log_Pos":
                this.Until_Log_Pos = rs.getInt(key);
                break;
                
            case "Master_SSL_Allowed":
                this.Master_SSL_Allowed = rs.getString(key);
                break;
                
            case "Master_SSL_CA_File":
                this.Master_SSL_CA_File = rs.getString(key);
                break;
                
            case "Master_SSL_CA_Path":
                this.Master_SSL_CA_Path = rs.getString(key);
                break;
                
            case "Master_SSL_Cert":
                this.Master_SSL_Cert = rs.getString(key);
                break;
                
            case "Master_SSL_Cipher":
                this.Master_SSL_Cipher = rs.getString(key);
                break;
                
            case "Master_SSL_Key":
                this.Master_SSL_Key = rs.getString(key);
                break;
                
            case "Seconds_Behind_Master":
                this.Seconds_Behind_Master = rs.getInt(key);
                break;
                
            case "Master_SSL_Verify_Server_Cert":
                this.Master_SSL_Verify_Server_Cert = rs.getString(key);
                break;
                
            case "Last_IO_Errno":
                this.Last_IO_Errno = rs.getInt(key);
                break;
                
            case "Last_IO_Error":
                this.Last_IO_Error = rs.getString(key);
                break;
                                
            case "Last_SQL_Errno":
                this.Last_SQL_Errno = rs.getInt(key);
                break;
                
            case "Last_SQL_Error":
                this.Last_SQL_Error = rs.getString(key);
                break;
                
            case "Replicate_Ignore_Server_Ids":
                this.Replicate_Ignore_Server_Ids = rs.getString(key);
                break;
                
            case "Master_Server_Id":
                this.Master_Server_Id = rs.getString(key);
                break;
                
            case "Master_UUID":
                this.Master_UUID = rs.getString(key);
                break;
                
            case "Master_Info_File":
                this.Master_Info_File = rs.getString(key);
                break;
                
            case "SQL_Delay":
                this.SQL_Delay = rs.getInt(key);
                break;
                
            case "SQL_Remaining_Delay":
                this.SQL_Remaining_Delay = rs.getInt(key);          
                break;
                
            case "Slave_SQL_Running_State":
                this.Slave_SQL_Running_State = rs.getString(key);
                break;
                
            case "Master_Retry_Count":
                this.Master_Retry_Count = rs.getInt(key);
                break;
                
            case "Master_Bind":
                this.Master_Bind = rs.getString(key); 
                break;
                
            case "Last_IO_Error_Timestamp":
                this.Last_IO_Error_Timestamp = rs.getString(key);
                break;
                
            case "Last_SQL_Error_Timestamp":
                this.Last_SQL_Error_Timestamp = rs.getString(key);
                break;
                
            case "Master_SSL_Crl":
                this.Master_SSL_Crl = rs.getString(key);
                break;
                
            case "Master_SSL_Crlpath":
                this.Master_SSL_Crlpath = rs.getString(key);
                break;
                
            case "Retrieved_Gtid_Set":
                this.Retrieved_Gtid_Set = rs.getString(key);
                break;
                
            case "Executed_Gtid_Set":
                this.Executed_Gtid_Set = rs.getString(key);
                break;
                
            case "Auto_Position":
                this.Auto_Position = rs.getInt(key);
                break;
        }
    }
    
    public boolean containsKey(String key) {
        return KEYS.contains(key);
    }
    
    public Set<String> keySet() {
        return KEYS;
    }
    
    
    public SlaveStatus copyValue() {
        SlaveStatus ss = new SlaveStatus();
        ss.Slave_IO_State = Slave_IO_State;
        ss.Master_Host = Master_Host;
        ss.Master_User = Master_User;
        ss.Master_Port = Master_Port;
        ss.Connect_Retry = Connect_Retry;
        ss.Master_Log_File = Master_Log_File;
        ss.Read_Master_Log_Pos = Read_Master_Log_Pos;
        ss.Relay_Log_File = Relay_Log_File;
        ss.Relay_Log_Pos = Relay_Log_Pos;
        ss.Relay_Master_Log_File = Relay_Master_Log_File;
        ss.Slave_IO_Running = Slave_IO_Running;
        ss.Slave_SQL_Running = Slave_SQL_Running;
        ss.Replicate_Do_DB = Replicate_Do_DB;
        ss.Replicate_Ignore_DB = Replicate_Ignore_DB;
        ss.Replicate_Do_Table = Replicate_Do_Table;
        ss.Replicate_Ignore_Table = Replicate_Ignore_Table;
        ss.Replicate_Wild_Do_Table = Replicate_Wild_Do_Table;
        ss.Replicate_Wild_Ignore_Table = Replicate_Wild_Ignore_Table;
        ss.Last_Errno = Last_Errno;
        ss.Last_Error = Last_Error;
        ss.Exec_Master_Log_Pos = Exec_Master_Log_Pos;
        ss.Relay_Log_Space = Relay_Log_Space;
        ss.Until_Condition = Until_Condition;
        ss.Until_Log_File = Until_Log_File;
        ss.Until_Log_Pos = Until_Log_Pos;
        ss.Master_SSL_Allowed = Master_SSL_Allowed;
        ss.Master_SSL_CA_File = Master_SSL_CA_File;
        ss.Master_SSL_CA_Path = Master_SSL_CA_Path;
        ss.Master_SSL_Cert = Master_SSL_Cert;
        ss.Master_SSL_Cipher = Master_SSL_Cipher;
        ss.Master_SSL_Key = Master_SSL_Key;
        ss.Seconds_Behind_Master = Seconds_Behind_Master;
        ss.Master_SSL_Verify_Server_Cert = Master_SSL_Verify_Server_Cert;
        ss.Last_IO_Errno = Last_IO_Errno;
        ss.Last_IO_Error = Last_IO_Error;
        ss.Last_SQL_Errno = Last_SQL_Errno;
        ss.Last_SQL_Error = Last_SQL_Error;
        ss.Replicate_Ignore_Server_Ids = Replicate_Ignore_Server_Ids;
        ss.Master_Server_Id = Master_Server_Id;
        ss.Master_UUID = Master_UUID;
        ss.Master_Info_File = Master_Info_File;
        ss.SQL_Delay = SQL_Delay;
        ss.SQL_Remaining_Delay = SQL_Remaining_Delay;
        ss.Slave_SQL_Running_State = Slave_SQL_Running_State;
        ss.Master_Retry_Count = Master_Retry_Count;
        ss.Master_Bind = Master_Bind;
        ss.Last_IO_Error_Timestamp = Last_IO_Error_Timestamp;
        ss.Last_SQL_Error_Timestamp = Last_SQL_Error_Timestamp;
        ss.Master_SSL_Crl = Master_SSL_Crl;
        ss.Master_SSL_Crlpath = Master_SSL_Crlpath;
        ss.Retrieved_Gtid_Set = Retrieved_Gtid_Set;
        ss.Executed_Gtid_Set = Executed_Gtid_Set;
        ss.Auto_Position = Auto_Position;
        return ss;
    }
}