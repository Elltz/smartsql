/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.tech.mysql;

import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.Keyword;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
public abstract class MySQLKeyword implements Keyword {

    private Integer length;

    private Set list;

    private Integer precision;

    protected String name;

    private boolean hasLength;

    protected MySQLKeyword(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayValue() {
        return name;
    }

    @Override
    public TechnologyType getTech() {
        return TechnologyType.MYSQL;
    }

    public int compareTo(DataType o) {
        return Objects.equals(o.getName(), getName()) ? 0 : 1;
    }

    public String toString() {
        return getName();
    }

    @Override
    public int compareTo(Keyword o) {
        return Objects.equals(o.getName(), getName()) ? 0 : -1;
    }

    @Override
    public Keyword copy(Keyword type) {
        name = type.getName();
        return this;
    }

}
