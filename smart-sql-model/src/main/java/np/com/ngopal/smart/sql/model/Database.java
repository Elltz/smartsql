/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.text.Collator;
import java.util.*;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Database implements Comparable<Database>, DBElement {

    private ConnectionParams params;

    private String name;
    
    private String charset;
    private String collate;

    private List<DBTable> tables;
    private List<View> views;
    private List<StoredProc> storedProc;
    private List<Function> functions;
    private List<Trigger> triggers;
    private List<Event> events;
    
    @Transient
    private Map<String, List<Index>> indexCache;
    
    public void clearIndexCache() {
        if (indexCache != null) {
            indexCache.clear();
        }
    }
    
    public void addIndexCache(String table, List<Index> indexes) {
        if (indexCache == null) {
            indexCache = new HashMap<>();
        }
        indexCache.put(table, indexes);
    }
    
    public List<Index> getIndexCache(String table) {
        return indexCache != null ? indexCache.get(table) : null;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    @Override
    public int compareTo(Database children) {
        return Collator.getInstance().compare(getName(), children.getName());
    }


    public DBTable getTable(String name) {
        if (tables != null) {
            for (DBTable table : tables) {
                if (table.getName().equalsIgnoreCase(name)) {
                    return table;
                }
            }
        }
        return null;
    }
    
    public void addTable(DBTable table) {
        if (tables == null) {
            tables = new ArrayList<>();
        }
        
        tables.add(table);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Database) {
            Database db = (Database)obj;
            
            return Objects.equals(db.getName(), getName()) && Objects.equals(db.getParams(), getParams());
        }
        
        return false;
    }

}
