/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import lombok.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class Advanced {
    private String comment;

    private int autoIncrement = -1;

    private int avgRowLength = -1;

    private int maxRows = -1;

    private int minRows = -1;

    private Rowformat rowformat;

    private Checksum checksum;

    private DelayKeyWrite delayKeyWrite;

    public void copy(Advanced adv) {
        this.comment = adv != null ? adv.comment : null;
        this.autoIncrement = adv != null ? adv.autoIncrement : -1;
        this.avgRowLength = adv != null ? adv.avgRowLength : -1;
        this.maxRows = adv != null ? adv.maxRows : -1;
        this.minRows = adv != null ? adv.minRows : -1;
        this.rowformat = adv != null ? adv.rowformat : null;
        this.checksum = adv != null ? adv.checksum : null;
        this.delayKeyWrite = adv != null ? adv.delayKeyWrite : null;
    }

}
