/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.tech.mysql;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.DynamicType;
import np.com.ngopal.smart.sql.model.Keyword;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public abstract class MySQLKeywords extends DynamicType<Keyword> {

    public static final Set<Keyword> TYPES;

    public static Keyword getEmptyDataType() {
        return new MySQLKeyword("") {
        };
    }

    static {

        TYPES = new TreeSet<>();
        try {
            Properties p = new Properties();
            p.load(MySQLDataType.class.getResourceAsStream("keywords.properties"));
            for (Object kObj : p.keySet()) {
                final String key = kObj.toString();
                final boolean value = Boolean.parseBoolean(p.get(key).toString());
                TYPES.add(new MySQLKeyword(key) {
                });
            }
        } catch (IOException ex) {
            Logger.getLogger(MySQLDataType.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Set<Keyword> getTypes() {
        return TYPES;
    }

    public static String[] getTypesArray() {
        String[] array = new String[TYPES.size()];
        int i = 0;
        for (Keyword TYPES1 : TYPES) {
            array[i++] = TYPES1.getName();
        }
        return array;
    }

    @Override
    public Keyword getTypeByName(String name) {
        Keyword type = super.getTypeByName(name);
        return type == null ? type : getEmptyDataType().copy(type);
    }

}
