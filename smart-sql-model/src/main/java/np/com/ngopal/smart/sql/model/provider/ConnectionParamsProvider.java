/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.provider;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class ConnectionParamsProvider implements Provider<ConnectionParams> {

    @Inject
    private Injector injector;

    @Override
    public ConnectionParams get() {

        return injector.getInstance(ConnectionParams.class);
    }

}
