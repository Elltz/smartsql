/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.ArrayList;
import java.util.HashMap;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import lombok.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Index implements DBElement {

    private String name;

    private DBTable table;
    private List<Column> columns;

    private IndexType type;
    
    private Map<String, Long> seqInIndexMap;
    private Map<String, Long> cardinalityInIndexMap;
    private Map<String, Integer> columnLengthMap;
    
    public void copy(Index index) {

        this.name = index.name;
        this.type = index.type;
        this.seqInIndexMap = index.seqInIndexMap != null ? new HashMap<>(index.seqInIndexMap) : null;
        this.cardinalityInIndexMap = index.cardinalityInIndexMap != null ? new HashMap<>(index.cardinalityInIndexMap) : null;
        this.columnLengthMap = index.columnLengthMap != null ? new HashMap<>(index.columnLengthMap) : null;
        
        this.columns = new ArrayList<>();
        if (index.getColumns() != null) {
            for (int i = 0; i < index.getColumns().size(); i++) {

                Column column = new Column();
                this.columns.add(column);
                this.columns.get(i).copy(index.getColumns().get(i));
            }
        }
    }

    public boolean containsColumn(String columnName) {
        if (columns != null) {
            for (Column c0: columns) {
                if (c0.getName().equalsIgnoreCase(columnName)) {
                    return true;
                }
            }
        }
        
        return false;
    }

    @Override
    public String toString() {
        
        String columnStr = "";
        if (columns != null) {
            for (Column c: columns) {
                columnStr += c.getName() + ", ";
            }
            
            if (columnStr.endsWith(", ")) {
                columnStr = columnStr.substring(0, columnStr.length() - 2);
            }
        }
        return name + "(" + columnStr + ")" + (type != null && type != IndexType.PRIMARY ? " " + type.getDisplayValue() : "");
    }

    public int getColumnLength(String name) {
        Integer value = columnLengthMap != null ? columnLengthMap.get(name) : 0;
        return value != null ? value : 0;
    }
    
    public void setColumnLength(String name, Integer length) {
        if (columnLengthMap == null) {
            columnLengthMap = new HashMap<>();
        }
        columnLengthMap.put(name, length);
    }
}
