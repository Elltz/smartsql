/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@AllArgsConstructor
public enum IndexType implements Displayable {

    PRIMARY("Primary"), FULL_TEXT("Full Text"), UNIQUE_TEXT("Unique"), SPATIAL("Spatial");
//, FOREIGN("Foreign")

    private String displayValue;

}
