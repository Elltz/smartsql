/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PROFILER_SETTINGS")
public class ProfilerSettings extends BaseModel {

    @Column(name = "POLL_INTERVAL")
    private BigDecimal pollInterval = BigDecimal.TEN; // Very short (0.1sec); Short (0.25); Normal(1); Long(3); Very long(10)

    @Column(name = "SLOW_QUERY_THRESHOLD")
    private Long slowQueryThreshold = 2000L; // in miliseconds

    @Column(name = "DATA_ROTATION")
    private Long dataRotation = 7L; // in days
    
    @Column(name = "DATA_REFRESH")
    private Long dataRefresh = 60L; // in sec
    
    @Column(name = "CONNECTION_ID")
    private Long connectionId;
    
    @Column(name = "STORAGE_TYPE")
    private int storageType = 0;
    
    @Column(name = "DATABASE_NAME")
    private String databaseName;
}
