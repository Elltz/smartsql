package np.com.ngopal.smart.sql.model;

import java.util.Arrays;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import lombok.NoArgsConstructor;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Schedule extends BaseModel  {
    
    @Column(name = "NAME")
    private String name;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "PORT")
    private int port;
    @Column(name = "DATABASE")
    private String database;

    @Column(name = "SSHHOST")
    private String sshHost;
    @Column(name = "SSHPORT")
    private int sshPort = 22;
    @Column(name = "SSHUSER")
    private String sshUser;
    @Column(name = "SSHPASSWORD")
    private String sshPassword;
    
    @Column(name = "USE_COMPRESSED_PROTOCOL")
    private boolean useCompressedProtocol;
    @Column(name = "SESSION_IDLE_TIMEOUT")
    private boolean sessionIdleTimeout;
    @Column(name = "SESSION_IDLE_TIMEOUT_VALUE")
    private long sessionIdleTimeoutValue;
    
    @Column(name = "NOTIFICATION_EMAIL")
    private boolean notificationEmail;
    
    @Column(name = "EMAIL_FROM_NAME")
    private String emailFromName;
    @Column(name = "EMAIL_FROM_EMAIL")
    private String emailFromEmail;
    @Column(name = "EMAIL_REPLY_EMAIL")
    private String emailReplyEmail;
    
    @Column(name = "EMAIL_TO_EMAIL")
    private String emailToEmail;
    @Column(name = "EMAIL_CC")
    private String emailCc;
    @Column(name = "EMAIL_BCC")
    private String emailBcc;
    
    @Column(name = "EMAIL_HOST")
    private String emailHost;
    @Column(name = "EMAIL_PORT")
    private int emailPort = 25;
    
    @Column(name = "EMAIL_ENCRYPTIOR")
    private int emailEncryptior;
    @Column(name = "EMAIL_AUTH")
    private boolean emailAuth = false;
   
    @Column(name = "EMAIL_ACCOUNT_NAME")
    private String emailAccountName;
    @Column(name = "EMAIL_PASSWD")
    private String emailPasswd;
    
    @Column(name = "LOG_FILE")
    private String logFile;
    
    @Column(name = "RUN_FREQUENCY")
    private int runFrequency;
    @Column(name = "FREQUENCY_HOUR")
    private int frequencyHour;
    @Column(name = "FREQUENCY_MINUTES")
    private int frequencyMinutes;
    @Column(name = "FREQUENCY_WEEKLY")
    private String frequencyWeekly;
        
    @Column(name = "USE_WINDOWS_SCHEDULE")
    private boolean useWindowsSchedule = false;
    
    @Transient
    private ConnectionParams params;
    
    public Schedule(ConnectionParams params) {
        this.params = params;
        
        if (params != null) {
            name = params.getName();
            address = params.getAddress();
            username = params.getUsername();
            password = params.getPassword();
            port = params.getPort();     
            if (params.isUseSshTunneling()) {
                sshHost = params.getSshHost();
                sshPort = params.getSshPort();
                sshUser = params.getSshUser();
                sshPassword = params.getSshPassword();
            }
        }
    }
    
    public ConnectionParams toConnectionParams() {
        ConnectionParams p = new ConnectionParams(name, TechnologyType.MYSQL, address, username, password, port);
        if (sshHost != null && !sshHost.isEmpty()) {
            p.setUseSshTunneling(true);
            p.setSshHost(sshHost);
            p.setSshPort(sshPort);
            p.setSshUser(sshUser);
            p.setSshPassword(sshPassword);
        }
        
        if (database != null) {
            p.setDatabase(Arrays.asList(new String[]{database}));
        }
        
        return p;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
