/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import np.com.ngopal.model.core.BaseModel;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Transient;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */

@Entity
@Table(name = "TableTemplate")

public class TableTemplate  extends BaseModel {
    
    private static final String COLUMN_ATTR_PK = "primary_key";
    private static final String COLUMN_ATTR_NN = "not_null";
    private static final String COLUMN_ATTR_AI = "auto_increment";
    private static final String COLUMN_ATTR_NAME = "column_name";
    private static final String COLUMN_ATTR_DATATYPE = "column_datatype";
    private static final String COLUMN_ATTR_DEFAULT = "column_default";
    private static final String ALL_ATTR_COLLATION = "collation";
    private static final String COLUMN_ATTR_COMMENT = "column_comment";
    private static final String TABLE_ATTR_ENGINE_TYPE = "table_engine";
    private static final String COLUMN_ATTR_UQ = "column_uq";
    private static final String TABLE_ELEMENT_NAME = "TABLE";
    private static final String COLUMN_ELEMENT_NAME = "COLUMN";
    
    
    @Column(name = "TEMPLATENAME")
    private String name = "";
    
    public void setTemplateName(String templateName) throws IOException,
            ParserConfigurationException, SAXException{
        if (name.isEmpty()) {
            name = templateName;
            initTable();
        } else {
            name = templateName;
        }
    }
    
    /**
     * xml
     */
    @Column(name = "TEMPLATEDATA")
    private String data= "";
    
    @Transient
    public np.com.ngopal.smart.sql.model.DBTable table = null;
    
    public void initTable() throws ParserConfigurationException,SAXException,IOException{
        table = new np.com.ngopal.smart.sql.model.DBTable();
        table.setName(name);
        if(!data.isEmpty()){
            Document doc = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().parse(new InputSource(new StringReader(data)));
            Element element = (Element) doc.getFirstChild();
            table.setCollation(element.getAttribute(ALL_ATTR_COLLATION));
            //table.setEngineType();
            List<np.com.ngopal.smart.sql.model.Column> cols  = new ArrayList<>();
            for (int i = 0; i < element.getChildNodes().getLength(); i++) {
                if (element.getChildNodes().item(i) instanceof Element) {
                    Element el = (Element) element.getChildNodes().item(i);
                    np.com.ngopal.smart.sql.model.Column c
                            = new np.com.ngopal.smart.sql.model.Column();
                    c.setAutoIncrement(Boolean.valueOf(el.getAttribute(COLUMN_ATTR_AI)));
                    c.setNotNull(Boolean.valueOf(el.getAttribute(COLUMN_ATTR_NN)));
                    c.setPrimaryKey(Boolean.valueOf(el.getAttribute(COLUMN_ATTR_PK)));
                    c.setName(el.getAttribute(COLUMN_ATTR_NAME));
                    c.setComment(el.getAttribute(COLUMN_ATTR_COMMENT));
                    c.setDefaults(el.getAttribute(COLUMN_ATTR_DEFAULT));
                    cols.add(c);
                }
            }
            table.setColumns(cols);
        }
        
    }
    
    private final Element retrieveNext(Element n){
        Element el  = (Element) n.getNextSibling();
        if(el == null){
            return (Element)n.getFirstChild();
        }
        return el;
    }
    
    public boolean persistTemplate() throws ParserConfigurationException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.newDocument();
            //doc.setXmlStandalone(true);
            Element parent = doc.createElement(TABLE_ELEMENT_NAME);
            parent.setAttribute(ALL_ATTR_COLLATION, table.getCollation());

            for (np.com.ngopal.smart.sql.model.Column c : table.getColumns()) {
                Element column = doc.createElement(COLUMN_ELEMENT_NAME);
                column.setAttribute(COLUMN_ATTR_AI, String.valueOf(c.isAutoIncrement()));
                column.setAttribute(COLUMN_ATTR_COMMENT, c.getComment());
                column.setAttribute(COLUMN_ATTR_DEFAULT, c.getDefaults());
                column.setAttribute(COLUMN_ATTR_NAME, c.getName());
                column.setAttribute(COLUMN_ATTR_NN, String.valueOf(c.isNotNull()));
                column.setAttribute(COLUMN_ATTR_PK, String.valueOf(c.isPrimaryKey()));
                //column.setAttribute(COLUMN_ATTR_UQ, ); i do not know this uq
                parent.appendChild(column);
            }
            doc.appendChild(parent);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            data = writer.getBuffer().toString();
            if(runs != null){
                runs.run();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
    
    @Transient
    public Runnable runs;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(20);
        sb.append(name);
        sb.append("\n");
        if (table.getColumns() != null) {
            for (np.com.ngopal.smart.sql.model.Column c : table.getColumns()) {
                sb.append(c.getName());
                sb.append(",");
            }
        }
        return sb.toString();
    }
    
}
