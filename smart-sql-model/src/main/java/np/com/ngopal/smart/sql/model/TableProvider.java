package np.com.ngopal.smart.sql.model;

import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface TableProvider {
    
    String getName();
    String getAlias();
    Database getDatabase();
    
    List<Column> getColumns();
}
