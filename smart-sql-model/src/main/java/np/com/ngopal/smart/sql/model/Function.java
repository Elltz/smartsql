package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Function implements DatabaseChildren, DBElement {

    private String name;

    private Database database;

    private String createScript;
    
    @Override
    public String toString() {
        return name;
    }

}
