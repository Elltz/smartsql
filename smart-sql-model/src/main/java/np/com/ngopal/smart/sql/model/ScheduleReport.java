package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Entity
@Getter
@Setter
@Table(name = "SCHEDULE_REPORT")
public class ScheduleReport extends Schedule {
    
    @Column(name = "SEND_RESULT_AN_EMAIL")
    private boolean sendResultAnEmail;
    @Column(name = "INCLUDE_ORIGINAL_QUERY")
    private boolean includeOriginalQuery;
    @Column(name = "INCLUDE_MESSAGES_FROM_SERVER")
    private boolean includeMessagesFromServer;
    @Column(name = "SEND_MAIL_ON_ERROR")
    private boolean sendMailOnError;
    
    @Column(name = "EXIT_IMMEDIATLY")
    private boolean exitImmediatly;
    
    @Column(name = "USER_DEFINED")
    private String userDefined;
    @Column(name = "DEFINED_WITH_TIME")
    private boolean definedWithTime;
    
    @Column(name = "SELECT_FILE")
    private boolean selectFile;
    @Column(name = "SELECT_FILE_PATH")
    private String selectFilePath;
    @Column(name = "QUERY")
    private String query;
    
    @Transient
    private String scheduleType = "report";
    
    public ScheduleReport() {
    }

    public ScheduleReport(ConnectionParams params) {
        super(params);
    }
}
