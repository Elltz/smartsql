/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.*;
import lombok.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class DBTable implements DatabaseChildren, TableProvider, DBElement {

    private String name;
    private String alias;

    private String characterSet;

    private String collation;

    private List<Column> columns;

    private Database database;

    private EngineType engineType;

    private Advanced advanced;

    private List<Index> indexes;    
    private List<ForeignKey> foreignKeys;
        
    @Override
    public String toString() {
        return name;
    }

    public void setColumns(List<Column> cols) {        
        this.columns = cols;
    }

    public void addColumn(Column c) {        
        if (columns == null) {
            columns = new ArrayList<>();
        }
        
        c.setTable(this);
        columns.add(c);
    }

    public Column getColumnByName(String name) {
        if (columns != null) {
            for (Column column : columns) {
                if (column.getName().equals(name)) {
                    return column;
                }
            }
        }
        return null;
    }

//    public Column getColumnById(Long id) {
//        for (Column column : columns) {
//            if (Objects.equals(column.getId(), id)) {
//                return column;
//            }
//        }
//        return null;
//    }

    // TODO Need check commented, maybe need some equals
//    @Override
//    public boolean equals(Object t) {
//        if (t != null && t instanceof DBTable) {
//            // TODO This need check
//            // but I have a lot of problem with this equals
//            // one of them is renamed Table - it has diferent names
//            // but has equals Id, so for now I try change this
//            // if table has id - that check for it
//            // also i add method isChanged
//            DBTable t2 = (DBTable) t;
//            
//            return getDatabase() != null && getDatabase().equals(t2.getDatabase()) && Objects.equals(getName(), t2.getName());
//        }
//        return false;
//    }
//    
    public boolean isChanged(DBTable otherTable) {
        return !Objects.equals(name, otherTable.name)
            || !Objects.equals(characterSet, otherTable.characterSet)
            || !Objects.equals(collation, otherTable.collation)
            || !Objects.equals(engineType, otherTable.engineType)
            || !Objects.equals(advanced, otherTable.advanced)
            || !Objects.equals(indexes, otherTable.indexes)
            || !Objects.equals(foreignKeys, otherTable.foreignKeys);
    }

//    @Override
//    public int hashCode() {
//        int hash = 5;
//        
//        hash = 67 * hash + Objects.hashCode(this.name);
//        hash = 67 * hash + Objects.hashCode(this.database);
//        
//        return hash;
//    }

    public DBTable makeCopy() {
        DBTable t = new DBTable();
        t.copy(this);
        
        if (this.getColumns() != null) {
            for (Column copyColumn: this.getColumns()) {
                //ignore empty columns
                if (copyColumn.getName() != null && !copyColumn.getName().trim().isEmpty()) {
                    Column column = new Column();
                    column.setTable(t);
                    column.copy(copyColumn);

                    t.columns.add(column);                
                }
            }
        }
        
        return t;
    }
    
    
    public void copy(DBTable t) {
        this.name = t.name;
        this.characterSet = t.characterSet;
        this.database = t.database;
        // this method used only in alter table
        // no need copy columns they will be recreated
        // but need check this code
//        this.columns = t.columns;
        this.columns = new ArrayList<>();
        this.collation = t.collation;
        this.engineType = t.engineType;
        this.advanced = new Advanced();
        this.advanced.copy(t.getAdvanced());
        this.indexes = new ArrayList<>();
        
        if (t.getIndexes() != null) {
            for (Index copyIndex: t.getIndexes()) {
                if (copyIndex.getName() != null) {
                    Index index = new Index();
                    index.setTable(this);
                    index.copy(copyIndex);

                    this.indexes.add(index);                
                }
            }
        }
        
        this.foreignKeys = new ArrayList<>();
        if (t.getForeignKeys()!= null) {
            for (ForeignKey copyFk: t.getForeignKeys()) {
                if (copyFk.getName() != null) {
                    ForeignKey fk = new ForeignKey();
                    fk.copy(copyFk);

                    this.foreignKeys.add(fk);    
                }
            }
        }
    }

    public void merge(DBTable t) {
        this.name = t.name;
        this.characterSet = t.characterSet;
        this.collation = t.collation;
        
        if (t.database != null) {
            this.database = t.database;
        }
    }

}
