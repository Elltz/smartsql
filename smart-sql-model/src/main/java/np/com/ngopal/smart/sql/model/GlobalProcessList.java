package np.com.ngopal.smart.sql.model;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GlobalProcessList extends BaseModel {

    private Integer threadId;    
    private String user;
    private String host;
    private String db;
    private String command;
    private BigDecimal time;
    private String state;
    private String querytemp;
    private Date datetime;
}
