
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({
    @NamedQuery(
        name = "columncardinality.find", 
        query = "SELECT c FROM ColumnCardinality c where c.paramsKey = :paramsKey")
})
@Table(name = "COLUMN_CARDINALITY")
public class ColumnCardinality extends BaseModel {
                    
    @javax.persistence.Column(name = "PARAMS_KEY")
    private String paramsKey;
    
    @javax.persistence.Column(name = "DATABASE")
    private String database;    
        
    @javax.persistence.Column(name = "DB_TABLE")
    private String dbtable;    
    
    @Column(name = "DB_COLUMN")
    private String dbcolumn;
    
    @Column(name = "CARDINALITY")
    private Long cardinality;
    
}
