
package np.com.ngopal.smart.sql.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LICENSE_DATA_USAGE")
public class LicenseDataUsage extends BaseModel {
    
    @Column(name = "USED_CREDITS")
    private String usedCredits;
    
    @Column(name = "USED_FULL_USAGE")
    private String usedFullUsage;
    
    @Column(name = "LAST_UPDATE")
    private Timestamp lastUpdate;
}
