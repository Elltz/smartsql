/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@AllArgsConstructor
public class Event implements DatabaseChildren, DBElement {
    
    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Database database;
    
    @Getter
    @Setter
    private String createScript;

    @Override
    public String toString() {
        return name;
    }

}
