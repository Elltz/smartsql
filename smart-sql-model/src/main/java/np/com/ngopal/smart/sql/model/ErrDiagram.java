package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@javax.persistence.Table(name = "DESIGNER_ERR_DIAGRAM")
public class ErrDiagram extends BaseModel {

    @javax.persistence.Column(name = "NAME")
    private String name;
    
    @javax.persistence.Column(name = "HOST")
    private String host;
    
    @javax.persistence.Column(name = "DATABASE")
    private String database;

    @javax.persistence.Column(name = "DIAGRAM_DATA")
    private String diagramData;
    
    @javax.persistence.Column(name = "PUBLIC_ID")
    private Long publicId;
    
    @Transient
    private String userId;
    @Transient
    private Long domainId;
    @Transient
    private String domainName;
    @Transient
    private Long subDomainId;
    @Transient
    private String subDomainName;
    @Transient
    private String description;
    @Transient
    private int numberOfTables;
}
