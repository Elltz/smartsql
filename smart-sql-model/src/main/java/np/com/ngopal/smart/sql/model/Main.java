/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
import java.io.File;
import java.net.MalformedURLException;
import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;

/**
 * @author rb
 *
 */
public class Main {

    /**
     * @param args
     * @throws MalformedURLException
     */
    public static void main(final String[] args) throws MalformedURLException {

        /*
         * The is the only time you have to access a class directly. Just returns the
         * Plugin manager.
         */
        final PluginManager pmf = PluginManagerFactory.createPluginManager();

        /*
         * Add plugins from somewhere. Be sure to put *the right path* in here. This
         * method may be called multiple times. If you plan to deliver your application
         * replace bin/ with for example myplugins.jar
         */
        pmf.addPluginsFrom(new File("bin/").toURI());

        /*
         * Thats it. Technically all plugins have now been loaded and are running. If you
         * would like to retrieve one, do it like this:
         */
//        final OutputService plugin = pmf.getPlugin(OutputService.class);
//        plugin.doSomething();
    }

}
