/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import lombok.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
@NoArgsConstructor
public class QueryResult {

    @Setter
    private double executionDuration;

    @Setter
    @Getter
    private double startDuration;

    @Setter
    @Getter
    private double endDuration;

    @Getter
    @Setter
    private String query;
    
    @Getter
    @Setter
    private String simpleQuery;
    
    @Getter
    @Setter
    private String rowAffected;
    
    @Getter
    @Setter
    private int errorCode;
    
    @Getter
    @Setter
    private String errorMessage;
    
    @Getter
    @Setter
    private int errorLine;
    
    @Getter
    @Setter
    private List<String> warningCodes;
    
    @Getter
    @Setter
    private List<String> warningMessages;

    @Getter
    @Setter
    private List<Object> result;    
    
    @Getter
    @Setter
    private boolean resultSet;

    @Getter
    @Setter
    private ResultSet generatedKey;
    
    @Getter
    @Setter
    private Object mainResultSet;

    public QueryResult(double executionDuration, double startDuration, double endDuration, String query, List<Object> result) {
        this.executionDuration = executionDuration;
        this.startDuration = startDuration;
        this.endDuration = endDuration;
        this.query = query;
        this.result = result;
    }

    public void addWarningCode(String warningCode) {
        if (warningCodes == null) {
            warningCodes = new ArrayList<>();
        }
        
        warningCodes.add(warningCode);
    }
    
    public void addWarningMessage(String warningMessage) {
        if (warningMessages == null) {
            warningMessages = new ArrayList<>();
        }
        
        warningMessages.add(warningMessage);
    }
    
    public double getTransferTime() {
        return Math.max(0, (endDuration - executionDuration) / 1000f);
    }

    public double getExecutionTime() {
        return Math.max(0, (executionDuration - startDuration) / 1000f);
    }

    public double getResponseTime() {
        return Math.max(0, (endDuration - startDuration) / 1000f);
    }

    public void addResult(Object value) {
        if (result == null) {
            result = new ArrayList<>();
        }
        
        result.add(value);
    }
}
