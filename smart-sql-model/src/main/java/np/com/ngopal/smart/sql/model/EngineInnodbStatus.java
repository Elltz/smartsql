package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EngineInnodbStatus extends BaseModel {

    private String name;
    private Double value;
    private Double difference;
    
    
    public EngineInnodbStatus copyValue() {
        return new EngineInnodbStatus(name, value, difference);
    }
}