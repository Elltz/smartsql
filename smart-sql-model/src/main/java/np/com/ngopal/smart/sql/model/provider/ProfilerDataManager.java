package np.com.ngopal.smart.sql.model.provider;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.DigestQuery;
import np.com.ngopal.smart.sql.model.DigestText;
import np.com.ngopal.smart.sql.model.EngineInnodbStatus;
import np.com.ngopal.smart.sql.model.GlobalProcessList;
import np.com.ngopal.smart.sql.model.GlobalStatusStart;
import np.com.ngopal.smart.sql.model.GlobalVariables;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.SlaveStatus;
import np.com.ngopal.smart.sql.model.StatementDigest;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public abstract class ProfilerDataManager {

    
    private boolean needCache = false;
    
    public static final int EMPTY_DELAY__EIS = 90000;
    public static final int EMPTY_DELAY__SS = 30000;
    public static final int EMPTY_DELAY__GS = 10000;
    
    
    private final SimpleDateFormat slowFormat = new SimpleDateFormat("yyMMddHHmmsss");
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHH");
    private final SimpleDateFormat gsDateFormat = new SimpleDateFormat("ddHHmmss");
    private final SimpleDateFormat fullDateFormat = new SimpleDateFormat("yyMMddHHmmss");
    private final SimpleDateFormat sdFormat = new SimpleDateFormat("yyMMdd");
    
    private static final String TABLE_PREFIX__GLOBALSTATUS_START = "GS_START_";
    private static final String TABLE_PREFIX__GLOBALSTATUS = "GS_";
    private static final String TABLE_PREFIX__GLOBALVARIABLES = "GLOBALVARIABLES_";
    private static final String TABLE_PREFIX__SLOW_QUERY = "SQ_";
    private static final String TABLE_PREFIX__SLAVE_STATUS = "SS_";
    private static final String TABLE_PREFIX__ENGINE_INNODB_STATUS = "EIS_";
    
    private static final String TABLE_PREFIX__STATEMENT_DIGEST = "SD_";
    
    private final Map<String, String> createdGSStartTableMap = new HashMap<>();
    
    
    private Map<String, StorageProvider> storages = new HashMap<>();
    protected StorageProvider storage;
    
    public ProfilerDataManager() {
    }
    
    public void addStorageProvider(String key, StorageProvider storage) {
        storages.put(key, storage);
    }
    
    public StorageProvider activateStorage(String key) {
        this.storage = storages.get(key);
        return storage;
    }
    
    public String getStorageType() {
        return storage.getProviderType();
    }

    protected abstract Double convertIfNan(Double v);
    
    protected abstract String makeQueryTemplate(String query) throws Exception;
    protected abstract Map<String, String> getTableNamesFromQuery(String query);
    
    
    protected abstract String queryDropTable();
    
    protected abstract String querySelectGsTables();
    protected abstract String queryRemoveFromGsTables();
    
    protected abstract String querySelectSdTables();
    protected abstract String queryRemoveFromSdTables();
    protected abstract String queryFindSdTables();
    protected abstract String queryCreateSdTable();
    protected abstract String queryInsertSdTables();
    
    protected abstract String querySelectSsTables();
    protected abstract String queryRemoveFromSsTables();
    
    protected abstract String querySelectEisTables();
    protected abstract String queryRemoveFromEisTables();
    
    protected abstract String querySelectSqTables();
    protected abstract String queryRemoveFromSqTables();
    protected abstract String queryFindSqTables();
    protected abstract String queryCreateSqTable();
    protected abstract String queryInsertSqTables();
    
    protected abstract String queryLoadPQueryById();
    protected abstract String queryDeleteGVByConnectionId();
    
    protected abstract String queryRemoveGsStart();
    protected abstract String queryRemoveGv();
    
    protected abstract String queryFindGvTablesByTableName();
    protected abstract String queryCreateGvTable();
    protected abstract String queryInsertGvTables();
    
    protected abstract String queryFindGsStartTablesByTableName();
    protected abstract String queryCreateGsStartTable();
    protected abstract String queryInsertGsStartTables();
    
    protected abstract String queryFindGsTablesByTableName();
    protected abstract String queryCreateGsTable();
    protected abstract String queryInsertGsTable();
    
    protected abstract String queryUpdateGsStartTables();
    protected abstract String queryUpdateStartTimeGsStartTables();
    
    protected abstract String querySelectAll();
    
    protected abstract String queryFindSsTablesByTableName();
    protected abstract String queryCreateSsTable();
    protected abstract String queryInsertSsTable();
    
    protected abstract String queryFindEisTablesByTableName();
    protected abstract String queryCreateEisTable();
    protected abstract String queryInsertEisTable();
    
    protected abstract String queryInsertGs();
    protected abstract String queryInsertEis();
    protected abstract String queryInsertSs();
    protected abstract String queryInsertGsStart();
    
    protected abstract String queryLoadPqDb();
    protected abstract String queryInsertPqDb();
    protected abstract String queryInsertPqTable();
    protected abstract String queryLoadPqTable();
    protected abstract String queryLoadPQueryIds();
    protected abstract String queryLoadPQueryTemplateId();
    protected abstract String queryInsertPQueryTemplate();
    protected abstract String queryInsertPQuery();
    protected abstract String queryLoadTemplateTableRelation();
    protected abstract String queryInsertTemplateTableRelation();
    protected abstract String queryLoadPqUserHost();
    protected abstract String queryInsertPqUserHost();
    protected abstract String queryLoadPqState();
    protected abstract String queryInsertPqState();
    protected abstract String queryLoadPQueryStats();
    protected abstract String queryInsertPQueryStats();
    protected abstract String queryUpdatePQueryStats();
    protected abstract String queryLoadPqStateStat();
    protected abstract String queryUpdatePqStateStat();
    protected abstract String queryInsertPqStateStat();
            
    protected abstract String queryLoadDigestText(Integer connectionId, Integer schemaId, String digest);
    protected abstract String queryInsertDigestText();
    protected abstract String queryInsertDigestQuery();
    
    protected abstract String queryInsertSd();    
    protected abstract String querySelectGs();
    protected abstract String querySelectEis();
    protected abstract String querySelectSs();
    
    protected abstract String queryInsertGv();
    protected abstract String queryInsertSqTable();
    
    
    
    
    
    public void clearOldGSData(ProfilerSettings settings)
    {
        final Date currentDate = new Date();

        synchronized (storage) {
            storage.beginTransaction();
            try {
                removeOldTables(
                    settings,
                    querySelectGsTables(), 
                    queryRemoveFromGsTables(), 
                    queryDropTable(),
                    currentDate);

                storage.commitTransaction();
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
                storage.rollbackTransaction();
            }
        }
    }
    
    public void clearOldSDData(ProfilerSettings settings)
    {
        final Date currentDate = new Date();
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                removeOldTables(
                    settings,
                    querySelectSdTables(), 
                    queryRemoveFromSdTables(), 
                    queryDropTable(),
                    currentDate);

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void clearOldSSData(ProfilerSettings settings)
    {
        final Date currentDate = new Date();
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                removeOldTables(
                    settings,
                    querySelectSsTables(), 
                    queryRemoveFromSsTables(), 
                    queryDropTable(),
                    currentDate);

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void clearOldEISData(ProfilerSettings settings)
    {
        final Date currentDate = new Date();
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                removeOldTables(
                    settings,
                    querySelectEisTables(), 
                    queryRemoveFromEisTables(), 
                    queryDropTable(),
                    currentDate);

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void clearOldSlowData(Date currentDate)
    {        
        synchronized (storage) {
            
            try {
                removeOldSlowQueries(
                    querySelectSqTables(), 
                    queryDropTable(),
                    queryRemoveFromSqTables(), 
                    currentDate);

            } catch (Throwable th) {
            }
        }
    }
    
    public void prepareStatementDigestTables(String serverHashCode, Date date) {
        prepareStatementDigestTable(
            TABLE_PREFIX__STATEMENT_DIGEST, 
            serverHashCode, 
            date,
            queryFindSdTables(), 
            queryCreateSdTable(), 
            queryInsertSdTables());
    }
    
    public void prepareSlowQueryTables(String serverHashCode, Date date) {
        prepareSlowQueryTable(
            TABLE_PREFIX__SLOW_QUERY, 
            serverHashCode, 
            date,
            queryFindSqTables(), 
            queryCreateSqTable(), 
            queryInsertSqTables());
    }
    
    public void executeUpdate(String query) {
        synchronized (storage) {
            try {
                storage.beginTransaction();
                
                storage.executeQuery(query);
                
                storage.commitTransaction();
                
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    
    public List<Object[]> getProfilerTopData(String query, long time) {
        synchronized (storage) {            
            return storage.getResultList(String.format(query, time / 1000, time));
        }
    }
    
    public List<Object[]> getProfilerAnalyzerData(String query, long time) {
        synchronized (storage) {            
            return storage.getResultList(String.format(query, time, time));
        }
    }
    
    public List<Object[]> getProfilerTopData(String query, long time, String formatedParams) {
        synchronized (storage) {            
            return storage.getResultList(String.format(query, time, time / 1000, time, formatedParams));
        }
    }
    
    public List<Object[]> getSlowQueryData(String query) {
        synchronized (storage) {            
            return storage.getResultList(query);
        }
    }
    
    public String getQueryById(Integer queryId) {
        synchronized (storage) {            
            return (String)storage.getSingleResult(queryLoadPQueryById(), queryId);
        }
    }
    
    
    public void deleteByConnectionId(Long connectionId) {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                storage.executeQuery(queryDeleteGVByConnectionId(), connectionId);

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void clearGlobalStatusStartTable(String serverHashCode) {
        synchronized (storage) {
            String tableName = makeTableName(TABLE_PREFIX__GLOBALSTATUS_START, serverHashCode, null);

            storage.beginTransaction();
            try {
                storage.executeQuery(String.format(queryRemoveGsStart(), tableName));
                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void clearGlobalVariablesTable(String serverHashCode) {
        synchronized (storage) {
            String tableName = makeTableName(TABLE_PREFIX__GLOBALVARIABLES, serverHashCode, null);
            storage.beginTransaction();
            try {
                storage.executeQuery(String.format(queryRemoveGv(), tableName));
                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void prepareGlobalVariablesTables(String serverHashCode) {
        prepareTable(TABLE_PREFIX__GLOBALVARIABLES, 
            serverHashCode, 
            null, 
            queryFindGvTablesByTableName(),
            queryCreateGvTable(),
            queryInsertGvTables());
        
        clearGlobalVariablesTable(serverHashCode);
    }
    
    public Map<String, GlobalStatusStart> prepareGlobalStatusTables(String serverHashCode, Date date) {
        boolean created = prepareGSStartTable(
            TABLE_PREFIX__GLOBALSTATUS_START, 
            serverHashCode, 
            queryFindGsStartTablesByTableName(), 
            queryCreateGsStartTable(), 
            queryInsertGsStartTables());
        
        prepareTable(
            TABLE_PREFIX__GLOBALSTATUS, 
            serverHashCode, 
            date, 
            queryFindGsTablesByTableName(), 
            queryCreateGsTable(),
            queryInsertGsTable());
        
        Map<String, GlobalStatusStart> map = new HashMap<>(); 
        if (!created) {
            
            // if exist need select all statuses
            List<GlobalStatusStart> list = getAllGlobalStatusStart(serverHashCode);
            for (GlobalStatusStart gs: list) {
                map.put(gs.getName(), gs);
            }
        }
        
        return map;
    }
        
    public void prepareSlaveStatusTables(String serverHashCode, Date date) {
       
        prepareTable(
            TABLE_PREFIX__SLAVE_STATUS, 
            serverHashCode, 
            date, 
            queryFindSsTablesByTableName(), 
            queryCreateSsTable(), 
            queryInsertSsTable());
    }
    
    public void prepareEngineInnodbStatusTables(String serverHashCode, Date date) {
       
        prepareTable(
            TABLE_PREFIX__ENGINE_INNODB_STATUS, 
            serverHashCode, 
            date, 
            queryFindEisTablesByTableName(), 
            queryCreateEisTable(), 
            queryInsertEisTable());
    }
    
    
    public List<GlobalStatusStart> getAllGlobalStatusStart(String serverHashCode) {
        
        synchronized (storage) {
            String tableName = makeTableName(TABLE_PREFIX__GLOBALSTATUS_START, serverHashCode, null);

            List<GlobalStatusStart> list = new ArrayList<>();

            List<Object[]> result = storage.getResultList(String.format(querySelectAll(), tableName));
            if (result != null) {
                for (Object[] obj: result) {
                    GlobalStatusStart gs = new GlobalStatusStart();
                    gs.setId(((Number) obj[0]).longValue());
                    gs.setName((String) obj[1]);
                    gs.setValue(((Long) obj[2]).doubleValue());
                    gs.setDifferece(0.0);

                    list.add(gs);
                }
            }
            return list;
        }
    }
    
    public void insertGSDistinction(Long gsId, String name, Double gsValue, String serverHashCode, Date date) {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                storage.executeQuery(
                    String.format(queryInsertGs(), makeTableName(TABLE_PREFIX__GLOBALSTATUS, serverHashCode, date)), 
                    Integer.valueOf(gsDateFormat.format(date)),
                    gsId,
                    name,
                    gsValue
                );
                
                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }                
        }
    }
    
    public void insertEISDistinction(String name, Double value, String serverHashCode, Date date) {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                storage.executeQuery(
                    String.format(queryInsertEis(), makeTableName(TABLE_PREFIX__ENGINE_INNODB_STATUS, serverHashCode, date)),    
                    Integer.valueOf(gsDateFormat.format(date)),
                    name,
                    convertIfNan(value)
                );

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }                  
        }
    }
    
    public void insertSSDistinction(SlaveStatus ss, String serverHashCode, Date date) {
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                storage.executeQuery(
                    String.format(queryInsertSs(), makeTableName(TABLE_PREFIX__SLAVE_STATUS, serverHashCode, date)),    
                    Integer.valueOf(gsDateFormat.format(date)),
                    ss.getSlave_IO_State(),
                    ss.getMaster_Host(),
                    ss.getMaster_User(),
                    ss.getMaster_Port(),
                    ss.getConnect_Retry(),
                    ss.getMaster_Log_File(),
                    ss.getRead_Master_Log_Pos(),
                    ss.getRelay_Log_File(),
                    ss.getRelay_Log_Pos(),
                    ss.getRelay_Master_Log_File(),
                    ss.getSlave_IO_Running(),
                    ss.getSlave_SQL_Running(),
                    ss.getReplicate_Do_DB(),
                    ss.getReplicate_Ignore_DB(),
                    ss.getReplicate_Do_Table(),
                    ss.getReplicate_Wild_Do_Table(),
                    ss.getReplicate_Wild_Ignore_Table(),
                    ss.getLast_Errno(),
                    ss.getLast_Error(),
                    ss.getSkip_Counter(),
                    ss.getExec_Master_Log_Pos(),
                    ss.getRelay_Log_Space(),
                    ss.getUntil_Condition(),
                    ss.getUntil_Log_File(),
                    ss.getUntil_Log_Pos(),
                    ss.getMaster_SSL_Allowed(),
                    ss.getMaster_SSL_CA_File(),
                    ss.getMaster_SSL_CA_Path(),
                    ss.getMaster_SSL_Cert(),
                    ss.getMaster_SSL_Cipher(),
                    ss.getMaster_SSL_Key(),
                    ss.getSeconds_Behind_Master(),
                    ss.getMaster_SSL_Verify_Server_Cert(),
                    ss.getLast_IO_Errno(),
                    ss.getLast_IO_Error(),
                    ss.getLast_SQL_Errno(),
                    ss.getLast_SQL_Error(),
                    ss.getReplicate_Ignore_Server_Ids(),
                    ss.getMaster_Server_Id(),
                    ss.getMaster_UUID(),
                    ss.getMaster_Info_File(),
                    ss.getSQL_Delay(),
                    ss.getSQL_Remaining_Delay(),
                    ss.getSlave_SQL_Running_State(),
                    ss.getMaster_Retry_Count(),
                    ss.getMaster_Bind(),
                    ss.getLast_IO_Error_Timestamp(),
                    ss.getLast_SQL_Error_Timestamp(),
                    ss.getMaster_SSL_Crl(),
                    ss.getMaster_SSL_Crlpath(),
                    ss.getRetrieved_Gtid_Set(),
                    ss.getExecuted_Gtid_Set(),
                    ss.getAuto_Position()
                );

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }           
        }
    }
    
    private Long loadDigestText(Integer connectionId, Integer schemaId, String digest) {
        return (Long) storage.getSingleResult(queryLoadDigestText(connectionId, schemaId, digest));
    }
    
    public void insertSD(StatementDigest sd, String serverHashCode, Date date) {
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                
                // check schema
                Integer schemaId = loadPQDB(sd.getSchemaName());
                if (schemaId == null && sd.getSchemaName() != null) {                
                    // if not exists - insert new
                    insertPQDB(sd.getSchemaName());

                    // load generated id
                    schemaId = loadPQDB(sd.getSchemaName());
                }
                
                Long digestId = null;
                // query can return 0 in digest text - we must ignore it
                if (sd.getDigestText() != null && !"0".equals(sd.getDigestText())) {
                    
                    digestId = loadDigestText(sd.getConnectionId(), schemaId, sd.getDigest());
                    
                    if (digestId == null) {
                        // insert new degist
                        storage.executeQuery(queryInsertDigestText(), sd.getConnectionId(), schemaId, sd.getDigest(), sd.getDigestText());
                    }
                }
                
                if (digestId == null) {
                    digestId = loadDigestText(sd.getConnectionId(), schemaId, sd.getDigest());
                }
                
                storage.executeQuery(
                    String.format(queryInsertSd(), makeSDTableName(TABLE_PREFIX__STATEMENT_DIGEST, serverHashCode, date)), 
                    digestId,
                    sd.getCountStarDiff(),
                    sd.getSumTimerWaitDiff(),
                    sd.getMinTimerWaitDiff(),
                    sd.getAvgTimerWaitDiff(),
                    sd.getMaxTimerWaitDiff(),
                    sd.getSumLockTimeDiff(),
                    sd.getSumErrorsDiff(),
                    sd.getSumWarningsDiff(),
                    sd.getSumRowsAffectedDiff(),
                    sd.getSumRowsSentDiff(),
                    sd.getSumRowsExaminedDiff(),
                    sd.getSumCreatedTmpDiskTablesDiff(),
                    sd.getSumCreatedTmpTablesDiff(),
                    sd.getSumSelectFullJoinDiff(),
                    sd.getSumSelectFullRangeJoinDiff(),
                    sd.getSumSelectRangeDiff(),
                    sd.getSumSelectRangeCheckDiff(),
                    sd.getSumSelectScanDiff(),
                    sd.getSumSortMergePassesDiff(),
                    sd.getSumSortRangeDiff(),
                    sd.getSumSortRowsDiff(),
                    sd.getSumSortScanDiff(),
                    sd.getSumNoIndexUsedDiff(),
                    sd.getSumNoGoodIndexUsedDiff(),
                    sd.getFirstSeen(),
                    sd.getLastSeen()
                );

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }                
        }
    }
    
    public void insertDigestQuery(DigestQuery dq) {
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                
                // check schema
                Integer schemaId = loadPQDB(dq.getSchemaName());
                if (schemaId == null && dq.getSchemaName() != null) {                
                    // if not exists - insert new
                    insertPQDB(dq.getSchemaName());

                    // load generated id
                    schemaId = loadPQDB(dq.getSchemaName());
                }
                
                Long digestId = null;
                // query can return 0 in digest text - we must ignore it
                if (dq.getDigestText() != null && !"0".equals(dq.getDigestText())) {
                    
                    digestId = loadDigestText(dq.getConnectionId(), schemaId, dq.getDigest());
                    
                    if (digestId == null) {
                        // insert new degist
                        storage.executeQuery(
                            queryInsertDigestText(), 
                            dq.getConnectionId(),
                            schemaId,
                            dq.getDigest(),
                            dq.getDigestText()
                        );
                    }
                }
                
                if (digestId == null) {
                    digestId = loadDigestText(dq.getConnectionId(), schemaId, dq.getDigest());
                }
                
                storage.executeQuery(queryInsertDigestQuery(), digestId, dq.getSqlText());

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }                
        }
    }
    
    public void insertDigestText(DigestText dt) {
        
        synchronized (storage) {
            storage.beginTransaction();
            try {
                                
                // check schema
                Integer schemaId = loadPQDB(dt.getSchemaName());
                if (schemaId == null && dt.getSchemaName() != null) {                
                    // if not exists - insert new
                    insertPQDB(dt.getSchemaName());

                    // load generated id
                    schemaId = loadPQDB(dt.getSchemaName());
                }                
                
                Long digestId = loadDigestText(dt.getConnectionId(), schemaId, dt.getDigest());
                        
                if (digestId == null) {
                    storage.executeQuery(
                        queryInsertDigestText(),
                        dt.getConnectionId(),
                        schemaId,
                        dt.getDigest(),
                        dt.getDigestText()
                    );
                }

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }                
        }
    }
    
    public static final Map<String, GlobalStatusStart> EMPTY_STATUSES = new HashMap<>();
    public Map<Long, Map<String, GlobalStatusStart>> selectGSDistinction(String serverHashCode, Date dateFrom, long timeTick, Date nextDate) {
        synchronized (storage) {
            Map<Long, Map<String, GlobalStatusStart>> result = new LinkedHashMap<>();
            
            Integer dataId = Integer.valueOf(dateFormat.format(dateFrom));
            
            List<String> tables = (List<String>)storage.getResultList(querySelectGsTables());
            if (tables != null) {
                
                List<String> tableForUse = new ArrayList<>();                
                for (String table: tables) {
                    
                    String[] ts = table.split("_");
                    // need get only for our connection
                    if (serverHashCode.equals(ts[1])) {
                        
                        Integer id = Integer.valueOf(ts[2]);
                        if (id >= dataId) {
                            tableForUse.add(table);
                        }
                    }
                }
                
                Date lastTime = null;

                EMPTY_STATUSES.clear();
                boolean emptyAdded = true;
                for (String table: tableForUse) {
                    try {
                        String timeKey = table.split("_")[2];
                        List<Object[]> data = storage.getResultList(String.format(querySelectGs(), table));
                        if (data != null) {
                            
                            Map<String, GlobalStatusStart> latest = new HashMap<>();
                            
                            for (Object[] objs: data) {
                                
                                String name = (String) objs[0];
                                Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : null;
                                Integer time = (Integer) objs[2];
                                
                                // 8 = ddHHmmss, but 01122333 = 1122333
                                Date dateTime = fullDateFormat.parse(timeKey + (time.toString().length() != 8 ? time.toString().substring(3) : time.toString().substring(4)));
                                
//                                if (lastTime != null && lastTime.getTime() != dateTime.getTime()) {
//                                    System.out.println("DATE: " + dateTime);
//                                    System.out.println("LAST: " + lastTime);
//                                    System.out.println("DIFF: " + (dateTime.getTime() - lastTime.getTime()));
//                                }
                                
                                // TODO Need check performance, this is just for first time
                                if (lastTime != null && dateTime.getTime() - lastTime.getTime() >= timeTick) {
                                    
                                    Map<String, GlobalStatusStart> m = result.get(lastTime.getTime());
                                    if (m != null) {
                                        emptyAdded = false;                                            
                                        for (String key: m.keySet()) {
                                            EMPTY_STATUSES.put(key, new GlobalStatusStart(key, 0.0, 0.0));
                                        }
                                    }
                                }
                                
                                if (!emptyAdded) {
                                    result.put(lastTime.getTime() + 1000, EMPTY_STATUSES);
                                    result.put(dateTime.getTime() - 1000, EMPTY_STATUSES);
                                    emptyAdded = true;
                                }
                                
                                Map<String, GlobalStatusStart> map = result.get(dateTime.getTime());
                                
//                                if (map == EMPTY_STATUSES) {
//                                    System.out.println("USED EMPTY!!!");
//                                }
                                
                                if (map == null) {
                                    map = new HashMap<>();
                                    
                                    result.put(dateTime.getTime(), map);
                                }
                                
                                GlobalStatusStart old = latest.get(name.toUpperCase());

                                map.put(name.toUpperCase(), new GlobalStatusStart(name, value.doubleValue(), old != null ? value.doubleValue() - old.getValue() : 0.0));
                                
                                latest.put(name.toUpperCase(), new GlobalStatusStart(name, value, 0.0));
                                   
                                
                                lastTime = dateTime;
                            }
                        }
                    } catch (Throwable th) {
                        // its ok - old tables
                        log.info("Error", th);
                    }
                }
                
                if (nextDate != null && lastTime != null && nextDate.getTime() - lastTime.getTime() > EMPTY_DELAY__GS) {
                    result.put(lastTime.getTime() + 1000, EMPTY_STATUSES);
                    result.put(nextDate.getTime() - 1000, EMPTY_STATUSES);
                }
            }
            
            return result;
        }
    }
    
    public Map<Long, Map<String, GlobalStatusStart>> selectLastGS(String serverHashCode, Date lastDateUpdate) {
        synchronized (storage) {
            
            Map<Long, Map<String, GlobalStatusStart>> result = new LinkedHashMap<>();
            
            List<String> tables = (List<String>)storage.getResultList(querySelectGsTables());
            if (tables != null) {
                
                String prevTableForUse = null;
                String tableForUse = null;
                Integer maxId = 0;
                for (String table: tables) {
                    
                    String[] ts = table.split("_");
                    // need get only for our connection
                    if (serverHashCode.equals(ts[1])) {
                        
                        Integer id = Integer.valueOf(ts[2]);
                        if (id > maxId) {
                            prevTableForUse = tableForUse;
                            tableForUse = table;
                            maxId = id;
                        }
                    }
                }
                
                try {
                    if (tableForUse != null) {
                        
                        // we need check last 2 tables (1 hour)
                        List<String> tableNames = new ArrayList<>();
                        tableNames.add(tableForUse);
                        if (prevTableForUse != null) {
                            tableNames.add(prevTableForUse);
                        }
                        
                        Map<String, GlobalStatusStart> latest = new HashMap<>();
                        
                        for (String t: tableNames) {
                            List<Object[]> data = storage.getResultList(String.format(querySelectGs(), t));

                            if (data != null) {

                                String timeKey = t.split("_")[2];
                                for (Object[] objs: data) {

                                    String name = (String) objs[0];
                                    Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : null;
                                    Integer time = (Integer) objs[2];

                                    Date dateTime = fullDateFormat.parse(timeKey + (time.toString().length() != 8 ? time.toString().substring(3) : time.toString().substring(4)));
                                    if (dateTime.after(lastDateUpdate) || dateTime.compareTo(lastDateUpdate) == 0) {

                                        Map<String, GlobalStatusStart> map = result.get(dateTime.getTime());
                                        if (map == null) {
                                            map = new HashMap<>();
                                            result.put(dateTime.getTime(), map);
                                        }
                                            
                                        GlobalStatusStart old = latest.get(name.toUpperCase());
                                        GlobalStatusStart gs = map.get(name.toUpperCase());
                                        if (gs == null) {
                                            gs = new GlobalStatusStart(name, value, old != null ? value - old.getValue() : 0.0);
                                        } else {
                                            gs.setDifferece(old != null ? value - old.getValue() : 0.0);
                                            gs.setValue(value);
                                        }

                                        map.put(name, gs);
                                    }
                                    
                                    latest.put(name.toUpperCase(), new GlobalStatusStart(name, value, 0.0));
                                }
                            }
                        }
                    }
                } catch (Throwable th) {
                    // its ok - old tables
                    log.info("Error", th);
                }
            }
            
            return result;
        }
    }
    
    public Map<Long, Map<String, EngineInnodbStatus>> selectLastEIS(String serverHashCode, Date lastDateUpdate) {
        synchronized (storage) {
            
            Map<Long, Map<String, EngineInnodbStatus>> result = new LinkedHashMap<>();
            
            List<String> tables = (List<String>)storage.getResultList(querySelectEisTables());
            if (tables != null) {
                
                String prevTableForUse = null;
                String tableForUse = null;
                Integer maxId = 0;
                for (String table: tables) {
                    
                    String[] ts = table.split("_");
                    // need get only for our connection
                    if (serverHashCode.equals(ts[1])) {
                        
                        Integer id = Integer.valueOf(ts[2]);
                        if (id > maxId) {
                            prevTableForUse = tableForUse;
                            tableForUse = table;
                            maxId = id;
                        }
                    }
                }
                
                try {
                    if (tableForUse != null) {
                        
                        // we need check last 2 tables (1 hour)
                        List<String> tableNames = new ArrayList<>();
                        tableNames.add(tableForUse);
                        if (prevTableForUse != null) {
                            tableNames.add(prevTableForUse);
                        }
                        
                        Map<String, EngineInnodbStatus> latest = new HashMap<>();
                        
                        for (String t: tableNames) {
                            List<Object[]> data = storage.getResultList(String.format(querySelectEis(), t));

                            if (data != null) {

                                String timeKey = t.split("_")[2];
                                for (Object[] objs: data) {
                                    
                                    String name = (String) objs[0];
                                    Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : 0.0;
                                    if (Double.isNaN(value)) {
                                        value = 0.0;
                                    }

//                                    value = new BigDecimal(value).setScale(0, RoundingMode.HALF_UP).doubleValue();
                                    Integer time = (Integer) objs[2];

                                    Date dateTime = fullDateFormat.parse(timeKey + (time.toString().length() != 8 ? time.toString().substring(3) : time.toString().substring(4)));
                                    if (dateTime.after(lastDateUpdate) || dateTime.compareTo(lastDateUpdate) == 0) {

                                        Map<String, EngineInnodbStatus> map = result.get(dateTime.getTime());
                                        if (map == null) {
                                            map = new HashMap<>();
                                            result.put(dateTime.getTime(), map);
                                        }

                                        EngineInnodbStatus old = latest.get(name.toUpperCase());
                                        EngineInnodbStatus gs = map.get(name.toUpperCase());
                                        if (gs == null) {
                                            gs = new EngineInnodbStatus(name, value, old != null ? value - old.getValue() : 0.0);
                                        } else {
                                            gs.setValue(value);
                                        }

                                        map.put(name, gs);
                                    }
                                    
                                    latest.put(name, new EngineInnodbStatus(name, value, 0.0));
                                }
                            }
                        }
                    }
                } catch (Throwable th) {
                    // its ok - old tables
                    log.info("Error", th);
                }
            }
            
            return result;
        }
    }
    
    public Map<Long, SlaveStatus> selectLastSS(String serverHashCode, Date lastDateUpdate) {
        
        synchronized (storage) {
            
            Map<Long, SlaveStatus> slaveStatuses = new LinkedHashMap<>();
            
            List<String> tables = (List<String>)storage.getResultList(querySelectSsTables());
            if (tables != null) {
                
                String prevTableForUse = null;
                String tableForUse = null;
                Integer maxId = 0;
                for (String table: tables) {
                    
                    String[] ts = table.split("_");
                    // need get only for our connection
                    if (serverHashCode.equals(ts[1])) {
                        
                        Integer id = Integer.valueOf(ts[2]);
                        if (id > maxId) {
                            prevTableForUse = tableForUse;
                            tableForUse = table;
                            maxId = id;
                        }
                    }
                }
                
                if (tableForUse != null) {
                    
                    // we need check last 2 tables (1 hour)
                    List<String> tableNames = new ArrayList<>();
                    tableNames.add(tableForUse);
                    if (prevTableForUse != null) {
                        tableNames.add(prevTableForUse);
                    }
                    
                    for (String t: tableNames) {
                        try {
                            String timeKey = t.split("_")[2];
                            List<Object[]> data = storage.getResultList(String.format(querySelectSs(), t));
                            if (data != null) {

                                for (Object[] objs: data) {

                                    Integer time = (Integer) objs[0];

                                    // 8 = ddHHmmss, but 01122333 = 1122333
                                    Date dateTime = fullDateFormat.parse(timeKey + (time.toString().length() != 8 ? time.toString().substring(3) : time.toString().substring(4)));

                                    if (dateTime.after(lastDateUpdate) || dateTime.compareTo(lastDateUpdate) == 0) {                                

                                        slaveStatuses.put(dateTime.getTime(), new SlaveStatus(
                                            (String) objs[1],
                                            (String) objs[2],
                                            (String) objs[3],
                                            (Integer) objs[4],
                                            (Integer) objs[5],
                                            (String) objs[6],
                                            (Integer) objs[7],
                                            (String) objs[8],
                                            (Integer) objs[9],
                                            (String) objs[10],
                                            (String) objs[11],
                                            (String) objs[12],
                                            (String) objs[13],
                                            (String) objs[14],
                                            (String) objs[15],
                                            (String) objs[16],
                                            (String) objs[17],
                                            (String) objs[18],
                                            (Integer) objs[19],
                                            (String) objs[20],
                                            (Integer) objs[21],
                                            (Integer) objs[22],
                                            (Integer) objs[23],
                                            (String) objs[24],
                                            (String) objs[25],
                                            (Integer) objs[26],
                                            (String) objs[27],
                                            (String) objs[28],
                                            (String) objs[29],
                                            (String) objs[30],
                                            (String) objs[31],
                                            (String) objs[32],
                                            (Integer) objs[33],
                                            (String) objs[34],
                                            (Integer) objs[35],
                                            (String) objs[36],
                                            (Integer) objs[37],
                                            (String) objs[38],
                                            (String) objs[39],
                                            (String) objs[40],
                                            (String) objs[41],
                                            (String) objs[42],
                                            (Integer) objs[43],
                                            (Integer) objs[44],
                                            (String) objs[45],
                                            (Integer) objs[46],
                                            (String) objs[47],
                                            (String) objs[48],
                                            (String) objs[49],
                                            (String) objs[50],
                                            (String) objs[51],
                                            (String) objs[52],
                                            (String) objs[53],
                                            (Integer) objs[54]
                                        ));
                                    }
                                }
                            }
                        } catch (Throwable th) {
                            // its ok - old tables
                            log.info("Error", th);
                        }
                    }
                }
            }
            
            return slaveStatuses;
        }
    }
    
    public static final Map<String, EngineInnodbStatus> EMPTY_ENGINE_INNODB_STATUSES = new HashMap<>();
    public Map<Long, Map<String, EngineInnodbStatus>> selectEISDistinction(String serverHashCode, Date dateFrom, long timeTick, Date nextDate) {
        synchronized (storage) {
            Map<Long, Map<String, EngineInnodbStatus>> result = new LinkedHashMap<>();
            
            Integer dataId = Integer.valueOf(dateFormat.format(dateFrom));
            
            List<String> tables = (List<String>)storage.getResultList(querySelectEisTables());
            if (tables != null) {
                
                List<String> tableForUse = new ArrayList<>();                
                for (String table: tables) {
                    
                    String[] ts = table.split("_");
                    // need get only for our connection
                    if (serverHashCode.equals(ts[1])) {
                        
                        Integer id = Integer.valueOf(ts[2]);
                        if (id >= dataId) {
                            tableForUse.add(table);
                        }
                    }
                }
                
                Date lastTime = null;

                Map<String, EngineInnodbStatus> latest = new HashMap<>();
                
                EMPTY_ENGINE_INNODB_STATUSES.clear();
                boolean emptyAdded = true;
                for (String table: tableForUse) {
                    try {
                        String timeKey = table.split("_")[2];
                        List<Object[]> data = storage.getResultList(String.format(querySelectEis(), table));
                        if (data != null) {
                            
                            for (Object[] objs: data) {
                                
                                String name = (String) objs[0];
                                Double value = objs[1] != null ? ((Number) objs[1]).doubleValue() : null;
                                if (Double.isNaN(value)) {
                                    value = 0.0;
                                }

                                Integer time = (Integer) objs[2];
                                
                                // 8 = ddHHmmss, but 01122333 = 1122333
                                Date dateTime = fullDateFormat.parse(timeKey + (time.toString().length() != 8 ? time.toString().substring(3) : time.toString().substring(4)));
                                
                                // TODO Need check performance, this is just for first time
                                if (lastTime != null && dateTime.getTime() - lastTime.getTime() >= timeTick) {
                                    
                                    Map<String, EngineInnodbStatus> m = result.get(lastTime.getTime());
                                    if (m != null) {
                                        emptyAdded = false;                                            
                                        for (String key: m.keySet()) {
                                            EMPTY_ENGINE_INNODB_STATUSES.put(key, new EngineInnodbStatus(key, 0.0, 0.0));
                                        }
                                    }
                                }
                                
                                if (!emptyAdded) {
                                    result.put(lastTime.getTime() + 1000, EMPTY_ENGINE_INNODB_STATUSES);
                                    result.put(dateTime.getTime() - 1000, EMPTY_ENGINE_INNODB_STATUSES);
                                    emptyAdded = true;
                                }
                                
                                Map<String, EngineInnodbStatus> map = result.get(dateTime.getTime());
                                
//                                if (map == EMPTY_STATUSES) {
//                                    System.out.println("USED EMPTY!!!");
//                                }
                                
                                if (map == null) {
                                    map = new HashMap<>();
                                    
                                    result.put(dateTime.getTime(), map);
                                }
                                
                                EngineInnodbStatus old = latest.get(name);
                                map.put(name, new EngineInnodbStatus(name, value.doubleValue(), old != null ? value.doubleValue() - old.getValue() : 0.0));
                                latest.put(name, new EngineInnodbStatus(name, value, 0.0));
                                        
                                lastTime = dateTime;
                            }
                        }
                    } catch (Throwable th) {
                        // its ok - old tables
                        log.info("Error", th);
                    }
                }
                
                if (nextDate != null && lastTime != null && nextDate.getTime() - lastTime.getTime() > EMPTY_DELAY__EIS) {
                    result.put(lastTime.getTime() + 1000, EMPTY_ENGINE_INNODB_STATUSES);
                    result.put(nextDate.getTime() - 1000, EMPTY_ENGINE_INNODB_STATUSES);
                }
            }
            
            return result;
        }
    }
    
    public static final SlaveStatus EMPTY_SLAVE_STATUS = SlaveStatus.empty();
    public Map<Long, SlaveStatus> selectSSDistinction(String serverHashCode, Date dateFrom, long timeTick, Date nextDate) {
        synchronized (storage) {
            Map<Long, SlaveStatus> result = new LinkedHashMap<>();
            
            Integer dataId = Integer.valueOf(dateFormat.format(dateFrom));
            
            List<String> tables = (List<String>)storage.getResultList(querySelectSsTables());
            if (tables != null) {
                
                List<String> tableForUse = new ArrayList<>();                
                for (String table: tables) {
                    
                    String[] ts = table.split("_");
                    // need get only for our connection
                    if (serverHashCode.equals(ts[1])) {
                        
                        Integer id = Integer.valueOf(ts[2]);
                        if (id >= dataId) {
                            tableForUse.add(table);
                        }
                    }
                }
                
                Date lastTime = null;
                boolean emptyAdded = true;
                for (String table: tableForUse) {
                    try {
                        String timeKey = table.split("_")[2];
                        List<Object[]> data = storage.getResultList(String.format(querySelectSs(), table));
                        if (data != null) {
                            
                            for (Object[] objs: data) {
                                
                                Integer time = (Integer) objs[0];
                                
                                SlaveStatus ss = new SlaveStatus(
                                    (String) objs[1],
                                    (String) objs[2],
                                    (String) objs[3],
                                    (Integer) objs[4],
                                    (Integer) objs[5],
                                    (String) objs[6],
                                    (Integer) objs[7],
                                    (String) objs[8],
                                    (Integer) objs[9],
                                    (String) objs[10],
                                    (String) objs[11],
                                    (String) objs[12],
                                    (String) objs[13],
                                    (String) objs[14],
                                    (String) objs[15],
                                    (String) objs[16],
                                    (String) objs[17],
                                    (String) objs[18],
                                    (Integer) objs[19],
                                    (String) objs[20],
                                    (Integer) objs[21],
                                    (Integer) objs[22],
                                    (Integer) objs[23],
                                    (String) objs[24],
                                    (String) objs[25],
                                    (Integer) objs[26],
                                    (String) objs[27],
                                    (String) objs[28],
                                    (String) objs[29],
                                    (String) objs[30],
                                    (String) objs[31],
                                    (String) objs[32],
                                    (Integer) objs[33],
                                    (String) objs[34],
                                    (Integer) objs[35],
                                    (String) objs[36],
                                    (Integer) objs[37],
                                    (String) objs[38],
                                    (String) objs[39],
                                    (String) objs[40],
                                    (String) objs[41],
                                    (String) objs[42],
                                    (Integer) objs[43],
                                    (Integer) objs[44],
                                    (String) objs[45],
                                    (Integer) objs[46],
                                    (String) objs[47],
                                    (String) objs[48],
                                    (String) objs[49],
                                    (String) objs[50],
                                    (String) objs[51],
                                    (String) objs[52],
                                    (String) objs[53],
                                    (Integer) objs[54]
                                );
                                
                                
                                // 8 = ddHHmmss, but 01122333 = 1122333
                                Date dateTime = fullDateFormat.parse(timeKey + (time.toString().length() != 8 ? time.toString().substring(3) : time.toString().substring(4)));

                                // TODO Need check performance, this is just for first time
                                if (lastTime != null && dateTime.getTime() - lastTime.getTime() >= timeTick) {
                                    emptyAdded = false;
                                }
                                
                                if (!emptyAdded) {
                                    result.put(lastTime.getTime() + 1000, EMPTY_SLAVE_STATUS);
                                    result.put(dateTime.getTime() - 1000, EMPTY_SLAVE_STATUS);
                                    emptyAdded = true;
                                }
                                
                                result.put(dateTime.getTime(), ss);
                                
                                
                                lastTime = dateTime;
                            }
                        }
                    } catch (Throwable th) {
                        // its ok - old tables
                        log.info("Error", th);
                    }
                }
                
                if (nextDate != null && lastTime != null && nextDate.getTime() - lastTime.getTime() > EMPTY_DELAY__SS) {
                    result.put(lastTime.getTime() + 1000, EMPTY_SLAVE_STATUS);
                    result.put(nextDate.getTime() - 1000, EMPTY_SLAVE_STATUS);
                }
            }
            
            return result;
        }
    }
    
    public void save(GlobalVariables gv, String serverHashCode)
    {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                
                storage.executeQuery(
                    String.format(queryInsertGv(), makeTableName(TABLE_PREFIX__GLOBALVARIABLES, serverHashCode, null)), 
                    gv.getVariableName(),
                    gv.getValues()
                );

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void save(EngineInnodbStatus eis, String serverHashCode)
    {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                
                storage.executeQuery(
                    String.format(queryInsertEis(), makeTableName(TABLE_PREFIX__ENGINE_INNODB_STATUS, serverHashCode, null)),
                    eis.getName(),
                    eis.getValue()
                );

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
            
    public void save(GlobalStatusStart status, String serverHashCode)
    {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                storage.executeQuery(
                    String.format(queryInsertGsStart(), makeTableName(TABLE_PREFIX__GLOBALSTATUS_START, serverHashCode, null)), 
                    status.getName(),
                    status.getValue()
                );

                storage.commitTransaction();
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    public void save(GlobalProcessList processList)
    {        
        save(processList, null, false, null, null, null, null);
    }
    
    public void startCaching() {
        synchronized (storage) {
            needCache = true;
        }
    }
    
    public void stopCaching() {
        synchronized (storage) {
            cacheTemplates.clear();
            cacheCrcs.clear();
            cacheQueryIds.clear();
            cacheQueryTemplateIds.clear();
            cacheDBIds.clear();
            cacheUserhostIds.clear();
            cacheStatesIds.clear();
            cacheTableIds.clear();
            cacheRelations.clear();

            needCache = false;
        }
    }
    
    
    private final Map<String, String> cacheTemplates = new HashMap<>();
    private final Map<String, Long> cacheCrcs = new HashMap<>();
    private final Map<Long, Object[]> cacheQueryIds = new HashMap<>();
    private final Map<Long, Integer> cacheQueryTemplateIds = new HashMap<>();
    private final Map<String, Integer> cacheDBIds = new HashMap<>();
    private final Map<String, Integer> cacheUserhostIds = new HashMap<>();
    private final Map<String, Integer> cacheStatesIds = new HashMap<>();
    private final Map<Integer, Map<String, Integer>> cacheTableIds = new HashMap<>();
    private final Map<Integer, Map<Integer, Boolean>> cacheRelations = new HashMap<>();
    
    
    public void save(GlobalProcessList processList, Date date, boolean slow, String serverHashCode, BigDecimal lockTime, Integer rowsSent, Integer rowsExamined)
    {
        synchronized (storage) {
            storage.beginTransaction();
            try {
                String querySource = processList.getQuerytemp();
                String queryTemplate = processList.getQuerytemp();                
                try
                {
                    String str = null;
                    if (needCache) {
                        str = cacheTemplates.get(querySource);
                    }
//                    insert into `users` (`country_id`, `created_at`, `email`, `gender`, `grade`, `last_login`, `name`, `password`, `password_version`, `phone`, `updated_at`) values (DEFAULT, '2017-10-13 22:48:26', 'merm9005@gmail.com', NULL, NULL, '2017-10-13 22:48:26', 'Maram Habib"
                    if (str == null)
                    {
                        str = makeQueryTemplate(querySource);
                        if (needCache) {
                            cacheTemplates.put(querySource, str);
                        }
                    }
                    
                    queryTemplate = str;                    
                }
                catch (Exception ex)
                {
                    // TODO What we do if throws exception?
                    log.error("Error while make query template", ex);
                }

                Long queryCRC = null;
                if (needCache) {
                    queryCRC = cacheCrcs.get(querySource);
                }
                
                if (queryCRC == null) {
                    // calculate crc sums
                    CRC32 queryCrc = new CRC32();
                    queryCrc.update(querySource.getBytes());
                    
                    queryCRC = queryCrc.getValue();
                    if (needCache) {
                        cacheCrcs.put(querySource, queryCRC);
                    }
                }
                                
                
                Long templateCRC = null;
                if (needCache) {
                    templateCRC = cacheCrcs.get(queryTemplate);
                }
                
                if (templateCRC == null) {
                    // calculate crc sums
                    CRC32 queryCrc = new CRC32();
                    queryCrc.update(queryTemplate.getBytes());
                    
                    templateCRC = queryCrc.getValue();
                    if (needCache) {
                        cacheCrcs.put(queryTemplate, templateCRC);
                    }
                }
                
                // 1. Check if query exist PQUERY
                // if not exist:
                // 1.1 Get template
                // 1.2 Find exist template
                // 1.3 If not found  insert PQUERYTEMPLATE
                // 1.4 Insert PQUERY
                // if exist do nothing

                // 2. Get tables from query
                // 2.1 Insert if new PQTable and PQUserhost
                // 2.2 Add if need many-to-many realtion for PQueryTemplate and PQTable

                // 3. Save Latest info for query in PQUERYSTATS (update or insert if new)

                Integer queryId = null;
                Integer templateId = null;

                // check if query exist
                Object[] ids = loadPQueryIds(queryCRC);

                // if found just get ids
                if (ids != null && ids.length > 0) {
                    queryId = (Integer) ids[0];
                    templateId = (Integer) ids[1];
                } else {
                    // if not found need insert at least PQUERY
                    // first try find template
                    templateId = loadPQueryTemplateId(templateCRC);

                    // if templateId is null than we need insert new record
                    if (templateId == null) {

                        // insert new query template record
                        insertPQueryTemplate(queryTemplate, querySource, templateCRC);

                        // load generated id
                        templateId = loadPQueryTemplateId(templateCRC);
                    }

                    // insert new query record
                    insertPQuery(templateId, querySource, queryCRC);

                    // load generated id
                    ids = loadPQueryIds(queryCRC);

                    // its generated query id
                    queryId = (Integer) ids[0];            
                }

                // check database
                Integer dataBaseId = loadPQDB(processList.getDb());
                if (dataBaseId == null && processList.getDb() != null) {                
                    // if not exists - insert new
                    insertPQDB(processList.getDb());

                    // load generated id
                    dataBaseId = loadPQDB(processList.getDb());
                }
                
                // Get tables from query
                Map<String, String> tableNames = getTableNamesFromQuery(querySource);
                for (String tableName: tableNames.keySet()) {

//                    if (tableName.contains("slow_log")) {
//                        System.out.println("Query: " + querySource);
//                        System.out.println("tableName: " + tableName);
//                    }
                    
                    if (tableName.contains(".")) {
                        tableName = tableName.substring(tableName.indexOf(".") + 1);
                    }
                    
                    if (tableName.startsWith("`") && tableName.endsWith("`")) {
                        tableName = tableName.substring(1, tableName.length() - 1);
                    }
                    
                    // find table for current dataBaseId
                    Integer tableId = loadPQTable(tableName, dataBaseId);
                    if (tableId == null) {
                        // if not exists - insert new
                        insertPQTable(tableName, dataBaseId);

                        // load generated id
                        tableId = loadPQTable(tableName, dataBaseId);
                    }

                    // now we can add many to many relation if need
                    if (!isExistPQueryTemplatePQTableRelation(templateId, tableId)) {
                        // if not exist insert record
                        insertPQueryTemplatePQTableRelation(templateId, tableId);
                        
                        if (needCache) {
                            cacheRelations.get(templateId).put(tableId, true);
                        }
                    }

                }
                
                // find user host id
                Integer userHostId = loadPQUserHost(processList.getUser(), processList.getHost());
                if (userHostId == null) {
                    // if not exists - insert new record
                    insertPQUserHost(processList.getUser(), processList.getHost());

                    // load generated id
                    userHostId = loadPQUserHost(processList.getUser(), processList.getHost());
                }
                                
                // find state id
                Integer stateId = loadPQState(processList.getState());
                if (stateId == null) {
                    // if not found insert it
                    insertPQState(processList.getState());

                    // load generated id
                    stateId = loadPQState(processList.getState());
                }

                // i dont why how it can be NULL
                // need test this and fix
                // because i was get error
                if (stateId == null) {
                    log.error("STATE_ID is NULL for state = " + processList.getState());
                }

                if (!slow) {
                    // find query stats id
                    Object[] result = loadPQueryStats(templateId, queryId, dataBaseId, userHostId, processList.getThreadId());
                    
                    Integer queryStatsId = null;
                    if (result != null) {
                        queryStatsId = (Integer) result[0];
                        Timestamp dateTime = (Timestamp) result[1];
                        BigDecimal time = (BigDecimal) result[2];

                        // check its new call or the same
                        // time must related to executin time
                        Long newDateTime = processList.getDatetime().getTime();
                        Long oldDateTime = dateTime.getTime();
                        if (queryStatsId != null && (newDateTime - processList.getTime().longValue() != oldDateTime)) {
                            // then need insert bacuase its new call
                            queryStatsId = null;
                        }
                    }
                    
                    
                    if (queryStatsId == null) {
                        // if not found insert new record
                        insertPQueryStats(templateId, queryId, dataBaseId, userHostId, processList.getThreadId(), processList.getTime(), processList.getDatetime());

                        // load generated id
                        result = loadPQueryStats(templateId, queryId, dataBaseId, userHostId, processList.getThreadId());
                        queryStatsId = (Integer) result[0];
                    } else {                        
                        
                        // if found - update time and datetime
                        updatePQueryStats(queryStatsId, processList.getTime(), processList.getDatetime());
                    }
                    
                    // find state stat id
                    Integer stateStatId = loadPQStateStat(stateId, queryStatsId);
                    if (stateStatId == null) {
                        // if not found insert new record
                        insertPQStateStat(stateId, queryStatsId, processList.getTime(), processList.getDatetime());
                    } else {
                        // if found - update time and datetime
                        updatePQStateStat(stateId, processList.getTime(), processList.getDatetime());
                    }
                } else {
                    
                    storage.executeQuery(
                        String.format(queryInsertSqTable(), makeSlowTableName(TABLE_PREFIX__SLOW_QUERY, serverHashCode, date)), 
                        userHostId,
                        dataBaseId,
                        queryId,
                        templateId,
                        processList.getTime(),
                        processList.getDatetime(),
                        lockTime,
                        rowsSent,
                        rowsExamined
                    );
                }

                storage.commitTransaction();
                
            } catch (Throwable th) {
                storage.rollbackTransaction();
            }
        }
    }
    
    private Integer loadPQState(String state) {
        
        Integer result = null;
        if (needCache) {
            result = cacheStatesIds.get(state);
        }
      
        result = (Integer) storage.getSingleResult(queryLoadPqState(), state);

        if (needCache) {
            cacheStatesIds.put(state, result);
        }
        
        return result;
    }    
    
    private Integer loadPQueryTemplateId(Long templateCRC) {
        
        Integer result = null;
        if (needCache) {
            result = cacheQueryTemplateIds.get(templateCRC);
        }
        
        if (result == null) {
            result = (Integer) storage.getSingleResult(queryLoadPQueryTemplateId(), templateCRC);

            if (needCache) {
                cacheQueryTemplateIds.put(templateCRC, result);
            }
        }
        
        return result;
    }
    
    private Object[] loadPQueryIds(Long queryCRC) {
        
        Object[] result = null;
        if (needCache) {
            result = cacheQueryIds.get(queryCRC);
        }
        
        if (result == null) {
            
            result = (Object[]) storage.getSingleResult(queryLoadPQueryIds(), queryCRC);

            if (needCache) {
                cacheQueryIds.put(queryCRC, result);
            }
        }
        
        return result;
    }
    
    private Integer loadPQDB(String db) {
        
        Integer result = null;
        if (needCache) {
            result = cacheDBIds.get(db);
        }
        
        if (result == null) {
            
            result = (Integer) storage.getSingleResult(queryLoadPqDb(), db);
            if (needCache) {
                cacheDBIds.put(db, result);
            }
        }
        
        return result;
    }
    
    private Integer loadPQTable(String tableName, Integer dbId) {
        
        Integer result = null;
        if (needCache) {
            Map<String, Integer> tables = cacheTableIds.get(dbId);
            if (tables == null) {
                tables = new HashMap<>();
                cacheTableIds.put(dbId, tables);
            }
            
            result = tables.get(tableName);
        }
        
        if (result == null) {
                
            result = (Integer) storage.getSingleResult(queryLoadPqTable(), tableName, dbId);;

            if (needCache) {
                cacheTableIds.get(dbId).put(tableName, result);
            }
        }
        
        return result;
    }
    
    private Integer loadPQUserHost(String user, String host) {
        
        Integer result = null;
        if (needCache) {
            result = cacheUserhostIds.get(user + ":" + host);
        }
        
        if (result == null) {
            result = (Integer) storage.getSingleResult(queryLoadPqUserHost(), user, host);

            if (needCache) {
                cacheUserhostIds.put(user + ":" + host, result);
            }
        }
        
        return result;
    }
    
    private Object[] loadPQueryStats(Integer templateId, Integer queryId, Integer dbId, Integer userHostId, Integer threadId) {
        List list = storage.getResultList(
            queryLoadPQueryStats(), 
            templateId,
            queryId,
            userHostId,
            dbId,
            threadId
        );

        return list != null && !list.isEmpty() ? (Object[]) list.get(list.size() - 1) : null;
    }
    
    private Integer loadPQStateStat(Integer stateId, Integer pqueryStatsId) {
        return (Integer) storage.getSingleResult(queryLoadPqStateStat(), stateId, pqueryStatsId);
    }
    
    private void updatePQueryStats(Integer id, BigDecimal time, Date datetime) {        
        storage.executeQuery(queryUpdatePQueryStats(), time, datetime, id);
    }
    
    private void updatePQStateStat(Integer id, BigDecimal time, Date datetime) {        
        storage.getSingleResult(queryUpdatePqStateStat(), time, datetime, id);
    }
    
    private void insertPQueryStats(Integer templateId, Integer queryId, Integer dbId, Integer userHostId, Integer threadId, BigDecimal time, Date datetime) {
        
        storage.executeQuery(
            queryInsertPQueryStats(), 
            templateId,
            queryId,
            userHostId,
            dbId,
            threadId,
            time,
            datetime
        );
    }
    
    private void insertPQStateStat(Integer stateId, Integer pqueryStatsId, BigDecimal time, Date datetime) {
        
        storage.executeQuery(
            queryInsertPqStateStat(), 
            stateId,
            pqueryStatsId,
            time,
            datetime
        );
    }
    
    private boolean isExistPQueryTemplatePQTableRelation(Integer templateId, Integer tableId) {
        
        if (needCache) {
            Map<Integer, Boolean> map = cacheRelations.get(templateId);
            if (map == null) {
                map = new HashMap<>();
                cacheRelations.put(templateId, map);
            }
            
            if (map.get(tableId) != null) {
                return true;
            }
        }
        
        boolean result = storage.getSingleResult(queryLoadTemplateTableRelation(), templateId, tableId) != null;

        if (needCache && result)  {
            cacheRelations.get(templateId).put(tableId, true);
        }
            
        return result;
    }
    
    private void insertPQUserHost(String user, String host) {
        storage.executeQuery(queryInsertPqUserHost(), user, host);
    }
    
    private void insertPQState(String state) {
        storage.executeQuery(queryInsertPqState(), state);
    }
    
    private void insertPQueryTemplate(String queryTemplate, String querySource, Long templateCRC) {
        storage.executeQuery(queryInsertPQueryTemplate(), queryTemplate, querySource, templateCRC);
    }
    
    private void insertPQuery(Integer queryTemplateId, String querySource, Long queryCRC) {
        storage.executeQuery(queryInsertPQuery(), queryTemplateId, querySource, queryCRC);
    }
    
    private void insertPQueryTemplatePQTableRelation(Integer templateId, Integer tableId) {
        storage.executeQuery(queryInsertTemplateTableRelation(), templateId, tableId);
    }
    
    private void insertPQDB(String db) {
        storage.executeQuery(queryInsertPqDb(), db);
    }
    
    private void insertPQTable(String tableName, Integer dbId) {
        storage.executeQuery(queryInsertPqTable(), tableName, dbId);
    }
    
    
    private boolean prepareTable(String dbPrefix, String serverHashCode, Date date, String propertyFindByName, String propertyCreateTable, String propertyInsert) {
        synchronized (storage) {
            
            storage.beginTransaction();
            try {
            
                String tableName = makeTableName(dbPrefix, serverHashCode, date);

                // check maybe table already exists
                if (storage.getSingleResult(propertyFindByName, tableName) != null) {
                    return false;
                } else {
                    // its ok - table not exist, so need create it
                    storage.executeQuery(String.format(propertyCreateTable, tableName));

                    // and save db name
                    storage.executeQuery(propertyInsert, tableName);

                    return true;
                }
            } catch (Throwable th) {
                storage.rollbackTransaction();
                return false;
                
            } finally {            
                storage.commitTransaction();
            }
        }
    }
    
    private boolean prepareSlowQueryTable(String dbPrefix, String serverHashCode, Date currentDate, String propertyFindByName, String propertyCreateTable, String propertyInsert) {
        synchronized (storage) {
            storage.beginTransaction();

            try {
                String tableName = makeSlowTableName(dbPrefix, serverHashCode, currentDate);

                if (storage.getSingleResult(propertyFindByName, tableName) != null)
                {
                    return false;
                }
                else
                {
                    // its ok - table not exist, so need create it
                    storage.executeQuery(String.format(propertyCreateTable, tableName, tableName, tableName, tableName, tableName));

                    // and save db name
                    storage.executeQuery(propertyInsert, tableName);//, new Date());

                    return true;
                }
            } catch (Throwable th) {
                storage.rollbackTransaction();
                return false;
                
            } finally {            
                storage.commitTransaction();
            }
        }
    }
    
    private boolean prepareStatementDigestTable(String dbPrefix, String serverHashCode, Date currentDate, String propertyFindByName, String propertyCreateTable, String propertyInsert) {
        synchronized (storage) {
            storage.beginTransaction();

            try {
                String tableName = makeSDTableName(dbPrefix, serverHashCode, currentDate);

                if (storage.getSingleResult(propertyFindByName, tableName) != null)
                {
                    return false;
                }
                else
                {
                    // its ok - table not exist, so need create it
                    storage.executeQuery(String.format(propertyCreateTable, tableName, tableName, tableName, tableName, tableName));

                    // and save db name
                    storage.executeQuery(propertyInsert, tableName);

                    return true;
                }
            } catch (Throwable th) {
                storage.rollbackTransaction();
                return false;
                
            } finally {            
                storage.commitTransaction();
            }
        }
    }
    
    private boolean prepareGSStartTable(String dbPrefix, String serverHashCode, String propertyFindByName, String propertyCreateTable, String propertyInsert) {
        synchronized (storage) {
            storage.beginTransaction();
            
            try {

                String tableName = makeTableName(dbPrefix, serverHashCode, null);

                // check maybe table already exist
                if (storage.getSingleResult(propertyFindByName, tableName) != null) {

                    createdGSStartTableMap.put(serverHashCode, tableName);
                    return false;

                } else {

                    // its ok - table not exist, so need create it
                    storage.executeQuery(String.format(propertyCreateTable, tableName));

                    // and save db name
                    storage.executeQuery(propertyInsert, tableName, new Date());

                    updateLatestGSStartTable(serverHashCode, false);

                    createdGSStartTableMap.put(serverHashCode, tableName);
                    
                    return true;
                }
            } catch (Throwable th) {
                storage.rollbackTransaction();
                return false;
                
            } finally {
                storage.commitTransaction();
            }
        }
    }
    
    public void updateLatestGSStartTable(String serverHashCode, boolean needTransaction) {
        synchronized (storage) {
            String createdGSStartTable = createdGSStartTableMap.get(serverHashCode);
            if (createdGSStartTable != null) {
                if (needTransaction) {
                    storage.beginTransaction();
                }
                try {
                    storage.executeQuery(queryUpdateGsStartTables(), new Date(), createdGSStartTable);
                    
                    if (needTransaction) {
                        storage.commitTransaction();
                    }
                } catch (Throwable th) {
                    if (needTransaction) {
                        storage.rollbackTransaction();
                    }
                }

                createdGSStartTableMap.remove(serverHashCode);
            }
        }
    }
    
    public void updateStartTimeGSStartTable(String serverHashCode, Date date) {
        synchronized (storage) {
            String createdGSStartTable = createdGSStartTableMap.get(serverHashCode);
            if (createdGSStartTable != null) {
                storage.beginTransaction();
                try {
                    storage.executeQuery(queryUpdateStartTimeGsStartTables(), date, createdGSStartTable);
                
                    storage.commitTransaction();
                } catch (Throwable th) {
                    storage.rollbackTransaction();
                }
            }
        }
    }
    
    public String getSlowQueryTableName(String serverHashCode, Date date) {
        return makeSlowTableName(TABLE_PREFIX__SLOW_QUERY, serverHashCode, date);
    }
    
    private String makeTableName(String dbPrefix, String serverHashCode, Date date)
    {
        return (dbPrefix + serverHashCode + (date != null ? "_" + dateFormat.format(date) : "")).toUpperCase();
    }
    
    private String makeSlowTableName(String dbPrefix, String serverHashCode, Date date)
    {
        return (dbPrefix + serverHashCode + (date != null ? "_" + slowFormat.format(date) : "")).toUpperCase();
    }
    
    private String makeSDTableName(String dbPrefix, String serverHashCode, Date date)
    {
        return (dbPrefix + serverHashCode + (date != null ? "_" + sdFormat.format(date) : "")).toUpperCase();
    }
    
    private void removeOldTables(ProfilerSettings settings, String tablesQuery, String removeFromTablesQuery, String dropTableQuery, Date currentDate)
    {
        synchronized (storage) {
            List<String> tables = storage.getResultList(tablesQuery);
            if (tables != null)
            {
                for (String table: tables)
                {
                    if (isNeedRemoveTable(settings, table, currentDate))
                    {
                        // remove from tables table
                        storage.executeQuery(removeFromTablesQuery, table);

                        // drop db
                        storage.executeQuery(String.format(dropTableQuery, table));
                    }
                }
            }
        }
    }
    
    private boolean isNeedRemoveTable(ProfilerSettings settings, String tableName, Date currentDate)
    {
        // get YYYYMMDDHH from table name
        final String tableSufix = tableName.substring(tableName.lastIndexOf("_") + 1);
        
        try {
            Date tableDate = dateFormat.parse(tableSufix);
            
            return currentDate.getTime() - tableDate.getTime() > settings.getDataRotation() * 24 * 60 * 60 * 1000;
            
        } catch (ParseException ex) {
            log.error("Error while parsing table prefix", ex);
        }
        
        return false;
    }
    
    
    private void removeOldSlowQueries(String tablesQuery, String dropTableQuery, String removeTableNameQuery, Date currentDate)
    {
        synchronized (storage) {
            List<String> tables = storage.getResultList(tablesQuery);
            if (tables != null)
            {
                for (Object table: tables)
                {
                    if (isNeedRemoveSlowQueries(table.toString(), currentDate))
                    {
                        // drop db
                        try{
                            storage.beginTransaction();
                            storage.executeQuery(String.format(dropTableQuery, table));
                            storage.commitTransaction();
                        } catch (Throwable th) {
                            storage.rollbackTransaction();
                        }
                        
                        try{
                            storage.beginTransaction();
                            storage.executeQuery(String.format(removeTableNameQuery, table));
                            storage.commitTransaction();
                        } catch (Throwable th) {
                            storage.rollbackTransaction();
                        }
                    }
                }
            }
        }
    }
    
    private boolean isNeedRemoveSlowQueries(String tableName, Date currentDate)
    {
        // get YYYYMMDDHH from table name
        final String tableSufix = tableName.substring(tableName.lastIndexOf("_") + 1);
        
        try {
            Date tableDate = slowFormat.parse(tableSufix);
            
            return currentDate.getTime() - tableDate.getTime() > 2 * 24 * 60 * 60 * 1000;
            
        } catch (ParseException ex) {
            return true;
        }
    }
}
