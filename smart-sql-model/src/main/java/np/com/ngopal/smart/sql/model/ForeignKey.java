/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

import java.util.List;
import lombok.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ForeignKey extends BaseModel {
    
    private String name;
    private List<Column> columns;

    private String referenceDatabase;
    private String referenceTable;
    private List<Column> referenceColumns;
    
    private String onUpdate;
    private String onDelete;
    
    public void copy(ForeignKey fk) {

        this.name = fk.name;
        this.onUpdate = fk.onUpdate;
        this.onDelete = fk.onDelete;
        
        this.columns = new ArrayList<>();
        if (fk.getColumns() != null) {
            for (int i = 0; i < fk.getColumns().size(); i++) {
                Column column = new Column();
                this.columns.add(column);
                this.columns.get(i).copy(fk.getColumns().get(i));
            }
        }
        
        this.referenceDatabase = fk.referenceDatabase;
        this.referenceTable = fk.referenceTable;
        
        this.referenceColumns = new ArrayList<>();
        if (fk.getReferenceColumns() != null) {
            for (int i = 0; i < fk.getReferenceColumns().size(); i++) {
                Column column = new Column();
                this.referenceColumns.add(column);
                this.referenceColumns.get(i).copy(fk.getReferenceColumns().get(i));
            }
        }
    }
    
    @Override
    public String toString() {
        
        String columnStr = "";
        if (columns != null) {
            for (Column c: columns) {
                columnStr += c.getName() + ", ";
            }
            
            if (columnStr.endsWith(", ")) {
                columnStr = columnStr.substring(0, columnStr.length() - 2);
            }
        }
        return name + "(" + columnStr + ")";
    }
}
