/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@AllArgsConstructor
public enum TechnologyType implements Displayable {

    MYSQL("Mysql", 3306, "mysql://"),
    POSTGRESQL("PostgreSQL", 5432, "postgresql://");

    private String displayValue;

    private int defaultPort;

    private String prefix;

}
