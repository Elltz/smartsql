package np.com.ngopal.smart.sql.model.tech.mysql;

import java.io.IOException;
import java.util.Properties;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class MySQLFunctions {

    public static final TreeSet<String> FUNCTIONS;
    
    static {

        FUNCTIONS = new TreeSet<>();
        
        try {
            
            Properties p = new Properties();
            p.load(MySQLFunctions.class.getResourceAsStream("functions.properties"));
            
            for (Object kObj : p.keySet()) {
                FUNCTIONS.add(kObj.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(MySQLFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
}
