/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Entity
@javax.persistence.Table(name = "DesignerTableTemplate")

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DesignerTableTemplate extends BaseModel {

    @javax.persistence.Column(name = "NAME")
    private String name = "";
    
    @javax.persistence.Column(name = "CONTENT")
    private String Data = "";
    
    public void setData(String s){
        Data = s;
    }

}
