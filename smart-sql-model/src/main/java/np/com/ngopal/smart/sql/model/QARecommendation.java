
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({
    @NamedQuery(
        name = "qarecommendation.findRecommendations", 
        query = "SELECT q FROM QARecommendation q where q.connectionId = :connectionParamsId and q.database = :database and q.query = :query"),
    @NamedQuery(
        name = "qarecommendation.removeRecommendations", 
        query = "DELETE FROM QARecommendation q where q.connectionId = :connectionParamsId and q.database = :database and q.query = :query")
})
@Table(name = "QA_RECOMMENDATION")
public class QARecommendation extends BaseModel {
                    
    @Column(name="CONNECTION_ID")
    private Long connectionId;
    
    @javax.persistence.Column(name = "DATABASE")
    private String database;    
    
    @Column(name = "QUERY")
    private String query;
    
    @javax.persistence.Column(name = "DBTABLE")
    private String dbtable;    
    
    @Column(name = "RECOMMENDATION")
    private String recommendation;
    
    @Column(name = "INDEX_NAME")
    private String indexName;
    
    @Column(name = "SQL_INDEX_QUERY")
    private String sqlIndexQuery;
    
    @Column(name = "REWRITEN_QUERY")
    private String rewritenQuery;
    
    @Column(name = "OPTIMIZED_QUERY")
    private String optimizednQuery;
    
    @Column(name = "EMPTY_WITH_OR")
    private boolean emptyWithOr;
    
    @Column(name = "HAS_ANALYZERS")
    private boolean hasAnylyzers = true;
    
    @Column(name = "QA_RECOMMENDATIONS")
    private String qaRecommendation;
    
    @Column(name = "QA_SQL_INDEX_QUERY")
    private String qaSqlIndexQuery;
    
    @Column(name = "CACHED_ROWS")
    private Long cachedRows;
    
    @Column(name = "ROWS_MULTI")
    private Long rowsMulti;
    
}
