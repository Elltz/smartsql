/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@Entity
@NamedQueries({
    @NamedQuery(name = "favourites.findByParent", query = "SELECT f FROM Favourites f where f.parent = :parent OR (f.parent is NULL AND :parent is NULL)")
})
@Table(name = "Favourites")
public class Favourites extends BaseModel {
    
    public Favourites() {}
    
    
    @Column(name = "PARENT")
    private Long parent;

    @Column(name = "FOLDER")
    private boolean folder = false;

    @Column(name = "NAME")
    private String name;

    @Transient
    private String tempName;
    
    @Column(name = "QUERY")
    private String query;
        
    @Column(name = "PREDEFINED")
    private boolean predefined = false;
    
    @Override
    public String toString() { 
        return name;
    }

}
