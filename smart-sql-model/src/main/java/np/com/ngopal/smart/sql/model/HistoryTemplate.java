package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@javax.persistence.Table(name = "HISTORY_QUERY_TEMPLATE")
@NamedQueries({
    @NamedQuery(name = "history.findByTemplateAndConnectionId", query = "SELECT h FROM HistoryTemplate h WHERE h.queryTemplate = :queryTemplate AND h.connectionId = :connectionId"),
    @NamedQuery(name = "history.selectByConnectionIdWithOrder", query = "SELECT h FROM HistoryTemplate h WHERE h.connectionId = :connectionId ORDER BY h.noOfOccurrence DESC")
})
public class HistoryTemplate extends BaseModel {

    @javax.persistence.Column(name = "QUERY_TEMPLATE")
    private String queryTemplate;

    @javax.persistence.Column(name = "QUERY")
    private String query;

    @javax.persistence.Column(name = "NO_OF_OCCURRENCE")
    private Integer noOfOccurrence;
    
    @javax.persistence.Column(name = "RECENTLY_EXECUTED")
    private Timestamp recentlyExecuted;
    
    @javax.persistence.Column(name = "FIRST_EXECUTED")
    private Timestamp firstExecuted;
    
    @javax.persistence.Column(name = "CONNECTION_ID")
    private Long connectionId;
    
    @Transient
    private int rank;
    @Transient
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    
    
    public String[] getValuesFoFilter() {
        return new String[] {rank + "", noOfOccurrence != null ? noOfOccurrence.toString() : "", getRecentlyExecutedString(), queryTemplate != null ? queryTemplate : ""};
    }
    
    
    
    public String getRecentlyExecutedString() {
        return recentlyExecuted != null ? dateFormat.format(recentlyExecuted) : "";
    }
    
    public String getPresentation() {
        return getRank()+ " " + 
            (getNoOfOccurrence() != null ? getNoOfOccurrence().toString() : "") + " " + 
            getRecentlyExecutedString() + " " + 
            (getQueryTemplate() != null ? getQueryTemplate() : "");
    }
}