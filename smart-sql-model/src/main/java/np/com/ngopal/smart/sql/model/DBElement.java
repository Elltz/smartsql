package np.com.ngopal.smart.sql.model;

/**
 * Using by Database, DBTable, Column, View, Trigger, Procedure, Function, Event
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface DBElement {

    String getName();
}
