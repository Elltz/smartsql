/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.tech.mysql;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import np.com.ngopal.smart.sql.model.DynamicType;
import np.com.ngopal.smart.sql.model.EngineType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class MySQLEngineTypes extends DynamicType<EngineType> {

    public static final List<EngineType> TYPES;

    static {

        TYPES = new ArrayList<>();
        try {
            Properties p = new Properties();
            p.load(MySQLEngineType.class.getResourceAsStream("engines.properties"));

            for (Object kObj : p.keySet()) {
                final String key = kObj.toString();

                TYPES.add(new MySQLEngineType() {

                    @Override
                    public String getName() {
                        return key;
                    }

                    @Override
                    public String getDisplayValue() {
                        return p.get(key).toString();
                    }

                });
            }
        } catch (IOException ex) {
            Logger.getLogger(MySQLEngineType.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<EngineType> getTypes() {
        return TYPES;
    }
}
