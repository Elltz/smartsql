/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.ui;

import lombok.AllArgsConstructor;
import lombok.Getter;
import np.com.ngopal.smart.sql.model.Displayable;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@AllArgsConstructor
public enum MenuCategory implements Displayable {

    FILE("File"), SSH("SSH"), VIEW("View"), EDIT("Edit"), FAVORITES("Favorites"), DATABASE("Database"),
    TABLE("Table"), OTHERS("Others"), TOOLS("Tools"), POWERTOOLS("PowerTools"), REPORTS("Reports"), PROFILER("Profiler"), SETTINGS("Settings"),
    /*WINDOW("Window"),*/ HELP("Help"),SCHEMATOOLS("SchemaTools"),COMPONENTTOOLS("ComponentTools"), RIGHT_MENU("NOT FOR SHOWING - IT SPECIAL");

    private String displayValue;

}
