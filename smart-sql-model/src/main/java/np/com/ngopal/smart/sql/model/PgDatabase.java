/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.model;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PgDatabase extends Database {
    
    private int iD;
    private String serverEncoding;
    private boolean canCreate;
    private boolean allowConn;
    private int lastSysId;
    private int tableSpace;

    @Override
    public boolean equals(Object obj) {
        return  super.equals(obj) && (obj instanceof PgDatabase) && Objects.equals(getID(), ((PgDatabase)obj).getID());
    }
    
}
