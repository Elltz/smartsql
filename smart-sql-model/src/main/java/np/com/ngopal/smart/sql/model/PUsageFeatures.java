
package np.com.ngopal.smart.sql.model;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class PUsageFeatures  {
            
    public static final String QB = "Query builder";
    public static final String QA = "Query optimizer";
    public static final String SQA = "Slow query ANALYZE";
    public static final String SQAQA = "Slow query ANALYZE Query optimizer";
    public static final String DEBUGGER = "Debugger";
    public static final String DQA = "Debugger query optimizer";
    public static final String MST = "MySQL configure tunner";
    public static final String PROFILER = "Profiler";
    public static final String PQA = "Profiler query optimizer";
    public static final String REPORTS = "Reports";
    public static final String SPWC = "Stored procedure with cursor";
    public static final String SD = "Schema designer";
    public static final String PSU = "Public schema";
    public static final String QF = "Query format";
    public static final String LFE = "Lock free execution";
    public static final String SBQ = "Show bad query icon";
    public static final String CBQ = "Show bad query click";
    public static final String TC = "Total DB connection saved";
         
    public static final String KEY__USER_SYSTEM_ID  = "userSystemId";
    public static final String KEY__FEATURE         = "feature";
    public static final String KEY__STATUS          = "status";
    public static final String KEY__CREATE_AT       = "createAt";
}
