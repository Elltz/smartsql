package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@javax.persistence.Table(name = "HISTORY_QUERY")
@NamedQueries({
    @NamedQuery(name = "historyQuery.selectByConnectionIdWithOrder", query = "SELECT h FROM HistoryQuery h WHERE h.connectionId = :connectionId ORDER BY h.executed DESC")
})
public class HistoryQuery extends BaseModel {

    @javax.persistence.Column(name = "QUERY")
    private String query;    
    
    @javax.persistence.Column(name = "EXECUTED")
    private Timestamp executed;
    
    @javax.persistence.Column(name = "CONNECTION_ID")
    private Long connectionId;
    
    @Transient
    private int number;
    
    @Transient
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    
    
    
    
    public String getExecutedString() {
        return executed != null ? dateFormat.format(executed) : "";
    }
    
    public String[] getValuesFoFilter() {
        return new String[] {number + "", getExecutedString(), query != null ? query : ""};
    }
}