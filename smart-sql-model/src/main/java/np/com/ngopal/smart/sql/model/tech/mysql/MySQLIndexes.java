/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model.tech.mysql;

import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 */
public abstract class MySQLIndexes implements EngineType {

    @Override
    public TechnologyType getTechnologyType() {
        return TechnologyType.MYSQL;
    }

    @Override
    public String toString() {
        return getDisplayValue();
    }
}
