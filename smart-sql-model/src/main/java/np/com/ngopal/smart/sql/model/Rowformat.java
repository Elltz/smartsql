/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@AllArgsConstructor
public enum Rowformat implements Displayable {

    DYNAMIC("Dynamic"), FIXED("Fixed"), COMPRESSED("Compressed");
//, FOREIGN("Foreign")

    private String displayValue;

}
