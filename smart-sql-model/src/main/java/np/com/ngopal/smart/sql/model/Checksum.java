/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@AllArgsConstructor
public enum Checksum {

    FALSE(0), TRUE(1);
//, FOREIGN("Foreign")

    private int displayValue;

    @Override
    public String toString() {
        return displayValue + ""; //To change body of generated methods, choose Tools | Templates.
    }

}
