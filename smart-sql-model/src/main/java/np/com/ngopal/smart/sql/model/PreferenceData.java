/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PREFERENCE_DATA")
public class PreferenceData extends BaseModel {

    @Column(name = "BLOCK_STYLE")
    private boolean blockStyle = true;
    
    @Column(name = "INDENT_ENABLED")
    private boolean indentEnabled = true;
    
    @Column(name = "INDENTATION")
    private int identation = 2;

    
    @Column(name = "QUERIES_USING_BACKQUOTE")
    private boolean queriesUsingBackquote = true;
    @Column(name = "KEEP_FOCUS_ON_EDITOR")
    private boolean keepFocusOnEditor = true;
    @Column(name = "PROMT_UNSAVED_TAB")
    private boolean promtUnsavedTab = true;
    @Column(name = "PASTE_NAME_ON_DOUBLE_CLICK")
    private boolean pasteNameOnDoubleClick = true;
    @Column(name = "WORD_WRAP_IN_EDITOR")
    private boolean wordWrapInEditor = false;
    @Column(name = "TRANSACTION_SUPPORT")
    private boolean transactionSupport = true;
    @Column(name = "SHOW_WARNINGS")
    private boolean showWarnings = false;
    @Column(name = "HALT_EXECUTION_ON_ERROR")
    private boolean haltExecutionOnError = false;
    
    @Column(name = "EXPORT_OPTION_SERVER_DEFAULT")
    private boolean exportOptionsServerDefault = true;
    @Column(name = "EXPORT_OPTIONS_CUSTOM_SIZE")
    private String exportOptionsCustomSize = "1";
    @Column(name = "EXPORT_OPTIONS_DONT_BREAK_INTO_CHUNKS")
    private boolean exportOptionsDontBreakIntoChunks = true;
    @Column(name = "EXPORT_OPTIONS_CHUNK_SIZE")
    private String exportOptionsChunkSize = "1000";
    
    @Column(name = "POSITIONING_TABLE_DATA")
    private boolean positioningTableData = true;
    @Column(name = "POSITIONING_INFO_DATA")
    private boolean positioningInfoData = true;
    @Column(name = "POSITIONING_HISTORY")
    private boolean positioningHistory = false;
    
    
    @Column(name = "AUTOCOMPLETE_ENABLE")
    private boolean autocompleteEnable = true;
    @Column(name = "AUTOCOMPLETE_SHOW_HELP")
    private boolean autocompleteShowHelp = true;
    
    @Column(name = "PROFILLER_QUERY_ENABLE")
    private boolean profillerQueryEnable = true;
    @Column(name = "PROFILLER_SHOW_PROFILE")
    private boolean profillerShowProfile = true;
    @Column(name = "PROFILLER_EXPLAIN_RESULT")
    private boolean profillerExplainResult = true;
    @Column(name = "PROFILLER_STATUS_VARIABLES")
    private boolean profillerStatusVariables = true;
    @Column(name = "PROFILLER_EXPLAIN_EXTENDED")
    private boolean profillerExplainExtended = false;
    
    
    @Column(name = "FONTS_SQL_EDITOR")
    private String fontsSqlEditor = "Courier New, Regular, 14.0";
    @Column(name = "FONTS_HISTORY_INFO")
    private String fontsHistoryInfo = "Courier New, Regular, 14.0";
    @Column(name = "FONTS_BLOB_VIEWER")
    private String fontsBlobViewer = "Courier New, Regular, 14.0";
    @Column(name = "FONTS_OBJECT_BROWSER")
    private String fontsObjectBrowser = "System, Regular, 14.0";
    @Column(name = "FONTS_OTHERS")
    private String fontsOthers = "Courier New, Regular, 14.0";
    
    @Column(name = "TABS_SIZE")
    private String tabsSize = "8";
    @Column(name = "TABS_INSERT_SPACES")
    private boolean tabsInsertSpaces = false;
    
    
    @Column(name = "MISC_RESTORE_SESSION")
    private boolean miscRestoreSession = true;
    @Column(name = "PAGGING_NUMBER_ROWS")
    private String paggingNumberRows = "1000";
    
    @Column(name = "DEBUGGER_REFRESH_INTERVAL")
    private String debuggerRefreshInterval = "15";
    @Column(name = "DEBUGGER_ENABLE_BLINK")
    private boolean debuggerEnableBlink = true;
    @Column(name = "DEBUGGER_RQ_SLOW")
    private String debuggerRQSlow = "6";
    @Column(name = "DEBUGGER_RQ_MEDIUM")
    private String debuggerRQMedium = "1";
    @Column(name = "DEBUGGER_RQ_NORMAL")
    private String debuggerRQNormal = "0";
    @Column(name = "DEBUGGER_DEADLOCK_HISTORY")
    private String debuggerDeadlockHistory = "7";
    @Column(name = "DEBUGGER_DEADLOCK_ALERT")
    private String debuggerDeadlockAlert = "5";
    
    @Column(name = "DEBUGGER_ALERT_RQ")
    private boolean debuggerAlertRQ = true;
    @Column(name = "DEBUGGER_ALERT_TR")
    private boolean debuggerAlertTR = true;
    @Column(name = "DEBUGGER_ALERT_DL")
    private boolean debuggerAlertDL = true;
    @Column(name = "DEBUGGER_ALERT_QL")
    private boolean debuggerAlertQL = true;
    @Column(name = "DEBUGGER_ALERT_CONN")
    private boolean debuggerAlertConn = true;
    @Column(name = "DEBUGGER_ALERT_DB_LOAD")
    private boolean debuggerAlertDBLoad = true;
    @Column(name = "DEBUGGER_ALERT_CONFIG")
    private boolean debuggerAlertConfig = true;
    @Column(name = "DEBUGGER_ALERT_RL")
    private boolean debuggerAlertRL = true;
    @Column(name = "DEBUGGER_ALERT_RM")
    private boolean debuggerAlertRM = true;
    @Column(name = "DEBUGGER_ALERT_FK")
    private boolean debuggerAlertFK = true;
    @Column(name = "DEBUGGER_ALERT_OT")
    private boolean debuggerAlertOT = true;
    @Column(name = "DEBUGGER_ALERT_BP")
    private boolean debuggerAlertBP = true;
    @Column(name = "DEBUGGER_ALERT_RO")
    private boolean debuggerAlertRO = true;
    @Column(name = "DEBUGGER_ALERT_INNO_DB_STATUS")
    private boolean debuggerAlertInnoDBStatus = true;
    @Column(name = "DEBUGGER_ALERT_MY_ISAM_STATUS")
    private boolean debuggerAlertMyISAMStatus = true;
    @Column(name = "DEBUGGER_ALERT_DB_SIZE")
    private boolean debuggerAlertDBSize = true;
    @Column(name = "DEBUGGER_ALERT_UI")
    private boolean debuggerAlertUI = true;
    @Column(name = "DEBUGGER_ALERT_DI")
    private boolean debuggerAlertDI = true;
    @Column(name = "DEBUGGER_ALERT_SQA")
    private boolean debuggerAlertSQA = true;
    @Column(name = "DEBUGGER_ALERT_EL")
    private boolean debuggerAlertEL = true;
    
    
    @Column(name = "QA_INDEX_MAX_NO_COLUMNS")
    private String qaIndexMaxNoColumns = "4";    
    @Column(name = "QA_INDEX_SUBSTR_LENGTH")
    private String qaIndexSubstrLength = "62";
    
    @Column(name = "QA_OTHER_USE_CACHE")
    private boolean qaOtherUseCache = true;
    
    
    @Column(name = "QB_ACTIVATE_AUTO_COMPLETE")
    private boolean qbActivateAutoComplete = true;
    @Column(name = "QB_COLUMNS")
    private boolean qbColumns = true;
    @Column(name = "QB_TABLES_VIEWS")
    private boolean qbTablesViews = true;    
    @Column(name = "QB_DATABASE")
    private boolean qbDatabase = true;
    @Column(name = "QB_VARIABLES")
    private boolean qbVariables = true;
    @Column(name = "QB_STORED_PROCEDURES")
    private boolean qbStoredProcedures = true;
    @Column(name = "QB_HISTORY")
    private boolean qbHistory = true;
    @Column(name = "QB_FAVORITES")
    private boolean qbFavorites = true;
    @Column(name = "QB_FEQ")
    private boolean qbFeq = true;
    
    @Column(name = "QB_ENABLE_ANIMATION")
    private boolean qbEnableAnimation = true;
    @Column(name = "QB_FLIP_ANIMATION")
    private boolean qbFlipAnimation = true;
    @Column(name = "QB_EXPAND_ANIMATION")
    private boolean qbExpandAnimation = false;
        
    public void restoreQADefaults()
    {
        qaIndexMaxNoColumns = "4";
        qaIndexSubstrLength = "16";
        qaOtherUseCache = true;
    }
    
    public void restoreSqlFormatDefaults()
    {
        blockStyle = true;
        indentEnabled = true;
        identation = 2;
    }
    
    public void restoreGeneralDefaults()
    {        
        queriesUsingBackquote = true;
        keepFocusOnEditor = true;
        promtUnsavedTab = true;
        pasteNameOnDoubleClick = true;
        wordWrapInEditor = false;
        transactionSupport = true;
        showWarnings = false;
        haltExecutionOnError = false;
    
        exportOptionsServerDefault = true;
        exportOptionsCustomSize = "1";
        exportOptionsDontBreakIntoChunks = true;
        exportOptionsChunkSize = "1000";
    
        positioningTableData = true;
        positioningInfoData = true;
        positioningHistory = false;
    }
    
    public void restorePowerToolsDefaults()
    {        
        autocompleteEnable = true;
        autocompleteShowHelp = true;
        profillerQueryEnable = true;
        profillerShowProfile = true;
        profillerExplainResult = true;
        profillerStatusVariables = true;
        profillerExplainExtended = false;
    }
    
    public void restoreFontsDefaults()
    {        
        String defaultFontSize = "14.0";
//        if (PlatformUtil.isMac() || PlatformUtil.isWindows()) {
//            defaultFontSize = "14.0";
//        }
        fontsSqlEditor = "Courier New, Regular, " + defaultFontSize;
        fontsHistoryInfo = "Courier New, Regular, " + defaultFontSize;
        fontsBlobViewer = "Courier New, Regular, " + defaultFontSize;
        fontsObjectBrowser = "System, Regular, " + defaultFontSize;
        fontsOthers = "Courier New, Regular, " + defaultFontSize;
        
        tabsSize = "8";
        miscRestoreSession = true;        
    }
    
    public void restoreOthersDefaults()
    {  
        paggingNumberRows = "1000";
        tabsInsertSpaces = false;        
    }
    
    public void restoreQueryBrowserDefaults() {
        qbActivateAutoComplete = true;
        qbColumns = true;
        qbTablesViews = true;    
        qbDatabase = true;
        qbVariables = true;
        qbStoredProcedures = true;
        qbHistory = true;
        qbFavorites = true;
        qbFeq = true;
    
        qbEnableAnimation = true;
        qbFlipAnimation = true;
        qbExpandAnimation = false;
    }
    
    public void restoreDebuggerDefaults()
    {  
        debuggerEnableBlink = true;
        
        debuggerRefreshInterval = "15";
        
        debuggerRQSlow = "6";
        debuggerRQMedium = "1";
        debuggerRQNormal = "0";
        
        debuggerDeadlockHistory = "7";
        debuggerDeadlockAlert = "5";
        
        debuggerAlertRQ = true;
        debuggerAlertTR = true;
        debuggerAlertDL = true;
        debuggerAlertQL = true;
        debuggerAlertConn = true;
        debuggerAlertDBLoad = true;
        debuggerAlertConfig = true;
        debuggerAlertRL = true;
        debuggerAlertRM = true;
        debuggerAlertFK = true;
        debuggerAlertOT = true;
        debuggerAlertBP = true;
        debuggerAlertRO = true;
        debuggerAlertInnoDBStatus = true;
        debuggerAlertMyISAMStatus = true;
        debuggerAlertDBSize = true;
        debuggerAlertUI = true;
        debuggerAlertDI = true;
        debuggerAlertSQA = true;
        debuggerAlertEL = true;
    }    
    
    public void restoreAllDefaults()
    {
        restoreGeneralDefaults();
        restorePowerToolsDefaults();
        restoreSqlFormatDefaults();
        restoreFontsDefaults();
        restoreOthersDefaults();
        restoreQueryBrowserDefaults();
        restoreQADefaults();
        restoreDebuggerDefaults();
    }
    
    public String getTab() {
        return "";
    }
    
    public String getIndentString()  {
        return String.valueOf(identation);
    }
    
    public String getStyle() {
        return blockStyle ? "block" : "expanded";
    }
}
