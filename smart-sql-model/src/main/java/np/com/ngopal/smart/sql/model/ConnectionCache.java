/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.model;

import java.util.List;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author NGM
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@AttributeOverride(name = "id", column = @javax.persistence.Column(name = "param"))
public class ConnectionCache extends BaseModel {

    @Id
    @javax.persistence.JoinColumn(updatable = false)
    private ConnectionParams param;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Database> databases;

}
