package np.com.ngopal.smart.sql.model;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StatementDigest extends BaseModel {
    
    private Integer connectionId;
    private String schemaName;
    private String digest;
    private String digestText;
    private Long countStar = 0l;
    private Long countStarDiff = 0l;
    private Long sumTimerWait = 0l;
    private Long sumTimerWaitDiff = 0l;
    private Long minTimerWait = 0l;
    private Long minTimerWaitDiff = 0l;
    private Long avgTimerWait = 0l;
    private Long avgTimerWaitDiff = 0l;
    private Long maxTimerWait = 0l;
    private Long maxTimerWaitDiff = 0l;
    private Long sumLockTime = 0l;
    private Long sumLockTimeDiff = 0l;
    private Long sumErrors = 0l;
    private Long sumErrorsDiff = 0l;
    private Long sumWarnings = 0l;
    private Long sumWarningsDiff = 0l;
    private Long sumRowsAffected = 0l;
    private Long sumRowsAffectedDiff = 0l;
    private Long sumRowsSent = 0l;
    private Long sumRowsSentDiff = 0l;
    private Long sumRowsExamined = 0l;
    private Long sumRowsExaminedDiff = 0l;
    private Long sumCreatedTmpDiskTables = 0l;
    private Long sumCreatedTmpDiskTablesDiff = 0l;
    private Long sumCreatedTmpTables = 0l;
    private Long sumCreatedTmpTablesDiff = 0l;
    private Long sumSelectFullJoin = 0l;
    private Long sumSelectFullJoinDiff = 0l;
    private Long sumSelectFullRangeJoin = 0l;
    private Long sumSelectFullRangeJoinDiff = 0l;
    private Long sumSelectRange = 0l;
    private Long sumSelectRangeDiff = 0l;
    private Long sumSelectRangeCheck = 0l;
    private Long sumSelectRangeCheckDiff = 0l;
    private Long sumSelectScan = 0l;
    private Long sumSelectScanDiff = 0l;
    private Long sumSortMergePasses = 0l;
    private Long sumSortMergePassesDiff = 0l;
    private Long sumSortRange = 0l;
    private Long sumSortRangeDiff = 0l;
    private Long sumSortRows = 0l;
    private Long sumSortRowsDiff = 0l;
    private Long sumSortScan = 0l;
    private Long sumSortScanDiff = 0l;
    private Long sumNoIndexUsed = 0l;
    private Long sumNoIndexUsedDiff = 0l;
    private Long sumNoGoodIndexUsed;
    private Long sumNoGoodIndexUsedDiff;
    private Timestamp firstSeen;
    private Timestamp lastSeen;
    
}