
package np.com.ngopal.smart.sql.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javax.persistence.OrderBy;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "GS_CHART")
public class GSChart extends BaseModel {
    
    public enum UI_TYPE {
        TABLE_BOTTOM,
        TABLE_RIGHT,
        DEFAULT
    }
    
    public enum CHART_STYLE {
        BAR,
        AREA,
        LINE
    }
    
    @Column(name = "CHART_NAME")
    private String chartName; // name of the chat it need to dispaly
    
    @Column(name = "CHARTTYPE")
    private boolean chartType = false; // only two types 0 -> default chats 1 -> user defind chats
        
    @Column(name = "DESCRIPTION")
    private String description; // we need to dispaly some contat about the chat and usage
    
    @Column(name = "STATUS")
    private boolean status = true; // 1 means active , 0 means inactive
    
    @Column(name = "UITYPE")
    private String uiType = UI_TYPE.DEFAULT.name();
    
    @Column(name = "CHARTSTYLE")
    private String chartStyle = CHART_STYLE.LINE.name();
    
    @Column(name = "GROUP_NAME")
    private String groupName;
    
    @Column(name = "GROUP_WIDTH")
    private int groupWidth;
    
    @Column(name = "CHART_ORDER")
    private int chartOrder;
    
    @Column(name = "HOURLY")
    private Integer hourly = 0; // 0 - simple chart, 1 - hourly table
    
    @Column(name = "TAB")
    private String tab;
    
    @OrderBy("id")
    @OneToMany(cascade=CascadeType.ALL, mappedBy="chart")
    private List<GSChartVariable> variables;
    
    
    @Transient
    private BooleanProperty statusProperty = new SimpleBooleanProperty(status) {
        @Override
        protected void invalidated() {
            status = statusProperty.get();
        }
        
    };
    
    public void setStatus(boolean status) {
        statusProperty.set(status);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof GSChart && Objects.equals(((GSChart)obj).getId(), getId());
    }
    
    public GSChartVariable getVariableByName(String name) {
        if (variables != null) {
            for (GSChartVariable v: variables) {
                if (v.getVariableName().equalsIgnoreCase(name)) {
                    return v;
                }
            }
        }
        
        return null;
    }
}
