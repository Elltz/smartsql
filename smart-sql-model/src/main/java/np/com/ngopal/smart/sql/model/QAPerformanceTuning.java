
package np.com.ngopal.smart.sql.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import np.com.ngopal.model.core.BaseModel;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({
    @NamedQuery(
        name = "qaperformancetuning.findUnique", 
        query = "SELECT q FROM QAPerformanceTuning q where q.connectionId = :connectionParamsId and q.database = :database and q.query = :query and q.recommendation = :recommendation"),
    @NamedQuery(
        name = "qaperformancetuning.findByConnectionParamsId", 
        query = "SELECT q FROM QAPerformanceTuning q where q.connectionId = :connectionParamsId")
})
@Table(name = "QA_PERFORMANCE_TUNING")
public class QAPerformanceTuning extends BaseModel {
    
    @javax.persistence.Column(name = "CREATE_DATE")
    private Timestamp createDate;
    
    @Column(name="EXECUTING_DATETIME")
    private Timestamp executingDateTime;
                
    @Column(name="CONNECTION_ID")
    private Long connectionId;
    
    @javax.persistence.Column(name = "DATABASE")
    private String database;
    
    @javax.persistence.Column(name = "DBTABLE")
    private String dbtable;
    
    @Column(name = "QUERY")
    private String query;
    
    @Column(name = "RECOMMENDATIONS")
    private String recommendation;

    @Column(name = "SQL_INDEX_QUERY")
    private String sqlIndexQuery;
    
    @javax.persistence.Column(name = "ROWS_SCAN_BEFORE")
    private Long rowsScanBefore; // NRSBI
    
    @javax.persistence.Column(name = "ROWS_SCAN_AFTER")
    private Long rowsScanAfter; //NRSAI
    
    @Column(name = "IMPROVEMENT")
    private String performanceImprovement;  // ( (NRSBI - NRSAI)/NRSAI )X times 
    
    @javax.persistence.Column(name = "IMPROVEMENT_PERCENT")
    private String improvementPercent; // ( (NRSBI - NRSAI)/NRSAI )*100 %
       
    @javax.persistence.Column(name = "STATUS")
    private String status;// [good, Note at created, nutal] if the percentage is positive number then good , else nutal (Please use some icons for it) , if user not yet create index then “Note at created”. 
 
    @javax.persistence.Column(name = "USED_CREDIT")
    private boolean usedCredit = false;
}
