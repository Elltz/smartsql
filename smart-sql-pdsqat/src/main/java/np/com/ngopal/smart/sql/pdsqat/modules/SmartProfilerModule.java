package np.com.ngopal.smart.sql.pdsqat.modules;

import java.util.Map;
import java.util.function.Consumer;
import javafx.scene.control.MenuItem;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.pdsqat.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.modules.ProfilerModule;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.SessionPersistor;

/**
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class SmartProfilerModule extends ProfilerModule implements SessionPersistor {

    public SmartProfilerModule(UIController icontroller) {
        controller = icontroller;
        profilerService = ServiceEntityProviders.getInstance().getProfilerService();
        preferenceDataService = ServiceEntityProviders.getInstance().getPrefDataService();
        recommendationService = ServiceEntityProviders.getInstance().getQARecommendationService();
        performanceTuningService = ServiceEntityProviders.getInstance().getQAPerformanceTuningService();
        chartService = ServiceEntityProviders.getInstance().getProfilerChartService();
        columnCardinalityService = ServiceEntityProviders.getInstance().getColumnCardinalityService();
        deadlockService = ServiceEntityProviders.getInstance().getDeadlockService();
        settingsService = ServiceEntityProviders.getInstance().getProfilerSettingsService();
        paramService = ServiceEntityProviders.getInstance().getDb();
        debuggerProfilerService = ServiceEntityProviders.getInstance().getDebuggerProfilerService();
        dbProfilerService = ServiceEntityProviders.getInstance().getSecondProfilerService();
        usageService = ServiceEntityProviders.getInstance().getUsageService();
    }

    @Override
    public synchronized boolean canOpenConnection(ConnectionParams params) {
        // we must denid open connection for the same address
        if (params != null) {
            for (ConnectionSession cs: profilerControllers.keySet()) {
                if (cs.getConnectionParam().getAddress() != null && cs.getConnectionParam().getAddress().equals(params.getAddress())) {
                    DialogHelper.showError(controller, "NEW CONNECTION ERROR", "Connection already being monitored", params, null);
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean install() {
        super.install();        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FILE, new MenuItem[]{newItem, openItem, saveItem, saveAsItem, exportItem, exitItem, monitoringStatusItem});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{startItem, stopItem});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.PROFILER, new MenuItem[]{settingsItem, clearCachedDataItem});

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{startItem, stopItem, exportItem, scrollBackwardItem,
            scrollForwardItem, chartEditItem, zoomAlt, /*zoomText, zoomIn, zoomOut,*/
            tTime, timeElapsedItem, monitoringStatusItem, debuggerItem, slowQueryAnalyzer, tunerItem});

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{zoomOut, zoomText, zoomIn, selectTime});
                
        return true;
    }

    @Override
    public Map<?, ?> getPersistingMap() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onRestore(Map<?, ?> restoreImage, ConnectionSession session, Long connection, Consumer<ConnectionSession> parentCallback) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void connectToConnection(ConnectionParams cp) {
        
    }

    @Override
    public void connectionFailedOpening(String error) {
        
    }
        
}
