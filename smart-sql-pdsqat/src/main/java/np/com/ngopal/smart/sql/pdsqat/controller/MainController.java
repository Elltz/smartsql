/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.pdsqat.controller;

import com.sun.javafx.scene.control.behavior.TabPaneBehavior;
import com.sun.javafx.scene.control.skin.TabPaneSkin;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.pdsqat.modules.SmartProfilerModule;
import np.com.ngopal.smart.sql.pdsqat.provider.ProfilerBaseControllerProvider;
import np.com.ngopal.smart.sql.pdsqat.provider.ProfilerImageProvider;
import np.com.ngopal.smart.sql.pdsqat.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.CheckForUpdatesController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.SmartPropertiesData;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
@Slf4j
public class MainController extends BaseController implements Initializable, UIController {

    @FXML
    private AnchorPane mainUI;

    @Getter
    ConnectionParamService db;

    @FXML
    private TabPane tabPane;

    @FXML
    private StackPane UiHolder;

    @FXML
    private HBox footer;

    @FXML
    private HBox notificationBox;

    @FXML
    private VBox information_panel_vbox;
    
    @FXML
    private Tab mainNew;
    
    @FXML
    private VBox homepageTabContent;
    
    @FXML
    private FlowPane connectionsList;
    
    @FXML
    private ImageView bannerImageView;
    
    @FXML
    private Button newConnectionButton;
    
    @FXML
    private HBox splashHolder;

    private UserUsageStatisticService usageService;

    private SmartProfilerModule profilerModule;

    @Getter(AccessLevel.PUBLIC)
    private Map<KeyCombination, List<Object>> keyCombinations = new HashMap<>();

    @Getter(AccessLevel.PUBLIC)
    private final Map<KeyCombination, Consumer> customKeyCombinations = new HashMap<>();

    private SimpleObjectProperty<ConnectionSession> selectedSession = new SimpleObjectProperty<ConnectionSession>();

    private final Map<Integer, ConnectionSession> connectionSessions = new LinkedHashMap<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.out.println("init");
        usageService = ServiceEntityProviders.getInstance().getUsageService();
        
        newConnectionButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                NewConnectionController ncc = new ProfilerBaseControllerProvider<NewConnectionController>().get("NewConnection");
                ncc.setDialogMode(true);
                Stage s = new Stage(StageStyle.UTILITY);
                s.setTitle("New Connection Dialog");
                s.initOwner(getStage());
                s.initModality(Modality.APPLICATION_MODAL);
                s.setScene(new Scene(ncc.getUI(), 600, 500));
                s.showAndWait();
                retrieveConnectionsAndInstall();
            }
        });

        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab unSelectedTab, Tab selectedTab) {
                if (selectedTab == mainNew) {
                    selectedSession.setValue(null);
                    refreshConnectionsUiState();
                }else{
                    
                    if (selectedTab != null && selectedTab.getUserData() instanceof SmartProfilerTabContentController)
                    {
                        hideApplicationFooter();
                        SmartProfilerTabContentController tabContentController = (SmartProfilerTabContentController) selectedTab.getUserData();
                        selectedSession.setValue(tabContentController.getSession());
                        tabContentController.isFocusedNow();
                        try { profilerModule.connectionChanged(tabContentController.getSession().getConnectionParam()); }catch(Exception e){}
                    }
                }
            }
        });

        Label mainNewGraphic = new Label();
        mainNewGraphic.getStyleClass().add("tab-label");
        mainNewGraphic.setGraphic(new ImageView(ProfilerImageProvider.getInstance().getHome_image()));
        mainNew.setGraphic(mainNewGraphic);
        mainNew.setText("");        
        retrieveConnectionsAndInstall();
    }

    @Getter(AccessLevel.PUBLIC)
    private HashMap<MenuCategory, List<MenuItem>> categories = new HashMap(1);

    @Override
    public void addMenuItems(Class depencence, MenuCategory category, MenuItem[] items) {
        System.out.println();

        if (!categories.containsKey(category)) {
            categories.put(category, new ArrayList<>());
        }

        List<MenuItem> mis = categories.get(category);

        for (MenuItem menuItem : items) {

            mis.add(menuItem);
            if (menuItem instanceof Menu) {
                for (MenuItem mi : ((Menu) menuItem).getItems()) {
                    configureMenuItemAccelerators(mi);
                }
            } else {
                configureMenuItemAccelerators(menuItem);
            }
        }
    }
    
    private void configureMenuItemAccelerators(MenuItem mi) {
        if (mi.getAccelerator() != null) {
            List<Object> list = keyCombinations.get(mi.getAccelerator());
            if (list == null) {
                list = new ArrayList<>();
                keyCombinations.put(mi.getAccelerator(), list);
            }
            list.add(mi);
        }
    }

    @Override
    public void clearMenuItems(Class depencence, MenuCategory category) { }

    @Override
    public void addToolbarContainerTabListener(Consumer<MenuCategory> listener) { }

    @Override
    public void removeToolbarContainerTabListener(Consumer<MenuCategory> listener) { }

    @Override
    public void addCustomKeyCombinations(KeyCombination key, Consumer action) {
        if (customKeyCombinations.put(key, action) != null) {
            log.warn("Diplicate custom key combination: " + key);
        }
    }

    @Override
    public void nextTab() {
        tabPane.getSelectionModel().selectNext();
    }

    @Override
    public void prevTab() {
        tabPane.getSelectionModel().selectPrevious();
    }

    @Override
    public void selectTab(int index) {
        tabPane.getSelectionModel().select(index);
    }

    @Override
    public void selectLast() {
        tabPane.getSelectionModel().selectLast();
    }

    @Override
    public void addTab(Tab tab, ConnectionSession session) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            tabController.addTab(tab);
        }
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedConnectionTab(ConnectionSession session) {
        return tabPane.getSelectionModel().selectedItemProperty();
    }

    @Override
    public TabContentController getTabControllerBySession(ConnectionSession session) {
        Tab t = getTabBySession(session);
        if (t != null) {
            return (TabContentController) t.getUserData();
        }
        return null;
    }

    @Override
    public void showTab(Tab tab, ConnectionSession session) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            tabController.showTab(tab);
        }
    }

    @Override
    public Tab getTab(String id, String text, ConnectionSession session) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem(ConnectionSession session) {
        return null;
    }

    @Override
    public void refreshTree(ConnectionSession session, boolean alertDatabaseCount) {

    }

    @Override
    public void toggleLeftPane() {

    }

    @Override
    public void hideContentPane(ConnectionSession session, boolean visibility) {

    }

    @Override
    public void hideResultPane(ConnectionSession session, boolean visibility) {

    }

    @Override
    public ReadOnlyObjectProperty<ConnectionSession> getSelectedConnectionSession() {
        return selectedSession;
    }

    @Override
    public ModuleController getModuleByClassName(String moduleClassname) {
        return profilerModule;
    }

    @Override
    public Stage getStage() {
        return (Stage) getUI().getScene().getWindow();
    }

    @Override
    public void addTabInConnection(Tab tab, int i) {

    }

    @Override
    public void closeAllTabs() {

    }

    @Override
    public void closeTab(TabContentController tcc) {

    }

    @Override
    public void addTabToBottomPane(ConnectionSession session, Tab... tab) {

    }

    @Override
    public void removeTabFromBottomPane(ConnectionSession session, Tab... tab) {

    }

    @Override
    public DisconnectPermission disconnect(ConnectionSession session, DisconnectPermission permission) {
        if (session != null) {
            int i = session.getId();
            Tab t = getTabBySession(session);
            if (t != null) {
                connectionSessions.remove(i);
                permission = ((TabContentController) t.getUserData()).destroyController(permission);
                TabPaneBehavior behavior = ((TabPaneSkin) tabPane.getSkin()).getBehavior();
                behavior.closeTab(t);
                //tabPane.getTabs().remove(t);
            }
        }

        return permission;
    }

    @Override
    public BooleanBinding centerTabForQueryTabBooleanProperty() {
        return null;
    }

    @Override
    public BooleanBinding centerTabForOnlyQueryTabBooleanProperty() {
        return null;
    }

    @Override
    public ObjectBinding selectedTabProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reachHistoryTab() {
    }

    @Override
    public void reachFeqTab() {
    }

    @Override
    public void saveSessions() {

    }

    @Override
    public void saveSession(ConnectionSession session) {

    }

    @Override
    public void saveSessionAs(ConnectionSession session) {

    }

    @Override
    public void saveSessionTab(ConnectionSession session, String tab) {

    }

    @Override
    public void openSessionPoint() {

    }

    @Override
    public void endSession(ConnectionSession session) {

    }

    @Override
    public void aboutPage() {
        Platform.runLater(()-> tabPane.getSelectionModel().select(mainNew) );        
    }

    @Override
    public void addBackgroundWork(WorkProc wp) {
        Recycler.addWorkProc(wp);
    }

    @Override
    public void togleTabPaneLayerToRight(boolean bool) {

    }

    @Override
    public HBox getApplicationFooter() {
        return footer;
    }

    @Override
    public void hideApplicationFooter() {
        //footer.setVisible(false);
        AnchorPane.setBottomAnchor(UiHolder, 0.0);
    }

    @Override
    public void showApplicationFooter() {
        if(footer.getChildren().size() > 1 || ((HBox)footer.getChildren().get(0)).getChildren().size() > 0){
            //footer.setVisible(true);
            AnchorPane.setBottomAnchor(UiHolder, 23.0);
        }
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public void registerModule(ModuleController module) {
        //modulesControllers.add(module);
    }

    public void registerProfilerModule(SmartProfilerModule pm) {
        profilerModule = pm;
        pm.install();
    }

    public ConnectionSession createConnectionSession(ConnectionParams params) {
        ConnectionSession session = null;
        if (profilerModule.canOpenConnection(params)) {
            session = new ConnectionSession();
            session.setOpen(false);
            session.setId(connectionSessions.size() + 1);
            session.setConnectionParam(params);

            DBService dbservice = ServiceEntityProviders.getInstance().getNewMysqlDBService();
            session.setService(dbservice);

            SmartProfilerTabContentController tabContentController = new ProfilerBaseControllerProvider<SmartProfilerTabContentController>().get("ProfilerModuleTabContent");
            tabContentController.setSession(session);
            tabContentController.setDbService(dbservice);
            tabContentController.setCategories(categories);
            tabContentController.setCustomKeyCombinations(customKeyCombinations);
            tabContentController.setKeyCombinations(keyCombinations);

            final Tab connectionTab = createConnectionTab(tabContentController);

            try { 
                
                tabContentController.openConnection(params);
                session.setOpen(true);
                connectionSessions.put(session.getId(), session);
                profilerModule.postConnection(session, connectionTab);
                
                Platform.runLater(() -> {
                    tabPane.getTabs().add(connectionTab);
                    tabPane.getSelectionModel().select(connectionTab);
                    profilerModule.connectionOpened(params);
                });

            } catch (Exception ex) {
                ex.printStackTrace();
                DialogHelper.showError(MainController.this, "Error", ex.getMessage(), params, ex);
                session = null;
                dbservice = null;
                tabContentController = null;
                System.gc();System.gc();
            }
        }

        return session;
    }

    private Tab createConnectionTab(SmartProfilerTabContentController tabController) {
        Tab t = new Tab() {
            @Override
            public boolean equals(Object tab) {
                Label tabLabel = (Label) getGraphic();
                return tab instanceof Tab && tabLabel != null && tabLabel.getText() != null
                        && tabLabel.getText().equals(getTabText((Tab) tab));
                // return tab instanceof Tab && getText() != null && getText().equals(((Tab) tab).getText());
            }
            
            private String getTabText(Tab tab) {
                if (tab.getGraphic() instanceof Label) {
                    return ((Label)tab.getGraphic()).getText();
                }
                
                return tab.getText();
            }

            @Override
            public int hashCode() {
                int hash = 7;
                return hash;
            }
        };
        
        Label tabGraphic = new Label(tabController.getTabName());
        tabGraphic.getStyleClass().add("tab-label");
        t.setGraphic(tabGraphic);
        ((Label) t.getGraphic()).setText(tabController.getTabName()
                + " (" + tabController.getSession().getId() + ")");
        //}
        t.setId("tab" + tabController.getSession().getId());
        t.setUserData(tabController);
        t.setContent(tabController.getUI());
        return t;
    }
        
    public void shutdown() {
        aboutPage();
        for(ConnectionSession cs : connectionSessions.values()){
            disconnect(cs, DisconnectPermission.NONE);
        }
        connectionSessions.clear();
        categories.clear();
        customKeyCombinations.clear();
        keyCombinations.clear();
        profilerModule = null;
        ServiceEntityProviders.destroyCurrentEntityProvider();
        System.gc();
    }
    
     public Tab getTabBySession(ConnectionSession session) {
        if (session != null) {
            for (Tab t : tabPane.getTabs()) {
                if(t == mainNew){ continue; }
                if (t.getId().equals("tab" + session.getId())) {
                    return t;
                }
            }
        }
        return null;
    }
     
     private void refreshConnectionsUiState(){
         for(Node n : connectionsList.getChildren()){
             if(n.getUserData() != null){
                 ConnectionCardsController cardsController = (ConnectionCardsController) n.getUserData();
                 cardsController.setConnected(cardsController.getSession() != null && cardsController.getSession().isOpen());
             }
         }
     }
     
     private void retrieveConnectionsAndInstall(){  
         connectionsList.getChildren().clear();
         connectionsList.getChildren().add(newConnectionButton);
         
         Recycler.addWorkProc(new WorkProc() {
             ArrayList<ConnectionParams> list = new ArrayList(10);
            @Override
            public void updateUI() {
                for(ConnectionParams cp : list){
                    ConnectionCardsController cardsController = new ProfilerBaseControllerProvider<ConnectionCardsController>().get("ConnectionCards");
                    ConnectionSession tempSession = getOpennedSessionFromParams(cp);
                    if(tempSession == null){
                        cardsController.setParams(cp);
                        cardsController.setConnected(false);                        
                    }else{
                        cardsController.updateSession(tempSession);
                    }
                    cardsController.init();
                    connectionsList.getChildren().add(0, cardsController.getUI());
                }
                list.clear();
                list = null;
            }
            
            private ConnectionSession getOpennedSessionFromParams(ConnectionParams p){
                for(ConnectionSession session : connectionSessions.values()){
                    if(Objects.equals(p, session.getConnectionParam())){
                        return session;
                    }
                }
                return null;
            }

            @Override
            public void run() { 
                callUpdate = true;
                List<ConnectionParams> paramasList = ServiceEntityProviders.getInstance().getDb().getAll();
                for(ConnectionParams params : paramasList){
                    String paramModuleClass = params.getSelectedModuleClassName();
                    paramModuleClass = paramModuleClass.substring(paramModuleClass.lastIndexOf('.')+1);
                    if(paramModuleClass.equals("SmartProfilerModule")){
                        list.add(params);                        
                    }
                }
                paramasList.clear();
                paramasList = null;
            }
        });
     }
     
     public void triggerUpdateMechanism(){
         if(StandardApplicationManager.getInstance().getSmartsqlSavedData().getPackageType() == Flags.MODE_UPDATEABLE_PACKAGE){
             CheckForUpdatesController checkForUpdatesController = StandardBaseControllerProvider.getController(this, "CheckForUpdates");
             splashHolder.getChildren().add(checkForUpdatesController.getUI());
             HBox.setHgrow(checkForUpdatesController.getUI(), Priority.ALWAYS);
             checkForUpdatesController.init();
         }
     }

    @Override
    public SmartPropertiesData getSmartProperties() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
