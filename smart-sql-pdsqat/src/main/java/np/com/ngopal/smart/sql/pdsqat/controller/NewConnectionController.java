/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.pdsqat.controller;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.metrics.DiskIOMetricExtract;
import np.com.ngopal.smart.sql.structure.metrics.MemoryMetricExtract;
import np.com.ngopal.smart.sql.structure.metrics.MetricExtract;
import np.com.ngopal.smart.sql.structure.metrics.MetricListener;
import np.com.ngopal.smart.sql.structure.metrics.MetricTypes;
import static np.com.ngopal.smart.sql.structure.metrics.MetricTypes.DISK_IO_METRIC;
import static np.com.ngopal.smart.sql.structure.metrics.MetricTypes.MEMORY_METRIC;
import np.com.ngopal.smart.sql.structure.metrics.RemoteMetricExtractor;
import np.com.ngopal.smart.sql.pdsqat.modules.SmartProfilerModule;
import np.com.ngopal.smart.sql.pdsqat.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class NewConnectionController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private ComboBox connectionTypeComboBox;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab mySqlTab;

    @FXML
    private TextField address;

    @FXML
    private TextField user;

    @FXML
    private TextField port;

    @FXML
    private PasswordField pass;

    @FXML
    private CheckBox savePasswordCheckBox;

    @FXML
    private Tab sshTab;

    @FXML
    private CheckBox useSshTunnelingCheckBox;

    @FXML
    private GridPane sshTunnelingPane;

    @FXML
    private Label sshPasswordLabel;

    @FXML
    private TextField sshHost;

    @FXML
    private TextField sshPort;

    @FXML
    private TextField sshUser;

    @FXML
    private PasswordField sshPassword;

    @FXML
    private CheckBox sshSavePasswordCheckBox;

    @FXML
    private RadioButton sshPasswordTypeRadioButton;

    @FXML
    private RadioButton sshPrivateKeyTypeRadioButton;

    @FXML
    private TextField sshPrivateKeyField;

    @FXML
    private Label sshPrivateKeyLabel;

    @FXML
    private Button sshOpenPrivateKeyButton;

    @FXML
    private Tab osTab;

    @FXML
    private TextField osHost;

    @FXML
    private TextField osPort;

    @FXML
    private TextField osUser;

    @FXML
    private PasswordField osPassword;

    @FXML
    private CheckBox osSavePasswordCheckBox;

    @FXML
    private CheckBox osUsePasswordCheckbox;

    @FXML
    private CheckBox osUsePrivateKeyCheckbox;

    @FXML
    private TextField osPrivateKeyField;

    @FXML
    private Button osOpenPrivateKeyButton;

    @FXML
    private TextField osDiskSizeField;

    @FXML
    private TextField osRamSizeField;

    @FXML
    private Button newBttn;

    @FXML
    private Button cloneBttn;

    @FXML
    private Button saveBttn;

    @FXML
    private Button renameBttn;

    @FXML
    private Button deleteBttn;

    @FXML
    private Button connectBttn;

    @FXML
    private Button testBttn;

    @FXML
    private Button cancelBttn;

    @FXML
    private TextField connectionNameTextfield;

    private SimpleObjectProperty<ConnectionParams> currentParams = new SimpleObjectProperty();

    private boolean hasChanges = false;

    @Setter
    @Getter
    private boolean dialogMode = false;

    private volatile boolean triggerListeners = true;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        ((ImageView) cancelBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCancelButton_image());
        ((ImageView) testBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getTestConnectionButton_image());
        ((ImageView) connectBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getConnectionButton_image());
        ((ImageView) deleteBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getDeleteButton_image());
        ((ImageView) renameBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRenameButton_image());
        ((ImageView) saveBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getSaveButton_image());
        ((ImageView) cloneBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCloneButton_image());
        ((ImageView) newBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getNewButton_image());

        renameBttn.setVisible(false);

        connectionNameTextfield.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                connectionNameTextfield.setDisable(false);
            }
        });

        connectionNameTextfield.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

            }
        });

        connectionNameTextfield.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (isTriggeringListeners() && newValue != null && !newValue.isEmpty()) {
                    if (currentParams.get() == null) {
                        createNewConnectionParamsInstance(newValue.toLowerCase());
                    }
                    currentParams.get().setName(newValue);
                }
            }
        });

//        .textProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                if(isTriggeringListeners() && newValue != null && !newValue.isEmpty()){
//                    currentParams.get().set(newValue);
//                }
//            }
//        });
        address.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getAddress(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setAddress(newValue);
                    }
                });
        user.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getUsername(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setUsername(newValue);
                    }
                });

        pass.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getPassword(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setPassword(newValue);
                    }
                    if (newValue == null) {
                        savePasswordCheckBox.setSelected(false);
                    } else {
                        savePasswordCheckBox.setSelected(true);
                    }
                });

        port.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (isTriggeringListeners()) {
                if (newValue.matches("\\d*")) {
                    try {
                        int value = Integer.parseInt(newValue);
                        if (currentParams.get() != null && !Objects.equals(currentParams.get().getPort(), value)) {
                            setHasChanges(true);
                            currentParams.get().setPort(value);
                        }
                    } catch (NumberFormatException ex) {
                        if (currentParams.get() != null && !Objects.equals(currentParams.get().getPort(), 0)) {
                            setHasChanges(true);
                            currentParams.get().setPort(0);
                        }
                    }
                } else {
                    port.setText(oldValue);
                }
            }
        });

        sshHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getSshHost(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setSshHost(newValue);
                    }
                });

        sshPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners()) {
                        if (newValue.matches("\\d*")) {
                            try {
                                int port = Integer.parseInt(newValue);
                                if (currentParams.get() != null && !Objects.equals(currentParams.get().getSshPort(), port)) {
                                    setHasChanges(true);
                                    currentParams.get().setSshPort(port);
                                }
                            } catch (NumberFormatException ex) {
                                if (currentParams.get() != null && !Objects.equals(currentParams.get().getSshPort(), 0)) {
                                    setHasChanges(true);
                                    currentParams.get().setSshPort(0);
                                }
                            }
                        } else {
                            sshPort.setText(oldValue);
                        }
                    }
                });

        sshUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getSshUser(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setSshUser(newValue);
                    }
                });

        sshPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getSshPassword(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setSshPassword(newValue);
                    }

                    sshSavePasswordCheckBox.setSelected(pass != null);
                });

        useSshTunnelingCheckBox.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (isTriggeringListeners() && newValue != null) {
                        if (currentParams.get() != null && !Objects.equals(currentParams.get().isUseSshTunneling(), newValue)) {
                            setHasChanges(true);
                            currentParams.get().setUseSshTunneling(newValue);
                        }
                    }
                });

        sshPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getSshPrivateKey(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setSshPrivateKey(newValue);
                    }
                });

        sshPrivateKeyTypeRadioButton.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (isTriggeringListeners() && newValue != null) {
                        if (currentParams.get() != null && !Objects.equals(currentParams.get().isUseSshPrivateKey(), newValue)) {
                            setHasChanges(true);
                            currentParams.get().setUseSshPrivateKey(newValue);
                        }
                    }
                });

        sshPasswordTypeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable,
                Boolean oldValue, Boolean newValue) -> {
            if (isTriggeringListeners()) {
                sshPasswordLabel.setText(newValue != null && newValue ? "Password" : "Passphrase");
                sshSavePasswordCheckBox.setText(newValue != null && newValue ? "Save Password" : "Save Passphrase");
            }
        });

        osHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getOsHost(), newValue)) {
                        currentParams.get().setOsHost(newValue);
                    }
                });

        osPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners()) {
                        if (newValue.matches("\\d*")) {
                            try {
                                int port = Integer.parseInt(newValue);
                                if (currentParams.get() != null && !Objects.equals(currentParams.get().getOsPort(), port)) {
                                    setHasChanges(true);
                                    currentParams.get().setOsPort(port);
                                }
                            } catch (NumberFormatException ex) {
                                if (currentParams.get() != null && !Objects.equals(currentParams.get().getOsPort(), 0)) {
                                    setHasChanges(true);
                                    currentParams.get().setOsPort(0);
                                }
                            }
                        } else {
                            sshPort.setText(oldValue);
                        }
                    }
                });

        osUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getOsUser(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setOsUser(newValue);
                    }
                });

        osDiskSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getOsMetricDiskSize(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setOsMetricDiskSize(newValue);
                    }
                });

        osRamSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getOsMetricRamSize(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setOsMetricRamSize(newValue);
                    }
                });

        osPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getOsPassword(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setOsPassword(newValue);
                    }

                    osSavePasswordCheckBox.setSelected(pass != null);
                });

        osUsePasswordCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePrivateKeyCheckbox.setSelected(false);
                }
                if (isTriggeringListeners() && currentParams.get() != null && newValue != null) {
                    setHasChanges(true);
                    currentParams.get().setOsUsePassword(newValue);
                }
            }
        });

        osUsePrivateKeyCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePasswordCheckbox.setSelected(false);
                }
            }
        });

        osPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (isTriggeringListeners() && currentParams.get() != null && !Objects.equals(currentParams.get().getOsPrivateKey(), newValue)) {
                        setHasChanges(true);
                        currentParams.get().setOsPrivateKey(newValue);
                    }
                });

        cloneBttn.disableProperty().bind(connectionNameTextfield.textProperty().isEmpty());
//        connectionTypeComboBox.disableProperty().bind(cloneBttn.disableProperty());
        connectBttn.getParent().disableProperty().bind(cloneBttn.disableProperty());
        deleteBttn.disableProperty().bind(cloneBttn.disableProperty());
        tabPane.disableProperty().bind(cloneBttn.disableProperty());
    }

    public void setAndUpdateParams(ConnectionParams params) {
        currentParams.set(params);
        updateFields(params);
    }

    @FXML
    void newBttnAction() {
        createNewConnectionParamsInstance("<New Connection Params>");
    }

    private void createNewConnectionParamsInstance(String nameOfParam) {
        ConnectionParams params = ServiceEntityProviders.getInstance().getNewConnectionParams();
        params.setName(nameOfParam);
        params.setSelectedModuleClassName(SmartProfilerModule.class.getName());
        setHasChanges(true);
        setAndUpdateParams(params);
    }

    @FXML
    void cloneBttnAction() {
        if (currentParams != null) {
            String result = DialogHelper.showInput(getController(), "New Connection", "Enter New Connection Name", "Name:", currentParams.getName() + "_New", getStage());
            if (result != null && !result.isEmpty()) {

                if (ServiceEntityProviders.getInstance().getDb().checkConnectionParamsName(result)) {
                    DialogHelper.showError(getController(), "Error", "Input name already exist!", null, null, getStage());
                    return;
                }

                ConnectionParams p = currentParams.get().copyOnlyParams();
                p.setName(result);

                setAndUpdateParams(p);
            }
        }
    }

    @FXML
    void saveActionBttn() {
        if (currentParams.get() != null&& saveConnection() != null) {
                DialogHelper.showInfo(getController(), "Info", "Successfully saved", null, getStage());
            }
    }

    @FXML
    void renameBttnAction() {
//        ConnectionParams params = savedConnectionComboBox.getSelectionModel().getSelectedItem();
//        if (params != null) {
//            String result = DialogHelper.showInput(getController(), "Rename Connection", "Enter New Connection Name", "Name:", params.getName(), getStage());
//            if (result != null && !result.isEmpty()) {
//
//                ConnectionParams cp = ServiceEntityProviders.getInstance().getDb().findConenctionParamsByName(result);
//                if (cp != null && !cp.getId().equals(params.getId())) {
//                    DialogHelper.showError(getController(), "Error", "Input name already exist!", null, null, getStage());
//                    return;
//                }
//
//                params.setName(result);
//
//                DialogResponce dr = DialogHelper.showConfirm(getController(), "ALERT DIALOG", "Do you wan to save changes??");
//                if (dr == DialogResponce.OK_YES) {
//                    ServiceEntityProviders.getInstance().getDb().update(params);
//                }
//            }
//        }
    }

    @FXML
    void deleteBttnAction() {

        DialogResponce result = DialogHelper.showConfirm(getController(), "Delete connection", "Do you realy want to delete the connection detail?", getStage(), false);

        if (result == DialogResponce.OK_YES) {
            if (currentParams.get() != null && ServiceEntityProviders.getInstance().getDb().delete(currentParams.getValue())) {
                DialogHelper.showInfo(getController(), "Info", "Sucessfully deleted '" + currentParams.getName() + "'", null, getStage());
            }
        }
    }

    @FXML
    void connectBttnAction() {
        if (currentParams.getValue() != null) {
            if (hasChanges) {
                DialogResponce dr = DialogHelper.showConfirm(getController(), "Changes Action", "Would you like to save your new changes?", getStage(), false);
                if (dr == DialogResponce.OK_YES) {

                    Long id = saveConnection();
                    if (id == null) {
                        DialogHelper.showError(getController(), "SAVE ERROR", "Could not save detected changes \n Please Try again with the save button", null, null, getStage());
                        return;
                    }
                } else if (dr == DialogResponce.CANCEL) {
                    return;
                }
            }

            ConnectionSession createdSession = ((MainController) getController()).createConnectionSession(currentParams.getValue());
            if(createdSession != null && isDialogMode()){
                getStage().hide();
            }
        }        
    }

    @FXML
    void testBttnAction() {
        makeBusy(true);
        if (hasChanges) {
            DialogResponce dr = DialogHelper.showConfirm(getController(),
                    "Changes Action", "Would you like to save your new changes?", getStage(), false);
            if (dr == DialogResponce.OK_YES) {
                saveConnection();
            } else if (dr == DialogResponce.CANCEL) {
                makeBusy(false);
                return;
            }
        }

        WorkProc<ConnectionParams> testConnection = new WorkProc<ConnectionParams>() {

            SQLException exception;
            String productInfo;

            BigDecimal totalMemory;
            BigDecimal totalDisk;

            ConnectionParams params = currentParams.getValue();

            @Override
            public void updateUI() {

                if (exception != null) {
                    DialogHelper.showError(getController(), "Error", "Error on test", exception, params, getStage());
                } else {
                    boolean isOsTab = tabPane.getSelectionModel().getSelectedItem() == osTab;

                    if (isOsTab && totalMemory != null && totalMemory.compareTo(BigDecimal.ZERO) > 0
                            && totalDisk != null && totalDisk.compareTo(BigDecimal.ZERO) > 0) {

                        osDiskSizeField.setText(totalDisk.toString());
                        osRamSizeField.setText(totalMemory.toString());

                        DialogHelper.showInfo(getController(), "Info", "Connetion successful and remote metrics are updated!\n" + productInfo, null, getStage());
                    } else if (isOsTab && params.isRemote() && params.hasOSDetails()) {
                        DialogHelper.showInfo(getController(), "Info", "Connetion successful, but can't get os metrics with current OS details!\n" + productInfo, null, getStage());
                    } else {
                        DialogHelper.showInfo(getController(), "Info", "Connetion successful!\n" + productInfo, null, getStage());
                    }
                }
                makeBusy(false);
            }

            @Override
            public void run() {
                try {
                    makeBusy(true);
                    productInfo = ServiceEntityProviders.getInstance().getGeneralMysqlDBService().testConnection(params);
                    if (tabPane.getSelectionModel().getSelectedItem() == osTab) {

                        if (params.isRemote() && params.hasOSDetails()) {

                            final RemoteMetricExtractor remoteMetricExtractor = new RemoteMetricExtractor(params, MetricTypes.MEMORY_METRIC, MetricTypes.DISK_IO_METRIC);
                            remoteMetricExtractor.connectAndExecuteServer();
                            remoteMetricExtractor.setMetricListener(new MetricListener() {

                                int checkBoth = 0;

                                @Override
                                public void onReceiveRemoteMetrics(MetricTypes mt, MetricExtract extract) {
                                    switch (mt) {
                                        case DISK_IO_METRIC:
                                            BigDecimal KB_TO_GB = new BigDecimal(1024 * 1024);
                                            totalDisk = new BigDecimal(((DiskIOMetricExtract) extract).getTotalSize()).divide(KB_TO_GB, 2, RoundingMode.HALF_UP);
                                            checkBoth++;
                                            break;
                                        case MEMORY_METRIC:
                                            BigDecimal B_TO_GB = new BigDecimal(1024 * 1024 * 1024);
                                            totalMemory = new BigDecimal(((MemoryMetricExtract) extract).getTotalMemory()).divide(B_TO_GB, 2, RoundingMode.HALF_UP);
                                            checkBoth++;
                                            break;
                                    }

                                    if (checkBoth == 2) {
                                        remoteMetricExtractor.stop();
                                    }
                                }

                                @Override
                                public void onDone() {
                                }
                            });

                        }
                    }
                } catch (SQLException ex) {
                    exception = ex;
                }
                callUpdate = true;
            }
        };

        Recycler.addWorkProc(testConnection);
    }

    @FXML
    void cancelBttnAction() {

    }

    @FXML
    void openPrivateKey() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PuTTY Private Key Files", "*.ppk"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showOpenDialog(getStage());
        if (file != null && sshTab.isSelected()) {
            sshPrivateKeyField.setText(file.getAbsolutePath());
        } else if (file != null && osTab.isSelected()) {
            osPrivateKeyField.setText(file.getAbsolutePath());
        }
    }

    public Long saveConnection() {

        if (currentParams != null) {
            if (!savePasswordCheckBox.isSelected()) {
                currentParams.getValue().setPassword("");
            }

            if (!sshSavePasswordCheckBox.isSelected()) {
                currentParams.getValue().setSshPassword("");
            }

            if (!osSavePasswordCheckBox.isSelected()) {
                currentParams.getValue().setOsPassword("");
            }

            setHasChanges(false);

            /**
             * here we are trying to get the new changes into the reference we
             * hold
             */
            ConnectionParams connectionParams = FunctionHelper.getConnectionParamsDuplicateInstance(
                    ServiceEntityProviders.getInstance().getDb(), currentParams.getValue());
            if (connectionParams == null) {
                connectionParams = ServiceEntityProviders.getInstance().getDb().save(currentParams.getValue());
            } else {
                connectionParams = ServiceEntityProviders.getInstance().getDb().update(currentParams.getValue());
            }
            //selectedConnParam.set(connectionParams);
            //we are re-selecting an already selected item in the combo box because it is binded 
            // to the selectionConnParam
            updateFields(currentParams.getValue());

            return connectionParams.getId();
        }

        return null;
    }

    private void updateFields(ConnectionParams params) {
        synchronized (NewConnectionController.this) {
            triggerListeners = false;
//            if (params != null && params.getConnectionType() != null) {
//                connectionTypeComboBox.getSelectionModel().select(params.getConnectionType());
//            } else {
//                connectionTypeComboBox.getSelectionModel().select(0);
//            }

            connectionNameTextfield.setText(params != null ? params.getName() : "");
            address.setText(params != null ? params.getAddress() : "");
            user.setText(params != null ? params.getUsername() : "");
            pass.setText(params != null ? params.getPassword() : "");
            port.setText(params != null ? params.getPort() + "" : "");

            // Ssh module
            sshHost.setText(params != null ? params.getSshHost() : "");
            sshPort.setText(params != null ? params.getSshPort() + "" : "");
            sshUser.setText(params != null ? params.getSshUser() : "");
            sshPassword.setText(params != null ? params.getSshPassword() : "");
            useSshTunnelingCheckBox.setSelected(params != null ? params.isUseSshTunneling() : false);
            sshPrivateKeyTypeRadioButton.setSelected(params != null && params.isUseSshPrivateKey());
            sshPasswordTypeRadioButton.setSelected(params == null || !params.isUseSshPrivateKey());
            sshPrivateKeyField.setText(params != null ? params.getSshPrivateKey() : "");

            // OS part
            osHost.setText(params != null ? params.getOsHost() : "");
            osPort.setText(params != null ? params.getOsPort() + "" : "");
            osUser.setText(params != null ? params.getOsUser() : "");
            osPassword.setText(params != null ? params.getOsPassword() : "");
            osUsePasswordCheckbox.setSelected(params != null && params.isOsUsePassword());
            osUsePrivateKeyCheckbox.setSelected(params != null && !params.isOsUsePassword());
            osPrivateKeyField.setText(params != null ? params.getOsPrivateKey() : "");

            osRamSizeField.setText(params != null ? params.getOsMetricRamSize() : "");
            osDiskSizeField.setText(params != null ? params.getOsMetricDiskSize() : "");
            triggerListeners = true;
        }
    }

    private void setHasChanges(boolean state) {
        hasChanges = state;
    }

    boolean isTriggeringListeners() {
        synchronized (NewConnectionController.this) {
            return triggerListeners;
        }
    }
}
