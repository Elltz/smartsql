/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.pdsqat.provider;

import javafx.scene.image.Image;
import lombok.AccessLevel;
import lombok.Getter;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Getter(AccessLevel.PUBLIC)
public class ProfilerImageProvider extends DefaultImageProvider {
    
    private static ProfilerImageProvider pip;
    
    public static ProfilerImageProvider getInstance(){
        if(pip == null){
            pip = new ProfilerImageProvider();
        }
        return pip;
    }

    private ProfilerImageProvider() { }    
    
    @Override
    public String getImagePathRoute() {
        return "/np/com/ngopal/smart/sql/pdsqat/ui/images/";
    }
    
    
    private Image profiler_logo_bg_image = new Image(getImagePathRoute() + "profiler_logo_bg.png", 22, 22, false, false);
    private Image home_image = new Image(getImagePathRoute() + "home.png", 30, 24, false, false);
    private Image edit_connection_image = new Image(getImagePathRoute() + "reduce.png", 22, 22, false, false);
    //private Image open_image = new Image(getImagePathRoute() + "open.png", 22, 22, false, false);
    //private Image open_image = new Image(getImagePathRoute() + "open.png", 22, 22, false, false);
    
}
