package np.com.ngopal.smart.sql.queryoptimizer.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class QORecommendation {
    private String database;
    private String query;
    private String smartSqlQuery;
    private String optimizedQuery;
    private boolean emptyWithOr = false;
    private boolean hasAnalyzerResults = false;
    private Long rowsMulti;
    private Long rowsSum;
    
    private QueryOptimizerUtil analyzer;

    private boolean hasCredits = true;

    private boolean error = false;

    private Map<String, List<String>> recommendations = new HashMap<>();
    private Map<String, List<QOIndexRecommendation>> indexes = new HashMap<>();

    public static QORecommendation emptyError(String query, String database) {
        QORecommendation c = new QORecommendation();
        c.database = database;
        c.query = query;
        c.recommendations.put(null, Arrays.asList(new String[] {"Unable to explain the query"}));
        c.error = true;

        return c;
    }
}
