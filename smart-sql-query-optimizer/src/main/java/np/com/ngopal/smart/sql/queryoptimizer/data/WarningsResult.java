
package np.com.ngopal.smart.sql.queryoptimizer.data;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class WarningsResult {
    
    private final String level;
    private final Long code;
    private final String message;
 
    public WarningsResult(String level, Long code, String message) {
        this.level = level;
        this.code = code;
        this.message = message;
    }
 
        
    public String getLevel() {
        return level;
    }
    
    
    public Long getCode() {
        return code;
    }
    
    public String getMessage() {
        return message;
    }
}