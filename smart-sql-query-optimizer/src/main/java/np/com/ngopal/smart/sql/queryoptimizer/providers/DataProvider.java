package np.com.ngopal.smart.sql.queryoptimizer.providers;

import java.sql.SQLException;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;
import np.com.ngopal.smart.sql.model.QueryResult;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface DataProvider {

    Object execute(String sql) throws SQLException;
    Object execute(String sql, QueryResult qr) throws SQLException;
    
    String getUsedDatabase();
        
    DBTable getCachedTable(String database, String table);
    Database getCachedDatabase(String database);
    
    boolean canExplainExtended();
    
    boolean hasCredits();
    
    Long getColumnCardinality(String database, String table, String column);    
    void saveCardinality(String database, String table, String column, Long cardinality);
        
    void removeRecommendations(String database, String query);
    void saveRecommendation(String database, String query, String table, String recommendation, String index, String alterQuery, String smartSqlQuery, String optimizedQuery, boolean emptyWithOr, boolean hasAnalyzerResults, String qoRecommentaion, String qoAlterQuery, Long rows, Long rowsMulti);
    
    QAPerformanceTuning savePerformanceTuning(String database, String table, String query, String recommendation, String sqlCommand, Long rows);
    
    void logError(String message, Throwable th);
    void logDebug(String message);
}
