package np.com.ngopal.smart.sql.queryoptimizer.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.utils.SqlUtils;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.query.clauses.ConditionClause;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.GroupByClause;
import np.com.ngopal.smart.sql.query.clauses.HavingClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.LimitClause;
import np.com.ngopal.smart.sql.query.clauses.OrderByClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.clauses.WhereClause;
import np.com.ngopal.smart.sql.query.commands.AbstractCommand;
import np.com.ngopal.smart.sql.query.commands.DeleteCommand;
import np.com.ngopal.smart.sql.query.commands.FromProvider;
import np.com.ngopal.smart.sql.query.commands.InsertCommand;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.query.commands.UpdateCommand;
import np.com.ngopal.smart.sql.queryoptimizer.providers.DataProvider;
/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class AnalyzerResult {
    
    
    private transient static final Pattern OPERATOR_PATTERN = Pattern.compile("(\\!=|>=|<=|=|<>|>|<|\\bis\\s+not\\b|\\bis\\b|\\bisnull\\b|\\bin\\b|\\bnot\\s+in\\b|\\blike\\b|\\bilike\\b|\\bbetween\\b|\\bnot\\b\\s+\\bbetween\\b)", Pattern.CASE_INSENSITIVE);    
    
            
    public transient static final String CONDITION_TYPE__CONDITION    = "condition";
    public transient static final String CONDITION_TYPE__JOIN         = "join" ;
    public transient static final String CONDITION_TYPE__FILTER       = "filter";
    
    public static final String PARTITION_METHOD__RANGE  = "RANGE";
    public static final String PARTITION_METHOD__HASH   = "HASH";
    public static final String PARTITION_METHOD__LIST   = "LIST";
    public static final String PARTITION_METHOD__KEY    = "KEY";
    
    public static final String PARTITION_EXPRESSION = "expression";
    public static final String PARTITION_METHOD = "method";
            
    private String variable;
    private Integer degree;
    private String type;
    private String query;
    private String optimizerQuery;
    private List<ColumnResult> columns;
    private FromResult from;
    private ConditionResult where;
    private List<ColumnResult> groupBy;
    private ConditionResult having;
    private List<ColumnResult> orderBy;
    private String limit = "";
    private List<RecommendationResult> recomendations;
    private String priority;
    
    private Map<String, List<Map<String, String>>> partitionData = new HashMap<>();
    
    private boolean ignoreJoin = false;
    
    private boolean distinct = false;
    
    private transient AnalyzerResult parent;
    
    private List<AnalyzerResult> children;

    private transient final List<DBTable> tableInfo = new ArrayList<>();
    private transient final Map<String, List<DBTable>> tableMap = new HashMap<>();
    
    private transient final Map<String, Long> cacheRows = new HashMap<>();
    
    public static final String NO_IMPROVEMENTS = "No improvements Identified";
    public static final String EMPTY_OR = "Empty OR";
    
    @Getter
    @Setter
    private boolean recommendationEmptyWithOR = false;
    
    @Getter
    @Setter
    private transient QueryOptimizerUtil queryAnalyzer;
        
    private transient DataProvider service;
    
    //select * from actor where actor_id= and first_name>10 and last_name < 4
    
    public AnalyzerResult(AnalyzerResult parent, DataProvider service, Database database, String variable, Integer degree, String type, String query, String optimizerQuery, boolean checkRecomendations, Map<String, List<Map<String, String>>> cachePartitionData) {
        this.variable = variable;
        this.degree = degree != null ? degree : 1;
        this.type = type;
        this.parent = parent;
        this.service = service;
                
        if (query.trim().endsWith(";")) {
            query = query.trim().substring(0, query.length() - 1);
        }
        
        this.query = query;  
        this.optimizerQuery = optimizerQuery;
        
        try {
            findData(database, query, checkRecomendations);
        } catch (Throwable ex) {
            log.error("Error", ex);
        }
                
        if (service != null) {
            
            List<String> needIndexes = new ArrayList<>();
            List<String> needPartition = new ArrayList<>();
                    
                    
            String multiQuery = "";
            List<String> alreadyTable = new ArrayList<>();
            for (DBTable table: tableInfo) {
                
                List<Index> l = database.getIndexCache(table.getName());
                if (l != null) {
                    table.setIndexes(l);
                } else {
                    multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_INDEX_FOR_TABLE), database.getName().toLowerCase(), table.getName().toLowerCase());
                    if (!multiQuery.trim().endsWith(";")) {
                        multiQuery += ";";
                    }
                    
                    needIndexes.add(table.getName().toLowerCase());
                }
                
                
                String tableName = table.getName().toLowerCase();
                if (cachePartitionData.containsKey(tableName)) {                    
                    List<Map<String, String>> list = new ArrayList<>(cachePartitionData.get(tableName));
                    partitionData.put(tableName, list);
                } else {
                    if (!alreadyTable.contains(table.getName())) {                
                        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_PARTITION), database.getName().toLowerCase(), table.getName().toLowerCase());
                        if (!multiQuery.trim().endsWith(";")) {
                            multiQuery += ";";
                        }
                        alreadyTable.add(table.getName());
                        needPartition.add(table.getName().toLowerCase());
                    }
                }
            }
            
            try {
                if (!multiQuery.trim().isEmpty()) {
                    boolean ignorePartition = false;

                    QueryResult qr = new QueryResult();
                    service.execute(multiQuery, qr);

                    Map<String, List<Index>> loadedIndexes = new HashMap<>();
                    Map<String, List<Map<String, String>>> localPartitionData = new HashMap<>();

                    
                    int resultIndex = 0;
                    alreadyTable = new ArrayList<>();
                    for (DBTable table: tableInfo) {

                        if (needIndexes.contains(table.getName().toLowerCase())) {
                            try {
                                ResultSet rs = (ResultSet) qr.getResult().get(resultIndex);
                                readIndexs(table, rs);
                                rs.close();

                                loadedIndexes.put(table.getName(), table.getIndexes());
                                resultIndex++;
                            } catch (Throwable ex) {
                            }
                        }

                        String tableName = table.getName().toLowerCase();
                        
                        if (needPartition.contains(tableName)) {
                            
                            if (!alreadyTable.contains(table.getName())) {      

                                List<Map<String, String>> list = partitionData.get(tableName);
                                if (list == null) {
                                    list = new ArrayList<>();
                                    partitionData.put(tableName, list);

                                    localPartitionData.put(tableName, list);
                                }

                                ResultSet rs = (ResultSet) qr.getResult().get(resultIndex);
                                resultIndex++;
                                
                                while (rs.next()) {

                                    String expression = rs.getString("PartitoinColumnName");
                                    if (expression != null) {

                                        Map<String, String> m = new HashMap<>();
                                        if (expression.startsWith("`")) {
                                            expression = expression.substring(1);
                                        }

                                        if (expression.endsWith("`")) {
                                            expression = expression.substring(0, expression.length() - 1);
                                        }

                                        m.put(PARTITION_EXPRESSION, expression);
                                        m.put(PARTITION_METHOD, rs.getString("PartitionName"));

                                        if (expression.contains("(") || expression.contains(")")) {
                                            // RULE: if NOT RANGE partition_express has "(" or ")" then ignore partition rules  ex: YEAR(joined)                        
                                            if (!PARTITION_METHOD__RANGE.equalsIgnoreCase(m.get(PARTITION_METHOD))) {
                                                ignorePartition = true;
                                                break;
                                            } else {
                                                m.put(PARTITION_EXPRESSION, expression.substring(expression.indexOf("(") + 1, expression.lastIndexOf(")")));
                                            }
                                        }

                                        list.add(m);
                                    }
                                }
                                rs.close();

                                alreadyTable.add(table.getName());
                            }
                        }
                    }

                    if (ignorePartition) {
                        partitionData.clear();
                    }

                    for (String key: loadedIndexes.keySet()) {
                        database.addIndexCache(key, loadedIndexes.get(key));
                    }

                    cachePartitionData.putAll(localPartitionData);
                }
                
            } catch (Throwable ex) {
                log.error("Error", ex);
            }
            
        }
    }
    
    public AnalyzerResult(String database, String tableName, String executedQuery) {
        this(database, tableName, executedQuery, NO_IMPROVEMENTS);
    }
    
    public AnalyzerResult(String database, String tableName, String executedQuery, String recommendation) {
        addRecomendation(database, tableName, recommendation, null, null, executedQuery, new ArrayList<>());
    }
    
    public boolean isPartition(String tableName) {
        return tableName != null && partitionData.get(tableName.toLowerCase()) != null && !partitionData.get(tableName.toLowerCase()).isEmpty();
    }
    
    public boolean isPartitionRange(String tableName, String columnName) {
        return checkMethod(tableName, columnName, PARTITION_METHOD__RANGE);
    }
    
    public boolean isPartitionHash(String tableName, String columnName) {
        return checkMethod(tableName, columnName, PARTITION_METHOD__HASH);
    }
    
    public boolean isPartitionList(String tableName, String columnName) {
        return checkMethod(tableName, columnName, PARTITION_METHOD__LIST);
    }
    
    public boolean isPartitionKey(String tableName, String columnName) {
        return checkMethod(tableName, columnName, PARTITION_METHOD__KEY);
    }
    
    public boolean isPartitionAny(String tableName, String columnName) {
        if (tableName != null && columnName != null) {
            List<Map<String, String>> list = partitionData.get(tableName.toLowerCase());
            if (list != null) {
                for (Map<String, String> m: list) {
                    if (columnName.equalsIgnoreCase(m.get(PARTITION_EXPRESSION))) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    public boolean hasIndexForTable(String table) {
        if (recomendations != null) {
            for (RecommendationResult r: recomendations) {
                if (r.getIndexName() != null && r.getTableName().equalsIgnoreCase(table)) {
                    return true;
                }
            }
        }
        
        return false;
    }
            
    public List<String> getPartitionColumns(String tableName) {
        
        Set<String> cs = new HashSet<>();
        if (tableName != null) {
            List<Map<String, String>> list = partitionData.get(tableName.toLowerCase());
            if (list != null) {
                for (Map<String, String> m: list) {
                    cs.add(m.get(PARTITION_EXPRESSION));
                }
            }
        }
        
        return new ArrayList(cs);
    }
    
    private boolean checkMethod(String tableName, String columnName, String method) {
        if (tableName != null && columnName != null) {
            List<Map<String, String>> list = partitionData.get(tableName.toLowerCase());
            if (list != null) {
                for (Map<String, String> m: list) {
                    if (columnName.equalsIgnoreCase(m.get(PARTITION_EXPRESSION))) {
                        if (method.equalsIgnoreCase(m.get(PARTITION_METHOD))) {
                            return true;
                        }
                    }
                }
            }
        }
        
        return false;
    }
    
    
    List<List<DBTable>> tables() {
        List<List<DBTable>> tables = new ArrayList<>();
        tables.addAll(tableMap.values());
        if (parent != null) {
            tables.addAll(parent.tables());
        }
        
        return tables;
    }
    
    List<DBTable> findTables(String table) {
        if (tableMap != null) {
            
            List<DBTable> tables = null;
            for (String t: tableMap.keySet()) {
                if (t.equalsIgnoreCase(table)) {
                    tables = tableMap.get(t);
                }
            }
            
            if (parent != null) {
                List<DBTable> list = parent.findTables(table);
                if (list != null) {
                    if (tables != null) {
                        tables.addAll(list);
                    } else {
                        tables = list;
                    }
                }
            }

            return tables;
        }
        
        return null;
    }
    
    private void readIndexs(DBTable table, ResultSet rs) throws SQLException {
        List<Index> indexes = new ArrayList();

        List<Column> primaryColumns = new ArrayList();
        Map<String, List<Column>> uniqueIndexes = new LinkedHashMap<>();
        Map<String, List<Column>> fullTextIndexes = new LinkedHashMap<>();
        Map<String, List<Column>> keyIndexes = new LinkedHashMap<>();
        
        Map<String, Long> seqInIndexMap = new HashMap<>();
        Map<String, Long> cardinalityInIndexMap = new HashMap<>();
        Map<String, Integer> columnLengthMap = new HashMap<>();

        while (rs.next()) {

            String keyName = rs.getString("Key_name");
            String indexType = rs.getString("Index_type");
            String columnName = rs.getString("Column_name");

            Long nonUnique = rs.getLong("Non_unique");

            seqInIndexMap.put(columnName, rs.getLong("Seq_in_index"));
            cardinalityInIndexMap.put(columnName, rs.getLong("Cardinality"));
            columnLengthMap.put(columnName, rs.getInt("Sub_part"));

            Column column = table.getColumnByName(columnName);

            if ("PRIMARY".equals(keyName)) {
                primaryColumns.add(column);

            } else if ("FULLTEXT".equals(indexType)){
                List<Column> fulltextColumns = fullTextIndexes.get(keyName);
                if (fulltextColumns == null) {
                    fulltextColumns = new ArrayList<>();
                    fullTextIndexes.put(keyName, fulltextColumns);
                }

                fulltextColumns.add(column);

            } else if (nonUnique == 0) {
                // its unique columns
                List<Column> uniqueColumns = uniqueIndexes.get(keyName);
                if (uniqueColumns == null) {
                    uniqueColumns = new ArrayList<>();
                    uniqueIndexes.put(keyName, uniqueColumns);
                }

                uniqueColumns.add(column);
            } else {
                List<Column> keyColumns = keyIndexes.get(keyName);
                if (keyColumns == null) {
                    keyColumns = new ArrayList<>();
                    keyIndexes.put(keyName, keyColumns);
                }

                keyColumns.add(column);
            }
        }

        if (!primaryColumns.isEmpty()) {
            indexes.add(new Index(IndexType.PRIMARY.getDisplayValue().toUpperCase(), table, primaryColumns, IndexType.PRIMARY, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
        }

        if (!uniqueIndexes.isEmpty()) {
            for (Map.Entry<String, List<Column>> entry: uniqueIndexes.entrySet()) {
                indexes.add(new Index(entry.getKey(), table, entry.getValue(), IndexType.UNIQUE_TEXT, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
            }                
        }

        if (!fullTextIndexes.isEmpty()) {
            for (Map.Entry<String, List<Column>> entry: fullTextIndexes.entrySet()) {
                indexes.add(new Index(entry.getKey(), table, entry.getValue(), IndexType.FULL_TEXT, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
            }                
        }

        if (!keyIndexes.isEmpty()) {
            for (Map.Entry<String, List<Column>> entry: keyIndexes.entrySet()) {
                indexes.add(new Index(entry.getKey(), table, entry.getValue(), null, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
            }                
        }

        table.setIndexes(indexes);
    }
    
    public boolean isIgnoreJoin() {
        return ignoreJoin;
    }
    
    public boolean isDistinct() {
        return distinct;
    }
    
    public String getVariable() {
        return variable;
    }

    public Integer getDegree() {
        return degree;
    }
    
    public String getType() {
        return type;
    }
    
    public String getDegreeString() {
        return degree != null ? degree + "" : "";
    }
            
    public String getQuery() {
        return query;
    }
    
    public String getOptimizedQuery() {
        return optimizerQuery;
    }

    public List<ColumnResult> getColumns() {
        return columns;
    }        

    public FromResult getFrom() {
        return from;
    }

    public ConditionResult getWhere() {
        return where;
    }

    public List<ColumnResult> getGroupBy() {
        return groupBy;
    }
    
    public void clearGroupBy() {
        if (groupBy != null) {
            groupBy.clear();
        }
    }

    public ConditionResult getHaving() {
        return having;
    }

    public List<ColumnResult> getOrderBy() {
        return orderBy;
    }
    
    public void clearOrderBy() {
        if (orderBy != null) {
            orderBy.clear();
        }
    }

    public String getLimit() {
        return limit;
    }

    public List<AnalyzerResult> getChildren() {
        return children;
    }
    
    public void addChild(AnalyzerResult child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        
        children.add(child);
    }
    
    public List<RecommendationResult> getRecomendations() {
        return recomendations;
    }
    
    public int getRecomendationsSize() {
        return recomendations == null ? 0 : recomendations.size();
    }
    
    public boolean hasIndexRecomendationsSize() {
        if (recomendations != null && !recomendations.isEmpty()) {
            for (RecommendationResult rr: recomendations) {
                if (rr.getSqlCommand() != null) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public void addRecomendation(String databaseName, String tableName, String recommendation, String indexName, String sqlCommand, String executedSqlCommand, List<Column> columns) {
        addRecomendation(false, databaseName, tableName, recommendation, indexName, sqlCommand, executedSqlCommand, columns);
    }
    
    public void addRecomendation(boolean empty, String databaseName, String tableName, String recommendation, String indexName, String sqlCommand, String executedSqlCommand, List<Column> columns) {
        addRecomendation(empty, databaseName, tableName, recommendation, indexName, sqlCommand, executedSqlCommand, null, columns);
    }
    
    public static RecommendationResult createRecomendation(AnalyzerResult analyzerResult, boolean empty, String databaseName, String tableName, String recommendation, String indexName, String sqlCommand, String executedSqlCommand, String optimizerQuery, String note, List<Column> columns) {
        return new RecommendationResult(analyzerResult, databaseName, tableName, recommendation, indexName, sqlCommand, executedSqlCommand, optimizerQuery, note, null, columns, null, empty);
    }
    
    public void addRecomendation(boolean empty, String databaseName, String tableName, String recommendation, String indexName, String sqlCommand, String executedSqlCommand, String note, List<Column> columns) {
        if (recomendations == null) {
            recomendations = new ArrayList<>();
        }
        
        RecommendationResult r = new RecommendationResult(this, databaseName, tableName, recommendation, indexName, sqlCommand, executedSqlCommand, optimizerQuery, note, null, columns, null, empty);
        if (!containsRecommendation(r)) {
            
            boolean noNeedAdd = false;
            List<RecommendationResult> forDelete = new ArrayList<>();
            
            for (RecommendationResult r0: recomendations) {
                if (r0.include(r)) {
                    noNeedAdd = true;
                    forDelete.clear();
                    break;
                }
                
                if (r.include(r0)) {
                    forDelete.add(r0);
                }
            }
            if (!noNeedAdd) {
                recomendations.add(r);
            }
            
            recomendations.removeAll(forDelete);
        }
    }
    
    protected boolean containsRecommendation(RecommendationResult r) {
        AnalyzerResult arParent = this;
        while (arParent.parent != null) {
            arParent = arParent.parent;
        }
        
        return checkAnalyzeResult(arParent, r);
    }
    
    protected boolean checkAnalyzeResult(AnalyzerResult arParent, RecommendationResult r) {
        boolean contains = false;
        
        if (arParent.recomendations != null){
            contains = arParent.recomendations.contains(r);
            if (!contains) {
                for (RecommendationResult rr: arParent.recomendations) {
                    if (rr.include(r)) {
                        return true;
                    }
                    
                    if (r.include(rr)) {
                        arParent.recomendations.remove(rr);
                        break;
                    }
                }
            }
        }
        
        if (!contains && arParent.getChildren() != null) {
            
            for (AnalyzerResult ar: arParent.getChildren()) {
                contains = checkAnalyzeResult(ar, r);
                if (contains) {
                    break;
                }
            }
        }
        
        return contains;
    }
    
    public RecommendationResult createEmptyRecomendation(String query) {
        return new RecommendationResult(null, null, null, null, null, null, query, null, null, null, new ArrayList<>(), null, false);
    }
    
    public void putCachedRows(String tableName, Long rows) {
        if (tableName != null) {
            List<DBTable> tables = findTables(tableName);
            if (tables != null && !tables.isEmpty()) {
                cacheRows.put(tables.get(0).getName().toLowerCase(), rows);
            } else {
                cacheRows.put(tableName.toLowerCase(), rows);
            }
        }
    }
    
    public Long getCachedRows(String tableName) {
        
        Long result = cacheRows.get(tableName.toLowerCase());
        if (result == null && children != null) {
            for (AnalyzerResult ar: children) {
                result = ar.getCachedRows(tableName.toLowerCase());
                if (result != null) {
                    break;
                }
            }
        }
        
        return result;
    }
    
    public String getPriority() {
        return priority;
    }
    public void setPriority(String priority) {
        this.priority = priority;
    }
        
    
    
    private void findData(Database database, String query, boolean checkRecomendations) throws SQLException {
        
        // query can have /* comment */
        if (query != null) {
            query = query.replaceAll("/\\*.*?\\*/", "");
        }
        
        String str = query != null ? query.trim().toUpperCase() : null;        
        if (str != null && (str.startsWith("SELECT") || str.startsWith("UPDATE") || str.startsWith("INSERT") || str.startsWith("DELETE"))) {

            query = SqlUtils.replaceSubQueries(query != null ? query.trim() : "", " 1 ").toUpperCase();
            
            String[] split = SqlUtils.splitByUnion(query);
            if (split.length > 1) {
                query = split[0];
            }
            

            // TODO Need ALL mode to parsing in select command
            QueryClause command = QueryFormat.parseQuery(query);
            
            int distinctIndex = query.indexOf("DISTINCT");
            if (distinctIndex >= 0) {
                query = query.replaceAll("DISTINCT", "");
                distinct = true;
            }
            
            int fromIndex = query.indexOf("FROM");
            int whereIndex = query.indexOf("WHERE");
            int groupByIndex = query.indexOf("GROUP BY");
            int havingIndex = query.indexOf("HAVING");
            int orderByIndex = query.indexOf("ORDER BY");
            int limitIndex = query.indexOf("LIMIT");

            // search columns
//            int startIndex = str.startsWith("SELECT") ? "SELECT".length() : str.startsWith("UPDATE") ? "UPDATE".length() : str.startsWith("INSERT") ? "INSERT".length() : str.startsWith("DELETE") ? "DELETE".length() : 0;
//            int endIndex = query.length();
//            
//            if (fromIndex >= 0) {
//                endIndex = fromIndex;
//            } else if (whereIndex >= 0) {
//                endIndex = whereIndex;
//            } else if (groupByIndex >= 0) {
//                endIndex = groupByIndex;
//            } else if (havingIndex >= 0) {
//                endIndex = havingIndex;
//            } else if (orderByIndex >= 0) {
//                endIndex = orderByIndex;
//            } else if (limitIndex >= 0) {
//                endIndex = limitIndex;
//            }
            
//            columns = makeColumns(query.substring(startIndex, endIndex).trim(), false);
            columns = makeColumns(command, getColumnsSource(command), false, false, false);
            
            // find from
            if (fromIndex >= 0) {                
//                startIndex = fromIndex + "FROM".length();
//                endIndex = query.length();
//
//                if (whereIndex >= 0) {
//                    endIndex = whereIndex;
//                } else if (groupByIndex >= 0) {
//                    endIndex = groupByIndex;
//                } else if (havingIndex >= 0) {
//                    endIndex = havingIndex;
//                } else if (orderByIndex >= 0) {
//                    endIndex = orderByIndex;
//                } else if (limitIndex >= 0) {
//                    endIndex = limitIndex;
//                }
                
                List<TableResult> tableList = new ArrayList<>();
//                String fromSource = query.substring(startIndex, endIndex).trim();

                if (command instanceof FromProvider) {
                    FromClause fromClause = ((FromProvider)command).getFromClause();
                    if (fromClause.getTables() != null) {

                        for (TableReferenceClause trc: fromClause.getTables()) {

                            TableResult tr = parseTableResult(
                                    database, 
                                    trc.toString().toUpperCase(), 
                                    trc.getName() != null ? trc.getName().toUpperCase() : trc.getName(), 
                                    trc.getAlias() != null ? trc.getAlias().toUpperCase() : null);

                            tr.setJoins(parseJoinResults(command, database, trc.getJoinClauses()));

                            tableList.add(tr);                        
                        }
                    }
                    
//                    from = new FromResult(fromSource, tableList);
                    from = new FromResult(getFromSource(command), tableList);
                    boolean emptyJoin = true;
                    for (TableResult tr: from.getTables()) {
                        if (tr.getJoins() != null && !tr.getJoins().isEmpty()) {
                            emptyJoin = false;
                            break;
                        }
                    }
                    if (emptyJoin) {
                        ignoreJoin = true;
                    }
                }
            }
            

            // find where
            if (whereIndex >= 0) {                
//                startIndex = whereIndex + "WHERE".length();
//                endIndex = query.length();
//
//                if (groupByIndex >= 0) {
//                    endIndex = groupByIndex;
//                } else if (havingIndex >= 0) {
//                    endIndex = havingIndex;
//                } else if (orderByIndex >= 0) {
//                    endIndex = orderByIndex;
//                } else if (limitIndex >= 0) {
//                    endIndex = limitIndex;
//                }
//
//                if (query.trim().endsWith(";")) {
//                    endIndex--;
//                }
                
//                where = makeCondition(query.substring(startIndex, endIndex).trim());
                where = makeCondition(command, getWhereSource(command));
            }

            // find goup by
            if (groupByIndex >= 0) {                
//                startIndex = groupByIndex + "GROUP BY".length();
//                endIndex = query.length();
//
//                if (havingIndex >= 0) {
//                    endIndex = havingIndex;
//                } else if (orderByIndex >= 0) {
//                    endIndex = orderByIndex;
//                } else if (limitIndex >= 0) {
//                    endIndex = limitIndex;
//                }

//                groupBy = makeColumns(query.substring(startIndex, endIndex).trim(), true);
                groupBy = makeColumns(command, getGroupBySource(command), true, true, false);
            }

            // find having
            if (havingIndex >= 0) {                
//                startIndex = havingIndex + "HAVING".length();
//                endIndex = query.length();
//
//                if (orderByIndex >= 0) {
//                    endIndex = orderByIndex;
//                } else if (limitIndex >= 0) {
//                    endIndex = limitIndex;
//                }

//                having = makeCondition(query.substring(startIndex, endIndex).trim());
                having = makeCondition(command, getHavingSource(command));
            }

            // find order by
            if (orderByIndex >= 0) {                
//                startIndex = orderByIndex + "ORDER BY".length();
//                endIndex = query.length();
//
//                if (limitIndex >= 0) {
//                    endIndex = limitIndex;
//                }

//                orderBy = makeColumns(query.substring(startIndex, endIndex).trim(), true);
                orderBy = makeColumns(command, getOrderBySource(command), true, false, true);
            }

            // find limit
            if (limitIndex >= 0) {                
//                startIndex = limitIndex + "LIMIT".length();
//                endIndex = query.length();

//                limit = query.substring(startIndex, endIndex).trim();
                limit = getLimitSource(command);
            }
        }
    }
    
    private String getColumnsSource(QueryClause clause) {
        String columnSource = null;
        if (clause instanceof SelectCommand) {
            columnSource = ((SelectCommand)clause).getColumnSource();
            
        } else if (clause instanceof UpdateCommand) {
            columnSource = ((UpdateCommand)clause).getSetClause() != null ? ((UpdateCommand)clause).getSetClause().getSource() : null;
            
        } else if (clause instanceof DeleteCommand) {
            columnSource = null;
        
        } else if (clause instanceof InsertCommand) {
            if (((InsertCommand)clause).getIntoTable() != null && ((InsertCommand)clause).getIntoTable().getValuesClause() != null) {
                columnSource = String.join("),(", ((InsertCommand)clause).getIntoTable().getValuesClause().getValue());
            }
        }
        
        return columnSource != null ? columnSource.trim() : "";
    }
    
    private String getFromSource(QueryClause clause) {
        FromClause fromClause = null;
        if (clause instanceof SelectCommand) {
            fromClause = ((SelectCommand)clause).getFromClause();
            
        } else if (clause instanceof UpdateCommand) {
            fromClause = null;
            
        } else if (clause instanceof DeleteCommand) {
            fromClause = ((DeleteCommand)clause).getFromClause();
        
        } else if (clause instanceof InsertCommand) {
            fromClause = null;
        }
        
        return fromClause != null && fromClause.getSource() != null ? fromClause.getSource().trim() : "";
    }
    
    private String getWhereSource(QueryClause clause) {
        WhereClause whereClause = null;
        if (clause instanceof SelectCommand) {
            whereClause = ((SelectCommand)clause).getWhereClause();
            
        } else if (clause instanceof UpdateCommand) {
            whereClause = ((UpdateCommand)clause).getWhereClause();
            
        } else if (clause instanceof DeleteCommand) {
            whereClause = ((DeleteCommand)clause).getWhereClause();
        
        } else if (clause instanceof InsertCommand) {
            whereClause = null;
        }
        
        return whereClause != null && whereClause.getSource() != null ? whereClause.getSource().trim() : "";
    }
    
    private String getGroupBySource(QueryClause clause) {
        GroupByClause groupByClause = null;
        if (clause instanceof SelectCommand) {
            groupByClause = ((SelectCommand)clause).getGroupByClause();
            
        } else if (clause instanceof UpdateCommand) {
            groupByClause = null;
            
        } else if (clause instanceof DeleteCommand) {
            groupByClause = null;
        
        } else if (clause instanceof InsertCommand) {
            groupByClause = null;
        }
        
        return groupByClause != null && groupByClause.getSource() != null ? groupByClause.getSource().trim() : "";
    }
    
    private String getHavingSource(QueryClause clause) {
        HavingClause havingClause = null;
        if (clause instanceof SelectCommand) {
            havingClause = ((SelectCommand)clause).getHavingClause();
            
        } else if (clause instanceof UpdateCommand) {
            havingClause = null;
            
        } else if (clause instanceof DeleteCommand) {
            havingClause = null;
        
        } else if (clause instanceof InsertCommand) {
            havingClause = null;
        }
        
        return havingClause != null && havingClause.getSource() != null ? havingClause.getSource().trim() : "";
    }
    
    private String getOrderBySource(QueryClause clause) {
        OrderByClause orderByClause = null;
        if (clause instanceof SelectCommand) {
            orderByClause = ((SelectCommand)clause).getOrderByClause();
            
        } else if (clause instanceof UpdateCommand) {
            orderByClause = ((UpdateCommand)clause).getOrderByClause();
            
        } else if (clause instanceof DeleteCommand) {
            orderByClause = ((DeleteCommand)clause).getOrderByClause();
        
        } else if (clause instanceof InsertCommand) {
            orderByClause = null;
        }
        
        return orderByClause != null && orderByClause.getSource() != null ? orderByClause.getSource().trim() : "";
    }
    
    private String getLimitSource(QueryClause clause) {
        LimitClause limitClause = null;
        if (clause instanceof SelectCommand) {
            limitClause = ((SelectCommand)clause).getLimitClause();
            
        } else if (clause instanceof UpdateCommand) {
            limitClause = ((UpdateCommand)clause).getLimitClause();
            
        } else if (clause instanceof DeleteCommand) {
            limitClause = ((DeleteCommand)clause).getLimitClause();
        
        } else if (clause instanceof InsertCommand) {
            limitClause = null;
        }
        
        return limitClause != null && limitClause.getSource() != null ? limitClause.getSource().trim() : "";
    }
    
    // select actor_id, first_name as name, last_name last, ADDTIME('2007-12-31 23:59:59.999999', '1 1:1:1.000002') as date from actor
    private List<ColumnResult> makeColumns(QueryClause command, String text, boolean columnsAsDigit, boolean groupBy, boolean orderBy) {
        np.com.ngopal.smart.sql.query.clauses.ColumnResult columnResult = QueryFormat.parseColumnsResult(text);
        
        List<ColumnResult> result = new ArrayList<>();
        if (columnResult != null) {
            for (String s: columnResult.getColumns()) {
                String[] data = s.trim().split(" ");
                
                boolean descFound = false;
                
                String columnName = data[0].trim();
                String aliasName = null;
                if (data.length > 1) {
                    aliasName = data[data.length - 1].trim();
                    if ("ASC".equalsIgnoreCase(aliasName) || "DESC".equalsIgnoreCase(aliasName)) {
                        
                        descFound = "DESC".equalsIgnoreCase(aliasName);
                        
                        if (data.length > 2) {
                            aliasName = data[data.length - 2].trim();
                        } else {
                            aliasName = null;
                        }
                    }
                }
                
                Boolean isFunction = null;
                // check may its sub query or function
                String subQuery = columnResult.getSubQuery(QueryFormat.findSubQuery0Index(columnName));
                if (subQuery == null && command instanceof AbstractCommand) {
                    subQuery = ((AbstractCommand)command).getSubQuery(columnName);
                }
                
                if (subQuery != null) {
                    columnName = subQuery;
                } else {
                    String function = columnResult.getFunction(QueryFormat.findFunctionIndex(columnName));
                    if (function == null && command instanceof AbstractCommand) {
                        function = ((AbstractCommand)command).getFunction(columnName);
                    }
                    
                    if (function != null) {
                        columnName = function;
                        isFunction = true;
                    }
                }
                
                if (columnsAsDigit && columnName.matches("\\d+")) {
                    Integer columnIndex = Integer.valueOf(columnName);
                    if (columnIndex <= columns.size()) {
                        columnName = columns.get(columnIndex - 1).getColumnName();
                    }
                }
                
                Boolean f = descFound ? descFound : null;
                
                result.add(new ColumnResult(s, columnName, aliasName, isFunction, groupBy ? f : null, orderBy ? f : null));
            }
        }
        return result;
    }
    
    private ConditionResult makeCondition(QueryClause command, String text) {
        if (text.toLowerCase().matches(".*\\bcase\\b.*")) {
            ignoreJoin = true;
            return null;
        }
        
        text = text.trim();
        
        while (text.startsWith("(")) {
            boolean needBreak = false;
            int brachesDiff = 0;
            boolean found = false;
            for (int i = 0; i < text.length(); i++)
            {
                switch (text.charAt(i))
                {
                    case '(':
                        found = true;
                        brachesDiff++;
                        break;
                        
                    case ')':
                        if (found) {
                            brachesDiff--;
                        }
                        break;
                }
                
                // we find end of select query
                if (brachesDiff == 0 && found)
                {
                    if (i == text.length() - 1) {
                        text = text.substring(1, text.length() - 1);
                        break;
                    } else {
                        needBreak = true;
                        break;
                    }
                }
                
            }
            
            if (brachesDiff != 0 || needBreak) {
                break;
            }
        }
        
        np.com.ngopal.smart.sql.query.clauses.ConditionResult conditionResult = QueryFormat.parseConditionFilters(text);
                  
        return findConditionClause(command, conditionResult, conditionResult.getCondition(null));
    }
    
    //select * from actor WHERE first_name ='1222' or (first_name =last_name or actor_id > 2) and (first_name ='test' or last_name ='fff' and first_name =abs(last_name))
    private ConditionResult findConditionClause(QueryClause command, np.com.ngopal.smart.sql.query.clauses.ConditionResult conditionResult, List<String> conditions) {
        
        if (conditions != null) {
            // find operand
            for (int i = 1; i < conditions.size(); i += 2) {
                // need find OR it has hight priority
                if (ConditionClause.ConditionExpression.OR.name().equalsIgnoreCase(conditions.get(i))) {
                    
                    String source = String.join(" ", conditions);
                    ConditionResult cr = new ConditionResult(
                        source, 
                        CONDITION_TYPE__CONDITION, 
                        ConditionClause.ConditionExpression.OR.name(), 
                        findConditionClause(command, conditionResult, conditions.subList(0, i)),
                        null,
                        null,
                        findConditionClause(command, conditionResult, conditions.subList(i + 1, conditions.size())),
                        null,
                        null,
                        false,
                        false,
                        command);
                    
                    return cr;
                }
            }
            
            if (conditions.size() == 1) {
                String c = conditions.get(0);
                List<String> l = conditionResult.getCondition(c);
                if (l != null && !l.isEmpty()) {
                    if (l.size() > 1) {
                        return findConditionClause(command, conditionResult, l);
                    } else {
                        c = l.get(0);
                    }
                }
                
                String[] operands = splitByOperator(c);
                
                if ((operands == null || operands.length == 1) && c.startsWith("^")) {
                    // its sub condition
                    return findConditionClause(command, conditionResult, conditionResult.getCondition(c));
                } else {
                    
                    String fullTextColumns = null;
                    if (c.trim().matches("!\\d+\\s+!\\d+")) {
                        String[] functions = c.split("\\s+");
                        
                        Integer index = QueryFormat.findFunctionIndex(functions[0]);
                        String first = conditionResult.getFunction(index);
                        
                        index = QueryFormat.findFunctionIndex(functions[1]);
                        String second = conditionResult.getFunction(index);
                        
                        if (first.toLowerCase().startsWith("match(") && second.toLowerCase().startsWith("against(")) {
                            fullTextColumns = first.substring(6, first.length() - 1);
                        }                       
                    } else if (c.trim().toLowerCase().startsWith("match ")) {
                        String[] functions = c.trim().split("\\s+");
                        if (functions.length == 4) {

                            Integer index = null;
                            try {
                                index = Integer.valueOf(QueryFormat.findSubQueryIndex(functions[1]));
                            } catch (Throwable th) {}

                            String first = conditionResult.getSubCondition(index);

                            if (functions[0].toLowerCase().equalsIgnoreCase("match") && functions[2].toLowerCase().equalsIgnoreCase("against")) {
                                fullTextColumns = first;
                            }   
                        }
                    }
                    
                    if (fullTextColumns != null) {
                        return new ConditionResult(
                            c, 
                            CONDITION_TYPE__FILTER, 
                            "match", 
                            fullTextColumns, 
                            null,
                            null,
                            null,
                            null,
                            null,
                            false,
                            true,
                            command);
                    }
                    

                    Object right = null;
                    boolean isRightColumn = false;
                    boolean isRightFunction = false;

                    Object left = null;
                    boolean isLeftColumn = false;
                    boolean isLeftFunction = false;

                    String operand = null;

                    if (operands != null) {
                        if (operands.length > 0) {

                            String rightStr = operands[operands.length - 1].trim();
                            Column rightColumn = findColumn(this, rightStr);

                            if (rightColumn != null) {
                                right = rightStr;
                                isRightColumn = true;
                            } else {
                                if (rightStr.startsWith("!")) {
                                    Integer index = QueryFormat.findFunctionIndex(rightStr);
                                    right = conditionResult.getFunction(index);
                                    isRightFunction = true;
                                    
                                } else if (rightStr.startsWith("^")) {
                                    Integer index = QueryFormat.findSubQuery0Index(rightStr);
                                    right = conditionResult.getSubCondition(index);
                                    
                                } else if (rightStr.startsWith("#")) {
                                    String index = QueryFormat.findSubQueryIndex(rightStr);
                                    if (index != null) {
                                        right = conditionResult.getSubCondition(Integer.valueOf(index));
                                    }                                    
                                } else {
                                    right = rightStr;
                                }
                            }
                        }

                        if (operands.length > 1) {                        
                            operand = operands[operands.length - 2].trim();
                        }

                        if (operands.length > 2) {

                            String leftStr = operands[operands.length - 3].trim();
                            Column leftColumn = findColumn(this, leftStr);

                            if (leftColumn != null) {
                                left = leftStr;
                                isLeftColumn = true;
                            } else {
                                if (leftStr.startsWith("!")) {
                                    Integer index = QueryFormat.findFunctionIndex(leftStr);
                                    left = conditionResult.getFunction(index);
                                    isLeftFunction = true;
                                    
                                } else if (leftStr.startsWith("^")) {
                                    Integer index = QueryFormat.findSubQuery0Index(leftStr);
                                    left = conditionResult.getSubCondition(index);
                                    
                                } else if (leftStr.startsWith("#")) {
                                    String index = QueryFormat.findSubQueryIndex(leftStr);
                                    if (index != null) {
                                        left = conditionResult.getSubCondition(Integer.valueOf(index));
                                    }                                    
                                } else {
                                    left = leftStr;
                                }
                            }
                        }
                    } else {
                        left = c;
                    }

                    boolean ignore = !isLeftColumn && left != null && left.toString().trim().startsWith("'%") ||
                                     !isRightColumn && right != null && right.toString().trim().startsWith("'%");

                    return new ConditionResult(
                        c, 
                        isLeftColumn && isRightColumn ? CONDITION_TYPE__JOIN : CONDITION_TYPE__FILTER, 
                        operand, 
                        left, 
                        isLeftColumn ? true : null,
                        isLeftFunction ? true : null,
                        right,
                        isRightColumn ? true : null,
                        isRightFunction ? true : null,
                        ignore,
                        false,
                        command);
                }
            } else {
                // NO AND, than only OR
                return new ConditionResult(
                    String.join(" ", conditions), 
                    CONDITION_TYPE__CONDITION, 
                    ConditionClause.ConditionExpression.AND.name(), 
                    findConditionClause(command, conditionResult, conditions.subList(0, 1)),
                    null,
                    null,
                    findConditionClause(command, conditionResult, conditions.subList(2, conditions.size())),
                    null,
                    null,
                    false,
                    false,
                    command);
            }
        }
        
        return null;
    }
    
    private List<JoinResult> parseJoinResults(QueryClause command, Database database, List<JoinClause> joinClauses) throws SQLException {
        if (joinClauses != null) {
            
            List<JoinResult> result = new ArrayList<>();
            for (JoinClause joinClause: joinClauses) {
                TableReferenceClause trc = joinClause.getTable();

                TableResult tr = parseTableResult(database, trc.toString().toUpperCase(), trc.getName().toUpperCase(), trc.getAlias() != null ? trc.getAlias().toUpperCase() : null);
                tr.setJoins(parseJoinResults(command, database, trc.getJoinClauses()));

                result.add(new JoinResult(
                        joinClause.toString().toUpperCase(), 
                        joinClause.getType().name(), 
                        tr, 
                        joinClause.getCondition() != null ? makeCondition(command, joinClause.getCondition().toUpperCase()) : null, 
                        makeColumns(command, joinClause.getUsingColumns(), false, false, false)));
            }
            
            return result;
        }
        return null;
    }
    
    private TableResult parseTableResult(Database database, String source, String table, String alias) throws SQLException {
        String[] names = table.split("\\.");
        // last name is table name
        // before table name is database name
        String tableName = names[names.length - 1].trim();
        tableName = substringQuotes(tableName);
        
        alias = substringQuotes(alias);
        
        String databaseName = null;
        if (names.length > 1) {
            databaseName = names[names.length - 2];
            databaseName = substringQuotes(databaseName);
        }        
        
        DBTable t = service.getCachedTable(databaseName == null ? database.getName() : databaseName, tableName);//service.getTableSchemaInformation(databaseName, tableName);
        
        if (t != null && !tableInfo.contains(t)) {
            
            tableInfo.add(t);
            List<DBTable> list = findTables(tableName);
            if (list == null) {
                list = new ArrayList<>();
                tableMap.put(tableName, list);
            }
            
            if (!list.contains(t)) {
                list.add(t);
            }
            
            if (alias != null) {
                List<DBTable> list0 = findTables(alias);
                if (list0 == null) {
                    list0 = new ArrayList<>();
                    tableMap.put(alias, list0);
                }
                
                if (!list0.contains(t)) {
                    list0.add(t);
                }
            }
        }
                        
        return new TableResult(source, databaseName, tableName, alias, null);
    }
    
        
    public static String substringQuotes(String text) {
        if (text != null) {
            if (text.startsWith("`")) {
                text = text.substring(1);
            }
            if (text.endsWith("`")) {
                text = text.substring(0, text.length() - 1);
            }
        }
        
        return text;
    }
    
    
    public static boolean checkIndexes(AnalyzerResult analyzerResult, Column column) {
        for (DBTable tableInfo: analyzerResult.tableInfo) {
            if (tableInfo.getIndexes()!= null) {
                for (Index tableIndex: tableInfo.getIndexes()) {
                    if (tableIndex.getColumns() != null && tableIndex.getColumns().contains(column) && 
                        tableIndex.getSeqInIndexMap() != null && tableIndex.getSeqInIndexMap().get(column.getName()) == 1) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    public static Long getIndexCardinality(AnalyzerResult analyzerResult, Column column) {
        for (DBTable tableInfo: analyzerResult.tableInfo) {
            if (tableInfo.getIndexes()!= null) {
                for (Index tableIndex: tableInfo.getIndexes()) {
                    if (tableIndex.getColumns() != null && tableIndex.getColumns().contains(column) && 
                        tableIndex.getSeqInIndexMap() != null) {
                        
                        return tableIndex.getCardinalityInIndexMap().get(column.getName());
                    }
                }
            }
        }
        
        return null;
    }
    
    public static List<DBTable> findTables(AnalyzerResult analyzerResult, String tableName) {
        return analyzerResult != null ? analyzerResult.findTables(tableName) : null;
    }
    
    public static Column findColumn(AnalyzerResult analyzerResult, String column) {
        if (!analyzerResult.tables().isEmpty() && column != null) {
            column = column.trim();
            
            if (column.endsWith(";")) {
                column = column.substring(0, column.length() - 1);
            }
            
            String[] columnData = column.split("\\.");
                
            String databaseName = null;
            String tableName = null;
            String columnName = substringQuotes(columnData[columnData.length - 1]);
            if (columnData.length > 1) {
                tableName = substringQuotes(columnData[columnData.length - 2]);
            }
            if (columnData.length > 2) {
                databaseName = substringQuotes(columnData[columnData.length - 3]);
            }
            
            List<DBTable> listTables = new ArrayList<>();
            if (tableName != null) {
                List<DBTable> tables = analyzerResult.findTables(tableName);
                if (tables != null) {
                    listTables.addAll(tables);
                }
            } else {
                for (List<DBTable> tables: analyzerResult.tables()) {
                    if (tables != null) {
                        listTables.addAll(tables);
                    }
                }
            }
            
            for (DBTable table: listTables) {
                if ((databaseName == null || table.getDatabase().getName().equalsIgnoreCase(databaseName))) {
                    if (table.getColumns() != null) {
                        for (Column c: table.getColumns()) {
                            if (c.getName().equalsIgnoreCase(columnName.trim())) {
                                return c.isComumnNeedIgnoreForQA() ? null : c;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public static String findColumnTableName(AnalyzerResult analyzerResult, String column) {
        if (!analyzerResult.tables().isEmpty()) {
            column = column.trim();
            
            if (column.endsWith(";")) {
                column = column.substring(0, column.length() - 1);
            }
            
            String[] columnData = column.split("\\.");
                
            String databaseName = null;
            String tableName = null;
            String columnName = substringQuotes(columnData[columnData.length - 1]);
            if (columnData.length > 1) {
                tableName = substringQuotes(columnData[columnData.length - 2]);
            }
            if (columnData.length > 2) {
                databaseName = substringQuotes(columnData[columnData.length - 3]);
            }
            
            List<DBTable> listTables = new ArrayList<>();
            if (tableName != null) {
                return tableName;
            } else {
                for (List<DBTable> tables: analyzerResult.tables()) {
                    if (tables != null) {
                        listTables.addAll(tables);
                    }
                }
            }
            
            for (DBTable table: listTables) {
                if ((databaseName == null || table.getDatabase().getName().equals(databaseName))) {
                    if (table.getColumns() != null) {
                        for (Column c: table.getColumns()) {
                            if (c.getName().equalsIgnoreCase(columnName.trim())) {
                                return table.getName();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
    
    private String[] splitByOperator(String filter) {
        Matcher m = OPERATOR_PATTERN.matcher(filter);
        if (m.find()) {
            int start = m.start();
            int end = m.end();
            
            String left = filter.substring(0, start);
            String right = filter.substring(end);
            
            return new String[]{left, m.group(1).replaceAll("\\s+", " "), right};
        }
        
        return null;
    }
    @AllArgsConstructor
    @Getter
    @Setter
    public class FromResult {
        private String source;
        private List<TableResult> tables;
    }
    
    @AllArgsConstructor
    @Getter
    @Setter
    public class TableResult {
        private String source;
        private String databaseName;
        private String tableName;
        private String aliasName;        
        private List<JoinResult> joins;
    }
    
    @AllArgsConstructor
    @Getter
    @Setter
    public static class RecommendationResult {
        private transient AnalyzerResult parent;
        private String databaseName;
        private String tableName;
        private String recommendation;
        private String indexName;
        private String sqlCommand;
        private String executedSqlCommand;
        private String optimizerQuery;
        private String note;
        private Boolean executed;
        
        @Transient
        private transient List<Column> columns;
        @Transient
        private transient QAPerformanceTuning qAPerformanceTuning;
        
        private boolean empty = false;
        
        
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof RecommendationResult) {
                RecommendationResult rr = (RecommendationResult)obj;
                
                return Objects.equals(indexName, rr.indexName) && Objects.equals(tableName, rr.tableName) && Objects.equals(recommendation, rr.recommendation) && Objects.equals(sqlCommand, rr.sqlCommand) && Objects.equals(executedSqlCommand, rr.executedSqlCommand);
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            int hash = 5;
            hash = 89 * hash + java.util.Objects.hashCode(this.databaseName);
            hash = 89 * hash + java.util.Objects.hashCode(this.tableName);
            hash = 89 * hash + java.util.Objects.hashCode(this.indexName);
            hash = 89 * hash + java.util.Objects.hashCode(this.recommendation);
            hash = 89 * hash + java.util.Objects.hashCode(this.executedSqlCommand);
            hash = 89 * hash + java.util.Objects.hashCode(this.sqlCommand);
            return hash;
        }
        
        public boolean include(RecommendationResult r) {
            if (r.columns != null && columns != null) {
                if (r.columns.size() <= columns.size() && !r.columns.isEmpty() && !columns.isEmpty()) {
                    for (Column c: r.columns) {

                        boolean found = false;
                        for (Column c0: columns) {
                            if (c0.getTable().getName().equals(c.getTable().getName()) && c0.getName().equals(c.getName())) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {
                            return false;
                        }
                    }

                    return true;
                } else if (r.getTableName() != null && getTableName() != null && r.getTableName().equals(getTableName()) && r.getRecommendation().equals(getRecommendation())) {
                    return true;
                } else if (r.sqlCommand != null && r.sqlCommand.equals(sqlCommand)) {
                    return true;
                }
            } else if (r.recommendation != null && r.recommendation.equals(recommendation) || r.sqlCommand != null && r.sqlCommand.equals(sqlCommand)){
                return true;
            }
            
            return false;
        }
    }
    
    @AllArgsConstructor
    @Getter
    @Setter
    public class JoinResult {
        private String source;
        private String type;
        private TableResult table;        
        private ConditionResult on;
        private List<ColumnResult> using;
    }
    
    @AllArgsConstructor
    @Getter
    @Setter
    public class ColumnResult {
        private String source;
        private String columnName;
        private String aliasName;
        private Boolean isFunction;
        private Boolean isGroupDesc;
        private Boolean isOrderDesc;
        
        public Boolean getIsFunction() {
            return isFunction == null ? Boolean.FALSE : isFunction;
        }
        
        public Boolean getIsGroupDesc() {
            return isGroupDesc == null ? Boolean.FALSE : isGroupDesc;
        }
        
        public Boolean getIsOrderDesc() {
            return isOrderDesc == null ? Boolean.FALSE : isOrderDesc;
        }
        
    }
    
    @AllArgsConstructor
    @Getter
    @Setter
    public class ConditionResult {
        private String source;
        private String type; // condition, filter, joinFilter
        private String operand;
        
        private Object left;
        private Boolean isLeftColumn;
        private Boolean isLeftFunction;
        
        private Object right;
        private Boolean isRightColumn;
        private Boolean isRightFunction;
        
        private Boolean ignore = false;
        
        private Boolean isFullText;
        
        @Transient
        private QueryClause command;
        
        public Boolean getIsLeftColumn() {
            return isLeftColumn == null ? Boolean.FALSE : isLeftColumn;
        }
        
        public Boolean getIsLeftFunction() {
            return isLeftFunction == null ? Boolean.FALSE : isLeftFunction;
        }
        
        public Boolean getIsRightColumn() {
            return isRightColumn == null ? Boolean.FALSE : isRightColumn;
        }
        
        public Boolean getIsRightFunction() {
            return isRightFunction == null ? Boolean.FALSE : isRightFunction;
        }
        
        public Boolean getIgnore() {
            return ignore == null ? Boolean.FALSE : ignore;
        }
        
        public Boolean getIsFullText() {
            return isFullText == null ? Boolean.FALSE : isFullText;
        }
        
        // @TODO: Implement all functionality
        public String convert() {
            String result = "";
            if (left instanceof ConditionResult) {
                result += ((ConditionResult)left).convert();
            } else if (isLeftFunction != null && isLeftFunction) {
                result += ((AbstractCommand)command).getFunction(left.toString());
            } else {
                String r = left.toString().trim();
                if (r.startsWith("@")) {
                    r = "'" + ((AbstractCommand)command).getString(r) + "'";
                }
                result += r;
            }
            
            result += " " + operand + " ";
            
            if (right instanceof ConditionResult) {
                result += ((ConditionResult)right).convert();
            } else if (isRightFunction != null && isRightFunction) {
                result += ((AbstractCommand)command).getFunction(right.toString());
            } else {
                String r = right.toString().trim();
                if (r.startsWith("@")) {
                    r = ((AbstractCommand)command).getString(r);
                }
                result += r;
            }
            
            return result;
        }
    }
}