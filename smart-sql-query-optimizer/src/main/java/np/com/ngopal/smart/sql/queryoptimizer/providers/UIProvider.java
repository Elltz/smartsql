package np.com.ngopal.smart.sql.queryoptimizer.providers;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface UIProvider {

    boolean showConfirm(String message);
    
    void showInfo(String message);
}
