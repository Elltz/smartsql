package np.com.ngopal.smart.sql.queryoptimizer.data;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Getter
@Setter
public class QOIndexRecommendation {

    private String indexName;
    private String indexQuery;
    private String recommendation;
    private Long cachedRows;
}
