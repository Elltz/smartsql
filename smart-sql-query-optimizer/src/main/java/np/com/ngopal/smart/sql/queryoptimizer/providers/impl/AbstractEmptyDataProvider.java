package np.com.ngopal.smart.sql.queryoptimizer.providers.impl;

import java.sql.Timestamp;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;
import np.com.ngopal.smart.sql.queryoptimizer.providers.DataProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public abstract class AbstractEmptyDataProvider implements DataProvider {

    @Override
    public String getUsedDatabase() {
        return null;
    }

    @Override
    public Long getColumnCardinality(String database, String table, String column) {
        return null;
    }

    @Override
    public void saveCardinality(String database, String table, String column, Long cardinality) {
        // do nothing
    }

    @Override
    public void removeRecommendations(String database, String query) {
        // do nothing
    }

    @Override
    public void saveRecommendation(String database, String query, String table, String recommendation, String index, String alterQuery, String smartSqlQuery, String optimizedQuery, boolean emptyWithOr, boolean hasAnalyzerResult, String qoRecommentaion, String qoAlterQuery, Long rows, Long rowsMulti) {
        // do nothing
    }

    @Override
    public QAPerformanceTuning savePerformanceTuning(String database, String table, String query, String recommendation, String sqlCommand, Long rows) {
        
        Date currentDate = new Date();
        
        QAPerformanceTuning qa = new QAPerformanceTuning();
        qa.setDatabase(database);
        qa.setDbtable(table);
        qa.setQuery(query);            
        qa.setSqlIndexQuery(sqlCommand);
        qa.setRowsScanBefore(rows);
        qa.setCreateDate(new Timestamp(currentDate.getTime()));
        qa.setUsedCredit(sqlCommand != null && hasCredits());
        
        qa.setExecutingDateTime(new Timestamp(currentDate.getTime()));
        qa.setRowsScanAfter(null);
        qa.setRecommendation(recommendation);
        
        qa.setPerformanceImprovement(null);
        qa.setImprovementPercent(null);
        qa.setStatus("Note at created");
        
        return qa;
    }

    @Override
    public void logError(String message, Throwable th) {
        log.error(message, th);
    }

    @Override
    public void logDebug(String message) {
        log.debug(message);
    }
}
