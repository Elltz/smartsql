package np.com.ngopal.smart.sql.queryoptimizer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import np.com.ngopal.smart.sql.model.QAPerformanceTuning;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.ExplainResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.QOIndexRecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QueryOptimizerUtil;
import np.com.ngopal.smart.sql.queryoptimizer.providers.DataProvider;
import np.com.ngopal.smart.sql.queryoptimizer.providers.UIProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryOptimizer {

    
    public static QORecommendation analyzeQuery(DataProvider dataProvider, UIProvider uiProvider, String database, String query, int indexMaxNoCount, int indexSubstrLength, boolean withCache) throws SQLException {
        QueryOptimizerUtil util = new QueryOptimizerUtil(dataProvider, dataProvider.getCachedDatabase(database), query, indexMaxNoCount, indexSubstrLength);
        return analyze(dataProvider, util, uiProvider, withCache);
    }
    
    public static QORecommendation analyzeQuery(DataProvider dataProvider, UIProvider uiProvider, String database, String query, boolean withCache) throws SQLException {
        QueryOptimizerUtil util = new QueryOptimizerUtil(dataProvider, dataProvider.getCachedDatabase(database), query);
        return analyze(dataProvider, util, uiProvider, withCache);
    }
    
    public static QORecommendation analyzeQuery(DataProvider dataProvider, String database, String query) throws SQLException {
        return analyzeQuery(dataProvider, null, database, query, false);
    }
    
    public static QORecommendation analyzeQuery(DataProvider dataProvider, String database, String query, int indexMaxNoCount, int indexSubstrLength) throws SQLException {
        return analyzeQuery(dataProvider, null, database, query, indexMaxNoCount, indexSubstrLength, false);
    }
    
    private static QORecommendation analyze(DataProvider dataProvider, QueryOptimizerUtil util, UIProvider uiProvider, boolean withCache) throws SQLException {
        
        QORecommendation recommendation = new QORecommendation();
        recommendation.setAnalyzer(util);
        
        boolean stopedAnalyze = !util.analyze(uiProvider, withCache);
        if (!stopedAnalyze) {
            // clear old
            dataProvider.removeRecommendations(util.getDatabase().getName(), util.getQuery());
            
            recommendation.setSmartSqlQuery(util.getSmartMySQLRewrittenQuery());
            recommendation.setOptimizedQuery(util.getOptimizerQuery());
            recommendation.setQuery(util.getQuery());
            recommendation.setRowsMulti(util.getRowsMulty());
            recommendation.setRowsSum(util.getRowsSum());

            List<AnalyzerResult> analyzerResults = new ArrayList<>();
            if (!hasAnalyzerRows(util.getAnalyzerResult())) {

                String tableName = "UNKNOWN";
                List<ExplainResult> qrs = util.getvQueryExplainResults();
                if (qrs == null) {
                    qrs = util.getVNQueryExplainResults();
                }

                if (qrs != null && !qrs.isEmpty()) {
                    ExplainResult er = qrs.get(0);
                    tableName = er.getTable();
                }

                recommendation.setEmptyWithOr(checkEmptyWithOr(util.getAnalyzerResult()));
                recommendation.setHasAnalyzerResults(false);
                
                AnalyzerResult ar = recommendation.isEmptyWithOr() ? new AnalyzerResult(util.getDatabase().getName(), tableName, util.getQuery(), AnalyzerResult.EMPTY_OR) : new AnalyzerResult(util.getDatabase().getName(), tableName, util.getQuery());
                ar.setQueryAnalyzer(util);
                analyzerResults.add(ar);
                
            } else {
                
                recommendation.setEmptyWithOr(false);
                recommendation.setHasAnalyzerResults(true);

                analyzerResults.addAll(util.getAnalyzerResult());
            }
            
            // save static for all recommendations
            List<AnalyzerResult.RecommendationResult> listRecs = collectAnalyzerRows(analyzerResults);

            if (listRecs != null) {
                for (AnalyzerResult.RecommendationResult rr: listRecs) {
                    
                    if (rr.getDatabaseName() != null) {
                        if (rr.getRecommendation() != null && !rr.isEmpty()) { 
                            QAPerformanceTuning qat = dataProvider.savePerformanceTuning(
                                rr.getDatabaseName(),
                                rr.getTableName(),
                                util.getQuery(),
                                rr.getRecommendation(), 
                                rr.getSqlCommand(), 
                                rr.getParent().getCachedRows(rr.getTableName())
                            );

                            rr.setQAPerformanceTuning(qat);
                        }
                        
                        dataProvider.saveRecommendation(
                            rr.getDatabaseName(), 
                            util.getQuery(),
                            rr.getTableName(), 

                            rr.isEmpty() ? null : rr.getRecommendation(), 
                            rr.getIndexName(),
                            rr.isEmpty() ? null : rr.getSqlCommand(), 

                            recommendation.getSmartSqlQuery(),
                            recommendation.getOptimizedQuery(),
                            recommendation.isEmptyWithOr(),
                            recommendation.isHasAnalyzerResults(),

                            rr.getRecommendation(), 
                            rr.getSqlCommand(),
                            rr.getParent().getCachedRows(rr.getTableName()),
                            util.getRowsMulty()
                        );

                        if (!rr.isEmpty() && rr.getRecommendation() != null && !rr.getRecommendation().isEmpty()) {
                            List<String> recommendations = recommendation.getRecommendations().get(rr.getTableName());
                            if (recommendations == null) {
                                recommendations = new ArrayList<>();
                                recommendation.getRecommendations().put(rr.getTableName(), recommendations);
                            }

                            recommendations.add(rr.getRecommendation());
                        }

                        if (!rr.isEmpty() && rr.getSqlCommand() != null && !rr.getSqlCommand().isEmpty()) {
                            List<QOIndexRecommendation> indexes = recommendation.getIndexes().get(rr.getTableName());
                            if (indexes == null) {
                                indexes = new ArrayList<>();
                                recommendation.getIndexes().put(rr.getTableName(), indexes);
                            }

                            QOIndexRecommendation ir = new QOIndexRecommendation();
                            ir.setIndexName(rr.getIndexName());
                            ir.setIndexQuery(rr.getSqlCommand());
                            ir.setRecommendation(rr.getRecommendation());
                            ir.setCachedRows(rr.getParent().getCachedRows(rr.getTableName()));
                            indexes.add(ir);
                        }
                    }
                }
            }
        }
        
        return recommendation;
    }
    
    private static List<AnalyzerResult.RecommendationResult> collectAnalyzerRows(List<AnalyzerResult> list) {
        
        List<AnalyzerResult.RecommendationResult> result = new ArrayList<>();
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.getRecomendationsSize() > 0) {

                    for (AnalyzerResult.RecommendationResult rr: t.getRecomendations()) {
                        result.add(rr);
                    }
                }

                if (t.getChildren() != null) {
                    result.addAll(collectAnalyzerRows(t.getChildren()));
                } else {
                    result.add(t.createEmptyRecomendation(t.getQuery()));
                }
            }
        }
        
        return result;
    }
    
    private static boolean checkEmptyWithOr(List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.isRecommendationEmptyWithOR()) {
                    return true;
                }

                if (t.getChildren() != null){
                    if (checkEmptyWithOr(t.getChildren())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    
    private static boolean hasAnalyzerRows(List<AnalyzerResult> list) {
        if (list != null) {
            for (AnalyzerResult t: list) {
                if (t.getRecomendationsSize() > 0) {
                    return true;

                }

                if (t.getChildren() != null){
                    if (hasAnalyzerRows(t.getChildren())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    
}
