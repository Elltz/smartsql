
use smartMySQLSchemaDesign;

CREATE TABLE `Domain` (
    id BIGINT NOT NULL AUTO_INCREMENT, 
    `name` VARCHAR(255) NOT NULL, 
    PRIMARY KEY (id)
);

CREATE TABLE Subdomain (
    id BIGINT NOT NULL AUTO_INCREMENT, 
    `name` VARCHAR(255) NOT NULL, 
    PRIMARY KEY (id)
);


create table ERR_Diagram (
    id BIGINT not null AUTO_INCREMENT,
    `name` VARCHAR(100),
    diagram_data LONGTEXT,
    database_name VARCHAR(1000),
    domain_id BIGINT not null,
    subdomain_id BIGINT not null,
    description TEXT,
    user_id VARCHAR(100),
    primary key (id)
);

ALTER TABLE ERR_Diagram ADD FOREIGN KEY fk_domain_id(domain_id) REFERENCES `Domain`(id);
ALTER TABLE ERR_Diagram ADD FOREIGN KEY fk_subdomain_id(subdomain_id) REFERENCES `Subdomain`(id);

CREATE UNIQUE INDEX diagram_name_unique ON ERR_Diagram (`name`);

ALTER TABLE ERR_Diagram ADD COLUMN removed BOOLEAN DEFAULT FALSE;