/*
LOCK/UNLOCK Statement
Source : MySQL documentation
*/
LOCK TABLES tbl_name [ AS alias ] { READ | [ READ LOCAL ] | 
	[ LOW_PRIORITY ] WRITE }
	[ , tbl_name { READ | [ LOW_PRIORITY ] WRITE } ... ]
...
UNLOCK TABLES

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/