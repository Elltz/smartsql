/*
Create Function Statement
Source : MySQL documentation
*/
CREATE
[DEFINER = { user | CURRENT_USER }] 
 FUNCTION sp_name ([[ IN | OUT | INOUT ] param_name type[,...]])
    RETURNS type
    [characteristic ...] routine_body
 LANGUAGE SQL
  | [NOT] DETERMINISTIC
  | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
  | SQL SECURITY { DEFINER | INVOKER }
  | COMMENT 'string'

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/