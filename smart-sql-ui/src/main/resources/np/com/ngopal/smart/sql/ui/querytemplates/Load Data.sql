/*
Loaddata Statement
Source : MySQL documentation
*/
LOAD DATA [ LOW_PRIORITY | CONCURRENT ] [ LOCAL ] 
	INFILE 'file_name.txt'
	[ REPLACE | IGNORE ]
	INTO TABLE tbl_name
	[ FIELDS
		[ TERMINATED BY '' ]
		[ [ OPTIONALLY ] ENCLOSED BY '' ]
		
		[ ESCAPED BY '\'  ]
	 ]
	[ LINES TERMINATED BY '' ]
	[ IGNORE number LINES ]
	[ (col_name,...) ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/