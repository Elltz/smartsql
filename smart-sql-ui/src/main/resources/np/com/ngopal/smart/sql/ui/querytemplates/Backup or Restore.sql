/*
Backup/Restore Statement
Source : MySQL documentation
*/
BACKUP TABLE tbl_name[,tbl_name...] TO '/path/to/backup/directory'

RESTORE TABLE tbl_name[,tbl_name...] FROM '/path/to/backup/directory'

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/