/*
Create Index Statement
Source : MySQL documentation
*/
CREATE [ UNIQUE| FULLTEXT ] INDEX index_name 
	ON tbl_name (col_name[ (length) ],... ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/