/*
Alter Function Statement
Source : MySQL documentation
*/
ALTER FUNCTION function_name 
[{ CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
  | SQL SECURITY { DEFINER | INVOKER }
  | COMMENT 'string']

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/