/*
Rename Table Statement
Source : MySQL documentation
*/
RENAME TABLE tbl_name TO new_table_name[ , tbl_name2 TO 
	new_table_name2,... ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/