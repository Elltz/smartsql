/*
Delete Statement
Source : MySQL documentation
*/
DELETE [ LOW_PRIORITY ] FROM tbl_name
	[ WHERE where_definition ]
	[ LIMIT rows ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/