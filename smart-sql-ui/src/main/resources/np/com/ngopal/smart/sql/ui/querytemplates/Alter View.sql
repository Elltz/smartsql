/*
Alter View Statement
Source : MySQL documentation
*/
ALTER  [ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]  
    [DEFINER = { user | CURRENT_USER }]
 [SQL SECURITY { DEFINER | INVOKER }]
  VIEW view_name [(column_list)]
    AS select_statement
    [WITH [CASCADED | LOCAL] CHECK OPTION]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/