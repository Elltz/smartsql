/*
Create Database Statement
Source : MySQL documentation
*/
CREATE DATABASE [ IF NOT EXISTS ] db_name
   [DEFAULT] CHARACTER SET charset_name
 | [DEFAULT] COLLATE collation_name

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/