/*
Create Table Statement
Source : MySQL documentation
*/
CREATE [ TEMPORARY ] TABLE [ IF NOT EXISTS ] 
	tbl_name [ (create_definition,...) ]
	[ table_options ] [ select_statement ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/