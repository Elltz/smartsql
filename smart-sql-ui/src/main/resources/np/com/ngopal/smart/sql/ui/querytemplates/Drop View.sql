/*
Drop View Statement
Source : MySQL documentation
*/
DROP VIEW [IF EXISTS]
    view_name [, view_name] ...
    [RESTRICT | CASCADE]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/