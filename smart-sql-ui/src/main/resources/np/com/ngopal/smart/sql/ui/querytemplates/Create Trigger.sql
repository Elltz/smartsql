/*
Create Trigger Statement
Source : MySQL documentation
*/
CREATE 
[DEFINER = { user | CURRENT_USER }] 
TRIGGER trigger_name trigger_time trigger_event
    ON tbl_name FOR EACH ROW trigger_stmt

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/