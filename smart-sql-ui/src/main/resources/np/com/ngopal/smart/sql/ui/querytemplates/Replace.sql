/*
Replace Statement
Source : MySQL documentation
*/
REPLACE [ LOW_PRIORITY | DELAYED ]
	[ INTO ] tbl_name [ (col_name,...) ]
	VALUES (expression,...),(...),...

REPLACE [ LOW_PRIORITY | DELAYED ]
	[ INTO ] tbl_name [ (col_name,...) ]
	SELECT ...
	REPLACE [ LOW_PRIORITY | DELAYED ]
	[ INTO ] tbl_name
	SET col_name=expression, col_name=expression,...

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/