/*
Select Statement
Source : MySQL documentation
*/
SELECT [ STRAIGHT_JOIN ]  [ SQL_SMALL_RESULT ]  
	[ SQL_BIG_RESULT ]  [ SQL_BUFFER_RESULT ] 
		[ HIGH_PRIORITY ]  
		[ DISTINCT | DISTINCT ROW | ALL ] 
	select_expression,... 
	[ INTO {OUTFILE | DUMPFILE} \'file_name\' export_options ] 
	[ FROM table_references[ WHERE where_definition ]
	[ GROUP BY {unsigned_integer | col_name | formula} [ ASC | DESC ], ... ]
	[ HAVING where_definition ]
	[ ORDER BY {unsigned_integer | col_name | formula} [ ASC | DESC ] ,... ]
	[ LIMIT [ offset, ] rows ]
	[ PROCEDURE procedure_name ]
	FOR UPDATE | LOCK IN SHARE MODE ] ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/