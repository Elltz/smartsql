/*
Update Statement
Source : MySQL documentation
*/
UPDATE [ LOW_PRIORITY ] [ IGNORE ] tbl_name
	SET col_name1=expr1, [ col_name2=expr2, ... ]
	[ WHERE where_definition ]
	[ LIMIT # ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/