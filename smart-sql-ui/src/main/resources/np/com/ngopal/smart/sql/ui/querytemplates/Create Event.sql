/*
Create Event Statement
Source : MySQL documentation
*/
CREATE
[DEFINER = { user | CURRENT_USER }]
EVENT [IF NOT EXISTS]
event_name ON SCHEDULE
AT timestamp [+ INTERVAL interval]
| EVERY interval
[STARTS timestamp [+ INTERVAL interval]]
[ENDS timestamp [+ INTERVAL interval]]
[ON COMPLETION [NOT]PRESERVE]
[ENABLE | DISABLE | DISABLE ON SLAVE]
[COMMENT 'comment']
DO sql_statement;

Interval:
quantity {YEAR | QUARTER | MONTH | DAY | HOUR | MINUTE |
WEEK | SECOND | YEAR_MONTH | DAY_HOUR | DAY_MINUTE |
DAY_SECOND | HOUR_MINUTE | HOUR_SECOND | MINUTE_SECOND}   

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/