/*
Alter Database Statement
Source : MySQL documentation
*/
ALTER DATABASE db_name
   [DEFAULT] CHARACTER SET charset_name
 | [DEFAULT] COLLATE collation_name

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/