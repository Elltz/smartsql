/*
Alter Table Statement
Source : MySQL documentation
*/
ALTER [ IGNORE ] TABLE tbl_name alter_spec 
	[ , alter_spec ... ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/