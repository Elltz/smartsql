/*
Drop table Statement
Source : MySQL documentation
*/
DROP TABLE [ IF EXISTS ] tbl_name [ , tbl_name,... ] 
	[ RESTRICT | CASCADE ]

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/