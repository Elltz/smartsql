/*
BEGIN/COMMMIT/ROLLBACK Statement
Source : MySQL documentation
*/
BEGIN;
SELECT @A:=SUM(salary) FROM table1 WHERE type=1;
UPDATE table2 SET summmary=@A WHERE type=1;
COMMIT;
ROLLBACK

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/