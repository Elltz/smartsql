/*
Insert Statement
Source : MySQL documentation
*/
INSERT [ LOW_PRIORITY | DELAYED ] [ IGNORE ]
	[ INTO ] tbl_name [ (col_name,...) ]
	VALUES (expression,...),(...),...

INSERT [ LOW_PRIORITY | DELAYED ] [ IGNORE ]
	[ INTO ] tbl_name [ (col_name,...) ]
	SELECT ...

INSERT [ LOW_PRIORITY | DELAYED ] [ IGNORE ]
	[ INTO ] tbl_name
	SET col_name=expression, col_name=expression, ...

/*
SMARTMYSQL SQL QUERY TEMPLATE
*/