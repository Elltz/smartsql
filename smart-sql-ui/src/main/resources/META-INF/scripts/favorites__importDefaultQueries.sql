INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_createTable', 
'CREATE TABLE TABLE_NAME
(
	id bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	Column1Name Varchar(50) not null, 
	Column2Name Varchar(50) not null, 
	/* add columns  */
	CreateAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	UpdateAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	Key Idx_CreateAt(CreateAt)

	/* FOREIGN KEY (Column1Name) REFERENCES ReferenceTable(Column1Name) ON UPDATE CASCADE ON DELETE CASCADE */
);
', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_addColumn', 
'ALTER TABLE table_name ADD COLUMN column_name varchar(50) -- please change datatype and name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_dropColumn', 
'ALTER TABLE table_name DROP COLUMN column_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_modifyColumn', 
'ALTER TABLE table_name MODIFY column_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_changeColumn', 
'ALTER TABLE table_name CHANGE column_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_addIndex', 
'ALTER TABLE table_name ADD idx_name(column1, column2..);', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_dropIndex', 
'ALTER TABLE table_name DROP idx_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_dropTable', 
'DROP TABLE table_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_truncateTable', 
'TRUNCATE TABLE table_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_createTableLike', 
'CREATE TABLE table_name LIKE table_name1;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_createTableSelect', 
'CREATE TABLE table_name SELECT * FROM table_name1;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_select', 
'SELECT * FROM ', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_serverInsights', 
'SELECT thread_id, EVENT_NAME, SUM(NUMBER_OF_BYTES/1024) sum_kb FROM performance_schema.events_waits_history
 WHERE NUMBER_OF_BYTES > 0 GROUP BY thread_id, EVENT_NAME ORDER BY sum_kb DESC LIMIT 25;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_serverInsightsStatements', 
'SELECT eshl.event_name, sql_text, eshl.timer_wait/1000000000000 w_s
FROM performance_schema.events_stages_history_long eshl
JOIN performance_schema.events_statements_history_long esthl
ON (eshl.nesting_event_id = esthl.event_id)
WHERE eshl.timer_wait > 1*10000000000;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_preparedStatementSQLCommandCoun', 
'SELECT SQL_TEXT, SUM(COUNT_EXECUTE) FROM prepared_statements_instances GROUP BY SQL_TEXT;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_storedProcedureEvents', 
'SELECT thread_id, event_name, sql_text FROM events_statements_history
WHERE event_name LIKE ''statement/sp%''
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_metaDataLocks', 
'SELECT processlist_id, object_type, lock_type, lock_status, source
FROM metadata_locks JOIN threads ON (owner_thread_id=thread_id)
WHERE object_schema=''sbtest'' AND object_name=''sbtest1'';
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '_memoryUsage', 
'SELECT event_name, count_alloc, count_free,
CURRENT_NUMBER_OF_BYTES_USED/1024/1024 AS used_mb
FROM memory_summary_global_by_event_name WHERE CURRENT_NUMBER_OF_BYTES_USED > 0
ORDER BY used_mb desc LIMIT 8;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__createView', 
'CREATE
    /*[ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
    [DEFINER = { user | CURRENT_USER }]
    [SQL SECURITY { DEFINER | INVOKER }]*/
    VIEW view_name 
    AS
(SELECT * FROM ...);
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__createStoredProcedure', 
'DELIMITER $$

CREATE
    /*[DEFINER = { user | CURRENT_USER }]*/
    PROCEDURE storedprocedure_name()
    /*LANGUAGE SQL
    | [NOT] DETERMINISTIC
    | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
    | SQL SECURITY { DEFINER | INVOKER }
    | COMMENT ''string''*/
    BEGIN

    END$$

DELIMITER ;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__createFunction', 
'DELIMITER $$

CREATE
    /*[DEFINER = { user | CURRENT_USER }]*/
    FUNCTION function_name()
    RETURNS TYPE
    /*LANGUAGE SQL
    | [NOT] DETERMINISTIC
    | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
    | SQL SECURITY { DEFINER | INVOKER }
    | COMMENT ''string''*/
    BEGIN

    END$$

DELIMITER ;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__createTrigger', 
'DELIMITER $$

CREATE
    /*[DEFINER = { user | CURRENT_USER }]*/
    TRIGGER trigger_name BEFORE UPDATE
    ON table_name
    FOR EACH ROW BEGIN

    END$$

DELIMITER ;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__createEvent', 
'DELIMITER $$

/* SET GLOBAL event_scheduler = ON$$      required for event to execute but not create*/    

CREATE    /*[DEFINER = { user | CURRENT_USER }]*/    EVENT event_name

ON SCHEDULE
    /* uncomment the example below you want to use */

    /* scheduleexample 1: run once*/

    /*  AT ''YYYY-MM-DD HH:MM.SS''/CURRENT_TIMESTAMP { + INTERVAL 1 [HOUR|MONTH|WEEK|DAY|MINUTE|...] }*/

    /* scheduleexample 2: run at intervals forever after creation*/

    /* EVERY 1 [HOUR|MONTH|WEEK|DAY|MINUTE|...]*/

    /* scheduleexample 3: specified start time, end time and interval for execution*/
    /*EVERY 1  [HOUR|MONTH|WEEK|DAY|MINUTE|...]

    STARTS CURRENT_TIMESTAMP/''YYYY-MM-DD HH:MM.SS'' { + INTERVAL 1[HOUR|MONTH|WEEK|DAY|MINUTE|...] }

    ENDS CURRENT_TIMESTAMP/''YYYY-MM-DD HH:MM.SS'' { + INTERVAL 1 [HOUR|MONTH|WEEK|DAY|MINUTE|...] } */

/*[ON COMPLETION [NOT] PRESERVE]
[ENABLE | DISABLE]
[COMMENT ''comment'']*/

DO
    BEGIN
        (sql_statements)
    END$$

DELIMITER ;
', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__dropView', 
'DROP VIEW view_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__dropStoredProcedure', 
'DROP PROCEDURE storedprocedure_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__dropFunction', 
'DROP FUNCTION function_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__dropTrigger', 
'DROP TRIGGER trigger_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, '__dropEvent', 
'DROP EVENT event_name;', TRUE);

INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'RQ (Running Queries)', 'select * from information_schema.PROCESSLIST where COMMAND <> ''sleep'' order by time DESC;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'TS (Table Size)', 'SELECT
  TABLE_NAME                                                    AS `Table`,
  ROUND((DATA_LENGTH + INDEX_LENGTH + DATA_FREE) / 1024 / 1024) AS `Size (MB)`
FROM
  information_schema.TABLES
WHERE
  TABLE_SCHEMA = database() /* you can change database name */ /*TABLE_NAME= ''enter your table name '' you can use it IF you need find out selected table*/
ORDER BY
  (DATA_LENGTH + INDEX_LENGTH + DATA_FREE ) DESC;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATS (All Tables Size)', 'SELECT
  TABLE_NAME                                                    AS `Table`,
  ROUND((DATA_LENGTH + INDEX_LENGTH + DATA_FREE) / 1024 / 1024) AS `Size (MB)`
FROM
  information_schema.TABLES
WHERE
 1  /*TABLE_SCHEMA = database()  you can chage database name */
  /*TABLE_NAME= ''enter your table name ''  you can use it if you need findo ut selected table*/ 
ORDER BY
  (DATA_LENGTH + INDEX_LENGTH + DATA_FREE ) DESC;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ADS (All Database Size)', 'SELECT
  TABLE_NAME                                                    AS `Table`,
  ROUND((DATA_LENGTH + INDEX_LENGTH + DATA_FREE) / 1024 / 1024) AS `Size (MB)`
FROM
  information_schema.TABLES
WHERE
 1  /*TABLE_SCHEMA = database()  you can change database name */
  /*TABLE_NAME= ''enter your table name ''  you can use it if you need find out selected table*/ 
group by 
TABLE_SCHEMA
ORDER BY
  (DATA_LENGTH + INDEX_LENGTH + DATA_FREE ) DESC;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'DS (Database Size)', 'SELECT
  TABLE_NAME                                                    AS `Table`,
  ROUND((DATA_LENGTH + INDEX_LENGTH + DATA_FREE) / 1024 / 1024) AS `Size (MB)`
FROM
  information_schema.TABLES
WHERE
 TABLE_SCHEMA = database() 
  /*TABLE_NAME= ''enter your table name ''  you can use it if you need find o ut selected table*/ 
group by 
TABLE_SCHEMA
ORDER BY
  (DATA_LENGTH + INDEX_LENGTH + DATA_FREE ) DESC;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SEIS', 'SHOW ENGINE innodb status;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SCT', 'SHOW CREATE TABLE /*Enter Table Name */;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SCV', 'SHOW CREATE VIEW /* Enter View Name */;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SCTR', 'SHOW CREATE TRIGGER /* Enter Trigger Name*/;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SCP', 'SHOW CREATE PROCEDURE /*Enter Procedure Name*/ ;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SCF', 'SHOW CREATE FUNCTION  /*Enter Function Name*/;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SCU', 'SHOW CREATE USER /* Enter User name */;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SVL', 'SHOW VARIABLES like ''%%'' /*Enter MySQL variables Substring Part between %% */ ;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SGVL', 'SHOW GLOBAL VARIABLES like ''%%'' /*Enter MySQL Global variables Substring Part between %% */ ;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SSL', 'SHOW STATUS LIKE ''%%'' /*Enter MySQL Status Variables Substring Part between %% */ ;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SGSL', 'SHOW GLOBAL STATUS like ''%%'' /*Enter MySQL Global Status Variables Substring Part between %% */;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SSS', 'SHOW SLAVE STATUS;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SMS', 'SHOW MASTER STATUS;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SSH', 'SHOW SLAVE HOSTS;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SBL', 'SHOW BINARY LOGS;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SML', 'SHOW MASTER LOGS;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'OTT', 'OPTIMIZE TABLE Table_Name;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATT', 'ANALYZE TABLE Table_Name;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'RTT', 'REPAIR TABLE Table_Name;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'FTWRL', 'FLUSH TABLES WITH READ LOCK;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SBE', 'SHOW BINLOG EVENTS [IN ''log_name''] [FROM pos] [LIMIT [offset,] row_count]', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATAC', 'ALTER TABLE Table_Name ADD COLUMN_Name DataType;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATDC', 'ALTER TABLE Table_name DROP Column_Name;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATCC', 'ALTER TABLE Table_Name CHANGE Column_Name New_colun_name DataType;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATMC', 'ALTER TABLE Table_Name MODIFY Column_Name  DataType;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATAI', 'ALTER TABLE Table_Name ADD INDEX (col1,col2..);', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'ATDI', 'ALTER TABLE Table_Name DROP INDEX Indx_Name;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SFP', 'SHOW FULL PROCESSLIST;', TRUE);
INSERT INTO FAVOURITES(PARENT, FOLDER, NAME, QUERY, PREDEFINED) VALUES ($$PARENT$$, false, 'SP', 'SHOW PROCESSLIST;', TRUE);
