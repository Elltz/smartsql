package org.fife.ui.autocomplete;

import java.awt.Window;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CustomPopup extends AutoCompletePopupWindow {

    public CustomPopup(Window parent, AutoCompletion ac) {
        super(parent, ac);
        
    }

}
