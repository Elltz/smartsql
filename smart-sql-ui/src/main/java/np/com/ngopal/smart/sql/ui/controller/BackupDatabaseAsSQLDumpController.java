package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.utils.DumpUtils;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ScheduleParams;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.components.DatabaseSelectableTreeView;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class BackupDatabaseAsSQLDumpController extends BaseController implements Initializable {
    
    @Getter
    @Setter
    DBService dbService;
  
    @FXML
    private AnchorPane mainUI;
    
  
    @FXML
    private RadioButton exportStructure;
    @FXML
    private RadioButton exportData;
    @FXML
    private RadioButton exportStructureAndData;
    
    @FXML
    private ComboBox<Database> databasesCombobox;
    
    @FXML
    private TextField exportToField;
    @FXML
    private Button chooseButton;
    
    @FXML
    private DatabaseSelectableTreeView dataTree;
    
    @FXML
    private CheckBox lockTables;
    @FXML
    private CheckBox flushLogs;    
    @FXML
    private CheckBox singleTransaction;
    
    @FXML
    private CheckBox includeUseDatabase;
    @FXML
    private CheckBox includeCreateDatabase;    
    @FXML
    private CheckBox setForeignKeyChecks;
    @FXML
    private CheckBox addLockArroundInsert;
    @FXML
    private CheckBox createBulkInsert;    
    @FXML
    private CheckBox oneRowPerLine;
    @FXML
    private CheckBox includeDrop;
    @FXML
    private CheckBox ignoreDefiner;    
    @FXML
    private CheckBox includeVersionInformation;
    
    @FXML
    private CheckBox prefixWithTimestamp;    
    @FXML
    private CheckBox filePerObject;
        
    @FXML
    private ProgressBar exportProgressBar;
    @FXML
    private Label exportStatusLabel;
    
    @FXML
    private Button cancelButton;
    @FXML
    private Button exportButton;
    
    @FXML
    private Button refreshDatabaseButton;
    
    private FileChooser fileChooser;
    private DirectoryChooser dirChooser;
    
    private File selectedFile;
    
    private ScheduleParams settings;
    
    private volatile boolean exportRunned = false;
    
    private boolean firstInit = true;
    
    private ConnectionParams params;
    
    public void init(DBService dbService, ConnectionParams params, Database database, Map<String, String> selectedData) {
        setDbService(dbService);
        
        this.params = params;
        
        firstInit = true;
        
        try {
            List<Database> databases = dbService.getDatabases();
            for (Database db: databases) {
                Database db0 = DatabaseCache.getInstance().getDatabase(params.cacheKey(), db.getName());
                databasesCombobox.getItems().add(db0 != null ? db0 : db);
            }
        } catch (SQLException ex) {
            log.error("Error", ex);
        }
        
        databasesCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
            // load data for tree view

            makeBusy(true);
            Thread th = new Thread(() -> {
                
                Platform.runLater(() -> {
                    if (firstInit) {
                        firstInit = false;
                        // load data for tree view
                        if (selectedData != null) {
                            dataTree.loadData(dbService, newValue, false, selectedData);
                        } else {
                            dataTree.loadData(dbService, newValue);
                        }
                    } else {                
                        dataTree.loadData(dbService, newValue);
                    }

                    makeBusy(false);
                });
            });
            th.setDaemon(true);
            th.start();
        });

        databasesCombobox.getSelectionModel().select(database);
    }    
    
    @FXML
    public void refreshDatabase(ActionEvent event) {
        
        Database database = databasesCombobox.getSelectionModel().getSelectedItem();
        if (database != null) {
            makeBusy(true);

            Thread th = new Thread(() -> {
                try {
                    
                    Database db = DatabaseCache.getInstance().syncDatabase(database.getName(), params.cacheKey(), (AbstractDBService) dbService, true);
                    Platform.runLater(() -> {
                        databasesCombobox.getItems().set(databasesCombobox.getItems().indexOf(database), db);
                        dataTree.loadData(dbService, db);
                        makeBusy(false);
                    });                
                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        DialogHelper.showError(getController(), "Error", "Error refreshing database", ex, getStage());
                        makeBusy(false);
                    });
                }
            });
            th.setDaemon(true);
            th.start();
        }        
    }
    
   
    @FXML
    public void selectFileAction(ActionEvent event) {
        if (filePerObject.isSelected()) {
            // if file per object - need show directory chooser
            if (dirChooser == null) {
                dirChooser = new DirectoryChooser();
            }

            File dir = dirChooser.showDialog(getStage());
            if (dir != null) {
                selectedFile = dir;
                exportToField.setText(selectedFile.getAbsolutePath());
            }
        } else {
            // else show file chooser
            if (fileChooser == null) {
                fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("SQL files", "*.sql"),
                    new FileChooser.ExtensionFilter("All files", "*.*"));
            }

            File file = fileChooser.showSaveDialog(getStage());
            if (file != null) {
                selectedFile = file;
                exportToField.setText(selectedFile.getAbsolutePath());
            }
        }
    }
    
    @FXML
    public synchronized void exportAction(ActionEvent event) {
        
        exportProgressBar.setProgress(0.0);
        exportStatusLabel.setText("");
        
        if (exportRunned) {
            if (settings != null) {
                settings.setStopExport(true);
            }
            
            exportStatusLabel.setText("Aborted by user");
        } else {
            if(dataTree.hasObjects()){
                String exportPath = exportToField.getText();
                if (exportPath != null && !exportPath.isEmpty()) {

                    if (new File(exportPath).exists()) {
                        if (DialogHelper.showConfirm(getController(), "File alreay exist", "File already exist, do you want to replace it?", getStage(), false) != DialogResponce.OK_YES) {
                            return;
                        }
                    }
                    cancelButton.setText("Done");

                    settings = new ScheduleParams();
                    settings.setStopExport(false);

                    exportRunned = true;                

                    validateOptions(true);

                    status(0,null,null);
                    exportStatusLabel.setText("");

                    new Thread(() ->
                    {
                        Database database = (Database) databasesCombobox.getSelectionModel().getSelectedItem();

                        settings.setAddLockArroundInsert(addLockArroundInsert.isSelected());
                        settings.setCreateBulkInsert(createBulkInsert.isSelected());
                        settings.setExportOnlyData(exportData.isSelected());
                        settings.setExportOnlyStructure(exportStructure.isSelected());
                        settings.setFlushLogs(flushLogs.isSelected());
                        settings.setIgnoreDefiner(ignoreDefiner.isSelected());
                        settings.setIncludeCreateDatabase(includeCreateDatabase.isSelected());
                        settings.setIncludeDrop(includeDrop.isSelected());
                        settings.setIncludeUseDatabase(includeUseDatabase.isSelected());
                        settings.setLockTables(lockTables.isSelected());
                        settings.setOneRowPerLine(oneRowPerLine.isSelected());
                        settings.setSetForeignKeyChecks(setForeignKeyChecks.isSelected());
                        settings.setSingleTransaction(singleTransaction.isSelected());
                        settings.setDatabase(database.getName());
                        settings.setSelectedData(dataTree.getSelectedDataAsString());
                        settings.setFilesFileField(exportPath);
                        settings.setAbortOnError(true);

                        // check export per object option
                        if (filePerObject.isSelected()) {

                            settings.setSubFolderWithTimestamp(prefixWithTimestamp.isSelected());

                            DumpUtils.dumpPerObjectDatabase(dbService, settings, new DumpUtils.DumpStatusCallback() {
                                @Override
                                public void error(String errorMessage, Throwable th) {
                                    Platform.runLater(() -> exportStatusLabel.setText("Error"));
                                }

                                @Override
                                public void success() {
                                }

                                @Override
                                public void progress(int progress, String objectLevel, String currentDumpedObject) {
                                    status(progress,objectLevel,currentDumpedObject);
                                }
                            });
                        } else {

                            settings.setPrefixWithTimestamp(prefixWithTimestamp.isSelected());

                            DumpUtils.dumpAllDatabase(dbService, settings, new DumpUtils.DumpStatusCallback() {
                                @Override
                                public void error(String errorMessage, Throwable th) {
                                    Platform.runLater(() -> {
                                        exportStatusLabel.setText("Error");
                                        ((MainController)getController()).showError("Error", th.getMessage(), th);
                                    });
                                }

                                @Override
                                public void success() {
                                }

                                @Override
                                public void progress(int progress, String objectLevel, String currentDumpedObject) {
                                    status(progress,objectLevel,currentDumpedObject);
                                }
                            });
                        }

                        validateOptions(false);
                        exportRunned = false;

                    }).start();
                } else { 
                    DialogHelper.showInfo(getController(), "Info", "Please specify a file name", null); 
                }
            } else {
                DialogHelper.showInfo(getController(), "SMART-MYSQL ERROR", "There is nothing to export", 
                        "you have not specified the object in which you want"
                        + "back up.");
            }
        }
    }
    
    
    private void validateOptions(boolean exportRunned) {
        Platform.runLater(() -> {
            
            exportButton.setText(exportRunned ? "Stop" : "Export");
            cancelButton.setDisable(exportRunned);
            
            exportToField.setDisable(exportRunned);
            
            exportData.setDisable(exportRunned);
            exportStructure.setDisable(exportRunned);
            exportStructureAndData.setDisable(exportRunned);

            databasesCombobox.setDisable(exportRunned);
            
            dataTree.setDisable(exportRunned);
            
            lockTables.setDisable(exportRunned);
            flushLogs.setDisable(exportRunned);
            singleTransaction.setDisable(exportRunned);
            
            includeUseDatabase.setDisable(exportRunned);
            includeCreateDatabase.setDisable(exportRunned);
            setForeignKeyChecks.setDisable(exportRunned);
            addLockArroundInsert.setDisable(exportRunned);
            createBulkInsert.setDisable(exportRunned);
            oneRowPerLine.setDisable(exportRunned);
            includeDrop.setDisable(exportRunned);
            ignoreDefiner.setDisable(exportRunned);
            includeVersionInformation.setDisable(exportRunned);
            
            prefixWithTimestamp.setDisable(exportRunned);
            filePerObject.setDisable(exportRunned);
        });
    }
    
    
    
    private void status(int progress, String objectLevel, String currentDumpedObject) {
        Platform.runLater(() -> {
            if (progress == 100) {
                exportStatusLabel.setText("Success");
            }else if(progress == 0){
                exportStatusLabel.setText("");
            }else{
                StringBuilder sb = new StringBuilder("DUMPED ");
                if(objectLevel != null){
                    sb.append(objectLevel);
                    sb.append(" ENTITY");
                }
                if(currentDumpedObject != null){                    
                    sb.append(" - ");
                    sb.append(currentDumpedObject.toLowerCase());
                }
                exportStatusLabel.setText(sb.toString());
            }
            
            exportProgressBar.setProgress(progress / 100.0);
        });
//        System.gc();
    }
            
    
    @FXML
    public void closeAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ((ImageView)refreshDatabaseButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        propertyListeners();
    }


    public void propertyListeners() {
        createBulkInsert.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            oneRowPerLine.setDisable(!newValue);
        });
        
        filePerObject.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                // need check selected file for dirrectory
                if (selectedFile != null && selectedFile.exists() && !selectedFile.isDirectory()) {
                    DialogHelper.showInfo(getController(), "Info", "Path specified is not a directory, Please select a directory", null);
                }
            }
        });
        
        exportStructure.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
           addLockArroundInsert.setDisable(newValue);
           createBulkInsert.setDisable(newValue);
           oneRowPerLine.setDisable(newValue || !createBulkInsert.isSelected());
        });
        
        exportData.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
           includeCreateDatabase.setDisable(newValue);
           includeDrop.setDisable(newValue);
        });
    }
    
    
    

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}