package np.com.ngopal.smart.sql.ui.visualexplain;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class VisualJoin {
    public double x;
    public double y;
    public Object join;
}
