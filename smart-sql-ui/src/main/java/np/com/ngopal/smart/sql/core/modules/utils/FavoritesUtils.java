package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.List;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.model.Favourites;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class FavoritesUtils {

    public static final String FAVORITES_ROOT_NAME = "Favorite";
    
    public static final Image IMAGE__FOLDER = SmartImageProvider.getInstance().getFolder_image();
    public static final Image IMAGE__QUERY = SmartImageProvider.getInstance().getKeywordTab_image();
    public static final Image IMAGE__FAVORITES = SmartImageProvider.getInstance().getOrganizeFavoriteTab_image();

    
    public static void deleteTreeItem(FavouritesService favServ, TreeItem<Favourites> item) {
        
        if (item != null && item.getValue() != null) {
            favServ.delete(item.getValue());
            
            if (item.getChildren() != null) {
                
                for (TreeItem<Favourites> child: item.getChildren()) {
                    deleteTreeItem(favServ, child);
                }
            }
        }
    }
    
    public static void createFolder(UIController controller, Window window, FavouritesService favServ, TreeItem<Favourites> selectedItem, TreeItem<Favourites> root) {
        String res = DialogHelper.showInput(controller, "Create New Folder", "SmartMySQL will create the following folder for you. You can \nuse this folder to Organize Shortcuts in your Favorite menu.", "Folder", "", window);
        if (res != null && !res.isEmpty()) {
            
            Favourites parent = null;
            if (!selectedItem.equals(root)) {
                if (!selectedItem.getValue().isFolder()) {
                    parent = selectedItem.getParent().getValue();
                } else {
                    parent = selectedItem.getValue();
                }
            }
            
            if (favServ.find(parent != null ? parent.getId() : null, res, true) == null) {
                Favourites favorite = new Favourites();
                favorite.setName(res);
                if (parent != null) {
                    favorite.setParent(parent.getId()); 
                }
                favorite.setFolder(true);
                
                favServ.save(favorite);
                
                FavoritesUtils.refreshTree(favServ, root);
                
            } else {                
                DialogHelper.showError(controller, "Error", "Cannot create Favorite Folder", null, window);
            }
        }
    }
    
    public static void refreshTree(FavouritesService favServ, TreeItem<Favourites> root) {
        refreshTree(favServ, root, false);
    }
    
    public static void refreshTree(FavouritesService favServ, TreeItem<Favourites> root, boolean onlyFolders) {
        root.getChildren().clear();
        
        updateTree(favServ, root, null, onlyFolders);
    }
    
    private static void updateTree(FavouritesService favServ, TreeItem parentItem, Long parentId, boolean onlyFolders) {
        
        List<Favourites> list = favServ.findChildren(parentId);
        if (list != null) {
            
            for (Favourites f: list) {
                
                if (!onlyFolders || f.isFolder()) {
                    TreeItem item = new TreeItem(f);
                    parentItem.getChildren().add(item);

                    if (f.isFolder()) {
                        item.setGraphic(new ImageView(IMAGE__FOLDER));
                        updateTree(favServ, item, f.getId(), onlyFolders);

                    } else {
                        item.setGraphic(new ImageView(IMAGE__QUERY));
                    }
                }
            }
        }
    }
}
