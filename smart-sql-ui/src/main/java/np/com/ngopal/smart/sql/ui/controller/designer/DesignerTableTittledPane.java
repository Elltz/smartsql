package np.com.ngopal.smart.sql.ui.controller.designer;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import static javafx.scene.layout.Region.USE_COMPUTED_SIZE;
import javafx.scene.layout.VBox;
import lombok.Getter;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerTableTittledPane extends VBox implements DesignerPane {

    @Getter
    boolean collapsed = false;
    String color;
    SchemaDesignerTabContentController.GraphNodeType type;

    @Getter
    HBox headerBox;
    VBox subDataBox;
    DesignerTittledPaneContainer contentBox;

    DesignerTable data;

    double oldHeight;

    Label titleLabel;

    boolean sizeChanged = false;

    int columnCount = 0;
    boolean lableShowed = false;
    
    private ImageView collapsIv;
    private SchemaDesignerTabContentController controller;
    
    public DesignerTableTittledPane(SchemaDesignerTabContentController controller, DesignerTable data, SchemaDesignerTabContentController.GraphNodeType type) {

        this.type = type;
        this.data = data;
        this.controller = controller;

        addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                controller.showTableTab(data);
            }
        });

        // header
        ImageView iv = new ImageView(type.getImage());
        iv.setFitHeight(20);
        iv.setFitWidth(20);

        titleLabel = new Label("", iv);
        titleLabel.setMaxWidth(Double.MAX_VALUE);
        titleLabel.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER_LABEL);

        titleLabel.textProperty().bind(data.name());

        headerBox = new HBox(titleLabel);
        headerBox.setPrefWidth(110);
        headerBox.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER_EXPANDED);
        headerBox.setStyle("-fx-background-color: " + data.color().get());
        
        HBox.setHgrow(titleLabel, Priority.ALWAYS);
        getChildren().add(headerBox);

        // if can collaps - need add button
        if (type.isCanCollaps()) {
            collapsIv = new ImageView(SmartImageProvider.getInstance().getRightArrow_small_image());
            collapsIv.setRotate(90);
            Label collapsLabel = new Label(" ", collapsIv);
            headerBox.getChildren().add(collapsLabel);

            subDataBox = new VBox();
            subDataBox.setFillWidth(true);
            subDataBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            subDataBox.setMinWidth(70);

            getChildren().add(subDataBox);
            VBox.setVgrow(subDataBox, Priority.ALWAYS);

            contentBox = new DesignerTittledPaneContainer(this);
            contentBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            contentBox.setMinHeight(0);
            //contentBox.getChildren().add(new Label("Test"));

            HBox footerBox = new HBox();                
            footerBox.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_FOOTER);
            footerBox.setStyle("-fx-background-color: " + type.getColor());

            subDataBox.getChildren().addAll(contentBox, footerBox);

            VBox.setVgrow(contentBox, Priority.ALWAYS);

            collapsLabel.setOnMouseClicked((MouseEvent event) -> {
                collapse();
            });
        }

        setFillWidth(true);

        updateData();
        
        data.color().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            headerBox.setStyle("-fx-background-color: " + newValue);
        });
        
        data.expanded().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            // new value - is expand, so expand = !collapsed
            if (newValue != !collapsed) {
                collapse();
            }
        });
        
        data.columns().addListener((ListChangeListener.Change<? extends DesignerColumn> c) -> {
            updateData();
        });

        data.indexes().addListener((ListChangeListener.Change<? extends DesignerIndex> c) -> {
            updateData();
        });
        
        addDefaultDataListeners();
    }

    private void collapse() {
        if (collapsIv != null) {
            if (collapsed) {
                collapsIv.setRotate(90);

                headerBox.getStyleClass().remove(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER);
                if (!headerBox.getStyleClass().contains(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER_EXPANDED)) {
                    headerBox.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER_EXPANDED);
                }

                if (!getChildren().contains(subDataBox)) {
                    getChildren().add(subDataBox);
                }

                setPrefHeight(sizeChanged ? oldHeight : USE_COMPUTED_SIZE);

                updateData();
            } else {
                collapsIv.setRotate(0);
                oldHeight = getHeight();

                headerBox.getStyleClass().remove(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER_EXPANDED);
                if (!headerBox.getStyleClass().contains(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER)) {
                    headerBox.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER);
                }

                getChildren().remove(subDataBox);
                double width1 = headerBox.getWidth();
                double height1 = headerBox.getHeight();

                DesignerTableTittledPane.super.setPrefSize(width1, height1);
                setMinHeight(height1);

                resize(width1, height1);
            }

            collapsed = !collapsed;
            
            data.expanded().set(!collapsed);

            Platform.runLater(() -> {
                for (DesignerTable dt: controller.getDesignerDatabase().tables()) {
                    controller.updateEdges(dt);
                }
            });
        }
    }
    
    @Override
    public DesignerTable getData() {
        return data;
    }

    @Override
    public Region getRegion() {
        return this;
    }

    @Override
    public boolean canChangeHeight() {
        return data.expanded().get();
    }
    
    @Override
    public void setPrefSize(double prefWidth, double prefHeight) {
        super.setPrefSize(prefWidth, prefHeight);

        sizeChanged = true;            
        updateData();
    }

    public void updateColumns() {

        columnCount = 0;
        double currentHeight = getPrefHeight();

        // 29 - header
        // 28 - indexes header
        // 8  - bootom
        // 10 - padding top + bottom
        // 12 - more label
        // 17.5 - row height
        // other - indexes
        int maxVisibleColumnsCount = (int) ((currentHeight - 29 - (data.indexes().isEmpty() ? 0 : 28) - 7 - 10 -  (contentBox.indexesCollapsed ? 0 : 10 + data.indexes().size() * 17.5)) / 17.5);

        if (maxVisibleColumnsCount > data.columns().size()) {
            maxVisibleColumnsCount = data.columns().size();
        }

        lableShowed = maxVisibleColumnsCount > 0 && maxVisibleColumnsCount < data.columns().size();
        if (lableShowed) {
            maxVisibleColumnsCount = (int) ((currentHeight - 29 - (data.indexes().isEmpty() ? 0 : 28) - 7 - 10 - 12 - (contentBox.indexesCollapsed ? 0 : 10 + data.indexes().size() * 17.5)) / 17.5);
        }
        // draw columns
        if (data.columns() != null) {

            for (int i = 0; i < data.columns().size(); i ++) {

                if (i == maxVisibleColumnsCount) {
                    break;
                }

                contentBox.addColumn(data.columns().get(i));
                columnCount++;
            }
        }

        if (lableShowed || maxVisibleColumnsCount == 0 && !data.columns().isEmpty()) {
            contentBox.addLabel(data.columns().size() - maxVisibleColumnsCount);
        }           

    }

    private void updateData() {               

        contentBox.clearChildren();

        updateColumns();

        // draw indexes
        if (data.indexes() != null) {
            for (int i = 0; i < data.indexes().size(); i ++) {                    
                contentBox.addIndex(data.indexes().get(i));
                columnCount++;
            }
        }

        // 29 - header
        // 28 - one line with paddings
        // 8  - bootom
        // 28 - indexes header
        // 17.5 - row height
        // other - indexes
        // check if table has manualy sizes
        if (sizeChanged && !collapsed) {
            setMinSize(70, 29 + (data.columns().isEmpty() ? 0 : 28) + 8 + (data.indexes().isEmpty() ? 0 : 28)  + (contentBox.indexesCollapsed ? 0 : 10 + data.indexes().size() * 17.5));
        }
    }
}
    