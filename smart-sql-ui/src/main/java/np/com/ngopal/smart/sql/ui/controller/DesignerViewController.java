package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.models.DesignerView;
import np.com.ngopal.smart.sql.structure.queryutils.RSyntaxTextAreaBuilder;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DesignerViewController extends BaseController implements Initializable {
    
    @Inject
    private ConnectionParamService service;
    
    @FXML
    private AnchorPane mainUI;
     
    @FXML
    private TextField nameField;
    @FXML
    private BorderPane ddlAreaPane;
    @FXML 
    private TextArea commentsArea;
        
    private RSyntaxTextArea ddlArea;

    private DesignerView view;
    private DesignerView sourceView;
    private SchemaDesignerTabContentController parent;
    private Tab tab;
    
    public void openScript(ActionEvent event) {
        FileChooser fc = new FileChooser();
        File f = fc.showOpenDialog(getStage());
        if (f != null) {
            try {                
                byte[] data = Files.readAllBytes(f.toPath());
                ddlArea.setText(new String(data, "UTF-8"));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }
    
    public void saveScript(ActionEvent event) {
        if (ddlArea != null) {
            
            FileChooser fc = new FileChooser();
            File userFile = fc.showSaveDialog(getStage());
            
            if (userFile != null) {
                try {
                    Files.write(userFile.toPath(), ddlArea.getText().getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (Throwable ex) {
                    ((MainController)getController()).showError("Error saving script", ex.getMessage(), ex);
                }
            }
        }
    }
    
    public void init(Tab tab, SchemaDesignerTabContentController parent, DesignerView sourceView, ConnectionSession session) {
        
        this.sourceView = sourceView;
        this.tab = tab;
        this.parent = parent;
        
        this.view = new DesignerView();
        this.view.fromMap(sourceView.toMap());
        
        RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), session);
        SwingUtilities.invokeLater(() -> {
            builder.init();
            ddlArea = builder.getTextArea();
            Platform.runLater(() -> {
                ddlAreaPane.setCenter(builder.getSwingNode());
            });

            ddlArea.setText(view.view().get());
            
            ddlArea.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    changed();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    changed();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    changed();
                }

                private void changed() {
                    view.view().set(ddlArea.getText());
                }
            });
        });

        nameField.textProperty().bindBidirectional(view.name());   
        
        commentsArea.textProperty().bindBidirectional(view.comments());        
    }
    
    @FXML
    public void saveAction(ActionEvent event) {
        
        sourceView.fromMap(view.toMap());
        parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + view.name().get() + "'", parent.getCurrentDiagram());
        
        closeAction(null);
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        module.makeCanSave();
    }
    
    @FXML
    public void closeAction(ActionEvent event) {
        if (tab != null && tab.getTabPane() != null) {
            tab.getTabPane().getTabs().remove(tab);
        }
    }
    
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
}
