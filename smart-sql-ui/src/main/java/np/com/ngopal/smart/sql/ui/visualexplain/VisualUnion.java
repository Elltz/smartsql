package np.com.ngopal.smart.sql.ui.visualexplain;

import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class VisualUnion extends VisualTable {
    
    public String tableType;
    public List<VisualTable> tables;
}
