package np.com.ngopal.smart.sql.ui.controller.designer;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerTittledPaneContainer  extends VBox {

    final Background COLUMN_BACKGROUND = new Background(new BackgroundFill( Color.web("ffc978", 0.7), CornerRadii.EMPTY, Insets.EMPTY));

    VBox indexesContainer;
    VBox columnsContainer;

    boolean indexesCollapsed = true;
    DesignerTableTittledPane parent;

    public DesignerTittledPaneContainer(DesignerTableTittledPane parent) {
        getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_CONENT);
        setFillWidth(true);
        this.parent = parent;

//            parent.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
//                System.out.println("listener: " + newValue);
//            });
    }

    public void clearChildren() {
        if (indexesContainer != null) {
            indexesContainer.getChildren().clear();
            indexesContainer = null;
        }

        if (columnsContainer != null) {
            columnsContainer.getChildren().clear();
            columnsContainer = null;
        }

        getChildren().clear();
    }

    public void addLabel(int count) {

        initColumnsContaner();

        Label label = new Label(count + " more...");
        label.setMaxWidth(Double.MAX_VALUE);
        label.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_MORE_LABEL);
        columnsContainer.getChildren().add(label);
    }

    public void addColumn(DesignerColumn column) {

        initColumnsContaner();

        Label nameLabel = new Label("", new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/column.png"));
        nameLabel.textProperty().bind(Bindings.concat(column.name(), " ", column.dataType()));

        SimpleBooleanProperty pkProperty = new SimpleBooleanProperty(false);           
        pkProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                nameLabel.setGraphic(new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/primaryKey.png"));
            } else if (column.notNull().get()) {
                nameLabel.setGraphic(new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/column_notnull.png"));
            } else {
                nameLabel.setGraphic(new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/column.png"));
            }
        });
        pkProperty.bind(column.primaryKey());



        SimpleBooleanProperty notNullProperty = new SimpleBooleanProperty(false);           
        notNullProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!column.primaryKey().get()) {
                if (newValue) {
                    nameLabel.setGraphic(new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/column_notnull.png"));
                } else {
                    nameLabel.setGraphic(new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/column.png"));
                }
            }
        });
        notNullProperty.bind(column.notNull());

//            Label typeLabel = new Label();
//            typeLabel.textProperty().bind(column.dataType());

        HBox box = new HBox(nameLabel);//, typeLabel);
        HBox.setHgrow(nameLabel, Priority.ALWAYS);
        box.setSpacing(5);
        columnsContainer.getChildren().add(box);

        box.backgroundProperty().bind(column.backgroundColor());
    }

    public void addIndex(DesignerIndex index) {

        initIndexesContaner();

        Label nameLabel = new Label();
        nameLabel.setMaxWidth(Double.MAX_VALUE);
        nameLabel.textProperty().bind(index.name());

        nameLabel.setOnMouseEntered((MouseEvent event) -> {
            nameLabel.setBackground(COLUMN_BACKGROUND);
            for (DesignerColumn c: index.columns()) {
                c.backgroundColor().set(COLUMN_BACKGROUND);
            }
        });

        nameLabel.setOnMouseExited((MouseEvent event) -> {
            nameLabel.setBackground(DesignerColumn.DEFAULT_BACKGROUND);
            for (DesignerColumn c: index.columns()) {
                c.backgroundColor().set(DesignerColumn.DEFAULT_BACKGROUND);
            }
        });

        indexesContainer.getChildren().add(nameLabel);
    }


    private void initColumnsContaner() {
        if (columnsContainer == null) {
            columnsContainer = new VBox();
            columnsContainer.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_CONTAINER);
            getChildren().add(columnsContainer);

            columnsContainer.setMaxHeight(Double.MAX_VALUE);
            VBox.setVgrow(columnsContainer, Priority.ALWAYS);
            setAlignment(Pos.BOTTOM_CENTER);
        }
    }

    private void initIndexesContaner() {
        // create on first adding index
        if (indexesContainer == null) {
            indexesContainer = new VBox();
            indexesContainer.setFillWidth(true);
            indexesContainer.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_CONTAINER);

            Label l = new Label("Indexes");
            l.setMaxWidth(Double.MAX_VALUE);

            ImageView collapsIv = new ImageView(SmartImageProvider.getInstance().getRightArrow_small_image());
            if (!indexesCollapsed) {
                collapsIv.setRotate(90);
            }

            Label collapsLabel = new Label(" ", collapsIv);
            collapsLabel.setMaxWidth(Double.MAX_VALUE);
            collapsLabel.setContentDisplay(ContentDisplay.RIGHT);
            collapsLabel.setOnMouseClicked((MouseEvent event) -> {

                double height = 10 + parent.data.indexes().size() * 17.5;
                if (indexesCollapsed) {
                    collapsIv.setRotate(90);

                    if (!getChildren().contains(indexesContainer)) {
                        getChildren().add(indexesContainer);
                    }
                } else {
                    collapsIv.setRotate(0);                        
                    height = -height;
                    getChildren().remove(indexesContainer);
                }


                indexesCollapsed = !indexesCollapsed;

                parent.setPrefHeight(parent.getHeight() + height);
                parent.setPrefWidth(parent.getWidth());
                if (parent.getMinHeight() > parent.getPrefHeight()) {
                    parent.setMinHeight(parent.getPrefHeight());
                }
                parent.resize(parent.getPrefWidth(), parent.getPrefHeight());
                parent.requestLayout();

                if (columnsContainer != null) {
                    columnsContainer.getChildren().clear();
                    parent.updateColumns();
                }
            });

            HBox b = new HBox(l, collapsLabel);
            b.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_INDEXES_HEADER_LABEL);
            HBox.setHgrow(l, Priority.ALWAYS);

            getChildren().add(b);
        }

        if (!indexesCollapsed && !getChildren().contains(indexesContainer)) {
            getChildren().add(indexesContainer);
        }
    }
}