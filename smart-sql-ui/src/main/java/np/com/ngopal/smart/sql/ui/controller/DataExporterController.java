/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DatabaseChildren;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class DataExporterController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Label sectorTitleLabel;

    @FXML
    private Label sectorsubtitleLabel;

    @FXML
    private Button backButton;

    @FXML
    private Button nextButton;

    @FXML
    private Button exportButton;

    @FXML
    private Button exitButton;

    @FXML
    private VBox exportformatUI;

    @FXML
    private ToggleButton toggle_html;

    @FXML
    private ToggleButton toggle_xls;
    
    @FXML
    private ToggleButton toggle_pdf;

    @FXML
    private ToggleButton toggle_json;

    @FXML
    private ToggleButton toggle_xml;

    @FXML
    private ToggleButton toggle_csv;

    @FXML
    private ToggleButton toggle_sql;

    @FXML
    private ToggleButton toggle_txt;

    @FXML
    private VBox sourcepage;

    @FXML
    private ChoiceBox<String> sourcepage_connectionBox;

    @FXML
    private ChoiceBox<np.com.ngopal.smart.sql.model.Database> sourcepage_databaseBox;

    @FXML
    private ListView<EntitySelectorHelper> sourcepage_entitiesList;

    @FXML
    private TextField sourcepage_searchBox;
    
    @FXML
    private VBox text_dataformatspage;

    @FXML
    private VBox sql_optionspage;

    @FXML
    private Label sourcepage_tablesViews_label;
    
    @FXML
    private CheckBox sourcepage_tablesViews_checkBox;
    
    @FXML
    private HBox sourcepage_tablesViews_container;
    
    @FXML
    private ImageView finalExportTypeLogo;
    
    @FXML
    private AnchorPane progress_page;
        
    @FXML
    private Label exportLabel;
    
    @FXML
    private TextField exportdestinationField;
    
    @FXML
    private Button exportdestinationbutton;
    
    @FXML
    private ComboBox<String> charsetCombobox;
        
    @FXML
    private TitledPane csvOptionPane;
    
    @FXML
    private TextField fieldsTerminated;
    
    @FXML
    private TextField fieldsEnclosed;
    
    @FXML
    private TextField fieldsEscaped;
    
    @FXML
    private TextField lineTerminated;
    
    @FXML
    private TitledPane sqlOptions;
    
    @FXML
    private TitledPane xlsOptions;
    
    @FXML
    private RadioButton sqlStructureOnly;
    
    @FXML
    private RadioButton sqlDataOnly;
    
    @FXML
    private RadioButton sqlDataAndStructure;
    
    @FXML
    private CheckBox sqlIncludeVersion;
    
    @FXML
    private AnchorPane export_entity_container;
    
    @FXML
    private Pane emptycover;
    
    @FXML
    private TextField xlsColumnSize;
    
    @FXML
    private TextField xlsDecimalPlaces;
    
    @FXML
    private HBox exporterHeader;
    
    @FXML
    private HBox exporterFooter;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    private final String ORIGINAL_LIST = "list";  
    
    private ArrayList<Section> availableSections = new ArrayList(4);
    
    private int currenctionSectionPosition = 0;    
    
    private final Object progressChangeFunctionKey = new Object();
    
    private volatile boolean processCurrentExportOperation = false;
    
    private volatile boolean isSimpleExport = false;
    
    private ExportUtils.ExportStatusCallback exportCallback;
    
    private volatile String lastExportedPath;
    
    private DialogResponce responce;
    
    private final int XLS_EXPORT_MAX = 35000;
    
    private void prepareExportsPage() {
        try{
            charsetCombobox.getSelectionModel().select("UTF-8");
            charsetCombobox.setItems(FXCollections.observableArrayList(
                    getController().getSelectedConnectionSession().get().getService().getCharSet()));
        }catch(Exception e){}
        
        exportButton.visibleProperty().bind(nextButton.visibleProperty().not());
    }

    public void triggerSimpleExport() {
        isSimpleExport = true;   
        availableSections.remove(1);
        
        exporterHeader.setStyle("-fx-background-color : #e1ebf9;");
        exporterFooter.setStyle("-fx-background-color : #e1ebf9;");
        sectorTitleLabel.setStyle("-fx-text-fill: #3c71b3; -fx-font-weight: bold;");
        sectorsubtitleLabel.setStyle("-fx-text-fill: #3c71b3; -fx-font-weight: bold;");
    }

    enum ExportFormats {
        TEXT, MS_EXCEL, MS_EXCEL_2007, MS_ACCESS, XML, JSON, CSV, ODBC, DBF, HTML, RTF, PDF, SQL;

        @Override
        public String toString() {
            String name = name();
            if (name.charAt(0) == 'M' && name.charAt(3) == 'E') {
                return "xls";
            } else if(name.charAt(0) == 'T' && name.charAt(1) == 'E'){
                return "txt";
            } else { return name.toLowerCase(); }
        } 
    }

    private SimpleObjectProperty<ExportFormats> currentExportFormats
            = new SimpleObjectProperty();

    private class Section {

        String name, description;
        Node[] nodes;
        Runnable cleanUpCode;

        public Section(String name, Node node) {
            this(name, null, node);
        }

        public Section(String name, String description, Node... nodes) {
            this(name, description, null, nodes);
        }
        
        public Section(String name, String description,Runnable cleanUpCode, Node... nodes) {
            this.name = name;
            this.nodes = nodes;
            this.description = description;
            this.cleanUpCode = cleanUpCode;
        }

        public Runnable getCleanUpCode() {
            return cleanUpCode;
        }
    }

    private void activateSection(Section newValue) {
        sectorTitleLabel.setText(newValue.name);
        sectorsubtitleLabel.setText(newValue.description);
        
        for(Node n : newValue.nodes){
            n.toFront();
        }
        
        if(newValue.getCleanUpCode() != null){
            newValue.getCleanUpCode().run();
        }
    }

    private void defaults() {
        availableSections.addAll(Arrays.asList(new Section("EXPORT FORMAT", "Choose an export format",new Runnable() {
            @Override
            public void run() {
                toggle_csv.getToggleGroup().selectToggle(null);
            }
        }, exportformatUI),
                new Section("SOURCE", "Choose a source table for exporting of data", sourcepage),
                new Section("EXPORTS & DESTINATIONS","exporting data and destination of final file location",progress_page,charsetCombobox)));
                
        prepareExportFormatUiPage();
        prepareSourcePage();
        prepareExportsPage();
        activateSection(availableSections.get(0));
    }

    private void prepareExportFormatUiPage() {
        ToggleGroup exportFormatsToggleGroup = new ToggleGroup();
        exportFormatsToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue != null) {
                    currentExportFormats.setValue((ExportFormats) newValue.getUserData());
                    Image im = ((ImageView)((ToggleButton)newValue).getGraphic()).getImage();
                    finalExportTypeLogo.setImage(im);
                                        
                    switch(currentExportFormats.get()){
                        case CSV: case TEXT:
                            unManageEntityOptionsPane(csvOptionPane);
                            //charsetCombobox.setVisible(true);
                            nextButton.setDisable(false);
                            break;
                        case HTML: case PDF: case XML:
                            unManageEntityOptionsPane(null);
                            //charsetCombobox.setVisible(false);
                            nextButton.setDisable(false);
                            break;
                        case MS_EXCEL:
                            //charsetCombobox.setVisible(true);
                            unManageEntityOptionsPane(xlsOptions);
                            nextButton.setDisable(false);
                            break;
                        case SQL:
                            unManageEntityOptionsPane(sqlOptions);
                            //charsetCombobox.setVisible(false);
                            nextButton.setDisable(false);
                            break;
                        case JSON:
                            //charsetCombobox.setVisible(true);
                            unManageEntityOptionsPane(null);
                            nextButton.setDisable(false);
                            break;
                        default:
                            DialogHelper.showError(getController(), "TYPE FORMAT ERROR", "CURRENT FORMAT IS NOT SUPPORTED.", null);
                            unManageEntityOptionsPane(emptycover);
                            nextButton.setDisable(true);
                            break;
                    }
                }else{
                    unManageEntityOptionsPane(emptycover);
                    nextButton.setDisable(true);
                }
            }
        });
        
        toggle_csv.setToggleGroup(exportFormatsToggleGroup);
        toggle_html.setToggleGroup(exportFormatsToggleGroup);
        toggle_json.setToggleGroup(exportFormatsToggleGroup);
        toggle_sql.setToggleGroup(exportFormatsToggleGroup);
        toggle_xls.setToggleGroup(exportFormatsToggleGroup);
        toggle_xml.setToggleGroup(exportFormatsToggleGroup);
        toggle_pdf.setToggleGroup(exportFormatsToggleGroup);
        toggle_txt.setToggleGroup(exportFormatsToggleGroup);
        
        toggle_csv.setUserData(ExportFormats.CSV);
        toggle_html.setUserData(ExportFormats.HTML);
        toggle_json.setUserData(ExportFormats.JSON);
        toggle_pdf.setUserData(ExportFormats.PDF);
        toggle_sql.setUserData(ExportFormats.SQL);
        toggle_txt.setUserData(ExportFormats.TEXT);
        toggle_xls.setUserData(ExportFormats.MS_EXCEL);
        toggle_xml.setUserData(ExportFormats.XML);
        
        nextButton.setDisable(true);
        export_entity_container.managedProperty().bind(csvOptionPane.managedProperty().or(sqlOptions.managedProperty().or(xlsOptions.managedProperty())));
        currentExportFormats.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                setExportDestination();
            }
        });
    }
    
    private void setExportDestination() {
        String des = "";
        try {
            if (isSimpleExport && currentExportFormats.get() != null && !sourcepage_entitiesList.getItems().isEmpty()) {
                EntitySelectorHelper esh = sourcepage_entitiesList.getItems().get(0);
                String extension = "." + currentExportFormats.get().toString();
                File f = new File(getExportDirByDatabaseName(), esh.item + extension);
                des = f.getPath();
            } else {
                des = getExportDirByDatabaseName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        exportdestinationField.setText(des);
    }
    
    private void unManageEntityOptionsPane(Node except){
        for(Node n : Arrays.asList(emptycover,csvOptionPane,xlsOptions,sqlOptions)){
            if(except == n){
                n.toFront();
                n.setManaged(true);
                n.setVisible(true);
            }else{
                n.setManaged(false);
                n.setVisible(false);
            }
        }
    }
    
    private String getExportDirByDatabaseName(){
        Database db = sourcepage_databaseBox.getSelectionModel().getSelectedItem();
        return StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(
                "exports" + (db == null ? "" : File.separator + db.getName())
        ).getPath();
    }
    
    private class EntitySelectorHelper {
        
        SimpleBooleanProperty selected;
        String item;
        
        public EntitySelectorHelper(boolean shouldSelected, DatabaseChildren item, ReadOnlyBooleanProperty parentSelector) {
            this(shouldSelected, item.getName(), parentSelector);
        }

        public EntitySelectorHelper(boolean shouldSelected, String item, ReadOnlyBooleanProperty parentSelector) {
            this.selected = new SimpleBooleanProperty(shouldSelected);
            this.item = item;
            if (parentSelector != null) {
                parentSelector.addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        selected.setValue(newValue);
                    }
                });
            }
        }
    }

    private void prepareSourcePage() {
        ConnectionSession session = getController().getSelectedConnectionSession().get();

        sourcepage_connectionBox.setDisable(false);
        sourcepage_connectionBox.getItems().add(session.getConnectionParam().getName());
        sourcepage_connectionBox.getSelectionModel().select(0);
        
        sourcepage_tablesViews_checkBox.setStyle("-fx-border-width: 0.5; -fx-border-color: transparent silver transparent transparent; -fx-background-color : transparent;");
        sourcepage_tablesViews_checkBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                sourcepage_entitiesList.refresh();
            }
        });
        
        sourcepage_entitiesList.setCellFactory(new Callback<ListView<EntitySelectorHelper>, ListCell<EntitySelectorHelper>>() {
            @Override
            public ListCell<EntitySelectorHelper> call(ListView<EntitySelectorHelper> param) {
                return new ListCell<EntitySelectorHelper>() {
                    {
                        CheckBox checky = new CheckBox();
                        checky.setStyle(sourcepage_tablesViews_checkBox.getStyle());    
                        checky.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                getItem().selected.setValue(!getItem().selected.get());
                                if(sourcepage_tablesViews_checkBox.isSelected()){
                                    sourcepage_tablesViews_checkBox.setIndeterminate(true);
                                }
                            }
                        });
                        setGraphic(checky);
                    }

                    @Override
                    protected void updateItem(EntitySelectorHelper item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setStyle("-fx-border-width: 0.5; -fx-border-color: transparent;");
                            setText("");
                            setContentDisplay(ContentDisplay.TEXT_ONLY);
                        } else {
                            setContentDisplay(ContentDisplay.LEFT);
                            setStyle("-fx-border-width: 0.2; -fx-border-color: transparent transparent silver transparent; -fx-background-color : "
                                    + (isSelected() ? "blue;" : "white;"));
                            ((CheckBox)getGraphic()).setSelected(getItem().selected.get());
                            setText(getItem().item);
                        }
                    }
                };
            }
        });

        sourcepage_databaseBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Database>() {
            @Override
            public void changed(ObservableValue<? extends Database> observable,
                    Database oldValue, Database newValue) {
                sourcepage_entitiesList.getItems().clear();
                if (newValue != null) {
                    
                    if (isSimpleExport) {
                        sourcepage_entitiesList.getItems().add(
                                new EntitySelectorHelper(true, 
                                        (DatabaseChildren) getController().getSelectedTreeItem(session).getValue().getValue(),
                                        sourcepage_tablesViews_checkBox.selectedProperty()));                        
                    } else {
                        for (DatabaseChildren dc : newValue.getTables()) {
                            sourcepage_entitiesList.getItems().add(new EntitySelectorHelper(false, dc, sourcepage_tablesViews_checkBox.selectedProperty()));
                        }
//                    for(DatabaseChildren dc : newValue.getViews()){
//                        sourcepage_entitiesList.getItems().add(new EntitySelectorHelper(false, dc, sourcepage_tablesViews_checkBox.selectedProperty()));
//                    }                        
                    }
                    sourcepage_entitiesList.getProperties().put(ORIGINAL_LIST, new ArrayList(sourcepage_entitiesList.getItems()));

                    setExportDestination();
                }
            }
        });
        
        sourcepage_searchBox.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                Recycler.addWorkProc(new WorkProc<String>(newValue) {

                    List<EntitySelectorHelper> list;

                    @Override
                    public void updateUI() {
                        sourcepage_entitiesList.getItems().clear();
                        sourcepage_entitiesList.getItems().addAll(list);
                    }

                    @Override
                    public void run() {
                        if (getAddData() == null || getAddData().isEmpty()) {
                            list = (List<EntitySelectorHelper>) sourcepage_entitiesList.getProperties().get(ORIGINAL_LIST);
                            System.out.println("----size " + list.size());
                        } else {
                            list = new ArrayList<>();
                            for (EntitySelectorHelper dc : (List<EntitySelectorHelper>) sourcepage_entitiesList.getProperties().get(ORIGINAL_LIST)) {
                                if (dc.item.contains(getAddData())) {
                                    list.add(dc);
                                }
                            }
                        }
                        callUpdate = !list.isEmpty();
                    }
                });
            }
        });
        
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {}
            @Override
            public void run() {
                try {
                    DatabaseCache databaseCache = DatabaseCache.getInstance();
                    AbstractDBService service = (AbstractDBService) session.getService();
                    databaseCache.syncDatabases(service.loadDBStructure(), session.getConnectionParam(), service, true);
                    Platform.runLater(() -> {
                        sourcepage_databaseBox.getItems().addAll(databaseCache.getDatabases(session.getConnectionParam().cacheKey()));
                    });
                    np.com.ngopal.smart.sql.model.Database database = getDatabaseFromTreeItemValue(getController()
                            .getSelectedTreeItem(session).getValue().getValue());
                    if (database != null) {
                        Platform.runLater(() -> {
                            sourcepage_databaseBox.getSelectionModel().select(databaseCache.getDatabase(session.getConnectionParam().cacheKey(),
                                    database.getName()));
                        });
                    }
                } catch (Exception e) { e.printStackTrace(); }
            }
        });        
    }

    private np.com.ngopal.smart.sql.model.Database getDatabaseFromTreeItemValue(Object selectedDatabaseObject) {
        if (selectedDatabaseObject instanceof np.com.ngopal.smart.sql.model.Database) {
            return (np.com.ngopal.smart.sql.model.Database) selectedDatabaseObject;
        } else if (selectedDatabaseObject instanceof np.com.ngopal.smart.sql.model.DBTable) {
            return ((np.com.ngopal.smart.sql.model.DBTable) selectedDatabaseObject).getDatabase();
        } else {
            return null;
        }
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        defaults();
        buttonListenerInstaller();        
    }
    
    private void buttonListenerInstaller(){
        EventHandler<ActionEvent> buttonHandlers = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(event.getSource() == nextButton){
                    sectionNavigator(true);
                }else if(event.getSource() == exportButton){
                    export_entity_container.setDisable(true);
                    createProgressBarPopup();
                    export();
                    showProgressBarPopup();
                }else if(event.getSource() == exitButton){
                    getUI().getScene().getWindow().hide();
                }else if(event.getSource() == backButton){
                    sectionNavigator(false);
                }else if (event.getSource() == exportdestinationbutton) {
                    if (currentExportFormats.get() == null) {
                        DialogHelper.showInfo(getController(), "Info", "Please select export option", null);
                        return;
                    }
                    
                    File file = null;
                    
                    if (isSimpleExport) {
                        String extension = "." +currentExportFormats.get().toString();
                        file = new File(exportdestinationField.getText());
                        boolean isDirectory = file.getName().lastIndexOf('.') == -1;
                        
                        FileChooser fileChooser = new FileChooser();
                        fileChooser.setTitle("Select Export file");
                        File dir = isDirectory ? file : file.getParentFile();
                        if (dir.exists()) {
                            fileChooser.setInitialDirectory(dir);   
                        }
                        fileChooser.getExtensionFilters().addAll(
                                new ExtensionFilter(currentExportFormats.get().name(), "*" + extension),
                                new ExtensionFilter("All Files", "*.*"));
                        
                        if(!isDirectory){
                            fileChooser.setInitialFileName(file.getName());                            
                        }
                        
                        file = null;                        
                        file = fileChooser.showSaveDialog(mainUI.getScene().getWindow());
                    } else {
                        DirectoryChooser directoryChooser = new DirectoryChooser();
                        File dir = new File(exportdestinationField.getText());
                        if (dir.exists()) {
                            directoryChooser.setInitialDirectory(dir);
                        }
                        directoryChooser.setTitle("Export To");
                        file = directoryChooser.showDialog(mainUI.getScene().getWindow());
                    }

                    if (file != null) {
                        exportdestinationField.setText(file.getAbsolutePath());
                    }
                }
            }
        };
        
        nextButton.setOnAction(buttonHandlers);
        backButton.setOnAction(buttonHandlers);
        backButton.setVisible(false);
        exitButton.setOnAction(buttonHandlers);
        exportButton.setOnAction(buttonHandlers);
        exportdestinationbutton.setOnAction(buttonHandlers);
    }
    
    private void sectionNavigator(boolean forward__backward){
        currenctionSectionPosition += forward__backward ? 1 : -1;
        if(currenctionSectionPosition < 0 || currenctionSectionPosition > availableSections.size()){ 
            currenctionSectionPosition += forward__backward ? -1 : 1; //we reset it - make it inverse
            return;
        }
        backButton.setVisible(currenctionSectionPosition > 0);
        nextButton.setVisible(currenctionSectionPosition < availableSections.size()-1);
        
        Section section = availableSections.get(currenctionSectionPosition);
        if(section != null){
            activateSection(section);
        }
    }
    
    private interface MiniProgressInterfaceHelper{
        void setMax(double max);
        void updateProgressByOne();
        void updateProgress(double currentFigure);
        void updateNoticeText(String s);
        void onFinish();
    }
    
    private Stage progressPopup;
    
    public void createProgressBarPopup(){
        if (progressPopup == null) {
            Window win = mainUI.getScene().getWindow();
            progressPopup = new Stage(StageStyle.UTILITY);
            progressPopup.initModality(Modality.WINDOW_MODAL);
            progressPopup.initOwner(win);
            progressPopup.setTitle("EXPORTS PROGRESS DIALOG");
            progressPopup.setY((win.getHeight() - 150) / 2);
            progressPopup.setX( win.getX() + (win.getWidth() - 470)/2 );
            progressPopup.setResizable(false);
            progressPopup.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    DialogResponce dr = DialogHelper.showConfirm(getController(), "EXPORT NOTICE", "DO YOU WANT TO CANCEL CURRENT OPERATION? ",
                            progressPopup, false);
                    if(dr == DialogResponce.NO || dr == DialogResponce.NO_TO_ALL){
                        event.consume();
                        return;
                    }
                    //showInfo("EXPORT NOTIFICATION", "dialog will self-destruct. when exporting is done", null, null);
                    processCurrentExportOperation = false;
                }
            });
            
            AnchorPane popupParent = new AnchorPane();
            popupParent.getStylesheets().addAll(mainUI.getStylesheets());
            popupParent.setPrefSize(470, 150);
            popupParent.setMinSize(AnchorPane.USE_PREF_SIZE, AnchorPane.USE_PREF_SIZE);
            popupParent.setStyle("-fx-background-color: white;");

            final ProgressBar pb = new ProgressBar(0.0);
            pb.getStyleClass().add("simple-progress-bar");
            pb.progressProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(newValue != null && newValue.doubleValue() >= 1.0){
                        pb.getStyleClass().add("progress-done");
                    }else if(newValue != null && newValue.doubleValue() == 0.0){
                        pb.getStyleClass().clear();
                        pb.getStyleClass().addAll("progress-bar","simple-progress-bar");
                    }
                }
            });
            AnchorPane.setTopAnchor(pb, 30.0);
            AnchorPane.setRightAnchor(pb, 30.0);
            AnchorPane.setLeftAnchor(pb, 30.0);

            final Label text = new Label("initializing...");
            text.setPrefWidth(Label.USE_COMPUTED_SIZE);
            text.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            text.setStyle("-fx-font-weight: bold; fx-text-fill: #3c71b3;");
            text.setWrapText(true);
            AnchorPane.setTopAnchor(text, 60.0);
            AnchorPane.setRightAnchor(text, 30.0);
            AnchorPane.setLeftAnchor(text, 30.0);

            popupParent.getChildren().addAll(pb, text);
            progressPopup.setScene(new Scene(popupParent));
            progressPopup.getScene().getProperties().put(progressChangeFunctionKey, new MiniProgressInterfaceHelper() {
                
                volatile double overallMax = 0.0;
                volatile double useableIncrementor = 0;
                
                @Override
                public void setMax(double max) {
                    overallMax = max;
                    useableIncrementor = 0.0;
                    Platform.runLater(()-> pb.setProgress(0.0) );
                }

                @Override
                public void updateProgressByOne() {
                    useableIncrementor+=1.0;
                    updateProgress(useableIncrementor);
                }

                @Override
                public void updateProgress(double currentFigure) {
                    if(currentFigure <= overallMax){
                        double currentFinalProgress = currentFigure / overallMax;
                        Platform.runLater(()-> pb.setProgress(currentFinalProgress) );
                    }
                }

                @Override
                public void updateNoticeText(String s) {
                    if (s != null && !s.isEmpty()) {
                        Platform.runLater(()-> text.setText(s));
                    }
                }

                @Override
                public void onFinish() {
                    setMax(0.0);
                    export_entity_container.setDisable(false);
                    emptycover.toFront();
                    
                    sectionNavigator(false);
                    sectionNavigator(false);
                    
                    DialogResponce resp = DialogHelper.showConfirm(getController(), "Information",
                            "Data exported Successfully, Would you like to open " + (isSimpleExport ? "file ?" : "folder"),
                            progressPopup,false);
                    
                    Platform.runLater(() -> {
                        if (resp == DialogResponce.OK_YES) {
                            try {
                                Desktop.getDesktop().open(new File((isSimpleExport ? lastExportedPath : exportdestinationField.getText())));
                            } catch (IOException ex) {
                                DialogHelper.showError(getController(), "Error", "There is an error while opening exported file", ex);
                            }
                        }
                    });                    
                }
            });
        }
    }
    
    private void showProgressBarPopup() {
        if(!progressPopup.isShowing()){
            Platform.runLater(()-> progressPopup.showAndWait() );            
        }

    }
    
    private void closeProgressBarPopup(){
        if(progressPopup.isShowing()){
            progressPopup.hide();
        }
        ((MiniProgressInterfaceHelper)progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).setMax(0.0);
    }
    
    private void export() {
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
                if(processCurrentExportOperation){
                    ((MiniProgressInterfaceHelper)progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).onFinish();
                }
                closeProgressBarPopup();
            }

            @Override
            public void run() {                             
                getExportCallback();
                
                if(isSimpleExport){
                    ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).setMax(1);
                    EntitySelectorHelper esh = sourcepage_entitiesList.getItems().get(0);
                    responce = DialogResponce.YES_TO_ALL;
                    
                    exportTableEntity(sourcepage_databaseBox.getSelectionModel().getSelectedItem().getTable(esh.item),
                            new File(exportdestinationField.getText()));
                    
                    responce = null;
                }else{
                    exportDatabaseEntity();
                }
                
                callUpdate = true;
                System.gc();
            }
        });
    }
    
    private void parseTableEntityToRowsData(String tableEntity, DBService service, ObservableList<ObservableList> rowsData) throws Exception{
        String selectAllQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, sourcepage_databaseBox.getSelectionModel().getSelectedItem(), tableEntity)/* + " LIMIT " + ExportUtils.MAX_ROWS_COUNT*/;
        try (ResultSet rs = (ResultSet) service.execute(selectAllQuery)) {//"SELECT * FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "` LIMIT " + ExportUtils.MAX_ROWS_COUNT)) {
            while (rs.next()) {
                ObservableList row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    row.add(rs.getObject(i));
                }
                rowsData.add(row);
            }
        }
    }

    private void exportTableEntity(DBTable table, File lastKnownExportFile) {
        processCurrentExportOperation = true;

        StringBuilder sb = new StringBuilder("EXPORTING " + table.getName() + " to directory...");
        ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).updateNoticeText(sb.toString());
        ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).updateProgressByOne();
        
        if (lastKnownExportFile.exists()) {

            if (responce == DialogResponce.NO_TO_ALL) {
                ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey))
                        .updateNoticeText(sb.append("\n\n\t").append("SKIPPING FILE AS IT ALREADY EXIST")
                                .toString());                
                return;
            } else if (responce != DialogResponce.YES_TO_ALL) {
                responce = DialogHelper.showConfirm(getController(), "FILE OVERWRITE", lastKnownExportFile.getName()
                        + " already exist, would you like to replace it ?", progressPopup, true);

                if (responce == DialogResponce.NO || responce == DialogResponce.NO_TO_ALL || responce == DialogResponce.CANCEL) {
                    processCurrentExportOperation = (responce != DialogResponce.CANCEL);
                    if(processCurrentExportOperation){
                        ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey))
                        .updateNoticeText(sb.append("\n\n\t").append("SKIPPING FILE AS IT ALREADY EXIST")
                                .toString()); 
                    }
                    return;
                }
            }

        }
        
        ObservableList<ObservableList> rowsData = FXCollections.observableArrayList();
        try {
            parseTableEntityToRowsData(table.getName(), getController().getSelectedConnectionSession().get().getService(), rowsData);
        } catch (Exception ex) {
            DialogHelper.showError(getController(), "Error", "Error loading export data", ex);
            processCurrentExportOperation = false;
            return;
        }

        sb.append("\n\t").append(String.valueOf(rowsData.size()))
                .append(" ROWS")
                .append("\n\t")
                .append(String.valueOf(table.getColumns().size()))
                .append(" COLUMNS");
        ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).updateNoticeText(sb.toString());

        parseEntityToFormat(lastKnownExportFile, table, rowsData);

        sb.append("\n\t").append("Saved to file ").append(" ").append(lastKnownExportFile.getName());
        ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).updateNoticeText(sb.toString());
        
        lastExportedPath = lastKnownExportFile.getPath();
    }
    
    private void exportDatabaseEntity() {

        if (exportdestinationField.getText() == null || exportdestinationField.getText().isEmpty()) {
            Platform.runLater(() -> {
                DialogHelper.showInfo(getController(), "EXPORT ALERT", "Please select export directory", null);
                closeProgressBarPopup();
            });
            return;
        }

        int selecteeCount = 0;
        for (EntitySelectorHelper esh : (List<EntitySelectorHelper>) sourcepage_entitiesList.getProperties().get(ORIGINAL_LIST)) {
            if (esh.selected.get()) {
                selecteeCount++;
            }
        }

        if (selecteeCount == 0) {
            Platform.runLater(() -> {
                DialogHelper.showInfo(getController(), "EXPORT ALERT", "Please select at least one(1) table entity to export", null);
                closeProgressBarPopup();
            });
            return;
        }
        
        processCurrentExportOperation = true;
        ((MiniProgressInterfaceHelper) progressPopup.getScene().getProperties().get(progressChangeFunctionKey)).setMax(selecteeCount);
        responce = null;
        
        for (EntitySelectorHelper esh : (List<EntitySelectorHelper>) sourcepage_entitiesList.getProperties().get(ORIGINAL_LIST)) {
            if (processCurrentExportOperation == false) {
                break;
            }
            if (esh.selected.get()) {
                exportTableEntity(sourcepage_databaseBox.getSelectionModel().getSelectedItem().getTable(esh.item),
                        new File(exportdestinationField.getText(), esh.item + "." + currentExportFormats.get().toString()));
                try { Thread.sleep(2000); } catch (Exception e) { }
            }
        }
        
        responce = null;
        
    }
    
    private void parseEntityToFormat(File exportFilePath, DBTable table, ObservableList<ObservableList> rowsData) {
        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

        for (np.com.ngopal.smart.sql.model.Column c : table.getColumns()) {
            columns.put(c.getName(), true);
        }

        switch (currentExportFormats.get()) {
            case CSV:
            case TEXT:
                ExportUtils.exportDelimiter(exportFilePath.getPath(), rowsData, columns,
                        charsetCombobox.getSelectionModel().getSelectedItem(), fieldsTerminated.getText(), fieldsEnclosed.getText(), lineTerminated.getText(), "NULL", false, exportCallback);
                break;

            case MS_EXCEL:

                Integer maxTextSizeLimit = null;
                try { maxTextSizeLimit = Integer.valueOf(xlsColumnSize.getText()); } catch (NumberFormatException ex) {}

                Integer decimalPlaces = null;
                try { decimalPlaces = Integer.valueOf(xlsDecimalPlaces.getText()); } catch (NumberFormatException ex) {}

                int requiredLoopinCount = (rowsData.size() / XLS_EXPORT_MAX) + (rowsData.size() % XLS_EXPORT_MAX);      
                int loops = 0;
                while(!rowsData.isEmpty()){
                    ObservableList<ObservableList> trimmedRowsData = FXCollections.observableArrayList();                    
                    while(!rowsData.isEmpty() && trimmedRowsData.size() <= XLS_EXPORT_MAX){
                        trimmedRowsData.add(rowsData.remove(0));                    
                    } 
                    String exportPath = exportFilePath.getPath();
                    if(loops > 0){ 
                        String name = exportFilePath.getName();
                        name = name.substring(0, name.indexOf('.')) + "_" + String.valueOf(loops) + name.indexOf('.');
                        exportPath = new File(exportFilePath.getParent(), name).getPath();
                    }
                    ExportUtils.exportXLS(exportPath, trimmedRowsData, columns, maxTextSizeLimit, decimalPlaces, charsetCombobox.getSelectionModel().getSelectedItem(), exportCallback);
                    loops++;
                } 
                
                break;

            case PDF:
                ExportUtils.exportPDF(exportFilePath.getPath(), rowsData, columns, exportCallback);
                break;

            case HTML:
                ExportUtils.exportHTML(exportFilePath.getPath(), rowsData, columns, exportCallback);
                break;

            case XML:
                ExportUtils.exportXML(exportFilePath.getPath(), rowsData, columns, exportCallback);
                break;

            case SQL:
                ExportUtils.ExportSQLType type = ExportUtils.ExportSQLType.STUCTURE_AND_DATA;
                if (sqlDataOnly.isSelected()) {
                    type = ExportUtils.ExportSQLType.ONLY_DATA;
                } else if (sqlStructureOnly.isSelected()) {
                    type = ExportUtils.ExportSQLType.ONLY_STRUCTURE;
                }

                String version = null;
                try {
                    if (sqlIncludeVersion.isSelected()) {
                        DatabaseMetaData metadata = getController().getSelectedConnectionSession().get().getService().getDB().getMetaData();
                        version = metadata.getDatabaseProductVersion();
                    }
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }

                PreferenceData pd = preferenceService.getPreference();

                Long size = null;
                Long rows = null;

                if (!pd.isExportOptionsServerDefault()) {
                    try {
                        size = Long.valueOf(pd.getExportOptionsCustomSize()) * 1024;
                    } catch (Throwable th) {
                    }
                }

                if (!pd.isExportOptionsDontBreakIntoChunks()) {
                    try {
                        rows = Long.valueOf(pd.getExportOptionsChunkSize());
                    } catch (Throwable th) {
                    }
                }

                ExportUtils.exportSQL(exportFilePath.getPath(), table, rowsData, columns, type, version, size, rows, exportCallback);
                break;

            case JSON:
                ExportUtils.exportJSON(exportFilePath.getPath(), Charset.forName("UTF-8"), rowsData, columns, exportCallback);
                break;
        }

    }

    private ExportUtils.ExportStatusCallback getExportCallback() {
        if (exportCallback == null) {
            exportCallback = new ExportUtils.ExportStatusCallback() {
                @Override
                public void error(String errorMessage, Throwable th) {
                    th.printStackTrace();
                    DialogHelper.showError(getController(), "Error", errorMessage, th);
                }

                @Override
                public void success() {
                }
            };
        }
        return exportCallback;
    }

     private String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, queryId);
    }
    
    private String getFormatedQuery(String queryId, Object...values) {
        return String.format(getQuery(queryId), values);
    }
}
