/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class CodeAreaComputeOffset {
    
    private int top = -1;
    private int bottom = -1;

    public CodeAreaComputeOffset() { }

    CodeAreaComputeOffset(Integer integer0, Integer integer1) { 
        top = integer0;
        bottom = integer1;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CodeAreaComputeOffset other = (CodeAreaComputeOffset) obj;
        if (this.top != other.top) {
            return false;
        }
        if (this.bottom != other.bottom) {
            return false;
        }
        return true;
    }
    
    public boolean isOffsetInComputedRegion(int min, int max){
        return isOffsetInComputedRegion(new CodeAreaComputeOffset(min, max));
    }
    public boolean isOffsetInComputedRegion(CodeAreaComputeOffset areaComputeOffset){        
        return this.equals(areaComputeOffset) || (this.top < areaComputeOffset.top && 
                this.bottom > areaComputeOffset.bottom);
    }
    
}
