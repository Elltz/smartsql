/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

/**
 *
 * @author NGM
 */
public class SQLValidationException extends Exception {

    public SQLValidationException() {
    }

    public SQLValidationException(String message) {
        super(message);
    }

    public SQLValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
