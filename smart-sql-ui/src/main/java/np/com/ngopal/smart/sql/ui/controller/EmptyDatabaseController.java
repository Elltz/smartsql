/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 *
 * @author terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class EmptyDatabaseController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label title;
    
    @FXML
    private CheckBox selectAll;
    @FXML
    private CheckBox tables;
    @FXML
    private CheckBox views;
    @FXML
    private CheckBox procs;
    @FXML
    private CheckBox functions;
    @FXML
    private CheckBox triggers;
    @FXML
    private CheckBox events;
    
    @FXML
    private Button deleteSelectedButton;
    
    private Database database;
    private ConnectionSession session;
    private TreeItem databaseTreeItem;
    
    @FXML
    public void deleteSelected(ActionEvent event) {
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            try {                            
                session.getService().execute("SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;");                            

                // drop views
                if (views.isSelected()) {
                    List<View> views = session.getService().getViews(database);
                    if (views != null) {
                        for (View v : views) {
                            session.getService().dropView(v);
                            database.getViews().remove(v);

                            for (TreeItem ti: (List<TreeItem>)databaseTreeItem.getChildren()) {
                                if (ti.getValue() == ConnectionTabContentController.DatabaseChildrenType.VIEWS) {
                                    for (TreeItem ti0: new ArrayList<>((List<TreeItem>)ti.getChildren())) {
                                        if (ti0.getValue() != null) {
                                            LeftTreeHelper.viewDroped(session.getConnectionParam(), ti0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // drop procedures
                if (procs.isSelected()) {
                    List<StoredProc> procedures = session.getService().getStoredProcs(database);
                    if (procedures != null) {
                        for (StoredProc p : procedures) {
                            session.getService().dropStoredProcedure(p);
                            database.getStoredProc().remove(p);

                            for (TreeItem ti: (List<TreeItem>)databaseTreeItem.getChildren()) {
                                if (ti.getValue() == ConnectionTabContentController.DatabaseChildrenType.STORED_PROCS) {
                                    for (TreeItem ti0: new ArrayList<>((List<TreeItem>)ti.getChildren())) {
                                        if (ti0.getValue() != null) {
                                            LeftTreeHelper.storedProcDroped(session.getConnectionParam(), ti0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // drop triggers
                if (triggers.isSelected()) {
                    List<Trigger> triggers = session.getService().getTriggers(database);
                    if (triggers != null) {
                        for (Trigger t: triggers) {
                            session.getService().dropTrigger(t);
                            database.getTriggers().remove(t);

                            for (TreeItem ti: (List<TreeItem>)databaseTreeItem.getChildren()) {
                                if (ti.getValue() == ConnectionTabContentController.DatabaseChildrenType.TRIGGERS) {
                                    for (TreeItem ti0: new ArrayList<>((List<TreeItem>)ti.getChildren())) {
                                        if (ti0.getValue() != null) {
                                            LeftTreeHelper.triggerDroped(session.getConnectionParam(), ti0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // drop functions
                if (functions.isSelected()) {
                    List<Function> fucntions = session.getService().getFunctions(database);
                    if (fucntions != null) {
                        for (Function f : fucntions) {
                            session.getService().dropFunction(f);
                            database.getFunctions().remove(f);

                            for (TreeItem ti: (List<TreeItem>)databaseTreeItem.getChildren()) {
                                if (ti.getValue() == ConnectionTabContentController.DatabaseChildrenType.EVENTS) {
                                    for (TreeItem ti0: new ArrayList<>((List<TreeItem>)ti.getChildren())) {
                                        if (ti0.getValue() != null) {
                                            LeftTreeHelper.eventDroped(session.getConnectionParam(), ti0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // drop events
                if (events.isSelected()) {
                    List<Event> events = session.getService().getEvents(database);
                    if (events != null) {
                        for (Event e : events) {
                            session.getService().dropEvent(e);

                            for (TreeItem ti: (List<TreeItem>)databaseTreeItem.getChildren()) {
                                if (ti.getValue() == ConnectionTabContentController.DatabaseChildrenType.EVENTS) {
                                    for (TreeItem ti0: new ArrayList<>((List<TreeItem>)ti.getChildren())) {
                                        if (ti0.getValue() != null) {
                                            LeftTreeHelper.eventDroped(session.getConnectionParam(), ti0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // drop tables
                if (tables.isSelected()) {
                    List<DBTable> tables = session.getService().getTables(database);
                    if (tables != null) {
                        for (DBTable t : tables) {
                            t.setDatabase(database);
                            session.getService().dropTable(t);

                            for (TreeItem ti: (List<TreeItem>)databaseTreeItem.getChildren()) {
                                if (ti.getValue() == ConnectionTabContentController.DatabaseChildrenType.TABLES) {
                                    for (TreeItem ti0: new ArrayList<>((List<TreeItem>)ti.getChildren())) {
                                        if (ti0.getValue() != null) {
                                            LeftTreeHelper.tableDroped(session.getConnectionParam(), ti0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // reset foreign key
                session.getService().execute(session.getService().getClearForeignKeyCheckSQL());

                // and refresh tree            
                getController().refreshTree(session, false);

            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", "Error while execute drop database action", ex);

            } finally {
                try {
                    session.getService().execute("SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;");
                } catch (SQLException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
            
            Platform.runLater(() -> {
                DialogHelper.showInfo(getController(), "Info", "Selected data was deleted!", null);
                dialog.close();
            });
            
        });
        th.setDaemon(true);
        th.start();        
    }
    
    @FXML
    public void cancel(ActionEvent event) {
        dialog.close();
    }
    

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    private Stage dialog;

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        selectAll.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            tables.setSelected(newValue);
            views.setSelected(newValue);
            procs.setSelected(newValue);
            functions.setSelected(newValue);
            triggers.setSelected(newValue);
            events.setSelected(newValue);
        });
        
        deleteSelectedButton.disableProperty().bind(
            tables.selectedProperty().not()
            .and(views.selectedProperty().not())
            .and(procs.selectedProperty().not())
            .and(functions.selectedProperty().not())
            .and(triggers.selectedProperty().not())
            .and(events.selectedProperty().not())
        );
    }
    
    public void init(Window win, ConnectionSession session, Database database, TreeItem databaseTreeItem){
        
        this.database = database;
        this.session = session;
        this.databaseTreeItem = databaseTreeItem;
        
        title.setText(String.format(title.getText(), database.getName()));
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Empty database");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();
        
    }
    
}
