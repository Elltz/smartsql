/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author terah laweh(rumorsapp@gmail.com)
 */
public class TableDiagnosticsController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private ImageView titleImageView;
    
    @FXML
    private CheckBox selectDeselectCheckbox;

    @FXML
    private ListView<TableDiagnosticsWrapper> listview;

    @FXML
    private ComboBox<Database> databaseChooser;

    @FXML
    private Button checkCheckbox;
    @FXML
    private CheckBox localCheckboxInRepair;

    @FXML
    private Button repairButton;

    @FXML
    private CheckBox quickCheckboxInRepair;

    @FXML
    private Button cancelButton;

    @FXML
    private Button optimiseButton;

    @FXML
    private CheckBox localCheckboxInAnalyze;

    @FXML
    private CheckBox useFrmCheckboxInRepair;

    @FXML
    private CheckBox extendedCheckboxInRepair;

    @FXML
    private CheckBox localCheckboxInOptimise;

    @FXML
    private Button analyzeButton;

    @FXML
    private ComboBox<String> operationCombobox;

    
    private Stage dialog;
    private ConnectionSession session;
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    private class TableDiagnosticsWrapper {
        DBTable table;
        SimpleBooleanProperty checked = new SimpleBooleanProperty(false) {
            @Override
            protected void invalidated() {
                super.invalidated();
                
                checkVisibility();
            }
            
        };
        public TableDiagnosticsWrapper(DBTable table){
            this.table = table;
            selectDeselectCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {

                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                    checked.setValue(t1);
                }
            });
        }

        @Override
        public String toString() {
            return table != null ? table.getName() : "";
        }
    }
    
    public void checkVisibility() {
        boolean enabled = false;
        
        for (TableDiagnosticsWrapper row: listview.getItems()) {
            if (row.checked.get()) {
                enabled = true;
                break;
            }
        }
        
        analyzeButton.setDisable(!enabled);
        checkCheckbox.setDisable(!enabled);
        extendedCheckboxInRepair.setDisable(!enabled);
        localCheckboxInAnalyze.setDisable(!enabled);
        localCheckboxInOptimise.setDisable(!enabled);
        localCheckboxInRepair.setDisable(!enabled);
        operationCombobox.setDisable(!enabled);
        optimiseButton.setDisable(!enabled);
        quickCheckboxInRepair.setDisable(!enabled);
        repairButton.setDisable(!enabled);
        useFrmCheckboxInRepair.setDisable(!enabled);
    }
    
    
    private List<String> getSelectedTables() {
        
        List<String> list = new ArrayList<>();
        
        for (TableDiagnosticsWrapper row: listview.getItems()) {
            if (row.checked.get() && row.table != null) {
                list.add(row.table.getName());
            }
        }
        
        return list;
    }
    
    
    public void optimizeAction(ActionEvent event) {
        
        List<String> names = getSelectedTables();
        if (!names.isEmpty()) {
            makeBusy(true);
            
            Thread th = new Thread(() -> {
                
                String tables = String.join(", ", names);
                String option = "";
                String query = "OPTIMIZE %s TABLE %s";
                if (localCheckboxInOptimise.isSelected()) {
                    option = "LOCAL";
                }
                
                query = String.format(query, option, tables);
                
                try {
                    ((AbstractDBService)session.getService()).changeDatabase(databaseChooser.getSelectionModel().getSelectedItem());
                    try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
                        List<Map<String, String>> list = new ArrayList<>();
                        if (rs != null) {
                            while (rs.next()) {
                                Map<String, String> m = new HashMap<>();
                                m.put("table", rs.getString("Table"));
                                m.put("op", rs.getString("Op"));
                                m.put("msgType", rs.getString("Msg_type"));
                                m.put("msgText", rs.getString("Msg_text"));
                                
                                list.add(m);
                            }
                            Platform.runLater(() -> {
                                DiagnoticTableResultController control =  ((MainController)getController()).getTypedBaseController("DiagnosticTableResultDialog");
                                Parent p = control.getUI();
                                Stage stage = new Stage();
                                stage.initStyle(StageStyle.UTILITY);
                                
                                Scene scene = new Scene(p);
                                stage.setScene(scene);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(getController().getStage());
                                stage.setTitle("Result");
                        
                                control.init(list);
                                
                                stage.showAndWait();
                            });
                        }
                    }
                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", "Error on executing OPTIMIZE query", ex);
                    });
                } finally {
                    makeBusy(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    public void checkAction(ActionEvent event) {
        List<String> names = getSelectedTables();
        if (!names.isEmpty()) {
            makeBusy(true);
            
            Thread th = new Thread(() -> {
                
                String tables = String.join(", ", names);
                String option = "";
                String query = "CHECK TABLE %s %s";
                
                String value = operationCombobox.getSelectionModel().getSelectedItem();
                if (value != null && !"None".equals(value)) {
                    option = value;
                }
                
                query = String.format(query, tables, option);
                
                try {
                    ((AbstractDBService)session.getService()).changeDatabase(databaseChooser.getSelectionModel().getSelectedItem());
                    session.getService().execute(query);
                    try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
                        List<Map<String, String>> list = new ArrayList<>();
                        if (rs != null) {
                            while (rs.next()) {
                                Map<String, String> m = new HashMap<>();
                                m.put("table", rs.getString("Table"));
                                m.put("op", rs.getString("Op"));
                                m.put("msgType", rs.getString("Msg_type"));
                                m.put("msgText", rs.getString("Msg_text"));
                                
                                list.add(m);
                            }
                            Platform.runLater(() -> {
                                DiagnoticTableResultController control = ((MainController)getController()).getTypedBaseController("DiagnosticTableResultDialog");
                                Parent p = control.getUI();
                                Stage stage = new Stage();
                                stage.initStyle(StageStyle.UTILITY);
                                
                                Scene scene = new Scene(p);
                                stage.setScene(scene);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(getController().getStage());
                                stage.setTitle("Result");
                        
                                control.init(list);
                                
                                stage.showAndWait();
                            });
                        }
                    }
                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", "Error on executing CHECK query", ex);
                    });
                } finally {
                    makeBusy(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
        
    }
    
    public void analyzeAction(ActionEvent event) {
        List<String> names = getSelectedTables();
        if (!names.isEmpty()) {
            makeBusy(true);
            
            Thread th = new Thread(() -> {
                
                String tables = String.join(", ", names);
                String option = "";
                String query = "ANALYZE %s TABLE %s";
                
                if (localCheckboxInAnalyze.isSelected()) {
                    option = "LOCAL";
                }
                
                query = String.format(query, option, tables);
                
                try {
                    ((AbstractDBService)session.getService()).changeDatabase(databaseChooser.getSelectionModel().getSelectedItem());
                    try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
                        List<Map<String, String>> list = new ArrayList<>();
                        if (rs != null) {
                            while (rs.next()) {
                                Map<String, String> m = new HashMap<>();
                                m.put("table", rs.getString("Table"));
                                m.put("op", rs.getString("Op"));
                                m.put("msgType", rs.getString("Msg_type"));
                                m.put("msgText", rs.getString("Msg_text"));
                                
                                list.add(m);
                            }
                            Platform.runLater(() -> {
                                DiagnoticTableResultController control = ((MainController)getController()).getTypedBaseController("DiagnosticTableResultDialog");
                                Parent p = control.getUI();
                                Stage stage = new Stage();
                                stage.initStyle(StageStyle.UTILITY);
                                
                                Scene scene = new Scene(p);
                                stage.setScene(scene);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(getController().getStage());
                                stage.setTitle("Result");
                        
                                control.init(list);
                                
                                stage.showAndWait();
                            });
                        }
                    }
                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", "Error on executing ANALYZE query", ex);
                    });
                } finally {
                    makeBusy(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }        
    }
    
    public void repairAction(ActionEvent event) {
        
        List<String> names = getSelectedTables();
        if (!names.isEmpty()) {
            makeBusy(true);
            
            Thread th = new Thread(() -> {
                
                String tables = String.join(", ", names);
                String option0 = "";
                String option1 = "";
                String option2 = "";
                String option3 = "";
                String query = "REPAIR %s TABLE %s %s %s %s";
                
                if (localCheckboxInRepair.isSelected()) {
                    option0 = "LOCAL";
                }
                
                if (quickCheckboxInRepair.isSelected()) {
                    option1 = "QUICK";
                }
                
                if (extendedCheckboxInRepair.isSelected()) {
                    option2 = "EXTENDED";
                }
                
                if (useFrmCheckboxInRepair.isSelected()) {
                    option3 = "USE_FRM";
                }
                
                query = String.format(query, option0, tables, option1, option2, option3);
                
                try {
                    ((AbstractDBService)session.getService()).changeDatabase(databaseChooser.getSelectionModel().getSelectedItem());
                    try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
                        List<Map<String, String>> list = new ArrayList<>();
                        if (rs != null) {
                            while (rs.next()) {
                                Map<String, String> m = new HashMap<>();
                                m.put("table", rs.getString("Table"));
                                m.put("op", rs.getString("Op"));
                                m.put("msgType", rs.getString("Msg_type"));
                                m.put("msgText", rs.getString("Msg_text"));
                                
                                list.add(m);
                            }
                            Platform.runLater(() -> {
                                
                                DiagnoticTableResultController control = ((MainController)getController()).getTypedBaseController("DiagnosticTableResultDialog");
                                Parent p = control.getUI();
                                Stage stage = new Stage();
                                stage.initStyle(StageStyle.UTILITY);
                                
                                Scene scene = new Scene(p);
                                stage.setScene(scene);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(((MainController)getController()).getStage());
                                stage.setTitle("Result");
                        
                                control.init(list);
                                
                                stage.showAndWait();
                            });
                        }
                    }
                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", "Error on executing REPAIR query", ex);
                    });
                } finally {
                    makeBusy(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        operationCombobox.getItems().addAll("None", "FOR UPGRADE", "QUICK", "FAST", "MEDIUM", "EXTENDED", "CHANGED");
        operationCombobox.getSelectionModel().selectFirst();
        
        
        listview.setCellFactory(CheckBoxListCell.forListView(new Callback<TableDiagnosticsWrapper, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableDiagnosticsWrapper item) {
                return item.checked;
            }
            
            
        }));
        
        
        cancelButton.setOnAction(onAction);

        titleImageView.setImage(SmartImageProvider.getInstance().getTableDiagnostics_image());
    }
    
    
    public void init(ConnectionSession session, Window win)throws SQLException{
        
        this.session = session;
        
        databaseChooser.getItems().addAll(session.getService().getDatabases());
        databaseChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Database>() {

           @Override
           public void changed(ObservableValue<? extends Database> ov,
                   Database t, Database data) { 
               //now listview can benenfit
               selectDeselectCheckbox.setSelected(false);
               listview.getItems().clear();  
               if(data == null){
                   return;
               }
               try {
                   for(Object table : session.getService().getTables(data)){
                       TableDiagnosticsWrapper tdw = new TableDiagnosticsWrapper((DBTable)table);
                       listview.getItems().add(tdw);
                   }                   
               } catch (SQLException ex) {}
           }
       });
        
        
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setResizable(false);
        dialog.setTitle("Table Diagnostics");
        dialog.setScene(new Scene(getUI()));
        databaseChooser.getSelectionModel().selectFirst();
        dialog.showAndWait();
        
        
    }
    
    private EventHandler<ActionEvent> onAction = new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent t) {
            Button b = (Button)t.getSource();
            if(b == cancelButton){
                dialog.close();
            }
        }
    };

}
