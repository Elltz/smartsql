
package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class MessagesTabModule extends AbstractStandardModule {

//    private static final Paint  ERROR_SELECTION_PAINT       = Color.web("#ffb3b3");
    
    
    private static final String URL_DEV_MYSQL               = "http://dev.mysql.com/doc/refman/8.0/en/error-messages-server.html";
    
    private Image messageImage = null;
    private HashMap<ConnectionSession, List<Tab>> tabHolder = new HashMap();
    private final String messageTabText = "Messages";          
    
    private HashMap<ConnectionSession, Tab> bottomTabs = new HashMap();
    private HashMap<ConnectionSession, Tab> messageTabs = new HashMap();
        
    @Inject
    private PreferenceDataService preferenceService;
    
    
    
    @Override
    public boolean install() {
        return true;
    }
    
    @Override
    public int getOrder() {
        return 6;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) { return true;}

    @Override
    public boolean uninstall() { 
        tabHolder.clear();
        return true;
    }

    @Override
    public void postConnection(ConnectionSession session, Tab t) throws Exception {

        if (messageImage == null) {
            messageImage = StandardDefaultImageProvider.getInstance().getMessageInformation_image();
        }
        Tab tab = new Tab(messageTabText);
        tab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(messageImage), 18, 18));
        final MessageText textFlow = new MessageText();
        ScrollPane sp = new ScrollPane(textFlow);
//        textArea.setEditable(false);
        ObservableMap<Tab, MessageData> messageResults = FXCollections.observableHashMap();
        messageResults.addListener((MapChangeListener.Change<? extends Tab, ? extends MessageData> change) -> {
            if(change.wasAdded()){
                
                sp.setHvalue(sp.getHmin());
                sp.setVvalue(sp.getVmin());
                
                change.getKey().selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t1, Boolean t2) -> {
                    if (t2) {                        
                        Object obj = change.getValueAdded();
                        if (obj instanceof String) {
                            textFlow.getChildren().setAll(new Text[] {new Text((String)obj)});
                        } else {
                            textFlow.init(change.getValueAdded());
                        }
                    }
                });
                
                change.getKey().setOnClosed((Event t1) -> {
                    change.getMap().remove(change.getKey());
                });                
            }
        });
        textFlow.setUserData(messageResults);
        textFlow.setPadding(new Insets(2, 0, 0, 5));
        sp.getStyleClass().add("messageScroll");
        
        textFlow.setStyle(FontUtils.parseCSSFont(preferenceService.getPreference().getFontsOthers()));
        
        ComboBox<MessageData.MessageType> messageTypeComboBox = new ComboBox<>();
        messageTypeComboBox.getItems().addAll(MessageData.MessageType.values());
        messageTypeComboBox.getSelectionModel().select(MessageData.MessageType.ALL);
        
        messageTypeComboBox.setPrefHeight(20);
        messageTypeComboBox.setMaxHeight(20);
        messageTypeComboBox.setMinHeight(20);
        
        HBox messageFooter = new HBox(messageTypeComboBox);
        messageFooter.setStyle("-fx-background-color: #293955; -fx-alignment: center-left; -fx-padding: 0 0 0 2");
        messageFooter.setPrefHeight(24);
        
        messageTypeComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends MessageData.MessageType> observable, MessageData.MessageType oldValue, MessageData.MessageType newValue) -> {
            textFlow.changeType(newValue != null ? newValue : MessageData.MessageType.ALL);
        });
        
        BorderPane bp = new BorderPane(sp) {
            @Override
            public void requestFocus() {
                super.requestFocus();
                // This part for requesting foruc to needed node
                // dont remove this!!!
                textFlow.requestFocus();
            }            
        };
        bp.setBottom(messageFooter);
        
        tab.setContent(bp);
        tab.setClosable(false);         
        messageTabs.put(session, tab);
                
        Platform.runLater(() -> {
            addToMessageSector(session, tab, true);
            textFlow.prefWidthProperty().bind(tab.getTabPane().widthProperty().subtract(50));
        });
    }
    
    public TextFlow getSessionMessageBottomTabTextFlow(ConnectionSession session){
        TabContentController tcc  = ((MainController)getController()).getTabControllerBySession(session);
        if(tcc instanceof ConnectionTabContentController){
            ConnectionTabContentController ctcc = (ConnectionTabContentController)tcc;
            for(Tab tab : ctcc.getBottamTabs()){
                if(tab.getText().charAt(0) == 'M'){//trying to save computation time
                    if(tab.getText().equals(messageTabText)){
                        return (TextFlow)((ScrollPane)((BorderPane)tab.getContent()).getCenter()).getContent();                               
                    }
                }
            }
        }
        return null;
    }
    
    public void addToMessageSector(ConnectionSession session, Tab tab, boolean showTab){
        if(tab.getGraphic() == null){
            tab.setGraphic(new ImageView(messageImage));
        }
        
        getController().addTabToBottomPane(session, tab);
        
        List<Tab> tabs = tabHolder.get(session);
        if (tabs == null) {
            tabs = new ArrayList<>();
            tabHolder.put(session, tabs);
        }
        
        tabs.add(tab);
        tab.setOnCloseRequest((Event t) -> {
            List<Tab> list = tabHolder.get(session);
            if (list != null) {
                list.remove(tab);
            }            
        });
        
        bottomTabs.put(session, tab);
        
        if (showTab) {
            getController().showTab(tab, session);
        }
    }
    
    public void messageTabToFront(ConnectionSession session) {
        Tab tab = messageTabs.get(session);
        if (tab != null)
        {
            getController().showTab(tab, session);
        }
    }
    
    
    public void removeFromMessageSector(ConnectionSession session, Tab tab){
        getController().removeTabFromBottomPane(session, tab);
        
        List<Tab> list = tabHolder.get(session);
        if (list != null) {
            list.remove(tab);
        }
    }

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    
//    @Override
//    public boolean onRestore(Map<?, ?> restoreImage) {
////        if(restoreImage == null){
////            return false;
////        }
////        if (tabHolder.get(getController().
////                        getSelectedConnectionSession().get()) == null) {
////            try {
////                postConnection(getController().
////                        getSelectedConnectionSession().get(), null);
////            } catch (Exception ex) {}
////        }
////        
////
//////        ((TextArea)((AnchorPane)tabHolder.get(getController().
//////                        getSelectedConnectionSession().get())
//////                .getContent()).getChildren().get(0)).setText(
//////                        ((Map<Byte,String>)restoreImage).get((byte)
//////                                getController().
//////                        getSelectedConnectionSession().get()
//////                        .getConnectionId()));
////
////        for (Tab t: tabHolder.get(getController().getSelectedConnectionSession().get())) {
////            ((TextArea)((Pane)t.getContent()).getChildren().get(0)).setText(
////                ((Map<Byte, String>)restoreImage).get((byte)
////                        getController().
////                getSelectedConnectionSession().get()
////                .getConnectionId()));
////        }
//        return true;
//    }
    
    @Override
    public String getInfo() { 
        return "";
    }
    
    
    
    
    public class MessageText extends TextFlow {
        
        private MessageData.MessageType type = MessageData.MessageType.ALL;
        private MessageData messageData;
        
        private final DecimalFormat format = new DecimalFormat("##.###");
        
        
        public void init(MessageData messageData) {
            this.messageData = messageData;
            showMessages();
        }
        
        public void changeType(MessageData.MessageType type) {
            this.type = type;
            showMessages();
        }
        
        
        private void showMessages() {
                        
            String cssFont = FontUtils.parseCSSFont(preferenceService.getPreference().getFontsOthers());
            setStyle(cssFont);
            
            if (messageData != null) {
                List<Node> texts = new ArrayList<>();

                texts.add(new Text(messageData.getQueriesSize() + " queries executed, " + messageData.getSuccessCount() + " success, " + (messageData.getQueriesSize() - (messageData.getSuccessCount() + messageData.getErrorsCount())) + " skipped, " + messageData.getErrorsCount() + " errors, " + messageData.getWarningsCount() + " warnings\n\n"));

                for (MessageDataItem item : messageData.getItems()) {
                    if (checkItem(item)) {
                        
                        TextField t0 = createSelectableText("");
                        t0.setStyle(t0.getStyle() + cssFont);
                        
                        String query = item.getQuery();
                        Text more;
                        if (query.length() > 150) {
                                                   
                            String subQuery = query.substring(0, 150) + "...";
                            
                            more = new Text("More");
                            more.setStyle(cssFont);
                            more.setFill(Color.CORNFLOWERBLUE);
                            more.setUnderline(true);
                            more.setCursor(Cursor.HAND);
                            more.setOnMouseClicked((MouseEvent event) -> {
                                if ("More".equals(more.getText())) {
                                    t0.setText(query + " ");
                                    more.setText("Hide");
                                } else {
                                    t0.setText(subQuery + " ");
                                    more.setText("More");
                                }
                            });
                            
                            t0.setText(subQuery + " ");
                        } else {
                            more = null;
                            t0.setText(query);
                        }
                        
                        Text t = new Text("Query:");
                        t.setFill(Color.BLUE);
                        t.setFont(Font.font(t.getFont().getFamily(), FontWeight.BOLD, t.getFont().getSize()));
                        t.setStyle(cssFont);
                        
                        texts.add(t);
                        texts.add(new Group(t0));
                        if (more != null) {
                            texts.add(more);
                        }
                        
//                        texts.add(new Text("\n\n"));
                        
                        String message = item.getMessage();
                        if (message != null) {
                            if (message.startsWith("Error Code:")) {

                                String codeText = message.split("\\n")[0];

                                Label link = new Label(codeText);
                                link.setStyle(cssFont + "-fx-text-fill: red; -fx-underline: true");
                                link.setTooltip(new Tooltip("dev.mysql.com"));
                                link.setCursor(Cursor.HAND);
                                link.setOnMouseClicked((MouseEvent event) -> {
                                    URLUtils.openUrl(URL_DEV_MYSQL);
                                });
                                texts.add(new Text("\n"));
                                texts.add(link);

                                message = message.replace(codeText, "");
                            }
                            
//                            RSyntaxTextAreaBuilder msg = createSelectableTextArea(message.trim(), item.isError());
//                            if (item.getCustomRange() != null) {
//                                msg.getTextArea().select(item.getCustomRange()[0], item.getCustomRange()[1]);
//                                msg.getTextArea().setSelectionColor(java.awt.Color.decode("#ffb3b3"));
//                            }

                            TextArea area = new TextArea(message.trim());
                            area.setWrapText(true);
                            area.getStyleClass().add("message-text-area");
                            area.setPrefRowCount(message.split("\n").length);
                            area.setEditable(false);
                            area.setFocusTraversable(false);
                            area.prefWidthProperty().bind(widthProperty());
                            
                            area.setStyle(cssFont);
                            
                            texts.add(new Text("\n"));
                            texts.add(area);
                        }
                        
                        if (item.isError()) {
                            Label googleLabel = new Label();
                            googleLabel.setTooltip(new Tooltip("Google search"));
                            googleLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getGoogle_image()));
                            googleLabel.setCursor(Cursor.HAND);
                            googleLabel.setOnMouseClicked((MouseEvent event) -> {
                                URLUtils.openSearchUrl(URLUtils.SEARCH__GOOGLE_LINK, item.getMessage());
                            });
                            
                            Label stackoverflowLabel = new Label();
                            stackoverflowLabel.setTooltip(new Tooltip("Stackoverflow search"));
                            stackoverflowLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getStackOverflow_image()));
                            stackoverflowLabel.setCursor(Cursor.HAND);
                            stackoverflowLabel.setOnMouseClicked((MouseEvent event) -> {
                                URLUtils.openSearchUrl(URLUtils.SEARCH__STACKOVERFLOW_LINK, item.getMessage());
                            });
                            
                            Label yahooLabel = new Label();
                            yahooLabel.setTooltip(new Tooltip("Yahoo search"));
                            yahooLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getYahoo_image()));
                            yahooLabel.setCursor(Cursor.HAND);
                            yahooLabel.setOnMouseClicked((MouseEvent event) -> {
                                URLUtils.openSearchUrl(URLUtils.SEARCH__YAHOO_LINK, item.getMessage());
                            });
                            
                            Label bingLabel = new Label();
                            bingLabel.setTooltip(new Tooltip("Bing search"));
                            bingLabel.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getBing_image()));
                            bingLabel.setCursor(Cursor.HAND);
                            bingLabel.setOnMouseClicked((MouseEvent event) -> {
                                URLUtils.openSearchUrl(URLUtils.SEARCH__BING_LINK, item.getMessage());
                            });
                            
                            HBox b = new HBox(5, googleLabel, stackoverflowLabel, yahooLabel, bingLabel);
                            b.setPadding(new Insets(-20, 0, 0, 0));
                            texts.add(b);
                            texts.add(new Text("\n"));
                        }
                        
//                        texts.add(new Text("\n"));

                        Text t2 = new Text("Execution Time: " + format.format(item.getExecutionTime()) + " sec\n"
                        + "Transfer Time: " + format.format(item.getTransferTime()) + " sec\n"
                        + "Total Time: " + format.format(item.getResponseTime()) + " sec\n");
                        
                        t2.setStyle(cssFont);
                        
                        texts.add(t2);
                        
                        if (item.isWarning()) {
                            PreferenceData pd = preferenceService.getPreference();
                            if (pd.isShowWarnings()) {
                                int size = item.getWarningCodes() != null ? item.getWarningCodes().size() : 0;
                                for (int j = 0; j < size; j++) {
                                   Text t3 = new Text("\nWarning Code : " + item.getWarningCodes().get(j) + "\n" + item.getWarningMessages().get(j) + "\n");
                                   t3.setStyle(cssFont);
                                   texts.add(t3);
                                }
                            } else {
                                Text t3 = new Text("\nNote: To see complete list of warning(s), enable Tools -> Preferences -> General -> Show Warning(s) in Messages Tab\n");
                                t3.setStyle(cssFont);
                                texts.add(t3);
                            }
                        }
                        
                        texts.add(new Text("----------------------------------------------------------------\n\n"));
                    }
                }
            
                getChildren().setAll(texts);
            }
        }
        
        private TextField createSelectableText(String text) {
            TextField t1 = new TextField(text);
            t1.prefWidthProperty().bind(widthProperty());
            t1.setEditable(false);
            t1.setStyle("-fx-background-color: transparent ; -fx-background-insets: 0px; -fx-font-size: 13px; -fx-padding: 0; ");
            
            return t1;
        }
        
        private RSyntaxTextAreaBuilder createSelectableTextArea(String text, boolean error) {
            
            RSyntaxTextAreaBuilder t1 = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), getController().getSelectedConnectionSession().get());
            widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                SwingUtilities.invokeLater(() -> {
                    t1.getSwingNode().resize(newValue.doubleValue(), 18 * t1.getTextArea().getLineCount());
                });
            });
            SwingUtilities.invokeLater(() -> {
                t1.init();
                t1.getTextArea().setEditable(false);
                t1.getTextArea().setWrapStyleWord(true);
            });
            //t1.getStyleClass().add("selectableTextArea" + (error ? "-error" : ""));
            
            return t1;
        }
        
        private boolean checkItem(MessageDataItem item) {
            switch (type) {
                
                case ALL:
                    return true;
                
                case ERRORS:
                    return item.isError();
                    
                case ERRORS_WARNINGS:
                    return item.isError() || item.isWarning();
                    
                case WARNINGS:
                    return item.isWarning();
                    
                case RESULT_SET:
                    return item.isResultSet();
                    
                case WITHOUT_RESULT_SER:
                    return !item.isResultSet();
            }
            
            return true;
        }
    }
}
