package np.com.ngopal.smart.sql.ui.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class EditingComboBoxCell<T, V> extends TableCell<T, Object> {
    
    protected ComboBox<V> comboBox;
    protected HBox box;
    
    private final ObservableList<V> values;
    private final Function<Object, V> findSelectionValue;
    private final BiFunction<Integer, Object, Boolean> checkAcceptedValue;
    private final BiFunction<Integer, Object, Void> commitValue;
    
    private boolean comited = false;
    private boolean editable = true;
    
    public EditingComboBoxCell(ObservableList<V> values, Function<Object, V> findSelectionValue, BiFunction<Integer, Object, Boolean> checkAcceptedValue, BiFunction<Integer, Object, Void> commitValue) {
        this(values, findSelectionValue, checkAcceptedValue, commitValue, true);
    }
            
    public EditingComboBoxCell(ObservableList<V> values, Function<Object, V> findSelectionValue, BiFunction<Integer, Object, Boolean> checkAcceptedValue, BiFunction<Integer, Object, Void> commitValue, boolean editable) {
        setStyle("-fx-padding: 0 0 0 0; -fx-alignment: center-left;");
        this.values = values;
        this.findSelectionValue = findSelectionValue;
        this.checkAcceptedValue = checkAcceptedValue;
        this.commitValue = commitValue;
        this.editable = editable;
    }

    public ObservableList<V> getValues() {
        return null;
    }
    
    
    @Override
    public void startEdit() {
        super.startEdit();
        
        comited = false;
        if (comboBox == null) {
            createComboBox();
        }
                
        ObservableList<V> list = getValues();
        if (list != null) {
            comboBox.setItems(list);
        }
            
        setGraphic(box);
        
        if (editable) {
            comboBox.getEditor().setText(getString());
        }
        comboBox.getSelectionModel().select(findSelectionValue != null ? findSelectionValue.apply(getString()) : (V)getItem());
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        Platform.runLater(() -> {
            comboBox.requestFocus();
            comboBox.getEditor().selectAll();
        });
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getString());
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (comboBox != null && editable) {
                    comboBox.getEditor().setText(getString());
                }
                comboBox.getSelectionModel().select(findSelectionValue != null ? findSelectionValue.apply(getItem()) : (V)getItem());
                
                setGraphic(box);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                setText(getString());
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
    }
    
    private Object getValue() {
        return comboBox.isEditable() ? comboBox.getEditor().getText() : comboBox.getSelectionModel().getSelectedItem() != null ? comboBox.getSelectionModel().getSelectedItem() : null;
    }

    private void createComboBox() {
        comboBox = new ComboBox(values);
        comboBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        comboBox.setEditable(editable);
        
        new AutoCompleteComboBoxListener<>(comboBox);
        Node n = comboBox.isEditable() ? comboBox.getEditor() : comboBox;
        n.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!comited && oldValue != null && oldValue == true && newValue != null && newValue == false) {
                
                Object value = getValue();
                if (value instanceof String && findSelectionValue != null) {
                    value = comboBox.getConverter().fromString((String) value);
                }
                if (checkAcceptedValue == null || checkAcceptedValue.apply(getIndex(), value)) {
                    commitValue.apply(getIndex(), value);
                    commitEdit(value);                    
                } else {
                    cancelEdit();
                }
            }
        });
        
        comboBox.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if (!comboBox.isShowing() && event.getCode() == KeyCode.ENTER) {
                comited = true;
                
                Object value = getValue();
                if (value instanceof String && findSelectionValue != null) {
                    value = comboBox.getConverter().fromString((String) value);
                }
                
                if (checkAcceptedValue == null || checkAcceptedValue.apply(getIndex(), value)) {
                    commitValue.apply(getIndex(), value);
                    commitEdit(value);                    
                } else {
                    cancelEdit();
                }
            } else if (event.getCode() == KeyCode.ESCAPE) {
                comited = true;
                cancelEdit();
            }
        });

        comboBox.setConverter(new StringConverter<V>() {
            @Override
            public String toString(V object) {
                return object != null ? object.toString() : "";
            }

            @Override
            public V fromString(String string) {
                return findSelectionValue != null ? findSelectionValue.apply(string) : null;
            }
        });

        box = new HBox(comboBox);
        HBox.setHgrow(comboBox, Priority.ALWAYS);
        box.setFillHeight(true);
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    /**
     *
     * @param forward true gets the column to the right, false the column to the left of the current column
     * @return
     */
    private TableColumn<T, ?> getNextColumn(boolean forward) {
        List<TableColumn<T, ?>> columns = new ArrayList<>();
        for (TableColumn<T, ?> column : getTableView().getColumns()) {
            columns.addAll(getLeaves(column));
        }
        //There is no other column that supports editing.
        if (columns.size() < 2) {
            return null;
        }
        int currentIndex = columns.indexOf(getTableColumn());
        int nextIndex = currentIndex;
        if (forward) {
            nextIndex++;
            if (nextIndex > columns.size() - 1) {
                nextIndex = 0;
            }
        } else {
            nextIndex--;
            if (nextIndex < 0) {
                nextIndex = columns.size() - 1;
            }
        }
        return columns.get(nextIndex);
    }

    private List<TableColumn<T, ?>> getLeaves(TableColumn<T, ?> root) {
        List<TableColumn<T, ?>> columns = new ArrayList<>();
        if (root.getColumns().isEmpty()) {
            //We only want the leaves that are editable.
            if (root.isEditable()) {
                columns.add(root);
            }
            return columns;
        } else {
            for (TableColumn<T, ?> column : root.getColumns()) {
                columns.addAll(getLeaves(column));
            }
            return columns;
        }
    }
}
