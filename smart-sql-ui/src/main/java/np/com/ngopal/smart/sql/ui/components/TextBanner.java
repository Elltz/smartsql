package np.com.ngopal.smart.sql.ui.components;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.web.WebView;
import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class TextBanner extends HBox {

    private WebView webView;
    private Button closeButton;

    private List<String> messages = new ArrayList<>();

    @Getter
    private boolean inited = false;
    private volatile boolean runned = false;
    private int currentLine = 0;
    
    private static final String LEFT  = "<html><body style='background: #d9e3f4; font-size: 14px; margin-top: 4px; margin-left: 10px; overflow-x: hidden; overflow-y: hidden;'>";
    private static final String RIGHT = "</body></html>";

    public TextBanner() { }
    
    public void prepareUiState(){
        
        setStyle(
            "-fx-background-color: \n" +
            "   linear-gradient(#f2f2f2, #d6d6d6),\n" +
            "   linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%),\n" +
            "   linear-gradient(#dddddd 0%, #f6f6f6 50%);\n" +
            "-fx-background-insets: 0,1,2;" +
            "-fx-padding: 3 0 4 0");
        
        webView = new WebView();
        closeButton = new Button();
        closeButton.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getSimpleClose_image()), 12, 12));
        closeButton.setStyle(
            "-fx-background-color: #d9e3f4;\n" +
            "-fx-padding: 5");
        closeButton.setMinHeight(22);
        closeButton.setPrefHeight(22);
        closeButton.setMaxHeight(22);
        closeButton.setOnAction((ActionEvent event) -> {
            stop();
        });
                    
        webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState) {
                if (newState == Worker.State.SUCCEEDED) {
                    
                    Document doc = webView.getEngine().getDocument();
                    NodeList lista = doc.getElementsByTagName("a");
                    
                    for (int i = 0; i < lista.getLength(); i++) {
                        Node refNode = lista.item(i);
                        ((EventTarget)refNode).addEventListener("click", new EventListener() {
                            @Override
                            public void handleEvent(Event evt) {
                                URLUtils.openUrl(refNode.getAttributes().getNamedItem("href").getNodeValue());
                                evt.preventDefault();
                            }
                        }, false);
                    }
                }
            }
        });

        getChildren().add(webView);
        getChildren().add(closeButton);        
        HBox.setHgrow(webView, Priority.ALWAYS);        
        setMaxHeight(30);
        setMinHeight(30);
        setPrefHeight(30);
        stop();
    }

    private String currentText;

    public void init(String text) {

        inited = true;
        
        currentText = text;

        parseTextToNodes();
    }

    private void parseTextToNodes() {
        if (currentText != null) {
            
            for (String s : currentText.split("<!(-)+!>")) {                
                messages.add(LEFT + s.trim() + RIGHT);
            }
        }
    }

    public void start(long delayInMillisec) {
        boolean canStart = false;
        currentLine = 0;
        if (!messages.isEmpty()) {

            Platform.runLater(() -> {
                webView.getEngine().loadContent(messages.get(0));
                
                setVisible(true);
                setManaged(true);
            });

            canStart = true;
        }
        
        if (canStart) {
            Thread th = new Thread(() -> {
                runned = true;
                while (runned) {
                    try {
                        Thread.sleep(delayInMillisec);
                    } catch (InterruptedException ex) {
                    }
                    
                    currentLine++;
                    if (currentLine >= messages.size()) {
                        currentLine = 0;
                    }
                    
                    Platform.runLater(() -> {
                        webView.getEngine().loadContent(messages.get(currentLine));
                    });                    
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }

    public void stop() {
        runned = false;
        Platform.runLater(() -> {
            webView.getEngine().loadContent("");
            
            setVisible(false);
            setManaged(false);
        });
    }
}
