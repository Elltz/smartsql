/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class TestQuery {

    /**
     * For checking the index of keywords specially for r5 voilation
     * <p>
     * @param s
     * @return
     */
    private boolean checkIndex(String s) {
        String checks = "SELECT|FROM|WHERE|GROUP BY|HAVING|ORDER BY";
        int lastInd = -2;
        for (String split : checks.split("\\|")) {

            int ind = s.indexOf(split);
            if (ind >= 0) {
                if (ind > lastInd) {
                    lastInd = ind;
                } else {
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * Makes clean SQL text
     * <p>
     * @param s
     * @return
     */
    public String cleanFormat(String s) {
        String checks = "SELECT|FROM|WHERE|GROUP BY|HAVING|ORDER BY";
        for (String split : checks.split("\\|")) {
            int ind = s.toUpperCase().indexOf(split);
            if (ind >= 0) {
                s = s.substring(0, ind) + split + s.substring(ind + split.length());
            }
        }
        return s.replace("\n", "");
    }

    public String checkSQL(String s) throws SQLValidationException {
        String old = s;
        int start = s.indexOf("(SELECT");
        int end = s.lastIndexOf(")");
        if (start > 2) {
            String subQuery = s.substring(start + 1);
            s = checkSQL(subQuery);
            if (s.length() == subQuery.length()) {
                System.out.println("R7 voliation");
                return "";
            }
        } else {
            s = (start >= 0 && end > 0) ? s.substring(start, end - 1) : s;
        }
//        String regex = "^(INSERT INTO|UPDATE|SELECT|DELETE)(?:[^;]|(?:'.*?'))+;\\s*$";
//String regex="^(SELECT)" +
//"    (ALL | DISTINCT | DISTINCTROW )?" +
//"     (HIGH_PRIORITY)?" +
//"      (STRAIGHT_JOIN)?" +
//"      (SQL_SMALL_RESULT])? (SQL_BIG_RESULT)? (SQL_BUFFER_RESULT)?" +
//"      (SQL_CACHE | SQL_NO_CACHE)? (SQL_CALC_FOUND_ROWS)?" +
////"    select_expr [, select_expr ...]\n" +
//"    (FROM .*" +
//"    (WHERE .*)?" +
//"    (GROUP BY (.*)? " +
//"      (ASC | DESC)?)" +
//"    (HAVING .*)?" +
//"    (ORDER BY .*)?" +
//"      [ASC | DESC], ...]\n" +
//"    [LIMIT {[offset,] row_count | row_count OFFSET offset}]\n" +
//"    [PROCEDURE procedure_name(argument_list)]\n" +
//"    [INTO OUTFILE 'file_name' export_options\n" +
//"      | INTO DUMPFILE 'file_name'\n" +
//"      | INTO var_name [, var_name]]\n" +
//"    [FOR UPDATE | LOCK IN SHARE MODE])?";
        String R1 = "^SELECT.*";
        Pattern pR1 = Pattern.compile(R1);
        String R2 = ".*(WHERE|GROUP BY|HAVING|ORDER BY).*";
        Pattern pR2 = Pattern.compile(R2);
        String R3 = ".*(GROUP BY).*(HAVING).*";
        Pattern pR3 = Pattern.compile(R3);
        String R6 = "^(SELECT)\\s[a-zA-Z\\'\\,\\.\\*]+?\\sFROM.*";
        Pattern pR6 = Pattern.compile(R6);
        String msg = null;

        //R1
        if (!pR1.matcher(s).find()) {
            throw new SQLValidationException("R1 voilation ");
        } //R2
        else if (pR2.matcher(s).find() && !s.contains("FROM")) {
            throw new SQLValidationException("R2 voilation");
        } //R3
        else if (s.contains("HAVING") && !pR3.matcher(s).find()) {
            throw new SQLValidationException("R3 voilation");
        } //R5
        else if (!checkIndex(s)) {
            throw new SQLValidationException("R5 voilation");
        } //R6
        else if (s.contains("FROM") && !pR6.matcher(s).find()) {
            throw new SQLValidationException("R6 voilation");
        }
//        Pattern p = Pattern.compile(regex, Pattern.MULTILINE | Pattern.DOTALL);
//        if (!p.matcher(s).find()) {
//            throw new SQLValidationException("INCORRECT SQL");
//        }
        System.out.println(msg);

        return (start >= 0 && end > 0) ? old.substring(0, start) + old.substring(end + 1) : old;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestQuery t = new TestQuery();
        String s = "SELECT * FROM KSDL WHERE id=2;";
//                  + "FRDSFOM EMP";
        String newQuery;
        try {
            newQuery = t.checkSQL(t.cleanFormat(s));
            System.out.println("Valid");

        } catch (SQLValidationException ex) {
            Logger.getLogger(TestQuery.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Invalid");
        }

    }

}
