package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 * @author Andrii Tsepushel &lt; a.tsepushel@gmail.com &gt;
 */
public class SessionPointDialogController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
       
    
    @FXML
    private TableView<UserSession> sessionsTable;

    @FXML
    private TableColumn<UserSession, String> nameCol;    
    @FXML
    private TableColumn<UserSession, String> lastUpdateCol;

    private UserSession selectedSession;

    private Stage stage;

    public class UserSession {
        private String name;
        private String lastUpdate;
        private String connection;
        
        public UserSession(String name, String conenction, String lastUpdate) {
            this.name = name;
            this.lastUpdate = lastUpdate;
            this.connection = conenction;
        }
        
        public String getName() {
            return name;
        }
        
        public String getLastUpdate() {
            return lastUpdate;
        }
        
        public String getConnection() {
            return connection;
        }
    }
    

    @FXML
    void cancel(ActionEvent event) {
        selectedSession = null;
        
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @FXML
    void ok(ActionEvent event) {
        selectedSession = sessionsTable.getSelectionModel().getSelectedItem();
        
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    
    public UserSession getSelectedSession() {
        return selectedSession;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void init(List<Map<String, String>> userSessions) {
        for (Map<String, String> m: userSessions) {
            sessionsTable.getItems().add(new UserSession(m.get("name"), m.get("connection"), m.get("updated")));
        }
                
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name")); 
        lastUpdateCol.setCellValueFactory(new PropertyValueFactory<>("lastUpdate"));
    }

}
