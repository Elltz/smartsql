package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.Mnemonic;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.DesignerDiagramHistory;
import np.com.ngopal.smart.sql.db.DesignerTableTemplateService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DesignerTableTemplate;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerImagePane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerLayerTittledPane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerPane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerTableTittledPane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerTextPane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerViewPane;
import np.com.ngopal.smart.sql.ui.fxgraph.FXEdge;
import np.com.ngopal.smart.sql.ui.fxgraph.FXGraph;
import np.com.ngopal.smart.sql.ui.fxgraph.FXGraphBuilder;
import np.com.ngopal.smart.sql.ui.fxgraph.FXNode;
import np.com.ngopal.smart.sql.ui.fxgraph.FXNodeBuilder;
import np.com.ngopal.smart.sql.ui.provider.SmartBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaDesignerTabContentController extends TabContentController {

    private static final Image SCHEMA_PRIVATE = new Image("/np/com/ngopal/smart/sql/ui/images/designer/schema_private.png");
    private static final Image SCHEMA_PUBLIC  = new Image("/np/com/ngopal/smart/sql/ui/images/designer/schema_public.png");
    
    public static final String STYLE_CLASS__DESIGNER_GRAPH = "designer-graph";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_HEADER = "designer-component-header";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_CONENT = "designer-component-content";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_FOOTER = "designer-component-footer";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_MORE_LABEL = "designer-component-more-label";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_HEADER_EXPANDED = "designer-component-header-expanded";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_HEADER_LABEL = "designer-component-header-label";
    public static final String STYLE_CLASS__DESIGNER_BUTTON = "designer-button";
    public static final String STYLE_CLASS__DESIGNER_BUTTON_HOVER = "designer-button-hover";
    public static final String STYLE_CLASS__DESIGNER_BUTTON_SELECTED = "designer-button-selected";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_INDEXES_HEADER_LABEL = "designer-component-indexes-header-label";
    public static final String STYLE_CLASS__DESIGNER_COMPONENT_CONTAINER = "designer-component-container";
    
    public static final String STYLE_CLASS__DESIGNER_LAYER = "designer-layer";
    public static final String STYLE_CLASS__DESIGNER_LAYER_HEADER_EXPANDED = "designer-layer-header-expanded";
    
    public static final String STYLE_CLASS__DESIGNER_TEXT = "designer-text";
    
    public static final String STYLE_CLASS__DESIGNER_IMAGE = "designer-image";
    
    public static final String STYLE_CLASS__DESIGNER_VIEW = "designer-view";
    
    public static final String DIAGRAM_TYPE__LOCAL     = "Local";
    public static final String DIAGRAM_TYPE__PUBLIC    = "Public";
    public static final String DIAGRAM_TYPE__ALL       = "All";
    
    private final ObjectProperty<Tab> nullTab = new SimpleObjectProperty<>();
    private final ObjectProperty<TreeItem> nullTreeItem = new SimpleObjectProperty<>();
    
    private static final Color SOURCE_COLOR = Color.web("b3f0b3");
    private static final Background SOURCE_BACKGROUND = new Background(new BackgroundFill(SOURCE_COLOR, CornerRadii.EMPTY, Insets.EMPTY));
    
    private static final Color DESTINATION_COLOR = Color.web("b3e1ff");
    private static final Background DESTINATION_BACKGROUND = new Background(new BackgroundFill(DESTINATION_COLOR, CornerRadii.EMPTY, Insets.EMPTY));
    
    private final MenuItem cutItem = new MenuItem();
    private final MenuItem copyItem = new MenuItem();
    private final MenuItem pasteItem = new MenuItem();
    private final MenuItem editItem = new MenuItem();
    private final MenuItem deleteItem = new MenuItem();
    
    private DesignerData copiedData;
    private double contexMenuX = 0;
    private double contexMenuY = 0;
    
    private ConnectionSession session;
    
    @Inject
    public SmartBaseControllerProvider provider;
    
    @FXML
    private AnchorPane mainUI;
    @FXML
    private AnchorPane centerPane;
    
    @FXML
    private Canvas birdsEye;
    
    @FXML
    private TreeView catalogTree;
    @FXML
    private TreeView layersTree;
    
    @FXML
    private TextArea descriptionTextArea;
    
    @FXML
    private Button addLayerButton;
    @FXML
    private Button addTextButton;
    @FXML
    private Button addImageButton;
    @FXML
    private Button addTableButton;
    @FXML
    private Button addViewButton;
    @FXML
    private Button removeButton;
    @FXML
    private Button identifyingButton;
    @FXML
    private Button nonIdentifyingButton;
    @FXML
    private Button nonIdentifyingOneToOneButton;
    @FXML
    private Button identifyingOneToOneButton;
    @FXML
    private Button manyToManyButton;
    
    @FXML
    private SplitPane centerSplit;
    
    @FXML
    private AnchorPane bottomContainer;
    @FXML
    private TabPane bottomTabPane;
    
    @FXML
    private ListView modelingAdiitionsListview;
    @FXML
    private AnchorPane templateRightPane;
    @FXML
    private SplitPane rootSplitPane;
        
    @FXML
    private TextField diagramName;
    @FXML
    private HBox diagramPane;
    
    @FXML
    private ListView<ErrDiagram> savedDiagramsListview;
    
    @FXML
    private ComboBox<String> zoomComboBox;
    @FXML
    private ImageView zoomIn;
    @FXML
    private ImageView zoomOut;
    
    @FXML
    private ComboBox<String> diagramTypeCombobox;
    
    @FXML
    private TitledPane diagramsTitledPane;
    
    @FXML
    private TableView<DesignerProperty> propertiesTable;
    @FXML
    private TableColumn<DesignerProperty, String> propertiesNameColumn;
    @FXML
    private TableColumn<DesignerProperty, Object> propertiesValueColumn;
    
    @FXML
    private ListView<DesignerDiagramHistory.HistoryPoint> historyListView;
    
    @Inject
    private DesignerTableTemplateService dtts;
    
        
    private GraphNodeType nodeForAdd;
    private Button lastSelectedButton;
    
    private FXGraph graph;
    private FXGraphBuilder graphBuilder;
    
    private volatile boolean disableHidePath = false;

    @Getter
    private DesignerDatabase designerDatabase;
    
    private DesignerTable sourceRelation;

    @Getter
    private DesignerErrDiagram currentDiagram;
    
    @Getter
    private DesignerDiagramHistory designerDiagramHistory;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    private Font currentFont;
    
    
    @Override
    public void changeFont(String font) {
        currentFont = FontUtils.parseFXFont(font);
        catalogTree.refresh();
        layersTree.refresh();
    }

    
    public enum GraphNodeType {
        LAYER("#F0F1FE", new Image("/np/com/ngopal/smart/sql/ui/images/designer/layer.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_layer.png"), false),
        TEXT("#fefded", new Image("/np/com/ngopal/smart/sql/ui/images/designer/text.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_text.png"), false),
        IMAGE(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/image.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_image.png"), false),
        TABLE("#98BFDA", new Image("/np/com/ngopal/smart/sql/ui/images/designer/table.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_table.png"), true),
        VIEW("#fede58", new Image("/np/com/ngopal/smart/sql/ui/images/designer/view.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_view.png"), false),
        REMOVE(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/remove.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_remove.png"), false),
        NON_IDENTIFYING_MANY_TO_ONE(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/non_identifying.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_non_identifying.png"), false),
        IDENTIFYING_MANY_TO_ONE(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/identifying.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_identifying.png"), false),
        NON_IDENTIFYING_ONE_TO_ONE(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/non_identifyingOneToOne.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_non_identifying.png"), false),
        IDENTIFYING_ONE_TO_ONE(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/identifyingOneToOne.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_non_identifying.png"), false),
        MANY_TO_MANY(null, new Image("/np/com/ngopal/smart/sql/ui/images/designer/manyToMany.png"), new Image("/np/com/ngopal/smart/sql/ui/images/designer/cursor_non_identifying.png"), false);
        
        private final Image image;
        private final Image cursor;
        private final String color;
        private final boolean canCollaps;
        GraphNodeType(String color, Image image, Image cursor, boolean canCollaps) {
            this.image = image;
            this.color = color;
            this.cursor = cursor;
            this.canCollaps = canCollaps;
        }
        
        
        public Image getImage() {
            return image;
        }
        
        public Image getCursor() {
            return cursor;
        }
        
        public String getColor() {
            return color;
        }
        
        public boolean isCanCollaps() {
            return canCollaps;
        }
        
        
    };
    
    
    private final Map<DesignerLayer, FXNode> layersMap = new HashMap<>();
    
    @Getter
    private final Map<DesignerTable, FXNode> tablesMap = new HashMap<>();
    @Getter
    private final Map<DesignerText, FXNode> textsMap = new HashMap<>();
    @Getter
    private final Map<DesignerImage, FXNode> imagesMap = new HashMap<>();
    @Getter
    private final Map<DesignerView, FXNode> viewsMap = new HashMap<>();
    
    private final Map<DesignerTable, NotifyForeightKeysListener> listenersMap = new HashMap<>();
    private final Map<DesignerTable, List<FXEdge>> edgesMap = new HashMap<>();
    private final Map<DesignerTable, List<FXEdge>> destinationEdges = new HashMap();
    
    private TreeItem catalogRootItem;
    private TreeItem tablesItem;
    private TreeItem viewsItem;
    
    private TreeItem layersRootItem;
    
    private final BooleanProperty hasUndo = new SimpleBooleanProperty(false);
    private final BooleanProperty hasRedo = new SimpleBooleanProperty(false);
    private final BooleanProperty hasPaste = new SimpleBooleanProperty(false);
    
    
    public SchemaDesignerTabContentController() {
        cutItem.setOnAction((ActionEvent event) -> {
            cutSelected();
        });
        
        copyItem.setOnAction((ActionEvent event) -> {
            copySelected();
        });
        
        pasteItem.setOnAction((ActionEvent event) -> {
            pasteSelected();
        });
        
        editItem.setOnAction((ActionEvent event) -> {
            editSelected();
        });
        
        deleteItem.setOnAction((ActionEvent event) -> {
            deleteSelected();
        });
    }
    
    public String getSelectedDiagramType() {
        return diagramTypeCombobox.getSelectionModel().getSelectedItem();
    }
    
    public void setDisableDiagramTitledPane(boolean disabled) {
        diagramsTitledPane.setDisable(disabled);
    }
    
    @Override
    public void addOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }
    
    @Override
    public void removeOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }
    
    public void init(DesignerErrDiagram diagram) {
        
        registrateAddButton(addLayerButton, GraphNodeType.LAYER, new KeyCodeCombination(KeyCode.L, KeyCombination.ALT_DOWN));
        registrateAddButton(addTextButton, GraphNodeType.TEXT, new KeyCodeCombination(KeyCode.N, KeyCombination.ALT_DOWN));
        registrateAddButton(addImageButton, GraphNodeType.IMAGE, new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN));
        registrateAddButton(addTableButton, GraphNodeType.TABLE, new KeyCodeCombination(KeyCode.T, KeyCombination.ALT_DOWN));
        registrateAddButton(addViewButton, GraphNodeType.VIEW, new KeyCodeCombination(KeyCode.V, KeyCombination.ALT_DOWN));
        registrateAddButton(removeButton, GraphNodeType.REMOVE, new KeyCodeCombination(KeyCode.DELETE, KeyCombination.ALT_DOWN));
        registrateAddButton(nonIdentifyingButton, GraphNodeType.NON_IDENTIFYING_MANY_TO_ONE, new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.ALT_DOWN));
        registrateAddButton(identifyingButton, GraphNodeType.IDENTIFYING_MANY_TO_ONE, new KeyCodeCombination(KeyCode.DIGIT2, KeyCombination.ALT_DOWN));
        registrateAddButton(nonIdentifyingOneToOneButton, GraphNodeType.NON_IDENTIFYING_ONE_TO_ONE, new KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.ALT_DOWN));
        registrateAddButton(identifyingOneToOneButton, GraphNodeType.IDENTIFYING_ONE_TO_ONE, new KeyCodeCombination(KeyCode.DIGIT4, KeyCombination.ALT_DOWN));
        registrateAddButton(manyToManyButton, GraphNodeType.MANY_TO_MANY, new KeyCodeCombination(KeyCode.DIGIT5, KeyCombination.ALT_DOWN));
        
        ((MainController)getController()).usedSchemaDesigner();
        
        if (diagram != null) {
            catalogRootItem.valueProperty().bind(diagram.database());
            designerDiagramHistory = new DesignerDiagramHistory(diagram);
            historyListView.setItems(designerDiagramHistory.getHistoryPoints());

            designerDiagramHistory.getHistoryPoints().addListener((ListChangeListener.Change<? extends DesignerDiagramHistory.HistoryPoint> c) -> {
                hasUndo.set(!designerDiagramHistory.getHistoryPoints().isEmpty());
            });
            
            designerDiagramHistory.disabledHistoryPointProperty().addListener((ObservableValue<? extends DesignerDiagramHistory.HistoryPoint> observable, DesignerDiagramHistory.HistoryPoint oldValue, DesignerDiagramHistory.HistoryPoint newValue) -> {
                DesignerDiagramHistory.HistoryPoint hp = designerDiagramHistory.getLastEnabledHistoryPoint();

                DesignerErrDiagram d;
                if (hp != null) {
                    d = new DesignerErrDiagram();
                    d.fromMap(hp.getDiagram());
                    
                    hasRedo.set(hp != designerDiagramHistory.getHistoryPoints().get(designerDiagramHistory.getHistoryPoints().size() - 1));
                } else {
                    d = new DesignerErrDiagram();
                    hasRedo.set(false);
                }

                if (!designerDiagramHistory.getHistoryPoints().get(0).isEnabled()) {
                    hasUndo.set(false);
                    
                } else {
                    hasUndo.set(!designerDiagramHistory.getHistoryPoints().isEmpty());
                }
                
                restoreDiagram(d);
            });
        }
        
        restoreDiagram(diagram);        
    }
    
    
    public BooleanProperty hasUndo() {
        return hasUndo;
    }
    
    public BooleanProperty hasRedo() {
        return hasRedo;
    }
    
    public BooleanProperty hasPaste() {
        return hasPaste;
    }
    
    public void undo() {
        designerDiagramHistory.selectHistoryPoint(designerDiagramHistory.getLastEnabledHistoryPoint());
        refreshHistory();
    }
    
    public void redo() {
        DesignerDiagramHistory.HistoryPoint hp = designerDiagramHistory.getLastEnabledHistoryPoint();
        int index = designerDiagramHistory.getHistoryPoints().indexOf(hp);
        designerDiagramHistory.selectHistoryPoint(designerDiagramHistory.getHistoryPoints().get(index + 1));
        refreshHistory();
    }
    
    
    
    
    public void zoomIn() {        
        int idx = zoomComboBox.getSelectionModel().getSelectedIndex();
        if (idx > 0) {
            idx--;
            zoomComboBox.getSelectionModel().select(idx);
        }
    }
    
    public void zoomOut() {
        int idx = zoomComboBox.getSelectionModel().getSelectedIndex();
        if (idx < zoomComboBox.getItems().size() - 1) {
            idx++;
            zoomComboBox.getSelectionModel().select(idx);
        }
    }
    
    public void zoom100() {
        zoomComboBox.getSelectionModel().select("100%");
    }
        
    public void cutSelected() {
        FXNode selectedNode = graph.getSelectedNode();
        if (selectedNode != null) {            
            copiedData = ((DesignerPane)selectedNode.getWrapedNode()).getData();
            hasPaste.set(true);

            // remove node from graph
            removeNode(selectedNode);
        }
    }
    
    public void copySelected() {
        FXNode selectedNode = graph.getSelectedNode();
        if (selectedNode != null) {            
            copiedData = ((DesignerPane)selectedNode.getWrapedNode()).getData().copyData();
            copiedData.name().set(copiedData.name().get() + "_copy");
            hasPaste.set(true);
        }
    }
    
    public void pasteSelected() {
        if (copiedData != null) {            

            copiedData.x().set(contexMenuX);
            copiedData.y().set(contexMenuY);

            addDesignerData(copiedData, false);
            
            copiedData = null;
            hasPaste.set(false);
        }
    }
    
    public void deleteSelected() {
        FXNode selectedNode = graph.getSelectedNode();
        if (selectedNode != null) {            
            removeNode(selectedNode);
        }
    }
    
    public void editSelected() {
        FXNode selectedNode = graph.getSelectedNode();
        if (selectedNode != null) {            
            showDesignerTab(((DesignerPane)selectedNode.getWrapedNode()).getData());
        }
    }
    
    public SimpleObjectProperty<FXNode> selectedNode() {
        return graph.selectedNodeProperty();
    }
    
    private void restoreDiagram(DesignerErrDiagram diagram) {
        centerSplit.setDisable(diagram == null);
        diagramPane.setDisable(diagram == null);
        
        graph.clearNodes();
        graph.clearEdges();
        
        edgesMap.clear();
        destinationEdges.clear();
        listenersMap.clear();
        
        tablesMap.clear();
        layersMap.clear();
        
        bottomTabPane.getTabs().clear();
        
        if (currentDiagram != null) {
            diagramName.textProperty().unbindBidirectional(currentDiagram.name());
        }
        
        currentDiagram = diagram;
        if (diagram != null) {            
            diagramName.textProperty().bindBidirectional(currentDiagram.name());
        }
        
        
        
        Platform.runLater(() -> {
            
            viewsItem.getChildren().clear();
            tablesItem.getChildren().clear();
            
            layersRootItem.getChildren().clear();

            if (diagram != null) {
                
                for (DesignerText t: diagram.texts()) { 
                    t.setParent(diagram);
                    showText(t, GraphNodeType.TEXT);            
                    addTextTreeItem(t);
                }

                for (DesignerImage img: diagram.images()) { 
                    img.setParent(diagram);
                    showImage(img, false);            
                    addImageTreeItem(img);
                }

                for (DesignerView view: diagram.views()) { 
                    view.setParent(diagram);
                    showView(view, GraphNodeType.VIEW);            
                    addViewTreeItem(view);
                }

                for (DesignerTable t: diagram.tables()) {    
                    t.setParent(diagram);
                    showTable(t, GraphNodeType.TABLE);
                    addTableTreeItem(t);
                }

                for (DesignerTable t: diagram.tables()) {
                    t.setParent(diagram);
                    updateEdges(t);
                }
                
                for (DesignerLayer l: diagram.layers()) { 
                    l.setParent(diagram);
                    showLayer(l, GraphNodeType.LAYER);            
                    addLayerTreeItem(l);
                }
                
                diagram.startListenChanges();
            }
            
            updateBirdsEye();
        });
    }
    
    
    private void calcTableSize(DesignerTable t) {
        double columnHegiht = 18.5;
        t.width().set(200);
        if (t.columns().size() >= 7 && !t.indexes().isEmpty() || t.columns().size() >= 8) {
            t.height().set(200);
        } else {
            t.height().set(t.columns().size() * columnHegiht + (t.indexes().isEmpty() ? 44 : 74));
        }
    }
    
    public void increaseWidth() {
        graph.increaseWidth();
    }
    
    public void increaseHeight() {
        graph.increaseHeight();
    }
    
    
    // TODO Refactor ADD and REMOVE methods
    private void addTableTreeItem(DesignerTable t) {
        
        TreeItem tableItem = null;
        for (Object obj: tablesItem.getChildren()) {
            TreeItem ti = (TreeItem) obj;
            if (t.equals(((TreeItemDesignerTable)ti.getValue()).getData())) {
                
                ti.setValue(new TreeItemDesignerTable(t));
                tableItem = ti;
                break;
            }
        }
        
        TreeItem layerViewItem = new TreeItem(new TreeItemDesignerTable(t));
        
        if (tableItem == null) {
            tableItem = new TreeItem(new TreeItemDesignerTable(t));            
            tablesItem.getChildren().add(tableItem);
            
            TreeItem ti = tableItem;
            // tree item refreshing
            t.name().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                ti.setValue(new TreeItemDesignerTable(t));
                layerViewItem.setValue(new TreeItemDesignerTable(t));
            });
        }

        
        TreeItem parentItem = layersRootItem;
        if (t.layerUuid().get() != null) {
            for (Object ti: layersRootItem.getChildren()) {
                if (((TreeItem)ti).getValue() instanceof TreeItemDesignerLayer) {
                    if (((TreeItemDesignerLayer)((TreeItem)ti).getValue()).getData().uuid().get().equals(t.layerUuid().get())) {
                        parentItem = (TreeItem)ti;
                    }
                }
            }
        }

        parentItem.getChildren().add(layerViewItem);
    }
    
    private void removeTableTreeItem(DesignerTable t) {        
        if (t != null) {
            for (Object obj: tablesItem.getChildren()) {
                TreeItem ti = (TreeItem) obj;
                if (t.equals(((TreeItemDesignerTable)ti.getValue()).getData())) {
                    ti.setValue(new TreeItemDesignerTable(t, true));
                    //tablesItem.getChildren().remove(ti);
                    break;
                }
            }
            
            removeTreeItemFromLayers(layersRootItem, t);
        }
    }
    
    private void addLayerTreeItem(DesignerLayer t) {
        TreeItem layerItem = new TreeItem(new TreeItemDesignerLayer(t));            
        layersRootItem.getChildren().add(layerItem);
        
        // tree item refreshing
        t.name().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            layerItem.setValue(new TreeItemDesignerLayer(t));
        });
    }
    private void removeLayerTreeItem(DesignerLayer t) {      
        if (t != null) {            
            removeTreeItemFromLayers(layersRootItem, t);
        }
    }
    
    private void addViewTreeItem(DesignerView v) {
        TreeItem viewItem = new TreeItem(new TreeItemDesignerView(v));            
        viewsItem.getChildren().add(viewItem);
        
        TreeItem layerViewItem = new TreeItem(new TreeItemDesignerView(v));
        TreeItem parentItem = layersRootItem;
        if (v.layerUuid().get() != null) {
            for (Object ti: layersRootItem.getChildren()) {
                if (((TreeItem)ti).getValue() instanceof TreeItemDesignerLayer) {
                    if (((TreeItemDesignerLayer)((TreeItem)ti).getValue()).getData().uuid().get().equals(v.layerUuid().get())) {
                        parentItem = (TreeItem)ti;
                    }
                }
            }
        }
        
        parentItem.getChildren().add(layerViewItem);
        
        
        // tree item refreshing
        v.name().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            viewItem.setValue(new TreeItemDesignerView(v));
            layerViewItem.setValue(new TreeItemDesignerView(v));
        });
    }
    
    
    private void removeViewTreeItem(DesignerView t) {      
        if (t != null) {
            for (Object obj: viewsItem.getChildren()) {
                TreeItem ti = (TreeItem) obj;
                if (t.equals(((TreeItemDesignerView)ti.getValue()).getData())) {
                    viewsItem.getChildren().remove(ti);
                    break;
                }
            }
            
            removeTreeItemFromLayers(layersRootItem, t);
        }
    }
    
    private void addTextTreeItem(DesignerText t) {
        TreeItem textViewItem = new TreeItem(new TreeItemDesignerText(t));
        TreeItem parentItem = layersRootItem;
        if (t.layerUuid().get() != null) {
            for (Object ti: layersRootItem.getChildren()) {
                if (((TreeItem)ti).getValue() instanceof TreeItemDesignerLayer) {
                    if (((TreeItemDesignerLayer)((TreeItem)ti).getValue()).getData().uuid().get().equals(t.layerUuid().get())) {
                        parentItem = (TreeItem)ti;
                    }
                }
            }
        }
        
        parentItem.getChildren().add(textViewItem);
        
        
        // tree item refreshing
        t.name().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            textViewItem.setValue(new TreeItemDesignerText(t));
        });
    }
    private void removeTextTreeItem(DesignerText t) {      
        if (t != null) {            
            removeTreeItemFromLayers(layersRootItem, t);
        }
    }
    
    private void addImageTreeItem(DesignerImage img) {
        TreeItem imageViewItem = new TreeItem(new TreeItemDesignerImage(img));
        TreeItem parentItem = layersRootItem;
        if (img.layerUuid().get() != null) {
            for (Object ti: layersRootItem.getChildren()) {
                if (((TreeItem)ti).getValue() instanceof TreeItemDesignerLayer) {
                    if (((TreeItemDesignerLayer)((TreeItem)ti).getValue()).getData().uuid().get().equals(img.layerUuid().get())) {
                        parentItem = (TreeItem)ti;
                    }
                }
            }
        }
        
        parentItem.getChildren().add(imageViewItem);
        
        
        // tree item refreshing
        img.name().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            imageViewItem.setValue(new TreeItemDesignerImage(img));
        });
    }
    private void removeImageTreeItem(DesignerImage t) {
        if (t != null) {            
            removeTreeItemFromLayers(layersRootItem, t);
        }
    }
    
    
    private boolean removeTreeItemFromLayers(TreeItem parent, DesignerData data) {
        if (data != null && parent != null) {
            for (Object obj: parent.getChildren()) {
                TreeItem ti = (TreeItem) obj;
                if (data.equals(((TreeItemDesigner)ti.getValue()).getData())) {
                    parent.getChildren().remove(ti);
                    return true;
                }
                if (ti.getChildren() != null && !ti.getChildren().isEmpty()) {
                    if (removeTreeItemFromLayers(ti, data)) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    
    public FXNode getDesignerDataNode(DesignerData dt) {
        FXNode node = tablesMap.get(dt);
        if (node == null) {
            node = textsMap.get(dt);
        }
        
        if (node == null) {
            node = imagesMap.get(dt);
        }
        
        if (node == null) {
            node = viewsMap.get(dt);
        }
        
        if (node == null) {
            node = layersMap.get(dt);
        }
        
        return node;
    }
    
   
    
    
    private void showText(DesignerText t, GraphNodeType type) {
        FXNodeBuilder theNodeBuilder = new FXNodeBuilder(graph);
      
        if (t.color().get() == null) {
            t.color().set(type.color);
        }
        
        DesignerTextPane pane = new DesignerTextPane(this, t);
                
        FXNode node0 = theNodeBuilder
                .node(pane)
                .canChangeHeigt(n -> true)
                .x(t.x().get())
                .y(t.y().get())
                .color(n -> t.color().get() != null ? t.color().get() : type.color)
            .build();

        if (t.width().get() > 0 && t.height().get() > 0) {
            pane.setPrefSize(t.width().get(), t.height().get());
        }
                
        pane.layoutXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.x().set(newValue.doubleValue());
            }
        });
        
        pane.layoutYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.y().set(newValue.doubleValue());
            }
        });
        
        pane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.width().set(newValue.doubleValue());
            }
        });
        
        pane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.height().set(newValue.doubleValue());
            }
        });
        
                
        ChangeListener<Number> moveListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            DesignerLayer layer = currentDiagram.findLayer(t.x().get(), t.y().get());
            t.layerUuid().set(layer != null ? layer.uuid().get() : null);
            
            removeTextTreeItem(t);
            addTextTreeItem(t);
        };
        
        DesignerLayer layer = currentDiagram.findLayer(t.x().get(), t.y().get());
        t.layerUuid().set(layer != null ? layer.uuid().get() : null);
            
        t.x().addListener(moveListener);
        t.y().addListener(moveListener);
        t.width().addListener(moveListener);
        t.height().addListener(moveListener);
        
        textsMap.put(t, node0);
    }
    
    
    // TODO Check this
    private volatile boolean imageChangingInListener = false;
    private void showImage(DesignerImage img, boolean needChooseImage) {
        
        String fileName = img.filename().get();
        String imageData = img.imageData().get();
        
        if (needChooseImage) {
            FileChooser fc = new FileChooser();
            File f = fc.showOpenDialog(getStage());
            if (f != null) {
                try {
                    fileName = f.getName();
                    imageData = Base64.getEncoder().encodeToString(Files.readAllBytes(f.toPath()));
                } catch (IOException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
        
        if (fileName != null && !fileName.trim().isEmpty()) {
            
            img.filename().set(fileName);
            img.imageData().set(imageData);
            
            FXNodeBuilder theNodeBuilder = new FXNodeBuilder(graph);

            DesignerImagePane pane = new DesignerImagePane(this, img);

            FXNode node0 = theNodeBuilder
                    .node(pane)
                    .canChangeHeigt(n -> true)
                    .x(img.x().get())
                    .y(img.y().get())
                    .canChangeCorners(n -> !img.aspectRatio().get())
                    .color(n -> null)
                .build();

            if (img.width().get() > 0 && img.height().get() > 0) {
                pane.setPrefSize(img.width().get(), img.height().get());
            }

            pane.layoutXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                if (newValue != null) {
                    img.x().set(newValue.doubleValue());
                }
            });
            
            pane.layoutYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                if (newValue != null) {
                    img.y().set(newValue.doubleValue());
                }
            });
            
            
            pane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                if (newValue != null) {
                    img.width().set(newValue.doubleValue());
                }
            });

            pane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                if (newValue != null) {
                    img.height().set(newValue.doubleValue());
                }
            });

            ChangeListener<Number> widthListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                if (img.aspectRatio().get()) {
                    if (!imageChangingInListener) {
                        imageChangingInListener = true;
                        
                        double alpha = img.imageDefaultHeight().get() / img.imageDefaultWidth().get();
                        pane.setPrefSize(newValue.doubleValue(), (int) newValue.doubleValue() * alpha);
                        pane.resize(pane.getPrefWidth(), pane.getPrefHeight());
                        
                        imageChangingInListener = false;
                    }
                } else {
                    pane.setPrefSize(img.width().get(), img.height().get());
                    pane.resize(pane.getPrefWidth(), pane.getPrefHeight());
                }
            };
            ChangeListener<Number> heightListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                if (img.aspectRatio().get()) {
                    if (!imageChangingInListener) {
                        imageChangingInListener = true;
                        
                        double alpha = img.imageDefaultWidth().get() / img.imageDefaultHeight().get();
                        pane.setPrefSize((int) newValue.doubleValue() * alpha, newValue.doubleValue());
                        pane.resize(pane.getPrefWidth(), pane.getPrefHeight());
                        
                        imageChangingInListener = false;
                    }
                } else {
                    pane.setPrefSize(img.width().get(), img.height().get());
                    pane.resize(pane.getPrefWidth(), pane.getPrefHeight());
                }
            };

            ChangeListener<Number> moveListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                DesignerLayer layer = currentDiagram.findLayer(img.x().get(), img.y().get());
                img.layerUuid().set(layer != null ? layer.uuid().get() : null);
                
                removeImageTreeItem(img);
                addImageTreeItem(img);
            };

            DesignerLayer layer = currentDiagram.findLayer(img.x().get(), img.y().get());
            img.layerUuid().set(layer != null ? layer.uuid().get() : null);

            img.x().addListener(moveListener);
            img.y().addListener(moveListener);
            
            img.width().addListener(moveListener);
            img.width().addListener(widthListener);
            
            img.height().addListener(moveListener);
            img.height().addListener(heightListener);

            imagesMap.put(img, node0);
        }
    }
    
    
    private void showLayer(DesignerLayer l, GraphNodeType type) {
        FXNodeBuilder theNodeBuilder = new FXNodeBuilder(graph);
      
        if (l.color().get() == null) {
            l.color().set(type.color);
        }
        
        DesignerLayerTittledPane pane = new DesignerLayerTittledPane(this, l);
                
        FXNode node0 = theNodeBuilder
                .node(pane)
                .canChangeHeigt(n -> true)
                .x(l.x().get())
                .y(l.y().get())
                .color(n -> l.color().get() != null ? l.color().get() : type.color)
            .build();

        if (l.width().get() > 0 && l.height().get() > 0) {
            pane.setPrefSize(l.width().get(), l.height().get());
        }
        
        pane.layoutXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                l.x().set(newValue.doubleValue());
            }
        });

        pane.layoutYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                l.y().set(newValue.doubleValue());
            }
        });


        pane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                l.width().set(newValue.doubleValue());
            }
        });

        pane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                l.height().set(newValue.doubleValue());
            }
        });
                    
        
        l.x().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (oldValue != null && newValue != null) {
                
                double diff = newValue.doubleValue() - oldValue.doubleValue();
                
                // TODO Need refactor this
                List<DesignerData> designerData = currentDiagram.findLayerData(l.uuid().get());
                if (designerData != null && !designerData.isEmpty()) {
                    for (DesignerData dt: designerData) {
                        
                        FXNode node = getDesignerDataNode(dt);
                        if (node != null) {
                            node.translatePosition(diff, 0, 1.0, null);
                        }
                    }
                }
            }
        });
        
        l.y().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (oldValue != null && newValue != null) {
                
                double diff = newValue.doubleValue() - oldValue.doubleValue();
                
                List<DesignerData> designerData = currentDiagram.findLayerData(l.uuid().get());
                if (designerData != null && !designerData.isEmpty()) {
                    for (DesignerData dt: designerData) {
                        
                        FXNode node = getDesignerDataNode(dt);
                        if (node != null) {
                            node.translatePosition(0, diff, 1.0, null);
                        }
                    }
                }
            }
        });

        layersMap.put(l, node0);
    }
    
    
    private void showTable(DesignerTable t, GraphNodeType type) {
        FXNodeBuilder theNodeBuilder = new FXNodeBuilder(graph);
                    
        if (t.color().get() == null || t.color().get().isEmpty()) {
            t.color().set(type.color);
        }
        
        NotifyForeightKeysListener l = new NotifyForeightKeysListener(t);
        listenersMap.put(t, l);

        t.foreignKeysChanged().addListener(l);

        designerDatabase.tables().add(t);
        
        DesignerTableTittledPane pane = new DesignerTableTittledPane(this, t, type);
        
        pane.getHeaderBox().setOnMouseEntered((MouseEvent event) -> {
            List<FXEdge> sourceEdges = edgesMap.get(t);
            if (sourceEdges != null) {
                for (FXEdge e: sourceEdges) {
                    e.drawEdgePath(true, SOURCE_COLOR, SOURCE_BACKGROUND);
                }
            }
            
            List<FXEdge> destinationeEdges = destinationEdges.get(t);
            if (destinationeEdges != null) {
                for (FXEdge e: destinationeEdges) {
                    e.drawEdgePath(true, DESTINATION_COLOR, DESTINATION_BACKGROUND);
                }
            }
        });
        
        pane.getHeaderBox().setOnMouseExited((MouseEvent event) -> {
            
            if (!disableHidePath) {
                Bounds b0 = pane.getHeaderBox().localToScene(pane.getHeaderBox().getBoundsInLocal());
                if (event.getSceneX() < b0.getMinX() || event.getSceneX() > b0.getMaxX() || 
                        event.getSceneY() < b0.getMinY() || event.getSceneY() > b0.getMaxY()) {

                    List<FXEdge> sourceEdges = edgesMap.get(t);
                    if (sourceEdges != null) {
                        for (FXEdge e: sourceEdges) {
                            e.drawEdgePath(false, SOURCE_COLOR, SOURCE_BACKGROUND);
                        }
                    }

                    List<FXEdge> destinationeEdges = destinationEdges.get(t);
                    if (destinationeEdges != null) {
                        for (FXEdge e: destinationeEdges) {
                            e.drawEdgePath(false, DESTINATION_COLOR, DESTINATION_BACKGROUND);
                        }
                    }
                }
            }
        });
        
        pane.getHeaderBox().setOnMousePressed((MouseEvent event) -> {
            disableHidePath = true;
        });
        
        pane.getHeaderBox().setOnMousePressed((MouseEvent event) -> {
            disableHidePath = false;
        });
                
        FXNode node0 = theNodeBuilder
                .node(pane)
                .canChangeHeigt(node -> !((DesignerTableTittledPane)node).isCollapsed())
                .x(t.x().get())
                .y(t.y().get())
                .color(n -> t.color().get() != null || t.color().get().isEmpty() ? t.color().get() : type.color)
            .build();

        if (t.width().get() > 0 && t.height().get() > 0) {
            pane.setPrefSize(t.width().get(), t.height().get());
        }
               
        pane.layoutXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.x().set(newValue.doubleValue());
            }
        });

        pane.layoutYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.y().set(newValue.doubleValue());
            }
        });


        pane.prefWidthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.width().set(newValue.doubleValue());
            }
        });

        pane.prefHeightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                t.height().set(newValue.doubleValue());
            }
        });
        
        ChangeListener<Number> moveListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            DesignerLayer layer = currentDiagram.findLayer(t.x().get(), t.y().get());
            
            String oldV = t.layerUuid().get();            
            t.layerUuid().set(layer != null ? layer.uuid().get() : "");
            
            if (!Objects.equals(oldV, t.layerUuid().get())) {
                removeTableTreeItem(t);
                addTableTreeItem(t);
            }
        };
        
        DesignerLayer layer = currentDiagram.findLayer(t.x().get(), t.y().get());
        t.layerUuid().set(layer != null ? layer.uuid().get() : "");
        
        t.x().addListener(moveListener);
        t.y().addListener(moveListener);
        t.width().addListener(moveListener);
        t.height().addListener(moveListener);

        tablesMap.put(t, node0);
    }
    
    private void showView(DesignerView view, GraphNodeType type) {
        FXNodeBuilder theNodeBuilder = new FXNodeBuilder(graph);                    
      
        if (view.color().get() == null) {
            view.color().set(type.color);
        }
        
        DesignerViewPane pane = new DesignerViewPane(this, view, type);
        
        
        FXNode node0 = theNodeBuilder
                .node(pane)
                .canChangeHeigt(node -> false)
                .x(view.x().get())
                .y(view.y().get())
                .color(n -> view.color().get() != null ? view.color().get() : type.color)
            .build();

        if (view.width().get() > 0 && view.height().get() > 0) {
            pane.setPrefSize(view.width().get(), view.height().get());
        }
                
        pane.layoutXProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                view.x().set(newValue.doubleValue());
            }
        });

        pane.layoutYProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                view.y().set(newValue.doubleValue());
            }
        });


        pane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                view.width().set(newValue.doubleValue());
            }
        });

        pane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                view.height().set(newValue.doubleValue());
            }
        });
        
        ChangeListener<Number> moveListener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            DesignerLayer layer = currentDiagram.findLayer(view.x().get(), view.y().get());
            view.layerUuid().set(layer != null ? layer.uuid().get() : null);
            
            removeViewTreeItem(view);
            addViewTreeItem(view);
        };
        
        DesignerLayer layer = currentDiagram.findLayer(view.x().get(), view.y().get());
        view.layerUuid().set(layer != null ? layer.uuid().get() : null);
        
        view.x().addListener(moveListener);
        view.y().addListener(moveListener);
        view.width().addListener(moveListener);
        view.height().addListener(moveListener);

        viewsMap.put(view, node0);
    }
    
    
    private void addDesignerData(DesignerData data, boolean needChooseImage) {
        if (data instanceof DesignerLayer) {
            currentDiagram.addLayer((DesignerLayer) data);

            showLayer((DesignerLayer) data, GraphNodeType.LAYER);
            addLayerTreeItem((DesignerLayer) data);
            
        } else if (data instanceof DesignerText) {
            currentDiagram.addText((DesignerText) data);

            showText((DesignerText) data, GraphNodeType.TEXT);
            addTextTreeItem((DesignerText) data);
            
        } else if (data instanceof DesignerImage) {
            currentDiagram.addImage((DesignerImage) data);

            showImage((DesignerImage) data, needChooseImage);
            addImageTreeItem((DesignerImage) data);
            
        } else if (data instanceof DesignerTable) {
            currentDiagram.addTable((DesignerTable) data);

            showTable((DesignerTable) data, GraphNodeType.TABLE);
            addTableTreeItem((DesignerTable) data);
            
        } else if (data instanceof DesignerView) {
            currentDiagram.addView((DesignerView) data);

            showView((DesignerView) data, GraphNodeType.VIEW);
            addViewTreeItem((DesignerView) data);
        }
    }
    
    private void processMousePressed(MouseEvent event, String layerUuid) {
        
        if (nodeForAdd != null) {
            
            // if primary - do action, else - cancel action
            if (event.getButton() == MouseButton.PRIMARY) {
                switch (nodeForAdd) {
                    
                    case LAYER:

                        DesignerLayer l = new DesignerLayer();
                        l.name().set("New Layer");
                        l.x().set(event.getX());
                        l.y().set(event.getY());
                        l.width().set(200);
                        l.height().set(150);
                        l.color().set(nodeForAdd.color);

                        addDesignerData(l, false);

                        designerDiagramHistory.putHistoryPoint("Place '" + l.name().get() + "'", currentDiagram);
                        
                        event.consume();

                        break;
                        
                    case TEXT:

                        DesignerText t = new DesignerText();
                        t.name().set(findTextName("text"));
                        t.text().set("Text");
                        t.x().set(event.getX());
                        t.y().set(event.getY());
                        t.width().set(100);
                        t.height().set(30);
                        t.color().set(nodeForAdd.color);

                        addDesignerData(t, false);
                        
                        designerDiagramHistory.putHistoryPoint("Place '" + t.name().get() + "'", currentDiagram);

                        event.consume();

                        break;
                        
                    case IMAGE:

                        DesignerImage img = new DesignerImage();
                        img.name().set(findImageName("Image"));
                        img.x().set(event.getX());
                        img.y().set(event.getY());
                        
                        addDesignerData(img, true);
                        
                        designerDiagramHistory.putHistoryPoint("Place '" + img.name().get() + "'", currentDiagram);

                        event.consume();

                        break;
                        
                    case TABLE:

                        DesignerTable table = new DesignerTable();
                        table.name().set(findTableName());
                        table.schema().set(currentDiagram.getDatabaseName());
                        table.x().set(event.getX());
                        table.y().set(event.getY());
                        table.color().set(nodeForAdd.color);

                        table.layerUuid().set(layerUuid);
                        
                        addDesignerData(table, false);
                        
                        designerDiagramHistory.putHistoryPoint("Place '" + table.name().get() + "'", currentDiagram);
                        
                        event.consume();

                        break;
                        
                    case VIEW:

                        DesignerView view = new DesignerView();
                        view.name().set(findViewName("view"));
                        view.view().set("CREATE VIEW `" + view.name().get() + "` AS");
                        view.x().set(event.getX());
                        view.y().set(event.getY());
                        view.width().set(100);
                        view.height().set(30);
                        view.color().set(nodeForAdd.color);

                        addDesignerData(view, false);
                        
                        designerDiagramHistory.putHistoryPoint("Place '" + view.name().get() + "'", currentDiagram);

                        event.consume();

                        break;

                    case REMOVE:
                        removeNode(graph.getNode(event.getX(), event.getY()));
                        break;

                    case NON_IDENTIFYING_MANY_TO_ONE:
                    case IDENTIFYING_MANY_TO_ONE:
                    case NON_IDENTIFYING_ONE_TO_ONE:
                    case IDENTIFYING_ONE_TO_ONE:
                        FXNode fxNode = graph.getNode(event.getX(), event.getY());
                        if (fxNode != null) {                        
                            DesignerTable dt = ((DesignerTableTittledPane)fxNode.getWrapedNode()).getData();

                            // table must have at least 1 column
                            if (dt.columns().isEmpty()) {
                                return;
                            }

                            if (sourceRelation == null) {
                                sourceRelation = dt;
                                return;
                            } else {

                                // destination table must be different
                                if (sourceRelation.equals(dt)) {
                                    return;
                                }

                                DesignerIndex pk = null;
                                // destination table must have primary column
                                for (DesignerIndex idx: dt.indexes()) {
                                    if (idx.isPrimaryKey()) {
                                        pk = idx;
                                        break;
                                    }
                                }                            
                                if (pk == null || pk.columns().isEmpty()) {
                                    return;
                                }

                                // if we set destination we can add realtion                            
                                // find source pk
                                DesignerIndex sourcePk = null;                            
                                if (nodeForAdd == GraphNodeType.IDENTIFYING_MANY_TO_ONE || nodeForAdd == GraphNodeType.IDENTIFYING_ONE_TO_ONE) {
                                    for (DesignerIndex idx: sourceRelation.indexes()) {
                                        if (idx.isPrimaryKey()) {
                                            sourcePk = idx;
                                            break;
                                        }
                                    }   

                                    // if not found create new
                                    if (sourcePk == null) {                                    
                                        sourcePk = new DesignerIndex();
                                        sourcePk.type().set(DesignerIndex.IndexType.PRIMARY);
                                        sourcePk.name().set("PRIMARY");

                                        sourceRelation.indexes().add(sourcePk);
                                    }
                                }
 
                                // create column (if need pk) on source table with type from pk destination table
                                // create foreign key
                                // create index
                                // fill all properties
                                addRelation(sourceRelation, sourcePk, dt, pk, nodeForAdd == GraphNodeType.IDENTIFYING_ONE_TO_ONE || nodeForAdd == GraphNodeType.NON_IDENTIFYING_ONE_TO_ONE);

                                DesignerTable forUpdateEdges = sourceRelation;
                                Platform.runLater(() -> updateEdges(forUpdateEdges)); 
                                                       

                                sourceRelation = null;
                            }

                        }
                        
                        break;
                        
                    case MANY_TO_MANY:
                        fxNode = graph.getNode(event.getX(), event.getY());
                        if (fxNode != null) {                        
                            DesignerTable dt = ((DesignerTableTittledPane)fxNode.getWrapedNode()).getData();

                            // table must have at least 1 column
                            if (dt.columns().isEmpty()) {
                                return;
                            }
                            
                            DesignerIndex foundedPk = null;
                            // table must have primary column
                            for (DesignerIndex idx: dt.indexes()) {
                                if (idx.isPrimaryKey()) {
                                    foundedPk = idx;
                                    break;
                                }
                            }    

                            if (foundedPk == null) {
                                return;
                            }
                            
                            if (sourceRelation == null) {
                                sourceRelation = dt;
                                return;
                            } else {

                                // destination table must be different
                                if (sourceRelation.equals(dt)) {
                                    return;
                                }

                                // need create new table with FKs to selected tables
                                DesignerTable manyToManyTable = new DesignerTable();
                                manyToManyTable.name().set(findTableName(sourceRelation.name().get() + "_has_" + dt.name().get()));
                                manyToManyTable.layerUuid().set(sourceRelation.layerUuid().get());
                                
                                // create columns from both tables
                                DesignerIndex sourcePk = null;
                                for (DesignerIndex idx: sourceRelation.indexes()) {
                                    if (idx.isPrimaryKey()) {
                                        sourcePk = idx;
                                        break;
                                    }
                                }
                                
                                // its imposible
                                if (sourcePk == null) {
                                    return;
                                }
                                
                                DesignerIndex primaryKey = new DesignerIndex();
                                primaryKey.name().set("PRIMARY");
                                primaryKey.type().set(DesignerIndex.IndexType.PRIMARY);
                                
                                manyToManyTable.indexes().add(primaryKey);
                                                                
                                // source relation
                                int order = addManyToManyRelation(manyToManyTable, primaryKey, sourceRelation, sourcePk, 1);
                                
                                // destination relation
                                addManyToManyRelation(manyToManyTable, primaryKey, dt, foundedPk, order);
                                
                                
                                manyToManyTable.schema().set(currentDiagram.getDatabaseName());
                                currentDiagram.addTable(manyToManyTable);
                                
                                showTable(manyToManyTable, GraphNodeType.TABLE);
                                
                                addTableTreeItem(dt);
                                
                                Platform.runLater(() -> updateEdges(manyToManyTable)); 

                                sourceRelation = null;
                            }

                        }
                        break;
                    
                    default:
                        ((MainController)getController()).showInfo("Not implemented yet", "Not implemented yet", null);
                }
            }
            
            graph.setCursor(Cursor.DEFAULT);
            
            if (lastSelectedButton != null) {
                lastSelectedButton.getStyleClass().remove(STYLE_CLASS__DESIGNER_BUTTON_SELECTED);
                lastSelectedButton = null;
            }

            nodeForAdd = null;
        }
    }
    
    private void removeNode(FXNode node) {
        if (node != null) {
            Object obj = ((DesignerPane)node.getWrapedNode()).getData();

            if (obj instanceof DesignerTable)  {

                DesignerTable dt = (DesignerTable)obj;
                designerDatabase.tables().remove(dt);

                for (Tab tab: bottomTabPane.getTabs()) {
                    if (Objects.equals(tab.getUserData(), ((DesignerPane) node.getWrapedNode()).getData())) {
                        bottomTabPane.getTabs().remove(tab);
                        break;
                    }
                }

                tablesMap.remove(dt);
                dt.foreignKeysChanged().removeListener(listenersMap.get(dt));
                listenersMap.remove(dt);

                List<FXEdge> edges = edgesMap.remove(dt);
                if (edges != null) {
                    for (FXEdge edge: edges) {
                        graph.removeEdge(edge);
                    }
                }
                
                // also need remove from others
                for (List<FXEdge> list: edgesMap.values()) {
                    
                    List<FXEdge> forRemove = new ArrayList<>();
                    for (FXEdge ed: list) {
                        if (node.equals(ed.getSource()) || node.equals(ed.getDestination())) {
                            forRemove.add(ed);
                        }
                    }
                    
                    list.removeAll(forRemove);
                }

                edges = destinationEdges.remove(dt);
                if (edges != null) {
                    for (FXEdge edge: edges) {
                        graph.removeEdge(edge);
                    }
                }
                
                // also need remove from others
                for (List<FXEdge> list: destinationEdges.values()) {
                    
                    List<FXEdge> forRemove = new ArrayList<>();
                    for (FXEdge ed: list) {
                        if (node.equals(ed.getSource()) || node.equals(ed.getDestination())) {
                            forRemove.add(ed);
                        }
                    }
                    
                    list.removeAll(forRemove);
                }

                currentDiagram.tables().remove(dt);
                removeTableTreeItem(dt);
                
                designerDiagramHistory.putHistoryPoint("Remove '" + dt.name().get() + "'", currentDiagram);

            } else if (obj instanceof DesignerLayer) {
                DesignerLayer layer = (DesignerLayer)obj;

                for (Tab tab: bottomTabPane.getTabs()) {
                    if (Objects.equals(tab.getUserData(), ((DesignerPane) node.getWrapedNode()).getData())) {
                        bottomTabPane.getTabs().remove(tab);
                        break;
                    }
                }

                layersMap.remove(layer);


                currentDiagram.layers().remove(layer);
                removeLayerTreeItem(layer);
                
                designerDiagramHistory.putHistoryPoint("Remove '" + layer.name().get() + "'", currentDiagram);

            } else if (obj instanceof DesignerText) {
                DesignerText text = (DesignerText)obj;

                for (Tab tab: bottomTabPane.getTabs()) {
                    if (Objects.equals(tab.getUserData(), ((DesignerPane) node.getWrapedNode()).getData())) {
                        bottomTabPane.getTabs().remove(tab);
                        break;
                    }
                }

                textsMap.remove(text);


                currentDiagram.texts().remove(text);
                removeTextTreeItem(text);
                
                designerDiagramHistory.putHistoryPoint("Remove '" + text.name().get() + "'", currentDiagram);

            } else if (obj instanceof DesignerImage) {
                DesignerImage image = (DesignerImage)obj;

                for (Tab tab: bottomTabPane.getTabs()) {
                    if (Objects.equals(tab.getUserData(), ((DesignerPane) node.getWrapedNode()).getData())) {
                        bottomTabPane.getTabs().remove(tab);
                        break;
                    }
                }

                imagesMap.remove(image);


                currentDiagram.images().remove(image);
                removeImageTreeItem(image);
                
                designerDiagramHistory.putHistoryPoint("Remove '" + image.name().get() + "'", currentDiagram);
                
            } else if (obj instanceof DesignerView) {
                DesignerView v = (DesignerView)obj;

                for (Tab tab: bottomTabPane.getTabs()) {
                    if (Objects.equals(tab.getUserData(), ((DesignerPane) node.getWrapedNode()).getData())) {
                        bottomTabPane.getTabs().remove(tab);
                        break;
                    }
                }

                viewsMap.remove(v);


                currentDiagram.views().remove(v);
                removeViewTreeItem(v);
                
                designerDiagramHistory.putHistoryPoint("Remove '" + v.name().get() + "'", currentDiagram);
            }           
            
            graph.removeNode(node);
        }
    }
    
    private int addManyToManyRelation(DesignerTable manyToManyTable, DesignerIndex primaryKey, DesignerTable table, DesignerIndex tablePrimaryKey, int order) {
        DesignerForeignKey fk1 = new DesignerForeignKey();
        fk1.name().set(findForeignKeyName("fk_" + manyToManyTable.name().get() + "_" + table.name().get(), manyToManyTable.foreignKeys()));
        fk1.referencedTable().set(table);

        manyToManyTable.foreignKeys().add(fk1);


        DesignerIndex fkIndex1 = new DesignerIndex();
        fkIndex1.name().set(findIndexName("fk_" + manyToManyTable.name().get() + "_" + table.name().get(), manyToManyTable.indexes()));

        manyToManyTable.indexes().add(fkIndex1);

        for (DesignerColumn dc: tablePrimaryKey.columns()) {
            DesignerColumn dc0 = new DesignerColumn();
            dc0.name().set(table.name().get() + "_" + dc.name().get());
            dc0.dataType().set(dc.dataType().get());
            dc0.primaryKey().set(true);
            dc0.notNull().set(true);

            manyToManyTable.columns().add(dc0);

            primaryKey.columns().add(dc0);
            primaryKey.sortMap().put(dc0, DesignerIndex.IndexOrder.ASC);
            primaryKey.orderMap().put(dc0, order);
            order++;

            fk1.columns().add(dc0);
            fk1.referencedColumns().put(dc0, dc);

            fkIndex1.addColumn(dc0);
        }
        
        return order;
    }
    
    private void addRelation(DesignerTable from, DesignerIndex fromPk, DesignerTable to, DesignerIndex toPk, boolean unique) {
        DesignerForeignKey fk = new DesignerForeignKey();
        fk.name().set(findForeignKeyName("fk_" + from.name().get() + "_" + to.name().get(), from.foreignKeys()));
        fk.referencedTable().set(to);

        DesignerIndex fkIndex = new DesignerIndex();
        fkIndex.name().set(findIndexName("fk_" + from.name().get() + "_" + to.name().get(), from.indexes()));
        
        DesignerIndex uqIndex = new DesignerIndex();
        uqIndex.type().set(DesignerIndex.IndexType.UNIQUE);
        uqIndex.name().set(findIndexName("uq_" + from.name().get() + "_", from.indexes()));

        int order = 1;
        for (DesignerColumn dc: toPk.columns()) {
            DesignerColumn c = new DesignerColumn();
            c.name().set(findColumnName(to.name().get() + "_" + dc.name().get(), from.columns()));
            c.dataType().set(dc.dataType().get());
            c.notNull().set(true);
            c.unique().set(unique);

            fk.columns().add(c);
            fk.referencedColumns().put(c, dc);

            fkIndex.columns().add(c);
            fkIndex.orderMap().put(c, order);
            fkIndex.sortMap().put(c, DesignerIndex.IndexOrder.ASC);
            
            if (unique) {
                uqIndex.columns().add(c);
                uqIndex.orderMap().put(c, order);
                uqIndex.sortMap().put(c, DesignerIndex.IndexOrder.ASC);
            }

            order++;

            // if need try add columns to exist
            if (fromPk != null) {
                c.primaryKey().set(true);

                int nextIndex = fromPk.columns().size() + 1;
                fromPk.columns().add(c);
                fromPk.orderMap().put(c, nextIndex);
                fromPk.sortMap().put(c, DesignerIndex.IndexOrder.ASC);                                    
            }

            from.columns().add(c);
        }

        from.foreignKeys().add(fk);
        from.indexes().add(fkIndex);
        
        if (unique) {
            from.indexes().add(uqIndex);
        }
    }
    
    public void importTable(DesignerTable dt, double x, double y) {
        // TODO Change settings of schema
        dt.schema().set(currentDiagram.getDatabaseName());
        dt.x().set(x);
        dt.y().set(y);
        
        calcTableSize(dt);
        
        currentDiagram.addTable(dt);

        showTable(dt, GraphNodeType.TABLE);

        addTableTreeItem(dt);
    }
    
    public DesignerErrDiagram getCurrentDiagram() {
        return currentDiagram;
    }
    
    private static final int POSITIONING_COLUMNS    = 3;
    private static final int POSITIONING_ROWS       = 2;
    private static final int DEFAULT_SIZE           = 300;
    
    public void importTables(List<DesignerTable> designerTables) {
        
        // calc table relations count
        for (DesignerTable dt: designerTables) {
            
            int relationCount = dt.foreignKeys().size();
            
            for (DesignerTable dt0: designerTables) {
                if (!dt0.equals(dt)) {
                    for (DesignerForeignKey dfk: dt0.foreignKeys()) {
                        if (dt.equals(dfk.referencedTable().get())) {
                            relationCount++;
                        }
                    }
                }
            }
            
            dt.setRelationCount(relationCount);
        }
        
        final int centerTablesCount = POSITIONING_COLUMNS * POSITIONING_ROWS;
        
        // calc tables area        
        
        int step = 0;
        if (designerTables.size() > centerTablesCount) {
            
            step++;
            // find step
            while (true) {
                if ((POSITIONING_COLUMNS + 2 * step) * (POSITIONING_ROWS + 2 * step) < designerTables.size()) {
                    step++;
                } else {
                    break;
                }
            }
        }
                
        
        // sort table by fk counts
        designerTables.sort((DesignerTable o1, DesignerTable o2) -> o2.getRelationCount() - o1.getRelationCount());
        
        int startX = step * DEFAULT_SIZE + 100;
        int startY = step * DEFAULT_SIZE + 100;
        
        int defaultX = startX;
        int defaultY = startY;
        
        int i = 0;
        int column = 0;
        
        int size = POSITIONING_COLUMNS * POSITIONING_ROWS;
        if (size > designerTables.size()) {
            size = designerTables.size();
        }
        
        for (i = 0; i < size; i++) {
            DesignerTable dt = designerTables.get(i);
            
            importTable(dt, startX, startY);
            
            if (column >= POSITIONING_COLUMNS - 1) {
                column = 0;
                
                startX = defaultX;
                startY += DEFAULT_SIZE;
            } else {
                column++;
                startX += DEFAULT_SIZE;
            }
        }
        
        
        
        column = 0;
        int row = 0;
        
        startX = defaultX - DEFAULT_SIZE;
        defaultX = startX;
        
        startY = defaultY - DEFAULT_SIZE;
        defaultY = startY;
        
        int columnCount = POSITIONING_COLUMNS + 2;
        int rowCount = POSITIONING_ROWS + 2;
        
        for (int j = i; j < designerTables.size(); j++) {
            DesignerTable dt = designerTables.get(j);
            
            importTable(dt, startX, startY);
            
            if (column >= columnCount - 1) {
                
                if (startX == defaultX) {
                    startY -= DEFAULT_SIZE;
                } else {
                    if (row >= rowCount - 1) {
                        startX -= DEFAULT_SIZE;
                    } else {
                        startY += DEFAULT_SIZE;
                        row++;
                    }
                }
                
                // clear all
                if (startY == defaultY) {
                    column = 0;
                    row = 0;
                    
                    columnCount += 2;
                    rowCount += 2;
                    
                    startX = defaultX - DEFAULT_SIZE;
                    defaultX = startX;
                    
                    startY = defaultY - DEFAULT_SIZE;
                    defaultY = startY;
                }
            } else {
                startX += DEFAULT_SIZE;
                column++;
            }
        }
        
        for (DesignerTable dt: designerTables) {
            updateEdges(dt);
        }
    }
    
    
    private String findForeignKeyName(String name, List<DesignerForeignKey> fks) {
        
        int index = 1;
        
        if (fks != null && !fks.isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerForeignKey fk: fks) {
                    if (fk.name().get() != null && fk.name().get().equals(name + index)) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + index;
    }
    
    private String findIndexName(String name, List<DesignerIndex> indexes) {
        
        int index = 1;
        
        if (indexes != null && !indexes.isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerIndex indx: indexes) {
                    if (indx.name().get() != null && indx.name().get().equals(name + index + "_idx")) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + index + "_idx";
    }
    
    private String findColumnName(String name, List<DesignerColumn> columns) {
        
        int index = 1;
        
        if (columns != null && !columns.isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerColumn c: columns) {
                    if (c.name().get() != null && c.name().get().equals(name + "_" + index)) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + "_" + index;
    }
    
    private String findTableName() {
        
        int index = 1;
        
        if (currentDiagram.tables() != null && !currentDiagram.tables().isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerTable t: currentDiagram.tables()) {
                    if (t.name().get() != null && t.name().get().equals("table" + index )) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return "table" + index;
    }
    
    private String findTableName(String name) {
        
        int index = 0;
        
        if (currentDiagram.tables() != null && !currentDiagram.tables().isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerTable t: currentDiagram.tables()) {
                    if (t.name().get() != null && t.name().get().equals(name + (index == 0 ? "" : index) )) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + (index == 0 ? "" : index);
    }
    
    private String findTextName(String name) {
        
        int index = 0;
        
        if (currentDiagram.texts() != null && !currentDiagram.texts().isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerText t: currentDiagram.texts()) {
                    if (t.name().get() != null && t.name().get().equals(name + (index == 0 ? "" : index) )) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + (index == 0 ? "" : index);
    }
    
    private String findImageName(String name) {
        
        int index = 0;
        
        if (currentDiagram.images()!= null && !currentDiagram.images().isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerImage t: currentDiagram.images()) {
                    if (t.name().get() != null && t.name().get().equals(name + (index == 0 ? "" : index) )) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + (index == 0 ? "" : index);
    }
    
    private String findViewName(String name) {
        
        int index = 0;
        
        if (currentDiagram.views()!= null && !currentDiagram.views().isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (DesignerView t: currentDiagram.views()) {
                    if (t.name().get() != null && t.name().get().equals(name + (index == 0 ? "" : index) )) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return name + (index == 0 ? "" : index);
    }
    
    private void registrateAddButton(Button button, GraphNodeType nodeType, KeyCombination keyCombination) {
        button.getStyleClass().add(STYLE_CLASS__DESIGNER_BUTTON);
        button.addEventHandler(MouseEvent.ANY, (MouseEvent event) -> {
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                if (!button.getStyleClass().contains(STYLE_CLASS__DESIGNER_BUTTON_HOVER)) {
                    button.getStyleClass().add(STYLE_CLASS__DESIGNER_BUTTON_HOVER);
                }
            } else if (event.getEventType() == MouseEvent.MOUSE_EXITED) {
                button.getStyleClass().remove(STYLE_CLASS__DESIGNER_BUTTON_HOVER);
            }
        });
        
        button.setOnAction((ActionEvent event) -> {
            if (lastSelectedButton != null) {
                lastSelectedButton.getStyleClass().remove(STYLE_CLASS__DESIGNER_BUTTON_SELECTED);
            }

            if (!button.getStyleClass().contains(STYLE_CLASS__DESIGNER_BUTTON_SELECTED)) {
                button.getStyleClass().add(STYLE_CLASS__DESIGNER_BUTTON_SELECTED);
            }
            
            graph.setCursor(new ImageCursor(nodeType.cursor));

            nodeForAdd = nodeType;
            lastSelectedButton = button;
            
            sourceRelation = null;
        });
        
        getStage().getScene().addMnemonic(new Mnemonic(button, keyCombination));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        currentFont = FontUtils.parseFXFont(preferenceService.getPreference().getFontsObjectBrowser());
        
        diagramTypeCombobox.getItems().addAll(DIAGRAM_TYPE__LOCAL, DIAGRAM_TYPE__PUBLIC, DIAGRAM_TYPE__ALL);
        diagramTypeCombobox.getSelectionModel().select(DIAGRAM_TYPE__LOCAL);
        
        diagramTypeCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null) {
        
                SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
                switch (newValue) {
                    
                    case DIAGRAM_TYPE__LOCAL:
                        module.showLocalDiagrams(true);
                        break;
                        
                    case DIAGRAM_TYPE__PUBLIC:
                        module.showPublicDiagrams();
                        break;
                        
                    case DIAGRAM_TYPE__ALL:
                        module.showAllDiagrams();
                        break;
                }
            }
        });
        
        catalogRootItem = new TreeItem("");
        tablesItem = new TreeItem("Tables");
        viewsItem = new TreeItem("Views");
        
        catalogRootItem.getChildren().addAll(tablesItem, viewsItem);
        
        catalogTree.setRoot(catalogRootItem);
        catalogRootItem.setExpanded(true);
        
        catalogTree.setCellFactory(new Callback<TreeView<Object>, TreeCell<Object>>() {
            @Override
            public TreeCell<Object> call(TreeView<Object> p) {
                TreeCell tc = new TreeCell<Object>() {
                    @Override
                    protected void updateItem(Object item, boolean paramBoolean) {
                        super.updateItem(item, paramBoolean);
                        setText("");
                        setGraphic(null);
                        if (!isEmpty()) {
                            
                            if (item instanceof TreeItemDesigner) {
                                
                                TreeItemDesigner ti = (TreeItemDesigner)item;
                                
                                setText(ti.toString());
                                setGraphic(new ImageView(new Image(ti.getImage(), 16, 16, false, false)));
                                
                                this.setOnMouseClicked((MouseEvent event) -> {
                                    if (event.getClickCount() == 2) {
                                        ti.showTab();
                                    }
                                });
                            } else {
                                String text = item != null ? item.toString() : "";
                                setText(text);
                                
                                
                                if (getTreeItem() == catalogRootItem) {
                                    setGraphic(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/database.png", 16, 16, false, false)));
                                } else {
                                    setGraphic(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/folder.png", 16, 16, false, false)));
                                }
                            }
                        }
                        
                        if (currentFont != null) {
                            setFont(currentFont);
                        }
                    }
                };
                
                tc.setOnDragDetected((MouseEvent event) -> {
                    if (tc.getItem() instanceof TreeItemDesignerTable && ((TreeItemDesignerTable)tc.getItem()).disabled) {
                        
                        Dragboard dragBoard = tc.startDragAndDrop(TransferMode.MOVE);
                        ClipboardContent content = new ClipboardContent();

                        DesignerTable table = ((TreeItemDesignerTable)tc.getItem()).getData();
                        content.put(DataFormat.PLAIN_TEXT, table.uuid().get());                         

                        dragBoard.setContent(content);
                    }
                    event.consume();
                });

                tc.setOnDragOver((DragEvent event) -> {
                    Dragboard db = event.getDragboard();
                    if (db.hasString()) {
                        event.acceptTransferModes(TransferMode.MOVE);
                    }
                    event.consume(); 
                });
                
                return tc;
            }
        });
        
        layersTree.setCellFactory(new Callback<TreeView<Object>, TreeCell<Object>>() {
            @Override
            public TreeCell<Object> call(TreeView<Object> p) {
                return new TreeCell<Object>() {
                    @Override
                    protected void updateItem(Object item, boolean paramBoolean) {
                        super.updateItem(item, paramBoolean);
                        setText("");
                        setGraphic(null);
                        if (!isEmpty()) {
                            
                            if (item instanceof TreeItemDesigner) {
                                
                                TreeItemDesigner ti = (TreeItemDesigner)item;
                                
                                setText(ti.toString());
                                setGraphic(new ImageView(new Image(ti.getImage(), 16, 16, false, false)));
                                
                                this.setOnMouseClicked((MouseEvent event) -> {
                                    if (event.getClickCount() == 2) {
                                        ti.showTab();
                                    }
                                });
                            }
                        }
                        
                        if (currentFont != null) {
                            setFont(currentFont);
                        }
                    }
                };
            }
        });
        
        
        layersRootItem = new TreeItem();
        layersTree.setRoot(layersRootItem);
        layersTree.setShowRoot(false);
        layersRootItem.setExpanded(true);
        
        
        centerSplit.setDisable(true);
        
        this.designerDatabase = new DesignerDatabase();
             
        descriptionTextArea.setDisable(true);
        descriptionTextArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (graph.getSelectedNode() != null) {
                ((DesignerPane)graph.getSelectedNode().getWrapedNode()).getData().description().set(newValue != null ? newValue : "");
            }
        });
        
        graphBuilder = FXGraphBuilder.create();
        graph = graphBuilder.build();
        graph.setBirdsEyeCanvas(birdsEye);
        
        graph.movedProperty().addListener((ObservableValue<? extends FXNode> observable, FXNode oldValue, FXNode newValue) -> {
            if (newValue != null) {
                designerDiagramHistory.putHistoryPoint("Move '" + ((DesignerPane)newValue.getWrapedNode()).getData().name().get() + "'", currentDiagram);
            }
        });
        
        graph.resizedProperty().addListener((ObservableValue<? extends FXNode> observable, FXNode oldValue, FXNode newValue) -> {
            if (newValue != null) {
                designerDiagramHistory.putHistoryPoint("Resize '" + ((DesignerPane)newValue.getWrapedNode()).getData().name().get() + "'", currentDiagram);
            }
        });
        
        graph.selectedNodeProperty().addListener((ObservableValue<? extends FXNode> observable, FXNode oldValue, FXNode newValue) -> {
            descriptionTextArea.setText(newValue != null ? ((DesignerPane)newValue.getWrapedNode()).getData().description().get() : "");
            descriptionTextArea.setDisable(newValue == null);
            
            List<DesignerProperty> properties = new ArrayList<>();
            if (newValue != null) {
                properties.addAll(((DesignerPane)newValue.getWrapedNode()).getData().getProperties());
            }
            
            propertiesTable.getItems().clear();
            propertiesTable.getItems().addAll(properties);
        });
         
        graph.getStyleClass().add(STYLE_CLASS__DESIGNER_GRAPH);
        
        graph.getContentPane().addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent event) -> {
            
            FXNode n = graph.getNode(event.getX(), event.getY());
            if (n != null && n.getWrapedNode() instanceof DesignerLayerTittledPane) {
                processMousePressed(event, ((DesignerLayerTittledPane)n.getWrapedNode()).getData().uuid().get());
            } else {
                processMousePressed(event, "");
            }
            
            if (event.isPopupTrigger() || event.isSecondaryButtonDown()) {
                contexMenuX = event.getX();
                contexMenuY = event.getY();
                showContextMenu(n, event.getSceneX(), event.getSceneY());
            }
        });
        
        graph.getContentPane().setOnDragEntered((DragEvent event) -> {
        });

        graph.getContentPane().setOnDragExited((DragEvent event) -> {
        });

        graph.getContentPane().setOnDragOver((DragEvent event) -> {
            
            Dragboard db = event.getDragboard();
            if (db.hasString()) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });

        graph.getContentPane().setOnDragDropped((DragEvent event) -> {
            
            Dragboard db = event.getDragboard();
            if (db.hasString()) {
                String tableUuid = db.getString();
                
                DesignerTable foundedTable = null;
                for (TreeItem ti: (List<TreeItem>)tablesItem.getChildren()) {
                    
                    DesignerTable table = ((TreeItemDesignerTable)ti.getValue()).getData();
                    if (table.uuid().get().equals(tableUuid)) {
                        
                        foundedTable = table;
                        break;
                    }
                }
                
                if (foundedTable != null) {
                    importTable(foundedTable, event.getX(), event.getY());                    
                    
                    foundedTable.columns().stream().forEach(c -> c.backgroundColor().set(DesignerColumn.DEFAULT_BACKGROUND));
                    
                    updateEdges(foundedTable);
                }
            }
            
            event.setDropCompleted(true);
            event.consume();
        });
        
        centerPane.getChildren().add(graph);
        
        AnchorPane.setTopAnchor(graph, 0d);
        AnchorPane.setLeftAnchor(graph, 32d);
        AnchorPane.setBottomAnchor(graph, 0d);
        AnchorPane.setRightAnchor(graph, 0d);
        
        setBottomPaneVisibility(false);
        
        bottomTabPane.getTabs().addListener((ListChangeListener.Change<? extends Tab> c) -> {
            setBottomPaneVisibility(bottomTabPane.getTabs().size() > 0);
        });
        
        bottomTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
        subInitialiser();
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        
        savedDiagramsListview.setCellFactory(new Callback<ListView<ErrDiagram>, ListCell<ErrDiagram>>() {
            @Override 
            public ListCell<ErrDiagram> call(ListView<ErrDiagram> list) {
                return new DiagramCell();
            }
        });
        
        savedDiagramsListview.setOnKeyPressed((KeyEvent event) -> {
            ErrDiagram selectedDiagram = savedDiagramsListview.getSelectionModel().getSelectedItem();
            if (event.getCode() == KeyCode.DELETE && selectedDiagram != null) {
                module.deleteErrDiagram(selectedDiagram);  
                event.consume();
            }
        });
        
        
        MenuItem deleteDiagramItem = new MenuItem("Delete diagram", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/delete.png", 16, 16, true, false)));
        deleteDiagramItem.setOnAction((ActionEvent event) -> {
            module.deleteErrDiagram(savedDiagramsListview.getSelectionModel().getSelectedItem());  
        });
        ContextMenu cm = new ContextMenu(deleteDiagramItem);
        
        savedDiagramsListview.setItems(module.errDiagrams());
        
        savedDiagramsListview.setContextMenu(cm);
        
        rootSplitPane.getItems().remove(templateRightPane);
        
        zoomComboBox.getItems().addAll(new String[]{"200%", "150%", "100%", "95%", "90%", "85%", "80%", "75%", "70%", "60%", "50%", "40%", "30%", "20%", "10%"});
        zoomComboBox.getSelectionModel().select("100%");
        
        zoomComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null) {
                try {
                    int v = Integer.parseInt(newValue.substring(0, newValue.length() - 1));
                    graph.zoom(v);
                } catch (Throwable th) {}
            }
        });
        
        
        zoomIn.setOnMouseClicked((MouseEvent event) -> {
            zoomIn();
        });
        
        zoomOut.setOnMouseClicked((MouseEvent event) -> {
            zoomOut();
        });
        
        updateBirdsEye();
        
        
        propertiesNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        propertiesValueColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerProperty, Object> p) -> ((DesignerProperty)p.getValue()).observable());
        propertiesValueColumn.setCellFactory(new Callback<TableColumn<DesignerProperty, Object>, TableCell<DesignerProperty, Object>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new DesignerPropertyTableCell();
            }
        });
        
        
        historyListView.setCellFactory(listView -> new ListCell<DesignerDiagramHistory.HistoryPoint>() {
            @Override
            public void updateItem(DesignerDiagramHistory.HistoryPoint item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                    setGraphic(null);   
                } else {
                    setText(item.toString());
                    setId(item.isEnabled() ? "" : "disabledHistoryPoint");
                    
                    setGraphic(new ImageView("/np/com/ngopal/smart/sql/ui/images/designer/historyPoint.png"));
                }
            }
        });
        
        historyListView.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                
                designerDiagramHistory.selectHistoryPoint(historyListView.getSelectionModel().getSelectedItem());
                
                // refresh listview
                refreshHistory();
            }
        });
    }
    
    
    private void refreshHistory() {        
        historyListView.setItems(null);
        historyListView.setItems(designerDiagramHistory.getHistoryPoints());
    }
    
    private void showContextMenu(FXNode node, double x, double y) {
        
        prepareMenuItems(node != null ? node.getWrapedNode() : null);
        
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(cutItem, copyItem, pasteItem, new SeparatorMenuItem(), editItem, new SeparatorMenuItem(), deleteItem);
        
        menu.show(getStage(), x, y);
    }
    
    private void prepareMenuItems(Object node) {
        
        String cutStr = "Cut %s";
        String copyStr = "Copy %s";
        String pasteStr = "Paste %s";
        String editStr = "Edit %s";
        String deleteStr = "Delete %s";
        
        String objectStr = "";
        if (node instanceof DesignerImagePane) {
            objectStr = "'Image'";
            
        } else if (node instanceof DesignerLayerTittledPane) {
            objectStr = "'Layer'";
            
        } else if (node instanceof DesignerTableTittledPane) {
            objectStr = "'Table'";
            
        } else if (node instanceof DesignerTextPane) {
            objectStr = "'Text'";
            
        } else if (node instanceof DesignerViewPane) {
            objectStr = "'View'";            
        }
        
        
        cutItem.setText(String.format(cutStr, objectStr));
        cutItem.setDisable(node == null);
        
        copyItem.setText(String.format(copyStr, objectStr));
        copyItem.setDisable(node == null);
        
        pasteItem.setText(String.format(pasteStr, objectStr)); 
        pasteItem.setDisable(copiedData == null);
        
        editItem.setText(String.format(editStr, objectStr));
        editItem.setDisable(node == null);
                
        deleteItem.setText(String.format(deleteStr, objectStr));
        deleteItem.setDisable(node == null);
    }
    
    private void updateBirdsEye() {
        Platform.runLater(() -> {
            log.debug("Update birds eye");
            graph.updateBirdsEye();
        });
    }
    
    class DesignerPropertyTableCell extends TableCell<DesignerProperty, Object> {
        
        private Node editingNode;
        
        @Override
        public void startEdit() {
            super.startEdit();
            
            // always recreate - node depened on value type
            editingNode = createNode();

            setGraphic(editingNode);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        }
        
        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(getStringValue());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
        
        public String getStringValue() {
            if (getTableView().getItems() != null && getTableView().getItems().size() > getIndex()) {
                DesignerProperty dp = getTableView().getItems().get(getIndex());
                if (dp != null) {
                    return dp.observable().getValue() != null ? dp.observable().getValue().toString() : "";
                }
            }
            
            return "";
        }
    
        
        @Override
        protected void updateItem(Object item, boolean empty) {
            super.updateItem(item, empty);
            
            if (getTableRow() != null && getIndex() >= 0 && getTableView().getItems() != null && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (isEditing()) {
                    if (editingNode != null) {
                        setValue(item);
                    }

                    setGraphic(editingNode);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(item != null ? item.toString() : "");
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }              
            } else {
                setText("");
                setGraphic(null);
            }
        }
        
        private void setValue(Object item) {
            if (editingNode instanceof TextField) {
                ((TextField)editingNode).setText(item != null ? item.toString() : "");
            } else {
                // TODO
            }
        }
        
        private Node createNode() {
            if (getTableView().getItems() != null && getTableView().getItems().size() > getIndex()) {
                
                DesignerProperty dp = getTableView().getItems().get(getIndex());
                Object value = dp.observable().getValue();
                
                if (dp.getType() == DesignerProperty.PropertyType.STRING || dp.getType() == DesignerProperty.PropertyType.DOUBLE) {
                    TextField tf = new TextField();
                    tf.setText(getStringValue());

                    tf.setOnKeyPressed((KeyEvent t) -> {

                        if (t.getCode() != null) {                            
                            switch (t.getCode()) {

                                case ENTER:                            
                                    try {
                                        Object v = dp.convertValue(tf.getText());
                                        commitEdit(v);
                                    } catch (Throwable th) {
                                        cancelEdit();
                                    }
                                    break;

                                case ESCAPE:
                                    cancelEdit();
                                    break;
                            }
                        }
                    });


                    Platform.runLater(() -> {
                        tf.requestFocus();
                        tf.selectAll();
                    });
                    return tf;
                } else if (dp.getType() == DesignerProperty.PropertyType.BOOLEAN){
                    
                    ComboBox<String> cb = new ComboBox<>();
                    cb.setMaxWidth(Double.MAX_VALUE);
                    cb.getItems().addAll("True", "False");
                    
                    cb.getSelectionModel().select((Boolean) value ? 0 : 1);
                    cb.setOnKeyPressed((KeyEvent t) -> {

                        if (t.getCode() != null) {                            
                            switch (t.getCode()) {

                                case ENTER:                            
                                    try {
                                        Object v = dp.convertValue(cb.getSelectionModel().getSelectedItem());
                                        commitEdit(v);
                                    } catch (Throwable th) {
                                        cancelEdit();
                                    }
                                    break;

                                case ESCAPE:
                                    cancelEdit();
                                    break;
                            }
                        }
                    });
                    
                    return cb;
                }
            }
            
            return null;
        }
        
        
        @Override
        public void commitEdit(Object newValue) {
            super.commitEdit(newValue);
            
            DesignerProperty dp = getTableView().getItems().get(getIndex());
            designerDiagramHistory.putHistoryPoint("Change '" + ((DesignerPane)graph.getSelectedNode().getWrapedNode()).getData().name().get() + "' " + dp.getName(), currentDiagram);
        }
        
    }
    
    
    class DiagramCell extends ListCell<ErrDiagram> {
        @Override
        public void updateItem(ErrDiagram item, boolean empty) {
            super.updateItem(item, empty);
            
            if (item != null) {
                setGraphic(new ImageView(item.getPublicId() != null ? SCHEMA_PUBLIC : SCHEMA_PRIVATE));
                setText(item.getName());
                
                setOnMouseClicked((MouseEvent event) -> {
                    if (event.getClickCount() == 2) {
                        
                        savedDiagramsListview.getSelectionModel().clearSelection();
                        
                        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
                        
                        if (module.getSelectedDiagram().get() != null && module.getSelectedDiagram().get().changed().get()) {
                            DialogResponce responce = ((MainController)getController()).showConfirm("Save diagram", "Do you want save current Diagram?");
                            if (DialogResponce.OK_YES == responce) {
                                module.saveDiagram();
                            }
                        }
                        
                        module.openDiagram(item);
                    }
                });                
            } else {
                setGraphic(null);
                setText("");
            }
        }
    }
    
    public byte[] getDiagramImage() throws IOException {
        return graph.getImage();
    }
    
    public void setBottomPaneVisibility(boolean visble) {
        boolean contains = centerSplit.getItems().contains(bottomContainer);
        if (visble) {
            if (!contains) {
                centerSplit.getItems().add(bottomContainer);
                centerSplit.setDividerPosition(0, 1.0);
            }
        } else {
            if (contains) {
                centerSplit.getItems().remove(bottomContainer);
            }
        }
    }
    
    
    public void updateEdges(DesignerTable table) {
        if (edgesMap.containsKey(table)) {
            for (FXEdge edge: edgesMap.get(table)) {
                graph.removeEdge(edge);
                
                for (List<FXEdge> edges: destinationEdges.values()) {
                    edges.remove(edge);
                    int index = 0;
                    for (FXEdge ed: edges) {
                        ed.setDestinationIndex(index);
                        index++;
                    }
                }
            }
        }
        
        
        addEdges(table);
    }
         
    private void addEdges(DesignerTable table) {

        edgesMap.remove(table);
        
        int sourceIndex = 0;
        for (DesignerForeignKey fk: table.foreignKeys()) {
            
            DesignerTable destinationtable = fk.referencedTable().get();
            if (destinationtable != null && !table.equals(destinationtable)) {
                
                // check for Indetifying and Non Identifying relation
                // if all columns is PK - the true
                // if empty - false
                boolean identifying = !fk.columns().isEmpty();
                for (DesignerColumn dc: fk.columns()) {
                    if (!dc.primaryKey().get()) {
                        identifying = false;
                        break;
                    }
                }
                
                // calc edge type
                // 1. Try find other table that have fk to the same table on other part of PK, then its N:M
                // 2. if fk columns unique and have unique index - its 1:1
                // 3. if not its 1:N
                
                // 1. So we must check if FK referenced only on the part of PK
                
                List<DesignerColumn> otherPartOfPK = new ArrayList<>();
                for (DesignerIndex di: destinationtable.indexes()) {
                    if (di.isPrimaryKey()) {
                        for (DesignerColumn dc: di.columns()) {
                            if (!fk.referencedColumns().values().contains(dc)) {
                                otherPartOfPK.add(dc);
                            }
                        }
                    }
                }
                
                // default value
                FXEdge.EdgeType edgeType = null;
                
                // if not empty we can try find second table of N:M relation
                if (!otherPartOfPK.isEmpty()) {
                    
                    for (DesignerTable dt: designerDatabase.tables()) {
                        if (!dt.equals(table)) {
                            
                            for (DesignerForeignKey fk0: dt.foreignKeys()) {            
                                DesignerTable dt0 = fk0.referencedTable().get();
                                // if refer on the destination table and columns size == otherPartOfPK size
                                if (dt0 != null && dt0.equals(destinationtable) && dt0.columns().size() == otherPartOfPK.size()) {
                                    boolean founded = true;
                                    for (DesignerColumn dc0: dt0.columns()) {
                                        if (!otherPartOfPK.contains(dc0)) {
                                            founded = false;
                                        }
                                    }
                                    
                                    if (founded) {
                                        edgeType = FXEdge.EdgeType.MANY_TO_MANY;
                                        break;
                                    }
                                }
                            }
                            
                            if (edgeType != null) {
                                break;
                            }
                        }
                    }
                }
                
                if (edgeType == null) {
                    // 2. Check fk columns unique and uniqu indexes
                    boolean founded = true;
                    for (DesignerColumn dc: fk.columns()) {
                        if (!dc.unique().get()) {
                            founded = false;
                            break;
                        }
                    }

                    if (founded) {
                        for (DesignerIndex di: table.indexes()) {
                            if (di.isUniqueKey() && di.columns().size() == fk.columns().size()) {
                                founded = true;
                                for (DesignerColumn dc: di.columns()) {
                                    if (!fk.columns().contains(dc)) {
                                        founded = false;
                                        break;
                                    }
                                }
                                
                                if (founded) {
                                    edgeType = identifying ? FXEdge.EdgeType.IDENTINFYING__ONE_TO_ONE : FXEdge.EdgeType.NON_IDENTINFYING__ONE_TO_ONE;
                                    break;
                                }
                            }
                        }
                    }
                }
                
                // 3. if null - N:M
                if (edgeType == null) {
                    edgeType = identifying ? FXEdge.EdgeType.IDENTINFYING__MANY_TO_ONE : FXEdge.EdgeType.NON_IDENTINFYING__MANY_TO_ONE;
                }
                
                
                FXEdge edge = new FXEdge(graph, fk, tablesMap.get(table), table.foreignKeys().size() + findCountRefForeignKeys(table), sourceIndex, tablesMap.get(destinationtable), findCountRefForeignKeys(destinationtable), destinationtable.foreignKeys().size(), findDestinationEdgeIndex(destinationEdges.get(destinationtable)), edgeType);
                graph.addEdge(edge);
                sourceIndex++;
                List<FXEdge> list = destinationEdges.get(destinationtable);
                if (list == null) {
                    list = new ArrayList<>();
                    destinationEdges.put(destinationtable, list);
                }
                
                list.add(edge);

                List<FXEdge> edges = edgesMap.get(table);
                if (edges == null) {
                    edges = new ArrayList<>();
                    edgesMap.put(table, edges);
                }

                edges.add(edge);
            }
        }
    }
    
    private int findCountRefForeignKeys(DesignerTable refTable) {
        int count = 0;
        if (refTable != null) {
            for (DesignerTable t: tablesMap.keySet()) {
                if (!t.equals(refTable)) {
                    for (DesignerForeignKey fk: t.foreignKeys()) {
                        if (refTable.equals(fk.referencedTable().get())) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }
    
    
    private int findDestinationEdgeIndex(List<FXEdge> edges) {
        
        int index = 0;
        
        if (edges != null && !edges.isEmpty()) {
            boolean notFound = true;
            while (notFound) {
                notFound = false;
                for (FXEdge edg: edges) {
                    if (edg.getDestinationIndex() == index) {
                        notFound = true;
                        index++;
                    }
                }
            }
        }
        
        return index;
    }
    
    @Override
    public void openConnection(ConnectionParams params) throws SQLException {
        
        if (params != null && 
                params.getAddress() != null && !params.getAddress().isEmpty() &&
                params.getUsername() != null && !params.getUsername().isEmpty() &&
                params.getPassword() != null && !params.getPassword().isEmpty()) {
            
            // then try connect
            getDbService().connect(params);
        }
        
        if (params != null) {
            
            // TODO Check this
            session = new ConnectionSession();
            session.setId(100000);
            session.setConnectionParam(params);
            session.setService(getDbService());
            
            SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
            
            DesignerErrDiagram diagram = new DesignerErrDiagram();          
            
            if (params.isMakeBackwardEngine()) {
            
                List<String> list = params.getDatabase();
                
                if (list != null && list.size() == 1) {
                    diagram.host().set(params.getAddress() + ":" + (params.getPort() > 0 ? params.getPort() : 3306));
                    diagram.database().set(list.get(0));

                    Platform.runLater(() -> {
                       module.showReverseEngineer(diagram); 
                    });

                }
                
            } else {
                diagram.name().set(params.getNewSchemaName() != null ? params.getNewSchemaName() : "ERDiagram_" + diagram.getDatabaseName());
                module.openDesignerDiagram(diagram); 
            }
        }
        
    }
    
    @Override
    public void focusLeftTree() {
        // do nothing
    }

    @Override
    public void addTab(Tab t) {
    }

    @Override
    public Tab getTab(String id, String text) {
        return null;
    }
    
    @Override
    public void showTab(Tab t) {
    }

    @Override
    public void addTabToBottomPane(Tab... tab) {
        bottomTabPane.getTabs().addAll(tab);
    }
    
    @Override
    public void removeTabFromBottomPane(Tab... tab) {
        bottomTabPane.getTabs().removeAll(tab);
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
        return nullTab;
    }

    @Override
    public List<Tab> getBottamTabs() {
        return null;
    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem() {
        return nullTreeItem;
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return true;
    }

    @Override
    public String getTabName() {
        return "Schema Designer";
    }

    @Override
    public DisconnectPermission destroyController(DisconnectPermission permission) {
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        if (module.getSelectedErrDiagram() != null && currentDiagram.changed().get()) {
            switch (permission) {

                case NONE:
                    DialogResponce responce = ((MainController)getController()).showConfirm("Save diagram", "Do you want save Diagram?", getStage(), true);
                    if (DialogResponce.OK_YES == responce || DialogResponce.YES_TO_ALL == responce) {

                        module.saveDiagram();

                        if (DialogResponce.YES_TO_ALL == responce) {
                            permission = DisconnectPermission.YES_TO_ALL;
                        }
                    } else if (DialogResponce.NO_TO_ALL == responce) {
                        permission = DisconnectPermission.NO_TO_ALL;
                    }
                    break;

                case YES_TO_ALL:
                    module.saveDiagram();
                    break;
            }
        }
        
        return permission;
    }

    @Override
    public void refreshTree(boolean alertDatabaseCount) {
    }
    
    @Override
    public Object getTreeViewUserData() {
        return null;
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    public void showHideRightPane() {
        if (rootSplitPane.getItems().contains(templateRightPane)) {
            rootSplitPane.getItems().remove(templateRightPane);
        } else {
            rootSplitPane.getItems().add(templateRightPane);
            rootSplitPane.setDividerPosition(1, 0.88);
        }
    }

    public void showDesignerTab(DesignerData data) {
        if (data instanceof DesignerTable) {
            showTableTab((DesignerTable) data);
            
        } else if (data instanceof DesignerImage) {
            showImageTab((DesignerImage) data);
            
        } else if (data instanceof DesignerLayer) {
            showLayerTab((DesignerLayer) data);
            
        } else if (data instanceof DesignerText) {
            showTextTab((DesignerText) data);
            
        } else if (data instanceof DesignerView) {
            showViewTab((DesignerView) data);
            
        }
    }
    
    public void showTableTab(DesignerTable data) {
        Tab tab = null;
        for (Tab t: bottomTabPane.getTabs()) {
            if (t.getText().equals(data.name().get() + " - Table")) {
                tab = t;
            }
        }

        if (tab == null) {
            tab = new Tab();
            tab.setUserData(data);
            tab.textProperty().bind(data.name().concat(" - Table"));
            SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
            DesignerTableController designerTableController = ((MainController)getController()).getTypedBaseController("DesignerTable");
            designerTableController.init(tab, this, getDbService(), module.getSelectedErrDiagram(), designerDatabase, data);

            tab.setContent(designerTableController.getUI());

            addTabToBottomPane(tab);
        }


        bottomTabPane.getSelectionModel().select(tab);
    }
       
    
    public void showLayerTab(DesignerLayer data) {
        Tab tab = null;
        for (Tab t: bottomTabPane.getTabs()) {
            if (t.getText().equals(data.name().get() + " - Layer")) {
                tab = t;
            }
        }

        if (tab == null) {
            tab = new Tab();
            tab.setUserData(data);
            tab.textProperty().bind(data.name().concat(" - Layer"));

            provider.setInput("DesignerLayer");

            DesignerLayerController designerLayerController = (DesignerLayerController) provider.get();
            designerLayerController.init(tab, this, data);

            tab.setContent(designerLayerController.getUI());

            addTabToBottomPane(tab);
        }


        bottomTabPane.getSelectionModel().select(tab);
    }
    
    public void showTextTab(DesignerText data) {
        Tab tab = null;
        for (Tab t: bottomTabPane.getTabs()) {
            if (t.getText().equals(data.name().get() + " - Text")) {
                tab = t;
            }
        }

        if (tab == null) {
            tab = new Tab();
            tab.setUserData(data);
            tab.textProperty().bind(data.name().concat(" - Text"));

            provider.setInput("DesignerText");

            DesignerTextController designerTextController = (DesignerTextController) provider.get();
            designerTextController.init(tab, this, data);

            tab.setContent(designerTextController.getUI());

            addTabToBottomPane(tab);
        }


        bottomTabPane.getSelectionModel().select(tab);
    }
    
    public void showImageTab(DesignerImage data) {
        Tab tab = null;
        for (Tab t: bottomTabPane.getTabs()) {
            if (t.getText().equals(data.name().get() + " - Image")) {
                tab = t;
            }
        }

        if (tab == null) {
            tab = new Tab();
            tab.setUserData(data);
            tab.textProperty().bind(data.name().concat(" - Image"));

            provider.setInput("DesignerImage");

            DesignerImageController designerImageController = (DesignerImageController) provider.get();
            designerImageController.init(tab, this, graph, imagesMap.get(data), data);

            tab.setContent(designerImageController.getUI());

            addTabToBottomPane(tab);
        }


        bottomTabPane.getSelectionModel().select(tab);
    }
    
    
    public void showViewTab(DesignerView data) {
        Tab tab = null;
        for (Tab t: bottomTabPane.getTabs()) {
            if (t.getText().equals(data.name().get() + " - View")) {
                tab = t;
            }
        }

        if (tab == null) {
            tab = new Tab();
            tab.setUserData(data);
            tab.textProperty().bind(data.name().concat(" - View"));

            provider.setInput("DesignerView");

            DesignerViewController designerViewController = (DesignerViewController) provider.get();
            designerViewController.init(tab, this, data, session);

            tab.setContent(designerViewController.getUI());

            addTabToBottomPane(tab);
        }


        bottomTabPane.getSelectionModel().select(tab);
    }
    
    // <editor-fold desc=" Components ">
    
        
    private class NotifyForeightKeysListener implements ChangeListener<Boolean> {

        private DesignerTable table;
        public NotifyForeightKeysListener(DesignerTable table) {
            this.table = table;
        }
        
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            updateEdges(table);
        }
        
    }
    
    private abstract class TreeItemDesigner<T extends DesignerData> {
        
        private T data;
        
        public TreeItemDesigner(T data) {
            this.data = data;
        }
        
        public abstract String getImage();
        public abstract void showTab();

        public T getData() {
            return data;
        }
        
        @Override
        public String toString() {
            return data.name().get();
        }
    }
    
    private class TreeItemDesignerTable extends TreeItemDesigner<DesignerTable> {
        
        private boolean disabled = false;
        
        public TreeItemDesignerTable(DesignerTable dt, boolean disabled) {
            super(dt);
            this.disabled = disabled;
        }
        
        public TreeItemDesignerTable(DesignerTable dt) {
            this(dt, false);
        }

        @Override
        public String getImage() {
            return disabled ? "/np/com/ngopal/smart/sql/ui/images/connection/table_tab_disabled.png" : "/np/com/ngopal/smart/sql/ui/images/connection/table_tab.png";
        }
        
        @Override
        public void showTab() {
            graph.selectNode(tablesMap.get(getData()));
            showTableTab(getData());
        }
    }
    
    private class TreeItemDesignerView extends TreeItemDesigner<DesignerView> {
        
        public TreeItemDesignerView(DesignerView data) {
            super(data);
        }

        @Override
        public String getImage() {
            return "/np/com/ngopal/smart/sql/ui/images/connection/view_tab.png";
        }
        
        @Override
        public void showTab() {
            graph.selectNode(viewsMap.get(getData()));
            showViewTab(getData());
        }
    }
    
    private class TreeItemDesignerLayer extends TreeItemDesigner<DesignerLayer> {
        
        public TreeItemDesignerLayer(DesignerLayer data) {
            super(data);
        }

        @Override
        public String getImage() {
            return "/np/com/ngopal/smart/sql/ui/images/designer/layer_tab.png";
        }
        
        @Override
        public void showTab() {
            graph.selectNode(layersMap.get(getData()));
            showLayerTab(getData());
        }
    }
    
    private class TreeItemDesignerText extends TreeItemDesigner<DesignerText> {
        
        public TreeItemDesignerText(DesignerText data) {
            super(data);
        }

        @Override
        public String getImage() {
            return "/np/com/ngopal/smart/sql/ui/images/designer/text_tab.png";
        }
        
        @Override
        public void showTab() {
            graph.selectNode(textsMap.get(getData()));
            showTextTab(getData());
        }
    }
    
    private class TreeItemDesignerImage extends TreeItemDesigner<DesignerImage> {
        
        public TreeItemDesignerImage(DesignerImage data) {
            super(data);
        }

        @Override
        public String getImage() {
            return "/np/com/ngopal/smart/sql/ui/images/designer/image_tab.png";
        }
        
        @Override
        public void showTab() {
            graph.selectNode(textsMap.get(getData()));
            showImageTab(getData());
        }
    }
    
    // </editor-fold>
    
    /** Terah Additions */
    
    private void subInitialiser(){       
        modelingAdiitionsListview.setCellFactory(new Callback() {

            @Override
            public ListCell call(Object p) { 
                return new ListCell(){
                    
                    Image fav = new Image(
                            "/np/com/ngopal/smart/sql/ui/images/connection/organize_favorite.png",
                            27, 27, true, true),
                            pref = new Image(
                            "/np/com/ngopal/smart/sql/ui/images/connection/alter_storedprocs.png",
                            27, 27, true, true);
                    
                    {
                        setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent t) { 
                                if(getIndex() == 0){
                                    
                                }else{
                                    if(t.getClickCount() > 1){
                                        //we select and put it on the canvas
                                    }
                                }
                            }
                        });
                    }
                    
                    @Override
                    public void updateIndex(int i) {
                        super.updateIndex(i);
                       
                    }

                    @Override
                    public void updateSelected(boolean bln) {
                        super.updateSelected(bln); 
                        if (bln) {
                            setStyle("-fx-background-color: #3c71b3;"
                                    + "-fx-text-fill: black;");
                        }else{
                             setStyle("-fx-background-color: white;");
                        }
                    }
                    
                    Button newButton(){
                        Button b = new Button("",
                                       new ImageView(pref));
                        b.setPadding(new Insets(0.1));
                               b.getStyleClass().add("beauty-button");
                               b.setOnAction(new EventHandler<ActionEvent>() {
                                   @Override
                                   public void handle(ActionEvent t) {
                                       provider.setInput("TableTemplateEdittor");
                                       TableTemplateEdittorController ttec
                                               = (TableTemplateEdittorController) provider.get();
                                       Stage s = ttec.init(dtts, convertDesignerTableTemplateJsonToObject(),
                                               savingMechanism);
                                       s.setOnHidden(new EventHandler<WindowEvent>() {

                                           ArrayList<DesignerTable> list = null;

                                           @Override
                                           public void handle(WindowEvent t) {
                                               getController().addBackgroundWork(new WorkProc() {
                                                   @Override
                                                   public void updateUI() {
                                                       modelingAdiitionsListview.getItems().clear();
                                                       modelingAdiitionsListview.getItems().add("template editor");
                                                       modelingAdiitionsListview.getItems().
                                                               addAll(list);
                                                   }

                                                   @Override
                                                   public void run() {
                                                       list = convertDesignerTableTemplateJsonToObject();
                                                       callUpdate = true;
                                                   }
                                               });
                                           }
                                       });
                                       s.showAndWait();
                                   }
                               });
                               return b;
                    }
                    
                    @Override
                    protected void updateItem(Object t, boolean bln) {
                        super.updateItem(t, bln);
                        
                        if(bln){
                            setContentDisplay(ContentDisplay.TEXT_ONLY);
                            setText("");
                            setStyle("-fx-background-color: lightgray;");
                            return;
                        }
                        
                        setStyle("-fx-background-color: white;");
                        setAlignment(Pos.CENTER_LEFT);
                        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                       if (getIndex() == 0) {
                            if (getGraphic() == null) {
                                setGraphic(newButton());
                                Tooltip.install(getGraphic(),new Tooltip(t.toString()));
                           } else {
                               if (getGraphic() instanceof Button) {
                                   Button b = ((Button) getGraphic());
                                   if (((ImageView) b.getGraphic()).getImage() != pref) {
                                       ((ImageView) b.getGraphic()).setImage(pref);
                                   }
                               } else {
                                  setGraphic(newButton());
                               }
                           }
                            
                        } else {
                            DesignerTable dt = (DesignerTable) t;
                            if (getGraphic() == null || !(getGraphic() instanceof HBox)) {
                                HBox hb = new HBox(3);
                                VBox pa = new VBox(1);
                                ImageView iv = new ImageView(fav);
                                iv.setId("image");
                                Label topic = new Label(), des = new Label();
                                topic.setId("topic");
                                topic.setFont(Font.font(Font.getDefault().getName(), FontWeight.BOLD, 
                                        Font.getDefault().getSize()));
                                des.setId("des");
                                topic.setText(dt.name().get());
                                StringBuilder sb = new StringBuilder();
                                for (DesignerColumn dc : dt.columns()) {
                                    if (sb.toString().isEmpty()) {
                                        sb.append(dc.name().get());
                                    } else {
                                        sb.append(",");
                                        sb.append(dc.name().get());
                                    }

                                }
                                des.setText(sb.toString());
                                pa.getChildren().addAll(topic,des);
                                hb.getChildren().addAll(iv,pa);
                                setGraphic(hb);
                            }else{
                                getGraphic().getStyleClass().remove("beauty-button");
                                HBox hb = (HBox)getGraphic();
                                ((Label)hb.lookup("#topic")).setText(dt.name().get());
                                StringBuilder sb = new StringBuilder();
                                for (DesignerColumn dc : dt.columns()) {
                                    if (sb.toString().isEmpty()) {
                                        sb.append(dc.name().get());
                                    } else {
                                        sb.append(",");
                                        sb.append(dc.name().get());
                                    }

                                }
                                ((Label)hb.lookup("#des")).setText(sb.toString());
                                ((ImageView)hb.lookup("#image")).setImage(fav);
                            }

                            setTextFill(Color.BLACK);
                        }
                                                
                    }
                    
                };
            }
        });
        
        modelingAdiitionsListview.getItems().add("template editor");
        modelingAdiitionsListview.getItems().addAll(convertDesignerTableTemplateJsonToObject());
        
        mainUI.sceneProperty().addListener(new ChangeListener<Scene>() {
            Gson gson = new Gson();

            @Override
            public void changed(ObservableValue<? extends Scene> ov, Scene t, Scene t1) {
                if (t1 == null) {
                    savingMechanism.setAdditionalData(modelingAdiitionsListview.getItems());
                    Recycler.addWorkProc(savingMechanism);                    
                }
            }
        });
    }
    
    private ArrayList<DesignerTable> convertDesignerTableTemplateJsonToObject() {
        ArrayList<DesignerTable> tables = new ArrayList();
        Gson gson = new Gson();
        for (DesignerTableTemplate dtt : dtts.getAll()) {
            DesignerTableWrapper dtw = gson.fromJson(dtt.getData(), DesignerTableWrapper.class);
            if (dtw != null) {
                tables.add(dtw.convertToDesignerTable());
            }
        }
        
        if(tables.isEmpty()){
            
            DesignerTable dt = new DesignerTable(),dt1 = new DesignerTable(),dt2 = new DesignerTable();
            dt.name().set("User");
            DesignerColumn c1= new DesignerColumn(),c2= new DesignerColumn(),
                    c3 = new DesignerColumn(),c4 = new DesignerColumn();
            c1.name().set("Username");
            c1.notNull().set(true);
            c2.name().set("Email");
            c3.name().set("Password");
            c3.notNull().set(true);
            c4.name().set("Create time");
            c4.defaultValue().set("CURRENT TIMESTAMP");
            dt.columns().addAll(c1,c2,c3,c4);
            ///////////////////////////////////////////////
            dt1.name().set("Timestamps");
            c1= new DesignerColumn();c4= new DesignerColumn();
            c4.name().set("Create Time");
            c4.defaultValue().set("CURRENT TIMESTAMP");
            c1.name().set("Update Time");
            dt1.columns().addAll(c1,c4);
            ////////////////////////////////////////////////
            dt2.name().set("Category");
            c1= new DesignerColumn();c4= new DesignerColumn();
            c1.name().set("Name");
            c1.notNull().set(true);
            c4.name().set("Category id");
            c4.notNull().set(true);
            c4.primaryKey().set(true);
            dt2.columns().addAll(c1,c4);
            ////////////////////////////////////////////////
            tables.add(dt);
            tables.add(dt1);
            tables.add(dt2);
            savingMechanism.setAdditionalData(tables);
            Recycler.addWorkProc(savingMechanism);
            
        }

        return tables;
    }  
    
    private WorkProc<Collection<?>> savingMechanism = new WorkProc<Collection<?>>(){

        Gson gson = new Gson();
        
        @Override
        public void updateUI() { }

        @Override
        public void run() {
            
            for (Object o : getAddData()) {
                if (o instanceof DesignerTable) {
                    DesignerTable x = (DesignerTable) o;
                    DesignerTableTemplate dtt = new DesignerTableTemplate(
                            x.name().get().trim(), gson.toJson(new DesignerTableWrapper(x)));
                    dtts.save(dtt);
                }
            }            
        }
        
    };
    
    @Override
    public void isFocusedNow() {}
    
    @Override
    public void showLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hideLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty(){
        return null;
    }
}
