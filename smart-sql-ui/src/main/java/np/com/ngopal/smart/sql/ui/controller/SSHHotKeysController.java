package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.SSHHotKeyService;
import np.com.ngopal.smart.sql.model.SSHHotKeys;
import np.com.ngopal.smart.sql.modules.SSHModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SSHHotKeysController extends BaseController implements
        Initializable {

    @Inject
    SSHHotKeyService hotkeysService;

    @Setter
    SSHModule sshModule;

    @FXML
    private BorderPane hotkeysBorderPane;

    @FXML
    private Button saveButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField nextTab;

    @FXML
    private TextField previousTab;

    @FXML
    private TextField addServer;

    @FXML
    private TextField removeServer;

    @FXML
    private TextField connectToSelected;

    @FXML
    private TextField properties;

    @FXML
    private TextField importTree;

    @FXML
    private TextField exportTree;

    @FXML
    private TextField connectTo;

    @FXML
    private TextField dublicate;

    @FXML
    private TextField remapHotkeys;

    @FXML
    private AnchorPane mainUI;

    private ObjectProperty<SSHHotKeys> hotkeys;

    @FXML
    public void saveAction(ActionEvent event) {
        hotkeysService.save(hotkeys.get());

        sshModule.updateHotKeys(hotkeys.get());

        getStage().close();
    }

    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        hotkeys = new SimpleObjectProperty<>();

        final List<SSHHotKeys> list = hotkeysService.getAll();
        if (list == null || list.isEmpty()) {
            hotkeys.set(new SSHHotKeys());
        } else {
            hotkeys.set(list.get(0));
        }

        updateFields(hotkeys.get());
        propertyListeners();
    }

    private void updateFields(SSHHotKeys hotkeys) {
        nextTab.setText(hotkeys.getNextServerTab() == null ? "No" : hotkeys.getNextServerTab());
        nextTab.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(nextTab));

        previousTab.setText(hotkeys.getPrevServerTab() == null ? "No" : hotkeys.getPrevServerTab());
        previousTab.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(previousTab));

        addServer.setText(hotkeys.getAddServer() == null ? "No" : hotkeys.getAddServer());
        addServer.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(addServer));

        removeServer.setText(hotkeys.getRemoveServer() == null ? "No" : hotkeys.getRemoveServer());
        removeServer.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(removeServer));

        connectToSelected.setText(hotkeys.getConnectToSelected() == null ? "No" : hotkeys.getConnectToSelected());
        connectToSelected.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(connectToSelected));

        properties.setText(hotkeys.getPropertiesOfSelected() == null ? "No" : hotkeys.getPropertiesOfSelected());
        properties.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(properties));

        importTree.setText(hotkeys.getImportTree() == null ? "No" : hotkeys.getImportTree());
        importTree.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(importTree));

        exportTree.setText(hotkeys.getExportTree() == null ? "No" : hotkeys.getExportTree());
        exportTree.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(exportTree));

        connectTo.setText(hotkeys.getConnectTo() == null ? "No" : hotkeys.getConnectTo());
        connectTo.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(connectTo));

        dublicate.setText(hotkeys.getDuplicateConnection() == null ? "No" : hotkeys.getDuplicateConnection());
        dublicate.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(dublicate));

        remapHotkeys.setText(hotkeys.getRemapHotkeys() == null ? "No" : hotkeys.getRemapHotkeys());
        remapHotkeys.addEventFilter(KeyEvent.ANY, new CheckKeysEventHandler(remapHotkeys));
    }

    private class CheckKeysEventHandler implements EventHandler<KeyEvent> {

        private final TextField field;

        private String oldValue;

        private boolean alt = false;

        private boolean ctrl = false;

        private boolean shift = false;

        private boolean accepted = true;

        public CheckKeysEventHandler(final TextField field) {
            this.field = field;
            oldValue = field.getText();
        }

        @Override
        public void handle(KeyEvent event) {
            String text = "";
            switch (event.getCode()) {
                case SHIFT:
                    if (event.getEventType() == KeyEvent.KEY_PRESSED) {
                        shift = true;
                        accepted = false;
                    } else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
                        shift = false;
                    }
                    break;

                case CONTROL:
                    if (event.getEventType() == KeyEvent.KEY_PRESSED) {
                        ctrl = true;
                        accepted = false;
                    } else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
                        ctrl = false;
                    }
                    break;

                case ALT:
                    if (event.getEventType() == KeyEvent.KEY_PRESSED) {
                        alt = true;
                        accepted = false;
                    } else if (event.getEventType() == KeyEvent.KEY_RELEASED) {
                        alt = false;
                    }
                    break;

                default:
                    if (event.getEventType() == KeyEvent.KEY_RELEASED) {
                        if ((event.getCode() == KeyCode.DELETE || event.getCode() == KeyCode.BACK_SPACE) && accepted) {
                            field.setText("No");
                        } else {
                            text = event.getCode().getName();
                        }
                    }
            }

            if (!accepted) {
                if (shift || ctrl || alt) {
                    field.setText((shift ? "Shift + " : "") + (ctrl ? "Ctrl + " : "") + (alt ? "Alt + " : ""));

                    if (text != null && !text.isEmpty()) {
                        field.setText(field.getText() + text);

                        oldValue = field.getText();
                        accepted = true;
                    }
                } else {
                    field.setText(oldValue);
                }
            }
        }

    }

    public void propertyListeners() {
        nextTab.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setNextServerTab(newValue);
        });

        previousTab.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setPrevServerTab(newValue);
        });

        addServer.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setAddServer(newValue);
        });

        removeServer.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setRemoveServer(newValue);
        });

        connectToSelected.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setConnectToSelected(newValue);
        });

        properties.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setPropertiesOfSelected(newValue);
        });

        importTree.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setImportTree(newValue);
        });

        exportTree.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setExportTree(newValue);
        });

        connectTo.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setConnectTo(newValue);
        });

        dublicate.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setDuplicateConnection(newValue);
        });

        remapHotkeys.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            hotkeys.get().setRemapHotkeys(newValue);
        });
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }

}
