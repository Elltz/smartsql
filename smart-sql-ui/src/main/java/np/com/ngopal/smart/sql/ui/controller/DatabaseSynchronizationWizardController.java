/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author terah laweh(rumorsapp@gmail.com)
 */
public class DatabaseSynchronizationWizardController extends BaseController implements Initializable {

    //footer buttons 
    @FXML
    private Button backButton;

    @FXML
    private Button nextButton;

    @FXML
    private Button cancelButton;

    //welcome screen 
    @FXML
    private HBox welcomeScreen;

    @FXML
    private RadioButton startNewJobRadioButton;

    @FXML
    private RadioButton editSavedJobRadioButton;

    @FXML
    private TextField directoryTextfield;

    @FXML
    private Button directoryChooser;

    //details selection page for source
    @FXML
    private ComboBox fromConnectionChooserSource;

    @FXML
    private ComboBox mysqlDatabaseChooserSource;

    @FXML
    private CheckBox mysqlTabCompressedProSource;

    @FXML
    private Button compressedProtocolHelpButtonSource;

    @FXML
    private Button mysqlSessionIdleHelpSource;

    @FXML
    private RadioButton sessionIdleTimeoutDefaultRadioButtonSource;

    @FXML
    private RadioButton sessionIdleTimeoutCustomRadioButtonSource;

    @FXML
    private TextField mySQLHostAddresTextfieldSource;

    @FXML
    private TextField mySQLUsernameTextFieldSource;

    @FXML
    private TextField mySQLPasswordTextFieldSource;

    @FXML
    private TextField mySQLPortTextfieldSource;

    @FXML
    private CheckBox useHTTPTunnelCheckboxSource;

    @FXML
    private Button useHTTPTunnelHelpButtonSource;

    @FXML
    private TextField urlHTTPTextfieldSource;

    @FXML
    private TextField httpTimeoutTextfieldSource;

    @FXML
    private Button httpAdvancedButtonSource;

    @FXML
    private CheckBox useHTTPSocketCheckboxSource;

    @FXML
    private TextField httpSocketFilepathTextfieldSource;

    @FXML
    private CheckBox useSSHTunnelCheckboxSource;

    @FXML
    private Button useSHHTunnelHelpButtonSource;

    @FXML
    private TextField sshHostTextfieldSource;

    @FXML
    private TextField sshPortTextfieldSource;

    @FXML
    private TextField sshUsernameTextfieldSource;

    @FXML
    private RadioButton sshPublickeyRadioButtonSource;

    @FXML
    private RadioButton sshPasswordRadioButtonSource;

    @FXML
    private TextField sshPasswordTextfieldSource;

    @FXML
    private TextField sshPrivatekeyTexfieldSource;

    @FXML
    private Button sshPrivatekeyButtonChooserSource;

    @FXML
    private CheckBox useSSLEncryptionCheckboxSource;

    @FXML
    private Button useSSLEncryptionHelpButtonSource;

    @FXML
    private TextField sslCACertificateTextfieldSource;

    @FXML
    private Button sslCACertificateButtonChooserSource;

    @FXML
    private TextField sslCipherTextfieldSource;

    @FXML
    private CheckBox sslUseAuthenCheckboxSource;

    @FXML
    private TextField sslClientKeyTextfieldSource;

    @FXML
    private Button sslClientKeyChooserButtonSource;

    @FXML
    private TextField sslClientCertificateTextfieldSource;

    @FXML
    private Button sslClientCertificateChooserButtonSource;

    @FXML
    private FlowPane selectDatabaseContainerSource;

    // details selection page for target
    @FXML
    private ComboBox fromConnectionChooserTarget;

    @FXML
    private ComboBox mysqlDatabaseChooserTarget;

    @FXML
    private CheckBox mysqlTabCompressedProTarget;

    @FXML
    private Button compressedProtocolHelpButtonTarget;

    @FXML
    private Button mysqlSessionIdleHelpTarget;

    @FXML
    private RadioButton sessionIdleTimeoutDefaultRadioButtonTarget;

    @FXML
    private RadioButton sessionIdleTimeoutCustomRadioButtonTarget;

    @FXML
    private TextField mySQLHostAddresTextfieldTarget;

    @FXML
    private TextField mySQLUsernameTextFieldTarget;

    @FXML
    private TextField mySQLPasswordTextFieldTarget;

    @FXML
    private TextField mySQLPortTextfieldTarget;

    @FXML
    private CheckBox useHTTPTunnelCheckboxTarget;

    @FXML
    private Button useHTTPTunnelHelpButtonTarget;

    @FXML
    private TextField urlHTTPTextfieldTarget;

    @FXML
    private TextField httpTimeoutTextfieldTarget;

    @FXML
    private Button httpAdvancedButtonTarget;

    @FXML
    private CheckBox useHTTPSocketCheckboxTarget;

    @FXML
    private TextField httpSocketFilepathTextfieldTarget;

    @FXML
    private CheckBox useSSHTunnelCheckboxTarget;

    @FXML
    private Button useSHHTunnelHelpButtonTarget;

    @FXML
    private TextField sshHostTextfieldTarget;

    @FXML
    private TextField sshPortTextfieldTarget;

    @FXML
    private TextField sshUsernameTextfieldTarget;

    @FXML
    private RadioButton sshPublickeyRadioButtonTarget;

    @FXML
    private RadioButton sshPasswordRadioButtonTarget;

    @FXML
    private TextField sshPasswordTextfieldTarget;

    @FXML
    private TextField sshPrivatekeyTexfieldTarget;

    @FXML
    private Button sshPrivatekeyButtonChooserTarget;

    @FXML
    private CheckBox useSSLEncryptionCheckboxTarget;

    @FXML
    private Button useSSLEncryptionHelpButtonTarget;

    @FXML
    private TextField sslCACertificateTextfieldTarget;

    @FXML
    private Button sslCACertificateButtonChooserTarget;

    @FXML
    private TextField sslCipherTextfieldTarget;

    @FXML
    private CheckBox sslUseAuthenCheckboxTarget;

    @FXML
    private TextField sslClientKeyTextfieldTarget;

    @FXML
    private Button sslClientKeyChooserButtonTarget;

    @FXML
    private TextField sslClientCertificateTextfieldTarget;

    @FXML
    private Button sslClientCertificateChooserButtonTarget;

    @FXML
    private FlowPane selectDatabaseContainerTarget;

    //tables selection 
    @FXML
    private FlowPane selectTablesContainer;

    @FXML
    private RadioButton syncAllTables;

    @FXML
    private RadioButton syncSelectedTables;

    @FXML
    private TableView tableview;

    @FXML
    private TableColumn tableColumn;

    @FXML
    private TableColumn columnsToCompareColumn;

    @FXML
    private TableColumn whereColumn;

    @FXML
    private Button upButton;

    @FXML
    private Button downButton;

    @FXML
    private CheckBox foreignkeychecksCheckbox;

    @FXML
    private Button helpButton;

    //advanced options screen
    @FXML
    private FlowPane advancedOptionsContainer;

    @FXML
    private RadioButton oneWaySyncRadioButton;

    @FXML
    private CheckBox dontDeleteCheckbox;

    @FXML
    private RadioButton twoWaySyncRadioButton;

    @FXML
    private RadioButton visuallyMergeRadioButton;

    private final ToggleGroup advancedOptionsDecider = new ToggleGroup();

   //////////////////////////
    @FXML
    private AnchorPane mainUI;

    @FXML
    private StackPane subUI;

    private Stage dialog;

    public void init(Window win) {
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(610);
        dialog.setHeight(531);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setResizable(false);
        dialog.setTitle("Database Synchronization Wizard ");
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        welcomeScreen.toFront(); //since everything is disordered in the fxml
        
        oneWaySyncRadioButton.setToggleGroup(advancedOptionsDecider);
        twoWaySyncRadioButton.setToggleGroup(advancedOptionsDecider);
        visuallyMergeRadioButton.setToggleGroup(advancedOptionsDecider);
        
        /////////////////////////////////////////////
        subUI.getChildren().addListener(screenChangerNotifier);
        nextButton.setOnAction(onAction);
        backButton.setOnAction(onAction);
        cancelButton.setOnAction(onAction);
        compressedProtocolHelpButtonSource.setOnAction(onAction);
        compressedProtocolHelpButtonTarget.setOnAction(onAction);
        directoryChooser.setOnAction(onAction);
        dontDeleteCheckbox.setOnAction(onAction);
        downButton.setOnAction(onAction);
        foreignkeychecksCheckbox.setOnAction(onAction);
        helpButton.setOnAction(onAction);
        httpAdvancedButtonSource.setOnAction(onAction);
        httpAdvancedButtonTarget.setOnAction(onAction);
        mysqlSessionIdleHelpSource.setOnAction(onAction);
        mysqlSessionIdleHelpTarget.setOnAction(onAction);
    }
    
    /**
     * Since our changing mechanism uses toFront() and toBack() ;
     * 
     * this listener will be the place that we handle our new Screen.
     * for example if i am in the welcome screen and i press next i will go
     *to the selection of database from source. here is where i will 
     * populate the node and know who is showing.
     * 
     * 
    */
    private final ListChangeListener<Node> screenChangerNotifier = new ListChangeListener<Node>() {

        @Override
        public void onChanged(ListChangeListener.Change<? extends Node> change) {
            while (change.next()) {
                if (change.wasPermutated()) {
                    Node previousNode = change.getList().get(change.getPermutation(0));//the node that was previously showing
                    Node showingNode = change.getList().get((change.getList().size() - 1));
                    System.out.println("currently showing screen is " + showingNode.getId());
                    System.out.println("previously showing screen is " + previousNode.getId());
                    
                    if (showingNode == welcomeScreen) {
                        /**
                         * Make neccesary changes to UI to fit this page
                         */                        
                    } else if (showingNode == advancedOptionsContainer) {
                        /**
                         * Make neccesary changes to UI to fit this page
                         */
                    } else if (showingNode == selectDatabaseContainerSource) {
                        /**
                         * Make neccesary changes to UI to fit this page
                         */
                    } else if (showingNode == selectTablesContainer) {
                        /**
                         * Make neccesary changes to UI to fit this page
                         */
                    } else if (showingNode == selectDatabaseContainerTarget) {
                        /**
                         * Make neccesary changes to UI to fit this page
                         */
                    }
                }
            }
        }
    };
    
    private final EventHandler<ActionEvent> onAction = new EventHandler(){
        
        private byte pos = 0; //welcome screen
        
        private void navigator(int position){            
            if(position < subUI.getChildren().size() && position > -1){
                subUI.getChildren().get(position).toFront();
            }else{
                pos = 0;
            }
        }

        @Override
        public void handle(Event t) { 
           Object o =  t.getSource();
           
           if(o == nextButton){
               pos++;
               navigator(pos);
           }else if(o == backButton){
               pos--;
               navigator(pos);
           }else if(o == cancelButton){
               dialog.hide();
           }
        }
        
    };

}
