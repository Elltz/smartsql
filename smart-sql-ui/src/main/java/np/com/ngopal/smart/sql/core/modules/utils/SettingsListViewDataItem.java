/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.dialogs.InputDialogController;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public abstract class SettingsListViewDataItem implements SettingsListViewAdaptation {

    private boolean isBoolean;
    private boolean bValue;
    private String sValue;
    private boolean selected;
    private int type;
    protected Node n;

    private static EventHandler<MouseEvent> labelMouseClicked = new EventHandler<MouseEvent>() {

        InputDialogController inputDialog;

        @Override
        public void handle(MouseEvent t) {
            if (inputDialog == null) {
                FXMLLoader fxml = new FXMLLoader(getClass().getResource(
                        "/np/com/ngopal/smart/sql/ui/InputDialog.fxml"));
                Stage s = new Stage(StageStyle.UTILITY);
                s.setResizable(false);
                s.setWidth(410);
                s.setAlwaysOnTop(true);
                s.setTitle("INPUT DIALOG");
                try {
                    s.setScene(new Scene(fxml.load()));
                    inputDialog = fxml.getController();
                } catch (Exception e) {
                }
            }
            inputDialog.init("MODIFY TEXT", "",
                    ((Label) t.getSource()).getText());
            if (!inputDialog.getStage().isShowing()) {
                inputDialog.getStage().showAndWait();
            }
            if (inputDialog.getResponce() == DialogResponce.OK_YES
                    || inputDialog.getResponce() == DialogResponce.YES_TO_ALL) {
                ((Label) t.getSource()).setText(inputDialog.getInputValue());
            }
        }
    };

    public SettingsListViewDataItem() {
    }

    public SettingsListViewDataItem(boolean isBoolean, boolean bValue, String sValue, int type) {
        this.isBoolean = isBoolean;
        this.bValue = bValue;
        this.sValue = sValue;
        this.type = type;
    }

    public SettingsListViewDataItem(boolean bValue, int t) {
        this.isBoolean = true;
        this.bValue = bValue;
        type = t;
    }

    public SettingsListViewDataItem(String sValue, int t) {
        this.isBoolean = false;
        this.sValue = sValue;
        type = t;
    }

    public boolean isIsBoolean() {
        return isBoolean;
    }

    public void setValue(boolean bool) {
        bValue = bool;
    }

    public void setValue(String s) {
        sValue = s;
    }

    public void setItemSelected(boolean bool) {
        selected = bool;
    }

    public boolean isSelected() {
        return selected;
    }

    @Override
    public Node getCanvas() {
        if (n == null) {
            if (isBoolean) {
                Circle circle = new Circle(11, bValue ? Color.WHITE : Color.TOMATO);
                ToggleButton tb = new ToggleButton(getValue());
                tb.setGraphic(circle);
                tb.setContentDisplay(bValue ? ContentDisplay.RIGHT : ContentDisplay.LEFT);
                tb.setGraphicTextGap(10);
                tb.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> ov,
                            Boolean t, Boolean t1) {
                        setValue(t1);
                        circle.setFill(t1 ? Color.WHITE : Color.TOMATO);
                        tb.setContentDisplay(bValue ? ContentDisplay.RIGHT : ContentDisplay.LEFT);
                        tb.setText(getValue());
                    }
                });

                tb.getStyleClass().addAll("beauty-button", "settings-toggle");
                tb.setPrefSize(ToggleButton.USE_COMPUTED_SIZE, ToggleButton.USE_COMPUTED_SIZE);
                n = tb;
            } else {
                Label l = new Label(getValue());
                l.setText(l.getText().isEmpty() ? "< empty >" : l.getText());
                l.setMinWidth(100);
                if (l.getOnMouseClicked() == null) {                    
                    l.setTooltip(new Tooltip("click text to change"));
                    l.setTextFill(Color.web("#293955"));
                    l.setFont(Font.font(Font.getFontNames().get(11), FontWeight.EXTRA_BOLD, Font.getDefault().getSize()));
                    l.setOnMouseClicked(labelMouseClicked);
                    l.textProperty().addListener(new ChangeListener<String>() {
                        @Override
                        public void changed(ObservableValue<? extends String> ov,
                                String t, String t1) { 
                            try {
                                if (call(t1)) { setValue(t1); }
                                else {
                                    l.textProperty().removeListener(this);
                                    l.setText(t);
                                    l.textProperty().addListener(this);
                                }
                            } catch(Exception e) { }
                        }
                    });
                }
                n = l;
            }
        }
        return n;
    }

    @Override
    public String getValue() {
        return isBoolean ? bValue ? " YES " : " NO " : sValue;
    }

    @Override
    public String getRepresentation() {
        switch (type) {
            case SETTINGS_AUTO_RESTORE_LAST_KNOWN_SETTINGS:
                return "AUTO RESTORE LAST KNOW APPLICATION STATE  ";
            case SETTINGS_AUTO_START_MYSQL_ON_SYSTEM:
                return "AUTO START MYSQL ON SYSTEM IF EXIST";
            case SETTINGS_MYSQL_SERVER_STATUS:
                return "CURRENT MYSQL SERVER STATUS";
            case SETTINGS_EMAIL:
                return "REGISTERED SMARTSQL EMAIL ACCOUNT -:-     ";
            case SETTINGS_NAME:
                return "REGISTERED SMARSQL NAME ACCOUNT -:-       ";
            case SETTINGS_KEYTYPE:
                return "CURRENT VALID SERIAL REGISTRATION LICENSE ";
            case SETTINGS_PAYMENT_LEVEL:
                return "APPLICATION STATUS IS PAID VERSION        ";
            case SETTINGS_SAVE_DURATION_INTERVAL:
                return "AUTOMATED APP STATE PERSISTING DURATION   ";
            case SETTINGS_MYSQL_SERVER_NAME:
                return "MYSQL SERVER ON SYSTEM SERVICE NAME       ";
            case SETTINGS_RUN_APP_IN_FULL_SCREEN:
                return "ALWAYS START APPLICATION IN FULLSCREEN    ";
            case SETTINGS_APPLICATION_TEXT_FONT_SIZE:
                return "SQL EDITOR TEXT SIZE                      ";
            case SETTINGS_SHOW_AUTO_COMPLETE_IN_QUERY:
                return "ACTIVATE AUTO-COMPLETE IN QUERY EDITOR    ";
            default:
                return "";
        }
    }
}
