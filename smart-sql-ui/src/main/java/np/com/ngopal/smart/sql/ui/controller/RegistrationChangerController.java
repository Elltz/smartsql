/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class RegistrationChangerController extends BaseController implements Initializable{

    @FXML
    private Label title;
    
    @FXML
    private TextField emailField;
    
    @FXML
    private TextField countryField;
    
    @FXML
    private TextField nameField;
    
    @FXML
    private Button okButton;
    
    @FXML
    private Button closeButton;
    
    @FXML
    private AnchorPane mainUI;
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    Stage stage = new Stage(StageStyle.UTILITY);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        countryField.setDisable(true);
        countryField.setVisible(true);
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                //better implementation will follow
                if(emailField.getText().isEmpty() || nameField.getText().isEmpty() /**||
                        countryField.getText().isEmpty()*/){
                    ((MainController)getController()).showError("INPUT ERROR", "Please fill all fields", null);
                    return;
                }
                synchronized (((MainController)getController()).getSmartDataLocker()) {
                    Properties props = new Properties();
                    props.setProperty(Flags.USER_EMAIL_ID, emailField.getText());
                    props.setProperty(Flags.USER_FULL_NAME, nameField.getText());
                    ((MainController)getController()).getSmartData().updateProperties(props);
                    //controller.getSmartData().setCountry(countryField.getText());
                }
                stage.close();
            }
        }); 
        
        stage.setScene(new Scene(getUI()));
        stage.setResizable(false);        
    }
    
    public void isFirstTime(boolean firstTime) {
        if (firstTime) {
            stage.setTitle("Registration Details");
        } else {
            synchronized (((MainController)getController()).getSmartDataLocker()) {
                emailField.setText(((MainController)getController()).getSmartData().getEmailId());
                nameField.setText(((MainController)getController()).getSmartData().getFullName());
                //countryField.setText(controller.getSmartData().getCountry());
            }
            stage.setTitle("Edit Registration Details");
        }
        
        stage.initOwner(getController().getStage());
        stage.showAndWait();
    }
    
}
