/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.ui.controller.BuilderViewController;
import np.com.ngopal.smart.sql.ui.controller.BuilderViewController.ColumnOnColumnRowData;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class QueryBuilderRowData {

    private SimpleBooleanProperty show;
    private SimpleBooleanProperty groupBy;
    private SimpleStringProperty ageFunction;
    private SimpleStringProperty sort;
    private SimpleStringProperty whereCondition;
    private SimpleStringProperty alias;

    private ColumnOnColumnRowData columnSquared;
    private BuilderViewController builderViewController;
    
    ArrayList<SimpleStringProperty> conditions
            = new ArrayList<SimpleStringProperty>();

    public QueryBuilderRowData(BuilderViewController builderViewController, ColumnOnColumnRowData columnSquared) {       
        this.columnSquared = columnSquared;
        this.builderViewController = builderViewController;
        
        show = new SimpleBooleanProperty(true){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                onReflectChange.run();
            }            
        };
        groupBy = new SimpleBooleanProperty(false){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                onReflectChange.run();
            }            
        };
        ageFunction = new SimpleStringProperty(""){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                onReflectChange.run();
            }            
        };
        sort = new SimpleStringProperty("(Not Sorted)"){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                onReflectChange.run();
            }            
        };
        whereCondition = new SimpleStringProperty(""){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                onReflectChange.run();
            }            
        };
        alias = new SimpleStringProperty(""){
            @Override
            protected void invalidated() {
                super.invalidated(); 
                onReflectChange.run();
            }            
        };

    }


    public Column getColumn() {
        return columnSquared.getColumn();
    }

    public void setColumn(Column column) {
        columnSquared.setColumn(column);
    }

    public String getTable() {
        return builderViewController.getLabel();
    }
    
    public String getFullTable() {
        return columnSquared.getColumn().getTable().getDatabase().getName() + "." + columnSquared.getColumn().getTable().getName() + " " + builderViewController.getLabel();
    }

    public SimpleBooleanProperty getShow() {
        return show;
    }

    public SimpleBooleanProperty getGroupBy() {
        return groupBy;
    }

    public SimpleStringProperty getAgeFunction() {
        return ageFunction;
    }

    public SimpleStringProperty getSort() {
        return sort;
    }

    public SimpleStringProperty getWhereCondition() {
        return whereCondition;
    }

    public SimpleStringProperty getAlias() {
        return alias;
    }

    public ArrayList<SimpleStringProperty> getConditions() {
        return conditions;
    }
    
    public SimpleStringProperty addNewCondition(String input) {
        SimpleStringProperty ssp = new SimpleStringProperty(input) {
            @Override
            protected void invalidated() { 
                onReflectChange.run();
            }
            
        };
        conditions.add(ssp);
        return ssp;
    }
    
    private Runnable onReflectChange;
    
    public void setOnReflectChange(Runnable run){
        onReflectChange = run;
    }
}
