package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class DataSearchFilterDialog extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private CheckBox selectAllCheckbox;
    
    @FXML
    private ListView<String[]> listview;
    
    @FXML
    private Button cancelButton;
    
    @FXML
    private Button okButton;
    
    @FXML
    private CheckBox blobCheckbox;
    
    @FXML
    private CheckBox  textCheckbox;
    
    @FXML
    private CheckBox dateCheckbox;
    
    @FXML
    private CheckBox binaryCheckbox;
    
    @FXML
    private CheckBox stringCheckbox;
    
    @FXML
    private CheckBox numericCheckbox;

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        
        listview.setCellFactory(new Callback<ListView<String[]>, ListCell<String[]>>() {

            @Override
            public ListCell<String[]> call(ListView<String[]> p) { 
                return new CheckBoxListCell<String[]>(){
                    {
                        setSelectedStateCallback(new Callback<String[], ObservableValue<Boolean>>() {

                            @Override
                            public ObservableValue<Boolean> call(String[] p) { 
                                if(p[0].equals("0")){
                                    return new SimpleBooleanProperty(false);
                                }else{
                                    return new SimpleBooleanProperty(true);
                                }
                            }
                        });
                        
                     graphicProperty().addListener(new ChangeListener<Node>() {

                         @Override
                         public void changed(ObservableValue<? extends Node> ov, Node t, Node check) { 
                             if(check != null){
                                 selectAllCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                                     @Override
                                     public void changed(ObservableValue<? extends Boolean> ov,
                                             Boolean t, Boolean t1) {
                                         ((CheckBox)check).setSelected(t1);
                                     }
                                 });
                                 
                                 ((CheckBox)check).selectedProperty().addListener(new ChangeListener<Boolean>() {
                                     @Override
                                     public void changed(ObservableValue<? extends Boolean> ov,
                                             Boolean t, Boolean t1) { 
                                         if(t1){
                                             getItem()[0] = "1";
                                         }else{
                                             getItem()[0] = "0";
                                         }
                                     }
                                 });                                 
                             }
                         }
                     });
                    } 

                    @Override
                    public void updateItem(String[] t, boolean bln) {
                        super.updateItem(t, bln);
                        if(bln){
                            setText("");
                        }else{
                            setText(getItem()[1]);
                        }
                    }                                       
                };
            }
        });
        
        selectAllCheckbox.setSelected(false);
        listview.setOrientation(Orientation.HORIZONTAL);
        listview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
    }
    
    Stage dialog;
    public void show(Object model){
        if(model == null){
            return;
        }
        if(dialog == null){
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setWidth(402);
            dialog.setHeight(402);
            dialog.initOwner(getStage());
            dialog.initModality(Modality.APPLICATION_MODAL);           
            dialog.setScene(new Scene(getUI()));
            
        }
        background.setAdditionalData(model);
        Recycler.addWorkProc(background);
        dialog.showAndWait();
    }
    
    public ConnectionSession session;
    
    private WorkProc background = new WorkProc() {
        
        ArrayList<String[]> output = new ArrayList();

        @Override
        public void updateUI() { 
            listview.getItems().clear();
            listview.getItems().addAll(output);
            output.clear();
        }

        @Override
        public void run() {
            if (getAddData() instanceof Database) {
                try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_FULL_TABLES))){
                    while(rs.next()){
                        output.add(new String[]{"0",rs.getString(1)});
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();                    
                }
            } else if (getAddData() instanceof DBTable) {
                try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_COLUMNS))){
                    while(rs.next()){
                        output.add(new String[]{"0",rs.getString(1)});
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();                    
                }
            } else {
                try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(
                            QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_DATABASES))){
                    while(rs.next()){
                        output.add(new String[]{"0",rs.getString(1)});
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();                    
                }
            }
        }
    };
    
    public ArrayList<String> getItems() {
        ArrayList<String> returns = new ArrayList();
        for (String[] s : listview.getSelectionModel().getSelectedItems()) {
            returns.add(s[1]);
        }
        return returns;
    }
}
