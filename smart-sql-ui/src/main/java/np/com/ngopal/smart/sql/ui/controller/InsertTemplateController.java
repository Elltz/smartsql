/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.Flags;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.queryutils.QueryAreaWrapper;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class InsertTemplateController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button loadTemplateButton;

    @FXML
    private Button insertTemplateButton;

    @FXML
    private Button exitButton;
    
    @FXML
    private TextArea customQueryTextPreviewer;
    
    @FXML
    private ListView<String[]> customQueryListview;

    private QueryAreaWrapper textArea;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadDefaultQueriesFromCache();
        EventHandler<ActionEvent> generalActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (event.getSource() == loadTemplateButton) {
                    loadTemplateFunction();
                } else if (event.getSource() == exitButton) {
                    getUI().getScene().getWindow().hide();
                } else if (event.getSource() == insertTemplateButton) {
                    insertTemplateFromDefaultsFunction();
                    exitButton.fire();//bad code - idk why i used this 
                }
            }
        };

        loadTemplateButton.setOnAction(generalActionHandler);
        exitButton.setOnAction(generalActionHandler);
        insertTemplateButton.setOnAction(generalActionHandler);
        
        customQueryListview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String[]>() {
            @Override
            public void changed(ObservableValue<? extends String[]> observable,
                    String[] oldValue, String[] newValue) { 
                if(newValue != null){
                    customQueryTextPreviewer.setText(newValue[1]);
                }
            }
        });
        customQueryListview.setCellFactory(new Callback<ListView<String[]>, ListCell<String[]>>() {
            @Override
            public ListCell<String[]> call(ListView<String[]> param) {
                return new ListCell<String[]>(){
                    {
                        setMaxHeight(30);                        
                        setTextFill(Color.BLACK);
                    }
                    
                    @Override
                    protected void updateItem(String[] item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? "" : item[0]);
                        setStyle("-fx-background-color: whitesmoke;");
                    }

                    @Override
                    public void updateSelected(boolean selected) {
                        super.updateSelected(selected);
                        setStyle("-fx-background-color: " + (selected ? "-fx-light-blue-color;" : "whitesmoke;"));
                    }
                };
            }
        });
        
    }
    
    private void insertTemplateFromDefaultsFunction(){
        //insertToCodeArea(customQueryListview.getSelectionModel().getSelectedItem()[1]);
        insertToCodeArea(customQueryTextPreviewer.getText().trim());
    }

    private void loadTemplateFunction() {
        String fileText = readFromUserFile();
        customQueryTextPreviewer.setText(fileText);
        customQueryListview.getSelectionModel().select(-1);
    }
    
    private final void insertToCodeArea(String text) {
        if (textArea != null && text != null) {
            textArea.insertText(textArea.getCaretPosition(CARET_POSITION_FROM_START), text);
        }
    }
    
    public void insertQueryCodeArea(QueryAreaWrapper textArea) {
        this.textArea = textArea;
    }

    private String readFromUserFile() {
        FileChooser fc = new FileChooser();
        File userFile = fc.showOpenDialog(getStage());
        if (userFile != null) {
            try {
                byte[] data = Files.readAllBytes(userFile.toPath());
                return new String(data, "UTF-8");
            } catch (IOException ex) {
                DialogHelper.showError(getController(), "Error reading user template file", ex.getMessage(), ex);
            }
        }

        return null;
    }

    private void loadDefaultQueriesFromCache(){
        Recycler.addWorkProc(new WorkProc() {
            
            private Pattern PATTERN = Pattern.compile(".+\\.sql");
            
            ArrayList<String[]> defaultQuereis = new ArrayList(100);
            
            @Override
            public void updateUI() {
                customQueryListview.getItems().addAll(defaultQuereis);
            }

            @Override
            public void run() { 
                try {
                    //we load from resources
                    // THIS IS NOT WORKING with production - only from IDE
//                    System.out.println(new File(getClass().getResource("/np/com/ngopal/smart/sql/ui/querytemplates/").toURI()).getPath());
//                    for (File templates : new File(getClass().getResource("/np/com/ngopal/smart/sql/ui/querytemplates/").toURI()).listFiles()) {
//                        defaultQuereis.add(new String[] { templates.getName().substring(0, templates.getName().length()-4),
//                            new String(Files.readAllBytes(templates.toPath())) });
//                    }
                    Set<String> resources = new Reflections("np.com.ngopal.smart.sql.ui.querytemplates", new ResourcesScanner()).getResources(PATTERN);
                    for (String resource: resources) {
                        
                        URI uri = ClassLoader.getSystemResource(resource).toURI();
                        
                        String uriStr = URLDecoder.decode(uri.toString(), "UTF-8");
                          
                        String[] array = uriStr.split("!");
                        if (array.length == 2) {
                        
                            FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), new HashMap<>());

                            Path path = fs.getPath(array[1]);

                            defaultQuereis.add(new String[] { 
                                resource.substring(resource.lastIndexOf("/") + 1, resource.length() - 4),
                                new String(Files.readAllBytes(path)) 
                            });

                            fs.close();
                        } else {
                            // for IDE running        
                            String file = array[0];
                            String name = file.substring(file.lastIndexOf("/") + 1);
                            file = file.replace("file:/", "");
                            
                            Path rootPath = Paths.get(file);
                            defaultQuereis.add(new String[] { name.substring(0, name.length() - 4), new String(Files.readAllBytes(rootPath)) });
                           
                        }
                    }

                } catch (Throwable e) {
                    log.error("Error", e);
                }
                callUpdate = !defaultQuereis.isEmpty();
            }
        });
    }
    
}
