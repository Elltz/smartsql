package np.com.ngopal.smart.sql.ui.controller;

import com.itextpdf.text.DocumentException;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.ComboBoxAutoFilterListener;
import np.com.ngopal.smart.sql.core.modules.utils.UsedFilePathsUtil;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ExportTableAsXlsController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private ImageView titleImageView;
    
    @FXML
    private TitledPane xlsOptions;
    @FXML
    private TextField xlsColumnSize;
    @FXML
    private TextField xlsDecimalPlaces;
    
    
    @FXML
    private TitledPane charsetOptions;
    @FXML
    private ComboBox<String> charsetCombobox;
    
    
    @FXML
    private ComboBox<String> exportPathField;    
   
    
    @FXML
    private ListView<String> columnsListView;
    
      

    private ObservableList<String> columnsData;
    private ObservableList<ObservableList> rowsData;
    

    public boolean response;
    
    private ComboBoxAutoFilterListener<String> autoFilterListener;

    public void setResponse(boolean response) {
        this.response = response;
    }

    public boolean getResponse() {
        return response;
    }

    
    @FXML
    void columnsSelectAll(ActionEvent event) {
        columnsListView.getSelectionModel().selectAll();
    }
    
    @FXML
    void columnsDeselectAll(ActionEvent event) {
        columnsListView.getSelectionModel().clearSelection();
    }

    @FXML
    void chooseExportFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(mainUI.getScene().getWindow());
        if (file != null) {
            exportPathField.getEditor().setText(file.getAbsolutePath());
        }
    }

    public void init(DBService service, ObservableList<ObservableList> rowsData, ObservableList<String> columnsData) throws SQLException {
        this.rowsData = rowsData;
        this.columnsData = columnsData;
        
        charsetCombobox.setItems(FXCollections.observableArrayList(service.getCharSet()));
        charsetCombobox.getSelectionModel().select("utf8");
        if (columnsData != null) {
            columnsListView.setItems(columnsData);
        }
        columnsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        columnsListView.getSelectionModel().selectAll();
        
        exportPathField.getItems().setAll(UsedFilePathsUtil.loadFilePaths(UsedFilePathsUtil.TYPE__EXPORT_AS_XLS_TABLE));
        exportPathField.getSelectionModel().selectLast();
        
        autoFilterListener = new ComboBoxAutoFilterListener<>(exportPathField, exportPathField.getItems());
        exportPathField.getEditor().textProperty().addListener(autoFilterListener);
        
    }

    @FXML
    void export(ActionEvent event) throws IOException, DocumentException {

        String filename = exportPathField.getEditor().getText();
        if (filename == null || filename.equals("")) {
            DialogHelper.showError(getController(), "Error", "Please Specify filename", null);
            return;
        }
        
        if (columnsListView.getSelectionModel().getSelectedItems().isEmpty()) {
            DialogHelper.showError(getController(), "Error", "Please select at least one column", null);
            return;
        }
        
        if (new File(filename).exists()) {
            if (DialogHelper.showConfirm(getController(), "File alreay exist", "File already exist, do you want to replace it?", getStage(), false) != DialogResponce.OK_YES) {
                return;
            }
        }
        
        if (!exportPathField.getItems().contains(filename)) {
            
            autoFilterListener.addSourcePath(filename);
            
            UsedFilePathsUtil.addFilePaths(UsedFilePathsUtil.TYPE__EXPORT_AS_XLS_TABLE, filename);
            exportPathField.getItems().add(filename);
        }
        
        ExportUtils.ExportStatusCallback exportCallback = new ExportUtils.ExportStatusCallback() {
            @Override
            public void error(String errorMessage, Throwable th) {
                DialogHelper.showError(getController(), "Error", errorMessage, th);
            }

            @Override
            public void success() {
                DialogResponce resp = DialogHelper.showConfirm(getController(), "Information",
                    "Data exported Successfully, Would you like to open file?");
                
                if (resp == DialogResponce.OK_YES) {
                    try {
                        Desktop.getDesktop().open(new File(filename));
                    } catch (IOException ex) {
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", "There is an error while opening exported file", ex);
                        });
                    }
                }
            }
        };

        
        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
        for (String c: columnsListView.getItems()) {
            columns.put(c, columnsListView.getSelectionModel().getSelectedItems().contains(c));
        }

        Integer maxTextSizeLimit = null;
        try {
            maxTextSizeLimit = Integer.valueOf(xlsColumnSize.getText());
        } catch (NumberFormatException ex) {
        }

        Integer decimalPlaces = null;
        try {
            decimalPlaces = Integer.valueOf(xlsDecimalPlaces.getText());
        } catch (NumberFormatException ex) {
        }

        ExportUtils.exportXLS(filename, rowsData, columns, maxTextSizeLimit, decimalPlaces, charsetCombobox.getSelectionModel().getSelectedItem(), exportCallback);
    }

    @FXML
    void cancel(ActionEvent event) {
        Stage stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        titleImageView.setImage(SmartImageProvider.getInstance().getXls_image());
    }

}
