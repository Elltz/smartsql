package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.model.Favourites;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Pair;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.utils.FavoritesUtils;
import np.com.ngopal.smart.sql.core.modules.utils.Flags;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.queryutils.QueryAreaWrapper;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;


/**
 *
 * @author Terah laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class AddFavoriteController extends BaseController implements Initializable {

        
    @FXML
    private AnchorPane mainUI;

    @FXML
    private TextField nameTextfield;
   
    @FXML
    private TreeView<Favourites> favoritesTree;
    
    private TreeItem<Favourites> root;

    @Inject
    private FavouritesService favServ;
    
    private QueryTabModule querymodule;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Favourites f = new Favourites();
        f.setName(FavoritesUtils.FAVORITES_ROOT_NAME);
        f.setFolder(true);

        root = new TreeItem<>(f);
        favoritesTree.setRoot(root);
        root.setExpanded(true);
        root.setGraphic(new ImageView(FavoritesUtils.IMAGE__FAVORITES));
        
        querymodule = (QueryTabModule) MainUI.getMainController().getModuleByClassName(QueryTabModule.class.getName());
        
        FavoritesUtils.refreshTree(favServ, root);
    }

    @FXML
    public void okAction(ActionEvent event) {
        String res = nameTextfield.getText();
        
        if (res == null || res.isEmpty()) {
            DialogHelper.showError(getController(), "Error", "Please enter a reference name for the query", null, dialog);
            return;
        }
        
        Favourites favorite;
        Favourites parent = favoritesTree.getSelectionModel().getSelectedItem().getValue();
        if (parent.isFolder()) {
            
            Favourites exist = favServ.find(parent.getId(), res, false);
            if (exist != null) {
                
                DialogResponce responce = DialogHelper.showConfirm(getController(), 
                        "Add Favorite", "The name you have entered for the shortcut already exist in Favorites menu.\nWould you like to overwrite it?", dialog, false);
                if (responce == DialogResponce.OK_YES) {
                    favorite = exist;
                    
                } else {
                    favorite = null;
                }                
            } else {
                
                favorite = new Favourites();
                favorite.setName(res);
                if (!parent.equals(root.getValue())) {
                    favorite.setParent(parent.getId());
                }                            
            }
            
        } else {
            DialogResponce responce = DialogHelper.showConfirm(getController(), "Add Favorite", "The name you have entered for the shortcut already exist in Favorites menu.\nWould you like to overwrite it?", dialog, false);
            if (responce == DialogResponce.OK_YES) {
                favorite = parent;
                
            } else {
                favorite = null;
            }
        }
        
        if (favorite != null) {
            QueryAreaWrapper area = querymodule.getSessionCodeArea();
            String query = area.getSelectedText();

            // find query by position
            if (query == null || query.isEmpty()) {
                Pair<Integer, String> result = new ScriptRunner(null, true).findQueryAtPosition(area.getText(), area.getCaretPosition(CARET_POSITION_FROM_START), false);
                query = result != null ? result.getValue() : null;
            }

            // if still empty - need show error
            if (query == null || query.isEmpty()) {
                DialogHelper.showError(getController(), "Error", "the Query editor is empty! Please enter some query/queries in the editor.", null, dialog);
                return;

            }

            favorite.setQuery(query);
            favServ.save(favorite);
            
            DialogHelper.showInfo(getController(), "Favorite added", "Favorite added: " + favorite.getName(), favorite.getQuery());
            
            dialog.close();
        }
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        dialog.close();
    }
    
    @FXML
    public void newFolderAction(ActionEvent event) {
        FavoritesUtils.createFolder(getController(), dialog, favServ, favoritesTree.getSelectionModel().getSelectedItem(), root);
    }


    private Stage dialog;
    public void init(Window win) {
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(470);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setResizable(false);
        dialog.setTitle("Add Favorite");
        dialog.setScene(new Scene(getUI()));

        Platform.runLater(() -> {
            nameTextfield.requestFocus();
            favoritesTree.getSelectionModel().select(root);
        });

        dialog.showAndWait();
    }
}
