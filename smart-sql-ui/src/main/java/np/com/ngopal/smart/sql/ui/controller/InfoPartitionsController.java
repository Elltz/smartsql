package np.com.ngopal.smart.sql.ui.controller;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.InformationModule;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBElement;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController.DatabaseChildrenType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.html.HTMLButtonElement;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class InfoPartitionsController extends BaseController implements Initializable{
        
    private final Image hostImage = SmartImageProvider.getInstance().getServer_image();
    private final Image databaseImage = SmartImageProvider.getInstance().getDatabase_image();
    private final Image tableImage = SmartImageProvider.getInstance().getTable_image();
    private final Image viewImage = SmartImageProvider.getInstance().getView_image();
    private final Image functionImage = SmartImageProvider.getInstance().getFunction_image();
    private final Image triggerImage = SmartImageProvider.getInstance().getTrigger_image();
    private final Image storedProcedureImage = SmartImageProvider.getInstance().getStoredProc_image();
    private final Image eventImage = SmartImageProvider.getInstance().getEvent_image();
    
    @FXML
    private AnchorPane mainUI;
        
    @FXML
    private Button refreshButton;
    
    @FXML
    private Label selectedItemName;
    
    @FXML
    private RadioButton tableViewFormat;
    
    @FXML
    private RadioButton textViewFormat;
    
    @FXML
    private TextArea textDetailed;
    
    @FXML
    private WebView webView;
    
    @FXML
    private BorderPane htmlContainer;
    
    @FXML
    private VBox titleVBox;
    
    private DBService service;    
        
    ConnectionSession session;
  
    private Object previousItem = null;
    private String databaseName;

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }

    @Override
    public void requestFocus() {
        Platform.runLater(() -> {
           if (tableViewFormat.isSelected()) {
                webView.requestFocus();
            } else {
                textDetailed.requestFocus();
            } 
        });
    }
    
    
    
    private volatile boolean loadDataRunned = false;
    private volatile boolean loadDataStop = false;
    
    
    private StringBuilder sb = new StringBuilder();
    private ArrayList<Node> children = new ArrayList();
    private Label loaderMsg = new Label(); 
    
    public void init(Object obj, String databaseName) {
        loadEntity(obj, databaseName);
    }
            
    
    private synchronized boolean checkStopLoad() {
        if (loadDataStop) {
            return true;
        }
        return false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) { 
        session = getController().getSelectedConnectionSession().get();
        service = session.getService();
        
        ToggleGroup tg = new ToggleGroup();
        tableViewFormat.setToggleGroup(tg);
        textViewFormat.setToggleGroup(tg);
        tableViewFormat.setSelected(true);
        tg.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) -> {
            if(t1 == tableViewFormat){
                htmlContainer.toFront();
            }else if(t1 == textViewFormat){
                textDetailed.toFront();
            }
        });
        refreshButton.setOnAction((ActionEvent t) -> {
            refresh();
        });
        textDetailed.setEditable(false);
        textDetailed.setFont(Font.font("Monospaced"));
        
        ProgressIndicator pi = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
        pi.setMaxSize(30, 30);  
        loaderMsg.setGraphic(pi);
    }
      
    
        
    private void showTablesInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("tablesView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Tables
        List<Map<String, String>> textTablesRows = new ArrayList<>();
        List<String[]> tablesRows = createTables(sb, databaseName, textTablesRows);
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateTablesInfo(newValue, tablesRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateTablesInfo(doc, tablesRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Table Information*/", sb);
        sb.append("\n");  
        
        if (checkStopLoad()) {
            return;
        }
        
        String[] h0 = null;
        List<String[]> v0 = new ArrayList<>();
        for (Map<String, String> map: textTablesRows) {
            if (h0 == null) {
                h0 = map.keySet().toArray(new String[]{});
            }
            v0.add(map.values().toArray(new String[]{}));
        }
        
        writeTable(h0, v0, sb);
    }
    
    private void showViewsInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("viewsView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Views
        List<Map<String, String>> textViewsRows = new ArrayList<>();                
        List<String[]> viewsRows = createViews(sb, databaseName, textViewsRows);
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateViewsInfo(newValue, viewsRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateViewsInfo(doc, viewsRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*View Information*/", sb);
        sb.append("\n"); 
        
        String[] h1 = null;
        List<String[]> v1 = new ArrayList<>();
        for (Map<String, String> map: textViewsRows) {
            if (h1 == null) {
                h1 = map.keySet().toArray(new String[]{});
            }
            v1.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h1, v1, sb);
    }
    
    private void showProceduresInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("proceduresView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Procedures
        List<Map<String, String>> textProsRows = new ArrayList<>();                
        List<String[]> proceduresRows = createProcedures(sb, databaseName, textProsRows);
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateProceduresInfo(newValue, proceduresRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateProceduresInfo(doc, proceduresRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Procedure Information*/", sb);
        sb.append("\n"); 
        
        if (checkStopLoad()) {
            return;
        }
        
        String[] h2 = null;
        List<String[]> v2 = new ArrayList<>();
        for (Map<String, String> map: textProsRows) {
            if (h2 == null) {
                h2 = map.keySet().toArray(new String[]{});
            }
            v2.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h2, v2, sb);
    }
    
    private void showFunctionsInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("functionsView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Functions
        List<Map<String, String>> textFucntionsRows = new ArrayList<>();                
        List<String[]> functionsRows = createFunctions(sb, databaseName, textFucntionsRows);
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateFunctionsInfo(newValue, functionsRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateFunctionsInfo(doc, functionsRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Function Information*/", sb);
        sb.append("\n"); 
        
        if (checkStopLoad()) {
            return;
        }
        
        String[] h3 = null;
        List<String[]> v3 = new ArrayList<>();
        for (Map<String, String> map: textFucntionsRows) {
            if (h3 == null) {
                h3 = map.keySet().toArray(new String[]{});
            }
            v3.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h3, v3, sb);
    }
    
    private void showTriggersInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("triggersView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Triggers
        List<Map<String, String>> textTriggersRows = new ArrayList<>();                
        List<String[]> triggersRows = createTriggers(sb, databaseName, textTriggersRows);
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateTriggersInfo(newValue, triggersRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateTriggersInfo(doc, triggersRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Trigger Information*/", sb);
        sb.append("\n"); 
        
        String[] h4 = null;
        List<String[]> v4 = new ArrayList<>();
        for (Map<String, String> map: textTriggersRows) {
            if (h4 == null) {
                h4 = map.keySet().toArray(new String[]{});
            }
            v4.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h4, v4, sb);
    }
    
    private void showEventsInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("eventsView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Events
        List<Map<String, String>> textEventsRows = new ArrayList<>();                
        List<String[]> eventsRows = createEvents(sb, databaseName, textEventsRows);
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateEventsInfo(newValue, eventsRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateEventsInfo(doc, eventsRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Event Information*/", sb);
        sb.append("\n"); 
        
        String[] h5 = null;
        List<String[]> v5 = new ArrayList<>();
        for (Map<String, String> map: textEventsRows) {
            if (h5 == null) {
                h5 = map.keySet().toArray(new String[]{});
            }
            v5.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h5, v5, sb);
    }
    
    private void showTableInfo(StringBuilder sb, String tableName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("tableView.html").toExternalForm());
            selectedItemName.setText("Table: " + tableName);
            selectedItemName.setGraphic(new ImageView(tableImage));
        });
                  
        if (checkStopLoad()) {
            return;
        }
        
        // Columns
        List<Map<String, String>> textColumnsRows = new ArrayList<>();
        List<String[]> columnsRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.getColumnInformationResultSet(databaseName, tableName)) {
                        
            while (rs.next()) {
                String[] row = new String[4];
                
                boolean isNull = false;
                try {
                    isNull = rs.getBoolean("Null");
                } catch (Throwable th) {
                    try {
                        isNull = "YES".equalsIgnoreCase(rs.getString("Null"));
                    } catch (Throwable th0) {
                    }
                }
                
                row[0] = "";
                row[1] = rs.getString("Field");
                row[2] = rs.getString("Type") + " " + (isNull ? "NULL" : "NOT NULL");
                row[3] = rs.getString("Comment");
                
                if ("PRI".equalsIgnoreCase(rs.getString("Key"))) {
                    String img = "";
                    try {
                        img = getClass().getResource("/np/com/ngopal/smart/sql/ui/images/connection/primaryKey.png").toURI().toString();
                    } catch (Throwable th) {
                        log.error("Error getting primary key image", th);
                    }
                    row[0] = "img::" + img;
                }
                
                columnsRows.add(row);       
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textColumnsRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting columns info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        // Indexes
        List<Map<String, String>> textIndexsRows = new ArrayList<>();
        List<String[]> indexesRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.getKeys(databaseName, tableName)) {
                   
            Map<String, String[]> map = new LinkedHashMap<>();
            while (rs.next()) {
                
                String keyName = rs.getString("Key_Name");
                if (map.containsKey(keyName)) {
                    String[] row = map.get(keyName);
                    
                    row[2] = row[2] + ", " + rs.getString("Column_name");
                } else {
                        
                    String[] row = new String[4];
                    row[0] = "";
                    row[1] = rs.getString("Key_Name");
                    row[2] = rs.getString("Column_name");
                    row[3] = "";
                    
                    int nonUnique = rs.getInt("Non_unique");                
                    String type = rs.getString("Index_Type");                

                    if ("FULLTEXT".equalsIgnoreCase(type)) {
                        row[3] = "Fulltext";

                    } else if ("UNIQUE".equalsIgnoreCase(type)) {
                        row[3] = "Unique";

                    } else if (nonUnique == 0) {
                        row[3] = "Unique";
                    }
                    
                    if ("PRIMARY".equalsIgnoreCase(row[1])) {
                        String img = "";
                        try {
                            img = getClass().getResource("/np/com/ngopal/smart/sql/ui/images/connection/primaryKey.png").toURI().toString();
                        } catch (Throwable th) {
                            log.error("Error getting primary key image", th);
                        }
                        row[0] = "img::" + img;
                    }
                    
                    map.put(keyName, row);
                }
                
                Map<String, String> map0 = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map0.put(name, rs.getString(name));
                }
                
                textIndexsRows.add(map0);
            }
            
            indexesRows.addAll(map.values());
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting columns info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        // DDL 
        String ddl = "";
        try (ResultSet rs = (ResultSet) service.execute(service.getShowCreateTableSQL(databaseName, tableName))) {
            if (rs.next()) {
                ddl = rs.getString("Create Table");
            }                
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting table DDL info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        String tableDDL = ddl;
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateTableInfo(newValue, tableName, columnsRows, indexesRows, tableDDL);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateTableInfo(doc, tableName, columnsRows, indexesRows, tableDDL);
            }
        });
        
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Table: " + tableName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Column Information*/", sb);
        sb.append("\n"); 
        
        if (checkStopLoad()) {
            return;
        }
        
        String[] h0 = null;
        List<String[]> v0 = new ArrayList<>();
        for (Map<String, String> map: textColumnsRows) {
            if (h0 == null) {
                h0 = map.keySet().toArray(new String[]{});
            }
            v0.add(map.values().toArray(new String[]{}));
        }
        
        writeTable(h0, v0, sb);
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Index Information*/", sb);
        sb.append("\n"); 
        
        String[] h1 = null;
        List<String[]> v1 = new ArrayList<>();
        for (Map<String, String> map: textIndexsRows) {
            if (h1 == null) {
                h1 = map.keySet().toArray(new String[]{});
            }
            v1.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h1, v1, sb);
        
        writeWithUnderline("/*DDL Information*/", sb);
        sb.append("\n"); 
        
        sb.append(ddl); 
    }
    
    
    private void showViewInfo(StringBuilder sb, String viewName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("viewView.html").toExternalForm());
            selectedItemName.setText("View: " + viewName);
            selectedItemName.setGraphic(new ImageView(viewImage));            
        });
          
        if (checkStopLoad()) {
            return;
        }
        
        // Columns
        List<Map<String, String>> textColumnsRows = new ArrayList<>();
        List<String[]> columnsRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.getColumnInformationResultSet(databaseName, viewName)) {
                        
            while (rs.next()) {
                String[] row = new String[4];
                
                row[0] = rs.getString("Field");
                row[1] = rs.getString("Type");
                row[2] = rs.getBoolean("Null") ? "YES" : "NO";
                row[3] = rs.getString("Comment");
                
                columnsRows.add(row);     
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textColumnsRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting columns info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        // DDL 
        String ddl = "";
        try (ResultSet rs = (ResultSet) service.execute(service.getShowCreateTableSQL(databaseName, viewName))) {
            if (rs.next()) {
                ddl = rs.getString("Create View");
            }                
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting view DDL info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        String viewDDL = ddl;
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateViewInfo(newValue, columnsRows, viewDDL);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateViewInfo(doc, columnsRows, viewDDL);
            }
        });
        
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*View: " + viewName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Column Information*/", sb);
        sb.append("\n"); 
        
        if (checkStopLoad()) {
            return;
        }
        
        String[] h0 = null;
        List<String[]> v0 = new ArrayList<>();
        for (Map<String, String> map: textColumnsRows) {
            if (h0 == null) {
                h0 = map.keySet().toArray(new String[]{});
            }
            v0.add(map.values().toArray(new String[]{}));
        }
       
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h0, v0, sb);
       
        
        writeWithUnderline("/*DDL Information*/", sb);
        sb.append("\n"); 
        
        sb.append(ddl); 
    }
    
    
    private void showProcedureInfo(StringBuilder sb, String name) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("procedureView.html").toExternalForm());
            selectedItemName.setText("Stored Proc: " + name);
            selectedItemName.setGraphic(new ImageView(storedProcedureImage));
        });
          
        if (checkStopLoad()) {
            return;
        }
        
        // DDL 
        String ddl = "";
        String sqlMode = "";
        try (ResultSet rs = (ResultSet) service.execute(service.getShowCreateStoredProcedureSQL(databaseName, name))) {
            if (rs.next()) {
                ddl = rs.getString("Create Procedure");
                sqlMode = rs.getString("sql_mode");
            }                
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting stored proc DDL info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        String procDDL = ddl;
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateProcedureInfo(newValue, procDDL);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateProcedureInfo(doc, procDDL);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Stored Proc: " + name + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("Create Procedure:", sb);
        sb.append("\n"); 
        sb.append(procDDL);
        sb.append("\n\n"); 
        
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("sql_mode:", sb);
        sb.append("\n"); 
        sb.append(sqlMode);
    }
    
    private void showFunctionInfo(StringBuilder sb, String name) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("functionView.html").toExternalForm());
            selectedItemName.setText("Function: " + name);
            selectedItemName.setGraphic(new ImageView(functionImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
          
        // DDL 
        String ddl = "";
        String sqlMode = "";
        try (ResultSet rs = (ResultSet) service.execute(service.getShowCreateFunctionSQL(databaseName, name))) {
            if (rs.next()) {
                ddl = rs.getString("Create Function");
                sqlMode = rs.getString("sql_mode");
            }                
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting function DDL info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        String functionDDL = ddl;
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateFunctionInfo(newValue, functionDDL);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateFunctionInfo(doc, functionDDL);
            }
        });
        
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Function: " + name + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("Create Function:", sb);
        sb.append("\n"); 
        sb.append(ddl);
        sb.append("\n\n"); 
        
        
        writeWithUnderline("sql_mode:", sb);
        sb.append("\n"); 
        sb.append(sqlMode);
    }
    
    
    private void showTriggerInfo(StringBuilder sb, String name) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("triggerView.html").toExternalForm());
            selectedItemName.setText("Trigger: " + name);
            selectedItemName.setGraphic(new ImageView(triggerImage));
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        // Columns
        String created = "";
        String[] columns = new String[4];  
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_TRIGGERS_BY_NAME), databaseName, name))){
                        
            if (rs.next()) {
                columns[0] = rs.getString("Table");
                columns[1] = rs.getString("Event");
                columns[2] = rs.getString("Timing");
                columns[3] = rs.getString("Statement");
                created = rs.getString("Created");
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting  trigger info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateTriggerInfo(newValue, columns);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateTriggerInfo(doc, columns);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Trigger: " + name + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("Statement:", sb);
        sb.append("\n"); 
        sb.append(columns[3]);
        sb.append("\n\n");
        
        writeWithUnderline("Table:", sb);
        sb.append("\n"); 
        sb.append(columns[0]);
        sb.append("\n\n");
        
        writeWithUnderline("Event:", sb);
        sb.append("\n"); 
        sb.append(columns[1]);
        sb.append("\n\n");
        
        writeWithUnderline("Timing:", sb);
        sb.append("\n"); 
        sb.append(columns[2]);
        sb.append("\n\n");
        
        writeWithUnderline("Created:", sb);
        sb.append("\n"); 
        sb.append(created);
    }
    
    private void showEventInfo(StringBuilder sb, String name) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("eventView.html").toExternalForm());
            selectedItemName.setText("Event: " + name);
            selectedItemName.setGraphic(new ImageView(eventImage));
        });
          
        if (checkStopLoad()) {
            return;
        }
        
        // DDL 
        String ddl = "";
        String sqlMode = "";
        String timeZone = "";
        String characterSetClient = "";
        String collationConnection = "";
        String databaseCollation = "";
        try (ResultSet rs = (ResultSet) service.execute(service.getShowCreateEventSQL(databaseName, name))) {
            if (rs.next()) {
                ddl = rs.getString("Create Event");
                sqlMode = rs.getString("sql_mode");
                timeZone = rs.getString("time_zone");
                characterSetClient = rs.getString("character_set_client");
                collationConnection = rs.getString("collation_connection");
                databaseCollation = rs.getString("Database Collation");
            }                
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting event DDL info", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        String eventDDL = ddl;
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateEventInfo(newValue, eventDDL);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateEventInfo(doc, eventDDL);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Trigger: " + name + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("Create Event:", sb);
        sb.append("\n"); 
        sb.append(ddl);
        sb.append("\n\n");
        
        writeWithUnderline("sql_mode:", sb);
        sb.append("\n"); 
        sb.append(sqlMode);
        sb.append("\n\n");
        
        writeWithUnderline("time_zone:", sb);
        sb.append("\n"); 
        sb.append(timeZone);
        sb.append("\n\n");
        
        writeWithUnderline("character_set_client:", sb);
        sb.append("\n"); 
        sb.append(characterSetClient);
        sb.append("\n\n");
        
        writeWithUnderline("collation_connection:", sb);
        sb.append("\n"); 
        sb.append(collationConnection);
        sb.append("\n\n");
        
        writeWithUnderline("Database Collation:", sb);
        sb.append("\n"); 
        sb.append(databaseCollation);
    }
    
    private void showDatabaseInfo(StringBuilder sb, String databaseName) {
        Platform.runLater(() -> {
            webView.getEngine().load(InformationModule.class.getResource("databaseView.html").toExternalForm());
            selectedItemName.setText("Database: " + databaseName);
            selectedItemName.setGraphic(new ImageView(databaseImage));
        });
          
        List<Map<String, String>> textTablesRows = new ArrayList<>();                
        List<String[]> tablesRows = createTables(sb, databaseName, textTablesRows);
        
        if (checkStopLoad()) {
            return;
        }

        List<Map<String, String>> textViewsRows = new ArrayList<>();                
        List<String[]> viewsRows = createViews(sb, databaseName, textViewsRows);
        
        if (checkStopLoad()) {
            return;
        }
        
        List<Map<String, String>> textProsRows = new ArrayList<>();                
        List<String[]> procsRows = createProcedures(sb, databaseName, textProsRows);
        
        if (checkStopLoad()) {
            return;
        }

        List<Map<String, String>> textFucntionsRows = new ArrayList<>();                
        List<String[]> functionsRows = createFunctions(sb, databaseName, textFucntionsRows);
                
        if (checkStopLoad()) {
            return;
        }
        
        List<Map<String, String>> textTriggersRows = new ArrayList<>();                
        List<String[]> triggersRows = createTriggers(sb, databaseName, textTriggersRows);
        
        if (checkStopLoad()) {
            return;
        }
        
        List<Map<String, String>> textEventsRows = new ArrayList<>();                
        List<String[]> eventsRows = createEvents(sb, databaseName, textEventsRows);
        
        if (checkStopLoad()) {
            return;
        }
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateDatabaseInfo(newValue, tablesRows, viewsRows, procsRows, functionsRows, triggersRows, eventsRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateDatabaseInfo(doc, tablesRows, viewsRows, procsRows, functionsRows, triggersRows, eventsRows);
            }
        });
        
        String ddl = "";
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_CREATE_DATABASE), databaseName))){
            if (rs.next()) {
                ddl = rs.getString(2);               
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting create database ddl", ex.getMessage(), ex);
        }
        
        writeWithUnderline("/*Database: " + databaseName + "*/", sb);
        sb.append("\n");
        
        writeWithUnderline("/*Table Information*/", sb);
        sb.append("\n");  
        
        String[] h0 = null;
        List<String[]> v0 = new ArrayList<>();
        for (Map<String, String> map: textTablesRows) {
            if (h0 == null) {
                h0 = map.keySet().toArray(new String[]{});
            }
            v0.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h0, v0, sb);
        
        
        writeWithUnderline("/*View Information*/", sb);
        sb.append("\n"); 
        
        String[] h1 = null;
        List<String[]> v1 = new ArrayList<>();
        for (Map<String, String> map: textViewsRows) {
            if (h1 == null) {
                h1 = map.keySet().toArray(new String[]{});
            }
            v1.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h1, v1, sb);
        
        writeWithUnderline("/*Procedure Information*/", sb);
        sb.append("\n"); 
        
        String[] h2 = null;
        List<String[]> v2 = new ArrayList<>();
        for (Map<String, String> map: textProsRows) {
            if (h2 == null) {
                h2 = map.keySet().toArray(new String[]{});
            }
            v2.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h2, v2, sb);
        
        writeWithUnderline("/*Function Information*/", sb);
        sb.append("\n"); 
        
        String[] h3 = null;
        List<String[]> v3 = new ArrayList<>();
        for (Map<String, String> map: textFucntionsRows) {
            if (h3 == null) {
                h3 = map.keySet().toArray(new String[]{});
            }
            v3.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h3, v3, sb);
        
        writeWithUnderline("/*Trigger Information*/", sb);
        sb.append("\n"); 
        
        String[] h4 = null;
        List<String[]> v4 = new ArrayList<>();
        for (Map<String, String> map: textTriggersRows) {
            if (h4 == null) {
                h4 = map.keySet().toArray(new String[]{});
            }
            v4.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h4, v4, sb);
        
        writeWithUnderline("/*Event Information*/", sb);
        sb.append("\n"); 
        
        String[] h5 = null;
        List<String[]> v5 = new ArrayList<>();
        for (Map<String, String> map: textEventsRows) {
            if (h5 == null) {
                h5 = map.keySet().toArray(new String[]{});
            }
            v5.add(map.values().toArray(new String[]{}));
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        writeTable(h5, v5, sb);
        
        writeWithUnderline("/*DDL Information For - " + databaseName + "*/", sb);
        sb.append("\n"); 
        
        sb.append(ddl);
    }
    
    
    private List<String[]> createTables(StringBuilder sb, String databaseName, List<Map<String, String>> textTablesRows) {
        // Tables
        List<String[]> tablesRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ALL_TABLES_STATUS), databaseName))){
                        
            while (rs.next()) {
                String[] row = new String[6];
                
                row[0] = rs.getString("Name");
                row[1] = rs.getString("Engine");
                row[2] = rs.getString("Rows");
                
                long dataSize = rs.getLong("Data_length");
                row[3] = NumberFormater.compactByteLength(dataSize);
                
                long indexSize = rs.getLong("Index_length");
                row[4] = NumberFormater.compactByteLength(indexSize);
                
                row[5] = NumberFormater.compactByteLength(dataSize + indexSize);
                
                tablesRows.add(row);

                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textTablesRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting tables info", ex.getMessage(), ex);
        }
        
        return tablesRows;
    }
    
    private List<String[]> createViews(StringBuilder sb, String databaseName, List<Map<String, String>> textViewsRows) {
        // Views
        List<String[]> viewsRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ALL_VIEWS_STATUS), databaseName))){
                        
            while (rs.next()) {
                String[] row = new String[4];
                
                row[0] = rs.getString("View_name");
                row[1] = rs.getString("Is_updatable");
                row[2] = rs.getString("Definer");                
                row[3] = rs.getString("Security_type");
                
                viewsRows.add(row);                
                
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textViewsRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting views info", ex.getMessage(), ex);
        }
        
        return viewsRows;
    }
    
    private List<String[]> createProcedures(StringBuilder sb, String databaseName, List<Map<String, String>> textProcsRows) {    
        // Procedures
        List<String[]> procsRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_FULL_STORED_PROCEDURES), databaseName))){
                        
            while (rs.next()) {
                String[] row = new String[4];
                
                row[0] = rs.getString("Name");
                row[1] = rs.getString("Definer");
                row[2] = rs.getString("Security_type");                
                row[3] = rs.getString("Comment");
                
                procsRows.add(row);   
                                
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textProcsRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting procs info", ex.getMessage(), ex);
        }
        
        return procsRows;
    }
    
    private List<String[]> createFunctions(StringBuilder sb, String databaseName, List<Map<String, String>> textFunctionsRows) {        
        // Functions
        List<String[]> functionsRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_FULL_FUNCTIONS), databaseName))){
                        
            while (rs.next()) {
                String[] row = new String[4];
                
                row[0] = rs.getString("Name");
                row[1] = rs.getString("Definer");
                row[2] = rs.getString("Security_type");                
                row[3] = rs.getString("Comment");
                
                functionsRows.add(row);   
                                
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textFunctionsRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting functions info", ex.getMessage(), ex);
        }
        
        return functionsRows;
    }
    
    private List<String[]> createTriggers(StringBuilder sb, String databaseName, List<Map<String, String>> textTriggersRows) {
        // Triggers
        List<String[]> triggersRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_TRIGGERS), databaseName))){
                        
            while (rs.next()) {
                String[] row = new String[6];
                
                row[0] = rs.getString("Trigger");
                row[1] = rs.getString("Event");
                row[2] = rs.getString("Table");                
                row[3] = rs.getString("Timing");
                row[4] = rs.getString("sql_mode");
                row[5] = rs.getString("Definer");
                
                triggersRows.add(row);             
                
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textTriggersRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting triggers info", ex.getMessage(), ex);
        }
        
        return triggersRows;
    }
    
    private List<String[]> createEvents(StringBuilder sb, String databaseName, List<Map<String, String>> textEventsRows) {        
        // Events
        List<String[]> eventsRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_FULL_EVENTS), databaseName))){
                        
            while (rs.next()) {
                String[] row = new String[9];
                
                row[0] = rs.getString("Event_name");
                row[1] = rs.getString("Definer");
                row[2] = rs.getString("Event_type");                
                row[3] = rs.getString("Execute_at");
                row[4] = rs.getString("Interval_value");
                row[5] = rs.getString("Interval_field");
                row[6] = rs.getString("Starts");
                row[7] = rs.getString("Ends");
                row[8] = rs.getString("Status");
                
                eventsRows.add(row);       
                
                
                Map<String, String> map = new LinkedHashMap<>();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String name = rs.getMetaData().getColumnLabel(i);
                    map.put(name, rs.getString(name));
                }
                
                textEventsRows.add(map);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting events info", ex.getMessage(), ex);
        }
        
        return eventsRows;
    }
    
    
    private void showConnectionInfo(StringBuilder sb, ConnectionParams connectionParams) {
        
        String version = "";
        String characterSet = "";
        String port = "";
        
        try (ResultSet rs  = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__MYSQL_VERSION))){
            if (rs.next()) {
                version = rs.getString(1);
                
                String vrs = version;
                Platform.runLater(() -> {
                    webView.getEngine().load(InformationModule.class.getResource("hostView.html").toExternalForm());
                    selectedItemName.setText("MySQL Server: " + vrs);
                    selectedItemName.setGraphic(new ImageView(hostImage));
                });
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting server version", ex.getMessage(), ex);
        }   
        
        if (checkStopLoad()) {
            return;
        }
        
        List<String[]> variablesRows = new ArrayList<>();        
        try (ResultSet rs  = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_VARIABLES))){
                        
            while (rs.next()) {                
                if (characterSet.isEmpty() && "character_set_server".equals(rs.getString(1))) {
                    characterSet = rs.getString(2);
                } else if (port.isEmpty() && "port".equals(rs.getString(1))) {
                    port = rs.getString(2);
                }
                
                variablesRows.add(new String[]{rs.getString(1), rs.getString(2)});                
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting server version", ex.getMessage(), ex);
        }
        
        if (checkStopLoad()) {
            return;
        }
                
        List<String[]> statusRows = new ArrayList<>();
        try (ResultSet rs  = (ResultSet) service.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_STATUS))){
            
            while (rs.next()) {                
                statusRows.add(new String[]{rs.getString(1), rs.getString(2)});                
            }            
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error getting server version", ex.getMessage(), ex);
        }
                
        if (checkStopLoad()) {
            return;
        }
        
        Platform.runLater(() -> {
            Document doc = webView.getEngine().getDocument();
            if (doc == null) {
                ChangeListener<Document> listener = new ChangeListener<Document>() {
                    @Override
                    public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                        updateHostInfo(newValue, variablesRows, statusRows);
                        webView.getEngine().documentProperty().removeListener(this);
                    }
                };
                webView.getEngine().documentProperty().addListener(listener);
            } else {
                updateHostInfo(doc, variablesRows, statusRows);
            }
        });
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*Server Information For - " + connectionParams.toString() + "*/", sb);
        sb.append("\n");                
        sb.append("MySQL Version                 : ").append(version).append("\n");
        sb.append("Host                          : ").append(connectionParams.getAddress()).append("\n");
        sb.append("User                          : ").append(connectionParams.getUsername()).append("\n");
        sb.append("Port                          : ").append(port).append("\n");
        sb.append("Server Default Charset        : ").append(characterSet).append("\n");
        
        sb.append("\n");
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*SHOW VARIABLES Output*/", sb);
        sb.append("\n");
        
        writeTable(new String[]{"Variable_name", "Value"}, variablesRows, sb);
        
        if (checkStopLoad()) {
            return;
        }
        
        writeWithUnderline("/*SHOW STATUS Output*/", sb);
        sb.append("\n");
        
        writeTable(new String[]{"Variable_name", "Value"}, statusRows, sb);
    }
    
    private void updateHostInfo(Document doc, List<String[]> variablesRows, List<String[]> statusRows) {
        Element table = doc.getElementById("showVariablesTable");
        boolean odd = false;
        
        if (table != null) {
            for (String[] row: variablesRows) {
                addRow(row, doc, table, "variables-", 0, odd, null);
                odd = !odd;
            }
        }
        
        table = doc.getElementById("showStatusTable");
        if (table != null) {
            odd = false;
            for (String[] row: statusRows) {
                addRow(row, doc, table, "status-", 0, odd, null);
                odd = !odd;
            }
        }
    }
    
    
    private void updateTableInfo(Document doc, String table, List<String[]> columnsRows, List<String[]> indexesRows, String tableDDL) {
        
        Element title = doc.getElementById("columnsTitle");
        title.setTextContent("Columns (" + columnsRows.size() + ")");

        Element element = doc.getElementById("showColumns");
        boolean odd = false;
        for (String[] row: columnsRows) {
            addRow(row, doc, element, "columns-", 1, odd, null);
            odd = !odd;
        }
        
        title = doc.getElementById("indexesTitle");
        title.setTextContent("Indexes (" + indexesRows.size() + ")");

        List<String> indexesNames = new ArrayList<>();
        
        element = doc.getElementById("showIndexes");
        odd = false;
        for (String[] row: indexesRows) {
            indexesNames.add(row[1]);
            
            addRow(row, doc, element, "indexes-", 1, odd, null);
            odd = !odd;
        }
        
        element = doc.getElementById("showDDL");
        
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: " + (odd ? "#e5e5e5;" : "white;"));
        
        Element cell = doc.createElement("td");
        Element pre = doc.createElement("pre");
        
        pre.appendChild(doc.createTextNode(tableDDL));
        cell.appendChild(pre);
        
        element.appendChild(cell);
        
        EventTarget redundantRef = (EventTarget)doc.getElementById("redundantRef");
        redundantRef.addEventListener("click", new EventListener() {
            @Override
            public void handleEvent(org.w3c.dom.events.Event evt) {
                URLUtils.openUrl("http://www.smartmysql.com/smartmysql-documentation/");
            }
        }, false);
        
        HTMLButtonElement redundantButton = (HTMLButtonElement)doc.getElementById("redundantButton");
        ((EventTarget) redundantButton).addEventListener("click", new EventListener() {
            
            boolean redundant = false; 
            private List<String> alterIds = new ArrayList<>();
            
            @Override
            public void handleEvent(org.w3c.dom.events.Event evt) {
                redundant = !redundant;
                redundantButton.setTextContent(redundant ? "Hide Redundant Indexes Info" : "Find Redundant Indexes");
                
                Element isRedundantHeader = doc.getElementById("isRedundantHeader");
                Element redundantOfHeader = doc.getElementById("redundantOfHeader");
                
                Element dropRedundantTitle = doc.getElementById("dropRedundantTitle");
                Element showDropRedundant = doc.getElementById("showDropRedundant");
                
                Element dropRedundantEmpty = doc.getElementById("dropRedundantEmpty");
                        
                if (redundant) {
                    
                    isRedundantHeader.setAttribute("style", "");
                    redundantOfHeader.setAttribute("style", "");
                    
                    try ( ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__FIND_REDUNDANT_INDEXES_BY_TABLE), databaseName, table, databaseName, table))) {
                        if (rs != null) {
                            Map<String, String> redundantData = new HashMap<String, String>();
                            while (rs.next()) {
                                String redundantIndex = rs.getString(2);
                                String index = rs.getString(3);
                                
                                if (redundantIndex.startsWith("`") && redundantIndex.endsWith("`")) {
                                    redundantIndex = redundantIndex.substring(1, redundantIndex.length() - 1);
                                }
                                
                                if (index.startsWith("`") && index.endsWith("`")) {
                                    index = index.substring(1, index.length() - 1);
                                }
                                
                                redundantData.put(redundantIndex, index);
                            }
                            
                            for (String name: indexesNames) {
                                Element trElem = doc.getElementById("indexes-" + name + "-row");

                                Element cell0 = doc.createElement("td");
                                cell0.setAttribute("id", "indexes-" + name + "4-cell");
                                Element cell1 = doc.createElement("td");
                                cell1.setAttribute("id", "indexes-" + name + "5-cell");

                                String value0 = "No";
                                String value1 = "";
                                if (redundantData.containsKey(name)) {
                                    value0 = "Yes";
                                    value1 = redundantData.get(name);
                                    // only for redundant
                                    trElem.setAttribute("style", "background-color: #fac8a5");
                                } else {
                                    cell0.setAttribute("style", "background-color: #d6e7ff");
                                    cell1.setAttribute("style", "background-color: #d6e7ff");
                                }
                                
                                cell0.appendChild(doc.createTextNode(value0));
                                trElem.appendChild(cell0);
                                
                                cell1.appendChild(doc.createTextNode(value1));
                                trElem.appendChild(cell1);
                            }
                            
                            if (!redundantData.isEmpty()) {
                                dropRedundantTitle.setAttribute("style", "text-align: left;");
                                showDropRedundant.setAttribute("style", "");
                                dropRedundantEmpty.setAttribute("style", "");

                                Element showDropRedundantTable = doc.getElementById("showDropRedundantTable");
                                for (String name: redundantData.keySet()) {
                                    String id = "alter-" + name;                                
                                    alterIds.add(id + "-row");

                                    addRow(new String[] {String.format("ALTER TABLE `%s`.`%s` DROP INDEX `%s`", databaseName, table, name)}, doc, showDropRedundantTable, id, -1, false, "#d6e7ff");
                                }
                            }
                        }
                    } catch (Throwable th) {
                        log.error("Error", th);
                    }
                    
                    
                } else {
                    
                    isRedundantHeader.setAttribute("style", "display: none;");
                    redundantOfHeader.setAttribute("style", "display: none;");
                    
                    dropRedundantTitle.setAttribute("style", "text-align: left; display: none;");
                    showDropRedundant.setAttribute("style", "display: none;");
                    dropRedundantEmpty.setAttribute("style", "display: none;");
                    
                    boolean odd = false;
                    for (String name: indexesNames) {
                        Element trElem = doc.getElementById("indexes-" + name + "-row");
                        trElem.setAttribute("style", "background-color: " + (odd ? "#e5e5e5;" : "white;"));

                        Element cell0 = doc.getElementById("indexes-" + name + "5-cell");
                        if (cell0 != null) {
                            trElem.removeChild(cell0);
                        }
                        
                        Element cell1 = doc.getElementById("indexes-" + name + "4-cell");
                        if (cell1 != null) {
                            trElem.removeChild(cell1);
                        }
                        
                        odd = !odd;
                    }
                    
                    Element showDropRedundantTable = doc.getElementById("showDropRedundantTable");
                    for (String id: alterIds) {
                        Element row = doc.getElementById(id);
                        showDropRedundantTable.removeChild(row);
                    }
                    
                    alterIds.clear();
                }
            }            
        }, false);
    }
    
    
    private void updateViewInfo(Document doc, List<String[]> columnsRows, String viewDDL) {
        
        Element element = doc.getElementById("showColumns");
        boolean odd = false;
        for (String[] row: columnsRows) {
            addRow(row, doc, element, "view-", 0, odd, null);
            odd = !odd;
        }
               
        element = doc.getElementById("showDDL");
        
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: " + (odd ? "#e5e5e5;" : "white;"));
        
        Element cell = doc.createElement("td");
        Element pre = doc.createElement("pre");
        
        pre.appendChild(doc.createTextNode(viewDDL));
        cell.appendChild(pre);
        
        element.appendChild(cell);
    }
    
    private void updateTriggerInfo(Document doc, String[] columnsRows) {
        
        Element element = doc.getElementById("showColumns");
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: white;");
        
        {
            Element cell = doc.createElement("td");
            String value = columnsRows[0];
            cell.appendChild(doc.createTextNode(value != null ? value : "(NULL)"));            
            newRow.appendChild(cell);
        }
        
        {
            Element cell = doc.createElement("td");
            String value = columnsRows[1];
            cell.appendChild(doc.createTextNode(value != null ? value : "(NULL)"));            
            newRow.appendChild(cell);
        }
        
        {
            Element cell = doc.createElement("td");
            String value = columnsRows[2];
            cell.appendChild(doc.createTextNode(value != null ? value : "(NULL)"));            
            newRow.appendChild(cell);
        }
        
        {
            Element cell = doc.createElement("td");
            String value = columnsRows[0];
                        
            Element pre = doc.createElement("pre");
        
            pre.appendChild(doc.createTextNode(columnsRows[3]));
            cell.appendChild(pre);

            newRow.appendChild(cell);
        }      
        
        element.appendChild(newRow);
    }
    
    private void updateProcedureInfo(Document doc, String ddl) {
        
        Element element = doc.getElementById("showDDL");
        
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: white;");
        
        Element cell = doc.createElement("td");
        Element pre = doc.createElement("pre");
        
        pre.appendChild(doc.createTextNode(ddl));
        cell.appendChild(pre);
        
        element.appendChild(cell);
    }
    
    private void updateFunctionInfo(Document doc, String ddl) {
        
        Element element = doc.getElementById("showDDL");
        
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: white;");
        
        Element cell = doc.createElement("td");
        Element pre = doc.createElement("pre");
        
        pre.appendChild(doc.createTextNode(ddl));
        cell.appendChild(pre);
        
        element.appendChild(cell);
    }
    
    private void updateEventInfo(Document doc, String ddl) {
        
        Element element = doc.getElementById("showDDL");
        
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: white;");
        
        Element cell = doc.createElement("td");
        Element pre = doc.createElement("pre");
        
        pre.appendChild(doc.createTextNode(ddl));
        cell.appendChild(pre);
        
        element.appendChild(cell);
    }
    
    
    private void updateDatabaseInfo(
        Document doc, 
        
        List<String[]> tablesRows, 
        List<String[]> viewsRows, 
        List<String[]> procsRows, 
        List<String[]> functionsRows, 
        List<String[]> triggersRows, 
        List<String[]> eventsRows) {
        
        // tables
        updateTablesInfo(doc, tablesRows);
        
        // views
        updateViewsInfo(doc, viewsRows);        
        
        // procedures
        updateProceduresInfo(doc, procsRows);
        
        // functions
        updateFunctionsInfo(doc, functionsRows);
        
        // triggers
        updateTriggersInfo(doc, triggersRows);
        
        // events
        updateEventsInfo(doc, eventsRows);
    }
    
    private void updateTablesInfo(Document doc, List<String[]> tablesRows) {
        // tables
        Element tablesTitle = doc.getElementById("tablesTitle");
        tablesTitle.setTextContent("Tables (" + tablesRows.size() + ")");
        
        List<String> tablesNames = new ArrayList<>();
        
        Element table = doc.getElementById("showTables");
        boolean odd = false;
        for (String[] row: tablesRows) {
            
            tablesNames.add(row[0]);
            
            addRow(row, doc, table, "tables-", 0, odd, null);
            odd = !odd;
        }
        
        EventTarget redundantRef = (EventTarget)doc.getElementById("redundantRef");
        redundantRef.addEventListener("click", new EventListener() {
            @Override
            public void handleEvent(org.w3c.dom.events.Event evt) {
                URLUtils.openUrl("http://www.smartmysql.com/smartmysql-documentation/");
            }
        }, false);
        
        HTMLButtonElement redundantButton = (HTMLButtonElement)doc.getElementById("redundantButton");
        ((EventTarget) redundantButton).addEventListener("click", new EventListener() {
            
            boolean redundant = false; 
            @Override
            public void handleEvent(org.w3c.dom.events.Event evt) {
                redundant = !redundant;
                redundantButton.setTextContent(redundant ? "Hide Redundant Indexes Info" : "Find Redundant Indexes");
                
                Element redundantHeader = doc.getElementById("redundantHeader");
                
                if (redundant) {
                    redundantHeader.setAttribute("style", "");
                    
                    try ( ResultSet rs = (ResultSet) service.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__FIND_REDUNDANT_INDEXES), databaseName, databaseName))) {
                        if (rs != null) {
                            Map<String, String> redundantData = new HashMap<String, String>();
                            while (rs.next()) {
                                String redundantIndex = rs.getString(2);
                                if (redundantIndex.startsWith("`") && redundantIndex.endsWith("`")) {
                                    redundantIndex = redundantIndex.substring(1, redundantIndex.length() - 1);
                                }
                                
                                redundantData.put(rs.getString(1), redundantIndex);
                            }
                            
                            for (String name: tablesNames) {
                                Element trElem = doc.getElementById("tables-" + name + "-row");

                                Element cell = doc.createElement("td");
                                cell.setAttribute("id", "tables-" + name + "6-cell");

                                String textValue = "";
                                if (redundantData.containsKey(name)) {
                                    textValue = redundantData.get(name);
                                    // only for redundant
                                    trElem.setAttribute("style", "background-color: #fac8a5");
                                } else {
                                    cell.setAttribute("style", "background-color: #d6e7ff");
                                }
                                
                                cell.appendChild(doc.createTextNode(textValue));
                                trElem.appendChild(cell);
                            }
                        }
                    } catch (Throwable th) {
                        log.error("Error", th);
                    }
                } else {
                    
                    redundantHeader.setAttribute("style", "display: none;");
                    
                    boolean odd = false;
                    for (String name: tablesNames) {
                        Element trElem = doc.getElementById("tables-" + name + "-row");
                        trElem.setAttribute("style", "background-color: " + (odd ? "#e5e5e5;" : "white;"));

                        Element cell = doc.getElementById("tables-" + name + "6-cell");
                        if (cell != null) {
                            trElem.removeChild(cell);
                        }
                        
                        odd = !odd;
                    }
                }
            }            
        }, false);
    }
        
    private void updateViewsInfo(Document doc, List<String[]> viewsRows) {
        // views
        Element viewsTitle = doc.getElementById("viewsTitle");
        viewsTitle.setTextContent("Views (" + viewsRows.size() + ")");
        
        Element table = doc.getElementById("showViews");
        boolean odd = false;
        for (String[] row: viewsRows) {
            addRow(row, doc, table, "views-", 0, odd, null);
            odd = !odd;
        }
    }
    
    private void updateProceduresInfo(Document doc, List<String[]> procsRows) {    
        // procedures
        Element proceduresTitle = doc.getElementById("proceduresTitle");
        proceduresTitle.setTextContent("Procedures (" + procsRows.size() + ")");
        
        Element table = doc.getElementById("showProcedures");
        boolean odd = false;
        for (String[] row: procsRows) {
            addRow(row, doc, table, "procedures-", 0, odd, null);
            odd = !odd;
        }
    }
     
    private void updateFunctionsInfo(Document doc, List<String[]> functionsRows) {
        // functions
        Element functionsTitle = doc.getElementById("functionsTitle");
        functionsTitle.setTextContent("Functions (" + functionsRows.size() + ")");
        
        Element table = doc.getElementById("showFunctions");
        boolean odd = false;
        for (String[] row: functionsRows) {
            addRow(row, doc, table, "functions-", 0, odd, null);
            odd = !odd;
        }
    }
    
    private void updateTriggersInfo(Document doc, List<String[]> triggersRows) {
        // triggers
        Element triggersTitle = doc.getElementById("triggersTitle");
        triggersTitle.setTextContent("Triggers (" + triggersRows.size() + ")");
        
        Element table = doc.getElementById("showTriggers");
        boolean odd = false;
        for (String[] row: triggersRows) {
            addRow(row, doc, table, "triggers-", 0, odd, null);
            odd = !odd;
        }
    }
    
    private void updateEventsInfo(Document doc, List<String[]> eventsRows) {
        // events
        Element eventsTitle = doc.getElementById("eventsTitle");
        eventsTitle.setTextContent("Events (" + eventsRows.size() + ")");
        
        Element table = doc.getElementById("showEvents");
        boolean odd = false;
        for (String[] row: eventsRows) {
            addRow(row, doc, table, "events-", 0, odd, null);
            odd = !odd;
        }
    }
    
    
    private void addRow(String[] row, Document doc, Element table, String idPrefix, int prefixIndex, boolean odd, String background) {
        Element newRow = doc.createElement("tr");
        newRow.setAttribute("style", "background-color: " + (background != null ? background : (odd ? "#e5e5e5;" : "white;")));
        
        String trId = idPrefix + (prefixIndex > -1 ? row[prefixIndex] : "") + "-row";
        newRow.setAttribute("id", trId);
        
        for (int i = 0; i < row.length; i++) {
            Element cell = doc.createElement("td");
            
            String cellId = idPrefix + (prefixIndex > -1 ? row[prefixIndex] : "") + i + "-cell";
            cell.setAttribute("id", cellId);
            
            String value = row[i];
            if (value != null && value.startsWith("img::")) {
                
                Element img = doc.createElement("img");                
                img.setAttribute("src", value.substring(5));
                cell.appendChild(img);
            } else {
                cell.appendChild(doc.createTextNode(value != null ? value : "(NULL)"));
            }
            newRow.appendChild(cell);
        }        
        
        table.appendChild(newRow);
    }
    
    
    private void refresh(){
        loadEntity(previousItem, databaseName);
    }
    
    public void loadEntity(Object item, String databaseName){
        
        while(loadDataRunned) {
            loadDataStop = true;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {}
        }
        
        loadDataRunned = true;
        loadDataStop = false;
        
        this.databaseName = databaseName;
        previousItem = item;
        
        reSetStringbuilder();
        
        Thread th = new Thread(() -> {
            try {
                if (loaderMsg.getParent() == null) {
                    loaderMsg.setText("Please wait while we load information for " + (item != null ? item.toString() : ""));
                }
                Platform.runLater(() -> {
                    titleVBox.getChildren().clear();
                    titleVBox.getChildren().add(selectedItemName);
                    titleVBox.getChildren().add(loaderMsg);
                    textDetailed.clear();
                });


                if (item instanceof DBElement) {

                    if (item instanceof Database) {
                        showDatabaseInfo(sb, databaseName);

                    } else if (item instanceof DBTable) {
                        showTableInfo(sb, ((DBTable)item).getName());

                    } else if (item instanceof View) {
                        showViewInfo(sb, ((View)item).getName());

                    } else if (item instanceof StoredProc) {
                        showProcedureInfo(sb, ((StoredProc)item).getName());

                    } else if (item instanceof Function) {
                        showFunctionInfo(sb, ((Function)item).getName());

                    } else if (item instanceof Trigger) {
                        showTriggerInfo(sb, ((Trigger)item).getName());

                    } else if (item instanceof Event) {
                        showEventInfo(sb, ((Event)item).getName());

                    }

                } else if (item instanceof DatabaseChildrenType) {

                    DatabaseChildrenType name = (DatabaseChildrenType)item;
                    switch (name) {
                        case TABLES:
                            showTablesInfo(sb, databaseName);
                            break;

                        case VIEWS:
                            showViewsInfo(sb, databaseName);
                            break;

                        case STORED_PROCS:
                            showProceduresInfo(sb, databaseName);
                            break;

                        case FUNCTIONS:
                            showFunctionsInfo(sb, databaseName);
                            break;

                        case TRIGGERS:
                            showTriggersInfo(sb, databaseName);
                            break;

                        case EVENTS:
                            showEventsInfo(sb, databaseName);
                            break;
                    }
                }else if (item instanceof ConnectionParams) {                    
                    showConnectionInfo(sb, (ConnectionParams)item);
                }
                
                if (checkStopLoad()) {
                    return;
                }

                Platform.runLater(() -> {
                    titleVBox.getChildren().addAll(children);
                    children.clear();
                    titleVBox.getChildren().remove(loaderMsg);
                    titleVBox.autosize();
                    textDetailed.setText(sb.toString());
                });
                

            } finally {
                loadDataRunned = false;
            }
        });
        
        th.setDaemon(true);
        th.start();
    }
    
    private void reSetStringbuilder(){
        sb.delete(0, sb.length());
        sb.append("\n");
    }
   
    
    private void writeWithUnderline(String text, StringBuilder sb) {
        sb.append(text).append("\n");
        String underLine = "";
        for (int i = 0; i < text.length(); i++) {
            underLine += "-";
        }
        sb.append(underLine).append("\n");        
    }
    
    private void writeTable(String[] headers, List<String[]> rows, StringBuilder sb) {
                
        if(rows.isEmpty()){
            return;
        }
        
        // calculate each column size
        Integer[] columnsSize = new Integer[headers.length];        
        Boolean[] aligns = new Boolean[headers.length];
        for (int i = 0; i < headers.length; i++) {
            
            String h = headers[i];
            aligns[i] = true;
            
            int max = h.length();
            int j = 0;
            for (String[] row: rows) {
                
                String v = row[i];

                int length = notNullValue(v).length();
                if (length > max) {
                    max = length;
                }
                
                if (v != null && aligns[i]) {
                    try {
                        new BigDecimal(v);
                    } catch (NumberFormatException ex) {
                        aligns[i] = false;
                    }
                }
                
                if (j%5 == 0 && checkStopLoad()) {
                    return;
                }
                j++;
            }
            
            columnsSize[i] = max;
            
            if (i%5 == 0 && checkStopLoad()) {
                return;
            }
        }
            
        if (checkStopLoad()) {
            return;
        }
        
        // header names
        for (int i = 0; i < headers.length; i++) {            
            String h = headers[i];
            
            sb.append(prepareString(h, columnsSize[i], false)).append("  ");
        }
        
        if (checkStopLoad()) {
            return;
        }
        
        sb.append("\n");
        
        for (int i = 0; i < headers.length; i++) {
            
            String underLine = "";
            for (int j = 0; j < columnsSize[i]; j++) {
                underLine += "-";
            }
            sb.append(underLine).append("  ");
        }
        /////////////////////
        
        if (checkStopLoad()) {
            return;
        }
        
        sb.append("\n");
        
        // draw rows
        for (String[] row: rows) {    
            
            boolean hasEnter = false;
            for (int i = 0; i < headers.length; i++) {
                
                String v = notNullValue(row[i]);
                if (v.contains("\n")) {
                    addEmptyRows(v, row, i, columnsSize, sb);
                    hasEnter = true;
                    
                } else if (!hasEnter) {
                    sb.append(prepareString(v, columnsSize[i], aligns[i])).append("  ");
                }
                
                if (i%100 == 0 && checkStopLoad()) {
                    return;
                }
            }
            
            if (checkStopLoad()) {
                return;
            }

            sb.append("\n");
        }        
        
        sb.append("\n");
    }
    
    private void addEmptyRows(String v, String[] columns, int column, Integer[] columnsSize, StringBuilder sb) {
        String[] rows = v.split("\n");
        
        boolean first = true;
        boolean appended = false;
        for (String r: rows) {
            
            for (int i = 0; i < columnsSize.length; i++) {
                if (i == column) {
                    sb.append(prepareString(r, columnsSize[i], false)).append("  ");
                    appended = true;
                    
                } else {                    
                    if (appended) {
                        sb.append(prepareString(first ? columns[i] : "", columnsSize[i], false)).append("  ");
                    }                    
                }
            }

            first = false;
            sb.append("\n");
        }
    }
    
    private String notNullValue(String v) {
        return v != null ? v : "(NULL)";
    }
    
    private String prepareString(String s, int length, boolean toRight) {
        String result = s;
        for (int i = 0; i < length - s.length(); i++) {
            if (toRight) {
                result = " " + result;
            } else {
                result += " ";
            }
        }
        
        return result;
    }
}