package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.ColorDialogController;
import np.com.ngopal.smart.sql.ui.controller.DatabaseController;
import np.com.ngopal.smart.sql.ui.controller.ImportTableController;
import np.com.ngopal.smart.sql.ui.controller.ScheduledBackupWizardController;
import np.com.ngopal.smart.sql.ui.controller.SelectSchemaController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.ui.controller.ExecuteSqlScriptDialogController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
public class MenuInRootModule extends AbstractStandardModule {

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public boolean install() {
        return true;
    }
     
    @Override
    public boolean selectedTreeItemChanged(TreeItem obj) {return true; }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        
        MenuItem refreshObjectBrowserMenuItem = new MenuItem("Refresh Object Browser",
                new ImageView(StandardDefaultImageProvider.getInstance().getRefresh_image()));
        
        refreshObjectBrowserMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F5));
        refreshObjectBrowserMenuItem.setOnAction((ActionEvent event) -> {
            getController().refreshTree(getController().getSelectedConnectionSession().get(), true);
        });
        
        
        MenuItem createDatabaseItem = new MenuItem("Create Database...",
                new ImageView(SmartImageProvider.getInstance().getDatabaseCreate_image()));
        createDatabaseItem.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.SHORTCUT_DOWN));
        createDatabaseItem.setOnAction((ActionEvent event) -> {
            
            DatabaseController databaseController = ((MainController)getController()).getTypedBaseController("DatabaseDialog");
            databaseController.setDBService(getController().getSelectedConnectionSession().get().getService());
            
            if (databaseController.showCreate()) {
                
                // show choosing scheam if need
                if (databaseController.isNeedUseSchema()) {
                    SelectSchemaController selectSchemaController = ((MainController)getController()).getTypedBaseController("SelectSchema");
                    selectSchemaController.setDBService(getController().getSelectedConnectionSession().get().getService());
                    
                    selectSchemaController.show(databaseController.getDatabaseName(), (Consumer) (Object t) -> {
                        getController().refreshTree(getController().getSelectedConnectionSession().get(), false);
                        ((MainController)getController()).showInfo("Info", "Database created successfully", null);                        
                    });
                    
                } else {
                    getController().refreshTree(getController().getSelectedConnectionSession().get(), false);
                    ((MainController)getController()).showInfo("Info", "Database created successfully", null);
                }
            }
        });
        
        
        MenuItem scheduledBackupsItem = new MenuItem("Scheduled Backups...",
                new ImageView(SmartImageProvider.getInstance().getScheduleBackUp_image()));
        scheduledBackupsItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        scheduledBackupsItem.setOnAction((ActionEvent event) -> {
            showScheduledBackupsDialog(getController().getSelectedConnectionSession().get());
        });
        
        
        MenuItem importExternalDataItem = new MenuItem("Import External Data...",
                new ImageView(SmartImageProvider.getInstance().getDatabaseExternalImport()));
        importExternalDataItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
         importExternalDataItem.setOnAction((ActionEvent event) -> {
             showImportExternalDataDialog(getController().getSelectedConnectionSession().get());
        });
        
         
        MenuItem executeSQLScriptItem = new MenuItem("Execute SQL Script...",
                new ImageView(SmartImageProvider.getInstance().getDatabaseExecuteScript_image()));
        executeSQLScriptItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.SHORTCUT_DOWN,KeyCombination.SHIFT_DOWN));
        executeSQLScriptItem.setOnAction((ActionEvent event) -> {
            showExecuteSqlScriptDialog(getController().getSelectedConnectionSession().get());
        });
        
        
        MenuItem changeObjectBrowserColorMenuItem = new MenuItem("Change Object Browser Color",
                new ImageView(SmartImageProvider.getInstance().getColor_image()));
        changeObjectBrowserColorMenuItem.setOnAction((ActionEvent event) -> {
            
            ColorDialogController _controller = ((MainController)getController()).getTypedBaseController("ColorDialog");
            _controller.create();
            ConnectionParams cp = getController().getSelectedConnectionSession().get().getConnectionParam();
            if (cp.getForegroundColor() != null && cp.getBackgroundColor() != null) {
                _controller.setPaintDefaults(Color.web(cp.getForegroundColor()),
                        Color.web(cp.getBackgroundColor()));
            }
            _controller.show_hide();
            ConnectionSession s = getController().getSelectedConnectionSession().get();
            if (s != null) {
                Object[] res = _controller.getResults("tab" + String.valueOf(s.getId()));
                if (res != null) {                
                    TabContentController tabController = getMainController().getTabControllerBySession(s);
                    if (tabController != null) {
                        ((SimpleObjectProperty)tabController.getTreeViewUserData()).setValue(res);
                    }
                }
            }
        });
        
        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("root0", new ArrayList<>(Arrays.asList(new MenuItem[]{refreshObjectBrowserMenuItem}))), Hooks.QueryBrowser.ROOT);
        map.put(new HookKey("root1", new ArrayList<>(Arrays.asList(new MenuItem[]{createDatabaseItem}))), Hooks.QueryBrowser.ROOT);
        map.put(new HookKey("root2", new ArrayList<>(Arrays.asList(new MenuItem[]{scheduledBackupsItem, importExternalDataItem, executeSQLScriptItem}))), Hooks.QueryBrowser.ROOT);        
        map.put(new HookKey("root3", new ArrayList<>(Arrays.asList(new MenuItem[]{changeObjectBrowserColorMenuItem}))), Hooks.QueryBrowser.ROOT);

        return map;
    }
    
    public void showScheduledBackupsDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ScheduledBackupWizardController backupWizardController = getTypedBaseController("ScheduledBackupWizard");
        
        Database database = null;
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof Database) {
                database = (Database)itemValue;
            }
        }
        try {
            backupWizardController.init(session.getService(), session.getConnectionParam(), database);
            final Parent p = backupWizardController.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Export Data As Batch Script Wizard");

            dialog.showAndWait();
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), ex);
        }
    }
    
    private void showExecuteSqlScriptDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ExecuteSqlScriptDialogController controller = ((MainController)getController()).getTypedBaseController("ExecuteSqlScriptDialog");
        controller.setDbService(session.getService());
        
        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Execute sql script");

        dialog.showAndWait();
        
        // and refresh tree
        getController().refreshTree(session, false);
    }
    
    private void showImportExternalDataDialog(ConnectionSession session) {
        final Stage dialog = new Stage();        
        ImportTableController  importTableController = getTypedBaseController("ImportTable");
        importTableController.init(session, null);
        
        final Scene scene = new Scene(importTableController.getUI());
        
        dialog.setScene(scene);
        dialog.initStyle(StageStyle.UTILITY);        
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Import External Data Wizard");
        dialog.showAndWait();
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
