package np.com.ngopal.smart.sql.ui.controller.designer;

import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;
import np.com.ngopal.smart.sql.ui.fxgraph.FXNode;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerLayerTittledPane extends Pane implements DesignerPane {
        
    HBox headerBox;        
    DesignerLayer data;

    Label titleLabel;

    boolean sizeChanged = false;
    
    SchemaDesignerTabContentController controller;


    public DesignerLayerTittledPane(SchemaDesignerTabContentController controller, DesignerLayer data) {

        getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_LAYER);
        this.data = data;
        this.controller = controller;

        data.color().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            setStyle("-fx-background-color: " + data.color().get());
        });
        setStyle("-fx-background-color: " + data.color().get());

        addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                controller.showLayerTab(data);
            }
        });

        // header

        titleLabel = new Label("");
        titleLabel.setMaxWidth(Double.MAX_VALUE);
        titleLabel.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_COMPONENT_HEADER_LABEL);

        titleLabel.textProperty().bind(data.name());

        headerBox = new HBox(titleLabel);
        headerBox.setPrefWidth(USE_COMPUTED_SIZE);
        headerBox.maxWidthProperty().bind(widthProperty());
        headerBox.setLayoutX(0);
        headerBox.setLayoutY(0);
        headerBox.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_LAYER_HEADER_EXPANDED);
        headerBox.setStyle("");
        HBox.setHgrow(titleLabel, Priority.ALWAYS);

        getChildren().add(headerBox);
        
        updateData();            
        
        addDefaultDataListeners();
    }

    @Override
    public void toFront() {
        super.toFront();

        List<DesignerData> designerData = controller.getCurrentDiagram().findLayerData(data.uuid().get());
        if (designerData != null && !designerData.isEmpty()) {
            for (DesignerData dt: designerData) {
                FXNode node = controller.getDesignerDataNode(dt);
                if (node != null) {
                    ((Node)node.getWrapedNode()).toFront();
                }                        
            }
        }
    }

    @Override
    public DesignerLayer getData() {
        return data;
    }
    
    @Override
    public Region getRegion() {
        return this;
    }

    @Override
    public boolean canChangeHeight() {
        return true;
    }

    @Override
    public void setPrefSize(double prefWidth, double prefHeight) {
        super.setPrefSize(prefWidth, prefHeight);

        sizeChanged = true;            
        updateData();
    }

    private void updateData() {               

        setMinSize(110, 50); 
    }
}
