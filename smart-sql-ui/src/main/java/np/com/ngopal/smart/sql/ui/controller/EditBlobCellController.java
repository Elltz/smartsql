package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.queryutils.FontUtils;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class EditBlobCellController extends BaseController implements Initializable {
    
    @Inject
    private PreferenceDataService preferenceService;
    @FXML
    private AnchorPane mainUI;

    @FXML
    private ToggleButton textButton;
    @FXML
    private ToggleButton imageButton;
    @FXML
    private CheckBox setNullCheckBox;
    @FXML
    private Button importButton;
    @FXML
    private Button okButton;
    @FXML
    private TextArea textArea;
    @FXML
    private ScrollPane imageScroll;
    @FXML
    private ImageView imageView;
    @FXML
    private Label sizeLabel;   

    
    private String data = null;
    
    
    @FXML
    void saveAction(ActionEvent event) {
        FileChooser fc = new FileChooser();
        File f = fc.showSaveDialog((Stage)mainUI.getScene().getWindow());
        if (f != null) {
            try {
                byte[] bytes = null;
                if (textButton.isSelected()) {
                    try {
                        bytes = textArea.getText().getBytes("UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        log.error("Error", ex);
                    }
                } else {
                    try {
                        BufferedImage bImage = SwingFXUtils.fromFXImage(imageView.getImage(), null);
                        ByteArrayOutputStream s = new ByteArrayOutputStream();
                        ImageIO.write(bImage, "png", s);

                        bytes = s.toByteArray();
                    } catch (IOException ex) {
                        log.error("Error", ex);
                    }
                }
                
                if (bytes != null) {
                    Files.write(f.toPath(), bytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                }
            } catch (Throwable th) {
                log.error("Error", th);
            }
        }
    }
    
    @FXML
    void importAction(ActionEvent event) {
        FileChooser fc = new FileChooser();
        File f = fc.showOpenDialog((Stage)mainUI.getScene().getWindow());
        if (f != null) {
            try {
                // we must check this is image or no
                byte[] bytes = Files.readAllBytes(f.toPath());
                if(ImageIO.read(f) == null) {
                    textArea.setText(new String(bytes, "UTF-8"));
                } else {

                    try {
                        imageView.setImage(new Image(new ByteArrayInputStream(bytes)));
                    } catch (Throwable th) {}
                }                
            } catch (Throwable ex) {
                log.error("Error", ex.getMessage());
            }
        }
    }
    

    @FXML
    void cancelAction(ActionEvent event) {
        data = null;
        ((Stage)mainUI.getScene().getWindow()).close();
    }

    @FXML
    void okAction(ActionEvent event) {
        if (textButton.isSelected()) {
            data = textArea.getText();
        } else {
            
            if (imageView.getImage() != null) {
                try {
                    BufferedImage bImage = SwingFXUtils.fromFXImage(imageView.getImage(), null);
                    ByteArrayOutputStream s = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", s);
                    data = "x'" + Hex.encodeHexString(s.toByteArray()) + "'";
                } catch (Throwable th) {
                    log.error("Error", th);
                }
            }
        }
        ((Stage)mainUI.getScene().getWindow()).close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        textArea.visibleProperty().bind(textButton.selectedProperty());
        imageScroll.visibleProperty().bind(imageButton.selectedProperty());
        
        textButton.disableProperty().bind(setNullCheckBox.selectedProperty());
        imageButton.disableProperty().bind(setNullCheckBox.selectedProperty());
        textArea.disableProperty().bind(setNullCheckBox.selectedProperty());
        imageScroll.disableProperty().bind(setNullCheckBox.selectedProperty());
        importButton.disableProperty().bind(setNullCheckBox.selectedProperty());
        
        textArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (textButton.isSelected()) {
                try {
                    sizeLabel.setText("Size: " + new DecimalFormat("#,###").format(newValue != null ? newValue.getBytes("UTF-8").length : 0) + " bytes");
                } catch (Throwable th) {}
            }
        });
        
        Font f = FontUtils.parseFXFont(preferenceService.getPreference().getFontsBlobViewer());
        if (f != null) {
            textArea.setFont(f);
        }
    }
    
    public void init(String data, boolean editable) {
        
        okButton.setDisable(!editable);
        
        if ("(NULL)".equals(data)) {
            setNullCheckBox.setSelected(true);
        } else {
            try {
                if (data.startsWith("x'") && data.endsWith("'")) {
                    imageButton.setSelected(true);
                    
                    byte[] bytes = Hex.decodeHex(data.substring(2, data.length() - 1).toCharArray());
                    imageView.setImage(new Image(new ByteArrayInputStream(bytes)));
                    
                    sizeLabel.setText("Size: " + new DecimalFormat("#,###").format(bytes.length) + " bytes");
                } else {
                    textButton.setSelected(true);
                    textArea.setText(data);
                    sizeLabel.setText("Size: " + new DecimalFormat("#,###").format(data.getBytes("UTF-8").length) + " bytes");
                }
            } catch (Throwable th) {}
        }
    }
    
    public boolean isNullable() {
        return setNullCheckBox.isSelected();
    }

    public String getData() {
        return data;
    }
}
