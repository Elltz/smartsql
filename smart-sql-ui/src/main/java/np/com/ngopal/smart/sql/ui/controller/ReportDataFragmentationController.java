package np.com.ngopal.smart.sql.ui.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportDataFragmentationController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
    @FXML
    private ComboBox<String> databaseCombobox;
    @FXML
    private Button exportButton;
    
    private ConnectionSession session;
    
    private Double totalSize = 0.0;
    private Double totalDataSize = 0.0;
    private Double totalIndexSize = 0.0;
    private Double totalFragmentationSize = 0.0;
    
    
    @FXML
    private ToggleButton gridButton;
    @FXML
    private ToggleButton piechartButton;
    @FXML
    private ToggleButton barchartButton;
    
    @FXML
    private HBox filterBox;
    
    @FXML
    private PieChart piechart;
    @FXML
    private StackedBarChart<String, Number> barchart;
    @FXML
    private CategoryAxis categoryAxis;
    @FXML
    private NumberAxis numberAxis;
    
    @FXML
    private Button refreshButton;
    
    private TableColumn databaseColumn;
    private TableColumn engineColumn;
    private TableColumn dataSizeColumn;
    private TableColumn dataSizePercentColumn;
    private TableColumn indexSizeColumn;
    private TableColumn indexSizePercentColumn;
    private TableColumn fragmentationSizeColumn;
    private TableColumn fragmentationSizePercentColumn;
    private TableColumn ratioColumn;
    
    @FXML
    public void refreshAction(ActionEvent event) {
        
        totalDataSize = 0.0;
        totalIndexSize = 0.0;
        totalFragmentationSize = 0.0;
        
        if (session != null) {
            boolean all = "All".equals(databaseCombobox.getSelectionModel().getSelectedItem());
            String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FRAGMENTATION_0);
            if (!all) {
                query = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FRAGMENTATION_1), databaseCombobox.getSelectionModel().getSelectedItem());
            }
            
            int maxPieLength = 10;
            piechart.getData().clear();          
            
            
            barchart.getData().clear();
            
            XYChart.Series seriesDataSize = new XYChart.Series();
            seriesDataSize.setName("Data size");

            XYChart.Series seriesIndexSize = new XYChart.Series();
            seriesIndexSize.setName("Index size");

            XYChart.Series seriesFragmentationSize = new XYChart.Series();
            seriesFragmentationSize.setName("Fragmentation size");
            
            double max = 0;
            
            try (ResultSet rs = (ResultSet) session.getService().execute(query)) {
                if (rs != null) {
                    List<ReportDataFragmentation> list = new ArrayList<>();
                    
                    int i = 0;
                    
                    boolean hasOthers = false;
                    double otherTotalSize = 0.0;
                    double otherDataSize = 0.0;
                    double otherIndexSize = 0.0;
                    double otherFragmentSize = 0.0;
                    
                    while (rs.next()) {
                        ReportDataFragmentation rdf = new ReportDataFragmentation(
                                rs.getString(1), 
                                rs.getString(3), 
                                new BigDecimal(rs.getDouble(7)).setScale(2, RoundingMode.HALF_UP), 
                                new BigDecimal(rs.getDouble(4)).setScale(2, RoundingMode.HALF_UP),
                                new BigDecimal(rs.getDouble(5)).setScale(2, RoundingMode.HALF_UP),
                                new BigDecimal(rs.getDouble(6)).setScale(2, RoundingMode.HALF_UP),
                                rs.getString(2)
                        );
                        
                        list.add(rdf);
                        
                        totalDataSize += rdf.getDataSize().doubleValue();
                        totalIndexSize += rdf.getIndexSize().doubleValue();
                        totalFragmentationSize += rdf.getFragmentationSize().doubleValue();
                        
                        if (i < maxPieLength) {
                            PieChart.Data d = new PieChart.Data(rdf.getTableName(), rdf.getTotalSize().doubleValue());
                            
                            BigDecimal dataSize = rdf.getDataSize();
                            BigDecimal indexSize = rdf.getIndexSize();
                            BigDecimal fragmentationSize = rdf.getFragmentationSize();
                            
                            String text = "Table: " + d.getName() + "\n" +
                                    "--------------------\n" +
                                    "Data size = " + dataSize.toString() + "MB\n" + 
                                    "Index size = " + indexSize.toString() + "MB\n" + 
                                    "Fragmentation size = " + fragmentationSize.toString() + "MB";

                            Tooltip tooltip = new Tooltip();
                            tooltip.setText(text);
                            
                            d.nodeProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                                if (newValue instanceof Node) {
                                    Tooltip.install((Node) newValue, tooltip);
                                }
                            });
                            piechart.getData().add(d);
                            
//                            EventHandler<MouseEvent> handler = (MouseEvent event1) -> {
//                                databaseCombobox.getSelectionModel().select(d.getName());
//                            };
                            
                            max = max < rdf.getTotalSize().doubleValue() ? rdf.getTotalSize().doubleValue() : max;
 
                            String text0 = "Data size = " + dataSize.toString() + "MB";
                            Tooltip tooltip0 = new Tooltip();
                            tooltip0.setText(text0);
                            
                            XYChart.Data d0 = new XYChart.Data(d.getName(), dataSize);
                            d0.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                if (newValue instanceof Node) {
                                    Tooltip.install((Node)newValue, tooltip0);
                                }
                            });
                            seriesDataSize.getData().add(d0);
                            

                            String text1 = "Index size = " + indexSize.toString() + "MB";
                            Tooltip tooltip1 = new Tooltip();
                            tooltip1.setText(text1);
                            XYChart.Data d1 = new XYChart.Data(d.getName(), indexSize);
                            d1.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                if (newValue instanceof Node) {
                                    Tooltip.install((Node)newValue, tooltip1);
                                }
                            });
                            seriesIndexSize.getData().add(d1);
                            
                            
                            String text2 = "Fragmentation size = " + fragmentationSize.toString() + "MB";
                            Tooltip tooltip2 = new Tooltip();
                            tooltip2.setText(text2);
                            XYChart.Data d2 = new XYChart.Data(d.getName(), fragmentationSize);
                            d2.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                if (newValue instanceof Node) {
                                    Tooltip.install((Node) newValue, tooltip2);
                                }
                            });
                            seriesFragmentationSize.getData().add(d2);
                            
                            i++;
                        } else {
                            hasOthers = true;
                            otherTotalSize += rdf.getTotalSize().doubleValue();
                            otherDataSize += rdf.getDataSize().doubleValue();
                            otherIndexSize += rdf.getIndexSize().doubleValue();
                            otherFragmentSize += rdf.getFragmentationSize().doubleValue();
                        }
                    }
                    
                    for (ReportDataFragmentation rdf: list) {
                        rdf.setDataSizePercent(calcPercent(rdf.getDataSize().doubleValue(), "dataSize") + "%");                        
                        rdf.setIndexSizePercent(calcPercent(rdf.getIndexSize().doubleValue(), "indexSize") + "%");
                        rdf.setFragmentationSizePercent(calcPercent(rdf.getFragmentationSize().doubleValue(), "fragmentationSize") + "%");
                    }
                    
                    max = max < otherTotalSize ? otherTotalSize : max;
                    
                    tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                    
                    if (hasOthers) {
                        PieChart.Data other = new PieChart.Data("others", otherTotalSize);                        
                        
                        BigDecimal dataSize = new BigDecimal(otherDataSize).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal indexSize = new BigDecimal(otherIndexSize).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal fragmentationSize = new BigDecimal(otherFragmentSize).setScale(2, RoundingMode.HALF_UP);

                        String text = "Table: others\n" +
                                "--------------------\n" +
                                "Data size = " + dataSize.toString() + "MB\n" + 
                                "Index size = " + indexSize.toString() + "MB\n" + 
                                "Fragmentation size = " + fragmentationSize.toString() + "MB";

                        Tooltip tooltip = new Tooltip();
                        tooltip.setText(text);
                        other.nodeProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip);
                            }
                        });
                        piechart.getData().add(other); 
                        
                        
                        String text0 = "Data size = " + dataSize.toString() + "MB";
                        Tooltip tooltip0 = new Tooltip();
                        tooltip0.setText(text0);
                        XYChart.Data d0 = new XYChart.Data("others", dataSize);
                        d0.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip0);
                            }
                        });
                        seriesDataSize.getData().add(d0);

                        String text1 = "Index size = " + indexSize.toString() + "MB";
                        Tooltip tooltip1 = new Tooltip();
                        tooltip1.setText(text1);
                        XYChart.Data d1 = new XYChart.Data("others", indexSize);
                        d1.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip1);
                            }
                        });
                        seriesIndexSize.getData().add(d1);
                        
                        String text2 = "Fragmentation size = " + fragmentationSize.toString() + "MB";
                        Tooltip tooltip2 = new Tooltip();
                        tooltip2.setText(text2);                        
                        XYChart.Data d2 = new XYChart.Data("others", fragmentationSize);
                        d2.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                            if (newValue instanceof Node) {
                                Tooltip.install((Node) newValue, tooltip2);
                            }
                        });
                        seriesFragmentationSize.getData().add(d2);
                    }
                }
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", ex.getMessage(), null);
            }
            
            totalSize = totalDataSize + totalIndexSize + totalFragmentationSize;
            
            numberAxis.setUpperBound(max + max / 10.0);
            barchart.getData().addAll(seriesDataSize, seriesIndexSize, seriesFragmentationSize);
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        
        databaseCombobox.getItems().add("All");
        databaseCombobox.getSelectionModel().select(0);
                
        if (session != null && session.getConnectionParam() != null) {
            if (session.getConnectionParam().getDatabases() != null) {
                for (Database db: session.getConnectionParam().getDatabases()) {
                    databaseCombobox.getItems().add(db.getName());
                }
            }            
        }
        
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    public boolean isDatabaseMode() {
        return "All".equals(databaseCombobox.getSelectionModel().getSelectedItem());
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            byte[] pieChartImage;
            try {
                WritableImage image = piechart.snapshot(new SnapshotParameters(), null);
                
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

                pieChartImage = bos.toByteArray();
            } catch (Throwable e) {
                log.error("Error", e);
                pieChartImage = null;                
            }
            
            byte[] barChartImage;
            try {
                WritableImage image = barchart.snapshot(new SnapshotParameters(), null);
                
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

                barChartImage = bos.toByteArray();
            } catch (Throwable e) {
                log.error("Error", e);
                barChartImage = null;                
            }
            
            byte[] pieChartImg = pieChartImage;
            byte[] barChartImg = barChartImage;
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportDataFragmentation rdf: (List<ReportDataFragmentation>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rdf.getDatabase());
                        row.add(rdf.getEngine());
                        row.add(rdf.getTableName());
                        row.add(rdf.getDataSize());
                        row.add(rdf.getDataSizePercent());
                        row.add(rdf.getIndexSize());
                        row.add(rdf.getIndexSizePercent());
                        row.add(rdf.getFragmentationSize());
                        row.add(rdf.getFragmentationSizePercent());
                        row.add(rdf.getFragmentationRation());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    columns.put("Database", true);
                    columnWidths.add((int)databaseColumn.getWidth());
                    
                    columns.put("Engine", true);
                    columnWidths.add((int)engineColumn.getWidth());
                    
                    columns.put("Table name", true);
                    columnWidths.add((int)dataSizeColumn.getWidth());

                    columns.put("Data Size(MB)", true);
                    columnWidths.add((int)dataSizePercentColumn.getWidth());

                    columns.put("(%)", true);
                    columnWidths.add((int)indexSizeColumn.getWidth());
                    
                    columns.put("Index Size(MB)", true);
                    columnWidths.add((int)indexSizePercentColumn.getWidth());
                    
                    columns.put(" (%)", true);
                    columnWidths.add((int)fragmentationSizeColumn.getWidth());
                    
                    columns.put("Fragmentation size(MB)", true);
                    columnWidths.add((int)fragmentationSizePercentColumn.getWidth());
                    
                    columns.put("   (%)", true);
                    columnWidths.add((int)tableView.getColumnByName("(%)").getWidth());
                    
                    columns.put("Fragmentation ratio", true);
                    columnWidths.add((int)ratioColumn.getWidth());
                                        
                    sheets.add("Data fragmentation report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    
                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
                                        
                    sheets.add("Bar chart");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    
                    images.add(barChartImg);
                    heights.add(30);
                    colspans.add(14);
                }
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    
                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
                                        
                    sheets.add("Pie chart");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    
                    images.add(pieChartImg);
                    heights.add(30);
                    colspans.add(14);
                }
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    private Callback<TableColumn<ReportDataFragmentation, Object>, TableCell<ReportDataFragmentation, Object>> percentCallback = new Callback<TableColumn<ReportDataFragmentation, Object>, TableCell<ReportDataFragmentation, Object>>() {
        @Override
        public TableCell<ReportDataFragmentation, Object> call(TableColumn<ReportDataFragmentation, Object> param) {
            return new TableCell<ReportDataFragmentation, Object>() {
                @Override
                protected void updateItem(Object item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item != null ? item.toString() : "");
                    setStyle("-fx-padding: 0 0 2 0");
                    setAlignment(Pos.CENTER_RIGHT);
                }

            };
        }
    };

    @Override
    public BaseController getReportController() {
        return this;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)barchartButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getBarChart_image());
        ((ImageView)piechartButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getPieChart_image());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        tableView.setSearchEnabled(true);
        
        piechart.managedProperty().bind(piechart.visibleProperty());
        tableView.managedProperty().bind(tableView.visibleProperty());
        
        numberAxis.setAutoRanging(false);
        barchart.setCategoryGap(50);
        
        gridButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                tableView.toFront();
            }
        });
        
        barchartButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                barchart.toFront();
            }
        });
        
        piechartButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                piechart.toFront();
            }
        });
        
        databaseColumn = createColumn("Database", "database");
        databaseColumn.setCellFactory(new Callback<TableColumn<ReportDataFragmentation, String>, TableCell<ReportDataFragmentation, String>>() {
            @Override
            public TableCell<ReportDataFragmentation, String> call(TableColumn<ReportDataFragmentation, String> param) {
                TableCell tc = new TableCell<ReportDataFragmentation, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setText(null);
                        setGraphic(null);
                            
                        if (item != null && !item.isEmpty() && !empty) {
                                                      
                            Hyperlink link = new Hyperlink(item);
                            link.setFocusTraversable(false);
                            link.setOnAction((ActionEvent event) -> {
                                Platform.runLater(() -> {
                                    databaseCombobox.getSelectionModel().select(item);
                                });                                
                            });

                            setGraphic(link);
                        }
                    }
                };
                return tc;
            }
        });
        
        engineColumn = createColumn("Engine", "engine");        
        TableColumn tableNameColumn = createColumn("Table name", "tableName");
        tableNameColumn.setCellFactory(new Callback<TableColumn<ReportDataFragmentation, String>, TableCell<ReportDataFragmentation, String>>() {
            @Override
            public TableCell<ReportDataFragmentation, String> call(TableColumn<ReportDataFragmentation, String> param) {
                TableCell tc = new TableCell<ReportDataFragmentation, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : getString());
                        setGraphic(null);
                        setTextFill(Color.BLACK);
                        
                        List list = tableView.getCopyItems();
                        int row = getTableRow().getIndex();
                        if (row > -1 && list.size() > row) {

                            ReportDataFragmentation rds = (ReportDataFragmentation) list.get(row);
                            if (rds.getDataSize().doubleValue() < rds.getFragmentationSize().doubleValue() && rds.getDataSize().doubleValue() > 1024) {
                                setTextFill(Color.RED);
                                
                            } else if (rds.getDataSize().doubleValue() < 2 * rds.getFragmentationSize().doubleValue() && rds.getDataSize().doubleValue() > 1024) {
                                setTextFill(Color.BLUE);
                                
                            } else if (rds.getDataSize().doubleValue() < 4 * rds.getFragmentationSize().doubleValue() && rds.getDataSize().doubleValue() > 1024) {
                                setTextFill(Color.YELLOW);
                            }
                        }
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem();
                    }
                };
                return tc;
            }
        });
        
        
        dataSizeColumn = createColumn("Data Size(MB)", "dataSize");
        dataSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataFragmentation, BigDecimal>, TableCell<ReportDataFragmentation, BigDecimal>>() {
            @Override
            public TableCell<ReportDataFragmentation, BigDecimal> call(TableColumn<ReportDataFragmentation, BigDecimal> param) {
                return new NumberTableCell("dataSize");
            }
        });
        
        dataSizePercentColumn = createColumn("(%)", "dataSizePercent");
        dataSizePercentColumn.setCellFactory(percentCallback);
        
        indexSizeColumn = createColumn("Index Size(MB)", "indexSize");
        indexSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataFragmentation, BigDecimal>, TableCell<ReportDataFragmentation, BigDecimal>>() {
            @Override
            public TableCell<ReportDataFragmentation, BigDecimal> call(TableColumn<ReportDataFragmentation, BigDecimal> param) {
                return new NumberTableCell("indexSize");
            }
        });
        
        indexSizePercentColumn = createColumn("(%)", "indexSizePercent");
        indexSizePercentColumn.setCellFactory(percentCallback);
        
        fragmentationSizeColumn = createColumn("Fragmentation size(MB)", "fragmentationSize");
        fragmentationSizeColumn.setCellFactory(new Callback<TableColumn<ReportDataFragmentation, BigDecimal>, TableCell<ReportDataFragmentation, BigDecimal>>() {
            @Override
            public TableCell<ReportDataFragmentation, BigDecimal> call(TableColumn<ReportDataFragmentation, BigDecimal> param) {
                return new NumberTableCell("fragmentationSize");
            }
        });
        
        fragmentationSizePercentColumn = createColumn("(%)", "fragmentationSizePercent");
        fragmentationSizePercentColumn.setCellFactory(percentCallback);
        
        ratioColumn = createColumn("Fragmentation ratio", "fragmentationRation");
        ratioColumn.setCellFactory(percentCallback);
        
        tableView.addTableColumn(databaseColumn, 0.1);
        tableView.addTableColumn(engineColumn, 0.07);
        tableView.addTableColumn(tableNameColumn, 0.25);
        tableView.addTableColumn(dataSizeColumn, 0.07);
        tableView.addTableColumn(dataSizePercentColumn, 38, 38, false);
        tableView.addTableColumn(indexSizeColumn, 0.07);
        tableView.addTableColumn(indexSizePercentColumn, 38, 38, false);
        tableView.addTableColumn(fragmentationSizeColumn, 0.12);
        tableView.addTableColumn(fragmentationSizePercentColumn, 38, 38, false);
        tableView.addTableColumn(ratioColumn, 0.12);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        databaseCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            refreshAction(null);
        });
    }
    
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }
    
    
    private class NumberTableCell extends TableCell<ReportDataFragmentation, BigDecimal> {    
        
        private final String property;        
        
        public NumberTableCell(String property) {
            this.property = property;
        }
        
        @Override
        protected void updateItem(BigDecimal item, boolean empty) {
            super.updateItem(item, empty); 
            
            setStyle("");
            setText("");
            
            setAlignment(Pos.CENTER_RIGHT);
            
            if (!empty) {
                if (item != null) {                    
                    setText(item.toString());
                    
                    if (item.compareTo(BigDecimal.ZERO) > 0) {
                        
                        List list = tableView.getCopyItems();
                        int row = getTableRow().getIndex();
                        if (row > -1 && list.size() > row) {
                            
                            Label l = new Label(item.toString());
                            l.setAlignment(Pos.CENTER_RIGHT);
                            l.setStyle("-fx-padding: 2 0 0 0; -fx-background-insets: 4 0 0 0; "
                                    + "-fx-background-color: linear-gradient(from 0% 100% to 100% 100%, " 
                                        + calcColor((ReportDataFragmentation) list.get(row), this, property) + " 0%, transparent " 
                                        + calcPercent(item.doubleValue(), property).setScale(2, RoundingMode.HALF_UP) + "%)");
                            l.setMaxWidth(Double.MAX_VALUE);
                            
                            setGraphic(l);
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);                            
                        }
                    }
                }
            }
        }
    }
        
    private BigDecimal calcPercent(Double value, String property) {
        
        switch (property) {
                
            case "dataSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            case "indexSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            case "fragmentationSize":
                return new BigDecimal(totalSize > 0.0 ? value * 100.0 / totalSize : 0.0).setScale(0, RoundingMode.HALF_UP);
                
            default:
                return BigDecimal.ZERO;
        }
    }
    
    
    private String calcColor(ReportDataFragmentation rds, NumberTableCell cell, String property) {
        
        switch (property) {
                
            case "dataSize":
                if (rds.getFragmentationSize().doubleValue() < 1.0 && rds.getFragmentationSize().doubleValue() < rds.getDataSize().doubleValue()) {
                    return "green";
                } else {
                    return "yellow";
                }
                
            case "indexSize":
                if (rds.getDataSize().doubleValue() < rds.getIndexSize().doubleValue()) {
                    cell.setTooltip(new Tooltip("It looks the " + (isDatabaseMode() ? "database" : "table") + " has too many indexes."));
                    return "red";
                } else {
                    cell.setTooltip(null);
                    return "yellow";
                }
                
            case "fragmentationSize":
                if (rds.getDataSize().doubleValue() < rds.getFragmentationSize().doubleValue()) {
                    cell.setTooltip(new Tooltip("The Engine has data fragmentation."));
                    return "red";
                } else {
                    cell.setTooltip(null);
                    return "yellow";
                }
                
            default:
                return "yellow";
        }
    }
}
