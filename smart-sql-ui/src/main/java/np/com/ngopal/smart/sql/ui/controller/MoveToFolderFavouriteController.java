package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TreeView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.core.modules.utils.FavoritesUtils;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.model.Favourites;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
/**
 *
 * @author Terah laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class MoveToFolderFavouriteController extends BaseController implements Initializable{
    
    @FXML
    private AnchorPane mainUI;  
 
    @FXML
    private TreeView<Favourites> treeview;    
    
    @Inject
    private FavouritesService favServ;
    
    private TreeItem<Favourites> root;
            
    private Stage dialog;
    
    
    private Favourites selected;
    
    public void okAction(ActionEvent event) {
        TreeItem<Favourites> item = treeview.getSelectionModel().getSelectedItem();
        if (item != null) {
            selected = item.getValue();
        }
        
        dialog.close();
    }
    
    public void closeAction(ActionEvent event) {
        selected = null;
        dialog.close();
    }
    
    public Favourites getSelected() {
        return selected;
    }
    
    public void init(Window win){
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setResizable(false);
        dialog.setTitle("Browse fo Folder");
        dialog.setScene(new Scene(getUI()));      
        
        treeview.getSelectionModel().select(root);
        
        dialog.showAndWait();
    }

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }    

    @Override
    public void initialize(URL url, ResourceBundle rb) {         
        
        root = new TreeItem(new Favourites(null, true, FavoritesUtils.FAVORITES_ROOT_NAME, null, null, false), new ImageView(FavoritesUtils.IMAGE__FAVORITES));

        treeview.setRoot(root);
        root.setExpanded(true);
                
        treeview.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        FavoritesUtils.refreshTree(favServ, root, true);
    }
    
}
