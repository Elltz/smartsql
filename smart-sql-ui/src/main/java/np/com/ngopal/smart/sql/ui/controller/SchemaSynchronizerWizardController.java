package np.com.ngopal.smart.sql.ui.controller;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.AppSettingsService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.TechnologyType;
import np.com.ngopal.smart.sql.model.provider.ConnectionParamsProvider;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaSynchronizerWizardController extends BaseController implements Initializable {

    private SimpleObjectProperty<WizardPageInfo> currentPage = new SimpleObjectProperty<>();
    
    private static final ColorItem FOREGROUND_DEFAULT_ITEM = new ColorItem("Default", "000000");
    private static final ColorItem FOREGROUND_AUTOCONTRAST_ITEM = new ColorItem("Auto Contrast", "000000");
    private static final ColorItem FOREGROUND_COLOR_ITEM = new ColorItem("ffffff");

    private static final ColorItem BACKGROUND_COLOR_ITEM = new ColorItem("000000");

    
    private final String KEY__OPTION_ITEM__IGNORE_AUTOINC_TABLE     = "schema_sync__option_item__ignore_autoinc_table";
    private final String KEY__OPTION_ITEM__IGNORE_AVERAGE_ROW       = "schema_sync__option_item__ignore_average_row";
    private final String KEY__OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE  = "schema_sync__option_item__ignore_column_def_value";
    private final String KEY__OPTION_ITEM__IGNORE_DEFINER           = "schema_sync__option_item__ignore_definer";
    private final String KEY__OPTION_ITEM__IGNORE_ENDS              = "schema_sync__option_item__ignore_ends";
    private final String KEY__OPTION_ITEM__IGNORE_FOREIGN_KEYS      = "schema_sync__option_item__ignore_foreign_keys";
    private final String KEY__OPTION_ITEM__IGNORE_STARTS            = "schema_sync__option_item__ignore_starts";
    private final String KEY__OPTION_ITEM__IGNORE_TABLE_ENGINE      = "schema_sync__option_item__ignore_table_engine";

    private final String KEY__OPTION_ITEM__FORCE_COLUMN_ORDER       = "schema_sync__option_item__force_column_order";
        
        
    private final OptionItem OPTION_ITEM__IGNORE_AUTOINC_TABLE      = new OptionItem("Ignore auto increment table option", true, "Select to ignore auto increment table option.");
    private final OptionItem OPTION_ITEM__IGNORE_AVERAGE_ROW        = new OptionItem("Ignore average row length", true, "Select to ignore average row length  table  option.");
    private final OptionItem OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE   = new OptionItem("Ignore column default values", false, "Select to ignore column default values.");
    private final OptionItem OPTION_ITEM__IGNORE_DEFINER            = new OptionItem("Ignore DEFINER and SQL SECURITY clauses", true, "Select to ignore DEFINER and SQL SECURITY clauses for procedures, triggers, views and events.");
    private final OptionItem OPTION_ITEM__IGNORE_ENDS               = new OptionItem("Ignore ENDS clause", false, "Select to ignore END clause in event definition.");
    private final OptionItem OPTION_ITEM__IGNORE_FOREIGN_KEYS       = new OptionItem("Ignore foreign keys", false, "When selected, foreign keys are ignored during comparison and synchronization. Foreign keys won't be restored after synchronization, for example, if a referenced column has no primary key.");
    private final OptionItem OPTION_ITEM__IGNORE_STARTS             = new OptionItem("Ignore STARTS clause", false, "Select to ignore STARTS or AT clause in event definition.");
    private final OptionItem OPTION_ITEM__IGNORE_TABLE_ENGINE       = new OptionItem("Ignore table ingine", false, "Select to ignore table ingine.");
    
    private final OptionItem OPTION_ITEM__FORCE_COLUMN_ORDER        = new OptionItem("Force column order", true, "When selected, the columns order will be preserved and counts in tables difference. Otherwise difference in columns order will be ignored and objects will be equal.");
    
    
    
    private final FilterItem FILTER_ITEM__TABLES        = new FilterItem("Tables", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/table_tab.png")));
    private final FilterItem FILTER_ITEM__EVENTS        = new FilterItem("Events", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/event_tab.png")));
    private final FilterItem FILTER_ITEM__FUNCTIONS     = new FilterItem("Functions", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/function_tab.png")));
    private final FilterItem FILTER_ITEM__PROCEDURES    = new FilterItem("Procedures", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/storedprocs_tab.png")));
    private final FilterItem FILTER_ITEM__TRIGGERS      = new FilterItem("Triggers", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/trigger_tab.png")));
//    private final FilterItem FILTER_ITEM__UDFS          = new FilterItem("UDFs", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/function_tab.png")));
    private final FilterItem FILTER_ITEM__VIEWS         = new FilterItem("Views", new Image(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/ui/images/connection/view_tab.png")));
    
    private final String KEY__FILTER_ITEM__TABLES       = "schema_sync__filter_item__tables";
    private final String KEY__FILTER_ITEM__EVENTS       = "schema_sync__filter_item__events";
    private final String KEY__FILTER_ITEM__FUNCTIONS    = "schema_sync__filter_item__functions";
    private final String KEY__FILTER_ITEM__PROCEDURES   = "schema_sync__filter_item__procedures";
    private final String KEY__FILTER_ITEM__TRIGGERS     = "schema_sync__filter_item__triggers";
    private final String KEY__FILTER_ITEM__UDFS         = "schema_sync__filter_item__udfs";
    private final String KEY__FILTER_ITEM__VIEWS        = "schema_sync__filter_item__views";
    
    private SyncCompareInfo selectedInfo;
    
    @Inject
    AppSettingsService settignsService;

    @Inject
    ConnectionParamService db;
    
    private MysqlDBService leftDbService;
    private MysqlDBService rightDbService;

    private Stage dialog;
    
    @FXML
    private AnchorPane mainUI;
    
    
    @FXML
    private Label titleLabel;
    @FXML
    private Label descriptionLabel;
    

    @FXML
    private ToggleButton sourceAndTargetButton;
    @FXML
    private ToggleButton optionsButton;
    @FXML
    private ToggleButton objectFilterButton;
    
    
    @FXML
    private TitledPane sourceContainer;
    @FXML
    private ComboBox<ConnectionParams> leftSavedConnectionComboBox;
    @FXML
    private Button leftNewBttn;
    @FXML
    private Button leftCloneBttn;
    @FXML
    private Button leftSaveBttn;
    @FXML
    private Button leftRenameBttn;
    @FXML
    private Button leftDeleteBttn;
    @FXML
    private Button leftCheckBttn;
    
    @FXML
    private BorderPane leftParamBorderPane;
    @FXML
    private TextField leftAddress;
    @FXML
    private TextField leftUser;
    @FXML
    private PasswordField leftPass;
    @FXML
    private TextField leftPort;
    @FXML
    private CheckBox leftSavePasswordCheckBox;
    @FXML
    private CheckBox leftUseCompressedProtocolCheckBox;
    @FXML
    private TextField leftSessionIdleField;
    @FXML
    private RadioButton leftSessionIdleDefault;
    @FXML
    private RadioButton leftSessionIdleRadioButton;
    @FXML
    private ComboBox<Database> leftDatabase;
    @FXML
    private BorderPane leftSshParamBorderPane;
    @FXML
    private CheckBox leftUseSshTunnelingCheckBox;
    @FXML
    private GridPane leftSshTunnelingPane;
    @FXML
    private TextField leftSshHost;
    @FXML
    private TextField leftSshPort;
    @FXML
    private TextField leftSshUser;
    @FXML
    private TextField leftSshPassword;
    @FXML
    private CheckBox leftSshSavePasswordCheckBox;
    @FXML
    private Label leftSshPasswordLabel;
    @FXML
    private RadioButton leftSshPasswordTypeRadioButton;
    @FXML
    private RadioButton leftSshPrivateKeyTypeRadioButton;
    @FXML
    private TextField leftSshPrivateKeyField;
    @FXML
    private Label leftSshPrivateKeyLabel;
    @FXML
    private Button leftSshOpenPrivateKeyButton;
    @FXML
    private ComboBox<ColorItem> leftBackgroundColorComboBox;
    @FXML
    private ComboBox<ColorItem> leftForegroundColorComboBox;
    @FXML
    private RadioButton leftCustomSqlModeRadioButton;
    @FXML
    private TextField leftCustomSqlModeField;
    @FXML
    private TextField leftInitCommandsField;


    @FXML
    private TitledPane destinationContainer;
    @FXML
    private ComboBox<ConnectionParams> rightSavedConnectionComboBox;
    @FXML
    private Button rightNewBttn;
    @FXML
    private Button rightCloneBttn;
    @FXML
    private Button rightSaveBttn;
    @FXML
    private Button rightRenameBttn;
    @FXML
    private Button rightDeleteBttn;
    @FXML
    private Button rightCheckBttn;
    
    @FXML
    private BorderPane rightParamBorderPane;
    @FXML
    private TextField rightAddress;
    @FXML
    private TextField rightUser;
    @FXML
    private PasswordField rightPass;
    @FXML
    private TextField rightPort;
    @FXML
    private CheckBox rightSavePasswordCheckBox;
    @FXML
    private CheckBox rightUseCompressedProtocolCheckBox;
    @FXML
    private TextField rightSessionIdleField;
    @FXML
    private RadioButton rightSessionIdleDefault;
    @FXML
    private RadioButton rightSessionIdleRadioButton;
    @FXML
    private ComboBox<Database> rightDatabase;
    @FXML
    private BorderPane rightSshParamBorderPane;
    @FXML
    private CheckBox rightUseSshTunnelingCheckBox;
    @FXML
    private GridPane rightSshTunnelingPane;
    @FXML
    private TextField rightSshHost;
    @FXML
    private TextField rightSshPort;
    @FXML
    private TextField rightSshUser;
    @FXML
    private TextField rightSshPassword;
    @FXML
    private CheckBox rightSshSavePasswordCheckBox;
    @FXML
    private Label rightSshPasswordLabel;
    @FXML
    private RadioButton rightSshPasswordTypeRadioButton;
    @FXML
    private RadioButton rightSshPrivateKeyTypeRadioButton;
    @FXML
    private TextField rightSshPrivateKeyField;
    @FXML
    private Label rightSshPrivateKeyLabel;
    @FXML
    private Button rightSshOpenPrivateKeyButton;
    @FXML
    private ComboBox<ColorItem> rightBackgroundColorComboBox;
    @FXML
    private ComboBox<ColorItem> rightForegroundColorComboBox;
    @FXML
    private RadioButton rightCustomSqlModeRadioButton;
    @FXML
    private TextField rightCustomSqlModeField;
    @FXML
    private TextField rightInitCommandsField;
    

    @FXML
    private HBox sourceTargetButtonsPane;
    @FXML
    private HBox defaultsButtonsPane;
    @FXML
    private BorderPane optionsContainer;
    @FXML
    private TitledPane descriptionContainer;
    @FXML
    private Label descriptionInfoLabel;
    @FXML
    private BorderPane objectFilterContainer;

    @FXML
    private ListView<OptionItem> generalListView;
    @FXML
    private ListView<OptionItem> tablesListView;
    
    
    @FXML
    private ListView<FilterItem> filterLeftListView;
//    @FXML
//    private ListView<FilterItem> filterRightListView;
    @FXML
    private CheckBox filterSelectAllCheckBox;
//    @FXML
//    private CheckBox filterGroupByCheckBox;
//    @FXML
//    private CheckBox leftGroupedCheckbox;
//    @FXML
//    private CheckBox rightGroupedCheckbox;
    
    
    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button compareButton;
    @FXML
    private Button cancelButton;

    @Inject
    private ConnectionParamsProvider provider;

    private ObservableList<ConnectionParams> leftConnectionParamsList;
    private ObservableList<ConnectionParams> rightConnectionParamsList;

    private ObjectProperty<ConnectionParams> leftSelectedConnParam;
    private ObjectProperty<ConnectionParams> rightSelectedConnParam;


    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    private void disableLeftDatabase() {
        leftDatabase.getItems().clear();
        leftDatabase.setDisable(true);
    }
    
    @FXML
    public void refreshSourceDatabasesAction(ActionEvent event) {
        makeBusy(true);
        leftDatabase.getItems().clear();
        Thread th = new Thread(() -> {
            
            try {
                leftDbService.connect(leftSavedConnectionComboBox.getSelectionModel().getSelectedItem());
                List<Database> databases = leftDbService.getDatabases();
                Platform.runLater(() -> {
                    leftDatabase.setItems(FXCollections.observableArrayList(databases)); 
                    leftDatabase.setDisable(false);
                    makeBusy(false);
                });
            } catch (SQLException ex) {
                Platform.runLater(() -> {
                    ((MainController)getController()).showError("Error", ex.getMessage(), ex);
                    makeBusy(false);
                });
            }

        });
        th.setDaemon(true);
        th.start();
    }
    
    private void disableRightDatabase() {
        rightDatabase.getItems().clear();
        rightDatabase.setDisable(true);
    }
    
    @FXML
    public void refreshDestinationDatabasesAction(ActionEvent event) {
        makeBusy(true);
        rightDatabase.getItems().clear();
        Thread th = new Thread(() -> {
            
            try {
                rightDbService.connect(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem());
                List<Database> databases = rightDbService.getDatabases();
                Platform.runLater(() -> {
                    rightDatabase.setItems(FXCollections.observableArrayList(databases)); 
                    rightDatabase.setDisable(false);
                    makeBusy(false);
                });
            } catch (SQLException ex) {
                Platform.runLater(() -> {
                    ((MainController)getController()).showError("Error", ex.getMessage(), ex);
                    makeBusy(false);
                });
            }

        });
        th.setDaemon(true);
        th.start();
        
        
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        init();            
        
        ((ImageView)leftNewBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getNewButton_image());
        ((ImageView)leftCloneBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCloneButton_image());
        ((ImageView)leftSaveBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getSaveButton_image());
        ((ImageView)leftRenameBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRenameButton_image());
        ((ImageView)leftDeleteBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getDeleteButton_image());
        ((ImageView)leftCheckBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getTestConnectionButton_image());
        
        ((ImageView)rightNewBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getNewButton_image());
        ((ImageView)rightCloneBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCloneButton_image());
        ((ImageView)rightSaveBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getSaveButton_image());
        ((ImageView)rightRenameBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRenameButton_image());
        ((ImageView)rightDeleteBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getDeleteButton_image());
        ((ImageView)rightCheckBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getTestConnectionButton_image());
                
            
        // left side
        {            
        
            BooleanBinding leftSelectedItemIsNull = leftSavedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull();           
                        
            leftCloneBttn.disableProperty().bind(leftSelectedItemIsNull);
            leftSaveBttn.disableProperty().bind(leftSelectedItemIsNull);
            leftRenameBttn.disableProperty().bind(leftSelectedItemIsNull);
            leftDeleteBttn.disableProperty().bind(leftSelectedItemIsNull);
            leftParamBorderPane.disableProperty().bind(leftSelectedItemIsNull);
            leftSshParamBorderPane.disableProperty().bind(leftSelectedItemIsNull);
            leftSessionIdleField.disableProperty().bind(leftSessionIdleDefault.selectedProperty());
            leftSshTunnelingPane.disableProperty().bind(leftUseSshTunnelingCheckBox.selectedProperty().not());
            leftSshPrivateKeyField.disableProperty().bind(leftSshPasswordTypeRadioButton.selectedProperty());
            leftSshPrivateKeyLabel.disableProperty().bind(leftSshPasswordTypeRadioButton.selectedProperty());
            leftSshOpenPrivateKeyButton.disableProperty().bind(leftSshPasswordTypeRadioButton.selectedProperty());
            leftCustomSqlModeField.disableProperty().bind(leftCustomSqlModeRadioButton.selectedProperty().not());
        }
        
        // right side
        {
            BooleanBinding rightSelectedItemIsNull = rightSavedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull();
            
            rightCloneBttn.disableProperty().bind(rightSelectedItemIsNull);
            rightSaveBttn.disableProperty().bind(rightSelectedItemIsNull);
            rightRenameBttn.disableProperty().bind(rightSelectedItemIsNull);
            rightDeleteBttn.disableProperty().bind(rightSelectedItemIsNull);
            rightParamBorderPane.disableProperty().bind(rightSelectedItemIsNull);
            rightSshParamBorderPane.disableProperty().bind(rightSelectedItemIsNull);
            rightSessionIdleField.disableProperty().bind(rightSessionIdleDefault.selectedProperty());
            rightSshTunnelingPane.disableProperty().bind(rightUseSshTunnelingCheckBox.selectedProperty().not());
            rightSshPrivateKeyField.disableProperty().bind(rightSshPasswordTypeRadioButton.selectedProperty());
            rightSshPrivateKeyLabel.disableProperty().bind(rightSshPasswordTypeRadioButton.selectedProperty());
            rightSshOpenPrivateKeyButton.disableProperty().bind(rightSshPasswordTypeRadioButton.selectedProperty());
            rightCustomSqlModeField.disableProperty().bind(rightCustomSqlModeRadioButton.selectedProperty().not());
        }
        
        initOptionComponents();
        
        initFilterComponents();
        
        
        sourceAndTargetButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue == null || !newValue) {
                makeBusy(true);
                rightDatabase.getItems().clear();
                leftDatabase.getItems().clear();
                
                Thread th = new Thread(() -> {
                    try {
                        rightDbService.connect(rightParams());
                        List<Database> rightDatabases = rightDbService.getDatabases();
                        
                        leftDbService.connect(leftParams());
                        List<Database> leftDatabases = leftDbService.getDatabases();
                        
                        
                        Platform.runLater(() -> {
                            rightDatabase.setItems(FXCollections.observableArrayList(rightDatabases)); 
                            leftDatabase.setItems(FXCollections.observableArrayList(leftDatabases)); 
                            makeBusy(false);
                        });
                        
                        
                    } catch (SQLException ex) {
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                            makeBusy(false);
                            showPage(currentPage.get(), SOURCE_AND_TARGET);
                        });
                    }
                });
                th.setDaemon(true);
                th.start();
            }
        });
        
        new AutoCompleteComboBoxListener<Database>(leftDatabase);
        new AutoCompleteComboBoxListener<Database>(rightDatabase);
        
        leftDatabase.setConverter(new StringConverter<Database>() {
            @Override
            public String toString(Database object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public Database fromString(String string) {
                for (Database d: leftDatabase.getItems()) {
                    if (d.getName().equals(string)) {
                        return d;
                    }
                }
                
                return null;
            }
        });
        
        rightDatabase.setConverter(new StringConverter<Database>() {
            @Override
            public String toString(Database object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public Database fromString(String string) {
                for (Database d: rightDatabase.getItems()) {
                    if (d.getName().equals(string)) {
                        return d;
                    }
                }
                
                return null;
            }
        });
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setResizable(false);
        dialog.setScene(new Scene(mainUI));
        dialog.setTitle("New Schema Comparison");
    }

    
    private void initOptionComponents() {
        
        generalListView.setCellFactory(CheckBoxListCell.forListView(OptionItem::selectedProperty));
        generalListView.getItems().addAll(
            OPTION_ITEM__IGNORE_AUTOINC_TABLE,
            OPTION_ITEM__IGNORE_AVERAGE_ROW,
            OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE,
            OPTION_ITEM__IGNORE_DEFINER,
            OPTION_ITEM__IGNORE_ENDS,
            OPTION_ITEM__IGNORE_FOREIGN_KEYS,
            OPTION_ITEM__IGNORE_STARTS,
            OPTION_ITEM__IGNORE_TABLE_ENGINE
        );
        
        generalListView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends OptionItem> observable, OptionItem oldValue, OptionItem newValue) -> {
            descriptionInfoLabel.setText(newValue != null ? newValue.getDescription() : "");
        });
        
        
        tablesListView.setCellFactory(CheckBoxListCell.forListView(OptionItem::selectedProperty));
        tablesListView.getItems().addAll(
            OPTION_ITEM__FORCE_COLUMN_ORDER
        );
        
        tablesListView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends OptionItem> observable, OptionItem oldValue, OptionItem newValue) -> {
            descriptionInfoLabel.setText(newValue != null ? newValue.getDescription() : "");
        });
    }
    
    
    private void initFilterComponents() {
        
        filterSelectAllCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            FILTER_ITEM__TABLES.selectedProperty().set(newValue);
            FILTER_ITEM__EVENTS.selectedProperty().set(newValue);
            FILTER_ITEM__FUNCTIONS.selectedProperty().set(newValue);
            FILTER_ITEM__PROCEDURES.selectedProperty().set(newValue);
            FILTER_ITEM__TRIGGERS.selectedProperty().set(newValue);
//            FILTER_ITEM__UDFS.selectedProperty().set(newValue);
            FILTER_ITEM__VIEWS.selectedProperty().set(newValue);
            
//            leftGroupedCheckbox.setSelected(newValue);
//            rightGroupedCheckbox.setSelected(newValue);
        });
        
//        leftGroupedCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
//            FILTER_ITEM__TABLES.selectedProperty().set(newValue);
//        });
//        
//        rightGroupedCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
//            FILTER_ITEM__EVENTS.selectedProperty().set(newValue);
//            FILTER_ITEM__FUNCTIONS.selectedProperty().set(newValue);
//            FILTER_ITEM__PROCEDURES.selectedProperty().set(newValue);
//            FILTER_ITEM__TRIGGERS.selectedProperty().set(newValue);
//            FILTER_ITEM__UDFS.selectedProperty().set(newValue);
//            FILTER_ITEM__VIEWS.selectedProperty().set(newValue);
//        });
        
//        filterGroupByCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
//            leftGroupedCheckbox.setVisible(newValue != null && newValue);
//            rightGroupedCheckbox.setVisible(newValue != null && newValue);
//            leftGroupedCheckbox.setManaged(newValue != null && newValue);
//            rightGroupedCheckbox.setManaged(newValue != null && newValue);
//            
//            filterLeftListView.getItems().clear();
//            if (newValue) {
//                filterLeftListView.getItems().addAll(
//                    FILTER_ITEM__TABLES
//                ); 
//            } else {
//                filterLeftListView.getItems().addAll(
//                    FILTER_ITEM__EVENTS,
//                    FILTER_ITEM__FUNCTIONS,
//                    FILTER_ITEM__PROCEDURES
//                );
//            }
//            
//            filterRightListView.getItems().clear();
//            if (newValue) {
//                filterRightListView.getItems().addAll(
//                    FILTER_ITEM__EVENTS,   
//                    FILTER_ITEM__FUNCTIONS, 
//                    FILTER_ITEM__PROCEDURES,
//                    FILTER_ITEM__TRIGGERS,
//                    FILTER_ITEM__UDFS, 
//                    FILTER_ITEM__VIEWS    
//                ); 
//            } else {
//                filterRightListView.getItems().addAll(
//                    FILTER_ITEM__TABLES,
//                    FILTER_ITEM__TRIGGERS,
//                    FILTER_ITEM__UDFS,
//                    FILTER_ITEM__VIEWS
//                );
//            }
//        });
        
        
         
        filterLeftListView.setCellFactory(list -> {
            return new CheckBoxListCell<FilterItem>(FilterItem::selectedProperty) {                    
                @Override
                public void updateItem(FilterItem item, boolean empty) {
                    super.updateItem(item, empty);
                    
                    CheckBox checkBox = (CheckBox) getGraphic();
                    if (checkBox != null) {
                        checkBox.setGraphic(new ImageView(item.getIcon()));
                    }
                }
            };
        });
        filterLeftListView.getItems().addAll(
            FILTER_ITEM__TABLES,
            FILTER_ITEM__VIEWS,
            FILTER_ITEM__PROCEDURES,
            FILTER_ITEM__FUNCTIONS,
            FILTER_ITEM__EVENTS,
            FILTER_ITEM__TRIGGERS
        );       
        
//        filterRightListView.setCellFactory(list -> {
//            return new CheckBoxListCell<FilterItem>(FilterItem::selectedProperty) {                    
//                @Override
//                public void updateItem(FilterItem item, boolean empty) {
//                    super.updateItem(item, empty);
//                    
//                    CheckBox checkBox = (CheckBox) getGraphic();
//                    if (checkBox != null) {
//                        checkBox.setGraphic(new ImageView(item.getIcon()));
//                    }
//                }
//            };
//        });
    }
    

    private void updateLeftFields(ConnectionParams params) {        

        leftUseCompressedProtocolCheckBox.setSelected(params != null ? params.isUseCompressedProtocol() : true);
        leftAddress.setText(params != null ? params.getAddress() : "");
        leftUser.setText(params != null ? params.getUsername() : "");
        leftPass.setText(params != null ? params.getPassword() : "");
        leftPort.setText(params != null ? params.getPort() + "" : "");
        
        if (params != null && params.getDatabase() != null && !params.getDatabase().isEmpty()) {
            for (Database db: leftDatabase.getItems()) {
                if (db.getName() != null && db.getName().equals(params.getDatabase().get(0))) {
                    leftDatabase.getSelectionModel().select(db);
                    break;
                }
            }            
        }

        leftSessionIdleField.setText(params != null && params.getSessionIdleTimeout() > 0 ? params.getSessionIdleTimeout() + "" : "28800");
        leftSessionIdleRadioButton.setSelected(params != null && params.getSessionIdleTimeout() != 28800);
        leftSessionIdleDefault.setSelected(params == null || params.getSessionIdleTimeout() == 0 || params.getSessionIdleTimeout() == 28800);

        // Ssh module
        leftSshHost.setText(params != null ? params.getSshHost() : "");
        leftSshPort.setText(params != null ? params.getSshPort() + "" : "");
        leftSshUser.setText(params != null ? params.getSshUser() : "");
        leftSshPassword.setText(params != null ? params.getSshPassword() : "");
        leftUseSshTunnelingCheckBox.setSelected(params != null ? params.isUseSshTunneling() : false);
        leftSshPrivateKeyTypeRadioButton.setSelected(params != null && params.isUseSshPrivateKey());
        leftSshPasswordTypeRadioButton.setSelected(params == null || !params.isUseSshPrivateKey());
        leftSshPrivateKeyField.setText(params != null ? params.getSshPrivateKey() : "");
        
        // advanced
        if (params != null) {

            boolean found = false;
            for (ColorItem n : leftBackgroundColorComboBox.getItems()) {
                if (n.color != null && n.color.equals(params.getBackgroundColor())) {
                    leftBackgroundColorComboBox.getSelectionModel().select(n);
                    found = true;
                    break;
                }
            }

            // need set color to OTHER COLORS
            if (!found && params.getBackgroundColor() != null) {
                BACKGROUND_COLOR_ITEM.setColor(params.getBackgroundColor());
                leftBackgroundColorComboBox.getSelectionModel().select(BACKGROUND_COLOR_ITEM);
            }

            found = false;
            for (ColorItem n : leftForegroundColorComboBox.getItems()) {
                if (n.color != null && n.color.equals(params.getForegroundColor())) {
                    leftForegroundColorComboBox.getSelectionModel().select(n);
                    found = true;
                    break;
                }
            }

            if (!found && params.getForegroundColor() != null) {
                FOREGROUND_COLOR_ITEM.setColor(params.getForegroundColor());
                leftForegroundColorComboBox.getSelectionModel().select(FOREGROUND_COLOR_ITEM);
            }
        }

        if (params != null && params.isEnabledCustomSqlMode()) {
            leftCustomSqlModeRadioButton.setSelected(true);
        }

        leftCustomSqlModeField.setText(params != null ? params.getCustomSqlMode() : "");
        leftInitCommandsField.setText(params != null ? params.getInitCommands() : "");

    }
    
    
    private void updateRightFields(ConnectionParams params) {        

        rightUseCompressedProtocolCheckBox.setSelected(params != null ? params.isUseCompressedProtocol() : true);
        rightAddress.setText(params != null ? params.getAddress() : "");
        rightUser.setText(params != null ? params.getUsername() : "");
        rightPass.setText(params != null ? params.getPassword() : "");
        rightPort.setText(params != null ? params.getPort() + "" : "");
        
        if (params != null && params.getDatabase() != null && !params.getDatabase().isEmpty()) {
            for (Database db: rightDatabase.getItems()) {
                if (db.getName() != null && db.getName().equals(params.getDatabase().get(0))) {
                    rightDatabase.getSelectionModel().select(db);
                    break;
                }
            }            
        }

        rightSessionIdleField.setText(params != null && params.getSessionIdleTimeout() > 0 ? params.getSessionIdleTimeout() + "" : "28800");
        rightSessionIdleRadioButton.setSelected(params != null && params.getSessionIdleTimeout() != 28800);
        rightSessionIdleDefault.setSelected(params == null || params.getSessionIdleTimeout() == 0 || params.getSessionIdleTimeout() == 28800);

        // Ssh module
        rightSshHost.setText(params != null ? params.getSshHost() : "");
        rightSshPort.setText(params != null ? params.getSshPort() + "" : "");
        rightSshUser.setText(params != null ? params.getSshUser() : "");
        rightSshPassword.setText(params != null ? params.getSshPassword() : "");
        rightUseSshTunnelingCheckBox.setSelected(params != null ? params.isUseSshTunneling() : false);
        rightSshPrivateKeyTypeRadioButton.setSelected(params != null && params.isUseSshPrivateKey());
        rightSshPasswordTypeRadioButton.setSelected(params == null || !params.isUseSshPrivateKey());
        rightSshPrivateKeyField.setText(params != null ? params.getSshPrivateKey() : "");
        
        // advanced
        if (params != null) {

            boolean found = false;
            for (ColorItem n : rightBackgroundColorComboBox.getItems()) {
                if (n.color != null && n.color.equals(params.getBackgroundColor())) {
                    rightBackgroundColorComboBox.getSelectionModel().select(n);
                    found = true;
                    break;
                }
            }

            // need set color to OTHER COLORS
            if (!found && params.getBackgroundColor() != null) {
                BACKGROUND_COLOR_ITEM.setColor(params.getBackgroundColor());
                rightBackgroundColorComboBox.getSelectionModel().select(BACKGROUND_COLOR_ITEM);
            }

            found = false;
            for (ColorItem n : leftForegroundColorComboBox.getItems()) {
                if (n.color != null && n.color.equals(params.getForegroundColor())) {
                    rightForegroundColorComboBox.getSelectionModel().select(n);
                    found = true;
                    break;
                }
            }

            if (!found && params.getForegroundColor() != null) {
                FOREGROUND_COLOR_ITEM.setColor(params.getForegroundColor());
                rightForegroundColorComboBox.getSelectionModel().select(FOREGROUND_COLOR_ITEM);
            }
        }

        if (params != null && params.isEnabledCustomSqlMode()) {
            rightCustomSqlModeRadioButton.setSelected(true);
        }

        rightCustomSqlModeField.setText(params != null ? params.getCustomSqlMode() : "");
        rightInitCommandsField.setText(params != null ? params.getInitCommands() : "");

    }

    public ListCell<ConnectionParams> getConnectionParamsListCell() {
        return new ListCell<ConnectionParams>() {
            @Override
            protected void updateItem(ConnectionParams item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (item != null) {
                    setText(item.getName());
                }
            }
        };
    }

    private void init() {     

        BooleanBinding notValidDatabases = Bindings.isNull(leftDatabase.getSelectionModel().selectedItemProperty()).or(Bindings.isNull(rightDatabase.getSelectionModel().selectedItemProperty()));
        
        backButton.disableProperty().bind(Bindings.equal(SOURCE_AND_TARGET, currentPage));
        nextButton.disableProperty().bind(Bindings.equal(OBJECT_FILTER, currentPage));
        compareButton.disableProperty().bind(notValidDatabases);
        
        currentPage.addListener((ObservableValue<? extends WizardPageInfo> observable, WizardPageInfo oldValue, WizardPageInfo newValue) -> {
            
            if (newValue == OPTIONS) {
                optionsButton.setSelected(true);
                
            } else  if (newValue == OBJECT_FILTER) {
                objectFilterButton.setSelected(true);
                
            } else {                    
                sourceAndTargetButton.setSelected(true);
            }
        });
        
        optionsButton.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            if (optionsButton.isSelected()) {
                event.consume();
            }
        });
        
        objectFilterButton.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            if (objectFilterButton.isSelected()) {
                event.consume();
            }
        });
        
        sourceAndTargetButton.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            if (sourceAndTargetButton.isSelected()) {
                event.consume();
            }
        });
        
        // left side
        {
            leftSelectedConnParam = new SimpleObjectProperty<ConnectionParams>() {
                @Override
                protected void invalidated() {
                    super.invalidated(); 
                    get();
                }            
            };
            leftSelectedConnParam.bind(leftSavedConnectionComboBox.getSelectionModel().selectedItemProperty());

            leftConnectionParamsList = FXCollections.observableArrayList();
            leftConnectionParamsList.addAll(db.getAll());

            Collections.sort(leftConnectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
                return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
            });

            leftSavePasswordCheckBox.setSelected(true);
            leftSshSavePasswordCheckBox.setSelected(true);

            leftSavedConnectionComboBox.setButtonCell(getConnectionParamsListCell());
            leftSavedConnectionComboBox.setCellFactory((ListView<ConnectionParams> param) -> getConnectionParamsListCell());
            leftSavedConnectionComboBox.setConverter(new StringConverter<ConnectionParams>() {
                @Override
                public String toString(ConnectionParams object) {
                    return object != null ? object.getName() : "";
                }

                @Override
                public ConnectionParams fromString(String string) {
                    for (ConnectionParams cp : leftConnectionParamsList) {
                        if (cp.getName().equals(string)) {
                            return cp;
                        }
                    }

                    return null;
                }
            });

            new AutoCompleteComboBoxListener<>(leftSavedConnectionComboBox);

            leftSavedConnectionComboBox.setItems(leftConnectionParamsList);

            leftBackgroundColorComboBox.setItems(defaultBackgroundColors());
            leftBackgroundColorComboBox.setCellFactory((ListView<ColorItem> list) -> new ColorRectCell(leftBackgroundColorComboBox, BACKGROUND_COLOR_ITEM));
            leftBackgroundColorComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
                if (newValue != null) {
                    Color color = Color.web(newValue.color).invert();
                    FOREGROUND_AUTOCONTRAST_ITEM.setColor(color.toString().substring(2, 8).toUpperCase());

                    if (leftForegroundColorComboBox.getSelectionModel().getSelectedItem() != FOREGROUND_COLOR_ITEM) {
                        leftForegroundColorComboBox.getSelectionModel().clearSelection();
                    }
                    leftForegroundColorComboBox.getItems().remove(FOREGROUND_AUTOCONTRAST_ITEM);
                    leftForegroundColorComboBox.getItems().add(1, FOREGROUND_AUTOCONTRAST_ITEM);
                }
            });

            leftForegroundColorComboBox.setItems(defaultForegroundColors());
            leftForegroundColorComboBox.setCellFactory((ListView<ColorItem> list) -> new ColorRectCell(leftForegroundColorComboBox, FOREGROUND_COLOR_ITEM));
        }
        
        
        // right side
        {
            rightSelectedConnParam = new SimpleObjectProperty<ConnectionParams>() {
                @Override
                protected void invalidated() {
                    super.invalidated(); 
                    get();
                }            
            };
            rightSelectedConnParam.bind(rightSavedConnectionComboBox.getSelectionModel().selectedItemProperty());

            rightConnectionParamsList = FXCollections.observableArrayList();
            rightConnectionParamsList.addAll(db.getAll());

            Collections.sort(rightConnectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
                return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
            });

            rightSavePasswordCheckBox.setSelected(true);
            rightSshSavePasswordCheckBox.setSelected(true);

            rightSavedConnectionComboBox.setButtonCell(getConnectionParamsListCell());
            rightSavedConnectionComboBox.setCellFactory((ListView<ConnectionParams> param) -> getConnectionParamsListCell());
            rightSavedConnectionComboBox.setConverter(new StringConverter<ConnectionParams>() {
                @Override
                public String toString(ConnectionParams object) {
                    return object != null ? object.getName() : "";
                }

                @Override
                public ConnectionParams fromString(String string) {
                    for (ConnectionParams cp : rightConnectionParamsList) {
                        if (cp.getName().equals(string)) {
                            return cp;
                        }
                    }

                    return null;
                }
            });

            new AutoCompleteComboBoxListener<>(rightSavedConnectionComboBox);

            rightSavedConnectionComboBox.setItems(rightConnectionParamsList);

            rightBackgroundColorComboBox.setItems(defaultBackgroundColors());
            rightBackgroundColorComboBox.setCellFactory((ListView<ColorItem> list) -> new ColorRectCell(rightBackgroundColorComboBox, BACKGROUND_COLOR_ITEM));
            rightBackgroundColorComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
                if (newValue != null) {
                    Color color = Color.web(newValue.color).invert();
                    FOREGROUND_AUTOCONTRAST_ITEM.setColor(color.toString().substring(2, 8).toUpperCase());

                    if (rightForegroundColorComboBox.getSelectionModel().getSelectedItem() != FOREGROUND_COLOR_ITEM) {
                        rightForegroundColorComboBox.getSelectionModel().clearSelection();
                    }
                    rightForegroundColorComboBox.getItems().remove(FOREGROUND_AUTOCONTRAST_ITEM);
                    rightForegroundColorComboBox.getItems().add(1, FOREGROUND_AUTOCONTRAST_ITEM);
                }
            });

            rightForegroundColorComboBox.setItems(defaultForegroundColors());
            rightForegroundColorComboBox.setCellFactory((ListView<ColorItem> list) -> new ColorRectCell(rightForegroundColorComboBox, FOREGROUND_COLOR_ITEM));
        }
        
        
        propertyListeners();
    }

    static class ColorItem extends Label {

        String color;
        String text;

        ColorItem(String text, String color) {
            this.color = color;
            this.text = text;
            setMaxWidth(Double.MAX_VALUE);
            if (color != null) {
                if (!color.startsWith("#")) {
                    color = "#" + color;
                }
                setStyle("-fx-background-color: " + color);
            }
        }

        ColorItem(String color) {
            this(null, color);
        }

        public void setColor(String color) {
            this.color = color;
            if (color != null) {
                if (!color.startsWith("#")) {
                    color = "#" + color;
                }
                setStyle("-fx-background-color: " + color);
            } else {
                setStyle("");
            }
        }
    }

    static class ColorRectCell extends ListCell<ColorItem> {

        private final ColorItem otherColors;
        private final ComboBox<ColorItem> combobox;

        public ColorRectCell(ComboBox<ColorItem> combobox, ColorItem otherColors) {
            this.combobox = combobox;
            this.otherColors = otherColors;
        }

        @Override
        public void updateItem(ColorItem item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                if (item != null && otherColors != item) {
                    if (item.text != null) {
                        Rectangle rect = new Rectangle(50, 20);
                        rect.setFill(Color.web(item.color));

                        HBox h = new HBox(rect, new Label(item.text));
                        h.setAlignment(Pos.CENTER_LEFT);
                        h.setSpacing(10);
                        setGraphic(h);
                    } else {
                        Rectangle rect = new Rectangle(getListView().getWidth() - 50, 20);
                        rect.setFill(Color.web(item.color));
                        setGraphic(rect);
                    }
                } else {
                    ColorPicker l = new ColorPicker(Color.web(otherColors.color));
                    l.getStyleClass().add(ColorPicker.STYLE_CLASS_BUTTON);
                    l.setPrefSize(getListView().getWidth() - 50, 20);
                    l.setOnMousePressed((MouseEvent event) -> {
                        combobox.getSelectionModel().select(otherColors);
                    });
                    l.setOnAction((ActionEvent event) -> {
                        otherColors.setColor(l.getValue().toString().substring(2, 8).toUpperCase());
                        combobox.getItems().remove(otherColors);
                        combobox.getItems().add(otherColors);
                        combobox.getSelectionModel().select(otherColors);
                    });

                    setGraphic(l);
                }
            }
        }
    }

    
    private void propertyListeners() {
        // left side
        {
            leftSelectedConnParam.addListener(
                    (ObservableValue<? extends ConnectionParams> observable, ConnectionParams oldValue, ConnectionParams newValue) -> {
                        updateLeftFields(newValue);
                    });

            leftUseCompressedProtocolCheckBox.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null) {
                            if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().isUseCompressedProtocol(), newValue)) {
                                leftSelectedConnParam.get().setUseCompressedProtocol(newValue);
                            }
                        }
                    });

            leftAddress.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getAddress(), newValue)) {
                            leftSelectedConnParam.get().setAddress(newValue);
                        }
                    });
            leftUser.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getUsername(), newValue)) {
                            leftSelectedConnParam.get().setUsername(newValue);
                        }
                    });

            leftPass.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getPassword(), newValue)) {
                            leftSelectedConnParam.get().setPassword(newValue);
                        }

                        leftSavePasswordCheckBox.setSelected(newValue != null);
                    });

            leftPort.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (newValue.matches("\\d*")) {
                    try {
                        int value = Integer.parseInt(newValue);
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getPort(), value)) {
                            leftSelectedConnParam.get().setPort(value);
                        }
                    } catch (NumberFormatException ex) {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getPort(), 0)) {
                            leftSelectedConnParam.get().setPort(0);
                        }
                    }
                } else {
                    leftPort.setText(oldValue);
                }
            });

            leftSessionIdleField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (newValue.matches("\\d*")) {
                    try {
                        int value = Integer.parseInt(newValue);
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSessionIdleTimeout(), value)) {
                            leftSelectedConnParam.get().setSessionIdleTimeout(value);
                        }
                    } catch (NumberFormatException ex) {
                    }
                } else {
                    leftSessionIdleField.setText(oldValue);
                }
            });

            leftDatabase.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
                if (leftSelectedConnParam.get() != null) {
                    leftSelectedConnParam.get().setDatabase(newValue != null ? Arrays.asList(new String[] {newValue.getName()}) : null);
                }
            });

            leftBackgroundColorComboBox.valueProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
                if (leftSelectedConnParam.get() != null && newValue != null && !Objects.equal(leftSelectedConnParam.get().getBackgroundColor(), newValue.color)) {
                    leftSelectedConnParam.get().setBackgroundColor(newValue.color);
                }
            });

            leftForegroundColorComboBox.valueProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
                if (leftSelectedConnParam.get() != null && newValue != null && !Objects.equal(leftSelectedConnParam.get().getForegroundColor(), newValue.color)) {
                    leftSelectedConnParam.get().setForegroundColor(newValue.color);
                }
            });

            leftSshHost.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSshHost(), newValue)) {
                            leftSelectedConnParam.get().setSshHost(newValue);
                        }
                    });

            leftSshPort.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (newValue.matches("\\d*")) {
                            try {
                                int port = Integer.parseInt(newValue);
                                if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSshPort(), port)) {
                                    leftSelectedConnParam.get().setSshPort(port);
                                }
                            } catch (NumberFormatException ex) {
                                if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSshPort(), 0)) {
                                    leftSelectedConnParam.get().setSshPort(0);
                                }
                            }
                        } else {
                            leftSshPort.setText(oldValue);
                        }
                    });

            leftSshUser.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSshUser(), newValue)) {
                            leftSelectedConnParam.get().setSshUser(newValue);
                        }
                    });

            leftSshPassword.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSshPassword(), newValue)) {
                            leftSelectedConnParam.get().setSshPassword(newValue);
                        }

                        leftSshSavePasswordCheckBox.setSelected(newValue != null);
                    });

            leftUseSshTunnelingCheckBox.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null) {
                            if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().isUseSshTunneling(), newValue)) {
                                leftSelectedConnParam.get().setUseSshTunneling(newValue);
                            }
                        }
                    });

            leftSshPrivateKeyField.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getSshPrivateKey(), newValue)) {
                            leftSelectedConnParam.get().setSshPrivateKey(newValue);
                        }
                    });

            leftSshPrivateKeyTypeRadioButton.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null) {
                            if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().isUseSshPrivateKey(), newValue)) {
                                leftSelectedConnParam.get().setUseSshPrivateKey(newValue);
                            }
                        }
                    });

            leftSshPasswordTypeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                leftSshPasswordLabel.setText(newValue != null && newValue ? "Password" : "Passphrase");
                leftSshSavePasswordCheckBox.setText(newValue != null && newValue ? "Save Password" : "Save Passphrase");
            });

            leftSessionIdleDefault.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                if (newValue != null && newValue) {
                    leftSessionIdleField.setText("28800");
                }
            });

            leftCustomSqlModeField.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getCustomSqlMode(), newValue)) {
                            leftSelectedConnParam.get().setCustomSqlMode(newValue);
                        }
                    });

            leftInitCommandsField.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().getInitCommands(), newValue)) {
                            leftSelectedConnParam.get().setInitCommands(newValue);
                        }
                    });

            leftCustomSqlModeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                if (leftSelectedConnParam.get() != null && !Objects.equal(leftSelectedConnParam.get().isEnabledCustomSqlMode(), newValue)) {
                    leftSelectedConnParam.get().setEnabledCustomSqlMode(newValue);
                }
            });
        }
        
        // right side
        {
            rightSelectedConnParam.addListener(
                    (ObservableValue<? extends ConnectionParams> observable, ConnectionParams oldValue, ConnectionParams newValue) -> {
                        updateRightFields(newValue);
                    });

            rightUseCompressedProtocolCheckBox.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null) {
                            if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().isUseCompressedProtocol(), newValue)) {
                                rightSelectedConnParam.get().setUseCompressedProtocol(newValue);
                            }
                        }
                    });

            rightAddress.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getAddress(), newValue)) {
                            rightSelectedConnParam.get().setAddress(newValue);
                        }
                    });
            rightUser.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getUsername(), newValue)) {
                            rightSelectedConnParam.get().setUsername(newValue);
                        }
                    });

            rightPass.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getPassword(), newValue)) {
                            rightSelectedConnParam.get().setPassword(newValue);
                        }

                        rightSavePasswordCheckBox.setSelected(newValue != null);
                    });

            rightPort.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (newValue.matches("\\d*")) {
                    try {
                        int value = Integer.parseInt(newValue);
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getPort(), value)) {
                            rightSelectedConnParam.get().setPort(value);
                        }
                    } catch (NumberFormatException ex) {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getPort(), 0)) {
                            rightSelectedConnParam.get().setPort(0);
                        }
                    }
                } else {
                    rightPort.setText(oldValue);
                }
            });

            rightSessionIdleField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (newValue.matches("\\d*")) {
                    try {
                        int value = Integer.parseInt(newValue);
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSessionIdleTimeout(), value)) {
                            rightSelectedConnParam.get().setSessionIdleTimeout(value);
                        }
                    } catch (NumberFormatException ex) {
                    }
                } else {
                    rightSessionIdleField.setText(oldValue);
                }
            });

            rightDatabase.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
                if (rightSelectedConnParam.get() != null) {
                    rightSelectedConnParam.get().setDatabase(newValue != null ? Arrays.asList(new String[] {newValue.getName()}) : null);
                }
            });

            rightBackgroundColorComboBox.valueProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
                if (rightSelectedConnParam.get() != null && newValue != null && !Objects.equal(rightSelectedConnParam.get().getBackgroundColor(), newValue.color)) {
                    rightSelectedConnParam.get().setBackgroundColor(newValue.color);
                }
            });

            rightForegroundColorComboBox.valueProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
                if (rightSelectedConnParam.get() != null && newValue != null && !Objects.equal(rightSelectedConnParam.get().getForegroundColor(), newValue.color)) {
                    rightSelectedConnParam.get().setForegroundColor(newValue.color);
                }
            });

            rightSshHost.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSshHost(), newValue)) {
                            rightSelectedConnParam.get().setSshHost(newValue);
                        }
                    });

            rightSshPort.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (newValue.matches("\\d*")) {
                            try {
                                int port = Integer.parseInt(newValue);
                                if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSshPort(), port)) {
                                    rightSelectedConnParam.get().setSshPort(port);
                                }
                            } catch (NumberFormatException ex) {
                                if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSshPort(), 0)) {
                                    rightSelectedConnParam.get().setSshPort(0);
                                }
                            }
                        } else {
                            rightSshPort.setText(oldValue);
                        }
                    });

            rightSshUser.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSshUser(), newValue)) {
                            rightSelectedConnParam.get().setSshUser(newValue);
                        }
                    });

            rightSshPassword.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSshPassword(), newValue)) {
                            rightSelectedConnParam.get().setSshPassword(newValue);
                        }

                        rightSshSavePasswordCheckBox.setSelected(newValue != null);
                    });

            rightUseSshTunnelingCheckBox.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null) {
                            if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().isUseSshTunneling(), newValue)) {
                                rightSelectedConnParam.get().setUseSshTunneling(newValue);
                            }
                        }
                    });

            rightSshPrivateKeyField.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getSshPrivateKey(), newValue)) {
                            rightSelectedConnParam.get().setSshPrivateKey(newValue);
                        }
                    });

            rightSshPrivateKeyTypeRadioButton.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null) {
                            if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().isUseSshPrivateKey(), newValue)) {
                                rightSelectedConnParam.get().setUseSshPrivateKey(newValue);
                            }
                        }
                    });

            rightSshPasswordTypeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                rightSshPasswordLabel.setText(newValue != null && newValue ? "Password" : "Passphrase");
                rightSshSavePasswordCheckBox.setText(newValue != null && newValue ? "Save Password" : "Save Passphrase");
            });

            rightSessionIdleDefault.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                if (newValue != null && newValue) {
                    rightSessionIdleField.setText("28800");
                }
            });

            rightCustomSqlModeField.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getCustomSqlMode(), newValue)) {
                            rightSelectedConnParam.get().setCustomSqlMode(newValue);
                        }
                    });

            rightInitCommandsField.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().getInitCommands(), newValue)) {
                            rightSelectedConnParam.get().setInitCommands(newValue);
                        }
                    });

            rightCustomSqlModeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                if (rightSelectedConnParam.get() != null && !Objects.equal(rightSelectedConnParam.get().isEnabledCustomSqlMode(), newValue)) {
                    rightSelectedConnParam.get().setEnabledCustomSqlMode(newValue);
                }
            });
        }
    }

    private void addConnectionNameList(ConnectionParams connectionParam, ObservableList<ConnectionParams> paramsList, ComboBox<ConnectionParams> combobox, boolean select) {
        connectionParam.setType(TechnologyType.MYSQL);

        paramsList.add(connectionParam);
        Collections.sort(paramsList, (ConnectionParams o1, ConnectionParams o2) -> {
            return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
        });

        if (select) {
            combobox.getSelectionModel().select(connectionParam);
        }
    }


    private ObservableList<ColorItem> defaultBackgroundColors() {
        ObservableList<ColorItem> colorItems = FXCollections.observableArrayList();
        colorItems.add(new ColorItem("ffffff"));
        colorItems.add(new ColorItem("ffe3e3"));
        colorItems.add(new ColorItem("f0fff0"));
        colorItems.add(new ColorItem("e0ecff"));
        colorItems.add(new ColorItem("fffacd"));
        colorItems.add(new ColorItem("f4ecff"));
        colorItems.add(new ColorItem("f9ffef"));
        colorItems.add(new ColorItem("ffe6ff"));
        colorItems.add(new ColorItem("cffafe"));
        colorItems.add(new ColorItem("eaeaae"));
        colorItems.add(new ColorItem("fff0f5"));
        colorItems.add(BACKGROUND_COLOR_ITEM);

        return colorItems;
    }

    private ObservableList<ColorItem> defaultForegroundColors() {
        ObservableList<ColorItem> colorItems = FXCollections.observableArrayList();
        colorItems.add(FOREGROUND_DEFAULT_ITEM);
        colorItems.add(FOREGROUND_AUTOCONTRAST_ITEM);
        colorItems.add(FOREGROUND_COLOR_ITEM);

        return colorItems;
    }


   
    public void show(ConnectionParams params) {
        
        leftDbService = new MysqlDBService(null);
        rightDbService = new MysqlDBService(null);
        
        leftSavedConnectionComboBox.getSelectionModel().select(params);
                
        nextAction(null);
        
        dialog.showAndWait();
    }
    
    
    
    @FXML
    void sourceAndTargetAction(ActionEvent event) {
        showPage(currentPage.get(), SOURCE_AND_TARGET);
    }
    
    @FXML
    void optionsAction(ActionEvent event) {
        showPage(currentPage.get(), OPTIONS);
    }
    
    @FXML
    void objectFilterAction(ActionEvent event) {
        showPage(currentPage.get(), OBJECT_FILTER);
    }
    
    
    @FXML
    void copyToRightAction(ActionEvent event) {
        
        Database dbLeft = leftDatabase.getSelectionModel().getSelectedItem();
        
        rightSavedConnectionComboBox.getSelectionModel().clearSelection();
        rightSavedConnectionComboBox.getSelectionModel().select(leftSavedConnectionComboBox.getSelectionModel().getSelectedItem());
        
        rightDatabase.getSelectionModel().select(dbLeft);
    }
    
    @FXML
    void copyToBothAction(ActionEvent event) {
        Database dbLeft = leftDatabase.getSelectionModel().getSelectedItem();
        Database dbRight = rightDatabase.getSelectionModel().getSelectedItem();
        
        ConnectionParams left = leftSavedConnectionComboBox.getSelectionModel().getSelectedItem();
        
        leftSavedConnectionComboBox.getSelectionModel().clearSelection();
        leftSavedConnectionComboBox.getSelectionModel().select(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem());
        
        rightSavedConnectionComboBox.getSelectionModel().clearSelection();
        rightSavedConnectionComboBox.getSelectionModel().select(left);
        
        leftDatabase.getSelectionModel().select(dbRight);
        rightDatabase.getSelectionModel().select(dbLeft);
    }
    
    @FXML
    void copyToLeftAction(ActionEvent event) {
        Database dbRight = rightDatabase.getSelectionModel().getSelectedItem();
        
        leftSavedConnectionComboBox.getSelectionModel().clearSelection();
        leftSavedConnectionComboBox.getSelectionModel().select(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem());
        
        leftDatabase.getSelectionModel().select(dbRight);
    }
    
    @FXML
    void saveMyDefaultsAction(ActionEvent event) {     
        if (currentPage.get() == OPTIONS) {
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_AUTOINC_TABLE, OPTION_ITEM__IGNORE_AUTOINC_TABLE.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_AVERAGE_ROW, OPTION_ITEM__IGNORE_AVERAGE_ROW.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE, OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_DEFINER, OPTION_ITEM__IGNORE_DEFINER.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_ENDS, OPTION_ITEM__IGNORE_ENDS.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_FOREIGN_KEYS, OPTION_ITEM__IGNORE_FOREIGN_KEYS.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_STARTS, OPTION_ITEM__IGNORE_STARTS.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__IGNORE_TABLE_ENGINE, OPTION_ITEM__IGNORE_TABLE_ENGINE.toSettingsValue());
            settignsService.putSettingValue(KEY__OPTION_ITEM__FORCE_COLUMN_ORDER, OPTION_ITEM__FORCE_COLUMN_ORDER.toSettingsValue());        
            
        } else if (currentPage.get() == OBJECT_FILTER) {
            settignsService.putSettingValue(KEY__FILTER_ITEM__TABLES, FILTER_ITEM__TABLES.toSettingsValue());
            settignsService.putSettingValue(KEY__FILTER_ITEM__EVENTS, FILTER_ITEM__EVENTS.toSettingsValue());
            settignsService.putSettingValue(KEY__FILTER_ITEM__FUNCTIONS, FILTER_ITEM__FUNCTIONS.toSettingsValue());
            settignsService.putSettingValue(KEY__FILTER_ITEM__PROCEDURES, FILTER_ITEM__PROCEDURES.toSettingsValue());
            settignsService.putSettingValue(KEY__FILTER_ITEM__TRIGGERS, FILTER_ITEM__TRIGGERS.toSettingsValue());
//            settignsService.putSettingValue(KEY__FILTER_ITEM__UDFS, FILTER_ITEM__UDFS.toSettingsValue());
            settignsService.putSettingValue(KEY__FILTER_ITEM__VIEWS, FILTER_ITEM__VIEWS.toSettingsValue());
        }
    }
    
    @FXML
    void devartDefaultsAction(ActionEvent event) { 
        if (currentPage.get() == OPTIONS) {
            OPTION_ITEM__IGNORE_AUTOINC_TABLE.toDefultValue();
            OPTION_ITEM__IGNORE_AVERAGE_ROW.toDefultValue();
            OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE.toDefultValue();
            OPTION_ITEM__IGNORE_DEFINER.toDefultValue();
            OPTION_ITEM__IGNORE_ENDS.toDefultValue();
            OPTION_ITEM__IGNORE_FOREIGN_KEYS.toDefultValue();
            OPTION_ITEM__IGNORE_STARTS.toDefultValue();
            OPTION_ITEM__IGNORE_TABLE_ENGINE.toDefultValue();
            OPTION_ITEM__FORCE_COLUMN_ORDER.toDefultValue();        
            
        } else if (currentPage.get() == OBJECT_FILTER) {
            FILTER_ITEM__TABLES.toDefultValue();
            FILTER_ITEM__EVENTS.toDefultValue();
            FILTER_ITEM__FUNCTIONS.toDefultValue();
            FILTER_ITEM__PROCEDURES.toDefultValue();
            FILTER_ITEM__TRIGGERS.toDefultValue();
//            FILTER_ITEM__UDFS.toDefultValue();
            FILTER_ITEM__VIEWS.toDefultValue();
        }
    }
    
    @FXML
    void myDefaultsAction(ActionEvent event) {
        if (currentPage.get() == OPTIONS) {
            OPTION_ITEM__IGNORE_AUTOINC_TABLE.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_AUTOINC_TABLE));
            OPTION_ITEM__IGNORE_AVERAGE_ROW.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_AVERAGE_ROW));
            OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE));
            OPTION_ITEM__IGNORE_DEFINER.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_DEFINER));
            OPTION_ITEM__IGNORE_ENDS.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_ENDS));
            OPTION_ITEM__IGNORE_FOREIGN_KEYS.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_FOREIGN_KEYS));
            OPTION_ITEM__IGNORE_STARTS.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_STARTS));
            OPTION_ITEM__IGNORE_TABLE_ENGINE.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__IGNORE_TABLE_ENGINE));
            OPTION_ITEM__FORCE_COLUMN_ORDER.fromSettingsValue(settignsService.getSettingValue(KEY__OPTION_ITEM__FORCE_COLUMN_ORDER));
            
        } else if (currentPage.get() == OBJECT_FILTER) {
            FILTER_ITEM__TABLES.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__TABLES));
            FILTER_ITEM__EVENTS.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__EVENTS));
            FILTER_ITEM__FUNCTIONS.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__FUNCTIONS));
            FILTER_ITEM__PROCEDURES.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__PROCEDURES));
            FILTER_ITEM__TRIGGERS.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__TRIGGERS));
//            FILTER_ITEM__UDFS.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__UDFS));
            FILTER_ITEM__VIEWS.fromSettingsValue(settignsService.getSettingValue(KEY__FILTER_ITEM__VIEWS));
        }
    }
    
    @FXML
    void backAction(ActionEvent event) {
        WizardPageInfo prev = currentPage.get();
        WizardPageInfo next;
        if (prev != null) {
            if (prev == OBJECT_FILTER) {
                next = OPTIONS;
            } else {
                next = SOURCE_AND_TARGET;
            }
        } else {
            next = SOURCE_AND_TARGET;
        }
        
        showPage(prev, next);
    }
    
    private void showPage(WizardPageInfo prev, WizardPageInfo next) {
        if (prev != next) {

            currentPage.set(next);

            if (prev != null) {
                prev.hidePage();
            }
            if (next != null) {
                next.showPage();
            }
        } else {
            sourceAndTargetButton.fire();
            ((MainController) getController()).showError("SmartSQL Error", "Please select different database source and target to compare.", null, dialog);
        }
    }
    
    @FXML
    void nextAction(ActionEvent event) {
        WizardPageInfo prev = currentPage.get();
        if (prev != null) {
            if (prev == SOURCE_AND_TARGET) {                
                showPage(prev, OPTIONS);
            } else {
                showPage(prev, OBJECT_FILTER);
            }
        } else {
            showPage(prev, SOURCE_AND_TARGET);
        }
    }
    
    
    @FXML
    void compareAction(ActionEvent event) {
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            Database left = null;
            Database right = null;

            try {
                left = DatabaseCache.getInstance().syncDatabase(leftDatabase.getSelectionModel().getSelectedItem().getName(), leftSavedConnectionComboBox.getValue().cacheKey(), leftDbService, true);
                right = DatabaseCache.getInstance().syncDatabase(rightDatabase.getSelectionModel().getSelectedItem().getName(), rightSavedConnectionComboBox.getValue().cacheKey(), rightDbService, true);
            } catch (SQLException ex) {
                log.error("Error", ex);
                makeBusy(false);
                return;
            }

            ConnectionParams leftParams = leftParams();
            ConnectionParams rightParams = rightParams();

            if (left != null && right != null && (left.getName().equals(right.getName()) && leftParams.cacheKey().equals(rightParams.cacheKey()))) {            
                DialogHelper.showError(getController(), "SmartSQL Error", "Please select different database source and target to compare.", null, dialog);
                if (currentPage.get() != OBJECT_FILTER) {
                    Platform.runLater(() -> {
                        objectFilterButton.fire();       
                    });
                }
                makeBusy(false);
                return;
            }        

            selectedInfo = new SyncCompareInfo(
                leftSavedConnectionComboBox.getValue(), 
                rightSavedConnectionComboBox.getValue(), 

                left,
                right,

                OPTION_ITEM__IGNORE_AUTOINC_TABLE.isSelected(),
                OPTION_ITEM__IGNORE_AVERAGE_ROW.isSelected(),
                OPTION_ITEM__IGNORE_COLUMN_DEF_VALUE.isSelected(),
                OPTION_ITEM__IGNORE_DEFINER.isSelected(),
                OPTION_ITEM__IGNORE_ENDS.isSelected(),
                OPTION_ITEM__IGNORE_FOREIGN_KEYS.isSelected(),
                OPTION_ITEM__IGNORE_STARTS.isSelected(),
                OPTION_ITEM__IGNORE_TABLE_ENGINE.isSelected(),
                OPTION_ITEM__FORCE_COLUMN_ORDER.isSelected(),  

                FILTER_ITEM__TABLES.isSelected(),
                FILTER_ITEM__EVENTS.isSelected(),
                FILTER_ITEM__FUNCTIONS.isSelected(),
                FILTER_ITEM__PROCEDURES.isSelected(),
                FILTER_ITEM__TRIGGERS.isSelected(),
//                FILTER_ITEM__UDFS.isSelected(),
                FILTER_ITEM__VIEWS.isSelected()
            );

            Platform.runLater(() -> {
               dialog.close(); 
            });
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    void cancelAction(ActionEvent event) {
        selectedInfo = null;
        dialog.close();
    }
    
    
    public SyncCompareInfo getSelectedComapreInfo() {
        return selectedInfo;
    }
    
    @FXML
    void leftCloneBttnAction(ActionEvent event) {
        showClone(leftSavedConnectionComboBox.getSelectionModel().getSelectedItem(), true);
    }

    @FXML
    void leftSaveActionBttn(ActionEvent event) {
        showSave(leftSavedConnectionComboBox.getSelectionModel().getSelectedItem(), leftSavePasswordCheckBox.isSelected(), leftSshSavePasswordCheckBox.isSelected());
    }

    @FXML
    void leftNewBttnAction(ActionEvent event) {
        showNew(true);
    }
    
    @FXML
    void leftRenameBttnAction(ActionEvent event) {
        showRename(leftSavedConnectionComboBox.getSelectionModel().getSelectedItem());
    }

    @FXML
    void leftDeleteBttnAction(ActionEvent event) {
        showDelete(leftSavedConnectionComboBox.getSelectionModel().getSelectedItem());
    }
    
    @FXML
    void leftCheckBttnAction(ActionEvent event) {
        refreshConnection(leftParams(), leftDbService, false);
    }
    
    private ConnectionParams leftParams() {
        ConnectionParams cp = new ConnectionParams();
        cp.setMultiQueries(true);
        
        cp.setAddress(leftAddress.getText());
        cp.setUsername(leftUser.getText());
        cp.setPassword(leftPass.getText());

        try {
            cp.setPort(Integer.parseInt(leftPort.getText()));
        } catch (NumberFormatException ex) {
            cp.setPort(3306);
        }

        try {
            cp.setSessionIdleTimeout(Integer.parseInt(leftSessionIdleField.getText()));
        } catch (NumberFormatException ex) {
        }

        cp.setUseSshTunneling(leftUseSshTunnelingCheckBox.isSelected());
        cp.setSshHost(leftSshHost.getText());
        cp.setSshPassword(leftSshPassword.getText());
        cp.setSshUser(leftSshUser.getText());
        cp.setUseCompressedProtocol(leftUseCompressedProtocolCheckBox.isSelected());
        
        return cp;
    }
    
    
    @FXML
    void leftOpenPrivateKey(ActionEvent event) {
        showPrivatKey(leftSshPrivateKeyField);
    }
    
    
    
    @FXML
    void rightCloneBttnAction(ActionEvent event) {
        showClone(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem(), false);
    }

    @FXML
    void rightSaveActionBttn(ActionEvent event) {
        showSave(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem(), rightSavePasswordCheckBox.isSelected(), rightSshSavePasswordCheckBox.isSelected());
    }

    @FXML
    void rightNewBttnAction(ActionEvent event) {
        showNew(false);
    }
    
    @FXML
    void rightRenameBttnAction(ActionEvent event) {
        showRename(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem());
    }

    @FXML
    void rightDeleteBttnAction(ActionEvent event) {
        showDelete(rightSavedConnectionComboBox.getSelectionModel().getSelectedItem());
    }
    
    @FXML
    void rightCheckBttnAction(ActionEvent event) {
        refreshConnection(rightParams(), rightDbService, false);
    }
        
    private ConnectionParams rightParams() {
        ConnectionParams cp = new ConnectionParams();
        cp.setMultiQueries(true);
        
        cp.setAddress(rightAddress.getText());
        cp.setUsername(rightUser.getText());
        cp.setPassword(rightPass.getText());

        try {
            cp.setPort(Integer.parseInt(rightPort.getText()));
        } catch (NumberFormatException ex) {
            cp.setPort(3306);
        }

        try {
            cp.setSessionIdleTimeout(Integer.parseInt(rightSessionIdleField.getText()));
        } catch (NumberFormatException ex) {
        }

        cp.setUseSshTunneling(rightUseSshTunnelingCheckBox.isSelected());
        cp.setSshHost(rightSshHost.getText());
        cp.setSshPassword(rightSshPassword.getText());
        cp.setSshUser(rightSshUser.getText());
        cp.setUseCompressedProtocol(rightUseCompressedProtocolCheckBox.isSelected());
        
        return cp;
    }
    
    @FXML
    void rightOpenPrivateKey(ActionEvent event) {
        showPrivatKey(rightSshPrivateKeyField);
    }
    
    
    
    private void showNew(boolean left) {
        String result = ((MainController)getController()).showInput("New Connection", "Enter New Connection Name", "Name:", "", dialog);

        if (result != null && !result.isEmpty()) {

            if (db.checkConnectionParamsName(result)) {
                ((MainController)getController()).showError("Error", "the name already exists! Please enter other name!!", null, dialog);
                return;
            }

            ConnectionParams p = provider.get();
            p.setName(result);

            addConnectionNameList(p, leftConnectionParamsList, leftSavedConnectionComboBox, left);
            addConnectionNameList(p, rightConnectionParamsList, rightSavedConnectionComboBox, !left);
        }
    }
    
    private void showClone(ConnectionParams connectionParams, boolean left) {
        
        if (connectionParams != null) {

            String result = ((MainController)getController()).showInput("New Connection", "Enter New Connection Name", "Name:", connectionParams.getName() + "_New", dialog);
            if (result != null && !result.isEmpty()) {

                if (db.checkConnectionParamsName(result)) {
                    ((MainController)getController()).showError("Error", "the name already exists! Please enter other name!!", null, dialog);
                    return;
                }

                ConnectionParams p = connectionParams.copyOnlyParams();
                p.setName(result);

                addConnectionNameList(p, leftConnectionParamsList, leftSavedConnectionComboBox, left);
                addConnectionNameList(p, rightConnectionParamsList, rightSavedConnectionComboBox, !left);
            }
        }
    }
    
    private void showSave(ConnectionParams selectedConnParam, boolean savePassword, boolean saveSshPassword) {
        
        if (selectedConnParam != null) {
            if (!savePassword) {
                selectedConnParam.setPassword("");
            }

            if (!saveSshPassword) {
                selectedConnParam.setSshPassword("");
            }

            db.save(selectedConnParam);
            
            ((MainController)getController()).showInfo("Info", "Successfully saved", null, dialog);
        }
    }    
    
    private void showRename(ConnectionParams params) {
        
        if (params != null) {
            String result = ((MainController)getController()).showInput("Rename Connection", "Enter New Connection Name", "Name:", params.getName(), dialog);
            if (result != null && !result.isEmpty()) {

                ConnectionParams cp = db.findConenctionParamsByName(result);
                if (cp != null && !cp.getId().equals(params.getId())) {
                    ((MainController)getController()).showError("Error", "the name already exists! Please enter other name!!", null, dialog);
                    return;
                }

                params.setName(result);

                int leftSelected = leftSavedConnectionComboBox.getSelectionModel().getSelectedIndex();
                int index = leftConnectionParamsList.indexOf(params);
                leftConnectionParamsList.remove(params);
                leftConnectionParamsList.add(index, params);
                Collections.sort(leftConnectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
                    return o1.getName().compareTo(o2.getName());
                });
                
                if (leftSelected == index) {
                    leftSavedConnectionComboBox.getSelectionModel().select(params);
                }

                int rightSelected = rightSavedConnectionComboBox.getSelectionModel().getSelectedIndex();
                index = rightConnectionParamsList.indexOf(params);
                rightConnectionParamsList.remove(params);
                rightConnectionParamsList.add(index, params);
                Collections.sort(rightConnectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
                    return o1.getName().compareTo(o2.getName());
                });
                
                if (rightSelected == index) {
                    rightSavedConnectionComboBox.getSelectionModel().select(params);
                }
            }
        }
    }
    
    
    private void showDelete(ConnectionParams params) {
        DialogResponce result = ((MainController)getController()).showConfirm("Delete connection", "Do you realy want to delete the connection detail?", dialog, false);

        if (result == DialogResponce.OK_YES) {
            
            if (params != null && db.delete(params)) {
                
                leftConnectionParamsList.remove(params);
                rightConnectionParamsList.remove(params);
                
                ((MainController)getController()).showInfo("Info", "Successfully deleted '" + params.getName() + "'", null, dialog);
            }
        }
    }
    
    private void refreshConnection(ConnectionParams params, MysqlDBService service, boolean hiden) {
    
        if (params != null) {
            try {
                
                if (!hiden) {
                    service.connect(params);                
                    ((MainController)getController()).showInfo("Info", "Connetion successful, Databases list refreshed!", null, dialog);
                }
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", ex.getMessage(), ex, dialog);
            }
        }
    }
    
    
    private void showPrivatKey(TextField field) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PuTTY Private Key Files", "*.ppk"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            field.setText(file.getAbsolutePath());
        }
    }
    
    
    
    private WizardPageInfo SOURCE_AND_TARGET = new WizardPageInfo(
        "Source and Target", 
        "Select Source and Target connections and specify the names to be compared.", 
        obj -> {
            sourceContainer.setVisible(true);
            destinationContainer.setVisible(true);
            sourceTargetButtonsPane.setVisible(true);
        },
        obj -> {
            sourceContainer.setVisible(false);
            destinationContainer.setVisible(false);
            sourceTargetButtonsPane.setVisible(false);
        }
    );
        
    private WizardPageInfo OPTIONS = new WizardPageInfo(
        "Options", 
        "Specify the options of schema comparison.", 
        obj -> {
            optionsContainer.setVisible(true);
            descriptionContainer.setVisible(true);
            defaultsButtonsPane.setVisible(true);
        },
        obj -> {
            optionsContainer.setVisible(false);
            descriptionContainer.setVisible(false);
            defaultsButtonsPane.setVisible(false);
        }
    );
    
    private WizardPageInfo OBJECT_FILTER = new WizardPageInfo(
        "Object Filter", 
        "Select the types of objects you want to compare.", 
        obj -> {
            objectFilterContainer.setVisible(true);
            defaultsButtonsPane.setVisible(true);
        },
        obj -> {
            objectFilterContainer.setVisible(false);
            defaultsButtonsPane.setVisible(false);
        }
    );
    
    private class WizardPageInfo {
        
        private String title;
        private String description;
        private Consumer pageShower;
        private Consumer pageHider;
        
        WizardPageInfo(String title, String description, Consumer pageShower, Consumer pageHider) {
            this.title = title;
            this.description = description;
            this.pageShower = pageShower;
            this.pageHider = pageHider;
        }
        
        public void showPage() {
            Platform.runLater(() -> {
                titleLabel.setText(title);
                descriptionLabel.setText(description);
                pageShower.accept(null); 
            });
        }
        
        public void hidePage() {
            pageHider.accept(null);
        }
    }
    
    
    private class OptionItem {
        
        private final StringProperty name = new SimpleStringProperty();
        private final StringProperty description = new SimpleStringProperty();
        private final BooleanProperty selected = new SimpleBooleanProperty();
        
        private final boolean defaultValue;
        public OptionItem(String name, boolean selected, String description) {
            setName(name);
            setSelected(selected);
            setDescription(description);
            
            defaultValue = selected;            
        }
        
        public final void toDefultValue() {
            selected.set(defaultValue);
        }
        
        public final String toSettingsValue() {
            return String.valueOf(selected.get());
        }
        
        public final void fromSettingsValue(String value) {
            selected.set(value != null ? Boolean.valueOf(value) : defaultValue);
        }

        public final StringProperty nameProperty() {
            return this.name;
        }

        public final String getName() {
            return this.nameProperty().get();
        }

        public final void setName(final String name) {
            this.nameProperty().set(name);
        }
        
        public final StringProperty descriptionProperty() {
            return this.description;
        }
        
        public final String getDescription() {
            return this.descriptionProperty().get();
        }

        public final void setDescription(final String description) {
            this.descriptionProperty().set(description);
        }

        public final BooleanProperty selectedProperty() {
            return this.selected;
        }

        public final boolean isSelected() {
            return this.selectedProperty().get();
        }

        public final void setSelected(final boolean selected) {
            this.selectedProperty().set(selected);
        }

        @Override
        public String toString() {
            return getName();
        }
    }
    
    private class FilterItem {
        
        private final StringProperty name = new SimpleStringProperty();
        private final BooleanProperty selected = new SimpleBooleanProperty(true);
        
        private final Image icon;
        
        public FilterItem(String name, Image icon) {
            setName(name);
            
            this.icon = icon;
        }
        
        public final void toDefultValue() {
            selected.set(true);
        }
        
        public final String toSettingsValue() {
            return String.valueOf(selected.get());
        }
        
        public final void fromSettingsValue(String value) {
            selected.set(value != null ? Boolean.valueOf(value) : true);
        }
        
        public final StringProperty nameProperty() {
            return this.name;
        }

        public final String getName() {
            return this.nameProperty().get();
        }

        public final void setName(final String name) {
            this.nameProperty().set(name);
        }

        
        public final BooleanProperty selectedProperty() {
            return this.selected;
        }

        public final boolean isSelected() {
            return this.selectedProperty().get();
        }

        public final void setSelected(final boolean selected) {
            this.selectedProperty().set(selected);
        }
        
        
        public Image getIcon() {
            return icon;
        }
        

        @Override
        public String toString() {
            return getName();
        }
    }
}
