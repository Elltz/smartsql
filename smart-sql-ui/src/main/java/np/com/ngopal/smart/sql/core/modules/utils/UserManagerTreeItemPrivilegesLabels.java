/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class UserManagerTreeItemPrivilegesLabels {
    
    String name;

    public UserManagerTreeItemPrivilegesLabels(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
