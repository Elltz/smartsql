/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.AbstractConnectionModule;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.ConstantWorkProcedures;
import np.com.ngopal.smart.sql.structure.utils.SessionPersistor;
import np.com.ngopal.smart.sql.structure.utils.SmartsqlSavedData;
import np.com.ngopal.smart.sql.structure.utils.SynchorniseWorkProccessor;
import np.com.ngopal.smart.sql.ui.controller.MainController;
/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class PersistenceAppSaver extends ConstantWorkProcedures<Object> {

    private SimpleObjectProperty<SmartsqlSavedData> smartDataProps;
    private static final long maxOperationsToTriggerCancel = (1000 / 2);// prod is 20 seconds (1000 * 10), debug is 500 millisecs (1000 / 2)
    
    private SimpleBooleanProperty startSavingThreadOperation;
    private SimpleBooleanProperty threadOperationRunning;
    private SimpleBooleanProperty stopThreadOperation;

    /**
     * 0 = SAVING SATE 1 = RESTORING STATE
     */
    private volatile byte OPERATION = -1;
    private final byte OPERATION_SAVE = 0;
    private final byte OPERATION_RESTORE = 1;

    private Consumer finishCallback;
    @Getter
    private SimpleObjectProperty<Boolean> currentRestoringKeyProperty;
    private boolean initiated = false;
    private ArrayList<ModuleController> sortedModules;
    
    private MainController controller;
    
    private volatile boolean inited = false;
    
    private boolean ignoreSessionChanging = false;
    
    private static final String SESSIONS_DIR = "sessions";
    private static final String LAST_CONNECTIONS = "last_connections.json";
    private static final String SESSION_PREFIX = "session_";
    private static final String USER_SESSION_PREFIX = "user_session_";
    private static final String TAB_SUFIX = "_tab.json";

    public PersistenceAppSaver(MainController cont) {
        controller = cont;
        workingProcedure = "PersistenceAppSaver";
        smartDataProps = new SimpleObjectProperty<SmartsqlSavedData>(controller.getSmartData()) {
            @Override
            protected void invalidated() {
                super.invalidated();
                if (get() == null) {
                    nullifyItems();
                }
            }
        };
        currentRestoringKeyProperty = new SimpleObjectProperty<Boolean>() {
            @Override
            protected void invalidated() {
                super.invalidated();
                
                if (ignoreSessionChanging || get() == null || !get()) {
                    return;
                }
                
                synchronized (getAuthorization()) {
//                    log.info("sync start: currentRestoringKeyProperty");
                    OPERATION = OPERATION_RESTORE;
                    Flags.changeRestorationState(true);
                    SynchorniseWorkProccessor mainSWP = Recycler.getMainSWP();
                    mainSWP.notifyMainSynWorkProc4Restoration();
                    mainSWP.wakeUp();
//                    log.info("sync end: currentRestoringKeyProperty");
                }
                
                startInitiation();
            }
        };

        stopThreadOperation = new SimpleBooleanProperty(false) {
            @Override
            protected void invalidated() {
                super.invalidated();

            }
        };

        threadOperationRunning = new SimpleBooleanProperty(false) {

            @Override
            protected void invalidated() {
                super.invalidated();
                if (!get()) {
                    OPERATION = -1;
                }
            }

        };

        startSavingThreadOperation = new SimpleBooleanProperty(false) {

            @Override
            protected void invalidated() {
                super.invalidated();
                if (get()) {
                    OPERATION = OPERATION_SAVE;
                }
            }

        };
        
        inited = true;
    }

    public void stopCurrentLoadings() {
        synchronized (getAuthorization()) {
//            log.info("sync start: stopCurrentLoadings");
            stopThreadOperation.set(true);
            Flags.changeRestorationState(false);
            controller.forceBusyChanged(false);
//            log.info("sync end: stopCurrentLoadings");
        }
    }
    
    private void saveOpenedQueryTabs(String[] connections) {
        
        File dir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
        dir.mkdirs();
        
        for (ModuleController mc : controller.getModules()) {
            try {
                if (mc instanceof QueryTabModule) {
                    
                    List<File> existFiles = new ArrayList<>();
                    for (String connection: connections) {                                                
                        
                        List<Map<String, String>> data = ((QueryTabModule)mc).getPersistingMap(connection, null);
                        if (data != null) {
                            
                            for (File f: dir.listFiles()) {
                                if (f.getName().startsWith(connection + "_")) {
                                    existFiles.add(f);                                    
                                }
                            }

                            for (Map<String, String> m: data) {
                                
                                File f = new File(dir, connection + "_" + m.get("uuid") + "_" + TAB_SUFIX);                                
                                existFiles.remove(f);
                                
                                try {
                                    Files.deleteIfExists(f.toPath());
                                    Files.write(f.toPath(), new Gson().toJson(m).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                                } catch (IOException ex) {
                                    log.error("Error", ex);
                                }    
                            }
                        }
                    }
                    
                    for (File f: existFiles) {
                        try {
                            Files.deleteIfExists(f.toPath()); 
                        } catch (IOException ex) {
                            log.error("Error", ex);
                        } 
                    }
                    
                    existFiles.clear();
                }
            } catch (UnsupportedOperationException uoe) {}
        }
    }
    
    
    private void saveUserQueryTabs(String connection, String name) {
        
        File dir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
        dir.mkdirs();
        
        for (ModuleController mc : controller.getModules()) {
            try {
                if (mc instanceof QueryTabModule) {
                    
                    List<File> existFiles = new ArrayList<>();                                        
                        
                    List<Map<String, String>> data = ((QueryTabModule)mc).getPersistingMap(connection, name);
                    if (data != null) {

                        for (File f: dir.listFiles()) {
                            if (f.getName().startsWith(name + "_" + connection + "_")) {
                                existFiles.add(f);                                    
                            }
                        }

                        for (Map<String, String> m: data) {

                            File f = new File(dir, name + "_" + connection + "_" +  m.get("uuid") + "_" + TAB_SUFIX);
                            existFiles.remove(f);

                            try {
                                Files.deleteIfExists(f.toPath()); 
                                Files.write(f.toPath(), new Gson().toJson(m).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                            } catch (IOException ex) {
                                log.error("Error", ex);
                            }    
                        }
                    }
                    
                    for (File f: existFiles) {
                        try {
                            Files.deleteIfExists(f.toPath()); 
                        } catch (IOException ex) {
                            log.error("Error", ex);
                        } 
                    }
                    
                    existFiles.clear();
                }
            } catch (UnsupportedOperationException uoe) {}
        }
    }
    
    public void saveQueryTab(String connection, String name, String uuid) {
        
        synchronized (getAuthorization()) {
//            log.info("sync start: saveQueryTab");
            File dir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
            dir.mkdirs();

            for (ModuleController mc : controller.getModules()) {
                try {
                    if (mc instanceof QueryTabModule) {

                        List<Map<String, String>> data = ((QueryTabModule)mc).getPersistingMap(connection, name);
                        if (data != null) {
                            for (Map<String, String> m: data) {
                                if (uuid.equals(m.get("uuid"))) {

                                    File f = new File(dir, connection + "_" + m.get("uuid") + "_" + TAB_SUFIX);

                                    try {
                                        Files.deleteIfExists(f.toPath()); 
                                        String str = new Gson().toJson(m);
                                        byte[] dataBytes = str.getBytes("UTF-8");
                                        
                                        try (
                                            FileWriter fw = new FileWriter(f);
                                            BufferedWriter out = new BufferedWriter(fw, dataBytes.length);
                                            ){
                                            out.write(str);
                                        }
                                        
//                                        Files.write(f.toPath(), dataBytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                                    } catch (IOException ex) {
                                        log.error("Error", ex);
                                    }    
                                    return;
                                }
                            }
                        }
                    }
                } catch (UnsupportedOperationException uoe) {}
            }
//            log.info("sync end: saveQueryTab");
        }
    }
    
    private Map getConnectionData(String connection) {
        
        ModuleController mc = controller.getModuleByClassName(ConnectionModule.class.getName());
        ModuleController tabMc = controller.getModuleByClassName(QueryTabModule.class.getName());
        Map<String, Object> data = new HashMap<>();
        try {
            File dir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
            if (dir.exists()) {
                for (File f: dir.listFiles()) {
                    if (f.getName().startsWith(connection + "_")) {
                        
                        try {
                            Map m = new Gson().fromJson(String.join("\n", Files.readAllLines(f.toPath())), HashMap.class);
                            if (m != null) {
                                
                                Map<String, List<String>> m0 = (Map<String, List<String>>) data.get(tabMc.getName());
                                if (m0 == null) {
                                    m0 = new HashMap<>();
                                    data.put(tabMc.getName(), m0);
                                }
                                
                                if (f.getName().endsWith("_" + TAB_SUFIX)) {
                                    List list = (List) m0.get(QueryTabModule.USER_DATA__QUERY_TABS);
                                    if (list == null) {
                                        list = new ArrayList();
                                        m0.put(QueryTabModule.USER_DATA__QUERY_TABS, list);
                                    }

                                    list.add(m);
                                    
                                    list.sort(new Comparator<Map<String, String>>() {
                                        @Override
                                        public int compare(Map<String, String> o1, Map<String, String> o2) {

                                            Integer order1 = null;
                                            try {
                                                order1 = Integer.valueOf(o1.get("order"));
                                            } catch (Throwable th) {}

                                            Integer order2 = null;
                                            try {
                                                order2 = Integer.valueOf(o2.get("order"));
                                            } catch (Throwable th) {}

                                            if (order1 == null) {
                                                return -1;
                                            }

                                            if (order2 == null) {
                                                return 1;
                                            }

                                            return order1 - order2;
                                        }
                                    });
                                }
                            }
                        } catch (Throwable th) {
                            log.error("Error", th);
                        }
                    }
                }
            }
        } catch (Throwable th) {
            log.error("Error", th);
        }
        
        Map<String, Object> result = new HashMap<>();
        result.put(mc.getName(), data);
        
        return result;
    }
    
    private Map getConnectionData(String connection, String session) {
        
        ModuleController mc = controller.getModuleByClassName(ConnectionModule.class.getName());
        ModuleController tabMc = controller.getModuleByClassName(QueryTabModule.class.getName());
        Map<String, Object> data = new HashMap<>();
        try {
            File dir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
            if (dir.exists()) {
                for (File f: dir.listFiles()) {
                    if (f.getName().startsWith(session + "_" + connection + "_")) {
                        
                        FileReader fr = null;
                        try {
                            fr = new FileReader(f);
                            Map m = new Gson().fromJson(fr, HashMap.class);
                            if (m != null) {
                                
                                Map<String, List<String>> m0 = (Map<String, List<String>>) data.get(tabMc.getName());
                                if (m0 == null) {
                                    m0 = new HashMap<>();
                                    data.put(tabMc.getName(), m0);
                                }
                                
                                if (f.getName().endsWith("_" + TAB_SUFIX)) {
                                    List list = (List) m0.get(QueryTabModule.USER_DATA__QUERY_TABS);
                                    if (list == null) {
                                        list = new ArrayList();
                                        m0.put(QueryTabModule.USER_DATA__QUERY_TABS, list);
                                    }

                                    list.add(m);
                                    
                                    list.sort(new Comparator<Map<String, String>>() {
                                        @Override
                                        public int compare(Map<String, String> o1, Map<String, String> o2) {

                                            Integer order1 = null;
                                            try {
                                                order1 = Integer.valueOf(o1.get("order"));
                                            } catch (Throwable th) {}

                                            Integer order2 = null;
                                            try {
                                                order2 = Integer.valueOf(o2.get("order"));
                                            } catch (Throwable th) {}

                                            if (order1 == null) {
                                                return -1;
                                            }

                                            if (order2 == null) {
                                                return 1;
                                            }

                                            return order1 - order2;
                                        }
                                    });
                                }
                            }
                        } catch (Throwable th) {
                            log.error("Error", th);
                        } finally {
                            if (fr != null) {
                                try {
                                    fr.close();
                                } catch (IOException ex) {
                                    log.error("Error", ex);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
            log.error("Error", th);
        }
        
        Map<String, Object> result = new HashMap<>();
        result.put(mc.getName(), data);
        
        return result;
    }
    
    private List<String> saveLastOpenedConnections() {
        
        try {
            File sessDir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
            sessDir.mkdirs();
            File f = new File(sessDir, LAST_CONNECTIONS);

            Files.deleteIfExists(f.toPath()); 
            
            Map<String, List<String>> openedConnections = new HashMap<>();            
            List<String> connections = new ArrayList<>();
            for (ModuleController mc : controller.getModules()) {
                try {
                    if (mc instanceof ConnectionModule) {
                        Map<String, List<Object>> data = (Map<String, List<Object>>) ((ConnectionModule)mc).getPersistingMap();
                        if (data != null) {
                            for (List<Object> value: data.values()) {
                                if (value != null && !value.isEmpty()) {
                                    String connection = (String) value.get(0);
                                    if (!connections.contains(connection)) {
                                        connections.add(connection);
                                    }
                                    
                                    saveConnection((String) value.get(0), null);
                                }
                            }
                        }
                    }
                } catch (Throwable uoe) {}
            }
            
            openedConnections.put("lastConnections", connections);
            
            Files.write(f.toPath(), new Gson().toJson(openedConnections).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            
            return connections;
        } catch (Throwable ex) {
            log.error("Error", ex);
        }
        
        return null;
    }
    
    private SimpleDateFormat df0 = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
    private void saveConnection(String connection, String name) {
        
        try {
            File sessDir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
            sessDir.mkdirs();            
            File f = new File(sessDir, (name != null ? USER_SESSION_PREFIX + name + "_" : SESSION_PREFIX) + connection + ".json");
            
            Files.deleteIfExists(f.toPath()); 
            
            Map<String, String> session = new HashMap<>();
            session.put("name", name);
            session.put("connection", connection);
            session.put("updated", df0.format(new Date()));
                                    
            Files.write(f.toPath(), new Gson().toJson(session).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            
        } catch (IOException ex) {
            log.error("Error", ex);
        }
    }
    
    private List<String> getLastConnections() {
        
        FileReader fr = null;
        try {
            File f = new File(StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR),
                    LAST_CONNECTIONS);
            fr = new FileReader(f);
            
            Map<String, List<String>> data = new Gson().fromJson(fr, HashMap.class);
            if (data != null) {
                return data.get("lastConnections");
            }
        } catch (IOException ex) {
            log.error("Error", ex);
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ex) {
                    log.error("Error", ex);
                }
            }
        }
        
        return null;
    }
    

    public void saveOpenedSessions() {
        synchronized (getAuthorization()) {
//            log.info("sync start: saveOpenedSessions");
            List<String> connections = saveLastOpenedConnections();
            if (connections != null) {
                saveOpenedQueryTabs(connections.toArray(new String[0]));
            }
//            log.info("sync end: saveOpenedSessions");
        }
    }
    
    public List<Map<String, String>> getAllUserSessions() {
        synchronized (getAuthorization()) {
//            log.info("sync start: getAllUserSessions");
            List<Map<String, String>> list = new ArrayList<>();
            File dir = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(SESSIONS_DIR);
            if (dir.exists()) {
                Gson gson = new Gson();
                for (File f: dir.listFiles()) {
                    if (f.getName().startsWith(USER_SESSION_PREFIX)) {

                        FileReader fr = null;

                        try {
                            fr = new FileReader(f);
                            Map<String, String> m = gson.fromJson(fr, HashMap.class);
                            if (m != null) {
                                list.add(m);
                            }
                        } catch (Throwable th) {
                        } finally {
                            try {
                               if (fr != null)  {
                                   fr.close();
                               }
                            } catch (Throwable th) {}
                        }
                    }
                }
            }
            
//            log.info("sync end: getAllUserSessions");

            return list;
        }
    }
    
    
    public void saveUserSession(ConnectionSession sesison, String connection, String name) {
        
        synchronized (getAuthorization()) {
//            log.info("sync start: saveUserSession");
            if (name == null) {

                name = controller.showInput("User session", "Enter session name:", "Session name");
                if (name == null || name.trim().isEmpty()) {
                    controller.showError("Error", "Please enter correct session name", null);
                    return;
                }

                name = name.trim();

                // check name
                List<Map<String, String>> userSessions = getAllUserSessions();
                for (Map<String, String> us: userSessions) {
                    if (name.equalsIgnoreCase(us.get("name"))) {
                        controller.showError("Error", "Session name already exist, please save with new name", null);
                        return;
                    }
                }

                sesison.setName(name);
                sesison.setOpen(false);
            }

            saveConnection(connection, name);
            saveUserQueryTabs(connection, name);

//            log.info("sync end: saveUserSession");
        }
    }

    public void restoreSessions() {
        
        log.info("RUN RESTORE SESSION");
        controller.forceBusyChanged(true);
        try {
            
            controller.getLoaderScreen().startLoader();
            boolean runRestore = false;
            synchronized (getAuthorization()) {
//                log.info("sync start: restoreSessions");
                if (smartDataProps.get() != null) {
                    runRestore = smartDataProps.get().isPaid() || !smartDataProps.get().isEvaluationTimeOver();
                    System.out.println("runRestore flag = " + runRestore);                  
                } else {
                    smartDataProps.set(new SmartsqlSavedData());
                }
//                log.info("sync end: restoreSessions");
            }
            
            interruptRestorationLoop();

            if (runRestore) {
                
                List<String> connections = getLastConnections();
                if (connections != null && !connections.isEmpty()) {
                    
                    AbstractConnectionModule amc = (AbstractConnectionModule) controller.getModuleByClassName(ConnectionModule.class.getName());
                    for (String connection: connections) {
                        restoreConnection_Cons(amc, getConnectionData(connection), connection, null);
                    }
                    
                }

                Platform.runLater(() -> { controller.getLoaderScreen().closeLoader(); });                
                stopThreadOperation.set(false);
            }
            
        } finally { 
            controller.forceBusyChanged(false); 
            // Need change it to false
            Flags.changeRestorationState(false);
            Platform.runLater(() -> {
//                // check if this is first run
//                // at now we will save youtube link in file
//                // if file exist then no need show
//                String youtubeLinkText = controller.getSmartData().getYoutubeText();
//                File youtubeFile = StandardApplicationManager.getInstance().getSmartDataDirectory(np.com.ngopal.smart.sql.structure.utils.Flags.YOUTUBE_LINK_FILE);
//                if (youtubeLinkText == null || youtubeLinkText.trim().isEmpty() || youtubeFile.exists()) {
                    // ignore showing youtube demo at now
                    // maby we will check if it changed - then we can show again 1 time
                    if (controller.getSelectedConnectionSession().get() == null) {
                        controller.activateFrontPage();
                    }
//                } else {
//
//                    if (youtubeFile.exists()) {
//                        youtubeFile.delete();
//                    }
//
//                    try {
//                        Files.write(youtubeFile.toPath(), youtubeLinkText.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
//                    } catch (IOException ex) {
//                        log.error(ex.getMessage(), ex);
//                    }
//                }
            });
            
            if (finishCallback != null) {
                finishCallback.accept(null);
            }
        }
    }
    
    private void interruptRestorationLoop() {
        synchronized (getAuthorization()) {
//            log.info("sync start: interruptRestorationLoop");
            if (stopThreadOperation.get()) {
                stopThreadOperation.set(false);
                Platform.runLater(() -> {
                    controller.getLoaderScreen().closeLoader();
                });
                throw new RuntimeException("Friendly error to stop restoration");
            }
//            log.info("sync end: interruptRestorationLoop");
        }
    }
    
    //this methods resorts the modules from controller by putting querytabmodule last
    private ArrayList<ModuleController> getSortedModules(){
        if(sortedModules == null){
            sortedModules = new ArrayList(controller.getModules().size());
            for (ModuleController mc : controller.getModules()) {
                if (mc instanceof SessionPersistor) {
                    sortedModules.add(mc);
                }
            }
            int pos = -1;
            for (int i=0; i < sortedModules.size(); i++) {
                if(sortedModules.get(i) instanceof QueryTabModule){
                    pos = i;
                    break;
                }
            }
            if(pos > -1){
                ModuleController mc = sortedModules.remove(pos);
                sortedModules.add(mc);
            }
        }
        return sortedModules;
    }
    
    private void restoreInnerModules(Map<?, ?> cachedImage, ConnectionSession t) {
        for (ModuleController mc : getSortedModules()) {
            if (mc instanceof AbstractConnectionModule) { continue; }
            interruptRestorationLoop();
            try { 
                ((SessionPersistor)mc).onRestore((Map<?, ?>) cachedImage.get(mc.getName()), t, t.getConnectionParam().getId(), null);
            } catch (Exception uoe) {
                if (mc instanceof QueryTabModule) {
                    Platform.runLater(() -> {
                        controller.showError("QUERY RESTORATION ERROR", "Failed to restore query for SESSION "
                                + String.valueOf(controller.getSelectedConnectionSession().get().getId()), uoe);
                    });
                } else {
                    if (!(uoe instanceof UnsupportedOperationException)) {
                        Logger.getLogger(PersistenceAppSaver.class.getName()).log(Level.SEVERE, null, uoe);
                    }
                }
            }
        }
    }
    
    private void restoreConnection_Cons(AbstractConnectionModule amc, Map cachedImage, String connection, String name) {
        Map map = (Map<?, ?>) cachedImage.get(amc.getName());
        if(map == null) {
            return;
        }
        
        interruptRestorationLoop();        
        amc.onRestore(map, null, Long.valueOf(connection), (ConnectionSession t) -> {
            
            t.setName(name);
            t.setOpen(name == null);
            
//            log.info("in lambda restoreInnerModules " + Thread.currentThread().getName());
            restoreInnerModules((Map<?, ?>) cachedImage.get(amc.getName()), t);
            
//            log.info("FINISH: " + Thread.currentThread().getName());
        });
    }

    /**
     * I am doing this so that all threaded methods can synchronize on both save
     * and restore. Since if thread a is locked on this object, thread B calling
     * this authorization will wait till thread A is done
     *
     * @return returns the locker
     */
    private Object getAuthorization() {
        synchronized (controller.getSmartDataLocker()) {//consider using controller.getSmartDataLocker()
            return controller.getSmartDataLocker();
        }
    }

    public void restoreSession(Consumer consumer) {
        getAuthorization();
        this.finishCallback = consumer;
        currentRestoringKeyProperty.setValue(true);
    }
    
    
    public void openSession(String connection, String name) {
        Thread th = new Thread(() -> {
            synchronized (getAuthorization()) {
//                log.info("sync start: openSession");
                controller.forceBusyChanged(true);
                try {
                    controller.getLoaderScreen().notifyBadPress(1,
                        "RESTORATION IS TAKING TOO LONG ",
                        (ActionEvent t) -> {
                            stopCurrentLoadings();
                        }
                    );

                    controller.getLoaderScreen().startLoader();
                    
                    AbstractConnectionModule amc = (AbstractConnectionModule) controller.getModuleByClassName(ConnectionModule.class.getName());
                    restoreConnection_Cons(amc, getConnectionData(connection, name), connection, name);
                    Platform.runLater(() -> { controller.getLoaderScreen().closeLoader(); });      

                } finally { 
                    controller.forceBusyChanged(false); 
                }

//                log.info("sync end: openSession");
            }        
        });
        th.setDaemon(true);
        th.start();
    }

    private void nullifyItems() { }

    public void close() {  nullifyItems(); }

    @Override
    public void updateUI() { super.updateUI(); }

    @Override
    public void run() {
        super.run();
        
        if (inited) {
            threadOperationRunning.set(true);

            long saveEvery = 30000;
//            try {
//                saveEvery = (long)(Double.valueOf(smartDataProps.get().getSavingDataDuration()) * 60000);
//            } catch (Exception e) {
//                saveEvery = 15 * 1000;
//            }
            try {
                Long currentTime = System.currentTimeMillis();
                if (OPERATION == OPERATION_RESTORE) {
                    restoreSessions();
                } 
                else if (OPERATION == OPERATION_SAVE
                        || (currentTime - constantWPStartTime) > saveEvery) {

                    startSavingThreadOperation.set(true);
                    saveOpenedSessions();
                    constantWPStartTime = currentTime;
                    startSavingThreadOperation.set(false);
                } else {}
            } finally {
                if (OPERATION != -1 && controller.getLoaderScreen().isLoading()) {
                    controller.getLoaderScreen().notifyBadPress(0,
                        "ERROR OCCURED WHILE RETRIVING SESSION \n "
                        + "RESTORATION WILL TERMINATE IN 10 secs",
                        (ActionEvent t) -> {}
                    );

                    Platform.runLater(() -> {
                        controller.getLoaderScreen().closeLoader();
                    }); 
                }

                lastVerifier();
                threadOperationRunning.set(false);
    //            System.gc();
            }        
        }
    }

    @Override
    protected void startInitiation() {
        if(initiated){return;}
        initiated = true;
        super.startInitiation(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean isSaving(){
        return OPERATION == OPERATION_SAVE;
    }
    
    public boolean isRestoring(){
        return OPERATION == OPERATION_RESTORE;
    }
    
    public SimpleBooleanProperty getOperationState(){
        return threadOperationRunning;
    }
}
