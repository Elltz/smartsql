package np.com.ngopal.smart.sql.core.modules;

import java.util.HashMap;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.ui.controller.InfoPartitionsController;
import np.com.ngopal.smart.sql.ui.controller.StatusController;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class StatusModule extends AbstractStandardModule{
    
    Image statusImage = SmartImageProvider.getInstance().getStatus_image();
        
    private HashMap<ConnectionSession, Tab> statusTabsInSession = new HashMap();

    @Override
    public ConnectionParams connectionChanged(ConnectionParams param) throws Exception {
        return super.connectionChanged(param);
    }

    @Override
    public int getOrder() {
        return 5;
    }

    
    
    @Override
    public boolean install() {
        return true;
    }
    
    private Tab createInfoTab(ConnectionSession session) {
        Tab statusTab = new Tab("Status");
        statusTab.setClosable(false);
        statusTab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(statusImage), 18, 18));
        
        final StatusController sc = getTypedBaseController("Status");
        statusTab.setContent(sc.getUI());
        statusTab.setUserData(sc);
        statusTab.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                sc.init();
            }
        });
        
        return statusTab;
    }

    @Override
    public void postConnection(ConnectionSession session, Tab tab) throws Exception {
        Platform.runLater(() -> {
            statusTabsInSession.putIfAbsent(session, createInfoTab(session));
            getController().addTabToBottomPane(session, statusTabsInSession.get(session));
        });
    }
    

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem ti) {
        Tab tab = statusTabsInSession.get(getController().getSelectedConnectionSession().get());
        if(tab.isSelected()){
            showInfo(tab, ti);
        }
        return true;
    }
    
    private void showInfo(Tab tab, TreeItem ti) {
        InfoPartitionsController ipc = (InfoPartitionsController) tab.getUserData();

        Object value = ti.getValue();

        if (ti.getValue() instanceof String) {
            value = ti.getParent().getValue();
        } else if (ti.getValue() instanceof Column || ti.getValue() instanceof Index){
            value = ti.getParent().getParent().getValue();
        }

        ipc.loadEntity(value, findDatabaseName(ti));
    }

    private String findDatabaseName(TreeItem ti) {
        if (ti != null) {
            if (ti.getValue() instanceof Database) {
                return ((Database)ti.getValue()).getName();
            } else {
                return findDatabaseName(ti.getParent());
            }
        }
        
        return null;
    }
    
    @Override
    public boolean uninstall() { 
        return false;
    }

    @Override
    public String getInfo() { 
        return "Retrieves Information about a selected base object";
    }
}
