/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.db.DesignerTableTemplateService;
import np.com.ngopal.smart.sql.model.DesignerTableTemplate;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class TableTemplateEdittorController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Button deleteTemplate;
    
    @FXML
    private Button duplicateTemplate;
    
    @FXML
    private Button newTemplate;
    
    @FXML
    private Button newColumn;
    
    @FXML
    private ComboBox<String> columnCollationBox;
    
    @FXML
    private TableView<DesignerTable> templateLister;
    
    @FXML
    private TableView<DesignerColumn> columnsLister;
    
    private DesignerTableTemplateService dtts;
    
    @FXML
    private Label collationIndicator;
    
    private MenuItem addColumn,delColumn;
    
    private SimpleBooleanProperty columnOrtableCollation = new SimpleBooleanProperty(true){

        @Override
        protected void invalidated() {
            super.invalidated(); 
            columnCollationBox.getSelectionModel().select(0);
            if(get()){
                collationIndicator.setText("Column Collation");
                DesignerColumn dc = columnsLister.getSelectionModel().getSelectedItem();
                if (dc != null) {
                    columnCollationBox.getSelectionModel().select(
                            dc.collation().get());
                }
            }else{
                collationIndicator.setText("Table Collation");
                DesignerTable dt = templateLister.getSelectionModel().getSelectedItem();
                if (dt != null) {
                    columnCollationBox.getSelectionModel().select(
                            dt.collation().get());
                }
            }
        }
        
    };

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnCollationBox.getItems().addAll(SchemaDesignerModule.COLLATIONS);
        duplicateTemplate.setOnAction(onAction);
        newTemplate.setOnAction(onAction);
        deleteTemplate.setOnAction(onAction);
        newColumn.setOnAction(onAction);  
        templateLister.getSelectionModel().selectedItemProperty().
                addListener(new ChangeListener<DesignerTable>() {
            @Override
            public void changed(ObservableValue<? extends DesignerTable> ov,
                    DesignerTable t, DesignerTable t1) {
                if (t1 != null) {
                    columnsLister.setItems(t1.columns());
                    columnCollationBox.getSelectionModel().
                            select(t1.collation().get());
                }
            }
        });
        
        columnsLister.getSelectionModel().selectedItemProperty().
                addListener(new ChangeListener<DesignerColumn>() {
            @Override
            public void changed(ObservableValue<? extends DesignerColumn> ov,
                    DesignerColumn t, DesignerColumn t1) {
                if (t1 != null) {
                    columnCollationBox.getSelectionModel().
                            select(t1.collation().get());
                }
            }
        });
        
        columnCollationBox.getSelectionModel().selectedItemProperty().
                addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov,
                    String t, String t1) { 
                if(columnOrtableCollation.get()){
                    DesignerColumn dc = columnsLister.getSelectionModel().getSelectedItem();
                    if (dc != null) {
                        dc.collation().set(t1);
                    }
                }else{
                   DesignerTable dt =  templateLister.getSelectionModel().getSelectedItem();
                   if(dt != null){
                        dt.collation().set(t1);
                   }
                }
            }
        });
        
        columnsLister.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov,
                    Boolean t, Boolean t1) { 
                if(t1){
                    columnOrtableCollation.set(true);
                }
            }
        });
        
        templateLister.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov,
                    Boolean t, Boolean t1) { 
                if(t1){
                    columnOrtableCollation.set(false);
                }
            }
        });
        
        templateLister.setEditable(true);
        columnsLister.setEditable(true);
        addColumn = new MenuItem("Add more columns",new ImageView(new Image(
        "/np/com/ngopal/smart/sql/ui/images/connection/column_manage.png",
                16,16,false,false)));
        
        delColumn = new MenuItem("Remove selected column",new ImageView(new Image(
        "/np/com/ngopal/smart/sql/ui/images/connection/column_drop.png",
                16,16,false,false)));
        delColumn.setOnAction(onAction);
        addColumn.setOnAction(onAction);
        columnsLister.setContextMenu(new ContextMenu(addColumn,delColumn));
        
        TableColumn<DesignerTable,String> tc = (TableColumn<DesignerTable,String>)
                templateLister.getColumns().get(0);
        tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DesignerTable, String>,
                ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<DesignerTable, String> p) {                
                return p.getValue().name();
            }
        });
        tc.setCellFactory(new Callback<TableColumn<DesignerTable, String>,
                TableCell<DesignerTable, String>>() {

            @Override
            public TableCell<DesignerTable, String> call(TableColumn<DesignerTable, String> p) {
                return new TextFieldTableCell<DesignerTable,String>(){

                    {
                        setOnMouseClicked(new EventHandler<MouseEvent>() {

                            @Override
                            public void handle(MouseEvent t) {
                                if (t.getClickCount() < 1) {
                                    setEditable(false);
                                } else {
                                    setEditable(true);
                                }
                            }
                        });
                        setConverter(new StringConverter<String>() {

                                    @Override
                                    public String toString(String t) { 
                                        return t;
                                    }

                                    @Override
                                    public String fromString(String string) { 
                                        return string;
                                    }
                                });
                    }
                    
                    @Override
                    public void updateItem(String t, boolean bln) {
                        super.updateItem(t, bln);                        
                        if(bln){return;}
                        setText(t);
                    }                    

                    @Override
                    public void updateSelected(boolean bln) {
                        super.updateSelected(bln);
                        if(bln){
                            setStyle("-fx-background-color: gray;");
                        }else{
                            setStyle("-fx-background-color: white;");
                        }
                    }

                    @Override
                    public void commitEdit(String t) {
                        super.commitEdit(t);
                        getTableView().getSelectionModel().clearSelection();
                        if (hasEmptyItems) {
                            hasEmptyItems = false;
                        }
                    }
                    
                };
            }
        });
        
        for (int k = 0; k < columnsLister.getColumns().size(); k++) {
            if (k < 3) {
                TableColumn<DesignerColumn, String> tcs = (TableColumn<DesignerColumn, String>)
                        columnsLister.getColumns().get(k);
                
                final int i = k;
                
                tcs.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<
                        DesignerColumn, String>, ObservableValue<String>>() {

                    @Override
                    public ObservableValue<String> call(TableColumn.
                            CellDataFeatures<DesignerColumn, String> p) {
                        if(i ==0){
                           return p.getValue().name();
                        }else if(i == 1){
                            return p.getValue().dataType();
                        }else if(i ==2){
                            return p.getValue().defaultValue();
                        }else{
                            return null;
                        }
                    }
                });
                
                tcs.setCellFactory(new Callback<TableColumn<DesignerColumn, String>,
                        TableCell<DesignerColumn, String>>() {

                    @Override
                    public TableCell<DesignerColumn, String> call(TableColumn<DesignerColumn, String> p) {
                        return new TextFieldTableCell<DesignerColumn, String>() {

                            {
                                setOnMouseClicked(new EventHandler<MouseEvent>() {

                                    @Override
                                    public void handle(MouseEvent t) {
                                        if (t.getClickCount() == 1) {
                                            setEditable(false);
                                        } else {
                                            setEditable(true);
                                        }
                                    }
                                });
                                setConverter(new StringConverter<String>() {

                                    @Override
                                    public String toString(String t) { 
                                        return t;
                                    }

                                    @Override
                                    public String fromString(String string) { 
                                        return string;
                                    }
                                });
                            }
                            
                            @Override
                            public void updateItem(String t, boolean bln) {
                                super.updateItem(t, bln);
                                
                                if (bln) {
                                    return;
                                }
                                setText(t);
                            }

                            @Override
                            public void commitEdit(String t) {
                                super.commitEdit(t);  
                                getTableView().getSelectionModel().clearSelection();
                                if (hasEmptyItems) {
                                    hasEmptyItems = false;
                                }
                            }
                        };
                    }
                });
                
            } else {
                TableColumn<DesignerColumn, Boolean> tcs = (TableColumn<DesignerColumn, Boolean>)
                        columnsLister.getColumns().get(k);
                final int i = k;
                tcs.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<
                        DesignerColumn, Boolean>, ObservableValue<Boolean>>() {
                            
                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<
                            DesignerColumn, Boolean> p) {

                        if (i == 3) {
                            return p.getValue().primaryKey();
                        } else if (i == 4) {
                             return p.getValue().notNull();
                        } else if (i == 5) {
                             return p.getValue().unique();
                        } else if (i == 6) {
                             return p.getValue().autoIncrement();
                        }else{
                            return null;
                        }
                    }
                });
                
                tcs.setCellFactory(new Callback<TableColumn<DesignerColumn, Boolean>,
                        TableCell<DesignerColumn, Boolean>>() {

                    @Override
                    public TableCell<DesignerColumn, Boolean> call(TableColumn<DesignerColumn, Boolean> p) {
                        return new CheckBoxTableCell<>();
                    }
                });                
            }
        }        
    }
    
    private final EventHandler onAction = new EventHandler() {

        @Override
        public void handle(Event t) { 
            Object o = t.getSource();
            if(o == newTemplate){                                 
                    templateLister.getItems().add(newTemplate(
                    "New Template"));                
                hasEmptyItems = true;
            }else if(o == deleteTemplate){
                final DesignerTable tt = templateLister.getSelectionModel().getSelectedItem();
                DialogResponce dr = ((MainController)getController()).showConfirm("DELETE ALERT", 
                        "Are you sure you want to delete "+ tt.name().get() +
                        " Template ?");
                if(dr != null){
                    if(dr == DialogResponce.CANCEL ||
                            dr == DialogResponce.NO){
                        return;
                    }
                }
                templateLister.getItems().remove(tt);
                Recycler.addWorkProc(new WorkProc() {

                    @Override
                    public void updateUI() {}

                    @Override
                    public void run() { 
                        boolean contains = false;
                        DesignerTableTemplate toDel = null;
                        for (DesignerTableTemplate dtt : dtts.getAll()) {
                            if(dtt.getName().equals(tt.name().get())){
                                contains = true;
                                toDel = dtt;
                                break;
                            }
                        }
                        if (contains) {
                            dtts.delete(toDel);
                        }
                    }
                });
                
            }else if(o == duplicateTemplate){
                DesignerTable dt = templateLister.getSelectionModel().getSelectedItem();
                if(dt == null){return;}
                DesignerTable dt2 = newTemplate("duplicated-"+dt.name().get());
                dt2.collation().set(dt.collation().get());
                dt2.columns().addAll(dt.columns());
                dt2.comments().set(dt.comments().get());
                dt2.engine().set(dt.engine().get());
                dt2.schema().set(dt.schema().get());
                dt2.foreignKeys().addAll(dt.foreignKeys());
                dt2.indexes().addAll(dt.indexes());
                templateLister.getItems().add(dt2);
            }else if(o == newColumn || o == addColumn){
                DesignerTable dt = templateLister.
                        getSelectionModel().getSelectedItem();
                if(dt == null){return;}
                DesignerColumn dc = new DesignerColumn();
                dt.columns().add(dc);
                columnsLister.getSelectionModel().select(dc);
                hasEmptyItems = true;
            }else if(o == delColumn){
                templateLister.
                        getSelectionModel().getSelectedItem().columns().remove(
                                columnsLister.getSelectionModel().getSelectedItem());
            }
        }
    };
       
    private DesignerTable newTemplate(String name){
        DesignerTable dt = new DesignerTable();
                    dt.name().set(name);
        return dt;
    }
    
    boolean hasEmptyItems = false;
    
    public Stage init(DesignerTableTemplateService tts,ArrayList<DesignerTable> list,WorkProc work){
        this.dtts = tts;
        templateLister.getItems().addAll(list);
        Stage s = new Stage();
        s.setTitle("Table Templates");
        s.initModality(Modality.APPLICATION_MODAL);
        s.initOwner(getStage());
        s.setScene(new Scene(mainUI));
        s.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                
                    if (hasEmptyItems) {
                        DialogResponce dr = ((MainController)getController()).showConfirm("ALERT", "You have empty/null items. \n"
                                + " Do you want to save data along with null/empty items ");
                        if (dr == DialogResponce.OK_YES) {
                            work.setAdditionalData(templateLister.getItems());
                            Recycler.addWorkProc(work);
                        }else{
                            t.consume();                            
                        }                       
                    }else{
                        work.setAdditionalData(templateLister.getItems());
                        Recycler.addWorkProc(work);
                    }
                
            }
        });
        s.setResizable(false);
        return s;
    }    
}
