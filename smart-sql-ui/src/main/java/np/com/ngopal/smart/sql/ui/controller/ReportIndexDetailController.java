package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportIndexDetailController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
    
    @FXML
    private Button refreshButton;
    
    @FXML
    private Button exportButton;
    
    private ConnectionSession session;
    
    @FXML
    public void refreshAction(ActionEvent event) {
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            if (session != null) {

                try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_DETAIL))) {
                    if (rs != null) {
                        List<ReportIndexDetail> list = new ArrayList<>();
                        while (rs.next()) {
                            ReportIndexDetail rid = new ReportIndexDetail(
                                rs.getString(1), 
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                    
                                rs.getLong(5),
                                rs.getLong(6),
                                rs.getLong(7),
                                rs.getLong(8),
                                rs.getLong(9),
                                    
                                rs.getLong(10),
                                rs.getLong(11),
                                rs.getLong(12),
                                rs.getLong(13),
                                rs.getLong(14),
                                    
                                rs.getLong(15),
                                rs.getLong(16),
                                rs.getLong(17),
                                rs.getLong(18),
                                rs.getLong(19),
                                    
                                rs.getLong(20),
                                rs.getLong(21),
                                rs.getLong(22),
                                rs.getLong(23),
                                rs.getLong(24),
                                    
                                rs.getLong(25),
                                rs.getLong(26),
                                rs.getLong(27),
                                rs.getLong(28),
                                rs.getLong(29),
                                    
                                rs.getLong(30),
                                rs.getLong(31),
                                rs.getLong(32),
                                rs.getLong(33),
                                rs.getLong(34),
                                    
                                rs.getLong(35),
                                rs.getLong(36),
                                rs.getLong(37),
                                rs.getLong(38),
                                rs.getLong(39)
                            );  

                            list.add(rid);
                        }

                        tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                    }
                } catch (SQLException ex) {
                    Platform.runLater(() -> ((MainController)getController()).showError("Error", ex.getMessage(), null));
                }
            }
            
            Platform.runLater(() -> makeBusy(false));
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportIndexUnused rds: (List<ReportIndexUnused>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rds.getDatabase());
                        row.add(rds.getTableName());
                        row.add(rds.getIndexName());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    columns.put("Object Type", true);
                    columnWidths.add(100);

                    columns.put("Object Schema", true);
                    columnWidths.add(100);
                    
                    columns.put("Object Name", true);
                    columnWidths.add(100);

                    columns.put("Index Name", true);
                    columnWidths.add(100);
                    
                    columns.put("Count Star", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Wait", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Wait", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Wait", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Wait", true);
                    columnWidths.add(100);
                    
                    columns.put("Count Read", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Read", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Read", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Read", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Read", true);
                    columnWidths.add(100);
                    
                    columns.put("Count Write", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Write", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Write", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Write", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Write", true);
                    columnWidths.add(100);
                    
                    columns.put("Count Fetch", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Fetch", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Fetch", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Fetch", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Fetch", true);
                    columnWidths.add(100);
                                        
                    columns.put("Count Insert", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Insert", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Insert", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Insert", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Insert", true);
                    columnWidths.add(100);
                    
                    columns.put("Count Update", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Update", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Update", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Update", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Update", true);
                    columnWidths.add(100);
                    
                    columns.put("Count Delete", true);
                    columnWidths.add(100);
                    
                    columns.put("Sum Timer Delete", true);
                    columnWidths.add(100);
                    
                    columns.put("Min Timer Delete", true);
                    columnWidths.add(100);
                    
                    columns.put("Avg Timer Delete", true);
                    columnWidths.add(100);
                    
                    columns.put("Max Timer Delete", true);
                    columnWidths.add(100);
                    
                    sheets.add("Index Detailed Summary Report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        tableView.setSearchEnabled(true);
        
        TableColumn typeColumn = createColumn("Object Type", "type");        
        TableColumn databaseColumn = createColumn("Object Schema", "database");
        TableColumn tableNameColumn = createColumn("Object Name", "tableName");
        TableColumn indexNameColumn = createColumn("Index Name", "indexName");
        TableColumn countStarColumn = createColumn("Count Star", "countStar");
        TableColumn sumTimerWaitColumn = createColumn("Sum Timer Wait", "sumTimerWait");
        TableColumn minTimerWaitColumn = createColumn("Min Timer Wait", "minTimerWait");
        TableColumn avgTimerWaitColumn = createColumn("Avg Time Wait", "avgTimerWait");
        TableColumn maxTimerWaitColumn = createColumn("Max Timer Wait", "maxTimerWait");
        TableColumn countReadColumn = createColumn("Count Read", "countRead");
        TableColumn sumTimerReadColumn = createColumn("Sum Timer Read", "sumTimerRead");
        TableColumn minTimerReadColumn = createColumn("Min Timer Read", "minTimerRead");
        TableColumn avgTimerReadColumn = createColumn("Avg Time Read", "avgTimerRead");
        TableColumn maxTimerReadColumn = createColumn("Max Timer Read", "maxTimerRead");
        TableColumn countWriteColumn = createColumn("Count Write", "countWrite");
        TableColumn sumTimerWriteColumn = createColumn("Sum Timer Write", "sumTimerWrite");
        TableColumn minTimerWriteColumn = createColumn("Min Timer Write", "minTimerWrite");
        TableColumn avgTimerWriteColumn = createColumn("Avg Time Write", "avgTimerWrite");
        TableColumn maxTimerWriteColumn = createColumn("Max Timer Write", "maxTimerWrite");
        TableColumn countFetchColumn = createColumn("Count Fetch", "countFetch");
        TableColumn sumTimerFetchColumn = createColumn("Sum Timer Fetch", "sumTimerFetch");
        TableColumn minTimerFetchColumn = createColumn("Min Timer Fetch", "minTimerFetch");
        TableColumn avgTimerFetchColumn = createColumn("Avg Time Fetch", "avgTimerFetch");
        TableColumn maxTimerFetchColumn = createColumn("Max Timer Fetch", "maxTimerFetch");
        TableColumn countInsertColumn = createColumn("Count Insert", "countInsert");
        TableColumn sumTimerInsertColumn = createColumn("Sum Timer Insert", "sumTimerInsert");
        TableColumn minTimerInsertColumn = createColumn("Min Timer Insert", "minTimerInsert");
        TableColumn avgTimerInsertColumn = createColumn("Avg Time Insert", "avgTimerInsert");
        TableColumn maxTimerInsertColumn = createColumn("Max Timer Insert", "maxTimerInsert");
        TableColumn countUpdateColumn = createColumn("Count Update", "countUpdate");
        TableColumn sumTimerUpdateColumn = createColumn("Sum Timer Update", "sumTimerUpdate");
        TableColumn minTimerUpdateColumn = createColumn("Min Timer Update", "minTimerUpdate");
        TableColumn avgTimerUpdateColumn = createColumn("Avg Time Update", "avgTimerUpdate");
        TableColumn maxTimerUpdateColumn = createColumn("Max Timer Update", "maxTimerUpdate");
        TableColumn countDeleteColumn = createColumn("Count Delete", "countDelete");
        TableColumn sumTimerDeleteColumn = createColumn("Sum Timer Update", "sumTimerDelete");
        TableColumn minTimerDeleteColumn = createColumn("Min Timer Delete", "minTimerDelete");
        TableColumn avgTimerDeleteColumn = createColumn("Avg Time Delete", "avgTimerDelete");
        TableColumn maxTimerDeleteColumn = createColumn("Max Timer Delete", "maxTimerDelete");
        
        tableView.addTableColumn(typeColumn, 100, 100, false);
        tableView.addTableColumn(databaseColumn, 100, 100, false);
        tableView.addTableColumn(tableNameColumn, 150, 150, false);
        tableView.addTableColumn(indexNameColumn, 150, 150, false);
        tableView.addTableColumn(countStarColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(minTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(countReadColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerReadColumn, 100, 100, false);
        tableView.addTableColumn(minTimerReadColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerReadColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerReadColumn, 100, 100, false);
        tableView.addTableColumn(countWriteColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerWriteColumn, 100, 100, false);
        tableView.addTableColumn(minTimerWriteColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerWriteColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerWriteColumn, 100, 100, false);
        tableView.addTableColumn(countFetchColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerFetchColumn, 100, 100, false);
        tableView.addTableColumn(minTimerFetchColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerFetchColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerFetchColumn, 100, 100, false);
        tableView.addTableColumn(countInsertColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerInsertColumn, 100, 100, false);
        tableView.addTableColumn(minTimerInsertColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerInsertColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerInsertColumn, 100, 100, false);
        tableView.addTableColumn(countUpdateColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerUpdateColumn, 100, 100, false);
        tableView.addTableColumn(minTimerUpdateColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerUpdateColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerUpdateColumn, 100, 100, false);
        tableView.addTableColumn(countDeleteColumn, 100, 100, false);
        tableView.addTableColumn(sumTimerDeleteColumn, 100, 100, false);
        tableView.addTableColumn(minTimerDeleteColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerDeleteColumn, 100, 100, false);
        tableView.addTableColumn(maxTimerDeleteColumn, 100, 100, false);
                        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }    
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {

        final Stage dialog = new Stage();

        SlowQueryTemplateCellController cellController = ((MainController)getController()).getTypedBaseController("SlowQueryTemplateCell");
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    private String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    log.error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }

    @Override
    public BaseController getReportController() {
        return this;
    }
}
