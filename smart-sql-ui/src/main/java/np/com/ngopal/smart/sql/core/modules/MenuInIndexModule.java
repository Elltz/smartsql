package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInIndexModule extends AbstractStandardModule {

    private Image tableImage;
    
    private MenuItem manageIndex;
    private MenuItem editIndex;
    private MenuItem dropIndex;
    private MenuItem createIndex;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 1;
    }
    
    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();
        
        manageIndex = getManageIndexesMenuItem(null);
        editIndex = getEditIndexMenuItem(null);
        dropIndex = getDropIndexMenuItem(null);
        createIndex = getCreateIndexMenuItem(null);
        
        SeparatorMenuItem separate = new SeparatorMenuItem();
        Menu indexMenu = new Menu("Indexes", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getIndex_image()), 18, 18));
        indexMenu.getItems().addAll(createIndex, editIndex, dropIndex, separate, manageIndex);
                
        manageIndex.disableProperty().bind(busy());
        editIndex.disableProperty().bind(busy());
        dropIndex.disableProperty().bind(busy());
        createIndex.disableProperty().bind(busy());
        indexMenu.disableProperty().bind(busy());
        
        /**
         * Toolbar other menuitem -column menu items are inactive.
         */
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{indexMenu});
        
        if (tableImage == null) {
            tableImage = SmartImageProvider.getInstance().getTableTab_image();
        }
        
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        
        boolean isTable = item.getValue() instanceof DBTable;
        boolean isInsideTable = item.getValue() instanceof String;
        boolean isIndex = item.getValue() instanceof Index;
        
        manageIndex.setDisable(!isTable && !isInsideTable && !isIndex);
        editIndex.setDisable(!isIndex);
        dropIndex.setDisable(!isIndex);
        createIndex.setDisable(!isTable && !isInsideTable && !isIndex);
        
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        SeparatorMenuItem separate = new SeparatorMenuItem();

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("index", new ArrayList<>(Arrays.asList(new MenuItem[]{getCreateIndexMenuItem(null), getEditIndexMenuItem(null), getDropIndexMenuItem(null), separate, getManageIndexesMenuItem(null)}))), Hooks.QueryBrowser.INDEX_ITEM);
 
        return map;
    }
    
    private MenuItem getCreateIndexMenuItem(ConnectionSession session) {
        MenuItem createIndexMenuItem = new MenuItem("Create Index",
                 new ImageView(SmartImageProvider.getInstance().getCreateIndex_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createIndexMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createIndexMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AlterTableController altertableController = showAlterTable(true);
                    if (altertableController != null) {
                        altertableController.manageIndexes();
                        altertableController.createIndex();
                    }
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }
            }
        });
        return createIndexMenuItem;
    }


    private MenuItem getManageIndexesMenuItem(ConnectionSession session) {
        MenuItem manageIndexesMenuItem = new MenuItem("Manage Indexes", new ImageView(SmartImageProvider.getInstance().getManage_image()));
        manageIndexesMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F7));
        manageIndexesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                manageIndexes();
            }
        });
        
        return manageIndexesMenuItem;
    }
    
    public void manageIndexes() {
        try {
            AlterTableController altertableController = showAlterTable(true);
            if (altertableController != null) {
                altertableController.manageIndexes();
            }
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }


    private MenuItem getEditIndexMenuItem(ConnectionSession session) {
        MenuItem editIndexMenuItem = new MenuItem("Edit Index", new ImageView(SmartImageProvider.getInstance().getEditIndex_image()));
        editIndexMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        editIndexMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AlterTableController altertableController = showAlterTable(true);
                    if (altertableController != null) {
                        altertableController.manageIndexes();
                    }
                    
                    ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(
                        session!= null? session : getController().getSelectedConnectionSession().get());
                    if (object.get().getValue() instanceof Index) {
                        altertableController.editIndex((Index)object.get().getValue());
                    }
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }
            }
        });
        return editIndexMenuItem;
    }

    private MenuItem getDropIndexMenuItem(ConnectionSession session) {
        MenuItem dropIndexMenuItem = new MenuItem("Drop Index...", new ImageView(SmartImageProvider.getInstance().getDropIndex_image()));
        dropIndexMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropIndexMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                
                ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(
                        currentSession);
                
                if (object.get().getValue() instanceof Index) {
                    Index c = (Index)object.get().getValue();
                    DBTable table = (DBTable) object.get().getParent().getParent().getValue();
                    DialogResponce responce = DialogHelper.showConfirm(getController(), "Delete index", "Do you really want to drop index (" + c.getName() + ")?");
                    if (responce == DialogResponce.OK_YES) {
                        try {
                            currentSession.getService().dropIndex(table.getDatabase().getName(), table.getName(), c.getName());                        
                            LeftTreeHelper.indexDroped(currentSession.getConnectionParam(), object.get());                        
                            DialogHelper.showInfo(getController(), "Delete index", "Index deleted successfully", null);
                        } catch (SQLException ex) {
                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                        }   
                    }
                }
            }
        });
        return dropIndexMenuItem;
    }

    // @TODO Need make some singleton for methods like this
    private AlterTableController showAlterTable(boolean alterMode) throws SQLException {
        
        ConnectionSession currentSession = getController().getSelectedConnectionSession().get();        
        ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(currentSession);
        
        Object obj = object.get().getValue();
        
        DBTable selectedTable = null;
        if (obj instanceof DBTable) {
            selectedTable = (DBTable)obj;

        } else if (obj instanceof Index) {                
            selectedTable = (DBTable) object.get().getParent().getParent().getValue();

        } else if (obj instanceof String) {
            selectedTable = (DBTable) (object.get().getParent()).getValue();
        }

        AlterTableController altertableController = null;        
        if (selectedTable != null) {
            
            Tab alterTab = null;

            ConnectionTabContentController connTab = (ConnectionTabContentController) getController().getTabControllerBySession(currentSession);
            List<Tab> tabs = connTab.findControllerTabs(AlterTableController.class);
            if (tabs != null && !tabs.isEmpty()) {

                for (Tab t: tabs) {
                    if (((AlterTableController)t.getUserData()).getTable().equals(selectedTable)) {
                        altertableController = (AlterTableController)t.getUserData();
                        alterTab = t;
                        break;
                    }
                }
            }

            if (altertableController != null) {
                getController().showTab(alterTab, currentSession);
            } else {
                
                altertableController = getTypedBaseController("AlterTable");
                altertableController.setService(currentSession.getService());

                alterTab = new Tab(selectedTable.toString());
                alterTab.setGraphic(new ImageView(tableImage));
                alterTab.setUserData(altertableController);

                Parent p = altertableController.getUI();
                alterTab.setContent(p);

                DBTable table = DatabaseCache.getInstance().getTable(currentSession.getConnectionParam().cacheKey(), 
                    selectedTable.getDatabase().toString(),
                    selectedTable.toString());

                altertableController.init(alterTab, table, currentSession, alterMode);

                getController().addTab(alterTab, currentSession);
                getController().showTab(alterTab, currentSession);
            }
        } else {
            log.error("Table is NULL");
        }
        
        return altertableController;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
