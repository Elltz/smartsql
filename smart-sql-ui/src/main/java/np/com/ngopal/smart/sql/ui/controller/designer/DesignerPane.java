
package np.com.ngopal.smart.sql.ui.controller.designer;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Region;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public interface DesignerPane {
    
    DesignerData getData();
    
    Region getRegion();
    
    boolean canChangeHeight();
    
    default void addDefaultDataListeners() {
        
        getData().x().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            getRegion().setLayoutX(newValue != null ? newValue.doubleValue() : 0);
        });
        
        getData().y().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            getRegion().setLayoutY(newValue != null ? newValue.doubleValue() : 0);
        }); 
        
        getData().width().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            getRegion().setPrefWidth(newValue != null ? newValue.doubleValue() : 0);
        });
        
        getData().height().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            // using for collapsed objects
            if (canChangeHeight()) {
                getRegion().setPrefHeight(newValue != null ? newValue.doubleValue() : 0);
            }
        });
    }
}
