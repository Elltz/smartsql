package np.com.ngopal.smart.sql.ui.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.modules.ReportsModule;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportTemplateController extends BaseController implements Initializable, ReportsContract {
    
    public static final String DATABASE_ITEM__EMPTY      = "";
    public static final String DATABASE_ITEM__ALL_TABLES = "All tables";
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label titleLabel;
    
    @FXML
    private FilterableTableView tableView;
    @FXML
    private ComboBox<String> databaseCombobox;    
    @FXML
    private Button exportButton;
    @FXML
    private ToggleButton gridButton;
    @FXML
    private ToggleButton piechartButton;
    @FXML
    private ToggleButton barchartButton;
        
    @FXML
    private HBox filterBox;
    @FXML
    private HBox chartButtons;
    
    @FXML
    private PieChart piechart;
    @FXML
    private StackedBarChart<String, Number> barchart;
    @FXML
    private CategoryAxis categoryAxis;
    @FXML
    private NumberAxis numberAxis;
        
    
    @FXML
    private Button refreshButton;
    
    private ConnectionSession session;
    
    private boolean disableListen = true;
    
    private ReportsModule.ReportConfiguration configuration;
    
    private boolean columnsInited = false;
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            byte[] barChartImg = null;
            byte[] pieChartImg = null;
            
            if (configuration.isWithCharts()) {
                byte[] pieChartImage;
                try {
                    WritableImage image = piechart.snapshot(new SnapshotParameters(), null);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

                    pieChartImage = bos.toByteArray();
                } catch (Throwable e) {
                    log.error("Error", e);
                    pieChartImage = null;                
                }

                byte[] barChartImage;
                try {
                    WritableImage image = barchart.snapshot(new SnapshotParameters(), null);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", bos);

                    barChartImage = bos.toByteArray();
                } catch (Throwable e) {
                    log.error("Error", e);
                    barChartImage = null;                
                }

                barChartImg = barChartImage;
                pieChartImg = pieChartImage;
            }
            
            byte[] finalBarChartImg = barChartImg;
            byte[] finalPieChartImg = pieChartImg;
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportTemplateData rds: (List<ReportTemplateData>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.addAll(rds.values());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    for (int i = 1; i < tableView.getTable().getColumns().size(); i++) {
                        TableColumn tc = tableView.getColumnByIndex(i);
                        columns.put(((Label)tc.getGraphic()).getText(), true);
                        columnWidths.add((int)tc.getWidth());
                    }
                                        
                    sheets.add(configuration.getTitle());
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }              
                
                if (configuration.isWithCharts()) {
                    {
                        ObservableList rowsData = FXCollections.observableArrayList();

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        sheets.add("Bar chart");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);

                        images.add(finalBarChartImg);
                        heights.add(30);
                        colspans.add(14);
                    }

                    {
                        ObservableList rowsData = FXCollections.observableArrayList();

                        List<Integer> columnWidths = new ArrayList<>();
                        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                        sheets.add("Pie chart");
                        totalData.add(rowsData);
                        totalColumns.add(columns);
                        totalColumnWidths.add(columnWidths);

                        images.add(finalPieChartImg);
                        heights.add(30);
                        colspans.add(14);
                    }
                }
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    @FXML
    public void refreshAction(ActionEvent event) {
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            if (session != null) {

                if (configuration.beforeLoad()) {

                    XYChart.Series series = null;
                    
                    if (configuration.isWithCharts()) {
                        Platform.runLater(() -> {
                            piechart.getData().clear();                        
                            barchart.getData().clear();
                        });                        
                        series = new XYChart.Series();
                    }
                    
                    try {
                        Connection connection = session.getService().getDB();

                        if (configuration.getBeforeQueryFunction() != null) {
                            connection.createStatement().execute(configuration.getBeforeQueryFunction());
                        }
                        
                        Map<String, Object> map = configuration.getResultFunction("");
                        if (map == null) {
                            try (ResultSet rs = connection.createStatement().executeQuery(configuration.getQuery(databaseCombobox.getSelectionModel().getSelectedItem()))) {
                                if (rs != null) {

                                    if (!columnsInited) {
                                        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                            TableColumn column = createColumn(rs.getMetaData().getColumnLabel(i), rs.getMetaData().getColumnLabel(i), configuration.isNeedBlobDialog(rs.getMetaData().getColumnLabel(i)));
                                            int width = configuration.getColumnWidth(i);
                                            Platform.runLater(() -> tableView.addTableColumn(column, width, width, false));
                                        }

                                        if (configuration.isWithCharts()) {
                                            series.setName(rs.getMetaData().getColumnLabel(configuration.getChartValueIndex()));
                                        }

                                        columnsInited = true;
                                    }

                                    int index = 0;
                                    List<ReportTemplateData> list = new ArrayList<>();
                                    while (rs.next()) {

                                        ReportTemplateData data = new ReportTemplateData();
                                        list.add(data);

                                        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                            data.put(rs.getMetaData().getColumnLabel(i), rs.getObject(i));
                                        }

                                        if (configuration.isWithCharts() && index < 10) {

                                            String key = data.get(rs.getMetaData().getColumnLabel(configuration.getChartKeyIndex())).toString();
                                            Double value = Double.valueOf(data.get(rs.getMetaData().getColumnLabel(configuration.getChartValueIndex())).toString());

                                            PieChart.Data d = new PieChart.Data(key, value);

                                            String text = key + ": " + value;

                                            Tooltip tooltip = new Tooltip();
                                            tooltip.setText(text);

                                            d.nodeProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                                                if (newValue instanceof Node) {
                                                    Tooltip.install((Node) newValue, tooltip);
                                                }
                                            });

                                            Platform.runLater(() -> piechart.getData().add(d));

                                            Tooltip tooltip0 = new Tooltip();
                                            tooltip0.setText(text);
                                            XYChart.Data d0 = new XYChart.Data(key, value);
                                            d0.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                                if (newValue instanceof Node) {
                                                    Tooltip.install((Node) newValue, tooltip0);
                                                }
                                            });

                                            XYChart.Series finalSeries = series;
                                            Platform.runLater(() -> finalSeries.getData().add(d0));
                                        }

                                        index++;
                                    }

                                    if (configuration.isWithCharts()) {
                                        XYChart.Series finalSeries = series;
                                        Platform.runLater(() -> {
                                            barchart.getData().addAll(finalSeries);
                                            barchart.setCategoryGap(50);
                                        });
                                    }

                                    tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                                }
                            }
                        } else {
                            if (!columnsInited) {
                                List<String> columns = (List<String>) map.get("columns");
                                
                                List<String[]> dataList = (List<String[]>) map.get("data");
                                        
                                for (int i = 0; i < columns.size(); i++) {
                                    TableColumn column = createColumn(columns.get(i), columns.get(i), configuration.isNeedBlobDialog(columns.get(i)));
                                    int width = configuration.getColumnWidth(i);
                                    Platform.runLater(() -> tableView.addTableColumn(column, width, width, false));
                                }

                                if (configuration.isWithCharts()) {
                                    series.setName(columns.get(configuration.getChartValueIndex()));
                                }

                                columnsInited = true;

                                int index = 0;
                                List<ReportTemplateData> list = new ArrayList<>();
                                for (int j = 0; j < dataList.size(); j++) {

                                    ReportTemplateData data = new ReportTemplateData();
                                    list.add(data);

                                    for (int i = 0; i < columns.size(); i++) {
                                        data.put(columns.get(i), dataList.get(j)[i]);
                                    }

                                    if (configuration.isWithCharts() && index < 10) {

                                        String key = data.get(columns.get(configuration.getChartKeyIndex())).toString();
                                        Double value = Double.valueOf(data.get(columns.get(configuration.getChartValueIndex())).toString());

                                        PieChart.Data d = new PieChart.Data(key, value);

                                        String text = key + ": " + value;

                                        Tooltip tooltip = new Tooltip();
                                        tooltip.setText(text);

                                        d.nodeProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                                            if (newValue instanceof Node) {
                                                Tooltip.install((Node) newValue, tooltip);
                                            }
                                        });

                                        Platform.runLater(() -> piechart.getData().add(d));

                                        Tooltip tooltip0 = new Tooltip();
                                        tooltip0.setText(text);
                                        XYChart.Data d0 = new XYChart.Data(key, value);
                                        d0.nodeProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                                            if (newValue instanceof Node) {
                                                Tooltip.install((Node) newValue, tooltip0);
                                            }
                                        });

                                        XYChart.Series finalSeries = series;
                                        Platform.runLater(() -> finalSeries.getData().add(d0));
                                    }

                                    index++;
                                }

                                if (configuration.isWithCharts()) {
                                    XYChart.Series finalSeries = series;
                                    Platform.runLater(() -> {
                                        barchart.getData().addAll(finalSeries);
                                        barchart.setCategoryGap(50);
                                    });
                                }

                                tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                            }
                        }
                    } catch (SQLException ex) {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Error", configuration.getQueryErrorMessage() != null ? configuration.getQueryErrorMessage() : ex.getMessage(), null));
                    }

                    configuration.afterLoad();
                    
                } else if (configuration.getBeforeLoadMessage() != null) {
                    Platform.runLater(() -> ((MainController)getController()).showInfo("Error", configuration.getBeforeLoadMessage(), null));
                }
            }
            
            Platform.runLater(() -> makeBusy(false));
        });
        th.setDaemon(true);
        th.start();
    }
        
    
    public void init(ConnectionSession session, ReportsModule.ReportConfiguration configuration) {
        this.session = session;
        this.configuration = configuration;
        
        titleLabel.setText(configuration.getTitle());
        
        disableListen = false;
        
        databaseCombobox.getItems().add(DATABASE_ITEM__EMPTY);
        databaseCombobox.getItems().add(DATABASE_ITEM__ALL_TABLES);
                
        if (session != null && session.getConnectionParam() != null) {
            if (session.getConnectionParam().getDatabases() != null) {
                for (Database db: session.getConnectionParam().getDatabases()) {
                    databaseCombobox.getItems().add(db.getName());
                }
            }
        }
        
        databaseCombobox.getSelectionModel().select(0);
        
        disableListen = true;
        
        filterBox.managedProperty().bind(filterBox.visibleProperty());
        filterBox.setVisible(configuration.isWithDatabase());
        
        chartButtons.managedProperty().bind(chartButtons.visibleProperty());
        chartButtons.setVisible(configuration.isWithCharts());
                
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)barchartButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getBarChart_image());
        ((ImageView)piechartButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getPieChart_image());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        tableView.setSearchEnabled(true);
        
        piechart.managedProperty().bind(piechart.visibleProperty());
        tableView.managedProperty().bind(tableView.visibleProperty());
        
        numberAxis.setAutoRanging(false);
        
        barchart.setCategoryGap(50);
        
        gridButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                tableView.toFront();
            }
        });
        
        barchartButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                barchart.toFront();
            }
        });
        
        piechartButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!gridButton.isSelected() && !piechartButton.isSelected() && !barchartButton.isSelected()) {
                gridButton.setSelected(true);
            }
            
            if (newValue != null && newValue) {
                piechart.toFront();
            }
        });
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        databaseCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (disableListen) {
                refreshAction(null);
            }
        });
    }
    
    
    private TableColumn createColumn(String title, String property, boolean needBlobDialog) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReportTemplateData, Object>,ObservableValue<Object>>() {
            @Override
            public ObservableValue<Object> call(TableColumn.CellDataFeatures<ReportTemplateData, Object> param) {
                return new SimpleObjectProperty<>(param.getValue().get(property));
            }
        });
        
        if (needBlobDialog) {
            column.setCellFactory(new Callback<TableColumn<ReportTemplateData, String>, TableCell<ReportTemplateData, String>>() {
                @Override
                public TableCell<ReportTemplateData, String> call(TableColumn<ReportTemplateData, String> param) {
                    return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Running query"));
                }
            });
        }
        
        return column;
    }    
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {

        final Stage dialog = new Stage();
        
        SlowQueryTemplateCellController cellController = StandardBaseControllerProvider.getController(getController(), "SlowQueryTemplateCell");
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    private String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    log.error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }
    
    
    private class ReportTemplateData extends LinkedHashMap<String, Object> implements FilterableData {

        @Override
        public String[] getValuesFoFilter() {
            return values().stream().map((Object obj) -> obj != null ? obj.toString() : "").collect(Collectors.toList()).toArray(new String[0]);
        }

        @Override
        public boolean equals(Object o) {
            return o == this;
        }        
        
    }
}


