/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author terah laweh (rumorsapp@gmail.com)
 */
public class ImportResultController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Button warningsButton;
    
    @FXML
    private Label recordsField;
    @FXML
    private Label deletedField;
    @FXML
    private Label skippedField;
    @FXML
    private Label warningsField;
    
    private Stage dialog;
    
    private QueryResult queryResult;
    
    
    @FXML
    public void showWarningsAction(ActionEvent event) {
        ShowWarningsController contr = ((MainController)getController()).getTypedBaseController("ShowWarnings");
        contr.init(queryResult, dialog);
    }
    
    @FXML
    public void okAction(ActionEvent event) {
        dialog.close();
    }
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
    }
    
    public void init(int records, int deleted, int skipped, int warnings, QueryResult queryResult, Window win){
        
        this.queryResult = queryResult;
        if (warnings == 0) {
            warningsButton.setDisable(false);
        }
        
        recordsField.setText(String.valueOf(records));
        deletedField.setText(String.valueOf(deleted));
        skippedField.setText(String.valueOf(skipped));
        warningsField.setText(String.valueOf(warnings));
        
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("CSV Import Result");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();        
    }
    
}
