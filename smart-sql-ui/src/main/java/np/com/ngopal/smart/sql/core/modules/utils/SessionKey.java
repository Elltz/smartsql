/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class SessionKey implements Comparable<SessionKey>{
    
    public static transient byte STATUS_INTERRUPTED = 0;
    public static transient byte STATUS_USER_SAVED = 1;
    public static transient byte STATUS_SYSTEM_SAVED = 2;
    
    
    private byte status = STATUS_SYSTEM_SAVED;
    private String name = "";
    private Long id;
    
    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    
    public byte getStatus(){
        return status;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String s){
        name = s;
    }
    
    public void setStatus(byte s){
        status = s;
    }
    
    private String order = "";
    
    public String getOrder(){
        return order;
    }
    
    public void setOrder(String order){
        this.order = order;
    }

    @Override
    public boolean equals(Object obj) { 
        if(obj != null && obj instanceof SessionKey){
            SessionKey sk = (SessionKey)obj;
            return this.name.equals(sk.getName()) &&
                    this.order.equals(sk.getOrder());
        }
        return false;
    }
    
    

    @Override
    public int compareTo(SessionKey o) { 
        return order.equals(o.getOrder()) ? 0 : 1;
    }

    @Override
    public String toString() {
        return "SessionKey{" + "status=" + status + ", name=" + name + ", order=" + order + '}';
    }
    
    public SessionKey(){}
    
    public SessionKey(String s){
        this();
        StringBuilder sb = new StringBuilder(s.length());
        byte who = -1;
        for(char c : s.substring(s.indexOf("{")+1)
                .toCharArray()){
            if(c == ',' || c == '}'){
                if(who == 0){
                    setStatus(Byte.valueOf(sb.toString()));
                }else if(who == 1){
                    setName(sb.toString());
                }else if(who == 2){
                    setOrder(sb.toString());
                }
            }else if(c == '='){                
                sb.delete(0, sb.length());
                who++;
            }else{
                sb.append(c);
            }
        }
    }
    
}
