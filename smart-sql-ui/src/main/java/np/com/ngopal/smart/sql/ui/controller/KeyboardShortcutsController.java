/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class KeyboardShortcutsController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private ComboBox<String> sectionIndicator;

    @FXML
    private TableView<String[]> tableview;

    @FXML
    private TableColumn<String[], String> keyboardShortcut;

    @FXML
    private TableColumn<String[], String> context;
    
    private String[][] keyboardShortcutsMappings = new String[][]{
            new String[]{"CONNECTION",""}
            , new String[]{"CTRL + M","Create a new connection"}
            ,new String[]{"CTRL + N","Create a new connection with current connection settings"}
            , new String[]{"CTRL + F4 OR CTRL + W","Disconnection current connection"}
            ,new String[]{"CTRL + TAB","Switch to next connection"}
            , new String[]{"CTRL + SHIFT + TAB","Switch to previous connection"}
            , new String[]{"CTRL + 1...8","Select connection (1 through 8)"}
            ,new String[]{"CTRL + 9","Select last connection"}
            , new String[]{"OBJECT BROWSER",""}
            ,new String[]{"F5","Referesh object browser"}
            , new String[]{"CTRL + B","Set focus on object browser"}
            , new String[]{"CTRL + SHIFT + B","Set focus on object browser filter"}
            ,new String[]{"SQL WINDOW",""}
            , new String[]{"CTRL + T","New query editor"}
            ,new String[]{"ALT + F2","rename query editor"}
            , new String[]{"CTRL + E","Set focus on SQL window"}
            , new String[]{"CTRL + Y","Redo"}
            ,new String[]{"CTRL + Z","Undo"}
            , new String[]{"CTRL + X","Cut"}
            ,new String[]{"CTRL + C","Copy"}
            , new String[]{"CTRL + V","Paste"}
            , new String[]{"CTRL + H","Replace"}
            ,new String[]{"CTRL + O","Open file"}
            , new String[]{"CTRL + SHIFT + U","Make selection uppercase"}
            ,new String[]{"CTRL + SHIFT + L","Make selection lowercase"}
            , new String[]{"CTRL +SHIFT +C","Comment selection in SQL window"}
            , new String[]{"CTRL + SHIFT + R","Remove comment from selection"}
            ,new String[]{"CTRL + SHIFT + T","Insert templates"}
            , new String[]{"CTRL + SPACE","List all tags"}
            ,new String[]{"CTRL + ENTER","List matching tags"}
            , new String[]{"CTRL + SHIFT + SPACE","List function and rountine parameters"}
            , new String[]{"QUERY EXECUTION",""}
            ,new String[]{"CTRL + F8","Execute current query and edit result"}
            , new String[]{"F9","Execute query"}
            ,new String[]{"CTRL + F9","Execute all query(s)"}
            , new String[]{"SQL FORMATTER",""}
            , new String[]{"F12","Format current query"}
            ,new String[]{"CTRL + F12","Format selected query"}
            , new String[]{"SHIFT + F12","Format all queries"}
            ,new String[]{"PASTE SQL STATEMENTS",""}
            , new String[]{"ALT + SHIFT + I","Inset statement"}
            , new String[]{"ALT + SHIFT + U","Update statement"}
            ,new String[]{"ALT + SHIFT + D","Delete statement"}
            , new String[]{"ALT + SHIFT + S","Select statement"}
            ,new String[]{"RESULT",""}
            , new String[]{"F11","Insert update dialog"}
            , new String[]{"CTRL + R","Set focus to the active tab in result pane"}
            ,new String[]{"CTRL + L","Switch result window/table data between Grid/Text Mode"}
            , new String[]{"CTRL + ALT + C","Export table data as CSV, EXCEL, SQL etc"}
            ,new String[]{"CTRL + ALT + E","Backup data as SQL dump"}
            , new String[]{"CTRL + SHIFT + M","Import data from CSV"}
            , new String[]{"CTRL + SHIFT + E","Export resultset"}
            , new String[]{"CTRL + SPACE","Open foreign key lookup"}
            ,new String[]{"ALT + 1...8","Select tab in result window (1 through 8)"}
            , new String[]{"ALT + 9","Select last tab in the result window"}
            ,new String[]{"CONTENT VISIBILITY",""}
            , new String[]{"CTRL + SHIFT + 1","Show/Hide object browser"}
            , new String[]{"CTRL + SHIFT + 2","Show/Hide result pane"}
            ,new String[]{"CTRL + SHIFT + 3","Show/Hide query window"}
            , new String[]{"DATABASE/TABLE",""}
            ,new String[]{"CTRL + D","Create database"}
            , new String[]{"F4","Create Table/Index/View/Stored proc/Function/Trigger/Event"}
            , new String[]{"F6","Alter Database/Table/Index/View/Stored proc/Function/Trigger/Event"}
            ,new String[]{"F2","Rename Table/View/Trigger/Event"}
            , new String[]{"SHIFT + DELETE","Truncate Database/Table"}
            ,new String[]{"F7","Manage Index window"}
            , new String[]{"F10","Relationships/Foreign keys"}
            , new String[]{"CTRL + ALT + R","Reorder Column(s)"}
            ,new String[]{"CTRL + ALT + T","Table diagnostics"}
            , new String[]{"CTRL + ALT + F","Flush dialog"}
            ,new String[]{"CTRL + SHIFT + ALT + S","Create schema for database in HTML"}
            , new String[]{"POWERTOOLS",""}
            , new String[]{"CTRL + ALT + W","Databse synchronization wizard"}
            ,new String[]{"CTRL + ALT + Q","Visual data compare"}
            , new String[]{"CTRL + Q","Schema synchronization tool"}
            ,new String[]{"CTRL + ALT + O","Import external data wizard"}
            , new String[]{"CTRL + ALT + N","Notification services wizard"}
            , new String[]{"CTRL + ALT + S","Scheduled backup wizard"}
            ,new String[]{"CTRL + K ","Query builder"}
            , new String[]{"CTRL + ALT + D","Schema designer"}
            ,new String[]{"USER MANAGER",""}
            , new String[]{"CTRL + U","Open user manager"}
            , new String[]{"FAVOURITES",""}
            ,new String[]{"CTRL + SHIFT + F","Add current SQL to favourites"}
            , new String[]{"VISUAL DATA COMPARE",""}
            ,new String[]{"CTRL + T","Select table"}
            , new String[]{"CTRL + ENTER","Compare"}
            , new String[]{"CTRL + H","Hide similar rows"}
            ,new String[]{"CTRL + N","Next difference"}
            , new String[]{"CTRL + P","Previous difference"}
            ,new String[]{"CTRL + L","Source"}
            , new String[]{"CTRL + R","Target"}
             , new String[]{"CTRL + RIGHT","Merge to target"}
            ,new String[]{"CTRL + LEFT","Merge to source"}
            , new String[]{"CTRL + Q","Preview SQL"}
            ,new String[]{"CTRL + S","Apply changes"}
            , new String[]{"CTRL + Z","Revert changes"}
            , new String[]{"CTRL + SHIFT + Z","Revert selected changes"}
            ,new String[]{"OTHER",""}
            , new String[]{"F1","Help"}
            ,new String[]{"CTRL + SHIFT + O","Open session savepoint"}
            , new String[]{"CTRL + SHIFT + S","Save session"}
             , new String[]{"CTRL + SHIFT + X","End session"}
            ,new String[]{"CTRL + SHIFT + H","Open history tab"}
            , new String[]{"CTRL + SHIFT + I","Open info tab"}
            ,new String[]{"CTRL + A","Select All"}
            , new String[]{"CTRL + F","Find (in all sectors)"}
            , new String[]{"CTRL + PgUP","Switch to previous tab"}
            ,new String[]{"CTRL + PgDown","Switch to next tab"}
            , new String[]{"DELETE","Delete selected"}
            ,new String[]{"ALT + SHIFT + L","Change language"}
            , new String[]{"ALT +L","Close tab"}
            , new String[]{"ALT + F4","Exit App"}
            };
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) { 
        
        keyboardShortcut.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<String[], String>,
                        ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<String[],
                    String> p) { 
                return new SimpleStringProperty(p.getValue()[0]);
            }
        });
        
        context.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<String[], String>,
                        ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<String[],
                    String> p) { 
                return new SimpleStringProperty(p.getValue()[1]);
            }
        });
        
        keyboardShortcut.setCellFactory(new Callback<TableColumn<String[], String>,
                TableCell<String[], String>>() {

            @Override
            public TableCell<String[], String> call(TableColumn<String[],
                    String> p) { 
                return new TableCell<String[], String>(){

                    @Override
                    protected void updateItem(String t, boolean bln) {
                        super.updateItem(t, bln);
                        if(bln){
                            setText("");
                            setStyle("-fx-background-color : whitesmoke;");
                        }else{
                            if(t.contains("+") || t.length() < 4){
                                getTableRow().setStyle("-fx-background-color: white;");
                                setTextFill(Color.BLACK);
                            }else{
                               getTableRow().setStyle("-fx-background-color: #3c71b3;");
                               setTextFill(Color.WHITE);
                            }
                            setText(t);
                        }
                    }

                    @Override
                    protected boolean isItemChanged(String t, String t1) {
                        if(sectionIndicator.getItems().contains(t1)){
                            sectionIndicator.getSelectionModel().select(t1);
                        }
                        return super.isItemChanged(t, t1);                         
                    }
                    
                };
            }
        });
        
        context.setCellFactory(new Callback<TableColumn<String[], String>,
                TableCell<String[], String>>() {

            @Override
            public TableCell<String[], String> call(TableColumn<String[],
                    String> p) { 
                return new TableCell<String[], String>(){

                    @Override
                    protected void updateItem(String t, boolean bln) {
                        super.updateItem(t, bln);
                        if(bln){
                            setText("");
                            setStyle("-fx-background-color : whitesmoke;");
                        }else{
                            setTextFill(Color.BLACK);
                            setText(t);
                        }
                    }
                    
                };
            }
        });
        
        sectionIndicator.getItems().addAll(
                "CONNECTION","OBJECT BROWSER","SQL WINDOW","QUERY EXECUTION",
                "SQL FORMATTER","PASTE SQL STATEMENTS","RESULT",
                "CONTENT VISIBILITY","DATABASE/TABLE","POWERTOOLS",
                "USER MANAGER","FAVOURITES","VISUAL DATA COMPARE","OTHER");
        
        tableview.getItems().addAll(keyboardShortcutsMappings);
        sectionIndicator.getSelectionModel().selectFirst();
//        sectionIndicator.getSelectionModel().selectedItemProperty().
//                addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> ov,
//                    String t, String t1) { 
//            }
//        });
        
//        sectionIndicator.getSelectionModel().selectedIndexProperty()
//                .addListener(new ChangeListener<Number>() {
//
//            @Override
//            public void changed(ObservableValue<? extends Number> ov,
//                    Number t, Number t1) {
//                System.out.println(tableview.getVisibleLeafIndex(context));
//                switch (t1.intValue()) {
//                    case 0:
//                        tableview.scrollTo(0);
//                        break;
//                    case 1:
//                        tableview.scrollTo(8);
//                        break;
//                    case 2:
//                        break;
//                    case 3:
//                        break;
//                    case 4:
//                        break;
//                    case 5:
//                        break;
//                }
//            }
//        });
                
        stage = new Stage(StageStyle.UTILITY);
        stage.setScene(new Scene(getUI()));
        stage.setResizable(false);
        stage.initOwner(getController().getStage());
        stage.setTitle("KEYBOARD SHORTCUTS");
        stage.show();
    }
 
   private Stage stage; 
    
}
