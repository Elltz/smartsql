package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class OpenSchemaDesignerDialogController extends BaseController implements Initializable {
   
    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button okButton;
    
    @FXML
    private RadioButton databaseRadioButton;
    @FXML
    private RadioButton schemaRadioButton;
    
    @FXML
    private ComboBox<Database> databaseComboBox;
    @FXML
    private TextField schemaDesignerFiled;

    private Stage stage;

    private Database selectedDatabase;
    private String newSchemaName;
    
    private final List<String> IGNORED_DBS = Arrays.asList(new String[]{"mysql", "information_schema", "sys", "performance_schema"});

    public String getSelectedDatabase() {
        return selectedDatabase != null ? selectedDatabase.getName() : null;
    }
    
    public String getSchemaName() {
        return newSchemaName;
    }
    
    @FXML
    void cancelAction(ActionEvent event) {
        selectedDatabase = null;
        newSchemaName = null;
                
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @FXML
    void okAction(ActionEvent event) {
        
        if (databaseRadioButton.isSelected()) {
            selectedDatabase = databaseComboBox.getSelectionModel().getSelectedItem();
            newSchemaName = null;
            
        } else {
            newSchemaName = schemaDesignerFiled.getText();
            selectedDatabase = null;
        }
        
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();

    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        okButton.disableProperty().bind(
            Bindings.and(
                databaseRadioButton.selectedProperty(), 
                Bindings.isNotNull(databaseComboBox.getSelectionModel().selectedItemProperty())
            ).or(
                Bindings.and(
                    schemaRadioButton.selectedProperty(), 
                    Bindings.isNotEmpty(schemaDesignerFiled.textProperty())
                )
            ).not()
        );
        
        databaseComboBox.disableProperty().bind(schemaRadioButton.selectedProperty());
        schemaDesignerFiled.disableProperty().bind(databaseRadioButton.selectedProperty());
    }
    
    public void init(List<Database> data) {
       databaseComboBox.getItems().addAll(data.stream().filter(d -> !IGNORED_DBS.contains(d.getName().toLowerCase())).collect(Collectors.toList()));
    }

}
