package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public class SearchPublicSchemaController  extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TextField searchField;
    @FXML
    private Button openButton;
    @FXML
    private Button searchButton;
    
    @FXML
    private ComboBox<DomainItem> domainCombobox;
    @FXML
    private ComboBox<DomainItem> subdomainCombobox;
    
    @FXML
    private TableColumn<ErrDiagram, String> nameColumn;
    @FXML
    private TableColumn<ErrDiagram, String> domainColumn;
    @FXML
    private TableColumn<ErrDiagram, String> subdomainColumn;
    @FXML
    private TableColumn<ErrDiagram, String> numberOfTablesColumn;
    @FXML
    private TableColumn<ErrDiagram, String> descriptionColumn;
    
    @FXML
    private TableView<ErrDiagram> publicDiagramsTable;
    
    private Stage dialog;
    
    private DBService service;
    private SchemaDesignerModule parentController;
    
    private final ObservableList data = FXCollections.observableArrayList();
        
    public void init(SchemaDesignerModule parentModule, DBService service) {
        this.service = service;
        this.parentController = parentModule;        
        
        domainCombobox.getItems().add(DomainItem.EMPTY);
        subdomainCombobox.getItems().add(DomainItem.EMPTY);
        
        // load domains
        try {
            List<Map<String, Object>> domains = PublicDataAccess.getInstanсe().loadDomains();
            if (domains != null) {

                for (Map<String, Object> m: domains) {
                    DomainItem di = new DomainItem((Long)m.get("id"), (String)m.get("name"));
                    domainCombobox.getItems().add(di);
                }

                domainCombobox.getSelectionModel().select(null);
            }
        }  catch (PublicDataException ex) {
            ((MainController)getController()).showError("Publish error", ex.getMessage(), ex);
            return;
        }
        
        // load subdomains
        try {
            List<Map<String, Object>> subdomains = PublicDataAccess.getInstanсe().loadSubDomains();
            if (subdomains != null) {

                for (Map<String, Object> m: subdomains) {
                    DomainItem di = new DomainItem((Long)m.get("id"), (String)m.get("name"));
                    subdomainCombobox.getItems().add(di);
                }

                subdomainCombobox.getSelectionModel().select(null);
            } 
        } catch (PublicDataException ex) {
            ((MainController)getController()).showError("Publish error", ex.getMessage(), ex);
            return;
        }
        
        domainCombobox.getSelectionModel().select(0);
        subdomainCombobox.getSelectionModel().select(0);
        
        if (dialog == null) {
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setResizable(false);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getStage());
            dialog.setTitle("Publish Schema");
            dialog.setScene(new Scene(getUI()));
        }
        
        dialog.showAndWait();
    }
    
    @FXML
    public void searchAction(ActionEvent event) {
        
        data.clear();
        
        domainCombobox.getSelectionModel().select(0);
        subdomainCombobox.getSelectionModel().select(0);
        
        String searchText = searchField.getText();

        try {
            List<ErrDiagram> list = PublicDataAccess.getInstanсe().searchErrDiagram(searchText);
            if (list != null) {
                data.addAll(list);
            } 
        } catch (PublicDataException ex) {
            log.error(ex);
        }
    }
    
    @FXML
    public void openAction(ActionEvent event) {     
        
        ConnectionParams cp = getController().getSelectedConnectionSession().get().getConnectionParam().copyOnlyParams();
        cp.setSelectedModuleClassName(SchemaDesignerModule.class.getName());
        cp.setErrDiagram(publicDiagramsTable.getSelectionModel().getSelectedItem());
        cp.setNeedReinit(true);
        
        // if yes need open connection
        if (parentController.canOpenConnection(cp)) {
            ((MainController)getController()).openConnection(service, cp, true, null);
            
        } else {
            
            if (parentController.getSelectedDiagram().get() != null) {
                DialogResponce responce = ((MainController)getController()).showConfirm("Save diagram", "Do you want save current Diagram?");
                if (DialogResponce.OK_YES == responce) {
                    parentController.saveDiagram();
                }
            }

            DesignerErrDiagram d = parentController.openDiagram(publicDiagramsTable.getSelectionModel().getSelectedItem());
            d.changed().set(true);
        }
        
        dialog.close();
    }

    @FXML
    public void cancelAction(ActionEvent event) {
        dialog.close();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        searchField.setOnAction((ActionEvent event) -> {
            searchAction(event);
        });
        
        publicDiagramsTable.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                openAction(null);
            }
        });
        
        searchButton.disableProperty().bind(Bindings.isEmpty(searchField.textProperty()));
        openButton.disableProperty().bind(Bindings.isNull(publicDiagramsTable.getSelectionModel().selectedItemProperty()));
        
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        domainColumn.setCellValueFactory(new PropertyValueFactory<>("domainName"));
        subdomainColumn.setCellValueFactory(new PropertyValueFactory<>("subDomainName"));
        numberOfTablesColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfTables"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
                
        publicDiagramsTable.setItems(new FilteredList<>(data));
        
        domainCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends DomainItem> observable, DomainItem oldValue, DomainItem newValue) -> {
            ((FilteredList<ErrDiagram>)publicDiagramsTable.getItems()).setPredicate(this::filterDiagram);
        });
        
        subdomainCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends DomainItem> observable, DomainItem oldValue, DomainItem newValue) -> {
            ((FilteredList<ErrDiagram>)publicDiagramsTable.getItems()).setPredicate(this::filterDiagram);
        });
    }
    
    
    private boolean filterDiagram(ErrDiagram t) {
        DomainItem di = domainCombobox.getSelectionModel().getSelectedItem();
        DomainItem sdi = subdomainCombobox.getSelectionModel().getSelectedItem();

        return (di == DomainItem.EMPTY || di.name.equals(t.getDomainName())) && (sdi == DomainItem.EMPTY || sdi.name.equals(t.getSubDomainName()));
    }
    
    private static class DomainItem {
        
        public static DomainItem EMPTY = new DomainItem(null, "All");
        
        private Long id;
        private String name;
        
        public DomainItem(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }        
    }

    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
}
