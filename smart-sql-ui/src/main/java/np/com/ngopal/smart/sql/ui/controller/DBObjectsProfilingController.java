package np.com.ngopal.smart.sql.ui.controller;

import lombok.extern.slf4j.Slf4j;

/**
 * TODO: REMOTE IT
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class DBObjectsProfilingController {//extends BaseController implements Initializable, ProfilerEntity {
//
//    @Inject
//    private DBProfilerService dbProfilerService;
//    
//    private ProfilerTabContentController parentController;
//
//    private final Image analyzerImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/analyze.png", 16, 16, true, true);
//    private final Image executeImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/execute.png", 16, 16, true, true);
//    
//    private final ObjectProperty<TreeItem> nullTreeItem = new SimpleObjectProperty<>();
//    private final ObjectProperty<Tab> nullTab = new SimpleObjectProperty<>();
//    private final BooleanProperty profilerStarted = new SimpleBooleanProperty(false);
//
//    private volatile boolean keepThreadsAlive = true;
//    private Connection connection;
//        
//    @FXML
//    private AnchorPane mainUI;
//
//    @FXML
//    private FlowPane dbObjectsFlowContainer;
//    
//    @FXML
//    private ScrollPane dbObjectsScrollContainer;
//    
//    @FXML
//    private Label zoomOutLabel;
//    @FXML
//    private Label zoomInLabel;
//    @FXML
//    private Label zoomMessageLabel;
//
//    @Getter
//    private FilterableTableViewController topQueriesTable;
//    private ChartTableViewController topTablesTable;
//    private ChartTableViewController topSchemasTable;
//    private ChartTableViewController topUsersTable;
//    private ChartTableViewController topHostsTable;
//
//    private WorkProc allBackgroundTask;
//
//    private final Image ratingGreenImage = new Image(getClass().getResource(
//            "/np/com/ngopal/smart/sql/ui/images/rating_green.png").toExternalForm());
//    private final Image ratingBlueImage = new Image(getClass().getResource(
//            "/np/com/ngopal/smart/sql/ui/images/rating_blue.png").toExternalForm());
//    private final Image ratingYellowImage = new Image(getClass().getResource(
//            "/np/com/ngopal/smart/sql/ui/images/rating_yellow.png").toExternalForm());
//    private final Image ratingRedImage = new Image(getClass().getResource(
//            "/np/com/ngopal/smart/sql/ui/images/rating_red.png").toExternalForm());
//
//    @Override
//    public void showUserCharts() {}
//    
//    @Override
//    public BooleanProperty profilerStarted() {
//        return profilerStarted;
//    }
//    
//    @Override
//    public void showSettings() {}
//
//    @Override
//    public void zoomInAction() {}
//
//    @Override
//    public void clearDraggedData() {}
//
//    @Override
//    public void zoomDragedAction(Long start, Long finish) {}
//
//    @Override
//    public void zoomOutAction() {}
//
//    @Override
//    public void showAllAction() {}
//
//    @Override
//    public void updateCharts() {}
//    
//    @Override
//    public void saveProfillerData(ProfilerData pd) {       
//        pd.setTopQueries(collectFilterTableData(topQueriesTable.getContentNodeAsType().getSourceData()));
//        pd.setTopTables(collectTopTableData(topTablesTable.getContentNodeAsType().getData()));
//        pd.setTopSchemas(collectTopTableData(topSchemasTable.getContentNodeAsType().getData()));
//        pd.setTopUsers(collectTopTableData(topUsersTable.getContentNodeAsType().getData()));
//        pd.setTopHosts(collectTopTableData(topHostsTable.getContentNodeAsType().getData()));
//    }
//
//    @Override
//    public void openData(ProfilerData data) {
//
//        if (topQueriesTable.isActive()) {
//            collectQueriesData(new HashMap<>(), topQueriesTable.getContentNodeAsType(), data.getTopQueries());
//        }
//        
//        if (topTablesTable.isActive()) {
//            collectTopData(topTablesTable.getContentNodeAsType(), data.getTopTables());
//        }
//        
//        if (topSchemasTable.isActive()) {
//            collectTopData(topSchemasTable.getContentNodeAsType(), data.getTopSchemas());
//        }
//        
//        if (topUsersTable.isActive()) {
//            collectTopData(topUsersTable.getContentNodeAsType(), data.getTopUsers());
//        }
//        
//        if (topHostsTable.isActive()) {
//            collectTopData(topHostsTable.getContentNodeAsType(), data.getTopHosts());
//        }
//    }
//    
//    public List<Object[]> collectTopTableData(List<ProfilerTopData> data) {
//        List<Object[]> list = new ArrayList<>();
//        for (ProfilerTopData f : data) {
//            list.add(f.getValues());
//        }
//
//        return list;
//    }
//
//    public List<Object[]> collectFilterTableData(List<FilterableData> data) {
//        List<Object[]> list = new ArrayList<>();
//        for (FilterableData f : data) {
//            list.add(((ProfilerQueryData) f).getValues());
//        }
//
//        return list;
//    }
//    
//    @Override
//    public void startProfiler() {
//        profilerStarted.set(true);
//        
//        createAndInitThread();
//    }
//    
//    @Override
//    public void clearProfilerData() {
//
//        topQueriesTable.getContentNodeAsType().addAndRemoveItems(new ArrayList<>(), new ArrayList<>(topQueriesTable.getContentNodeAsType().getSourceData()));
//
//        topTablesTable.clear();
//        topSchemasTable.clear();
//        topUsersTable.clear();
//        topHostsTable.clear();
//    }
//    
//    @Override
//    public void stopProfiler() {
//        profilerStarted.set(false);
//    }
//    
//    @Override
//    public boolean isValid() {
//        return true;
//    }
//    
//    @Override
//    public void createConnection() throws SQLException {
//        // Db service can change connection - so we use exectly our connection
//        connection = getDbService().connect(getConnectionParams());
//    }
//    
//    @Override
//    public void destroy() {
//        keepThreadsAlive = false;
//        
//        try {
//            if (connection != null) {
//                connection.close();
//            }
//        } catch (SQLException ex) {
//            log.error("Eror", ex);
//        }
//    }
//        
//    private DBService getDbService() {
//        return controller.getSelectedConnectionSession().get().getService();
//    }
//
//    @Override
//    public ConnectionParams getConnectionParams() {
//        return controller.getSelectedConnectionSession().get().getConnectionParam();
//    }
//
//    @Override
//    public ObjectProperty<TreeItem> getSelectedTreeItem() {
//        return nullTreeItem;
//    }
//
//    @Override
//    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
//        return nullTab;
//    }
//
//    private void collectTopData(ChartTableView chartTableView, List<Object[]> result) {
//        if (result != null) {
//
//            List<ProfilerTopData> list = new ArrayList<>();
//
//            for (Object[] row : result) {
//
//                ProfilerTopData ptd = new ProfilerTopData(
//                        (String) row[0],
//                        (BigDecimal) row[1],
//                        (BigDecimal) row[2]
//                );
//
//                list.add(ptd);
//            }
//
//            Platform.runLater(() -> {
//                synchronized (chartTableView) {
//                    chartTableView.setItems(list);
//                    chartTableView.refresh();
//                }
//            });
//        }
//    }
//
//
//    private void collectQueriesData(Map<String, ProfilerQueryData> mapTopQueries, FilterableTableView tableView, List<Object[]> result) {
//
//        List<ProfilerQueryData> allQueries = new ArrayList<>(mapTopQueries.values());
//        if (result != null) {
//
//            List<ProfilerQueryData> newOrOldQueries = new ArrayList<>();
//
//            for (Object[] row : result) {
//
//                String user = (String) row[0];
//                String host = (String) row[1];
//                String query = (String) row[2];
//                Integer queryId = (Integer) row[3];
//                BigDecimal threads = (BigDecimal) row[4];
//                BigDecimal time = (BigDecimal) row[5];
//
//                String graph1 = (String) row[6];
//                String graph2 = (String) row[7];
//
//                ProfilerQueryData ptq = mapTopQueries.get(query);
//                if (ptq == null) {
//                    ptq = new ProfilerQueryData(
//                            user,
//                            host,
//                            query,
//                            queryId,
//                            threads,
//                            time,
//                            graph1,
//                            graph2,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null
//                    );
//
//                    mapTopQueries.put(query, ptq);
//                } else {
//                    ptq.setHost(host);
//                    ptq.setUser(user);
//                    ptq.setQueryTemplate(query);
//                    ptq.setQueryId(queryId);
//                    ptq.setThreads(threads);
//                    ptq.setTime(time);
//
//                    allQueries.remove(ptq);
//                }
//
//                newOrOldQueries.add(ptq);
//            }
//
//            //newOrOldQueries.add(new ProfilerQueryData("test", "test", "test", 1, BigDecimal.ONE, BigDecimal.ONE));
//            Platform.runLater(() -> {
//
//                synchronized (tableView) {
//                    // remove that left in allQueries
//                    tableView.addAndRemoveItems(newOrOldQueries, allQueries);
//
//                    for (ProfilerQueryData p : allQueries) {
//                        mapTopQueries.remove(p.getQueryTemplate());
//                    }
//
//                    tableView.refresh();
//                }
//            });
//        }
//    }
//
//
//    @Override
//    public void promptRemainingMetric() {}
//
//    @Override
//    public OsServerSpecs getCurrentServerSpecs() {
//        return null;
//    }
//
//    
//    private void initQueryDataTable(FilterableTableView tableView, boolean withAnalyzer) {
//
//        TableColumn userColumn = new TableColumn("User");
//        userColumn.setCellValueFactory(new PropertyValueFactory<>("user"));
//        userColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, String>, TableCell<ProfilerQueryData, String>>() {
//            @Override
//            public TableCell<ProfilerQueryData, String> call(TableColumn<ProfilerQueryData, String> param) {
//                return new StringLimitTableCell(16);
//            }
//        });
//
//        TableColumn hostColumn = new TableColumn("Host");
//        hostColumn.setCellValueFactory(new PropertyValueFactory<>("host"));
//        hostColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, String>, TableCell<ProfilerQueryData, String>>() {
//            @Override
//            public TableCell<ProfilerQueryData, String> call(TableColumn<ProfilerQueryData, String> param) {
//                return new StringLimitTableCell(16);
//            }
//        });
//
//        TableColumn queryTemplateColumn = new TableColumn("Query");
//        queryTemplateColumn.setCellValueFactory(new PropertyValueFactory<>("queryTemplate"));
//        queryTemplateColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, String>, TableCell<ProfilerQueryData, String>>() {
//            @Override
//            public TableCell<ProfilerQueryData, String> call(TableColumn<ProfilerQueryData, String> param) {
//                return new QueryTableCell<>(10, (item) -> new String("" + (item != null ? item.toString().length() : 0)), (data, item) -> showPopup(new Label(item), data.getButton()));
//            }
//        });
//
//        TableColumn explainColumn = new TableColumn("Explain");
//        explainColumn.setCellValueFactory(new PropertyValueFactory<>("queryId"));
//        explainColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, Integer>, TableCell<ProfilerQueryData, Integer>>() {
//            @Override
//            public TableCell<ProfilerQueryData, Integer> call(TableColumn<ProfilerQueryData, Integer> param) {
//                return new ExplainTableCell();
//            }
//        });
//
//        TableColumn threadsColumn = new TableColumn("Threads");
//        threadsColumn.setCellValueFactory(new PropertyValueFactory<>("threads"));
//        threadsColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, BigDecimal>, TableCell<ProfilerQueryData, BigDecimal>>() {
//            @Override
//            public TableCell<ProfilerQueryData, BigDecimal> call(TableColumn<ProfilerQueryData, BigDecimal> param) {
//                return new ThreadsTableCell<>();
//            }
//        });
//
//        TableColumn timeColumn = new TableColumn("Time");
//        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
//        timeColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, Number>, TableCell<ProfilerQueryData, Number>>() {
//            @Override
//            public TableCell<ProfilerQueryData, Number> call(TableColumn<ProfilerQueryData, Number> param) {
//                return new TimeTableCell();
//            }
//        });
//
//        TableColumn qedtColumn = new TableColumn("QEDT");
//        qedtColumn.setCellValueFactory(new PropertyValueFactory<>("graph1"));
//        qedtColumn.setCellFactory(new Callback<TableColumn<GraphData, String>, TableCell<GraphData, String>>() {
//            @Override
//            public TableCell<GraphData, String> call(TableColumn<GraphData, String> param) {
//                return new GraphQEDTTableCell(DBObjectsProfilingController.this);
//            }
//        });
//
//        TableColumn qetiColumn = new TableColumn("QETI");
//        qetiColumn.setCellValueFactory(new PropertyValueFactory<>("graph2"));
//        qetiColumn.setCellFactory(new Callback<TableColumn<GraphData, String>, TableCell<GraphData, String>>() {
//            @Override
//            public TableCell<GraphData, String> call(TableColumn<GraphData, String> param) {
//                return new GraphQETITableCell(DBObjectsProfilingController.this);
//            }
//        });
//
//        tableView.addTableColumn(userColumn, 0.07);
//        tableView.addTableColumn(hostColumn, 0.07);
//        tableView.addTableColumn(queryTemplateColumn, withAnalyzer ? 0.41 : 0.43);
//        tableView.addTableColumn(explainColumn, withAnalyzer ? 0.09 : 0.10);
//        tableView.addTableColumn(threadsColumn, withAnalyzer ? 0.09 : 0.10);
//        tableView.addTableColumn(timeColumn, 0.09);
//        tableView.addTableColumn(qedtColumn, 0.04);
//        tableView.addTableColumn(qetiColumn, 0.04);
//
//        // bind column size
//        timeColumn.setSortType(TableColumn.SortType.DESCENDING);
//        tableView.addColumnToSortOrder(timeColumn);
//
//        if (withAnalyzer) {
//
//            // add for slow and lock queries tables SQL Optimizer button
//            TableColumn analyzeColumn = new TableColumn("Analyze");
//            analyzeColumn.setCellValueFactory(new PropertyValueFactory<>("queryId"));
//            analyzeColumn.setCellFactory(new Callback<TableColumn<ProfilerQueryData, Integer>, TableCell<ProfilerQueryData, Integer>>() {
//                @Override
//                public TableCell<ProfilerQueryData, Integer> call(TableColumn<ProfilerQueryData, Integer> param) {
//                    return new AnalyzeButtonCell("", analyzerImage);
//                }
//            });
//
//            tableView.addTableColumn(analyzeColumn, 0.05);
//        }
//    }
//
//    private class StringLimitTableCell extends TableCell<ProfilerQueryData, String> {
//
//        private final int limit;
//
//        public StringLimitTableCell(int limit) {
//            this.limit = limit;
//        }
//
//        @Override
//        protected void updateItem(String item, boolean empty) {
//            super.updateItem(item, empty);
//            if (item != null && item.length() >= limit) {
//                setTooltip(new Tooltip(item));
//                item = item.substring(0, limit) + "..";
//            }
//            this.setText(item);
//        }
//    }
//
//    private class TimeTableCell extends TableCell<ProfilerQueryData, Number> {
//
//        @Override
//        protected void updateItem(Number item, boolean empty) {
//            super.updateItem(item, empty);
//            this.setText(item != null ? item.toString() : "");
//            this.setAlignment(Pos.CENTER_RIGHT);
//        }
//    }
//
//    private void showExplainPopup(ProfilerQueryData p, Integer item, Node node, TableCell cell) {
//
//        String query = dbProfilerService.getQueryById(item);
//        String explainQuery = QueryFormat.convertToExplainQuery("EXPLAIN", query);
//
//        if (explainQuery != null) {
//            Long rowsSum = 0l;
//            List<ExplainResult> result = new ArrayList<>();
//            try (ResultSet rs = connection.prepareStatement(explainQuery).executeQuery()) {
//                while (rs.next()) {
//
//                    Long rows = rs.getLong(10);
//                    result.add(new ExplainResult(
//                            rs.getLong(1),
//                            rs.getString(2),
//                            rs.getString(3),
//                            rs.getString(4),
//                            rs.getString(5),
//                            rs.getString(6),
//                            rs.getString(7),
//                            rs.getLong(8),
//                            rs.getString(9),
//                            rows,
//                            rs.getLong(11),
//                            rs.getString(12))
//                    );
//
//                    rowsSum += rows;
//                }
//            } catch (Throwable th) {
//                showError("Error", "Error in explain", th);
//            }
//
//            TableView<ExplainResult> table = new TableView<>();
//            table.setFocusTraversable(false);
//            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
//
//            TableColumn idColumn = new TableColumn("Id");
//            idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
//
//            TableColumn selectTypeColumn = new TableColumn("SelectType");
//            selectTypeColumn.setCellValueFactory(new PropertyValueFactory<>("selectType"));
//
//            TableColumn tableColumn = new TableColumn("Table");
//            tableColumn.setCellValueFactory(new PropertyValueFactory<>("table"));
//
//            TableColumn typeColumn = new TableColumn("Type");
//            typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
//
//            TableColumn posibleKyesColumn = new TableColumn("PosibleKyes");
//            posibleKyesColumn.setCellValueFactory(new PropertyValueFactory<>("posibleKyes"));
//
//            TableColumn keyColumn = new TableColumn("Key");
//            keyColumn.setCellValueFactory(new PropertyValueFactory<>("key"));
//
//            TableColumn keyLenColumn = new TableColumn("KeyLen");
//            keyLenColumn.setCellValueFactory(new PropertyValueFactory<>("keyLen"));
//
//            TableColumn refColumn = new TableColumn("Ref");
//            refColumn.setCellValueFactory(new PropertyValueFactory<>("ref"));
//
//            TableColumn rowsColumn = new TableColumn("Rows");
//            rowsColumn.setCellValueFactory(new PropertyValueFactory<>("rows"));
//
//            TableColumn extraColumn = new TableColumn("Extra");
//            extraColumn.setCellValueFactory(new PropertyValueFactory<>("extra"));
//
//            table.getColumns().addAll(idColumn, selectTypeColumn, tableColumn, typeColumn, posibleKyesColumn, keyColumn, keyLenColumn, refColumn, rowsColumn, extraColumn);
//
//            table.getItems().addAll(result);
//            table.getSelectionModel().clearSelection();
//
//            if (node instanceof ExplainTableCell) {
//                cell.setGraphic(createRatingNode(p, rowsSum, item, cell));
//                p.setRowsSum(rowsSum);
//            }
//
//            showPopup(table, node);
//
//        } else {
//            showPopup(new Label("Can't call EXPLAIN query"), node);
//        }
//    }
//
//    private Node createRatingNode(ProfilerQueryData p, Long rowsSum, Integer item, TableCell cell) {
//        HBox box = new HBox();
//        box.setCursor(Cursor.HAND);
//        box.setOnMouseClicked((MouseEvent event) -> {
//            showExplainPopup(p, item, box, cell);
//        });
//
//        if (rowsSum < 10) {
//            box.getChildren().add(new ImageView(ratingGreenImage));
//        } else if (rowsSum < 100) {
//            box.getChildren().add(new ImageView(ratingBlueImage));
//        } else if (rowsSum < 1000) {
//            box.getChildren().add(new ImageView(ratingYellowImage));
//        } else if (rowsSum < 10000) {
//            box.getChildren().add(new ImageView(ratingRedImage));
//        } else if (rowsSum < 20000) {
//            box.getChildren().addAll(new ImageView(ratingRedImage), new ImageView(ratingRedImage));
//        } else if (rowsSum < 30000) {
//            box.getChildren().addAll(new ImageView(ratingRedImage), new ImageView(ratingRedImage), new ImageView(ratingRedImage));
//        } else if (rowsSum < 50000) {
//            box.getChildren().addAll(new ImageView(ratingRedImage), new ImageView(ratingRedImage), new ImageView(ratingRedImage), new ImageView(ratingRedImage));
//        } else {
//            box.getChildren().addAll(new ImageView(ratingRedImage), new ImageView(ratingRedImage), new ImageView(ratingRedImage), new ImageView(ratingRedImage), new ImageView(ratingRedImage));
//        }
//
//        return box;
//    }
//
//    private class ExplainTableCell extends TableCell<ProfilerQueryData, Integer> {
//
//        @Override
//        protected void updateItem(Integer item, boolean empty) {
//            super.updateItem(item, empty);
//
//            ProfilerQueryData p = (ProfilerQueryData) getTableRow().getItem();
//
//            if (!empty && p != null) {
//
//                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//
//                if (p.getRowsSum() != null) {
//                    setGraphic(createRatingNode(p, p.getRowsSum(), item, this));
//                } else {
//                    Hyperlink link = new Hyperlink("EXPLAIN");
//                    link.setFocusTraversable(false);
//                    link.setOnAction((ActionEvent event) -> {
//                        showExplainPopup(p, item, ExplainTableCell.this, ExplainTableCell.this);
//                    });
//
//                    setGraphic(link);
//                }
//            } else {
//                setContentDisplay(ContentDisplay.TEXT_ONLY);
//            }
//        }
//    }
//
//    private void showPopup(Node content, Node node) {
//
//        HBox box = new HBox(content);
//        HBox.setHgrow(node, Priority.ALWAYS);
//        box.setMaxSize(700, 150);
//        box.getStyleClass().add("profilerPopup");
//
//        Popup popup = new Popup();
//        popup.setAutoHide(true);
//        popup.getContent().addAll(box);
//
//        Point2D windowCoord = new Point2D(node.getScene().getWindow().getX(), node.getScene().getWindow().getY());
//        Point2D sceneCoord = new Point2D(node.getScene().getX(), node.getScene().getY());
//
//        Point2D nodeCoord = node.localToScene(0.0, 0.0);
//
//        double clickX = Math.round(windowCoord.getX() + sceneCoord.getY() + nodeCoord.getX());
//        double clickY = Math.round(windowCoord.getY() + sceneCoord.getY() + nodeCoord.getY());
//
//        popup.setX(clickX);
//        popup.setY(clickY);
//
//        popup.show(controller.getStage());
//    }
//
//    private class AnalyzeButtonCell extends TableCell<ProfilerQueryData, Integer> {
//
//        final Button cellButton;
//
//        AnalyzeButtonCell(String text, Image image) {
//            cellButton = new Button(text, new ImageView(image));
//            cellButton.setOnAction((ActionEvent event) -> {
//                makeBusy(true);
//                Thread th = new Thread(() -> {
//                    try {
//                        ProfilerQueryData pqd = getTableView().getItems().get(getTableRow().getIndex());
//                        String query = dbProfilerService.getQueryById(getItem());
//
//                        Database database = DatabaseCache.getInstance().getDatabase(controller.getSelectedConnectionSession().get().getConnectionParam(), pqd.getDatabase());
//                        if (database == null) {
//                            database = DatabaseCache.getInstance().syncDatabase(pqd.getDatabase(), getConnectionParams(), (AbstractDBService) getDbService());
//                        }
//
//                        if (database != null) {
//
//                            controller.usedQueryAnalyzerFromProfiler();
//
//                            QACache.QARecommendationCache qaRecommendations = QACache.getQARecommandation(controller, DBObjectsProfilingController.this, controller.getSelectedConnectionSession().get(), database, query, !controller.getSmartData().isUseCacheForQA());
//
//                            TableView<AnalyzerResult.RecommendationResult> analyzerResultTable = createTable();
//                            for (String table: qaRecommendations.getIndexes().keySet()) {
//                                List<QACache.QAIndexRecommendation> indexes = qaRecommendations.getIndexes().get(table);
//                                if (indexes != null) {
//                                    for (QACache.QAIndexRecommendation rec: indexes) {
//                                        analyzerResultTable.getItems().add(AnalyzerResult.createRecomendation(
//                                            null, 
//                                            false, 
//                                            qaRecommendations.getDatabase(), 
//                                            table, 
//                                            rec.getRecommendation(), 
//                                            rec.getIndexName(), 
//                                            rec.getIndexQuery(), 
//                                            "", 
//                                            qaRecommendations.getOptimizedQuery(), 
//                                            "",  
//                                            null));
//                                    }
//                                }
//                            }
//                            showPopup(analyzerResultTable, cellButton);
//                        }
//
//                    } catch (SQLException ex) {
//                        showError("Error", "Error in analyzing", ex);
//                    }
//                });
//                th.setDaemon(true);
//                th.start();
//            });
//            cellButton.setFocusTraversable(false);
//        }
//
//        @Override
//        protected void updateItem(Integer t, boolean empty) {
//            super.updateItem(t, empty);
//            if (!empty) {
//                setGraphic(cellButton);
//                setAlignment(Pos.CENTER);
//            } else {
//                setGraphic(null);
//            }
//        }
//
//        private TableView<AnalyzerResult.RecommendationResult> createTable() {
//            TableView<AnalyzerResult.RecommendationResult> analyzerResultTable = new TableView<>();
//            analyzerResultTable.setPrefSize(700, 150);
//            analyzerResultTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
//
//            TableColumn<AnalyzerResult.RecommendationResult, Number> indexColumn = new TableColumn<>("SNo");
//            indexColumn.setSortable(false);
//            indexColumn.setMaxWidth(50);
//            indexColumn.setMinWidth(50);
//            indexColumn.setCellValueFactory(column -> new ReadOnlyObjectWrapper<>(analyzerResultTable.getItems().indexOf(column.getValue()) + 1));
//
//            //select * from actor where first_name=1
//            TableColumn<AnalyzerResult.RecommendationResult, String> tableNameColumn = new TableColumn<>("Table name");
//            tableNameColumn.setCellValueFactory(new PropertyValueFactory<>("tableName"));
//
//            TableColumn<AnalyzerResult.RecommendationResult, String> recommendationColumn = new TableColumn<>("Recommendation");
//            recommendationColumn.setCellValueFactory(new PropertyValueFactory<>("recommendation"));
//
//            TableColumn<AnalyzerResult.RecommendationResult, String> sqlCommandColumn = new TableColumn<>("SQL command");
//            sqlCommandColumn.setCellValueFactory(new PropertyValueFactory<>("sqlCommand"));
//
//            TableColumn<AnalyzerResult.RecommendationResult, Boolean> executionButtonColumn = new TableColumn<>("Execution button");
//            executionButtonColumn.setCellValueFactory(new PropertyValueFactory<>("executed"));
//            executionButtonColumn.setCellFactory((TableColumn<AnalyzerResult.RecommendationResult, Boolean> p) -> new ExecuteButtonCell());
//            executionButtonColumn.setSortable(false);
//
//            analyzerResultTable.getColumns().addAll(indexColumn, tableNameColumn, recommendationColumn, sqlCommandColumn, executionButtonColumn);
//
//            return analyzerResultTable;
//        }
//
//        private void prepareAnalyzerRows(TableView<AnalyzerResult.RecommendationResult> analyzerResultTable, List<AnalyzerResult> list) {
//
//            for (AnalyzerResult t : list) {
//                if (t.getRecomendationsSize() > 0) {
//                    for (AnalyzerResult.RecommendationResult rr : t.getRecomendations()) {
//                        analyzerResultTable.getItems().add(rr);
//                    }
//                }
//
//                if (t.getChildren() != null) {
//                    prepareAnalyzerRows(analyzerResultTable, t.getChildren());
//                }
//            }
//        }
//    }
//
//    private class ExecuteButtonCell extends TableCell<AnalyzerResult.RecommendationResult, Boolean> {
//
//        final Button cellButton = new Button("Execute");
//
//        ExecuteButtonCell() {
//            cellButton.setOnAction((ActionEvent t) -> {
//                AnalyzerResult.RecommendationResult rr = (AnalyzerResult.RecommendationResult) getTableRow().getItem();
//                if (rr != null) {
//                    rr.setExecuted(Boolean.TRUE);
//                    cellButton.setDisable(true);
//
//                    mainUI.setDisable(true);
//                    Thread thread = new Thread(() -> {
//                        try {
//                            getDbService().execute(rr.getSqlCommand());
//                        } catch (SQLException ex) {
//                            showError("Error", "Error in executing recommendation command", ex);
//                        }
//                        Platform.runLater(() -> mainUI.setDisable(false));
//                    });
//                    thread.setDaemon(true);
//                    thread.start();
//                }
//            });
//
//            cellButton.setGraphic(new ImageView(executeImage));
//            cellButton.setFocusTraversable(false);
//        }
//
//        @Override
//        protected void updateItem(Boolean t, boolean empty) {
//            super.updateItem(t, empty);
//            if (!empty) {
//                cellButton.setDisable(t != null ? t : false);
//                setGraphic(cellButton);
//                setAlignment(Pos.CENTER);
//            } else {
//                setGraphic(null);
//            }
//        }
//    }
//
//    private void createAndInitThread() {
//        createThread();
//        // THIS NOT WORK WITH THIS BACKGROUND THREADS
//        // SOME TIMES IT JUST IGNORING WORKPROC, 
//        // ALSO WE HAVE ONLY 3 THREAD - THIS IS NOT ENOUGH
//        //Recycler.getLessBusySWP().addWorkProc(allBackgroundTask);
//        
//        Thread th = new Thread(() -> {
//            allBackgroundTask.run();
//        }, "DBObjectsProfiling");
//        th.setDaemon(true);
//        th.start();
//    }
//
//    private void createThread() {
//
//        // generate server hash code
//        final String serverHashCode = serverHashCode(getConnectionParams(), "");
//
//        /**
//         * Using multiple threads causes overhead, so i use one thread with lots
//         * of checks
//         */
//        if (allBackgroundTask == null) {
//            allBackgroundTask = new WorkProc() {
//
//                long lastOverallCodeExecutionRun = 0l;
//
//                @Override
//                public void updateUI() {
//                }
//
//                @Override
//                public void run() {
//                    System.out.println("ALL BACKGROUND TASK WORK PROC ACTIVE");
//
//                    dbProfilerService.clearGlobalStatusStartTable(serverHashCode);
//
//                    Map<String, ProfilerQueryData> mapTopQueries = new HashMap<>();
//                    
//                    while (keepThreadsAlive && profilerStarted.get()) {
//                        
//                        /**
//                         * *****************************************************
//                         * QUERIES DATA COLLECTION AND TABLEVIEW DATA runs
//                         * every 1 seconds
//                         * ******************************************************
//                         */
//                        if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastOverallCodeExecutionRun) < 1) {
//                            try {
//                                Thread.sleep(250);
//                            } catch (InterruptedException ex) {
//                            }
//                            continue;
//                        }
//                        
//                        ChartScale currentScale = ChartScale.values()[parentController.currentScaleIndex().get()];
//                        if (topQueriesTable.isActive()) {
//                            collectQueriesData(mapTopQueries, topQueriesTable.getContentNodeAsType(), dbProfilerService.getProfilerTopData(QueryProvider.QUERY__LOAD_TOP_QUERIES, currentScale.getValue()));
//                        }
//                        if (topTablesTable.isActive()) {
//                            collectTopData(topTablesTable.getContentNodeAsType(), dbProfilerService.getProfilerTopData(QueryProvider.QUERY__LOAD_TOP_TABLES, currentScale.getValue()));
//                        }
//                        if (topSchemasTable.isActive()) {
//                            collectTopData(topSchemasTable.getContentNodeAsType(), dbProfilerService.getProfilerTopData(QueryProvider.QUERY__LOAD_TOP_SCHEMAS, currentScale.getValue()));
//                        }
//                        if (topUsersTable.isActive()) {
//                            collectTopData(topUsersTable.getContentNodeAsType(), dbProfilerService.getProfilerTopData(QueryProvider.QUERY__LOAD_TOP_USERS, currentScale.getValue()));
//                        }
//                        if (topHostsTable.isActive()) {
//                            collectTopData(topHostsTable.getContentNodeAsType(), dbProfilerService.getProfilerTopData(QueryProvider.QUERY__LOAD_TOP_HOSTS, currentScale.getValue()));
//                        }
//
//                        /**
//                         * EDNING
//                         */
//                        lastOverallCodeExecutionRun = System.currentTimeMillis();
//                    }
//                }
//            };
//        }
//    }
//
//
//    public void exportToXls(
//            List<String> sheets, 
//            List<ObservableList<ObservableList>> totalData,
//            List<LinkedHashMap<String, Boolean>> totalColumns,
//            List<List<Integer>> totalColumnWidths,
//            List<byte[]> images,
//            List<Integer> heights,
//            List<Integer> colspans) {
//        
//        {
//            ObservableList rowsData = FXCollections.observableArrayList();
//            for (ProfilerQueryData sq : (List<ProfilerQueryData>) (List) topQueriesTable.getContentNodeAsType().getCopyItems()) {
//                ObservableList row = FXCollections.observableArrayList();
//
//                row.add(sq.getUser());
//                row.add(sq.getHost());
//                row.add(sq.getQueryTemplate());
//                row.add(sq.getThreads());
//                row.add(sq.getTime());
//
//                rowsData.add(row);
//            }
//
//            List<Integer> columnWidths = new ArrayList<>();
//            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
//
//            columns.put("User", true);
//            columnWidths.add((int) topQueriesTable.getContentNodeAsType().getColumnByName("User").getWidth());
//
//            columns.put("Host", true);
//            columnWidths.add((int) topQueriesTable.getContentNodeAsType().getColumnByName("Host").getWidth());
//
//            columns.put("Query", true);
//            columnWidths.add((int) topQueriesTable.getContentNodeAsType().getColumnByName("Query").getWidth());
//
//            columns.put("Threads", true);
//            columnWidths.add((int) topQueriesTable.getContentNodeAsType().getColumnByName("Threads").getWidth());
//
//            columns.put("Time", true);
//            columnWidths.add((int) topQueriesTable.getContentNodeAsType().getColumnByName("Time").getWidth());
//
//            sheets.add("Top Queries");
//            totalData.add(rowsData);
//            totalColumns.add(columns);
//            totalColumnWidths.add(columnWidths);
//            images.add(null);
//            heights.add(null);
//            colspans.add(null);
//        }
//
//        {
//            ObservableList rowsData = FXCollections.observableArrayList();
//            int i = 1;
//            for (ProfilerTopData sq : (List<ProfilerTopData>) (List) topTablesTable.getContentNodeAsType().getCopyItems()) {
//                ObservableList row = FXCollections.observableArrayList();
//
//                row.add(i);
//                row.add(sq.getSource());
//                row.add(sq.getThreads());
//
//                rowsData.add(row);
//            }
//
//            List<Integer> columnWidths = new ArrayList<>();
//            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
//
//            columns.put("#", true);
//            columnWidths.add((int) topTablesTable.getContentNodeAsType().getColumnByName("#").getWidth());
//
//            columns.put("Table", true);
//            columnWidths.add((int) topTablesTable.getContentNodeAsType().getColumnByName("Table").getWidth());
//
//            columns.put("Threads", true);
//            columnWidths.add((int) topTablesTable.getContentNodeAsType().getColumnByName("Threads").getWidth());
//
//            sheets.add("Top Tables");
//            totalData.add(rowsData);
//            totalColumns.add(columns);
//            totalColumnWidths.add(columnWidths);
//            images.add(topTablesTable.getContentNodeAsType().getChartImage());
//            heights.add(22);
//            colspans.add(3);
//        }
//
//        {
//            ObservableList rowsData = FXCollections.observableArrayList();
//            int i = 1;
//            for (ProfilerTopData sq : (List<ProfilerTopData>) (List) topHostsTable.getContentNodeAsType().getCopyItems()) {
//                ObservableList row = FXCollections.observableArrayList();
//
//                row.add(i);
//                row.add(sq.getSource());
//                row.add(sq.getThreads());
//
//                rowsData.add(row);
//            }
//
//            List<Integer> columnWidths = new ArrayList<>();
//            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
//
//            columns.put("#", true);
//            columnWidths.add((int) topHostsTable.getContentNodeAsType().getColumnByName("#").getWidth());
//
//            columns.put("Host", true);
//            columnWidths.add((int) topHostsTable.getContentNodeAsType().getColumnByName("Host").getWidth());
//
//            columns.put("Threads", true);
//            columnWidths.add((int) topHostsTable.getContentNodeAsType().getColumnByName("Threads").getWidth());
//
//            sheets.add("Top Hosts");
//            totalData.add(rowsData);
//            totalColumns.add(columns);
//            totalColumnWidths.add(columnWidths);
//            images.add(topHostsTable.getContentNodeAsType().getChartImage());
//            heights.add(22);
//            colspans.add(3);
//        }
//
//        {
//            ObservableList rowsData = FXCollections.observableArrayList();
//            int i = 1;
//            for (ProfilerTopData sq : (List<ProfilerTopData>) (List) topUsersTable.getContentNodeAsType().getCopyItems()) {
//                ObservableList row = FXCollections.observableArrayList();
//
//                row.add(i);
//                row.add(sq.getSource());
//                row.add(sq.getThreads());
//
//                rowsData.add(row);
//            }
//
//            List<Integer> columnWidths = new ArrayList<>();
//            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
//
//            columns.put("#", true);
//            columnWidths.add((int) topUsersTable.getContentNodeAsType().getColumnByName("#").getWidth());
//
//            columns.put("User", true);
//            columnWidths.add((int) topUsersTable.getContentNodeAsType().getColumnByName("User").getWidth());
//
//            columns.put("Threads", true);
//            columnWidths.add((int) topUsersTable.getContentNodeAsType().getColumnByName("Threads").getWidth());
//
//            sheets.add("Top Users");
//            totalData.add(rowsData);
//            totalColumns.add(columns);
//            totalColumnWidths.add(columnWidths);
//            images.add(topUsersTable.getContentNodeAsType().getChartImage());
//            heights.add(22);
//            colspans.add(3);
//        }
//
//        {
//            ObservableList rowsData = FXCollections.observableArrayList();
//            int i = 1;
//            for (ProfilerTopData sq : (List<ProfilerTopData>) (List) topSchemasTable.getContentNodeAsType().getCopyItems()) {
//                ObservableList row = FXCollections.observableArrayList();
//
//                row.add(i);
//                row.add(sq.getSource());
//                row.add(sq.getThreads());
//
//                rowsData.add(row);
//            }
//
//            List<Integer> columnWidths = new ArrayList<>();
//            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
//
//            columns.put("#", true);
//            columnWidths.add((int) topSchemasTable.getContentNodeAsType().getColumnByName("#").getWidth());
//
//            columns.put("Schema", true);
//            columnWidths.add((int) topSchemasTable.getContentNodeAsType().getColumnByName("Schema").getWidth());
//
//            columns.put("Threads", true);
//            columnWidths.add((int) topSchemasTable.getContentNodeAsType().getColumnByName("Threads").getWidth());
//
//            sheets.add("Top Schemas");
//            totalData.add(rowsData);
//            totalColumns.add(columns);
//            totalColumnWidths.add(columnWidths);
//            images.add(topSchemasTable.getContentNodeAsType().getChartImage());
//            heights.add(22);
//            colspans.add(3);
//        }
//    }
//
//
//    @Override
//    public AnchorPane getUI() {
//        return mainUI;
//    }
//    
//    public void setParentController(ProfilerTabContentController parentController) {
//        this.parentController = parentController;
//        zoomMessageLabel.textProperty().bind(parentController.zoomCountStage());
//    }
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//
//        createAllEntities();
//        
//        zoomOutLabel.setOnMouseClicked((MouseEvent event) -> {
//            parentController.zoomOutAction();
//        });
//        
//        zoomInLabel.setOnMouseClicked((MouseEvent event) -> {
//            parentController.zoomInAction();
//        });
//                
//
//        initQueryDataTable(topQueriesTable.getContentNodeAsType(), false);
//
//        topTablesTable.getContentNodeAsType().init("Top Tables", "Table", "source", "Threads", "threads");
//        topSchemasTable.getContentNodeAsType().init("Top Schemas", "Schema", "source", "Threads", "threads");
//        topUsersTable.getContentNodeAsType().init("Top Users", "User", "source", "Threads", "threads");
//        topHostsTable.getContentNodeAsType().init("Top Hosts", "Host", "source", "Threads", "threads");
//    }
//
//    private FilterableTableViewController loadFilterableTableEntityMetricControllers(String s) {
//        FilterableTableViewController ftvc = loadEntityMetricControllers(FilterableTableViewController.class);
//        ftvc.setRepresentableNameString(s);
//        return ftvc;
//    }
//
//    private ChartTableViewController loadChartTableViewEntityMetricControllers() {
//        return loadEntityMetricControllers(ChartTableViewController.class);
//    }
//
//
//    private <T extends MetricEntityContainerController> T loadEntityMetricControllers(Class<T> meccClass) {
//        try {
//            FXMLLoader loader = new FXMLLoader(MainUI.class.getResource("MetricEntityContainer.fxml").toURI().toURL());
//            loader.setController(meccClass.newInstance());
//            loader.load();
//            T t = loader.getController();
//            t.activateMetric();
//            return t;
//        } catch (Exception ex) {
//            log.error("Error", ex);
//        }
//        return null;
//    }
//
//    private void createAllEntities() {
//
//        topQueriesTable = loadFilterableTableEntityMetricControllers("top Queries Table");
//        topTablesTable = loadChartTableViewEntityMetricControllers();
//        topSchemasTable = loadChartTableViewEntityMetricControllers();
//        topUsersTable = loadChartTableViewEntityMetricControllers();
//        topHostsTable = loadChartTableViewEntityMetricControllers();
//
//        topQueriesTable.addFilters();
//
//        if (getUI().getScene() == null) {
//            getUI().sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
//                if (newValue != null && oldValue == null) {
//                    applyNewUiModifications();
//                }
//            });
//        } else {
//            applyNewUiModifications();
//        }
//    }
//
//    private void applyNewUiModifications() {
//        dbObjectsScrollContainer.getStyleClass().add(Flags.METRIC_STYLE_CLASS);
//        
//        Utitlities.prepFlowPaneDimensionConditionWithParent(dbObjectsFlowContainer, 2.0, true);
//        dbObjectsFlowContainer.getChildren().addAll(topSchemasTable.getUI(), topTablesTable.getUI(), topHostsTable.getUI(), topUsersTable.getUI());
//    }
//
//    @Override
//    public void finalizeConnectionCreation() {
//        getUI().requestLayout();
//    }
//    
}
