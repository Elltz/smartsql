package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.input.KeyCode;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.StageStyle;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.structure.dialogs.CreateTriggerDialogController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import np.com.ngopal.smart.sql.ui.controller.BackupDatabaseAsSQLDumpController;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.DatabaseController;
import np.com.ngopal.smart.sql.ui.controller.DatabaseCopierController;
import np.com.ngopal.smart.sql.ui.controller.EmptyDatabaseController;
import np.com.ngopal.smart.sql.ui.controller.ExecuteSqlScriptDialogController;
import np.com.ngopal.smart.sql.ui.controller.ImportTableController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.controller.ScheduledBackupWizardController;
import np.com.ngopal.smart.sql.ui.controller.SchemaCreationController;
import np.com.ngopal.smart.sql.ui.controller.SelectSchemaController;
import np.com.ngopal.smart.sql.ui.controller.StoredProcedureWithCursorController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;


/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInDatabaseItemModule extends AbstractStandardModule {
    
    
    private Image viewImage;
    private Image procedureImage;
    private Image functionImage;
    private Image triggerImage;
    private Image eventImage;
    
    private Image tableImage;
        
    private static final BooleanProperty disabledProperty = new SimpleBooleanProperty(false);
        
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public int getOrder() {
        return 4;
    }
    
    MenuItem createDatabaseItem, alterDatabaseItem, tableItem, viewItem, storedProcedureItem, storedProcedureWithCursorItem,
            functionItem, triggerItem, auditTriggerItem, eventItem, dropDatabaseItem, truncateDatabaseItem, emptyDatabaseItem,
            scheduledBackupsItem, exportDatabaseItem, backupDatabaseAsSQLDumpItem, importExternalDataItem, executeSQLScriptItem,
            copyTableToDifferentItem, createSchemaForDbInHtml, backupDatabaseAsSQLDumpItemForToolbar, executeSQLScriptItemForToolbar, copyTableToDifferentItemForToolbar;
    
    Menu createMenu,moreDatabaseOperationsMenu,importMenu,backupOrExportMenu;

    @Override
    public boolean install() {
        super.install();        
        registrateServiceChangeListener();        
        
        copyTableToDifferentItem = getCopyTableToDifferentItem(null);        
        
        createDatabaseItem = getCreateDatabaseItem(null);
        
        alterDatabaseItem = getAlterDatabaseItem(null);

        createMenu = new Menu("Create",
        new ImageView(SmartImageProvider.getInstance().getCreate_image()));
        tableItem = getTableItem(null);
        viewItem = getViewItem(null);
        
        storedProcedureItem = getStoredProcedureItem(null);
        storedProcedureWithCursorItem = getStoredProcedureWithCursorItem(null);
        
        functionItem = getFunctionItem(null);
        
        triggerItem = getTriggerItem(null);
        auditTriggerItem = getAuditTriggerItem(null);
        
        eventItem = getEventItem(null);
        createMenu.getItems().addAll(tableItem, viewItem, storedProcedureItem, storedProcedureWithCursorItem, functionItem, triggerItem, auditTriggerItem, eventItem);

        moreDatabaseOperationsMenu = new Menu("More Operations",
            new ImageView(SmartImageProvider.getInstance().getMore_image()));
        dropDatabaseItem = getDropDatabaseItem(null);
        
        truncateDatabaseItem = getTruncateDatabaseItem(null);
        
        emptyDatabaseItem = getEmptyDatabaseItem(null);
        
        moreDatabaseOperationsMenu.getItems().addAll(dropDatabaseItem, truncateDatabaseItem, emptyDatabaseItem);
        

        backupOrExportMenu = new Menu("Backup/Export",
        new ImageView(SmartImageProvider.getInstance().getBackUpExport_image()));
        
        scheduledBackupsItem = getScheduledBackupsItem(null);
        backupDatabaseAsSQLDumpItem = getBackupDatabaseAsSQLDumpItem(null);
        exportDatabaseItem = getExportDatabase(null);

        backupOrExportMenu.getItems().addAll(exportDatabaseItem, scheduledBackupsItem, backupDatabaseAsSQLDumpItem);

        importMenu = new Menu("Import",
        new ImageView(SmartImageProvider.getInstance().getImport_image()));
        importExternalDataItem = getImportExternalDataItem(null);
        
        executeSQLScriptItem = getExecuteSQLScriptItem(null);
       
        
        importMenu.getItems().addAll(importExternalDataItem, executeSQLScriptItem);
        createSchemaForDbInHtml = getCreateSchemaForDbInHtml(null);
        createSchemaForDbInHtml.setText("Create Schema");
        createSchemaForDbInHtml.setUserData("Create Schema For Database In HTML");
        
        
        backupDatabaseAsSQLDumpItemForToolbar = getBackupDatabaseAsSQLDumpItem(null);
        executeSQLScriptItemForToolbar = getExecuteSQLScriptItem(null);
        copyTableToDifferentItemForToolbar = getCopyTableToDifferentItem(null);
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.DATABASE, new MenuItem[]{copyTableToDifferentItem, createDatabaseItem, alterDatabaseItem, createMenu, moreDatabaseOperationsMenu});
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.DATABASE, new MenuItem[]{createSchemaForDbInHtml});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.DATABASE, new MenuItem[]{importMenu, backupOrExportMenu});
        
        
//        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{backupDatabaseAsSQLDumpItemForToolbar, executeSQLScriptItemForToolbar, copyTableToDifferentItemForToolbar});
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{executeSQLScriptItem});
        
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        
        Object obj = item.getValue();
        disabledProperty.set(!(obj instanceof Database));
        
        alterDatabaseItem.disableProperty().bind(disabledProperty);
        tableItem.disableProperty().bind(disabledProperty);
        viewItem.disableProperty().bind(disabledProperty);
        storedProcedureItem.disableProperty().bind(disabledProperty);
        storedProcedureWithCursorItem.disableProperty().bind(disabledProperty);
        functionItem.disableProperty().bind(disabledProperty);
        triggerItem.disableProperty().bind(disabledProperty);
        auditTriggerItem.disableProperty().bind(disabledProperty);
        eventItem.disableProperty().bind(disabledProperty);
        dropDatabaseItem.disableProperty().bind(disabledProperty);
        truncateDatabaseItem.disableProperty().bind(disabledProperty);
        emptyDatabaseItem.disableProperty().bind(disabledProperty);
        scheduledBackupsItem.disableProperty().bind(disabledProperty);
        backupDatabaseAsSQLDumpItem.disableProperty().bind(disabledProperty);
        importExternalDataItem.disableProperty().bind(disabledProperty);
        copyTableToDifferentItem.disableProperty().bind(disabledProperty);
        createSchemaForDbInHtml.disableProperty().bind(disabledProperty);
        
        executeSQLScriptItemForToolbar.disableProperty().bind(disabledProperty.or(busy()));
        backupDatabaseAsSQLDumpItemForToolbar.disableProperty().bind(executeSQLScriptItemForToolbar.disableProperty());
        copyTableToDifferentItemForToolbar.disableProperty().bind(executeSQLScriptItemForToolbar.disableProperty());
        
        executeSQLScriptItem.setDisable(!(obj instanceof Database || obj instanceof DBTable || obj == ConnectionTabContentController.DatabaseChildrenType.TABLES));
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        

        MenuItem copyTableToDifferentItem = getCopyTableToDifferentItem(session);
        
        MenuItem createDatabaseItem = getCreateDatabaseItem(session);
        
        MenuItem alterDatabaseItem = getAlterDatabaseItem(session);


//        MenuItem newDataSearchItem = new MenuItem("New Data Search", 
//            new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/database_search.png",DefaultImageProvider.IMAGE_SIZE_WIDTH,DefaultImageProvider.IMAGE_SIZE_HEIGHT,false,false)));
//        newDataSearchItem.setAccelerator(KeyCombination.keyCombination("Ctrl+Shift+D"));
        Menu createMenu = new Menu("Create", 
            new ImageView(SmartImageProvider.getInstance().getCreate_image()));
        MenuItem tableItem = getTableItem(session);
        MenuItem viewItem = getViewItem(session);
        
        MenuItem storedProcedureItem = getStoredProcedureItem(session);
        MenuItem storedProcedureWithCursorItem = getStoredProcedureWithCursorItem(session);
        
        MenuItem functionItem = getFunctionItem(session);
        
        MenuItem triggerItem = getTriggerItem(session);
        MenuItem auditTriggerItem = getAuditTriggerItem(session);
        
        MenuItem eventItem = getEventItem(session);
        createMenu.getItems().addAll(tableItem, viewItem, storedProcedureItem, storedProcedureWithCursorItem, functionItem, triggerItem, auditTriggerItem, eventItem);

        Menu moreDatabaseOperationsMenu = new Menu("More Database Operations", 
                FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getMore_image()), 18,18));
        
        MenuItem dropDatabaseItem = getDropDatabaseItem(session);
        
        MenuItem truncateDatabaseItem = getTruncateDatabaseItem(session);
        
        MenuItem emptyDatabaseItem = getEmptyDatabaseItem(session);
        
        moreDatabaseOperationsMenu.getItems().addAll(dropDatabaseItem, truncateDatabaseItem, emptyDatabaseItem);
        

        Menu backupOrExportMenu = new Menu("Backup/Export", 
                FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getBackUpExport_image()), 18,18));
        
        MenuItem scheduledBackupsItem = getScheduledBackupsItem(session);
        
        MenuItem exportDatabaseItem = getExportDatabase(session);

        MenuItem backupDatabaseAsSQLDumpItem = getBackupDatabaseAsSQLDumpItem(session);

        backupOrExportMenu.getItems().addAll(exportDatabaseItem, new SeparatorMenuItem(), scheduledBackupsItem, backupDatabaseAsSQLDumpItem);

        Menu importMenu = new Menu("Import", 
                FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getImport_image()), 18,18));
        
        MenuItem importExternalDataItem = getImportExternalDataItem(session);
        
        MenuItem executeSQLScriptItem = getExecuteSQLScriptItem(session);
        
        importMenu.getItems().addAll(importExternalDataItem, executeSQLScriptItem);
        MenuItem createSchemaForDbInHtml = getCreateSchemaForDbInHtml(session);

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("database0", new ArrayList<>(Arrays.asList(new MenuItem[]{copyTableToDifferentItem, createDatabaseItem, alterDatabaseItem, /*newDataSearchItem,*/ createMenu, moreDatabaseOperationsMenu}))), Hooks.QueryBrowser.DATABASE);
        map.put(new HookKey("database1", new ArrayList<>(Arrays.asList(new MenuItem[]{backupOrExportMenu, importMenu}))), Hooks.QueryBrowser.DATABASE);
        map.put(new HookKey("database2", new ArrayList<>(Arrays.asList(new MenuItem[]{createSchemaForDbInHtml}))), Hooks.QueryBrowser.DATABASE);

        return map;
    }
    
    
    /**
     * For re-usability of the menuItems, i adusted the codes. For readability i
     * each menuItem had its own getmethod.
     *
     * @param session
     * connection session for the current session. In toolbar's menu the injected session is
     * null, since there is none at the time. Because of that i did a little modification on each
     * menuItem so as it checks for null sessions. 
     * most of the are in the format [session = != null ? function : function ;]
     */
    
    
    private MenuItem getCreateSchemaForDbInHtml(final ConnectionSession session) {
        MenuItem createSchemaForDbInHtml = new MenuItem("Create Schema For Database In HTML",
                new ImageView(SmartImageProvider.getInstance().getDatabaseHtml_image()));
        createSchemaForDbInHtml.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN,KeyCombination.SHIFT_DOWN,KeyCombination.ALT_DOWN));
        //createSchemaForDbInHtml.disableProperty().bind();
        createSchemaForDbInHtml.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SchemaCreationController scc = ((MainController)getController()).getTypedBaseController("SchemaCreation");
                if (session == null) {
                    scc.insertSession(getController().getSelectedConnectionSession().get());
                } else {
                    scc.insertSession(session);
                }
                scc.showNow(getController().getStage());
            }
        });
        return createSchemaForDbInHtml;
    }

    private MenuItem getExecuteSQLScriptItem(final ConnectionSession session) {
        MenuItem executeSQLScriptItem = new MenuItem("Execute",
                new ImageView(SmartImageProvider.getInstance().getDatabaseExecuteScript_image()));
        executeSQLScriptItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.SHORTCUT_DOWN,KeyCombination.SHIFT_DOWN));
        executeSQLScriptItem.setUserData("Execute SQL Script");
        executeSQLScriptItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (session == null) {
                    showExecuteSqlScriptDialog(getController().getSelectedConnectionSession().get());
                } else {
                    showExecuteSqlScriptDialog(session);
                }
            }
        });

        return executeSQLScriptItem;

    }

    private MenuItem getImportExternalDataItem(final ConnectionSession session) {
        MenuItem importExternalDataItem = new MenuItem("Import External Data...",
                new ImageView(SmartImageProvider.getInstance().getDatabaseExternalImport()));
        importExternalDataItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        importExternalDataItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (session == null) {
                    showImportExternalDataDialog(getController().getSelectedConnectionSession().get());
                } else {
                    showImportExternalDataDialog(session);
                }
                
                event.consume();
            }
        });

        return importExternalDataItem;
    }
    
    private MenuItem getExportDatabase(final ConnectionSession session){
        MenuItem exportDatabase = new MenuItem("Export Database",
                new ImageView(SmartImageProvider.getInstance().getDatabaseExport_image()));
        exportDatabase.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConnectionSession realSession = null;
                if (session == null) {
                    realSession = getController().getSelectedConnectionSession().get();
                } else {
                    realSession = session;
                }
                
                Stage exportDialog = new Stage(StageStyle.UTILITY);
                exportDialog.initOwner(getController().getStage());
                exportDialog.initModality(Modality.WINDOW_MODAL);
                exportDialog.setTitle("     EXPORT DATABASE DIALOG");
                exportDialog.setScene(new Scene(((MainController)getController()).getTypedBaseController("DataExporter").getUI()));
                exportDialog.setResizable(false);
                exportDialog.show();
            }
        });
        
        return exportDatabase;
    }

    private MenuItem getBackupDatabaseAsSQLDumpItem(final ConnectionSession session) {
        MenuItem backupDatabaseAsSQLDumpItem = new MenuItem("Backup Database As SQL Dump",
                new ImageView(SmartImageProvider.getInstance().getDatabaseDump_image()));
        backupDatabaseAsSQLDumpItem.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        backupDatabaseAsSQLDumpItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (session == null) {
                    showBackupDatabaseAsSQLDumpDialog(getController().getSelectedConnectionSession().get());
                } else {
                    showBackupDatabaseAsSQLDumpDialog(session);
                }
            }
        });

        return backupDatabaseAsSQLDumpItem;
    }

    private MenuItem getScheduledBackupsItem(final ConnectionSession session) {
        MenuItem scheduledBackupsItem = new MenuItem("Scheduled Backups...",
                new ImageView(SmartImageProvider.getInstance().getScheduleBackUp_image()));
        scheduledBackupsItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        //scheduledBackupsItem.setAccelerator(KeyCombination.keyCombination("Ctrl+Alt+E"));
        scheduledBackupsItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (session == null) {
                    showScheduledBackupsDialog(getController().getSelectedConnectionSession().get());
                } else {
                    showScheduledBackupsDialog(session);
                }
            }
        });

        return scheduledBackupsItem;
    }

    private MenuItem getEmptyDatabaseItem(final ConnectionSession session) {
        MenuItem emptyDatabaseItem = new MenuItem("Empty Database...", 
                new ImageView(SmartImageProvider.getInstance().getDatabaseEmpty_image()));
        emptyDatabaseItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                final ConnectionSession session1 = session == null ? getController().getSelectedConnectionSession().get() : session;

                ReadOnlyObjectProperty<TreeItem> selectedDataBase = getController().getSelectedTreeItem(session1);
                if (selectedDataBase != null) {
                    final Database database = (Database) ((TreeItem) selectedDataBase.get()).getValue();                    
                    EmptyDatabaseController controller = ((MainController)getController()).getTypedBaseController("EmptyDatabase");
                    controller.init(getController().getStage(), session1, database, selectedDataBase.getValue());
                }                
            }
        });

        return emptyDatabaseItem;
    }

    private MenuItem getTruncateDatabaseItem(final ConnectionSession session) {

        MenuItem truncateDatabaseItem = new MenuItem("Truncate Database",
                new ImageView(SmartImageProvider.getInstance().getDatabaseTruncate_image()));
        truncateDatabaseItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.SHIFT_DOWN));
        truncateDatabaseItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                truncateDatabase();
            }
        });

        return truncateDatabaseItem;
    }
    
    public void truncateDatabase() {
        ConnectionSession currentSession = getController().getSelectedConnectionSession().get();

        ReadOnlyObjectProperty<TreeItem> selectedDataBase = getController().getSelectedTreeItem(currentSession);
        if (selectedDataBase != null) {
            Database database = (Database) ((TreeItem) selectedDataBase.get()).getValue();

            DialogResponce responce = showConfirm("Truncate Database...", 
                "Do you really want to truncate the database '" + database.getName() + "'?");

            if (responce == DialogResponce.OK_YES) {

                try {
                    // drop db
                    currentSession.getService().dropDatabase(database);
                    currentSession.getConnectionParam().getDatabases().remove(database);

                    // create db
                    currentSession.getService().createDatabase(database.getName(), null, null);

                    // and refresh tree
                    getController().refreshTree(currentSession, false); 
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        }
    }

    private MenuItem getDropDatabaseItem(final ConnectionSession session) {
        MenuItem dropDatabaseItem = new MenuItem("Drop Database...", 
                new ImageView(SmartImageProvider.getInstance().getDatabaseDrop_image()));
        dropDatabaseItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropDatabaseItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dropDatabase();
            }
        });

        return dropDatabaseItem;
    }

    public void dropDatabase() {
        ConnectionSession session1 = getController().getSelectedConnectionSession().get();

        ReadOnlyObjectProperty<TreeItem> selectedDataBase = getController().getSelectedTreeItem(session1);
        if (selectedDataBase != null) {
            Database database = (Database) ((TreeItem) selectedDataBase.get()).getValue();

            DialogResponce responce = showConfirm("Drop Database...", 
                "Do you really want to drop the database '" + database.getName() + "'?" );

            if (responce == DialogResponce.OK_YES) {
                try {
                    // drop db
                    session1.getService().dropDatabase(database);
                    session1.getConnectionParam().getDatabases().remove(database);

                    LeftTreeHelper.databaseDroped(session1.getConnectionParam(), selectedDataBase.get());
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        }
    }
    
    private MenuItem getEventItem(final ConnectionSession session) {
        MenuItem eventItem = new MenuItem("Event...", 
                new ImageView(SmartImageProvider.getInstance().getCreateEvent_image()));
        eventItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                ConnectionSession session1 = session;
                if (session1 == null) {
                    session1 = getController().getSelectedConnectionSession().get();
                }

                QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
                if (queryTabModule != null) {
                    Database db = session1.getConnectionParam().getSelectedDatabase();

                    if (eventImage == null) {
                        eventImage = SmartImageProvider.getInstance().getEventTab_image();
                    }
                    final ConnectionSession session2 = session1;
                    queryTabModule.addTemplatedQueryTab(
                            session1,
                            "Create event",
                            "Enter new event name:",
                            new java.util.function.Function<String, String>() {
                                @Override
                                public String apply(String t) {
                                    return session2.getService().getCreateEventTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                                }
                            },QueryTabModule.TAB_USER_DATA__EVENT_TAB );
                }
            }
        });

        return eventItem;
    }

    private MenuItem getTriggerItem(final ConnectionSession session) {
        MenuItem triggerItem = new MenuItem("Trigger...", 
                new ImageView(SmartImageProvider.getInstance().getCreateTrigger_image()));
        triggerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
                if (queryTabModule != null) {
                    
                    ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                    Database db = sessionTemp.getConnectionParam().getSelectedDatabase();
                    
                    String dbName = db != null ? db.getName() : "<db-name>";

                    if (triggerImage == null) {
                        triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
                    }

                    queryTabModule.addTemplatedQueryTab(sessionTemp, (Void t) -> {                    
                        CreateTriggerDialogController contr = StandardBaseControllerProvider.getController("CreateTrigger");
                        contr.init(sessionTemp.getConnectionParam(), db, null, false);
                        
                        DialogHelper.show("Create Trigger", contr.getUI(), getController().getStage());

                        if (contr.getResponce() == DialogResponce.OK_YES) {

                            String content = sessionTemp.getService().getCreateTriggerTemplateSQL(
                                dbName,
                                contr.getTriggerNameValue(),
                                contr.getBeforeAfterValue(),
                                contr.getTriggerEventValue(),
                                contr.getTriggerTableValue(),
                                contr.getComment()
                            );

                            return new Pair<>(contr.getTriggerNameValue(), content);                        
                        }                    

                        return null;


                    }, QueryTabModule.TAB_USER_DATA__TRIGGER_TAB );
                }
            }
        });

        return triggerItem;
    }

    private MenuItem getAuditTriggerItem(final ConnectionSession session) {
        MenuItem triggerItem = new MenuItem("Audit Log Trigger...", 
                new ImageView(SmartImageProvider.getInstance().getCreateTrigger_image()));
        triggerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
                if (queryTabModule != null) {
                    
                    ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                    Database db = sessionTemp.getConnectionParam().getSelectedDatabase();
                    
                    String dbName = db != null ? db.getName() : "<db-name>";

                    if (triggerImage == null) {
                        triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
                    }

                    queryTabModule.addTemplatedQueryTab(sessionTemp, (Void t) -> {                    
                        CreateTriggerDialogController contr = StandardBaseControllerProvider.getController("CreateTrigger");
                        contr.init(sessionTemp.getConnectionParam(), db, null, true);
                        
                        DialogHelper.show("Create Trigger for Audit Logs", contr.getUI(), getController().getStage());

                        if (contr.getResponce() == DialogResponce.OK_YES) {

                            String content = sessionTemp.getService().getCreateAuditTriggerTemplateSQL(
                                dbName,
                                contr.getTriggerNameValue(),
                                contr.getBeforeAfterValue(),
                                contr.getTriggerEventValue(),
                                contr.getTriggerTableValue(),
                                contr.getTriggerBody(),
                                contr.getComment()                                
                            );

                            return new Pair<>(contr.getTriggerNameValue(), content);                        
                        }                    

                        return null;


                    }, QueryTabModule.TAB_USER_DATA__TRIGGER_TAB );
                }
            }
        });

        return triggerItem;
    }

    private MenuItem getFunctionItem(final ConnectionSession session) {
        MenuItem functionItem = new MenuItem("Function...", 
                new ImageView(SmartImageProvider.getInstance().getCreateFunction_image()));
        functionItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
                if (queryTabModule != null) {
                    
                    ConnectionSession session1 = session == null ? getController().getSelectedConnectionSession().get() : session;
                
                    Database db = session1.getConnectionParam().getSelectedDatabase();

                    if (functionImage == null) {
                        functionImage = SmartImageProvider.getInstance().getFunctionTab_image();
                    }

                    queryTabModule.addTemplatedQueryTab(
                            session1,
                            "Create function",
                            "Enter new function name:",
                            new java.util.function.Function<String, String>() {
                                @Override
                                public String apply(String t) {
                                    return session1.getService().getCreateFunctionTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                                }
                            }, QueryTabModule.TAB_USER_DATA__FUNCTION_TAB );
                }
            }
        });

        return functionItem;
    }

    private MenuItem getStoredProcedureItem(final ConnectionSession session) {
        MenuItem storedProcedureItem = new MenuItem("Stored Procedure...", 
                new ImageView(SmartImageProvider.getInstance().getCreateStoredProcs_image()));
        storedProcedureItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
                if (queryTabModule != null) {
                    ConnectionSession session1 = session == null ? getController().getSelectedConnectionSession().get() : session;
                    
                    Database db = session1.getConnectionParam().getSelectedDatabase();

                    if (procedureImage == null) {
                        procedureImage = SmartImageProvider.getInstance().getStoredProcTab_image();
                    }

                    queryTabModule.addTemplatedQueryTab(
                            session1,
                            "Create stored procedure",
                            "Enter new stored procedure name:",
                            new java.util.function.Function<String, String>() {
                                @Override
                                public String apply(String t) {
                                    return session1.getService().getCreateStoredProcedureTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                                }
                            }, QueryTabModule.TAB_USER_DATA__PROCEDURE_TAB  );
                }
            }
        });

        return storedProcedureItem;
    }
    
    private MenuItem getStoredProcedureWithCursorItem(final ConnectionSession session) {
        MenuItem storedProcedureItem = new MenuItem("Stored Procedure With Cursor...", 
                new ImageView(SmartImageProvider.getInstance().getCreateStoredProcs_image()));
        storedProcedureItem.setOnAction((ActionEvent event) -> {
            
            QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
            if (queryTabModule != null) {
                
                ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                
                Database db = sessionTemp.getConnectionParam().getSelectedDatabase();
                
                if (procedureImage == null) {
                    procedureImage = SmartImageProvider.getInstance().getStoredProcTab_image();
                }
                
                
                
                queryTabModule.addTemplatedQueryTab(sessionTemp, (Void t) -> {
                    
                    StoredProcedureWithCursorController contr = ((MainController)getController()).getTypedBaseController("StoredProcedureWithCursor");
                    
                    DialogHelper.show("Create Stored Procedure With Cursor", contr.getUI(), getController().getStage());
                    
                    if (contr.getResponce() == DialogResponce.OK_YES) {
                        try {
                            
                            ((MainController)getController()).usedStoredProcedureWithCursor();
                            
                            String content = sessionTemp.getService().getCreateStoredProcedureWithCursorTemplateSQL(
                                db != null ? db.getName() : "<db-name>",
                                contr.getProcedureNameValue(),
                                contr.getCursorNameValue(),
                                contr.getCursorQueryValue()
                            );
                        
                            return new Pair<>(contr.getProcedureNameValue(), content);
                        } catch (SQLException ex) {
                            DialogHelper.showError(getController(), "Error opening stored procedure script", ex.getMessage(), ex);
                        }
                        
                    }                    
                    
                    return null;
                    
                }, QueryTabModule.TAB_USER_DATA__PROCEDURE_TAB  
                );
            }
        });

        return storedProcedureItem;
    }
    
    private MenuItem getTableItem(final ConnectionSession session) {
        MenuItem tableItem = new MenuItem("Table...", 
                new ImageView(SmartImageProvider.getInstance().getCreateTable_image()));
        tableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableImage == null) {
                    tableImage = SmartImageProvider.getInstance().getTableTab_image();
                }
                ConnectionSession s = (session!= null? session : getController().getSelectedConnectionSession().get());
                Database db = s.getConnectionParam().getSelectedDatabase();                
                AlterTableController altertableController = ((MainController)getController()).getTypedBaseController("AlterTable");
                altertableController.setService(s.getService());
                
                Tab alterTab = new Tab("New table");
                alterTab.setGraphic(new ImageView(tableImage));
                alterTab.setUserData(altertableController);
                
                Parent p = altertableController.getUI();
                alterTab.setContent(p);                
                DBTable table = new DBTable("", null, null, null, new ArrayList<>(), db, null, null, new ArrayList<>(), new ArrayList<>());
                altertableController.init(alterTab, table, s, false);
                getController().addTab(alterTab, s);
                getController().showTab(alterTab, s);
            }
        });

        return tableItem;
    }

    private MenuItem getViewItem(final ConnectionSession session) {
        MenuItem viewItem = new MenuItem("View...", 
                new ImageView(SmartImageProvider.getInstance().getCreateView_image()));
        viewItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
                if (queryTabModule != null) {
                    
                    ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                
                    
                    Database db = sessionTemp.getConnectionParam().getSelectedDatabase();

                    if (viewImage == null) {
                        viewImage = SmartImageProvider.getInstance().getViewTab_image();
                    }
                    queryTabModule.addTemplatedQueryTab(
                            sessionTemp,
                            "Create view",
                            "Enter new view name:",
                            new java.util.function.Function<String, String>() {
                                @Override
                                public String apply(String t) {
                                    return sessionTemp.getService().getCreateViewTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                                }
                            }, QueryTabModule.TAB_USER_DATA__VIEW_TAB );
                }
            }
        });

        return viewItem;
    }

    private MenuItem getAlterDatabaseItem(final ConnectionSession session) {
        MenuItem alterDatabaseItem = new MenuItem("Alter Database",
                new ImageView(SmartImageProvider.getInstance().getDatabaseAlter_image()));
        alterDatabaseItem.setAccelerator(KeyCombination.keyCombination("F6"));
        alterDatabaseItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();                                
                DatabaseController databaseController = ((MainController)getController()).getTypedBaseController("DatabaseDialog");
                databaseController.setDBService(sessionTemp.getService());
                if (databaseController.showAlter(sessionTemp.getConnectionParam().getSelectedDatabase())) {
                    getController().refreshTree(sessionTemp, false);
                }
            }

        });

        return alterDatabaseItem;
    }

    private MenuItem getCreateDatabaseItem(final ConnectionSession session) {
        MenuItem createDatabaseItem = new MenuItem("Create Database",
                new ImageView(SmartImageProvider.getInstance().getDatabaseCreate_image()));
        createDatabaseItem.setAccelerator(KeyCombination.keyCombination("Ctrl+D"));
        createDatabaseItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession session0 = session != null ? session : getController().getSelectedConnectionSession().get();
                DatabaseController databaseController = ((MainController)getController()).getTypedBaseController("DatabaseDialog");
                databaseController.setDBService(session0.getService());

                if (databaseController.showCreate()) {

                    // show choosing scheam if need
                    if (databaseController.isNeedUseSchema()) {
                        SelectSchemaController selectSchemaController = ((MainController)getController()).getTypedBaseController("SelectSchema");
                        selectSchemaController.setDBService(session0.getService());

                        selectSchemaController.show(databaseController.getDatabaseName(), (Consumer) (Object t) -> {
                            getController().refreshTree(session0, false);
                            DialogHelper.showInfo(getController(), "Info", "Database created successfully", null);                        
                        });

                    } else {
                        getController().refreshTree(session0, false);
                        DialogHelper.showInfo(getController(), "Info", "Database created successfully", null);
                    }
                }            
            }

        });

        return createDatabaseItem;
    }

    private MenuItem getCopyTableToDifferentItem(final ConnectionSession session) {
        MenuItem copyTableToDifferentItem = new MenuItem("Copy DB to different Host/DB", new ImageView(SmartImageProvider.getInstance().getDatabaseCopy_image()));
        copyTableToDifferentItem.setUserData("Copy Database(s)To Different Host/Database...");
        copyTableToDifferentItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConnectionSession session0 = session != null ? session : getController().getSelectedConnectionSession().get();
                                
                DatabaseCopierController copierCon = ((MainController)getController()).getTypedBaseController("DatabaseCopier");
                Database db = session0.getConnectionParam().getSelectedDatabase();
                copierCon.insertSession(session0, db.getName());

                if (copierCon.showNow(getController().getStage())) {
                    getController().refreshTree(session0, false);
                } 
            }
        });

        return copyTableToDifferentItem;
    }

    ////////////////////// End of menu getters ///////////////////

    
    private void showScheduledBackupsDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ScheduledBackupWizardController controller = ((MainController)getController()).getTypedBaseController("ScheduledBackupWizard");
        
        Database database = null;
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof Database) {
                database = (Database)itemValue;
            }
        }
        
        try  {
            controller.init(session.getService(), session.getConnectionParam(), database);
            final Parent p = controller.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Export Data As Batch Script Wizard");

            dialog.showAndWait();
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
        }
    }
    
    private void showBackupDatabaseAsSQLDumpDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        BackupDatabaseAsSQLDumpController controller = ((MainController)getController()).getTypedBaseController("BackupDatabaseAsSQLDump");
        
        Database database = null;
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof Database) {
                database = (Database)itemValue;
            }
        }
        
        controller.init(session.getService(), session.getConnectionParam(), database, null);
        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("SQL Dump");

        dialog.showAndWait();
    }
            
    
    
   private void showImportExternalDataDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ImportTableController controller = ((MainController)getController()).getTypedBaseController("ImportTable");
        controller.init(session, null);
        
        final Scene scene = new Scene(controller.getUI());
        
        dialog.setScene(scene);
        dialog.initStyle(StageStyle.UTILITY);        
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Import External Data Wizard");
        dialog.showAndWait();
    }
    
    private void showExecuteSqlScriptDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ExecuteSqlScriptDialogController controller = ((MainController)getController()).getTypedBaseController("ExecuteSqlScriptDialog");
        controller.setDbService(session.getService());
        
        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Execute sql script");

        dialog.showAndWait();
        
        // and refresh tree
        getController().refreshTree(session, false);
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
