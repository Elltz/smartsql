package np.com.ngopal.smart.sql.modules;

import com.google.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.function.Function;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.components.TextMenu;
import np.com.ngopal.smart.sql.ui.controller.ReportDataFragmentationController;
import np.com.ngopal.smart.sql.structure.controller.ReportDataSizeController;
import np.com.ngopal.smart.sql.ui.controller.ReportIndexDetailController;
import np.com.ngopal.smart.sql.structure.controller.ReportIndexDublicateController;
import np.com.ngopal.smart.sql.ui.controller.ReportIndexMisingFkController;
import np.com.ngopal.smart.sql.ui.controller.ReportIndexSummaryController;
import np.com.ngopal.smart.sql.structure.controller.ReportIndexUnusedController;
import np.com.ngopal.smart.sql.ui.controller.ReportRamConfigurationController;
import np.com.ngopal.smart.sql.ui.controller.ReportRamUsageController;
import np.com.ngopal.smart.sql.ui.controller.ReportSqlCommandsHistoryController;
import np.com.ngopal.smart.sql.ui.controller.ReportTemplateController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.provider.BaseControllerProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.ui.controller.FileSortingSQLQueryController;
import np.com.ngopal.smart.sql.ui.controller.FullTableSQLScanController;
import np.com.ngopal.smart.sql.ui.controller.Ninety5PercentileReponseSQLQueryController;
import np.com.ngopal.smart.sql.ui.controller.SQLStatementsController;
import np.com.ngopal.smart.sql.ui.controller.TempTableCreatingSQLQueryController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportsModule extends AbstractStandardModule {

    private static Image fragmentationReportImage;
    private static Image ramConfigurationReportImage;
    private static Image ramUsageReportImage;
    private static Image indexSummaryReportImage;
    private static Image misingFkReportImage;
    private static Image detailIndexesReportImage;
    private static Image sqlStatementsReportImage;

    @Inject
    private PreferenceDataService preferenceDataService;

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 200;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        return true;
    }

    @Override
    public boolean install() {

        ReportConfiguration miscellaneouseEventsStages = new ReportConfiguration();
        miscellaneouseEventsStages.title = "Events Stages Summary Report";
        miscellaneouseEventsStages.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_miscellaneous_events.png");
        miscellaneouseEventsStages.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_miscellaneous_events_tab.png");
        miscellaneouseEventsStages.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_MISCELLANIOUCE_EVENTS);
        miscellaneouseEventsStages.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        miscellaneouseEventsStages.beforeLoad = (Object obj) -> {
            return ReportsModule.isServerVersionGreaterEqual(getController().getSelectedConnectionSession().get().getService().getDbSQLVersion(), 5, 7) && getController().getSelectedConnectionSession().get().getService().isPerformanceSchemaOn();
        };
        miscellaneouseEventsStages.queryErrorMessage = "The MySQL server  Memory Summary Tables are not enabled. \nPlease enable for mroe details https://dev.mysql.com/doc/refman/5.7/en/memory-summary-tables.html";
        miscellaneouseEventsStages.withCharts = true;
        miscellaneouseEventsStages.chartKeyIndex = 1;
        miscellaneouseEventsStages.chartValueIndex = 2;

        ReportConfiguration miscellaneouseQAStatistic = new ReportConfiguration();
        miscellaneouseQAStatistic.title = "SQL Optimizer Statistic Report";
        miscellaneouseQAStatistic.menuItemImage = StandardDefaultImageProvider.getInstance().getAnalyzer_image();
        miscellaneouseQAStatistic.tabImage = StandardDefaultImageProvider.getInstance().getQueryAnalyzeTab_image();
        miscellaneouseQAStatistic.needBlobDialog = (String name) -> {
            return "Query".equals(name) || "Recommendation".equals(name) || "Recommendation Index".equals(name);
        };
        miscellaneouseQAStatistic.resultFunction = (String database) -> {
            return getMainController().getPerformanceTuningService().getReportDataByConnectionParamsId(getController().getSelectedConnectionSession().get().getConnectionParam().getId());
        };

        ReportConfiguration ramAllocationByClient = new ReportConfiguration();
        ramAllocationByClient.title = "RAM Allocation By Client's Host";
        ramAllocationByClient.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_by_client.png");
        ramAllocationByClient.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_by_client_tab.png");
        ramAllocationByClient.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_ALLOCATION_BY_CLIENT);

        ReportConfiguration ramAllocationByThread = new ReportConfiguration();
        ramAllocationByThread.title = "RAM Allocation By Thread";
        ramAllocationByThread.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_by_thread.png");
        ramAllocationByThread.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_by_thread_tab.png");
        ramAllocationByThread.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_ALLOCATION_BY_THREAD);

        ReportConfiguration ramAllocationByUser = new ReportConfiguration();
        ramAllocationByUser.title = "RAM Allocation By User";
        ramAllocationByUser.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_by_user.png");
        ramAllocationByUser.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_by_user_tab.png");
        ramAllocationByUser.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_ALLOCATION_BY_USER);

        ReportConfiguration ramAllocationDetails = new ReportConfiguration();
        ramAllocationDetails.title = "RAM Allocation Details";
        ramAllocationDetails.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_details.png");
        ramAllocationDetails.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_allocation_details_tab.png");
        ramAllocationDetails.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_ALLOCATION_DETAILS);

        ReportConfiguration ramTotalAllocation = new ReportConfiguration();
        ramTotalAllocation.title = "Total RAM Allocation";
        ramTotalAllocation.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_total_allocation.png");
        ramTotalAllocation.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_total_allocation_tab.png");
        ramTotalAllocation.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_TOTAL_ALLOCATION);

        ReportConfiguration ramSchemaWiseBufferUsage = new ReportConfiguration();
        ramSchemaWiseBufferUsage.title = "Schema Wise Innodb Buffer Usage";
        ramSchemaWiseBufferUsage.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_schema_wise_buffer_usage.png");
        ramSchemaWiseBufferUsage.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_schema_wise_buffer_usage_tab.png");
        ramSchemaWiseBufferUsage.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_SCHEMA_WISE_BUF_USAGE);

        ReportConfiguration ramTableWiseBufferUsage = new ReportConfiguration();
        ramTableWiseBufferUsage.title = "Table Wise Innodb Buffer Usage";
        ramTableWiseBufferUsage.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_table_wise_buffer_usage.png");
        ramTableWiseBufferUsage.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_table_wise_buffer_usage_tab.png");
        ramTableWiseBufferUsage.queryFunction = (String database) -> QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_TABLE_WISE_BUF_USAGE);

        ReportConfiguration fileIoTableWise = new ReportConfiguration();
        fileIoTableWise.title = "Table Wise File IO";
        fileIoTableWise.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_table_wise.png");
        fileIoTableWise.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_table_wise_tab.png");
        fileIoTableWise.queryFunction = (String database) -> {
            String s = "/";
            try (ResultSet rs = (ResultSet) getController().getSelectedConnectionSession().get().getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_TABLE_WISE_0))) {
                if (rs != null && rs.next()) {
                    if (rs.getInt(1) == 1) {
                        s = "\\\\";
                    }
                }
            } catch (SQLException ex) {
            }

            return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_TABLE_WISE_1).replace("%s", s);
        };
        fileIoTableWise.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        fileIoTableWise.beforeLoad = (Object obj) -> {
            DBService serv = getController().getSelectedConnectionSession().get().getService();
            return ReportsModule.isServerVersionGreaterEqual(serv.getDbSQLVersion(), 5, 7) && serv.isPerformanceSchemaOn();
        };

        ReportConfiguration fileIoTableLatencyWise = new ReportConfiguration();
        fileIoTableLatencyWise.title = "Table Wise File IO latency";
        fileIoTableLatencyWise.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_table_latency_wise.png");
        fileIoTableLatencyWise.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_table_latency_wise_tab.png");
        fileIoTableLatencyWise.queryFunction = (String database) -> {
            String s = "/";
            try (ResultSet rs = (ResultSet) getController().getSelectedConnectionSession().get().getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_TABLE_LATEN_WISE_0))) {
                if (rs != null && rs.next()) {
                    if (rs.getInt(1) == 1) {
                        s = "\\\\";
                    }
                }
            } catch (SQLException ex) {
            }

            return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_TABLE_LATEN_WISE_1).replace("%s", s);
        };
        fileIoTableLatencyWise.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        fileIoTableLatencyWise.beforeLoad = (Object obj) -> {
            DBService serv = getController().getSelectedConnectionSession().get().getService();
            return ReportsModule.isServerVersionGreaterEqual(serv.getDbSQLVersion(), 5, 7) && serv.isPerformanceSchemaOn();
        };

        ReportConfiguration fileIoThreadLatencyWise = new ReportConfiguration();
        fileIoThreadLatencyWise.title = "Thread Wise File IO Latency";
        fileIoThreadLatencyWise.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_thread_wise.png");
        fileIoThreadLatencyWise.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_thread_wise_tab.png");
        fileIoThreadLatencyWise.queryFunction = (String database) -> {
            return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_THREAD_WISE);
        };
        fileIoThreadLatencyWise.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        fileIoThreadLatencyWise.beforeLoad = (Object obj) -> {
            DBService serv = getController().getSelectedConnectionSession().get().getService();
            return ReportsModule.isServerVersionGreaterEqual(serv.getDbSQLVersion(), 5, 7) && serv.isPerformanceSchemaOn();
        };

        ReportConfiguration fileIoUserWise = new ReportConfiguration();
        fileIoUserWise.title = "User Wise FILE IO Latency";
        fileIoUserWise.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_user_wise.png");
        fileIoUserWise.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_user_wise_tab.png");
        fileIoUserWise.queryFunction = (String database) -> {
            return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_USER_WISE);
        };
        fileIoUserWise.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        fileIoUserWise.beforeLoad = (Object obj) -> {
            DBService serv = getController().getSelectedConnectionSession().get().getService();
            return ReportsModule.isServerVersionGreaterEqual(serv.getDbSQLVersion(), 5, 7) && serv.isPerformanceSchemaOn();
        };

        ReportConfiguration fileIoEventWiseBytes = new ReportConfiguration();
        fileIoEventWiseBytes.title = "Event Wise File IO Latency by Bytes";
        fileIoEventWiseBytes.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_event_bytes_wise.png");
        fileIoEventWiseBytes.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_event_bytes_wise_tab.png");
        fileIoEventWiseBytes.queryFunction = (String database) -> {
            return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_EVENT_BYTE_WISE);
        };
        fileIoEventWiseBytes.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        fileIoEventWiseBytes.beforeLoad = (Object obj) -> {
            DBService serv = getController().getSelectedConnectionSession().get().getService();
            return ReportsModule.isServerVersionGreaterEqual(serv.getDbSQLVersion(), 5, 7) && serv.isPerformanceSchemaOn();
        };

        ReportConfiguration fileIoEventWise = new ReportConfiguration();
        fileIoEventWise.title = "Event Wise File IO Latency";
        fileIoEventWise.menuItemImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_event_wise.png");
        fileIoEventWise.tabImage = new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io_event_wise_tab.png");
        fileIoEventWise.queryFunction = (String database) -> {
            return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_FILE_IO_EVENT_WISE);
        };
        fileIoEventWise.beforeLoadMessage = "The following reports work only in mysql 5.7 or higher versions \nand performance schema must be enabled!";
        fileIoEventWise.beforeLoad = (Object obj) -> {
            DBService serv = getController().getSelectedConnectionSession().get().getService();
            return ReportsModule.isServerVersionGreaterEqual(serv.getDbSQLVersion(), 5, 7) && serv.isPerformanceSchemaOn();
        };

        // TODO Need change all reports
        fragmentationReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_fragmentation_tab.png").toExternalForm());
        ramConfigurationReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_configuration_tab.png").toExternalForm());
        ramUsageReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_usage_tab.png").toExternalForm());
        indexSummaryReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_index_summary_tab.png").toExternalForm());
        misingFkReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_mising_fk_index_tab.png").toExternalForm());
        detailIndexesReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_index_detail_tab.png").toExternalForm());
        sqlStatementsReportImage = new Image(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/reports/report_sql_statements_tab.png").toExternalForm());

        Menu dataReport = new TextMenu();
        dataReport.setText("Data Size Report");
        dataReport.setGraphic(constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_data.png")), 20, 20));

        MenuItem dataSizeReportItem = new MenuItem("Database size report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_data_size.png")), 20, 20));
        dataSizeReportItem.setOnAction((ActionEvent event) -> {
            dataSizeReport(getController(), ReportDataSizeController.DATABASE_ITEM__EMPTY, ReportDataSizeController.ENGINE_ITEM__EMPTY);
        });

        MenuItem tableSizeReportItem = new MenuItem("Table size report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_data_size.png")), 20, 20));
        tableSizeReportItem.setOnAction((ActionEvent event) -> {
            dataSizeReport(getController(), ReportDataSizeController.DATABASE_ITEM__ALL_TABLES, ReportDataSizeController.ENGINE_ITEM__EMPTY);
        });

        MenuItem engineSizeReportItem = new MenuItem("Engine wise data size report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_data_size.png")), 20, 20));
        engineSizeReportItem.setOnAction((ActionEvent event) -> {
            dataSizeReport(getController(), ReportDataSizeController.DATABASE_ITEM__ALL_TABLES, ReportDataSizeController.ENGINE_ITEM__ALL_ENGINES);
        });

        MenuItem fragmentationReportItem = new MenuItem("Data fragmentation report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_fragmentation.png")), 20, 20));
        fragmentationReportItem.setOnAction((ActionEvent event) -> {
            fragmentationReport(ReportsModule.this);
        });

        dataReport.getItems().addAll(dataSizeReportItem, tableSizeReportItem, engineSizeReportItem, fragmentationReportItem);

        Menu ramReport = new TextMenu();
        ramReport.setText("RAM Reports");
        ramReport.setGraphic(constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram.png")), 20, 20));

        MenuItem ramConfigurationReportItem = new MenuItem("RAM configuration report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_configuration.png")), 20, 20));
        ramConfigurationReportItem.setOnAction((ActionEvent event) -> {
            ramConfigurationReport(ReportsModule.this);
        });

        MenuItem ramUsageReportItem = new MenuItem("RAM usage from Performance schema",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_ram_usage.png")), 20, 20));
        ramUsageReportItem.setOnAction((ActionEvent event) -> {
            ramUsageReport(ReportsModule.this);
        });

        ramReport.getItems().addAll(
                ramConfigurationReportItem,
                ramUsageReportItem,
                createMenuItem(this, ramAllocationByClient),
                createMenuItem(this, ramAllocationByThread),
                createMenuItem(this, ramAllocationByUser),
                createMenuItem(this, ramAllocationDetails),
                createMenuItem(this, ramTotalAllocation),
                createMenuItem(this, ramSchemaWiseBufferUsage),
                createMenuItem(this, ramTableWiseBufferUsage)
        );

        Menu indexReport = new TextMenu();
        indexReport.setText("Index Reports");
        indexReport.setGraphic(constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_index.png")), 20, 20));

        MenuItem dublicateIndexReportItem = new MenuItem("Duplicate index report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_dublicate_index.png")), 20, 20));
        dublicateIndexReportItem.setOnAction((ActionEvent event) -> {
            dublicateIndexReport(getController(), preferenceDataService);
        });

        MenuItem indexesSummaryItem = new MenuItem("Indexes Summary report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_index_summary.png")), 20, 20));
        indexesSummaryItem.setOnAction((ActionEvent event) -> {
            indexesSummaryReport(ReportsModule.this);
        });

        MenuItem missingFkItem = new MenuItem("Missing Foreign key report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_mising_fk_index.png")), 20, 20));
        missingFkItem.setOnAction((ActionEvent event) -> {
            missingFkReport(ReportsModule.this);
        });

        MenuItem unusedIndexItem = new MenuItem("Unused Indexes report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_index_unused.png")), 20, 20));
        unusedIndexItem.setOnAction((ActionEvent event) -> {
            unusedIndexReport(getController());
        });

        MenuItem detailIndexItem = new MenuItem("Index Detailed Summary report",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_index_detail.png")), 20, 20));
        detailIndexItem.setOnAction((ActionEvent event) -> {
            detailIndexReport(ReportsModule.this);
        });

        indexReport.getItems().addAll(dublicateIndexReportItem, indexesSummaryItem, missingFkItem, unusedIndexItem, detailIndexItem);

        Menu sqlStatementsReport = new TextMenu();
        sqlStatementsReport.setText("SQL Statements");
        sqlStatementsReport.setGraphic(constructImageViewWithSize(
                new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_sql_statements.png")), 20, 20));

        MenuItem sqlCommandsHistoryItem = new MenuItem("SQL Commands history",
                constructImageViewWithSize(new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_sql_statements.png")), 20, 20));
        sqlCommandsHistoryItem.setOnAction((ActionEvent event) -> {
            sqlCommandsHistoryReport(ReportsModule.this);
        });
        
        MenuItem fullTableScanningSQLQueriesItem = new MenuItem("Table scanned queries",
                new ImageView(SmartImageProvider.getInstance().getFullTableScanningSQLQueries_image()));
        fullTableScanningSQLQueriesItem.setUserData(getSQLStatementSubControllers(FullTableSQLScanController.class));
        fullTableScanningSQLQueriesItem.setOnAction((ActionEvent event) -> {
            SQLStatementsController contr = (SQLStatementsController) ((MenuItem)event.getSource()).getUserData();
            createSQLStatements(contr, contr.getName(),
                    ((ImageView)((MenuItem)event.getSource()).getGraphic()).getImage(),
                    this);
        });
        
        MenuItem fileSortingSQLQueriesItem = new MenuItem("File sorting queries",
                new ImageView(SmartImageProvider.getInstance().getFileSortingSQLQueries_image()));
        fileSortingSQLQueriesItem.setUserData(getSQLStatementSubControllers(FileSortingSQLQueryController.class));
        fileSortingSQLQueriesItem.setOnAction((ActionEvent event) -> {
            SQLStatementsController contr = (SQLStatementsController) ((MenuItem)event.getSource()).getUserData();
            createSQLStatements(contr, contr.getName(),
                    ((ImageView)((MenuItem)event.getSource()).getGraphic()).getImage(),
                    this);
        });
        
        MenuItem tempTableCreatingQueriesItem = new MenuItem("Temp table creating queries",
                new ImageView(SmartImageProvider.getInstance().getTempTableCreatingQueries_image()));
        tempTableCreatingQueriesItem.setUserData(getSQLStatementSubControllers(TempTableCreatingSQLQueryController.class));
        tempTableCreatingQueriesItem.setOnAction((ActionEvent event) -> {
            SQLStatementsController contr = (SQLStatementsController) ((MenuItem)event.getSource()).getUserData();
            createSQLStatements(contr, contr.getName(),
                    ((ImageView)((MenuItem)event.getSource()).getGraphic()).getImage(),
                    this);
        });
        
        MenuItem sqlQuery95PercentileItem = new MenuItem("SQL Query 95 percentile queries",
                new ImageView(SmartImageProvider.getInstance().getSQLQuery95PercentileResTime_image()));
        sqlQuery95PercentileItem.setUserData(getSQLStatementSubControllers(Ninety5PercentileReponseSQLQueryController.class));
        sqlQuery95PercentileItem.setOnAction((ActionEvent event) -> {
            SQLStatementsController contr = (SQLStatementsController) ((MenuItem)event.getSource()).getUserData();
            createSQLStatements(contr, contr.getName(),
                    ((ImageView)((MenuItem)event.getSource()).getGraphic()).getImage(),
                    this);
        });

        sqlStatementsReport.getItems().addAll(sqlCommandsHistoryItem,fullTableScanningSQLQueriesItem, 
                fileSortingSQLQueriesItem,tempTableCreatingQueriesItem,sqlQuery95PercentileItem);
        
        for(MenuItem mi : sqlStatementsReport.getItems()){
            mi.setDisable(true);
        }
        sqlStatementsReport.setOnShowing(new EventHandler() {
            @Override
            public void handle(Event event) {
                for (MenuItem mi : sqlStatementsReport.getItems()) {
                    SQLStatementsController contr = (SQLStatementsController) mi.getUserData();
                    mi.setDisable(contr != null && !contr.isCompatibleWithSystem());
                }
            }
        });

        Menu miscellaneousReport = new TextMenu();
        miscellaneousReport.setText("Miscellaneous Reports");
        miscellaneousReport.setGraphic(constructImageViewWithSize(
                new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_miscellaneous.png")), 20, 20));

        miscellaneousReport.getItems().addAll(
                createMenuItem(this, miscellaneouseEventsStages),
                createMenuItem(this, miscellaneouseQAStatistic)
        );

        Menu fileIoReport = new TextMenu();
        fileIoReport.setText("File IO Reports");
        fileIoReport.setGraphic(constructImageViewWithSize(
                new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/reports/report_file_io.png")), 20, 20));

        fileIoReport.getItems().addAll(
                createMenuItem(this, fileIoTableWise),
                createMenuItem(this, fileIoTableLatencyWise),
                createMenuItem(this, fileIoThreadLatencyWise),
                createMenuItem(this, fileIoUserWise),
                createMenuItem(this, fileIoEventWiseBytes),
                createMenuItem(this, fileIoEventWise)
        );

        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.REPORTS, 
                new MenuItem[]{dataReport, ramReport, fileIoReport, indexReport, sqlStatementsReport, miscellaneousReport});

        return true;
    }

    public static boolean isServerVersionGreaterEqual(String version, int minor, int major) {

        String[] versions = version.split("\\.");
        if (versions.length > 0) {
            try {
                int v0 = Integer.valueOf(versions[0]);
                if (v0 > minor) {
                    return true;

                } else if (v0 == minor && versions.length > 1) {
                    int v1 = Integer.valueOf(versions[1]);
                    if (v1 >= major) {
                        return true;
                    }
                }
            } catch (Throwable th) {
            }
        }

        return false;
    }

    public MenuItem createMenuItem(AbstractStandardModule controller, ReportConfiguration configuration) {
        MenuItem mi = new MenuItem(configuration.title, new ImageView(configuration.menuItemImage));
        mi.setOnAction((ActionEvent event) -> {
            controller.getMainController().usedReports();

            Tab tab = new Tab(configuration.title);
            tab.setGraphic(constructImageViewWithSize(new ImageView(configuration.tabImage), 20, 20));

            final ReportTemplateController contr = getTypedBaseController("ReportTemplate");
            contr.init(controller.getController().getSelectedConnectionSession().get(), configuration);

            Parent p = contr.getUI();
            tab.setContent(p);

            tab.setOnClosed(new EventHandler<Event>() {
                @Override
                public void handle(Event event) {
                    contr.onExit();
                }
            });

            tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable,
                        Boolean oldValue, Boolean newValue) {
                    if (newValue != null && newValue) {
                        Platform.runLater(() -> contr.onEntry());
                    }
                }
            });

            controller.getController().addTab(tab, controller.getController().getSelectedConnectionSession().get());
            controller.getController().showTab(tab, controller.getController().getSelectedConnectionSession().get());
        });
        return mi;
    }

    public static void createSQLStatements(SQLStatementsController contr, String tabName, Image image, AbstractStandardModule controller) {
        
        Tab tab = new Tab(tabName);
        tab.setGraphic(new ImageView(image));        
        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });
        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });
        
        Parent p = contr.getUI();
        tab.setContent(p);
        contr.setTab(tab);
        contr.checkStatus();
        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());        
    }
    
    private <T extends SQLStatementsController> T getSQLStatementSubControllers(Class<T> mecclass){
        T contr = new BaseControllerProvider<T>(getController(), mecclass) {
            @Override
            protected String getPreface() { return "/np/com/ngopal/smart/sql/ui/"; }
        }.get("SQLStatements");
        
        return contr;
    }
    
    public static void sqlCommandsHistoryReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        Tab tab = new Tab("SQL Commands history report");
        tab.setGraphic(new ImageView(sqlStatementsReportImage));

        final ReportSqlCommandsHistoryController contr = controller.getTypedBaseController("ReportSqlCommandsHistory");
        contr.init(controller.getMainController().getSelectedConnectionSession().get());

        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });

        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());
    }

    public static void indexesSummaryReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        Tab tab = new Tab("Indexes Summary report");
        tab.setGraphic(new ImageView(indexSummaryReportImage));

        final ReportIndexSummaryController contr = controller.getTypedBaseController("ReportIndexSummary");
        contr.init(controller.getMainController().getSelectedConnectionSession().get());

        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });

        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());
    }

    public static void detailIndexReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        Tab tab = new Tab("Index Detailed Summary report");
        tab.setGraphic(new ImageView(detailIndexesReportImage));

        final ReportIndexDetailController contr = controller.getTypedBaseController("ReportIndexDetail");
        contr.init(controller.getMainController().getSelectedConnectionSession().get());

        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });

        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());
    }

    public static void missingFkReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        Tab tab = new Tab("Missing Foreign key report");
        tab.setGraphic(new ImageView(misingFkReportImage));

        final ReportIndexMisingFkController contr = controller.getTypedBaseController("ReportIndexMisingFk");
        contr.init(controller.getMainController().getSelectedConnectionSession().get());

        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });

        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());

        controller.showInfo("Info", "Please select database from the dropdown list", null);
    }

    public static void fragmentationReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        Tab tab = new Tab("Data fragmentation report");
        tab.setGraphic(new ImageView(fragmentationReportImage));

        final ReportDataFragmentationController contr = controller.getTypedBaseController("ReportDataFragmentation");
        contr.init(controller.getMainController().getSelectedConnectionSession().get());

        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });

        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());
    }

    public static void ramConfigurationReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        Tab tab = new Tab("RAM configuration report");
        tab.setGraphic(new ImageView(ramConfigurationReportImage));

        final ReportRamConfigurationController contr = controller.getTypedBaseController("ReportRamConfiguration");
        contr.init(controller.getMainController().getSelectedConnectionSession().get());

        Parent p = contr.getUI();
        tab.setContent(p);

        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                contr.onExit();
            }
        });

        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    Platform.runLater(() -> contr.onEntry());
                }
            }
        });

        controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());
    }

    public static void ramUsageReport(AbstractStandardModule controller) {

        controller.getMainController().usedReports();

        ConnectionSession cs = controller.getMainController().getSelectedConnectionSession().get();

        if (!cs.getService().isPerformanceSchemaOn()) {
            controller.showInfo("Info", "The report works if you enable Performance schema. Please visit https://dev.mysql.com/doc/refman/5.7/en/performance-schema-startup-configuration for more details", null);

        } else {
            Tab tab = new Tab("RAM usage from Performance schema");
            tab.setGraphic(new ImageView(ramUsageReportImage));
            final ReportRamUsageController contr = controller.getTypedBaseController("ReportRamUsage");
            contr.init(controller.getMainController().getSelectedConnectionSession().get());

            Parent p = contr.getUI();
            tab.setContent(p);

            tab.setOnClosed(new EventHandler<Event>() {
                @Override
                public void handle(Event event) {
                    contr.onExit();
                }
            });

            tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable,
                        Boolean oldValue, Boolean newValue) {
                    if (newValue != null && newValue) {
                        Platform.runLater(() -> contr.onEntry());
                    }
                }
            });

            controller.getController().addTab(tab, controller.getMainController().getSelectedConnectionSession().get());
            controller.getController().showTab(tab, controller.getMainController().getSelectedConnectionSession().get());
        }
    }

    @Override
    public boolean uninstall() {
        return true;
    }

    @Override
    public String getInfo() {
        return "Slow Analyze Queries";
    }

    public class ReportConfiguration {

        Image menuItemImage;
        Image tabImage;

        String title;

        boolean withDatabase = false;
        boolean withCharts = false;

        Function<String, Map<String, Object>> resultFunction;
        Function<String, Boolean> needBlobDialog;

        String beforeQueryFunction;
        Function<String, String> queryFunction;
        String queryErrorMessage;

        Function<Object, Boolean> beforeLoad;
        String beforeLoadMessage;

        Function<Object, Boolean> afterLoad;

        int chartKeyIndex;
        int chartValueIndex;

        public String getBeforeQueryFunction() {
            return beforeQueryFunction;
        }

        public int getChartKeyIndex() {
            return chartKeyIndex;
        }

        public int getChartValueIndex() {
            return chartValueIndex;
        }

        public boolean isNeedBlobDialog(String name) {
            return needBlobDialog != null ? needBlobDialog.apply(name) : false;
        }

        public Map<String, Object> getResultFunction(String database) {
            return resultFunction != null ? resultFunction.apply(database) : null;
        }

        public String getQuery(String selectedDatabase) {
            return queryFunction.apply(selectedDatabase);
        }

        public boolean beforeLoad() {
            if (beforeLoad != null) {
                return beforeLoad.apply(null);
            }

            return true;
        }

        public boolean afterLoad() {
            if (afterLoad != null) {
                return afterLoad.apply(null);
            }

            return true;
        }

        public boolean isWithDatabase() {
            return withDatabase;
        }

        public boolean isWithCharts() {
            return withCharts;
        }

        public String getTitle() {
            return title;
        }

        public int getColumnWidth(int column) {
            // TODO calculate for each report, maybe add some list of widths
            return 150;
        }

        public String getBeforeLoadMessage() {
            return beforeLoadMessage;
        }

        public String getQueryErrorMessage() {
            return queryErrorMessage;
        }
    }
}
