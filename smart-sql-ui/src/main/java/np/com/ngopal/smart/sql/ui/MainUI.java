package np.com.ngopal.smart.sql.ui;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
public class MainUI extends Application {
    
    private static SynchorniseWorkProccessor snyProccessor;
    public static MainController controller;
    public static Injector injector;
    private static org.slf4j.Logger mainLogger;

    private String title;   
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            mainLogger.error(e.getMessage(), e);
        });
        
        Thread.currentThread().setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            mainLogger.error(e.getMessage(), e);
        });
        
        //IntroductionScreenController screenController = 
        StandardApplicationManager.getInstance().showInteractivePreLoader(primaryStage);
        
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                try {
                    StandardApplicationManager.getInstance().initializeDatabaseDriver("smart-sql.db", "np/com/ngopal/smart/sql/db/db.changelog.xml");
                    StandardApplicationManager.getInstance().initializeDatabaseDriver("profiler-sql.db", "np/com/ngopal/smart/sql/profiler/profiler.changelog.xml");
                    injector = Guice.createInjector(new UIModule());                    
                    controller = injector.getInstance(MainController.class);
                    controller.initConditionHelper();
                    callUpdate = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    mainLogger.error(e.getMessage(), e);
                }
            }

            @Override
            public void updateUI() {
                super.updateUI();
                showMainStage(primaryStage);                
                Platform.runLater(() -> {
                    controller.checkAppMiscSettings();
                });
            }            
        });
    }
    
    @Override
    public void init() {}


    @Override
    public void stop() throws Exception {
        if (controller != null) {
            controller.shutDown();              
        }
        StandardApplicationManager.getInstance().shutdown();
        snyProccessor.destroy();
        snyProccessor = null;
        controller = null;
        System.exit(0);//for now until i refractor syn processor
    }

    public static void main(String[] args) {           
        StandardApplicationManager.getInstance().handleApplicatEntryArguments(args);
        mainLogger = org.slf4j.LoggerFactory.getLogger(MainUI.class);
        snyProccessor = new SynchorniseWorkProccessor();
        snyProccessor.makeMain();   
        snyProccessor.start();
        launch(args);
    }
    
    private void showMainStage(Stage s) {
        Stage mainStage = s;
        if(mainStage == null){ mainStage = new Stage(StageStyle.DECORATED); }        
        mainStage.setTitle(title);
        Scene scene = new Scene(controller.getUI(), 1000, 680);                
        mainStage.setScene(scene);
        scene.getStylesheets().addAll(StandardResourceProvider.getInstance().getDependenceStylesheet(),
                getClass().getResource("styles/style.css").toExternalForm());        
        mainStage.setResizable(true);       
        mainStage.getIcons().clear();
        mainStage.getIcons().add(new Image(getClass().getResource("images/SmartMysql-setup-icon.png").toExternalForm()));
        StandardApplicationManager.getInstance().decorateApplicationAsStandard();
        controller.callCalculateUI();
        mainStage.setAlwaysOnTop(false);
        mainStage.show();
        mainLogger.info("showMainStage");
    }
    
    public void initTitle(String title) {
        this.title = title;
    }

    // TODO Maybe need remove this
    // but some times we need access to main controller
    // from non-controller classes
    public static MainController getMainController() {
        return controller;
    }
    
    
    // qb
    public static void usedQueryBuilder() {
        controller.usedQueryBuilder();
    }

    //  qa
    public static void usedQueryAnalyzer() {
        controller.usedQueryAnalyzer();
    }

    // sqa
    public static void usedSlowQueryAnalyzer() {
        controller.usedSlowQueryAnalyzer();
    }

    // sqaqa
    public static void usedQueryAnalyzerFromSlowQueryAnalyzer() {
        controller.usedQueryAnalyzerFromSlowQueryAnalyzer();
    }

    // debugger
    public static void usedDebugger() {
        controller.usedDebugger();
    }

    // dqa
    public static void usedQueryAnalyzerFromDebugger() {
        controller.usedQueryAnalyzerFromDebugger();
    }

    // reports
    public static void usedReports() {
        controller.usedReports();
    }

    // spwc
    public static void usedStoredProcedureWithCursor() {
        controller.usedStoredProcedureWithCursor();
    }

    // sd
    public static void usedSchemaDesigner() {
        controller.usedSchemaDesigner();
    }

    // psu
    public static void usedPublicSchema() {
        controller.usedPublicSchema();
    }

    // mst
    public static void usedMySqlTuner() {
        controller.usedMySqlTuner();
    }

    // qf
    public static void usedQueryFormat() {
        controller.usedQueryFormat();
    }

    // lfe
    public static void usedLockExecution() {
        controller.usedLockExecution();
    }

    // show_bq
    public static void usedWarnIcon(int count) {
        controller.usedWarnIcon(count);
    }

    // click_bq
    public static void usedClickWarnIcon() {
        controller.usedClickWarnIcon();
    }

    // total_connections
    public static void usedTotalDBConnections(Long size) {
        controller.usedTotalDBConnections(size);
    }    
}
