/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.structure.components.FilterableTableView;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.FilterableData;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.ReportsContract;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.utils.ExportUtils;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public abstract class SQLStatementsController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    protected Label titleLabel;
    
    @FXML
    protected AnchorPane mainUI;
    
    @FXML
    protected Button refreshButton;
    
    @FXML
    protected Button exportButton;
    
    @FXML
    protected FilterableTableView tableView;
    
    private ProgressIndicator pi = new ProgressIndicator();
    
    @FXML
    public void exportAction(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void run() { 
                    
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (FilterableData filterData: tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();
                        for(String s : filterData.getValuesFoFilter()){
                            row.add(s);
                        }
                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
                    
                    ObservableList<TableColumn> tableViewColumns = tableView.getTable().getColumns();
                    for(int i =1; i < tableViewColumns.size(); i++){
                        Label label = (Label) tableViewColumns.get(i).getGraphic();
                        columns.put(label.getText(), true);
                        columnWidths.add(150);                        
                    }
                 
                    sheets.add(getName() + " report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
                }
            });
        }
    }
    
    @FXML
    public void refreshAction(){      
        Platform.runLater(()-> tableView.getTable().setPlaceholder(pi) );
        tableView.removeItems(new ArrayList(tableView.getSourceData()));
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() { 
                List<? extends FilterableData> data = getData();
                Platform.runLater(()-> tableView.addItems(data) );
                Platform.runLater(()-> tableView.getTable().setPlaceholder(null) );
            }
        });
    }
    
    protected Tab tab;

    public boolean isCompatibleWithSystem() {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        String[] vers = getCompatibiltyVers().split("\\.");
        return FunctionHelper.mysqlVersion(session, Integer.valueOf(vers[0]), Integer.valueOf(vers[1]));
    }

    public void checkEventCollation() {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL,
                QueryProvider.QUERY__REPORT_SQL_COMMANDS_HISTORY_0))) {
            if (rs != null && rs.next()) {
                int value = rs.getInt(1);
                onEventCollationCheckDone(value == 0);
            }
        } catch (SQLException ex) {
            Platform.runLater(() -> ((MainController) getController()).showError("Error", ex.getMessage(), null));
        }
    }
    
    public void onEventCollationCheckDone(boolean needActivation) {
        if (needActivation) {
            EnablingPerformanceSchemaController c = ((MainController) getController()).getTypedBaseController("EnablingPerformanceSchema");
            Stage dialog = new Stage(StageStyle.UTILITY);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Message");
            dialog.setScene(new Scene(c.getUI()));
            dialog.showAndWait();
            
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            if (c.getResponce() == DialogResponce.CANCEL) {
                destroyController();
            }else{
                String[] queries = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL,
                        QueryProvider.QUERY__REPORT_SQL_COMMANDS_HISTORY_1).split(";");
                try {
                    for (String query : queries) {
                        session.getService().execute(query);
                    }
                } catch (SQLException ex) {
                    Platform.runLater(() -> ((MainController) getController()).showError("Error", ex.getMessage(), null));
                }
                init();
            }
        }else { init(); }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView) refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView) exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        titleLabel.setText(getName());        
        pi.setMaxWidth(30);
        pi.setMaxHeight(30);
    }
    
    public final void checkStatus() {
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                checkEventCollation();
            }
        });
    }
    
    public String getCompatibiltyVers(){
        return "5.6";
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    public void setTab(Tab tab){
        this.tab = tab;
    }
    
    public void init(){
        refreshAction();
    }

    @Override
    public BaseController getReportController() {
        return this;
    }
    
    public void destroyController(){
        tab.getTabPane().getTabs().remove(tab);
        tab.setContent(null);
    }
    
    
    public abstract String getName();
    public abstract List<? extends FilterableData> getData();
    
}
