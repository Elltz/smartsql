package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;
import np.com.ngopal.smart.sql.structure.utils.*;
/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class CreateDatabaseFromSchemaController  extends BaseController implements Initializable {
    
    private static final String DEFAULT = "[default]";
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TextField databaseNameField;
    @FXML
    private ComboBox<String> charsetCombobox;
    @FXML
    private ComboBox<String> collationCombobox;
    
    
    @FXML
    private GridPane publicSchemaGridPane;
    @FXML
    private CheckBox usingPublicSchemaCheckbox;
    @FXML
    private TextField searchField;
    @FXML
    private Button createButton;
    @FXML
    private Button searchButton;
    
    @FXML
    private TableColumn<ErrDiagram, String> nameColumn;
    @FXML
    private TableColumn<ErrDiagram, String> domainColumn;
    @FXML
    private TableColumn<ErrDiagram, String> subdomainColumn;
    @FXML
    private TableColumn<ErrDiagram, String> numberOfTablesColumn;
    @FXML
    private TableColumn<ErrDiagram, String> descriptionColumn;
    
    @FXML
    private TableView<ErrDiagram> publicDiagramsTable;
    
    private Stage dialog;
    
    private DBService dbService;
    private SchemaDesignerModule parentController;
        
    public void init(SchemaDesignerModule parentModule, DBService dbService) {
        this.dbService = dbService;
        this.parentController = parentModule;     
        
        initData();
        
        if (dialog == null) {
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setResizable(false);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Create database");
            dialog.setScene(new Scene(getUI()));
        }
        
        dialog.showAndWait();
    }
    
    
    private void initData() {
        charsetCombobox.getItems().add(DEFAULT);
        try {
            charsetCombobox.getItems().addAll(dbService.getCharSet());
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error loading charset", ex);
        }
        collationCombobox.getItems().add(DEFAULT);
        charsetCombobox.getSelectionModel().select(0);
        collationCombobox.getSelectionModel().select(0);
        charsetCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            collationCombobox.getItems().clear();
            collationCombobox.setValue(null);
            if (DEFAULT.equals(newValue)) {
                collationCombobox.getItems().add(DEFAULT);
                collationCombobox.getSelectionModel().select(0);
            } else {
                collationCombobox.getItems().add(DEFAULT);
                try {
                    collationCombobox.getItems().addAll(dbService.getCollation(newValue));
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", "Error loading collations", ex);
                }
                collationCombobox.getSelectionModel().select(0);
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////
    }
    
    @FXML
    public void searchAction(ActionEvent event) {
        
        publicDiagramsTable.getItems().clear();
        
        String searchText = searchField.getText();

        try {
            List<ErrDiagram> list = PublicDataAccess.getInstanсe().searchErrDiagram(searchText);
            if (list != null) {
                publicDiagramsTable.getItems().addAll(list);
            } 
        } catch (PublicDataException ex) {
           DialogHelper.showError(getController(), "Error searching public schemas", ex.getMessage(), ex, dialog);
        }
    }
    
    @FXML
    public void createAction(ActionEvent event) {   
        
        try {
            String charset = charsetCombobox.getSelectionModel().getSelectedItem();
            String collation = collationCombobox.getSelectionModel().getSelectedItem();
            
            int result = (int)dbService.createDatabase(databaseNameField.getText(), DEFAULT.equals(charset) ? null : charset, DEFAULT.equals(collation) ? null : collation);
            if (result >= 0) {
                
                ConnectionSession session = getController().getSelectedConnectionSession().get();
                
                if (usingPublicSchemaCheckbox.isSelected()) {
                    ErrDiagram ed = publicDiagramsTable.getSelectionModel().getSelectedItem();
                    if (ed != null) {
                        if (parentController.getSelectedDiagram().get() != null) {
                            DialogResponce responce = DialogHelper.showConfirm(getController(), "Save diagram", "Do you want save current Diagram?");
                            if (DialogResponce.OK_YES == responce) {
                                parentController.saveDiagram();
                            }
                        }
                        DesignerErrDiagram diagram = parentController.openDiagram(publicDiagramsTable.getSelectionModel().getSelectedItem());
                        
                        if (diagram != null) {

                            diagram.database().set(databaseNameField.getText());
                            diagram.host().set(session.getConnectionParam().getAddress() + ":" + session.getConnectionParam().getPort());
                            diagram.name().set(databaseNameField.getText());

                            parentController.saveDiagram();
                            parentController.showForwardEngineer(diagram);
                        }
                    }
                } else {
                    
                    if (parentController.getSelectedDiagram().get() != null) {
                        DialogResponce responce = DialogHelper.showConfirm(getController(), "Save diagram", "Do you want save current Diagram?");
                        if (DialogResponce.OK_YES == responce) {
                            parentController.saveDiagram();
                        }
                    }
                    
                    DesignerErrDiagram diagram = new DesignerErrDiagram();
                    diagram.database().set(databaseNameField.getText());
                    diagram.host().set(session.getConnectionParam().getAddress() + ":" + session.getConnectionParam().getPort());
                    diagram.name().set(databaseNameField.getText());
                    
                    
                    parentController.openDesignerDiagram(diagram);
                    parentController.saveDiagram();
                }
                
            } else {
                DialogHelper.showError(getController(), "Error", "Database creation error", null);
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error creating database", ex);
        }
        
//        DesignerErrDiagram d = parentController.openDiagram(publicDiagramsTable.getSelectionModel().getSelectedItem());
//        d.changed().set(true);
        
        dialog.close();
    }

    @FXML
    public void cancelAction(ActionEvent event) {
        dialog.close();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        searchField.setOnAction((ActionEvent event) -> {
            searchAction(event);
        });
        
        
        publicSchemaGridPane.disableProperty().bind(usingPublicSchemaCheckbox.selectedProperty().not());
        
        searchButton.disableProperty().bind(Bindings.isEmpty(searchField.textProperty()));
        
        createButton.disableProperty().bind(Bindings.isEmpty(databaseNameField.textProperty()));
        
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        domainColumn.setCellValueFactory(new PropertyValueFactory<>("domainName"));
        subdomainColumn.setCellValueFactory(new PropertyValueFactory<>("subDomainName"));
        numberOfTablesColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfTables"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
}
