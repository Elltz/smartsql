package np.com.ngopal.smart.sql.ui.controller.designer;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerViewPane  extends VBox implements DesignerPane {

    SchemaDesignerTabContentController.GraphNodeType type;
    String color;
    DesignerView data;

    Label titleLabel;

    public DesignerViewPane(SchemaDesignerTabContentController controller, DesignerView data, SchemaDesignerTabContentController.GraphNodeType type) {

        this.data = data;
        this.type = type;
        
        addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                controller.showViewTab(data);
            }
        });

        // header
        ImageView iv = new ImageView(type.getImage());
        iv.setFitHeight(20);
        iv.setFitWidth(20);

        titleLabel = new Label("", iv);
        titleLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        titleLabel.getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_VIEW);

        titleLabel.textProperty().bind(data.name());
        
        titleLabel.setStyle("-fx-background-color: " + data.color().get());
        data.color().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            titleLabel.setStyle("-fx-background-color: " + newValue);
        });

        getChildren().add(titleLabel);

        VBox.setVgrow(titleLabel, Priority.ALWAYS);
        setFillWidth(true);

        updateData();
        
        addDefaultDataListeners();
    }

    @Override
    public DesignerView getData() {
        return data;
    }

    @Override
    public Region getRegion() {
        return this;
    }
    
    @Override
    public boolean canChangeHeight() {
        return true;
    }

    @Override
    public void setPrefSize(double prefWidth, double prefHeight) {
        super.setPrefSize(prefWidth, prefHeight);
        
        updateData();
    }


    private void updateData() {  
        
        setMinSize(80, 30);
    }
}
    