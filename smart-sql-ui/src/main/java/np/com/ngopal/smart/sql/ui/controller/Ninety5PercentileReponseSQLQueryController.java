/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TreeItem;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.models.Ninety5PercentileResponseSQLQueryData;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class Ninety5PercentileReponseSQLQueryController extends SQLStatementsController {

    @Override
    public String getName() {
        return "SQL Query 95 percentile queries";
    }

    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public String getCompatibiltyVers() {
        return "5.7";
    }

    public List<Ninety5PercentileResponseSQLQueryData> getData() {
        String query = "SELECT\n"
                + "  `stmts`.`DIGEST_TEXT`                                                                      AS `query`,\n"
                + "  `stmts`.`SCHEMA_NAME`                                                                      AS `db`,\n"
                + "  if(((`stmts`.`SUM_NO_GOOD_INDEX_USED` > 0) OR (`stmts`.`SUM_NO_INDEX_USED` > 0)), '*', '') AS `full_scan`,\n"
                + "  `stmts`.`COUNT_STAR`                                                                       AS `exec_count`,\n"
                + "  `stmts`.`SUM_ERRORS`                                                                       AS `err_count`,\n"
                + "  `stmts`.`SUM_WARNINGS`                                                                     AS `warn_count`,\n"
                + "  `stmts`.`SUM_TIMER_WAIT`                                                                   AS `total_latency`,\n"
                + "  `stmts`.`MAX_TIMER_WAIT`                                                                  AS `max_latency`,\n"
                + "  `stmts`.`AVG_TIMER_WAIT`                                                                   AS `avg_latency`,\n"
                + "  `stmts`.`SUM_ROWS_SENT`                                                                    AS `rows_sent`,\n"
                + "  round(ifnull((`stmts`.`SUM_ROWS_SENT` / nullif(`stmts`.`COUNT_STAR`, 0)), 0), 0)           AS `rows_sent_avg`,\n"
                + "  `stmts`.`SUM_ROWS_EXAMINED`                                                                AS `rows_examined`,\n"
                + "  round(ifnull((`stmts`.`SUM_ROWS_EXAMINED` / nullif(`stmts`.`COUNT_STAR`, 0)), 0), 0)       AS `rows_examined_avg`,\n"
                + "  `stmts`.`FIRST_SEEN`                                                                       AS `first_seen`,\n"
                + "  `stmts`.`LAST_SEEN`                                                                        AS `last_seen`,\n"
                + "  `stmts`.`DIGEST`                                                                           AS `digest`\n"
                + "FROM\n"
                + "  (`performance_schema`.`events_statements_summary_by_digest` `stmts` JOIN `sys`.`x$ps_digest_95th_percentile_by_avg_us` `top_percentile` ON((ROUND((`stmts`.`AVG_TIMER_WAIT` / 1000000), 0) >= `top_percentile`.`avg_us`)))\n"
                + "ORDER BY\n"
                + "  `stmts`.`AVG_TIMER_WAIT` DESC;";

        ArrayList<Ninety5PercentileResponseSQLQueryData> data = new ArrayList(100);
        try {
            ResultSet rs = (ResultSet) getController().getSelectedConnectionSession().get().getService().execute(query);
            while (rs.next()) {
                data.add(new Ninety5PercentileResponseSQLQueryData(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14),
                        rs.getString(15),
                        rs.getString(16)
                ));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources); //To change body of generated methods, choose Tools | Templates.
        TableColumn queryColumn = createColumn("Query", "query");
        TableColumn dbColumn = createColumn("DB", "db");
        TableColumn full_scanColumn = createColumn("Full Scan", "full_scan");
        TableColumn exec_countColumn = createColumn("Exec Count", "exec_count");
        TableColumn err_countColumn = createColumn("Err Count", "err_count");
        TableColumn warn_countColumn = createColumn("Warn Count", "warn_count");
        TableColumn total_latencyColumn = createColumn("Total Latency", "total_latency");
        TableColumn max_latencyColumn = createColumn("Max Latency", "max_latency");
        TableColumn avg_latencyColumn = createColumn("Avg Latency", "avg_latency");
        TableColumn rows_sentColumn = createColumn("Rows Sent", "rows_sent");
        TableColumn rows_sent_avgColumn = createColumn("Rows Sent Avg", "rows_sent_avg");
        TableColumn rows_examinedColumn = createColumn("Rows Examined", "rows_examined");
        TableColumn rows_examined_avgColumn = createColumn("Rows Examined Avg", "rows_examined_avg");
        TableColumn first_seenColumn = createColumn("First seen", "first_seen");
        TableColumn last_seenColumn = createColumn("Last Seen", "last_seen");
        TableColumn digestColumn = createColumn("Digest", "digest");

        tableView.setSearchEnabled(true);
        tableView.addTableColumn(queryColumn, 170, 170, false);
        tableView.addTableColumn(dbColumn, 100, 100, false);
        tableView.addTableColumn(full_scanColumn, 100, 100, false);
        tableView.addTableColumn(exec_countColumn, 100, 100, false);
        tableView.addTableColumn(err_countColumn, 100, 100, false);
        tableView.addTableColumn(warn_countColumn, 100, 100, false);
        tableView.addTableColumn(total_latencyColumn, 100, 100, false);
        tableView.addTableColumn(max_latencyColumn, 100, 100, false);
        tableView.addTableColumn(avg_latencyColumn, 100, 100, false);
        tableView.addTableColumn(rows_sentColumn, 100, 100, false);
        tableView.addTableColumn(rows_sent_avgColumn, 100, 100, false);
        tableView.addTableColumn(rows_examinedColumn, 100, 100, false);
        tableView.addTableColumn(rows_examined_avgColumn, 100, 100, false);
        tableView.addTableColumn(first_seenColumn, 100, 100, false);
        tableView.addTableColumn(last_seenColumn, 100, 100, false);
        tableView.addTableColumn(digestColumn, 100, 100, false);

        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }

    @Override
    public void onEntry() {
        //we need Left pane
        super.onExit();
    }

    @Override
    public void refreshAction() {
        TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().getValue()).get();
        if (item != null && item.getValue() != null && !item.getValue().getClass().isAssignableFrom(ConnectionParams.class)) {
            super.refreshAction();
        } else {
            DialogHelper.showInfo(getController(), "ACCESS DENIED", "Select a database & refresh", null);
        }
    }
}
