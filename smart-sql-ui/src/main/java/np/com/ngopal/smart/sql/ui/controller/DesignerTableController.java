package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.inject.Inject;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Function;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.ui.factories.EditingCell;
import np.com.ngopal.smart.sql.ui.factories.EditingComboBoxCell;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import tools.java.pats.models.SqlFormatter;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.ui.MainUI;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DesignerTableController extends BaseController implements Initializable {

    
    private static final String PRIMARY_KEY         = "PRIMARY_KEY";
    private static final String NOT_NULL            = "NOT_NULL";
    private static final String UNIQUE              = "UNIQUE";
    private static final String BINARY              = "BINARY";
    private static final String UNSIGNED            = "UNSIGNED";
    private static final String ZERO_FILL           = "ZERO_FILL";
    private static final String AUTO_INCREMENTAL    = "AUTO_INCREMENTAL";
    private static final String GENERATED           = "GENERATED";
    
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private GridPane gridPane;
    
    @FXML
    private TabPane alterTableTabPane;
    
    @FXML
    private TextField tableNameField;
    @FXML
    private Label schemaField;    
    
    @FXML
    private Label collationLabel;
    @FXML
    private Label engineLabel;
    @FXML
    private Label commentsLabel;
    @FXML
    private ComboBox collationComboBox;
    @FXML
    private ComboBox engineComboBox;
    @FXML
    private TextArea commentsArea;
    
    @FXML
    private GridPane tableButtonsPane;
    @FXML
    private TableView<DesignerColumn> columnsTableView;
     
    @FXML
    private TableColumn<DesignerColumn, String> columnNameCol;
    @FXML
    private TableColumn<DesignerColumn, String> dataTypeCol;
    @FXML
    private TableColumn<DesignerColumn, Boolean> pkCol;
    @FXML
    private TableColumn<DesignerColumn, Boolean> notNullCol;
    @FXML
    private TableColumn<DesignerColumn, Boolean> uniqueCol;
    @FXML
    private TableColumn<DesignerColumn, String> binaryCol;
    @FXML
    private TableColumn<DesignerColumn, String> unsignedCol;
    @FXML
    private TableColumn<DesignerColumn, String> zeroFillCol;
    @FXML
    private TableColumn<DesignerColumn, String> autoIncrCol;
    @FXML
    private TableColumn<DesignerColumn, Boolean> generatedCol;
    @FXML
    private TableColumn<DesignerColumn, String> defaultCol;
    
    
    @FXML
    private TableView<DesignerIndex> indexesTableView;
    
    @FXML
    private TableColumn<DesignerIndex, String> indexNameColumn;
    @FXML
    private TableColumn<DesignerIndex, DesignerIndex.IndexType> indexTypeColumn;
    
    @FXML
    private TableView<DesignerIndexColumn> indexColumnsTableView;
    @FXML
    private TableColumn<DesignerIndexColumn, Boolean> icColumnColumn;
    @FXML
    private TableColumn<DesignerIndexColumn, String> icPriorityColumn;
    @FXML
    private TableColumn<DesignerIndexColumn, String> icOrderColumn;
    @FXML
    private TableColumn<DesignerIndexColumn, String> icLengthColumn;
    
    @FXML
    private ComboBox<DesignerIndex.IndexStorageType> indexStorageType;
    @FXML
    private TextField indexKeyBlockSize;
    @FXML
    private TextField indexKeyParser;
    @FXML
    private TextArea indexComment;
    
    
    @FXML
    private TableView<DesignerForeignKey> foreignKeysTableView;
    @FXML
    private TableColumn<DesignerForeignKey, String> fkNameColumn;
    @FXML
    private TableColumn<DesignerForeignKey, DesignerTable> fkReferencedTableColumn;
    
    @FXML
    private TableView<DesignerForeignColumn> fkReferencedColumnsTableView;
    @FXML
    private TableColumn<DesignerForeignColumn, Boolean> fkColumn;
    @FXML
    private TableColumn<DesignerForeignColumn, DesignerColumn> fkReferencedColumn;
    
    @FXML
    private ComboBox fkOnUpdate;
    @FXML
    private ComboBox fkOnDelete;
    @FXML
    private CheckBox fkSkipSQLGeneration;
    @FXML
    private TextArea fkComment;
    
    
    @FXML
    private TextField autoIncrementTextField;
    @FXML
    private TextField avgRowLengthTextField;
    @FXML
    private TextField maxRowsTextField;
    @FXML
    private TextField minRowsTextField;
    @FXML
    private ComboBox<Integer> checksumComboBox;
    @FXML
    private ComboBox<DesignerTable.RowFormat> rowFormatComboBox;
    @FXML
    private ComboBox<Integer> delayKeyWriteComboBox;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    @FXML
    private Tab sqlPreviewTab;
    
    private DBService service;
    //private WebView webView;
    private RSyntaxTextArea previewArea;
    
    private String actualSQL;
    
    private DesignerDatabase designerDatabase;
    
    
    private final SimpleBooleanProperty expandedProperty = new SimpleBooleanProperty(true);
    
    
    private DesignerTable table;
    private int generatedTableColumnIndex = 0;
    
    private SchemaDesignerTabContentController parent;
    
    private Function<String, DesignerDataType> findDataTypeFunction = (String t) -> {            
        if (t != null) {
            String findText = t.trim().replaceAll("\\s", "");
            int index = findText.indexOf("(");
            if (index >= 0) {
                findText = findText.substring(0, index);
            }
            findText = findText.trim();
                    
            for (DesignerDataType type: SchemaDesignerModule.TYPES) {
                
                String name = type.name;
                int i = name.indexOf("(");
                if (i >= 0) {
                    name = name.substring(0, i).trim();
                }
                
                if (index >= 0 && name.toUpperCase().startsWith(findText.toUpperCase()) || index < 0 && name.toUpperCase().equals(findText.toUpperCase())) {
                    return type;
                }
            }
        }

        return null;
    };

    private final BiFunction<Integer, String, Boolean> checkAcceptedValue = (Integer index, String t) -> {
        boolean error = false;
        DesignerDataType dt = findDataTypeFunction.apply(t);
        if (dt == null) {
            error = true;
        } else if (t.contains("(")){
            if (t.contains(")")) {
                String length = t.substring(t.indexOf("(") + 1, t.lastIndexOf(")"));
                if (length.trim().isEmpty()) {
                    error = true;
                }
            } else {
                error = true;
            }
        }
        if (error) {
            DialogHelper.showError(getController(), "Could not use new data type", "The given data type\n\n " + t + "\n\n contains errors and cannot be accepted. The previous value is kept instead.", null,getStage());
        }

        return !error;
    };
        
            
    public void expandTableInfoAction(MouseEvent event) {
        expandedProperty.set(expandedProperty.not().get());
    }
    
    
    public void addColumn(ActionEvent event) {
        DesignerColumn c = new DesignerColumn();

        if (table.columns().isEmpty()) {
            c.name().set("id" + table.name().get());
            
            c.primaryKey().set(true);
            c.notNull().set(true);
            c.dataType().set("INT");
            
            primaryKeyChanged(c, true);
            
        } else {
            
            String index = generatedTableColumnIndex == 0 ? "" : "" + generatedTableColumnIndex;
            c.name().set(table.name().get() + "col" + index);
            
            c.dataType().set("VARCHAR(45)");
            
            generatedTableColumnIndex++;
        }
        
        table.columns().add(c);
        
        dataChanged();
        
        parent.getDesignerDiagramHistory().putHistoryPoint("Add column '" + table.name().get() + "." + c.name().get() + "'", parent.getCurrentDiagram());
    }
    
    @FXML
    public void saveAction(ActionEvent event) {
        Map<String, Object> tableMap = table.toMap();
        sourceTable.fromMapWithoutIndexesAndFks(tableMap);
        
        Map<String, DesignerTable> map = new HashMap<>();
        designerDatabase.tables().stream().forEach(t -> map.put(t.uuid().get(), t));
        sourceTable.fromMapIndexesAndFks(tableMap, map);        
        
        errDiagram.setDiagramData(new Gson().toJson(parent.getCurrentDiagram().toMap()));
        
        closeAction(null);
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        module.openDiagram(errDiagram);        
        module.makeCanSave();
    }
    
    @FXML
    public void closeAction(ActionEvent event) {
        if (tab != null && tab.getTabPane() != null) {
            tab.getTabPane().getTabs().remove(tab);
        }
    }
    
    public void removeColumn(ActionEvent event) {        
        if (!table.columns().isEmpty() && columnsTableView.getSelectionModel().getSelectedIndex() >= 0) {
            DesignerColumn dc = table.columns().remove(columnsTableView.getSelectionModel().getSelectedIndex());
            
            // remove column from index
            // if need remove index
            List<DesignerIndex> indexToRemove = new ArrayList<>();
            for (DesignerIndex di: table.indexes()) {
                di.columns().remove(dc);
                if (di.columns().isEmpty()) {
                    indexToRemove.add(di);
                }
            }
            
            for (DesignerIndex di: indexToRemove) {
                table.indexes().remove(di);
            }
            
            parent.getDesignerDiagramHistory().putHistoryPoint("Remove column '" + table.name().get() + "." + dc.name().get() + "'", parent.getCurrentDiagram());
        }
        
        table.notifyForeignKeysChanged();
        dataChanged();
    }
    
    public void moveColumnUp(ActionEvent event) {
        int index = columnsTableView.getSelectionModel().getSelectedIndex();
        if (index > 0) {
            DesignerColumn upColumn = table.columns().remove(index);
            table.columns().add(index - 1, upColumn);
            
            columnsTableView.getSelectionModel().select(upColumn);
            
            parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "." + upColumn.name().get() + "' Column Position UP", parent.getCurrentDiagram());
        }
        dataChanged();
    }
    
    public void moveColumnDown(ActionEvent event) {
        int index = columnsTableView.getSelectionModel().getSelectedIndex();
        if (index < table.columns().size() - 1) {
            DesignerColumn downColumn = table.columns().remove(index);
            table.columns().add(index + 1, downColumn);
            
            columnsTableView.getSelectionModel().select(downColumn);
            
            parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "." + downColumn.name().get() + "' Column Position DOWN", parent.getCurrentDiagram());
        }
        dataChanged();
    }
    
    
    public void addIndex(ActionEvent event) {
        table.indexes().add(new DesignerIndex());
        dataChanged();
    }
    
    public void removeIndex(ActionEvent event) {        
        if (!table.indexes().isEmpty() && indexesTableView.getSelectionModel().getSelectedIndex() >= 0) {
            
            DesignerIndex di = table.indexes().get(indexesTableView.getSelectionModel().getSelectedIndex());    
            
            if (di.foreignKey().get() != null) {
                DialogHelper.showError(getController(), "Cannot Delete Index","The index '" + di.name().get() + "' belongs to the Foreign Key '" + di.foreignKey().get().name().get() + "'.\nYou must delete the Foreign Key to delete index.",null, getStage());
            } else {
                
                table.indexes().remove(indexesTableView.getSelectionModel().getSelectedIndex());
                for (DesignerColumn c: di.columns()) {
                    if (di.isPrimaryKey()) {
                        c.primaryKey().set(false);
                    } else if (di.isUniqueKey()) {
                        c.unique().set(false);
                    }
                }
            }
            
            table.notifyForeignKeysChanged();
        }
        dataChanged();
    }
    
    
    public void addForeignKey(ActionEvent event) {
         table.foreignKeys().add(new DesignerForeignKey());
         dataChanged();
    }
    
    public void removeForeignKey(ActionEvent event) {
        if (!table.foreignKeys().isEmpty() && foreignKeysTableView.getSelectionModel().getSelectedIndex() >= 0) {
             
            DesignerForeignKey fk = table.foreignKeys().remove(foreignKeysTableView.getSelectionModel().getSelectedIndex());
            
            for (DesignerIndex di: table.indexes()) {
                if (di.foreignKey().get() != null && di.foreignKey().get().equals(fk)) {
                    table.indexes().remove(di);
                    break;
                }
            }
        }
        
        table.notifyForeignKeysChanged();
        dataChanged();
    }
    
    private void primaryKeyChanged(DesignerColumn column, boolean added) {
        
        // find primary key
        DesignerIndex pk = null;
        for (DesignerIndex di: table.indexes()) {
            if (di.isPrimaryKey()) {
                pk = di;
                break;
            }
        }
            
        if (added) {            
            // if not found create new
            if (pk == null) {
                pk = new DesignerIndex();
                pk.type().set(DesignerIndex.IndexType.PRIMARY);
                pk.name().set("PRIMARY");
                
                table.indexes().add(pk);
            }
            
            // add column to primary key
            pk.columns().add(column);
            pk.orderMap().put(column, pk.orderMap().size() + 1);
            pk.sortMap().put(column, DesignerIndex.IndexOrder.ASC);
            
        } else {
            
            if (pk != null) {
                // remove column from primary key
                pk.columns().remove(column);
                
                // if columns not empty remove from all maps
                if (!pk.columns().isEmpty()) {
                    pk.orderMap().remove(column);
                    pk.sortMap().remove(column);
                    pk.lengthMap().remove(column);
                    
                    // need change sort order
                    for (int i = 0; i < pk.columns().size(); i ++) {
                        DesignerColumn c = pk.columns().get(i);
                        pk.orderMap().put(c, i);
                    }
                } else {
                    // else remove primary key from table
                    table.indexes().remove(pk);
                }
            } else {
                // TODO What do?
                log.error("Try remove primary columns from null primary key!");
            }
        }
        
        table.notifyForeignKeysChanged();
        dataChanged();
    }
    
    
    private void uniqueChanged(DesignerColumn column, boolean added) {
        
        // find unique index
        DesignerIndex uq = null;
        for (DesignerIndex di: table.indexes()) {
            // need check only with size 1
            if (di.isUniqueKey() && di.columns().size() == 1) {
                for (DesignerColumn c: di.columns()) {
                    if (c.equals(column)) {
                        uq = di;
                        break;
                    }
                }
            }
        }
            
        if (added) {            
            // if not found create new
            if (uq == null) {
                uq = new DesignerIndex();
                uq.type().set(DesignerIndex.IndexType.UNIQUE);
                uq.name().set(generateUniqueIndexName(column));
                
                table.indexes().add(uq);
            }
            
            // add column to unique index
            uq.columns().add(column);
            uq.orderMap().put(column, uq.orderMap().size() + 1);
            uq.sortMap().put(column, DesignerIndex.IndexOrder.ASC);
            
        } else {
            
            if (uq != null) {
                // remove column from unique index
                uq.columns().remove(column);
                table.indexes().remove(uq);
            } else {
                // TODO What do?
                log.error("Try remove columns from null unique index!");
            }
        }
        dataChanged();
    }
    
    private String generateUniqueIndexName(DesignerColumn column) {
        // TODO refactor this
        String name = column.name().get() + "_UNIQUE";        
        int index = 1;
        String tempName = name;
        boolean notFoundUniquName = true;
        while (notFoundUniquName) {
            notFoundUniquName = false;
            for (DesignerIndex di: table.indexes()) {
                if (di.name().get().toUpperCase().equals(tempName.toUpperCase())) {
                    notFoundUniquName = true;
                    tempName = name + "_" + index;
                    index++;
                }
            }
        }
        
        return tempName;
    }
    
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        collationLabel.managedProperty().bind(collationLabel.visibleProperty());
        collationLabel.visibleProperty().bind(expandedProperty);
        
        engineLabel.managedProperty().bind(engineLabel.visibleProperty());
        engineLabel.visibleProperty().bind(expandedProperty);
        
        for (EngineType et: MySQLEngineTypes.TYPES) {
            engineComboBox.getItems().add(et.getDisplayValue());
        }
        
        commentsLabel.managedProperty().bind(commentsLabel.visibleProperty());
        commentsLabel.visibleProperty().bind(expandedProperty);
        
        collationComboBox.managedProperty().bind(engineComboBox.visibleProperty());
        collationComboBox.visibleProperty().bind(expandedProperty);
        
        collationComboBox.getItems().add("Schema Default");
        collationComboBox.getItems().addAll(SchemaDesignerModule.COLLATIONS);
        
//        columnCollationComboBox.getItems().add("Table Default");
//        columnCollationComboBox.getItems().addAll(SchemaDesignerModule.COLLATIONS);
        
        engineComboBox.managedProperty().bind(collationLabel.visibleProperty());
        engineComboBox.visibleProperty().bind(expandedProperty);
        
        commentsArea.managedProperty().bind(commentsArea.visibleProperty());
        commentsArea.visibleProperty().bind(expandedProperty);
        
        checksumComboBox.getItems().add(0);
        checksumComboBox.getItems().add(1);
        
        rowFormatComboBox.getItems().addAll(DesignerTable.RowFormat.values());

        delayKeyWriteComboBox.getItems().add(0);
        delayKeyWriteComboBox.getItems().add(1);
        
//        columnsTableView.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
//                boolean visible = newValue == null || newValue.intValue() > 50;     
//                tableButtonsPane.setVisible(visible);
//                columnsTableView.setVisible(visible);
//            
//        });
                
        expandedProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                // add removed info rows
                gridPane.getRowConstraints().get(1).setPrefHeight(25);
                gridPane.getRowConstraints().get(2).setPrefHeight(50);
            } else {
                // remove info row contrains
                gridPane.getRowConstraints().get(1).setPrefHeight(0);
                gridPane.getRowConstraints().get(2).setPrefHeight(0);
            }
        });
        
        expandedProperty.set(false);
        
        
        
        // <editor-fold desc=" Columns table columns ">

        columnsTableView.setEditable(true);

        columnNameCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).name());
        columnNameCol.setCellFactory(new Callback<TableColumn<DesignerColumn, String>, TableCell<DesignerColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        });
        columnNameCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<DesignerColumn, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<DesignerColumn, String> event) {
                
                event.getRowValue().name().set(event.getNewValue());
                dataChanged();
                
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "." + event.getRowValue().name().get() + "' Column Name", parent.getCurrentDiagram());
            }
        });


        final BiFunction<Integer, String, Void> commitValue = (Integer index, String value) ->  {
            table.columns().get(index).dataType().set(value.toUpperCase()); 
            dataChanged();
            return null;
        };
        
        List<String> types = new ArrayList<>();
        for (DesignerDataType dt: SchemaDesignerModule.TYPES) {
            types.add(dt.toString());
        }
        
        dataTypeCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).dataType());
        dataTypeCol.setCellFactory(new Callback<TableColumn<DesignerColumn, String>, TableCell<DesignerColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingComboBoxCell(FXCollections.observableArrayList(types), null, checkAcceptedValue, commitValue);
            }
        });
        dataTypeCol.setOnEditCommit((TableColumn.CellEditEvent<DesignerColumn, String> event) -> {
            parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "." + event.getRowValue().name().get() + "' Column Data Type", parent.getCurrentDiagram());
        });
        
        pkCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, Boolean> p) -> ((DesignerColumn)p.getValue()).primaryKey());
        pkCol.setCellFactory((TableColumn<DesignerColumn, Boolean> p) -> {            
            CheckBoxTableCells<DesignerColumn, Boolean> c = new CheckBoxTableCells<>(
                PRIMARY_KEY, 
                (DesignerColumn col) -> col.primaryKey().get(),
                (Integer t) -> table.columns().get(t).name() == null);        
            return c;
        });
        
        Label pkLabel = new Label("PK");
        pkLabel.setTooltip(new Tooltip("Belongs to primary key"));
        pkCol.setGraphic(pkLabel);
        
        notNullCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, Boolean> p) -> ((DesignerColumn)p.getValue()).notNull());
        notNullCol.setCellFactory((TableColumn<DesignerColumn, Boolean> p) -> {            
            CheckBoxTableCells<DesignerColumn, Boolean> c = new CheckBoxTableCells<>(
                NOT_NULL, 
                (DesignerColumn col) -> col.notNull().get(),
                (Integer t) -> table.columns().get(t).name() == null);        
            return c;
        });
        
        Label notNullLabel = new Label("NN");
        notNullLabel.setTooltip(new Tooltip("Not Null"));
        notNullCol.setGraphic(notNullLabel);
        
        uniqueCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, Boolean> p) -> ((DesignerColumn)p.getValue()).unique());
        uniqueCol.setCellFactory((TableColumn<DesignerColumn, Boolean> p) -> {            
            CheckBoxTableCells<DesignerColumn, Boolean> c = new CheckBoxTableCells<>(
                UNIQUE, 
                (DesignerColumn col) -> col.unique().get(),
                (Integer t) -> table.columns().get(t).name() == null);        
            return c;
        });
        
        Label uniqueLabel = new Label("UQ");
        uniqueLabel.setTooltip(new Tooltip("Unique index"));
        uniqueCol.setGraphic(uniqueLabel);
        
        
        binaryCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).dataType());
        binaryCol.setCellFactory((TableColumn<DesignerColumn, String> p) -> {            
            CheckBoxTableCells<DesignerColumn, String> c = new CheckBoxTableCells<>(
                BINARY, 
                (DesignerColumn col) -> col.binary().get(),
                (Integer t) -> {
                    DesignerDataType dt = findDataTypeFunction.apply(table.columns().get(t).dataType().get());
                    return dt == null || !dt.binary;
                });
            return c;
        });
        
        Label binaryLabel = new Label("B");
        binaryLabel.setTooltip(new Tooltip("Is binary column"));
        binaryCol.setGraphic(binaryLabel);
        
        unsignedCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).dataType());
        unsignedCol.setCellFactory((TableColumn<DesignerColumn, String> p) -> {            
            CheckBoxTableCells<DesignerColumn, String> c = new CheckBoxTableCells<>(
                UNSIGNED, 
                (DesignerColumn col) -> col.unsigned().get(),
                (Integer t) -> {
                    DesignerDataType dt = findDataTypeFunction.apply(table.columns().get(t).dataType().get());
                    return dt == null || !dt.unsigned;
                });
            return c;
        });
        
        Label unsignedLabel = new Label("UN");
        unsignedLabel.setTooltip(new Tooltip("Unsigned data type"));
        unsignedCol.setGraphic(unsignedLabel);
        
        zeroFillCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).dataType());
        zeroFillCol.setCellFactory((TableColumn<DesignerColumn, String> p) -> {            
            CheckBoxTableCells<DesignerColumn, String> c = new CheckBoxTableCells<>(
                ZERO_FILL, 
                (DesignerColumn col) -> col.zeroFill().get(),
                (Integer t) -> {
                    DesignerDataType dt = findDataTypeFunction.apply(table.columns().get(t).dataType().get());
                    return dt == null || !dt.zeroFill;
                });     
            return c;
        });
        
        Label zeroFillLabel = new Label("ZF");
        zeroFillLabel.setTooltip(new Tooltip("Fill up values for thet column with 0's if it is numeric"));
        zeroFillCol.setGraphic(zeroFillLabel);
        
        autoIncrCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).dataType());
        autoIncrCol.setCellFactory((TableColumn<DesignerColumn, String> p) -> {            
            CheckBoxTableCells<DesignerColumn, String> c = new CheckBoxTableCells<>(
                AUTO_INCREMENTAL, 
                (DesignerColumn col) -> col.autoIncrement().get(),
                (Integer t) -> {
                    DesignerDataType dt = findDataTypeFunction.apply(table.columns().get(t).dataType().get());
                    return dt == null || !dt.autoIncremental;
                });
            return c;
        });
        
        Label autoIncrementLabel = new Label("AI");
        autoIncrementLabel.setTooltip(new Tooltip("Auto Incremental"));
        autoIncrCol.setGraphic(autoIncrementLabel);
        
        generatedCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, Boolean> p) -> ((DesignerColumn)p.getValue()).generated());
        generatedCol.setCellFactory((TableColumn<DesignerColumn, Boolean> p) -> {            
            CheckBoxTableCells<DesignerColumn, Boolean> c = new CheckBoxTableCells<>(
                GENERATED, 
                (DesignerColumn col) -> col.generated().get(),
                (Integer t) -> table.columns().get(t).name() == null);        
            return c;
        });
        
        Label generatedLabel = new Label("G");
        generatedLabel.setTooltip(new Tooltip("Generated Column"));
        generatedCol.setGraphic(generatedLabel);
        
        
        defaultCol.setCellValueFactory((TableColumn.CellDataFeatures<DesignerColumn, String> p) -> ((DesignerColumn)p.getValue()).defaultValue());
        defaultCol.setCellFactory(new Callback<TableColumn<DesignerColumn, String>, TableCell<DesignerColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        });
        defaultCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<DesignerColumn, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<DesignerColumn, String> event) {
                event.getRowValue().defaultValue().set(event.getNewValue());
                dataChanged();
                
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "." + event.getRowValue().name().get() + "' Column Defaults", parent.getCurrentDiagram());
            }
        });
        
       
        // </editor-fold>
        
        
        // <editor-fold desc=" Indexes ">
         
        indexesTableView.setEditable(true);
        
        Function<Object, DesignerIndex.IndexType> findIndexTypeFunction = (Object t) -> {            
            if (t != null) {
                String findText = t.toString().toUpperCase();                
                return DesignerIndex.IndexType.valueOf(findText);
            }

            return null;
        };
        
        BiFunction<Integer, DesignerIndex.IndexType, Boolean> checkIndexTypeAcceptedValue = (Integer index, DesignerIndex.IndexType type) -> {            
            // only 1 PRIMARY key available
            if (type == DesignerIndex.IndexType.PRIMARY) {
                for (DesignerIndex di: table.indexes()) {
                    if (di.isPrimaryKey()) {
                        return false;
                    }
                }
            }
            return true;
        };
        
        indexNameColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerIndex, String> p) -> ((DesignerIndex)p.getValue()).name());
        indexNameColumn.setCellFactory(new Callback<TableColumn<DesignerIndex, String>, TableCell<DesignerIndex, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell(new BiFunction<Integer, String, Boolean>() {
                    @Override
                    public Boolean apply(Integer index, String t) {
                        if (index < table.indexes().size()) {
                            return !table.indexes().get(index).isPrimaryKey() || "PRIMARY".equals(t);
                        } else {
                            return false;
                        }
                    }
                },getController().getSelectedConnectionSession().get());
            }
        });
        indexNameColumn.setOnEditCommit((TableColumn.CellEditEvent<DesignerIndex, String> event) -> {
            event.getRowValue().name().set(event.getRowValue().isPrimaryKey() ? "PRIMARY" : event.getNewValue());
            dataChanged();
        });
        
        final BiFunction<Integer, DesignerIndex.IndexType, Void> commitIndexValue = (Integer index, DesignerIndex.IndexType value) ->  {
            
            DesignerIndex di = table.indexes().get(index);
            processIndex(di, value);
            
            di.type().set(value); 
            if (di.isPrimaryKey()) {
                di.name().set("PRIMARY");
            }
            
            dataChanged();
            return null;
        };
        indexTypeColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerIndex, DesignerIndex.IndexType> p) -> ((DesignerIndex)p.getValue()).type());
        indexTypeColumn.setCellFactory(new Callback<TableColumn<DesignerIndex, DesignerIndex.IndexType>, TableCell<DesignerIndex, DesignerIndex.IndexType>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingComboBoxCell(FXCollections.observableArrayList(Arrays.asList(DesignerIndex.IndexType.values())), findIndexTypeFunction, checkIndexTypeAcceptedValue, commitIndexValue, false);
            }
        });
        indexTypeColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<DesignerIndex, DesignerIndex.IndexType>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<DesignerIndex, DesignerIndex.IndexType> event) {
                dataChanged();
            }
        });
        // </editor-fold>
        
        
        // <editor-fold desc=" Indexes columns ">
         
        
        Function<String, DesignerIndex.IndexOrder> findIndexOrderFunction = (String t) -> {            
            if (t != null && !t.isEmpty()) {
                String findText = t.trim().toUpperCase();
                return DesignerIndex.IndexOrder.valueOf(findText);
            }

            return null;
        };
        
        final BiFunction<Integer, DesignerIndex.IndexOrder, Void> commitIndexOrderValue = (Integer index, DesignerIndex.IndexOrder value) ->  {
            
            DesignerIndexColumn dic = indexColumnsTableView.getItems().get(index);
            dic.sort().set(value != null ? value.toString() : null); 
            
            dataChanged();
            
            return null;
        };
        
        indexColumnsTableView.setEditable(true);       
        
        icColumnColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerIndexColumn, Boolean> p) -> ((DesignerIndexColumn)p.getValue()).selected());
        icColumnColumn.setCellFactory(new Callback<TableColumn<DesignerIndexColumn, Boolean>, TableCell<DesignerIndexColumn, Boolean>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new IndexColumnCheckBoxTableCells();
            }
        });
        
        icPriorityColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerIndexColumn, String> p) -> ((DesignerIndexColumn)p.getValue()).order());
        icPriorityColumn.setCellFactory(new Callback<TableColumn<DesignerIndexColumn, String>, TableCell<DesignerIndexColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell(new BiFunction<Integer, String, Boolean>() {
                    @Override
                    public Boolean apply(Integer index, String t) {
                        return t.matches("\\d*");
                    }
                },getController().getSelectedConnectionSession().get());
            }
        });        
        
        icOrderColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerIndexColumn, String> p) -> ((DesignerIndexColumn)p.getValue()).sort());
        icOrderColumn.setCellFactory(new Callback<TableColumn<DesignerIndexColumn, String>, TableCell<DesignerIndexColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingComboBoxCell(FXCollections.observableArrayList(Arrays.asList(DesignerIndex.IndexOrder.values())), findIndexOrderFunction, null, commitIndexOrderValue, false);
            }
        });
        
        icLengthColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerIndexColumn, String> p) -> ((DesignerIndexColumn)p.getValue()).length());
        icLengthColumn.setCellFactory(new Callback<TableColumn<DesignerIndexColumn, String>, TableCell<DesignerIndexColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell(new BiFunction<Integer, String, Boolean>() {
                    @Override
                    public Boolean apply(Integer index, String t) {
                        return t.matches("\\d*");
                    }
                },getController().getSelectedConnectionSession().get());
            }
        });
        icLengthColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<DesignerIndexColumn, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<DesignerIndexColumn, String> event) {
                event.getRowValue().length().set(event.getNewValue());
                dataChanged();
            }
        });
        
        indexesTableView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends DesignerIndex> observable, DesignerIndex oldValue, DesignerIndex newValue) -> {
            initIndexColumnsTable(newValue);
        });
        
        alterTableTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            indexesTableView.getSelectionModel().clearSelection();
        });
       
        
        indexStorageType.setItems(FXCollections.observableArrayList(DesignerIndex.IndexStorageType.values()));
        // </editor-fold>
        
        
        // <editor-fold desc=" Foreign Keys ">
         
        foreignKeysTableView.setEditable(true);
        
        
        fkNameColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerForeignKey, String> p) -> ((DesignerForeignKey)p.getValue()).name());
        fkNameColumn.setCellFactory(new Callback<TableColumn<DesignerForeignKey, String>, TableCell<DesignerForeignKey, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        });
        fkNameColumn.setOnEditCommit((TableColumn.CellEditEvent<DesignerForeignKey, String> event) -> {
            event.getRowValue().name().set(event.getNewValue());
            dataChanged();
        });
        
        final BiFunction<Integer, DesignerTable, Void> commitFkRefTableValue = (Integer index, DesignerTable value) ->  {            
            DesignerForeignKey dfk = table.foreignKeys().get(index);
            dfk.referencedTable().set(value); 
            dfk.referencedColumns().clear();
            dfk.columns().clear();
            
            initForeignKeysReferencedColumnsTable(dfk);
            
            table.notifyForeignKeysChanged();
            dataChanged();
            
            return null;
        };
        
        final BiFunction<Integer, DesignerTable, Boolean> checkAcceptedReferencedTableValue = (Integer index, DesignerTable column) -> {
            if (table.foreignKeys().size() > index) {
                DesignerForeignKey dfk = table.foreignKeys().get(index);
                return dfk.name().get() != null && !dfk.name().get().isEmpty();
            } else {
                return false;
            }
        };
        
        final Function<String, DesignerTable> findTableSelectionValue = (String name) -> {
            for (DesignerTable t: designerDatabase.tables()) {
                if (t.name().get().equalsIgnoreCase(name)) {
                    return t;
                }
            }
            
            return null;
        };
        
        fkReferencedTableColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerForeignKey, DesignerTable> p) -> ((DesignerForeignKey)p.getValue()).referencedTable());
        fkReferencedTableColumn.setCellFactory(new Callback<TableColumn<DesignerForeignKey, DesignerTable>, TableCell<DesignerForeignKey, DesignerTable>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingComboBoxCell(designerDatabase.tables(), findTableSelectionValue, checkAcceptedReferencedTableValue, commitFkRefTableValue, false);
            }
        });
        
        
        foreignKeysTableView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends DesignerForeignKey> observable, DesignerForeignKey oldValue, DesignerForeignKey newValue) -> {
            initForeignKeysReferencedColumnsTable(newValue);
        });
        
        alterTableTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            indexesTableView.getSelectionModel().clearSelection();
            foreignKeysTableView.getSelectionModel().clearSelection();
        });
        
        
        
        fkReferencedColumnsTableView.setEditable(true);       
        
        fkColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerForeignColumn, Boolean> p) -> ((DesignerForeignColumn)p.getValue()).selected());
        fkColumn.setCellFactory(new Callback<TableColumn<DesignerForeignColumn, Boolean>, TableCell<DesignerForeignColumn, Boolean>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new ForeignColumnCheckBoxTableCells();
            }
        });
        
        final BiFunction<Integer, DesignerColumn, Void> commitFkRefColumnValue = (Integer index, DesignerColumn value) ->  {  
            if (index < fkReferencedColumnsTableView.getItems().size()) {
                DesignerForeignColumn dfk = fkReferencedColumnsTableView.getItems().get(index);
                dfk.referencedColumn().set(value); 
                
                table.notifyForeignKeysChanged();
                dataChanged();
            }
            return null;
        };
        
        BiFunction<Integer, DesignerColumn, Boolean> checkAcceptedReferencedColumnValue = (Integer index, DesignerColumn column) -> {            
            if (column != null && index < fkReferencedColumnsTableView.getItems().size()) {
                DesignerForeignColumn dfk = fkReferencedColumnsTableView.getItems().get(index);
                
                boolean error;
                // columns types must be the same
                // referenced column must have index
                if (!dfk.column.dataType().get().equals(column.dataType().get())) {
                    error = true;
                } else {
                    
                    boolean hasIndex = false;
                    for (DesignerIndex di: selectedForeignKey.referencedTable().get().indexes()) {
                        if (di.columns().contains(column)) {
                            hasIndex = true;
                            break;
                        }
                    }
                    
                    error = !hasIndex;
                }            
                    
                if (error) {
                    DialogHelper.showError(getController(),"Create Foreign Key", "Please selected column '" + column.name().get() + "' must be indexed if a compatible type for a Foreign Key is to be created.", null, getStage());
                    return false;
                }
            }
            

            return true;
        };
        
        
        final Function<String, DesignerColumn> findColumnSelectionValue = (String name) -> {
            
            List<DesignerColumn> list = selectedForeignKey != null && selectedForeignKey.referencedTable().get() != null ? selectedForeignKey.referencedTable().get().columns() : null;
            if (list != null) {
                for (DesignerColumn t: list) {
                    if (t.name().get().equalsIgnoreCase(name)) {
                        return t;
                    }
                }
            }
            
            return null;
        };
        
        fkReferencedColumn.setCellValueFactory((TableColumn.CellDataFeatures<DesignerForeignColumn, DesignerColumn> p) -> ((DesignerForeignColumn)p.getValue()).referencedColumn());
        fkReferencedColumn.setCellFactory(new Callback<TableColumn<DesignerForeignColumn, DesignerColumn>, TableCell<DesignerForeignColumn, DesignerColumn>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingComboBoxCell(FXCollections.observableArrayList(), findColumnSelectionValue, checkAcceptedReferencedColumnValue, commitFkRefColumnValue, false) {
                    @Override
                    public ObservableList getValues() {
                        return selectedForeignKey != null && selectedForeignKey.referencedTable().get() != null ? selectedForeignKey.referencedTable().get().columns() : null;
                    }                    
                };
            }
        });
        
        fkOnUpdate.setItems(FXCollections.observableArrayList(DesignerForeignKey.OnAction.values()));
        fkOnDelete.setItems(FXCollections.observableArrayList(DesignerForeignKey.OnAction.values()));
        
        // </editor-fold>
        
        
        
//        webView = new WebView();
//        webView.setContextMenuEnabled(false);
//        webView.getEngine().load(ConnectionModule.class.getResource("sqlView.html").toExternalForm());
//        webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
//            @Override
//            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
//                if (newValue == Worker.State.SUCCEEDED) {
//                    updatePreview();
//                }
//            }
//        });
//        sqlPreviewTab.setContent(webView);

        RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), null);
        SwingUtilities.invokeLater(() -> {
            builder.init();
            previewArea = builder.getTextArea();
            previewArea.setEditable(false);

            Platform.runLater(() -> {
                sqlPreviewTab.setContent(builder.getSwingNode());
            });
        });
    }
    
    private DesignerIndex selectedIndex;
    private void initIndexColumnsTable(DesignerIndex index) {
        
        indexColumnsTableView.getItems().clear();
        
        indexStorageType.setDisable(index == null);
        indexKeyBlockSize.setDisable(index == null);
        indexKeyParser.setDisable(index == null);
        indexComment.setDisable(index == null);
        
        if (selectedIndex != null) {
            indexStorageType.valueProperty().unbindBidirectional(selectedIndex.storageType());
            indexKeyBlockSize.textProperty().unbindBidirectional(selectedIndex.keyBlockSize());
            indexKeyParser.textProperty().unbindBidirectional(selectedIndex.parser());
            indexComment.textProperty().unbindBidirectional(selectedIndex.comment());
        }
        
        selectedIndex = index;
        
        if (index != null) {
            indexStorageType.valueProperty().bindBidirectional(index.storageType());
            indexKeyBlockSize.textProperty().bindBidirectional(index.keyBlockSize());
            indexKeyParser.textProperty().bindBidirectional(index.parser());
            indexComment.textProperty().bindBidirectional(index.comment());
        
            for (DesignerColumn dc: table.columns()) {
                
                DesignerIndexColumn dic = new DesignerIndexColumn();
                dic.column = dc;
                dic.selected.set(index.columns().contains(dc));
                
                Integer order = index.orderMap().get(dc);
                dic.order().set(order != null ? order.toString() : "");
                
                DesignerIndex.IndexOrder diOrder = index.sortMap().get(dc);
                dic.sort().set(diOrder != null ? diOrder.name() : "");
                
                Long length = index.lengthMap().get(dc);
                dic.length().set(length != null ? length.toString() : "");
                
                indexColumnsTableView.getItems().add(dic);
            }            
        }
    }
    
    
    private DesignerForeignKey selectedForeignKey;
    private void initForeignKeysReferencedColumnsTable(DesignerForeignKey foreignKey) {
        
        fkReferencedColumnsTableView.getItems().clear();
        
        
        fkOnUpdate.setDisable(foreignKey == null);
        fkOnDelete.setDisable(foreignKey == null);
        fkSkipSQLGeneration.setDisable(foreignKey == null);
        fkComment.setDisable(foreignKey == null);
        
        if (selectedForeignKey != null) {
            fkOnUpdate.valueProperty().unbindBidirectional(selectedForeignKey.onUpdate());
            fkOnDelete.valueProperty().unbindBidirectional(selectedForeignKey.onDelete());
            fkSkipSQLGeneration.selectedProperty().unbindBidirectional(selectedForeignKey.skipSqlGeneration());
            fkComment.textProperty().unbindBidirectional(selectedForeignKey.comment());
        }
        
        selectedForeignKey = foreignKey;
        
        if (foreignKey != null) {
                    
            fkOnUpdate.valueProperty().bindBidirectional(foreignKey.onUpdate());
            fkOnDelete.valueProperty().bindBidirectional(foreignKey.onDelete());
            fkSkipSQLGeneration.selectedProperty().bindBidirectional(foreignKey.skipSqlGeneration());
            fkComment.textProperty().bindBidirectional(foreignKey.comment());
            
            for (DesignerColumn dc: table.columns()) {
                
                DesignerForeignColumn dfc = new DesignerForeignColumn();
                dfc.column = dc;
                dfc.selected.set(foreignKey.columns().contains(dc));
                
                DesignerColumn referencedColumn = foreignKey.referencedColumns().get(dc);
                dfc.referencedColumn().set(referencedColumn);
                
                fkReferencedColumnsTableView.getItems().add(dfc);
            }            
        }
    }
    
    private void processIndex(DesignerIndex di, DesignerIndex.IndexType newType) {
        // we must check old an new value
        // for primary and unique
        
        // if old pk and was changed - need change columns pk flag
        if (di.isPrimaryKey() && newType != DesignerIndex.IndexType.PRIMARY) {
            for (DesignerColumn dc: di.columns()) {
                dc.primaryKey().set(false);
            }
        }
        // if old unique and was changed - need change columns unique flag
        else if (di.isUniqueKey() && newType != DesignerIndex.IndexType.UNIQUE) {
            for (DesignerColumn dc: di.columns()) {
                dc.unique().set(false);
            }
        }
        
        // second part check new value
        // if new value pk and old other - need set pk flag
        if (newType == DesignerIndex.IndexType.PRIMARY && !di.isPrimaryKey()) {
            for (DesignerColumn dc: di.columns()) {
                dc.primaryKey().set(true);
                dc.notNull().set(true);
            }
        }
        // if new value unique, columns count 1(ONLY if size == 1) and old other - need set unique flag
        else if (newType == DesignerIndex.IndexType.UNIQUE && di.columns().size() == 1 && !di.isUniqueKey()) {
            for (DesignerColumn dc: di.columns()) {
                dc.unique().set(true);
            }
        }
        
        dataChanged();
    }
    

    private boolean dataChanged = false;
    
    private DesignerTable sourceTable;
    private ErrDiagram errDiagram;
    private Tab tab;
    
    public void init(Tab tab, SchemaDesignerTabContentController parent, DBService service, ErrDiagram errDiagram, DesignerDatabase designerDatabase, DesignerTable sourceTable) {
        
        this.tab = tab;
        this.errDiagram = errDiagram;
        this.sourceTable = sourceTable;
        this.service = service;
        this.designerDatabase = designerDatabase;
        this.parent = parent;
        
        this.table = new DesignerTable();
        this.table.fromMapWithoutIndexesAndFks(sourceTable.toMap());
        
        Map<String, DesignerTable> map = new HashMap<>();
        designerDatabase.tables().stream().forEach(t -> map.put(t.uuid().get(), t));
        
        map.put(table.uuid().get(), this.table);
        this.table.fromMapIndexesAndFks(sourceTable.toMap(), map);
        
        tableNameField.textProperty().bindBidirectional(table.name());
        tableNameField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (tableNameField.isFocused()) {
                dataChanged = true;
            }
        });
        tableNameField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Name", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
                
        schemaField.textProperty().bind(table.schema());
        schemaField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (schemaField.isFocused()) {
                dataChanged = true;
            }
        });
        schemaField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Schema", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        
        collationComboBox.valueProperty().bindBidirectional(table.collation());
        collationComboBox.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            dataChanged();
            
            if (collationComboBox.isFocused()) {
                dataChanged = true;
            }
        });
        collationComboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Collation", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        engineComboBox.valueProperty().bindBidirectional(table.engine());
        engineComboBox.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            dataChanged();
            
            if (engineComboBox.isFocused()) {
                dataChanged = true;
            }
        });
        engineComboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Engine", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        commentsArea.textProperty().bindBidirectional(table.comments());
        commentsArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (commentsArea.isFocused()) {
                dataChanged = true;
            }
        });
        commentsArea.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Comment", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        columnsTableView.setItems(table.columns());        
        columnsTableView.itemsProperty().addListener((ObservableValue<? extends ObservableList<DesignerColumn>> observable, ObservableList<DesignerColumn> oldValue, ObservableList<DesignerColumn> newValue) -> {
            dataChanged();
        });
        
        indexesTableView.setItems(table.indexes());
        indexesTableView.itemsProperty().addListener((ObservableValue<? extends ObservableList<DesignerIndex>> observable, ObservableList<DesignerIndex> oldValue, ObservableList<DesignerIndex> newValue) -> {
            dataChanged();
        });
        
        foreignKeysTableView.setItems(table.foreignKeys());
        foreignKeysTableView.itemsProperty().addListener((ObservableValue<? extends ObservableList<DesignerForeignKey>> observable, ObservableList<DesignerForeignKey> oldValue, ObservableList<DesignerForeignKey> newValue) -> {
            dataChanged();
        });
        
        if (table.collation().get() == null) {
            table.collation().set("Schema Default");
        }
        
        
        autoIncrementTextField.textProperty().bindBidirectional(table.autoInc());
        autoIncrementTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (autoIncrementTextField.isFocused()) {
                dataChanged = true;
            }
        });
        autoIncrementTextField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Auto Increment", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        avgRowLengthTextField.textProperty().bindBidirectional(table.avgRowLength());
        avgRowLengthTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (avgRowLengthTextField.isFocused()) {
                dataChanged = true;
            }
        });
        avgRowLengthTextField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Avg Row Length", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        maxRowsTextField.textProperty().bindBidirectional(table.maxRows());
        maxRowsTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (maxRowsTextField.isFocused()) {
                dataChanged = true;
            }
        });
        maxRowsTextField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Max Rows", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        minRowsTextField.textProperty().bindBidirectional(table.minRows());
        minRowsTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            dataChanged();
            
            if (minRowsTextField.isFocused()) {
                dataChanged = true;
            }
        });
        minRowsTextField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Min Rows", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        checksumComboBox.valueProperty().bindBidirectional(table.checksum());
        checksumComboBox.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
            dataChanged();
            
            if (checksumComboBox.isFocused()) {
                dataChanged = true;
            }
        });
        checksumComboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Checksum", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        rowFormatComboBox.valueProperty().bindBidirectional(table.rowFormat());
        rowFormatComboBox.valueProperty().addListener((ObservableValue<? extends DesignerTable.RowFormat> observable, DesignerTable.RowFormat oldValue, DesignerTable.RowFormat newValue) -> {
            dataChanged();
            
            if (rowFormatComboBox.isFocused()) {
                dataChanged = true;
            }
        });
        rowFormatComboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Row Format", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        delayKeyWriteComboBox.valueProperty().bindBidirectional(table.delayKeyWrite());     
        delayKeyWriteComboBox.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
            dataChanged();
            
            if (delayKeyWriteComboBox.isFocused()) {
                dataChanged = true;
            }
        });
        delayKeyWriteComboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue && dataChanged) {
                parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + table.name().get() + "' Delay Key Write", parent.getCurrentDiagram());
                
                dataChanged = false;
            }
        });
        
        dataChanged();
    }

    
    class DesignerIndexColumn {
        private SimpleBooleanProperty selected = new SimpleBooleanProperty(false);
        
        private final SimpleStringProperty order = new SimpleStringProperty("1") {            
            @Override
            protected void invalidated() {
                super.invalidated();
                if (selectedIndex != null && selectedIndex.columns().contains(column)) {
                    selectedIndex.orderMap().put(column, get() != null && !get().isEmpty() ? Integer.parseInt(get()) : null);
                }
            }            
        };
        
        private final SimpleStringProperty sort = new SimpleStringProperty("ASC") {            
            @Override
            protected void invalidated() {
                super.invalidated();
                if (selectedIndex != null && selectedIndex.columns().contains(column)) {
                    selectedIndex.sortMap().put(column, get() != null && !get().isEmpty() ? DesignerIndex.IndexOrder.valueOf(get()) : null);
                }
            }            
        };
        
        private final SimpleStringProperty length = new SimpleStringProperty() {            
            @Override
            protected void invalidated() {
                super.invalidated();
                if (selectedIndex != null && selectedIndex.columns().contains(column)) {
                    selectedIndex.lengthMap().put(column, get() != null && !get().isEmpty() ? Long.valueOf(get()) : null);
                }
            }            
        };
        
        private DesignerColumn column;
        
        public SimpleBooleanProperty selected() {
            return selected;
        }
        
        public SimpleStringProperty order() {
            return order;
        }
        
        public SimpleStringProperty sort() {
            return sort;
        }
        
        public SimpleStringProperty length() {
            return length;
        }
    }
    
    
    class DesignerForeignColumn {
        private final SimpleBooleanProperty selected = new SimpleBooleanProperty(false);
        
        private final SimpleObjectProperty<DesignerColumn> referencedColumn = new SimpleObjectProperty<DesignerColumn>() {            
            @Override
            protected void invalidated() {
                super.invalidated();
                if (selectedForeignKey != null && selectedForeignKey.columns().contains(column)) {
                    selectedForeignKey.referencedColumns().put(column, get());
                }
            }            
        };
        
        
        private DesignerColumn column;
        
        public SimpleBooleanProperty selected() {
            return selected;
        }
        
        public SimpleObjectProperty<DesignerColumn> referencedColumn() {
            return referencedColumn;
        }
    }
    
    
    class CheckBoxTableCells<S, T> extends TableCell<S, T> {

        CheckBox checkBox;

        String attributeName;
        Function<Integer, Boolean> disabledFunction;
        Function<DesignerColumn, Boolean> valueFunction;

        public CheckBoxTableCells(String attributeName, Function<DesignerColumn, Boolean> valueFunction, Function<Integer, Boolean> disabledFunction) {
            this.attributeName = attributeName;
            this.disabledFunction = disabledFunction;
            this.valueFunction = valueFunction;
        }

        @Override
        public void updateItem(T item, boolean empty) {

            super.updateItem(item, empty);
            
            if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setFocusTraversable(false);
                    this.checkBox.setAlignment(Pos.CENTER);
                    setAlignment(Pos.CENTER);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        
                        //changed column to selectioncolumn
                        DesignerColumn col = table.columns().get(getIndex());
                        switch (attributeName) {
                            
                            case PRIMARY_KEY:
                                if (!Objects.equals(col.primaryKey().get(), newValue)) {
                                    col.primaryKey().set(newValue);
                                    
                                    primaryKeyChanged(col, newValue);
                                    
                                    if (newValue) {
                                        col.notNull().set(true);
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set '" + table.name().get() + "." + col.name().get() + "' PK", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset '" + table.name().get() + "." + col.name().get() + "' PK", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case NOT_NULL:
                                if (!Objects.equals(col.notNull().get(), newValue)) {
                                    col.notNull().set(newValue);
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set '" + table.name().get() + "." + col.name().get() + "' NOT NULL", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset '" + table.name().get() + "." + col.name().get() + "' NOT NULL", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case UNIQUE:
                                if (!Objects.equals(col.unique().get(), newValue)) {
                                    col.unique().set(newValue);
                                    
                                    uniqueChanged(col, newValue);
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Add Unique Index for '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Remove Unique Index for '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case BINARY:
                                if (!Objects.equals(col.binary(), newValue)) {
                                    col.binary().set(newValue);
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set BINARY of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset BINARY of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case UNSIGNED:
                                if (!Objects.equals(col.unsigned(), newValue)) {
                                    col.unsigned().set(newValue);
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set UNSIGNED of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset UNSIGNED of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case ZERO_FILL:
                                if (!Objects.equals(col.zeroFill(), newValue)) {
                                    col.zeroFill().set(newValue);
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set ZEROFILL of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset ZEROFILL of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case AUTO_INCREMENTAL:
                                if (!Objects.equals(col.autoIncrement(), newValue)) {
                                    if (newValue) {
                                        for (DesignerColumn c: table.columns()) {
                                            c.autoIncrement().set(false);
                                        }
                                    }
                                    
                                    col.autoIncrement().set(newValue);
                                    if (newValue) {
                                        col.generated().set(false);
                                    
                                        autoIncrCol.setVisible(false);
                                        autoIncrCol.setVisible(true);
                                        
                                        generatedCol.setVisible(false);
                                        generatedCol.setVisible(true);
                                    }
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set Auto Increment of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset Auto Increment of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                                
                            case GENERATED:
                                if (!Objects.equals(col.generated(), newValue)) {
                                    col.generated().set(newValue);
                                    if (newValue) {
                                        col.autoIncrement().set(false);
                                        
                                        autoIncrCol.setVisible(false);
                                        autoIncrCol.setVisible(true);
                                    }
                                    
                                    if (newValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Set Generated of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    } else if (oldValue != null && oldValue) {
                                        parent.getDesignerDiagramHistory().putHistoryPoint("Unset Generated of '" + table.name().get() + "." + col.name().get() + "'", parent.getCurrentDiagram());
                                    }
                                }
                                break;
                        }
                        
                        dataChanged();
                    });
                }

                if (valueFunction != null) {
                    checkBox.setSelected(valueFunction.apply(table.columns().get(getIndex())));
                } else {
                    checkBox.setSelected(item != null ? (Boolean)item : Boolean.FALSE);
                }
                
                setGraphic(checkBox);
                
                if (disabledFunction != null ) {
                    boolean disabled = disabledFunction.apply(getIndex());
                    setDisable(disabled);
                    
                    if (disabled && checkBox != null) {
                        checkBox.setSelected(false);
                    }
                }
                
            } else {
                setGraphic(null);
            }
        }
    }
        
        
    private class IndexColumnCheckBoxTableCells extends TableCell<DesignerIndexColumn, Boolean> {

        CheckBox checkBox;

        public IndexColumnCheckBoxTableCells() {
        }

        
        @Override
        public void updateItem(Boolean item, boolean empty) {

            super.updateItem(item, empty);
            
            if (getTableRow() != null && getIndex() >= 0 && getTableView().getItems() != null && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setFocusTraversable(false);
                    setAlignment(Pos.CENTER_LEFT);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        
                        // add or remove column to index columns
                        DesignerIndexColumn dic = getTableView().getItems().get(getIndex());
                        if (newValue != null && newValue) {
                            if (!selectedIndex.columns().contains(dic.column)) {
                                selectedIndex.columns().add(dic.column);
                                
                                dic.order.set("" + selectedIndex.columns().size());
                                dic.sort.set(DesignerIndex.IndexOrder.ASC.name());
                                
                                // if primary - need mark column
                                if (selectedIndex.isPrimaryKey()) {
                                    dic.column.primaryKey().set(true);
                                }
                            }
                        } else {
                            if (selectedIndex.columns().contains(dic.column)) {
                                selectedIndex.columns().remove(dic.column);
                                selectedIndex.orderMap().remove(dic.column);
                                selectedIndex.sortMap().remove(dic.column);
                                selectedIndex.lengthMap().remove(dic.column);
                                
                                if (selectedIndex.isPrimaryKey()) {
                                    dic.column.primaryKey().set(false);
                                }
                            }
                        }
                        
                        // check all unique columns
                        checkUniqueColumns();
                        
                        dataChanged();
                    });
                }
                
                DesignerIndexColumn dic = getTableView().getItems().get(getIndex());
                
                checkBox.setText(dic.column.name().get());
                checkBox.setSelected(item);
                setGraphic(checkBox);     
                
            } else {
                setGraphic(null);
            }
        }
        
        private void checkUniqueColumns() {
            for (DesignerColumn dc: table.columns()) {
                boolean unique = false;
                for (DesignerIndex di: table.indexes()) {
                    if (di.isUniqueKey() && di.columns().size() == 1 && di.columns().contains(dc)) {
                        unique = true;
                    }
                }

                dc.unique().set(unique);
            }
        }
    }
    
    
    private class ForeignColumnCheckBoxTableCells extends TableCell<DesignerForeignColumn, Boolean> {

        CheckBox checkBox;

        public ForeignColumnCheckBoxTableCells() {
        }

        
        @Override
        public void updateItem(Boolean item, boolean empty) {

            super.updateItem(item, empty);
            
            if (getTableRow() != null && getIndex() >= 0 && getTableView().getItems() != null && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setFocusTraversable(false);
                    setAlignment(Pos.CENTER_LEFT);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        
                        // add or remove column to index columns
                        DesignerForeignColumn dic = getTableView().getItems().get(getIndex());
                        if (newValue != null && newValue) {
                            if (!selectedForeignKey.columns().contains(dic.column)) {
                                selectedForeignKey.columns().add(dic.column);
                                
                                checkForeignKeysIndexes(true);
                            }
                        } else {
                            if (selectedForeignKey.columns().contains(dic.column)) {
                                selectedForeignKey.columns().remove(dic.column);
                                selectedForeignKey.referencedColumns().remove(dic.column);
                                
                                checkForeignKeysIndexes(false);
                            }
                        }
                        
                        table.notifyForeignKeysChanged();
                        dataChanged();
                    });
                }
                
                DesignerForeignColumn dic = getTableView().getItems().get(getIndex());
                
                checkBox.setText(dic.column.name().get());
                checkBox.setWrapText(true);
                checkBox.setSelected(item);
                setGraphic(checkBox);     
                
            } else {
                setGraphic(null);
            }
        }
        
        private void checkForeignKeysIndexes(boolean added) {
            
            if (added) {
                if (!selectedForeignKey.columns().isEmpty()) {
                    
                    //try found index with current foreign key
                    for (DesignerIndex di: table.indexes()) {
                        if (di.foreignKey().get() != null && di.foreignKey().get().equals(selectedForeignKey)) {
                            for (DesignerColumn dc: selectedForeignKey.columns()) {
                                if (!dc.primaryKey().get()) {
                                    if (!di.columns().contains(dc)) {
                                        di.addColumn(dc);
                                    }
                                }
                            }
                            
                            return;
                        }
                    }
                    
                    // else try found with out ref on fk
                    for (DesignerIndex di: table.indexes()) {
                        if (!di.columns().isEmpty()) {
                            boolean f = true;
                            for (DesignerColumn dc: selectedForeignKey.columns()) {
                                if (!di.columns().contains(dc)) {
                                    f = false;
                                    break;
                                }
                            }

                            if (f) {
                                return;
                            }
                        }
                    }

                    // if not found need create index
                    DesignerIndex idx = new DesignerIndex();

                    String name = selectedForeignKey.name().get();

                    int index = 1;
                    String tempName = name;
                    boolean notFoundUniquName = true;
                    while (notFoundUniquName) {
                        notFoundUniquName = false;
                        for (DesignerIndex di: table.indexes()) {
                            if (di.name().get() != null && di.name().get().toUpperCase().equals(tempName.toUpperCase() + "_IDX")) {
                                notFoundUniquName = true;
                                tempName = name + "_" + index;
                                index++;
                            }
                        }
                    }

                    idx.name().set(name + (index > 1 ? "_" + (index - 1) : "") + "_idx");
                    idx.foreignKey().set(selectedForeignKey);

                    for (DesignerColumn dc: selectedForeignKey.columns()) {
                        if (!dc.primaryKey().get()) {
                            idx.addColumn(dc);
                        }
                    }

                    table.indexes().add(idx);
                }
            } else {
                for (DesignerIndex di: table.indexes()) {
                    if (di.foreignKey().get() != null && di.foreignKey().get().equals(selectedForeignKey)) {
                        for (DesignerColumn dc: di.columns()) {
                            if (!selectedForeignKey.columns().contains(dc)) {
                                di.removeColumn(dc);                                
                                
                                if (di.columns().isEmpty()) {
                                    table.indexes().remove(di);
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    private synchronized void dataChanged() {
        if (service != null) {
            actualSQL = MysqlDBService.getNewSQL(SchemaDesignerModule.convertDesignerTableToTable(service, table));
            actualSQL = actualSQL.trim();
        } else {
            actualSQL = "";
        }

        if (!actualSQL.isEmpty()) {
            
            SqlFormatter formatter = new SqlFormatter();
            
            PreferenceData pd = preferenceService.getPreference();
            if (!pd.isQueriesUsingBackquote()) {
                actualSQL = actualSQL.replaceAll("`", "");
            }
            
            actualSQL = formatter.formatSql(actualSQL, pd.getTab(), pd.getIndentString(), pd.getStyle());
            
            MainUI.usedQueryFormat();

            updatePreview();
        }
    }
    
    private void updatePreview() {
        SwingUtilities.invokeLater(() -> {
            previewArea.setText(actualSQL);
        });
    }
    
    
           
}
