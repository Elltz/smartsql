/*
 * (C) Copyright 2012 JavaFXGraph (http://code.google.com/p/javafxgraph/).
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3.0 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package np.com.ngopal.smart.sql.ui.fxgraph;

import java.util.function.Function;
import javafx.scene.Node;
import javafx.scene.layout.Region;

public class FXNodeBuilder {

    private FXGraph graph;
    private Region node;
    private double positionX;
    private double positionY;
    private Function<Node, Boolean> canChageHeight;
    private Function<Node, Boolean> canChangeCorners;
    private Function<Node, String> color;

    public FXNodeBuilder(FXGraph aGraph) {
        graph = aGraph;
    }

    public FXNodeBuilder node(Region aNode) {
        node = aNode;
        return this;
    }
    
    public FXNodeBuilder canChangeHeigt(Function<Node, Boolean> canChageHeight) {
        this.canChageHeight = canChageHeight;
        return this;
    }
    
    public FXNodeBuilder canChangeCorners(Function<Node, Boolean> canChangeCorners) {
        this.canChangeCorners = canChangeCorners;
        return this;
    }
    
    public FXNodeBuilder color(Function<Node, String> color) {
        this.color = color;
        return this;
    }

    public FXNodeBuilder x(double aX) {
        positionX = aX;
        return this;
    }

    public FXNodeBuilder y(double aY) {
        positionY = aY;
        return this;
    }

    public FXNode build() {
        FXNode theNode = new FXNode(graph, node, canChageHeight, canChangeCorners, color);
        theNode.setPosition(positionX, positionY);

        graph.addNode(theNode);

        return theNode;
    }
}
