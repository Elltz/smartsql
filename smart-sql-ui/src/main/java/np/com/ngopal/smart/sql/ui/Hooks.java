/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui;

import lombok.AllArgsConstructor;
import lombok.Getter;
import np.com.ngopal.smart.sql.Hookable;

/**
 * This class is for hooking item in ContextMenu of Left handside panel. So
 * DATABASE is the treeitem , ROOT is the topmost treenode of left hand side.
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class Hooks {

    @Getter
    @AllArgsConstructor
    public enum QueryBrowser implements Hookable {

        ROOT("root"), DATABASE("database"), TABLES("tables"), COLUMNS("columns"), COLUMN_ITEM("column"), INDEXES("indexes"), INDEX_ITEM("index"), VIEWS("views"), VIEW_ITEM("view"), FUNCTIONS("functions"), FUNCTION_ITEM("function"), TABLE_ITEM("table"),
        TRIGGERS("Triggers"), TRIGGER_ITEM("Trigger"), STORED_PROCS("Stored Procs"), STORED_PROC_ITEM("Stored Proc"), EVENTS("Events"), EVENT_ITEM("Event");

        private String hookValue;

    }

}
