package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class DatabaseCompareUtils {

    public static SyncCompareResult compare(SyncCompareInfo compareInfo) {
        SyncCompareResult result = new SyncCompareResult(compareInfo.getSourceDatabase(), compareInfo.getSource().getAddress(), compareInfo.getTargetDatabase(), compareInfo.getTarget().getAddress());

        if (compareInfo.isFilterTables()) {
            
            Set<String> allTableNames = new HashSet<>();
            // 1.1 Tables
            Map<String, DBTable> sourceTables = new HashMap<>();
            if (compareInfo.getSourceDatabase().getTables() != null) {

                for (DBTable t: compareInfo.getSourceDatabase().getTables()) {                        
                    String name = t.getName().toLowerCase();
                    sourceTables.put(name, t);
                    allTableNames.add(name);
                }
            }

            Map<String, DBTable> targetTables = new HashMap<>();
            if (compareInfo.getTargetDatabase().getTables() != null) {
                for (DBTable t: compareInfo.getTargetDatabase().getTables()) {
                    String name = t.getName().toLowerCase();
                    targetTables.put(name, t);
                    allTableNames.add(name);
                }
            }

            for (String name: allTableNames) {

                SyncCompareResultItem resultItem = new SyncCompareResultItem();

                DBTable sourceTable = sourceTables.get(name);
                DBTable targetTable = targetTables.get(name);
                
                String DROP_TABLE = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DROP_TABLE);
                
                if (sourceTable != null && targetTable == null) {
                    // only in source - NEW                     
                    resultItem.setOperation(SyncCompareResultItem.Operation.CREATE);
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceTable);
                    resultItem.setType(SyncCompareResultItem.Type.TABLE);
                    resultItem.setCreateSourceQuery(MysqlDBService.getNewSQL(sourceTable, false));
                    resultItem.setDropSourceQuery(String.format(DROP_TABLE, sourceTable.getName()));

                    result.addOnlyInSource(resultItem); 

                } else if (sourceTable == null && targetTable != null) {
                    // only in target - DROP
                    resultItem.setOperation(SyncCompareResultItem.Operation.DROP);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetTable);
                    resultItem.setType(SyncCompareResultItem.Type.TABLE);
                    resultItem.setCreateTargetQuery(MysqlDBService.getNewSQL(targetTable, false));
                    resultItem.setDropTargetQuery(String.format(DROP_TABLE, targetTable.getName()));
                    
                    result.addOnlyInTarget(resultItem); 

                } else if (sourceTable != null && targetTable != null){
                    // in both - UPDATE or EQUAL
                    
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceTable);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetTable);
                    resultItem.setType(SyncCompareResultItem.Type.TABLE);
                    resultItem.setCreateSourceQuery(MysqlDBService.getNewSQL(sourceTable, false));
                    resultItem.setDropSourceQuery(String.format(DROP_TABLE, sourceTable.getName()));
                    resultItem.setCreateTargetQuery(MysqlDBService.getNewSQL(targetTable, false));
                    resultItem.setDropTargetQuery(String.format(DROP_TABLE, targetTable.getName()));
                    
                    Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diff = compareTables(compareInfo, sourceTables.get(name), targetTables.get(name));
                    
                    if (diff.isEmpty()) {
                        resultItem.setOperation(SyncCompareResultItem.Operation.NONE);
                        result.addEquals(resultItem); 
                        
                    } else {
                        resultItem.setOperation(SyncCompareResultItem.Operation.UPDATE);
                        resultItem.setDiffs(diff);
                        
                        result.addDifferent(resultItem); 
                    }                   
                }
            }
        }
        
        if (compareInfo.isFilterViews()) {
            Set<String> allViewNames = new HashSet<>();
            // 1.2 Views
            Map<String, View> sourceViews = new HashMap<>();
            if (compareInfo.getSourceDatabase().getViews() != null) {

                for (View t: compareInfo.getSourceDatabase().getViews()) {                        
                    String name = t.getName().toLowerCase();
                    sourceViews.put(name, t);
                    allViewNames.add(name);
                }
            }

            Map<String, View> targetViews = new HashMap<>();
            if (compareInfo.getTargetDatabase().getViews() != null) {
                for (View t: compareInfo.getTargetDatabase().getViews()) {
                    String name = t.getName().toLowerCase();
                    targetViews.put(name, t);
                    allViewNames.add(name);
                }
            }

            for (String name: allViewNames) {

                SyncCompareResultItem resultItem = new SyncCompareResultItem();

                View sourceView = sourceViews.get(name);
                View targetView = targetViews.get(name);
                
                String DROP_VIEW = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DROP_VIEW);
                
                if (sourceView != null && targetView == null) {
                    // only in source - NEW                     
                    resultItem.setOperation(SyncCompareResultItem.Operation.CREATE);
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceView);
                    resultItem.setType(SyncCompareResultItem.Type.VIEW);
                    resultItem.setCreateSourceQuery(sourceView.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_VIEW, sourceView.getName()));

                    result.addOnlyInSource(resultItem); 

                } else if (sourceView == null && targetView != null) {
                    // only in target - DROP
                    resultItem.setOperation(SyncCompareResultItem.Operation.DROP);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetView);
                    resultItem.setType(SyncCompareResultItem.Type.VIEW);
                    resultItem.setCreateTargetQuery(targetView.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_VIEW, targetView.getName()));

                    result.addOnlyInTarget(resultItem); 

                } else if (sourceView != null && targetView != null) {
                    // in both - UPDATE or EQUAL
                    
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceView);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetView);
                    resultItem.setType(SyncCompareResultItem.Type.VIEW);
                    resultItem.setCreateSourceQuery(sourceView.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_VIEW, sourceView.getName()));
                    resultItem.setCreateTargetQuery(targetView.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_VIEW, targetView.getName()));
                    
                    Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diff = compareViews(compareInfo, sourceViews.get(name), targetViews.get(name));
                    
                    if (diff.isEmpty()) {
                        resultItem.setOperation(SyncCompareResultItem.Operation.NONE);
                        result.addEquals(resultItem); 
                        
                    } else {
                        resultItem.setOperation(SyncCompareResultItem.Operation.UPDATE);
                        resultItem.setDiffs(diff);
                        
                        result.addDifferent(resultItem); 
                    }                    
                }
            }
        }
        
        
        if (compareInfo.isFilterFunctions()) {
            Set<String> allFunctionsNames = new HashSet<>();
            // 1.3 Functions
            Map<String, Function> sourceFunctions = new HashMap<>();
            if (compareInfo.getSourceDatabase().getFunctions() != null) {

                for (Function t: compareInfo.getSourceDatabase().getFunctions()) {                        
                    String name = t.getName().toLowerCase();
                    sourceFunctions.put(name, t);
                    allFunctionsNames.add(name);
                }
            }

            Map<String, Function> targetFunctions = new HashMap<>();
            if (compareInfo.getTargetDatabase().getFunctions() != null) {
                for (Function t: compareInfo.getTargetDatabase().getFunctions()) {
                    String name = t.getName().toLowerCase();
                    targetFunctions.put(name, t);
                    allFunctionsNames.add(name);
                }
            }

            for (String name: allFunctionsNames) {

                SyncCompareResultItem resultItem = new SyncCompareResultItem();

                Function sourceFunction = sourceFunctions.get(name);
                Function targetFunction = targetFunctions.get(name);
                
                String DROP_FUNCTION = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DROP_FUNCTION);
                
                if (sourceFunction != null && targetFunction == null) {
                    // only in source - NEW                     
                    resultItem.setOperation(SyncCompareResultItem.Operation.CREATE);
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceFunction);
                    resultItem.setType(SyncCompareResultItem.Type.FUNCTION);
                    resultItem.setCreateSourceQuery(sourceFunction.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_FUNCTION, sourceFunction.getName()));

                    result.addOnlyInSource(resultItem); 

                } else if (sourceFunction == null && targetFunction != null) {
                    // only in target - DROP
                    resultItem.setOperation(SyncCompareResultItem.Operation.DROP);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetFunction);
                    resultItem.setType(SyncCompareResultItem.Type.FUNCTION);
                    resultItem.setCreateTargetQuery(targetFunction.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_FUNCTION, targetFunction.getName()));

                    result.addOnlyInTarget(resultItem); 

                } else if (sourceFunction != null && targetFunction != null) {
                    // in both - UPDATE or EQUAL
                    
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceFunction);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetFunction);
                    resultItem.setType(SyncCompareResultItem.Type.FUNCTION);
                    resultItem.setCreateSourceQuery(sourceFunction.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_FUNCTION, sourceFunction.getName()));
                    resultItem.setCreateTargetQuery(targetFunction.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_FUNCTION, targetFunction.getName()));
                    
                    Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diff = compareFunctions(compareInfo, sourceFunctions.get(name), targetFunctions.get(name));
                    
                    if (diff.isEmpty()) {
                        resultItem.setOperation(SyncCompareResultItem.Operation.NONE);
                        result.addEquals(resultItem); 
                        
                    } else {
                        resultItem.setOperation(SyncCompareResultItem.Operation.UPDATE);
                        resultItem.setDiffs(diff);
                        
                        result.addDifferent(resultItem); 
                    }                    
                }
            }
        }
        
        if (compareInfo.isFilterProcedures()) {
            Set<String> allProceduresNames = new HashSet<>();
            // 1.3 Procedures
            Map<String, StoredProc> sourceProcedures = new HashMap<>();
            if (compareInfo.getSourceDatabase().getStoredProc() != null) {

                for (StoredProc t: compareInfo.getSourceDatabase().getStoredProc()) {                        
                    String name = t.getName().toLowerCase();
                    sourceProcedures.put(name, t);
                    allProceduresNames.add(name);
                }
            }

            Map<String, StoredProc> targetProcedures = new HashMap<>();
            if (compareInfo.getTargetDatabase().getStoredProc() != null) {
                for (StoredProc t: compareInfo.getTargetDatabase().getStoredProc()) {
                    String name = t.getName().toLowerCase();
                    targetProcedures.put(name, t);
                    allProceduresNames.add(name);
                }
            }

            for (String name: allProceduresNames) {

                SyncCompareResultItem resultItem = new SyncCompareResultItem();

                StoredProc sourceProcedure = sourceProcedures.get(name);
                StoredProc targetProcedure = targetProcedures.get(name);
                
                String DROP_PROCEDURE = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DROP_STORED_PROCEDURE);
                
                if (sourceProcedure != null && targetProcedure == null) {
                    // only in source - NEW                     
                    resultItem.setOperation(SyncCompareResultItem.Operation.CREATE);
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceProcedure);
                    resultItem.setType(SyncCompareResultItem.Type.PROCEDURE);
                    resultItem.setCreateSourceQuery(sourceProcedure.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_PROCEDURE, sourceProcedure.getName()));

                    result.addOnlyInSource(resultItem); 

                } else if (sourceProcedure == null && targetProcedure != null) {
                    // only in target - DROP
                    resultItem.setOperation(SyncCompareResultItem.Operation.DROP);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetProcedure);
                    resultItem.setType(SyncCompareResultItem.Type.PROCEDURE);
                    resultItem.setCreateTargetQuery(targetProcedure.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_PROCEDURE, targetProcedure.getName()));

                    result.addOnlyInTarget(resultItem); 

                } else if (sourceProcedure != null && targetProcedure != null) {
                    // in both - UPDATE or EQUAL
                    
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceProcedure);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetProcedure);
                    resultItem.setType(SyncCompareResultItem.Type.PROCEDURE);
                    resultItem.setCreateSourceQuery(sourceProcedure.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_PROCEDURE, sourceProcedure.getName()));
                    resultItem.setCreateTargetQuery(targetProcedure.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_PROCEDURE, targetProcedure.getName()));
                    
                    Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diff = compareProcedures(compareInfo, sourceProcedures.get(name), targetProcedures.get(name));
                    
                    if (diff.isEmpty()) {
                        resultItem.setOperation(SyncCompareResultItem.Operation.NONE);
                        result.addEquals(resultItem); 
                        
                    } else {
                        resultItem.setOperation(SyncCompareResultItem.Operation.UPDATE);
                        resultItem.setDiffs(diff);
                        
                        result.addDifferent(resultItem); 
                    }                    
                }
            }
        }
        
        if (compareInfo.isFilterTriggers()) {
            Set<String> allTriggersNames = new HashSet<>();
            // 1.4 Triggers
            Map<String, Trigger> sourceTriggers = new HashMap<>();
            if (compareInfo.getSourceDatabase().getTriggers() != null) {

                for (Trigger t: compareInfo.getSourceDatabase().getTriggers()) {                        
                    String name = t.getName().toLowerCase();
                    sourceTriggers.put(name, t);
                    allTriggersNames.add(name);
                }
            }

            Map<String, Trigger> targetTriggers = new HashMap<>();
            if (compareInfo.getTargetDatabase().getTriggers() != null) {
                for (Trigger t: compareInfo.getTargetDatabase().getTriggers()) {
                    String name = t.getName().toLowerCase();
                    targetTriggers.put(name, t);
                    allTriggersNames.add(name);
                }
            }

            for (String name: allTriggersNames) {

                SyncCompareResultItem resultItem = new SyncCompareResultItem();

                Trigger sourceTrigger = sourceTriggers.get(name);
                Trigger targetTrigger = targetTriggers.get(name);
                
                String DROP_TRIGGER = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DROP_TRIGGER);
                
                if (sourceTrigger != null && targetTrigger == null) {
                    // only in source - NEW                     
                    resultItem.setOperation(SyncCompareResultItem.Operation.CREATE);
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceTrigger);
                    resultItem.setType(SyncCompareResultItem.Type.TRIGGER);
                    resultItem.setCreateSourceQuery(sourceTrigger.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_TRIGGER, sourceTrigger.getName()));
                    
                    result.addOnlyInSource(resultItem); 

                } else if (sourceTrigger == null && targetTrigger != null) {
                    // only in target - DROP
                    resultItem.setOperation(SyncCompareResultItem.Operation.DROP);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetTrigger);
                    resultItem.setType(SyncCompareResultItem.Type.TRIGGER);
                    resultItem.setCreateTargetQuery(targetTrigger.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_TRIGGER, targetTrigger.getName()));

                    result.addOnlyInTarget(resultItem); 

                } else if (sourceTrigger != null && targetTrigger != null) {
                    // in both - UPDATE or EQUAL
                    
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceTrigger);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetTrigger);
                    resultItem.setType(SyncCompareResultItem.Type.TRIGGER);
                    resultItem.setCreateSourceQuery(sourceTrigger.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_TRIGGER, sourceTrigger.getName()));
                    resultItem.setCreateTargetQuery(targetTrigger.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_TRIGGER, targetTrigger.getName()));
                    
                    Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diff = compareTriggers(compareInfo, sourceTriggers.get(name), targetTriggers.get(name));
                    
                    if (diff.isEmpty()) {
                        resultItem.setOperation(SyncCompareResultItem.Operation.NONE);
                        result.addEquals(resultItem); 
                        
                    } else {
                        resultItem.setOperation(SyncCompareResultItem.Operation.UPDATE);
                        resultItem.setDiffs(diff);
                        
                        result.addDifferent(resultItem); 
                    }                    
                }
            }
        }
        
        
        if (compareInfo.isFilterEvents()) {
            Set<String> allEventsNames = new HashSet<>();
            // 1.5 Events
            Map<String, Event> sourceEvents = new HashMap<>();
            if (compareInfo.getSourceDatabase().getEvents() != null) {

                for (Event t: compareInfo.getSourceDatabase().getEvents()) {                        
                    String name = t.getName().toLowerCase();
                    sourceEvents.put(name, t);
                    allEventsNames.add(name);
                }
            }

            Map<String, Event> targetEvents = new HashMap<>();
            if (compareInfo.getTargetDatabase().getEvents() != null) {
                for (Event t: compareInfo.getTargetDatabase().getEvents()) {
                    String name = t.getName().toLowerCase();
                    targetEvents.put(name, t);
                    allEventsNames.add(name);
                }
            }

            for (String name: allEventsNames) {

                SyncCompareResultItem resultItem = new SyncCompareResultItem();

                Event sourceEvent = sourceEvents.get(name);
                Event targetEvent = targetEvents.get(name);
                
                String DROP_EVENT = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__DROP_EVENT);
                                
                if (sourceEvent != null && targetEvent == null) {
                    // only in source - NEW                     
                    resultItem.setOperation(SyncCompareResultItem.Operation.CREATE);
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceEvent);
                    resultItem.setType(SyncCompareResultItem.Type.EVENT);
                    resultItem.setCreateSourceQuery(sourceEvent.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_EVENT, sourceEvent.getName()));
                    
                    result.addOnlyInSource(resultItem); 

                } else if (sourceEvent == null && targetEvent != null) {
                    // only in target - DROP
                    resultItem.setOperation(SyncCompareResultItem.Operation.DROP);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetEvent);
                    resultItem.setType(SyncCompareResultItem.Type.EVENT);
                    resultItem.setCreateTargetQuery(targetEvent.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_EVENT, targetEvent.getName()));

                    result.addOnlyInTarget(resultItem); 

                } else if (sourceEvent != null && targetEvent != null) {
                    // in both - UPDATE or EQUAL
                    
                    resultItem.setSource(name);
                    resultItem.setSourceObject(sourceEvent);
                    resultItem.setTarget(name);
                    resultItem.setTargetObject(targetEvent);
                    resultItem.setType(SyncCompareResultItem.Type.EVENT);
                    resultItem.setCreateSourceQuery(sourceEvent.getCreateScript());
                    resultItem.setDropSourceQuery(String.format(DROP_EVENT, sourceEvent.getName()));
                    resultItem.setCreateTargetQuery(targetEvent.getCreateScript());
                    resultItem.setDropTargetQuery(String.format(DROP_EVENT, targetEvent.getName()));
                    
                    Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> diff = compareEvents(compareInfo, sourceEvents.get(name), targetEvents.get(name));
                    
                    if (diff.isEmpty()) {
                        resultItem.setOperation(SyncCompareResultItem.Operation.NONE);
                        result.addEquals(resultItem); 
                        
                    } else {
                        resultItem.setOperation(SyncCompareResultItem.Operation.UPDATE);
                        resultItem.setDiffs(diff);
                        
                        result.addDifferent(resultItem); 
                    }                    
                }
            }
        }
        
        return result;
    }
    
    private static Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> compareViews(SyncCompareInfo compareInfo, View source, View target) {
        Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result = new HashMap<>();
        
        different(compareInfo, String.class, SyncCompareResulDiff.Type.FULL, source.getCreateScript(), target.getCreateScript(), result);
        
        return result;
    }
    
    
    private static Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> compareFunctions(SyncCompareInfo compareInfo, Function source, Function target) {
        Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result = new HashMap<>();
        
        different(compareInfo, String.class, SyncCompareResulDiff.Type.FULL, source.getCreateScript(), target.getCreateScript(), result);
        
        return result;
    }
        
    private static Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> compareProcedures(SyncCompareInfo compareInfo, StoredProc source, StoredProc target) {
        Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result = new HashMap<>();
        
        different(compareInfo, String.class, SyncCompareResulDiff.Type.FULL, source.getCreateScript(), target.getCreateScript(), result);
        
        return result;
    }
    
    private static Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> compareTriggers(SyncCompareInfo compareInfo, Trigger source, Trigger target) {
        Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result = new HashMap<>();
        
        different(compareInfo, String.class, SyncCompareResulDiff.Type.FULL, source.getCreateScript(), target.getCreateScript(), result);
        
        return result;
    }
    
    private static Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> compareEvents(SyncCompareInfo compareInfo, Event source, Event target) {
        Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result = new HashMap<>();
        
        different(compareInfo, String.class, SyncCompareResulDiff.Type.FULL, source.getCreateScript(), target.getCreateScript(), result);
        
        return result;
    }
    
    private static Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> compareTables(SyncCompareInfo compareInfo, DBTable source, DBTable target) {
        Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result = new HashMap<>();
        
        if (!compareInfo.isOptionIgnoreTableEngine()) {
            different(compareInfo, EngineType.class, SyncCompareResulDiff.Type.ENGINE, source.getEngineType(), target.getEngineType(), result);
        }
        
        if (!compareInfo.isOptionIgnoreAverageRow()) {
            different(compareInfo, Number.class, SyncCompareResulDiff.Type.AVG_ROW_LENGTH, source.getAdvanced().getAvgRowLength(), target.getAdvanced().getAvgRowLength(), result);
        }
        
        different(compareInfo, String.class, SyncCompareResulDiff.Type.CHARSET, source.getCharacterSet(), target.getCharacterSet(), result);
        different(compareInfo, String.class, SyncCompareResulDiff.Type.COLLATE, source.getCollation(), target.getCollation(), result);
        
        
        Set<String> columns = new LinkedHashSet<>();
        Map<String, Column> sourceColumns = new LinkedHashMap<>();
        Map<String, Column> targetColumns = new LinkedHashMap<>();

        if (source.getColumns() != null) {
            for (Column c: source.getColumns()) {
                sourceColumns.put(c.getName().toLowerCase(), c);
                columns.add(c.getName().toLowerCase());
            }
        }

        if (target.getColumns() != null) {
            for (Column c: target.getColumns()) {
                targetColumns.put(c.getName().toLowerCase(), c);
                columns.add(c.getName().toLowerCase());
            }
        }

        // columns
        if (!compareInfo.isOptionForceColumnOrder()) {
            for (String c: columns) {
                different(compareInfo, Column.class, SyncCompareResulDiff.Type.COLUMN, sourceColumns.get(c), targetColumns.get(c), result); 
            }
            
        } else {
            
            int maxSize = Math.max(sourceColumns.size(), targetColumns.size());
            for (int i = 0; i < maxSize; i++) {                
                Column tc = target.getColumns() != null && target.getColumns().size() > i ? target.getColumns().get(i) : null;
                Column sc = source.getColumns() != null && source.getColumns().size() > i ? source.getColumns().get(i) : null;
                i++;
                
                different(compareInfo, Column.class, SyncCompareResulDiff.Type.COLUMN, sc, tc, result); 
            }
        }
        
        // indexes
        {
            Set<String> indexes = new LinkedHashSet<>();
            Map<String, Index> sourceIndexes = new HashMap<>();
            Map<String, Index> targetIndexes = new HashMap<>();

            if (source.getIndexes() != null) {
                for (Index i: source.getIndexes()) {
                    sourceIndexes.put(i.getName().toLowerCase(), i);
                    indexes.add(i.getName().toLowerCase());
                }
            }

            if (target.getIndexes() != null) {
                for (Index i: target.getIndexes()) {
                    targetIndexes.put(i.getName().toLowerCase(), i);
                    indexes.add(i.getName().toLowerCase());
                }
            }

            for (String i: indexes) {
                different(compareInfo, Index.class, SyncCompareResulDiff.Type.INDEX, sourceIndexes.get(i), targetIndexes.get(i), result); 
            }
        }
        
        // foreign keys
        if (!compareInfo.isOptionIgnoreForeignKeys())
        {
            Set<String> foreignKeys = new LinkedHashSet<>();
            Map<String, ForeignKey> sourceForeignKeys = new HashMap<>();
            Map<String, ForeignKey> targetForeignKeys = new HashMap<>();

            if (source.getForeignKeys() != null) {
                for (ForeignKey fk: source.getForeignKeys()) {
                    sourceForeignKeys.put(fk.getName().toLowerCase(), fk);
                    foreignKeys.add(fk.getName().toLowerCase());
                }
            }

            if (target.getForeignKeys() != null) {
                for (ForeignKey fk: target.getForeignKeys()) {
                    targetForeignKeys.put(fk.getName().toLowerCase(), fk);
                    foreignKeys.add(fk.getName().toLowerCase());
                }
            }

            for (String fk: foreignKeys) {
                different(compareInfo, ForeignKey.class, SyncCompareResulDiff.Type.FOREIGN, sourceForeignKeys.get(fk), targetForeignKeys.get(fk), result); 
            }
        }
        
        return result;
    }
    
    
    private static void addDifferent(Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result, SyncCompareResulDiff.Type type, Object source, Object target) {
        
        if (result != null) {
            List<SyncCompareResulDiff> list = result.get(type);
            if (list == null) {
                list = new ArrayList<>();
                result.put(type, list);
            }

            list.add(new SyncCompareResulDiff(type, source, target));
        }
    }
    
    private static boolean different(SyncCompareInfo compareInfo, Class compareType, SyncCompareResulDiff.Type diffType, Object source, Object target, Map<SyncCompareResulDiff.Type, List<SyncCompareResulDiff>> result) {
        
        if (compareType == EngineType.class) {
            if (source != null && target != null) {
                if (!((EngineType) source).getName().equals(((EngineType) target).getName())) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
            } else if (source != null || target != null) {
                addDifferent(result, diffType, source, target);
                return true;
            }
        }
        
        
        else if (compareType == Number.class || 
                 compareType == String.class || 
                 compareType == Boolean.class || 
                 compareType == DataType.class ||
                 compareType == IndexType.class) {
            
            if (source != null && target != null) {
                if (!source.equals(target)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
            } else if (source != null || target != null) {
                addDifferent(result, diffType, source, target);
                return true;
            }
        }
        
        
        else if (compareType == Column.class) {
            if (source != null && target != null) {
                                
                if (different(compareInfo, String.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).getName(), ((Column)target).getName(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, DataType.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).getDataType(), ((Column)target).getDataType(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, Boolean.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).isUnsigned(), ((Column)target).isUnsigned(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, Boolean.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).isNotNull(), ((Column)target).isNotNull(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (!compareInfo.isOptionIgnoreAutoincTable() && different(compareInfo, Boolean.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).isAutoIncrement(), ((Column)target).isAutoIncrement(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, Boolean.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).isAutoIncrement(), ((Column)target).isAutoIncrement(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, Boolean.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).isOnUpdate(), ((Column)target).isOnUpdate(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (!compareInfo.isOptionIgnoreColumnDefValue() && different(compareInfo, String.class, SyncCompareResulDiff.Type.COLUMN, ((Column)source).getDefaults(), ((Column)target).getDefaults(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                return false;
            } else if (source != null || target != null) {
                addDifferent(result, diffType, source, target);
                return true;
            }
        }
        
        
        else if (compareType == Index.class) {
            if (source != null && target != null) {
                                
                if (different(compareInfo, String.class, SyncCompareResulDiff.Type.INDEX, ((Index)source).getName(), ((Index)target).getName(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, IndexType.class, SyncCompareResulDiff.Type.COLUMN, ((Index)source).getType(), ((Index)target).getType(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (simpleColumnsDifferent(((Index)source).getColumns(), ((Index)target).getColumns())) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                return false;
            } else if (source != null || target != null) {
                addDifferent(result, diffType, source, target);
                return true;
            }
        }
        
        else if (compareType == ForeignKey.class) {
            if (source != null && target != null) {
                                
                if (different(compareInfo, String.class, SyncCompareResulDiff.Type.FOREIGN, ((ForeignKey)source).getName(), ((ForeignKey)target).getName(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, String.class, SyncCompareResulDiff.Type.FOREIGN, ((ForeignKey)source).getOnDelete(), ((ForeignKey)target).getOnDelete(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, String.class, SyncCompareResulDiff.Type.FOREIGN, ((ForeignKey)source).getOnUpdate(), ((ForeignKey)target).getOnUpdate(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (different(compareInfo, String.class, SyncCompareResulDiff.Type.FOREIGN, ((ForeignKey)source).getReferenceTable(), ((ForeignKey)target).getReferenceTable(), null)) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (simpleColumnsDifferent(((ForeignKey)source).getColumns(), ((ForeignKey)target).getColumns())) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                if (simpleColumnsDifferent(((ForeignKey)source).getReferenceColumns(), ((ForeignKey)target).getReferenceColumns())) {
                    addDifferent(result, diffType, source, target);
                    return true;
                }
                
                return false;
            } else if (source != null || target != null) {
                addDifferent(result, diffType, source, target);
                return true;
            }
        }
        
        return false;
    }
    
    
    private static boolean simpleColumnsDifferent(List<Column> source, List<Column> target) {
        List<String> sourceColumns = new ArrayList<>();
        if ((source) != null) {
            for (Column c: source) {
                sourceColumns.add(c.getName());
            }
        }

        List<String> targetColumns = new ArrayList<>();
        if (target != null) {
            for (Column c: target) {
                targetColumns.add(c.getName());
            }
        }

        if (!Arrays.equals(sourceColumns.toArray(), targetColumns.toArray())) {
            return true;
        }
        
        return false;
    }
}
