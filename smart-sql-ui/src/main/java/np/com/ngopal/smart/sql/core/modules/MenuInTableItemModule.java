package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import np.com.ngopal.smart.sql.ui.controller.BackupDatabaseAsSQLDumpController;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.DataExporterController;
import np.com.ngopal.smart.sql.ui.controller.DatabaseCopierController;
import np.com.ngopal.smart.sql.ui.controller.DuplicateTableController;
import np.com.ngopal.smart.sql.ui.controller.ExportTableAsPdfController;
import np.com.ngopal.smart.sql.ui.controller.ExportTableAsXlsController;
import np.com.ngopal.smart.sql.ui.controller.ExportTableController;
import np.com.ngopal.smart.sql.ui.controller.ImportCsvDataController;
import np.com.ngopal.smart.sql.ui.controller.ImportTableController;
import np.com.ngopal.smart.sql.ui.controller.ResultTabController;
import np.com.ngopal.smart.sql.ui.controller.ScheduledBackupWizardController;
import np.com.ngopal.smart.sql.ui.controller.ViewTablePropertiesController;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import tools.java.pats.models.SqlFormatter;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.CreateTriggerDialogController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;


/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInTableItemModule extends AbstractStandardModule {
    
    private Image triggerImage;
    private Image tableImage;
    
    @Inject
    private PreferenceDataService preferenceService;
        
    @Inject
    private ConnectionParamService service;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public int getOrder() {
        return 5;
    }
    
    private List<Menu> changeTypeMenus;
    
    private Menu pasteSQLStatementMenu;
    private Menu moreTableOperationsMenu;
    private Menu backupOrExportMenu;
    private Menu importMenu;
    private Menu changeTypeTable;
    
    private MenuItem importExternalDataItem;
    private MenuItem insertTable;
    private MenuItem updateTable;
    private MenuItem deleteTable;
    private MenuItem selectTable;
    private MenuItem renameTable;
    private MenuItem truncateTable;
    private MenuItem dropTable;
    private MenuItem reorderColumnsTable;
    private MenuItem dupeTable;
    private MenuItem viewTableProps;
    private MenuItem scheduledBackupsItem;
    private MenuItem backupTableAsSQLDumpItem;
    private MenuItem exportTableAsXls;
    private MenuItem exportTableAsPdf;
    private MenuItem exportTable;
    private MenuItem exportBasicTable;
    private MenuItem importCsvData;
    private MenuItem createTable;
    private MenuItem createTrigger;
    private MenuItem createAuditTrigger;
    private MenuItem relationsOrForeignKeys;
    private MenuItem manageIndex;
    private MenuItem alterTable;
    private MenuItem openTableInNewTab;
    private MenuItem openTable;
    private MenuItem copyTableToDiffHostOrDatabase;
    
    private MenuItem relationsOrForeignKeysForToolbar;
    private MenuItem manageIndexForToolbar;
    private MenuItem exportTableForToolbar;
    
    private static final BooleanProperty disabledProperty = new SimpleBooleanProperty(false);
    
    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();
        
        changeTypeMenus = new ArrayList<>();
        
        pasteSQLStatementMenu = new Menu("Paste SQL",FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getSql_image()), 18, 18));
        
        insertTable = getInsertTableItem(null);
        updateTable = getUpdateTableItem(null);
        deleteTable = getDeleteTableItem(null);
        selectTable = getSelectTableItem(null);
        
        pasteSQLStatementMenu.getItems().addAll(insertTable,
                updateTable, deleteTable, selectTable);        
        
        moreTableOperationsMenu = new Menu("Operations", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getMore_image()), 18, 18));
        
        renameTable = getRenameTableItem(null);
        truncateTable = getTruncateTableItem(null);
        dropTable = getDropTableItem(null);
        reorderColumnsTable = getReorderColumnsTableItem(null);
        dupeTable = getDuplicateTableItem(null);
        viewTableProps = getViewTablePropertiesItem(null);
        changeTypeTable = getChangeTypeTableItem(null);
        
        changeTypeMenus.add(changeTypeTable);
        
        moreTableOperationsMenu.getItems().addAll(renameTable,
                truncateTable, dropTable, reorderColumnsTable, dupeTable, viewTableProps, changeTypeTable);
        
        backupOrExportMenu = new Menu("Backup/Export", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getBackUpExport_image()), 18, 18));
        
        scheduledBackupsItem = getScheduledBackupsItem(null);
        backupTableAsSQLDumpItem = getBackupTableAsSQLDumpItem(null);
        backupTableAsSQLDumpItem.setText("Backup");
        backupTableAsSQLDumpItem.setUserData("Back up Database as SQL Dump");
                
        exportTableAsXls =  getExportTableDataAsXlsItem(null);
        exportTableAsPdf = getExportTableDataAsPdfItem(null);
        exportTable = getExportTableDataItem(null);  
        exportBasicTable = getSimpleExportTable(null);
        
        backupOrExportMenu.getItems().addAll(scheduledBackupsItem, backupTableAsSQLDumpItem,
                exportBasicTable,exportTable,exportTableAsXls,exportTableAsPdf);
        
        importMenu = new Menu("Import", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getImport_image()), 18, 18));
        
        importExternalDataItem = getImportExternalDataItem(null);
        importCsvData = getImportCSVDataUsingLoadLocalItem(null);
        
        importMenu.getItems().addAll(importExternalDataItem, importCsvData);
        
        createTable = getCreateTableItem(null);
        createTrigger = getCreateTriggerItem(null);
        createAuditTrigger = getCreateAuditTriggerItem(null);
        relationsOrForeignKeys = getRelationshipsOrForeignKeysItem(null);
        manageIndex = getManageIndexesItem(null);
        
        alterTable = getAlterTableItem(null);
        openTableInNewTab = getOpenTableInNewTabItem(null);
        openTable = getOpenTableItem(null);
        
        copyTableToDiffHostOrDatabase = getCopyTableToDifferentHostOrDatabaseItem(null);
        copyTableToDiffHostOrDatabase.setText("Copy Table");
        copyTableToDiffHostOrDatabase.setUserData("Copy Table(s) To Different Host/Database...");
        
        Menu openMenu = new Menu("Open",
                 FunctionHelper.constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getOpen_image()), 18, 18));
        openMenu.getItems().addAll(openTable, openTableInNewTab);
        
        Menu manageMenu = new Menu("Manage",FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getManage_image()), 18, 18));
        manageMenu.getItems().addAll(manageIndex, relationsOrForeignKeys);
        
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TABLE, new MenuItem[]{pasteSQLStatementMenu, copyTableToDiffHostOrDatabase});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TABLE, new MenuItem[]{openMenu});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TABLE, new MenuItem[]{createTable, alterTable, manageMenu});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TABLE, new MenuItem[]{moreTableOperationsMenu});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TABLE, new MenuItem[]{backupOrExportMenu, importMenu});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TABLE, new MenuItem[]{createTrigger, createAuditTrigger});
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{exportTable, backupTableAsSQLDumpItem});
        
        exportTableForToolbar = getExportTableDataItem(null);  
        relationsOrForeignKeysForToolbar = getRelationshipsOrForeignKeysItem(null);
        manageIndexForToolbar = getManageIndexesItem(null);
                
//        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{exportTableForToolbar, manageIndexForToolbar, relationsOrForeignKeysForToolbar});
        
        return true;
    }
    
    private Object selectedLastValue;
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        
        selectedLastValue = item.getValue();
        
        disabledProperty.set(!(selectedLastValue instanceof DBTable));
        
        checkChangeTableType();
        
        exportTableForToolbar.disableProperty().bind(disabledProperty.or(busy()));
        relationsOrForeignKeysForToolbar.disableProperty().bind(disabledProperty.or(busy()));
        manageIndexForToolbar.disableProperty().bind(disabledProperty.or(busy()));
        
        importExternalDataItem.disableProperty().bind(disabledProperty);
        insertTable.disableProperty().bind(disabledProperty);
        updateTable.disableProperty().bind(disabledProperty);
        deleteTable.disableProperty().bind(disabledProperty);
        selectTable.disableProperty().bind(disabledProperty);
        renameTable.disableProperty().bind(disabledProperty);
        truncateTable.disableProperty().bind(disabledProperty);
        dropTable.disableProperty().bind(disabledProperty);
        reorderColumnsTable.disableProperty().bind(disabledProperty);
        dupeTable.disableProperty().bind(disabledProperty);
        viewTableProps.disableProperty().bind(disabledProperty);        
        changeTypeTable.disableProperty().bind(disabledProperty);
        backupTableAsSQLDumpItem.disableProperty().bind(disabledProperty);
        exportTableAsXls.disableProperty().bind(disabledProperty);
        exportTableAsPdf.disableProperty().bind(disabledProperty);
        exportTable.disableProperty().bind(disabledProperty);
        importCsvData.disableProperty().bind(disabledProperty);        
        createTrigger.disableProperty().bind(disabledProperty);   
        createAuditTrigger.disableProperty().bind(disabledProperty); 
        relationsOrForeignKeys.disableProperty().bind(disabledProperty);         
        manageIndex.disableProperty().bind(disabledProperty);
        alterTable.disableProperty().bind(disabledProperty);
        openTableInNewTab.disableProperty().bind(disabledProperty);
        openTable.disableProperty().bind(disabledProperty);
        copyTableToDiffHostOrDatabase.disableProperty().bind(disabledProperty);
        
        if (selectedLastValue != null && (selectedLastValue instanceof DBTable || selectedLastValue instanceof Database || selectedLastValue == ConnectionTabContentController.DatabaseChildrenType.TABLES)) {
            createTable.setDisable(false);
            scheduledBackupsItem.setDisable(false);
        } else {
            createTable.setDisable(true);
            scheduledBackupsItem.setDisable(true);
        }
        
        return true;
    }
    
    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private MenuItem getAlterTableItem(final ConnectionSession session) {
     
        MenuItem item = new MenuItem("Alter Table", new ImageView(SmartImageProvider.getInstance().getAlterTable_image()));
        item.setAccelerator(new KeyCodeCombination(KeyCode.F6));

        item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alterTable();
            }

        });
        return item;
    }
    
    public void alterTable() {
        if (tableImage == null) {
            tableImage = SmartImageProvider.getInstance().getTableTab_image();
        }
        ConnectionSession currentSession = getController().getSelectedConnectionSession().get();
        ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(currentSession);
        log.debug("Seleted: {}", object);
        log.debug("Main Value: {}", object.get().getValue());
                
        DBTable table = DatabaseCache.getInstance().getTable(currentSession.getConnectionParam().cacheKey(), 
                    object.get().getParent().getParent().getValue().toString(),
                    object.get().getValue().toString());
        
        AlterTableController altertableController = null;
        Tab alterTab = null;

        ConnectionTabContentController connTab = (ConnectionTabContentController) getController().getTabControllerBySession(currentSession);
        List<Tab> tabs = connTab.findControllerTabs(AlterTableController.class);
        if (tabs != null && !tabs.isEmpty()) {

            for (Tab t: tabs) {
                if (((AlterTableController)t.getUserData()).getTable().equals(table)) {
                    altertableController = (AlterTableController)t.getUserData();
                    alterTab = t;
                    break;
                }
            }
        }

        if (altertableController != null) {
            getController().showTab(alterTab, currentSession);
        } else {
        
            altertableController = getTypedBaseController("AlterTable");
            altertableController.setService(currentSession.getService());

            alterTab = new Tab(object.get().getValue().toString());
            alterTab.setGraphic(new ImageView(tableImage));
            alterTab.setUserData(altertableController);

            Parent p = altertableController.getUI();
            alterTab.setContent(p);

            altertableController.init(alterTab, table, currentSession, true);

            getController().addTab(alterTab, currentSession);
            getController().showTab(alterTab, currentSession);
        }
    }
    
    
    private MenuItem getInsertTableItem(final ConnectionSession session) {
        MenuItem insertTableItem = new MenuItem("INSERT INTO <tablelname>...",
        new ImageView(SmartImageProvider.getInstance().getInsert_image()));
        insertTableItem.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN,
                KeyCodeCombination.SHIFT_DOWN));
        insertTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                
                    Object obj = (getController().getSelectedTreeItem(currentSession)).get().getValue();
                    if (obj instanceof DBTable) {
                        DBTable table = (DBTable) obj;                
                        showSQLTemplate(currentSession, currentSession.getService().getInsertTableSQL(table.getDatabase().getName(), table));
                    }
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        });
        return insertTableItem;
    }
    
    private MenuItem getUpdateTableItem(final ConnectionSession session) {
        
        MenuItem updateTableItem = new MenuItem("UPDATE<tablename> SET ...",
        new ImageView(SmartImageProvider.getInstance().getUpdate_image()));
        updateTableItem.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHIFT_DOWN,KeyCombination.ALT_DOWN));
        updateTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                
                    Object obj = (getController().getSelectedTreeItem(currentSession)).get().getValue();
                    
                    if (obj instanceof DBTable) {
                        DBTable table = (DBTable) obj;                
                        showSQLTemplate(currentSession, currentSession.getService().getUpdateTableSQL(table.getDatabase().getName(), table));
                    }
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        });
        return updateTableItem;
        
    }
    
    private MenuItem getDeleteTableItem(final ConnectionSession session) {
        MenuItem deleteTableItem = new MenuItem("DELETE FROM <tablename>...",
        new ImageView(SmartImageProvider.getInstance().getDelete_image()));
        deleteTableItem.setAccelerator(new KeyCodeCombination(KeyCode.D,KeyCombination.SHIFT_DOWN,KeyCombination.ALT_DOWN));
        deleteTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                    
                    Object obj = (getController().getSelectedTreeItem(currentSession)).get().getValue();                
                    if (obj instanceof DBTable) {
                        DBTable table = (DBTable) obj;
                        showSQLTemplate(currentSession, currentSession.getService().getDeleteTableSQL(table.getDatabase().getName(), table));
                    }
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        });
        return deleteTableItem;
    }
    
    private MenuItem getSelectTableItem(final ConnectionSession session) {
        
        MenuItem selectTableItem = new MenuItem("SELECT <col-1>...<col-n> FROM...",
            new ImageView(SmartImageProvider.getInstance().getSelect_image()));
        selectTableItem.setAccelerator(new KeyCodeCombination(KeyCode.S,KeyCombination.SHIFT_DOWN,KeyCombination.ALT_DOWN));
        selectTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                    
                    Object obj = (getController().getSelectedTreeItem(currentSession)).get().getValue();
                    if (obj instanceof DBTable) {
                        DBTable table = (DBTable) obj;         
                        showSQLTemplate(currentSession, currentSession.getService().getSelectTableSQL(table.getDatabase().getName(), table));
                    }
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        });
        return selectTableItem;
        
    }
    
    private MenuItem getCopyTableToDifferentHostOrDatabaseItem(final ConnectionSession session) {
        MenuItem copyTableToDifferentHostOrDatabaseItem = new MenuItem("Copy Table(s) To Different Host/Database...",
                new ImageView(SmartImageProvider.getInstance().getDatabaseCopy_image()));
        copyTableToDifferentHostOrDatabaseItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                    
                DatabaseCopierController copierCon = getTypedBaseController("DatabaseCopier");
                    
                Database db = currentSession.getConnectionParam().getSelectedDatabase();

                Map<String, String> selectedData = new HashMap<>();

                DBTable table = (DBTable) (getController().getSelectedTreeItem(currentSession)).get().getValue();

                selectedData.put(ConnectionTabContentController.DatabaseChildrenType.TABLES.name(), table.getName());

                copierCon.insertSession(currentSession, db.getName(), selectedData);
                        
                if (copierCon.showNow(getController().getStage())) {
                    getController().refreshTree(currentSession, false);
                }       
            }
        });
        return copyTableToDifferentHostOrDatabaseItem;
    }
    
    private MenuItem getManageIndexesItem(final ConnectionSession session) {
        MenuItem manageIndexesItem = new MenuItem("Manage Indexes",
                new ImageView(SmartImageProvider.getInstance().getIndex_image()));
        manageIndexesItem.setAccelerator(new KeyCodeCombination(KeyCode.F7));
        manageIndexesItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                manageIndexes();
            }
        });
        return manageIndexesItem;
    }
    
    public void manageIndexes() {
        try {
            AlterTableController altertableController = showAlterTable();
            altertableController.manageIndexes();
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }
    
    private MenuItem getOpenTableItem(final ConnectionSession session) {
        
        MenuItem openTableItem = new MenuItem("Open Table", 
                new ImageView(SmartImageProvider.getInstance().getTableTab_image()));
        openTableItem.setAccelerator(new KeyCodeCombination(KeyCode.F11));
        openTableItem.setOnAction((ActionEvent t) -> {
            
            ConnectionSession ses = session!= null? session : getController().getSelectedConnectionSession().get();
            
            QueryTabModule qtm = (QueryTabModule)
                    getController().getModuleByClassName(QueryTabModule.class.getName());
            
            ReadOnlyObjectProperty<Tab> selectedTab = (ReadOnlyObjectProperty<Tab>) getController().getSelectedConnectionTab(ses);
            
            Tab tab;
            if (selectedTab != null && selectedTab.get() != null && QueryTabModule.TAB_USER_DATA__QUERY_TAB.equals(selectedTab.get().getId())) {
                tab = selectedTab.get();
            } else {
                tab = qtm.addQueryTab(ses, null, "");
            }
            
            TabContentController tcc  = getMainController().getTabControllerBySession(ses);
            if(tcc instanceof ConnectionTabContentController){
                ConnectionTabContentController ctcc = (ConnectionTabContentController)tcc;
                ctcc.setSelectedBottomTabId(TabledataTabModule.TableData_ID);
            }
        });
        return openTableItem;
    }
    
    private MenuItem getOpenTableInNewTabItem(final ConnectionSession sess) {
       
        MenuItem openTableInNewTabItem = new MenuItem("Open Table in New Tab",
                new ImageView(SmartImageProvider.getInstance().getTableTab_image()));
        openTableInNewTabItem.setAccelerator(new KeyCodeCombination(KeyCode.F11, KeyCombination.SHORTCUT_DOWN));
        openTableInNewTabItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                
                ConnectionSession session = sess != null ? sess : getController().getSelectedConnectionSession().get();
                
                ReadOnlyObjectProperty<TreeItem> selectedTable = getController().getSelectedTreeItem(session);
                if (selectedTable != null) {
                    DBTable table = (DBTable) ((TreeItem) selectedTable.get()).getValue();
                    
                    // check may be table already opened
                    Tab tab = getController().getTab(QueryTabModule.TAB_USER_DATA__TABLE_TAB, table.getName(), session);
                    if (tab == null) {
                        tab = new Tab(table.getName());
                        tab.setGraphic(new ImageView(tableImage));
                        tab.setId(QueryTabModule.TAB_USER_DATA__TABLE_TAB);
                        
                        ResultTabController tData = getTypedBaseController("ResultTab");
                        tData.toggleForTab(true);
                        tData.setService(session.getService());
                        
                        // Strange wrong code - commented !!!
                        // String verified = Recycler.verifyTableName(table.getDatabase().getName(), table.getName());
                        // System.out.println(verified);
                        String qery = "select * from " + table.getName() + ";";

                        tData.init(null, null, qery, session, false, true, session.getConnectionParam().isWithLimit());

                        tab.setContent(tData.getUI());
                        getController().addTab(tab, session);
                        getController().showTab(tab, session);
                        tData.peekTabpane(tab.getTabPane());
                    } else {
                        getController().showTab(tab, session);
                    }
                }
            }
        });
        
        return openTableInNewTabItem;
    }
    
    private MenuItem getCreateTableItem(final ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem createTableItem = new MenuItem("Create Table", new ImageView(SmartImageProvider.getInstance().getCreateTable_image()));
        createTableItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableImage == null) {
                    tableImage = SmartImageProvider.getInstance().getTableTab_image();
                }
                ConnectionSession s = (session!= null? session : getController().getSelectedConnectionSession().get());
                Database db = s.getConnectionParam().getSelectedDatabase();
                
                AlterTableController altertableController = getTypedBaseController("AlterTable");
                altertableController.setService(s.getService());

                Tab alterTab = new Tab("New table");
                alterTab.setGraphic(new ImageView(tableImage));
                alterTab.setUserData(altertableController);

                Parent p = altertableController.getUI();
                alterTab.setContent(p);
                
                DBTable table = new DBTable("", null, null, null, new ArrayList<>(), db, null, null, new ArrayList<>(), new ArrayList<>());

                altertableController.init(alterTab, table, s, false);

                getController().addTab(alterTab, s);
                getController().showTab(alterTab, s);
            }
        });
        
        return createTableItem;
    }
    
    private MenuItem getRelationshipsOrForeignKeysItem(final ConnectionSession session) {
        MenuItem relationshipsOrForeignKeysItem = new MenuItem("Relationships/Foreign Keys", new ImageView(SmartImageProvider.getInstance().getForeignKey_image()));
        relationshipsOrForeignKeysItem.setAccelerator(new KeyCodeCombination(KeyCode.F10));
        relationshipsOrForeignKeysItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    AlterTableController altertableController = showAlterTable();
                    altertableController.manageForeignKeysTab();
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        });
        return relationshipsOrForeignKeysItem;
    }
    
    private MenuItem getRenameTableItem(final ConnectionSession session) {
        MenuItem renameTableItem = new MenuItem("Rename Table", new ImageView(SmartImageProvider.getInstance().getTableRename_image()));
        renameTableItem.setAccelerator(new KeyCodeCombination(KeyCode.F2));
        renameTableItem.setOnAction((ActionEvent event) -> {
            renameTable();
        });
        return renameTableItem;
    }
    
    public void renameTable() {
        ConnectionSession s = getController().getSelectedConnectionSession().get();

        DBTable table = (DBTable) (getController().getSelectedTreeItem(s)).get().getValue();

        String newName = showInput("Rename table", "Enter new name of table:", table.getName());
        if (newName != null && !newName.isEmpty()) {
            try {
                Database db1 = s.getConnectionParam().getSelectedDatabase();

                s.getService().renameTable(db1.getName(), table.getName(), newName);
                table.setName(newName);

                getController().refreshTree(s, false);
            }catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }
    }
    
    private MenuItem getTruncateTableItem(final ConnectionSession session) {
        MenuItem truncateTableItem = new MenuItem("Truncate Table...", new ImageView(SmartImageProvider.getInstance().getTableTruncate_image()));
        truncateTableItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE,KeyCombination.SHIFT_DOWN));
        truncateTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConnectionSession s = (session!= null? session : getController().getSelectedConnectionSession().get());
            
                ReadOnlyObjectProperty<TreeItem> selectedTable = getController().getSelectedTreeItem(s);
                if (selectedTable != null) {
                    DBTable table = (DBTable) ((TreeItem) selectedTable.get()).getValue();
                    
                    DialogResponce responce = showConfirm("Truncate Table...", 
                        "Do you really want to truncate the table '" + table.getName() + "'?\nWarning: You will lose all data!");
                    
                    if (responce == DialogResponce.OK_YES) {
                        try {
                            // truncate db
                            int result = (int) s.getService().truncateTable(table);
                            if (result == -1) {
                                showError("Error", "Can not truncate table", null);
                            } else {
                                showInfo("Info", "Table truncated successfully", null);
                            }
                        } catch (SQLException ex) {
                            showError("Error", ex.getMessage(), ex);
                        }
                    }
                }
            }
        });
        return truncateTableItem;
    }
    
    private Menu getChangeTypeTableItem(final ConnectionSession session) {
        Menu changeTypeTable0 = new Menu("Change Table Type To");
        
        ToggleGroup toggleGroup = new ToggleGroup();
        for (EngineType t: MySQLEngineTypes.TYPES) {
            RadioMenuItem item = new RadioMenuItem(t.getDisplayValue());
            item.setToggleGroup(toggleGroup);
            item.setOnAction((ActionEvent event) -> {
                changeTableType(session, t.getDisplayValue());
                
                checkChangeTableType();
            });
            
            changeTypeTable0.getItems().add(item);
        }
        
        return changeTypeTable0;
    }
    
    private void checkChangeTableType() {
        if (selectedLastValue instanceof DBTable && getController().getSelectedConnectionSession().get() != null) {
            
            DBTable table = DatabaseCache.getInstance().getCopyOfTable(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), ((DBTable)selectedLastValue).getDatabase().getName(), ((DBTable)selectedLastValue).getName());

            for (Menu m: changeTypeMenus) {
                for (MenuItem mi: m.getItems()) {
                    if (mi.getText().equals(table.getEngineType().getDisplayValue())) {
                        ((RadioMenuItem)mi).setSelected(true);
                        break;
                    }
                }
            }
        }
    }
        
    private void changeTableType(ConnectionSession session, String type) {
        ConnectionSession s = session!= null? session : getController().getSelectedConnectionSession().get();            
        ReadOnlyObjectProperty<TreeItem> selectedTable = getController().getSelectedTreeItem(s);
        if (selectedTable != null) {
            try {
                DBTable table = (DBTable) ((TreeItem) selectedTable.get()).getValue();
                s.getService().changeTableType(table.getDatabase().getName(), table.getName(), type);
            } catch (SQLException ex) {
                showError("Error changing table type", ex.getMessage(), ex);
            }
        }
    }
    
    private MenuItem getReorderColumnsTableItem(final ConnectionSession session) {
        MenuItem reorderColumnsTableItem = new MenuItem("Reorder Column(s)", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getColumnManage_image()), 20, 20));
        reorderColumnsTableItem.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        reorderColumnsTableItem.setOnAction((ActionEvent event) -> {
            try {
                AlterTableController altertableController = showAlterTable();
                altertableController.manageColumns();
            } catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        });
        return reorderColumnsTableItem;
    }
    
    private MenuItem getDropTableItem(final ConnectionSession session) {
        MenuItem dropTableItem = new MenuItem("Drop Table From Database ...", new ImageView(SmartImageProvider.getInstance().getTableDrop_image()));
        dropTableItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropTableItem.setOnAction((ActionEvent event) -> {
            ConnectionSession s = session!= null? session : getController().getSelectedConnectionSession().get();
            
            ReadOnlyObjectProperty<TreeItem> selectedTable = getController().getSelectedTreeItem(s);
            if (selectedTable != null) {
                DBTable table = (DBTable) ((TreeItem) selectedTable.get()).getValue();
                
                DialogResponce responce = showConfirm("Drop Table From Database...", 
                        "Do you really want to drop the table '" + table.getName() + "'?\nWarning: You will lose all data!");
                
                if (responce == DialogResponce.OK_YES) {
                    try {
                        // truncate db
                        s.getService().dropTable(table);
                        
                        LeftTreeHelper.tableDroped(s.getConnectionParam(), selectedTable.get());
                    } catch (SQLException ex) {
                        showError("Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        return dropTableItem;
    }
    
    private MenuItem getViewTablePropertiesItem(final ConnectionSession session) {
        MenuItem viewTablePropertiesItem = new MenuItem("View Table Properties", new ImageView(SmartImageProvider.getInstance().getTableProperties_image()));
        viewTablePropertiesItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showTablePropertiesDialog(session != null ? session :
                        getController().getSelectedConnectionSession().get());
            }
        });
        return viewTablePropertiesItem;
    }
    
    private MenuItem getDuplicateTableItem(final ConnectionSession session) {
        
        MenuItem duplicateTableItem = new MenuItem("Duplicate Table Structure/Data...", new ImageView(SmartImageProvider.getInstance().getTableDuplicate_image()));
                
        duplicateTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showDuplicateTableDialog(session!= null? session : 
                        getController().getSelectedConnectionSession().get());
            }
        });
        return duplicateTableItem;
    }
  
    
    private MenuItem getScheduledBackupsItem(final ConnectionSession session) {
        MenuItem scheduledBackupsItem = new MenuItem("Scheduled Backups...", new ImageView(SmartImageProvider.getInstance().getScheduleBackUp_image()));
        scheduledBackupsItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        scheduledBackupsItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showScheduledBackupsDialog(session != null ? session :
                        getController().getSelectedConnectionSession().get());
            }
        });
        return scheduledBackupsItem;
    }
    
    private MenuItem getBackupTableAsSQLDumpItem(final ConnectionSession session) {
        MenuItem backupTableAsSQLDumpItem = new MenuItem("Backup Table(s) As SQL Dump...", new ImageView(SmartImageProvider.getInstance().getDatabaseDump_image()));
        backupTableAsSQLDumpItem.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        backupTableAsSQLDumpItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showBackupDatabaseAsSQLDumpDialog(session != null ? session :
                        getController().getSelectedConnectionSession().get());
            }
        });
        return backupTableAsSQLDumpItem;
    }
    
    private MenuItem getSimpleExportTable(final ConnectionSession session){
        MenuItem simpleExportItem = new MenuItem("Export Basic", new ImageView(SmartImageProvider.getInstance().getTableExport_image()));
        simpleExportItem.setUserData("Basic table export");
        simpleExportItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) { 
                DataExporterController exporterController = getMainController().getTypedBaseController("DataExporter");
                exporterController.triggerSimpleExport();
                
                Stage exportDialog = new Stage(StageStyle.UTILITY);
                exportDialog.initModality(Modality.WINDOW_MODAL);
                exportDialog.initOwner(getMainController().getStage());
                exportDialog.setTitle("     BASIC TABLE EXPORT DIALOG");                
                
                exportDialog.setScene(new Scene(exporterController.getUI()));
                exportDialog.setResizable(false);
                exportDialog.show();
            }
        });
        
        return simpleExportItem;
    }
    
    private MenuItem getExportTableDataItem(final ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem exportTableDataItem = new MenuItem("Export Advanced",  new ImageView(SmartImageProvider.getInstance().getTableExport_image()));
        exportTableDataItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        exportTableDataItem.setUserData("Export All Rows");
        exportTableDataItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession currentSession = session != null ? session : getController().getSelectedConnectionSession().get();
                
                DBTable table = null;
                ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
                if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                    if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                        table = (DBTable)selectedTreeItem.getValue().getValue();
                    }
                }
                if (table != null) {
                    ExportTableController exportTableController = getTypedBaseController("ExportTable");
                    
                    Scene scene = new Scene(exportTableController.getUI());
                    ObservableList<ObservableList> datas = FXCollections.observableArrayList();
                    ObservableList<String> columnsData = FXCollections.observableArrayList();

                    for (Column c: table.getColumns()) {
                        columnsData.add(c.getName());
                    }
                    
                    String countQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_COUNT, currentSession.getConnectionParam().getSelectedDatabase(), table.getName());
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(countQuery)) { //"SELECT count(*) FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "`")) {
                        if (rs.next()) {
                            if (rs.getLong(1) > ExportUtils.MAX_ROWS_COUNT) {
                                showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ",\n only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                            }
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                        
                    String selectAllQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, currentSession.getConnectionParam().getSelectedDatabase(), table.getName()) + " LIMIT " + ExportUtils.MAX_ROWS_COUNT;
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(selectAllQuery)) {//"SELECT * FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "` LIMIT " + ExportUtils.MAX_ROWS_COUNT)) {
                        while (rs.next()) {
                            
                            ObservableList row = FXCollections.observableArrayList();
                            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                row.add(rs.getObject(i));
                            }
                            
                            datas.add(row);
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                    
                    try {
                        exportTableController.init(currentSession.getService(), table, datas, columnsData);

                        Stage stage = new Stage();
                        stage.setTitle("Export As");
                        stage.initModality(Modality.WINDOW_MODAL);
                        stage.initOwner(getController().getStage());
                        stage.setScene(scene);

                        stage.showAndWait();
                    } catch (SQLException ex) {
                        showError("Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        
        return exportTableDataItem;
    }
    
    private MenuItem getExportTableDataAsXlsItem(final ConnectionSession session) {
        MenuItem exportTableDataAsXlsItem = new MenuItem("Export Table Data As XLS", new ImageView(SmartImageProvider.getInstance().getXls_image()));
        exportTableDataAsXlsItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession currentSession = session != null ? session : getController().getSelectedConnectionSession().get();
                
                DBTable table = null;
                ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
                if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                    if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                        table = (DBTable)selectedTreeItem.getValue().getValue();
                    }
                }
                if (table != null) {
                    ExportTableAsXlsController exportTableAsXlsController = getMainController().getTypedBaseController("ExportTableAsXls");
                    
                    Scene scene = new Scene(exportTableAsXlsController.getUI());
                    ObservableList<ObservableList> datas = FXCollections.observableArrayList();
                    ObservableList<String> columnsData = FXCollections.observableArrayList();

                    for (Column c: table.getColumns()) {
                        columnsData.add(c.getName());
                    }
                        
                    String countQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_COUNT, currentSession.getConnectionParam().getSelectedDatabase(), table.getName());
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(countQuery)) {//"SELECT count(*) FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "`")) {
                        if (rs.next()) {
                            if (rs.getLong(1) > ExportUtils.MAX_ROWS_COUNT) {
                                showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ", only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                            }
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                    
                    String selectAllQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, currentSession.getConnectionParam().getSelectedDatabase(), table.getName()) + " LIMIT " + ExportUtils.MAX_ROWS_COUNT;
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(selectAllQuery)) {//"SELECT * FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "` LIMIT " + ExportUtils.MAX_ROWS_COUNT)) {
                        while (rs.next()) {
                            
                            ObservableList row = FXCollections.observableArrayList();
                            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                row.add(rs.getObject(i));
                            }
                            
                            datas.add(row);
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                    
                    try {
                        exportTableAsXlsController.init(currentSession.getService(), datas, columnsData);

                        Stage stage = new Stage();
                        stage.setTitle("Export As XLS");
                        stage.initModality(Modality.WINDOW_MODAL);
                        stage.initOwner(getController().getStage());
                        stage.setScene(scene);

                        stage.showAndWait();
                    } catch (SQLException ex) {
                        showError("Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        
        return exportTableDataAsXlsItem;
    }
    
    
    private MenuItem getExportTableDataAsPdfItem(final ConnectionSession session) {
        MenuItem exportTableDataAsPdfItem = new MenuItem("Export Table Data As PDF", new ImageView(SmartImageProvider.getInstance().getPdf_image()));
        exportTableDataAsPdfItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession currentSession = session != null ? session : getController().getSelectedConnectionSession().get();
                
                DBTable table = null;
                ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
                if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                    if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                        table = (DBTable)selectedTreeItem.getValue().getValue();
                    }
                }
                if (table != null) {
                    ExportTableAsPdfController exportTableAsPdfController = getTypedBaseController("ExportTableAsPdf");
                    
                    Scene scene = new Scene(exportTableAsPdfController.getUI());
                    ObservableList<ObservableList> datas = FXCollections.observableArrayList();
                    ObservableList<String> columnsData = FXCollections.observableArrayList();

                    for (Column c: table.getColumns()) {
                        columnsData.add(c.getName());
                    }
                    
                    String countQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_COUNT, currentSession.getConnectionParam().getSelectedDatabase(), table.getName());
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(countQuery)) {//"SELECT count(*) FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "`")) {
                        if (rs.next()) {
                            if (rs.getLong(1) > ExportUtils.MAX_ROWS_COUNT) {
                                showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ", only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                            }
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                        
                    String selectAllQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, currentSession.getConnectionParam().getSelectedDatabase(), table.getName()) + " LIMIT " + ExportUtils.MAX_ROWS_COUNT;
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(selectAllQuery)) {//"SELECT * FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "` LIMIT " + ExportUtils.MAX_ROWS_COUNT)) {
                        while (rs.next()) {
                            
                            ObservableList row = FXCollections.observableArrayList();
                            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                row.add(rs.getObject(i));
                            }
                            
                            datas.add(row);
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                    
                    exportTableAsPdfController.init(currentSession.getService(), datas, columnsData);

                    Stage stage = new Stage();
                    stage.setTitle("Export As PDF");
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.initOwner(getController().getStage());
                    stage.setScene(scene);

                    stage.showAndWait();
                }
            }
        });
        
        return exportTableDataAsPdfItem;
    }
    
    private MenuItem getImportExternalDataItem(final ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem importExternalDataItem = new MenuItem("Import External Data...", new ImageView(SmartImageProvider.getInstance().getDatabaseExternalImport()));
        importExternalDataItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        importExternalDataItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showImportExternalDataDialog(session != null ? session : getController().getSelectedConnectionSession().get());                
            }
        });
        return importExternalDataItem;

    }
    
    private void showImportExternalDataDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        
        DBTable table = null;
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem =  getController().getSelectedTreeItem(session != null ? session : getController().getSelectedConnectionSession().get());
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
            if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                table = (DBTable)selectedTreeItem.getValue().getValue();
            }
        }
        
        ImportTableController controller = getTypedBaseController("ImportTable");
        controller.init(session != null ? session : getController().getSelectedConnectionSession().get(), table);
        
        final Scene scene = new Scene(controller.getUI());
        
        dialog.setScene(scene);
        dialog.initStyle(StageStyle.UTILITY);        
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Import External Data Wizard");
        dialog.showAndWait();
    }
    
    private MenuItem getImportCSVDataUsingLoadLocalItem(final ConnectionSession session) {
        MenuItem importCSVDataUsingLoadLocalItem = new MenuItem("Import CSV Data Using LOAD LOCAL...", new ImageView(SmartImageProvider.getInstance().getImportCsv_image()));
        importCSVDataUsingLoadLocalItem.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHORTCUT_DOWN,KeyCombination.SHIFT_DOWN));
        importCSVDataUsingLoadLocalItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DBTable table = null;
                ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session != null ? session : getController().getSelectedConnectionSession().get());
                if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                    if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                        table = (DBTable)selectedTreeItem.getValue().getValue();
                    }
                }
        
                ImportCsvDataController importCsvDC = getTypedBaseController("ImportCsvData");
                importCsvDC.init(session != null ? session : getController().getSelectedConnectionSession().get(), table);
                importCsvDC.show(getController().getStage());
            }
        });
        return importCSVDataUsingLoadLocalItem;
    }
    
    private MenuItem getCreateTriggerItem(final ConnectionSession session) {
        MenuItem createTriggerItem = new MenuItem("Create Trigger", new ImageView(SmartImageProvider.getInstance().getCreateTrigger_image()));
        createTriggerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(session != null) {
                    createTrigger(session);
                } else {
                    createTrigger(getController().getSelectedConnectionSession().get());
                }
            }
        });
        return createTriggerItem;
    }
    
    private MenuItem getCreateAuditTriggerItem(final ConnectionSession session) {
        MenuItem createAuditTriggerItem = new MenuItem("Create Audit Log Trigger", new ImageView(SmartImageProvider.getInstance().getCreateTrigger_image()));
        createAuditTriggerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(session != null) {
                    createAuditTrigger(session);
                } else {
                    createAuditTrigger(getController().getSelectedConnectionSession().get());
                }
            }
        });
        return createAuditTriggerItem;
    }
    
    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);
        
        Menu pasteSQLStatementMenu = new Menu("Paste SQL Statement", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getSql_image()), 18, 18));
        
        pasteSQLStatementMenu.getItems().addAll(getInsertTableItem(session),
                getUpdateTableItem(session), getDeleteTableItem(session), getSelectTableItem(session));        
        
        Menu moreTableOperationsMenu = new Menu("More Table Operations", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getMore_image()), 18, 18));
        
        Menu changeTypeMenu = getChangeTypeTableItem(session);
        moreTableOperationsMenu.getItems().addAll(getRenameTableItem(session),
                getTruncateTableItem(session), getDropTableItem(session), getReorderColumnsTableItem(session), getDuplicateTableItem(session),
                getViewTablePropertiesItem(session), changeTypeMenu);
        
        changeTypeMenus.add(changeTypeMenu);
        
        Menu backupOrExportMenu = new Menu("Backup/Export", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getBackUpExport_image()), 18, 18));
        
        backupOrExportMenu.getItems().addAll(getScheduledBackupsItem(session), getBackupTableAsSQLDumpItem(session),
                getSimpleExportTable(session),getExportTableDataItem(session), getExportTableDataAsXlsItem(session), getExportTableDataAsPdfItem(session));
        
        Menu importMenu = new Menu("Import", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getImport_image()), 18, 18));
        
        importMenu.getItems().addAll(getImportExternalDataItem(session), getImportCSVDataUsingLoadLocalItem(session));
        
        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("tableItems0", new ArrayList<>(Arrays.asList(new MenuItem[]{pasteSQLStatementMenu,
            getCopyTableToDifferentHostOrDatabaseItem(session)}))), Hooks.QueryBrowser.TABLE_ITEM);
        map.put(new HookKey("tableItems1", new ArrayList<>(Arrays.asList(new MenuItem[]{getOpenTableItem(session),
            getOpenTableInNewTabItem(session), getCreateTableItem(session), getAlterTableItem(session), getManageIndexesItem(session),
            getRelationshipsOrForeignKeysItem(session), moreTableOperationsMenu}))), Hooks.QueryBrowser.TABLE_ITEM);
        map.put(new HookKey("tableItems2", new ArrayList<>(Arrays.asList(new MenuItem[]{backupOrExportMenu, importMenu}))), Hooks.QueryBrowser.TABLE_ITEM);
        map.put(new HookKey("tableItems3", new ArrayList<>(Arrays.asList(new MenuItem[]{getCreateTriggerItem(session), getCreateAuditTriggerItem(session)}))), Hooks.QueryBrowser.TABLE_ITEM);
        
        return map;
    }
    
    private void showSQLTemplate(ConnectionSession session, String sql) {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null)
        {
            SqlFormatter formatter = new SqlFormatter();
            
            PreferenceData pd = preferenceService.getPreference();
            if (!pd.isQueriesUsingBackquote()) {
                sql = sql.replaceAll("`", "");
            }
            
            sql = formatter.formatSql(sql, pd.getTab(), pd.getIndentString(), pd.getStyle());
            
            getMainController().usedQueryFormat();
            
            ReadOnlyObjectProperty<Tab> selectedTab = (ReadOnlyObjectProperty<Tab>) getController().getSelectedConnectionTab(session);
            // if opened query tab append in it
            if (selectedTab.get() != null && QueryTabModule.TAB_USER_DATA__QUERY_TAB.equals(selectedTab.get().getId())) {
                queryTabModule.appendToTab(selectedTab.get(), "\n" + sql + "\n\n");
            } else {
                queryTabModule.addQueryTab(session, "" + sql + "\n\n");
            }
        }
    }
    
    private AlterTableController showAlterTable() throws SQLException {
        if (tableImage == null) {
            tableImage = SmartImageProvider.getInstance().getTableTab_image();
        }
        
        ConnectionSession currentSession = getController().getSelectedConnectionSession().get();
        
        ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(currentSession);
        
        log.debug("Seleted: {}", object);
        
        log.debug("Main Value: {}", object.get().getValue());
        
        DBTable table = DatabaseCache.getInstance().getTable(currentSession.getConnectionParam().cacheKey(), 
                    object.get().getParent().getParent().getValue().toString(),
                    object.get().getValue().toString());
        
        AlterTableController altertableController = null;
        Tab alterTab = null;

        ConnectionTabContentController connTab = (ConnectionTabContentController) getController().getTabControllerBySession(currentSession);
        List<Tab> tabs = connTab.findControllerTabs(AlterTableController.class);
        if (tabs != null && !tabs.isEmpty()) {

            for (Tab t: tabs) {
                if (((AlterTableController)t.getUserData()).getTable().equals(table)) {
                    altertableController = (AlterTableController)t.getUserData();
                    alterTab = t;
                    break;
                }
            }
        }

        if (altertableController != null) {
            getController().showTab(alterTab, currentSession);
        } else {

            altertableController = getTypedBaseController("AlterTable");
            altertableController.setService(currentSession.getService());

            alterTab = new Tab(object.get().getValue().toString());
            alterTab.setGraphic(new ImageView(tableImage));
            alterTab.setUserData(altertableController);

            Parent p = altertableController.getUI();
            alterTab.setContent(p);        
            
            altertableController.init(alterTab, table, currentSession,true);

            getController().addTab(alterTab, currentSession);
            getController().showTab(alterTab, currentSession);
        }
        
        return altertableController;
    }
    
    private void createTrigger(final ConnectionSession session) {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null) {
            ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
            Database db = sessionTemp.getConnectionParam().getSelectedDatabase();

            String dbName = db != null ? db.getName() : "<db-name>";
            
            DBTable table = null;
            ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(sessionTemp);
            if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                    table = (DBTable)selectedTreeItem.getValue().getValue();
                }
            }
            
            String tableName = table != null ? table.getName() : null;
                
            if (triggerImage == null) {
                triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
            }
            
            queryTabModule.addTemplatedQueryTab(sessionTemp, (Void t) -> {

                CreateTriggerDialogController contr = StandardBaseControllerProvider.getController(getController(), "CreateTrigger");
                contr.init(sessionTemp.getConnectionParam(), db, tableName, false);

                DialogHelper.show("Create trigger", contr.getUI(), getMainController().getStage());

                if (contr.getResponce() == DialogResponce.OK_YES) {

                    String content = sessionTemp.getService().getCreateTriggerTemplateSQL(
                        dbName,
                        contr.getTriggerNameValue(),
                        contr.getBeforeAfterValue(),
                        contr.getTriggerEventValue(),
                        contr.getTriggerTableValue(),
                        contr.getComment()
                    );

                    return new Pair<>(contr.getTriggerNameValue(), content);                        
                }                    

                return null;


            }, QueryTabModule.TAB_USER_DATA__TRIGGER_TAB );
        }
    }
    
    private void createAuditTrigger(final ConnectionSession session) {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null) {
            ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
            Database db = sessionTemp.getConnectionParam().getSelectedDatabase();

            String dbName = db != null ? db.getName() : "<db-name>";
            
            DBTable table = null;
            ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(sessionTemp);
            if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                if (selectedTreeItem.getValue().getValue() instanceof DBTable) {
                    table = (DBTable)selectedTreeItem.getValue().getValue();
                }
            }
            
            String tableName = table != null ? table.getName() : null;
                
            if (triggerImage == null) {
                triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
            }
            
            queryTabModule.addTemplatedQueryTab(sessionTemp, (Void t) -> {

                CreateTriggerDialogController contr = StandardBaseControllerProvider.getController(getController(), "CreateTrigger");
                contr.init(sessionTemp.getConnectionParam(), db, tableName, true);

                DialogHelper.show("Create Trigger for Audit Logs", contr.getUI(), getMainController().getStage());

                if (contr.getResponce() == DialogResponce.OK_YES) {

                    String content = sessionTemp.getService().getCreateAuditTriggerTemplateSQL(
                        dbName,
                        contr.getTriggerNameValue(),
                        contr.getBeforeAfterValue(),
                        contr.getTriggerEventValue(),
                        contr.getTriggerTableValue(),
                        contr.getTriggerBody(),
                        contr.getComment()
                    );

                    return new Pair<>(contr.getTriggerNameValue(), content);                        
                }                    

                return null;


            }, QueryTabModule.TAB_USER_DATA__TRIGGER_TAB );
        }
    }
    
    private void showBackupDatabaseAsSQLDumpDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        
        BackupDatabaseAsSQLDumpController controller = getTypedBaseController("BackupDatabaseAsSQLDump");
        
        Database database = null;
        
        ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
        Map<String, String> selectedData = new HashMap<>();
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof DBTable) {
                database = ((DBTable) itemValue).getDatabase();                
                selectedData.put(ConnectionTabContentController.DatabaseChildrenType.TABLES.name(), ((DBTable) itemValue).getName());
            }
        }
        
        controller.init(currentSession.getService(), session.getConnectionParam(), database, selectedData);
        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);
        
        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("SQL Dump");
        
        dialog.showAndWait();
    }
    
    public void showScheduledBackupsDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ScheduledBackupWizardController controller = getTypedBaseController("ScheduledBackupWizard");
        
        Database database = null;
        
        ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
        
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof DBTable) {
                database = ((DBTable) itemValue).getDatabase();
            }
        }
        
        try {
            controller.init(currentSession.getService(), currentSession.getConnectionParam(), database);
            final Parent p = controller.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Export Data As Batch Script Wizard");

            dialog.showAndWait();
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }
    
    public void showTablePropertiesDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        
        ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
        
        ViewTablePropertiesController controller = getTypedBaseController("ViewTableProperties");
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof DBTable) {
                
                DBTable table = (DBTable) itemValue;
                
                controller.init(currentSession.getService(), table);
                final Parent p = controller.getUI();
                dialog.initStyle(StageStyle.UTILITY);
                
                final Scene scene = new Scene(p);
                dialog.setScene(scene);
                dialog.initModality(Modality.WINDOW_MODAL);
                dialog.initOwner(getController().getStage());
                dialog.setTitle("Table Properties - `" + table.getDatabase().getName() + "` in `" + table.getName() + "`");
                
                dialog.showAndWait();
            }
        }
    }
    
    public void showDuplicateTableDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
        
        DuplicateTableController controller = getTypedBaseController("DuplicateTable");
                
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getValue();            
            if (itemValue instanceof DBTable) {
                
                DBTable table = (DBTable)itemValue;
                try {
                    controller.init(currentSession, table);
                    final Parent p = controller.getUI();
                    dialog.initStyle(StageStyle.UTILITY);

                    final Scene scene = new Scene(p);
                    dialog.setScene(scene);
                    dialog.initModality(Modality.WINDOW_MODAL);
                    dialog.initOwner(getController().getStage());
                    dialog.setTitle("Duplicate Table");

                    dialog.showAndWait();
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        }
    }
    
    private String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, queryId);
    }
    
    private String getFormatedQuery(String queryId, Object...values) {
        return String.format(getQuery(queryId), values);
    }
    
    
    @Override
    public String getInfo() { 
        return "";
    }
}
