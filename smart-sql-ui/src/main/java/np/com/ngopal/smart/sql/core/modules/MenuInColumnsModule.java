
package np.com.ngopal.smart.sql.core.modules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class MenuInColumnsModule extends AbstractStandardModule {

    private Image tableImage;
        
    private MenuItem manageItem;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 0;
    }
    
    

    @Override
    public boolean install() {
        
        manageItem = getManageColumnsMenuItem(null);
        
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem obj) {
        
        boolean isColumn = obj.getValue() instanceof String && obj.getValue().toString().equals("Columns");
        
        manageItem.setDisable(!isColumn);
        
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("columns", new ArrayList<>(Arrays.asList(new MenuItem[]{manageItem}))), Hooks.QueryBrowser.COLUMNS);        

        return map;
    }
    

    private MenuItem getManageColumnsMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */

        MenuItem manageColumnsMenuItem = new MenuItem("Manage Columns",
        new ImageView(SmartImageProvider.getInstance().getColumnManage_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        manageColumnsMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        manageColumnsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                if (tableImage == null) {
                    tableImage = SmartImageProvider.getInstance().getTableTab_image();
                }
                
                ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                
                ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(
                        currentSession);
                
                DBTable table = DatabaseCache.getInstance().getTable(currentSession.getConnectionParam().cacheKey(), 
                    object.get().getParent().getParent().getParent().getValue().toString(),
                    object.get().getParent().getValue().toString());
                
                AlterTableController altertableController = null;
                Tab alterTab = null;

                ConnectionTabContentController connTab = (ConnectionTabContentController) getController().getTabControllerBySession(currentSession);
                List<Tab> tabs = connTab.findControllerTabs(AlterTableController.class);
                if (tabs != null && !tabs.isEmpty()) {

                    for (Tab t: tabs) {
                        if (((AlterTableController)t.getUserData()).getTable().equals(table)) {
                            altertableController = (AlterTableController)t.getUserData();
                            alterTab = t;
                            break;
                        }
                    }
                }
                
                if (altertableController != null) {
                    
                    altertableController.manageColumns();
                    getController().showTab(alterTab, currentSession);
                    
                } else {
                
                    altertableController = ((MainController)getController()).getTypedBaseController("AlterTable");
                    altertableController.setService(currentSession.getService());

                    alterTab = new Tab(object.get().getParent().getValue().toString());
                    alterTab.setGraphic(new ImageView(tableImage));
                    alterTab.setUserData(altertableController);

                    Parent p = altertableController.getUI();
                    alterTab.setContent(p);

                    altertableController.init(alterTab, table, currentSession, true);

                    getController().addTab(alterTab, currentSession);
                    getController().showTab(alterTab, currentSession);
                }
            }
        });
        
        return manageColumnsMenuItem;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
