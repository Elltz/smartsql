package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ScheduleParams;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.ui.components.DatabaseSelectableTreeView;
import np.com.ngopal.smart.sql.utils.DumpUtils;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DatabaseCopierController extends BaseController implements Initializable {

    @Inject
    private ConnectionParamService connectionService;
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private HBox container;

    /**
     * The Container is the parent into which the objects will be added since
     * the objects {storedProcs,tables,events} will all be retrieved
     * programmatically.
     */
    @FXML
    private VBox objectsContainer;

    @FXML
    private Label labelForDatabaseName;

    @FXML
    private VBox targetContainer;

    @FXML
    private ComboBox<ConnectionParams> targetConnectionPicker;

    /**
     * THis combox contains the database to which the user to choose as the
     * database to copy to.
     */
    @FXML
    private ComboBox<String> targetDatabasePicker;

    /**
     * Radio button to specify whether the user wants to copy both Structure and
     * Data. It is in TypeToggleGroup.
     */
    @FXML
    private RadioButton structDataRB;

    /**
     * Radio button to specify whether the user wants to copy only Structure It
     * is in TypeToggleGroup.
     */
    @FXML
    private RadioButton structRB;

    /**
     * To specify whether to drop if object if exist in target database
     */
    @FXML
    private CheckBox dropCheckbox;

    /**
     * To specify whether to use bulk insertion
     */
    @FXML
    private CheckBox insertCheckbox;

    /**
     * To specify whether to ignore definer
     */
    @FXML
    private CheckBox definerCheckbox;

    /**
     * To specify whether to copy associated triggers.
     */
    @FXML
    private CheckBox associatetriggerCheckbox;
    
    @FXML
    private Label statusLabel;

    @FXML
    private Button summaryButton;

    @FXML
    private Button copyButton;

    @FXML
    private Button closeButton;

    @FXML
    private ImageView labelImage;

    @FXML
    private DatabaseSelectableTreeView dataTree;

    private ToggleGroup typeToggleGroup;
    
    private Stage stage;
    private ConnectionSession session;
    private String database;
    
    private boolean treeInitiated = false;
    private boolean needRefresh = false;
    private Border whiteLine;
    private Background ghostB;
    
    private String summaryStatus;

    boolean withoutPassword = false;
//    boolean needUpdateDatabases = false;
            
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }    
    
    @FXML
    public void loadDatabasesAction(ActionEvent event) {
        
        ConnectionParams params = targetConnectionPicker.getSelectionModel().getSelectedItem();
        if (params == null) {
            return;
        }
        
        makeBusy(true);
        targetDatabasePicker.getItems().clear();
                
        String password = null;
        if (params.getPassword() == null || params.getPassword().isEmpty()) {
            password = DialogHelper.showPassword(getController(), "Connection", "Enter password", "", "", getStage());
            if (password == null || password.isEmpty()) {
                makeBusy(false);
                return;
            }
        }

        if (password != null) {
            params.setPassword(password);
            params.setEnteredPassword(password);
            withoutPassword = true;
        } else {
            withoutPassword = false;
        }
        
        Thread th = new Thread(() -> {
            
            if (params != null) {
                try {
                    MysqlDBService service = new MysqlDBService(null);
                    service.connect(params);
                    List<Database> connectionDatabases = service.getDatabases();

                    List<String> databases = new ArrayList<>();
                    for (Database o: connectionDatabases) {
                        databases.add(o.toString());
                    }
                    
                    Platform.runLater(() -> {
                        targetDatabasePicker.setDisable(false);
                        targetDatabasePicker.getItems().addAll(databases);  
                        makeBusy(false);
                    });

                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                        makeBusy(false);
                    });
                }  finally {

                    // clear password if we set it
                    if (withoutPassword) {
                        params.setPassword(null);
                    }
                }
            }
        });
        th.setDaemon(true);
        th.start();
        
        
    }
    
    private void disableDatabases() {
        targetDatabasePicker.getItems().clear();
        targetDatabasePicker.setDisable(true);
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ghostB = new Background(new BackgroundFill(Color.WHITESMOKE,
                CornerRadii.EMPTY, Insets.EMPTY));
        whiteLine = new Border(new BorderStroke(Color.WHITE,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0.3)));
        typeToggleGroup = new ToggleGroup();
        structDataRB.setToggleGroup(typeToggleGroup);
        structRB.setToggleGroup(typeToggleGroup);
        structDataRB.setOnAction(onActionForNode);
        structRB.setOnAction(onActionForNode);
        dropCheckbox.setOnAction(onActionForNode);
        insertCheckbox.setSelected(true);
        insertCheckbox.setOnAction(onActionForNode);
        definerCheckbox.setOnAction(onActionForNode);
        associatetriggerCheckbox.setOnAction(onActionForNode);
        mainUI.setBackground(ghostB);
        objectsContainer.setBackground(Background.EMPTY);
        targetContainer.setBackground(Background.EMPTY);
        //objectsContainer.setBorder(whiteLine);
        //targetContainer.setBorder(whiteLine);
        labelImage.setImage(SmartImageProvider.getInstance().getDatabaseDump_image());
        
        targetDatabasePicker.setDisable(true);
        new AutoCompleteComboBoxListener<>(targetDatabasePicker);                   
        
        targetConnectionPicker.setButtonCell(getConnectionParamsListCell());
        targetConnectionPicker.setCellFactory(p -> getConnectionParamsListCell());
        
        targetConnectionPicker.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ConnectionParams> observable, ConnectionParams oldValue, ConnectionParams newValue) -> {
            
            disableDatabases();
            withoutPassword = false;
            
            targetDatabasePicker.getItems().clear();
            targetDatabasePicker.setValue(null);
            
            log.error("selectedItemProperty: " + newValue);
        });        
        
        new AutoCompleteComboBoxListener<>(targetConnectionPicker);        
        targetConnectionPicker.setConverter(new StringConverter<ConnectionParams>() {
            @Override
            public String toString(ConnectionParams object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public ConnectionParams fromString(String string) {
                for (ConnectionParams cp: targetConnectionPicker.getItems()) {
                    if (cp.getName().equals(string)) {
                        return cp;
                    }
                }
                
                return null;
            }
        });
    }
    
    @FXML
    public void close(ActionEvent event) {
        stage.close();
    }

    @FXML
    public void copy(ActionEvent event) {
        
        makeBusy(true);
        
        statusLabel.setText("");
        
        if (!dataTree.hasObjects()) {
            statusLabel.setText("There was nothing to be copied");
            makeBusy(false);
            return;
        }
        
        String selectedDatabase = getSelectedDatabase();
        if (selectedDatabase == null || selectedDatabase.isEmpty()) {
            DialogHelper.showInfo(getController(), "SMART-MYSQL ERROR", "Please select database", null, getStage());
            makeBusy(false);
            return;
        }
        
        ScheduleParams params = new ScheduleParams();
        params.setSelectedData(dataTree.getSelectedDataAsString());
        params.setIncludeDrop(dropCheckbox.isSelected());
        params.setDatabase(database);
        params.setExportOnlyStructure(structRB.isSelected());
        params.setCreateBulkInsert(insertCheckbox.isSelected());
        params.setCreateCompleteInsert(true);
        params.setIgnoreDefiner(definerCheckbox.isSelected());
        
        summaryStatus = "";
        summaryButton.setDisable(true);
        
//        getUI().setDisable(true);
//        getUI().setCursor(Cursor.WAIT);
        
        
        Thread th = new Thread(() -> {
            
            ConnectionParams cp = targetConnectionPicker.getSelectionModel().getSelectedItem();
            
            try {
                
                if (cp.getEnteredPassword() != null) {
                    cp.setPassword(cp.getEnteredPassword());
                }
                
                ((AbstractDBService)session.getService()).setServiceBusy(true);
                DumpUtils.copyDatabaseTo(session.getService(), cp, params, selectedDatabase, associatetriggerCheckbox.isSelected(), new DumpUtils.CopyStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", errorMessage, th, stage);

                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            th.printStackTrace(pw);

                            summaryStatus += errorMessage + "\n" + sw.toString() + "\n\n";
                            summaryButton.setDisable(false);

                            needRefresh = true;
                            
                            makeBusy(false);
                        });
                    }

                    @Override
                    public void success(String summary) {
                        Platform.runLater(() -> {
                            DialogHelper.showInfo(getController(), "Info", "Copied successfuly", null, stage);

                            summaryStatus = summary;
                            summaryButton.setDisable(false);

                            needRefresh = true;
                            
                            makeBusy(false);
                        });
                    }

                    @Override
                    public void info(String info) {
                        Platform.runLater(() -> statusLabel.setText(info));
                    }

                    @Override
                    public void progress(int progress) {
                    }

                });
            } finally {
                
                ((AbstractDBService)session.getService()).setServiceBusy(false);
                
                if (cp.getEnteredPassword() != null) {
                    cp.setPassword(null);
                }
                
//                Platform.runLater(() -> {
//                    getUI().setDisable(false);
//                    getUI().setCursor(Cursor.DEFAULT);
//                });
            }
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void summary(ActionEvent event) {
        DialogHelper.showInfo(getController(), "Summary", summaryStatus, null, stage);
    }
    
    private String getSelectedDatabase() {
        String selected = targetDatabasePicker.getSelectionModel().getSelectedItem();
        return selected != null ? selected : targetDatabasePicker.getEditor().getText();
    }
    
    private ListCell<ConnectionParams> getConnectionParamsListCell() {
        return new ListCell<ConnectionParams>() {
            @Override
            protected void updateItem(ConnectionParams item, boolean empty) {
                super.updateItem(item, empty);
                
                if (item != null) {
                    setText(item.getName());
                }
            }
        };
    }
    
    /*called when structRB or structDataRB is checked
     * 0 for StructDataRB and 1 for structRB. used short.
     */
    private void onStructChecked(short who) {
        if (who == (short) 0) {
            //for StructDataRB
            insertCheckbox.setDisable(false);
        } else {
            insertCheckbox.setDisable(true);
        }

    }

    private final EventHandler<ActionEvent> onActionForNode = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            Node node = (Node) event.getSource();

            if (node == structRB) {
                onStructChecked((short) 1);
            } else if (node == structDataRB) {
                onStructChecked((short) 0);
            } else if (node == insertCheckbox) {

            } else if (node == dropCheckbox) {

            } else if (node == definerCheckbox) {

            } else if (node == associatetriggerCheckbox) {

            }
        }
    };

    

    public boolean showNow(Window owner) {
        
        needRefresh = false;
                
        if (!treeInitiated) {
            return false;
        }
        if (stage == null) {
            stage = new Stage(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setResizable(true);
            stage.setScene(new Scene(mainUI));
            stage.setTitle("Copy DB to different Host/DB");
            stage.setMinWidth(610);
        }
        if (owner != null) {
            stage.initOwner(owner);
        }
        stage.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue.doubleValue() > oldValue.doubleValue()) {
                
                double value = (newValue.doubleValue() - oldValue.doubleValue())/2;
                dataTree.setPrefWidth(dataTree.getPrefWidth()
                        + value);
                objectsContainer.setPrefWidth(objectsContainer.getPrefWidth()
                        + value);
                targetContainer.setPrefWidth(targetContainer.getPrefWidth()
                        + value);
                container.setPrefWidth(container.getPrefWidth()
                        + value);
                
            } else if (newValue.doubleValue() < oldValue.doubleValue()) {
                
                double value = (oldValue.doubleValue() - newValue.doubleValue())/2;
                dataTree.setPrefWidth(dataTree.getPrefWidth()
                        - value);
                objectsContainer.setPrefWidth(objectsContainer.getPrefWidth()
                        - value);
                targetContainer.setPrefWidth(targetContainer.getPrefWidth()
                        - value);
                container.setPrefWidth(container.getPrefWidth()
                        - value);
            }
        });
        stage.showAndWait();
        
        return needRefresh;
    }

    
    public void insertSession(ConnectionSession session, String database) {
        insertSession(session, database, null);
    }
    public void insertSession(ConnectionSession session, String database, Map<String, String> selectedData) {
        if (session == null) {
            return;
        }
        this.session = session;
        this.database = database;
        
        targetDatabasePicker.getSelectionModel().select(database);
        
        //change the label
        labelForDatabaseName.setText(session.getConnectionParam().getAddress() + " - " + database);
        
        initTree(selectedData);
    }
    
    

    private void initTree(Map<String, String> selectedData) {      
        
        makeBusy(true);
        treeInitiated = true;
        Thread th = new Thread(() -> {
            Database db = (Database) session.getConnectionParam().getSelectedDatabase();
            if (db != null) {
                try {
                    db = DatabaseCache.getInstance().syncDatabase(db.getName(), session.getConnectionParam().cacheKey(), (AbstractDBService) session.getService());
                } catch (SQLException ex) {
                    log.error("Error", ex);
                    db = null;
                }
                
                if (db != null) {
                    try {
                        db.setEvents(session.getService().getEvents(db));
                    } catch (SQLException ex) {
                    }

                    try {
                        db.setFunctions(session.getService().getFunctions(db));
                    } catch (SQLException ex) {
                    }

                    try {
                        db.setStoredProc(session.getService().getStoredProcs(db));
                    } catch (SQLException ex) {
                    }

                    try {
                        db.setTriggers(session.getService().getTriggers(db));
                    } catch (SQLException ex) {
                    }

                    try {
                        db.setViews(session.getService().getViews(db));
                    } catch (SQLException ex) {
                    }
                }
            }

            Database staticDB = db;
            Platform.runLater(() -> {
                dataTree.loadData(session.getService(), staticDB, selectedData == null, selectedData);

                try {
                    loadUpDatabase();
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", "Error loading database", ex);
                }
                makeBusy(false);
            });
        });
        th.setDaemon(true);
        th.start();
    }

    private void loadUpDatabase() throws SQLException {
        for (ConnectionParams cp: connectionService.getAll()) {
            targetConnectionPicker.getItems().add(cp);
        }
        
        targetConnectionPicker.getSelectionModel().select(session.getConnectionParam());
    }
    
    
}
