package np.com.ngopal.smart.sql.modules;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.AbstractConnectionModule;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.DesignerErrDiagramService;
import np.com.ngopal.smart.sql.model.Advanced;
import np.com.ngopal.smart.sql.model.Checksum;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.DelayKeyWrite;
import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.Rowformat;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataType;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.controller.CreateDatabaseFromSchemaController;
import np.com.ngopal.smart.sql.ui.controller.ForwardEngineerDialogController;
import np.com.ngopal.smart.sql.ui.controller.OpenSchemaDesignerDialogController;
import np.com.ngopal.smart.sql.ui.controller.PublishSchemaController;
import np.com.ngopal.smart.sql.ui.controller.ReverseEngineerDialogController;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerImportTablesController;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;
import np.com.ngopal.smart.sql.ui.controller.SearchPublicSchemaController;
import np.com.ngopal.smart.sql.utils.LinkedProperties;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaDesignerModule extends AbstractConnectionModule {
    
    @Inject
    private DesignerErrDiagramService diagramService;
    
    @Getter
    private final SimpleObjectProperty<DesignerErrDiagram> selectedDiagram = new SimpleObjectProperty<>();
    
    private final BooleanProperty diagramHasChanges = new SimpleBooleanProperty(false);
    private final BooleanProperty diagramHasRedo = new SimpleBooleanProperty(false);
    private final BooleanProperty diagramHasUndo = new SimpleBooleanProperty(false);
    private final BooleanProperty diagramHasSelectedNode = new SimpleBooleanProperty(false);
    private final BooleanProperty diagramHasPaste = new SimpleBooleanProperty(false);
    
    private final BooleanProperty diagramCanPublish = new SimpleBooleanProperty(false);
    private final BooleanProperty diagramCanUnPublish = new SimpleBooleanProperty(false);
    
    public static List<DesignerDataType> TYPES;
    public static List<String> COLLATIONS;
    
    private ConnectionSession connectionSession;
    
    private final ObservableList<ErrDiagram> errDiagrams = FXCollections.observableArrayList();
    
    @Getter
    private ErrDiagram selectedErrDiagram;
    
    private boolean opened = false;
    
    private MenuItem publishMi;
    private MenuItem unpublishMi;
    private MenuItem publishSearchMi;
    private MenuItem createDabaseMi;
    
    private MenuItem newDiagramMi;
    private MenuItem saveMi;
    private MenuItem saveAsMi;
    private MenuItem exportToPdfMi;
    private MenuItem exportToPngMi;
    private MenuItem importTablesMi;
    private MenuItem engineReverseMi;
    private MenuItem engineForwardMi;
    private MenuItem engineReverseFileMi;
    private MenuItem engineForwardFileMi;
    private MenuItem templatesMi;
    private MenuItem increaseWidthMi;
    private MenuItem increaseHeightMi;
    
    private MenuItem undoMi;
    private MenuItem redoMi;
    private MenuItem cutMi;
    private MenuItem copyMi;
    private MenuItem pastMi;
    private MenuItem deleteMi;
    private MenuItem editMi;
    
    private MenuItem zoomInMi;
    private MenuItem zoomOutMi;
    private MenuItem zoom100Mi;
    
    //private Map<DesignerErrDiagram, ErrDiagram> diagramsMap = new HashMap<>();
    
    static {

        TYPES = new ArrayList<>();
        try {
            Properties properties = new Properties();
            properties.load(MySQLDataType.class.getResourceAsStream("errdatatypes.properties"));
            
            for (Object kObj : properties.keySet()) {
                String key = kObj.toString();
                
                int order = 0;
                // obly 4 values BINARY | UNSIGNED | ZERO FILL | AUTO INCREMENTAL
                Boolean[] values = new Boolean[]{false, false, false, false};
                String value = properties.get(key).toString();
                if (value != null && !value.isEmpty()) {                    
                    int i = 0;
                    String[] results = value.split("\\|");
                    for (String v: results) {
                        if (i == 0) {
                            order = Integer.parseInt(v);
                            if (results.length == 1) {
                                break;
                            }
                        } else {
                            values[i - 1] = Boolean.parseBoolean(v);
                        }
                        i++;
                    }
                }
                
                // in property file keys was like ------
                // we change all for -
                if (key.startsWith("-")) {
                    key = "-";
                }
                
                TYPES.add(new DesignerDataType(key, order, values[0], values[1], values[2], values[3]));
            }
            
            TYPES.sort((DesignerDataType o1, DesignerDataType o2) -> {
                return o1.order - o2.order;
            });
            
        } catch (IOException ex) {
            Logger.getLogger(MySQLDataType.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        COLLATIONS = new ArrayList<>();
        try {
            
            LinkedProperties properties = new LinkedProperties();
            properties.load(MySQLDataType.class.getResourceAsStream("collations.properties"));
            
            Enumeration<Object> enumer = properties.keys();
            while (enumer.hasMoreElements()) {
                Object kObj = enumer.nextElement();
                String key = kObj.toString();                
                String value = properties.get(key).toString();
                
                COLLATIONS.add(value + " - " + key);
            }            
            
        } catch (IOException ex) {
            Logger.getLogger(MySQLDataType.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteErrDiagram(ErrDiagram diagram) {
        if (diagram != null && showConfirm("Delete diagram", "Do you want to delete '" + diagram.getName() + "' diagram?") == DialogResponce.OK_YES) {
            
            diagramService.delete(diagram);
            errDiagrams.remove(diagram);
            
            if (diagram.equals(selectedErrDiagram)) {
                clearDiagram();
            }
        }
    }
    
    public void unpublishErrDiagram(ErrDiagram diagram) {
        if (diagram != null && showConfirm("Unpublish diagram", "Do you want to unpulish '" + diagram.getName() + "' diagram?") == DialogResponce.OK_YES) {
            
            if (diagram.getPublicId() != null) {
                String diagramUser = diagram.getUserId();
                String smartUser = null;
                SmartsqlSavedData smartData = getMainController().getSmartData();
                if (smartData != null) {
                    smartUser = smartData.getEmailId();
                }            

                if (diagramUser != null && smartUser != null && diagramUser.equals(smartUser)) {
                    try {
                        PublicDataAccess.getInstanсe().deleteErrDiagram(diagram.getPublicId());                        
                    }  catch (PublicDataException ex) {
                        showError("Delete error", ex.getMessage(), ex);            
                        return;
                    }
                } else {
                    showError("Access error", "Only owner can unpublish public diagram!", null);
                    return;
                }
            } else {
                diagramService.delete(diagram);
            }
            
            errDiagrams.remove(diagram);
            
            selectedErrDiagram.setId(null);
            selectedErrDiagram.setPublicId(null);
            
            diagramService.save(selectedErrDiagram);
            
            reloadDiagrams();
            openDiagram(selectedErrDiagram);
        }
    }
    
    
    @Override
    public synchronized boolean canOpenConnection(ConnectionParams params) {
        return !opened;
    }
    
    @Override
    public void connectionOpened(ConnectionParams params) {
        opened = true;
        super.connectionOpened(params);
    }
    
    
    
    @Override
    public boolean canReinitConnection(ConnectionParams params) {
        return !(getMainController().getTabControllerBySession(getMainController().getSelectedConnectionSession().get()) instanceof SchemaDesignerTabContentController);
    }
    
    @Override
    public synchronized void reinitConnection(ConnectionParams params) {
        if (params != null) {
            DesignerErrDiagram diagram = new DesignerErrDiagram();
            
            if (params.isMakeBackwardEngine()) {
            
                List<String> list = params.getDatabase();
                
                if (list != null && list.size() == 1) {
                    diagram.host().set(params.getAddress() + ":" + (params.getPort() > 0 ? params.getPort() : 3306));
                    diagram.database().set(list.get(0));

                    DesignerErrDiagram diagram0 = diagram;
                    Platform.runLater(() -> {
                        showReverseEngineer(diagram0); 
                    });

                }
                
            } else if (params.getErrDiagram() != null) {
                
                Platform.runLater(() -> {
                    if (selectedDiagram.get() != null) {
                        DialogResponce responce = showConfirm("Save diagram", "Do you want save current Diagram?");
                        if (DialogResponce.OK_YES == responce) {
                            saveDiagram();
                        }
                    }

                    openDiagram(params.getErrDiagram());
                });
                
                
            }else {
                Platform.runLater(() -> {
                    if (selectedDiagram.get() != null) {
                        DialogResponce responce = showConfirm("Save diagram", "Do you want save current Diagram?");
                        if (DialogResponce.OK_YES == responce) {
                            saveDiagram();
                        }
                    }

                    diagram.name().set(params.getNewSchemaName() != null ? params.getNewSchemaName() : diagram.getDatabaseName());
                    openDesignerDiagram(diagram); 
                });
                
            }
        }
    }

    @Override
    public boolean install() {
        super.install();
        
        selectedDiagram.addListener((ObservableValue<? extends DesignerErrDiagram> observable, DesignerErrDiagram oldValue, DesignerErrDiagram newValue) -> {
            if (diagramHasChanges.isBound()) {
                diagramHasChanges.unbind();
            }
            
            if (newValue != null) {
                diagramHasChanges.bind(newValue.changed());
            } else {
                diagramHasChanges.set(false);
            }            
              
            
            if (diagramCanPublish.isBound()) {
                diagramCanPublish.unbind();
            }
            
            diagramCanPublish.set(newValue != null);
            
            if (diagramCanUnPublish.isBound()) {
                diagramCanUnPublish.unbind();
            }
            
            if (newValue != null) {
                SmartsqlSavedData smartData = getMainController().getSmartData();
                if (smartData != null && smartData.getEmailId() != null) {
                    diagramCanUnPublish.bind(newValue.domain().and(Bindings.equal(newValue.user(), smartData.getEmailId())));
                } else {
                    diagramCanUnPublish.set(false);
                }
            } else {
                diagramCanUnPublish.set(false);
            }
            
            
            if (diagramHasUndo.isBound()) {
                diagramHasUndo.unbind();
            }
            
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            
            if (newValue != null) {
                diagramHasUndo.bind(designerController.hasUndo());
            } else {
                diagramHasUndo.set(false);
            }
            
            
            if (diagramHasRedo.isBound()) {
                diagramHasRedo.unbind();
            }
            
            if (newValue != null) {
                diagramHasRedo.bind(designerController.hasRedo());
            } else {
                diagramHasRedo.set(false);
            }
            
            
            if (diagramHasSelectedNode.isBound()) {
                diagramHasSelectedNode.unbind();
            }
            
            if (newValue != null) {
                diagramHasSelectedNode.bind(Bindings.isNotNull(designerController.selectedNode()));
            } else {
                diagramHasSelectedNode.set(false);
            }
            
            if (diagramHasPaste.isBound()) {
                diagramHasPaste.unbind();
            }
            
            if (newValue != null) {
                diagramHasPaste.bind(designerController.hasPaste());
            } else {
                diagramHasPaste.set(false);
            }
        });
        
        publishMi = new MenuItem("Publish", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/publish.png")));
        publishMi.disableProperty().bind(diagramCanPublish.not());
        publishMi.setOnAction((ActionEvent event) -> {
            showPublish();
        });
        
        unpublishMi = new MenuItem("Un Publish", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/unpublish.png")));
        unpublishMi.disableProperty().bind(diagramCanUnPublish.not());
        unpublishMi.setOnAction((ActionEvent event) -> {
            unpublishErrDiagram(selectedErrDiagram);
        });
        
        publishSearchMi = new MenuItem("Search public schemas", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/publish_search.png")));
        publishSearchMi.setOnAction((ActionEvent event) -> {
            showPublishSearch();
        });
        
        createDabaseMi = new MenuItem("Create database from schema", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/publish_database.png")));
        createDabaseMi.setOnAction((ActionEvent event) -> {
            showCreateDatabase();
        });
        
        newDiagramMi = new MenuItem("New diagram", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/diagramNew.png")));
        newDiagramMi.setOnAction((ActionEvent event) -> {
            if (selectedDiagram.get() != null && selectedDiagram.get().changed().get()) {
                DialogResponce responce = showConfirm("Save diagram", "Do you want save current Diagram?");
                if (DialogResponce.OK_YES == responce) {
                    saveDiagram();
                } else if (DialogResponce.CANCEL == responce) {
                    return;
                }
            }
            
            try {
                OpenSchemaDesignerDialogController controller = getTypedBaseController("OpenSchemaDesignerDialog");
                controller.init((List<Database>)getMainController().getSelectedConnectionSession().get().getService().getDatabases());

                Parent p = controller.getUI();
                Scene scene = new Scene(p);
                scene.getStylesheets().add(QueryTabModule.class.getResource("css/dialog-style.css").toExternalForm());
                Stage dialog = new Stage();
                dialog.initStyle(StageStyle.UTILITY);
                dialog.setScene(scene);
                dialog.initModality(Modality.WINDOW_MODAL);
                dialog.initOwner(getController().getStage());

                dialog.setTitle("Select");
                dialog.showAndWait();

                String selectedDatabase = controller.getSelectedDatabase();
                String selectedSchemaName = controller.getSchemaName();

                if (selectedDatabase == null && selectedSchemaName == null) {
                    return;
                }

                ConnectionParams cp = getMainController().getSelectedConnectionSession().get().getConnectionParam().copyOnlyParams();
                cp.setSelectedModuleClassName(SchemaDesignerModule.class.getName());
                if (selectedDatabase != null) {
                    cp.setDatabase(Arrays.asList(new String[]{selectedDatabase}));
                    cp.setMakeBackwardEngine(true);
                } else {
                    cp.setNewSchemaName(selectedSchemaName);
                }

                reinitConnection(cp);
            } catch (SQLException ex) {
                showError("Error create new diagram", ex.getMessage(), ex);
            }
        });
        
        saveMi = new MenuItem("Save", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/save.png")));
        saveMi.disableProperty().bind(Bindings.isNull(selectedDiagram).or(diagramHasChanges.not()));
        saveMi.setOnAction((ActionEvent event) -> {
            saveDiagram();
        });
        
        saveAsMi = new MenuItem("Save as", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/save_as.png")));
        saveAsMi.disableProperty().bind(Bindings.isNull(selectedDiagram));
        saveAsMi.setOnAction((ActionEvent event) -> {
            String result = showInput("Save as", "Input diagram name", "");
            if (result != null && !result.isEmpty()) {
                saveAsDiagram(result);
            }            
        });
                
        exportToPdfMi = new MenuItem("Export to PDF", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/exportToPDF.png")));
        exportToPdfMi.disableProperty().bind(Bindings.isNull(selectedDiagram));
        exportToPdfMi.setOnAction((ActionEvent event) -> {
            exportToPdf();
        });
        
        exportToPngMi = new MenuItem("Export to PNG", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/exportToPNG.png")));
        exportToPngMi.disableProperty().bind(Bindings.isNull(selectedDiagram));
        exportToPngMi.setOnAction((ActionEvent event) -> {
            exportToPng();
        });
        
        
        importTablesMi = new MenuItem("Import tables", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/importTables.png")));
        importTablesMi.setOnAction((ActionEvent event) -> {
            showImportTable();
        });
        
        engineReverseMi = new MenuItem("Backward Engine: Get the DB tables from to MySQL server database", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/engineReverse.png")));
        engineReverseMi.setOnAction((ActionEvent event) -> {
            showReverseEngineer();
        });
        
        engineReverseFileMi = new MenuItem("Backward Engine", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/engineReverse.png")));
        engineReverseFileMi.setOnAction((ActionEvent event) -> {
            showReverseEngineer();
        });
        
        engineForwardMi = new MenuItem("Forward Engine: Apply the DB changes to MySQL server", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/engineForward.png")));
        engineForwardMi.setOnAction((ActionEvent event) -> {
            showForwardEngineer();
        });
        
        engineForwardFileMi = new MenuItem("Forward Engine", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/engineForward.png")));
        engineForwardFileMi.setOnAction((ActionEvent event) -> {
            showForwardEngineer();
        });
        
        templatesMi = new MenuItem("Show / Hide templates", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/alter_storedprocs.png")));
        templatesMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.showHideRightPane();
        });
        
        increaseWidthMi = new MenuItem("Increase diagram width", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/diagramIncreaseWidth.png")));
        increaseWidthMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.increaseWidth();
        });
        
        increaseHeightMi = new MenuItem("Increase diagram hight", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/diagramIncreaseHeight.png")));
        increaseHeightMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.increaseHeight();
        });

        
        
        undoMi = new MenuItem("Undo", new ImageView(StandardDefaultImageProvider.getInstance().getArrowUndo_image()));
        undoMi.disableProperty().bind(diagramHasUndo.not());
        undoMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.undo();
        });
                
        redoMi = new MenuItem("Redo", new ImageView(StandardDefaultImageProvider.getInstance().getArrowRedo_image()));
        redoMi.disableProperty().bind(diagramHasRedo.not());
        redoMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.redo();
        });
        
        
        cutMi = new MenuItem("Cut", new ImageView(SmartImageProvider.getInstance().getCut_image()));
        cutMi.disableProperty().bind(diagramHasSelectedNode.not());
        cutMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.cutSelected();
        });
        
        copyMi = new MenuItem("Copy", new ImageView(SmartImageProvider.getInstance().getCopy_image()));
        copyMi.disableProperty().bind(diagramHasSelectedNode.not());
        copyMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.copySelected();
        });
        
        pastMi = new MenuItem("Paste", new ImageView(SmartImageProvider.getInstance().getPaste_image()));
        pastMi.disableProperty().bind(diagramHasPaste.not());
        pastMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.pasteSelected();
        });
        
        deleteMi = new MenuItem("Delete", new ImageView(SmartImageProvider.getInstance().getDelete_image()));
        deleteMi.disableProperty().bind(diagramHasSelectedNode.not());
        deleteMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.deleteSelected();
        });
        
        editMi = new MenuItem("Edit", new ImageView(SmartImageProvider.getInstance().getModify_image()));
        editMi.disableProperty().bind(diagramHasSelectedNode.not());
        editMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.editSelected();
        });
        
        
        zoomInMi = new MenuItem("Zoom in", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/zoomIn.png")));
        zoomInMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.zoomIn();
        });
        
        zoomOutMi = new MenuItem("Zoom out", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/zoomOut.png")));
        zoomOutMi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.zoomOut();
        });
        
        zoom100Mi = new MenuItem("Zoom 100%", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/designer/zoom100.png")));
        zoom100Mi.setOnAction((ActionEvent event) -> {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.zoom100();
        });
        
        checkDBMenuITems(selectedDiagram.get(), getMainController().getSelectedConnectionSession().get());
        selectedDiagram.addListener((ObservableValue<? extends DesignerErrDiagram> observable, DesignerErrDiagram oldValue, DesignerErrDiagram newValue) -> {
            checkDBMenuITems(newValue, getMainController().getSelectedConnectionSession().get());
        });
        
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FILE, new MenuItem[]{newDiagramMi, saveMi, saveAsMi, engineReverseFileMi, engineForwardFileMi, exportToPdfMi, exportToPngMi, importTablesMi});
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{undoMi, redoMi, cutMi, copyMi, pastMi, deleteMi, editMi});
                
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.VIEW, new MenuItem[]{zoomInMi, zoomOutMi, zoom100Mi, increaseWidthMi, increaseHeightMi});
        
        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{newDiagramMi, saveMi, saveAsMi, publishMi, unpublishMi, publishSearchMi, createDabaseMi, exportToPdfMi, exportToPngMi, importTablesMi, engineReverseMi, engineForwardMi, templatesMi, increaseWidthMi, increaseHeightMi});
        
        getMainController().getSelectedConnectionSession().addListener((ObservableValue<? extends ConnectionSession> observable, ConnectionSession oldValue, ConnectionSession newValue) -> {
            checkDBMenuITems(selectedDiagram.get(), newValue);
        });
        
        return true;
    }
    
    
    
    public SchemaDesignerTabContentController getSchemaDesignerController() {
        return (SchemaDesignerTabContentController) getMainController()
                .getTabControllerBySession(getMainController().getLastOpenedConnectionSession());
    }
    
    public void showForwardEngineer() {
        showForwardEngineer(null);
    }
    
    public void showForwardEngineer(DesignerErrDiagram diagram) {
        
        ForwardEngineerDialogController controller = getTypedBaseController("ForwardEngineerDialog");
        
        if (getMainController().getSelectedConnectionSession().get() != null) {

            ConnectionSession session = getMainController().getSelectedConnectionSession().get();
            if (session != null) {
                SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            
                controller.init(session.getService(), designerController, diagram != null ? diagram.database().get() : null);    
                
                final Stage dialog = new Stage();
                final Parent p = controller.getUI();
                dialog.initStyle(StageStyle.UTILITY);

                final Scene scene = new Scene(p);
                dialog.setScene(scene);
                dialog.initModality(Modality.WINDOW_MODAL);
                dialog.initOwner(getController().getStage());
                dialog.setTitle("Forward engineer");

                dialog.showAndWait();
            }
        }
    }
    
    public void showReverseEngineer() {
        showReverseEngineer(null);
    }
    
    public void showReverseEngineer(DesignerErrDiagram diagram) {
        
        if (selectedDiagram.get() != null && selectedDiagram.get().changed().get()) {
            DialogResponce responce = showConfirm("Save diagram", "Do you want save current Diagram?");
            if (DialogResponce.OK_YES == responce) {
                saveDiagram();
            } else if (DialogResponce.CANCEL == responce) {
                return;
            }
        }
        
        String selectedDatabase = null;
        if (diagram != null) {
            selectedDatabase = diagram.database().get();
            
            ErrDiagram diagram0 = diagramService.loadByHostAndDatabase(diagram.host().get(), diagram.database().get());
            if (diagram0 != null) {
               openDiagram(diagram0);
               return;
               
            } else {
                openDesignerDiagram(diagram);
            }
           
        } else {
            
            diagram = new DesignerErrDiagram();            
            openDesignerDiagram(diagram);
        }
                
        ReverseEngineerDialogController controller = getTypedBaseController("ReverseEngineerDialog");
        
        ConnectionSession session = getMainController().getSelectedConnectionSession().get();
        if (session != null) {
            SchemaDesignerTabContentController designerController = getSchemaDesignerController();

            controller.init(session.getService(), designerController, selectedDatabase);    

            final Stage dialog = new Stage();
            final Parent p = controller.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Backward engineer");

            dialog.showAndWait();

            diagram.name().set(diagram.database().get());
        }
    }
    
    private void showPublishSearch() {        
        getMainController().usedPublicSchema();
       
        ConnectionSession session = getMainController().getSelectedConnectionSession().get();
        SearchPublicSchemaController controller = getTypedBaseController("SearchPublicSchema");
        controller.init(this, session.getService());
    }
    
    private void showCreateDatabase() {       
        CreateDatabaseFromSchemaController controller = getTypedBaseController("CreateDatabaseFromSchema");
        ConnectionSession session = getMainController().getSelectedConnectionSession().get();
      
        controller.init(this, session.getService());
    }
    
    private void showPublish() {       
        getMainController().usedPublicSchema();        
        PublishSchemaController controller = getTypedBaseController("PublishSchema");
        controller.init(this, selectedDiagram.get());
    }
    
    private void showImportTable() {
        SchemaDesignerImportTablesController controller = getTypedBaseController("SchemaDesignerImportTables");
        
        List<Database> databases = new ArrayList<>();
        String dbName = null;
        DBService dbService = null;
        if (getMainController().getSelectedConnectionSession().get() != null) {
            try {
                ConnectionSession session = getMainController().getSelectedConnectionSession().get();
                dbService = session.getService();
                
                if (session.getConnectionParam().getDatabase() != null && !session.getConnectionParam().getDatabase().isEmpty()) {
                    dbName = session.getConnectionParam().getDatabase().get(0);
                }
                databases = dbService.getDatabases();
            } catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }
        
        controller.init(dbService, databases, dbName);     
        
        if (dbService != null && !controller.getImportTables().isEmpty()) {
            try {
                
                SchemaDesignerTabContentController designerController = getSchemaDesignerController();
                
                for (String s: controller.getImportTables()) {
                    DBTable t = dbService.getTableSchemaInformation(controller.getSelectedDatabase(), s);  
                    
                    DesignerTable dt = convertDBTableToDesignerTableWithoutRelations(t, controller.getSelectedDatabase());  
                    
                    designerController.importTable(dt, 20, 20);
                }
            } catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }
    }
    
    
    public static void loadTableRelations(DBTable t, Map<String, DesignerTable> designerTables) {
        
        if (t.getForeignKeys() != null) {            
            DesignerTable dt = designerTables.get(t.getName());
            if (dt != null) {
                
                for (ForeignKey fk: t.getForeignKeys()) {
                    DesignerForeignKey dfk = new DesignerForeignKey();
                    dt.foreignKeys().add(dfk);
                    
                    dfk.name().set(fk.getName());
                    dfk.onDelete().set(DesignerForeignKey.OnAction.fromString(fk.getOnDelete()));
                    dfk.onUpdate().set(DesignerForeignKey.OnAction.fromString(fk.getOnUpdate()));
                    
                    dfk.referencedTable().set(designerTables.get(fk.getReferenceTable()));
                    
                    if (fk.getColumns() != null) {
                        for (Column c: fk.getColumns()) {
                            DesignerColumn dc = dt.findColumnByName(c.getName());
                            if (dc != null) {
                                dfk.columns().add(dc);
                            }
                        }
                    }
                    
                    if (dfk.referencedTable().get() != null) {
                        if (fk.getReferenceColumns() != null) {
                            int i = 0;
                            for (Column c: fk.getReferenceColumns()) {
                                DesignerColumn dc = dfk.referencedTable().get().findColumnByName(c.getName());
                                if (dc != null) {
                                    dfk.referencedColumns().put(dfk.columns().get(i), dc);
                                }
                                
                                i++;
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    public static DesignerTable convertDBTableToDesignerTableWithoutRelations(DBTable t, String databaseName) throws SQLException {
        DesignerTable dt = new DesignerTable();
        dt.name().set(t.getName());
        dt.collation().set(t.getCharacterSet() + "-" + t.getCollation());
        dt.engine().set(t.getEngineType() != null ? t.getEngineType().getDisplayValue() : null);
        dt.schema().set(databaseName);

        // columns
        if (t.getColumns() != null) {

            List<Column> columns = new ArrayList<>(t.getColumns());
            Collections.sort(columns, (Column o1, Column o2) -> {
                return o1.getOrderIndex() - o2.getOrderIndex();
            });

            for (Column c: columns) {

                DesignerColumn dc = new DesignerColumn();

                dc.name().set(c.getName());
                dc.dataType().set(c.getDataType() != null ? c.calcDataTypeStr() : null);

                if (t.getCollation() != null) {
                    String collation = c.getCharacterSet() == null ? t.getCollation().substring(0, t.getCollation().indexOf("_")) : c.getCharacterSet();
                    dc.collation().set(collation + " - " + t.getCollation());
                }
                dc.comments().set(c.getComment());
                
                String defaults = c.getDefaults() != null ? c.getDefaults() : "";
                if (c.getExtra() != null) {
                    defaults += " " + c.getExtra();// c.getDefaults() + (c.isOnUpdate() ? " ON UPDATE " + c.getDefaults() : "");
                }
                
                dc.defaultValue().set(defaults != null ? defaults.trim() : null);
                
                

                dc.autoIncrement().set(c.isAutoIncrement());
                dc.notNull().set(c.isNotNull());
                dc.primaryKey().set(c.isPrimaryKey());
                dc.unsigned().set(c.isUnsigned());
                dc.zeroFill().set(c.isZeroFill());
                
                dt.columns().add(dc);
            }
        }

        // indexes
        if (t.getIndexes() != null) {
            for (Index idx: t.getIndexes()) {
                DesignerIndex di = new DesignerIndex();

                di.name().set(idx.getName());
                if (idx.getType() != null) {
                    di.type().set(DesignerIndex.IndexType.fromString(idx.getType().name()));
                }

                if (idx.getColumns() != null) {
                    int i = 0;
                    for (Column c: idx.getColumns()) {
                        DesignerColumn dc = dt.findColumnByName(c.getName());
                        if (dc != null) {
                            di.columns().add(dc);
                            di.orderMap().put(dc, ++i);
                            di.sortMap().put(dc, DesignerIndex.IndexOrder.ASC);
                        }
                    }
                }

                dt.indexes().add(di);
            }
        }

        if (t.getAdvanced() != null) {
            dt.comments().set(t.getAdvanced().getComment());
        }
        
        return dt;
    }
    
    private void checkDBMenuITems(DesignerErrDiagram d, ConnectionSession session) {
        
        boolean closed = true;
        if (session != null) {
            DBService dbService = session.getService();

            try {
                closed = dbService == null || dbService.isClosed();
            } catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }

        importTablesMi.setDisable(d == null || closed);
        engineReverseMi.setDisable(closed);
        engineForwardMi.setDisable(d == null || closed);        
    }
    
    
    private void exportToPdf() {
        
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("PDF Files", "*.pdf"),
            new FileChooser.ExtensionFilter("All Files", "*.*"));

        String name = selectedDiagram.get().name().get();
        fc.setInitialFileName(name + ".pdf");
        File file = fc.showSaveDialog(getController().getStage());
        if (file != null) {
            try (FileOutputStream fos = new FileOutputStream(file)) {

                Document document = new Document();
                PdfWriter.getInstance(document, fos);

                document.open();

                SchemaDesignerTabContentController designerController = getSchemaDesignerController();

                // craete image
                byte[] bytes = designerController.getDiagramImage();
                if (bytes != null) {
                    com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(bytes);

                    float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin()) / image.getWidth()) * 100;
                    image.scalePercent(scaler);

                    document.add(image);
                    document.close();
                }

            } catch (Throwable th) {
                showError("Error", "Error exporting PDF file", th);
            }
        }
    }
    
    private void exportToPng() {
        
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("PNG Files", "*.png"),
            new FileChooser.ExtensionFilter("All Files", "*.*"));

        String name = selectedDiagram.get().name().get();
        fc.setInitialFileName(name + ".png");
        File file = fc.showSaveDialog(getController().getStage());
        if (file != null) {
            try (FileOutputStream fos = new FileOutputStream(file)) {

                SchemaDesignerTabContentController designerController = getSchemaDesignerController();

                // craete image
                byte[] bytes = designerController.getDiagramImage();
                if (bytes != null) {
                    Files.write(file.toPath(), bytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                }

            } catch (Throwable th) {
                showError("Error", "Error exporting PDF file", th);
            }
        }
    }
    
    // TODO Need make saveAsDiagram and saveDiagram in one method
    private void saveAsDiagram(String name) {        
        Map<String, Object> dataForSave = selectedDiagram.get().toMap();
        dataForSave.put("uuid", null);
        dataForSave.put("name", name);
        
        ErrDiagram d = new ErrDiagram();        
        
        d.setName(name);
        d.setHost(selectedDiagram.get().host().get());
        d.setDatabase(selectedDiagram.get().database().get());
        d.setDiagramData(new Gson().toJson(dataForSave));
        d.setPublicId(selectedDiagram.get().publicId().get());
        
        diagramService.save(d);
        
        int index = errDiagrams.indexOf(d);
        if (index >= 0) {
            errDiagrams.remove(d);
            errDiagrams.add(index, d);
        } else {
            errDiagrams.add(d);
        }
        
        openDiagram(d);
    }
    
    public void saveDiagram() {
        
        Map<String, Object> dataForSave = selectedDiagram.get().toMap();

        ErrDiagram d = selectedErrDiagram;//diagramsMap.get(selectedDiagram.get());
        if (d == null) {
            d = new ErrDiagram();
        }

        d.setName(selectedDiagram.get().name().get());
        d.setHost(selectedDiagram.get().host().get());
        d.setDatabase(selectedDiagram.get().database().get());
        d.setDiagramData(new Gson().toJson(dataForSave));
        d.setPublicId(selectedDiagram.get().publicId().get());
            
        diagramService.save(d);
        
        selectedErrDiagram = d;
        
        selectedDiagram.get().changed().set(false);
        
        reloadDiagrams();
    }
    
    public DesignerErrDiagram openDiagram(ErrDiagram diagram) {
        
        Map<String, Object> map = new Gson().fromJson(new StringReader(diagram.getDiagramData()), HashMap.class);

        DesignerErrDiagram d = new DesignerErrDiagram();
        d.fromMap(map);
        d.domain().set(diagram.getPublicId() != null);
        d.publicId().set(diagram.getPublicId());

        selectedErrDiagram = diagram;
        return openDesignerDiagram(d);
    }
    
    public void makeCanSave() {
        if (selectedDiagram.get() != null) {
            selectedDiagram.get().changed().set(true);
        }
    }
        
    
    public void clearDiagram() {
        selectedErrDiagram = null;
        selectedDiagram.set(null);
        
        SchemaDesignerTabContentController designerController = getSchemaDesignerController();
        designerController.init(null);
    }
    
    public DesignerErrDiagram openDesignerDiagram(DesignerErrDiagram diagram) {
        if (diagram != null && !diagram.equals(selectedDiagram.get())) {
            
            SmartsqlSavedData smartData = getMainController().getSmartData();
            if (smartData != null && smartData.getEmailId() != null) {
                diagram.user().set(smartData.getEmailId());
            }

            selectedDiagram.set(diagram);

            SchemaDesignerTabContentController designerController = getSchemaDesignerController();
            designerController.init(diagram);
        }
        
        return diagram;
    }

    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {
        super.postConnection(service, tab);
        
        showLocalDiagrams(false);
        Platform.runLater(() -> {
            tab.setOnClosed((Event event) -> {
                opened = false;
                selectedDiagram.set(null);
                selectedErrDiagram = null;
            });
        });
    }

    public void reloadDiagrams() {
        SchemaDesignerTabContentController designerController = getSchemaDesignerController();
        switch (designerController.getSelectedDiagramType()) {
            
            case SchemaDesignerTabContentController.DIAGRAM_TYPE__ALL:
                showAllDiagrams();
                break;
                
            case SchemaDesignerTabContentController.DIAGRAM_TYPE__LOCAL:
                showLocalDiagrams(true);
                break;
            
            case SchemaDesignerTabContentController.DIAGRAM_TYPE__PUBLIC:
                showPublicDiagrams();
                break;
            
        }
    }
    
    
    private List<ErrDiagram> loadLocal() {
        return diagramService.getAll().stream().filter(d -> d.getPublicId() == null).collect(Collectors.toList());
    }
    private List<ErrDiagram> loadPublic() {
        return diagramService.getAll().stream().filter(d -> d.getPublicId() != null).collect(Collectors.toList());
    }
    
    public void showLocalDiagrams(boolean useDiffThread) {
        SchemaDesignerTabContentController controller = getSchemaDesignerController();
        controller.setDisableDiagramTitledPane(true);
        
        Runnable run = new Runnable() {
            @Override
            public void run() {
                List<ErrDiagram> list = loadLocal();
                Platform.runLater(() -> {
                    errDiagrams.setAll(list);
                    controller.setDisableDiagramTitledPane(false);
                });
            }
        };
        
        if(useDiffThread){
            new Thread(run).start();
        }else{ run.run(); }        
    }
    
    public void showPublicDiagrams() {
        SchemaDesignerTabContentController controller = getSchemaDesignerController();
        controller.setDisableDiagramTitledPane(true);
        
        Thread th = new Thread(() -> {
            List<ErrDiagram> list = loadPublic();
            Platform.runLater(() -> {
               errDiagrams.setAll(list); 
               controller.setDisableDiagramTitledPane(false);
            });
        });
        th.start();
    }
    
    public void showAllDiagrams() {
        SchemaDesignerTabContentController controller = getSchemaDesignerController();
        controller.setDisableDiagramTitledPane(true);
        
        Thread th = new Thread(() -> {            
            Platform.runLater(() -> {
               errDiagrams.setAll(diagramService.getAll()); 
               controller.setDisableDiagramTitledPane(false);
            });
        });
        th.start();
    }
    
    public static DBTable convertDesignerTableToTable(DBService service, DesignerTable dt) {
        
        Database db = new Database();
        db.setName(dt.schema().get());
        DBTable t = new DBTable();
        t.setDatabase(db);
        
        t.setName(dt.name().get());
        t.setEngineType(convertToTableEngine(dt.engine().get()));
        
        // fill Advanced
        Advanced advanced = new Advanced();
        t.setAdvanced(advanced);
        try {
            advanced.setAutoIncrement(Integer.valueOf(dt.autoInc().get()));
        } catch (Throwable th) {}
        
        try {
            advanced.setAvgRowLength(Integer.valueOf(dt.avgRowLength().get()));
        } catch (Throwable th) {}
        
        Integer checkSum = dt.checksum().get();
        advanced.setChecksum(checkSum != null && checkSum == 1 ? Checksum.TRUE : Checksum.FALSE);
        
        advanced.setComment(dt.comments().get());
                
        Integer delayKW = dt.delayKeyWrite().get();
        advanced.setDelayKeyWrite(delayKW != null && delayKW == 1 ? DelayKeyWrite.TRUE : DelayKeyWrite.FALSE);
        
        try {
            advanced.setMaxRows(Integer.valueOf(dt.maxRows().get()));
        } catch (Throwable th) {}
        
        try {
            advanced.setMinRows(Integer.valueOf(dt.minRows().get()));
        } catch (Throwable th) {}
        
        advanced.setRowformat(convertToTableRowFormat(dt.rowFormat().get()));
        ///////////////////////////
        
        // Collation
        String collation = dt.collation().get();
        if (collation != null && collation.contains("-")) {
            String[] v = collation.split("-");
        
            t.setCharacterSet(v[0].trim());
            t.setCollation(v[1].trim());
        }
        ////////////////////////////
        
        
        // Columns
        List<Column> columns = new ArrayList<>();
        int index = 0;
        for (DesignerColumn dc: dt.columns()) {
            Column c = convertToColumn(service, dc, index);
            c.setTable(t);
            columns.add(c);
            index++;
        }
        t.setColumns(columns);
        /////////////////////////////////////
        
        // indexes
        List<Index> indexes = new ArrayList<>();
        for (DesignerIndex di: dt.indexes()) {
            Index idx = new Index();
            indexes.add(idx);
            
            idx.setName(di.name().get());
            idx.setType(convertToIndex(di.type().get()));
            
            List<Column> ics = new ArrayList<>();
            if (di.columns() != null) {
                for (DesignerColumn dc: di.columns()) {
                    ics.add(t.getColumnByName(dc.name().get()));
                }
            }
            idx.setColumns(ics);
        }
        t.setIndexes(indexes);
        /////////////////////////////////////
        
        // foreign keys
        List<ForeignKey> fks = new ArrayList<>();
        for (DesignerForeignKey dfk: dt.foreignKeys()) {
            ForeignKey fk = new ForeignKey();
            fks.add(fk);
            
            fk.setName(dfk.name().get());
            if (dfk.onDelete().get() != DesignerForeignKey.OnAction.NO_ACTION) {
                fk.setOnDelete(dfk.onDelete().get().toString());
            }
            if (dfk.onUpdate().get() != DesignerForeignKey.OnAction.NO_ACTION) {
                fk.setOnUpdate(dfk.onUpdate().get().toString());
            }
            
            DesignerTable dt0 = dfk.referencedTable().get();
            fk.setReferenceTable(dt0 != null ? dt0.name().get() : null);
            fk.setReferenceDatabase(dt.schema().get());
            
            List<Column> fcs = new ArrayList<>();
            if (dfk.columns() != null) {
                for (DesignerColumn dc: dfk.columns()) {
                    fcs.add(t.getColumnByName(dc.name().get()));
                }
            }
            fk.setColumns(fcs);
            
            if (dt0 != null) {
                List<Column> rcs = new ArrayList<>();
                if (dfk.columns() != null) {
                    for (DesignerColumn dc: dfk.columns()) {
                        rcs.add(convertToColumn(service, dfk.referencedColumns().get(dc), 0));
                    }
                }
                
                fk.setReferenceColumns(rcs);
            }
        }        
        t.setForeignKeys(fks);
        
        /////////////////////////////////////
        
        return t;
    }
    
    
    public static Column convertToColumn(DBService service, DesignerColumn dc, int index) {
        Column c = new Column();
        if (dc != null) {
            String cc = dc.collation().get();
            if (cc != null && cc.contains("-")) {
                String[] v = cc.split("-");

                c.setCharacterSet(v[0].trim());
                c.setCollation(v[1].trim());
            }

            int i = dc.defaultValue().get() != null ? dc.defaultValue().get().toUpperCase().indexOf("ON UPDATE") : -1;
            
            String defaults = i >= 0 ? dc.defaultValue().get().substring(0, i).trim() : dc.defaultValue().get();
            if (defaults != null) {
                defaults = defaults.toUpperCase().replace("AUTO_INCREMENT", "").trim();
            }
            
            c.setAutoIncrement(dc.autoIncrement().get());
            c.setComment(dc.comments().get());
            c.setDataType(((AbstractDBService)service).getDatatype(dc.dataType().get()));
            c.setDefaults(defaults);
            c.setName(dc.name().get());
            c.setNotNull(dc.notNull().get());
            c.setOrderIndex(index);
            c.setPrimaryKey(dc.primaryKey().get());
            c.setUnsigned(dc.unsigned().get());
            c.setZeroFill(dc.zeroFill().get());
            c.setOnUpdate(dc.defaultValue().get() != null ? i >= 0 : false);
        }
        return c;
    }
    
    public static IndexType convertToIndex(DesignerIndex.IndexType t) {
        if (t != null) {
            switch (t) {
                case INDEX:
                    // TODO DOnt know what todo
                    return null;
                    
                case FULLTEXT:
                    return IndexType.FULL_TEXT;
                    
                case PRIMARY:
                    return IndexType.PRIMARY;
                    
                case UNIQUE:
                    return IndexType.UNIQUE_TEXT;
            }
        }
        
        return null;
    }
    
    public static Rowformat convertToTableRowFormat(DesignerTable.RowFormat rf) {
        if (rf != null) {
            switch (rf) {
                case COMPRESSED:
                    return Rowformat.COMPRESSED;
                    
                case DYNAMIC:
                    return Rowformat.DYNAMIC;
                    
                case FIXED:
                    return Rowformat.FIXED;
            }
        }
        
        return null;
    }
    
    public static EngineType convertToTableEngine(String engine) {
        if (engine != null) {
            for (EngineType et: MySQLEngineTypes.TYPES) {
                if (et.getDisplayValue().equals(engine)) {
                    return et;
                }
            }
        }
        
        return null;            
    }
    
     
  
    public ObservableList<ErrDiagram> errDiagrams() {
        return errDiagrams;
    }

    @Override
    public int getOrder() {
        return 10;
    }    
    
    @Override
    public boolean isConnection() {
        return true;
    }
    
    @Override
    public Class getDependencyModuleClass() {
        return SchemaDesignerModule.class;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {return true; }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getInfo() { 
        return "Design Schema for Mysql";
    }
}
