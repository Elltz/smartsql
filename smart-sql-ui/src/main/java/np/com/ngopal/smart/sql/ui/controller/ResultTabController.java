package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.awt.FontMetrics;
import java.awt.Toolkit;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.utils.ObservableListWithLastNonDataItem;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.ui.components.AutoSizeTypedTableColumn;
import np.com.ngopal.smart.sql.structure.components.DateTimePicker;
import np.com.ngopal.smart.sql.structure.components.DateTimePickerSkin;
import np.com.ngopal.smart.sql.ui.factories.EnumTableCell;
import np.com.ngopal.smart.sql.ui.factories.EditingCell;
import np.com.ngopal.smart.sql.ui.factories.SetTableCell;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.queryutils.FindObject;
import np.com.ngopal.smart.sql.structure.queryutils.FontUtils;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class ResultTabController extends BaseController implements Initializable {
        
    private static int CHARACTER_WIDTH      = 6;
    private static int CHARACTER_WIDTH__10  = 10 * CHARACTER_WIDTH;
    private static int CHARACTER_WIDTH__20  = 20 * CHARACTER_WIDTH;
    private static int CHARACTER_WIDTH__30  = 30 * CHARACTER_WIDTH;
    private static int CHARACTER_WIDTH__50  = 50 * CHARACTER_WIDTH;
    private static int CHARACTER_WIDTH__100 = 100 * CHARACTER_WIDTH;
    
    
    public static final String NULL             = "(NULL)";
    private static final String BINARY          = "(Binary / Image)";
    private static final String X_WITH_QUOTE    = "x'";
    private static final String QUOTE           = "'";
    
    private static final int NULL_LENGTH            = NULL.length();
    private static final int BINARY_IMAGE_LENGTH    = BINARY.length();
                    
                    
                    
    private static final String READ_ONLY = "(Read Only)";
    
    private static final String FILTER_TEMPLATE__EQUALS     = "'TABLE_NAME' = 'CHARACTER_SETS'";
    private static final String FILTER_TEMPLATE__NONEQUALS  = "'TABLE_NAME' <> 'CHARACTER_SETS'";
    private static final String FILTER_TEMPLATE__GREATER    = "'TABLE_NAME' > 'CHARACTER_SETS'";
    private static final String FILTER_TEMPLATE__LESS       = "'TABLE_NAME' < 'CHARACTER_SETS'";
    
    private static final String FILTER_TEMPLATE__LIKE_BEFORE = "'TABLE_NAME' LIKE '%CHARACTER_SETS'";
    private static final String FILTER_TEMPLATE__LIKE_AFTER  = "'TABLE_NAME' LIKE 'CHARACTER_SETS%'";
    private static final String FILTER_TEMPLATE__LIKE_BOTH   = "'TABLE_NAME' LIKE '%CHARACTER_SETS%'";
   
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final Pattern p = Pattern.compile("[0-9a-zA-Z\\s@ _$\\[\\]><\\.\\?\";'\\}\\{\\!#%\\^&\\*\\(\\)\\-\\+=\\\\/№%:`~|,]");

    private static Map<Timestamp, String> timestampCache;
    private static Map<Time, String> timeCache;

    @Inject
    private ConnectionParamService connectionParamService;
    @Inject
    private PreferenceDataService preferenceService;

    @FXML
    private Button deleteButton;

    @FXML
    private Button insertNewRowButton;

    @FXML
    private Button duplicateRowButton;

    @FXML
    private Button cancelChangesButton;
    
    @FXML 
    private Button filterButton;
    
    @FXML 
    private Button visualExplainButton;
    
    @FXML 
    private Button exportAsButton;
    @FXML 
    private Button exportAsXlsButton;
    @FXML 
    private Button exportAsPdfButton;

    @FXML
    private Button saveBtn;

    @FXML
    private VBox dynamicTablePane;
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private TextField limitRowsTextField;

    @FXML
    private ComboBox<String> readEditComboBox;
    
    @FXML
    private MenuItem copyCellItem;
    @FXML
    private MenuItem copyAllRowsItem;
    @FXML
    private MenuItem copySelectedRowsItem;
    
    @FXML
    private Button refreshButton;
    
    private int startLimit = 0;
    private int offsetLimit = 1000;
    
    @FXML
    private ToolBar header;

    private TableColumn<RowData, Boolean> checkBoxCol;

    private List<FilterData> filterData;
            
    private TableProvider selectedTable;
    
    @FXML
    private TextField filterTextField;
    @FXML
    private CheckBox regexCheckBox;
    @FXML
    private CheckBox notMatchCheckBox;
        
//    private List<RowData> currentData;
    private ObservableListWithLastNonDataItem<RowData> data;
    private Map<RowData, RowData> sourceData;    

    private final ObservableMap<RowData, RowData> changesMap = FXCollections.observableHashMap();
    private final ObservableSet<RowData> newRowsSet = FXCollections.observableSet(new HashSet<RowData>());
    
    private BooleanProperty busy = new SimpleBooleanProperty(false);
    private BooleanProperty hasLimit = new SimpleBooleanProperty(false);
    
    private BooleanProperty dataFiltered;
    
    private boolean noNeedSave = false;
    
    private BooleanProperty isEditable;
    private BooleanProperty isNullable;
    private BooleanProperty isHasDefault;
    private BooleanProperty isSorted;

    private String baseQuery;
    private ConnectionSession session;
    
    private boolean readOnly = true;
    
    @Setter
    private DBService service;
    

    private TableInfoData tableInfoData;

    private CheckBox selectAllCheckBox;
    
    private TableColumn<RowData, ?> firstColumn;
    
    @FXML
    private HBox footer;
    
    @FXML
    private HBox resultFooter;
    
    @FXML
    private Label resultLabel;
    
     @FXML
    private Label notice;
     
     @FXML
    private Button decrementButton;

    @FXML
    private TextField numberField;

    @FXML
    private Button incrementButton;
    
    @FXML
    private CheckBox limitRowCheckBox;
    
    @FXML
    private MenuButton filterMenuButton;
    
    @FXML
    private HBox moveHBox;
    
    
    @FXML
    private Label ofRowsLabel;
    
    @FXML
    private StackPane contentHolder;
    
    @FXML
    private Label databaseNameLabel;

    @FXML
    private Label tableNameLabel;

    @FXML
    private ScrollPane formViewContainer;

    @FXML
    private GridPane grid;
    
    @FXML
    private VBox tableviewContainer;

    @FXML
    private HBox upperFooter;

    @FXML
    private Button previous;

    @FXML
    private Button previouslast;

    @FXML
    private Button next;

    @FXML
    private Button nextlast;

    @FXML
    private TextArea textUI;

    @FXML
    private TableView<RowData> tableview;
    
    @FXML
    private Button gridViewButton;
    
    @FXML
    private Button formViewButton;
    
    @FXML
    private Button textviewButton;
    
    @FXML
    private MenuButton copyMenuButton;
    
    @FXML
    private BorderPane busyPane;
    
    private ContextMenu menuC;
        
    MenuItem insertNRow,savechange,deleteSRow,cancelM,setToEmptyString,setToNull,setToDefault
            ,unsort,tableNameEquals,tableNameEquality,tableNameGreater,tableNameLess
            ,tableName_Like,tableNameLike_,tableName_Like_,customFliter,resetFliter,
            exportAllRowsOfTableRA,dupeCurrentRow;
    
    Menu fliter,copy;
    
    private ChangeListener<Number> peekTabpaneListener;
    private ReadOnlyDoubleProperty peekTabpaneHeightProperty;
    
    private Node currentScreen;
    
    private List<TableColumn<RowData, ?>> columns = new ArrayList<>();

    private Font currentFont;
    
    
    private ChangeListener<String> readEditListener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
        if (!readOnly && baseQuery.trim().toLowerCase().startsWith("select") && !READ_ONLY.equals(newValue)) {
            RowData rd = getNewRow();
            rd.setDataRow(false);
            if (data != null) {
                data.initLastNonDataRow(rd);
            }
        } else {
            data.removeLastNonDataRow();
        }                
        
        //tableview.setItems(data == null ? FXCollections.emptyObservableList() : FXCollections.observableList(data));
    };
    
    @Override
    public void requestFocus() {
        Platform.runLater(() -> {
            tableview.requestFocus();
            if (!tableview.getItems().isEmpty() && !columns.isEmpty()) {
                tableview.getSelectionModel().select(0, columns.get(0));
            }
        });
    }
    
    
    
    public void destroyController() {
        
        if (changesMap != null) {
            changesMap.clear();
        }

        if (data != null) {
            data.clear();
        }

        if (filterData != null) {
            filterData.clear();
        }

        if (filteredOut != null) {
            filteredOut.clear();
        }

        if (newRowsSet != null) {
            newRowsSet.clear();
        }

        synchronized (RSlocker) {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {}
            }
            rs = null;
        }
        statement = null;
        tableInfoData = null;
    }
     
    private EventHandler<Event> allHandlers = new EventHandler<Event>() {

        @Override
        public void handle(Event t) {
                                
            /**
            * All events listener codes that are directed to
            * contextmenu will fall under these sets of codes
            * actually just one
            * 
            * This is created so as to prevent me create anonymouse inner classes
            * left and right. this will called be using on every
            * control, to help usability and also reduce the creating of objects-memory;
            * 
            * It's function is to make known which node amongst the tree of 
            * Table| Form | Grid : is requesting for context menu and edit 
            * the menuItems respectively
            * 
            */ 
            if (t instanceof ContextMenuEvent) {
                menuC.setUserData(t.getSource());
            }else 
             /**
             * All events listener codes that are directed to windowevents will
             * fall under these sets of codes.
             */
            if (t instanceof WindowEvent) {
                WindowEvent  we = (WindowEvent)t;
                
                if(we.getEventType() == WindowEvent.WINDOW_SHOWING){
                    /**
                     * reason for the below is because of recycling hence i need
                     * to get the menuItems and send them back
                     */
                    copy.getItems().addAll(copyMenuButton.getItems());
                    copyMenuButton.getItems().clear();
                    
                }else 
                    
                    if(we.getEventType() == WindowEvent.WINDOW_HIDDEN){
                    copyMenuButton.getItems().addAll(copy.getItems());
                    copy.getItems().clear();
                }                
            }else 
               /**
             * All events listener codes that are directed to keyEvents will
             * fall under these sets of codes.
             */
                if(t instanceof KeyEvent){
                    
                    KeyEvent ke = (KeyEvent) t;
                    if (!ke.getCode().isDigitKey() && ke.getSource() == numberField) {// we are stopping anybody
                        t.consume();
                    }else 
                        
                        if (ke.getEventType().equals(KeyEvent.KEY_RELEASED)) {
                        if (ke.getCode() == KeyCode.ENTER) {
                            log.debug("key enter");
                            session.getConnectionParam().setLimitResult(
                                    Integer.parseInt(((SimpleStringProperty)limitRowsTextField.getUserData()).get()));
                            System.out.println("con:" + session.getConnectionParam().getLimitResult());
                            session.setConnectionParam(connectionParamService.save(session.getConnectionParam()));
                            refreshAction(null);
                        }
                    }
                }
        }

    };
    
    
    /**
     * for nodes triggering onActions
     */
    EventHandler<ActionEvent> onActions = new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent t) {        
            Object o = t.getSource();
            
            Platform.runLater(() -> {
                if(o instanceof MenuItem){
                    if(o == insertNRow){
                        insertNewRowButtonAction(t);
                    }else if (o ==savechange) {
                        saveButtonAction(t);
                    } else if (o ==deleteSRow) {
                        deleteButtonAction(t);
                    } else if (o == cancelM) {
                        cancelChangesButtonAction(t);
                    } else if (o == setToEmptyString) {
                        setValueForSelectedCell("");
                    } else if (o == setToNull) {
                        setValueForSelectedCell(null);
                    } else if (o == setToDefault) {
                        setValueForSelectedCell(getDefaultValueForSelectedCell());
                    } else if (o == unsort) {
                        tableview.getSortOrder().clear();
                    } else if (o == customFliter) {
                        if (showFilter()) {
                            doFilter();
                        }
                    } else if (o ==dupeCurrentRow) {
                        duplicateRowButtonAction(t);
                    } else if (o == resetFliter) {
                        clearFilter();
                        filterData();
                    } else if (o == tableNameEquality) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition("<>");
                            fd.setValue((String) tableNameEquality.getUserData());

                            filterData.add(fd);

                            doFilter();
                        }
                    } else if (o ==tableNameEquals) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition("=");
                            fd.setValue((String) tableNameEquals.getUserData());

                            filterData.add(fd);

                            doFilter();
                        }
                    } else if (o == tableNameGreater) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition(">");
                            fd.setValue((String) tableNameGreater.getUserData());

                            filterData.add(fd);

                            doFilter();
                        }
                    } else if (o == tableNameLess) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition("<");
                            fd.setValue((String) tableNameLess.getUserData());

                            filterData.add(fd);

                            doFilter();
                        }
                    } else if (o ==tableNameLike_) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition("LIKE");
                            fd.setValue((String) tableNameLike_.getUserData() + "%");

                            filterData.add(fd);

                            doFilter();
                        }
                    } else if (o == tableName_Like) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition("LIKE");
                            fd.setValue("%" + (String) tableName_Like.getUserData());

                            filterData.add(fd);

                            doFilter();
                        }
                    } else if (o == tableName_Like_) {
                        ColumnData column = getSelectedColumnData();
                        if (column != null) {
                            filterData = new ArrayList<>();
                            FilterData fd = new FilterData();
                            fd.setColumn(column.getName());
                            fd.setCondition("LIKE");
                            fd.setValue("%" + (String) tableName_Like_.getUserData() + "%");

                            filterData.add(fd);

                            doFilter();
                        }
                    }else if(o == exportAllRowsOfTableRA){
                        exportTable(t);
                    }


                }else 
                  /**
                 * My Nodes
                 */
                    
                if (o instanceof Node) {
                    TableInfoData staticTableInfoData = tableInfoData;
                    if (o == decrementButton) {
                        try {
                            int i = Integer.valueOf(numberField.getText()) - Integer.valueOf(limitRowsTextField.getText());
                            if (i < 0) {
                                i = 0;
                            }
                            numberField.setText(String.valueOf(i));

                            startLimit = i;
                            refreshAction(null);
                        } catch (NumberFormatException ex) {
                        }
                    } else if (o == incrementButton) {
                        try {
                            int i = Integer.valueOf(numberField.getText()) + Integer.valueOf(limitRowsTextField.getText());
                            numberField.setText(String.valueOf(i));

                            startLimit = i;
                            refreshAction(null);
                        } catch (NumberFormatException ex) {
                        }     

                    } else if (o == gridViewButton) {
                        toGridView();

                    } else if (o == formViewButton) {
                        toFormView();

                    } else if (o == textviewButton) {
                        toTextView();

                    } else if (o == next) {
                        saveButtonAction(null);
                        forFormPos = forFormPos + 1;//forFormPos =+1;
                        if (forFormPos >= data.size()) {
                            forFormPos = data.size() - 1;
                            return;
                        }
                        if (staticTableInfoData != null) {
                            formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
                        }
                    } else if (o == nextlast) {
                        saveButtonAction(null);
                        forFormPos = data.size() - 1;
                        if (staticTableInfoData != null) {
                            formViewPopulator((data.size() - 1), staticTableInfoData.getColumnsData());
                        }
                    } else if (o == previous) {
                        saveButtonAction(null);
                        if (forFormPos > 0) {
                            forFormPos = forFormPos - 1;
                            if (staticTableInfoData != null) {
                                formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
                            }
                        }
                    } else if (o == previouslast) {
                        saveButtonAction(null);
                        forFormPos = 0;
                        if (staticTableInfoData != null) {
                            formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
                        }
                    }
                }
            });
        }
        
    };
    
    private int selectedView = 0;
    
    public boolean isGridView() {
        return selectedView == 0;
    }
    
    public boolean isFormView() {
        return selectedView == 1;
    }
    
    public boolean isTextView() {
        return selectedView == 2;
    }
    
    public void toGridView() {
        selectedView = 0;
        changeScreen(tableviewContainer);
    }
    
    public void toFormView() {
        selectedView = 1;
        forFormPos = 0;
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData != null) {
            formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
        }
        changeScreen(formViewContainer);
    }
    
    public void toTextView() {
        selectedView = 2;
        // TODO at now it disabled - need make like HTML
        recreateTextViewData();                    
        changeScreen(textUI);
    }
    
    private void initContextMenus(){
        if(menuC != null){
            return;
        }
        menuC = new ContextMenu();
        menuC.addEventFilter(WindowEvent.WINDOW_HIDDEN, allHandlers);
        menuC.addEventFilter(WindowEvent.WINDOW_SHOWING, allHandlers);
        ////////////menuitems for menuC ////////////////
        
        insertNRow = new MenuItem("Insert New Row");
        savechange = new MenuItem("Save Changes");
        deleteSRow = new MenuItem("Delete selected row");
        cancelM = new MenuItem("Cancel Changes");

        setToEmptyString = new MenuItem("Set to Empty String");
        setToNull = new MenuItem("Set to NULL");
        setToDefault = new MenuItem("Set to Default");

        unsort = new MenuItem("Unsort");

        fliter = new Menu("Fliter");
        tableNameEquals = new MenuItem(FILTER_TEMPLATE__EQUALS);
        tableNameEquality = new MenuItem(FILTER_TEMPLATE__NONEQUALS);
        tableNameGreater = new MenuItem(FILTER_TEMPLATE__GREATER);
        tableNameLess = new MenuItem(FILTER_TEMPLATE__LESS);

        // The '_' is an indication of where the '%' is
        tableName_Like = new MenuItem(FILTER_TEMPLATE__LIKE_BEFORE);
        tableNameLike_ = new MenuItem(FILTER_TEMPLATE__LIKE_AFTER);
        tableName_Like_ = new MenuItem(FILTER_TEMPLATE__LIKE_BOTH);
        
        customFliter = new MenuItem("Custom Fliter");
        resetFliter = new MenuItem("Reset Fliter");

        exportAllRowsOfTableRA = new MenuItem("Export All Rows Of Table Data\\Results As", standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getTableExport_image()));
        copy = new Menu("Copy");
        dupeCurrentRow = new MenuItem("Duplicate Current Row");

        SeparatorMenuItem one = new SeparatorMenuItem(), two = new SeparatorMenuItem(),
                three = new SeparatorMenuItem(), four = new SeparatorMenuItem(),
                five = new SeparatorMenuItem(), six = new SeparatorMenuItem(),
                seven = new SeparatorMenuItem(), eight = new SeparatorMenuItem();
        
        dupeCurrentRow.addEventHandler(ActionEvent.ACTION,onActions);
        savechange.addEventHandler(ActionEvent.ACTION,onActions);
        resetFliter.addEventHandler(ActionEvent.ACTION,onActions);
        customFliter.addEventHandler(ActionEvent.ACTION,onActions);
        tableName_Like_.addEventHandler(ActionEvent.ACTION,onActions);
        tableNameLike_.addEventHandler(ActionEvent.ACTION,onActions);
        tableName_Like.addEventHandler(ActionEvent.ACTION,onActions);
        tableNameLess.addEventHandler(ActionEvent.ACTION,onActions);
        tableNameGreater.addEventHandler(ActionEvent.ACTION,onActions);
        tableNameEquality.addEventHandler(ActionEvent.ACTION,onActions);
        tableNameEquals.addEventHandler(ActionEvent.ACTION,onActions);
        unsort.addEventHandler(ActionEvent.ACTION,onActions);
        setToDefault.addEventHandler(ActionEvent.ACTION,onActions);
        setToNull.addEventHandler(ActionEvent.ACTION,onActions);
        setToEmptyString.addEventHandler(ActionEvent.ACTION,onActions);
        cancelM.addEventHandler(ActionEvent.ACTION,onActions);
        deleteSRow.addEventHandler(ActionEvent.ACTION,onActions);
        insertNRow.addEventHandler(ActionEvent.ACTION,onActions);
        exportAllRowsOfTableRA.addEventHandler(ActionEvent.ACTION,onActions);

        unsort.disableProperty().bind(isSorted.not());

        deleteSRow.disableProperty().bind(isEditable.not());
        insertNRow.disableProperty().bind(isEditable.not());
        
        savechange.disableProperty().bind(Bindings.isEmpty(changesMap).and(Bindings.isEmpty(newRowsSet)));
        cancelM.disableProperty().bind(Bindings.isEmpty(changesMap).and(Bindings.isEmpty(newRowsSet)));
        
        setToEmptyString.disableProperty().bind(isEditable.not());
        setToNull.disableProperty().bind(isEditable.not().or(isNullable.not()));
        setToDefault.disableProperty().bind(isEditable.not().or(isHasDefault.not()));
        
        resetFliter.disableProperty().bind(dataFiltered.not());
        
        fliter.getItems().addAll(tableNameEquals, tableNameEquality, five, tableNameGreater, tableNameLess,
            six, tableName_Like, tableNameLike_, tableName_Like_, seven, customFliter, eight, resetFliter);
        menuC.getItems().addAll(insertNRow, savechange, deleteSRow, cancelM, one,
            setToEmptyString, setToNull, setToDefault, two, unsort, three, fliter, four,
            exportAllRowsOfTableRA, copy, dupeCurrentRow);
    }
    
    private void setValueForSelectedCell(String cellValue) {
        RowData row = tableview.getSelectionModel().getSelectedItem();
        if (row != null) {
            List<TablePosition> list = tableview.getSelectionModel().getSelectedCells();
            if (!list.isEmpty()) {
                int columnIndex = list.get(0).getColumn();
                row.getData().set(columnIndex - 1, cellValue);
                tableview.getColumns().get(columnIndex).setVisible(false);
                tableview.getColumns().get(columnIndex).setVisible(true);
                 
                if (!row.isNewRow()) {
                    changesMap.put(row, sourceData.get(row));
                }
            }
        }
    }
    
    private String getDefaultValueForSelectedCell() {
        RowData row = tableview.getSelectionModel().getSelectedItem();
        if (row != null) {
            ColumnData column = getSelectedColumnData();
            if (column != null) {
                return column.getDefaultValue();
            }
        }
        
        return null;
    }
    
    private ColumnData getSelectedColumnData() {
        List<TablePosition> list = tableview.getSelectionModel().getSelectedCells();
        TableInfoData staticTableInfoData = tableInfoData;
        if (!list.isEmpty() && staticTableInfoData != null) {
            return staticTableInfoData.getColumnByIndex(list.get(0).getColumn() - 1);
        }
        
        return null;
    }
    
    private void addContextMenu(Control...controls){
        if(controls != null ){
            for(Control con : controls){
                if(menuC == null | con == null){
                    continue;
                }
                con.setContextMenu(menuC);
                con.addEventHandler(ContextMenuEvent.CONTEXT_MENU_REQUESTED,allHandlers);
            }
        }
    }
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;

    }
    

    public void informationOfDatabase(ConnectionParams connectionParam, ResultSet resultSet)
            throws SQLException {
        if (tableInfoData != null) {
            tableInfoData = null;
        }

        tableInfoData = new TableInfoData();    
//        log.error("!!!!!!!!!!!!!!!!!!!!!! informationOfDatabase");

        Map<String, List<String>> databases = new HashMap<>();

        if (resultSet != null) {

            // collect all columns from current result set
            // and indentify all primary keys
            ResultSetMetaData metaData = resultSet.getMetaData();

            int count = metaData.getColumnCount();
            for (int i = 1; i <= count; i++) {

                String columnAlias = metaData.getColumnLabel(i);
                String columnName = metaData.getColumnName(i);
                boolean nullable = metaData.isNullable(i) == 1;
                String databaseName = metaData.getCatalogName(i);
                String tableName = metaData.getTableName(i);
                String columnTypeName = metaData.getColumnTypeName(i);
                int columnType = metaData.getColumnType(i);
                String defaultValue = DatabaseCache.getInstance().getColumnDefaultValue(connectionParam.cacheKey(), databaseName, tableName, columnName);

                if (readOnly) {
                    if (tableName != null && !tableName.isEmpty()) {
                        readOnly = false;
                    }
                }

                ColumnData columnData = new ColumnData(columnAlias);
                columnData.setName(columnName);
                columnData.setNullable(nullable);
                columnData.setDatabaseName(databaseName);
                columnData.setTableName(tableName);
                
                DataType dt = DatabaseCache.getInstance().getColumnDataType(connectionParam.cacheKey(), databaseName, tableName, columnName);
                columnData.setDataType(dt != null ? dt : ((AbstractDBService)service).getDatatype(columnTypeName));
                
                columnData.setDisplayTable("`" + databaseName + "`.`" + tableName + "`");
                columnData.setDefaultValue(defaultValue);
                columnData.setAutoincrement(DatabaseCache.getInstance().isColumnAutoincrement(connectionParam.cacheKey(), databaseName, tableName, columnName));
                
                List<String> tables = databases.get(databaseName);
                if (tables == null) {
                    tables = new ArrayList<>();
                    databases.put(databaseName, tables);
                }

                if (!tables.contains(tableName)) {
                    tables.add(tableName);
                }

                switch (columnType) {
                    case Types.BIGINT:
                    case Types.NUMERIC:
                    case Types.DECIMAL:
                    case Types.DOUBLE:
                    case Types.FLOAT:
                    case Types.INTEGER:
                    case Types.SMALLINT:
                    case Types.TINYINT:
                    case Types.REAL:
                        columnData.setNumber(true);
                }
                
                switch (columnType) {
                    case Types.TIMESTAMP:
                    case Types.DATE:
                    case Types.TIME:
                    case Types.TIMESTAMP_WITH_TIMEZONE:
                    case Types.TIME_WITH_TIMEZONE:
                        columnData.setDate(true);
                }

                tableInfoData.addColumn(columnData);
            }
        }

        // collect all primary keys columns
        for (String database: databases.keySet()) {
            if (!database.isEmpty()) {
                for (String table: databases.get(database)) {
                    if (table != null && !table.isEmpty()) {

                        List<String> pkColumns = DatabaseCache.getInstance().getTablePrimaryColumnsNamesLowerCase(connectionParam.cacheKey(), database, table);
                        for (String c: pkColumns) {
                            ColumnData cd = tableInfoData.getColumnByNameAndDatabaseAndTable(c, database, table);
                            if (cd != null) {
                                cd.setPrimaryKey(true);
                            }
                        }

                        // find defaults and aotuincrement info
//                        try (ResultSet fullFieldsResultSet = service.getColumnInformationResultSet(database, table)) {
//                            while (fullFieldsResultSet != null && fullFieldsResultSet.next()) {
//                                String extra = fullFieldsResultSet.getString("Extra");
//                                String columnname = fullFieldsResultSet.getString("Field");
//                                String defaults = fullFieldsResultSet.getString("Default");                    
//                                String datatype = fullFieldsResultSet.getString("Type");
//
//                                ColumnData columnData = tableInfoData.getColumnByNameAndDatabaseAndTable(columnname, database, table);
//                                if (columnData != null) {
//                                    columnData.setDefaultValue(defaults);
//                                    columnData.setDataType(((AbstractDBService)service).getDatatype(datatype));
//
//                                    if (extra.equals("auto_increment") && columnData.isPrimaryKey()) {
//                                        columnData.setAutoincrement(true);
//                                    }
//                                }
//                            }
//                        } catch (SQLException ex) {
//                            // just ignore for non select queries
//                        }
                    }
                }
            }
        }
    }
    
   
    public void respondToTreeViewClicks(TableProvider o) {
        forTab = false;
        clearFilter();
        reinit(o);
    }
    
    public void clearGrid() {
        header.setDisable(true);
        if (tableview != null) {
            tableview.getItems().clear();
        }
        changeScreen(null);
    }
    
    /**
     * Responsible for changing between main view screens 
     * it sets all node in the parentView to invisible except
     * the injected node
     * 
     * @param node 
     * the node to exempt in the invisibility
     */
    private void changeScreen(Node node){
        
        currentScreen = node;
        
        if(contentHolder.getChildren().isEmpty()){
            return;
        }
        
        for(Node n : contentHolder.getChildren()){
            if (!n.visibleProperty().isBound()) {
                if(n == node){
                    n.setVisible(true);
                    continue;
                }
                n.setVisible(false);
            }
        }
        
        if (node != null) {
            
            if(node == formViewContainer){
                upperFooter.setVisible(true);
                if (!resultFooter.isVisible()) {
                    footer.setVisible(true);
                }
            }else{
                upperFooter.setVisible(false);
                if (!resultFooter.isVisible()) {
                    footer.setVisible(true);
                }
            }
            
            Platform.runLater(() -> {
                node.toFront();
            });
        }
    }
    
    public void peekTabpane(TabPane tp) {        
        
        if (peekTabpaneListener == null) {
            peekTabpaneListener = new ChangeListener<Number>(){
                /**
                 * PeekPoint is height at which height calculations begin
                 * 
                 */
                final double peekPoint = 100.0,/*was 300 before 200 */
                /**
                 * footerHeight is the height of the footer plus margin. which must be
                 * static and not adjusted
                 */
                footerHeight = 70.0;

                private void bondToNodes(double newH) {
                    double finalHeight = newH - footerHeight;
                    mainUI.setPrefHeight(finalHeight);
                    contentHolder.setPrefHeight(finalHeight);
                    formViewContainer.setPrefHeight(finalHeight);
                    textUI.setPrefHeight(finalHeight);
                    adjustDynamicTablePaneOnUI();
                }

                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                    double height = t1.doubleValue();
                    if (height > peekPoint) {                    
                        bondToNodes(height);
                    }
                }

            };
        }
        if (peekTabpaneHeightProperty != null) {
            peekTabpaneHeightProperty.removeListener(peekTabpaneListener);
        }

        peekTabpaneHeightProperty = tp.heightProperty();
        peekTabpaneHeightProperty.addListener(peekTabpaneListener);         
    }
    
    ChangeListener<Scene> parentListener_CloseListener = new ChangeListener<Scene>() {

        @Override
        public void changed(ObservableValue<? extends Scene> ov, Scene t, Scene t1) {
            if(t1 == null){
                /**
                 * Has been detached from the scenegraph.
                 * required for tabs
                 * 
                 * REASON: both bottom tab table data and centered tab table data has this
                 * but the bottomtab table data (a.k.a Btab) lives irrespective of it lossing
                 * parent. The Btab is in a bottom tabpane which lives in a split pane, when the
                 * query tab is clicked in the center pane, the bottom node in the split pane is
                 * removed hence it will loose its parent; BUT
                 * for centered tab table data (a.k.a Ctab) acts differently, hence the Ctab
                 * checks whether its parent is alive or dead if dead that means it as been removed
                 * from the scene graph for good.
                 * But it is not always the case that it will be removed from the scene graph maybe
                 * it was closed while still attached to the scenegraph, when this scenario happens
                 * the framework doesn't kill the hierachy children in a succession so this listener
                 * does not trigger hence the else code here is also required for Ctab controller.
                 * but for Btab only the else code
                 * 
                 * All these are related to the functioning of the background thread
                 */
                
                /**
                 * side notice related to forTab regarding formView node items.
                 * Since we recycle FormView grid labels and TextFields  when this Ctab is dying
                 * we will get its grid items and recycle them so we can use it in the next call
                 * for Btab or Ctab.
                 */
                if (forTab) {
                    if (grid != null) {
                        for (Node n : grid.getChildren()) {
                            if (n == null) {
                                continue;
                            }
                            if (n instanceof Label) {
                                Label l = (Label) n;
                                l.setFont(Font.getDefault());
                                l.setId("");
                                l.setText("");
                            } else {
                                TextField l = (TextField) n;
                                l.setId("");
                                l.setText("");
                                //l.focusedProperty().removeListener((ChangeListener<Boolean>) l.getUserData());
                                l.setUserData(null);
                            }

                            Recycler.addItem(n);
                        }
                        grid.getChildren().clear();
                    }
                    
                    ((Stage) t.getWindow()).removeEventFilter(
                        WindowEvent.WINDOW_CLOSE_REQUEST, allHandlers);
                }
                //it will be called once and after removed, why this is importatnt is addressed-Btab
                mainUI.sceneProperty().removeListener(this);                                
            }else{                             
                ((Stage) t1.getWindow()).addEventFilter(
                        WindowEvent.WINDOW_CLOSE_REQUEST, allHandlers);
            }        
        }
    };
    
    private boolean perished = false;
    private void reinit(TableProvider table) {
        
         // this table indicate that we show result set using table, not query
        selectedTable = table;
                
        if (perished) {
            resetData();
        }        
            
        String query = selectedTable != null ? "select * from `" + table.getDatabase().getName() + "`.`" + table.getName() + "`" : baseQuery;
     
        init(null, null, query, session, editMode, multiMode, limitRowCheckBox.isSelected());
    }
     
    private boolean forTab = false;
    
    public void toggleForTab(boolean bool){
        forTab = bool;
        // we must try remove first - listener must be only one
        mainUI.sceneProperty().removeListener(parentListener_CloseListener);
        mainUI.sceneProperty().addListener(parentListener_CloseListener);
    }
    
    private volatile ResultSet rs;
    private volatile Statement statement;
    private volatile Connection connection;
    private volatile boolean forceStop = false;
    private boolean editMode;
    private boolean multiMode;
    
    private String initedQuery;
    
    private StringProperty totalRows = new SimpleStringProperty("0 row(s)");
    
    public StringProperty totalRowsProperty() {
        return totalRows;
    }
    
    private class InitObjectHolder{        
        ResultSet rs;
        String initQuery;
        ConnectionSession session;
        Connection connection;
        boolean editMode;
        boolean multiMode;
        boolean withLimit;
        
        public InitObjectHolder(Connection conn, ResultSet rs, String initQuery, ConnectionSession session, boolean editMode, boolean multiMode, boolean withLimit) {
            this.rs = rs;
            this.connection = conn;
            this.initQuery = initQuery;
            this.session = session;
            this.editMode = editMode;
            this.multiMode = multiMode;
            this.withLimit = withLimit;
        }
        
    }
    
    public void chageFont(String font) {
        updateFonts(font);
        fixHeight();
        tableview.refresh();
    }
    
    private void updateFonts(String font) {
        currentFont = FontUtils.parseFXFont(font);
        fm = new javax.swing.JPanel().getFontMetrics(FontUtils.parseAWTFont(font));
    }
    
    private void fixHeight() {
        if (currentFont != null) {
            Text helper = new Text();
            helper.setFont(currentFont);
            helper.setText("XX");
            helper.setLineSpacing(0);
            double height = Math.ceil(helper.getLayoutBounds().getHeight());
            if (height > 0) {
                tableview.setFixedCellSize(height + 4);
            }
        }
    }
    
    public void init(Connection conn, ResultSet rs, String initQuery, ConnectionSession session, boolean editMode, boolean multiMode, boolean withLimit) {
        this.initedQuery = initQuery;
        initWorkProc.setAdditionalData(null);
        initWorkProc.setAdditionalData(new InitObjectHolder(conn, rs, initQuery, session, editMode, multiMode, withLimit));
        Recycler.addWorkProc(initWorkProc);
    }
    
    private WorkProc<InitObjectHolder> initWorkProc = new WorkProc<InitObjectHolder>() {
        @Override
        public void updateUI() {}

        @Override
        public void run() { 
            
            String query = getAddData().initQuery;
            try {
                synchronized (RSlocker) {
                    busy.set(true);
                    
                    baseQuery = query;

                    try {
                        offsetLimit = Integer.valueOf(preferenceService.getPreference().getPaggingNumberRows()); //getAddData().session.getConnectionParam().getLimitResult();
                    } catch (Throwable th) {
                        offsetLimit = 1000;
                    }
                    
                    limitRowCheckBox.setSelected(getAddData().withLimit);

                    if (!query.isEmpty() && query.toUpperCase().startsWith("SELECT")) {
                        if (!query.toUpperCase().contains("LIMIT")) {
                            if (limitRowCheckBox.isSelected()) {
                                if (query.trim().endsWith(";")) {
                                    query = query.trim().substring(0, query.trim().length() - 1);
                                }
                                query += " LIMIT " + startLimit + ", " + offsetLimit + ";";
                            }
                            hasLimit.set(false);
                        } else {
                            limitRowCheckBox.setSelected(false);
                            hasLimit.set(true);
                        }
                    }                    
                    
                    if (forceStop) {
                        busy.set(false);
                        forceStop = false;
                        return;
                    }
                    
                    if (getAddData().rs == null && !query.isEmpty()) {
                        ((MysqlDBService)getAddData().session.getService()).changeDatabase(getAddData().session.getConnectionParam().getSelectedDatabase());
                        statement = getAddData().session.getService().getDB().createStatement();
                        rs = statement.executeQuery(query);
                        connection = statement.getConnection();
                    } else {
                        rs = getAddData().rs;
                        connection = getAddData().connection;
                    }
                                        
                    if (forceStop) {
                        busy.set(false);
                        forceStop = false;
                        return;
                    }
                    
                    session = getAddData().session;
                    editMode = getAddData().editMode;
                    multiMode = getAddData().multiMode;
                }
                
                resultFooter.setManaged(getAddData().multiMode);
                resultFooter.setVisible(getAddData().multiMode);
                footer.setManaged(!getAddData().multiMode);
                if (getAddData().multiMode) {
                    Platform.runLater(() -> {
                        resultLabel.setText(baseQuery.replaceAll("[\r\n]+", " ").replaceAll("\\s+", " "));
                    });
                }

                if (getAddData().editMode && !getAddData().multiMode) {
                    //its table data
                    Platform.runLater(() -> {
                        header.getItems().remove(readEditComboBox);
                    });
                    if (forTab) {
                        Recycler.addWorkProc(myWorkProc);
                        perished = true;
                        header.setDisable(false);
                    } else {
                        //this logic mainly has to do with the showing of the notice label-first time
                        if (query.isEmpty()) {
                            Platform.runLater(() -> {
                                contentHolder.getChildren().add(notice);                    
                            });
                        }else{//below to is to run normal work                    
                            Recycler.addWorkProc(myWorkProc);
                            perished = true;
                            header.setDisable(false);
                        }
                    }
                } else {
                    if (!query.isEmpty()) {
                        header.setDisable(false);
                        perished = true;
                        myWorkProc.run();
                        Platform.runLater(() -> {
                            myWorkProc.updateUI();
                        });
                    }
                }
            } catch (SQLException ex) {
                log.error("Error", ex);
                rs = null;
                statement = null;
                Platform.runLater(() -> {
                    ((MainController)getController()).showError("Error", "an error occurred while re-initiating", ex);
                });
            }catch(Exception e){
                log.error("Error", e);
            }
        
        }
    };
    
    public void resetData() {
        if (data != null) {

            tableview.scrollToColumn(tableview.getColumns().get(0));
            tableview.scrollTo(0);

            tableview.getColumns().removeAll();
            tableview.refresh();

            data.clear();
            sourceData.clear();
            
            grid.getChildren().clear();


            textUI.clear();//getEngine().load(InformationModule.class.getResource("tableData.html").toExternalForm());
            firstColumn = null;//since columns are cleared
            tableInfoData = null;
            //////others that i think is clearable //////////
            noNeedSave = false;

            data = null;

            //tableInfoData = null; can be uncommented; doesn't make any special diff
        }
    }
    
    
    private void selection(Boolean selection) {
        for (int i = 0; i < data.size(); i++) {
            data.get(i).selected().set(selection);
        }
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        updateFonts(preferenceService.getPreference().getFontsOthers());
        fixHeight();
        
        visualExplainButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getVisualExplainTab_image()));
        exportAsButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getTableExport_small_image()));
        exportAsXlsButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getXls_image()));
        exportAsPdfButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getPdf_image()));
        copyMenuButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getCopyTableData_image()));
        insertNewRowButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getInsertNewRow_image()));
        duplicateRowButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getDuplicateCurrentRow_image()));
        saveBtn.setGraphic(standardSizeImageviewForResultTab(StandardDefaultImageProvider.getInstance().getSave_image()));
        deleteButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getDelete_image()));
        cancelChangesButton.setGraphic(standardSizeImageviewForResultTab(StandardDefaultImageProvider.getInstance().getCancelChanges_image()));
        gridViewButton.setGraphic(standardSizeImageviewForResultTab(StandardDefaultImageProvider.getInstance().getGridView_image()));
        formViewButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getFormView_image()));
        textviewButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getTextView_image()));
        refreshButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getRefreshObjectBrowser_image()));
        filterButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getFilter_small_image()));
        decrementButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getLeftArrow_small_image()));
        incrementButton.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getRightArrow_small_image()));
        previouslast.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getPreviousLast_image()));
        previous.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getPrevious_image()));
        nextlast.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getNextLast_image()));
        next.setGraphic(standardSizeImageviewForResultTab(SmartImageProvider.getInstance().getNext_image()));
                
        isEditable = new SimpleBooleanProperty();
        isNullable = new SimpleBooleanProperty();
        isHasDefault = new SimpleBooleanProperty();
        isSorted = new SimpleBooleanProperty();
        dataFiltered = new SimpleBooleanProperty();
        
//        textUI.setContextMenuEnabled(false);
        
        limitRowCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (session != null) {
                session.getConnectionParam().setWithLimit(newValue != null && newValue);
            }
        });
        
        busyPane.visibleProperty().bind(busy);
        busyPane.visibleProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                Platform.runLater(() -> {
                    busyPane.toFront();
                });
            }
        });
        copyMenuButton.disableProperty().bind(textUI.visibleProperty().or(busy));
        readEditComboBox.disableProperty().bind(textUI.visibleProperty().or(busy));
        insertNewRowButton.disableProperty().bind(isEditable.not().or(textUI.visibleProperty()).or(busy));
        duplicateRowButton.disableProperty().bind(isEditable.not().or(textUI.visibleProperty()).or(busy));
        saveBtn.disableProperty().bind(Bindings.isEmpty(changesMap).and(Bindings.isEmpty(newRowsSet)).or(textUI.visibleProperty()).or(busy));
        deleteButton.disableProperty().bind(isEditable.not().or(textUI.visibleProperty()).or(busy));
        cancelChangesButton.disableProperty().bind(Bindings.isEmpty(changesMap).and(Bindings.isEmpty(newRowsSet)).or(textUI.visibleProperty()).or(busy));
        
        upperFooter.managedProperty().bind(upperFooter.visibleProperty());
        
        tableview.getSortOrder().addListener((ListChangeListener.Change<? extends TableColumn<RowData, ?>> change) -> {
            isSorted.set(!tableview.getSortOrder().isEmpty());
        });
        
        tableview.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends RowData> 
                observableValue, RowData oldValue, RowData newValue) -> {
            //Check whether item is selected and set value of selected item to Label
            if (tableview.getSelectionModel().getSelectedItem() != null && !noNeedSave) {
                Platform.runLater(() -> {
                   if (!saveButtonAction(null)) {
                       boolean old = noNeedSave;
                       noNeedSave = true;
                       tableview.getSelectionModel().select(tableview.getItems().indexOf(oldValue), tableview.getColumns().get(0));
                       noNeedSave = old;
                   }
                });
            }
        });
        
        filterMenuButton.disableProperty().bind(busy);
        
        limitRowCheckBox.disableProperty().bind(busy.or(hasLimit));
        limitRowsTextField.disableProperty().bind(busy.or(limitRowCheckBox.selectedProperty().not()));
        moveHBox.disableProperty().bind(busy.or(limitRowCheckBox.selectedProperty().not()));
                
        filterButton.disableProperty().bind(busy);
        tableview.disableProperty().bind(busy);
        gridViewButton.disableProperty().bind(busy);
        formViewButton.disableProperty().bind(busy);
        textviewButton.disableProperty().bind(busy);
        
        visualExplainButton.disableProperty().bind(busy);
        exportAsButton.disableProperty().bind(busy);
        exportAsXlsButton.disableProperty().bind(busy);
        exportAsPdfButton.disableProperty().bind(busy);
        
        busy.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            Platform.runLater(() -> {
                refreshButton.setGraphic(standardSizeImageviewForResultTab(newValue == null || !newValue ? SmartImageProvider.getInstance().getRefreshObjectBrowser_image()
                        : SmartImageProvider.getInstance().getDisconnectAll_image()));
                refreshButton.setTooltip(newValue == null || !newValue ? new Tooltip("Refresh") : new Tooltip("Stop Execution"));
            });
        });
                
//        tableview.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableview.getSelectionModel().getSelectedCells().addListener((ListChangeListener.Change<? extends TablePosition> change) -> {
            List<TablePosition> list = tableview.getSelectionModel().getSelectedCells();
            TableInfoData staticTableInfoData = tableInfoData;
            if (!list.isEmpty() && staticTableInfoData != null) {
                ColumnData column = staticTableInfoData.getColumnByIndex(list.get(0).getColumn() - 1);
                if (column != null) {
                    isHasDefault.set(column.getDefaultValue() != null);
                    isNullable.set(column.isNullable());
                    
                    RowData row = tableview.getSelectionModel().getSelectedItem();
                    // if null but data not empty - than its first time selection
                    // and we must get row at 0
                    if (row == null && !data.isEmpty()) {
                        row = data.get(0);
                    }
                    
                    if (row != null && row.isDataRow()) {
                        Object v = row.getData().get(list.get(0).getColumn() - 1);
                        if (v == null) {
                            v = "(NULL)";
                        }
                        
                        String value = v.toString();                        
                        
                        tableNameEquals.setText(FILTER_TEMPLATE__EQUALS.replace("CHARACTER_SETS", value));
                        tableNameEquals.setUserData(value);
                        tableNameEquality.setText(FILTER_TEMPLATE__NONEQUALS.replace("CHARACTER_SETS", value));
                        tableNameEquality.setUserData(value);
                        tableNameGreater.setText(FILTER_TEMPLATE__GREATER.replace("CHARACTER_SETS", value));
                        tableNameGreater.setUserData(value);
                        tableNameLess.setText(FILTER_TEMPLATE__LESS.replace("CHARACTER_SETS", value));
                        tableNameLess.setUserData(value);
                        
                        tableName_Like.setText(FILTER_TEMPLATE__LIKE_BEFORE.replace("CHARACTER_SETS", value));
                        tableName_Like.setUserData(value);
                        tableNameLike_.setText(FILTER_TEMPLATE__LIKE_AFTER.replace("CHARACTER_SETS", value));
                        tableNameLike_.setUserData(value);
                        tableName_Like_.setText(FILTER_TEMPLATE__LIKE_BOTH.replace("CHARACTER_SETS", value));
                        tableName_Like_.setUserData(value);
                    }
                }
            }
        });
        
//        tableview.setFixedCellSize(20.0);
                
        readEditComboBox.valueProperty().addListener((ObservableValue<? extends String>
                ov, String t, String t1) -> {
//            log.debug("readEditComboBox value changed {}", t1);

            TableInfoData staticTableInfoData = tableInfoData;
            boolean editable = t1 != null && staticTableInfoData != null && staticTableInfoData.getTables().contains(t1);
            
            tableview.setEditable(editable);
            isEditable.setValue(editable);
            
        });
            
        limitRowsTextField.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            try {
                offsetLimit = Integer.valueOf(t1);
                if (session.getConnectionParam().getLimitResult() != offsetLimit) {
                    session.getConnectionParam().setLimitResult(offsetLimit);
                    connectionParamService.save(session.getConnectionParam());
                }
            } catch (NumberFormatException ex) {
            }
        });
        
        limitRowsTextField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue != null && oldValue && (newValue == null || !newValue)) {
                
                if (session.getConnectionParam().getLimitResult() != offsetLimit) {
                    session.getConnectionParam().setLimitResult(offsetLimit);
                    connectionParamService.save(session.getConnectionParam());
                }
            }
        });
        
        limitRowCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            refreshAction(null);
        });
        
        next.setOnAction(onActions);
        nextlast.setOnAction(onActions);
        decrementButton.setOnAction(onActions);
        incrementButton.setOnAction(onActions);
        previous.setOnAction(onActions);
        previouslast.setOnAction(onActions);
        formViewButton.setOnAction(onActions);
        gridViewButton.setOnAction(onActions);
        textviewButton.setOnAction(onActions);
        contentHolder.getChildren().remove(notice);
//        tableview.setPrefWidth(TableView.USE_COMPUTED_SIZE);
        
        
        filterTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            filterByField();
        });
        
        regexCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            filterByField();
        });
        
        notMatchCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            filterByField();
        });
        
        
        
        tableview.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.C) {
                copyCellAction(null);
                event.consume();
                
            } else if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.F) {
                            
                event.consume();
            }
        });
        
        textUI.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.C) {
                copyCellAction(null);
                event.consume();
                
            } else if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.F) {
            
                if (isTextView()) {
                    QueryTabModule queryTabModule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
                    queryTabModule.showFindDialog(new ResultTabFindObject(textUI));
                }
                
                event.consume();
            }
        });
        
        adjustDynamicTablePaneOnUI();
    }
    
    private void adjustDynamicTablePaneOnUI(){
        Platform.runLater(()-> AnchorPane.setTopAnchor(dynamicTablePane, header.getHeight()) );
    }
    
    private void filterByField() {
        restoreFilteredOut();

        // clear default filter
        filterData = null;
        doFilter();
        
        // make filter
        List<RowData> accepted = new ArrayList<>();

        int idx = -1;
        for (RowData row: data) {
            idx++;

            if (row.isNewRow()){
                continue;
            }

            if (acceptFilter(row)) {
                accepted.add(row);
            } else {
                filteredOut.put(row, idx);
            }
        }

        for (RowData row: accepted) {
            if (!data.contains(row)) {
                data.add(row);
            }
        }

        if (filteredOut.isEmpty()){
            //quick fix, update the table any way
            RowData rd = new RowData();
            tableview.getItems().add(rd);
            tableview.getItems().remove(rd);
        }else {
            tableview.getItems().removeAll(filteredOut.keySet());
        }

        tableview.sort();
    }
    
    
    private boolean acceptFilter(RowData row) {
        String filterCondition = filterTextField.getText();
        if (!filterCondition.isEmpty()) {
            
            try {
                Pattern pattern = regexCheckBox.isSelected() ? Pattern.compile(filterCondition) : null;

                String filterConditionLower = filterCondition.toLowerCase();

                boolean result = false;
                for (String filterValue: row.getValuesFoFilter()) {
                    if (regexCheckBox.isSelected() && pattern != null) {
                        // if regex - need check using regex
                        result = pattern.matcher(filterValue).find();
                    } else {
                        // else just

                        result = filterValue.toLowerCase().contains(filterConditionLower);
                    }

                    if (result) {
                        break;
                    }
                }

                if (notMatchCheckBox.isSelected()) {
                    result = !result;
                }

                return result;
            } catch (Throwable th) {
                // my be some errors in pattern compile - its ok just ignore and give user write full regex
            }
        }
        
        return true;
    }
    
    private void reCalcTableViewColumns() {
        double width = 0;
        double highestColumn = 0.0;
        for (int i = 0; i < tableview.getColumns().size(); i++) {
            double d = tableview.getColumns().get(i).getWidth();
            width += d;
            if(highestColumn < d){
                highestColumn = d;
            }
        }
        
        tableview.setPrefWidth(width + tableview.getColumns().size());        
    }     
    
    public String getInitedQuery() {
        return initedQuery;
    }
    
    @FXML
    void visualExplain(ActionEvent event) {
        if (initedQuery != null) {
            showVisualExplain(getController().getSelectedConnectionSession().get().getConnectionParam().getSelectedDatabase(), initedQuery,
                getController(), 
                ((MainController)getController()).getColumnCardinalityService(),
                ((MainController)getController()).getPreferenceService(), 
                ((MainController)getController()).getPerformanceTuningService(), 
                ((MainController)getController()).getRecommendationService(), 
                ((MainController)getController()).getUsageService(),
                null);
            }
    }
    
    // <editor-fold desc=" Export Actions ">
    
    @FXML
    public void exportTable(ActionEvent event) {
        TableInfoData staticTableInfoData = tableInfoData;
        ObservableList<String> columnsData = staticTableInfoData != null ? staticTableInfoData.getColumnsData() : null;
        if (columnsData != null && data != null) {            
            ExportTableController exportTableController = ((MainController)getController()).getTypedBaseController("ExportTable");
            if (exportTableController == null) {
                log.debug("exporttablecontroller null");
            }
            
            Scene scene = new Scene(exportTableController.getUI());
            ObservableList<ObservableList> datas = FXCollections.observableArrayList();
            ObservableList<ObservableList> selectedDatas = FXCollections.observableArrayList();
            boolean selectedFound = false;
            
            
            for (RowData row : data) {
                if (row.isDataRow()) {
                    if (row.selected().get()) {
                        selectedFound = true;
                        selectedDatas.add(FXCollections.observableList(row.getData()));
                    } else {
                        if (selectedFound == false) {
                            datas.add(FXCollections.observableList(row.getData()));
                        }
                    }
                }
            }

            ObservableList<ObservableList> exportData = selectedFound ? selectedDatas : datas;

            if (selectedFound) {
                System.out.println("exported from selected :" + selectedDatas.size());
            } else {
                System.out.println("exported from unselected :" + datas.size());
            }
            
            if (exportData.size() > ExportUtils.MAX_ROWS_COUNT) {
                ((MainController)getController()).showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ", only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                exportData.remove(ExportUtils.MAX_ROWS_COUNT, exportData.size());
            }
            try {
                exportTableController.init(service, null, exportData, columnsData);

                Stage stage = new Stage();
                stage.setTitle("Export As");
                stage.initStyle(StageStyle.UTILITY);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(getStage());
                stage.setScene(scene);
                stage.showAndWait();
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "Error exporting table", ex);
            }
        }
    }
    
    @FXML
    void exportTableAsXls(ActionEvent event) {
        TableInfoData staticTableInfoData = tableInfoData;
        ObservableList<String> columnsData = staticTableInfoData != null ? staticTableInfoData.getColumnsData() : null;
        if (columnsData != null && data != null) {
            
            ExportTableAsXlsController exportTableAsXlsController = ((MainController)getController()).getTypedBaseController("ExportTableAsXls");
            if (exportTableAsXlsController == null) {
                log.debug("exportTableAsXlsController null");
            }
            Scene scene = new Scene(exportTableAsXlsController.getUI());
            ObservableList<ObservableList> datas = FXCollections.observableArrayList();
            ObservableList<ObservableList> selectedDatas = FXCollections.observableArrayList();
            boolean selectedFound = false;
            
            for (RowData row : data) {
                if (row.isDataRow()) {
                    if (row.selected().get()) {
                        selectedFound = true;
                        selectedDatas.add(FXCollections.observableList(row.getData()));
                    } else {
                        if (selectedFound == false) {
                            datas.add(FXCollections.observableList(row.getData()));
                        }
                    }
                }
            }

            ObservableList<ObservableList> exportData = selectedFound ? selectedDatas : datas;

            if (selectedFound) {
                System.out.println("exported from selected :" + selectedDatas.size());
            } else {
                System.out.println("exported from unselected :" + datas.size());
            }
            
            if (exportData.size() > ExportUtils.MAX_ROWS_COUNT) {
                ((MainController)getController()).showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ", only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                exportData.remove(ExportUtils.MAX_ROWS_COUNT, exportData.size());
            }
            
            try {
                exportTableAsXlsController.init(service, exportData, columnsData);

                Stage stage = new Stage();
                stage.initStyle(StageStyle.UTILITY);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(getStage());
                stage.setTitle("Export As XLS");
                stage.setScene(scene);
                stage.showAndWait();
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "Error exporting xls", ex);
            }
        }
    }
    
    @FXML
    void exportTableAsPdf(ActionEvent event) {
        TableInfoData staticTableInfoData = tableInfoData;
        ObservableList<String> columnsData = staticTableInfoData != null ? staticTableInfoData.getColumnsData() : null;
        if (columnsData != null && data != null) {
            
            ExportTableAsPdfController exportTableAsPdfController = ((MainController)getController()).getTypedBaseController("ExportTableAsPdf");
            if (exportTableAsPdfController == null) {
                log.debug("exportTableAsPdfController null");
            }
            Scene scene = new Scene(exportTableAsPdfController.getUI());
            ObservableList<ObservableList> datas = FXCollections.observableArrayList();
            ObservableList<ObservableList> selectedDatas = FXCollections.observableArrayList();
            boolean selectedFound = false;
 
            for (RowData row : data) {
                if (row.isDataRow()) {
                    if (row.selected().get()) {
                        selectedFound = true;
                        selectedDatas.add(FXCollections.observableList(row.getData()));
                    } else {
                        if (selectedFound == false) {
                            datas.add(FXCollections.observableList(row.getData()));
                        }
                    }
                }
            }
            
            ObservableList<ObservableList> exportData = selectedFound ? selectedDatas : datas;

            if (selectedFound) {
                System.out.println("exported from selected :" + selectedDatas.size());
            } else {
                System.out.println("exported from unselected :" + datas.size());
            }
            
            if (exportData.size() > ExportUtils.MAX_ROWS_COUNT) {
                ((MainController)getController()).showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ", only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                exportData.remove(ExportUtils.MAX_ROWS_COUNT, exportData.size());
            }
            exportTableAsPdfController.init(service, exportData, columnsData);

            Stage stage = new Stage();
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            stage.setTitle("Export As PDF");
            stage.setScene(scene);
            stage.showAndWait();
        }
    }
    
    // </editor-fold>

    
    // <editor-fold desc=" Copy actions ">
    
    @FXML
    void copyCellAction(ActionEvent event) {
        RowData row = tableview.getSelectionModel().getSelectedItem();
        if (row != null) {
            List<TablePosition> list = tableview.getSelectionModel().getSelectedCells();
            if (!list.isEmpty()) {
                Object value = row.getData().get(list.get(0).getColumn() - 1);
                if (value == null) {
                    value = "(NULL)";
                } else {
                    value = value.toString();
                }
                
                Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
                Clipboard systemClipboard = defaultToolkit.getSystemClipboard();
                
                systemClipboard.setContents(new StringSelection((String) value), null);
            }
        }
    }
    
    @FXML
    void copyAllRowsAction(ActionEvent event) {
        TableInfoData staticTableInfoData = tableInfoData;
        ObservableList<String> columnsData = staticTableInfoData != null ? staticTableInfoData.getColumnsData() : null;
        if (columnsData != null && data != null) {            
            CopyResultController copyResultController = ((MainController)getController()).getTypedBaseController("CopyResult");
            
            Scene scene = new Scene(copyResultController.getUI());
            
            ObservableList<ObservableList> datas = FXCollections.observableArrayList();
            
            for (RowData row : data) {
                if (row.isDataRow()) {
                    datas.add(FXCollections.observableList(row.getData()));
                }
            }
            
            copyResultController.init(datas, columnsData);

            Stage stage = new Stage();
            stage.setTitle("Copy Result Data");
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            stage.setScene(scene);
            stage.showAndWait();
        }
    }
    
    @FXML
    void copySelectedRowsAction(ActionEvent event) {
        TableInfoData staticTableInfoData = tableInfoData;
        ObservableList<String> columnsData = staticTableInfoData != null ? staticTableInfoData.getColumnsData() : null;
        if (columnsData != null && data != null) {            
            CopyResultController copyResultController = ((MainController)getController()).getTypedBaseController("CopyResult");
            
            Scene scene = new Scene(copyResultController.getUI());
            ObservableList<ObservableList> datas = FXCollections.observableArrayList();
            
            for (RowData row : data) {
                if (row.isDataRow()) {
                    if (row.selected().get()) {
                        datas.add(FXCollections.observableList(row.getData()));
                    }
                }
            }
            
            if (datas.isEmpty()) {
                RowData selectedRow = tableview.getSelectionModel().getSelectedItem();
                if (selectedRow != null && selectedRow.isDataRow()) {
                    datas.add(FXCollections.observableList(selectedRow.getData()));
                }
            }
            
            if (!datas.isEmpty()) {
                copyResultController.init(datas, columnsData);

                Stage stage = new Stage();
                stage.setTitle("Copy Selected Result Data");
                stage.initStyle(StageStyle.UTILITY);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.initOwner(getStage());
                stage.setScene(scene);
                stage.showAndWait();
            }
        }
    }
    // </editor-fold>
    
    
    // <editor-fold desc=" Edit actions ">
    
    @FXML
    void cancelChangesButtonAction(ActionEvent event) {
        if (!newRowsSet.isEmpty()) {
            for (RowData row: newRowsSet) {
                data.remove(row);
            }
            
            newRowsSet.clear();
        }
        
        if (!changesMap.isEmpty()) {
            for (RowData row: changesMap.keySet()) {
                row.setData(changesMap.get(row).getData());
            }
            
            changesMap.clear();
        }
        
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData != null) {
            formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
        }
        
        if (firstColumn != null) {
            firstColumn.setVisible(false);
            firstColumn.setVisible(true);
        }
    }
    
    @FXML
    void insertNewRowButtonAction(ActionEvent event) {
        // force save
        if (!saveButtonAction(event)) {
            return;
        }

        noNeedSave = true;

        RowData row = getNewRow();
        fixPrimaryKey(row);
        data.add(row);

        newRowsSet.add(row);
        
        tableview.getSelectionModel().select(data.size() - 2, firstColumn);
        tableview.scrollTo(row);
        if (currentScreen == formViewContainer) {
            forFormPos = data.size() - 1;
            TableInfoData staticTableInfoData = tableInfoData;
            if (staticTableInfoData != null) {
                formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
            }
        }
        noNeedSave = false;
    }
    
    
    @FXML
    void duplicateRowButtonAction(ActionEvent event) {
        int duplicateIndex = tableview.getSelectionModel().getSelectedIndex();
        if (duplicateIndex >= 0 && duplicateIndex < data.size() - 1) {
            
            // force save
            if (!saveButtonAction(event)) {
                return;
            }

            noNeedSave = true;
            
            RowData row = getNewRow();
            row.setData(new ArrayList<>(data.get(duplicateIndex).getData()));
            fixPrimaryKey(row);
            data.add(row);

            newRowsSet.add(row);

            tableview.getSelectionModel().select(data.size() - 2, firstColumn);
            tableview.scrollTo(row);
            if (currentScreen == formViewContainer) {
                forFormPos = data.size() - 1;
                TableInfoData staticTableInfoData = tableInfoData;
                if (staticTableInfoData != null) {
                    formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
                }
            }
            noNeedSave = false;
        }
    }
    
    @FXML
    void deleteButtonAction(ActionEvent event) {
        
        DialogResponce responce = ((MainController)getController()).showConfirm("Delete", 
            "Do you really want to delete the currently selected row(s)");
        
        if (responce == DialogResponce.OK_YES) {
            ObservableList<RowData> datas = FXCollections.observableArrayList();
            
            if (currentScreen == formViewContainer) {
                datas.add(data.get(forFormPos));
            } else {
                for (RowData row : data) {
                    if (row.selected().get()) {
                        datas.add(row);
                    }
                }

                if (datas.isEmpty()) {
                    RowData selectedRow = tableview.getSelectionModel().getSelectedItem();
                    if (selectedRow != null && selectedRow.isDataRow()) {
                        datas.add(selectedRow);
                    }
                }
            }

            try {
                for (RowData rd: datas) {
                    if (!rd.isNewRow()) {
                        executeDeleteSQL(rd);
                    } else {
                        noNeedSave = true;
                        data.remove(rd);
                        newRowsSet.remove(rd);
                        noNeedSave = false;
                    }
                }
                
                if (currentScreen == formViewContainer) {
                    forFormPos--;
                    if (forFormPos < 0) {
                        forFormPos = 0;
                    }
                    
                    TableInfoData staticTableInfoData = tableInfoData;
                    if (staticTableInfoData != null) {
                        formViewPopulator(forFormPos, staticTableInfoData.getColumnsData());
                    }
                } 
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "an error occurred while deleting row", ex);
            }
        }
    }
    
    @FXML
    boolean saveButtonAction(ActionEvent event) {
                           
        // TODO Need refactor this
        if (READ_ONLY.equals(readEditComboBox.getSelectionModel().getSelectedItem())) {
            return true;
        }

        try {
            // need update all changes rows and insert new rows
            while(!changesMap.isEmpty()) {
                RowData row = changesMap.keySet().iterator().next();
                if (row.isDataRow()) {
                    executeUpdateSQL(row);

                    changesMap.remove(row);

                    // update source data
                    List list = sourceData.get(row).getData();
                    list.clear();
                    list.addAll(row.getData());
                } else {
                    changesMap.remove(row);
                }
            }

            // check new rows
            while(!newRowsSet.isEmpty()) {
                RowData row = newRowsSet.iterator().next();
                if (row.isDataRow()) {
                    if (executeInsertSQL(row) != -1) {
                        newRowsSet.remove(row);

                        RowData newRow = new RowData();
                        newRow.getData().addAll(row.getData());
                        sourceData.put(row, newRow);
                    } else {
                        return false;
                    }
                } else {
                    newRowsSet.remove(row);
                }
            }

//            if (firstColumn != null) {
//                firstColumn.setVisible(false);
//                firstColumn.setVisible(true);
//            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", "an error occurred while saving row", ex);
            return false;
        }
        
        return true;
    }

    
    // </editor-fold>

        
    
    // <editor-fold desc=" Execution block ">
    
    private void executeUpdateSQL(RowData row) throws SQLException {
        
        String selectBeforeUpdateQuery = getSelectBeforeActionSQL(sourceData.get(row));
        if (selectBeforeUpdateQuery != null) {
            try (ResultSet result = (ResultSet) service.execute(selectBeforeUpdateQuery)) {
                if (result.next()) {
                    int count = result.getInt(1);
                    if (count > 1) {                        
                        DialogResponce responce = ((MainController)getController()).showConfirm("Warning", 
                            "There are " + (count - 1) + " duplicates of the row you are trying to update\n"
                            + "Do you want to update all the duplicates?");
                        
                        if (responce == DialogResponce.OK_YES) {
                            doUpdate(row, false);
                        } else if (responce == DialogResponce.NO){
                            doUpdate(row, true);
                        }
                    } else {
                        doUpdate(row, false);
                    }
                }
            }
        }
    }
    
    private void doUpdate(RowData row, Boolean limitOne) {
        String updateSQL = getUpdateSQL(row, limitOne);
        log.debug("update sql :{}", updateSQL);
        int success = -1;
        if (updateSQL != null) {
            try {
                success = (Integer) service.execute(updateSQL);
                log.debug("update sql SUCCESS :{}", success);
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "an error occurred while updating row", ex);
            }
        }
    }
    
    
    private void executeDeleteSQL(RowData row) throws SQLException {
        
        String selectBeforeDeleteQuery = getSelectBeforeActionSQL(sourceData.get(row));
        if (selectBeforeDeleteQuery != null) {
            try (ResultSet result = (ResultSet) service.execute(selectBeforeDeleteQuery)) {
                if (result.next()) {
                    int count = result.getInt(1);
                    if (count > 1) {
                        DialogResponce responce = ((MainController)getController()).showConfirm("Warning", 
                            "There are " + (count - 1) + " duplicates of the row you are trying to delete\n"
                                + "Do you want to delete all the duplicates?");
                        
                        if (responce == DialogResponce.OK_YES) {
                            doDelete(row, false);
                        } else if (responce == DialogResponce.NO){
                            doDelete(row, true);
                        }
                    } else {
                        doDelete(row, false);
                    }
                }
            }
        }
    }
    
    private void doDelete(RowData row, Boolean limitOne) {
        String deleteQuery = getDeleteSQL(row, limitOne);
        if (deleteQuery != null) {
            try {
                int result = (int) service.execute(deleteQuery);
                if (result >= 0) {

                    // remove if exist from changed
                    changesMap.remove(row);

                    data.remove(row);
                    sourceData.remove(row);
                } else {
                    ((MainController)getController()).showError("Error", "Cannot delete selected row(s)", null);
                }
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "There is an error while deleting", ex);
            }   
        }
    }
    
    
    private int executeInsertSQL(RowData rowData) {
        String insertSQL = getInsertSQL(rowData);
        log.debug("INSERT NEW sql :{}", insertSQL);
        int success = -1;
        if (insertSQL != null) {
            try {
                QueryResult result = new QueryResult();

                success = (Integer) service.execute(insertSQL, result);
                if (success != -1) {
                    insertGeneratedKeys(rowData, result);
                    log.debug("INSERT sql SUCCESS :{}", success);
                } else {
                    ((MainController)getController()).showError("Error", "Error while inserting new row", null);
                }
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "an error occurred during insertion", ex);
            }
        }
        
        return success;
    }
    
    
    private void insertGeneratedKeys(RowData rowData, QueryResult result) throws SQLException {
        TableInfoData staticTableInfoData = tableInfoData;
        if (result != null && staticTableInfoData != null) {
            ResultSet set = result.getGeneratedKey();
            if (set != null) {
                while (set.next()) {
                    int key = set.getInt(1);
                    ColumnData row = staticTableInfoData.getAutoIncrementColumn();
                    if (row != null) {
                        int columnIndex = staticTableInfoData.getColumnIndex(row.getName());
                        rowData.getData().set(columnIndex, key);
                        
                        if (firstColumn != null)  {
                            firstColumn.setVisible(false);
                            firstColumn.setVisible(true);
                        }
                    }
                }
            }
        }
    }
    
    
    private void fixPrimaryKey(RowData row) {
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData != null) {
            for (int i = 0; i < row.getData().size(); i++) {

                ColumnData column = staticTableInfoData.getColumnByIndex(i);
                if (column != null) {

                    if (column.isPrimaryKey() && column.isAutoincrement()) {
                        row.getData().set(i, "(AUTO)");

                    } else if (column.isPrimaryKey()) {
                        Long max = 0L;
                        for (RowData r : data) {
                            try {
                                Long id = Long.parseLong(r.getData().get(i).toString());
                                max = id > max ? id : max;
                            } catch (Throwable th) {
                            }
                        }

                        row.getData().set(i, "" + (max + 1));                    
                    }
                }
            }
        }
    }

    private RowData getNewRow() {
        RowData row = new NewRowData();
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData != null) {
            for (int i = 0; i < staticTableInfoData.getColumns().size(); i++) {
                ColumnData column = staticTableInfoData.getColumnByIndex(i);
                if (column != null) {
                    row.getData().add(column.getDefaultValue() == null && column.isPrimaryKey() ? "(AUTO)" : column.getDefaultValue());
                }
            }
        }
        return row;
    }

   /**
    * RowData that is marked as a new row
    */
   private class NewRowData extends RowData{
        @Override
        public boolean isNewRow() {
            return true;
        }
    }
    
    // </editor-fold>
    
    
    // <editor-fold desc=" SQL block ">
    
   // TODO NEED REFACTOR THIS
    public String getUpdateSQL(RowData row, Boolean limitOne) {
        
        String whereClause = "";
        String editingSQL = "";
        
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData == null) {
            return "";
        }
        
        String tableName = readEditComboBox.getSelectionModel().getSelectedItem();
        // check what columns was changed
        for (int j = 0; j < row.getData().size(); j++) {
            RowData sourceRow = sourceData.get(row);
            if (!Objects.equals(row.getData().get(j), sourceRow.getData().get(j))) {
                TableColumn tableColumn = (TableColumn) tableview.getColumns().get(j + 1);
                String columnName = tableColumn.getText();
                
                if (tableName.equals(staticTableInfoData.getTableByColumn(columnName))) {

                    Object v = row.getData().get(j);
                    String columnValue = v != null ? v.toString() : null;
                    if (!editingSQL.equals("")) {
                        editingSQL += ",";
                    }

                    if (columnValue == null || "(NULL)".equals(columnValue)) {
                        columnValue = "null";
                    } else {

                        if (columnValue.startsWith("x'") && columnValue.endsWith("'")) {
                            // do nothing - binary data
                        } else if (!columnValue.equalsIgnoreCase("CURRENT_TIMESTAMP")){
                            columnValue = "'" + StringEscapeUtils.escapeSql(columnValue) + "'";
                        }
                    }

                    editingSQL += "`" + columnName + "`" + " = " + columnValue;
                }
            }
        }
        
        if (editingSQL.equals("")) {
            return null;
        }
        
        List<ColumnData> primaryColumns = staticTableInfoData.getPrimaryKeyColumns(tableName);
        if (primaryColumns != null && !primaryColumns.isEmpty()) {
            for (int i = 0; i < primaryColumns.size(); i++) {
                RowData sourceRow = sourceData.get(row);
                ColumnData pk = primaryColumns.get(i);
                Object v = sourceRow.getData().get(staticTableInfoData.getColumnIndex(pk.getName()));
                String value = v != null ? v.toString() : null;
                if (value != null && !"(NULL)".equals(value)) {
                    value = "= " + (value.startsWith("x'") && value.endsWith("'") ? value : "'" + StringEscapeUtils.escapeSql(value) + "'");
                } else {
                    value = "IS NULL";
                }
                
                whereClause += "`" + pk.getName() + "` " + value + (i < primaryColumns.size() - 1 ? " AND " : "");
            }
        }
        
        String updateSQL = String.format("UPDATE %s SET %s WHERE %s%s;", tableName,
                editingSQL, whereClause, limitOne != null && limitOne ? " LIMIT 1" : "");
        log.debug("updatesql:{}", updateSQL);
        
        return updateSQL;

    }
    
    
    private String getDeleteSQL(RowData row, Boolean limitOne) {
        
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData == null) {
            return "";
        }
        String deleteSQL = null;
        
        String tableName = readEditComboBox.getSelectionModel().getSelectedItem();
        
        List<ColumnData> primaryColumns = staticTableInfoData.getPrimaryKeyColumns(tableName);
        if (primaryColumns != null && !primaryColumns.isEmpty()) {
            
            deleteSQL = "DELETE FROM " + tableName + " WHERE ";
            for (int i = 0; i < primaryColumns.size(); i++) {
                ColumnData pk = primaryColumns.get(i);
                
                RowData sourceRow = sourceData.get(row);
                Object v = sourceRow.getData().get(staticTableInfoData.getColumnIndex(pk.getName()));
                String value = v != null ? v.toString() : null;
                if (value != null && !"(NULL)".equals(value)) {
                    value = "= " + (value.startsWith("x'") && value.endsWith("'") ? value : "'" + StringEscapeUtils.escapeSql(value) + "'");
                } else {
                    value = "IS NULL";
                }
                deleteSQL += "`" + pk.getName() + "` " + value + (i < primaryColumns.size() - 1 ? " AND " : "");
            }
            
            if (limitOne != null && limitOne) {
                deleteSQL += " LIMIT 1";
            }
        }
        
        return deleteSQL;
    }
    
    private String getSelectBeforeActionSQL(RowData row) {
        
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData == null) {
            return "";
        }
        
        String selectSQL = null;
        
        String tableName = readEditComboBox.getSelectionModel().getSelectedItem();
        
        List<ColumnData> primaryColumns = staticTableInfoData.getPrimaryKeyColumns(tableName);
        if (primaryColumns != null && !primaryColumns.isEmpty()) {
            
            selectSQL = "SELECT count(*) FROM " + tableName + " WHERE ";
            for (int i = 0; i < primaryColumns.size(); i++) {
                ColumnData pk = primaryColumns.get(i);
                Object v = row.getData().get(staticTableInfoData.getColumnIndex(pk.getName()));
                String value = v != null ? v.toString() : null;
                
                if (value != null && !"(NULL)".equals(value)) {
                    value = "= " + (value.startsWith("x'") && value.endsWith("'") ? value : "'" + StringEscapeUtils.escapeSql(value) + "'");
                } else {
                    value = " IS NULL";
                }
                selectSQL += "`" + pk.getName() + "` " + value + (i < primaryColumns.size() - 1 ? " AND " : "");
            }
        }
        
        return selectSQL;
    }

    
    
    public String getInsertSQL(RowData row) {
        
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData == null) {
            return "";
        }
        
        String columnsSQL = "";
        String valuesSQL = "";
        
        for (int i = 0; i < staticTableInfoData.getColumns().size(); i++) {
            ColumnData column = staticTableInfoData.getColumns().get(i);
            Object v = row.getData().get(i);
            String value = v != null ? v.toString() : null;
            
            // if primary key + auto increment + value not chaned
            if (column.isPrimaryKey() && column.isAutoincrement() && "(AUTO)".equals(value)) {
                // nothing to do
                
            // if column has default value and it not changed
//            } else if (value != null && value.equals(column.getDefaultValue())) {
//                // nothing to do
//            }
            } else {
                // in other case we must add column to insert SQL
                if (!columnsSQL.isEmpty()) {
                    columnsSQL += ", ";
                }
                columnsSQL += "`" + column.getName() + "`";
                
                if (!valuesSQL.isEmpty()) {
                    valuesSQL += ", ";
                }
                if (value == null || "(NULL)".equals(value)) {
                    valuesSQL += "NULL";
                } else if (!value.equalsIgnoreCase("CURRENT_TIMESTAMP")){
                    valuesSQL += value.startsWith("x'") && value.endsWith("'") ? value : "'" + StringEscapeUtils.escapeSql(value) + "'";
                } else {
                    valuesSQL += value;
                }
            }
        }

        String insertSQL = "INSERT INTO " + readEditComboBox.getSelectionModel().getSelectedItem() + " (" + columnsSQL + ") VALUES(" + valuesSQL + ");";
        log.debug("insertSQL:{}", insertSQL);
        return insertSQL;
    }

    
    // </editor-fold>
 
    
    @FXML
    void refreshAction(ActionEvent event) {
        
        if (busy.get()) {
            if (statement != null) {
                try {
                    statement.cancel();
                } catch (SQLException ex) {
                    log.error("Error", ex);
                }
            }
            
        } else {
            resetData();
            reinit(selectedTable); 
        }
    }
    
    @FXML
    void filterAction(ActionEvent event) {
        
        filterTextField.clear();
        
        if (!dataFiltered.get()) {
            if (!showFilter()) {
                return;
            }
        } else {
            filterData = null;
        }
        
        doFilter();
    }
    
    private void doFilter() {
        filterData();
        
        ImageView iv = new ImageView(dataFiltered.get() ? SmartImageProvider.getInstance().getFilterCancel_image() : SmartImageProvider.getInstance().getFilter_image());
        iv.setFitHeight(20);
        iv.setFitWidth(24);        
        iv.setPickOnBounds(true);
        iv.setPreserveRatio(true);
        
        filterButton.setGraphic(iv);
    }
    
    private boolean showFilter() {
        FilterDialogController filterController = ((MainController)getController()).getTypedBaseController("FilterDialog");
            
        List<TablePosition> selectedCells = tableview.getSelectionModel().getSelectedCells();
        TableColumn selectedColumn = selectedCells != null && !selectedCells.isEmpty() ? selectedCells.get(0).getTableColumn() : null;

        int selectedIndex = selectedCells != null && !selectedCells.isEmpty() ? selectedCells.get(0).getColumn() : -1;
        RowData row = tableview.getSelectionModel().getSelectedItem();

        Object v = row.getData().get(selectedIndex - 1);
        String value = v != null ? v.toString() : null;
        TableInfoData staticTableInfoData = tableInfoData;
        if (selectedTable != null) {
            if (staticTableInfoData != null) {
                filterController.init(
                    staticTableInfoData.getColumnsData(), 
                    filterData,
                    selectedColumn != null ? selectedColumn.getText() : null, 
                    row != null && selectedIndex >= 1 ? value : null
                );
            }
        } else {
            if (staticTableInfoData != null) {
                filterController.init(
                    staticTableInfoData.getColumnsData(), 
                    selectedColumn != null ? selectedColumn.getText() : null, 
                    row != null && selectedIndex >= 1 ? value : null
                );
            }
        }

        Scene scene = new Scene(filterController.getUI());        
        Stage stage = new Stage();
        stage.setTitle("Custom Filter");
        stage.initStyle(StageStyle.UTILITY);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(getStage());
        stage.setScene(scene);
        stage.sizeToScene();

        Bounds b = filterButton.localToScene(filterButton.getBoundsInLocal());
        stage.setX(b.getMinX());
        stage.setY(b.getMinY());

        stage.showAndWait();

        if (filterController.getResult() != null) {
            filterData = filterController.getResult();
            return true;
        } else {
            return false;
        }
    }
    
    private void clearFilter() {
        filterData = null;
        
        ImageView iv = new ImageView(SmartImageProvider.getInstance().getFilter_image());
        iv.setFitHeight(20);
        iv.setFitWidth(24);        
        iv.setPickOnBounds(true);
        iv.setPreserveRatio(true);
        
        filterButton.setGraphic(iv);
        dataFiltered.set(false);
    }

   /**
    * Map of a filtered item to the previous location index
    */
   private Map<RowData, Integer> filteredOut = new LinkedHashMap<>();

    private void restoreFilteredOut(){
        for(Map.Entry<RowData, Integer> element : filteredOut.entrySet()){
            data.add(element.getValue(), element.getKey());
        }
        filteredOut.clear();
    }

    private void filterData() {
        restoreFilteredOut();

        TableInfoData staticTableInfoData = tableInfoData;
        if (filterData != null && !filterData.isEmpty() && staticTableInfoData != null) {
            dataFiltered.set(true);
            
            // TODO Maybe need change this
            Collator stringCollator = Collator.getInstance();

            int idx = -1;
            for (RowData row: data) {
                idx++;

                if (row.isNewRow()){
                    continue;
                }

                boolean canAdd = true;
                
                for (FilterData fd: filterData) {
                    int columnIndex = staticTableInfoData.getColumnsData().indexOf(fd.getColumn());
                    if (columnIndex >= 0) {

                        Object v = row.getData().get(columnIndex);
                        String rowValue = v != null ? v.toString() : null;
                        if (rowValue == null) {
                            rowValue = "(NULL)";
                        }

                        ColumnData cd = staticTableInfoData.getColumnByIndex(columnIndex);
                        int compareValue = -1;
                        if (cd.isNumber()) {
                            try {
                                if (rowValue.equals(fd.getValue())) {
                                    compareValue = 0;
                                } else {
                                    compareValue = new BigDecimal((String)rowValue).compareTo(new BigDecimal(fd.getValue()));
                                }
                            } catch (NumberFormatException ex) {                            
                            }
                        } else {
                            compareValue = stringCollator.compare(rowValue, fd.getValue());
                        }


                        switch (fd.getCondition()) {
                            case "=": 
                                if (compareValue != 0) {
                                    canAdd = false;
                                }
                                break;

                            case "<>": 
                                if (compareValue == 0) {
                                    canAdd = false;
                                }
                                break;

                            case ">": 
                                if (compareValue <= 0) {
                                    canAdd = false;
                                }
                                break;

                            case "<": 
                                if (compareValue >= 0) {
                                    canAdd = false;
                                }
                                break;

                            case "LIKE": 
                                String filterValue = fd.getValue();
                                        
                                if (filterValue.startsWith("%") && filterValue.endsWith("%")) {
                                    if (!rowValue.toString().contains(filterValue.substring(1, filterValue.length() - 1))) {
                                        canAdd = false;
                                    }
                                } else if (filterValue.startsWith("%")){
                                    if (!rowValue.toString().endsWith(filterValue.substring(1, filterValue.length()))) {
                                        canAdd = false;
                                    }
                                } else if (filterValue.endsWith("%")){
                                    if (!rowValue.toString().startsWith(filterValue.substring(0, filterValue.length() - 1))) {
                                        canAdd = false;
                                    }
                                } else {
                                    if (!rowValue.toString().equals(filterValue)) {
                                        canAdd = false;
                                    }
                                }
                                break;
                        }
                    }
                    
                    if (!canAdd) {
                        break;
                    }
                }
                
                if (!canAdd) {
                    filteredOut.put(row, idx);
                }
            }

            data.removeAll(filteredOut.keySet());
        } else {
            dataFiltered.set(false);
        }
        

        if (tableview != null && tableview.getItems() != null && !tableview.getItems().isEmpty() && firstColumn != null) {
            tableview.getSelectionModel().select(0, firstColumn);
        }
    }

    private Font textFont = new Text().getFont();
    private java.awt.Font f = new java.awt.Font(textFont.getFamily(), java.awt.Font.PLAIN, 8);
    private FontMetrics fm = new javax.swing.JPanel().getFontMetrics(f);//use FontMetrics for very efficient width calculation


    private int stringWidth(int length){
//        if (length >= widths.length){
//            int[] newWidths = new int[length + 1];
//            System.arraycopy(widths, 0, newWidths, 0, widths.length);
//            widths = newWidths;
//        }
//
//        int width = widths[length];
//
//        if (width == 0){
            StringBuilder b = new StringBuilder(length);
            for(int i = 0; i <= length; i++){
                b.append('S');//middle-sized letter in pixel width
            }
            int width = stringWidth(b.toString());
//            widths[length] = width;
//        }
        
        return width + 8;
    }

    private int stringWidth(String s){
        return fm.stringWidth(s);
    }

    /*****
     * Background implementors
     * 
     */
    
    //SynchorniseWorkProccessor bj;
    
    /** i don't even need you as my workflows are organised 
     * but you are here because of uncertainties. :)
     */
    private final Object RSlocker = new Object();
    Font headerFont = Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 12);
    Text tt = new Text();
    
    
    private static final String TYPE__BLOB          = "BLOB";
    private static final String TYPE__BINARY        = "BINARY";
    private static final String TYPE__LONGBLOB      = "LONGBLOB";
    private static final String TYPE__LONGTEXT      = "LONGTEXT";
    private static final String TYPE__MEDIUMBLOB    = "MEDIUMBLOB";
    private static final String TYPE__MEDIUMTEXT    = "MEDIUMTEXT";
    private static final String TYPE__TEXT          = "TEXT";
    private static final String TYPE__TINYBLOB      = "TINYBLOB";
    private static final String TYPE__VARBINARY     = "VARBINARY";
    private static final String TYPE__TINYTEXT      = "TINYTEXT";
    private static final String TYPE__ENUM          = "ENUM";
    private static final String TYPE__SET           = "SET";
    private static final String TYPE__DATE          = "DATE";
    private static final String TYPE__DATETIME      = "DATETIME";
    private static final String TYPE__TIMESTAMP     = "TIMESTAMP";
    private static final String TYPE__TIME          = "TIME";
    private static final String TYPE__BIT           = "BIT";
    private static final String TYPE__BOOL          = "BOOL";
    private static final String TYPE__BOOLEAN       = "BOOLEAN";
    private static final String TYPE__BIGINT        = "BIGINT";
    private static final String TYPE__DOUBLE        = "DOUBLE";
    private static final String TYPE__DECIMAL       = "DECIMAL";
    private static final String TYPE__FLOAT         = "FLOAT";
    private static final String TYPE__INT           = "INT";
    private static final String TYPE__MEDIUMINT     = "MEDIUMINT";
    private static final String TYPE__NUMERIC       = "NUMERIC";
    private static final String TYPE__REAL          = "REAL";
    private static final String TYPE__SMALLINT      = "SMALLINT";
    private static final String TYPE__TINYINT       = "TINYINT";
    private static final String TYPE__YEAR          = "YEAR";
    private static final String TYPE__CHAR          = "CHAR";
    private static final String TYPE__VARCHAR       = "VARCHAR";
    
        
    final WorkProc myWorkProc = new WorkProc() {
        
        Callback<TableColumn<RowData, String>, TableCell<RowData, String>> cellFactory;
        Callback<TableColumn<RowData, String>, TableCell<RowData, String>> blobCellFactory;
        Callback<TableColumn<RowData, String>, TableCell<RowData, String>> enumCellFactory;
        Callback<TableColumn<RowData, String>, TableCell<RowData, String>> setCellFactory;
        Callback<TableColumn<RowData, String>, TableCell<RowData, String>> dateCellFactory;
        
        Callback<TableColumn<RowData, Integer>, TableCell<RowData, Integer>> integerCellFactory;
        Callback<TableColumn<RowData, BigInteger>, TableCell<RowData, BigInteger>> bigIntCellFactory;
        Callback<TableColumn<RowData, Double>, TableCell<RowData, Double>> doubleCellFactory;
        
        String database = "";
        @Override
        public void updateUI() {
                                
            try {     
                TableInfoData staticTableInfoData = tableInfoData;
                if (staticTableInfoData != null) {
                    totalRows.set(sourceData.size() + " row(s)");
                    contentHolder.getChildren().remove(notice);

                    formViewPopulated = false;                                
                    //tableview.requestFocus();                
                    forFormPos = 0;//we reset
//                    textUI.getEngine().load(InformationModule.class.getResource("tableData.html").toExternalForm());

                    String table = staticTableInfoData.getTables().isEmpty() ? "" : staticTableInfoData.getTables().get(0);
                    tableNameLabel.setText(table);                
                    databaseNameLabel.setText(database);
                    limitRowsTextField.end();

                    reCalcTableViewColumns();
                    changeScreen(tableviewContainer);   
                    
                    requestFocus();
                }
            } finally {
                busy.set(false);
            }
        }

        @Override
        public void run() {       
            
            synchronized (myWorkProc) {
                callUpdate = false;

                tt.setFont(currentFont != null ? currentFont : headerFont);
                tt.setText("X");
                CHARACTER_WIDTH = (int) tt.getLayoutBounds().getWidth();            

                try {
                    initContextMenus();
                    
                    synchronized (RSlocker) {
                        if (rs != null && !rs.isClosed()) {
                            rs.beforeFirst();
                        } else {
                            return;
                        }

                        informationOfDatabase(session.getConnectionParam(), rs);
                    }
                    
                    TableInfoData staticTableInfoData = tableInfoData;
//                    log.error("!!!!!!!!!!!!!!!!!!!!!! staticTableInfoData = " + staticTableInfoData);

                    if (data == null) {
                        data = new ObservableListWithLastNonDataItem<>(new ArrayList<>());//FXCollections.observableArrayList();
                        sourceData = new LinkedHashMap<>(300000);
                    }

                    changesMap.clear();
                    newRowsSet.clear();

                    cellFactory = new Callback<TableColumn<RowData, String>, TableCell<RowData, String>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            EditingCell<RowData, String> ec = new EditingCell<RowData, String>(null, getController().getSelectedConnectionSession().get(), 50) {
                                @Override
                                public void updateItem(String item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                                
                            };
                            if (p instanceof AutoSizeTypedTableColumn && ((AutoSizeTypedTableColumn) p).isNumber()) {
                                ec.setAlignment(Pos.CENTER_RIGHT);
                                ec.setTextAlignment(TextAlignment.RIGHT);
                            } else {
                                ec.setAlignment(Pos.CENTER_LEFT);
                            }
                            addContextMenu(ec);
                            return ec;
                        }
                    };
                    
                    integerCellFactory = new Callback<TableColumn<RowData, Integer>, TableCell<RowData, Integer>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            EditingCell<RowData, Integer> ec = new EditingCell<RowData, Integer>(null, ((MainController)getController()).getSelectedConnectionSession().get(), 50) {
                                @Override
                                protected Integer convertFromString(String item) {
                                    try {
                                        return Integer.valueOf(item);
                                    } catch (Throwable th) {
                                        return null;
                                    }
                                }   
                                
                                @Override
                                public void updateItem(Integer item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                            if (p instanceof AutoSizeTypedTableColumn && ((AutoSizeTypedTableColumn) p).isNumber()) {
                                ec.setAlignment(Pos.CENTER_RIGHT);
                                ec.setTextAlignment(TextAlignment.RIGHT);
                            } else {
                                ec.setAlignment(Pos.CENTER_LEFT);
                            }
                            addContextMenu(ec);
                            return ec;
                        }
                    };
                    
                    bigIntCellFactory = new Callback<TableColumn<RowData, BigInteger>, TableCell<RowData, BigInteger>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            EditingCell<RowData, BigInteger> ec = new EditingCell<RowData, BigInteger>( null, ((MainController)getController()).getSelectedConnectionSession().get(), 50) {
                                @Override
                                protected BigInteger convertFromString(String item) {
                                    try {
                                        return new BigInteger(item);
                                    } catch (Throwable th) {
                                        return null;
                                    }
                                }           
                                
                                @Override
                                public void updateItem(BigInteger item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                            if (p instanceof AutoSizeTypedTableColumn && ((AutoSizeTypedTableColumn) p).isNumber()) {
                                ec.setAlignment(Pos.CENTER_RIGHT);
                                ec.setTextAlignment(TextAlignment.RIGHT);
                            } else {
                                ec.setAlignment(Pos.CENTER_LEFT);
                            }
                            addContextMenu(ec);
                            return ec;
                        }
                    };
                    
                    doubleCellFactory = new Callback<TableColumn<RowData, Double>, TableCell<RowData, Double>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            EditingCell<RowData, Double> ec = new EditingCell<RowData, Double>(null, ((MainController)getController()).getSelectedConnectionSession().get(), 50) {
                                @Override
                                protected Double convertFromString(String item) {
                                    try {
                                        return Double.valueOf(item);
                                    } catch (Throwable th) {
                                        return null;
                                    }
                                }             
                                
                                @Override
                                public void updateItem(Double item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                            if (p instanceof AutoSizeTypedTableColumn && ((AutoSizeTypedTableColumn) p).isNumber()) {
                                ec.setAlignment(Pos.CENTER_RIGHT);
                                ec.setTextAlignment(TextAlignment.RIGHT);
                            } else {
                                ec.setAlignment(Pos.CENTER_LEFT);
                            }
                            addContextMenu(ec);
                            return ec;
                        }
                    };

                    dateCellFactory = new Callback<TableColumn<RowData, String>, TableCell<RowData, String>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            EditingCell ec = new EditingCell(null, ((MainController)getController()).getSelectedConnectionSession().get()) {
                                @Override
                                public void updateItem(Object item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                                
                                @Override
                                public void startEdit() {
                                    super.startEdit();

                                    String strDate = getItem() != null ? getItem().toString() : null;
                                    LocalDateTime d = LocalDateTime.now();
                                    try {
                                        d = LocalDateTime.parse(strDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                    } catch (Throwable th) {
                                    }

                                    DateTimePicker dtp = new DateTimePicker();
                                    dtp.setDateTimeValue(d);
                                    DateTimePickerSkin datePickerSkin = new DateTimePickerSkin(dtp);
                                    Node popupContent = datePickerSkin.getPopupContent();

                                    Popup p = new Popup();
                                    p.setAutoHide(true);
                                    p.getContent().add(popupContent);

                                    dtp.setOnAccept((LocalDateTime localDate) -> {

                                        int row = getTableRow().getIndex();
                                        RowData rd = data.get(row);
                                        if (rd.isDataRow()) {

                                            String value = timeStampFormat.format(Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant()));
                                            boolean changed = !value.equals(rd.getData().get(tableview.getColumns().indexOf(getTableColumn()) - 1));

                                            if (!rd.isNewRow() && changed) {
                                                changesMap.put(rd, sourceData.get(rd));
                                            }

                                            if (changed) {
                                                setItem(value);
                                            }

                                            commitEdit(value);
                                            p.hide();
                                        }
                                    });

                                    dtp.setOnCancel((LocalDateTime localDate) -> {
                                        cancelEdit();
                                        p.hide();
                                    });

                                    p.setX(localToScene(getBoundsInLocal()).getMaxX());
                                    p.setY(localToScene(getBoundsInLocal()).getMinY());
                                    p.show(getStage());
                                }

                            };
                            if (p instanceof AutoSizeTypedTableColumn && ((AutoSizeTypedTableColumn) p).isNumber()) {
                                ec.setAlignment(Pos.CENTER_RIGHT);
                                ec.setTextAlignment(TextAlignment.RIGHT);
                            } else {
                                ec.setAlignment(Pos.CENTER_LEFT);
                            }
                            addContextMenu(ec);
                            return ec;
                        }
                    };

                    blobCellFactory = new Callback<TableColumn<RowData, String>, TableCell<RowData, String>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            if (staticTableInfoData == null) {
                                return null;
                            }
                            ColumnData cd = staticTableInfoData.getColumnByIndex(tableview.getColumns().indexOf(p) - 1);
                            QueryTableCell cell = new QueryTableCell<RowData, String>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showEditBlobData(data, text, Bindings.equal(readEditComboBox.valueProperty(), cd.getDisplayTable()))) {
                                
                                @Override
                                public void updateItem(String item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                            addContextMenu(cell);
                            return cell;
                        }
                    };

                    enumCellFactory = new Callback<TableColumn<RowData, String>, TableCell<RowData, String>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            if (staticTableInfoData == null) {
                                return null;
                            }

                            ColumnData cd = staticTableInfoData.getColumnByIndex(tableview.getColumns().indexOf(p) - 1);
                            ObservableList l = FXCollections.observableArrayList(cd.getDataType().getList());
                            if (cd.isNullable()) {
                                l.add(0, NULL);
                            }

                            EnumTableCell cell = new EnumTableCell<RowData, String>(l, (EnumTableCell.TableCellData cellData, String value) -> {

                                if (value == null) {
                                    value = NULL;
                                }

                                RowData rd = data.get(cellData.getRow());
                                if (rd.isDataRow()) {

                                    boolean changed = !value.equals(rd.getData().get(cellData.getColumn() - 1));

                                    if (!rd.isNewRow() && changed) {
                                        changesMap.put(rd, sourceData.get(rd));
                                    }

                                    if (changed) {
                                        rd.getData().set(cellData.getColumn() - 1, value);
                                    }
                                }
                            }, Bindings.equal(readEditComboBox.valueProperty(), cd.getDisplayTable())) {
                                @Override
                                public void updateItem(String item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                            addContextMenu(cell);
                            return cell;
                        }
                    };

                    setCellFactory = new Callback<TableColumn<RowData, String>, TableCell<RowData, String>>() {
                        @Override
                        public TableCell call(TableColumn p) {
                            if (staticTableInfoData == null) {
                                return null;
                            }

                            ColumnData cd = staticTableInfoData.getColumnByIndex(tableview.getColumns().indexOf(p) - 1);
                            ObservableList l = FXCollections.observableArrayList(cd.getDataType().getList());

                            if (cd.isNullable()) {
                                l.add(0, NULL);
                            }

                            SetTableCell cell = new SetTableCell<RowData>(l, (SetTableCell.TableCellData cellData, String value) -> {

                                if (value == null) {
                                    value = NULL;
                                }

                                RowData rd = data.get(cellData.getRow());
                                if (rd.isDataRow()) {

                                    boolean changed = !value.equals(rd.getData().get(cellData.getColumn() - 1));

                                    if (!rd.isNewRow() && changed) {
                                        changesMap.put(rd, sourceData.get(rd));
                                    }

                                    if (changed) {
                                        rd.getData().set(cellData.getColumn() - 1, value);
                                    }
                                }
                            }, Bindings.equal(readEditComboBox.valueProperty(), cd.getDisplayTable())) {
                                @Override
                                public void updateItem(String item, boolean empty) {
                                    super.updateItem(item, empty);
                                    
                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                            
                            addContextMenu(cell);
                            return cell;
                        }
                    };

                    if (checkBoxCol == null) {
                        checkBoxCol = new TableColumn<>();
                        checkBoxCol.setPrefWidth(40);
                        checkBoxCol.setMinWidth(40);
                        checkBoxCol.setMaxWidth(40);
                        checkBoxCol.setId("checkbox");

                        selectAllCheckBox = new CheckBox();
                        checkBoxCol.setGraphic(selectAllCheckBox);
                        selectAllCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
                            if (new_val.booleanValue() != old_val.booleanValue()) {
                                log.debug("selection of selectallcheckbox:" + new_val);
                                selection(new_val);
                            }
                        });
                        checkBoxCol.setCellValueFactory(param -> param.getValue().selected());
                        checkBoxCol.setCellFactory(list -> {
                            return new CheckBoxTableCell<RowData, Boolean>(null) {
                                @Override
                                public void updateItem(Boolean item, boolean empty) {

                                    if (!empty && getTableRow().getIndex() == data.size() - 1 && !READ_ONLY.equals(readEditComboBox.getSelectionModel().getSelectedItem())) {
                                        setGraphic(new Label("*"));
                                    } else {
                                        super.updateItem(item, empty);
                                    }

                                    if (currentFont != null) {
                                        setFont(currentFont);
                                    }
                                }
                            };
                        });
                        checkBoxCol.setEditable(true);
                        checkBoxCol.setSortable(false);
                    }

                    Platform.runLater(() -> {
                        if (!tableview.getColumns().contains(checkBoxCol)) {
                            tableview.getColumns().add(checkBoxCol);
                        }
                    });

                    int columnCount = -1;
                    synchronized (RSlocker) {
                        if (rs == null || rs.isClosed()) {
                            return;
                        }
                        ResultSetMetaData metaData = rs.getMetaData();
                        columnCount = metaData.getColumnCount();

                        int[] columnTypes = new int[columnCount];

//                        String[] defaults = new String[columnCount];                    
                        for (int i = 0; i < columnCount; i++) {
//                            ColumnData cd = tableInfoData.getColumnByIndex(i);
                            columnTypes[i] = metaData.getColumnType(i + 1); // as 1-based
                        }

                        CHARACTER_WIDTH__10  = 10 * CHARACTER_WIDTH;
                        CHARACTER_WIDTH__20  = 20 * CHARACTER_WIDTH;
                        CHARACTER_WIDTH__30  = 30 * CHARACTER_WIDTH;
                        CHARACTER_WIDTH__100  = 100 * CHARACTER_WIDTH;

                        while (rs.next()) {
                            //Iterate Row

                            List<Object> rowData = new ArrayList<>(columnCount);

                            for (int i = 1; i <= columnCount; i++) {
                                int type = columnTypes[i - 1]; // as 0-based

                                Object value;
                                switch (type) {
                                    case Types.BLOB:
                                    case Types.BINARY:
                                    case Types.LONGVARBINARY: {
                                        value = rs.getString(i);
                                        if (checkIfBinaryData((String) value)) {
                                            byte[] bytes = rs.getBytes(i);
                                            value = bytes != null ? "x'" + Hex.encodeHexString(bytes) + "'" : null;
                                        }
                                        break;
                                    }

                                    case Types.DATE:
                                        int precision = metaData.getPrecision(i);
                                        if (precision == 2 || precision == 4) {
                                            value = "" + rs.getShort(i);
                                        } else {
                                            Date d0 = rs.getDate(i);
                                            value = d0 != null ? dateFormat.format(d0) : null;
                                        }
                                        break;

                                    case Types.TIME:
                                    case Types.TIME_WITH_TIMEZONE:

                                        Time d1 = rs.getTime(i);

                                        if (d1 != null) {
                                            if (timeCache == null) {
                                                timeCache = Collections.synchronizedMap(new MaxSizeHashMap(4000));
                                            }

                                            value = timeCache.get(d1);

                                            if (value == null) {
                                                value = timeFormat.format(d1);
                                                timeCache.put(d1, (String) value);
                                            }
                                        } else {
                                            value = null;
                                        }

                                        break;

                                    case Types.TIMESTAMP:
                                    case Types.TIMESTAMP_WITH_TIMEZONE:

                                        Timestamp d2 = rs.getTimestamp(i);
                                        if (d2 != null) {

                                            if (timestampCache == null) {
                                                timestampCache = Collections.synchronizedMap(new MaxSizeHashMap(4000));
                                            }

                                            value = timestampCache.get(d2);

                                            if (value == null) {
                                                value = timeStampFormat.format(d2);
                                                timestampCache.put(d2, (String) value);
                                            }
                                        } else {
                                            value = null;
                                        }

                                        break;

                                    case Types.VARBINARY:
                                        try {
                                            Long v = rs.getObject(i) != null ? rs.getLong(i) : null;
                                            value = v != null ? "b'" + Long.toBinaryString(v) + "'" : null;
                                        } catch (Throwable th) {
                                            value = rs.getString(i);
                                        }
                                        break;
                                        
                                    case Types.BIGINT:
                                        value = rs.getBigDecimal(i);
                                        if (value != null) {
                                            value = ((BigDecimal)value).toBigInteger();
                                        }
                                        break;
                                        
                                    case Types.DECIMAL:
                                    case Types.NUMERIC:
                                    case Types.FLOAT:
                                    case Types.DOUBLE:
                                    case Types.REAL:
                                        value = rs.getDouble(i);
                                        break;
                                   
                                    case Types.INTEGER:
                                    case Types.SMALLINT:
                                    case Types.TINYINT:                                    
                                        value = rs.getInt(i);
                                        break;
                                        
                                    default:
                                        value = rs.getString(i);
                                        break;
//                                        if (str != null) {
//                                            str = ((String) str).intern();
//                                        }
                                }

                                // if null need check for defualt value
//                                if (str == null) {
//                                    str = defaults[i - 1];                                
//                                }

                                rowData.add(value);
                            }

                            RowData row = new RowData(rowData);
                            RowData sourceRow = new RowData(new ArrayList<>(rowData));

                            sourceData.put(row, sourceRow);
                        }                    

                        Set<RowData> c = sourceData.keySet();
                        if (data != null) {
                            data.addAll(c);
                        }

                        // Need check this can make low performance
                        /// but it must be called after data filed
                        final int additional = stringWidth("00");
                        columns = new ArrayList<>();
                        for (int i = 0; i < columnCount; i++) {
                            initColumns(i, additional);
                        }

                        if (rs != null && !rs.isClosed()) {
                            rs.close();
                        }
                        statement = null;
                    }

                    filterData();

                    Platform.runLater(() -> {
                        readEditComboBox.getSelectionModel().selectedItemProperty().removeListener(readEditListener);

                        readEditComboBox.setVisible(multiMode);
                        readEditComboBox.getItems().clear();
                        if (staticTableInfoData != null) {
                            readEditComboBox.getItems().add(READ_ONLY);
                            if (!readOnly) {
                                readEditComboBox.getItems().addAll(staticTableInfoData.getTables());
                            }
                        }

                        SimpleStringProperty selectedConnParam = new SimpleStringProperty();
                        selectedConnParam.set("" + offsetLimit);
                        limitRowsTextField.setUserData(selectedConnParam);
                        limitRowsTextField.textProperty().bindBidirectional(selectedConnParam);                    
                    });

                    if (limitRowsTextField.getOnKeyReleased() == null) {
                        limitRowsTextField.setOnKeyReleased(allHandlers);
                    }

                    Platform.runLater(() -> {
                        if (staticTableInfoData != null) {
//                            log.error("BEFORE: " + tableview.getColumns().size());
                            tableview.getColumns().clear();
                            if (checkBoxCol != null) {
                                tableview.getColumns().add(checkBoxCol);
                            }
                            tableview.refresh();
//                            log.error("ON: " + tableview.getColumns().size());
                            tableview.getColumns().addAll(columns);
//                            log.error("AFTER: " + tableview.getColumns().size());

                            tableview.setItems(data);

                            readEditComboBox.getSelectionModel().selectedItemProperty().addListener(readEditListener);

                            if (!staticTableInfoData.getTables().isEmpty()) {
                                readEditComboBox.getSelectionModel().select(editMode ? staticTableInfoData.getTables().get(0) : READ_ONLY);
                            } else {
                                readEditComboBox.getSelectionModel().select(READ_ONLY);
                            }      

                            tableview.setEditable(editMode);
                            tableview.getSelectionModel().setCellSelectionEnabled(true);
                            tableview.requestLayout();
                            copyCellItem.disableProperty().bind(Bindings.isNull(tableview.getSelectionModel().selectedItemProperty()));
                            copyAllRowsItem.disableProperty().bind(Bindings.isEmpty(tableview.getItems()));
                            copySelectedRowsItem.disableProperty().bind(Bindings.isEmpty(tableview.getItems()));

                            tableview.getSelectionModel().clearSelection();

                            if (!tableview.getItems().isEmpty() && firstColumn != null) {
                                try {
                                    tableview.getSelectionModel().select(0, firstColumn);
                                } catch (Throwable th) {
                                    // at now do nothing, when we recreate many times this selection
                                    // can throw error index out of bounds, but no need do any actions
                                }
                            } 

                            PreferenceData pd = preferenceService.getPreference();
                            if (!pd.isKeepFocusOnEditor()) {
                                Platform.runLater(() -> {
                                    requestFocus();
                                });
                            }
                        }
                    });

                    callUpdate = true;
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", "Error result", ex);
                    });
                }
            }
        }
        
        
        private RowData editedRowData;
        private int editedColumn;
        
        private void initColumns(int i, int additional) throws SQLException {
            TableInfoData staticTableInfoData = tableInfoData;
            if (staticTableInfoData == null) {
                return;
            }
            ColumnData cd = staticTableInfoData.getColumnByIndex(i);
                        
            int columnIndex = i + 1;

            tt.setText(cd.getAlias());                        
            double minWidth = tt.getLayoutBounds().getWidth() + CHARACTER_WIDTH * 2;

            String columnType = StringUtils.EMPTY;
            
            DataType dt = cd.getDataType();
            if (dt != null) {
                columnType = dt.getName();
            }

            boolean blobColumn;
            if (TYPE__BLOB.equalsIgnoreCase(columnType) ||
                TYPE__BINARY.equalsIgnoreCase(columnType) ||
                TYPE__LONGBLOB.equalsIgnoreCase(columnType) ||
                TYPE__LONGTEXT.equalsIgnoreCase(columnType) ||
                TYPE__MEDIUMBLOB.equalsIgnoreCase(columnType) ||
                TYPE__MEDIUMTEXT.equalsIgnoreCase(columnType) ||
                TYPE__TEXT.equalsIgnoreCase(columnType) ||
                TYPE__TINYBLOB.equalsIgnoreCase(columnType) ||
                TYPE__VARBINARY.equalsIgnoreCase(columnType) ||
                TYPE__TINYTEXT.equalsIgnoreCase(columnType)) {

                blobColumn = true;
            } else {
                blobColumn = false;
            }

            boolean enumColumn = TYPE__ENUM.equalsIgnoreCase(columnType) || cd.isEnumType();
            boolean setColumn = TYPE__SET.equalsIgnoreCase(columnType) || cd.isSetType();

            boolean dateColumn = TYPE__DATE.equalsIgnoreCase(columnType) || TYPE__DATETIME.equalsIgnoreCase(columnType) || TYPE__TIMESTAMP.equalsIgnoreCase(columnType) || TYPE__TIME.equalsIgnoreCase(columnType);


            AutoSizeTypedTableColumn<RowData, ?> col = null;
            
            switch (columnType) {                    
                    
                case TYPE__BIGINT:
                    col = new ResultTableColumn<BigInteger>(cd.getAlias(), columnType, additional, blobColumn, columnIndex, minWidth);
                    ((ResultTableColumn<BigInteger>)col).setCellValueFactory((CellDataFeatures<RowData, BigInteger> param) -> {
                        BigInteger value = null;
                        if (param.getValue().getData().size() > 0 && param.getValue().getData().size() > i) {
                            try {
                                if (param.getValue().getData().get(i) == null) {
                                    value = new BigInteger(cd.getDefaultValue());
                                } else {
                                    value = (BigInteger) param.getValue().getData().get(i);
                                }
                            } catch (Throwable th) {
                            }
                        }

                        return new SimpleObjectProperty<BigInteger>(value);
                    });
                    ((ResultTableColumn<BigInteger>)col).setCellFactory(bigIntCellFactory);
                    break;
                    
                case TYPE__DOUBLE:
                case TYPE__FLOAT:
                case TYPE__DECIMAL:
                case TYPE__NUMERIC:
                case TYPE__REAL
                        :
                    col = new ResultTableColumn<Double>(cd.getAlias(), columnType, additional, blobColumn, columnIndex, minWidth);
                    ((ResultTableColumn<Double>)col).setCellValueFactory((CellDataFeatures<RowData, Double> param) -> {
                        Double value = null;
                        if (param.getValue().getData().size() > 0 && param.getValue().getData().size() > i) {
                            try {
                                if (param.getValue().getData().get(i) == null) {
                                    value = Double.valueOf(cd.getDefaultValue());
                                } else {
                                    value = (Double) param.getValue().getData().get(i);
                                }
                            } catch (Throwable th) {
                            }
                        }

                        return new SimpleObjectProperty<Double>(value);
                    });
                    ((ResultTableColumn<Double>)col).setCellFactory(doubleCellFactory);
                    break;
                
                case TYPE__INT:
                case TYPE__SMALLINT:
                case TYPE__TINYINT:
                    col = new ResultTableColumn<Integer>(cd.getAlias(), columnType, additional, blobColumn, columnIndex, minWidth);
                    ((ResultTableColumn<Integer>)col).setCellValueFactory((CellDataFeatures<RowData, Integer> param) -> {
                        Integer value = null;
                        if (param.getValue().getData().size() > 0 && param.getValue().getData().size() > i) {
                            try {
                                if (param.getValue().getData().get(i) == null) {
                                    value = Integer.valueOf(cd.getDefaultValue());
                                } else {
                                    value = (Integer) param.getValue().getData().get(i);
                                }
                            } catch (Throwable th) {
                            }
                        }

                        return new SimpleObjectProperty<Integer>(value);
                    });
                    ((ResultTableColumn<Integer>)col).setCellFactory(integerCellFactory);
                    break;
                    
                default:
                    col = new ResultTableColumn<String>(cd.getAlias(), columnType, additional, blobColumn, columnIndex, minWidth);
                    ((ResultTableColumn<String>)col).setCellValueFactory((CellDataFeatures<RowData, String> param) -> {
                        String value = StringUtils.EMPTY;
                        if (param.getValue().getData().size() > 0 && param.getValue().getData().size() > i) {
                            if (param.getValue().getData().get(i) == null) {
                                value = cd.getDefaultValue();
                            } else {
                                value = param.getValue().getData().get(i).toString();
                            }
                            value = value == null ? NULL : value;
                        }

                        return new SimpleStringProperty(value);
                    });
                    
                    if (blobColumn) {
                        ((ResultTableColumn<String>)col).setCellFactory(blobCellFactory);
                    } else if (enumColumn) {
                        ((ResultTableColumn<String>)col).setCellFactory(enumCellFactory);
                    } else if (setColumn) {
                        ((ResultTableColumn<String>)col).setCellFactory(setCellFactory);
                    } else if (dateColumn) {
                        ((ResultTableColumn<String>)col).setCellFactory(dateCellFactory);
                    } else {
                        ((ResultTableColumn<String>)col).setCellFactory(cellFactory);
                    }
                    
                    break;
                                    
            }
            
            col.setMinWidth(10);
            col.init(data);

            col.widthProperty().addListener((ObservableValue<? extends Number> ov, Number t, Number t1) -> {
                reCalcTableViewColumns();
            });

            col.editableProperty().bind(Bindings.equal(readEditComboBox.valueProperty(), cd.getDisplayTable()));
            
            ((ResultTableColumn<Object>)col).setOnEditStart((CellEditEvent<RowData, Object> event) -> {
                
                editedRowData = null;
                editedColumn = event.getTablePosition().getColumn();
                
                if (!readOnly) {
                    editedRowData = event.getRowValue();
                    if (!editedRowData.isDataRow()) {                        
                        
                        editedRowData.setDataRow(true);
                        newRowsSet.add(editedRowData);

                        RowData newRow = getNewRow();
                        newRow.setDataRow(false);

                        data.changeLastNonDataRow(newRow); 

                        Platform.runLater(() -> {
                            tableview.edit(data.size() - 2, event.getTableColumn());
                        });
                    }
                }
            });
            ((ResultTableColumn<Object>)col).setOnEditCommit((CellEditEvent<RowData, Object> t) -> {
                
                RowData row;
                if (t.getTablePosition() == null) {
                    row = editedRowData;
                } else {
                    row = t.getRowValue();
                }
                 
                if (!row.isNewRow()) {
                    changesMap.put(row, sourceData.get(row));
                }

                row.getData().set((t.getTablePosition() == null ? editedColumn : t.getTablePosition().getColumn()) - 1, t.getNewValue());
                
                Platform.runLater(() -> tableview.requestFocus());
            });

            columns.add(col); 
            
            if (firstColumn == null) {
                firstColumn = col;
            }
        }
    };
    
    
    private class ResultTableColumn<T> extends AutoSizeTypedTableColumn<RowData, T> {

        private int additionalLength;
        
        private int additional;
        private boolean blobColumn;
        private int columnIndex;
        private double minWidth;
        public ResultTableColumn(String name, String type, int additional, boolean blobColumn, int columnIndex, double minWidth) {
            super(name, type);
            
            this.additionalLength = additional + (blobColumn ? 40 : 0);
            this.additional = additional;
            this.blobColumn = blobColumn;
            this.columnIndex = columnIndex;
            this.minWidth = minWidth;
        }
        
        @Override
        public double calcWidth(List<RowData> rows, int maxWidth) {
            int maxLen = 0;
            int index = columnIndex - 1;

            for (RowData row : rows) {
                if (row.getData().size() > index) {
                    Object obj = row.getData().get(index);

                    int len;
                    if (obj == null) {
                        len = NULL_LENGTH;
                    } else {
                        if (obj instanceof String) {
                            String stringValue = (String) obj;

                            if (stringValue.startsWith(X_WITH_QUOTE) && stringValue.endsWith(QUOTE)) {
                                len = BINARY_IMAGE_LENGTH;
                            } else {
                                len = stringValue.length();
                            }
                        } else {
                            len = obj.toString().length();
                        }
                    }

                    if (len > maxLen) {
                        maxLen = len;

                        if (maxLen > maxWidth){
                            maxLen = maxWidth;
                            break;
                        }
                    }
                }
            }

            return stringWidth(maxLen) + additionalLength;
        }

        @Override
        public double getMinimumColumnWidth() {
            return minWidth;
        }

        @Override
        public int getMaximumWidth() {
            return CHARACTER_WIDTH__100;
        }


        @Override
        public double getMaximumWidth(String type) {
            synchronized (RSlocker) {
                if (type != null) {
                    if (TYPE__BIT.equalsIgnoreCase(type) ||
                        TYPE__BOOL.equalsIgnoreCase(type) ||
                        TYPE__BOOLEAN.equalsIgnoreCase(type)) {

                        return CHARACTER_WIDTH__10;
                    }

                    if (TYPE__BIGINT.equalsIgnoreCase(type) ||
                        TYPE__DECIMAL.equalsIgnoreCase(type) ||
                        TYPE__DOUBLE.equalsIgnoreCase(type) ||
                        TYPE__FLOAT.equalsIgnoreCase(type) ||
                        TYPE__INT.equalsIgnoreCase(type) ||
                        TYPE__MEDIUMINT.equalsIgnoreCase(type) ||
                        TYPE__NUMERIC.equalsIgnoreCase(type) ||
                        TYPE__REAL.equalsIgnoreCase(type) ||
                        TYPE__SMALLINT.equalsIgnoreCase(type) ||
                        TYPE__TINYINT.equalsIgnoreCase(type)) {

                        return CHARACTER_WIDTH__10;
                    }

                    if (TYPE__TIME.equalsIgnoreCase(type) ||
                        TYPE__TIMESTAMP.equalsIgnoreCase(type) ||
                        TYPE__YEAR.equalsIgnoreCase(type) ||
                        TYPE__DATE.equalsIgnoreCase(type) ||
                        TYPE__DATETIME.equalsIgnoreCase(type)) {

                        return CHARACTER_WIDTH__20;
                    }

                    if (TYPE__BLOB.equalsIgnoreCase(type) ||
                        TYPE__BINARY.equalsIgnoreCase(type) ||
                        TYPE__CHAR.equalsIgnoreCase(type) ||
                        TYPE__ENUM.equalsIgnoreCase(type) ||
                        TYPE__SET.equalsIgnoreCase(type) ||
                        TYPE__LONGBLOB.equalsIgnoreCase(type) ||
                        TYPE__LONGTEXT.equalsIgnoreCase(type) ||
                        TYPE__MEDIUMBLOB.equalsIgnoreCase(type) ||
                        TYPE__MEDIUMTEXT.equalsIgnoreCase(type) ||
                        TYPE__TEXT.equalsIgnoreCase(type) ||
                        TYPE__TINYBLOB.equalsIgnoreCase(type) ||
                        TYPE__VARBINARY.equalsIgnoreCase(type) ||
                        TYPE__VARCHAR.equalsIgnoreCase(type) ||
                        TYPE__TINYTEXT.equalsIgnoreCase(type)) {

                        return CHARACTER_WIDTH__50;
                    }
                }
            }

            return CHARACTER_WIDTH__100;
        }
    }
    

    public void clearData() {
        if (data != null) {
            data.clear();
        }

        if (sourceData != null) {
            sourceData.clear();
        }
    }

    private boolean checkIfBinaryData(String text) {
        int i = 0;
        while (text != null && text.length() > i && i < 20) {
            if (!p.matcher("" + text.charAt(i)).matches()) {
                return true;
            }
            i++;
        }
        
        return false;
    }
    
    private String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    log.error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
//        final String[] units = new String[] { "B", "K", "M", "G", "T" };
//        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
//        return new DecimalFormat("#0").format(size / Math.pow(1024, digitGroups)) + units[digitGroups];
    }
    
    private void showEditBlobData(QueryTableCell.QueryTableCellData cellData, String text, BooleanBinding editable) {

        RowData rd = data.get(cellData.getRow());
        if (rd.isDataRow()) {
            final Stage dialog = new Stage();

            EditBlobCellController editBlobCellController = ((MainController)getController()).getTypedBaseController("EditBlobCell");
            editBlobCellController.init(text, editable.get());

            final Parent p = editBlobCellController.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getStage());
            dialog.setTitle("Insert / Update Blob Field");

            dialog.showAndWait();
            
            String newValue = editBlobCellController.getData();
            if (newValue != null || editBlobCellController.isNullable()) {
                
                if (editBlobCellController.isNullable()) {
                    newValue = "(NULL)";
                }
                
                if (!rd.isNewRow()) {
                    changesMap.put(rd, sourceData.get(rd));
                }

                rd.getData().set(cellData.getColumn() - 1, newValue);
                TableColumn tc = tableview.getColumns().get(cellData.getColumn() - 1);
                tc.setVisible(false);
                tc.setVisible(true);
            }
        }        
    }
    
    private void recreateTextViewData() {
        
        TableInfoData staticTableInfoData = tableInfoData;
        if (staticTableInfoData == null) {
            return;
        }
        
        /////////// for text view //////////// 
        List<RowData> list = new ArrayList<>();
        RowData rd = new RowData();
        rd.getData().addAll(staticTableInfoData.getColumnsData());
        list.add(rd);
        list.addAll(data);
             
        Platform.runLater(() -> {
            updateTableInfo(list);
        });
    }
    
    private void updateTableInfo(List<RowData> tablesRows) {
        
        if (!tablesRows.isEmpty()) {
//            Document doc = textUI.getEngine().getDocument();
//
//            Element table = doc.getElementById("tableData");
//            NodeList list = table.getElementsByTagName("tr");
//            if (list != null) {
//                while (list.getLength() > 0) {
//                    table.removeChild(list.item(0));
//                }
//            }
            textUI.clear();
            
            Text widthText = new Text();
            widthText.setFont(textUI.getFont());
            
            double[] widths = new double[tablesRows.get(0).getData().size()];
            for (RowData rd: tablesRows) {
                int i = 0;
                for (Object obj: rd.getData()) {
                    widthText.setText("NULL");
                    if (obj != null) {
                        if (obj instanceof String && obj.toString().startsWith("x'")) {
                            // Binary / Image
                            widthText.setText("Binary / Image");
                        } else {
                            widthText.setText(obj.toString());
                        }
                    }
                    double width = widthText.getBoundsInLocal().getWidth();
                    if (width > widths[i]) {
                        widths[i] = width;
                    }
                    i++;
                }
            }

            addRow(tablesRows.get(0), widths, widthText, true);
            
            for (int i = 1; i < tablesRows.size(); i++) {
                addRow(tablesRows.get(i), widths, widthText, false);
            }
        }
    }
    
    private void addRow(RowData row, double[] widths, Text widthText, boolean underline) {
//        Element newRow = doc.createElement("tr");
//        
//        for (int i = 0; i < row.getData().size(); i++) {
//            Element cell = doc.createElement("td");
//            if (underline) {
//                cell.setAttribute("style", "border-bottom: 1pt solid black;");
//            }
//            
//            Object v = row.getData().get(i);
//            String value = v != null ? v.toString() : null;
//            cell.appendChild(doc.createTextNode(value));
//            
//            newRow.appendChild(cell);
//        }        
//        
//        table.appendChild(newRow);
        textUI.appendText("\n");
        for (int i = 0; i < row.getData().size(); i++) {
            Object v = row.getData().get(i);
            if (v instanceof String && v.toString().startsWith("x'")) {
                textUI.appendText(maximize("Binary / Image", widths[i], " ", widthText));
            } else {
                textUI.appendText(maximize(v != null ? v.toString() : "NULL", widths[i], " ", widthText));
            }
        }
        
        if (underline) {
            textUI.appendText("\n");
            for (int i = 0; i < row.getData().size(); i++) {
                textUI.appendText(maximize("-", widths[i], "-", widthText));
            }
        }
    }
    
    
    private String maximize(String text, double width, String str, Text widthText) {
        
        widthText.setText(text);
        double length = widthText.getBoundsInLocal().getWidth();
        
        widthText.setText(str);
        double shift = widthText.getBoundsInLocal().getWidth();
        
        while (length < width) {
            
            if (length + shift > width && width - length < length + shift - width) {
                break;
            }
                
            text += str;
            length += shift;
        }
        
        for (int i = 0; i < 10; i++) {
            text += " ";
        }
        
        return text;
    }
    
    
    /**
     * THis is my text logical algorithm to the text display. (PERFECT NOW)
     *
     * my approach to this is, to iterate over the list or overall items, and
     * get each individual row, which is a String array, then i iterate over it
     * to get the length of the longest item in each column of the row and slot
     * it into longestRow parameter, i continue with my iterations till i find a
     * String who is longer than what i already have.
     *
     * When i am done i then go into displaying the text, i do this by getting
     * the length of the text and subtract it from the longest integer in that
     * column so as to get the remaining number then i add the static space
     * integer which is 10.
     *
     *
     * @param initalText the initial text for the input
     * @param ta my textArea class //removed
     * @param strings my array of items.
     */
    
//    private static final String UNDERLINE   = "underline";
//    private static final String MINUS       = "-";
//    private static final String TAB         = "\t";
//    private static final String NEWLINE     = "\r\n";
//    private static final String SPACE       = " ";
//    private String textWritter(ArrayList<RowData> strings, String initalText) {
//        if(strings.isEmpty()){
//            return null;
//        }
//        
//        short max = (short)strings.get(0).getData().size();
//        short[] longestRow = new short[max];
//        //string array was changed to arralist
//        
//        StringBuilder sb = new StringBuilder(initalText);
//        //final byte space = 4;
//        for (int i = 0; i < strings.size(); i++) {
//            RowData row = strings.get(i);
//            String rowS = (String)row.getData().get(0);
//
//            if (UNDERLINE.equals(rowS)) {
//                continue;
//            }
//
//            for (int k = 0; k < max; k++) {
//                //max is of the same magnitude as,count, row.length and longestRow.length
//                rowS = (String)row.getData().get(k);
//                if (rowS != null && rowS.length() > longestRow[k]) {
//                    longestRow[k] = (short) rowS.length();
//                }
//            }
//        }
//
//        for (int i = 0; i < strings.size(); i++) {
//            RowData row = strings.get(i);
//            String rowS = (String)row.getData().get(0);
//
//            if (rowS != null && rowS.equals(UNDERLINE)) {
//                
//                for (int k = 0; k < longestRow.length; k++) {
//                    sb.append(StringUtils.repeat(MINUS, longestRow[k]));
//                    sb.append(TAB);
//                }
//                
//                sb.append(NEWLINE);
//                continue;
//            }
//
//            for (int k = 0; k < max; k++) {
//                rowS  = (String)row.getData().get(k);
//                if (rowS == null) {
//                    rowS = SPACE;
//                }
//                short spaceForspace = (short) ((short) (Math.abs(longestRow[k] - rowS.length())) /*+ space*/);
//
//                sb.append(rowS);
//                sb.append(StringUtils.repeat(SPACE, spaceForspace));                
//                sb.append(TAB);
//
//            }
//            
//            sb.append(NEWLINE);
//        }
//        
//        return sb.toString();
//    }
    
    //integer that determines the position in the row data currently being displayed
    volatile int forFormPos = 0; //tempted to make it short  
    
    private BooleanProperty formViewInited = new SimpleBooleanProperty(false);
    /**
     * 
     * Fills up data in the form view side of table data display     * 
     * How it works:
     * 
     * It first checks whether you are updating fields or starting a new displaying of data
     * If we are updating that is populating flag is true, we check if the start[position] is greater
     * than our backing data, after we get our item from the data and iterate through the items to display
     * them. if the item is null we leave it and continue to the next one. 
     *   IF we are not populating then we are displaying for the first time that particular tabledata
     * so we assign the where short to our start - (our start is always 0 if we are displaying for the first time
     * so is more of saying short where = 0;), then we iterate through our columnNames and display the columns 
     * in a label, we do the same for the textField and initially we display (null) - (this initial displaying of
     * null is to help keep that if an item is null in the populating function)
     * 
     * We assign id to both the textfield and label so as we can look for them and either change their text or
     * recycle them. we recycle for pace and performance.
     * 
     * since our where started with 0, and later incremented to 0 + (number of column we have) we should have that
     *  x number of pairs of node but eg if our previous table data had 10 columns and our next table data has 5 then
     * from how our formview works we will have excess nodes hence the next lines after that is responsible for 
     * removing and recycling excess nodes.
     * 
     * @param populating
     * Flag indicating whether we are just updating fileds in the form view or re-creating
     * @param start
     * Position in the row data to extract or display its items
     * @param columnNames 
     * An arraylist of columns available for the selected table
     */
    
    boolean formViewPopulated = false;
    
    private void formViewPopulator(final int start, final List<String> columnNames) {
        try {
            formViewInited.set(false);
        
            short i = 0;
            if (start >= data.size()) {
                grid.setDisable(true);
                return;
            } else {
                grid.setDisable(false);
            }
            RowData items = data.get(start);

            for (Object item : items.getData()) {
                if (item == null) {
                   item = "(NULL)";
                }
                
                Node tf = manualLook("grid_child" + String.valueOf(i));
                if (tf == null) {
                    
                    String idStr = String.valueOf(i);
                        
                    ColumnData cd = tableInfoData.getColumnByIndex(i);
                    if (cd != null && cd.isDate()) {
                        
                        tf = new DateTimePicker();
                        ((DateTimePicker)tf).editableProperty().bind(Bindings.equal(readEditComboBox.getSelectionModel().selectedItemProperty(), cd.getDisplayTable()));
                      
                        try {
                            ((DateTimePicker)tf).setDateTimeValue(LocalDateTime.parse(item.toString(), DateTimeFormatter.ofPattern("yyy-MM-dd HH:mm:ss")));
                        } catch (Throwable th) {
                            ((DateTimePicker)tf).setDateTimeValue(LocalDateTime.parse(item.toString(), DateTimeFormatter.ofPattern("yyy-MM-dd")));
                        }
                        ((DateTimePicker)tf).dateTimeValueProperty().addListener((ObservableValue<? extends LocalDateTime> observable, LocalDateTime oldValue, LocalDateTime newValue) -> {
                            checkFormViewFieldValue(idStr, newValue.format(DateTimeFormatter.ofPattern("yyy-MM-dd HH:mm:ss")));
                        });
                    } else {
                        
                        tf = new TextField(item.toString());
                        ((TextField)tf).editableProperty().bind(Bindings.equal(readEditComboBox.getSelectionModel().selectedItemProperty(), cd.getDisplayTable()));
                        
                        ((TextField)tf).textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
                            checkFormViewFieldValue(idStr, t1);
                        });
                    }
                    GridPane.setHalignment(tf, HPos.CENTER);
                    
                    tf.setId("grid_child" + idStr);
                    ((Control)tf).setMaxWidth(250);
                    ((Control)tf).setPrefWidth(100);
                    grid.add(tf, 1, i + 1);

                } else {
                    if (tf instanceof TextField) {
                        ((TextField)tf).setText(item.toString());
                    } else if (tf instanceof DateTimePicker) {
                        try {
                            ((DateTimePicker)tf).setDateTimeValue(LocalDateTime.parse(item.toString(), DateTimeFormatter.ofPattern("yyy-MM-dd HH:mm:ss")));
                        } catch (Throwable th) {
                            ((DateTimePicker)tf).setDateTimeValue(LocalDateTime.parse(item.toString(), DateTimeFormatter.ofPattern("yyy-MM-dd")));
                        }
                    }
                }
                i++;

//                if(bj != null && bj.isInterrupted()){
//                    return;
//                }
            }

//            if(bj != null && bj.isInterrupted()){
//                return;
//            }

            if (!formViewPopulated) {
                
                formViewPopulated = true;
                
                // @TODO
                // I think this is wrong
                // but work because do this when start = 0
                short where = (short) start;

                for (; where < columnNames.size(); where++) {
                    Label l = (Label) manualLook("label" + String.valueOf(where));
                    if (l == null) {
                        l = Recycler.getFromBin(Label.class);
                        if (l == null) {
                            l = new Label();
                        }
                        l.setId("label" + String.valueOf(where));
                        grid.add(l, 0, where + 1);
                        GridPane.setHalignment(l, HPos.CENTER);
                    }
                    l.setText(columnNames.get(where));

//                    if(bj != null && bj.isInterrupted()){
//                        return;
//                    }
                }
                
                while (true) {
//                    if(bj != null && bj.isInterrupted()){
//                        return;
//                    }

                    Label l = (Label) manualLook("label" + String.valueOf(where));
                    if (l == null) {
                        break;
                    }
                    l.setText("");
                    l.setId("");
                    grid.getChildren().remove(l);
                    Recycler.addItem(l);
                    Node tf =  manualLook("grid_child" + String.valueOf(where));
                    if (tf != null) {
                        tf.setUserData(null);
//                        tf.setText("");
                        tf.setId("");
                        grid.getChildren().remove(tf);
                        Recycler.addItem(tf);
                    }
                    where++;

//                    if(bj != null && bj.isInterrupted()){
//                        return;
//                    }
                }
            }
        } finally {
            formViewInited.set(true);
        }
    }

    private void checkFormViewFieldValue(String idStr, Object value) {
        if (formViewInited.get() && !data.isEmpty()) {
            int columnIndex = Integer.parseInt(idStr);

//            if(bj != null && bj.isInterrupted()){
//                return;
//            }
            
            RowData row = data.get(forFormPos);  
            if (row.getData().size() > columnIndex) {
                row.getData().set(columnIndex, value);
                if (!row.isNewRow()) {
                    Object v = sourceData.get(row).getData().get(columnIndex);
                    String strValue = v != null ? v.toString() : v.toString();
                    if (strValue == null) {
                        strValue = "(NULL)";
                    }
                    if (value != null && !value.equals(strValue)) {
                        changesMap.put(row, sourceData.get(row));
                    }
                }
            }
        }
    }
    
    
    /**
     * grid.lookUp is giving me problems and inconsistency so i re-created mine-
     * yes, i know; *
     */
    private Node manualLook(String s) {
        Node node = null;
        for (Node n : grid.getChildren()) {
            if (s.equals(n.getId())) {
                node = n;
                break;
            }
        }

        return node;
    }

   /**
    * A map (used for cache) which has a size limit.
    * @param <K>
    * @param <V>
    */
    public static class MaxSizeHashMap<K, V> extends LinkedHashMap<K, V> {
        private final int maxSize;

        public MaxSizeHashMap(int maxSize) {
            this.maxSize = maxSize;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > maxSize;
        }
    }

    private ImageView standardSizeImageviewForResultTab(Image im){
        return FunctionHelper.constructImageViewWithSize(new ImageView(im), 16, 16);
    }
    
    private class ResultTabFindObject implements FindObject {
        
        private TextArea area;
                
        public ResultTabFindObject(TextArea area) {
            this.area = area;
        }
        
        @Override
        public int getCaretPosition(byte a) {
            return area.getCaretPosition();
        }

        @Override
        public void replaceText(int from, int to, String text) {
            // not used
        }

        @Override
        public String getText() {
            return area.getText();
        }

        @Override
        public void clear() {
            area.positionCaret(0);
        }

        @Override
        public void appendText(String text) {
            // not used
        }

        @Override
        public void selectRange(int from, int to) {
            area.selectRange(from, to);
        }
        
    }
}
