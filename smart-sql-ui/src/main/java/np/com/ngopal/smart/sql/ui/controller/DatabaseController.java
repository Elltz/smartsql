package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 *
 * @author terah
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DatabaseController extends BaseController implements Initializable {

    private static final String DEFAULT = "[default]";
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private TextField databaseNameInput;
    @FXML
    private ComboBox<String> collationbox;
    @FXML
    private ComboBox<String> charsetbox;
    @FXML
    private CheckBox usingPublicSchemaCheckbox;
    
    private Dialog<Void> dialog;

    private Button myApply;
    
    private boolean status = false;

    public boolean isNeedUseSchema() {
        return usingPublicSchemaCheckbox.isSelected();
    }
    
    public String getDatabaseName() {
        return databaseNameInput.getText();
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public Stage getStage() {
        return (Stage) dialog.getOwner();
    }

    public boolean showCreate() {
        usingPublicSchemaCheckbox.setDisable(false);
        
        create();        
        dialog.showAndWait();
        
        return status;
    }
    
    public boolean showAlter(Database database) {
                        
        try (ResultSet rs0 = (ResultSet) dBService.execute(((AbstractDBService)dBService).getDatabaseCharsetAndCollateSQL(database.getName()))) {
            if (rs0.next()) {
                database = new Database(null, database.getName(), rs0.getString(1), rs0.getString(2), null, null, null, null, null, null, null);
            }
        } catch (SQLException ex) {
            log.error("Error", ex);
        }
        
        usingPublicSchemaCheckbox.setDisable(true);
        alter(database);        
        dialog.showAndWait();
        
        return status;
    }

    private void alter(Database database) {
        dialog = new Dialog<>();
        dialog.initStyle(StageStyle.UNIFIED);
        dialog.setTitle("Alter Database");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        myApply = ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK));
        myApply.setText("Alter");
        dialog.getDialogPane().setContent(mainUI);
        databaseNameInput.setDisable(true);
        databaseNameInput.setText(database.getName());
        myApply.addEventFilter(ActionEvent.ACTION, (ActionEvent event) -> {
            if (databaseNameInput.getText().length() < 1) {
                errorPrompt();
                event.consume();
            }
        });

        myApply.setOnAction((ActionEvent event) -> {
            String[] res = getInputs();
            try {
                int result = (int)dBService.alterDatabase(res[0], DEFAULT.equals(res[1]) ? "DEFAULT" : res[1], DEFAULT.equals(res[2]) ? "DEFAULT" : res[2]);
                Platform.runLater(() -> {
                   if (result >= 0) {
                        status = true;
                        DialogHelper.showInfo(getController(), "Info", "Database altered successfully", null);
                    } else {
                        DialogHelper.showError(getController(), "Error", "Database alteration error", null);
                    } 
                });
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", "Error modifying database", ex);
            }
        });

        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setOnAction((ActionEvent event) -> {
            dialog.close();
        });

        initData();
        
        if (database.getCharset() != null) {
            charsetbox.getSelectionModel().select(database.getCharset());
        }
        
        if (database.getCollate() != null) {
            collationbox.getSelectionModel().select(database.getCollate());
        }        
    }

    private void create() {
        dialog = new Dialog<>();
        dialog.initStyle(StageStyle.UNIFIED);
        dialog.setTitle("Create Database");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        myApply = ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK));
        myApply.setText("Create");
        dialog.getDialogPane().setContent(mainUI);
        myApply.addEventFilter(ActionEvent.ACTION, (ActionEvent event) -> {
            if (databaseNameInput.getText().length() < 1) {
                errorPrompt();
                event.consume();
            }
        });

        myApply.setOnAction((ActionEvent event) -> {
            String[] res = getInputs();
            try {
                int result = (int)dBService.createDatabase(res[0], DEFAULT.equals(res[1]) ? null : res[1], DEFAULT.equals(res[2]) ? null : res[2]);
                if (result < 0) {
                    DialogHelper.showError(getController(), "Error", "Database creation error", null);
                    
                } else {
                    status = true;
                }
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", "Error creating database", ex);
            }
        });

        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setOnAction((ActionEvent event) -> {
            dialog.close();
        });

        initData();
    }
    
    private void initData() {
        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////choicebox section///////////////////////////////////////
        charsetbox.getItems().add(DEFAULT);
        try {
            charsetbox.getItems().addAll(dBService.getCharSet());
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error loading charset", ex);
        }
        collationbox.getItems().add(DEFAULT);
        charsetbox.getSelectionModel().select(0);
        collationbox.getSelectionModel().select(0);
        charsetbox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            collationbox.getItems().clear();
            collationbox.setValue(null);
            if (DEFAULT.equals(newValue)) {
                collationbox.getItems().add(DEFAULT);
                collationbox.getSelectionModel().select(0);
            } else {
                collationbox.getItems().add(DEFAULT);
                try {
                    collationbox.getItems().addAll(dBService.getCollation(newValue));
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", "Error loading collations", ex);
                }
                collationbox.getSelectionModel().select(0);
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////
    }
    
    private String[] getInputs() {
        return new String[]{databaseNameInput.getText(), charsetbox.getSelectionModel().
            getSelectedItem(), collationbox.getSelectionModel().getSelectedItem()};
    }

    private DBService dBService;

    public void setDBService(DBService dBService) {
        this.dBService = dBService;
    }


    private void errorPrompt() {
        DialogHelper.showError(getController(), "Error", "Database name empty", null);
    }
}