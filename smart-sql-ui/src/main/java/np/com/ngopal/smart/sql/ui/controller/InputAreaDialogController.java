package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class InputAreaDialogController extends BaseController implements Initializable {
        
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TextArea inputArea;
        
    
    private String data = null;
    
    
    @FXML
    void okAction(ActionEvent event) {
        data = inputArea.getText();
        close();
    }    
    
    @FXML
    void cancelAction(ActionEvent event) {
        data = null;
        close();
    }
    
    private void close() {
        ((Stage)mainUI.getScene().getWindow()).close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {        
        
    }
        
    public String getData() {
        return data;
    }
}
