/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import javafx.geometry.Point2D;
import np.com.ngopal.smart.sql.model.DBElement;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class DBElementWrapperOnBuilder {
    
    public DBElement dbe;
    public Point2D point;
    
    public DBElementWrapperOnBuilder(Number x, Number y, DBElement dbelement){
        dbe = dbelement;
        point = new Point2D(x.doubleValue(), y.doubleValue());
    }
    
    public DBElementWrapperOnBuilder(Point2D point, DBElement dmelement){
        dbe = dmelement;
        this.point = point;
    }
    
}
