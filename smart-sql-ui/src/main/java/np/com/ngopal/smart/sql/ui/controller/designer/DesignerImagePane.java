package np.com.ngopal.smart.sql.ui.controller.designer;

import java.io.ByteArrayInputStream;
import java.util.Base64;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import np.com.ngopal.smart.sql.structure.models.DesignerImage;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerImagePane extends AnchorPane implements DesignerPane {
           
    DesignerImage data;

    ImageView image;

    boolean sizeChanged = false;
    
    SchemaDesignerTabContentController controller;


    public DesignerImagePane(SchemaDesignerTabContentController controller, DesignerImage data) {

        getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_IMAGE);
        this.data = data;
        this.controller = controller;

        data.color().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateData();
        });
        
        data.filename().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateData();
        });
        
        
        addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                controller.showImageTab(data);
            }
        });


        image = new ImageView();
        image.fitWidthProperty().bind(widthProperty());
        image.fitHeightProperty().bind(heightProperty());
        
        getChildren().add(image);
        
        AnchorPane.setBottomAnchor(image, 0d);
        AnchorPane.setTopAnchor(image, 0d);
        AnchorPane.setLeftAnchor(image, 0d);
        AnchorPane.setRightAnchor(image, 0d);

        updateData();
        
        addDefaultDataListeners();
    }
    
    public void showContextMenu() {
        
    }
    

    @Override
    public DesignerImage getData() {
        return data;
    }

    @Override
    public Region getRegion() {
        return this;
    }

    @Override
    public boolean canChangeHeight() {
        return true;
    }
    
    

    @Override
    public void setPrefSize(double prefWidth, double prefHeight) {
        super.setPrefSize(prefWidth, prefHeight);

        sizeChanged = true;            
        updateData();
    }

    private void updateData() {               

        String style = "";
        if (data.color().get() != null && !data.color().get().isEmpty()) {
            style += "-fx-background-color: " + data.color().get() + "; ";
        }
        
        try {
            if (data.imageData().get() != null) {
                Image img = new Image(new ByteArrayInputStream(Base64.getDecoder().decode(data.imageData().get())));
                image.setImage(img);            

                data.imageDefaultWidth().set(img.getWidth());
                data.imageDefaultHeight().set(img.getHeight());

                if (data.width().get() <= 0) {
                    data.width().set(img.getWidth());
                }

                if (data.height().get() <= 0) {
                    data.height().set(img.getHeight());
                }
            }
        } catch (Throwable th) {
        }
        
        setStyle(style);
        
        setMinSize(10, 10); 
    }
}
