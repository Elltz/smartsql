/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author terah laweh (rumorsapp@gmail.com)
 */
public class ScheduleJobsController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button editButton;

    @FXML
    private Button cancelButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button executeButton;

    @FXML
    private ListView jobsListview;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        editButton.setDisable(true);
        deleteButton.setDisable(true);
        executeButton.setDisable(true);

        editButton.setOnAction(onAction);
        cancelButton.setOnAction(onAction);
        executeButton.setOnAction(onAction);
        deleteButton.setOnAction(onAction);

    }

    private final EventHandler<ActionEvent> onAction = new EventHandler() {

        @Override
        public void handle(Event t) {
            Object o = t.getSource();
            if (o == editButton) {

            } else if (o == deleteButton) {

            } else if (o == executeButton) {

            } else if (o == cancelButton) {
                editButton.setOnAction(null);
                cancelButton.setOnAction(null);
                executeButton.setOnAction(null);
                deleteButton.setOnAction(null);
                dialog.hide();
            }
        }

    };

    private Stage dialog;

    public void init(Window win) {
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(360);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Scheduled Jobs");
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();
    }

}
