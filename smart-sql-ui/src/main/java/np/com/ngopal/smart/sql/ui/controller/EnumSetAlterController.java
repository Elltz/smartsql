package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class EnumSetAlterController extends BaseController implements Initializable {
   
    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button okButton;
    @FXML
    private Button upButton;
    @FXML
    private Button downButton;
    @FXML
    private Button addButton;
    @FXML
    private Button replaceButton;
    @FXML
    private Button removeButton;

    @FXML
    private ListView<String> dataListView;
    
    @FXML
    private TextField valueField;

    private Stage stage;

    private List<String> data;
    
    private volatile boolean changedFromListView = false;
    
    @FXML
    void up(ActionEvent event) {
        int index = dataListView.getSelectionModel().getSelectedIndex();
        String selected = dataListView.getItems().remove(index);
        
        dataListView.getItems().add(--index, selected);
        listViewChanged(index);
    }
    
    @FXML
    void down(ActionEvent event) {
        int index = dataListView.getSelectionModel().getSelectedIndex();
        String selected = dataListView.getItems().remove(index);
        
        dataListView.getItems().add(++index, selected);
        listViewChanged(index);
    }
    
    @FXML
    void add(ActionEvent event) {
        dataListView.getItems().add(valueField.getText());
        addButton.setDisable(true);
        
        listViewChanged(dataListView.getItems().size() - 1);
    }
    
    @FXML
    void replace(ActionEvent event) {
        int index = dataListView.getSelectionModel().getSelectedIndex();
        dataListView.getItems().set(index, valueField.getText());
        addButton.setDisable(true);
        
        listViewChanged(index);
    }
    
    @FXML
    void remove(ActionEvent event) {
        int index = dataListView.getSelectionModel().getSelectedIndex();
        dataListView.getItems().remove(index);
        
        listViewChanged(index);
    }

    @FXML
    void cancel(ActionEvent event) {
        data = null;
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @FXML
    void ok(ActionEvent event) {
        data = dataListView.getItems();
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();

    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        valueField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            checkEditButtons();
            if (!changedFromListView) {
                addButton.setDisable(false);
            }
        });
        
        dataListView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            checkEditButtons();
            
            int index = dataListView.getSelectionModel().getSelectedIndex();
            upButton.setDisable(index <= 0);
            downButton.setDisable(!(index >= 0 && index < dataListView.getItems().size() - 1));
            
            changedFromListView = true;
            if (newValue != null) {
                valueField.setText(newValue);
            }
            changedFromListView = false;
        });
        
        removeButton.setDisable(true);
        replaceButton.setDisable(true);
    }
    
    private void listViewChanged(int selectedIndex) {
        Platform.runLater(() -> {
            int size = dataListView.getItems().size();
            dataListView.getSelectionModel().select(selectedIndex > size ? size : selectedIndex);
            
            valueField.selectAll();
            valueField.requestFocus();
        });
    }
    
    private void checkEditButtons() {
        String valueText = valueField.getText();
        String selectedValue = dataListView.getSelectionModel().getSelectedItem();
        
        log.debug("value text: " + valueText + ", selected: " + selectedValue);
        
        replaceButton.setDisable(valueText.isEmpty() || selectedValue == null || selectedValue.equals(valueText));
        removeButton.setDisable(selectedValue == null);
    }

    public void init(Set<String> data) {
        valueField.clear();
        dataListView.getItems().clear();
        if (data != null) {
            dataListView.getItems().addAll(data);
        }
        
        if (mainUI.getScene() == null) {
            mainUI.sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
                if (newValue != null) {
                    listViewChanged(0);
                }
            });
        } else {
            listViewChanged(0);
        }
    }

    public Set<String> getData() {
        return data != null ? new LinkedHashSet<>(data) : null;
    }
}
