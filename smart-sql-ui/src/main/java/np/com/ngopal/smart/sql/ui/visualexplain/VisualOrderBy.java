package np.com.ngopal.smart.sql.ui.visualexplain;

import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class VisualOrderBy {
    public double x;
    public double y;
    public double width;
    public double height;
    public Color color;
    public String name;
    public String key;
}
