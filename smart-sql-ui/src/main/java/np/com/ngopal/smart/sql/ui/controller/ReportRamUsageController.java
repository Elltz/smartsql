package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportRamUsageController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
    
    @FXML
    private Button refreshButton;
    
    @FXML
    private Button exportButton;
    
    private ConnectionSession session;
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportRamUsage rds: (List<ReportRamUsage>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rds.getCodeArea());
                        row.add(rds.getCurrentAlloc());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    columns.put("Code area", true);
                    columnWidths.add(250);

                    columns.put("Current alloc", true);
                    columnWidths.add(250);
                                        
                    sheets.add("RAM usage from Performance schema report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    @FXML
    public void refreshAction(ActionEvent event) {
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            if (session != null) {

                try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_USAGE))) {
                    if (rs != null) {
                        List<ReportRamUsage> list = new ArrayList<>();
                        while (rs.next()) {
                            ReportRamUsage rds = new ReportRamUsage(
                                rs.getString(1), 
                                rs.getString(2)
                            );  

                            list.add(rds);
                        }

                        tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));

                        if (list.size() == 1) {
                            Platform.runLater(() -> {
                                
                                ((MainController)getController()).showInfo("Info", "To enable memory instruments, add a performance-schema-instrument rule \n"
                                               + "to your MySQL configuration file. For example, to enable all memory instruments,\n"
                                               + "add this rule to your configuration file and restart the server:\n\n" +
                                    "performance-schema-instrument='memory/%=COUNTED'\n\n" +
                                    "For more details: https://dev.mysql.com/doc/refman/5.7/en/memory-use.html", null);
                            });
                        }
                    }
                } catch (SQLException ex) {
                    ((MainController)getController()).showError("Error", ex.getMessage(), null);
                }
            }
            
            Platform.runLater(() -> makeBusy(false));
        });
        th.setDaemon(true);
        th.start();
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;        
        
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        TableColumn codeAreaColumn = createColumn("Code area", "codeArea");        
        TableColumn databaseSizeColumn = createColumn("Current alloc", "currentAlloc");
        
        tableView.addTableColumn(codeAreaColumn, 0.50);
        tableView.addTableColumn(databaseSizeColumn, 0.45);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        tableView.setSearchEnabled(true);
    }
    
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }    
}
