/*
 * (C) Copyright 2012 JavaFXGraph (http://code.google.com/p/javafxgraph/).
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3.0 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package np.com.ngopal.smart.sql.ui.fxgraph;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerImagePane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerLayerTittledPane;

public class FXGraph extends ScrollPane {

    static final double NODES_Z_OFFSET = 10;
    static final double EDGES_Z_OFFSET = 10;

    private static final double PAGE_WIDTH     = 985;
    private static final double PAGE_HEIGHT    = 1300;
    
    Pane contentPane;

    FXGraphModel model;
    FXGraphSelectionTool selectionTool;
    FXGraphZoomHandler zoomHandler;

    FXTool currentTool;

    private FXGraphMouseHandler mouseHandler;

    private ChangeListener<? super Bounds> birdsEyeListener;
    private Canvas birdsEyeCanvas;
    
    private int widthPages = 2;
    private int heightPages = 1;
        
    public FXGraph() {

        model = new FXGraphModel();

        setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
        contentPane = new Pane();
        
        contentPane.setPrefSize(PAGE_WIDTH * widthPages, PAGE_HEIGHT * heightPages);
        
        
        setContent(new Group(contentPane));
        

        zoomHandler = new FXGraphZoomHandler(this);
        selectionTool = new FXGraphSelectionTool(contentPane, model, zoomHandler);
        mouseHandler = new FXGraphMouseHandler(this);

        mouseHandler.registerHandlerFor(this);
        
        currentTool = selectionTool;
        
        birdsEyeListener = (ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) -> {
            updateBirdsEye();
        };
        
        viewportBoundsProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) -> {
            updateBirdsEye();
        });
        
        hvalueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            updateBirdsEye();
        });
        
        vvalueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            updateBirdsEye();
        });
    }
    
    public SimpleObjectProperty<FXNode> selectedNodeProperty() {
        return selectionTool.selectedNodeProperty();
    }
    
    public void zoom(int zoom) {
        contentPane.setScaleX(zoom / 100.0);
        contentPane.setScaleY(zoom / 100.0);
        
        updateBirdsEye();
    }
    
    public void increaseWidth() {
        widthPages++;
        contentPane.setPrefWidth(PAGE_WIDTH * widthPages);
        contentPane.resize(PAGE_WIDTH * widthPages, PAGE_HEIGHT * heightPages);
        Platform.runLater(() -> updateBirdsEye());
    }
    
    public void increaseHeight() {
        heightPages++;
        contentPane.setPrefHeight(PAGE_HEIGHT * heightPages);
        contentPane.resize(PAGE_WIDTH * widthPages, PAGE_HEIGHT * heightPages);
        Platform.runLater(() -> updateBirdsEye());
    }
    

    public void setBirdsEyeCanvas(Canvas birdsEyeCanvas) {
        this.birdsEyeCanvas = birdsEyeCanvas;
    }
    
    
    
    public byte[] getImage() throws IOException {
        selectionTool.resetSelection();
        
        Image diagramImage = contentPane.snapshot(null, null);

        ByteArrayOutputStream s = new ByteArrayOutputStream();
        ImageIO.write(SwingFXUtils.fromFXImage(diagramImage, null), "png", s);

        return s.toByteArray();
    }

    public void selectNode(FXNode fxNode) {
        selectionTool.select(fxNode);
    }
    
    public FXNode getSelectedNode() {
        return selectionTool.getCurrentSelection();
    }
    
    void updateEdge(FXEdge aEdge, double aZoomLevel) {
        aEdge.removeAllNodes(contentPane);
        aEdge.computeDisplayShape(aZoomLevel);
        aEdge.addAllNodes(contentPane);
        mouseHandler.registerNewEdge(aEdge);
    }

    void updateEdgeNodesFor(FXNode aNode, double aZoomLevel) {
        for (FXEdge theEdge : model.getEdges()) {
            if (theEdge.source == aNode || theEdge.destination == aNode) {
                updateEdge(theEdge, aZoomLevel);
            }
        }
    }

    void updateEdgeNodesFor(FXNode aNode) {
        // todo need remove zoom lvl
        updateEdgeNodesFor(aNode, 1.0);
    }

    public FXNode getNode(double x, double y) {
        
        for (FXNode node : model.getNodes()) {
            
            Bounds bounds = node.wrappedNode.getBoundsInParent();
            if (bounds.getMinX() <= x && bounds.getMaxX() > x && 
                bounds.getMinY() <= y && bounds.getMaxY() > y) {
                return node;
            }
        }
        
        return null;
    }
    
    public void removeNode(FXNode node) {        
        contentPane.getChildren().remove(node.wrappedNode);
        model.unregisterNode(node);
        mouseHandler.unregisterNode(node);
        
        node.wrappedNode.boundsInParentProperty().removeListener(birdsEyeListener);
        
        if (node == selectionTool.getCurrentSelection()) {
            selectionTool.resetSelection();
        }
    }

    public void addNode(FXNode aNode) {

        aNode.wrappedNode.setTranslateZ(NODES_Z_OFFSET);

        if (aNode.wrappedNode instanceof DesignerImagePane) {
        
            // image always at front
            contentPane.getChildren().add(aNode.wrappedNode);
            
        } else if (aNode.wrappedNode instanceof DesignerLayerTittledPane) {
            
            // layer always at backward
            int index = 0;
            for (Node n: contentPane.getChildren()) {
                if (!(n instanceof DesignerImagePane)) {
                    break;
                }
                
                index++;
            }
            
            contentPane.getChildren().add(index > contentPane.getChildren().size() ? contentPane.getChildren().size() : index, aNode.wrappedNode);
            
        } else {
            
            // others between image and layer
            int index = 0;
            for (Node n: contentPane.getChildren()) {
                if (!(n instanceof DesignerLayerTittledPane)) {
                    break;
                }
                
                index++;
            }
            
            contentPane.getChildren().add(index > contentPane.getChildren().size() ? contentPane.getChildren().size() : index, aNode.wrappedNode);
        }
        

        model.registerNewNode(aNode);
        mouseHandler.registerNewNode(aNode);
        
        aNode.wrappedNode.boundsInParentProperty().addListener(birdsEyeListener);
        
        aNode.wrappedNode.boundsInParentProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) -> {
            
            if (newValue.getMaxX() >= contentPane.getWidth()) {
                increaseWidth();
            }
            
            if (newValue.getMaxY() >= contentPane.getHeight()) {
                increaseHeight();
            }
        });
    }
    
    public void clearNodes() { 
        while (model.getNodes().size() > 0) {
            FXNode node = model.getNodes().iterator().next();
            removeNode(node);
        }
        
        selectionTool.resetSelection();
    }

    public void addEdge(FXEdge aEdge) {

        aEdge.computeDisplayShape(zoomHandler.currentZoomLevel);

        aEdge.addAllNodes(contentPane);
        model.registerNewEdge(aEdge);

        mouseHandler.registerNewEdge(aEdge);
    }

    public void clearEdges() {
        while (model.getEdges().size() > 0) {
            FXEdge edge = model.getEdges().iterator().next();
            removeEdge(edge);
        }
    }
    
    public void removeEdge(FXEdge edge) {
        edge.removeAllNodes(contentPane);
        model.unregisterEdge(edge);
        mouseHandler.unregisterEdge(edge);
    }
    
    
    protected void showCorners() {
        selectionTool.showCorners();
    }
    
    protected void hideCorners() {
        selectionTool.hideCorners();
    }
    
    public SimpleObjectProperty<FXNode> movedProperty() {
        return selectionTool.movedProperty();
    }
    public SimpleObjectProperty<FXNode> resizedProperty() {
        return selectionTool.resizedProperty();
    }

    public Pane getContentPane() {
        return contentPane;
    }    
    
    
    private final Image imageBird = new Image("/np/com/ngopal/smart/sql/ui/images/designer/image.png");
    public void updateBirdsEye() {
        
        if (birdsEyeCanvas != null) {
            GraphicsContext gc = birdsEyeCanvas.getGraphicsContext2D();
            gc.setFill(Color.WHITE);
            gc.setStroke(Color.BLACK);
            gc.setLineWidth(2.0);
            gc.fillRect(0, 0, birdsEyeCanvas.getWidth(), birdsEyeCanvas.getHeight());
            gc.strokeRect(0, 0, birdsEyeCanvas.getWidth(), birdsEyeCanvas.getHeight());

            gc.setStroke(Color.web("#1a1a1a"));
            gc.setLineWidth(1.0); 
            
            
            // canvas scaling
            double widthScale = birdsEyeCanvas.getWidth() / contentPane.getWidth();
            double heightScale = birdsEyeCanvas.getHeight() / contentPane.getHeight();

            // draw page lines
            
            int verticalCount = (int) (contentPane.getWidth() / PAGE_WIDTH) - 1;
            for (int i = 0; i < verticalCount; i++) {
                double x = widthScale * PAGE_WIDTH + i * widthScale * PAGE_WIDTH;
                gc.strokeLine((int)x + 0.5, 0, (int)x + 0.5, birdsEyeCanvas.getHeight());
            }

            int horizontalCount = (int) (contentPane.getHeight() / PAGE_HEIGHT) - 1;
            for (int i = 0; i < horizontalCount; i++) {
                double y = heightScale * PAGE_HEIGHT + i * heightScale * PAGE_HEIGHT;
                gc.strokeLine(0, (int)y + 0.5, birdsEyeCanvas.getWidth(), (int)y + 0.5);
            }



            for (FXNode n: model.getNodes()) {
                try {
                    Bounds b = n.wrappedNode.getBoundsInParent();

                    String color = n.color.apply(n.wrappedNode);

                    // its image
                    if (color == null) {
                        gc.drawImage(imageBird, b.getMinX() * widthScale, b.getMinY() * heightScale, b.getWidth() * widthScale, b.getHeight() * heightScale);
                    } else {
                        gc.setFill(Color.web(color));
                        gc.fillRect(b.getMinX() * widthScale, b.getMinY() * heightScale, b.getWidth() * widthScale, b.getHeight() * heightScale);
                    }
                } catch (Throwable th) {}
            }
            
                        
            double hmin = getHmin();
            double hmax = getHmax();
            double hvalue = getHvalue();
            double contentWidth = contentPane.getLayoutBounds().getWidth() * contentPane.getScaleX();
            double viewportWidth = getViewportBounds().getWidth();

            double hoffset = 
                Math.max(0, contentWidth - viewportWidth) * (hvalue - hmin) / (hmax - hmin);

            double vmin = getVmin();
            double vmax = getVmax();
            double vvalue = getVvalue();
            double contentHeight = contentPane.getLayoutBounds().getHeight() * contentPane.getScaleY();
            double viewportHeight = getViewportBounds().getHeight();

            double voffset = 
                Math.max(0,  contentHeight - viewportHeight) * (vvalue - vmin) / (vmax - vmin);

            gc.setStroke(Color.BLACK);
            gc.setLineWidth(1.0);
            gc.strokeRect(hoffset * widthScale / contentPane.getScaleX(), voffset * heightScale / contentPane.getScaleY(), viewportWidth * widthScale / contentPane.getScaleX(), viewportHeight * heightScale / contentPane.getScaleY());
        }
    }

    
}