package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Function;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.Mnemonic;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.StringConverter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Advanced;
import np.com.ngopal.smart.sql.model.Checksum;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.DelayKeyWrite;
import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.Rowformat;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataTypes;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.ui.factories.EditableDataTypeTableCell;
import np.com.ngopal.smart.sql.ui.factories.EditingCell;
import java.util.Objects;
import java.util.UUID;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javax.swing.SwingUtilities;
import lombok.Getter;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.core.modules.utils.TableViewTabMoverHandler;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.ui.MainUI;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import tools.java.pats.models.SqlFormatter;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.misc.SelectionColumn;
import np.com.ngopal.smart.sql.structure.misc.SelectionForeignKey;
import np.com.ngopal.smart.sql.structure.misc.SelectionIndex;
import np.com.ngopal.smart.sql.structure.misc.SelectionTable;
import np.com.ngopal.smart.sql.structure.queryutils.RSyntaxTextAreaBuilder;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 * FXML Controller class
 * <p>
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class AlterTableController extends BaseController implements
          Initializable {

    @Setter
    private DBService service;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    @FXML
    private TableColumn<Column, DataType> dataTypeCol;

    @FXML
    private ComboBox<Object> engineComboBox;

    @FXML
    private TableColumn<Column, String> columnNameCol;

    @FXML
    private TableColumn<Column, DataType> lengthCol;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button downBttn;

    @FXML
    private TableColumn<Column, DataType> unsignedCol;

    @FXML
    private Tab AdvancedTab;

    @FXML
    private CheckBox hideLanguageCheckBox;

    @FXML
    private TableColumn<Column, String> defaultCol;

    @FXML
    private Button addBttn;

    @FXML
    private Button addIndex;
    @FXML
    private Button removeIndex;
    
    @FXML
    private Button addFk;
    @FXML
    private Button removeFk;
    
    @FXML
    private Tab sqlPreviewTab;

    @FXML
    private TableColumn<Column, DataType> notNullCol;

    @FXML
    private TextField tableNameTf;

    @FXML
    private Tab foreignKeysTab;

    @FXML
    private ComboBox<String> characterSetComboBox;

    @FXML
    private ComboBox<String> collationComboBox;

    @FXML
    private TableColumn<Column, DataType> pkCol;

    @FXML
    private TableColumn<Column, DataType> zeroFillCol;
    
    @FXML
    private BorderPane areaPane;
    @FXML
    private Button copyPreview;

//    @FXML
//    private TableColumn<Column, Boolean> onUpdateCol;

    @FXML
    private TableColumn<Column, String> charsetCol;

    @FXML
    private TableColumn<Column, String> collationCol;

    @FXML
    private CheckBox checkAllCheckBox;

    @FXML
    private Button subtractBttn;
    
    @FXML
    private Button copyScriptBttn;

    @FXML
    private TabPane alterTableTabPane;

    @FXML
    private TableView<SelectionColumn> tableView;

    @FXML
    private Tab indexesTab;

    @FXML
    private Button upBttn;

    @FXML
    private TableColumn<Column, String> commentCol;

    @FXML
    private Tab ColumnsTab;

    @FXML
    private ComboBox<String> databaseComboBox;

    @FXML
    private TableColumn<Column, DataType> autoIncrCol;

    @FXML
    private TableColumn<Column, Boolean> checkBoxCol;

    @FXML
    private Button saveBttn;

    @FXML
    private Button revertBttn;

    @FXML
    private TableView<SelectionIndex> indexTableView;

    @FXML
    private CheckBox checkAllIndexesCheckBox;
    
    @FXML
    private TableColumn<Index, String> indexNameCol;

    @FXML
    private TableColumn<Index, String> indexColumnsCol;

    @FXML
    private TableColumn<Index, Boolean> indexCheckBox;

    @FXML
    private TextField minRowsTextField;

    @FXML
    private TextField autoIncrementTextField;

    @FXML
    private TextField maxRowsTextField;

    @FXML
    private TextField avgRowLengthTextField;

    @FXML
    private TextField rowFormatTextField;

    @FXML
    private TextArea commentTextArea;

    @FXML
    private ComboBox<DelayKeyWrite> delayKeyWriteComboBox;

    @FXML
    private ComboBox<Checksum> checksumComboBox;

    @FXML
    private ComboBox<Rowformat> rowFormatComboBox;

    @FXML
    private TableColumn<Index, IndexType> indexTypeCol;

    @FXML
    private Tab columnsTab;
    
//    private WebView webView;
    private RSyntaxTextArea previewArea; 
    private RSyntaxTextAreaBuilder builder;
    
    private boolean inited = false;
    private String actualSQL;
    
    private boolean alterMode = true;
    private Tab rootTab;
    
    @Getter
    private DBTable table;
    private ConnectionSession session;
    
    private ObjectProperty<SelectionTable> selectionTable;

    private final ObservableList<SelectionColumn> data = FXCollections.observableArrayList();

    private ObservableList<String> charSetList;

    private ObservableList<String> collationList;

    private ObservableList<String> databaseList;
    
    private ObservableList engineList;
    
    private final ObservableList<String> foreignKeyRules = FXCollections.observableArrayList("CASCADE", "NO ACTION", "RESTRICT", "SET NULL");
    
    private ObservableList<SelectionIndex> indexes;

    
    @FXML
    private TableView<SelectionForeignKey> foreignTableView;
    @FXML
    private CheckBox checkAllForeignKeysCheckBox;
    
    private ObservableList<SelectionForeignKey> foreignKeys;
    
    @FXML
    private TableColumn<ForeignKey, Boolean> foreignCheckBox;
    @FXML
    private TableColumn<ForeignKey, String> foreignConstraintNameCol;
    @FXML
    private TableColumn<ForeignKey, String> foreignReferencingCol;
    @FXML
    private TableColumn<ForeignKey, String> foreignReferencedDbCol;
    @FXML
    private TableColumn<ForeignKey, String> foreignReferencedTableCol;
    @FXML
    private TableColumn<ForeignKey, String> foreignReferencedCol;
    @FXML
    private TableColumn<ForeignKey, String> foreignOnUpdateCol;
    @FXML
    private TableColumn<ForeignKey, String> foreignOnDeleteCol;

    
    private void updateIndexes() {        
        indexes.clear();
        
        List<Index> selectionIndexes = new ArrayList<>();
        if (table.getIndexes() != null) {
            for (Index index : table.getIndexes()) {
                SelectionIndex idx = new SelectionIndex();
                idx.copy(index);
                selectionIndexes.add(idx);
                indexes.add(idx);
            }
        }
        
        selectionTable.get().setIndexes(selectionIndexes);
    }

    public void manageIndexes() {
        alterTableTabPane.getSelectionModel().select(indexesTab);
    }
    
    public void manageColumns() {
        alterTableTabPane.getSelectionModel().select(columnsTab);
    }
    
    
    public void createIndex() {
        indexPlusButton(null);
    }
    
    public void editIndex(Index index) {
        Platform.runLater(() -> {
            int idx = -1;
            int i = 0;
            for (SelectionIndex si: indexes) {
                if (index.getName() != null && index.getName().equals(si.getName())) {
                    idx = i;
                    break;
                }
                i++;
            }
            indexTableView.getSelectionModel().select(idx, indexTableView.getColumns().get(1));
            indexTableView.requestFocus(); 
        });
    }
    
    private void updateForeignKeys() {        
        foreignKeys.clear();
        
        List<ForeignKey> selectionForeignKeys = new ArrayList<>();
        if (table.getForeignKeys() != null) {
            for (ForeignKey fk : table.getForeignKeys()) {
                SelectionForeignKey sfk = new SelectionForeignKey();
                sfk.copy(fk);
                selectionForeignKeys.add(sfk);
                foreignKeys.add(sfk);
            }
        }
        
        selectionTable.get().setForeignKeys(selectionForeignKeys);
    }
    
    public void manageForeignKeysTab() {
        alterTableTabPane.getSelectionModel().select(foreignKeysTab);
    }
    
    /**
     * Initializes the controller class.
     */
    public void indexesTab() {
        indexes = FXCollections.observableArrayList();
        updateIndexes();
        log.debug("indexes size :{}", indexes.size());
        log.debug("indexes primary size :{}", indexes.isEmpty() ? 0 : indexes.get(0).getColumns().size());
        Platform.runLater(() -> {
            indexTableView.setItems(indexes);
            indexTableView.getSelectionModel().select(0, indexTableView.getColumns().get(1));
        });
    }
    
    
    public void foreignKeysTab() {
        foreignKeys = FXCollections.observableArrayList();
        updateForeignKeys();
        log.debug("foreignKeys size :{}", foreignKeys.size());
        Platform.runLater(() -> {
            foreignTableView.setItems(foreignKeys);
            foreignTableView.getSelectionModel().select(0, foreignTableView.getColumns().get(1));
        });
    }

    public void advancedTab() {
        if (selectionTable.get().getAdvanced() != null) {
            ObservableList<Rowformat> rowFormats = FXCollections.observableArrayList(Rowformat.values());
            ObservableList<Checksum> checksums = FXCollections.observableArrayList(Checksum.values());
            ObservableList<DelayKeyWrite> delayKeyWrites = FXCollections.observableArrayList(DelayKeyWrite.values());

            rowFormatComboBox.setItems(rowFormats);
            checksumComboBox.setItems(checksums);
            delayKeyWriteComboBox.setItems(delayKeyWrites);
            updateAdvancedTab(selectionTable.get());

            commentTextArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                selectionTable.get().getAdvanced().setComment(newValue);
                log.debug("chang: {}", selectionTable.get().getAdvanced().getComment());
                dataChanged();
            });
            
            autoIncrementTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                selectionTable.get().getAdvanced().setAutoIncrement(Integer.parseInt(newValue));
                dataChanged();
            });
            
            maxRowsTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                selectionTable.get().getAdvanced().setMaxRows(Integer.parseInt(newValue));
                dataChanged();
            });
            
            avgRowLengthTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                selectionTable.get().getAdvanced().setAvgRowLength(Integer.parseInt(newValue));
                dataChanged();
            });
            
            avgRowLengthTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                selectionTable.get().getAdvanced().setAvgRowLength(Integer.parseInt(newValue));
                dataChanged();
            });
            
            minRowsTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                selectionTable.get().getAdvanced().setMinRows(Integer.parseInt(newValue));
                dataChanged();
            });
            
            rowFormatComboBox.valueProperty().addListener((ObservableValue<? extends Rowformat> observable, Rowformat oldValue, Rowformat newValue) -> {
                selectionTable.get().getAdvanced().setRowformat(newValue);
                dataChanged();
            });
            
            checksumComboBox.valueProperty().addListener((ObservableValue<? extends Checksum> observable, Checksum oldValue, Checksum newValue) -> {
                selectionTable.get().getAdvanced().setChecksum(newValue);
                dataChanged();
            });
            
            delayKeyWriteComboBox.valueProperty().addListener((ObservableValue<? extends DelayKeyWrite> observable, DelayKeyWrite oldValue, DelayKeyWrite newValue) -> {
                selectionTable.get().getAdvanced().setDelayKeyWrite(newValue);
                dataChanged();
            });
        }

    }
    
    @FXML
    public void checkAll(ActionEvent event) {
        if (data != null) {
            for (SelectionColumn c: data) {
                c.selected().set(checkAllCheckBox.isSelected());
            }
        }
    }
    
    @FXML
    public void checkAllIndexes(ActionEvent event) {
        if (indexes != null) {
            for (SelectionIndex c: indexes) {
                c.selected().set(checkAllIndexesCheckBox.isSelected());
            }
        }
    }
    
    @FXML
    public void checkAllForeignKeys(ActionEvent event) {
        if (foreignKeys != null) {
            for (SelectionForeignKey fk: foreignKeys) {
                fk.selected().set(checkAllForeignKeysCheckBox.isSelected());
            }
        }
    }

    public void init(Tab rootTab, DBTable table, ConnectionSession session, boolean alterMode) {
        inited = false;
        charSetList.clear();
                
        //init all lists
        databaseList = FXCollections.observableArrayList();
            
        // tab using for renaming
        this.rootTab = rootTab;
        if (rootTab != null) {                
            rootTab.setText(alterMode ? table.getName() : "New table");
        }

        this.table = table;
        this.session = session;
        this.alterMode = alterMode;

        makeBusy(true);
        
        Thread th = new Thread(() -> {
            
            String multiQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_CHARSET);
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
            
            multiQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_DATABASES);
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
            
            SelectionTable sTable = new SelectionTable();
            sTable.copy(table);
            
            try {
                QueryResult qr = new QueryResult();
                service.execute(multiQuery, qr);

                List<String> charsets = new ArrayList<>();
                ResultSet rs = (ResultSet) qr.getResult().get(0);
                while (rs.next()) {
                    charsets.add(rs.getString(1));
                }
                rs.close();
                
                List<String> dbs = new ArrayList<>();
                rs = (ResultSet) qr.getResult().get(1);
                while (rs.next()) {
                    dbs.add(rs.getString(1));
                }
                rs.close();

                Platform.runLater(() -> {
                    charSetList.setAll(charsets);
                    charSetList.add(0, "[default]");
                    
                    databaseList.setAll(dbs);

                    selectionTable.set(sTable); 
                    indexesTab();
                    foreignKeysTab();
                    advancedTab(); 
                    
                    data.clear();
                    for (Column data1: table.getColumns()) {
                        if (data1.getName() != null) {
                            SelectionColumn col = new SelectionColumn();
                            col.copy(data1);
                            sTable.addColumn(col);
                            data.add(col);
                        }
                    }

                    SelectionColumn sc = new SelectionColumn();
                    // add last empty row
                    data.add(sc);
                    
                    selectionTable.get().getColumns().add(sc);
                    tableView.setItems(data);
                    tableView.getSelectionModel().select(0, tableView.getColumns().get(1));

                    // add 6 empt rows if we on create mode
                    if (!alterMode) {
                        addBttnAction(null);
                        addBttnAction(null);
                        addBttnAction(null);
                        addBttnAction(null);
                        addBttnAction(null);
                        addBttnAction(null);  

                        //for index tab
                        indexPlusButton(null);

                        //for foreign key
                        fkAddAction(null);            
                    }

                    //selecting in combobox
                    databaseComboBox.getSelectionModel().select(sTable.getDatabase() != null ? sTable.getDatabase().getName() : null);
                    databaseComboBox.setDisable(true);
                    engineComboBox.getSelectionModel().select(sTable.getEngineType());
                    selectionTableChanged();
                    setTableValues(selectionTable.get());

                    inited = true;

                    dataChanged();            
                    makeBusy(false);
                });
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", "Error reading character-sets", ex);
            }     
        });
        th.setDaemon(true);
        th.start();        
    }

    public synchronized void dataChanged() {
        if (inited) {
            if (service != null) {
                actualSQL = alterMode ? MysqlDBService.getChangesSQL(table, selectionTable.get()) : MysqlDBService.getNewSQL(selectionTable.get());
                actualSQL = actualSQL.trim();
            } else {
                actualSQL = "";
            }
            
            if (!actualSQL.isEmpty()) {
                
                SqlFormatter formatter = new SqlFormatter();
                
                PreferenceData pd = preferenceService.getPreference();
                if (!pd.isQueriesUsingBackquote()) {
                    actualSQL = actualSQL.replaceAll("`", "");
                }
                
                actualSQL = formatter.formatSql(actualSQL, pd.getTab(), pd.getIndentString(), pd.getStyle());
                
                MainUI.usedQueryFormat();
            }
            
            updatePreview();
        }
    }
    
    private void updatePreview() {
        
        SwingUtilities.invokeLater(() -> {
            previewArea.setText(actualSQL);
        });        
        
        SwingUtilities.invokeLater(() -> {
            builder.refreshTextArea(); 
        });        
    }

    public void selectionTableChanged() {
        selectionTable.addListener((ObservableValue<? extends SelectionTable> observable, SelectionTable oldValue, SelectionTable newValue) -> {
            setTableValues(newValue);
            dataChanged();
        });
    }

    public void setTableValues(DBTable table) {
        //collationList = FXCollections.observableArrayList(service.getCollation(table.getCharacterSet()));
        tableNameTf.setText(table.getName());
        databaseComboBox.getSelectionModel().select(table.getDatabase() != null ? table.getDatabase().getName() : null);
        
        if (table.getEngineType() == null) {
            engineComboBox.getSelectionModel().select(0);
        } else {
            engineComboBox.getSelectionModel().select(table.getEngineType());
        }
        
        if (table.getCharacterSet() == null){
            characterSetComboBox.getSelectionModel().select(0);
        } else {
            characterSetComboBox.getSelectionModel().select(table.getCharacterSet());
        }
        
//        updateCollation(table.getCharacterSet(), table.getCollation());

    }

    private void updateAdvancedTab(SelectionTable selectionTable) {
        Advanced advanced = selectionTable.getAdvanced();
        commentTextArea.setText(advanced.getComment());
        autoIncrementTextField.setText(advanced.getAutoIncrement() + "");
        avgRowLengthTextField.setText(advanced.getAvgRowLength() + "");
        maxRowsTextField.setText(advanced.getMaxRows() + "");
        minRowsTextField.setText(advanced.getMinRows() + "");
        rowFormatComboBox.getSelectionModel().select(advanced.getRowformat());
        checksumComboBox.getSelectionModel().select(advanced.getChecksum());
        delayKeyWriteComboBox.getSelectionModel().select(advanced.getDelayKeyWrite());
        autoIncrementTextField.setText(advanced.getAutoIncrement() + "");

    }

    public void updateCollation(String charset, String collation) {
        
        collationComboBox.getSelectionModel().clearSelection();
        collationList.clear();
        
        if (charset != null) {
            try {
                collationList.setAll(service.getCollation(charset));
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", "Error reading collations", ex);
            }
        }
        collationList.add(0, "[default]");
                
        if (!collationList.contains(collation)) {
            collationComboBox.getSelectionModel().select(0);
        } else {
            collationComboBox.getSelectionModel().select(collation);
        }

        selectionTable.get().setCharacterSet("[default]".equals(characterSetComboBox.getValue()) ? null : characterSetComboBox.getValue());
        Object v = engineComboBox.getValue();
        selectionTable.get().setEngineType(v instanceof EngineType ? (EngineType)v : null);

    }

    public void bind() {

        tableNameTf.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            selectionTable.get().setName(newValue);
            dataChanged();
        });
        
        engineComboBox.valueProperty().addListener((ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {            
            selectionTable.get().setEngineType(newValue instanceof EngineType ? (EngineType)newValue : null);
            dataChanged();
        });
        
        characterSetComboBox.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            
            if ("[default]".equals(newValue)) {
                newValue = null;
            }
            if (newValue != null) {
                updateCollation(newValue, table.getCollation());
            }
            selectionTable.get().setCharacterSet(newValue);
            dataChanged();
        });
        
        collationComboBox.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            System.out.println("collation: " + oldValue + " : " + newValue);
            
            String value = collationComboBox.getSelectionModel().getSelectedItem();
            if ("[default]".equals(value)) {
                value = null;
            }
            
            selectionTable.get().setCollation(value);
            dataChanged();
        });
        
        hideLanguageCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
            collationCol.setVisible(!hideLanguageCheckBox.isSelected());
            charsetCol.setVisible(!hideLanguageCheckBox.isSelected());
        });
        
        tableView.getSelectionModel().selectedIndexProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            int index = newValue == null ? -1 : newValue.intValue();
            upBttn.setDisable(index <= 0);
            downBttn.setDisable(index < 0 || index >= tableView.getItems().size() - 1);
        });
    }
    
    @FXML
    public void copyPreviewAction(ActionEvent event) {
        
        Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
        Clipboard systemClipboard = defaultToolkit.getSystemClipboard();

        // add to clipboard
        systemClipboard.setContents(new StringSelection(previewArea.getText()), null);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        webView = new WebView();
//        webView.setContextMenuEnabled(false);
//        webView.getEngine().load(ConnectionModule.class.getResource("sqlView.html").toExternalForm());
//        webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
//            @Override
//            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
//                if (newValue == Worker.State.SUCCEEDED) {
//                    updatePreview();
//                }
//            }
//        });
//        sqlPreviewTab.setContent(webView);

        tableView.getSelectionModel().setCellSelectionEnabled(true);
        indexTableView.getSelectionModel().setCellSelectionEnabled(true);
        foreignTableView.getSelectionModel().setCellSelectionEnabled(true);
                
        
        tableView.addEventHandler(KeyEvent.KEY_PRESSED, new TableViewTabMoverHandler(tableView, 1, true));
        indexTableView.addEventHandler(KeyEvent.KEY_PRESSED, new TableViewTabMoverHandler(indexTableView, 1, true));
        foreignTableView.addEventHandler(KeyEvent.KEY_PRESSED, new TableViewTabMoverHandler(foreignTableView, 1, true));
        
        copyScriptBttn.managedProperty().bind(copyScriptBttn.visibleProperty());
        
        alterTableTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            copyScriptBttn.setVisible(newValue == sqlPreviewTab);
        });
        
        builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), session);
        SwingUtilities.invokeLater(() -> {
            builder.init();
            previewArea = builder.getTextArea();
            previewArea.setEditable(false);


            Platform.runLater(() -> {
                areaPane.setCenter(builder.getSwingNode());
            });
        });
        
        checkBoxCol.setId("checkbox");
        checkBoxCol.setPrefWidth(40);
        checkBoxCol.setMinWidth(40);
        checkBoxCol.setMaxWidth(40);
        
        indexCheckBox.setId("checkbox");
        indexCheckBox.setPrefWidth(40);
        indexCheckBox.setMinWidth(40);
        indexCheckBox.setMaxWidth(40);
        
        foreignCheckBox.setId("checkbox");
        foreignCheckBox.setPrefWidth(40);
        foreignCheckBox.setMinWidth(40);
        foreignCheckBox.setMaxWidth(40);
        
        charSetList = FXCollections.observableArrayList();
        selectionTable = new SimpleObjectProperty<>();
        engineList = FXCollections.observableArrayList(MySQLEngineTypes.TYPES);
        engineList.sort(new Comparator<EngineType>() {
            private final Collator collator = Collator.getInstance();
            @Override
            public int compare(EngineType o1, EngineType o2) {
                return collator.compare(o1.getName(), o2.getName());
            }
        });
        engineList.add(0, "[default]");
        
        collationList = FXCollections.observableArrayList();
        tableView.setEditable(true);
        indexTableView.setEditable(true);
        foreignTableView.setEditable(true);
        collationCol.setVisible(false);
        charsetCol.setVisible(false);

        databaseComboBox.setItems(databaseList);
        engineComboBox.setItems(engineList);
        characterSetComboBox.setItems(charSetList);
        collationComboBox.setItems(collationList);

        dataTypeCol.setCellValueFactory(new PropertyValueFactory<>("dataType"));
        dataTypeCol.setCellFactory((TableColumn<Column, DataType> param) -> {
            TableCell<Column, DataType> cell = new TableCell<Column, DataType>() {
                private ComboBox<DataType> box;
                private HBox hbox;
                                
                @Override
                protected void updateItem(DataType item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    setStyle("-fx-padding: 0; -fx-alignment: center-left;");
                    
                    // DateType is null for new add row
                    //if (item != null) {
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        if (box == null) {
                            box = new ComboBox(FXCollections.observableArrayList(MySQLDataTypes.getCopyOfTypes()));
                            box.getEditor().setStyle("-fx-padding: 0;");
                            box.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                            box.valueProperty().addListener((ObservableValue<? extends DataType> observable, DataType oldValue, DataType newValue) -> {
                                
                                SelectionColumn col = data.get(getIndex());
                                if (!Objects.equals(col.getDataType(), newValue)) {
                                    System.out.println("change d with : " + (newValue != null ? newValue.getName() : "null"));
                                    
                                    if (newValue != null && newValue.hasLength()) {
                                        newValue.setLength(col.getDataType() != null && col.getDataType().hasLength() ?  col.getDataType().getLength() : null);
                                        newValue.setList(col.getDataType() != null && col.getDataType().hasLength() ?  col.getDataType().getList() : null);
                                    }
                                    col.setDataType(newValue);
                                    dataChanged();
                                }                                
                            });
                            
                            box.setConverter(new StringConverter<DataType>() {
                                @Override
                                public String toString(DataType object) {
                                    return object != null ? object.toString() : "";
                                }

                                @Override
                                public DataType fromString(String string) {
                                    if (string != null && !string.isEmpty()) {
                                        for (DataType dt: MySQLDataTypes.getCopyOfTypes()) {
                                            if (dt.toString().equalsIgnoreCase(string)) {
                                                return dt;
                                            }
                                        }
                                    }
                                    return null;
                                }
                            });
                            new AutoCompleteComboBoxListener<>(box);
                            
                            hbox = new HBox(box);
                            
                            box.setMinHeight(18);
                            box.setPrefHeight(18);
                            box.setMaxHeight(18);
                            
                            
                            HBox.setHgrow(box, Priority.ALWAYS);
                            hbox.setFillHeight(true);
                        }
                        
                        String name = data.get(getIndex()).getName();
                        box.setDisable(name == null || name.trim().isEmpty());
                        box.getSelectionModel().select(item);
                        setGraphic(hbox);
                    } else {
                        setGraphic(null);
                    }
                }
                
            };
            
            return cell;
        });    
        
        columnNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnNameCol.setCellFactory(new Callback<TableColumn<Column, String>, TableCell<Column, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                EditingCell cell = new EditingCell();
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        });
        columnNameCol.setOnEditCommit((CellEditEvent<Column, String> t) -> {
            
            if (t.getTablePosition() != null) {
                Column c = ((Column)t.getTableView().getItems().get(t.getTablePosition().getRow()));

                c.setName(t.getNewValue());
                if (t.getNewValue() == null || t.getNewValue().trim().isEmpty()) {
                    c.setDataType(null);
                }
                
                dataChanged();

                dataTypeCol.setVisible(false);
                dataTypeCol.setVisible(true);

                indexColumnsCol.setVisible(false);
                indexColumnsCol.setVisible(true);
                // only in last not empty
                if (data.isEmpty() || !data.get(data.size() - 1).isEmpty()) {
                    SelectionColumn sc = new SelectionColumn();
                    data.add(sc);
                    selectionTable.get().getColumns().add(sc);
                }
                
                tableView.requestFocus();
            }
        });
        
        lengthCol.setCellFactory((TableColumn<Column, DataType> param) -> {
            EnumSetAlterController esac = ((MainController)getController()).getTypedBaseController("EnumSetAlter");
            EditableDataTypeTableCell<Column, DataType> cell = new EditableDataTypeTableCell<>(AlterTableController.this, data, esac);
            cell.setEditable(true);            
            cell.setAlignment(Pos.CENTER_RIGHT);
            return cell;
        });
        
        lengthCol.setStyle("-fx-alignment: CENTER-RIGHT;");
        lengthCol.setCellValueFactory((CellDataFeatures<Column, DataType> p) -> ((SelectionColumn)p.getValue()).dataType());
        lengthCol.setOnEditCommit((TableColumn.CellEditEvent<Column, DataType> t) -> {
            Column c = ((Column)t.getTableView().getItems().get(
                    t.getTablePosition().getRow()));
            log.debug("New Data:: {}", t.getNewValue().getLength());
            c.setDataType(t.getNewValue());
            dataChanged();
            
            tableView.requestFocus();
        });
        
        defaultCol.setCellValueFactory(new PropertyValueFactory<>("defaults"));        
        defaultCol.setCellFactory(new Callback<TableColumn<Column, String>, TableCell<Column, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                EditingCell cell = new EditingCell<Column, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (getIndex() >= 0 && getIndex() < data.size()) {
                            String name = data.get(getIndex()).getName();
                            setDisable(name == null || name.trim().isEmpty());
                        }
                    }                    
                };
                
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        });
        defaultCol.setOnEditCommit((TableColumn.CellEditEvent<Column, String> t) -> {            
            Column c = ((Column)t.getTableView().getItems().get(t.getTablePosition().getRow()));            
            c.setDefaults(t.getNewValue());
            dataChanged();
            
            tableView.requestFocus();
        });
        
        notNullCol.setCellValueFactory((CellDataFeatures<Column, DataType> p) -> ((SelectionColumn)p.getValue()).dataType());
        notNullCol.setCellFactory((TableColumn<Column, DataType> p) -> {
            CheckBoxTableCells<Column, DataType> c = new CheckBoxTableCells<>(
                "NOT_NULL", 
                (Column col) -> col.isNotNull(),
                (Integer t) -> data.get(t).getDataType() == null);
            
            return c;
        });

        pkCol.setCellValueFactory((CellDataFeatures<Column, DataType> p) -> ((SelectionColumn)p.getValue()).dataType());
        pkCol.setCellFactory((TableColumn<Column, DataType> p) -> {
            CheckBoxTableCells<Column, DataType> c = new CheckBoxTableCells<Column, DataType>(
                "PRIMARY_KEY", 
                (Column col) -> col.isPrimaryKey(),
                (Integer t) -> data.get(t).getDataType() == null);        
            return c;
        });

        unsignedCol.setCellValueFactory((CellDataFeatures<Column, DataType> p) -> ((SelectionColumn)p.getValue()).dataType());
        unsignedCol.setCellFactory((TableColumn<Column, DataType> p) -> {
            CheckBoxTableCells<Column, DataType> c = new CheckBoxTableCells<>(
                "UNSIGNED", 
                (Column col) -> col.isUnsigned(),
                (Integer t) -> !canBeUnsigned(data.get(t).getDataType()));
            return c;
        });

        autoIncrCol.setCellValueFactory((CellDataFeatures<Column, DataType> p) -> ((SelectionColumn)p.getValue()).dataType());
        autoIncrCol.setCellFactory((TableColumn<Column, DataType> p) -> {
            CheckBoxTableCells<Column, DataType> c = new CheckBoxTableCells<>(
                "AUTO_INCREMENT",
                (Column col) -> col.isAutoIncrement(),
                (Integer t) -> !canBeAutoIncrement(data.get(t).getDataType()));
            return c;
        });

        zeroFillCol.setCellValueFactory((CellDataFeatures<Column, DataType> p) -> ((SelectionColumn)p.getValue()).dataType());
        zeroFillCol.setCellFactory((TableColumn<Column, DataType> p) -> {
            CheckBoxTableCells<Column, DataType> c = new CheckBoxTableCells<>(
                "ZERO_FILL", 
                (Column col) -> col.isZeroFill(),
                (Integer t) -> !canBeZeroFill(data.get(t).getDataType()));
            return c;
        });

        commentCol.setCellValueFactory(new PropertyValueFactory<>("comment"));
        commentCol.setCellFactory(new Callback<TableColumn<Column, String>, TableCell<Column, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                EditingCell cell = new EditingCell<Column, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if (getIndex() >= 0 && getIndex() < data.size()) {
                            String name = data.get(getIndex()).getName();
                            setDisable(name == null || name.trim().isEmpty());
                        }
                    }                    
                };
                
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        });
        commentCol.setOnEditCommit((TableColumn.CellEditEvent<Column, String> t) -> {            
            Column c = ((Column)t.getTableView().getItems().get(t.getTablePosition().getRow()));            
            c.setComment(t.getNewValue());
            dataChanged();
            
            tableView.requestFocus();
        });
        
        tableView.widthProperty().addListener((ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {
            
            double width = 0.0;
            for (TableColumn col: tableView.getColumns()) {
                if (col.isVisible() && col != commentCol) {
                    width += col.getWidth();
                }
            }
            
            commentCol.setPrefWidth(tableView.getWidth() - width - 10);
        });
        
        charsetCol.setCellValueFactory(cellData -> ((SelectionColumn)cellData.getValue()).characterSet());
        charsetCol.setCellFactory((TableColumn<Column, String> param) -> {
            ComboBoxTableCells<Column, String> c = new ComboBoxTableCells<>("characterSet", charSetList, true);
            c.setAlignment(Pos.CENTER_LEFT);
            return c;
        });
        charsetCol.setOnEditCommit((TableColumn.CellEditEvent<Column, String> t) -> {
            dataChanged();
        });  

        collationCol.setCellValueFactory(cellData -> ((SelectionColumn)cellData.getValue()).collation());
        collationCol.setCellFactory((TableColumn<Column, String> param) -> {
            
            ObservableList lists = FXCollections.observableArrayList();
            try {
                lists.addAll(service.getCollation(null));
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", "Error reading collations", ex);
            }
            
            ComboBoxTableCells<Column, String> c = new ComboBoxTableCells<Column, String>("collation", lists, true) {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    int rowIndex = getTableRow().getIndex();
                    if (rowIndex >= 0 && rowIndex < tableView.getItems().size()) {
                        try {
                            comboxBox.setItems(FXCollections.observableArrayList(service.getCollation(tableView.getItems().get(rowIndex).getCharacterSet())));
                        } catch (SQLException ex) {
                            DialogHelper.showError(getController(), "Error", "Error reading collations", ex);
                        }
                    }
                }                
            };

            c.setAlignment(Pos.CENTER_LEFT);
            return c;
        });
        collationCol.setOnEditCommit((TableColumn.CellEditEvent<Column, String> t) -> {
            dataChanged();
        }); 

        checkBoxCol.setCellValueFactory(cellData -> ((SelectionColumn)cellData.getValue()).selected());
        checkBoxCol.setCellFactory((TableColumn<Column, Boolean> p) -> {
            return new CheckBoxTableCells<>("selected", null, null);
        });
        
        
        Callback<TableColumn<Index, String>, TableCell<Index, String>> indexNameCellFactory = new Callback<TableColumn<Index, String>, TableCell<Index, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                EditingCell<Column, String> cell = new EditingCell<Column, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        setText(item != null ? item : "");
                    }
                    
                    
                    @Override
                    public void startEdit() {
                        
                        int rowIndex = getTableRow() != null ? getTableRow().getIndex() : -1;
                        if (rowIndex >= 0 && rowIndex < indexes.size() && indexes.get(rowIndex).getType() == IndexType.PRIMARY) {
                            return;
                        }
                        
                        super.startEdit();
                        
                        setText(getItem() != null ? getItem() : "");
                    }
                };
                
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        };
        
        indexCheckBox.setCellValueFactory(cellData -> ((SelectionIndex)cellData.getValue()).selected());
        indexCheckBox.setCellFactory((TableColumn<Index, Boolean> p) -> {
            return new IndexCheckBoxTableCells<>("selected");
        });
        
        ObservableList<IndexType> indexTypes = FXCollections.observableArrayList(IndexType.values());
        indexTypeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        indexTypeCol.setCellFactory((TableColumn<Index, IndexType> param) -> {
            TableCell<Index, IndexType> cell = new TableCell<Index, IndexType>() {
                private ComboBox<IndexType> box;
                private HBox hbox;
                
                private boolean validateNewIndex(Index index, IndexType type) {
                    
                    if (type == IndexType.PRIMARY) {
                        for (Index idx: indexes) {
                            if (idx != index && idx.getType() == IndexType.PRIMARY) {
                                return false;
                            }
                        }
                    }
                    
                    return true;
                }
           
                @Override
                protected void updateItem(IndexType item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    setStyle("-fx-padding: 0");
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        if (box == null) {
                            this.box = new ComboBox(indexTypes);
                            box.getEditor().setStyle("-fx-padding: 0;");
                            this.box.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                            setAlignment(Pos.CENTER_LEFT);

                            hbox = new HBox(box);
                            HBox.setHgrow(box, Priority.ALWAYS);
                            hbox.setFillHeight(true);

                            box.setConverter(new StringConverter<IndexType>() {
                                @Override
                                public String toString(IndexType object) {
                                    return object != null ? object.getDisplayValue() : "";
                                }

                                @Override
                                public IndexType fromString(String string) {
                                    if (string != null && !string.isEmpty()) {
                                        for (IndexType l: indexTypes) {
                                            if (l.getDisplayValue().equalsIgnoreCase(string)) {
                                                return (IndexType) l;
                                            }
                                        }
                                    }
                                    return null;
                                }
                            });

                            new AutoCompleteComboBoxListener<>(box);
                            
                            box.valueProperty().addListener((ObservableValue<? extends IndexType> observable, IndexType oldValue, IndexType newValue) -> {
                                
                                Index realIndex = indexes.get(getIndex());
                                
                                if (!validateNewIndex(realIndex, newValue)) {
                                    realIndex.setType(null);
                                    Platform.runLater(() -> {
                                        box.getSelectionModel().clearSelection();
                                        DialogHelper.showInfo(getController(), "Info", "Multiple primary keys cannot be defined", null, getStage());
                                    });
                                    
                                    return;
                                }
                                
                                realIndex.setType(newValue);
                                
                                boolean needChangeColumns = false;
                                boolean changeValue = false;
                                if (newValue != null && newValue.getDisplayValue().toUpperCase().equals("PRIMARY")) {
                                    realIndex.setName("PRIMARY");
                                    needChangeColumns = true;
                                    changeValue = true;
                                } else if (oldValue != null && oldValue.getDisplayValue().toUpperCase().equals("PRIMARY")){
                                    realIndex.setName("PRIMARY".equals(realIndex.getName()) ? "" : realIndex.getName());
                                    needChangeColumns = true;
                                    changeValue = false;
                                }   
                                
                                if (needChangeColumns) {
                                    if (realIndex.getColumns() != null) {
                                        for (Column c: realIndex.getColumns()) {
                                            for (Column column : data) {
                                                if (column.getName() != null && !column.getName().trim().isEmpty() && column.getName().equals(c.getName())) {
                                                    column.setPrimaryKey(changeValue);
                                                    // only if true
                                                    if (changeValue) {
                                                        column.setNotNull(changeValue);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    pkCol.setVisible(false);
                                    pkCol.setVisible(true);
                                }
                                
                                dataChanged();
                            });
                        }
                        
                        box.getSelectionModel().select(item);
                        box.setMinHeight(18);
                        box.setPrefHeight(18);
                        box.setMaxHeight(18);
                        
                        setGraphic(hbox);
                        
                    } else {
                        setGraphic(null);
                    }
                    
                }
                
            };
            
            return cell;
        });

        indexNameCol.setCellValueFactory(cellData -> ((SelectionIndex)cellData.getValue()).name());
        indexNameCol.setCellFactory(indexNameCellFactory);
        indexNameCol.setOnEditCommit((CellEditEvent<Index, String> t) -> {
            
            Index c = ((Index)t.getTableView().getItems().get(t.getTablePosition().getRow()));            
            c.setName(t.getNewValue());
            
            dataChanged();
        });
        
        indexColumnsCol.setCellFactory((TableColumn<Index, String> param) -> {
            TableCell<Index, String> cell = new TableCell<Index, String>() {
                private Label label;
                
                private Button button;
                
                private HBox hbox;
                
                public void updateLabel(Index index) {
                    List<Column> columns = index.getColumns();
                    String s = "";
                    for (int c = 0; c < columns.size(); c++) {
                        String name = columns.get(c).getName();
                        int length = index.getColumnLength(name);
                        s += name + (length > 0 ? "(" + length + ")" : "");
                        if (c != columns.size() - 1) {
                            s += ", ";
                        }
                    }
                    label.setText(s);
                }
                
                
                @Override
                protected void updateItem(String item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    //if (item != null) {
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        
                        if (button == null) {
                            button = new Button("...");
                            button.setMinSize(18, 18);
                            button.setPrefSize(18, 18);
                            button.setMaxSize(18, 18);
                            
                            button.setPadding(Insets.EMPTY);
                            button.setAlignment(Pos.CENTER);
                            
                            button.setOnAction((ActionEvent event) -> {
                                
                                ColumnDialogBoxController columnDialogBoxController = ((MainController)getController()).getTypedBaseController("ColumnDialogBox");
                                
                                Stage stage = new Stage();
                                Scene scene1 = new Scene(columnDialogBoxController.getUI());
                                
                                List<SelectionColumn> selectedColumns = new ArrayList<>();
                                
                                Map<Column, Column> indexToTable = new HashMap<>();
                                
                                String[] columns = label.getText().split(",");
                                for (int i = 0; i < columns.length; i++) {
                                    String c = columns[i];
                                    if (c.contains("(")) {
                                        columns[i] = c.substring(0, c.indexOf("("));
                                    }
                                }
                                
                                for (Column column : data) {
                                    if (column.getName() != null && !column.getName().trim().isEmpty()) {
                                        SelectionColumn selectionColumn = new SelectionColumn();
                                        selectionColumn.copy(column);

                                        indexToTable.put(selectionColumn, column);

                                        for (String columnName: columns) {
                                            
                                            if (columnName.trim().equals(column.getName())) {
                                                selectionColumn.selected().set(true);
                                                break;
                                            } else {
                                                selectionColumn.selected().set(false);
                                            }
                                        }
                                        selectedColumns.add(selectionColumn);
                                    }
                                }   
                                
//                                int i = 0;
//                                for (SelectionColumn s: selectedColumns) {
//                                    if (s.selected().get()) {
//                                        i++;
//                                    }
//                                }  

                                Index index1 = indexes.get(getIndex());
                                
                                Index index2 = new Index();
                                index2.copy(index1);
                                
                                columnDialogBoxController.init(index2, selectedColumns);
                                stage.initStyle(StageStyle.UTILITY);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(getStage());
                                stage.setTitle("Columns");
                                stage.setScene(scene1);
                                stage.showAndWait();
                                
                                if (columnDialogBoxController.getResponse()) {
                                    log.debug("{}", columnDialogBoxController.selectedColumns());
                                    
                                    index1.setColumns(columnDialogBoxController.selectedColumns());
                                    if (index2.getColumnLengthMap() != null) {
                                        index1.setColumnLengthMap(new HashMap<>(index2.getColumnLengthMap()));
                                    }
                                    
                                    // need update columns in data
                                    // need check for primary key changes
                                    if (index1.getType() == IndexType.PRIMARY) {
                                        for (Column indexColumn: indexToTable.keySet()) {
                                            
                                            Column tableColumn = indexToTable.get(indexColumn);
                                            if (index1.getColumns().contains(indexColumn)) {
                                                tableColumn.setPrimaryKey(true);
                                                tableColumn.setNotNull(true);
                                            } else {
                                                tableColumn.setPrimaryKey(false);
                                            }
                                        }
                                    }
                                    
                                    pkCol.setVisible(false);
                                    pkCol.setVisible(true);
                                                                        
                                    updateLabel(index1);
                                }
                                
                                dataChanged();
                            });
                        }
                        if (label == null) {
                            label = new Label();
                            label.setAlignment(Pos.CENTER_LEFT);
                        }
                        if (hbox == null) {
                            hbox = new HBox();
                            hbox.setAlignment(Pos.CENTER_LEFT);
                            hbox.getChildren().addAll(label, button);
                        }
                        label.setMaxWidth(Double.MAX_VALUE);
                        label.setText(item != null ? item : "");
                        HBox.setHgrow(label, Priority.ALWAYS);
                        setGraphic(hbox);
                        
                    } else {
                        setGraphic(null);
                        setText("");
                    }
                    
                }
                
            };
            
            return cell;
        });
        
        indexColumnsCol.setCellValueFactory((CellDataFeatures<Index, String> param) -> {
            String s = "";
            if (param.getValue().getColumns() != null) {
                List<Column> columns = param.getValue().getColumns();
                for (int c = 0; c < columns.size(); c++) {
                    
                    String name = columns.get(c).getName();
                    int length = param.getValue().getColumnLength(name);
                    s += name + (length > 0 ? "(" + length + ")" : "");
                    
                    if (c != columns.size() - 1) {
                        s += ",";
                    }
                }
            }
            System.out.println("columns :" + s);
            SimpleObjectProperty obj = new SimpleObjectProperty<>();
            
            obj.set(s);
            
            return obj;
        });
        
        Callback<TableColumn<ForeignKey, String>, TableCell<ForeignKey, String>> fkNameCellFactory = new Callback<TableColumn<ForeignKey, String>, TableCell<ForeignKey, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                EditingCell cell = new EditingCell();
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        };
        
        foreignCheckBox.setCellValueFactory(cellData -> ((SelectionForeignKey)cellData.getValue()).selected());
        foreignCheckBox.setCellFactory((TableColumn<ForeignKey, Boolean> p) -> {
            return new ForeignKeyCheckBoxTableCells<>("selected");
        });
        
        foreignConstraintNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        foreignConstraintNameCol.setCellFactory(fkNameCellFactory);
        foreignConstraintNameCol.setOnEditCommit((CellEditEvent<ForeignKey, String> t) -> {
            
            ForeignKey c = ((ForeignKey)t.getTableView().getItems().get(t.getTablePosition().getRow()));            
            c.setName(t.getNewValue());
            
            dataChanged();
        });
        
        foreignReferencingCol.setCellFactory((TableColumn<ForeignKey, String> param) -> {
            TableCell<ForeignKey, String> cell = new TableCell<ForeignKey, String>() {
                private Label label;                
                private Button button;                
                private HBox hbox;
                
                public void updateLabel(ForeignKey fk) {
                    List<Column> columns = fk.getColumns();
                    String s = "";
                    for (int c = 0; c < columns.size(); c++) {
                        s += columns.get(c).getName();
                        if (c != columns.size() - 1) {
                            s += ", ";
                        }
                    }
                    label.setText(s);
                }                
                
                @Override
                protected void updateItem(String item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        
                        if (button == null) {
                            button = new Button("...");
                            button.setAlignment(Pos.CENTER_RIGHT);
                            button.setMinSize(18, 18);
                            button.setPrefSize(18, 18);
                            button.setMaxSize(18, 18);
                            
                            button.setPadding(Insets.EMPTY);
                            button.setAlignment(Pos.CENTER);
                            
                            button.setOnAction((ActionEvent event) -> {
                                
                                ColumnDialogBoxController columnDialogBoxController = ((MainController)getController()).getTypedBaseController("ColumnDialogBox");
                                
                                Stage stage = new Stage();
                                Scene scene1 = new Scene(columnDialogBoxController.getUI());
                                
                                List<SelectionColumn> selectedColumns = new ArrayList<>();
                                
                                String[] columns = label.getText().split(",");
                                
                                for (Column column : data) {
                                    if (column.getName() != null && !column.getName().trim().isEmpty()) {
                                        SelectionColumn selectionColumn = new SelectionColumn();
                                        selectionColumn.copy(column);

                                        for (String columnName : columns) {
                                            if (columnName.trim().equals(column.getName())) {
                                                selectionColumn.selected().set(true);
                                                break;
                                            } else {
                                                selectionColumn.selected().set(false);
                                            }
                                        }
                                        selectedColumns.add(selectionColumn);
                                    }
                                }
                                
                                columnDialogBoxController.init(null, selectedColumns);
                                stage.initStyle(StageStyle.UTILITY);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(getStage());
                                stage.setTitle("Columns");
                                stage.setScene(scene1);
                                stage.showAndWait();
                                
                                if (columnDialogBoxController.getResponse()) {
                                    log.debug("{}", columnDialogBoxController.selectedColumns());
                                    
                                    ForeignKey fk1 = foreignKeys.get(getIndex());
                                    fk1.setColumns(columnDialogBoxController.selectedColumns());                          
                                    
                                    updateLabel(fk1);
                                }
                                
                                dataChanged();
                            });
                        }
                        
                        if (label == null) {
                            label = new Label();
                        }
                        
                        if (hbox == null) {
                            hbox = new HBox();
                            hbox.getChildren().addAll(label, button);
                        }
                        
                        label.setMaxWidth(Double.MAX_VALUE);
                        label.setText(item != null ? item : "");
                        HBox.setHgrow(label, Priority.ALWAYS);
                        setGraphic(hbox);
                        
                    } else {
                        setGraphic(null);
                        setText("");
                    }
                    
                }
                
            };
            
            cell.setAlignment(Pos.CENTER_LEFT);
            return cell;
        });
        
        foreignReferencingCol.setCellValueFactory((CellDataFeatures<ForeignKey, String> param) -> {
            String s = "";
            if (param.getValue().getColumns() != null) {
                List<Column> columns = param.getValue().getColumns();
                for (int c = 0; c < columns.size(); c++) {
                    s += columns.get(c).getName();
                    if (c != columns.size() - 1) {
                        s += ",";
                    }
                }
            }
            
            return new SimpleObjectProperty<>(s);
        });
        
        foreignReferencedDbCol.setCellValueFactory(new PropertyValueFactory<>("referenceDatabase"));
        foreignReferencedDbCol.setCellFactory((TableColumn<ForeignKey, String> param) -> {
            return new ComboBoxTableCells<>("referenceDatabase", FXCollections.observableArrayList(databaseList), false);
        });
        foreignReferencedDbCol.setOnEditCommit((CellEditEvent<ForeignKey, String> t) -> {
            dataChanged();
        });
        
        foreignReferencedTableCol.setCellValueFactory(cellData -> ((SelectionForeignKey)cellData.getValue()).referencedTable());
        foreignReferencedTableCol.setCellFactory((TableColumn<ForeignKey, String> param) -> {
            return new ComboBoxTableCells<ForeignKey, String>("referenceTable", FXCollections.observableArrayList(), false) {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    
                    if (item == null && getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        int rowIndex = getTableRow().getIndex();                    
                        if (rowIndex >= 0 && rowIndex < foreignTableView.getItems().size()) {
                            List<String> tables = new ArrayList<>();
                            
                            String database = foreignTableView.getItems().get(rowIndex).getReferenceDatabase();
                            if (database != null && !database.isEmpty()) {
                                List<DBTable> listTables = DatabaseCache.getInstance().getTables(session.getConnectionParam().cacheKey(), database);
                                for (DBTable t: listTables) {
                                    tables.add(t.getName());
                                }
                            }
                                
                            comboxBox.getItems().clear();
                            if (tables != null) {
                                comboxBox.getItems().addAll(tables);
                            }
                        }
                    }
                }                
            };
        });
        foreignReferencedTableCol.setOnEditCommit((CellEditEvent<ForeignKey, String> t) -> {
            dataChanged();
        });
        
        foreignReferencedCol.setCellFactory((TableColumn<ForeignKey, String> param) -> {
            TableCell<ForeignKey, String> cell = new TableCell<ForeignKey, String>() {
                private Label label;                
                private Button button;                
                private HBox hbox;
                
                @Override
                protected void updateItem(String item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                        
                        if (button == null) {
                            button = new Button("...");
                            button.setMinSize(18, 18);
                            button.setPrefSize(18, 18);
                            button.setMaxSize(18, 18);
                            
                            button.setPadding(Insets.EMPTY);
                            button.setAlignment(Pos.CENTER);
                            button.setOnAction((ActionEvent event) -> {
                                
                                ColumnDialogBoxController columnDialogBoxController = ((MainController)getController()).getTypedBaseController("ColumnDialogBox");
                                
                                Stage stage = new Stage();
                                Scene scene1 = new Scene(columnDialogBoxController.getUI());
                                
                                List<SelectionColumn> selectedColumns = new ArrayList<>();
                                                                
                                String[] columns = label.getText().split(",");
                                
                                ForeignKey fk =  getTableView().getItems().get(getTableRow().getIndex());
                                if (fk.getReferenceDatabase() != null && !fk.getReferenceDatabase().isEmpty() && 
                                        fk.getReferenceTable() != null && !fk.getReferenceTable().isEmpty()) {
                                    try {
                                        List<Column> columnsSet = service.getColumnInformation(fk.getReferenceDatabase(), fk.getReferenceTable());                                
                                        for (Column column : columnsSet) {
                                            SelectionColumn selectionColumn = new SelectionColumn();
                                            selectionColumn.copy(column);

                                            for (String columnName : columns) {
                                                if (columnName.trim().equals(column.getName())) {
                                                    selectionColumn.selected().set(true);
                                                    break;
                                                } else {
                                                    selectionColumn.selected().set(false);
                                                }
                                            }
                                            selectedColumns.add(selectionColumn);
                                        }   
                                    } catch (SQLException ex) {
                                        DialogHelper.showError(getController(), "Error", "Error reading column information", ex);
                                    }
                                }
                                
                                columnDialogBoxController.init(null, selectedColumns);
                                stage.initStyle(StageStyle.UTILITY);
                                stage.initModality(Modality.WINDOW_MODAL);
                                stage.initOwner(getStage());
                                stage.setTitle("Columns");
                                stage.setScene(scene1);
                                stage.showAndWait();
                                
                                if (columnDialogBoxController.getResponse()) {
                                    log.debug("{}", columnDialogBoxController.selectedColumns());
                                    
                                    ForeignKey fk1 = foreignKeys.get(getIndex());
                                    fk1.setReferenceColumns(columnDialogBoxController.selectedColumns());                          
                                    
                                    label.setText(((SelectionForeignKey)fk1).referenceColumns().get());
                                }
                                
                                dataChanged();
                            });
                        }
                        
                        if (label == null) {
                            label = new Label();
                        }
                        
                        if (hbox == null) {
                            hbox = new HBox();
                            hbox.getChildren().addAll(label, button);
                        }
                        
                        label.setMaxWidth(Double.MAX_VALUE);
                        label.setText(item != null ? item : "");
                        HBox.setHgrow(label, Priority.ALWAYS);
                        setGraphic(hbox);
                        
                    } else {
                        setGraphic(null);
                        setText("");
                    }
                    
                }
                
            };
            
            cell.setAlignment(Pos.CENTER_LEFT);
            return cell;
        });
        
        foreignReferencedCol.setCellValueFactory(cellData -> ((SelectionForeignKey)cellData.getValue()).referenceColumns());
        foreignReferencedCol.setOnEditCommit((CellEditEvent<ForeignKey, String> t) -> {
            dataChanged();
        });
        
        foreignOnUpdateCol.setCellValueFactory(new PropertyValueFactory<>("onUpdate"));
        foreignOnUpdateCol.setCellFactory((TableColumn<ForeignKey, String> param) -> {
            return new ComboBoxTableCells<>("onUpdate", FXCollections.observableArrayList(foreignKeyRules), false);
        });
        foreignOnUpdateCol.setOnEditCommit((CellEditEvent<ForeignKey, String> t) -> {
            dataChanged();
        });
        
        foreignOnDeleteCol.setCellValueFactory(new PropertyValueFactory<>("onDelete"));
        foreignOnDeleteCol.setCellFactory((TableColumn<ForeignKey, String> param) -> {
            return new ComboBoxTableCells<>("onDelete", FXCollections.observableArrayList(foreignKeyRules), false);
        });
        foreignOnDeleteCol.setOnEditCommit((CellEditEvent<ForeignKey, String> t) -> {
            dataChanged();
        });
        
        
        getController().getStage().getScene().addMnemonic(new Mnemonic(addBttn, new KeyCodeCombination(KeyCode.INSERT, KeyCombination.ALT_DOWN)));
        getController().getStage().getScene().addMnemonic(new Mnemonic(subtractBttn, new KeyCodeCombination(KeyCode.DELETE, KeyCombination.ALT_DOWN)));
        
        getController().getStage().getScene().addMnemonic(new Mnemonic(upBttn, new KeyCodeCombination(KeyCode.UP, KeyCombination.ALT_DOWN)));
        getController().getStage().getScene().addMnemonic(new Mnemonic(downBttn, new KeyCodeCombination(KeyCode.DOWN, KeyCombination.ALT_DOWN)));
        
        getController().getStage().getScene().addMnemonic(new Mnemonic(addIndex, new KeyCodeCombination(KeyCode.INSERT, KeyCombination.ALT_DOWN)));
        getController().getStage().getScene().addMnemonic(new Mnemonic(removeIndex, new KeyCodeCombination(KeyCode.DELETE, KeyCombination.ALT_DOWN)));
        
        getController().getStage().getScene().addMnemonic(new Mnemonic(addFk, new KeyCodeCombination(KeyCode.INSERT, KeyCombination.ALT_DOWN)));
        getController().getStage().getScene().addMnemonic(new Mnemonic(removeFk, new KeyCodeCombination(KeyCode.DELETE, KeyCombination.ALT_DOWN)));
        
        //getController().getStage().getScene().addMnemonic(new Mnemonic(copyPreview, new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY)));
        
        bind();
    }

    @FXML
    void revertBttnAction(ActionEvent event) {
        init(rootTab, table, session, alterMode);
    }

    @FXML
    void saveBttnAction(ActionEvent event) {
        // check table name
        if (selectionTable.get().getName() == null || selectionTable.get().getName().isEmpty()) {
            DialogHelper.showError(getController(), "Error", "Table name not specified", null);
            return;
        }
        
        // check at least one columns
        boolean notEmpty = false;
        for (Column col: selectionTable.get().getColumns()) {            
            if (col.getName() != null && !col.getName().trim().isEmpty()) {
                notEmpty = true;
                break;
            }
        }
        
        
        boolean enumEmpty = false;
        boolean setEmpty = false;
        for (Column col: selectionTable.get().getColumns()) {                        
            if (col.getDataType() != null && col.getDataType().isEnum() && (col.getDataType().getList() == null || col.getDataType().getList().isEmpty())) {
                enumEmpty = true;
                break;
            }
            
            if (col.getDataType() != null && col.getDataType().isSet() && (col.getDataType().getList() == null || col.getDataType().getList().isEmpty())) {
                setEmpty = true;
                break;
            }
        }
        
        if (!notEmpty) {
            DialogHelper.showError(getController(), "Error", "There are no columns in this table. Define at least one column", null);
            return;
        }
        
        if (enumEmpty) {
            DialogHelper.showError(getController(), "Error", "Length must be specified for enum data type", null);
            return;
        }
        
        if (setEmpty) {
            DialogHelper.showError(getController(), "Error", "Length must be specified for set data type", null);
            return;
        }
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {

            try {
                String actualSQL = alterMode ? MysqlDBService.getChangesSQL(table, selectionTable.get()) : MysqlDBService.getNewSQL(selectionTable.get());
                actualSQL = actualSQL.trim();

                if (actualSQL.endsWith(",")) {
                    actualSQL = actualSQL.substring(0, actualSQL.length() - 1);
                }

                String[] sqls = actualSQL.split(";");

                System.out.println("acutal sql :" + actualSQL);

                for (String sql : sqls) {
                    System.out.println("running sql :" + sql);
                    if (!sql.trim().isEmpty()) {
                        int result = (int)service.execute(sql + ";");
                        if (result == -1) {
                            throw new Exception("Invalid query");
                        }
                        log.debug("altertable result :" + result);
                    }
                }

                boolean needAlter = true;
                if (alterMode) {            
                    DialogHelper.showInfo(getController(), "Alter Table", "Table altered successfully.", null);

                } else {                
                    DialogResponce responce = DialogHelper.showConfirm(getController(), "Create Table", "Table created successfully.\n\nDo you want to create more tables?");
                    needAlter = responce != DialogResponce.OK_YES;
                }

                DatabaseCache.getInstance().updateDBTable(selectionTable.get().getDatabase().getName(), selectionTable.get(), session.getConnectionParam().cacheKey());
//                DBTable alteredTable = DatabaseCache.getInstance().syncDBTable(selectionTable.get().getDatabase().getName(), selectionTable.get().getName(), session.getConnectionParam(), (AbstractDBService) service, true);
                
                DBTable alteredTable = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), selectionTable.get().getDatabase().getName(), selectionTable.get().getName());
                LeftTreeHelper.tableAltered(((MainController)getController()).getConnectionTabContent(), ((ConnectionTabContentController)getController().getTabControllerBySession(session)).getTableTreeItem(table), alteredTable);

                if (needAlter) {
                    Platform.runLater(() -> {
                        // remove not used data
                        List<Index> removeIndexes = new ArrayList<>();
                        if (alteredTable.getIndexes() != null) {
                            for (Index idx: alteredTable.getIndexes()) {
                                if (idx.getName() == null) {
                                    removeIndexes.add(idx);
                                }
                            }
                        }
                        
                        List<ForeignKey> removeFks = new ArrayList<>();
                        if (alteredTable.getForeignKeys() != null) {
                            for (ForeignKey fk: alteredTable.getForeignKeys()) {
                                if (fk.getName() == null) {
                                    removeFks.add(fk);
                                }
                            }
                        }
                        
                        List<Column> removeColumns = new ArrayList<>();
                        if (alteredTable.getColumns() != null) {
                            for (Column c: alteredTable.getColumns()) {
                                if (c.getName() == null) {
                                    removeColumns.add(c);
                                }
                            }
                        }
                        
                        if (!removeIndexes.isEmpty()) {
                            alteredTable.getIndexes().removeAll(removeIndexes);
                        }
                        
                        if (!removeFks.isEmpty()) {
                            alteredTable.getForeignKeys().removeAll(removeFks);
                        }
                        
                        if (!removeColumns.isEmpty()) {
                            alteredTable.getColumns().removeAll(removeColumns);
                        }
                        
                        init(rootTab, alteredTable, session, true);
                    });
                } else {
                    Platform.runLater(() -> {
                        DBTable t = new DBTable("", null, null, null, new ArrayList<>(), selectionTable.get().getDatabase(), null, null, new ArrayList<>(), new ArrayList<>());

                        init(rootTab, t, session, false);
                    });
                }
                
            } catch (Throwable e) {            
                DialogHelper.showError(getController(), "Error" + (alterMode ? "Alter Table" : "Create Table"), String.format("Table '%s' cannot be altered", selectionTable.get().getName()), e);
                makeBusy(false);
            }

            // Its low performance action
//            controller.refreshTree(session.getId(), false);
        });
        
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    void fkAddAction(ActionEvent event) {
        SelectionForeignKey fk = new SelectionForeignKey();
        if (selectionTable.get() != null) {
            fk.setReferenceDatabase(selectionTable.get().getDatabase().getName());
        }
        foreignKeys.add(fk);
        selectionTable.get().getForeignKeys().add(fk);
        
        Platform.runLater(() -> {
            foreignTableView.getSelectionModel().select(foreignKeys.size() - 1, foreignTableView.getColumns().get(1));
            foreignTableView.requestFocus(); 
        });
    }
    
    @FXML
    void fkRemoveAction(ActionEvent event) {
        List<SelectionForeignKey> selectedFks = new ArrayList<>();
        for (SelectionForeignKey c: foreignKeys) {
            if (c.selected().get()) {
                selectedFks.add(c);
            }
        }
            
        int index = foreignTableView.getSelectionModel().getSelectedIndex();
            
        
        boolean canRemove = false;
        if (!selectedFks.isEmpty()) {
            
            DialogResponce responce = DialogHelper.showConfirm(getController(), "Droping foreign keys", 
                "Do you want to drop the selected foreign key(s)?");
            
            if (responce == DialogResponce.OK_YES) {
                canRemove = true;
            }
            
        } else {
            canRemove = true;                
            selectedFks.add(foreignKeys.get(index));
        }

        if (canRemove) {
            for (SelectionForeignKey fk: selectedFks) {                
                foreignKeys.remove(fk);
                selectionTable.get().getForeignKeys().remove(fk);
            }
        }
        
        dataChanged();
    }
    
    private boolean canBeUnsigned(DataType type) {
        if (type != null) {
            switch (type.getName()) {
                case "BIGINT":
                case "DECIMAL":
                case "DOUBLE":
                case "INT":
                case "MEDIUMINT":
                case "SMALLINT":
                case "TINYINT":
                    return true;
            }
        }
        
        return false;
    }
    
    private boolean canBeAutoIncrement(DataType type) {
        if (type != null) {
            switch (type.getName()) {
                case "BIGINT":
                case "BOOL":
                case "BOOLEAN":
                case "DOUBLE":
                case "FLOAT":
                case "INT":
                case "MEDIUMINT":
                case "REAL":
                case "SMALLINT":
                case "TINYINT":
                    return true;
            }
        }
        
        return false;
    }
    
    private boolean canBeZeroFill(DataType type) {
        if (type != null) {
            switch (type.getName()) {
                case "BIGINT":
                case "BOOL":
                case "BOOLEAN":
                case "DECIMAL":
                case "DOUBLE":
                case "FLOAT":
                case "INT":
                case "MEDIUMINT":
                case "NUMERIC":
                case "REAL":
                case "SMALLINT":
                case "TINYINT":
                    return true;
            }
        }
        
        return false;
    }
    
    
    @FXML
    void addBttnAction(ActionEvent event) {
        SelectionColumn column = new SelectionColumn(null, table, null, null, false, false, false, false, false, false,
                  null,
                  null, null);
        
        int selectedIndex = tableView.getSelectionModel().getSelectedIndex();
        selectedIndex++;
        
        column.setOrderIndex(selectedIndex);
        data.add(selectedIndex, column);
        for (int i = selectedIndex + 1; i < data.size(); i++) {
            data.get(i).setOrderIndex(i);
        }
        
        selectionTable.get().getColumns().add(column);
        
        int finalIndex = selectedIndex;
        Platform.runLater(() -> {
            tableView.getSelectionModel().select(finalIndex, tableView.getColumns().get(1));
            tableView.requestFocus(); 
        });
    }

    @FXML
    void subtractBttnAction(ActionEvent event) {
        if (data != null) {
            
            List<SelectionColumn> selectedColumns = new ArrayList<>();
            for (SelectionColumn c: data) {
                if (c.selected().get()) {
                    selectedColumns.add(c);
                }
            }
            
            int index = tableView.getSelectionModel().getSelectedIndex();
            
            
                    
            boolean canRemove = false;
            if (!selectedColumns.isEmpty()) {                
                DialogResponce responce = DialogHelper.showConfirm(getController(), "Droping columns", 
                    "Do you want drop selected column(s)?");
                
                if (responce == DialogResponce.OK_YES) {
                    canRemove = true;
                }
            } else {
                canRemove = true;                
                selectedColumns.add(data.get(index));
            }
            
            if (canRemove) {
                removeColumns(selectedColumns, index);
            }
            
            dataChanged();
        }        
    }

    private void removeColumns(List<SelectionColumn> selectedColumns, int index) {
        boolean needUpdateIndexes = false;
        boolean needRemoveIndexes = false;
        for (SelectionColumn c: selectedColumns) {

            if (indexes != null) {
                int j = 0;
                while (j < indexes.size()) {

                    Index ix = indexes.get(j);
                    if (ix.getColumns() != null) {

                        int i = 0;
                        while (i < ix.getColumns().size()) {
                            Column ic = ix.getColumns().get(i);
                            if (ic == c || ic.getName().equals(c.getName())) {
                                if (!needRemoveIndexes) {
                                    
                                    DialogResponce responce = DialogHelper.showConfirm(getController(), "Droping columns", 
                                        "The selected columns are referenced by one or more indexes.\n"
                                            + "Dropping these columns will also modify the corresponding\n"
                                            + "indexes.\n\nDo you want to continue?");
                                    
                                    if (responce == DialogResponce.OK_YES) {
                                        needRemoveIndexes = true;
                                    }
                                }

                                if (needRemoveIndexes) {
                                    ix.getColumns().remove(ic);
                                    if (ix.getColumns().isEmpty()) {
                                        indexes.remove(ix);
                                        selectionTable.get().getIndexes().remove(ix);
                                        j--;
                                    }

                                    needUpdateIndexes = true;
                                    continue;
                                } else {
                                    return;
                                }
                            }                                        
                            i++;
                        }
                    }
                    j++;
                }
            }
            data.remove(c);
            selectionTable.get().getColumns().remove(c);
        }
        
        if (needUpdateIndexes) {
            indexColumnsCol.setVisible(false);
            indexColumnsCol.setVisible(true);
        }
    }
    
    @FXML
    void changeOrder(ActionEvent event) {
        boolean up = event.getSource() == upBttn;
        int index = tableView.getSelectionModel().getSelectedIndex();
        if (up && index > 0) {
            SelectionColumn upIndex = data.get(index - 1);
            SelectionColumn currentIndex = data.get(index);
            data.remove(currentIndex);
            data.remove(upIndex);
            data.add(index - 1, currentIndex);
            currentIndex.setOrderIndex(index - 1);
            currentIndex.setUpdateIndex(index - 1);

            data.add(index, upIndex);

            upIndex.setOrderIndex(index);
            tableView.getSelectionModel().select(index - 1);

        } else if (!up && index >= 0 && index + 1 < data.size()) {

            SelectionColumn downIndex = data.get(index + 1);
            SelectionColumn currentIndex = data.get(index);
            data.remove(currentIndex);
            data.remove(downIndex);
            data.add(index, downIndex);
            downIndex.setOrderIndex(index);
            data.add(index + 1, currentIndex);
            currentIndex.setOrderIndex(index + 1);
            currentIndex.setUpdateIndex(index + 1);

            tableView.getSelectionModel().select(index + 1);

        }
        
        dataChanged();
    }

    @FXML
    void indexPlusButton(ActionEvent event) {
        SelectionIndex index = new SelectionIndex();
        
        indexes.add(index);
        selectionTable.get().getIndexes().add(index);
        
        Platform.runLater(() -> {
            indexTableView.getSelectionModel().select(indexes.size() - 1, indexTableView.getColumns().get(1));
            indexTableView.requestFocus(); 
        });
    }

    @FXML
    void indexMinusButton(ActionEvent event) {
        
        List<SelectionIndex> selectedIndexes = new ArrayList<>();
        for (SelectionIndex c: indexes) {
            if (c.selected().get()) {
                selectedIndexes.add(c);
            }
        }
            
        int index = indexTableView.getSelectionModel().getSelectedIndex();
            
        boolean canRemove = false;
        if (!selectedIndexes.isEmpty()) {
            
            DialogResponce responce = DialogHelper.showConfirm(getController(), "Droping indexes", 
                "Do you want drop selected index(s)?");
            
            if (responce == DialogResponce.OK_YES) {
                canRemove = true;
            }
        } else {
            canRemove = true;                
            selectedIndexes.add(indexes.get(index));
        }

        if (canRemove) {
            boolean pkColumnChanged = false;
            for (SelectionIndex idx: selectedIndexes) {                
                indexes.remove(idx);
                selectionTable.get().getIndexes().remove(idx);

                // if index primary need deselect columns
                if (idx.getType() == IndexType.PRIMARY) {
                    for (Column c: idx.getColumns()) {
                        for (Column column: data) {
                            if (c == column || 
                                c.getName() != null && c.getName().equals(column.getName())) {
                                column.setPrimaryKey(false);
                                break;
                            }
                        }
                    }
                }
                
                pkColumnChanged = true;
            }
            
            if (pkColumnChanged) {
                pkCol.setVisible(false);
                pkCol.setVisible(true);
            }
        }
        
        dataChanged();
    }


    public void setName(String name) {
        tableNameTf.setText(name);
    }

    public TableView<SelectionColumn> getTableView() {
        return tableView;
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;

    }

    class CheckBoxTableCells<S, T> extends TableCell<S, T> {

        CheckBox checkBox;

        String attributeName;
        Function<Integer, Boolean> disabledFunction;
        Function<Column, Boolean> valueFunction;

        public CheckBoxTableCells(String attributeName, Function<Column, Boolean> valueFunction, Function<Integer, Boolean> disabledFunction) {
            this.attributeName = attributeName;
            this.disabledFunction = disabledFunction;
            this.valueFunction = valueFunction;
        }

        @Override
        public void updateItem(T item, boolean empty) {

            super.updateItem(item, empty);
            
            if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setFocusTraversable(false);
                    this.checkBox.setAlignment(Pos.CENTER);
                    setAlignment(Pos.CENTER);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) -> {
                        
                        //changed column to selectioncolumn
                        Column col = data.get(getIndex());
                        switch (attributeName) {
                            case "PRIMARY_KEY":
                                if (!Objects.equals(col.isPrimaryKey(), newValue)) {
                                    System.out.println("chaklsjf;sldjnvsldf");
                                    col.setPrimaryKey(newValue);
                                    if (newValue) {
                                        col.setNotNull(true);
                                        notNullCol.setVisible(false);
                                        notNullCol.setVisible(true);

                                        // add to primary key index
                                        Index primaryIdx = findPimaryKey();
                                        if (primaryIdx == null) {
                                            // need create primary index
                                            primaryIdx = new SelectionIndex();
                                            primaryIdx.setName(IndexType.PRIMARY.name());
                                            primaryIdx.setType(IndexType.PRIMARY);

                                            indexes.add(0, (SelectionIndex)primaryIdx);
                                            selectionTable.get().getIndexes().add(0, (SelectionIndex)primaryIdx);
                                        }

                                        List<Column> columns = primaryIdx.getColumns();
                                        if (columns == null) {
                                            columns = new ArrayList<>();
                                            primaryIdx.setColumns(columns);
                                        }

                                        boolean contains = false;
                                        for (Column c: columns) {
                                            if (c == col || 
                                                c.getName() != null && c.getName().equals(col.getName())) {

                                                contains = true;
                                                break;
                                            }
                                        }

                                        if (!contains) {
                                            columns.add(col);
                                        }
                                    } else if (oldValue != null && oldValue){
                                        // if was primary key - need remove column
                                        Index primaryIdx = findPimaryKey();
                                        if (primaryIdx != null) {
                                            if (primaryIdx.getColumns() != null) {

                                                for (Column c: primaryIdx.getColumns()) {
                                                    if (c == col || 
                                                        c.getName() != null && c.getName().equals(col.getName())) {
                                                        primaryIdx.getColumns().remove(c);
                                                        break;
                                                    }
                                                }
                                            }

                                            // remove empty index
                                            if (primaryIdx.getColumns() == null || primaryIdx.getColumns().isEmpty()) {
                                                indexes.remove(primaryIdx);
                                                selectionTable.get().getIndexes().remove(primaryIdx);
                                            }
                                        }
                                    }

                                    indexColumnsCol.setVisible(false);
                                    indexColumnsCol.setVisible(true);
                                                          
                                    dataChanged();
                                }
                                break;
                                
                            case "NOT_NULL":
                                if (!Objects.equals(col.isNotNull(), newValue)) {
                                    System.out.println("notnull");
                                    if (!col.isPrimaryKey()) {
                                        col.setNotNull(newValue);                                    
                                        dataChanged();
                                    } else {
                                        checkBox.setSelected(true);
                                    }
                                }
                                break;
                                
                            case "UNSIGNED":
                                if (!Objects.equals(col.isUnsigned(), newValue)) {
                                    System.out.println("unsigned");
                                    col.setUnsigned(newValue);                                    
                                    dataChanged();
                                }
                                break;
                                
                            case "AUTO_INCREMENT":
                                if (!Objects.equals(col.isAutoIncrement(), newValue)) {
                                    System.out.println("auto increment");
                                    col.setAutoIncrement(newValue);
                                }
                                break;
                                
                            case "ZERO_FILL":
                                if (!Objects.equals(col.isZeroFill(), newValue)) {
                                    System.out.println("zerofill");
                                    col.setZeroFill(newValue);                                    
                                    dataChanged();
                                }
                                break;
                                
                            case "ON_UPDATE":
                                if (!Objects.equals(col.isOnUpdate(), newValue)) {
                                    System.out.println("onupdate");
                                    col.setOnUpdate(newValue);                                    
                                    dataChanged();
                                }
                                break;
                                
                            case "selected":
                                if (!Objects.equals(((SelectionColumn)col).selected().get(), newValue)) {
                                    ((SelectionColumn)col).selected().set(newValue);
                                }
                                break;
                        }
                    });
                }

                if (valueFunction != null) {
                    checkBox.setSelected(valueFunction.apply(data.get(getIndex())));
                } else {
                    checkBox.setSelected(item != null ? (Boolean)item : Boolean.FALSE);
                }
                
                setGraphic(checkBox);
                
                if (disabledFunction != null ) {
                    boolean disabled = disabledFunction.apply(getIndex());
                    setDisable(disabled);
                    
                    if (disabled && checkBox != null) {
                        checkBox.setSelected(false);
                    }
                }
                
            } else {
                setGraphic(null);
            }
        }

    }
    
    
    private Index findPimaryKey() {
        for (Index idx: indexes) {
            if (idx.getType() == IndexType.PRIMARY) {
                return idx;
            }
        }
        
        return null;
    }
    
    class ComboBoxTableCells<S, T> extends TableCell<S, T> {

        ComboBox<T> comboxBox;
        HBox hbox;

        String attributeName;
        ObservableList list;
        private boolean dataType;
        
        public ComboBoxTableCells(String attributeName, ObservableList list, boolean dataType) {
            this.attributeName = attributeName;
            this.list = list;
            this.dataType = dataType;
            setStyle("-fx-padding: 0;");
        }

        @Override
        public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            
            if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                
                if (dataType) {
                    DataType dt = data.get(getIndex()).getDataType();
                    setDisable(dt == null);
                }
                                
                if (comboxBox == null) {
                    this.comboxBox = new ComboBox(list);
                    this.comboxBox.getEditor().setStyle("-fx-padding: 0;");
                    this.comboxBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                    setAlignment(Pos.CENTER_LEFT);
                    
                    hbox = new HBox(comboxBox);
                    
                    HBox.setHgrow(comboxBox, Priority.ALWAYS);
                    hbox.setFillHeight(true);
        
                    comboxBox.setConverter(new StringConverter<T>() {
                        @Override
                        public String toString(T object) {
                            return object != null ? object.toString() : "";
                        }

                        @Override
                        public T fromString(String string) {
                            if (string != null && !string.isEmpty()) {
                                for (Object l: list) {
                                    if (l.toString().equalsIgnoreCase(string)) {
                                        return (T) l;
                                    }
                                }
                            }
                            return null;
                        }
                    });
                    
                    new AutoCompleteComboBoxListener<>(comboxBox);
                    
                    comboxBox.valueProperty().addListener((ObservableValue<? extends T> ov, T oldValue, T newValue) -> {
                        
                        //changed column to selectioncolumn
                        Column col = null;
                        ForeignKey fk = null;
                        switch (attributeName) {
                            case "characterSet":
                                col = data.get(getIndex());
                                if (!Objects.equals(col.getCharacterSet(), newValue)) {
                                    System.out.println("characterSet");
                                    col.setCharacterSet((String) newValue);
                                }
                                return;
                                
                            case "collation":
                                col = data.get(getIndex());
                                if (!Objects.equals(col.getCollation(), newValue)) {
                                    System.out.println("collation");
                                    col.setCollation((String) newValue);
                                }
                                break; 
                                
                            case "referenceDatabase":
                                fk = foreignKeys.get(getIndex());
                                if (!Objects.equals(fk.getReferenceDatabase(), newValue)) {
                                    System.out.println("referenceDatabase");
                                    fk.setReferenceDatabase((String) newValue);
                                    ((SelectionForeignKey)fk).referencedTable().set("");
                                    ((SelectionForeignKey)fk).referencedTable().set(null);
                                }
                                break;
                                
                            case "referenceTable":
                                fk = foreignKeys.get(getIndex());
                                if (!Objects.equals(fk.getReferenceTable(), newValue)) {
                                    System.out.println("referenceTable");
                                    ((SelectionForeignKey)fk).referencedTable().set((String) newValue);
                                }
                                break;
                                
                            case "onUpdate":
                                fk = foreignKeys.get(getIndex());
                                if (!Objects.equals(fk.getOnUpdate(), newValue)) {
                                    System.out.println("onUpdate");
                                    fk.setOnUpdate((String) newValue);
                                }
                                break;
                                
                            case "onDelete":
                                fk = foreignKeys.get(getIndex());
                                if (!Objects.equals(fk.getOnDelete(), newValue)) {
                                    System.out.println("onDelete");
                                    fk.setOnDelete((String) newValue);
                                }
                                break;
                        }
                        
                        dataChanged();
                    });
                }

                comboxBox.getSelectionModel().select(item);
                comboxBox.setMinHeight(18);
                comboxBox.setPrefHeight(18);
                comboxBox.setMaxHeight(18);
                
                setGraphic(hbox);                
            } else {
                setGraphic(null);
            }
        }
    }
    
    
    class IndexCheckBoxTableCells<S, T> extends TableCell<S, T> {

        CheckBox checkBox;
        String attributeName;

        public IndexCheckBoxTableCells(String attributeName) {
            this.attributeName = attributeName;
        }

        @Override
        public void updateItem(T item, boolean empty) {

            super.updateItem(item, empty);
            //if (item != null) {
            if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setFocusTraversable(false);
                    this.checkBox.setAlignment(Pos.CENTER);
                    setAlignment(Pos.CENTER);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
                        
                        //changed column to selectioncolumn
                        SelectionIndex index = indexes.get(getIndex());
                        switch (attributeName) {
                            case "selected":
                                index.selected().set(newVal);
                                break;
                        }
                    });
                }

                checkBox.setSelected(item != null ? (Boolean)item : Boolean.FALSE);
                setGraphic(checkBox);                
            } else {
                setGraphic(null);
            }
        }

    }
    
    class ForeignKeyCheckBoxTableCells<S, T> extends TableCell<S, T> {

        CheckBox checkBox;
        String attributeName;

        public ForeignKeyCheckBoxTableCells(String attributeName) {
            this.attributeName = attributeName;
        }

        @Override
        public void updateItem(T item, boolean empty) {

            super.updateItem(item, empty);
            //if (item != null) {
            if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
                if (checkBox == null) {
                    this.checkBox = new CheckBox();
                    this.checkBox.setFocusTraversable(false);
                    this.checkBox.setAlignment(Pos.CENTER);
                    setAlignment(Pos.CENTER);
                    
                    checkBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) -> {
                        
                        //changed column to selectioncolumn
                        SelectionForeignKey fk = foreignKeys.get(getIndex());
                        switch (attributeName) {
                            case "selected":
                                fk.selected().set(newVal);
                                break;
                        }
                    });
                }

                checkBox.setSelected(item != null ? (Boolean)item : Boolean.FALSE);
                setGraphic(checkBox);                
            } else {
                setGraphic(null);
            }
        }

    }
    
    public String getCharSetOfCollation(String collation) {
        for (String s : charSetList) {
            if (collation.contains(s)) {
                return s;
            }
        }
        return null;
    }

}
