/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.RunFrequency;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.utils.DumpUtils;
import np.com.ngopal.smart.sql.db.ScheduleParamsService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ScheduleParams;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.components.DatabaseSelectableTreeView;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @TODO Maybe need make many small wizard panes
 * 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ScheduledBackupWizardController extends AbstractScheduledWizardController<ScheduleParams> {
 
     
    @Inject
    ScheduleParamsService scheduleParamService;
    
    @FXML
    private AnchorPane welcomePane;
    @FXML
    private AnchorPane connectionPane;
    @FXML
    private AnchorPane selectObjectsPane;
    @FXML
    private AnchorPane filesPane;
    @FXML
    private AnchorPane exportOptionsPane;
    @FXML
    private AnchorPane backupOptionsPane;
    @FXML
    private AnchorPane errorHandlingPane;
    @FXML
    private AnchorPane runPane;
    @FXML
    private AnchorPane donePane;    
    @FXML
    private AnchorPane emailPane;  
    
    @FXML
    private RadioButton exportAllObjects;
    @FXML
    private DatabaseSelectableTreeView dataTree;
    
    @FXML
    private Label databaseNeedRefreshLabel;
    
    @FXML
    private CheckBox compressZip;
    @FXML
    private RadioButton singleFileForAll;
    @FXML
    private RadioButton separateFile;
    @FXML
    private Label filesFileName;
    @FXML
    private TextField filesFileField;
    @FXML
    private Button filesFileButton;
    @FXML
    private Label filesFolderName;
    @FXML
    private TextField filesFolderField;
    @FXML
    private Button filesFolderButton;
    @FXML
    private Label filesFolderNote;
    @FXML
    private CheckBox filesSubFolder;
    @FXML
    private Label filesSubFolderNote;
    @FXML
    private CheckBox filesPrefixTimestamp;
    @FXML
    private RadioButton filesPrefixTimestampOverride;
    @FXML
    private RadioButton filesPrefixTimestampAppend;
    
    @FXML
    private RadioButton structureAndData;
    @FXML
    private RadioButton structureOnly;
    @FXML
    private CheckBox includeCreateDatabase;
    @FXML
    private CheckBox includeUseDatabase;
    @FXML
    private CheckBox includeDrop;
    @FXML
    private CheckBox ignoreDefiner;
    @FXML
    private CheckBox generateBulInsert;
    @FXML
    private CheckBox oneRowPerLine;
    @FXML
    private CheckBox generateCompleteInsert;
    @FXML
    private CheckBox generateDelayedInsert;
    
    @FXML
    private CheckBox flushLogs;
    @FXML
    private CheckBox flushMasterLog;
    @FXML
    private CheckBox flushSlaveLog;
    @FXML
    private CheckBox lockTablesForRead;
    @FXML
    private CheckBox singleTransaction;
    @FXML
    private CheckBox autocommit;
    @FXML
    private CheckBox foreignKeysChecks;
    @FXML
    private CheckBox lockArroundInsert;
    @FXML
    private CheckBox keysArroundInsert;
    @FXML
    private CheckBox includeVersionInformation;
    @FXML
    private ComboBox charsetComboBox;
    @FXML
    private CheckBox serverDefaultSize;
    @FXML
    private TextField customSize;
    @FXML
    private CheckBox chunksBreak;
    @FXML
    private TextField chunkSize;
    
    @FXML
    private CheckBox abortOnError;
    @FXML
    private CheckBox emailNotification;
    @FXML
    private RadioButton onlyError;
    @FXML
    private RadioButton everyTime;
    
    @FXML
    private ImageView refreshImageView;
    @FXML
    private ImageView cancelImageView;
    @FXML
    private ImageView scheduleImageView;

    @Override
    ScheduleParams fromString(String string) {
        for (ScheduleParams sp: connectionsComboBox.getItems()) {
            if (sp.getName().equals(string)) {
                return sp;
            }
        }
        
        return null;
    }
   
    
    @Override
    public void init(DBService dbService, ConnectionParams connectionParam, Database database) throws SQLException {
        super.init(dbService, connectionParam, database);
        
        // prepare charset
        List<String> list = dbService.getCharSet();
        charsetComboBox.setItems(list != null ? FXCollections.observableArrayList(list) : FXCollections.emptyObservableList());
        charsetComboBox.setPromptText("default");
    }

    
    @Override
    List<ScheduleParams> loadSchedulers() {
        List<ScheduleParams> scheduleParamsList = scheduleParamService.getAll();        
        return scheduleParamsList != null ? scheduleParamsList : new ArrayList<>();
    }
    
    @Override
    void deleteSchedule(ScheduleParams schedule) {
        scheduleParamService.delete(schedule);
    }

    @Override
    public void databaseChanged(Database database) {
        dataTree.setDisable(database.getTables() == null || database.getTables().isEmpty());
        dataTree.loadData(dbService, database, false, schedule != null ? schedule.getSelectedData() : null);
    }
    
    
    @Override
    boolean checkNextAction() {
        switch (currentWizardIndex) {                
            // need check if selected saved jobs - than job must be selected
            case 0:                
                if (!newJobRadioButton.isSelected() && savedJobsComboBox.getSelectionModel().getSelectedItem() == null) {
                    ((MainController)getController()).showInfo("Info", "Please select saved job!", null);
                    return false;
                } else {
                    if (newJobRadioButton.isSelected() && jobNameField.getText().trim().isEmpty()) {
                        ((MainController)getController()).showInfo("Info", "Please set new job name!", null);
                        return false;
                    }
                    schedule = newJobRadioButton.isSelected() ? null : savedJobsComboBox.getSelectionModel().getSelectedItem();
                    if (schedule != null) {
                        updateAllWithScheduleParams();
                    }                        
                }
                break;

            // need check params is ok
            case 1: 
                makeBusy(true);
                Thread th = new Thread(() -> {
                   try {
                        ConnectionParams cp = schedule != null ? schedule.toConnectionParams() : new ConnectionParams();
                        cp.setMultiQueries(true);
                        
                        cp.setAddress(address.getText());
                        cp.setUsername(user.getText());
                        cp.setPassword(pass.getText());

                        try {
                            cp.setPort(Integer.parseInt(port.getText()));
                        } catch (NumberFormatException ex) {
                            backAction(null);
                            DialogHelper.showError(getController(), "Error", "Error parsing port", ex, getStage());
                            makeBusy(false);
                            return;
                        }

                        try {
                            cp.setSessionIdleTimeout(Integer.parseInt(sessionIdleTimeoutValue.getText()));
                        } catch (NumberFormatException ex) {
                            backAction(null);
                            DialogHelper.showError(getController(), "Error", "Error parsing idle timeout", ex, getStage());
                            makeBusy(false);
                            return;
                        }

                        cp.setSshHost(sshHost.getText());
                        cp.setSshPassword(sshPassword.getText());
                        cp.setSshUser(sshUser.getText());
                        cp.setUseCompressedProtocol(useCompressedProtocol.isSelected());
                        
                        dbService.connect(cp);
                        List<Database> databases = dbService.getDatabases();
                        if (databases != null) {
                        
                            for (Database database: databases) {
                                Database db = DatabaseCache.getInstance().getDatabase(cp.cacheKey(), database.getName());
                                databaseComboBox.getItems().add(db != null ? db : database);       
                            }
                        
                            Database selectedDatabase = null;
                            String initDb = initDatabase != null ? initDatabase.getName() : null;
                            if (schedule != null) {
                                initDb = schedule.getDatabase();
                            }
                            
                            if (initDb != null) {
                                for (Database db: databases) {
                                    if (db.getName().equals(initDb)) {
                                        selectedDatabase = db;
                                    }
                                }
                            }

                            if (selectedDatabase != null) {
                                Map<String, String> selectedData = schedule != null ? schedule.getSelectedData() : null;

                                Database staticDatabase = selectedDatabase;
                                Platform.runLater(() -> {
                                    exportAllObjects.setSelected(selectedData == null);
                                    databaseComboBox.getSelectionModel().select(staticDatabase);   
                                    makeBusy(false);
                                });
                            } else {
                                makeBusy(false);
                            }
                        }
                    } catch (SQLException ex) {
                        backAction(null);
                        DialogHelper.showInfo(getController(), "Info", "Connection is wrong!", ex.getMessage());
                        makeBusy(false);
                    } 
                });
                th.setDaemon(true);
                th.start();
                break;

            // need check selected data
            case 2: 
                if (!exportAllObjects.isSelected() && dataTree.getSelectedData().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please select at least one object!", null);
                    return false;
                }
                break;

            // need check folders or file
            case 3: 
                if (singleFileForAll.isSelected() && filesFileField.getText().trim().isEmpty() || !singleFileForAll.isSelected() && filesFolderField.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please select file or directory!", null);
                    return false;
                }
                break;

            case 6:
                if (!emailNotification.isSelected()) {
                    currentWizardIndex++;
                }
                break;                    

            case 7:
                if (emailFromName.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter 'From name'", null);
                    return false;
                }

                if (emailFromEmail.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter 'From email' address", null);
                    return false;
                }

                if (emailToEmail.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter 'To email' address", null);
                    return false;
                }

                if (emailHost.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter SMPT 'Host' server address", null);
                    return false;
                }

                if (emailPort.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter SMPT 'Port' number", null);
                    return false;
                }

                if (!emailPort.getText().matches("\\d+")) {
                    ((MainController)getController()).showError("Error", "Please enter valid 'Port' number", null);
                    return false;
                }

                if (emailAuth.isSelected()) {
                    if (emailAccountName.getText().trim().isEmpty()) {
                        ((MainController)getController()).showError("Error", "Please enter 'Account name'", null);
                        return false;
                    }

                    if (emailPasswd.getText().trim().isEmpty()) {
                        ((MainController)getController()).showError("Error", "Please enter 'Password'", null);
                        return false;
                    }
                }


                break;

            case 8:
                if (logFile.getText().trim().isEmpty() || logFile.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please select log file!", null);
                    return false;
                }

                if (useWindowsSchedule.isSelected()) {
                    if (runFrequency.getSelectionModel().getSelectedItem() == null) {
                        ((MainController)getController()).showError("Error", "Please select schedule frequence!", null);
                        return false; 
                    } else if (runFrequency.getSelectionModel().getSelectedItem() == RunFrequency.WEEKLY && getWeeklyString() == null) {
                        ((MainController)getController()).showError("Error", "Please select at least 1 week day!", null);
                        return false; 
                    }
                }
                
                doFinish();
                break;
        }
        
        return true;
    }
    
    @Override
    void checkBackAction() {
        if (currentWizardIndex == 8 && !emailNotification.isSelected())
        {
            currentWizardIndex--;
        }
    }
    
    @Override
    boolean isNeedSendEmail() {
        return emailNotification.isSelected();
    }
        
    @Override
    void fillWizardPanes(List<AnchorPane> wizardPanes) {
        wizardPanes.add(welcomePane);
        wizardPanes.add(connectionPane);
        wizardPanes.add(selectObjectsPane);
        wizardPanes.add(filesPane);
        wizardPanes.add(exportOptionsPane);
        wizardPanes.add(backupOptionsPane);
        wizardPanes.add(errorHandlingPane);
        wizardPanes.add(emailPane);
        wizardPanes.add(runPane);
        wizardPanes.add(donePane);
    }
    
    @Override
    ScheduleParams createSchedule() {
        return new ScheduleParams();
    }
    @Override
    ScheduleParams createScheduleFromConnectionParams(ConnectionParams params) {
        return new ScheduleParams(params);
    }
    
    
    @Override
    void updateAllComponentsUsingSchedule(ScheduleParams schedule) {
                
        lockArroundInsert.setSelected(schedule.isAddLockArroundInsert());
        generateBulInsert.setSelected(schedule.isCreateBulkInsert());
        structureAndData.setSelected(!schedule.isExportOnlyStructure());
        structureOnly.setSelected(schedule.isExportOnlyStructure());
        flushLogs.setSelected(schedule.isFlushLogs());
        ignoreDefiner.setSelected(schedule.isIgnoreDefiner());
        includeCreateDatabase.setSelected(schedule.isIncludeCreateDatabase());
        includeDrop.setSelected(schedule.isIncludeDrop());
        includeUseDatabase.setSelected(schedule.isIncludeUseDatabase());
        lockTablesForRead.setSelected(schedule.isLockTables());
        oneRowPerLine.setSelected(schedule.isOneRowPerLine());
        filesPrefixTimestamp.setSelected(schedule.isPrefixWithTimestamp());
        foreignKeysChecks.setSelected(schedule.isSetForeignKeyChecks());
        singleTransaction.setSelected(schedule.isSingleTransaction());        
        compressZip.setSelected(schedule.isZiping());
        filesSubFolder.setSelected(schedule.isSubFolderWithTimestamp());
        filesPrefixTimestampOverride.setSelected(schedule.isFilesPrefixTimestampOverride());
        generateCompleteInsert.setSelected(schedule.isCreateCompleteInsert());
        generateDelayedInsert.setSelected(schedule.isCreateDelayedInsert());
        flushMasterLog.setSelected(schedule.isFlushMasterLog());
        flushSlaveLog.setSelected(schedule.isFlushSlaveLog());
        autocommit.setSelected(schedule.isAutocommit());
        if (schedule.getCharset() != null) {
            charsetComboBox.getSelectionModel().select(schedule.getCharset());
        }
        keysArroundInsert.setSelected(schedule.isKeysArroundInsert());
        includeVersionInformation.setSelected(schedule.isIncludeVersionInformation());        
        serverDefaultSize.setSelected(schedule.isServerDefaultSize());                            
        abortOnError.setSelected(schedule.isAbortOnError());
                
        singleFileForAll.setSelected(schedule.isSingleFileForAll());
        separateFile.setSelected(!schedule.isSingleFileForAll());
        if (singleFileForAll.isSelected()) {
            filesFileField.setText(schedule.getFilesFileField());
        } else {
            filesFolderField.setText(schedule.getFilesFileField());
        }
        
        // email part
        if (isNeedSendEmail()) {
            emailNotification.setSelected(true);
            
            if (schedule.isOnlyError()) {
                onlyError.setSelected(true);
            } else {
                everyTime.setSelected(true);
            }
        }        
        makeBusy(true);
        Thread th = new Thread(() -> {
           try {
                dbService.connect(schedule.toConnectionParams());
                List<Database> databases = dbService.getDatabases();
                if (databases != null) {

                    Database selectedDatabase = null;
                    for (Database db: databases) {
                        if (db.getName().equals(schedule.getDatabase())) {
                            selectedDatabase = db;
                        }
                    }

                    if (selectedDatabase != null) {
                        Map<String, String> selectedData = schedule.getSelectedData();
                            
                        Database staticDatabase = selectedDatabase;
                        Platform.runLater(() -> {
                            exportAllObjects.setSelected(selectedData == null);
                            databaseComboBox.getSelectionModel().select(staticDatabase);   
                            makeBusy(false);
                        });
                    } else {
                        makeBusy(false);
                    }
                }
            } catch (SQLException ex) {
                ((MainController)getController()).showError("Error", "Error loading databases", ex);
                makeBusy(false);
            } 
        });
        th.setDaemon(true);
        th.start();
    }
    
    @Override
    void fillScheduleCustom() {
                
        schedule.setAddLockArroundInsert(lockArroundInsert.isSelected());
        schedule.setCreateBulkInsert(generateBulInsert.isSelected());
        schedule.setExportOnlyData(false);
        schedule.setExportOnlyStructure(!structureAndData.isSelected());
        schedule.setFlushLogs(flushLogs.isSelected());
        schedule.setIgnoreDefiner(ignoreDefiner.isSelected());
        schedule.setIncludeCreateDatabase(includeCreateDatabase.isSelected());
        schedule.setIncludeDrop(includeDrop.isSelected());
        schedule.setIncludeUseDatabase(includeUseDatabase.isSelected());
        schedule.setLockTables(lockTablesForRead.isSelected());
        schedule.setOneRowPerLine(oneRowPerLine.isSelected());
        schedule.setPrefixWithTimestamp(filesPrefixTimestamp.isSelected());
        schedule.setSetForeignKeyChecks(foreignKeysChecks.isSelected());
        schedule.setSingleTransaction(singleTransaction.isSelected());        
        schedule.setZiping(compressZip.isSelected());
        schedule.setSubFolderWithTimestamp(filesSubFolder.isSelected());
        schedule.setFilesPrefixTimestampOverride(filesPrefixTimestampOverride.isSelected());
        schedule.setCreateCompleteInsert(generateCompleteInsert.isSelected());
        schedule.setCreateDelayedInsert(generateDelayedInsert.isSelected());
        schedule.setFlushMasterLog(flushMasterLog.isSelected());
        schedule.setFlushSlaveLog(flushSlaveLog.isSelected());
        schedule.setAutocommit(autocommit.isSelected());
        schedule.setCharset((String) charsetComboBox.getSelectionModel().getSelectedItem());
        schedule.setKeysArroundInsert(keysArroundInsert.isSelected());
        schedule.setIncludeVersionInformation(includeVersionInformation.isSelected());        
        schedule.setServerDefaultSize(serverDefaultSize.isSelected());                            
        schedule.setAbortOnError(abortOnError.isSelected());
        
        schedule.setOnlyError(onlyError.isSelected());        
        
        schedule.setSingleFileForAll(singleFileForAll.isSelected());
        schedule.setFilesFileField(singleFileForAll.isSelected() ? filesFileField.getText() : filesFolderField.getText());        
     
        schedule.setSelectedData(exportAllObjects.isSelected() ? dataTree.getDataAsString(): dataTree.getSelectedDataAsString());
     
        // save
        scheduleParamService.save(schedule);
    }
    
    @Override
    void finishAction() {
        // check export per object option
        if (!singleFileForAll.isSelected()) {
            
            // create if need selected dir
            new File(schedule.getFilesFileField()).mkdirs();
            
            DumpUtils.dumpPerObjectDatabase(dbService, schedule, new DumpUtils.DumpStatusCallback() {
                @Override
                public void error(String errorMessage, Throwable th) {
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", errorMessage, th);

                        if (schedule.isNotificationEmail()) {

                            StringWriter sw = new StringWriter();
                            sw.append(errorMessage).append("\n\n");
                            th.printStackTrace(new PrintWriter(sw));

                            sendEmail(schedule, "Bakup failed - " + schedule.getName() + " at " +  DATETIME_FORMATER.format(new Date()), sw.toString());
                        }
                    });
                }

                @Override
                public void success() {
                    if (schedule.isNotificationEmail() && !schedule.isOnlyError()) {
                        sendEmail(schedule, "Bakup completed - " + schedule.getName() + " at " +  DATETIME_FORMATER.format(new Date()), "");
                    }
                }

                @Override
                public void progress(int progress, String objectLevel, String currentDumpedObject) { }
            });
        } else {
            DumpUtils.dumpAllDatabase(dbService, schedule, new DumpUtils.DumpStatusCallback() {
                @Override
                public void error(String errorMessage, Throwable th) {
                    Platform.runLater(() -> {
                        ((MainController)getController()).showError("Error", errorMessage, th);

                        if (schedule.isNotificationEmail()) {

                            StringWriter sw = new StringWriter();
                            sw.append(errorMessage).append("\n\n");
                            th.printStackTrace(new PrintWriter(sw));

                            sendEmail(schedule, "Bakup failed - " + schedule.getName() + " at " +  DATETIME_FORMATER.format(new Date()), sw.toString().replaceAll("\n", "<br>"));
                        }
                    });
                }

                @Override
                public void success() {
                    if (schedule.isNotificationEmail() && !schedule.isOnlyError()) {
                        sendEmail(schedule, "Bakup completed - " + schedule.getName() + " at " +  DATETIME_FORMATER.format(new Date()), "");
                    }
                }

                @Override
                public void progress(int progress, String objectLevel, String currentDumpedObject) { }
                
            });
        }
    }
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {        
        super.initialize(location, resources);
        
        refreshImageView.setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        cancelImageView.setImage(SmartImageProvider.getInstance().getCancelExecute_image());
        scheduleImageView.setImage(SmartImageProvider.getInstance().getScheduleBackUp_image());
        
//        makeBusy(true);
//                Thread th = new Thread(() -> {
//                    Database database = databaseComboBox.getSelectionModel().getSelectedItem();
//                    if (database != null && schedule != null) {
//                        try {
//                            database = DatabaseCache.getInstance().syncDatabase(database.getName(), schedule.toConnectionParams(), (AbstractDBService) dbService);
//                        } catch (SQLException ex) {
//                            log.error("Error", ex);
//                            database = null;
//                        }
//
//                        if (database != null) {
//                            try {
//                                database.setEvents(dbService.getEvents(database));
//                            } catch (SQLException ex) {
//                            }
//
//                            try {
//                                database.setFunctions(dbService.getFunctions(database));
//                            } catch (SQLException ex) {
//                            }
//
//                            try {
//                                database.setStoredProc(dbService.getStoredProcs(database));
//                            } catch (SQLException ex) {
//                            }
//
//                            try {
//                                database.setTriggers(dbService.getTriggers(database));
//                            } catch (SQLException ex) {
//                            }
//
//                            try {
//                                database.setViews(dbService.getViews(database));
//                            } catch (SQLException ex) {
//                            }
//                        }
//                    }
//
//                    Database staticDB = database;
//                    Platform.runLater(() -> {
//                        dataTree.loadData(dbService, staticDB, false, schedule != null ? schedule.getSelectedData() : null);
//                        makeBusy(false);
//                    });
//                });
//                th.setDaemon(true);
//                th.start();
    }
   
    
    @Override
    public void propertyListeners() {
        
        super.propertyListeners();
        
        exportAllObjects.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            dataTree.setDisable(newValue);
        });
        
        compressZip.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            filesSubFolder.setDisable(newValue);
            filesSubFolderNote.setDisable(newValue);
            filesPrefixTimestampOverride.setDisable(newValue || filesSubFolder.isSelected());
            filesPrefixTimestampAppend.setDisable(newValue || filesSubFolder.isSelected());
            
            filesFileName.setText(newValue ? "Archive name" : "File name");
            filesFolderName.setText(newValue ? "Archive name" : "Folder name");
            filesPrefixTimestamp.setText(newValue ? "Prefix Archive name with timestamp" : "Prefix SQL filenames with timestamp");
            
            filesPrefixTimestamp.setDisable(newValue ? false : filesSubFolder.isSelected());
            
            String folderName = filesFolderField.getText().trim();
            String fileName = filesFileField.getText().trim();
            
            if (newValue) {
                
                if (!folderName.isEmpty() && !folderName.endsWith(".zip")) {
                    filesFolderField.setText(folderName + ".zip");
                }
                
                if (!fileName.isEmpty() && fileName.endsWith(".sql")) {
                    filesFileField.setText(fileName.replace(".sql", ".zip"));
                }
            } else {
                
                if (!folderName.isEmpty() && folderName.endsWith(".zip")) {
                    filesFolderField.setText(folderName.replace(".zip", ""));
                }
                
                if (!fileName.isEmpty() && fileName.endsWith(".zip")) {
                    filesFileField.setText(fileName.replace(".zip", ".sql"));
                }
            }
        });
        
        singleFileForAll.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            filesFolderField.setDisable(newValue);
            filesFolderButton.setDisable(newValue);
            filesFolderNote.setDisable(newValue);
            filesSubFolder.setDisable(newValue || compressZip.isSelected());
            filesSubFolderNote.setDisable(newValue || compressZip.isSelected());
            
            filesFileField.setDisable(!newValue);
            filesFileButton.setDisable(!newValue);            
        });
        
        filesSubFolder.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            filesPrefixTimestamp.setDisable(newValue);
            filesPrefixTimestampOverride.setDisable(newValue);
            filesPrefixTimestampAppend.setDisable(newValue);
            filesPrefixTimestampOverride.setSelected(true);
        });
        
        filesPrefixTimestamp.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (compressZip.isSelected()) {
                filesPrefixTimestampOverride.setSelected(true);
            }
        });
        
        structureAndData.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            generateBulInsert.setDisable(!newValue);
            oneRowPerLine.setDisable(!newValue || !generateBulInsert.isSelected());
            generateCompleteInsert.setDisable(!newValue);
            generateDelayedInsert.setDisable(!newValue);
        });
        
        generateBulInsert.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            oneRowPerLine.setDisable(!newValue);
        });
        
        serverDefaultSize.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            customSize.setDisable(newValue);
            customSize.setText(newValue ? "" : "0");
        });
        
        chunksBreak.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            chunkSize.setDisable(newValue);
            chunkSize.setText(newValue ? "" : "1000");
        });
        
        emailNotification.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            onlyError.setDisable(!newValue);
            everyTime.setDisable(!newValue);
        });
    }
    
    @FXML
    public void selectSingleFileAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        
        if (compressZip.isSelected()) {
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("ZIP Files", "*.zip"));
        } else {
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("SQL files", "*.sql"),
                new FileChooser.ExtensionFilter("All files", "*.*"));
        }
        
        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            filesFileField.setText(file.getAbsolutePath());
        }
    }
    
    @FXML
    public void selectSeparateFileAction(ActionEvent event) {
        if (compressZip.isSelected()) {
            
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("ZIP Files", "*.zip"));
            
            File file = fileChooser.showSaveDialog(getStage());
            if (file != null) {
                filesFolderField.setText(file.getAbsolutePath());
            }
            
        } else {
            
            DirectoryChooser dirChooser = new DirectoryChooser();
            
            File dir = dirChooser.showDialog(getStage());
            if (dir != null) {
                filesFolderField.setText(dir.getAbsolutePath());
            }
        }
    }    
}
