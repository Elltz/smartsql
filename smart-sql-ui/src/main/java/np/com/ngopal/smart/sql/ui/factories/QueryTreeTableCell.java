/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.factories;

import java.util.function.BiConsumer;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TreeTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QueryTreeTableCell extends TreeTableCell<AnalyzerResult, String> {

    private final int maxLength;
    private final BiConsumer<Node, String> actionFunction;
    
    public QueryTreeTableCell(int maxLength, BiConsumer<Node, String> actionFunction) {
        this.maxLength = maxLength;
        this.actionFunction = actionFunction;
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty); 

        int length = item != null ? item.length(): 0;
        if (length > maxLength) {
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

            Label label = new Label(item);
            label.setMaxWidth(Double.MAX_VALUE);

            Button b = new Button("" + length);
            b.setPrefWidth(40);
            b.setFocusTraversable(false);
            b.setOnAction((ActionEvent event) -> {
                if (actionFunction != null) {
                    actionFunction.accept(b, item);
                }
            });

            HBox box = new HBox(label, new Group(b));
            box.setAlignment(Pos.CENTER_LEFT);
            HBox.setHgrow(label, Priority.ALWAYS);
            setGraphic(box);

        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            this.setText(item != null ? item.toString() : "");
            setAlignment(Pos.CENTER_LEFT);
        }
    }
}