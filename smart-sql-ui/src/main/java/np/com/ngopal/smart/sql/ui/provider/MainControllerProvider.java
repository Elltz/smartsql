/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.provider;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import np.com.ngopal.fx.utils.EasyFXMLLoader;
import np.com.ngopal.smart.sql.ui.controller.MainController;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class MainControllerProvider implements Provider<MainController> {

    @Inject
    Injector injector;

    @Override
    public MainController get() {
        MainController c = (MainController) EasyFXMLLoader.getController("Main");
        injector.injectMembers(c);
        return c;
    }

}
