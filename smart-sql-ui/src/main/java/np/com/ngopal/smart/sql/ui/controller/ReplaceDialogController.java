/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.FindReplaceHistoryService;
import np.com.ngopal.smart.sql.model.FindReplaceHistory;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.queryutils.FindCodeAreaData;
import np.com.ngopal.smart.sql.structure.queryutils.FindCodeAreaUtil;
import np.com.ngopal.smart.sql.structure.queryutils.FindObject;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReplaceDialogController extends BaseController implements Initializable {
    
    
    private FindObject codeArea;
    
    @FXML
    private Button findNextButton;
    
    @FXML
    private Button replaceButton;
    
    @FXML
    private Button reaplceAllButton;

    @FXML
    private Button cancelButton;

    @FXML
    private ComboBox<String> findField;
    
    @FXML
    private ComboBox<String> replaceField;

    @FXML
    private CheckBox matchWholeWordCheckBox;

    @FXML
    private CheckBox matchCaseCheckBox;
    
    @FXML
    private AnchorPane mainUI;

    @Getter
    private FindCodeAreaData findData;
    
    @Inject
    private FindReplaceHistoryService findReplaceService;
    
    public void setFindObject(FindObject object) {
        this.codeArea = object;
    }
        
    @FXML
    public void findNextAction(ActionEvent event) {
        makeBusy(true);
        Thread th = new Thread(() -> {
            
            String text = findField.getEditor().getText();
            if (!text.trim().isEmpty() && findReplaceService.isNeedSave(text)) {
                findReplaceService.save(new FindReplaceHistory(text));
                Platform.runLater(() -> {
                    updateUsage();
                });
            }

            findData = new FindCodeAreaData(
                text, 
                FindCodeAreaData.FindDirection.DOWN, 
                matchCaseCheckBox.isSelected(), 
                matchWholeWordCheckBox.isSelected(), true, codeArea.getCaretPosition(CARET_POSITION_FROM_START));

            FindCodeAreaUtil.findNext(codeArea, findData);
            
            if (findData.getLastPosition() == -1) {
                Platform.runLater(() -> {
                    ((MainController)getController()).showInfo("Info", "Finishing searching the document. Cannot find \"" + findData.getFindText() + "\"", null);
                });
            }
            
            makeBusy(false);
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void replaceAction(ActionEvent event) {
        if (findData != null) {
            makeBusy(true);
            Thread th = new Thread(() -> {
                String t0 = findField.getEditor().getText();
                if (!t0.trim().isEmpty() && findReplaceService.isNeedSave(t0)) {
                    findReplaceService.save(new FindReplaceHistory(t0));
                    Platform.runLater(() -> {
                       updateUsage(); 
                    });
                }

                String t1 = replaceField.getEditor().getText();
                if (!t1.trim().isEmpty() && findReplaceService.isNeedSave(t1)) {
                    findReplaceService.save(new FindReplaceHistory(t1));
                    Platform.runLater(() -> {
                       updateUsage(); 
                    });
                }

                if (findData.getLastPosition() >= 0) {
                    CountDownLatch mutex = new CountDownLatch(1);
                    Platform.runLater(() -> {
                        codeArea.replaceText(findData.getCurretPosition() - findData.getFindText().length() + 1, findData.getCurretPosition() + 1, t1);
                        mutex.countDown();
                    });
                    
                    if (mutex.getCount() > 0) {
                        try {
                            mutex.await();
                        } catch (InterruptedException ex) {
                            log.error("Error", ex);
                        }
                    }
                    
                    FindCodeAreaUtil.findNext(codeArea, findData);
                    
                    if (findData.getLastPosition() == -1) {
                        Platform.runLater(() -> {
                            ((MainController)getController()).showInfo("Info", "Finishing searching the document. Cannot find \"" + findData.getFindText() + "\"", null);
                        });
                    }
                }
                
                makeBusy(false);
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    private void updateUsage() {
        String s0 = findField.getEditor().getText();
        String s1 = replaceField.getEditor().getText();
        
        findField.getItems().clear();
        replaceField.getItems().clear();
        List<FindReplaceHistory> list = findReplaceService.getLastNames();
        if (list != null) {
            for (FindReplaceHistory f: list) {
                findField.getItems().add(f.getHistory());
                replaceField.getItems().add(f.getHistory());
            }
        }
        
        findField.getEditor().setText(s0);
        replaceField.getEditor().setText(s1);
    }
    
    @FXML
    public void replaceAllAction(ActionEvent event)
    {        
        makeBusy(true);
        Thread th = new Thread(() -> {
            String t0 = findField.getEditor().getText();
            if (!t0.trim().isEmpty() && findReplaceService.isNeedSave(t0)) {
                findReplaceService.save(new FindReplaceHistory(t0));
                Platform.runLater(() -> {
                    updateUsage();
                });
            }

            String t1 = replaceField.getEditor().getText();
            if (!t1.trim().isEmpty() && findReplaceService.isNeedSave(t1)) {
                findReplaceService.save(new FindReplaceHistory(t1));
                Platform.runLater(() -> {
                    updateUsage();
                });
            }

            findData = new FindCodeAreaData(
                t0, 
                FindCodeAreaData.FindDirection.DOWN, 
                matchCaseCheckBox.isSelected(), 
                matchWholeWordCheckBox.isSelected(), false, 0);

            FindCodeAreaUtil.findNext(codeArea, findData);

            String text = codeArea.getText();
            int shift = 0;
            int i = 0;
            
            while (findData.getLastPosition() >= 0) {
                i++;

                text = text.substring(0, findData.getCurretPosition() - 1 - shift) + t1 + text.substring(findData.getCurretPosition() - 1 + findData.getFoundedLength() - shift);
                shift += findData.getFoundedLength() - t1.length();

                FindCodeAreaUtil.findNext(codeArea, findData);
            }   

            String staticText = text;
            int index = i;
            Platform.runLater(() -> {
                codeArea.clear();
                codeArea.appendText(staticText);
                
                ((MainController)getController()).showInfo("Info", "Total of replaces is " + index, null);
                makeBusy(false);
            });        
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateUsage();
        
        Platform.runLater(() -> {
            new AutoCompleteComboBoxListener<>(findField);
            new AutoCompleteComboBoxListener<>(replaceField);
            findField.getEditor().requestFocus();
        });
        
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
