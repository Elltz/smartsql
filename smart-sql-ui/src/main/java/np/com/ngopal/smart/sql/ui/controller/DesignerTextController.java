package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DesignerTextController extends BaseController implements Initializable {
    @FXML
    private AnchorPane mainUI;
     
    @FXML
    private TextField textNameField;
    @FXML
    private TextArea textArea;
    
    private DesignerText text;
    private DesignerText sourceText;
    private SchemaDesignerTabContentController parent;
    private Tab tab;

    public void init(Tab tab, SchemaDesignerTabContentController parent, DesignerText sourceText) {        
        
        this.tab = tab;
        this.parent = parent;
        this.sourceText = sourceText;
        
        this.text = new DesignerText();
        this.text.fromMap(sourceText.toMap());
        
        textNameField.textProperty().bindBidirectional(text.name());                
        textArea.textProperty().bindBidirectional(text.text());        
    }
    
    @FXML
    public void saveAction(ActionEvent event) {
        
        sourceText.fromMap(text.toMap());
        parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + text.name().get() + "'", parent.getCurrentDiagram());
        
        closeAction(null);
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        module.makeCanSave();
    }
    
    @FXML
    public void closeAction(ActionEvent event) {
        if (tab != null && tab.getTabPane() != null) {
            tab.getTabPane().getTabs().remove(tab);
        }
    }
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
}
