package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class EnablingPerformanceSchemaController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;
        
    private DialogResponce responce = DialogResponce.CANCEL;    
    
    @FXML
    public void yesAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        responce = DialogResponce.CANCEL;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
    public DialogResponce getResponce() {
        return responce;
    }

    @Override
    public String getInputValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initDialog(Object... params) {
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }
}
