package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.RunFrequency;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.ScheduleReportsService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ScheduleReport;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.utils.ReportUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 * @TODO Maybe need make many small wizard panes
 * 123@testsmartmysql
 * testsmartmysql@gmail.com
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ScheduledReportWizardController extends AbstractScheduledWizardController<ScheduleReport> {   
    
    @Inject
    ScheduleReportsService reportsService;
            
    @FXML
    private AnchorPane welcomePane;
    @FXML
    private AnchorPane whatDoYouWantPane;
    @FXML
    private AnchorPane connectionPane;        
    @FXML
    private AnchorPane emailPane;
    @FXML
    private AnchorPane specifyEmailSubjectPane;
    @FXML
    private AnchorPane queryPane;
    @FXML
    private AnchorPane runPane;
    @FXML
    private AnchorPane donePane;
       
    
    @FXML
    private RadioButton sendResultAnEmail;
    @FXML
    private CheckBox includeOriginalQuery;
    @FXML
    private CheckBox includeMessagesFromServer;
    @FXML
    private RadioButton executeMaintenanceQuery;
    @FXML
    private CheckBox sndMailOnError;
    
    @FXML
    private RadioButton exitImmediatly;
    @FXML
    private RadioButton continueNextQuery;
    
       
    @FXML
    private RadioButton userDefined;
    @FXML
    private TextField userDefinedField;
    @FXML
    private RadioButton definedWithoutTime;
    @FXML
    private RadioButton definedWithTime;
    
    @FXML
    private HBox selectFileBox;
    @FXML
    private RadioButton selectFile;
    @FXML
    private TextField selectFileField;
    @FXML
    private RadioButton enterQuery;
    @FXML
    private BorderPane queryAreaPane;
    private RSyntaxTextArea queryArea;
        
    @Override
    ScheduleReport fromString(String string) {
        for (ScheduleReport sp: connectionsComboBox.getItems()) {
            if (sp.getName().equals(string)) {
                return sp;
            }
        }
        
        return null;
    }
    
    public void init(DBService dbService, ConnectionSession session) throws SQLException {
        super.init(dbService, session != null ? session.getConnectionParam() : null, null);
        
        RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), session);
        SwingUtilities.invokeLater(() -> {
            builder.init();
            queryArea = builder.getTextArea();
            Platform.runLater(() -> {
                queryAreaPane.setCenter(builder.getSwingNode());
            });
        });
    }

    @Override
    List<ScheduleReport> loadSchedulers() {
        List<ScheduleReport> list = reportsService.getAll();
        return list != null ? list : new ArrayList<>();
    }
    
    @Override
    void deleteSchedule(ScheduleReport schedule) {
        reportsService.delete(schedule);
    }

    @Override
    public void databaseChanged(Database database) {
        // do nothing
    }
    
    
    
    @Override
    boolean checkNextAction() {
        
        switch (currentWizardIndex) {                
            // need check if selected saved jobs - than job must be selected
            case 0:                
                if (!newJobRadioButton.isSelected() && savedJobsComboBox.getSelectionModel().getSelectedItem() == null) {
                    ((MainController)getController()).showInfo("Info", "Please select saved job!", null);
                    return false;
                } else {
                    if (newJobRadioButton.isSelected() && jobNameField.getText().trim().isEmpty()) {
                        ((MainController)getController()).showInfo("Info", "Please set new job name!", null);
                        return false;
                    }
                    schedule = newJobRadioButton.isSelected() ? null : savedJobsComboBox.getSelectionModel().getSelectedItem();
                    if (schedule != null) {
                        updateAllWithScheduleParams();
                    }                        
                }
                break;
                
            // need check database selection
            case 2:
//                if (databaseComboBox.getSelectionModel().getSelectedItem() == null) {
//                    ((MainController)getController()).showError("Error", "Please select a database!", null);
//                    return false;
//                }                
                makeBusy(true);

                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void run() {
                        try {
                        ConnectionParams cp = schedule != null ? schedule.toConnectionParams() : new ConnectionParams();
                        cp.setMultiQueries(true);
                        
                        cp.setAddress(address.getText());
                        cp.setUsername(user.getText());
                        cp.setPassword(pass.getText());

                        try {
                            cp.setPort(Integer.parseInt(port.getText()));
                        } catch (NumberFormatException ex) {
                            
                            backAction(null);                            
                            DialogHelper.showError(getController(), "Error", "Error parsing port", ex, getStage());
                            makeBusy(false);
                            return;
                        }

                        try {
                            cp.setSessionIdleTimeout(Integer.parseInt(sessionIdleTimeoutValue.getText()));
                        } catch (NumberFormatException ex) {                            
                            
                            backAction(null);
                            DialogHelper.showError(getController(), "Error", "Error parsing idle timeout", ex, getStage());
                            makeBusy(false);
                            return;
                        }

                        cp.setSshHost(sshHost.getText());
                        cp.setSshPassword(sshPassword.getText());
                        cp.setSshUser(sshUser.getText());
                        cp.setUseCompressedProtocol(useCompressedProtocol.isSelected());
                        
                        dbService.connect(cp);
                        List<Database> databases = dbService.getDatabases();
                        if (databases != null) {
                        
                            for (Database database: databases) {
                                Database db = DatabaseCache.getInstance().getDatabase(cp.cacheKey(), database.getName());
                                databaseComboBox.getItems().add(db != null ? db : database);       
                            }
                        
                            Database selectedDatabase = null;
                            String initDb = initDatabase != null ? initDatabase.getName() : null;
                            if (schedule != null) {
                                initDb = schedule.getDatabase();
                            }
                            
                            if (initDb != null) {
                                for (Database db: databases) {
                                    if (db.getName().equals(initDb)) {
                                        selectedDatabase = db;
                                    }
                                }
                            }

                            if (selectedDatabase != null) {

                                Database staticDatabase = selectedDatabase;
                                Platform.runLater(() -> {
                                    databaseComboBox.getSelectionModel().select(staticDatabase);
                                    makeBusy(false);
                                });
                            } else {
                                makeBusy(false);
                            }
                        }
                    } catch (SQLException ex) {
                        
                        backAction(null);                        
                        
                        DialogHelper.showInfo(getController(), "Info", "Connection is wrong!", ex.getMessage());
                        makeBusy(false);
                    } 
                    }
                });

                if (!isNeedSendEmail()) {
                    currentWizardIndex++;
                    currentWizardIndex++;
                }

                break;

            case 3:
                if (emailFromName.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter 'From name'", null);
                    return false;
                }

                if (emailFromEmail.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter 'From email' address", null);
                    return false;
                }

                if (emailToEmail.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter 'To email' address", null);
                    return false;
                }

                if (emailHost.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter SMPT 'Host' server address", null);
                    return false;
                }

                if (emailPort.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please enter SMPT 'Port' number", null);
                    return false;
                }

                if (!emailPort.getText().matches("\\d+")) {
                    ((MainController)getController()).showError("Error", "Please enter valid 'Port' number", null);
                    return false;
                }

                if (emailAuth.isSelected()) {
                    if (emailAccountName.getText().trim().isEmpty()) {
                        ((MainController)getController()).showError("Error", "Please enter 'Account name'", null);
                        return false;
                    }

                    if (emailPasswd.getText().trim().isEmpty()) {
                        ((MainController)getController()).showError("Error", "Please enter 'Password'", null);
                        return false;
                    }
                }
                break;
            
            case 4:
                if (userDefined.isSelected() && userDefinedField.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please specify a subject", null);
                    return false;
                }

                break;
                
            case 5:
                if (selectFile.isSelected() && !new File(selectFileField.getText().trim()).exists()) {
                    ((MainController)getController()).showError("Error", "Could not open file", null);
                    return false;
                }
                
                if (enterQuery.isSelected() && queryArea.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Enter query for reporting", null);
                    return false;
                }

                break;
                
            case 6:
                if (logFile.getText().trim().isEmpty() || logFile.getText().trim().isEmpty()) {
                    ((MainController)getController()).showError("Error", "Please select log file!", null);
                    return false;
                }

                if (useWindowsSchedule.isSelected()) {
                    if (runFrequency.getSelectionModel().getSelectedItem() == null) {
                        ((MainController)getController()).showError("Error", "Please select schedule frequence!", null);
                        return false; 
                    } else if (runFrequency.getSelectionModel().getSelectedItem() == RunFrequency.WEEKLY && getWeeklyString() == null) {
                        ((MainController)getController()).showError("Error", "Please select at least 1 week day!", null);
                        return false; 
                    }
                }
                
                doFinish();
                break;
        }
        
        return true;
    }
    
    @Override
    void fillScheduleCustom() {
        
        schedule.setSendResultAnEmail(sendResultAnEmail.isSelected());
        schedule.setIncludeOriginalQuery(includeOriginalQuery.isSelected());
        schedule.setIncludeMessagesFromServer(includeMessagesFromServer.isSelected());
        schedule.setSendMailOnError(sndMailOnError.isSelected());
        
        schedule.setExitImmediatly(exitImmediatly.isSelected());
        
        if (userDefined.isSelected()) {
            schedule.setUserDefined(userDefinedField.getText());
        } else {
            schedule.setDefinedWithTime(definedWithTime.isSelected());
            schedule.setUserDefined(null);
        }
        
        if (selectFile.isSelected()) {
            schedule.setSelectFile(selectFile.isSelected());
            schedule.setSelectFilePath(selectFileField.getText());
            schedule.setQuery(null);
        } else {
            schedule.setSelectFile(false);
            schedule.setQuery(queryArea.getText());
            schedule.setSelectFilePath(null);
        }
                
        reportsService.save(schedule);
    }
    
    @FXML
    public void executeAndTestQuery(ActionEvent event) {
        // getting queries for report
        String queries[] = null;
        if (schedule.isSelectFile()) {
            try {
                byte[] data = Files.readAllBytes(new File(selectFileField.getText()).toPath());

                queries = new String(data, "UTF-8").split(";");
            } catch (Throwable th) {
                ((MainController)getController()).showError("ERror", "Error encountered when running report scheduler", th);
            }
        } else {
            queries = queryArea.getText().split(";");
        }

        if (queries != null) {
            boolean error = false;
            for (String query: queries) {
                if (!query.trim().isEmpty())
                {
                    try {
                        // Execute query
                        Statement st = dbService.getDB().createStatement();
                        st.execute(query);
                    } catch (Throwable th) {
                        ((MainController)getController()).showError("Error encountered when running report scheduler", query, th);
                        error = true;
                    }
                }
            }
            
            if (!error) {
                ((MainController)getController()).showInfo("Info", "Query/queries entered are correct", null);
            }
        }
    }
    
    
    @Override
    void finishAction() {
        ReportUtils.doReport(dbService, schedule, new ReportUtils.ReportStatusCallback() {
            @Override
            public void error(String errorMessage, Throwable th) {
                ((MainController)getController()).showError("Error", errorMessage, th);
                
                if (isNeedSendEmail()) {

                    StringWriter sw = new StringWriter();
                    sw.append(errorMessage).append("\n\n");
                    if (th != null) {
                        th.printStackTrace(new PrintWriter(sw));
                    }

                    sendEmail(schedule, "Report failed - " + schedule.getName() + " at " +  DATETIME_FORMATER.format(new Date()), sw.toString().replaceAll("\n", "<br>"));
                }
            }

            @Override
            public void success(List<String> messages) {
                if (messages != null && !messages.isEmpty()) {
                    String subject = schedule.getUserDefined();
                    if (subject == null) {
                        subject = schedule.isDefinedWithTime() ? DATETIME_FORMATER.format(new Date()) : DATE_FORMATER.format(new Date());
                    }

                    sendEmail(schedule, subject, String.join("<br><br><br>", messages));
                }
            }
        });
    }
    
    @Override
    void checkBackAction() {
        if (currentWizardIndex == 5 && !isNeedSendEmail())
        {
            currentWizardIndex--;
            currentWizardIndex--;
        }
    }
    
    @Override
    boolean isNeedSendEmail() {
        return !executeMaintenanceQuery.isSelected() || sndMailOnError.isSelected();
    }
    
    @Override
    void updateAllComponentsUsingSchedule(ScheduleReport schedule) {
        
        sendResultAnEmail.setSelected(schedule.isSendResultAnEmail());
        executeMaintenanceQuery.setSelected(!schedule.isSendResultAnEmail());
        includeOriginalQuery.setSelected(schedule.isIncludeOriginalQuery());
        includeMessagesFromServer.setSelected(schedule.isIncludeMessagesFromServer());
        sndMailOnError.setSelected(schedule.isSendMailOnError());
        
        exitImmediatly.setSelected(schedule.isExitImmediatly());
        continueNextQuery.setSelected(!schedule.isExitImmediatly());
        
        if (schedule.getUserDefined() != null) {
            userDefinedField.setText(schedule.getUserDefined());
            userDefined.setSelected(true);
        } else {
            definedWithTime.setSelected(schedule.isDefinedWithTime());
            definedWithoutTime.setSelected(!schedule.isDefinedWithTime());
        }
        
        if (schedule.isSelectFile()) {
            selectFile.setSelected(schedule.isSelectFile());
            selectFileField.setText(schedule.getSelectFilePath());
        } else {
            enterQuery.setSelected(true);
            queryArea.setText(schedule.getQuery());
        }
        
        try {
            dbService.connect(schedule.toConnectionParams());
            List<Database> databases = dbService.getDatabases();
            if (databases != null) {
                databaseComboBox.getItems().clear();
                databaseComboBox.getItems().addAll(databases);
                
                Database selectedDatabase = null;
                for (Database db: databases) {
                    if (db.getName().equals(schedule.getDatabase())) {
                        selectedDatabase = db;
                    }
                }
                
                if (selectedDatabase != null) {
                    databaseComboBox.getSelectionModel().select(selectedDatabase);                    
                    databaseComboBox.setDisable(false);
                }
            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", "Error loading databases", ex);
        }
    }
    
    @FXML
    public void selectQueryFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("SQL files", "*.sql"),
            new FileChooser.ExtensionFilter("All files", "*.*"));
        
        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            selectFileField.setText(file.getAbsolutePath());
        }
    }
    
    @Override
    void fillWizardPanes(List<AnchorPane> wizardPanes) {
        wizardPanes.add(welcomePane);
        wizardPanes.add(whatDoYouWantPane);
        wizardPanes.add(connectionPane);
        wizardPanes.add(emailPane);
        wizardPanes.add(specifyEmailSubjectPane);
        wizardPanes.add(queryPane);
        wizardPanes.add(runPane);
        wizardPanes.add(donePane);
    }
    
    @Override
    ScheduleReport createSchedule() {
        return new ScheduleReport();
    }
    
    @Override
    ScheduleReport createScheduleFromConnectionParams(ConnectionParams params) {
        return new ScheduleReport(params);
    }
    
    
    @Override
    public void propertyListeners() {
        
        super.propertyListeners();
        
        sendResultAnEmail.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            includeMessagesFromServer.setDisable(!newValue);
            includeOriginalQuery.setDisable(!newValue);
            sndMailOnError.setDisable(newValue);
        });
        
        userDefined.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            userDefinedField.setDisable(!newValue);
        });
        
        enterQuery.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            queryArea.setEnabled(newValue);
            selectFileBox.setDisable(newValue);
        });
    }
}
