/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.DBElement;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class DataSearchController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUi;
    
    @FXML
    private Label databaseTextLabel;
    
    @FXML
    private Button clickToChangeLabel;
    
    @FXML
    private TextField searchInputField;
    
    @FXML
    private CheckBox regexCheckbox;
    
    @FXML
    private Button searchButton;

    @FXML
    private ToolBar header;
    
    @FXML
    private Button deleteButton;
    
    @FXML
    private Button textviewButton;
            
    @FXML
    private Button duplicateRowButton;
    
    @FXML
    private Button cancelChangesButton;
    
    @FXML
    private Button formViewButton;
    
    @FXML
    private MenuItem copyCellItem;
    
    @FXML
    private Button insertNewRowButton;
    
    @FXML
    private MenuItem copySelectedRowsItem;
    
    @FXML
    private MenuItem copyAllRowsItem;
    
    @FXML
    private Button gridViewButton;
    
    @FXML
    private Label infoSearchLabel;
    
    @FXML
    private TableView tableview;
    
    @Override
    public AnchorPane getUI() { 
        return mainUi;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {  
        dataTypes.add("numeric"); //default
       dataTypes.add("short string"); //default
        
    }
       
    private DBElement selected;
    public ConnectionSession session;

    public void init(ReadOnlyObjectProperty<TreeItem> item) {
        item.addListener(new ChangeListener<TreeItem>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem> ov,
                    TreeItem t, TreeItem t1) {
                if (t1 != null) {
                    if (t1.getValue() instanceof DBElement) {//it is always
                       selected = (DBElement)t1.getValue();
                    }
                }
            }
        });
        if (item.getValue().getValue() instanceof Database) {
            selected = ((DBElement) item.getValue().getValue());
            prepUI();
        }
    }
    
    private void prepUI() {
        
        if (selected != null) {
            if (selected instanceof Database) {
                //Search in database `<database name>` in all tables (numeric,short string)
                databaseTextLabel.setText("Search in database `" + selected.toString() + "` in all tables " + getDataTypes());
            } else if (selected instanceof DBTable) {
                databaseTextLabel.setText("Search in table `" + selected.toString() + "` in all columns " + getDataTypes());
            } else {
                databaseTextLabel.setText("Search in all database " + getDataTypes());
            }
        }

    }
    
    private ArrayList<String> dataTypes = new ArrayList<>();
    
    private String getDataTypes(){
     
        StringBuilder sb = new StringBuilder(dataTypes.size());
        sb.append('(');
        for(String s : dataTypes){
            sb.append(s);
            sb.append(',');
        }
        sb.append(')');
        return sb.toString();
    }
    
    private EventHandler<ActionEvent> onAction = new EventHandler(){

        @Override
        public void handle(Event t) {
            Object o = t.getSource();
            if(o == clickToChangeLabel){
                dataSearchFD.session = session;
                 dataSearchFD.show(selected);            }
        }
        
    };
    
    public DataSearchFilterDialog dataSearchFD;
    
}
