package np.com.ngopal.smart.sql.core.modules.utils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public enum EncryptiorValue {
    NOENCRYPTION(0, "No Encryption"),
    TLS(1, "TLS"),
    SSL(2, "SSL");

    public final int id;
    final String name;

    EncryptiorValue(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static EncryptiorValue toEnum(int id) {
        switch (id) {                
            case 0:
                return NOENCRYPTION;
            case 1:
                return TLS;
            case 2:
                return SSL;

            default:
                return NOENCRYPTION;
        }
    }

    @Override
    public String toString() {
        return name;
    }
};
