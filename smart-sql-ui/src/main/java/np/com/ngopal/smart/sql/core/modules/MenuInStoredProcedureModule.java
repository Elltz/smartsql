package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.util.Pair;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.ui.controller.StoredProcedureWithCursorController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInStoredProcedureModule extends AbstractStandardModule {

    private Image procedureImage;
    
    private MenuItem alterItem;
    private MenuItem dropItem;
    private MenuItem createItem;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();
        
        //SeparatorMenuItem separate = new SeparatorMenuItem();
        Menu storedProcMenu = new Menu("Stored Procedures",
                 new ImageView(SmartImageProvider.getInstance().getStoredProc_image(20, 20)));
        
        alterItem = getAlterStoredProcedureMenuItem(null);
        dropItem = getDropStoredProcedureItem(null);
        createItem = getCreateStoredProcedureMenuItem(null);
        
        storedProcMenu.getItems().addAll(createItem, alterItem, dropItem);
        
        alterItem.disableProperty().bind(busy());
        dropItem.disableProperty().bind(busy());
        createItem.disableProperty().bind(busy());
        storedProcMenu.disableProperty().bind(busy());
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{storedProcMenu});
        
        if (procedureImage == null) {
                procedureImage = SmartImageProvider.getInstance().getStoredProcTab_image();
            }
        
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        boolean isStroredProcedure = item.getValue() instanceof StoredProc;
        
        alterItem.setDisable(!isStroredProcedure);
        dropItem.setDisable(!isStroredProcedure);
        
        createItem.setDisable(!(isStroredProcedure || item.getValue().equals(ConnectionTabContentController.DatabaseChildrenType.STORED_PROCS)));
        return true;
    }
    
    private MenuItem getStoredProcedureWithCursorItem(final ConnectionSession session) {
        MenuItem storedProcedureItem = new MenuItem("Stored Procedure With Cursor...", 
                new ImageView(SmartImageProvider.getInstance().getCreateStoredProcs_image(20, 20)));
        storedProcedureItem.setOnAction((ActionEvent event) -> {
            QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
            if (queryTabModule != null) {
                
                ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                
                Database db = sessionTemp.getConnectionParam().getSelectedDatabase();
                 
                queryTabModule.addTemplatedQueryTab(sessionTemp, (Void t) -> {
                    
                    StoredProcedureWithCursorController contr = ((MainController)getController()).getTypedBaseController("StoredProcedureWithCursor");                    
                    DialogHelper.show("Create Stored Procedure With Cursor", contr.getUI(), getController().getStage());
                    
                    if (contr.getResponce() == DialogResponce.OK_YES) {
                        try {
                            
                            ((MainController)getController()).usedStoredProcedureWithCursor();
                            
                            String content = sessionTemp.getService().getCreateStoredProcedureWithCursorTemplateSQL(
                                db != null ? db.getName() : "<db-name>",
                                contr.getProcedureNameValue(),
                                contr.getCursorNameValue(),
                                contr.getCursorQueryValue()
                            );
                        
                            return new Pair<>(contr.getProcedureNameValue(), content);
                        } catch (SQLException ex) {
                            DialogHelper.showError(getController(), "Error opening stored procedure script", ex.getMessage(), ex);
                        }
                        
                    }                    
                    
                    return null;
                    
                }, QueryTabModule.TAB_USER_DATA__PROCEDURE_TAB  
                );
            }
        });

        return storedProcedureItem;
    }
    
    private MenuItem getCreateStoredProcedureMenuItem(ConnectionSession session) {

        MenuItem createStoredProcedureMenuItem = new MenuItem("Create Stored Procedure",
                new ImageView(SmartImageProvider.getInstance().getCreateStoredProcs_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createStoredProcedureMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createStoredProcedureMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createStoredProcedure();
            }
        });

        return createStoredProcedureMenuItem;
    }
    
  
    private MenuItem getAlterStoredProcedureMenuItem(ConnectionSession session) {
        MenuItem alterStoredProcedureMenuItem = new MenuItem("Alter Stored Procedure",
                new ImageView(SmartImageProvider.getInstance().getAlterStoredProcs_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        alterStoredProcedureMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        alterStoredProcedureMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                alterStoredProcedure();
            }
        });
        
        return alterStoredProcedureMenuItem;
    }
    
    public void alterStoredProcedure() {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        try {

            ConnectionSession sessionTemp = getController().getSelectedConnectionSession().get();

            StoredProc proc = (StoredProc) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();
 
            String alterQuery = sessionTemp.getService().getAlterStoredProcedureSQL(proc.getDatabase().getName(), proc.getName());
            if (alterQuery == null) {
                showError("Access denied", "Access denied for altering stored procedure", null);
            } else {
                queryTabModule.addNamedTabWithContext(
                    sessionTemp, 
                    proc.getName(), 
                    alterQuery,
                    QueryTabModule.TAB_USER_DATA__PROCEDURE_TAB,
                    null);
            }
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }
    
    
    private MenuItem getDropStoredProcedureItem(ConnectionSession session) {

        MenuItem dropStoredProcedureItem = new MenuItem("Drop Stored Procedure",
                new ImageView(SmartImageProvider.getInstance().getDropStoredProcs_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        dropStoredProcedureItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropStoredProcedureItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                    
                    StoredProc proc = (StoredProc) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();

                    DialogResponce responce = DialogHelper.showConfirm(getController(), "Drop stored procedure", 
                        "Do you really want to drop the stored procedure '" + proc.getName() + "'?");

                    if (responce == DialogResponce.OK_YES) {
                        // drop fucntion
                        sessionTemp.getService().dropStoredProcedure(proc);
                        LeftTreeHelper.storedProcDroped(sessionTemp.getConnectionParam(), ((ReadOnlyObjectProperty<TreeItem>) getController().getSelectedTreeItem(sessionTemp)).get());
                    }
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }
            }
        });

        return dropStoredProcedureItem;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        
        map.put(new HookKey("storedprocs0", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getCreateStoredProcedureMenuItem(session), getStoredProcedureWithCursorItem(session)}))), Hooks.QueryBrowser.STORED_PROCS);
        
        map.put(new HookKey("storedprocItems0", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getCreateStoredProcedureMenuItem(session), getAlterStoredProcedureMenuItem(session),
            getDropStoredProcedureItem(session)}))), Hooks.QueryBrowser.STORED_PROC_ITEM);
        
        return map;
    }
    
    public void createStoredProcedure() {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        QueryTabModule queryTabModule = (QueryTabModule) getMainController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null) {
            Database db = session.getConnectionParam().getSelectedDatabase();
            
            queryTabModule.addTemplatedQueryTab(session, 
                "Create stored procedure", 
                "Enter new stored procedure name:", 
                new java.util.function.Function<String, String>() {
                    @Override
                    public String apply(String t) {
                        return session.getService().getCreateStoredProcedureTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                    }
                },QueryTabModule.TAB_USER_DATA__PROCEDURE_TAB );
        }
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
