package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.ui.controller.FrequencyExecutedQueriesTabController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;
import org.w3c.dom.Element;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class FrequencyExecutedQueriesModule extends AbstractStandardModule {

    public static final String FEQ_History = "FEQ_Tab";
    private HashMap<ConnectionSession, Tab> tabHolder = new HashMap();
    private HashMap<ConnectionSession, FrequencyExecutedQueriesTabController> controllerHolder = new HashMap<>();

    private Image feqImage = null;

    
    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public boolean install() {
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem obj) {return true; }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postConnection(ConnectionSession session, Tab intab) throws Exception {
        
        if (feqImage == null) {
            feqImage = SmartImageProvider.getInstance().getFeg_image();
        }
        
        FrequencyExecutedQueriesTabController tabController = getTypedBaseController("FrequencyExecutedQueriesTab");
        tabController.init(session);
        
        Tab tab = new Tab("FEQ");
        tab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(feqImage), 16, 16));
        tab.setUserData(FEQ_History);
        
        ChangeListener<Object> listener = (ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {
            if (tab == newValue) {
                tabController.refreshData(null);
            }
        };
        
        tab.onClosedProperty().addListener((ObservableValue<? extends EventHandler<Event>> observable, EventHandler<Event> oldValue, EventHandler<Event> newValue) -> {
            getController().selectedTabProperty().removeListener(listener);
            tabController.destory();
        });
           
        
        getController().selectedTabProperty().addListener(listener);
        tab.setContent(tabController.getUI());

        Platform.runLater(() -> getController().addTab(tab, session) );
        tabHolder.put(session,tab);
        controllerHolder.put(session, tabController);
        
        tab.setOnCloseRequest((Event t) -> {
            tabHolder.remove(session);
            controllerHolder.remove(session);
        });
    }
    
    public void changeAllFont(String font) {
        for (FrequencyExecutedQueriesTabController tabController: controllerHolder.values()) {
            tabController.changeFont(font);
        }
    }
    

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    Element el;
    volatile boolean go;
    
    /*@Override
    public Map<?, ?> getPersistingMap() {        
        HashMap<String, String> history = new HashMap();
        if(true){return history;}//for temp hold up
        for (Map.Entry<ConnectionSession, Tab> en : tabHolder.entrySet()) {
            el = null;
            go = true;
            Platform.runLater(new Runnable() {
                @Override
                public void run() { 
                    el = ((WebView) en.getValue()
                    .getContent()).getEngine().getDocument().
                    getElementById("layer");
                    go = false;
                }
            });
            while(go){}
            
            if (el != null) {
                history.put(String.valueOf(en.getKey().getConnectionId()), el.getTextContent());
            }
        }
        return history;
    }*/
    
    @Override
    public String getInfo() { 
        return "";
    }
}
