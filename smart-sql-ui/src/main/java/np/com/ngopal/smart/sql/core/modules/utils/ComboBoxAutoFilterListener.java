package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ComboBoxAutoFilterListener<T> implements ChangeListener<T> {
    
    private final ComboBox<T> combobox;
    
    private final List<T> source = new ArrayList<>();
    
    public ComboBoxAutoFilterListener(ComboBox<T> combobox, List<T> sourceList) {
        this.combobox = combobox;        
        source.addAll(sourceList);
    }
    
    @Override
    public void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
        List<T> filtered = new ArrayList<>();
        for (T s: source) {
            if (newValue != null && !newValue.toString().isEmpty()) {
                if (s != null && s.toString().toLowerCase().startsWith(newValue.toString().toLowerCase())) {
                    filtered.add(s);
                }
            } else {
                filtered.add(s);
            }
        }
        
        Platform.runLater(() -> {
           combobox.getItems().setAll(filtered); 
        });
    }

    public void addSourcePath(T importFilePath) {
        source.add(importFilePath);
    }

}
