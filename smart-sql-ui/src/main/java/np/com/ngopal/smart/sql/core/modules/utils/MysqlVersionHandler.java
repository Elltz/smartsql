/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.Arrays;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class MysqlVersionHandler {
    
    private String version;
    private final Double versionDouble,subVersionDouble;
    private SimpleBooleanProperty userLockable , passwordExpirable;

    public MysqlVersionHandler(String version) {
        this.version = version;
        if (version != null && version.contains(".")) {
            int indexOfFirstDot = version.indexOf('.');
            versionDouble = Double.valueOf(version.substring(0, indexOfFirstDot + 2));
            int indexOfLastDot = version.lastIndexOf('.');
            if (indexOfLastDot > indexOfFirstDot) {
                subVersionDouble = Double.valueOf(version.substring(indexOfLastDot+1));
            }else{ subVersionDouble = 0.0; }
        } else {
            versionDouble = 0.0;
            subVersionDouble = 0.0;
        }
    }
    
    private boolean is5_7(){        
        return versionDouble == 5.7;
    }
    
    private boolean isAbove5_7(){        
        return versionDouble > 5.7;
    }
    
    private boolean is5_6(){        
        return versionDouble == 5.6;
    }
    
    public boolean isAbove5_6(){        
        return versionDouble > 5.6;
    }
    
    public boolean is5_5(){
        return versionDouble == 5.5;        
    }
    
    public boolean isAbove5_5(){
        return versionDouble > 5.5;        
    }
    
    public boolean canSetResourceLimit(){
        return isAbove5_5();
    }
    
    public boolean passwordExpiresAfter1yearOnDefault(){
        return is5_7() && subVersionDouble >= 4 && subVersionDouble <= 10;
    }
    
    public List<String> getPassWordExpireOptions() {
        if ((isAbove5_6() && is5_7() && subVersionDouble > 4) || isAbove5_7()) {
            return Arrays.asList(Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE,
                    Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE_DEFAULT, Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE_INTERVAL_N_DAY,
                    Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE_NEVER);
        }
        return Arrays.asList(Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE);
    }
    
    public boolean canSetPassWord_ResourceWithAlter(){
        return isAbove5_7() || (is5_7() && subVersionDouble >= 6);
    }
    
    public boolean canSetPassWordExpireWithAlter(){
        return (is5_6() && subVersionDouble >= 7) ||  isAbove5_6();
    }
    
    public boolean canSetResource_PasswordExpireWithCreate(){
        return isAbove5_7() || (is5_7() && subVersionDouble >= 6 );
    }

    public SimpleBooleanProperty getUserLockable() {
        if(userLockable == null){
            createProperties();
        }
        return userLockable;
    }

    public SimpleBooleanProperty getPasswordExpirable() {
        if(passwordExpirable == null){
            createProperties();
        }
        return passwordExpirable;
    }
    
    private final void createProperties() {
        userLockable = new SimpleBooleanProperty() {
            @Override
            public boolean get() {
                return isAbove5_6();
            }

            @Override
            public Boolean getValue() {
                return get();
            }

        };
        passwordExpirable = new SimpleBooleanProperty() {

            @Override
            public boolean get() {
                return isAbove5_5();
            }

            @Override
            public Boolean getValue() {
                return get();
            }
        };
    }
    
    public boolean usesAlter_Grant(){
        return canSetPassWordExpireWithAlter() && canSetPassWord_ResourceWithAlter();
    }
    
    public boolean canUseShowCreateUser(){
        return isAbove5_7() || (is5_7() && subVersionDouble >= 6);
    }
    
    public double getVersion(){
        return versionDouble;
    }
        
}
