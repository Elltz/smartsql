/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 */
public class CSVUtil {

    public static void writeCSVFile(String excelFileName, ObservableList<ObservableList> data) {
        FileWriter writer;
        try {
            writer = new FileWriter(excelFileName);
            System.out.println("started");
            for (ObservableList<String> ob : data) {
                String[] arr = new String[ob.size()];
                int i = 0;
                for (String ob1 : ob) {
                    arr[i++] = "\"" + ob1 + "\"";
                }
                String formatedString = String.join(",", arr);;

                System.out.println(formatedString);
                writer.append(formatedString + "\n");

            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(CSVUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
