/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.utils.LicenseManager;
import np.com.ngopal.smart.sql.structure.utils.SmartsqlSavedData;

/**
 *
 * @author terah laweh (rumorsapp@gmail.com)
 */
public class AboutController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Button okButton;
    
    @FXML
    private Label topAppStatusText;
    
    @FXML
    private Hyperlink linkText;
    
    @FXML
    private Label companyText;
    
    @FXML
    private Label versionIndicationText;
    
    @FXML
    private Label legalNotice;
    
    @FXML
    private TextField userRegisteredTextField;
    
    @FXML
    private TextField licenseTypeField;
    @FXML
    private TextField licenseDateExpireField;
    @FXML
    private TextField licenseCreditsField;
    @FXML
    private TextField licenseFullUsageField;

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    private Stage dialog;

    @FXML
    public void refreshAction(ActionEvent event) {
        
        makeBusy(true);
        Thread th = new Thread(() -> {
            try {
                LicenseManager.getInstance().refresh();
            } finally {
                Platform.runLater(() -> {
                    refresh();
                    makeBusy(false);
                });
            }
        });
        th.setDaemon(true);
        th.start();
    }
    
    private void refresh() {
        SmartsqlSavedData data = StandardApplicationManager.getInstance().getSmartsqlSavedData();
        
        String name = data.getEmailId();
        
        if(name != null && !name.isEmpty()){
            userRegisteredTextField.setDisable(false);
            userRegisteredTextField.setEditable(false);
            userRegisteredTextField.setText(name);
        }
        
        topAppStatusText.setText(StandardApplicationManager.getInstance().getApplicationBinaryName() + " - Database Software");
        
        StringBuilder sb = new StringBuilder();
        if(data.isPaid()){
            sb.append("PAID VERSION");
        }else{
            sb.append("VERSION"); 
            legalNotice.setTextFill(Color.TOMATO);
        }
        if (data.getVersion() != null && data.getVersion().length() > 0) {
            sb.append(" - ").append(data.getVersion());
        }
        versionIndicationText.setText(sb.toString());
        
        licenseTypeField.setText(LicenseManager.getInstance().getType());
        licenseDateExpireField.setText(LicenseManager.getInstance().getDateExpire());
        licenseCreditsField.setText(LicenseManager.getInstance().getCredits());
        licenseFullUsageField.setText(LicenseManager.getInstance().getFullUsage());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {  dialog.hide();}
        });
        
        refresh();
    }
    
    public void init(){        
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(550);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("About");        
        dialog.setScene(new Scene(getUI()));
        dialog.getScene().getStylesheets().addAll(getController().getStage().getScene().getStylesheets());
        dialog.showAndWait();        
    }
    
}
