/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.util.ArrayList;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.components.QueryBuilderRowData;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public abstract class BuilderViewController {
    
    private AnchorPane mainUI;
    private TableProvider tableProvider;
    private TitledPane tpane;
    private ImageView closeImageView;
    
    public final EventHandler mouseEvents = new EventHandler<MouseEvent>() {

        boolean clean = false;
        Cursor cur;
        double x, y;

        @Override
        public void handle(MouseEvent t) {
            if (t.isSecondaryButtonDown()) {
                if (t.getSource() instanceof TitledPane) {
                    ((TitledPane) t.getSource()).getParent().toFront();
                }
                return;
            }

            if (t.getSource() instanceof TitledPane) {
                final TitledPane tp = (TitledPane) t.getSource();
                final AnchorPane mainUI = (AnchorPane) tp.getParent();
                if (t.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    mainUI.toFront();
                    x = mainUI.getLayoutX() - t.getSceneX();
                    y = mainUI.getLayoutY() - t.getSceneY();
                } else if (t.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    //mainUI.setCursor(Cursor.DEFAULT);
                    tp.setCursor(Cursor.DEFAULT);
                    if (clean) {
                        tp.setExpanded(clean);
                        clean = false;
                        //runOnceMore.setValue(true);
                    }
                    ((ListView) tp.getContent()).refresh();
                } else if (t.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    cur = tp.getCursor();
                    if (cur == null || (cur != Cursor.V_RESIZE
                            && cur != Cursor.E_RESIZE && cur != Cursor.SE_RESIZE)) {
                        mainUI.setLayoutX(x + t.getSceneX());
                        mainUI.setLayoutY((y + t.getSceneY()));
                        clean = true;
                    } else {
                        Bounds bound = mainUI.getLayoutBounds();
                        if (cur == Cursor.V_RESIZE) {
                            mainUI.setPrefHeight(t.getY());
                            t.consume();
                        } else if (cur == Cursor.E_RESIZE) {
                            mainUI.setPrefWidth(t.getX());
                            t.consume();
                        } else if (cur == Cursor.W_RESIZE) {
//                         mainUI.setPrefWidth(bound.getMaxX()- t.getX());
//                         mainUI.setLayoutX(t.getX());
//                        t.consume();
                        } else if (cur == Cursor.SW_RESIZE) {
//                        mainUI.setPrefSize(bound.getMaxX()- t.getX(),
//                                t.getY());
//                         mainUI.setLayoutX(t.getX());
//                        System.out.println("drag sw");
//                        t.consume();
                        } else if (cur == Cursor.SE_RESIZE) {
                            mainUI.setPrefSize(t.getX(), t.getY());
                            t.consume();
                        }
                    }

                } else if (t.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    mainUI.setCursor(Cursor.MOVE);
                }
            }
        }

    };

    private TableView tableView;
    private Button closeButton;
    private ListView<ColumnOnColumnRowData> listview;
    
    private QueryBuilderController parentController;
    
    private String tableAlias;
    
    public static final double cellFixedSize = 25.6;
    
    public void setTableView(TableView tableview) {
        tableView = tableview;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
        Platform.runLater(() -> {
           tpane.setText(tableAlias != null ? tableAlias : tableProvider.getName()); 
        });
    }
        
    
    
    public BuilderViewController(QueryBuilderController parentController, TableProvider table, double x, double y) {
        tableProvider = table;
        this.parentController = parentController;
        
        try {
            FXMLLoader loader = new FXMLLoader(MainUI.class.getResource(
                    "BuilderView.fxml").toURI().toURL());
            mainUI = loader.load();
            tpane = (TitledPane) mainUI.getChildren().get(0);
            tpane.setOnMousePressed(mouseEvents);
            tpane.setOnMouseReleased(mouseEvents);
            tpane.setOnMouseDragged(mouseEvents);
            tpane.setText(table.getName());
            //propertiseParentContainer(mainUI);
            mainUI.setLayoutX(x);
            mainUI.setLayoutY(y);
            //addBuilderEffect(mainUIane);
            closeButton = (Button) mainUI.getChildren().get(1);
            ((ImageView)closeButton.getGraphic()).setImage(SmartImageProvider.getInstance().getExit_image());
            listview = (ListView) tpane.getContent();
            prepareListView();
            fillUpListView();
            //after you are done here - try to evaluate another mainUIproach by using QueryBuilderRowData as the datasource
            //for both the list view and the tableview so as to save yourself from all the loop
            closeButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    parentController.removeTable(getLabel());
                }
            });

            listview.setFixedCellSize(cellFixedSize);

            mainUI.setUserData(BuilderViewController.this);
        } catch (Exception e) {e.printStackTrace();
        }
    }

    public void moveTo(double x, double y) {
        mainUI.setLayoutX(x);
        mainUI.setLayoutY(y);
    }

    public double getX() {
        return mainUI.getLayoutX();
    }
    
    public double getY() {
        return mainUI.getLayoutY();
    }
    
    public TableProvider getTableProvider() {
        return tableProvider;
    }
    

    public void setPosition(double xPos, double yPos) {
        mainUI.setLayoutX(xPos);
        mainUI.setLayoutY(yPos);
    }

    public AnchorPane getUI() {
        return mainUI;
    }

    public void destroyController(){
        closeAndRemoveTable();
        tableView = null;
    }
    
    private void closeAndRemoveTable() {
        mainUI.setOpacity(0.5);
        for(ColumnOnColumnRowData item: listview.getItems()) {
            if(item.getSelectedRow().get()){
                item.getSelectedRow().set(false);
            }
        }        
        tpane.setCollapsible(true);
        tpane.setExpanded(false);
        listview.getItems().clear();     
        mainUI.setUserData(null);
        ((Pane)mainUI.getParent()).getChildren().remove(mainUI);
    }

    public void removeColumn(Column nameOfColumn) {
        ArrayList<QueryBuilderRowData> qbrlist = new ArrayList<>(tableView.getItems());
        for (QueryBuilderRowData querBuilderRD : qbrlist) {
            if (querBuilderRD.getColumn() == nameOfColumn) {
                tableView.getItems().remove(querBuilderRD);
                break;
            }
        }
    }

    public void addColumn(Column nameOfColumn) {
        loopForColumnOnColumnRowData(nameOfColumn).selectedRow.set(true);
    }
    
    public boolean toggleColumnState(Column nameOfColumn) {
        ColumnOnColumnRowData item = loopForColumnOnColumnRowData(nameOfColumn);
        item.selectedRow.set(!item.getSelectedRow().get());
        return item.getSelectedRow().get();
    }

    private final ColumnOnColumnRowData loopForColumnOnColumnRowData(Column nameOfColumn) {
        ColumnOnColumnRowData item = null;
        if (nameOfColumn != null) {
            for (ColumnOnColumnRowData ccrd : listview.getItems()) {
                if (ccrd.getColumn().equals(nameOfColumn)) {
                    item = ccrd;
                    break;
                }
            }
        }
        
        return item;
    }

    private void fillUpListView() {
        WorkProc wp = new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                for (Column col : tableProvider.getColumns()) {
                    ColumnOnColumnRowData ccrd = new ColumnOnColumnRowData(col);
                    if(ccrd.getColumn().getTable() == null){
                        ccrd.getColumn().setTable(tableProvider);
                    }
                    listview.getItems().add(ccrd);
                }
            }
        };
        getController().addWorkToBackground(wp);
    }

    private void prepareListView() { 
        listview.setCellFactory((ListView<ColumnOnColumnRowData> p) -> new CheckBoxListCell<ColumnOnColumnRowData>(){
            {
                setSelectedStateCallback((ColumnOnColumnRowData p1) -> p1.selectedRow);
            }
            @Override
            public void updateItem(ColumnOnColumnRowData t, boolean bln) {
                super.updateItem(t, bln);
                if(bln){return;}
                setText(t.getColumn().getName());
            }
            
        });
    }
    
    private void cache_DestroyBuilderUI(){
        //here you can recycle your BuilderViewController to save space.
    }

    public class ColumnOnColumnRowData {

        private Column column;
        private SimpleBooleanProperty selectedRow = new SimpleBooleanProperty(false) {
                    
            @Override
            protected void invalidated() {
                super.invalidated();
                
                if (get()) {
                    QueryBuilderRowData qbr = new QueryBuilderRowData(BuilderViewController.this, ColumnOnColumnRowData.this);
                    qbr.setOnReflectChange(() -> {
                        onReflectChange();
                    });
                    tableView.getItems().add(qbr);
                    
                } else {
                    ArrayList<QueryBuilderRowData> qbrlist = new ArrayList<>(tableView.getItems());
                    for (QueryBuilderRowData querBuilderRD: qbrlist) {
                        if (querBuilderRD.getColumn() == ColumnOnColumnRowData.this.getColumn()) {
                            tableView.getItems().remove(querBuilderRD);
                        }
                    }
                }
            }
        };

        public ColumnOnColumnRowData(Column col) {
            ColumnOnColumnRowData.this.column = col;
            selectedRow.set(false);
        }

        public ColumnOnColumnRowData(Column col, boolean init) {
            ColumnOnColumnRowData.this.column = col;
            selectedRow.set(init);
        }

        public Column getColumn() {
            return ColumnOnColumnRowData.this.column;
        }

        public void setColumn(Column column) {
            ColumnOnColumnRowData.this.column = column;
        }

        public SimpleBooleanProperty getSelectedRow() {
            return selectedRow;
        }

    }
    
    
    
    
    public abstract MainController getController();
    public abstract void onReflectChange();
    
    public String getLabel(){
        return tableProvider == null ? "" : tableAlias != null ? tableAlias : tableProvider.getName();
    }
    
    public String toString() {
        return getLabel();
    }
}
