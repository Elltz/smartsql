package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 * @author Andrii Tsepushel &lt; a.tsepushel@gmail.com &gt;
 */
public class DiagnoticTableResultController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
       
    
    @FXML
    private TableView<AdministrationStatement> sessionsTable;

    @FXML
    private TableColumn<AdministrationStatement, String> tableCol;    
    @FXML
    private TableColumn<AdministrationStatement, String> opCol;
    @FXML
    private TableColumn<AdministrationStatement, String> msgTypeCol;
    @FXML
    private TableColumn<AdministrationStatement, String> msgTextCol;

    private Stage stage;

    @Getter
    public class AdministrationStatement {
        
        private String table;
        private String op;
        private String msgType;
        private String msgText;
        
        public AdministrationStatement(String table, String op, String msgType, String msgText) {
            this.table = table;
            this.op = op;
            this.msgType = msgType;
            this.msgText = msgText;
        }        
    }
    

    @FXML
    public void close(ActionEvent event) {        
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }


    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void init(List<Map<String, String>> data) {
        for (Map<String, String> m: data) {
            sessionsTable.getItems().add(new AdministrationStatement(m.get("table"), m.get("op"), m.get("msgType"), m.get("msgText")));
        }
                
        tableCol.setCellValueFactory(new PropertyValueFactory<>("table")); 
        opCol.setCellValueFactory(new PropertyValueFactory<>("op"));
        msgTypeCol.setCellValueFactory(new PropertyValueFactory<>("msgType")); 
        msgTextCol.setCellValueFactory(new PropertyValueFactory<>("msgText"));
    }

}
