/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import javafx.collections.ObservableList;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 */
public class MyFirstTable {

    /** The resulting PDF file. */
    public static final String RESULT
              = "d:/first_table.pdf";

    /**
     * Main method.
     *
     * @param args no arguments needed
     * @throws DocumentException
     * @throws IOException
     */
    public static void main(String[] args)
              throws IOException, DocumentException {
//        new MyFirstTable().createPdf(RESULT);
    }

    /**
     * Creates a PDF with information about the movies
     *
     * @param filename the name of the PDF file that will be created.
     * @throws DocumentException
     * @throws IOException
     */
    public static void createPdf(String filename, ObservableList<ObservableList> data)
              throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        // step 3
        document.open();
        // step 4
        document.add(createFirstTable(data));
        // step 5
        document.close();
    }

    /**
     * Creates our first table
     *
     * @return our first table
     */
    public static PdfPTable createFirstTable(ObservableList<ObservableList> data) {
        // a table with three columns
        PdfPTable table = new PdfPTable(data.get(0).size());
        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
//        cell = new PdfPCell(new Phrase("Cell with colspan 3"));
//        cell.setColspan(3);
//        table.addCell(cell);
//        // now we add a cell with rowspan 2
//        cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
//        cell.setRowspan(2);
//        table.addCell(cell);
//        // we add the four remaining cells with addCell()
//        table.addCell("row 1; cell 1");
//        table.addCell("row 1; cell 2");
//        table.addCell("row 2; cell 1");
//        table.addCell("row 2; cell 2");

        for (int r = 0; r < data.size(); r++) {

            //iterating c number of columns
            for (int c = 0; c < data.get(0).size(); c++) {
                cell = new PdfPCell(new Phrase("" + data.get(r).get(c)));
                table.addCell(cell);

//                cell.setCellValue("Cell " + r + " " + c);
//                cell.setCellValue("" + data.get(r).get(c));
                System.out.println("cell value " + r + " " + c + " " + data.get(r).get(c));
            }
        }

        return table;
    }
}
