package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import static np.com.ngopal.smart.sql.ui.controller.MainController.desspec;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author terah laweh (rumorsapp@gmail.com)
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ImportConnectionDetails extends BaseController implements Initializable{
    
    @Inject
    private ConnectionParamService connectionService;
    
    @FXML
    private TableColumn<ConnectionParams, Boolean> selectColumn;
    
    @FXML
    private CheckBox selectDesectCheckbox;
    
    @FXML
    private Button importButton;
   
    @FXML
    private TableView<ConnectionParams> tableview;
    
    @FXML
    private TableColumn connectionNameTable;
    
    @FXML
    private TableColumn hostTable;
    
    @FXML
    private TableColumn usernameTable;
    
    @FXML
    private TableColumn portTable;
    
    
    @FXML
    private TableColumn connectionTypeTable;
    
    @FXML
    private AnchorPane mainUI;        
            
    
    private Stage dialog;
    
    

    public void openFile(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select path to import connections");
        
        final File importFile = fileChooser.showOpenDialog(dialog);
        initFile(importFile);
    }
    
    
    private void initFile(File importFile) {
        if (importFile != null) {
            try {
                final byte[] encryptBytes = Files.readAllBytes(importFile.toPath());

                // decrypt data
                final DESKeySpec desKeySpec = new DESKeySpec(desspec.getBytes());
                final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
                final SecretKey secretKey = keyFactory.generateSecret(desKeySpec);

                final Cipher decryptCipher = Cipher.getInstance("DES");
                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);

                final byte[] bytes = decryptCipher.doFinal(encryptBytes);

                final String json = new String(bytes, "UTF-8");
                final Gson gson = new Gson();

                final List<ConnectionParams> params = gson.fromJson(json, new TypeToken<List<ConnectionParams>>() {
                }.getType());
                
                tableview.getItems().setAll(params);
                
            } catch (Throwable ex) {
                
                try {
                    // try parse as SqlYog export file
                    Stream<String> lines = Files.lines(importFile.toPath());
                    
                    ConnectionParams params = null;
                    for (String s: lines.collect(Collectors.toList())) {
                        if (s != null && s.startsWith("[Connection")) {
                            params = new ConnectionParams();
                            tableview.getItems().add(params);
                            
                        } else if (params != null) {
                            String[] values = s.split("=");
                            if (values.length == 2) {
                                switch (values[0].toUpperCase()) {

                                    case "NAME":
                                        params.setName(values[1]);
                                        break;

                                    case "HOST":
                                        params.setAddress(values[1]);
                                        break;

                                    case "USER":
                                        params.setUsername(values[1]);
                                        break;

                                    case "PORT":
                                        try {
                                            params.setPort(Integer.valueOf(values[1]));
                                        } catch (NumberFormatException ex0) {}
                                        break;

                                    case "DATABASE":
                                        String[] databases = values[1].split(";");
                                        List<String> list = new ArrayList<>();
                                        for (String d: databases) {
                                            list.add(d);
                                        }
                                        params.setDatabase(list);
                                        break;

                                    case "COMPRESSEDPROTOCOL":
                                        params.setUseCompressedProtocol(values[1].equals("1"));
                                        break;

                                    case "WAITTIMEOUTVALUE":
                                        try {
                                            params.setSessionIdleTimeout(Integer.valueOf(values[1]));
                                        } catch (NumberFormatException ex0) {}
                                        break;

                                    case "SSH":
                                        params.setUseSshTunneling(values[1].equals("1"));
                                        break;

                                    case "SSHUSER":
                                        params.setSshUser(values[1]);
                                        break;

                                    case "SSHHOST":
                                        params.setSshHost(values[1]);
                                        break;

                                    case "SSHPORT":
                                        try {
                                            params.setSshPort(Integer.valueOf(values[1]));
                                        } catch (NumberFormatException ex0) {}
                                        break;

                                    case "SSHPRIVATERADIO":
                                        params.setUseSshPrivateKey(values[1].equals("1"));
                                        break;

                                    case "SSHPRIVATEKEYPATH":
                                        params.setSshPrivateKey(values[1]);
                                        break;   

                                    case "INIT_COMMAND":
                                        params.setInitCommands(values[1]);
                                        break;   

                                    case "SQLMODE_VALUE":
                                        params.setCustomSqlMode(values[1]);
                                        break;
                                }
                            }
                        }
                    }
                    
                    
                } catch (Throwable ex0) {
                    DialogHelper.showError(getController(), "Error import", "There is an error while importing selected file", ex0);
                }
            }
            
            tableview.setDisable(false);
            importButton.setDisable(false);
            selectDesectCheckbox.setDisable(false);
            selectDesectCheckbox.setSelected(true);
        }
    }
    
    public void importConnections(ActionEvent event) {
        try {
            List<ConnectionParams> forImport = new ArrayList<>();
            for (ConnectionParams p: tableview.getItems()) {
                if (p.getSelected() != null && p.getSelected()) {
                    forImport.add(p.copyOnlyParams());
                }
            }

            if (!forImport.isEmpty()) {
                List<String> sameNames = new ArrayList<>();
                for (final ConnectionParams p : forImport) {
                    // Gson return HashTreeMap
                    if (p.getModuleParams() != null) {
                        p.setModuleParams(new HashMap<>(p.getModuleParams()));
                    }
                    
                    // Check saved names
                    String name = calcName(p.getName());
                    if (name != null) {
                        p.setName(name);
                        connectionService.save(p);
                    } else {
                        sameNames.add(p.getName());
                    }
                }

                String message = "Connection(s) imported successfully.";
                if (!sameNames.isEmpty()) {
                    message += " Next names ignored: " + String.join(", ", sameNames);
                }
                
                DialogHelper.showInfo(getController(), "Done", message, null);
                dialog.hide();
//                controller.closeAllTabs();
            }
        } catch (Throwable th) {
            DialogHelper.showError(getController(), "Error", "There is an error while importing connection", th);
        }
    }
    
    private String calcName(String name) {
        if (connectionService.checkConnectionParamsName(name)) {
            return null;//calcName(name, 1);
        } else {
            return name;
        }
    }
    
//    private String calcName(String name, int index) {
//        if (connectionService.checkConnectionParamsName(name + "_" + index)) {
//            return calcName(name, index + 1);
//        } else {
//            return name + "_" + index;
//        }
//    }
    
    public void cancel(ActionEvent event) {
        dialog.hide();
    }
    
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        selectColumn.setCellValueFactory(new PropertyValueFactory<>("selected"));
        selectColumn.setCellFactory((TableColumn<ConnectionParams, Boolean> p) -> {
            TableCell<ConnectionParams, Boolean> cell = new TableCell<ConnectionParams, Boolean>() {

                private CheckBox checkBox;
                @Override
                public void updateItem(Boolean item, boolean empty) {

                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {                        
                        if (checkBox == null) {
                            this.checkBox = new CheckBox();
                            this.checkBox.setFocusTraversable(false);
                            this.checkBox.setAlignment(Pos.CENTER);
                            setAlignment(Pos.CENTER);
                            
                            checkBox.setOnAction((ActionEvent event) -> {
                                ConnectionParams col = tableview.getItems().get(getIndex());
                                col.setSelected(checkBox.isSelected());
                            });                            
                        }

                        checkBox.setSelected(item != null ? item : false);

                        setGraphic(checkBox);
                    } else {
                        setGraphic(null);
                    }
                }
            };
            return cell;
        });
        connectionNameTable.setCellValueFactory(new PropertyValueFactory<>("name"));
        hostTable.setCellValueFactory(new PropertyValueFactory<>("address"));
        usernameTable.setCellValueFactory(new PropertyValueFactory<>("username"));
        portTable.setCellValueFactory(new PropertyValueFactory<>("port"));
        connectionTypeTable.setCellValueFactory(new PropertyValueFactory<>("connectionType"));
               
        
        selectDesectCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (ConnectionParams p: tableview.getItems()) {
                p.setSelected(newValue);
            }
            
            selectColumn.setVisible(false);
            selectColumn.setVisible(true);
        });
        
    }
    
    public void init(Window win){
        init(win, null);
    }
    
    public void init(Window win, File f){
        
        initFile(f);
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(600);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Import Connection Details");
        dialog.setScene(new Scene(getUI()));        
        dialog.showAndWait();
    }

    @Override
    public AnchorPane getUI() {     
        return mainUI;    
    }
    
}
