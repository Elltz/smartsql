package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.sql.SQLException;
import java.util.Collection;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.modules.SchemaSynchronizerModule;
import np.com.ngopal.smart.sql.structure.modules.ConnectionModuleInterface;
import np.com.ngopal.smart.sql.ui.controller.DatabaseSynchronizationWizardController;
import np.com.ngopal.smart.sql.ui.controller.ImportTableController;
import np.com.ngopal.smart.sql.ui.controller.ScheduleJobsController;
import np.com.ngopal.smart.sql.ui.controller.ScheduledBackupWizardController;
import np.com.ngopal.smart.sql.ui.controller.ScheduledReportWizardController;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.queryutils.QueryAreaWrapper;
import np.com.ngopal.smart.sql.structure.queryutils.RsSyntaxCompletionHelper;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
public class PowertoolsModule extends AbstractStandardModule {

    @Inject
    MysqlDBService dbService;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 5;
    }
    
    

    @Override
    public boolean install() {

        Menu synchronizationMenu = new Menu("Synchronization", new ImageView(SmartImageProvider.getInstance().getSync_image()));
        synchronizationMenu.getItems().addAll(getDatabaseSynchronizationWizardItem(), getVisualDataComparisonWizardItem(), getSchemaSynchronizationToolItem());
        
        Menu schedulerMenu = new Menu("Scheduler", new ImageView(SmartImageProvider.getInstance().getScheduler_image()));
        schedulerMenu.getItems().addAll(getSQLSchedulerAndReportingWizardItem(), getScheduledBackupsItem(), getScheduledJobsItem());        
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.POWERTOOLS, new MenuItem[]{
            synchronizationMenu,
            getImportExternalDataItem(),
            schedulerMenu,
            getRebuildTagsItem()
        });
            
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) { return true;}

    private void showScheduledReportDialog() {
        try {
            final Stage dialog = new Stage();
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            ScheduledReportWizardController controller = getTypedBaseController("ScheduledReportWizard");
            controller.init(dbService, session != null ? session : null);

            final Parent p = controller.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("SQL Scheduler and Reporting Wizard");

            dialog.showAndWait();
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }
    
    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private MenuItem getDatabaseSynchronizationWizardItem(){
        MenuItem databaseSynchronizationItem = new MenuItem("Database Synchronization Wizard",
        new ImageView(SmartImageProvider.getInstance().getDatabaseSyncWizard_image()));
        databaseSynchronizationItem.setAccelerator(new KeyCodeCombination(KeyCode.W, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN));
        
        databaseSynchronizationItem.setOnAction(new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent t) {
                DatabaseSynchronizationWizardController dswc = getTypedBaseController("DatabaseSynchronizationWizard");
                dswc.init(getController().getStage());
            }
            
        });
        return databaseSynchronizationItem;
    }
    
    private MenuItem getVisualDataComparisonWizardItem(){
        MenuItem visualDataComparisonWizardItem = new MenuItem("Visual Data Comparison Wizard",
        new ImageView(SmartImageProvider.getInstance().getDataVisual_image()));
        visualDataComparisonWizardItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN));
        visualDataComparisonWizardItem.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent t) {  
                showInfo("Not implemented", "Not implemented", null);
            }
        });
        return visualDataComparisonWizardItem;
    }
    
    private MenuItem getSchemaSynchronizationToolItem(){
        MenuItem schemaSynchronizationToolItem = new MenuItem("Schema Synchronization Tool",
        new ImageView(SmartImageProvider.getInstance().getSync_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        schemaSynchronizationToolItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.SHORTCUT_DOWN));
        schemaSynchronizationToolItem.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent t) { 
                
                ConnectionParams params = getController().getSelectedConnectionSession().get().getConnectionParam();
                ConnectionParams cp = params.copyOnlyParams();
                cp.setId(params.getId());

                cp.setSelectedModuleClassName(SchemaSynchronizerModule.class.getName());
                ((ConnectionModuleInterface) getMainController().getModuleByClassName(SchemaSynchronizerModule.class.getName()))
                        .connectToConnection(cp);
            }
            
        });
        
        return schemaSynchronizationToolItem;
    }
    
    private MenuItem getSQLSchedulerAndReportingWizardItem(){
        MenuItem SQLSchedulerAndReportingWizard = new MenuItem("SQL Scheduler And Reporting Wizard",
        new ImageView(SmartImageProvider.getInstance().getScheduleReport_image()));
        SQLSchedulerAndReportingWizard.setAccelerator(KeyCombination.keyCombination("Ctrl+Alt+N"));
        SQLSchedulerAndReportingWizard.disableProperty().bind(getController().getSelectedConnectionSession().isNull());
        SQLSchedulerAndReportingWizard.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showScheduledReportDialog();
            }

        });
        return SQLSchedulerAndReportingWizard;
    }
   
    
    private MenuItem getScheduledBackupsItem() {
        MenuItem ScheduledBackups = new MenuItem("Scheduled Backups",
                new ImageView(SmartImageProvider.getInstance().getScheduleBackUp_image()));
        ScheduledBackups.setAccelerator(KeyCombination.keyCombination("Ctrl+Alt+S"));
        ScheduledBackups.disableProperty().bind(getController().getSelectedConnectionSession().isNull());
        ScheduledBackups.setOnAction(new EventHandler() {

            @Override
            public void handle(Event t) {
                ConnectionSession session = getController().getSelectedConnectionSession().get();
                final Stage dialog = new Stage();
                ScheduledBackupWizardController controller = getTypedBaseController("ScheduledBackupWizard");

                Database database = null;

                ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session);
                if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {

                    Object itemValue = selectedTreeItem.getValue().getValue();
                    if (itemValue instanceof Database) {
                        database = (Database) itemValue;
                    }
                }
                try {
                    controller.init(session.getService(), session.getConnectionParam(), database);
                    final Parent p = controller.getUI();
                    dialog.initStyle(StageStyle.UTILITY);

                    final Scene scene = new Scene(p);
                    dialog.setScene(scene);
                    dialog.initModality(Modality.WINDOW_MODAL);
                    dialog.initOwner(getController().getStage());
                    dialog.setTitle("Export Data As Batch Script Wizard");

                    dialog.showAndWait();
                } catch (SQLException ex) {
                    showError("Error", ex.getMessage(), ex);
                }
            }
        });
        return ScheduledBackups;
    }
    
    private MenuItem getImportExternalDataItem(){
        MenuItem importExternalData = new MenuItem("Import",
                new ImageView(SmartImageProvider.getInstance().getDatabaseExternalImport()));
        importExternalData.setUserData("Import External Data");        
        importExternalData.setAccelerator(KeyCombination.keyCombination("Ctrl+Alt+O"));
        importExternalData.disableProperty().bind(getController().getSelectedConnectionSession().isNull());
        importExternalData.setOnAction(new EventHandler() {

            @Override
            public void handle(Event t) {
                final Stage dialog = new Stage();
                ImportTableController controller = getTypedBaseController("ImportTable");
                controller.init(getController().getSelectedConnectionSession().get(), null);

                final Scene scene = new Scene(controller.getUI());

                dialog.setScene(scene);
                dialog.initStyle(StageStyle.UTILITY);
                dialog.initModality(Modality.WINDOW_MODAL);
                dialog.initOwner(getController().getStage());
                dialog.setTitle("Import External Data Wizard");
                dialog.showAndWait();
                
                t.consume();
            }

        });
        return importExternalData;
    }
    
    private MenuItem getScheduledJobsItem(){
        MenuItem scheduledJobs = new MenuItem("Scheduled Jobs",
        new ImageView(SmartImageProvider.getInstance().getScheduleJobs_image()));
        scheduledJobs.setOnAction(new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent t) { 
                showInfo("Not implemented", "Not implemented", null);
                ScheduleJobsController sjc = getTypedBaseController("ScheduledJobs");
                sjc.init(getController().getStage());
            }
            
        });
        return scheduledJobs;
    }
    
    private MenuItem getRebuildTagsItem(){
        MenuItem rebuildTags = new MenuItem("Rebuild Tags",
        new ImageView(SmartImageProvider.getInstance().getRebuildTags_image()));
        rebuildTags.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
//                QueryTabModule qtm = (QueryTabModule) getMainController().getModuleByClassName(
//                        QueryTabModule.class.getName());
//                if( qtm.getSessionCodeArea() != null ){       
                QueryTabModule queryTabModule = (QueryTabModule) ((MainController) getController()).getModuleByClassName(QueryTabModule.class.getName());
                Collection<QueryAreaWrapper> list = queryTabModule.getAllCodeAreas();
                showInfo("RE-BUILD STATUS", String.valueOf(RsSyntaxCompletionHelper.getInstance().rebuildTags(getController().getSelectedConnectionSession().get().getConnectionParam(), list))
                        + " Tags have been refreshed", null);
//                }else{ showInfo("RE-BUILD STATUS", "Nothing to rebuild", null); }
            }
        });
        return rebuildTags;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
