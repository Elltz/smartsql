package np.com.ngopal.smart.sql.ui.visualexplain;

import java.util.List;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
@Getter
public class VisualFactory {
    
    public static VisualUnion createUnion(double x, double y, double width, double height, String name, String key, String tableType, List<VisualTable> tables) {
        VisualUnion union = new VisualUnion();
        union.x = x;
        union.y = y;
        union.width = width;
        union.height = height;
        union.tables = tables;
        union.type = VisualTable.TableType.UNION;
        union.name = name;
        union.key = key;
        union.tableType = tableType;
        
        return union;
    }
    
    public static VisualJoin createJoin(double x, double y, Object joinComponent) {
        VisualJoin join = new VisualJoin();
        join.x = x;
        join.y = y;
        join.join = joinComponent;
        
        return join;
    }
    
    public static VisualGroupBy createGroupBy(double x, double y, double width, double height, String name, String key, Color color) {
        VisualGroupBy groupBy = new VisualGroupBy();
        groupBy.x = x;
        groupBy.y = y;
        groupBy.width = width;
        groupBy.height = height;
        groupBy.name = name;
        groupBy.key = key;
        groupBy.color = color;
        
        return groupBy;
    }
    
    public static VisualOrderBy createOrderBy(double x, double y, double width, double height, String name, String key, Color color) {
        VisualOrderBy orderBy = new VisualOrderBy();
        orderBy.x = x;
        orderBy.y = y;
        orderBy.width = width;
        orderBy.height = height;
        orderBy.name = name;
        orderBy.key = key;
        orderBy.color = color;
        
        return orderBy;
    }
    
    public static VisualTable createTable(double x, double y, double width, double height, String name, String key, VisualTable.TableType type, String extra, VisualGroupBy groupBy, VisualOrderBy orderBy, Double cost, Long rowsNumber, Object prevComponent) {
        VisualTable table = new VisualTable();
        table.x = x;
        table.y = y;
        table.width = width;
        table.height = height;
        table.name = name;
        table.key = key;
        table.type = type;
        table.cost = cost;
        table.groupBy = groupBy;
        table.orderBy = orderBy;
        table.prevComponent = prevComponent;
        table.rowsNumber = rowsNumber;
        table.extra = extra;
        
        return table;
    }
}
