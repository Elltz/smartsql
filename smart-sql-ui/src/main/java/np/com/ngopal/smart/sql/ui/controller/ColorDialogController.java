/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ColorDialogController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Label labelInfo;

    @FXML
    private ColorPicker foreColorPicker;

    @FXML
    private ColorPicker backColorPicker;
    
    @FXML
    private HBox backgroundHolder;

    @FXML
    private Button applyDefault;

    private Dialog<Void> window;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void create() {
        window = new Dialog();
        window.initStyle(StageStyle.UNIFIED);
        window.initModality(Modality.WINDOW_MODAL);
        window.getDialogPane().setContent(mainUI);
        window.setTitle("Object Browser Color");

        applyDefault.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                defaultsSet = true;
                window.close();
            }
        });

    }
    private boolean defaultsSet = false;
    private Color[] defaultColors, resColors;

    public void setPaintDefaults(Color... paints) {

        if (paints != null) {
            defaultColors = paints;
            foreColorPicker.setValue(paints[0]);
            backColorPicker.setValue(paints[1]);
        }
    }

    public void show_hide() {
        if (window.isShowing()) {
            window.close();
        } else {
            //i want the button types to be the last item in the scenegraph
            //for the flowpane
            addButtonTypes();
            //that was the initial idea, but now i do not need them 
            mainUI.setPrefHeight(FlowPane.USE_COMPUTED_SIZE);
            window.showAndWait();
        }
    }

    @Override
    public Stage getStage() {
        return (Stage) window.getOwner();
    }

    private void addButtonTypes() {
        window.getDialogPane().getButtonTypes().addAll(ButtonType.OK,
                ButtonType.CANCEL);
        ((Button) window.getDialogPane().lookupButton(ButtonType.CANCEL)).
                setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        resColors = null;
                        window.close();
                    }
                });

        ((Button) window.getDialogPane().lookupButton(ButtonType.OK)).
                setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        //it is automatic so i just close it.
                        resColors = new Color[]{foreColorPicker.getValue(),
                            backColorPicker.getValue()};
                        window.close();
                    }
                });
    }

    public Object[] getResults(String id) {
        if (resColors == null && !defaultsSet) {
            return null;
        }
        if (defaultsSet) {
            return new Object[]{null, null, id, defaultsSet};
        }
        return new Object[]{resColors[0], resColors[1], id, defaultsSet};
    }
    
    public void showOnlyOne(){
        mainUI.getChildren().remove(backgroundHolder);
        mainUI.getChildren().remove(labelInfo);
    }

}
