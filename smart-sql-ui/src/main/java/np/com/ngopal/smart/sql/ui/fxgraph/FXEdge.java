/*
 * (C) Copyright 2012 JavaFXGraph (http://code.google.com/p/javafxgraph/).
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3.0 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package np.com.ngopal.smart.sql.ui.fxgraph;

import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.RectangleBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Paint;
import javafx.scene.shape.PathElement;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerLayerTittledPane;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

public class FXEdge {

    FXGraph graph;
    FXNode source;
    FXNode destination;
    Node displayShape;
    List<FXEdgeWayPoint> wayPoints = new ArrayList<>();
    Map<FXEdgeWayPoint, Node> wayPointHandles = new HashMap<>();

    private static final Color EDGE_COLOR = Color.web("ffc978", 0.7);
    public static final Background EDGE_BACKGROUND = new Background(new BackgroundFill(EDGE_COLOR, CornerRadii.EMPTY, Insets.EMPTY));
    
    private static final double MARK__SHIFT_FROM_WIDTH  = 10;
    private static final double MARK__SHIFT_TO_WIDTH    = 8;
    private static final double MARK__LINE_WIDTH        = 5;
    private static final double MARK__TRIANGLE_WIDTH    = 12;
    
    
    int soureEdgesCount = 0;
    int destinationEdgesCount = 0;
    int destinationSourceEdgesCount = 0;
    int sourceIndex = 0;
    int destinationIndex = 0;
    EdgeType edgeType;
        
    Pane contentPane;
    Path hoverPath = new Path();
    Paint lastHoverColor;
    Background lastHoverBackground;
    
    Object relation;
    
    public enum EdgeType {
        IDENTINFYING__ONE_TO_ONE,
        NON_IDENTINFYING__ONE_TO_ONE,
        IDENTINFYING__MANY_TO_ONE,
        NON_IDENTINFYING__MANY_TO_ONE,
        MANY_TO_MANY,
    }
    enum EdgeOrientation {
        LEFT_TO_RIGHT,
//        LEFT_TO_BOTTOM,
//        LEFT_TO_TOP,
        TOP_TO_LEFT,
        TOP_TO_RIGHT,
        TOP_TO_BOTTOM,
//        RIGHT_TO_TOP,
        RIGHT_TO_LEFT,
//        RIGHT_TO_BOTTOM,
        BOTTOM_TO_TOP,
        BOTTOM_TO_LEFT,
        BOTTOM_TO_RIGHT       
    }
    
    public FXEdge(FXGraph aGraph, Object relation, FXNode aSource, int soureEdgesCount, int sourceIndex, FXNode aDestination, int destinationEdgesCount, int destinationSourceEdgesCount, int destinationIndex, EdgeType edgeType) {
        graph = aGraph;
        source = aSource;
        destination = aDestination;
        this.soureEdgesCount = soureEdgesCount;
        this.destinationEdgesCount = destinationEdgesCount;
        this.destinationSourceEdgesCount = destinationSourceEdgesCount;
        this.sourceIndex = sourceIndex;
        this.destinationIndex = destinationIndex;
        this.edgeType = edgeType;
        this.relation = relation;
        
        hoverPath.setVisible(false);
    }

    public FXNode getSource() {
        return source;
    }

    public FXNode getDestination() {
        return destination;
    }

    
    
    public int getDestinationIndex() {
        return destinationIndex;
    }
    public void setDestinationIndex(int index) {
        this.destinationIndex = index;
    }

    public Node compileDisplayShapeFor(FXEdgeWayPoint aWayPoint, double aZoomLevel) {
        RectangleBuilder theBuilder = RectangleBuilder.create();
//        theBuilder.width(1).height(1);
        Node theNode = theBuilder.build();
        theNode.setScaleX(aZoomLevel);
        theNode.setScaleY(aZoomLevel);
        theNode.setLayoutX((aWayPoint.positionX - 2) * aZoomLevel);
        theNode.setLayoutY((aWayPoint.positionY - 2) * aZoomLevel);
        theNode.setUserData(aWayPoint);
        return theNode;
    }
    
    private EdgeOrientation computeOrientation(Bounds theSourceBounds, Bounds theDestinationBounds) {
     
        // first check X coordinate
        
        if (theSourceBounds.getMinX() < theDestinationBounds.getMinX()) {
            
            if (theSourceBounds.getMinY() < theDestinationBounds.getMinY()) {
                
                if (theSourceBounds.getMaxY() + theSourceBounds.getHeight() / 2 < theDestinationBounds.getMinY()) {
                    
                    if (theSourceBounds.getMaxY() + theSourceBounds.getHeight() > theDestinationBounds.getMinY()) {
                        
                        if (theDestinationBounds.getMinX() - 20 > theSourceBounds.getMinX() + theSourceBounds.getWidth()/ 2) {
                            
                            return EdgeOrientation.BOTTOM_TO_LEFT;
                        } else {
                            
                            return EdgeOrientation.BOTTOM_TO_TOP;
                        }
                        
                    } else {
                        return EdgeOrientation.BOTTOM_TO_TOP;
                    }
                } else {  
                    
                    if (theSourceBounds.getMaxY() + 20 > theDestinationBounds.getMinY() + theDestinationBounds.getHeight() / 2) {
                        return EdgeOrientation.RIGHT_TO_LEFT;
                    } else {
                        
                        if (theDestinationBounds.getMinX() - 20 > theSourceBounds.getMinX() + theSourceBounds.getWidth()/ 2) {
                            
                            return EdgeOrientation.BOTTOM_TO_LEFT;
                        } else {
                            
                            return EdgeOrientation.BOTTOM_TO_TOP;
                        }
                    }              
                }
                
            } else {
                
                if (theSourceBounds.getMinY() - theSourceBounds.getHeight() / 2 > theDestinationBounds.getMaxY()) {
                    
                    if (theSourceBounds.getMinY() - theSourceBounds.getHeight() < theDestinationBounds.getMaxY  ()) {
                        
                        if (theDestinationBounds.getMinX() - 20 > theSourceBounds.getMinX() + theSourceBounds.getWidth() / 2) {
                            
                            return EdgeOrientation.TOP_TO_LEFT;
                        } else {
                            
                            return EdgeOrientation.TOP_TO_BOTTOM;
                        }
                    } else {
                        return EdgeOrientation.TOP_TO_BOTTOM;
                    }
                } else {
                    
                    if (theSourceBounds.getMinY() - 20 < theDestinationBounds.getMinY() + theDestinationBounds.getHeight() / 2) {
                        return EdgeOrientation.RIGHT_TO_LEFT;
                    } else {
                        if (theDestinationBounds.getMinX() - 20 > theSourceBounds.getMinX() + theSourceBounds.getWidth()/ 2) {
                            
                            return EdgeOrientation.TOP_TO_LEFT;
                        } else {
                            
                            return EdgeOrientation.TOP_TO_BOTTOM;
                        }
                        
                    }
                }                
            }
            
        } else {
            
            if (theSourceBounds.getMinY() < theDestinationBounds.getMinY()) {
                
                if (theSourceBounds.getMaxY() + theSourceBounds.getHeight() / 2 < theDestinationBounds.getMinY()) {
                    
                    if (theSourceBounds.getMaxY() + theSourceBounds.getHeight() > theDestinationBounds.getMinY()) {
                        
                        if (theDestinationBounds.getMaxX() + 20 < theSourceBounds.getMinX() + theSourceBounds.getWidth()/ 2) {
                            
                            return EdgeOrientation.BOTTOM_TO_RIGHT;
                        } else {
                            
                            return EdgeOrientation.BOTTOM_TO_TOP;
                        }
                        
                    } else {
                        return EdgeOrientation.BOTTOM_TO_TOP;
                    }
                } else {  
                    
                    if (theSourceBounds.getMaxY() + 20 > theDestinationBounds.getMinY() + theDestinationBounds.getHeight() / 2) {
                        return EdgeOrientation.LEFT_TO_RIGHT;
                    } else {
                        
                        if (theDestinationBounds.getMaxX() + 20 < theSourceBounds.getMinX() + theSourceBounds.getWidth()/ 2) {
                            
                            return EdgeOrientation.BOTTOM_TO_RIGHT;
                        } else {
                            
                            return EdgeOrientation.BOTTOM_TO_TOP;
                        }
                    }              
                }
                
            } else {
                
                if (theSourceBounds.getMinY() - theSourceBounds.getHeight() / 2 > theDestinationBounds.getMaxY()) {
                    
                    if (theSourceBounds.getMinY() - theSourceBounds.getHeight() < theDestinationBounds.getMaxY()) {
                        
                        if (theDestinationBounds.getMaxX() + 20 < theSourceBounds.getMinX() + theSourceBounds.getWidth() / 2) {
                            
                            return EdgeOrientation.TOP_TO_RIGHT;
                        } else {
                            
                            return EdgeOrientation.TOP_TO_BOTTOM;
                        }
                    } else {
                        return EdgeOrientation.TOP_TO_BOTTOM;
                    }
                } else {
                    
                    if (theSourceBounds.getMinY() - 20 < theDestinationBounds.getMinY() + theDestinationBounds.getHeight() / 2) {
                        return EdgeOrientation.LEFT_TO_RIGHT;
                    } else {
                        if (theDestinationBounds.getMaxX() + 20 > theSourceBounds.getMinX() + theSourceBounds.getWidth()/ 2) {
                            
                            return EdgeOrientation.TOP_TO_BOTTOM;
                        } else {
                            
                            return EdgeOrientation.TOP_TO_RIGHT;
                        }
                        
                    }
                }                
            }            
        }
    }

    public void computeDisplayShape(double aCurrentZoomLevel) {
        
        Path path = new Path();
        path.setUserData(this);
        path.setStroke(Color.BLACK);
        path.setStrokeWidth(1.5);
        if (edgeType == EdgeType.NON_IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.NON_IDENTINFYING__ONE_TO_ONE) {
            path.getStrokeDashArray().addAll(10.0, 5.0);
        }
        
        path.setOnMouseEntered((MouseEvent event) -> {
            drawEdgePath(true, EDGE_COLOR, EDGE_BACKGROUND);            
        });
        
        path.setOnMouseExited((MouseEvent event) -> {
            drawEdgePath(false, EDGE_COLOR, EDGE_BACKGROUND);
        });
        

        // From the middle of the source
        Bounds theSourceBounds = source.wrappedNode.getBoundsInParent();
        
        // To the middle of the destination
        Bounds theDestinationBounds = destination.wrappedNode.getBoundsInParent();
        
        
        wayPointHandles.clear();
        wayPoints.clear();
        
        EdgeOrientation orientation = computeOrientation(theSourceBounds, theDestinationBounds);
        switch (orientation) {
            
            case LEFT_TO_RIGHT:
                computeLeftToRight(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
                
//            case LEFT_TO_BOTTOM:
//                computeLeftToBottom(thePath, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
//                break;
//                
            case TOP_TO_RIGHT:
                computeTopToRight(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
//                
            case TOP_TO_LEFT:
                computeTopToLeft(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
                
            case TOP_TO_BOTTOM:
                computeTopToBottom(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
                
            case RIGHT_TO_LEFT:
                computeRightToLeft(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
                
//            case RIGHT_TO_TOP:
//                computeRightToTop(thePath, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
//                break;
                
//            case RIGHT_TO_BOTTOM:
//                computeRightToBottom(thePath, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
//                break;
                
            case BOTTOM_TO_TOP:
                computeBottomToTop(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
                
            case BOTTOM_TO_LEFT:
                computeBottomToLeft(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
                
            case BOTTOM_TO_RIGHT:
                computeBottomToRight(path, theSourceBounds, theDestinationBounds, aCurrentZoomLevel);
                break;
        }
        
        displayShape = path;
    }

    public synchronized void drawEdgePath(boolean show, Paint color, Background background) {
        if (show) {
            if (contentPane != null && !hoverPath.isVisible() && color != null) {
                
                int index = 0;
                for (Node n: contentPane.getChildren()) {
                    if (!(n instanceof DesignerLayerTittledPane)) {
                        break;
                    }

                    index++;
                }

                hoverPath.getElements().clear();
                
                lastHoverColor = color;
                lastHoverBackground = background;
        
                // no need last 10 elemetns - its marks
                for (int i = 0; i < ((Path)displayShape).getElements().size() - 10; i++) {
                    PathElement pe = ((Path)displayShape).getElements().get(i);

                    if (pe instanceof MoveTo) {
                        MoveTo m = new MoveTo(((MoveTo)pe).getX(), ((MoveTo)pe).getY());
                        hoverPath.getElements().add(m);
                    }

                    if (pe instanceof LineTo) {
                        LineTo l = new LineTo(((LineTo)pe).getX(), ((LineTo)pe).getY());
                        hoverPath.getElements().add(l);
                    }
                }
                
                hoverPath.setStroke(color);
                hoverPath.setStrokeWidth(10);
                hoverPath.setVisible(true);
                
                if (!contentPane.getChildren().contains(hoverPath)) {
                    contentPane.getChildren().add(index, hoverPath);
                }
                
                
                
                // change foreground color of referenced columns
                if (relation instanceof DesignerForeignKey) {
                    DesignerForeignKey fk = (DesignerForeignKey)relation;
                    for (DesignerColumn c: fk.columns()) {
                        c.backgroundColor().set(background);
                        DesignerColumn refC = fk.referencedColumns().get(c);
                        if (refC != null) {
                            refC.backgroundColor().set(background);
                        }
                    }
                }
            }
        } else if (contentPane != null) {
            
            hoverPath.setVisible(false);            
            
            lastHoverColor = null;
            lastHoverBackground = null;
                
            // change foreground color of referenced columns
            if (relation instanceof DesignerForeignKey) {
                DesignerForeignKey fk = (DesignerForeignKey)relation;
                for (DesignerColumn c: fk.columns()) {
                    c.backgroundColor().set(DesignerColumn.DEFAULT_BACKGROUND);
                    DesignerColumn refC = fk.referencedColumns().get(c);
                    if (refC != null) {
                        refC.backgroundColor().set(DesignerColumn.DEFAULT_BACKGROUND);
                    }
                }
            }
        }
    }
    
    
    
    
    private void computeLeftToRight(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromLeft(theSourceBounds);        
        LineTo lineTo = computeToRight(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, (moveTo.getX() + lineTo.getX()) / 2, moveTo.getY()));
        wayPoints.add(new FXEdgeWayPoint(this, (moveTo.getX() + lineTo.getX()) / 2, lineTo.getY()));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromLeftMark(thePath, moveTo);
        computeToRightMark(thePath, lineTo);
    }
    
    
    private void computeLeftToBottom(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromLeft(theSourceBounds);      
        LineTo lineTo = computeToBottom(theDestinationBounds);  
        
        // create FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, lineTo.getX(), moveTo.getY()));        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromLeftMark(thePath, moveTo);
        computeToBottomMark(thePath, lineTo);
    }
    
    private void computeLeftToTop(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromLeft(theSourceBounds);       
        LineTo lineTo = computeToTop(theDestinationBounds);  
        
        // create FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, lineTo.getX(), moveTo.getY()));        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromLeftMark(thePath, moveTo);
        computeToTopMark(thePath, lineTo);
    }
    
    private void computeTopToLeft(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromTop(theSourceBounds);         
        LineTo lineTo = computeToLeft(theDestinationBounds);  
        
        // create FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, moveTo.getX(), lineTo.getY()));
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromTopMark(thePath, moveTo);
        computeToLeftMark(thePath, lineTo);
    }
    
    private void computeTopToRight(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromTop(theSourceBounds);        
        LineTo lineTo = computeToRight(theDestinationBounds);  
        
        // create FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, moveTo.getX(), lineTo.getY()));
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromTopMark(thePath, moveTo);
        computeToRightMark(thePath, lineTo);
    }
    
    private void computeTopToBottom(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromTop(theSourceBounds);       
        LineTo lineTo = computeToBottom(theDestinationBounds);  
        
        // create FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, moveTo.getX(), (lineTo.getY() + moveTo.getY()) / 2));
        wayPoints.add(new FXEdgeWayPoint(this, lineTo.getX(), (lineTo.getY() + moveTo.getY()) / 2));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromTopMark(thePath, moveTo);
        computeToBottomMark(thePath, lineTo);
    }
    
    
    private void computeRightToLeft(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromRight(theSourceBounds);         
        LineTo lineTo = computeToLeft(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, (moveTo.getX() + lineTo.getX()) / 2, moveTo.getY()));
        wayPoints.add(new FXEdgeWayPoint(this, (moveTo.getX() + lineTo.getX()) / 2, lineTo.getY()));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromRightMark(thePath, moveTo);
        computeToLeftMark(thePath, lineTo);
    }
    
    private void computeRightToTop(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromRight(theSourceBounds);        
        LineTo lineTo = computeToTop(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, lineTo.getX(), moveTo.getY()));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromRightMark(thePath, moveTo);
        computeToTopMark(thePath, lineTo);
    }
    
    private void computeRightToBottom(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromRight(theSourceBounds);        
        LineTo lineTo = computeToBottom(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, lineTo.getX(), moveTo.getY()));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromRightMark(thePath, moveTo);
        computeToBottomMark(thePath, lineTo);
    }
    
    private void computeBottomToTop(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromBottom(theSourceBounds);        
        LineTo lineTo = computeToTop(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, moveTo.getX(), (lineTo.getY() + moveTo.getY()) / 2));
        wayPoints.add(new FXEdgeWayPoint(this, lineTo.getX(), (lineTo.getY() + moveTo.getY()) / 2));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromBottomMark(thePath, moveTo);
        computeToTopMark(thePath, lineTo);
    }
    
    private void computeBottomToLeft(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromBottom(theSourceBounds);        
        LineTo lineTo = computeToLeft(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, moveTo.getX(), lineTo.getY()));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromBottomMark(thePath, moveTo);
        computeToLeftMark(thePath, lineTo);
    }
    
    private void computeBottomToRight(Path thePath, Bounds theSourceBounds, Bounds theDestinationBounds, double aCurrentZoomLevel) {
        
        MoveTo moveTo = computeFromBottom(theSourceBounds);         
        LineTo lineTo = computeToRight(theDestinationBounds);  
        
        // create two FXEdgeWayPoint 
        wayPoints.add(new FXEdgeWayPoint(this, moveTo.getX(), lineTo.getY()));
        
        
        thePath.getElements().add(moveTo);
        
        for (FXEdgeWayPoint theWayPoint : wayPoints) {

            wayPointHandles.put(theWayPoint, compileDisplayShapeFor(theWayPoint, aCurrentZoomLevel));

            LineTo theLineTo = new LineTo(theWayPoint.positionX * aCurrentZoomLevel, theWayPoint.positionY * aCurrentZoomLevel);
            thePath.getElements().add(theLineTo);
        }        
        
        thePath.getElements().add(lineTo);
        

        // draw edge type data
        computeFromBottomMark(thePath, moveTo);
        computeToRightMark(thePath, lineTo);
    }
    
    private double calcPosition(double coord, double length, int count, int position) {
        
        double w = length / (double)count;
        double[] array = new double[count];
        
        for (int i = 0; i < count; i++) {
            array[i] = coord + w / 2.0 + w * i;
        }
        
        return position < array.length ? array[position] : coord;
    }
    
    
    private MoveTo computeFromLeft(Bounds theSourceBounds) {
        double x = theSourceBounds.getMinX();
        double y = calcPosition(theSourceBounds.getMinY(), theSourceBounds.getHeight(), soureEdgesCount, sourceIndex);

        MoveTo theMoveTo = new MoveTo(x, y);

        return theMoveTo;
    }
    
    private MoveTo computeFromTop(Bounds theSourceBounds) {
        double x = calcPosition(theSourceBounds.getMinX(), theSourceBounds.getWidth(), soureEdgesCount, sourceIndex);
        double y = theSourceBounds.getMinY();
        
        MoveTo theMoveTo = new MoveTo(x, y);
        
        return theMoveTo;
    }
    
    private MoveTo computeFromRight(Bounds theSourceBounds) {
        double x = theSourceBounds.getMaxX();
        double y = calcPosition(theSourceBounds.getMinY(), theSourceBounds.getHeight(), soureEdgesCount, sourceIndex);
        
        MoveTo theMoveTo = new MoveTo(x, y);
        
        return theMoveTo;
    }
    
    private MoveTo computeFromBottom(Bounds theSourceBounds) {
        double x = calcPosition(theSourceBounds.getMinX(),  theSourceBounds.getWidth(), soureEdgesCount, sourceIndex);
        double y = theSourceBounds.getMaxY();
        
        MoveTo theMoveTo = new MoveTo(x, y);
        
        return theMoveTo;
    }
    
    private LineTo computeToRight(Bounds theDestinationBounds) {
        double x = theDestinationBounds.getMaxX();
        double y = calcPosition(theDestinationBounds.getMinY(), theDestinationBounds.getHeight(), destinationEdgesCount + destinationSourceEdgesCount, destinationSourceEdgesCount + destinationIndex);
        
        LineTo theLineTo = new LineTo(x, y);
        
        return theLineTo;
    }
    
    private LineTo computeToLeft(Bounds theDestinationBounds) {
        double x = theDestinationBounds.getMinX();
        double y = calcPosition(theDestinationBounds.getMinY(), theDestinationBounds.getHeight(), destinationEdgesCount + destinationSourceEdgesCount, destinationSourceEdgesCount + destinationIndex);
        
        LineTo theLineTo = new LineTo(x, y);
        
        return theLineTo;
    }
    
    private LineTo computeToBottom(Bounds theDestinationBounds) {
        double x = calcPosition(theDestinationBounds.getMinX(), theDestinationBounds.getWidth(), destinationEdgesCount + destinationSourceEdgesCount, destinationSourceEdgesCount + destinationIndex);
        double y = theDestinationBounds.getMaxY();
        
        LineTo theLineTo = new LineTo(x, y);
        
        return theLineTo;
    }
    
    private LineTo computeToTop(Bounds theDestinationBounds) {
        double x = calcPosition(theDestinationBounds.getMinX(), theDestinationBounds.getWidth(), destinationEdgesCount + destinationSourceEdgesCount, destinationSourceEdgesCount + destinationIndex);
        double y = theDestinationBounds.getMinY();
        
        LineTo theLineTo = new LineTo(x, y);
        
        return theLineTo;
    }
    
    
    
    private void computeFromRightMark(Path thePath, MoveTo moveTo) {
        
        if (edgeType == EdgeType.IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.NON_IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(moveTo.getX(), moveTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__SHIFT_FROM_WIDTH, moveTo.getY()));
            thePath.getElements().add(new MoveTo(moveTo.getX() + MARK__SHIFT_FROM_WIDTH, moveTo.getY()));
            thePath.getElements().add(new LineTo(moveTo.getX(), moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX() + MARK__TRIANGLE_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__TRIANGLE_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX() + MARK__SHIFT_TO_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__SHIFT_TO_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
        }
    }
    
    private void computeFromTopMark(Path thePath, MoveTo moveTo) {
        if (edgeType == EdgeType.IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.NON_IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY()));
            thePath.getElements().add(new LineTo(moveTo.getX(), moveTo.getY() - MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX(), moveTo.getY() - MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY()));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() - MARK__TRIANGLE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() - MARK__TRIANGLE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() - MARK__SHIFT_TO_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() - MARK__SHIFT_TO_WIDTH));
        }
    }            
    
    private void computeFromLeftMark(Path thePath, MoveTo moveTo) {
        if (edgeType == EdgeType.IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.NON_IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(moveTo.getX(), moveTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() - MARK__SHIFT_FROM_WIDTH, moveTo.getY()));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__SHIFT_FROM_WIDTH, moveTo.getY()));
            thePath.getElements().add(new LineTo(moveTo.getX(), moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__TRIANGLE_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() - MARK__TRIANGLE_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__SHIFT_TO_WIDTH, moveTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() - MARK__SHIFT_TO_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
        }
    }
    
    private void computeFromBottomMark(Path thePath, MoveTo moveTo) {
        if (edgeType == EdgeType.IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.NON_IDENTINFYING__MANY_TO_ONE || edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY()));
            thePath.getElements().add(new LineTo(moveTo.getX(), moveTo.getY() + MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX(), moveTo.getY() + MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY()));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() + MARK__TRIANGLE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() + MARK__TRIANGLE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(moveTo.getX() - MARK__LINE_WIDTH, moveTo.getY() + MARK__SHIFT_TO_WIDTH));
            thePath.getElements().add(new LineTo(moveTo.getX() + MARK__LINE_WIDTH, moveTo.getY() + MARK__SHIFT_TO_WIDTH));
        }
    }
    
    
    private void computeToRightMark(Path thePath, LineTo lineTo) {
        if (edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(lineTo.getX(), lineTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__SHIFT_FROM_WIDTH, lineTo.getY()));
            thePath.getElements().add(new MoveTo(lineTo.getX() + MARK__SHIFT_FROM_WIDTH, lineTo.getY()));
            thePath.getElements().add(new LineTo(lineTo.getX(), lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX() + MARK__TRIANGLE_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__TRIANGLE_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX() + MARK__SHIFT_TO_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__SHIFT_TO_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
        }
    }
    
    private void computeToLeftMark(Path thePath, LineTo lineTo) {
        if (edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(lineTo.getX(), lineTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() - MARK__SHIFT_FROM_WIDTH, lineTo.getY()));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__SHIFT_FROM_WIDTH, lineTo.getY()));
            thePath.getElements().add(new LineTo(lineTo.getX(), lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__TRIANGLE_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() - MARK__TRIANGLE_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__SHIFT_TO_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() - MARK__SHIFT_TO_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
        }
    }
    
    private void computeToBottomMark(Path thePath, LineTo lineTo) {
        if (edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY()));
            thePath.getElements().add(new LineTo(lineTo.getX(), lineTo.getY() + MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX(), lineTo.getY() + MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY()));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() + MARK__TRIANGLE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() + MARK__TRIANGLE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() + MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() + MARK__SHIFT_TO_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() + MARK__SHIFT_TO_WIDTH));
        }
    }
    
    private void computeToTopMark(Path thePath, LineTo lineTo) {
        if (edgeType == EdgeType.MANY_TO_MANY) {
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY()));
            thePath.getElements().add(new LineTo(lineTo.getX(), lineTo.getY() - MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX(), lineTo.getY() - MARK__SHIFT_FROM_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY()));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() - MARK__TRIANGLE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() - MARK__TRIANGLE_WIDTH));
        } else {
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() - MARK__LINE_WIDTH));
            thePath.getElements().add(new MoveTo(lineTo.getX() - MARK__LINE_WIDTH, lineTo.getY() - MARK__SHIFT_TO_WIDTH));
            thePath.getElements().add(new LineTo(lineTo.getX() + MARK__LINE_WIDTH, lineTo.getY() - MARK__SHIFT_TO_WIDTH));
        }
    }
    
    public void removeAllNodes(Pane aPane) {
        aPane.getChildren().remove(displayShape);
        aPane.getChildren().removeAll(wayPointHandles.values());
        
        if (hoverPath != null) {
            aPane.getChildren().remove(hoverPath);
        }
    }

    public void addAllNodes(Pane aPane) {
        contentPane = aPane;
        // add after layers
        int index = 0;
        for (Node n: aPane.getChildren()) {
            if (!(n instanceof DesignerLayerTittledPane)) {
                break;
            }
            
            index++;
        }
        
        aPane.getChildren().add(index, displayShape);

        for (Node theNode : wayPointHandles.values()) {
            aPane.getChildren().add(index, theNode);
        }
        
        if (hoverPath != null) {
            hoverPath.setVisible(false);
            drawEdgePath(true, lastHoverColor, lastHoverBackground);
        }
    }
}
