/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ChooseDatabaseController extends BaseController implements Initializable {
    
    @FXML
    private ListView<Database> listview;
    
    @FXML
    private AnchorPane mainUI;

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setResizable(false);
        dialog.setWidth(306);
        dialog.setHeight(306);
        dialog.setScene(new Scene(mainUI));
        dialog.setTitle("Choose Database");
        listview.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        listview.getSelectionModel().selectedItemProperty()
                .addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) { 
                dialog.close();
            }
        });
    }
    
    private Stage dialog;
   
    public Database show(ConnectionSession session){        
        
        try {
            listview.getItems().addAll(session.getService().getDatabases());
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
        }
        
        dialog.showAndWait();
        return listview.getSelectionModel().getSelectedItem();
    }
    
    
    
}
