/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.geometry.Insets;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.core.modules.utils.SettingsListViewAdaptation;
import np.com.ngopal.smart.sql.core.modules.utils.SettingsListViewDataItem;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class SettingsHomeView {
    
    private ListView<SettingsListViewAdaptation> settingsListview;
    private Insets padding = new Insets(15);
    private boolean inPreference;
    
    public SettingsHomeView(ListView<SettingsListViewAdaptation> settingsListview) {
        this.settingsListview = settingsListview;
        settingsListview.setCellFactory(new Callback<ListView<SettingsListViewAdaptation>,
                ListCell<SettingsListViewAdaptation>>() {
            @Override
            public ListCell<SettingsListViewAdaptation> call(ListView<SettingsListViewAdaptation> p) {
                return new ListCell<SettingsListViewAdaptation>(){
                    
                    {
                        setContentDisplay(ContentDisplay.RIGHT);
                        setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, Font.getDefault().getSize()));
                    }

                    @Override
                    public void updateSelected(boolean bln) {
                        super.updateSelected(bln);
                        //(SettingsListViewDataItem)getItem()).setItemSelected(bln);
                    }

                    @Override
                    protected void updateItem(SettingsListViewAdaptation t, boolean bln) {
                        super.updateItem(t, bln);
                        if(bln){
                            setText("");
                            return;
                        } 
                        setPrefWidth(settingsListview.getScene().getWidth() - 300);
                        setGraphicTextGap((settingsListview.getScene().getWidth() - 300)/2);
                        setGraphic(t.getCanvas());
                        setText(t.getRepresentation());
                        setPadding(padding);
                        if(inPreference && getFont().getSize() > 10){
                            setFont(Font.font("Tahoma", FontWeight.LIGHT, FontPosture.REGULAR, 10));
                            setHeight(40);
                        }else if( getFont().getSize() == 10){
                            setFont(Font.font("Tahoma", FontWeight.BOLD, FontPosture.REGULAR, Font.getDefault().getSize()));
                            setHeight(USE_COMPUTED_SIZE);
                        }
                    }
                    
                };
            }
        });
        settingsListview.getScene().widthProperty().addListener(
                new InvalidationListener() {
            @Override
            public void invalidated(Observable o) { 
                settingsListview.refresh();
            }
        });        
    }

    public boolean isInPreference() {
        return inPreference;
    }

    public void setInPreference(boolean inPreference) {
        this.inPreference = inPreference;
    }
    
}
