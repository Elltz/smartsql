package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ConnectionParam4RestorationService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PostgresDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.modules.ConnectionModuleInterface;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.SessionPersistor;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.controller.NewConnectionController;
import np.com.ngopal.smart.sql.ui.provider.SmartBaseControllerProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com);
 */
@Getter
@Slf4j
public abstract class AbstractConnectionModule extends AbstractStandardModule implements SessionPersistor, ConnectionModuleInterface {

    @Override
    public void connectToConnection(ConnectionParams cp) {
        try {
            Class<? extends AbstractDBService> cls = null;
            try {
                cls = (Class<AbstractDBService>) Class.forName(System.getProperty("dbTechnologyType", MysqlDBService.class.getName()),
                        false, getClass().getClassLoader());
            } catch (ClassNotFoundException cfe) {
                cfe.printStackTrace();
            }
            AbstractDBService service = MainUI.injector.getInstance(cls);
            service.setServiceBusy(true);
            //settignsService.putSettingValue(AppSettingsService.SETTING_NAME__LAST_OPENED_CONNECTION, currentId);
            getMainController().openConnection(service, cp, true, (ConnectionSession t) -> {
                getMainController().saveSessions();
            });
        } catch (Exception e) {
            exception = e;
        }
    }
    
    @Override
    public boolean isNeedOpenInNewTab(){
        return true;
    }
    
    private ConnectionParam4RestorationService restoreService;
    
    private static final Object THREAD_RESTORATION_LOCKER = new Object();
    private static volatile boolean keepLooping = true;
    
    @Override
    public void connectionOpened(ConnectionParams params) {
        synchronized(THREAD_RESTORATION_LOCKER){
             keepLooping = false;
        }        
    }
    
    @Override
    public void connectionFailedOpening(String error) {
        synchronized (THREAD_RESTORATION_LOCKER) {
            keepLooping = false;
        }
    }
    
    private void checkRestoreService(){
        if(restoreService == null){
            restoreService = MainUI.injector.getInstance(ConnectionParam4RestorationService.class);
        }
    }
    
    @Override
    public Map<?, ?> getPersistingMap() {
        HashMap<String, ArrayList<String>> sessions = new LinkedHashMap();
        checkRestoreService();
         
        for (int key: getMainController().getSessionsMap().keySet()) {
            ConnectionSession cs = getMainController().getSessionsMap().get(key);
                        
            String moduleClass = cs.getConnectionParam().getSelectedModuleClassName();
            
            // Ignore this modules
            if (SchemaDesignerModule.class.getName().equals(moduleClass)) {
                continue;
            }
            
            ArrayList<String> value = new ArrayList();
            value.add(String.valueOf(cs.getConnectionParam().getId()));
            value.add(moduleClass);
            sessions.put(String.valueOf(key), value);                        
            restoreService.save(cs.getConnectionParam());            
            key++;
        }
        return sessions;
    }
    
    public boolean getLoopState(){
        synchronized (THREAD_RESTORATION_LOCKER) { return keepLooping; }
    }
    
    @Override
    public synchronized void onRestore(Map<?, ?> restoreImage, ConnectionSession session, Long key, Consumer<ConnectionSession> parentCallback) {
        log.error("ONRESTORE!!!!!!");
        
        if (restoreImage == null) { 
            return;
        }
        
        checkRestoreService();        

        if (key != null) {

            log.info("PARAM ID = "  + key);
            ConnectionParams param1 = restoreService.get(key);

            if (param1 != null) {
                ConnectionParams param = param1.copyOnlyParams();  
                param.setId(param1.getId());        
                param.setSelectedModuleClassName(this.getClass().getName());

                if (param.isLocalToMachine()) {
                    getMainController().autoStartMysqlServer(false) ;
                }
                Class<? extends AbstractDBService> cls = null;
                try {
                    cls = (Class <AbstractDBService>) Class.forName(System.getProperty("dbTechnologyType", MysqlDBService.class.getName()),
                            false, getClass().getClassLoader());
                } catch (ClassNotFoundException cfe) {
                    cfe.printStackTrace(); 
                }
                AbstractDBService service = MainUI.injector.getInstance(cls);
                
                synchronized (THREAD_RESTORATION_LOCKER) { keepLooping = true; }

                getMainController().openConnection(service, param, false, (ConnectionSession t) -> {
                    if (parentCallback != null) {
                        parentCallback.accept(t);
                    }
                });
            }
        }
    }
    
    @Override
    public boolean isConnection() {
        return true;
    }
}
