package np.com.ngopal.smart.sql.ui.components;

import java.util.List;
import javafx.scene.control.TableColumn;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public abstract class AutoSizeTypedTableColumn<S, T> extends TableColumn<S, T> {

    private final String type;
    public AutoSizeTypedTableColumn(String name, String type) {
        super(name);
        this.type = type;
    }
    
    public abstract double calcWidth(List<S> rows, int maxWidth);
    public abstract double getMinimumColumnWidth();
    public abstract int getMaximumWidth();
    public abstract double getMaximumWidth(String type);
    
    public void init(List<S> rows) {
        
        double minWidth = getMinimumColumnWidth();
        double max = getMaximumWidth(type);
        
        double prefWidth = minWidth;        
        if (rows != null && !rows.isEmpty()) {
            int maximumWidth = getMaximumWidth();

            double width = calcWidth(rows, maximumWidth);

            if (width > prefWidth) {
                prefWidth = width;

                if (prefWidth > maximumWidth) {
                    prefWidth = maximumWidth;
                }
            }
        }
        
        max = Math.min(prefWidth, max);
        setPrefWidth(max < minWidth ? minWidth : max);
    }

    public String getType() {
        return type;
    }
    
    public boolean isNumber() {
        switch (type) {
            case "BIGINT":
            case "DECIMAL":
            case "DOUBLE":
            case "FLOAT":
            case "INT":
            case "MEDIUMINT":
            case "NUMERIC":
            case "SMALLINT":
            case "TINYINT":
                return true;
        }
        
        return false;
    }
}
