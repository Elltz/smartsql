package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class FilterDialogController extends BaseController implements Initializable {
    
    private static final String[] CONDITIONS = new String[]{"=", "<>", ">", "<", "LIKE"};
    
    @FXML
    private GridPane gridPane;
    
    @FXML
    private ComboBox<String> fieldsCombobox;
    @FXML
    private ComboBox<String> conditionsCombobox;
    @FXML
    private TextField valueField;
    
    private List<ComboBox<String>> fieldsComboboxs;
    private List<ComboBox<String>> conditionsComboboxs;
    private List<TextField> valueFields;
    
    private Node focusedComponent;
    
    @FXML
    private AnchorPane mainUI;
    
    private List<FilterData> result;
    
    public void init(List<String> columns, String selectedColumn, String selectedValue) {
        fieldsCombobox.getItems().setAll(columns);
        
        fieldsCombobox.getSelectionModel().select(selectedColumn);
        valueField.setText(selectedValue == null ? "NULL" : selectedValue);
    }
    
    /**
     * Method showing dialog with column multi selection
     * @param columns possible column names
     * @param filterData already selected filtered data
     * @param selectedColumn current selected column
     * @param selectedValue current selected value
     */
    public void init(List<String> columns, List<FilterData> filterData, String selectedColumn, String selectedValue) {
        fieldsCombobox.getItems().setAll(columns);
        
        fieldsComboboxs = new ArrayList<>();
        conditionsComboboxs = new ArrayList<>();
        valueFields = new ArrayList<>();
        
        boolean foundedLastEmpty = false;
        
        if (filterData != null && !filterData.isEmpty()) {
            FilterData fd = filterData.get(0);
            fieldsCombobox.getSelectionModel().select(fd.getColumn());
            conditionsCombobox.getSelectionModel().select(fd.getCondition());
            valueField.setText(fd.getValue() == null ? "NULL" : fd.getValue());
        } else {
            fieldsCombobox.getSelectionModel().select(selectedColumn);
            conditionsCombobox.getSelectionModel().select("=");
            valueField.setText(selectedValue == null ? "NULL" : selectedValue);
            
            focusedComponent = valueField;
            foundedLastEmpty = true;
        }
        
        
        int row = 2;
        for (int i = 0; i < columns.size(); i++) {
            
            ComboBox<String> columnBox = new ComboBox<>(FXCollections.observableArrayList(columns));
            columnBox.setPrefWidth(150);
            fieldsComboboxs.add(columnBox);
            
            ComboBox<String> conditionsBox = new ComboBox<>(FXCollections.observableArrayList(CONDITIONS));
            conditionsBox.setPrefWidth(150);
            conditionsComboboxs.add(conditionsBox);
            
            TextField field = new TextField();
            valueFields.add(field);
            
            if (filterData != null && i < filterData.size() - 1) {
                FilterData fd = filterData.get(i + 1);
                columnBox.getSelectionModel().select(fd.getColumn());
                conditionsBox.getSelectionModel().select(fd.getCondition());
                field.setText(fd.getValue() == null ? "NULL" : fd.getValue());
                
            } else if (!foundedLastEmpty) {
                columnBox.getSelectionModel().select(selectedColumn);
                conditionsBox.getSelectionModel().select("=");
                field.setText(selectedValue == null ? "NULL" : selectedValue);
                
                focusedComponent = field;
                foundedLastEmpty = true;
            }
            
            gridPane.add(columnBox, 0, row);
            gridPane.add(conditionsBox, 1, row);
            gridPane.add(field, 2, row);
            
            RowConstraints rc = new RowConstraints();
            gridPane.getRowConstraints().add(rc);
            row++;
        }
        
        if (focusedComponent == null) {
            focusedComponent = valueField;
        }
    }
    
    @FXML
    void clearAction(ActionEvent event) {
        
        fieldsCombobox.getSelectionModel().clearSelection();        
        if (fieldsComboboxs != null) {
            for (ComboBox<String> c: fieldsComboboxs) {
                c.getSelectionModel().clearSelection();
            }
        }
        
        conditionsCombobox.getSelectionModel().clearSelection();
        if (conditionsCombobox != null) {
            for (ComboBox<String> c: conditionsComboboxs) {
                c.getSelectionModel().clearSelection();
            }
        }
        
        valueField.setText("");
        if (valueFields != null) {
            for (TextField c: valueFields) {
                c.setText("");
            }
        }
    }
    
    @FXML
    void filterAction(ActionEvent event) {
        
        result = new ArrayList<>();
        
        FilterData fd = getFilterData(fieldsCombobox, conditionsCombobox, valueField);
        if (fd != null) {
            result.add(fd);
        }
        if (fieldsComboboxs != null) {
            for (int i = 0; i < fieldsComboboxs.size(); i++) {            
                fd = getFilterData(fieldsComboboxs.get(i), conditionsComboboxs.get(i), valueFields.get(i));
                if (fd != null) {
                    result.add(fd);
                }
            }
        }
        
        getStage().close();
    }

    private FilterData getFilterData(ComboBox<String> column, ComboBox<String> condition, TextField value) {
        String resultColumn = column.getSelectionModel().getSelectedItem();
        String resultCondition = condition.getSelectionModel().getSelectedItem();
        String resultValue = value.getText();
        
        if (resultColumn != null && resultCondition != null) {
            FilterData fd = new FilterData();
            fd.setColumn(resultColumn);
            fd.setCondition(resultCondition);
            fd.setValue(resultValue);
            return fd;
        }
        
        return null;
    }
    
    @FXML
    void cancelAction(ActionEvent event) {
        result = null;        
        getStage().close();
    }

    public List<FilterData> getResult() {
        return result;
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        conditionsCombobox.getItems().setAll(CONDITIONS);
        conditionsCombobox.getSelectionModel().select("=");
        
        mainUI.sceneProperty().addListener((ObservableValue<? extends Scene> ov, Scene t, Scene t1) -> {
            if (focusedComponent != null) {
                focusedComponent.requestFocus();
            }
        });
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}