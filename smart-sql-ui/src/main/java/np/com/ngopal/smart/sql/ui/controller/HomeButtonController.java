/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class HomeButtonController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label tabName;
    
    @FXML
    private TextArea descriptionTextArea;
    

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {        
        descriptionTextArea.setWrapText(true);
        tabName.setContentDisplay(ContentDisplay.LEFT);
        mainUI.setUserData(HomeButtonController.this);
        
        mainUI.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (t.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    descriptionTextArea.toFront();
                } else if (t.getEventType() == MouseEvent.MOUSE_EXITED) {
                    descriptionTextArea.toBack();
                }
            }
        });
        
    }
    
    public void init(String name, String image, String description,
            EventHandler<MouseEvent> action,boolean addClose) {
        tabName.setText(name);
        tabName.setGraphic(new ImageView(new Image(image, 18, 18, false, false)));
        descriptionTextArea.setText(description);
        if (action != null) {
            mainUI.addEventFilter(MouseEvent.MOUSE_CLICKED, action);            
        }       
        if(addClose){
            mainUI.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent t) { 
                    
                }
            });
        }
    }
    
}
