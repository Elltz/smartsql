/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Cell;
import javafx.scene.control.TreeItem;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class AnimatableTreeItem<T> extends TreeItem {

    Cell<T> currentCell;
    
    public AnimatableTreeItem() {
    }

    public AnimatableTreeItem(Object value) {
        super(value);
    }

    public AnimatableTreeItem(T value, Node graphic) {
        super(value, graphic);
    }

    public Cell<T> getCurrentCell() {
        return currentCell;
    }

    public void setCurrentCell(Cell<T> currentCell) {
        this.currentCell = currentCell;
    }
    
}
