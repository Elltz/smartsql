/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.concurrent.Callable;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class Flags {
     
    private static volatile boolean restorationTime = false;    
    public static synchronized boolean inRestorationState(){
        return restorationTime;
    }    
    public static synchronized void changeRestorationState(boolean bool){
        restorationTime = bool;
    }
    
    public final static int FOOTER_ELEMENT_PARENT = 0;
    public final static int FOOTER_ELEMENT_NAV_BUTTON = 1;
    public final static int FOOTER_ELEMENT_NOTIFICATION_SECTION = 2;
    public final static int FOOTER_ELEMENT_CONNECTION_SESSIONS_SECTION = 3;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_N0_OF_CON = 4;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA = 5;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_ROWS = 6;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_TOTAL_TIME = 7;
    public final static int FOOTER_ELEMENT_NOTIFICATION_UPDATE_FRONTLINE = 8;
    public final static int FOOTER_ELEMENT_CONNECTIONS_SESSIONS_QUERIES = 9;
        
    public final static String MYSQL_AUTHENTICATION_PLUGIN__SHA256 = "sha256_password";
    public final static String MYSQL_AUTHENTICATION_PLUGIN__NATIVE = "mysql_native_password";
    
    public final static String SPLASH_PAGE_ANIMATOR_FLAG = "splag";
    
    public static final String[] checkIfServiceExistQuery(String os_name){
        if(os_name.startsWith("lin")){
            return new String[]{"/etc/init.d/mysqld.sh", "status"};
        }else if(os_name.startsWith("win")){
        return new String[]{"cmd.exe", "/c", "net", "start", "|", "find", "/I", "/C", "\"mysql\""};
        }else if(os_name.startsWith("mac")){
            return new String[]{"/etc/mach_init.d/mysqld.sh","status"};
        }else {
            return new String[]{};
        }
    }
    
    public static final String[] startServiceQuery(String os_name,Callable<String> getName){
        if(os_name.startsWith("lin")){
            return new String[]{"/etc/init.d/mysqld.sh", "start"};
        }else if(os_name.startsWith("win")){
            String name = "";
            try{ name = getName.call(); }catch(Exception e){}
        return new String[]{"cmd.exe", "/c", "sc", "start",name};        
        }else if(os_name.startsWith("mac")){
            return new String[]{"/etc/mach_init.d/mysqld.sh","start"};
        }else{
            return new String[]{};
        }
    }
    
    public static final String[] stopServiceQuery(String os_name,String serviceName){
        if(os_name.startsWith("lin")){
            return new String[]{"/etc/init.d/mysqld.sh", "stop"};
        }else if(os_name.startsWith("win")){
        return new String[]{"cmd.exe", "/c", "sc", "stop",serviceName};        
        }else if(os_name.startsWith("mac")){
            return new String[]{"/etc/mach_init.d/mysqld.sh","stop"};
        }else {
            return new String[]{};
        }
    }
    
    
    /**
     * MYSQL Privileges 
     * 
     * The privileges are listed in the names after CONTEXT_PRIV_
     * lowercase 'a' means "and" all uppercases represents the 
     * context in which they affect.
     * 
     * Then name of the privilege comes after `__`.
     * 
     */
    
    public static final String CONTEXT_PRIV_DTI__CREATE = "CREATE";
    public static final String CONTEXT_PRIV_DTV__DROP = "DROP";
    public static final String CONTEXT_PRIV_DTS__GRANT_OPTION = "GRANT OPTION";
    public static final String CONTEXT_PRIV_DBS__LOCK_TABLES = "LOCK TABLES";
    public static final String CONTEXT_PRIV_DaT__REF = "REFERENCES";
    public static final String CONTEXT_PRIV_DBS__EVENT = "EVENT";
    public static final String CONTEXT_PRIV_TAB__ALTER = "ALTER";
    public static final String CONTEXT_PRIV_TAB__DEL = "DELETE";
    public static final String CONTEXT_PRIV_TAB__INDEX = "INDEX";
    public static final String CONTEXT_PRIV_TaC__INSERT = "INSERT";
    public static final String CONTEXT_PRIV_TaC__SELECT = "SELECT";
    public static final String CONTEXT_PRIV_TaC__UPDATE = "UPDATE";
    public static final String CONTEXT_PRIV_TAB__CREA_TEMP = 
            "CREATE TEMPORARY TABLES";
    public static final String CONTEXT_PRIV_TAB__TRIGGER = "TRIGGER";
    public static final String CONTEXT_PRIV_VIW__CREA_VIEW = "CREATE VIEW";
    public static final String CONTEXT_PRIV_VIW__SHOW_VIEW = "SHOW VIEW";
    public static final String CONTEXT_PRIV_SDR__ALT_ROUT = "ALTER ROUTINE";
    public static final String CONTEXT_PRIV_SDR__CREA_ROUT = "CREATE ROUTINE";
    public static final String CONTEXT_PRIV_SDR__EXECUTE = "EXECUTE";
    public static final String CONTEXT_PRIV_FAS__FILE ="FILE";
    public static final String CONTEXT_PRIV_ADM__CREA_TABLESPACE = 
            "CREATE TABLESPACE";
    public static final String CONTEXT_PRIV_ADM__CREA_USER = "CREATE USER";
    public static final String CONTEXT_PRIV_ADM__PROCESS = "PROCESS";
    public static final String CONTEXT_PRIV_ADM__PROXY = "PROXY";
    public static final String CONTEXT_PRIV_ADM__RELOAD = "RELOAD";
    public static final String CONTEXT_PRIV_ADM__REPL_CLIENT = 
            "REPLICATION CLIENT";
    public static final String CONTEXT_PRIV_ADM__REPL_SLAVE = 
            "REPLICATION SLAVE";
    public static final String CONTEXT_PRIV_ADM__SHOW_DATABASE = 
            "SHOW DATABASES";
    public static final String CONTEXT_PRIV_ADM__SHUTDOWN = "SHUTDOWN";
    public static final String CONTEXT_PRIV_ADM__SUPER = "SUPER";
    public static final String CONTEXT_PRIV_ADM__ALL= "ALL PRIVILEGES";
    public static final String CONTEXT_PRIV_ADM__ALL_SHORTEN= "ALL";
    public static final String CONTEXT_PRIV_ADM__USAGE = "USAGE";
    
    public static final String MAX_QUERIES_PER_HOUR = " MAX_QUERIES_PER_HOUR ";
    public static final String MAX_UPDATES_PER_HOUR = " MAX_UPDATES_PER_HOUR ";
    public static final String MAX_CONNECTIONS_PER_HOUR = " MAX_CONNECTIONS_PER_HOUR ";
    public static final String MAX_USER_CONNECTIONS = " MAX_USER_CONNECTIONS ";
    public static final String PASSWORD = " IDENTIFIED BY ";
    public static final String ACCOUNT = " ACCOUNT ";
    
    public static final String MYSQL_ACCOUNT_PASSWORD = "PASSWORD STATUS";
    public static final String MYSQL_ACCOUNT_PASSWORD_EXPIRE = "PASSWORD EXPIRE";
    public static final String MYSQL_ACCOUNT_PASSWORD_EXPIRE_DEFAULT = "PASSWORD EXPIRE DEFAULT";
    public static final String MYSQL_ACCOUNT_PASSWORD_EXPIRE_NEVER = "PASSWORD EXPIRE NEVER";
    public static final String MYSQL_ACCOUNT_PASSWORD_EXPIRE_INTERVAL_N_DAY = "PASSWORD EXPIRE INTERVAL N DAY";
    
    public static final String MYSQL_USER__NAME = "username";
    public static final String MYSQL_USER__PASSPHRASE = "password";
    public static final String MYSQL_USER__HOST = "host";
    
    public static final String FLUSH_PRIVILEGE_FOR_USER_MANAGER = "FLUSH NO_WRITE_TO_BINLOG PRIVILEGES;";
    
    public static final double USER_MANAGER_UI_MINIMUM_INTERNAL_HEIGHT = 555.0;
    
}
