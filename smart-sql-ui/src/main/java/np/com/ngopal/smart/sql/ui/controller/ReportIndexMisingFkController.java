package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportIndexMisingFkController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Button refreshButton;
    @FXML
    private Button exportButton;
    @FXML
    private FilterableTableView tableView;
    @FXML
    private ComboBox<String> databaseCombobox;
    
    private ConnectionSession session;
      
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportIndexMisingFk rds: (List<ReportIndexMisingFk>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rds.getDatabaseName());
                        row.add(rds.getMt());
                        row.add(rds.getMtColumn());
                        row.add(rds.getMtcDataType());
                        row.add(rds.getRelation());
                        row.add(rds.getTt());
                        row.add(rds.getTtColumn());
                        row.add(rds.getTtcDataType());
                        row.add(rds.getTtEngine());
                        row.add(rds.getAlterSql());
                        row.add(rds.getComments());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
        
                    columns.put("Database name", true);
                    columnWidths.add(100);

                    columns.put("Master Table (MT)", true);
                    columnWidths.add(100);
                    
                    columns.put("MT Column (MTC)", true);
                    columnWidths.add(100);
                    
                    columns.put("MTC's Data Type", true);
                    columnWidths.add(100);
                    
                    columns.put("", true);
                    columnWidths.add(100);
                    
                    columns.put("Transaction Table (TT)", true);
                    columnWidths.add(100);
                    
                    columns.put("TT Column (TTC)", true);
                    columnWidths.add(100);
                    
                    columns.put("TTC Data Type", true);
                    columnWidths.add(100);
                    
                    columns.put("TT Engine", true);
                    columnWidths.add(100);
                    
                    columns.put("Alter SQL", true);
                    columnWidths.add(100);
                    
                    columns.put("Comments", true);
                    columnWidths.add(100);
                                        
                    sheets.add("RAM usage from Performance schema report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    @FXML
    public void refreshAction(ActionEvent event) {
                
        String database = databaseCombobox.getSelectionModel().getSelectedItem();
        if (database != null) {
            makeBusy(true);

            Thread th = new Thread(() -> {
                if (session != null) {

                    try (ResultSet rs = (ResultSet) session.getService().execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_MISING_FK), database, database))) {
                        if (rs != null) {
                            List<ReportIndexMisingFk> list = new ArrayList<>();
                            while (rs.next()) {
                                ReportIndexMisingFk rid = new ReportIndexMisingFk(
                                    rs.getString(1), 
                                    rs.getString(2),
                                    rs.getString(3), 
                                    rs.getString(4), 
                                    rs.getString(5), 
                                    rs.getString(6), 
                                    rs.getString(7), 
                                    rs.getString(8), 
                                    rs.getString(9), 
                                    rs.getString(10), 
                                    rs.getString(11)
                                );  

                                list.add(rid);
                            }

                            tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                        }
                    } catch (SQLException ex) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", ex.getMessage(), null));
                    }
                }

                Platform.runLater(() -> makeBusy(false));
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        
        if (session != null && session.getConnectionParam() != null) {
            if (session.getConnectionParam().getDatabases() != null) {
                for (Database db: session.getConnectionParam().getDatabases()) {
                    databaseCombobox.getItems().add(db.getName());
                }
            }            
        }
        
        databaseCombobox.getSelectionModel().clearSelection();        
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
        
    @Override
    public BaseController getReportController() {
        return this;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {      
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        tableView.setSearchEnabled(true);
        
        TableColumn databaseNameColumn = createColumn("Database name", "databaseName");
        TableColumn mtColumn = createColumn("Master Table (MT)", "mt");
        TableColumn mtColumnColumn = createColumn("MT Column (MTC)", "mtColumn");
        TableColumn mtcDataTypeColumn = createColumn("MTC's Data Type", "mtcDataType");
        TableColumn relationColumn = createColumn("", "relation");
        TableColumn ttColumn = createColumn("Transaction Table (TT)", "tt");
        TableColumn ttColumnColumn = createColumn("TT Column (TTC)", "ttColumn");
        TableColumn ttcDataTypeColumn = createColumn("TTC Data Type", "ttcDataType");
        TableColumn ttEngineColumn = createColumn("TT Engine", "ttEngine");
        TableColumn alterSqlColumn = createColumn("Alter SQL", "alterSql");
        alterSqlColumn.setCellFactory(new Callback<TableColumn<ReportIndexMisingFk, String>, TableCell<ReportIndexMisingFk, String>>() {
            @Override
            public TableCell<ReportIndexMisingFk, String> call(TableColumn<ReportIndexMisingFk, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Alter SQL"));
            }
        });
        TableColumn commentsColumn = createColumn("Comments", "comments");
        
        tableView.addTableColumn(databaseNameColumn, 150, 150, false);
        tableView.addTableColumn(mtColumn, 150, 150, false);
        tableView.addTableColumn(mtColumnColumn, 150, 150, false);
        tableView.addTableColumn(mtcDataTypeColumn, 150, 150, false);
        tableView.addTableColumn(relationColumn, 100, 100, false);
        tableView.addTableColumn(ttColumn, 150, 150, false);
        tableView.addTableColumn(ttColumnColumn, 150, 150, false);
        tableView.addTableColumn(ttcDataTypeColumn, 150, 150, false);
        tableView.addTableColumn(ttEngineColumn, 150, 150, false);
        tableView.addTableColumn(alterSqlColumn, 150, 150, false);
        tableView.addTableColumn(commentsColumn, 150, 150, false);
                
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        databaseCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            refreshAction(null);
        });
    }
    
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {

        final Stage dialog = new Stage();
        
        SlowQueryTemplateCellController cellController = ((MainController)getController()).getTypedBaseController("SlowQueryTemplateCell");
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    private String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    log.error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }
}
