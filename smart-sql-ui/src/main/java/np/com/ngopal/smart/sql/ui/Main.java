/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.ServiceModule;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class Main {

    public static void mainX(String[] args) {
        Injector injector = Guice.createInjector(new ServiceModule());


        /*
         * Now that we've got the injector, we can build objects.
         */
        DBService service = injector.getInstance(DBService.class);
        List<String> strs = new ArrayList<>();
        strs.add("employeemgmt");
        ConnectionParams p = new ConnectionParams("localhost", TechnologyType.MYSQL,
                  "localhost", "root", "root", 3306);
        try {
            Connection c = service.connect(p);
            ResultSet rs = c.createStatement().executeQuery("SHOW DATABASES");
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
