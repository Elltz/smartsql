package np.com.ngopal.smart.sql.ui.components;

import javafx.scene.control.MenuItem;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class IconedMenuItem extends MenuItem {

    public IconedMenuItem(String text) {
        super(text);
    }
}
