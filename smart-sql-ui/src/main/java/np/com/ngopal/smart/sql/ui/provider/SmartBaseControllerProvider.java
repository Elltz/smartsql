/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.provider;

import com.google.inject.Inject;
import com.google.inject.Injector;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.provider.BaseControllerProvider;
import np.com.ngopal.smart.sql.ui.controller.MainController;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class SmartBaseControllerProvider<T extends BaseController> extends BaseControllerProvider<T> {

    @Inject
    private Injector injector;
    
    @Inject
    private MainController controller;

    public SmartBaseControllerProvider() {
        super();        
    }
    
    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public T get() {
        if(controllerFactory == null){ controllerFactory = injector::getInstance; }
        /**
         * The reason i have this set of codes, is by hack
         * i should have extended BaseController in Structure module, but i used it directly
         * so i have no way of injecting the MainController before initialized is called
         * hence this code.
         * 
         * Works Good but by design pattern its bad
         */
        setIController(controller);
        
        return super.get();
    }

    @Override
    protected String getPreface() {
        return "/np/com/ngopal/smart/sql/ui/";
    }
}
