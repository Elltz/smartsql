/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import np.com.ngopal.smart.sql.db.DBService;
import static np.com.ngopal.smart.sql.core.modules.utils.PrivsLevelWrapper.*;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class MysqlUser {
    
    private ObservableList<PrivsLevelWrapper> objectLevelPrivs = FXCollections.observableArrayList();
    private ObservableList<PrivsLevelWrapper> edittingPrivs = FXCollections.observableArrayList();
    private ObservableList<PrivsLevelWrapper> edittingGrantPrivs = FXCollections.observableArrayList();
    private ObservableList<PrivsLevelWrapper> edittingRevokePrivs = FXCollections.observableArrayList();
    private DBService service;
    
    private Boolean isCurrentUser,newUser;
    private final Properties serverProps = new Properties(),
            edittingProps = new Properties();
    private boolean hasAlreadyWith = false;
    //private ArrayList listOfEdiitedObject = new ArrayList(4);
    public static MysqlVersionHandler mysqlVersionHandler;
    private SQLException sqlException;
    private boolean computeEditPrivs;
    
    public void setMysqlVersionHandler(MysqlVersionHandler mvh){
        mysqlVersionHandler = mvh;
    }
        
    public MysqlUser(DBService service, String name, String pass, String h){
        this(service,name,pass,h,false);
    }
    
    public MysqlUser(DBService service, String name, String pass, String h,boolean newUser){
        this.service = service;    
        serverProps.setProperty(Flags.MYSQL_USER__HOST, h == null ? "localhost" : h);
        serverProps.setProperty(Flags.MYSQL_USER__PASSPHRASE, pass == null ? "" : pass);
        serverProps.setProperty(Flags.MYSQL_USER__NAME, name == null ? "" : name);
        this.newUser = newUser;
        init();
    }
    
    public MysqlUser(DBService service){
        this(service,null,null,null,true);
    }
    
    public MysqlUser(){
        this(null,null,null,null,true);
    }
    
    private void init() {
        
    }
    
    public boolean modifyUserName(String a){
        if(a != null){
            edittingProps.setProperty(Flags.MYSQL_USER__NAME, a);
            return true;
        }
        return false;
    }
    
    public boolean modifyHost(String a){
        if(a != null){
            edittingProps.setProperty(Flags.MYSQL_USER__HOST, a);
            return true;
        }
        return false;
    }
    
    public boolean modifyPassword(String a){
        if(a != null){
            edittingProps.setProperty(Flags.MYSQL_USER__PASSPHRASE, a);
            return true;
        }
        return false;
    }
    
    public boolean modifyMaxes(String flag, String value) {
        if (flag != null && value != null) {
            edittingProps.setProperty(flag, value);
            return true;
        }
        return false;
    }
    
    public String getUserAccountName(){
        StringBuilder sb = new StringBuilder(30)
                .append("'")
                .append(getOnlyName())
                .append("'")
                .append("@")
                .append("'")
                .append(getHost())
                .append("'");
        return sb.toString();
    }
    
    public String getHost(){
        return edittingProps.getProperty(Flags.MYSQL_USER__HOST, serverProps.getProperty(Flags.MYSQL_USER__HOST));
    }
    
    public MysqlUser prepareUser(){
        serverProps.setProperty(Flags.MYSQL_USER__HOST, getHost());
        serverProps.setProperty(Flags.MYSQL_USER__NAME, getOnlyName());
        
        try { isCurrentUser(); } catch(SQLException e){setLatestException(e);}
        try { accumulatePrivileges(); } catch(SQLException e){setLatestException(e);}
        try { accumulateNonPrivileges(); } catch(SQLException e){setLatestException(e);}
        return this;
    }
    
    public void resetInState(){
        edittingProps.clear();
        edittingPrivs.clear();
        edittingGrantPrivs.clear();
        edittingRevokePrivs.clear();
        System.gc();System.gc();System.gc();System.gc();
    }
    
    public void refreshUser(){
        prepareUser();
        resetInState();
    }
    
    public void accumulatePrivileges() throws SQLException {
        objectLevelPrivs.clear();
        ResultSet rs = (ResultSet) service.execute(service.getShowGrantsSQL(getUserAccountName()));
        while (rs.next()) {
            objectLevelPrivs.addAll(PrivsLevelWrapper.getPrivsLevelKeyFromString(rs.getString(1), mysqlVersionHandler.getVersion()));            
        }
    }
    
    public void accumulateNonPrivileges() throws SQLException {
        ResultSet rs = (ResultSet) service.execute(getNonPrivilegesQuery());
        rs.next();
        int maxColumn = rs.getMetaData().getColumnCount();
        serverProps.setProperty(Flags.MAX_CONNECTIONS_PER_HOUR, rs.getString(1));
        serverProps.setProperty(Flags.MAX_QUERIES_PER_HOUR, rs.getString(2));
        serverProps.setProperty(Flags.MAX_UPDATES_PER_HOUR, rs.getString(3));
        serverProps.setProperty(Flags.MAX_USER_CONNECTIONS, rs.getString(4));
        serverProps.setProperty(Flags.MYSQL_ACCOUNT_PASSWORD, (maxColumn >= 5) ? rs.getString(5) : "");
        serverProps.setProperty(Flags.ACCOUNT, (maxColumn >= 6) ? rs.getString(6) : "");
    }

    public boolean isCurrentUser() throws SQLException {
        if(isCurrentUser == null){
            ResultSet rs = (ResultSet) service.execute("SELECT CURRENT_USER();");
            rs.next();
            String[] name_host = rs.getString(1).split("@");
            isCurrentUser = name_host[0].equalsIgnoreCase(getOnlyName()) && 
                    ((name_host[1].equals("%") || getHost().equals("%")) ? 
                    (name_host[1].equals("localhost") || getHost().equals("localhost"))
                    : (name_host[1].equals(getHost())));
            rs.close();
        }
        return isCurrentUser;
    }

    public ObservableList<PrivsLevelWrapper> getPrivs() {
        return objectLevelPrivs;
    }
    
    public List<PrivsLevelWrapper> getEditPrivs() {  
        if(computeEditPrivs){
            computeEditPrivs = false;
            computeDistinctEditPrivs();
        }
        return edittingPrivs;
    }
    
    public synchronized void revokePrivilege(PrivsLevelWrapper paramsWrapper, String... prv) {
        if (prv == null) {
            return;
        }
        if (prv.length > 1) {
            editUserPrivilege(false, paramsWrapper, prv);
        } else {
            editUserPrivilege(false, paramsWrapper, prv[0]);
        }
    }

    public synchronized void grantPrivilege(PrivsLevelWrapper paramsWrapper, String... prv) {
        if (prv == null) {
            return;
        }
        if (prv.length > 1) {
            editUserPrivilege(true, paramsWrapper, prv);
        } else {
            editUserPrivilege(true, paramsWrapper, prv[0]);
        }
    }
    
    private void editUserPrivilege(boolean grant_revoke, PrivsLevelWrapper paramsWrapper, String prv){
        computeEditPrivs = true;
        List<PrivsLevelWrapper> ls;
        PrivsLevelWrapper temp = null;
        
        if(grant_revoke){ ls = edittingGrantPrivs; }
        else{ ls = edittingRevokePrivs; }
        
        temp = loopWrappersForLevelEquality(((ls == edittingGrantPrivs) ? edittingRevokePrivs : edittingGrantPrivs), paramsWrapper);
        if (temp != null && (prv == null || temp.getRawPrivileges(true).contains(prv))) {
            temp.modifyPrivilege(false, prv);
            System.gc();
            return;
        }
        
        PrivsLevelWrapper levelPrvsWrapper = PrivsLevelWrapper.loopWrappersForLevelEquality(ls, paramsWrapper);
        if (levelPrvsWrapper == null) {
            levelPrvsWrapper = paramsWrapper.clone(false);
            ls.add(levelPrvsWrapper);
        }
        levelPrvsWrapper.editPrivilege(true, prv);
    }

    private void editUserPrivilege(boolean grant_revoke, PrivsLevelWrapper paramsWrapper, String... prvs){
        computeEditPrivs = true;
        List<PrivsLevelWrapper> ls;
        PrivsLevelWrapper temp = null;
        List<String> levelprvsList;
        
        if(grant_revoke){ ls = edittingGrantPrivs; }
        else{ ls = edittingRevokePrivs; }
        
        temp = loopWrappersForLevelEquality(((ls == edittingGrantPrivs) ? edittingRevokePrivs : edittingGrantPrivs), paramsWrapper);
        if (temp != null) {
            levelprvsList = new ArrayList(prvs.length);
            ArrayList<String> tempPrvsList = temp.getRawPrivileges(true);
            for (String prv : prvs) {
                if (prv == null || tempPrvsList.contains(prv)) {
                    temp.modifyPrivilege(false, prv);
                }else{
                    levelprvsList.add(prv);
                }
            }
        }else{
            levelprvsList = Arrays.asList(prvs);
        }
        
        PrivsLevelWrapper levelPrvsWrapper = loopWrappersForLevelEquality(ls, paramsWrapper);
        if (levelPrvsWrapper == null) {
            levelPrvsWrapper = paramsWrapper.clone(false);
            ls.add(levelPrvsWrapper);
        }
        
        for(String prv : levelprvsList){
            levelPrvsWrapper.editPrivilege(true, prv);
        }        
        System.gc();
    }

    @Override
    public String toString() {
        return getUserAccountName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getOnlyName(){
        return edittingProps.getProperty(Flags.MYSQL_USER__NAME, serverProps.getProperty(Flags.MYSQL_USER__NAME));
    }
    
    public String getPassPhrase(){
        return edittingProps.getProperty(Flags.MYSQL_USER__PASSPHRASE, serverProps.getProperty(Flags.MYSQL_USER__PASSPHRASE, ""));
    }
    
    public boolean isGenInformationModified() {
        return edittingProps.size() > 0;
    }

    public boolean isUserHavingEmptyEdittingPrivs() {
        if (!edittingPrivs.isEmpty()) {
            for (PrivsLevelWrapper pl : edittingPrivs) {
                if (!pl.getRawPrivileges(true).isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    public String convertUserToQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("/*");
        sb.append("SmartMySQL USER MANAGEMENT CONVERTED USER");
        sb.append("\n");
        sb.append("     This is a query representative of ");
        sb.append(getOnlyName());
        sb.append(" in user management console");
        sb.append("*/");
        sb.append("\n");sb.append("\n");
        sb.append("-- NON-PRIVILEGE INFORMATION");
        sb.append("\n");
        writeUserInformationQuery(sb);
        sb.append("\n");
        sb.append("-- PRIVILEGE INFORMATION");
        //sb.append("\n");
        writeUserPrivilegesQuery(sb);
        sb.append("\n");
        return sb.toString();
    }
    
    
    private int resourceLimitWriter(StringBuilder query, String flag) {
        String resourceLimit = edittingProps.getProperty(flag, "-");
        if (!resourceLimit.equals("-") && !resourceLimit.equals(serverProps.getProperty(flag))) {
            if(!hasAlreadyWith){query.append("WITH");hasAlreadyWith = true;}
            query.append(flag);
            query.append(resourceLimit);
            return 1;
        }  
        return 0;
    }
    
    private void writeUserInformationQuery(StringBuilder query) {
        byte count = 0;
        int index = query.length();
        if (isNewUser()) {
            query.append("CREATE USER ");
            query.append(getUserAccountName());
            query.append(Flags.PASSWORD);
            query.append("\"");
            query.append(getPassPhrase());
            query.append("\"");
            count++;
            if (mysqlVersionHandler.canSetResource_PasswordExpireWithCreate()) {
                query.append(" ");
                count += resourceLimitWriter(query, Flags.MAX_QUERIES_PER_HOUR);
                count += resourceLimitWriter(query, Flags.MAX_CONNECTIONS_PER_HOUR);
                count += resourceLimitWriter(query, Flags.MAX_UPDATES_PER_HOUR);
                count += resourceLimitWriter(query, Flags.MAX_USER_CONNECTIONS);
                hasAlreadyWith = false;
            }
        } else {
            query.append(mysqlVersionHandler.usesAlter_Grant() ? "ALTER USER " : "GRANT USAGE ON *.* FOR ");
            query.append(getUserAccountName());
            if (!getPassPhrase().isEmpty()) {
                query.append(Flags.PASSWORD);
                query.append("\"");
                query.append(getPassPhrase());
                query.append("\"");
                query.append("\n");
                count++;
            } else {
                query.append(" ");
            }

            if (mysqlVersionHandler.canSetResource_PasswordExpireWithCreate()) {
                count += resourceLimitWriter(query, Flags.MAX_QUERIES_PER_HOUR);
                count += resourceLimitWriter(query, Flags.MAX_CONNECTIONS_PER_HOUR);
                count += resourceLimitWriter(query, Flags.MAX_UPDATES_PER_HOUR);
                count += resourceLimitWriter(query, Flags.MAX_USER_CONNECTIONS);
                hasAlreadyWith = false;
            }
            
            if (mysqlVersionHandler.getPasswordExpirable().get()) {
                String passwordExpiry = edittingProps.getProperty(Flags.MYSQL_ACCOUNT_PASSWORD);
                if (passwordExpiry != null
                        && !serverProps.getProperty(Flags.MYSQL_ACCOUNT_PASSWORD,
                                Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE).equals(passwordExpiry)) {
                    query.append("\n");
                    query.append(edittingProps.getProperty(Flags.MYSQL_ACCOUNT_PASSWORD));
                    count++;
                }
            }

        }
        
        query.append(";");
        
        if(count == 0){
            query.delete(index, query.length());
        }else{
            query.append("\n");
        }
    }
    
    private void writeUserPrivilegesQuery(StringBuilder query){
        derivePrivilegeQuery(query);
    }
    
    public boolean saveModifiedChanges() {
        
        StringBuilder query = new StringBuilder(100);
        if (isGenInformationModified()) {
            writeUserInformationQuery(query);
            System.out.println(query.toString());
            try { Object o = service.execute(query.toString()); } 
            catch (SQLException e) { setLatestException(e); return false; }
            query.delete(0, query.length());    
            newUser = false;
        }   

        if (!isUserHavingEmptyEdittingPrivs()) {
            writeUserPrivilegesQuery(query);
            if (!query.toString().isEmpty()) {
                System.out.println(query.toString());
                try {
                    for (String q : query.toString().split(System.lineSeparator())) {
                        Object o = service.execute(q);
                    }
                } catch (SQLException e) {
                    setLatestException(e);
                    return false;
                }
            }            
        }
        
        return true;
    }
    
    /**
     * We check for the new user using the privileges methology,
     * a new unsaved user has not object level privileges but maybe editting level privileges
     * since it has been created once with nothing and new privs will be granted to it.
     * 
     * If nothing it should be USAGE privilege .
     */
    
    public boolean isNewUser(){
        return newUser;
    }
    
    public boolean deleteMe(){
        if(isNewUser()){
            return true;
        }
        try{service.execute("DROP USER " + getUserAccountName()); return true; }
        catch(SQLException e){ setLatestException(e); }
        return false;
    }

    private void writeGrant_RevokeQuery(StringBuilder query, PrivsLevelWrapper plw,boolean revoke) {
        if(plw == null){ return; }
        ArrayList<String> array = plw.getScriptablePrivileges(false);
        if(array.isEmpty()){return;}
        
        if (!query.toString().isEmpty()) {
            query.append(System.lineSeparator());
        }
        
        query.append(revoke ? "REVOKE " :"GRANT ");        
        
        if(plw.isGlobalPrivilege()){            
            for (int i = 0; i < array.size(); i++) {
                query.append(array.get(i));
                if ((i+1) < array.size()) {
                    query.append(",");
                }
            }
            query.append(" ON "); query.append("*.*");
            
        }else if(plw.isDatabasePrivilege()){
            for (int i = 0; i < array.size(); i++) {
                query.append(array.get(i));
                if ((i+1) < array.size()) {
                    query.append(",");
                }
            }
            query.append(" ON `").append(plw.getDatabase()).append("`.*");
            
        }else if(plw.isTablePrivilege()){
            for (int i = 0; i < array.size(); i++) {
                query.append(array.get(i));
                if ((i+1) < array.size()) {
                    query.append(",");
                }
            }
            query.append(" ON `")
                    .append(plw.getDatabase())
                    .append("`.`")
                    .append(plw.getTable())
                    .append('`');
            
        }else if(plw.isView()){}else if(plw.isFunction()){ }else if(plw.isStoredProcedure()){
        }else if(plw.isTrigger()){}else if(plw.isIndex()){}else if(plw.isColumnPrivilege()){
            for (int i = 0; i < array.size(); i++) {
                query.append('('); query.append(array.get(i)); query.append(')');
                if ((i+1) < array.size()) {
                    query.append(",");
                }
            }
            query.append(" ON `"); 
            query.append(plw.getDatabase())
                    .append("`.`")
                    .append(plw.getTable())
                    .append('`');
        }
        
        query.append(revoke ? " FROM " : " TO ")
                .append(getUserAccountName());
        
        if(!revoke && plw.hasGrantsOnPrivilegs()){
            query.append(" WITH GRANT OPTION");
        }
        
        query.append(";");
    }
    
    private final void derivePrivilegeQuery(StringBuilder query) {
        for (PrivsLevelWrapper userEditPrivWrapper : getEditPrivs()) {
            
            //revokeUserPrivWrapper is the privileges to be revoked
            PrivsLevelWrapper revokeUserPrivWrapper = loopWrappersForLevelEquality(edittingRevokePrivs, userEditPrivWrapper);
            //grantUserPrivWrapper is the privileges to be granted
            PrivsLevelWrapper grantUserPrivWrapper = loopWrappersForLevelEquality(edittingGrantPrivs, userEditPrivWrapper);

            writeGrant_RevokeQuery(query, revokeUserPrivWrapper, true);
            writeGrant_RevokeQuery(query, grantUserPrivWrapper, false);
        }
    }
    
    public String getPropsValue(String flag){
        return getGenInfoValue(flag, "0");
    }
    
    public String getGenInfoValue(String flag, String defVal){
        return edittingProps.getProperty(flag, serverProps.getProperty(flag, defVal));
    }
    
    
    public boolean isAccountLocked(){
        return getPropsValue(Flags.ACCOUNT).charAt(0) == 'Y';
    }

    public void modifyPassWordExpiry(String selectedItem) {
        if (selectedItem == null) {
            edittingProps.remove(Flags.MYSQL_ACCOUNT_PASSWORD);
        }
        edittingProps.setProperty(Flags.MYSQL_ACCOUNT_PASSWORD, selectedItem);
    }
    
    public boolean expireUserPassWord() {
        if (!isNewUser()) {
            String query = "ALTER USER " + getUserAccountName() + " PASSWORD EXPIRE;";
            try {
                Object o = service.execute(query);
                return true;
            } catch (SQLException e) {
                setLatestException(e);
            }
        }
        return false;
    }
    
    public void modifyAccountStatus(boolean v) {
        if (isNewUser() || mysqlVersionHandler.getUserLockable().get()) { return; }
        String query = "ALTER USER " + getUserAccountName();
        query += v ? " ACCOUNT LOCK;" : " ACCOUNT UNLOCK;";
        try {
            Object o = service.execute(query);
        } catch (SQLException e) {  setLatestException(e); }
    }
    
    private final void setLatestException(SQLException e){
        sqlException = e;
        e.printStackTrace();
    }
    
    /**
     * This is a hard Intensive works needs to be in background job
     * @return 
     * the string representation;
     */
    public String getLatestErrorMessage(){
        String a = sqlException.getMessage().toLowerCase();
        //"There was an error in saving modified changes.."
        if(a.contains("access denied")){
            a = "You do not have the PRIVILEGES being GRANTED to user";
        }
        return a;
    }
    
    private String getNonPrivilegesQuery(){        
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(Flags.MAX_USER_CONNECTIONS.toLowerCase());
        sb.append(",max_updates,max_questions,max_connections");
        
        if(mysqlVersionHandler.getPasswordExpirable().get()){
            sb.append(","); sb.append("password_expired");
        }
        if(mysqlVersionHandler.getUserLockable().get()){
            sb.append(","); sb.append("account_locked");
        }
        
        sb.append(" from mysql.user where `user` like \"")
                .append(getOnlyName())
                .append("\" AND `host` = \"")
                .append(getHost())
                .append("\";");
        
        return sb.toString();
    }

    private synchronized void computeDistinctEditPrivs() {
        edittingPrivs.clear();
        for (PrivsLevelWrapper pl : edittingGrantPrivs) {
            edittingPrivs.add(pl);
        }

        for (PrivsLevelWrapper pl : edittingRevokePrivs) {
            PrivsLevelWrapper wrapper = loopWrappersForLevelEquality(edittingPrivs, pl);
            if (wrapper != null) {
                edittingPrivs.remove(wrapper);
                edittingPrivs.add(assembleFinalPrivsFromWrappers(wrapper, pl, wrapper, mysqlVersionHandler.getVersion()));
            } else {
                edittingPrivs.add(pl);
            }
        }
    }
    
    /**
     * revokeUserPrivWrapper is the privileges to be revoked
     * 
     */
    public PrivsLevelWrapper getRevokeUserPrivWrapper(PrivsLevelWrapper paramsWrapper){
         return loopWrappersForLevelEquality(edittingRevokePrivs, paramsWrapper);
    }
    
    /**
     * grantUserPrivWrapper is the privileges to be granted
     */
    public PrivsLevelWrapper getGrantUserPrivWrapper(PrivsLevelWrapper paramsWrapper){
         return loopWrappersForLevelEquality(edittingGrantPrivs, paramsWrapper);
    }
    
    /**
     * pureUserStaticPrivWrapper is the privileges without intention to edit
     * 
     */
    public PrivsLevelWrapper getPureUserStaticPrivWrapper(PrivsLevelWrapper paramsWrapper){
         return getDifferenceInPrivsWrapper(loopWrappersForLevelEquality(objectLevelPrivs, paramsWrapper),
                 loopWrappersForLevelEquality(getEditPrivs(), paramsWrapper));
    }
    
    /**
     * Assembled user privileges, this is the final privileges to be given to the user should changes be persisted
     * 
     */
    public PrivsLevelWrapper getAssembleFinalPrivsFromWrappers(PrivsLevelWrapper paramsWrapper){
         return assembleFinalPrivsFromWrappers(getPureUserStaticPrivWrapper(paramsWrapper),
                 getGrantUserPrivWrapper(paramsWrapper), paramsWrapper, mysqlVersionHandler.getVersion());
    }
    
    
    public static MysqlUser mergeUsersTogether(MysqlUser precedentUser, MysqlUser overwritingUser) {
        /* 
        if (matchOnlyWithOverwritingUser) {

            if (overwritingUser.edittingProps.getOrDefault(Flags.MYSQL_USER__NAME, null) != null) {
                precedentUser.modifyPassword(overwritingUser.getOnlyName());
            }

            if (overwritingUser.edittingProps.getOrDefault(Flags.MYSQL_USER__HOST, null) != null) {
                precedentUser.modifyHost(overwritingUser.getHost());
            }

            if (overwritingUser.edittingProps.getOrDefault(Flags.MYSQL_USER__PASSPHRASE, null) != null) {
                precedentUser.modifyPassword(overwritingUser.getPassPhrase());
            }
            
            if (overwritingUser.edittingProps.getOrDefault(Flags.MAX_QUERIES_PER_HOUR, null) != null) {
                precedentUser.modifyMaxes(Flags.MAX_QUERIES_PER_HOUR, overwritingUser.getPropsValue(Flags.MAX_QUERIES_PER_HOUR));
            }

            if (overwritingUser.edittingProps.getOrDefault(Flags.MAX_UPDATES_PER_HOUR, null) != null) {
                precedentUser.modifyMaxes(Flags.MAX_UPDATES_PER_HOUR, overwritingUser.getPropsValue(Flags.MAX_UPDATES_PER_HOUR));
            }

            if (overwritingUser.edittingProps.getOrDefault(Flags.MAX_CONNECTIONS_PER_HOUR, null) != null) {
                precedentUser.modifyMaxes(Flags.MAX_CONNECTIONS_PER_HOUR, overwritingUser.getPropsValue(Flags.MAX_CONNECTIONS_PER_HOUR));
            }

            if (overwritingUser.edittingProps.getOrDefault(Flags.MAX_USER_CONNECTIONS, null) != null) {
                precedentUser.modifyMaxes(Flags.MAX_USER_CONNECTIONS, overwritingUser.getPropsValue(Flags.MAX_USER_CONNECTIONS));
            }

        } else { */

        if (!Objects.equals(precedentUser.getOnlyName(), overwritingUser.getOnlyName())) {
            precedentUser.modifyUserName(overwritingUser.getOnlyName());
        }
        
        if (!Objects.equals(precedentUser.getHost(), overwritingUser.getHost())) {
            precedentUser.modifyHost(overwritingUser.getHost());
        }
        
        if (!Objects.equals(precedentUser.getPassPhrase(), overwritingUser.getPassPhrase())) {
            precedentUser.modifyPassword(overwritingUser.getPassPhrase());
        }

        if (!Objects.equals(precedentUser.getPropsValue(Flags.MAX_QUERIES_PER_HOUR), overwritingUser.getPropsValue(Flags.MAX_QUERIES_PER_HOUR))) {
            precedentUser.modifyMaxes(Flags.MAX_QUERIES_PER_HOUR, overwritingUser.getPropsValue(Flags.MAX_QUERIES_PER_HOUR));
        }

        if (!Objects.equals(precedentUser.getPropsValue(Flags.MAX_UPDATES_PER_HOUR), overwritingUser.getPropsValue(Flags.MAX_UPDATES_PER_HOUR))) {
            precedentUser.modifyMaxes(Flags.MAX_UPDATES_PER_HOUR, overwritingUser.getPropsValue(Flags.MAX_UPDATES_PER_HOUR));
        }

        if (!Objects.equals(precedentUser.getPropsValue(Flags.MAX_CONNECTIONS_PER_HOUR), overwritingUser.getPropsValue(Flags.MAX_CONNECTIONS_PER_HOUR))) {
            precedentUser.modifyMaxes(Flags.MAX_CONNECTIONS_PER_HOUR, overwritingUser.getPropsValue(Flags.MAX_CONNECTIONS_PER_HOUR));
        }

        if (!Objects.equals(precedentUser.getPropsValue(Flags.MAX_USER_CONNECTIONS), overwritingUser.getPropsValue(Flags.MAX_USER_CONNECTIONS))) {
            precedentUser.modifyMaxes(Flags.MAX_USER_CONNECTIONS, overwritingUser.getPropsValue(Flags.MAX_USER_CONNECTIONS));
        }
        
        for (PrivsLevelWrapper p : overwritingUser.edittingRevokePrivs) {
            PrivsLevelWrapper pvl = loopWrappersForLevelEquality(precedentUser.edittingRevokePrivs, p);
            PrivsLevelWrapper fpvl = null;
            
            if (pvl == null) {
                fpvl = p;
            } else {
                fpvl = getDifferenceInPrivsWrapper(p, pvl);
            }

            if (fpvl != null) {
                for (String s : fpvl.getRawPrivileges(true)) {
                    precedentUser.revokePrivilege(fpvl, s);
                }
            }
            
        }
        
        for (PrivsLevelWrapper p : overwritingUser.edittingGrantPrivs) {
            PrivsLevelWrapper pvl = loopWrappersForLevelEquality(precedentUser.edittingGrantPrivs, p);
            PrivsLevelWrapper fpvl = null;
            
            if (pvl == null) {
                fpvl = p;
            } else {
                fpvl = getDifferenceInPrivsWrapper(p, pvl);
            }

            if (fpvl != null) {
                for (String s : fpvl.getRawPrivileges(true)) {
                    precedentUser.grantPrivilege(fpvl, s);
                }
            }
            
        }
        
        System.gc();
        
        return precedentUser;
    }
    
    public static boolean isSameUser(MysqlUser a, MysqlUser b){
        return Objects.equals(a.serverProps.getProperty(Flags.MYSQL_USER__NAME), b.serverProps.getProperty(Flags.MYSQL_USER__NAME)) 
                && Objects.equals(a.serverProps.getProperty(Flags.MYSQL_USER__HOST), b.serverProps.getProperty(Flags.MYSQL_USER__HOST));
    }
}
