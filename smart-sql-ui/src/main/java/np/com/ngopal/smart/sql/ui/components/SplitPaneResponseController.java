/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */

/**
 * 
 * This only support vertical oriented splitpanes
 */

public class SplitPaneResponseController {
        
    SplitPane splitpane;
    
    private double responsLevel;
    
    /**
     * The nodes are in the order at which they are divided
     */
    private ArrayList<Node> children = new ArrayList();
    
    EventHandler<MouseEvent> mouseReactors;
    
    
    public SplitPaneResponseController(SplitPane sp){
        
        if (sp == null) {
            throw new NullPointerException("Injected SplitPane can not be null");
        }
        sp.sceneProperty().addListener(new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> observable,
                    Scene oldValue, Scene sc) {
                responsLevel = ((sc.getY() + sc.getHeight()) - 190);
                System.out.println("response level " + String.valueOf(responsLevel));
                sp.sceneProperty().removeListener(this);
            }
        });
        splitpane = sp;
        children.addAll(sp.getItems());
        
        reset();
        
        mouseReactors = new EventHandler<MouseEvent>() {
            
            boolean forget = false,reset = false;
            
            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType() == MouseEvent.MOUSE_MOVED) {                    
                        double y = event.getY();                        
                        if(y > responsLevel){
                            for(byte i =1; i < children.size(); i ++){
                                splitpane.getItems().add(children.get(i));
                            }
                        }
                    
                } else if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    double y = event.getY();
                    if (responsLevel > y) {
                       reset = true;
                    }
                }else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    if(reset && !forget){
                        reset();
                    }
                    forget = false;reset = false;
                }/*else if(event.getEventType() == MouseEvent.MOUSE_DRAGGED){
                    System.out.println("dragged");
                    forget = true;
                }*/
            }
        };
         splitpane.addEventFilter(MouseEvent.ANY, mouseReactors); 
    }
    
    public void dispose(){
        if(splitpane != null){
            splitpane.removeEventFilter(MouseEvent.ANY, mouseReactors);
        }
        children.clear();
        children = null;
    }
    
    
    private void reset() {
        splitpane.getItems().clear();
        splitpane.getItems().add(children.get(0));
    }
}
