/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.AnchorPane;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.models.FullTableSQLScanData;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class FullTableSQLScanController extends SQLStatementsController {

    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public String getName() {
        return "Table scanning queries";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        
        TableColumn queryColumn = createColumn("Query", "Query");
        TableColumn dbColumn = createColumn("DB", "db");
        TableColumn execCountColumn = createColumn("Exec Count", "execCount");
        TableColumn totalLatencyColumn = createColumn("Total Latency", "totalLatency");
        TableColumn indexUsedCountColumn = createColumn("No Index Used Count", "indexUsedCount");
        TableColumn goodIndexUsedCountColumn = createColumn("No Good Index Used Count", "goodIndexUsedCount");
        TableColumn indexUsedPctColumn = createColumn("No Index Used PCT", "indexUsedPct");
        TableColumn rowsSentColumn = createColumn("Rows Sent", "rowSent");
        TableColumn rowsExaminedColumn = createColumn("Rows Examined", "rowsExamined");
        TableColumn rowsSentAvgColumn = createColumn("Rows Sent AVG", "rowsSentAvg");
        TableColumn rowsExaminedAvgColumn = createColumn("rows Examined AVG", "rowsExaminedAvg");
        TableColumn firstSeenColumn = createColumn("First Seen", "firstSeen");
        TableColumn lastSeenColumn = createColumn("Last Seen", "lastSeen");
        
        tableView.setSearchEnabled(true);        
        tableView.addTableColumn(queryColumn, 170, 170, false);
        tableView.addTableColumn(dbColumn, 100, 100, false);
        tableView.addTableColumn(execCountColumn, 100, 100, false);
        tableView.addTableColumn(totalLatencyColumn, 100, 100, false);
        tableView.addTableColumn(indexUsedCountColumn, 100, 100, false);
        tableView.addTableColumn(goodIndexUsedCountColumn, 100, 100, false);
        tableView.addTableColumn(indexUsedPctColumn, 100, 100, false);
        tableView.addTableColumn(rowsSentColumn, 100, 100, false);
        tableView.addTableColumn(rowsExaminedColumn, 100, 100, false);
        tableView.addTableColumn(rowsSentAvgColumn, 100, 100, false);
        tableView.addTableColumn(rowsExaminedAvgColumn, 100, 100, false);
        tableView.addTableColumn(firstSeenColumn, 100, 100, false);
        tableView.addTableColumn(lastSeenColumn, 100, 100, false);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
    public List<FullTableSQLScanData> getData() {
        String query = "SELECT\n"
                + "   (`performance_schema`.`events_statements_summary_by_digest`.`DIGEST_TEXT`)                                                                                                 AS `Query`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SCHEMA_NAME`                                                                                                                           AS `DB`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`                                                                                                                            AS `Exec Count`,\n"
                + " (`performance_schema`.`events_statements_summary_by_digest`.`SUM_TIMER_WAIT`)                                                                                                   AS `Total Latency`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_NO_INDEX_USED`                                                                                                                     AS `No Index Used Count`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_NO_GOOD_INDEX_USED`                                                                                                                AS `No Good Index Used Count`,\n"
                + "  round((ifnull((`performance_schema`.`events_statements_summary_by_digest`.`SUM_NO_INDEX_USED` / nullif(`performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`, 0)), 0) * 100), 0) AS `No Index Used PCT`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_ROWS_SENT`                                                                                                                         AS `Rows Sent`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_ROWS_EXAMINED`                                                                                                                     AS `Rows Examined`,\n"
                + "  round((`performance_schema`.`events_statements_summary_by_digest`.`SUM_ROWS_SENT` / `performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`), 0)                                   AS `Rows Sent AVG`,\n"
                + "  round((`performance_schema`.`events_statements_summary_by_digest`.`SUM_ROWS_EXAMINED` / `performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`), 0)                               AS `rows Examined AVG`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`FIRST_SEEN`                                                                                                                            AS `First Seen`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`LAST_SEEN`                                                                                                                             AS `Last Seen` \n"
                + "FROM\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`\n"
                + "WHERE\n"
                + "      (((`performance_schema`.`events_statements_summary_by_digest`.`SUM_NO_INDEX_USED` > 0)\n"
                + "  OR  (`performance_schema`.`events_statements_summary_by_digest`.`SUM_NO_GOOD_INDEX_USED` > 0))\n"
                + "  AND (NOT((`performance_schema`.`events_statements_summary_by_digest`.`DIGEST_TEXT` LIKE 'SHOW%')))\n"
                + "  and ( performance_schema.events_statements_summary_by_digest.SCHEMA_NAME not in ('mysql','sys','information_schema','performance_schema') ))\n"
                + "ORDER BY\n"
                + "  ROUND((ifnull((`performance_schema`.`events_statements_summary_by_digest`.`SUM_NO_INDEX_USED` / nullif(`performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`, 0)), 0) * 100), 0) desc,\n"
                + "   (`performance_schema`.`events_statements_summary_by_digest`.`SUM_TIMER_WAIT`) DESC;";
        
        ArrayList<FullTableSQLScanData> data = new ArrayList(100);
        try {
            ResultSet rs = (ResultSet) getController().getSelectedConnectionSession().get().getService().execute(query);
            while(rs.next()){
                data.add(new FullTableSQLScanData(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13)));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }

}
