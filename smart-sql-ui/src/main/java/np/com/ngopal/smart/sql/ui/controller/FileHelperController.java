package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation.Status;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.Duration;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.db.RecentFileService;
import np.com.ngopal.smart.sql.core.modules.utils.SettingsListViewAdaptation;
import np.com.ngopal.smart.sql.ui.components.RecentFilesHomeView;
import np.com.ngopal.smart.sql.ui.components.SettingsHomeView;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.SmartsqlSavedData;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
/**
 * This all should have been in the MainController, along with the UI
 * 
 * TODO -- remove all commented code - TERAH
 */
public class FileHelperController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private ToggleButton aboutButton;

    @FXML
    private ToggleButton newButton;

    @FXML
    private FlowPane connectionSettingsHolder;

    @FXML
    private ToggleButton openButton;

    @FXML
    private ToggleButton appSettingsButton;

    @FXML
    private ToggleButton manageSessionButton;

    @FXML
    private ToggleButton RecentFilesButton;

    @FXML
    private ToggleButton exitButton;

    @FXML
    private VBox appSettingsContainer;

    @FXML
    private ListView<SettingsListViewAdaptation> settingsListview;

    @FXML
    private Button deleteAllSession;

    @FXML
    private ScrollPane recentContainer;

    @FXML
    private VBox recentFileHolder;

    @FXML
    private ScrollPane newContainer;

    @FXML
    private FlowPane creationButtonsHolder;

    @FXML
    private VBox manageSessionContainer;

    @FXML
    private Button disconnectButton;

    @FXML
    private Button disconnectAll;

    @FXML
    private Button createSession;

    @FXML
    private Button saveSession;

    @FXML
    private Button saveSessionAs;

    @FXML
    private Button endSession;

    @FXML
    private StackPane contentLayout;
    
    @FXML
    private VBox sidebarContainer;

    @FXML
    public FlowPane windowsView;

    @FXML
    private Button backButton;

    @FXML
    private VBox aboutContainer;

    @FXML
    private Label dayLabel;

    @FXML
    private FlowPane sessionsHolder;

    @FXML
    private HBox expriyDurationContainer;

    @FXML
    private Button deleteSession;

    @FXML
    private ListView<File> recentListview;

    /**
     * The bold big label with the the text This is a trial version of smartsql
     * ------------------------------------ when paid : This is a Full version
     * of SMARTSQL
     */
    @FXML
    private Label versionIndicationText;

    /**
     * THis is the top text on the about page. currently displaying for trial
     * SMARTSQL TRIAL : blah blah blah -------------------------------- when
     * paid SMARTSQL : blah blah blah
     */
    @FXML
    private Label topText;

    @FXML
    private ScrollPane sessionsHolderParent;

    private File openedFile = null;

    public String version = "";

    private double sideBarlength = 130.0;

    private SettingsHomeView settingsHomeView;
    
    private LoadingScreenController loadingScreenController;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    private ToggleGroup fileTG;

    private WorkProc<Boolean> delayKiller;
    
    @Inject
    private RecentFileService recentFileServ;
    
    private RecentFilesHomeView recentFilesHomeView;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        aboutButton.setOnAction(onAction);
        openButton.setOnAction(onAction);
        endSession.setOnAction(onAction);
        newButton.setOnAction(onAction);
        //manageSessionButton.setOnAction(onAction);
        appSettingsButton.setOnAction(onAction);
        exitButton.setOnAction(onAction);
        fileTG = new ToggleGroup();
        aboutButton.setToggleGroup(fileTG);
        openButton.setToggleGroup(fileTG);
        newButton.setToggleGroup(fileTG);
        //manageSessionButton.setToggleGroup(fileTG);
        appSettingsButton.setToggleGroup(fileTG);
        exitButton.setToggleGroup(fileTG);
        RecentFilesButton.setToggleGroup(fileTG);
        fileTG.selectToggle(aboutButton);
        RecentFilesButton.setOnAction(onAction);
        createSession.setOnAction(onAction);
        backButton.setOnAction(onAction);
        disconnectAll.setOnAction(onAction);
        disconnectButton.setOnAction(onAction);
        aboutButton.fire();
        saveSessionAs.setOnAction(onAction);
        saveSession.setOnAction(onAction);
        deleteSession.setOnAction(onAction);
        deleteAllSession.setOnAction(onAction);

        endSession.disableProperty().bind(SessionContainerController.whosSelected.isNull());
        deleteSession.disableProperty().bind(endSession.disableProperty());
        saveSession.disableProperty().bind(endSession.disableProperty());
        saveSessionAs.disableProperty().bind(endSession.disableProperty());
    }

    private void aboutPageWrittings(SmartsqlSavedData smartData) {
        if (smartData != null) {
            if (smartData.isPaid()) {
                //TODO: edit the ui to represent that this is a paid version
                trialContainer.getChildren().remove(expriyDurationContainer);
                versionIndicationText.setText("SMARTSQL PRO VERSION  (C) : ");
                versionIndicationText.setTextFill(Color.GREEN);
            }/* else if (smartData.isEvaluationTimeOver()) {
                dayLabel.setVisible(false);
            } else {
                dayLabel.setVisible(true);
                dayLabel.setText(String.valueOf(smartData.getDays()));
                if (controller.getSmartData().getDays() < 20) {
                    dayLabel.setStyle("-fx-text-fill: red;");
                } else if (smartData.getDays() > 20
                        && smartData.getDays() < 60) {
                    dayLabel.setStyle("-fx-text-fill: gold;");
                }
            }*/
            mysqlversionLabel.setText(smartData.getMysqlVersion());
            versionIndicationText.setText("VERSION  " + smartData.getVersion());
            //versionNumberLabel.setText(smartData.getVersion());
        }
    }

    public void workOnSmartData(SmartsqlSavedData smartData) {
        aboutPageWrittings(smartData);
        /**
         * autoCreate(false);
        if (smartData.isAutoRestore()) {
            select();
        }
        */
    }

    public void calculateUI() {
        mainUI.prefWidthProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                double width = t1.doubleValue() - (sideBarlength + 2);
                AnchorPane.clearConstraints(contentLayout);
                AnchorPane.setTopAnchor(contentLayout, 0.0);
                AnchorPane.setLeftAnchor(contentLayout, sideBarlength);
                manageSessionContainer.setPrefWidth(width);
                appSettingsContainer.setPrefWidth(width);
                recentContainer.setPrefWidth(width);
                recentFileHolder.setPrefWidth(width);
                creationButtonsHolder.setPrefWidth(width);
                creationButtonsHolder.setPrefWrapLength(width);
                contentLayout.setPrefWidth(width);
                newContainer.setPrefWidth(width);
                aboutContainer.setPrefWidth(width);
                sessionsHolderParent.setPrefWidth(width);
                sessionsHolder.setPrefWidth(width - 40);
                sessionsHolder.setPrefWrapLength(width - 40);
                ((StackPane) newContainer.getContent()).setPrefWidth(width);
                sidebarContainer.setPrefWidth(sideBarlength);
                connectionSettingsHolder.setPrefWrapLength(width);
                connectionSettingsHolder.setPrefWidth(width);
            }
        });
        //manageSessionButton.setContentDisplay(ContentDisplay.TEXT_ONLY);
        manageSessionContainer.prefHeightProperty().bind(mainUI.prefHeightProperty());
        manageSessionContainer.prefHeightProperty().addListener(new ChangeListener<Number>() {
            double computedH = 0.0;

            {
                for (Node n : manageSessionContainer.getChildren()) {
                    if (n == sessionsHolderParent) {
                        continue;
                    }
                    computedH += n.prefHeight(-1);
                    System.out.println(computedH);
                }
                sessionsHolderParent.setPrefHeight(manageSessionContainer.getPrefHeight() - computedH);
                sessionsHolderParent.autosize();
            }

            @Override
            public void changed(ObservableValue<? extends Number> observable,
                    Number oldValue, Number newValue) {
                sessionsHolderParent.setPrefHeight(newValue.doubleValue() - computedH);
            }
        });
        mainUI.prefWidthProperty().bind(((AnchorPane) mainUI.getParent()).widthProperty());
        mainUI.prefHeightProperty().bind(((AnchorPane) mainUI.getParent()).heightProperty());
        appSettingsContainer.prefHeightProperty().bind(mainUI.prefHeightProperty());
        recentContainer.prefHeightProperty().bind(mainUI.prefHeightProperty());
        recentFileHolder.prefHeightProperty().bind(mainUI.prefHeightProperty());
        newContainer.prefHeightProperty().bind(mainUI.prefHeightProperty());
        contentLayout.prefHeightProperty().bind(mainUI.prefHeightProperty());
        aboutContainer.prefHeightProperty().bind(mainUI.prefHeightProperty());

//        creationButtonsHolder.disableProperty().bind(controller.currentSessionSavePoint.isNull());
//        newButton.disableProperty().bind(creationButtonsHolder.disableProperty());
        backButton.disableProperty().bind(getController().getSelectedConnectionSession().isNull());
        disconnectAll.disableProperty().bind(backButton.disableProperty());
        disconnectButton.disableProperty().bind(backButton.disableProperty());
        openButton.disableProperty().bind(backButton.disableProperty());
        prepRecentFiles();
        //prepSettings();
        translate.setToX(0);
    }
    
    private void prepRecentFiles(){
        recentFilesHomeView = new RecentFilesHomeView(recentListview,recentFileServ);
        recentFilesHomeView.addOpenFileDialogOperation(new Callback<File, Object>() {
            @Override
            public Object call(File param) { 
                openedFile = param;
                openFileDialogOperation();
                return null;
            }
        });
    }

    private TranslateTransition translate = new TranslateTransition(
            Duration.millis(400));

    public void show() {
        //re-adjust the specs
        if (translate.getToX() < 0) {
            translate.setNode(mainUI);
            translate.setFromX(0 - getController().getStage().getScene().getWidth());
            translate.setToX(0);
            getUI().setVisible(true);
            getUI().toFront();
            initAbout_Info_PageWorkflow();
            translate.play();
            System.out.println("show called");
        }
    }

    private ChangeListener<Status> closeListener = new ChangeListener<Status>() {

        @Override
        public void changed(ObservableValue<? extends Status> ov, Status t, Status t1) {
            if (t1 != null && t1 == Status.STOPPED) {
                getUI().setVisible(false);
                getUI().toBack();
                translate.statusProperty().removeListener(closeListener);
            }
        }
    };

    public void close() {
        if (translate.getToX() == 0) {
            translate.stop();
            translate.setNode(mainUI);
            translate.setFromX(0);
            translate.setToX(0 - getController().getStage().getScene().getWidth());
            translate.statusProperty().addListener(closeListener);
            translate.play();
            System.out.println("close called ");
        }
    }

    private EventHandler<ActionEvent> onAction = new EventHandler() {

        @Override
        public void handle(Event t) {
            Object o = t.getSource();

            if (o == backButton) {
                close();
            } else if (o == exitButton) {
                ((MainController)getController()).shutDown();
                Platform.exit();//System.exit(0);//Platform.exit();
            } else if (o == aboutButton) {
                aboutContainer.toFront();
                initAbout_Info_PageWorkflow();
            } else if (o == openButton) {
                callOpen();
            } else if (o == appSettingsButton) {
//                if (settingsListview.getItems().isEmpty()) {
//                    settingsListview.getItems().addAll(((MainController)getController()).getSmartData().getSettingsFieldItems());
////                    settingsListview.getItems().add(
////                            new SettingsListViewDataItem(true,controller.mysqlStatusProperty, null,Flags.SETTINGS_MYSQL_SERVER_STATUS));
//                    //settingsListview.refresh();
//                }
//                toggleSettingsHomeViewParentState();
//                appSettingsContainer.toFront();
            } else if (o == newButton) {
                newContainer.toFront();
            } else if (o == RecentFilesButton) {
                recentContainer.toFront();
            }else if (o == disconnectAll) {
                DisconnectPermission permission = DisconnectPermission.NONE;
                while (getController().getSelectedConnectionSession().get() != null) {
                    permission = getController().disconnect(getController().getSelectedConnectionSession().get(), permission);
                }
            } else if (o == disconnectButton) {
                getController().disconnect(getController().getSelectedConnectionSession().get(), DisconnectPermission.NONE);
                if (getController().getSelectedConnectionSession().get() != null) {
                    close();
                }
            }
        }
    };

    private FileChooser fileChooser = new FileChooser();
    private Stage openedFileDialog;

    void setLoaderScreen(LoadingScreenController lScreenController) { 
        loadingScreenController = lScreenController;
    }

    public LoadingScreenController getLoaderScreen() { 
        return loadingScreenController;
    }

    private enum FileMethods {

        NOT_INVOLVED,
        SCHEMA_BUILDER_FILE,
        QUERY_BUILDER_FILE,
        QUERY_EDITOR_FILE,
        DATABASE_DATA_FILE;

        @Override
        public String toString() {
            if (this == DATABASE_DATA_FILE) {
                return "  ADD COLLECTION TO YOUR DATABSES    ";
            } else if (this == QUERY_BUILDER_FILE) {
                return "  RE-CREATE QUERY BUILDING SEQUENCE  ";
            } else if (this == QUERY_EDITOR_FILE) {
                return "  ADD QUERIES TO BE EXECUTED IN MYSQL";
            } else if (this == SCHEMA_BUILDER_FILE) {
                return "  LOAD FILE(S) INTO SCHEMA BUILDER   ";
            } else if (this == NOT_INVOLVED) {
                return "PLEASE CHOOSE WHAT YOU WOULD LIKE TO DO WITH THIS FILE OBJECT \n"
                        + "                     double click to select operation";
            }
            return "";
        }
    }

    private void callOpen() {
        fileChooser.setTitle("Open File");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("SmartSQL Files", "*.sql",
                        "*.queryxml", "*.schemaxml"),
                new ExtensionFilter("SQL Files", "*.sql"),
                new ExtensionFilter("QueryXML Files", "*.queryxml"),
                //new ExtensionFilter("SchemaXML Files", "*.schemaxml"),
                new ExtensionFilter("All Files", "*.*"));
        openedFile = fileChooser.showOpenDialog(getController().getStage());
        //after we open our file and open a new query tab
        if (openedFile != null) {
            openFileDialogOperation();
        }
    }

    private void openFileDialogOperation() {
        if (openedFileDialog == null) {
            openedFileDialog = new Stage(StageStyle.UTILITY);
            openedFileDialog.initModality(Modality.APPLICATION_MODAL);
            openedFileDialog.initOwner(getStage());
            openedFileDialog.setResizable(false);
            openedFileDialog.setTitle("OPERATION DIALOG");
            ListView<FileMethods> parent = new ListView();
            parent.getItems().addAll(FileMethods.values());
            parent.setCellFactory(new Callback<ListView<FileMethods>, ListCell<FileMethods>>() {
                @Override
                public ListCell<FileMethods> call(ListView<FileMethods> p) {
                    return new ListCell<FileMethods>() {

                        ImageView queryBuilderImage = new ImageView(
                                new Image("/np/com/ngopal/smart/sql/ui/images/query_builder.png", 18, 18, false, false)),
                                databaseImage = new ImageView(
                                        new Image("/np/com/ngopal/smart/sql/ui/images/connection/rename_button.png", 18, 18, false, false)),
                                queryImage = new ImageView(
                                        new Image("/np/com/ngopal/smart/sql/ui/images/query_editor_add.png", 18, 18, false, false)),
                                schemaImage = new ImageView(
                                        new Image("/np/com/ngopal/smart/sql/ui/images/designer/schema_designer_big.png", 18, 18, false, false));

                        Background selectedBackground = new Background(new BackgroundFill(
                                Color.web("#293955"), CornerRadii.EMPTY, new Insets(0, 480, 0, 0)));

                        {
                            setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent t) {
                                    if (t.getClickCount() > 1) {
                                        QueryTabModule queryTabModule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(
                                                QueryTabModule.class.getName());
                                        switch (getItem()) {
                                            case DATABASE_DATA_FILE:
                                               // CodeArea cArea = queryTabModule.getSessionCodeArea();
                                                //cArea.appendText("SOURCE "+ openedFile.getAbsolutePath()+";");
                                                // ((Menu)cArea.getContextMenu().getItems().get(2)).getItems().get(0).fire();
                                                String query = "SOURCE " + openedFile.getAbsolutePath() + ";";
//                                                System.out.println(query);
                                                queryTabModule.executeLineActionPerformed(
                                                        getController().getSelectedConnectionSession().get(),
                                                        null,
                                                        query, 0, false);
                                                break;
                                            case QUERY_BUILDER_FILE:

                                                break;
                                            case QUERY_EDITOR_FILE:
                                                getController().addBackgroundWork(new WorkProc() {

                                                    List<String> queries = new ArrayList();

                                                    @Override
                                                    public void updateUI() {
                                                        for (String s : queries) {
                                                            queryTabModule.getSessionCodeArea().
                                                                    appendText(s);
                                                        }
                                                        close();
                                                    }

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            //queries = Files.readAllLines(openedFile.toPath(),Charset.forName("UTF-8"));
                                                            queries.add(new String(Files.readAllBytes(openedFile.toPath())));
                                                            callUpdate = true;
                                                        } catch (IOException ex) {
                                                            Logger.getLogger(FileHelperController.class.getName()).log(Level.SEVERE, null, ex);
                                                        }
                                                    }
                                                });
                                                break;
                                            case SCHEMA_BUILDER_FILE:
                                                break;
                                        }
                                        openedFileDialog.close();
                                    }
                                }
                            });
                        }

                        @Override
                        public void updateIndex(int i) {
                            super.updateIndex(i);
                        }

                        @Override
                        protected void updateItem(FileMethods t, boolean bln) {
                            super.updateItem(t, bln);
                            if (bln) {
                                return;
                            }
                            if (t == FileMethods.NOT_INVOLVED) {
                                setPrefHeight(130);
                                setContentDisplay(ContentDisplay.TEXT_ONLY);
                                setAlignment(Pos.CENTER);
                                setStyle("-fx-background-color:#3c71b3; -fx-text-fill: white;");
                                //setEffect(new DropShadow(1.0, Color.BLACK));
                                setFont(Font.font(Font.getDefault().getName(),
                                        FontWeight.BOLD, Font.getDefault().getSize()));
                            } else {
                                setPrefHeight(65);
                                setFont(Font.font("Monospace",
                                        FontWeight.NORMAL, Font.getDefault().getSize()));
                                setContentDisplay(ContentDisplay.RIGHT);
                                setStyle("-fx-background-color:white; -fx-text-fill: #3c71b3;");
                                //setEffect(null);
                                switch (t) {
                                    case DATABASE_DATA_FILE:
                                        setGraphic(databaseImage);
                                        break;
                                    case QUERY_BUILDER_FILE:
                                        setGraphic(queryBuilderImage);
                                        break;
                                    case QUERY_EDITOR_FILE:
                                        setGraphic(queryImage);
                                        break;
                                    case SCHEMA_BUILDER_FILE:
                                        setGraphic(schemaImage);
                                        break;
                                }
                                setGraphicTextGap(150);
                                setAlignment(Pos.CENTER_LEFT);
                            }
                            setText(t.toString());
                        }

                        @Override
                        public void updateSelected(boolean bln) {
                            super.updateSelected(bln);
                            if (bln && getItem() != FileMethods.NOT_INVOLVED) {
                                setStyle("-fx-text-fill:black;");
                                setBackground(selectedBackground);
                            } else {
                                setBackground(null);
                                updateItem(getItem(), false);
                            }
                        }
                    };
                }
            });
            openedFileDialog.setScene(new Scene(
                    parent, 500, 400));
        }
        openedFileDialog.showAndWait();
        recentFilesHomeView.addRecent(openedFile);
    }

    /**
     * Adds to the front Page.
     *
     * @param where where is under which category, whether General Connections =
     * 0, or Basic Tools 1;
     * @param name THe name of the entitty
     * @param image Its represented Image
     * @param description Description to the User
     * @param action MouseEvent on Action to do.
     */
    public void addToHomePageAsButton(byte where, String name, String image, String description,
            EventHandler<MouseEvent> action) {

        HomeButtonController hbc = ((MainController)getController()).getTypedBaseController("HomeButton");
        if (where == 0) {
            hbc.init(name, image, description, action, false);
            connectionSettingsHolder.getChildren().add(hbc.getUI());
        } else if (where == 1) {
            hbc.init(name, image, description, action, true);
            creationButtonsHolder.getChildren().add(hbc.getUI());
        }
    }

    @FXML
    private Label connectionNameLabel;

    @FXML
    private Label mysqlversionLabel;

    @FXML
    private Label connecteuserLabel;

    @FXML
    private Label dateLabel;

    @FXML
    private VBox trialContainer;

    private void initAbout_Info_PageWorkflow() {

        if (!creationButtonsHolder.isDisabled()) {
            ConnectionSession t1 = getController().getSelectedConnectionSession().get();
            if (t1 != null) {
                connectionNameLabel.setText(t1.getConnectionParam().getName());
                connecteuserLabel.setText(t1.getConnectionParam().toString());
                dateLabel.setText(LocalDate.now().toString());
            }
        }
        synchronized (((MainController)getController()).getSmartDataLocker()) {
            if (((MainController)getController()).getSmartData() != null) {
                aboutPageWrittings(((MainController)getController()).getSmartData());
            }
        }
    }

    /**
     * We will have 1:auto start server when application starts 2:auto restore
     * previously saved sessions 3:save sessions every -%- minutes or seconds
     */
//    public void prepSettings() {
//        settingsListview.getItems().addAll(((MainController)getController()).getSmartData().getSettingsFieldItems());
//        settingsHomeView = new SettingsHomeView(settingsListview);
//    }

    public void gotoSessions() {
        //manageSessionButton.fire();
    }

    public void gotoAboutPage() {
        show();
        aboutButton.fire();
    }

    private void toggleSettingsHomeViewParentState(){
        if(!appSettingsContainer.getChildren().contains(settingsListview)){
            settingsHomeView.setInPreference(false);
            appSettingsContainer.getChildren().add(settingsListview);
        }
        appSettingsContainer.toFront();
    }
    
    public Node getSettingsHomeViewAsNode(){
        appSettingsContainer.getChildren().remove(settingsListview);
        settingsHomeView.setInPreference(true);
        return settingsListview;
    }
    
}
