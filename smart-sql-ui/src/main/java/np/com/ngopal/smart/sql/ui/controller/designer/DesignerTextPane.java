package np.com.ngopal.smart.sql.ui.controller.designer;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import np.com.ngopal.smart.sql.ui.controller.SchemaDesignerTabContentController;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerTextPane extends AnchorPane implements DesignerPane {
           
    DesignerText data;

    Label label;

    boolean sizeChanged = false;
    
    SchemaDesignerTabContentController controller;


    public DesignerTextPane(SchemaDesignerTabContentController controller, DesignerText data) {

        getStyleClass().add(SchemaDesignerTabContentController.STYLE_CLASS__DESIGNER_TEXT);
        this.data = data;
        this.controller = controller;

        data.color().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateData();
        });
        
        data.textColor().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateData();
        });
        
        data.font().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateData();
        });
        
        addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                controller.showTextTab(data);
            }
        });


        label = new Label("");
        label.setMaxWidth(Double.MAX_VALUE);
        label.textProperty().bind(data.text());

        getChildren().add(label);
        
        AnchorPane.setBottomAnchor(label, 0d);
        AnchorPane.setTopAnchor(label, 0d);
        AnchorPane.setLeftAnchor(label, 0d);
        AnchorPane.setRightAnchor(label, 0d);

        updateData();
        
        addDefaultDataListeners();
    }
    

    @Override
    public DesignerText getData() {
        return data;
    }

    @Override
    public Region getRegion() {
        return this;
    }
    
    @Override
    public boolean canChangeHeight() {
        return true;
    }

    @Override
    public void setPrefSize(double prefWidth, double prefHeight) {
        super.setPrefSize(prefWidth, prefHeight);

        sizeChanged = true;            
        updateData();
    }

    private void updateData() {               

        String style = "";
        if (data.color().get() != null && !data.color().get().isEmpty()) {
            style += "-fx-background-color: " + data.color().get() + "; ";
        }
        
        if (data.textColor().get() != null && !data.textColor().get().isEmpty()) {
            try {
                label.setTextFill(Color.web(data.textColor().get()));
            } catch (Throwable th) {}
        }
        
        if (data.font().get() != null && !data.font().get().isEmpty()) {
            String[] fontData = data.font().get().split(" ");
            
            for (String f: fontData) {
                f = f.trim();
                
                if (f.matches("\\d+")) {
                    style += "-fx-font-size: " + f + "; ";
                } else if (f.equalsIgnoreCase("bold") || f.equalsIgnoreCase("normal")) {
                    style += "-fx-font-weight: " + f + "; ";
                } else if (f.equalsIgnoreCase("italic") || f.equalsIgnoreCase("oblique")) {
                    style += "-fx-font-style: " + f + "; ";
                } else {
                    style += "-fx-font-family: " + f + "; ";
                }
            }            
        }
        
        setStyle(style);
        
        
        setMinSize(100, 30); 
    }
}
