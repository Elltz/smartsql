package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class PublishSchemaController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TextField schemaField;
    @FXML
    private ComboBox<DomainItem> domainCombobox;
    @FXML
    private ComboBox<DomainItem> subdomainCombobox;
    @FXML
    private TextArea descriptionArea;
    @FXML
    private Button publishButton;
    
    @FXML
    private CheckBox updateCheckbox;
    
    private Stage dialog;
    
    private DesignerErrDiagram diagram;
    private SchemaDesignerModule parentController;
        
    public void init(SchemaDesignerModule parentModule, DesignerErrDiagram diagram) {
        this.diagram = diagram;
        this.parentController = parentModule;
                
        schemaField.setText(diagram.name().get());
        
        // load domains
        try {
            List<Map<String, Object>> domains = PublicDataAccess.getInstanсe().loadDomains();
            if (domains != null) {
            
                DomainItem selectedItem = null;
                for (Map<String, Object> m: domains) {
                    DomainItem di = new DomainItem((Long)m.get("id"), (String)m.get("name"));
                    domainCombobox.getItems().add(di);

                    if (selectedItem == null && di.id.equals(diagram.domainId().get())) {
                        selectedItem = di;
                    }
                }

                domainCombobox.getSelectionModel().select(selectedItem);
            }
            
        } catch (PublicDataException ex) {
            DialogHelper.showError(getController(), "Publish error", ex.getMessage(), ex);
            return;
        }
        
                
        // load subdomains
        try {
            List<Map<String, Object>> subdomains = PublicDataAccess.getInstanсe().loadSubDomains();
            if (subdomains != null) {

                DomainItem selectedItem = null;
                for (Map<String, Object> m: subdomains) {
                    DomainItem di = new DomainItem((Long)m.get("id"), (String)m.get("name"));
                    subdomainCombobox.getItems().add(di);

                    if (selectedItem == null && di.id.equals(diagram.subdomainId().get())) {
                        selectedItem = di;
                    }
                }

                subdomainCombobox.getSelectionModel().select(selectedItem);
            }        
        } catch (PublicDataException ex) {
            DialogHelper.showError(getController(), "Publish error", ex.getMessage(), ex);
            return;
        }
        
        descriptionArea.setText(diagram.description().get());
        
         if (dialog == null) {
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setResizable(false);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getStage());
            dialog.setTitle("Publish Schema");
            dialog.setScene(new Scene(getUI()));
        }
        
        dialog.showAndWait();
    }
    
    
    @FXML
    public void publishAction(ActionEvent event) {
       
        // check domain
        String domain = domainCombobox.getEditor().getText();
        
        Long domainId = null;
        try {
            domainId = PublicDataAccess.getInstanсe().getIdByDomain(domain);
            if (domainId == null) {
                domainId = PublicDataAccess.getInstanсe().insertDomain(domain);
            }
        } catch (PublicDataException ex) {
            DialogHelper.showError(getController(), "Publish error", ex.getMessage(), ex);
            return;
        }
                
        // check subdomain
        String subdomain = subdomainCombobox.getEditor().getText();
        Long subdomainId = null;
        try {
            subdomainId = PublicDataAccess.getInstanсe().getIdBySubDomain(subdomain);
            if (subdomainId == null) {
                subdomainId = PublicDataAccess.getInstanсe().insertSubDomain(subdomain);
            }
        } catch (PublicDataException ex) {
            DialogHelper.showError(getController(), "Publish error", ex.getMessage(), ex);
            return;
        }
        
        String diagramData = new Gson().toJson(diagram.toMap());
        
        boolean wasUpdated = false;
        
        if (updateCheckbox.isSelected() && diagram.publicId().get() != null) {
            
            String diagramUser = diagram.user().get();
            String smartUser = null;
            SmartsqlSavedData smartData = ((MainController)getController()).getSmartData();
            if (smartData != null) {
                smartUser = smartData.getEmailId();
            }            
            
            if (diagramUser != null && smartUser != null && diagramUser.equals(smartUser)) {
                try {
                    wasUpdated = PublicDataAccess.getInstanсe().updateErrDiagram(
                        schemaField.getText(), 
                        diagramData, 
                        domainId, 
                        subdomainId, 
                        descriptionArea.getText(), 
                        diagram.publicId().get()
                    );
                } catch (PublicDataException ex) {
                    DialogHelper.showError(getController(), "Publish error", ex.getMessage(), ex);
                    return;
                }
                
                if (wasUpdated) {
                    System.out.println("UPDATED");
                }
            } else {
                DialogHelper.showError(getController(), "Access error", "Only owner can save public diagram. Remove selection in 'Update if exists' and try publish as new schema.", null);
                return;
            }
        }
        
        if (!wasUpdated) {
            // if not update then need insert
            // insert diagram
            try {
                Long id = PublicDataAccess.getInstanсe().insertErrDiagram(
                    schemaField.getText(), 
                    diagramData, 
                    diagram.database().get(), 
                    domainId, 
                    subdomainId, 
                    descriptionArea.getText(), 
                    ((MainController)getController()).getSmartData().getEmailId()
                );

                diagram.publicId().set(id);            
            } catch (PublicDataException ex) {
                
                if (ex.getCause() == null) {
                    ((MainController)getController()).showError("Publish error", "The name already used, please specify another name.", null);
                    Platform.runLater(() -> {
                        schemaField.selectAll();
                        schemaField.requestFocus();
                    });  
                } else {
                    ((MainController)getController()).showError("Publish error", ex.getMessage(), ex);
                }
                return;
            }
        }
        
        diagram.name().set(schemaField.getText());
        diagram.domain().set(true);
        diagram.domainId().set(domainId);
        diagram.subdomainId().set(subdomainId);
        diagram.description().set(descriptionArea.getText());

        parentController.saveDiagram();


        dialog.close();
        
        parentController.reloadDiagrams();
    }

    @FXML
    public void cancelAction(ActionEvent event) {
        dialog.close();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        publishButton.disableProperty().bind(
            Bindings.isEmpty(schemaField.textProperty())
            .or(Bindings.isEmpty(domainCombobox.getEditor().textProperty()))
            .or(Bindings.isEmpty(subdomainCombobox.getEditor().textProperty())));
        
        new AutoCompleteComboBoxListener<>(domainCombobox);
        new AutoCompleteComboBoxListener<>(subdomainCombobox);
    }

    
    private class DomainItem {
        private Long id;
        private String name;
        
        public DomainItem(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }        
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
}
