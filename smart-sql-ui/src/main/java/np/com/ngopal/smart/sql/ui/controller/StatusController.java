package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.InformationModule;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class StatusController extends BaseController implements Initializable{
        
    @FXML
    private AnchorPane mainUI;
     
    @FXML
    private WebView webView;
       

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }    
   
    @Override
    public void requestFocus() {
        webView.requestFocus();
    }
            
    
    public void init() {
        
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        if (session != null) {
            
            Platform.runLater(() -> {
                webView.getEngine().load(InformationModule.class.getResource("status.html").toExternalForm());
            
                Document doc = webView.getEngine().getDocument();
                if (doc == null) {
                    ChangeListener<Document> listener = new ChangeListener<Document>() {
                        @Override
                        public void changed(ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                            if (newValue != null) {
                                updateStatus(newValue, session.getService());
                                webView.getEngine().documentProperty().removeListener(this);
                            }
                        }
                    };
                    webView.getEngine().documentProperty().addListener(listener);
                } else {
                    updateStatus(doc, session.getService());
                }
            });
        }
    }
    
    private void updateStatus(Document doc, DBService service) {
        
        String status = "";
        try (ResultSet rs = service.getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ENGINE_INNODB_STATUS)).executeQuery()) {
            if (rs.next()) {
                status = rs.getString("Status");
            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), ex, getStage());
        }
            
//        status = status.replaceAll("\n", "<br/>");
        
        Element statusTable = doc.getElementById("status");
        for (String row: status.split("\n")) {
            
            Element newRow = doc.createElement("tr");
            Element cell = doc.createElement("td");
            
            if (row.toUpperCase().contains("INNODB MONITOR OUTPUT") ||
                row.toUpperCase().equals("BACKGROUND THREAD") ||
                row.toUpperCase().equals("SEMAPHORES") ||
                row.toUpperCase().equals("TRANSACTIONS") ||    
                row.toUpperCase().equals("LATEST FOREIGN KEY ERROR") ||
                row.toUpperCase().equals("FILE I/O") ||
                row.toUpperCase().equals("INSERT BUFFER AND ADAPTIVE HASH INDEX") ||
                row.toUpperCase().equals("LOG") ||
                row.toUpperCase().equals("BUFFER POOL AND MEMORY") ||
                row.toUpperCase().equals("ROW OPERATIONS")) {
                
                newRow.setAttribute("style", "font-weight: bold;");
            }
            
            cell.appendChild(doc.createTextNode(row));
                
            newRow.appendChild(cell);
            statusTable.appendChild(newRow);
        }
    }
          

    @Override
    public void initialize(URL location, ResourceBundle resources) { 
    }      
}