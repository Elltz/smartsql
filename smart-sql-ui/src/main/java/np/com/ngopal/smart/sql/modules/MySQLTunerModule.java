package np.com.ngopal.smart.sql.modules;

import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.ui.MainUI;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class MySQLTunerModule extends AbstractStandardModule {

    @Override
    public boolean install() {
        
        registrateServiceChangeListener();
        
        MenuItem tunerItem = new MenuItem("MySQL Conf  Tuner", new ImageView(StandardDefaultImageProvider.getInstance().getTuner_image()));
        tunerItem.setOnAction((ActionEvent event) -> {
            showTunerSql(getController(), MainUI.getMainController().getUsageService());
        });                
        
        tunerItem.disableProperty().bind(busy());
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.POWERTOOLS, new MenuItem[] {tunerItem});
        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[] {tunerItem});
        
        return true;
    }
    
    @Override
    protected void serviceBusyChanged(Boolean newValue) {
        super.serviceBusyChanged(newValue);
    }
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 200;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        return true;
    }

    @Override
    public boolean uninstall() {
        return true;
    }

    @Override
    public String getInfo() {
        return "Analyze Queries";
    }
}
