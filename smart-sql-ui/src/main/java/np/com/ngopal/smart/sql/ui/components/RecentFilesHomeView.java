/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.db.RecentFileService;
import np.com.ngopal.smart.sql.model.RecentFiles;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class RecentFilesHomeView {

    private ListView<File> recentList;
    List<Node> cachedNodes = new ArrayList();
    private RecentFileService recentFileServ;
    private Callback<File,?> openDialogRunnable;

    public RecentFilesHomeView(ListView<File> a,RecentFileService recentFileServ) {
        recentList = a;
        this.recentFileServ = recentFileServ;
        
        a.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {
            @Override
            public ListCell<File> call(ListView<File> p) {
                return new ListCell<File>() {
                    @Override
                    protected void updateItem(File item, boolean bln) {
                        super.updateItem(item, bln);

                        if (bln) {
                            setStyle("-fx-background-color: white;");
                            if (getGraphic() != null) {
                                cachedNodes.add(getGraphic());
                                setGraphic(null);
                            }
                            return;
                        }
                        setStyle(".list-cell{}");
                        if (getGraphic() == null) {
                            if (cachedNodes.isEmpty()) {
                                try {
                                    FXMLLoader loader = new FXMLLoader(
                                            MainUI.class.getResource("RecentFileListCellNode.fxml")
                                            .toURI().toURL());
                                    setGraphic(loader.load());
                                } catch (IOException ex) {
                                    Logger.getLogger(RecentFilesHomeView.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (URISyntaxException ex) {
                                    Logger.getLogger(RecentFilesHomeView.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else {
                                setGraphic(cachedNodes.remove(0));
                            }
                        }
                        AnchorPane pane = (AnchorPane) getGraphic();
                        HBox hbox = (HBox) pane.getChildren().get(0);
                        Label name = (Label) hbox.getChildren().get(0);
                        Button open = (Button) hbox.getChildren().get(1);
                        Button remove = (Button) hbox.getChildren().get(2);

                        name.setText(item.getName());
                        open.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent t) {
                                openDialogRunnable.call(getItem());
                            }
                        });
                        remove.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent t) {
                                a.getItems().remove(item);
                                removeRecent(item);
                            }
                        });
                    }
                };
            }
        });

        List<RecentFiles> files = recentFileServ.getLastFiles();
        for (RecentFiles rf : files) {
            File f = new File(rf.getRecentFile());
            if (f.exists()) {
                a.getItems().add(f);
            } else {
                recentFileServ.delete(rf);
            }
        }
    }

    public void addRecent(File f) {
        if (f != null) {
            Recycler.addWorkProc(new WorkProc<File>(f) {
                @Override
                public void updateUI() {
                    recentList.getItems().add(f);
                }

                @Override
                public void run() {
                    for (RecentFiles rf : recentFileServ.getAll()) {
                        try {
                            File f = new File(rf.getRecentFile());
                            if (Files.isSameFile(f.toPath(), getAddData().toPath())) {
                                return;
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(RecentFilesHomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    callUpdate = true;
                    RecentFiles rf = new RecentFiles(f.getPath());
                    recentFileServ.save(rf);
                }
            });
        }
    }

    public void removeRecent(File f) {
        removeRecent(f.toPath());
    }

    public void removeRecent(Path a) {
        Recycler.addWorkProc(new WorkProc<Path>(a) {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                for (RecentFiles rf : recentFileServ.getAll()) {
                    try {
                        File f = new File(rf.getRecentFile());
                        if (Files.isSameFile(f.toPath(), getAddData())) {
                            recentFileServ.delete(rf);
                            break;
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(RecentFilesHomeView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void addOpenFileDialogOperation(Callback<File,?> runnable) {
        openDialogRunnable = runnable;
    }

}
