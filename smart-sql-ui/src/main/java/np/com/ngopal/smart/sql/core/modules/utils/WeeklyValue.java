package np.com.ngopal.smart.sql.core.modules.utils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public enum WeeklyValue {
    MONDAY(0, "Monday", "MON"),
    TUESDAY(1, "Tuesday", "TUE"),
    WEDNESDAY(2, "Wednesday", "WED"),
    THURSDAY(3, "Thursday", "THU"),
    FRIDAY(4, "Friday", "FRI"),
    SATURDAY(5, "Saturday", "SAT"),
    SUNDAY(6, "Sunday", "SUN");

    public final int id;
    final String name;
    final String scheduleTaskName;

    WeeklyValue(int id, String name, String scheduleTaskName) {
        this.id = id;
        this.name = name;
        this.scheduleTaskName = scheduleTaskName;
    }

    public static WeeklyValue toEnum(int id) {
        switch (id) {                
            case 0:
                return MONDAY;
            case 1:
                return TUESDAY;
            case 2:
                return WEDNESDAY;
            case 3:
                return THURSDAY;
            case 4:
                return FRIDAY;
            case 5:
                return SATURDAY;
            case 6:
                return SUNDAY;

            default:
                return MONDAY;
        }
    }

    public String getScheduleTaskName() {
        return scheduleTaskName;
    }

    @Override
    public String toString() {
        return name;
    }
};
