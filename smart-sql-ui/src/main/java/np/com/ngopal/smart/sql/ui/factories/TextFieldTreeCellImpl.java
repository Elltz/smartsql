/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.WindowEvent;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.core.modules.utils.DBElementWrapperOnBuilder;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBElement;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.components.AnimatableTreeItem;
import np.com.ngopal.smart.sql.ui.components.ObjectViewClickListeners;
import static np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController.DatabaseChildrenType.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class TextFieldTreeCellImpl extends TreeCell<Object> {
//    private Boolean altertable;
//
//    TabContentController tabContentController;

//    private ContextMenu addMenu;
    private ContextMenu contextMenu;

    private Map<HookKey, Hookable> menus;

    private DBService dbService;

    public TextFieldTreeCellImpl(SimpleObjectProperty<ObjectViewClickListeners> ovcl,
            Map<HookKey, Hookable> menus, ContextMenu contextMenu) {
        this.menus = menus;
        this.contextMenu = contextMenu;
                        
        treeItemProperty().addListener(new ChangeListener<TreeItem<Object>>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem<Object>> observable,
                    TreeItem<Object> oldValue, TreeItem<Object> newValue) {
                if (newValue != null && newValue instanceof AnimatableTreeItem) {
                    ((AnimatableTreeItem) newValue).setCurrentCell(TextFieldTreeCellImpl.this);                    
                }
            }
        });
        
        /**
         * This happens per ConnectionSession, and per active Tab
         */
        setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                log.debug("Right");
//                if(isDisclosureButton){
//                    isDisclosureButton = false;
//                    return;
//                }
                if (ovcl != null && ovcl.get() != null) {               
                    if (event.getClickCount() > 1 && getItem() instanceof DBElement) {                        
                        ovcl.get().addViewOverseer(true,new DBElementWrapperOnBuilder(0, 0, (DBElement) getItem()));                       
                    }
                }
            }
        });
        
        setOnDragDetected(new EventHandler<MouseEvent>() {
            
            @Override
            public void handle(MouseEvent event) {
                if (ovcl != null && ovcl.get() != null) {
                    System.out.println("$$$$$4 drag detected");
                    Dragboard db = startDragAndDrop(TransferMode.COPY);
                    db.setDragView(new Image("/np/com/ngopal/smart/sql/"
                            + "ui/images/table_duplicate_small.png")); 
                    ClipboardContent content = new ClipboardContent();
                    if (getItem() instanceof DBTable || getItem() instanceof View) {
                        content.putString("Builder");
                    }else{
                        content.putString("");
                    }
                    db.setContent(content);
                    event.consume();
                }
            }
        });
        
        selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if(newValue){
                    updateIndex(getIndex());
                }
            }
        });

        setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {

            @Override
            public void handle(ContextMenuEvent event) {
                log.debug("Requested");
                if(getItem() == null){
                    return;
                }
                contextMenu.getItems().clear();
                
                boolean visible = false;
                for (HookKey menuItems : menus.keySet()) {

                    Hookable hook = menus.get(menuItems);
                    boolean matched = false;
                    Object value = getTreeItem().getValue();
                    Object parentValue = getTreeItem().getParent() == null ? null : getTreeItem().getParent().getValue();
                    //For collections of Table
                    if (Hooks.QueryBrowser.TABLES == hook && Objects.equals(value, TABLES)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;
                        
                    } //for collections of functions
                    else if (Hooks.QueryBrowser.FUNCTIONS == hook && Objects.equals(value, FUNCTIONS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;
                        
                    } else if (Hooks.QueryBrowser.FUNCTION_ITEM == hook && Objects.equals(parentValue, FUNCTIONS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;
                        
                    } else if (Hooks.QueryBrowser.TRIGGERS == hook && Objects.equals(value, TRIGGERS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;
                        
                    } else if (Hooks.QueryBrowser.TRIGGER_ITEM == hook && Objects.equals(parentValue, TRIGGERS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.STORED_PROCS == hook && Objects.equals(value, STORED_PROCS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.STORED_PROC_ITEM == hook && Objects.equals(parentValue, STORED_PROCS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.EVENTS == hook && Objects.equals(value, EVENTS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.EVENT_ITEM == hook && Objects.equals(parentValue, EVENTS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.VIEWS == hook && Objects.equals(value, VIEWS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.VIEW_ITEM == hook && Objects.equals(parentValue, VIEWS)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    }//For real table data
                    else if (Hooks.QueryBrowser.TABLE_ITEM == hook && Objects.equals(parentValue, TABLES)) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.DATABASE == hook && value instanceof Database) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.ROOT == hook && value instanceof ConnectionParams) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (value instanceof String && "Columns".equals((String)value) && Hooks.QueryBrowser.COLUMNS == hook) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;
                        
                    } else if (value instanceof String && "Indexes".equals((String)value) && Hooks.QueryBrowser.INDEXES == hook) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;
                        
                    } else if (Hooks.QueryBrowser.COLUMN_ITEM == hook  && value instanceof Column) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    } else if (Hooks.QueryBrowser.INDEX_ITEM == hook  && value instanceof Index) {
                        addMenuItems(menuItems.getItems());
                        visible = true;
                        matched = true;

                    }
                    
                        
//                    else {
//                        for (MenuItem mi: menuItems.getItems()) {
//                            if (contextMenu.getItems().contains(mi) && mi.isVisible()) {
//                                mi.setVisible(false);
//                            }
//                        }
//                    }
//                    
//                    if (!matched) {
//                        menuItems.getItems().stream().forEach((mi) -> {
//                            mi.setVisible(false);
//                        });
//                    }
                }
                if (!visible) {
                    event.consume();
                    log.debug("Invisible");
                } else {

                }
          //only index 0 has the benefit of all the color changes object since it is the only
          //item with that menuItem.
                if (getIndex() == 0) {
                    contextMenu.setUserData(getTreeView().getUserData());
                }
            }

        });
//        altertable = false;
//only index 0 has the benefit of all the color changes object since it is the only
//item with that menuItem.
        if (getIndex() == 0) {
            contextMenu.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    System.out.println("context menu closing");
                    contextMenu.setUserData(null);
                }
            });
        }
        treeViewWorks();
    }
        
    private void addMenuItems(List<MenuItem> menuItems) {
        if (menuItems != null) {
            
            if (!contextMenu.getItems().isEmpty()) {
                contextMenu.getItems().add(new SeparatorMenuItem());
            }
            
            for (MenuItem mi: menuItems) {
                if (!contextMenu.getItems().contains(mi)) {
                    contextMenu.getItems().add(mi);
                    mi.setVisible(true);
                }
            }
        }
    }

    private void treeViewWorks() {
        treeViewProperty().addListener(new ChangeListener<TreeView<Object>>() {
            @Override
            public void changed(ObservableValue<? extends TreeView<Object>> observable,
                    TreeView<Object> oldValue, TreeView<Object> newValue) {

                if (newValue != null) {
                    
                    if(newValue.getUserData() == null){
                        treeViewProperty().removeListener(this);
                        return;
                    }

                    Object[] paints = (Object[]) ((SimpleObjectProperty<Object>) newValue.getUserData()).getValue();
                    fore = (Color) paints[0];
                    back = (Color) paints[1];
                    //this code might not make sense, but it works since a cell goes down with its
                    //treeitem and since TreeView().refresh() does not take down cells totally 
                    //this seems legit.
                    //The reason i am using the below code is call Tabpane's selectionModel to get the selected Tab
                    //will always result in the old selected Tab. so currently my way to solving that is this approach
                    //after i can change this to TabPane.getSelectionModel().getSelectedItem();
                    //but this happens only once per Tab
                    if (((String) paints[2]).isEmpty()) {
                        TabPane tpain = findTabPaneForNode(getTreeView().getParent(), null);
                        tab = tpain.getTabs().get(tpain.getTabs().size() - 2);
                        oneTime(fore != null && back != null && tab != null, false);
                        tab = null;// i do not trust the above's judgements
                    } else {
                        oneTimeWrapper((String) paints[2], false);
                    }

                    ((SimpleObjectProperty<Object>) newValue.getUserData()).addListener(new ChangeListener<Object>() {
                        @Override
                        public void changed(ObservableValue<? extends Object> observable,
                                Object oldValue, Object newValue) {

                            Object[] paints = (Object[]) newValue;
                            fore = (Color) paints[0];
                            back = (Color) paints[1];
                            if (fore != null && back != null) {
                                TextFieldTreeCellImpl.this.setStyle("-fx-text-fill: " + ("#" + fore.toString().substring(2))
                                        + "; -fx-background-color:" + ("#" + back.toString().substring(2)) + ";");
                            }
                            oneTimeWrapper((String) paints[2], (boolean) paints[3]);
                        }
                    });
                }
                //treeViewProperty().removeListener(this);
            }
        });
    }

    Tab tab;
    Paint fore;
    Paint back;

    private void oneTimeWrapper(String id, boolean resetDefaults) {
        if (getIndex() == 1) {
            if (tab == null) {
                TabPane tpain = findTabPaneForNode(getTreeView().getParent(), null);
                for (Tab tab : tpain.getTabs()) {
                    if (tab.getId().equals(id)) {
                        TextFieldTreeCellImpl.this.tab = tab;
                        break;
                    }
                }
            }
            oneTime(fore != null && back != null, resetDefaults);
        }
    }

    private void oneTime(boolean shouldRun, boolean resetDefaults) {

        if (shouldRun) {
            TreeView tv = getTreeView();
            Label ltf = (Label) tab.getContent().lookup("#LabelforfilterField");
            if (fore != null) {
                if (ltf != null) {
                    ltf.setTextFill(fore);
                }
                ((Label) tab.getGraphic()).setTextFill(fore);
            }
            ((Label) tab.getGraphic()).setBackground(Background.EMPTY);
            Background bg = new Background(new BackgroundFill(back, null, null));
            tv.setBackground(bg);
            Region r = ((Region) tv.getParent());
            r.setBackground(bg);
            tab.setStyle("-fx-background-color:" + ("#" + back.toString().substring(2)) + ";");        
        } else {
            TextFieldTreeCellImpl.this.setStyle(".tree-cell{}");
            if (resetDefaults) {
                ((Label) tab.getGraphic()).setStyle(".label{}");
                tab.setStyle(".tab{}");
                ((Label) tab.getGraphic()).setTextFill(TextFieldTreeCellImpl.this.getTextFill());
                getTreeView().setBackground(TextFieldTreeCellImpl.this.getBackground());
                ((Region) getTreeView().getParent()).setBackground(TextFieldTreeCellImpl.this.getBackground());
            }
        }
        //precaution code 
        try {
            getTreeView().refresh();
        } catch (Exception e) {
        }
    }

    @Override
    public void updateIndex(int i) {
        super.updateIndex(i);
        if (isSelected()) {
            if (fore == null) {
                //if this line is to cause trouble, java setBackground will be used
                TextFieldTreeCellImpl.this.setStyle(".tree-cell{-fx-background-color: #0093ff;}");
            } else {
                TextFieldTreeCellImpl.this.setStyle("-fx-text-fill: " + ("#" + fore.toString().substring(2))
                        + "; -fx-background-color: #0093ff;");
            }
            return;
        }
        
        if (fore != null && back != null) {
            TextFieldTreeCellImpl.this.setStyle("-fx-text-fill: " + ("#" + fore.toString().substring(2))
                    + "; -fx-background-color:" + ("#" + back.toString().substring(2)) + ";");
        } else {
            TextFieldTreeCellImpl.this.setStyle(".tree-cell{}");
        }
    }

    private TabPane findTabPaneForNode(Node node, Background background) {
        TabPane tabPane = null;

        for (Node n = node.getParent(); n != null && tabPane == null; n = n.getParent()) {
            if (n instanceof TabPane) {
                tabPane = (TabPane) n;
            } else if (background != null) {
                if (n instanceof Region) {
                    ((Region) n).setBackground(background);
                }
            }
        }
        return tabPane;
    }

//    public Boolean isAlterTable() {
//        return altertable;
//    }
    @Override
    public void startEdit() {
//        super.startEdit();
//
//        if (textField == null) {
//            createTextField();
//        }
//        setText(null);
//        setGraphic(textField);
//        textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText((String) getItem());
        setGraphic(getTreeItem().getGraphic());
    }

    @Override
    public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
//                if (textField != null) {
//                    textField.setText(getString());
//                }
                setText(null);
//                setGraphic(textField);
            } else {

                setText(getString());
                setGraphic(getTreeItem().getGraphic());
//                System.out.println(" name : " + getTreeItem().getValue().toString());

//                ContextMenu contextMenu = getContextMenu() == null ? new ContextMenu() : getContextMenu();
//                if (getTreeItem().getParent() != null) {
//                    if (addMenu == null) {
//                        addMenu = new ContextMenu();
//                        MenuItem addMenuItem = new MenuItem("Alter Table");
//                        addMenu.getItems().add(addMenuItem);
//                        addMenuItem.setOnAction(new EventHandler() {
//                            public void handle(javafx.event.Event t) {
//                                log.debug("Menu Item");
//                                System.out.println("table name : " + getTreeItem().getValue());
////                                altertable = true;
//                                addAlterTable(dbService, getTreeItem().getValue());
//
//                            }
//                        });
//                    }
//
//
//
//                }
            }
        }
    }

//    private void createTextField() {
//        textField = new TextField(getString());
//        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
//
//            @Override
//            public void handle(KeyEvent t) {
//                if (t.getCode() == KeyCode.ENTER) {
//                    commitEdit(textField.getText());
//                } else if (t.getCode() == KeyCode.ESCAPE) {
//                    cancelEdit();
//                }
//            }
//        });
//
//    }
    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    public static ArrayList<Runnable> animationsRunsFix = new ArrayList(200);
    
    @Override
    protected boolean isItemChanged(Object oldItem, Object newItem) { 
        //animationsRunsFix.add(()-> setOpacity(1.0));
        return super.isItemChanged(oldItem, newItem);
    }
}
