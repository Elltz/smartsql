package np.com.ngopal.smart.sql.ui.controller;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import java.awt.Color;
import java.awt.event.AdjustmentEvent;
import java.io.StringReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Pair;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.DatabaseCompareUtils;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import org.apache.commons.lang.StringUtils;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SynchronizationDiffComponent extends BaseController implements Initializable {
    
    @FXML
    private TextField searchField;
    @FXML
    private Label searchCountLabel;
    
    @FXML
    private Label sourceAdressLabel;
    @FXML
    private Label sourceDatabaseLabel;
    
    @FXML
    private Label targetAdressLabel;
    @FXML
    private Label targetDatabaseLabel;
    
    @FXML
    private Label allCountLabel;
    
    @FXML
    private Label titleTypeLabel;
    @FXML
    private Label titleSourceLabel;
    @FXML
    private HBox titleSelectPane;
    @FXML
    private CheckBox titleSelectCheckbox;
    @FXML
    private Label titleOperationLabel;
    @FXML
    private Label typeTargetLabel;
    
    
    @FXML
    private TitledPane onlyInTargetContainer;
    @FXML
    private TitledPane onlyInSourceContainer;
    @FXML
    private TitledPane differentContainer;
    @FXML
    private TitledPane equalsContainer;
    
    @FXML
    private VBox onlyInTargetVBox;
    @FXML
    private VBox onlyInSourceVBox;
    @FXML
    private VBox equalVBox;
    @FXML
    private VBox differentVBox;
    
    @FXML
    private BorderPane sourceQueryContainer;
    @FXML
    private BorderPane targetQueryContainer;
    
    private PreferenceDataService preferenceService;
    
    private final SimpleObjectProperty<DiffRow> selectedRow = new SimpleObjectProperty<>();
    
    @FXML
    private AnchorPane mainUI;
    
    private static final Image IMAGE_EMPTY  = SmartImageProvider.getInstance().getSortEmpty_image();// new Image("/np/com/ngopal/smart/sql/ui/images/sort_empty.png");
    private static final Image IMAGE_ASC    = SmartImageProvider.getInstance().getSortAsc_image(); //new Image("/np/com/ngopal/smart/sql/ui/images/sort_asc.png");
    private static final Image IMAGE_DESC   = SmartImageProvider.getInstance().getSortDesc_image();//new Image("/np/com/ngopal/smart/sql/ui/images/sort_desc.png");

    private static final String ONLY_IN_SOURCE_TEXT = "Only in source (%d of %d objects selected)";
    private static final String ONLY_IN_TARGET_TEXT = "Only in target (%d of %d objects selected)";
    private static final String DIFFERENT_TEXT      = "Different (%d of%d selected)";
    private static final String EQUAL_TEXT          = "Equal (%d objects)";
    private static final String TOTAL_TEXT          = "%d of %d objects selected";
    private static final String SEARCH_TEXT         = "%d of %d";
    
    // 0 - none, 1 - asc, -1 - desc: titleTypeLabel, titleSourceLabel, titleOperationLabel, typeTargetLabel
    private final Integer[] sortOrder = new Integer[] {0, 0, 0, 0};
    
    private final Collator DEFAULT_COLLATOR = Collator.getInstance();
    private SyncCompareResult compareResult;
    private SyncCompareInfo compareInfo;
        
    private SchemaSynchronizerTabContentController parentController;
    private ConnectionParams targetParams;
    
    private Integer currentTotalRows = 0;
    private Integer totalRows = 0;
    private Integer totalSelected = 0;
    
    private final Comparator<SyncCompareResultItem> SORT_COMPARATOR = (SyncCompareResultItem o1, SyncCompareResultItem o2) -> {
        int result = 0;
        if (sortOrder[0] == 1) {
            result = DEFAULT_COLLATOR.compare(getNonNullString(o1.getType()), getNonNullString(o2.getType()));
        } else if (sortOrder[0] == -1) {
            result = DEFAULT_COLLATOR.compare(getNonNullString(o2.getType()), getNonNullString(o1.getType()));
        } 
        
        if (result == 0) {
            if (sortOrder[1] == 1) {
                result = DEFAULT_COLLATOR.compare(getNonNullString(o1.getSource()), getNonNullString(o2.getSource()));
            } else if (sortOrder[1] == -1) {
                result = DEFAULT_COLLATOR.compare(getNonNullString(o2.getSource()), getNonNullString(o1.getSource()));
            }
        }
        
        if (result == 0) {
            if (sortOrder[2] == 1) {
                result = DEFAULT_COLLATOR.compare(getNonNullString(o1.getOperation()), getNonNullString(o2.getOperation()));
            } else if (sortOrder[2] == -1) {
                result = DEFAULT_COLLATOR.compare(getNonNullString(o2.getOperation()), getNonNullString(o1.getOperation()));
            }
        }
        
        if (result == 0) {
            if (sortOrder[3] == 1) {
                result = DEFAULT_COLLATOR.compare(getNonNullString(o1.getTarget()), getNonNullString(o2.getTarget()));
            } else if (sortOrder[3] == -1) {
                result = DEFAULT_COLLATOR.compare(getNonNullString(o2.getTarget()), getNonNullString(o1.getTarget()));
            }
        }
        
        return result;
    };
    
    private final Comparator<SyncCompareResultItem> DEFAULT_COMPARATOR = (SyncCompareResultItem o1, SyncCompareResultItem o2) -> {
        int compared = getTypeComareValue(o1) - getTypeComareValue(o2);
        return compared == 0 && o1.getSource() != null && o2.getSource() != null ? DEFAULT_COLLATOR.compare(o1.getSource(), o2.getSource()) : compared;
    };
    
    
    private String getNonNullString(Object obj) {
        return obj == null ? "" : obj.toString();
    }
    
    public void init(SchemaSynchronizerTabContentController parentController, PreferenceDataService preferenceService, SyncCompareResult compareResult, SyncCompareInfo compareInfo) {
    
        this.compareInfo = compareInfo;
        this.parentController = parentController;
        this.compareResult = compareResult;
        this.preferenceService = preferenceService;
        this.targetParams = compareInfo.getTarget();
    
        sourceAdressLabel.setText(compareResult.getSourceHost());
        sourceDatabaseLabel.setText(compareResult.getSourceDatabase().getName());
        
        targetAdressLabel.setText(compareResult.getTargetHost());
        targetDatabaseLabel.setText(compareResult.getTargetDatabase().getName());
        
        int size = compareResult.getOnlyInSourceList().size();
        onlyInSourceContainer.setVisible(size > 0);
        onlyInSourceContainer.setManaged(size > 0);
        currentTotalRows += size;
          
        size = compareResult.getOnlyInTargetList().size();
        onlyInTargetContainer.setVisible(size > 0);
        onlyInTargetContainer.setManaged(size > 0);
        currentTotalRows += size;
        
        size = compareResult.getDifferentList().size();
        differentContainer.setVisible(size > 0);
        differentContainer.setManaged(size > 0);
        currentTotalRows += size;
        
        size = compareResult.getEqualList().size();
        equalsContainer.setVisible(size > 0);
        equalsContainer.setManaged(size > 0);
        currentTotalRows += size;
        
        updateAllData(compareResult, DEFAULT_COMPARATOR);
    }
    
    
    private void updateAllData(SyncCompareResult compareResult, Comparator<SyncCompareResultItem> comparator) {
        
        totalRows = 0;
        totalSelected = 0;
        
        Pair<Integer, Integer> result = updateData(onlyInSourceVBox, compareResult.getOnlyInSourceList(), comparator);
        onlyInSourceContainer.setVisible(result.getValue() > 0);
        onlyInSourceContainer.setManaged(result.getValue() > 0);
        onlyInSourceContainer.setText(String.format(ONLY_IN_SOURCE_TEXT, result.getKey(), result.getValue()));
        totalRows += result.getValue();
        totalSelected += result.getKey();
        
        result = updateData(onlyInTargetVBox, compareResult.getOnlyInTargetList(), comparator);
        onlyInTargetContainer.setVisible(result.getValue() > 0);
        onlyInTargetContainer.setManaged(result.getValue() > 0);
        onlyInTargetContainer.setText(String.format(ONLY_IN_TARGET_TEXT, result.getKey(), result.getValue()));
        totalRows += result.getValue();
        totalSelected += result.getKey();
        
        result = updateData(differentVBox, compareResult.getDifferentList(), comparator);
        differentContainer.setVisible(result.getValue() > 0);
        differentContainer.setManaged(result.getValue() > 0);
        differentContainer.setText(String.format(DIFFERENT_TEXT, result.getKey(), result.getValue()));
        totalRows += result.getValue();
        totalSelected += result.getKey();
        
        result = updateData(equalVBox, compareResult.getEqualList(), comparator);
        equalsContainer.setVisible(result.getValue() > 0);
        equalsContainer.setManaged(result.getValue() > 0);
        equalsContainer.setText(String.format(EQUAL_TEXT, result.getValue()));
        totalRows += result.getValue();
        totalSelected += result.getKey();        
        
        allCountLabel.setText(String.format(TOTAL_TEXT, totalSelected, totalRows));
    }
    
    private Pair<Integer, Integer> updateData(VBox container, List<SyncCompareResultItem> list, Comparator<SyncCompareResultItem> comparator) {
        
        container.getChildren().clear();
        List<SyncCompareResultItem> listTemp = new ArrayList(list);
        listTemp.sort(comparator);
        
        if (!searchField.getText().trim().isEmpty()) {
            listTemp = listTemp.stream().filter(r -> r.containsFilterText(searchField.getText().trim())).collect(Collectors.toList());
            searchCountLabel.setText(String.format(SEARCH_TEXT, listTemp.size(), currentTotalRows));
        } else {
            searchCountLabel.setText("");
        }

        int selected = 0;
        
        int size = listTemp.size();
        for (int i = 0; i < size; i++) {
            SyncCompareResultItem item = listTemp.get(i);
            if (item.isSelected()) {
                selected++;
            }
            container.getChildren().add(new DiffRow(item, i == size - 1, container));
        }
        
        return new Pair<>(selected, size);
    }
    
    private int getTypeComareValue(SyncCompareResultItem item) {
        switch (item.getType()) {
            case TABLE: return 0;
            case VIEW: return 1;
            case PROCEDURE: return 2;
            case FUNCTION: return 3;
            case TRIGGER: return 4;
            default: return 5;
        }
    }
    
    @FXML
    public void clearSearchAction(MouseEvent event) {        
        searchField.setText("");
    }
    
    public String generateScript() {
        StringBuilder builder = new StringBuilder("USE `" + compareResult.getTargetDatabase().getName() + "`;\n\n");
        builder.append("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;");

        List<SyncCompareResultItem> onlyIntarget = compareResult.getOnlyInTargetList();
        if (onlyIntarget != null) {
            for (SyncCompareResultItem item: onlyIntarget) {

                if (item.isSelected() && item.getOperation() == SyncCompareResultItem.Operation.DROP) {
                    if (builder.length() > 0) {
                        builder.append("\n\n\n");
                    }

                    String query = item.getDropTargetQuery().trim();
                    if (!query.endsWith(";")) {
                        query += ";";
                    }
                    builder.append(query);
                }
            }
        }

        List<SyncCompareResultItem> equals = compareResult.getEqualList();
        if (equals != null) {
            for (SyncCompareResultItem item: equals) {

                if (item.isSelected() && item.getOperation() == SyncCompareResultItem.Operation.DROP) {
                    if (builder.length() > 0) {
                        builder.append("\n\n\n");
                    }

                    String query = item.getDropTargetQuery().trim();
                    if (!query.endsWith(";")) {
                        query += ";";
                    }
                    builder.append(query);
                }
            }
        }


        List<SyncCompareResultItem> different = compareResult.getDifferentList();
        if (different != null) {
            for (SyncCompareResultItem item: different) {

                if (item.isSelected()) {

                    if (item.getOperation() == SyncCompareResultItem.Operation.UPDATE) {

                        if (builder.length() > 0) {
                            builder.append("\n\n\n");
                        }

                        switch (item.getType()) {
                            case TABLE:
                                String q = MysqlDBService.getChangesSQL((DBTable)item.getTargetObject(), (DBTable)item.getSourceObject());
                                if (!q.endsWith(";")) {
                                    q += ";";
                                }
                                builder.append(q);
                                break;

                            case VIEW:   
                            case FUNCTION:
                            case PROCEDURE:
                            case TRIGGER:
                            case EVENT:
                                String query = item.getDropSourceQuery().trim();
                                if (!query.endsWith(";")) {
                                    query += ";";
                                }
                                builder.append(query).append("\n");

                                query = item.getCreateSourceQuery().trim();
                                if (!query.endsWith(";")) {
                                    query += ";";
                                }
                                builder.append(query);
                                break;
                        }

                    } else if (item.getOperation() == SyncCompareResultItem.Operation.DROP) {

                        if (builder.length() > 0) {
                            builder.append("\n\n\n");
                        }

                        switch (item.getType()) {
                            case TABLE:
                            case VIEW:   
                            case FUNCTION:
                            case PROCEDURE:
                            case TRIGGER:
                            case EVENT:
                                String query = item.getDropSourceQuery().trim();
                                if (!query.endsWith(";")) {
                                    query += ";";
                                }
                                builder.append(query).append("\n");
                                break;
                        }
                    }                    
                }
            }
        }


        List<SyncCompareResultItem> onlyInSource = compareResult.getOnlyInSourceList();
        if (onlyInSource != null) {
            for (SyncCompareResultItem item: onlyInSource) {

                if (item.isSelected() && item.getOperation() == SyncCompareResultItem.Operation.CREATE) {
                    if (builder.length() > 0) {
                        builder.append("\n\n\n");
                    }

                    String query = item.getCreateSourceQuery().trim();
                    if (!query.endsWith(";")) {
                        query += ";";
                    }
                    builder.append(query);
                }
            }
        }

        builder.append("\n\n/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;");

        return builder.toString();
    }
    
    
    public void importInTarget(boolean refresh) {
        parentController.getUI().setDisable(true);
        
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                try {
                    MysqlDBService service = new MysqlDBService(null);
                    service.connect(targetParams);        

                    ScriptRunner sr = new ScriptRunner(service.getDB(), true);
                    sr.runScript(new StringReader(generateScript().replaceAll("`" + compareResult.getSourceDatabase().getName() + "`", "`" + compareResult.getTargetDatabase().getName() + "`")), new ScriptRunner.ScriptRunnerCallback() {
                        @Override
                        public void started(Connection con, int queriesSize) throws SQLException {
                            con.setAutoCommit(false);
                        }

                        @Override
                        public void queryExecuting(Connection con, QueryResult queryResult) throws SQLException {
                        }

                        @Override
                        public void queryExecuted(Connection con, QueryResult queryResult) throws SQLException {
                        }

                        @Override
                        public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {
                            con.rollback();
                            Platform.runLater(() -> ((MainController)getController()).showError("Error", th.getMessage(), th, getStage()));
                        }

                        @Override
                        public void finished(Connection con) throws SQLException {
                            con.commit();
                            Platform.runLater(() -> {
                                if (refresh) {
                                    Database databaseRef = null;
                                    try {
                                        databaseRef = DatabaseCache.getInstance().syncDatabase(compareInfo.getTargetDatabase().getName(), targetParams.cacheKey(), service);
                                    } catch (SQLException ex) {
                                        log.error("Error", ex);
                                    }
                                    
                                    compareInfo.setTargetDatabase(databaseRef);
                                    init(parentController, preferenceService, DatabaseCompareUtils.compare(compareInfo), compareInfo);
                                }
                                parentController.getUI().setDisable(false);                                
                            });
                        }

                        @Override
                        public boolean isWithLimit(QueryResult queryResult) {
                            return false;
                        }

                        @Override
                        public boolean isNeedStop() {
                            return false;
                        }
                    });
                } catch (Throwable ex) {
                    Platform.runLater(() -> ((MainController)getController()).showError("Error", ex.getMessage(), ex, getStage()));
                }
            }
        });
    }

    @FXML
    public void compareAction(ActionEvent event) {
        ((SchemaSynchronizerComparingController) ((MainController)getController()).getTypedBaseController("SchemaSynchronizerComparing")).show(this, compareResult);
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        titleSelectCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node n: onlyInSourceVBox.getChildren()) {
                ((DiffRow)n).selectCheckBox.setSelected(newValue != null ? newValue : false);
            }
            
            for (Node n: onlyInTargetVBox.getChildren()) {
                ((DiffRow)n).selectCheckBox.setSelected(newValue != null ? newValue : false);
            }
            
            for (Node n: differentVBox.getChildren()) {
                ((DiffRow)n).selectCheckBox.setSelected(newValue != null ? newValue : false);
            }
            
            for (Node n: equalVBox.getChildren()) {
                ((DiffRow)n).selectCheckBox.setSelected(newValue != null ? newValue : false);
            }
        });
        
        searchField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            updateAllData(compareResult, SORT_COMPARATOR);
        });
        
        titleTypeLabel.getStyleClass().add("syncResultTitleHeaderLabel");
        titleSourceLabel.getStyleClass().add("syncResultTitleHeaderLabel");
        titleSelectPane.getStyleClass().add("syncResultTitleHeaderLabel");
        titleOperationLabel.getStyleClass().add("syncResultTitleHeaderLabel");
        typeTargetLabel.getStyleClass().add("syncResultTitleHeaderLabel");
        
        titleTypeLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        titleTypeLabel.setOnMouseClicked((MouseEvent event) -> {
            doSort(titleTypeLabel, 0, event.isShiftDown());
        });
        
        titleSourceLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        titleSourceLabel.setOnMouseClicked((MouseEvent event) -> {
            doSort(titleSourceLabel, 1, event.isShiftDown());
        });
        
        titleOperationLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        titleOperationLabel.setOnMouseClicked((MouseEvent event) -> {
            doSort(titleOperationLabel, 2, event.isShiftDown());
        });
        
        typeTargetLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        typeTargetLabel.setOnMouseClicked((MouseEvent event) -> {
            doSort(typeTargetLabel, 3, event.isShiftDown());
        });
        
        
        RSyntaxTextAreaBuilder sourceBuilder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), null);
        SwingUtilities.invokeLater(() -> {
            sourceBuilder.init();
            sourceBuilder.getTextArea().setEditable(false);
            Platform.runLater(() -> {
                sourceQueryContainer.setCenter(sourceBuilder.getSwingNode());
            });
            sourceBuilder.getTextArea().setBackground(new Color(0, 0, 0, 0));
            sourceBuilder.getTextArea().setCurrentLineHighlightColor(new Color(0, 0, 0, 0));
        });
        
        
        RSyntaxTextAreaBuilder targetBuilder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), null);
        SwingUtilities.invokeLater(() -> {
            targetBuilder.init();
            targetBuilder.getTextArea().setEditable(false);
            Platform.runLater(() -> {
                targetQueryContainer.setCenter(targetBuilder.getSwingNode());        
            });
            targetBuilder.getTextArea().setBackground(new Color(0, 0, 0, 0));
            targetBuilder.getTextArea().setCurrentLineHighlightColor(new Color(0, 0, 0, 0));
        });
        
        
        
        sourceBuilder.getTextScrollPane().getVerticalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
            if (targetBuilder.getTextScrollPane().getVerticalScrollBar().getValue() != e.getValue()) {
                targetBuilder.getTextScrollPane().getVerticalScrollBar().setValue(e.getValue());
            }
        });
        
        sourceBuilder.getTextScrollPane().getHorizontalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
            if (targetBuilder.getTextScrollPane().getHorizontalScrollBar().getValue() != e.getValue()) {
                targetBuilder.getTextScrollPane().getHorizontalScrollBar().setValue(e.getValue());
            }
        });
        
        
        targetBuilder.getTextScrollPane().getVerticalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
            if (sourceBuilder.getTextScrollPane().getVerticalScrollBar().getValue() != e.getValue()) {
                sourceBuilder.getTextScrollPane().getVerticalScrollBar().setValue(e.getValue());
            }
        });
        
        targetBuilder.getTextScrollPane().getHorizontalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
            if (sourceBuilder.getTextScrollPane().getHorizontalScrollBar().getValue() != e.getValue()) {
                sourceBuilder.getTextScrollPane().getHorizontalScrollBar().setValue(e.getValue());
            }
        });
        
        
        selectedRow.addListener((ObservableValue<? extends DiffRow> observable, DiffRow oldValue, DiffRow newValue) -> {
            if (oldValue != null) {
                oldValue.deselect();
            }
            
            if (newValue != null) {
                newValue.select();
            }
            
            String sourceQuery = newValue != null && newValue.item.getCreateSourceQuery() != null ? newValue.item.getCreateSourceQuery() : "";
            String targetQuery = newValue != null && newValue.item.getCreateTargetQuery() != null ? newValue.item.getCreateTargetQuery() : "";

            List<Character> sourceList = new ArrayList<>();
            for (char c: sourceQuery.toCharArray()) {
                sourceList.add(c);
            }
              
            List<Character> targetList = new ArrayList<>();
            for (char c: targetQuery.toCharArray()) {
                targetList.add(c);
            }
            
            
            sourceBuilder.clearCustomBackground();
            targetBuilder.clearCustomBackground();
            
            if (!sourceList.isEmpty() && !targetList.isEmpty()) {
                Patch result = DiffUtils.diff(sourceList, targetList);
                if (result.getDeltas() != null) {
                    for (Delta d: (List<Delta>)result.getDeltas()) {
                        sourceBuilder.addCustomBackground(d.getOriginal().getPosition(), d.getOriginal().getPosition() + d.getOriginal().getLines().size(), new Color(0, 255, 0, 100));
                        targetBuilder.addCustomBackground(d.getRevised().getPosition(), d.getRevised().getPosition() + d.getRevised().getLines().size(), new Color(255, 0, 0, 100));
                    }
                }
            }
            
            int sourceLines = StringUtils.countMatches(sourceQuery, "\n");
            int targetLines = StringUtils.countMatches(targetQuery, "\n");
            if (sourceLines > targetLines) {
                for (int i = 0; i < sourceLines - targetLines; i++) {
                    targetQuery += "\n";
                }
            } else if (targetLines > sourceLines) {
                for (int i = 0; i < targetLines - sourceLines; i++) {
                    sourceQuery += "\n";
                }
            }
            
            
            sourceBuilder.getTextArea().setText(sourceQuery);
            SwingUtilities.invokeLater(() -> sourceBuilder.getTextArea().setCaretPosition(0));
            
            targetBuilder.getTextArea().setText(targetQuery);
            SwingUtilities.invokeLater(() -> targetBuilder.getTextArea().setCaretPosition(0));
        });
    }
    
    private void doSort(Label label, int index, boolean multi) {

        int value = sortOrder[index];
        
        if (!multi) {
            clearSortOrder();
        }

        switch (value) {
            case 0:
                sortOrder[index] = 1;
                label.setGraphic(new ImageView(IMAGE_ASC));
                break;

            case 1:
                sortOrder[index] = -1;
                label.setGraphic(new ImageView(IMAGE_DESC));
                break;

            case -1:
                sortOrder[index] = 0;
                label.setGraphic(new ImageView(IMAGE_EMPTY));
                break;
        }
        
        updateAllData(compareResult, SORT_COMPARATOR);
    }

    private void clearSortOrder() {
        sortOrder[0] = 0;
        titleTypeLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        
        sortOrder[1] = 0;
        titleSourceLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        
        sortOrder[2] = 0;
        titleOperationLabel.setGraphic(new ImageView(IMAGE_EMPTY));
        
        sortOrder[3] = 0;
        typeTargetLabel.setGraphic(new ImageView(IMAGE_EMPTY));
    }

    
    private class DiffRow extends HBox  {
        
        private final Label typeLabel = new Label();
        private final Label sourceLabel = new Label();
        private final HBox selectHBox = new HBox();
        private final CheckBox selectCheckBox = new CheckBox();
        private final HBox operationHBox = new HBox();        
        private final ComboBox<SyncCompareResultItem.Operation> operationCombobox = new ComboBox();
        private final Label targetLabel = new Label();
        
        private final SyncCompareResultItem item;
        
        private DiffRow(SyncCompareResultItem item, boolean last, VBox parent) {
            
            this.item = item;
            
            typeLabel.setText(item.getType().toString());
            typeLabel.setGraphic(new ImageView(item.getType().getImage()));
            typeLabel.setAlignment(Pos.CENTER_RIGHT);
            typeLabel.setContentDisplay(ContentDisplay.RIGHT);
            
            sourceLabel.setText(item.getSource());
            sourceLabel.setAlignment(Pos.CENTER_RIGHT);
            
            selectCheckBox.setSelected(item.isSelected());
            
            
            operationCombobox.setCellFactory((ListView<SyncCompareResultItem.Operation> param) -> new ListCell<SyncCompareResultItem.Operation>() {
                @Override
                protected void updateItem(SyncCompareResultItem.Operation item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                        setText("");
                    } else {
                        setGraphic(new ImageView(item.getImage()));
                        setText(item.toString());
                    }
                }                
            });
            operationCombobox.setButtonCell(new ListCell<SyncCompareResultItem.Operation>() {
                @Override
                protected void updateItem(SyncCompareResultItem.Operation item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                        setText("");
                    } else {
                        setGraphic(new ImageView(item.getImage()));
                        setText(item.toString());
                    }
                }                
            });
            
            if (parent == onlyInSourceVBox){
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.NONE);
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.CREATE);
                
            } else if (parent == onlyInTargetVBox){
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.NONE);
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.DROP);
                
            } else if (parent == equalVBox){
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.NONE);
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.DROP);
                
            } else {
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.NONE);
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.DROP);
                operationCombobox.getItems().add(SyncCompareResultItem.Operation.UPDATE);
            }
            
            operationCombobox.getSelectionModel().select(item.getOperation());
            
            
            
            targetLabel.setText(item.getTarget());
            targetLabel.setTextAlignment(TextAlignment.RIGHT);
            
            setFillHeight(true);
            
            if (last) {
                typeLabel.getStyleClass().add("diffRow-typeLabel-last");
                sourceLabel.getStyleClass().add("diffRow-sourceLabel-last");
                targetLabel.getStyleClass().add("diffRow-targetLabel-last");
                selectHBox.getStyleClass().add("diffRow-selectHBox-last");
                operationHBox.getStyleClass().add("diffRow-operationHBox-last");
            } else {
                typeLabel.getStyleClass().add("diffRow-typeLabel");
                sourceLabel.getStyleClass().add("diffRow-sourceLabel");
                targetLabel.getStyleClass().add("diffRow-targetLabel");
                selectHBox.getStyleClass().add("diffRow-selectHBox");
                operationHBox.getStyleClass().add("diffRow-operationHBox");
            }
            
            operationCombobox.setPrefHeight(25);
            operationCombobox.setMinHeight(25);
            operationCombobox.setMaxHeight(25);
            operationCombobox.getStyleClass().add("diffRow-combobox");
            operationCombobox.setFocusTraversable(false);
            
            
            selectCheckBox.setFocusTraversable(false);
            selectHBox.getChildren().add(selectCheckBox);
            selectHBox.setAlignment(Pos.CENTER);
            selectHBox.setPrefWidth(50);
            selectHBox.setMinWidth(50);
            selectHBox.setMaxWidth(50);
            
            operationHBox.getChildren().add(operationCombobox);
            operationHBox.setAlignment(Pos.CENTER);
            operationHBox.setPrefWidth(105);
            operationHBox.setMinWidth(105);
            operationHBox.setMaxWidth(105);
            
            HBox.setHgrow(operationCombobox, Priority.ALWAYS);
            HBox.setHgrow(sourceLabel, Priority.ALWAYS);
            HBox.setHgrow(targetLabel, Priority.ALWAYS);
            
            
            operationCombobox.setMaxWidth(Double.MAX_VALUE);
            sourceLabel.setMaxWidth(Double.MAX_VALUE);
            targetLabel.setMaxWidth(Double.MAX_VALUE);
            
            operationCombobox.setMaxHeight(Double.MAX_VALUE);
            sourceLabel.setMaxHeight(Double.MAX_VALUE);
            targetLabel.setMaxHeight(Double.MAX_VALUE);
            typeLabel.setMaxHeight(Double.MAX_VALUE);
            
            
            getChildren().addAll(typeLabel, sourceLabel, selectHBox, operationHBox, targetLabel);
            
            
            setOnMouseClicked((MouseEvent event) -> {
                selectedRow.set(this);
            });
            
            
            selectCheckBox.setOnMouseClicked((MouseEvent event) -> {
                selectedRow.set(this);
            });
            
            operationCombobox.setOnMouseClicked((MouseEvent event) -> {
                selectedRow.set(this);
            });
            
            DoubleBinding bind = widthProperty().subtract(typeLabel.widthProperty()).subtract(selectHBox.widthProperty()).subtract(operationHBox.widthProperty());
            sourceLabel.prefWidthProperty().bind(bind.divide(2));
            targetLabel.prefWidthProperty().bind(bind.divide(2));
            
            
            selectCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                item.setSelected(newValue != null ? newValue : false);
                if (item.isSelected()) {
                    totalSelected++;
                } else {
                    totalSelected--;
                }
                
                allCountLabel.setText(String.format(TOTAL_TEXT, totalSelected, totalRows));
                
                Parent p = getParent();
                if (p == onlyInSourceVBox) {
                    onlyInSourceContainer.setText(String.format(ONLY_IN_SOURCE_TEXT, getSelectedCount(onlyInSourceVBox.getChildren()), onlyInSourceVBox.getChildren().size()));
                
                } else if (p == onlyInTargetVBox) {
                    onlyInTargetContainer.setText(String.format(ONLY_IN_TARGET_TEXT, getSelectedCount(onlyInTargetVBox.getChildren()), onlyInTargetVBox.getChildren().size()));
                
                } else if (p == differentVBox) {
                    differentContainer.setText(String.format(DIFFERENT_TEXT, getSelectedCount(differentVBox.getChildren()), differentVBox.getChildren().size()));
                
                } else if (p == equalVBox) {
                    equalsContainer.setText(String.format(EQUAL_TEXT, equalVBox.getChildren().size()));
                }
            });
            
            operationCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends SyncCompareResultItem.Operation> observable, SyncCompareResultItem.Operation oldValue, SyncCompareResultItem.Operation newValue) -> {
                item.setOperation(newValue);
            });
        }
       
        
        public void select() {
            if (!typeLabel.getStyleClass().contains("diffRow-selected")) {
                typeLabel.getStyleClass().add("diffRow-selected");
                sourceLabel.getStyleClass().add("diffRow-selected");
                targetLabel.getStyleClass().add("diffRow-selected");
                selectHBox.getStyleClass().add("diffRow-selected");
                operationHBox.getStyleClass().add("diffRow-selected");
            }
        }
        
        public void deselect() {
            typeLabel.getStyleClass().remove("diffRow-selected");
            sourceLabel.getStyleClass().remove("diffRow-selected");
            targetLabel.getStyleClass().remove("diffRow-selected");
            selectHBox.getStyleClass().remove("diffRow-selected");
            operationHBox.getStyleClass().remove("diffRow-selected");
        }
    }
    
    private int getSelectedCount(List<Node> nodes) {

        int count = 0;
        for (Node n: nodes) {
            if (n instanceof DiffRow) {
                if (((DiffRow)n).item.isSelected()) {
                    count++;
                }
            } else {
                return 0;
            }
        }

        return count;
    }
    
}
