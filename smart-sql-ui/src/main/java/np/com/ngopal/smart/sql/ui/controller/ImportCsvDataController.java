package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.utils.ImportUtils;

/**
 *
 * @author terah
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ImportCsvDataController extends BaseController implements Initializable {
    

    private ConnectionSession session;
    private DBTable initialTable;
    
    private Stage stage;
    
    @FXML
    Label titleLabel;
    
    @FXML
    AnchorPane mainUI; 
    
    @FXML
    ListView<DBTable> tablesListview;
     
    @FXML
    ListView<Column> columnListview;
     
    @FXML
    Button columnSAButton;
     
    @FXML
    Button columnDSAButton; 
    
    @FXML
    TextField fieldsTerminated;     
    @FXML
    TextField fieldsEnclosed;
    @FXML
    CheckBox enclosedOptionally;     
    @FXML
    TextField fieldsEscaped;     
    @FXML
    TextField lineTerminated;
     
     
    @FXML
    ComboBox<String> charsetBox;
     
    @FXML
    CheckBox lowPriorityCB;
     
    @FXML
    CheckBox concurrentCB;
     
    @FXML
    CheckBox replaceCB;
     
    @FXML
    CheckBox ignoreCB;
    
    @FXML
    CheckBox ignoreTF;
     
    @FXML
    TextField ignoreTextField;
     
    @FXML
    TextField importFilePathTextField;
     
    @FXML
    Button importFileDirectoryChooser;
     
    @FXML
    Button importButton;
     
    @FXML
    Button closeButton;
    
    @FXML
    private ImageView gridViewImageView;
   
    private volatile boolean needStop = false;
    
    @Override
    public AnchorPane getUI() {
    return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        gridViewImageView.setImage(StandardDefaultImageProvider.getInstance().getGridView_image());
        
        closeButton.setOnAction(onAction);
        importButton.setOnAction(onAction);
        importFileDirectoryChooser.setOnAction(onAction);
        replaceCB.setOnAction(onAction);
        columnDSAButton.setOnAction(onAction);
        concurrentCB.setOnAction(onAction);
        ignoreTF.setOnAction(onAction);
        lowPriorityCB.setOnAction(onAction);
        columnSAButton.setOnAction(onAction);
        
        sessionCaller();
    }
    
    //session codes
    private boolean called = false;
    private final Collator stringCollator = Collator.getInstance();
    private void sessionCaller(){
        if(session == null) {
            return;
        }
        
        if(called) {
            return;
        }
        
        charsetBox.getItems().clear();
        
        try {
            charsetBox.getItems().setAll(session.getService().getCharSet());  
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error loading charset", ex);
        }
        charsetBox.getItems().add(0, "[default]");
        charsetBox.getSelectionModel().selectFirst();
        
        ObservableList<DBTable> tables = FXCollections.observableArrayList(DatabaseCache.getInstance().getTables(session.getConnectionParam().cacheKey(), session.getConnectionParam().getSelectedDatabase().getName()));
        
        tables.sort((DBTable o1, DBTable o2) -> {
            return stringCollator.compare(o1.getName(), o2.getName());
        });
        
        tablesListview.getItems().setAll(tables);
        
        tablesListview.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends DBTable> observable, DBTable oldValue, DBTable newValue) -> {
            columnListview.getItems().clear();
            
            DBTable table = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), session.getConnectionParam().getSelectedDatabase().getName(), newValue.toString());
            
            columnListview.getItems().addAll(table.getColumns());
            columnSAButton.fire();
        });
        
        columnListview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        if (initialTable != null) {
            for (DBTable t: tablesListview.getItems()) {
                if (t.getName().equals(initialTable.getName())) {
                    tablesListview.getSelectionModel().select(t);
                    break;
                }
            }
            
        } else {
            tablesListview.getSelectionModel().selectFirst();
        }
        called = true;
    }
    
    /**
     * //////////////EMPHASIS FOR CODE IMPLEMENTORS //////////////// 
     * This is the event handler for every Node which fires ActionEvent, all
     * Nodes declared globally are featured here
     */
    private final EventHandler<ActionEvent> onAction = new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            Node n = (Node)event.getSource();
            if (n == closeButton) {
                needStop = true;
                stage.close();
            } else if (n == importButton) {
                doImport();
            }else if(n == importFileDirectoryChooser){
                try{
                    FileChooser fc = new FileChooser();
                    fc.setTitle("Open File");
                    fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(
                        "CSV Files", "*.csv"),
                        new FileChooser.ExtensionFilter(
                        "All Files", "*.*"));
                    File file = fc.showOpenDialog(getStage());
                    if (file != null) {
                        importFilePathTextField.setText(file.getAbsolutePath());
                    }
                }catch(NullPointerException npe){}
               
            }else if(n == replaceCB){
                if(replaceCB.isSelected()){
                    ignoreCB.setSelected(false);
                }
            }else if(n == columnDSAButton){
                columnListview.getSelectionModel().clearSelection();
            }else if(n == columnSAButton){
                columnListview.getSelectionModel().selectAll();
            }else if(n == concurrentCB){
                if(concurrentCB.isSelected()){
                    lowPriorityCB.setSelected(false);
                }
            }else if(n == ignoreCB){
                if(ignoreCB.isSelected()){
                    replaceCB.setSelected(false);
                }
            }else if(n == lowPriorityCB){
                if(lowPriorityCB.isSelected()){
                    concurrentCB.setSelected(false);
                }
            }else if(n == ignoreTF){
                ignoreTextField.setDisable(!ignoreTF.isSelected());
            }
        }
    };
    
    private void doImport() {
        
        List<String> selectedColumns = new ArrayList<>();
        List<Column> columns = columnListview.getSelectionModel().getSelectedItems();
        if (columns == null || columns.isEmpty()) {
            DialogHelper.showInfo(getController(), "Info", "Please select at least one field", null);
            return;
        } else {
            for (Column c: columns) {
                selectedColumns.add(c.getName());
            }
        }
        
        String fileName = importFilePathTextField.getText();
        if (fileName == null || fileName.isEmpty()) {
            DialogHelper.showInfo(getController(), "Info", "Please specify a file name", null);
            return;
        }
        
        Integer ignoreCount = null;
        if (ignoreTF.isSelected()) {
            try {
                ignoreCount = Integer.valueOf(ignoreTextField.getText());
            } catch (NumberFormatException ex) {
                DialogHelper.showError(getController(), "Error", "Error parsing ignore count", ex);
            }
        }
        
        String charset = charsetBox.getSelectionModel().getSelectedItem();
        if ("[default]".equals(charset)) {
            charset = null;
        }
        
        int records = 0;
        
        try {
            records = Files.readAllLines(new File(fileName).toPath()).size();
        } catch (IOException ex) {
        }
        
        int staticRecords = records;
        
        ImportUtils.importDelimiter(session.getService(), fileName, session.getConnectionParam().getSelectedDatabase().getName(), tablesListview.getSelectionModel().getSelectedItem().getName(),
                    charset, lowPriorityCB.isSelected() ? "LOW_PRIORITY" : concurrentCB.isSelected() ? "CONCURRENT" : null,
                    replaceCB.isSelected() ? "REPLACE" : ignoreCB.isSelected() ? "IGNORE" : null,
                    fieldsTerminated.getText(), fieldsEnclosed.getText(), enclosedOptionally.isSelected(), fieldsEscaped.getText(), lineTerminated.getText(),
                    ignoreCount, selectedColumns, new ImportUtils.ImportStatusCallback() {
                        
            @Override
            public boolean isNeedStop() {
                return needStop;
            }

            @Override
            public void progress(double progress) {
            }
            
            @Override
            public void error(String errorMessage, Throwable th) {
                Platform.runLater(() -> {
                   DialogHelper.showError(getController(), "Error", errorMessage, th); 
                });
            }

            @Override
            public void success(QueryResult queryResult, Map<String, Object> map) {
                ImportResultController contr = ((MainController)getController()).getTypedBaseController("ImportResult");
                contr.init(staticRecords, 0, 0, queryResult.getWarningMessages() != null ? queryResult.getWarningMessages().size() : 0, queryResult, stage);
            }
        });
    }

    public void init(ConnectionSession session, DBTable table) {
        this.session = session;
        this.initialTable = table;
    }    
    
    public void show(Window window){
        if(window == null)
            return;
        stage = new Stage(StageStyle.UTILITY);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(window);
        stage.setScene(new Scene(getUI()));
        stage.setTitle("Import CSV Data using LOAD LOCAL");
        sessionCaller();
        stage.showAndWait();
    }
    
}
