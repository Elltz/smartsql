 package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import com.sun.javafx.scene.control.skin.LabeledText;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.SQLListener;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.model.*;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.ui.components.ObjectViewClickListeners;
import np.com.ngopal.smart.sql.ui.factories.TextFieldTreeCellImpl;
import javafx.geometry.Point2D;
import com.sun.javafx.util.Utils;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Menu;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import np.com.ngopal.smart.sql.core.modules.utils.Flags;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.db.HistoryQueryService;
import np.com.ngopal.smart.sql.db.HistoryTemplateService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PostgresDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
 import np.com.ngopal.smart.sql.ui.components.AnimatableTreeItem;
import np.com.ngopal.smart.sql.structure.queryutils.FontUtils;
import np.com.ngopal.smart.sql.structure.queryutils.RsSyntaxCompletionHelper;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;



/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class ConnectionTabContentController extends TabContentController
        implements SQLListener, ChangeListener {

    @Inject
    private ConnectionParamService service;

    @Inject
    private SQLLogger logger;
    
    @Inject
    private FavouritesService favoriteService;
    
    @Inject
    private HistoryQueryService historyQueryService;
    
    @Inject
    private HistoryTemplateService historyTemplateService;
    
    @Inject
    private PreferenceDataService preferenceService;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private SplitPane mainSplitter;

    @FXML
    private AnchorPane leftPane;

    @FXML
    private SplitPane subSplitter;

    @FXML
    private AnchorPane centerPane;

    @FXML
    private AnchorPane bottomPane;

    @FXML
    private TextField filterField;

    @FXML
    private ImageView clearImageView;

    @FXML
    private TreeView treeView;
    
    private volatile Database selectedDatabase;
    
    @FXML
    private VBox searchContainer;
    
    private FilterTreeItem currentFilterItem;    
    
    private volatile boolean filtering = false;
    private volatile boolean onExpanding = false;
    
    private Map<Tab, Tab> selectedTabs = new HashMap<>();
        
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    
    private static final List<String> BASE_DBS = Arrays.asList(new String[] {"mysql", "information_schema", "performance_schema", "sys"});
    
    private static final String DRAG_CONTENT__TABLE_OR_VIEW = "tableOrView|%s|%s|SELECT %s.* FROM %s %s;";
    private static final String DRAG_CONTENT__FUNC_OR_PROC  = "functionOrProcudure|%s||call %s;";
    private static final String DRAG_CONTENT__COLUMN = "column|%s|%s|%s|SELECT %s FROM %s;";
    
    public static final String TREE_NODE__ERDIAGRAM = "ERDiagram";
    
    private Image serverImage;
    private Image serverFilterImage;
    
    public static Image databaseImage = null;

    public static Image folderImage = null;
    public static Image folderFilterImage = null;
    
    public static Image tablesImage = null;
    public static Image viewsImage = null;
    public static Image functionsImage = null;
    public static Image triggersImage = null;
    public static Image proceduresImage = null;
    public static Image eventsImage = null;

    public static Image tableImage = null;

    public static Image columnImage = null;

    public static Image primaryKeyImage = null;
    
    public static Image indexImage = null;

    public static Image viewImage = null;

    public static Image functionImage = null;

    public static Image storedProcImage = null;

    public static Image triggerImage = null;

    public static Image eventImage = null;

    private TabPane centertabPane;

    private TabPane bottomtabPane;

    private TreeItem currentRoot;

    private TreeItem firstFilterdItem;

    private boolean needFilter = true;

    private boolean needAutocomlete = true;
        
    private ArrayList<Node> disClosureButtons = new ArrayList();
    
    private String selectedBottomTabId;
        
    private SimpleObjectProperty<ObjectViewClickListeners> ovcl 
            = new SimpleObjectProperty<>();
    
    private SimpleBooleanProperty leftPaneState = new SimpleBooleanProperty(true);

    @FXML
    private CheckBox filterColumns;
    
    private List<Consumer> dbMouseListeners;
    
    private Font currentFont;
    
    @Override
    public void changeFont(String font) {
        currentFont = FontUtils.parseFXFont(font);
        treeView.refresh();
    }
    
    @Override
    public void addOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        if (dbMouseListeners == null) {
            dbMouseListeners = new ArrayList<>();
        }
        
        dbMouseListeners.add(listener);
    }
    
    @Override
    public void removeOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        if (dbMouseListeners != null) {
            dbMouseListeners.remove(listener);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        currentFont = FontUtils.parseFXFont(preferenceService.getPreference().getFontsObjectBrowser());
        
        treeView.backgroundProperty().addListener(
                new ChangeListener<Background>() {
                    
            Background back;                    
            @Override
            public void changed(ObservableValue<? extends Background> observable,
                    Background oldValue, Background newValue) { 
                if (newValue != null) {                    
                    
                    String background = getBackgroundColor(getConnectionParams());
                    if (background != null) {
                        if (newValue.getFills().get(0).getFill().equals(Color.web(background))) {
                            back = newValue;
                        } else {
                            if (oldValue != null && 
                                    oldValue.getFills().get(0).getFill().equals(Color.web(background))) {
                                back = oldValue;
                            }
                        }
                    }
                }
                
                if (back != null) {
                    searchContainer.setBackground(back);
                }
            }
        });
        searchContainer.setBackground(new Background(
                new BackgroundFill(Color.WHITE,CornerRadii.EMPTY,Insets.EMPTY)));
                
        treeView.setEditable(true);
        addConnectionTabClickListenerToLeftTree();

        getController().getStage().addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {

            if (event.isControlDown() && event.isShiftDown() && event.getCode() == KeyCode.B) {
                filterField.requestFocus();
                event.consume();
            }
        });
        
        filterColumns.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            filter(filterField.getText());
        });

        filterField.getStyleClass().add("clearableTextField");
        filterField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {

            if (needFilter) {
                filter(newValue);
            }
        });

        filterField.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {

            switch (event.getCode()) {
                case ESCAPE:
                    filterField.setText("");
                    event.consume();
                    break;

                case ENTER:
//                    filterField.positionCaret(filterField.getLength());
//                    autocompleteFilter(filterField.getText());

                    if (firstFilterdItem != null) {
                        treeView.getSelectionModel().select(firstFilterdItem);
                    }
                    event.consume();
                    break;

                case LEFT:
                    currentFilterItem.setFilterText(filterField.getText());
                    break;

                case RIGHT:
                    currentFilterItem.setFilterText(filterField.getText());
                    break;

                case END:
                    currentFilterItem.setFilterText(filterField.getText());
                    break;

                case HOME:
                    currentFilterItem.setFilterText(filterField.getText());
                    break;

                case DELETE:
                case BACK_SPACE:
                    needAutocomlete = false;
                    
                    IndexRange selectedRange = filterField.getSelection();
                    // if selected text exist and was pressed DELETE or BACKSAPCE
                    // thah remove selected text
                    if (selectedRange != null && selectedRange.getLength() > 0) {
                        filterField.deleteText(selectedRange);                        
                        
                    } else {                        
                        if (event.getCode() == KeyCode.DELETE) {
                            filterField.deleteNextChar();
                        } else {
                            filterField.deletePreviousChar();
                        }
                    }
                    
                    needAutocomlete = true;
                    event.consume();
                    break;
            }
        });

        clearImageView.setOnMouseClicked((MouseEvent event) -> {
            needAutocomlete = false;
            filterField.setText("");
            needAutocomlete = true;
        });
        
        initTabPane();
        
        treeView.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                
                TreeItem ti = (TreeItem) treeView.getSelectionModel().getSelectedItem();
                if (ti instanceof ErrDiagramTreeItem) {                 
                    ConnectionParams cp = getConnectionParams().copyOnlyParams();
                    cp.setSelectedModuleClassName(SchemaDesignerModule.class.getName());
                    cp.setDatabase(Arrays.asList(new String[]{((ErrDiagramTreeItem)ti).getDatabase().getName()}));
                    cp.setMakeBackwardEngine(true);
                    
                    ((MainController)getController()).openConnection(getDbService(), cp, true, null);

                } else if (dbMouseListeners != null) {
                    Object value = ti != null ? ti.getValue() : null;
                    for (Consumer c: dbMouseListeners) {
                        c.accept(value);
                    }
                }
            }
        });  
        
        /**
         * I am adding this so the Main Splitter be able to recognize divider ratios even after resize
         */
        mainSplitter.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double ratio = 330 / newValue.doubleValue();
                mainSplitter.setDividerPositions(ratio);
            }
        });
    }
    
    
    private void filter(String newValue) {
        
        filtering = true;
        
        // make visible clear view if text non empty
        clearImageView.setVisible(newValue != null && !newValue.isEmpty());

        TreeItem selectedItem = (TreeItem) treeView.getSelectionModel().getSelectedItem();
        treeView.getSelectionModel().clearSelection();

        currentFilterItem.setFilterText(newValue);

        expandTreeItem(currentFilterItem, selectedItem);

        boolean oldAutocomplete = needAutocomlete;
        Platform.runLater(() -> {

            treeView.getSelectionModel().select(currentFilterItem);

            if (oldAutocomplete == true){
                autocompleteFilter(currentFilterItem.getFilterText());
            }
            
            filtering = false;
        });
    }
    
//
//    private void checkFilterFieldKey(KeyEvent event, int currentPosition) {
//        IndexRange selectedRange = filterField.getSelection();
//        if (selectedRange != null && selectedRange.getLength() > 0) {
//            needAutocomlete = false;
//            filterField.positionCaret(currentPosition);
//            autocompleteFilter(filterField.getText());
//            needAutocomlete = true;
//
//            event.consume();
//        }
//    }
    
    private final ChangeListener connectionTabChangeListener = new ChangeListener() {
        @Override
        public void changed(ObservableValue ov, Object oldValue, Object newValue) {
            if (newValue instanceof TreeItem && (newValue == currentRoot || ((TreeItem) newValue).getParent() != null)) {
                Object value = ((TreeItem) newValue).getValue();

                if ((value instanceof ConnectionParams && newValue instanceof FilterTreeItem) || value instanceof DatabaseChildrenType) {
                    currentFilterItem = (FilterTreeItem) newValue;

                } else if (value instanceof View || value instanceof DBTable || value instanceof StoredProc || value instanceof Function || value instanceof Trigger || value instanceof Event) {
                    currentFilterItem = (FilterTreeItem) ((TreeItem) newValue).getParent();

                } else if (value instanceof Column || value instanceof Index) {
                    currentFilterItem = (FilterTreeItem) ((TreeItem) newValue).getParent().getParent().getParent();

                } else if (value instanceof Database) {
                    for (Object ti : ((TreeItem) newValue).getChildren()) {
                        if (((TreeItem) ti).getValue() == DatabaseChildrenType.TABLES) {
                            currentFilterItem = (FilterTreeItem) ti;
                        }
                    }

                } else {
                    currentFilterItem = (FilterTreeItem) ((TreeItem) newValue).getParent().getParent();
                }

                // if currentFilterItem is NULL - its wrong
                needFilter = false;

                int oldPosition = filterField.getCaretPosition();

                needAutocomlete = false;
                filterField.setText(currentFilterItem.getFilterText() != null ? currentFilterItem.getFilterText() : "");
                filterField.positionCaret(oldPosition);
                needAutocomlete = true;

                needFilter = true;
            }
        }
    };

    public void addConnectionTabClickListenerToLeftTree() {
        treeView.getSelectionModel().selectedItemProperty().addListener(connectionTabChangeListener);
    }

    public void removeConnectionTabClickListenerFromLeftTree() {
        treeView.getSelectionModel().selectedItemProperty().removeListener(connectionTabChangeListener);
    }
    
    private void expandTreeItem(TreeItem item, TreeItem selectedItem) {
        
        if (!item.getChildren().isEmpty()) {
            
            boolean expanded = ((!(item.getValue() instanceof Database && selectedItem != null && selectedItem.getValue() instanceof ConnectionParams) &&
                !(item.getValue() instanceof DBTable)) || !filterField.getText().isEmpty());
            item.setExpanded(expanded);
            if (expanded) {
                
                if (!(item.getValue() instanceof ConnectionParams)) {
                    for (TreeItem ti: (List<TreeItem>)item.getChildren()) {
                        if (!(ti.getValue() instanceof String && ti.getValue().toString().equals("Indexes")) && !(ti.getValue() instanceof DBTable && !filterColumns.isSelected())) {
                            expandTreeItem(ti, selectedItem);
                        } else {
                            ti.setExpanded(false);
                        }
                    }
                }
            }
        }
    }
    

    private void autocompleteFilter(String text) {
        
        if (needAutocomlete && text != null && !text.isEmpty()) {
            if (currentFilterItem != null && !currentFilterItem.getChildren().isEmpty()) {

                for (Object obj: currentFilterItem.getChildren()) {
                    TreeItem ti = (TreeItem)obj;

                    if (ti.getValue().toString().toLowerCase().startsWith(text.toLowerCase())) {
                        firstFilterdItem = ti;
                        break;
                    }
                }

                // no need add autocomplete if we remove chars
                if (firstFilterdItem != null) {
                    needFilter = false;
                    needAutocomlete = false;
                    
                    String t = firstFilterdItem.getValue().toString().toLowerCase();
                    if (t.contains(text.toLowerCase())) {
                        String autocompleteText = t.substring(t.indexOf(text.toLowerCase()) + text.length());

                        int oldPosition = filterField.getCaretPosition();

                        filterField.appendText(autocompleteText);

                        filterField.selectRange(filterField.getText().length(), oldPosition);
                    }
                    
                    needAutocomlete = true;
                    needFilter = true;
                }
            }
        }
    }

    @Override
    public DisconnectPermission destroyController(DisconnectPermission permission) {
        
        if (dbMouseListeners != null) {
            dbMouseListeners.clear();
        }
        
        FunctionHelper.debuggerClosed(getSession());
        
        /**
         * modules persister takes care of that
        QueryTabModule module = (QueryTabModule) controller.getModuleByClassName(QueryTabModule.class.getName());
        if (module.hasChanges()) {
            
            List<CodeArea> list = module.getChangedCodeAreas();
            for (CodeArea area: list) {
                if (!area.getText().isEmpty()) {
                    switch (permission) {

                        case NONE:
                            DialogResponce responce = showConfirm("SmartSQL", "Do you want to save the changes?", getStage(), true);
                            if (DialogResponce.OK_YES == responce || DialogResponce.YES_TO_ALL == responce) {
                                module.saveCodeAreaText(area);

                                if (DialogResponce.YES_TO_ALL == responce) {
                                    permission = UIController.DisconnectPermission.YES_TO_ALL;
                                }
                            } else if (DialogResponce.NO_TO_ALL == responce) {
                                permission = UIController.DisconnectPermission.NO_TO_ALL;
                            }
                            break;

                        case YES_TO_ALL:
                            module.saveCodeAreaText(area);
                            break;
                    }
                }
            }
        }
        */
        return permission;
    }

    /**
     * Filter connection tree
     */
//    private void filterConnectionTree(String filterText) {
//        // if filtered text is emty - just set current root
//        if (filterText.isEmpty()) {
//            treeView.setRoot(currentRoot);
//        } else {
//            // find filtered nodes
//            treeView.setRoot(foundFilterItem(currentRoot, filterText));
//        }
//    }

    /**
     * Recursion function for find all filtered nodes
     * <p>
     * @param parent Parent node
     * @param filterText Text for filtering
     * @return TreeItem filtered TreeItem, null if non found
     */
//    private TreeItem foundFilterItem(TreeItem parent, String filterText) {
//        TreeItem filteredParent = makeCopyOfTreeItem(parent);
//        for (Object n : parent.getChildren()) {
//            TreeItem ti = (TreeItem) n;
//            if (ti.getValue() != null) {
//
//                TreeItem filtered = null;
//
//                // if not start - try found in children
//                if (!ti.getValue().toString().toLowerCase().contains(filterText.toLowerCase())) {
//                    filtered = foundFilterItem(ti, filterText);
//                } else {
//                    filtered = makeCopyOfTreeItem(ti);
//
//                    if (firstFilterdItem == null && checkNeedFilter(ti)) {
//                        // save first filtered text for autocomplete
//                        firstFilterdItem = filtered;
//                    }
//
//                    // find inside item that already start with filtered text
//                    TreeItem temp = foundFilterItem(ti, filterText);
//                    if (temp != null) {
//                        filtered.getChildren().setAll(temp.getChildren());
//                        filtered.setExpanded(true);
//                    }
//                }
//
//                if (filtered != null) {
//                    // check node - we filter only: db, tables, columns, functions and etc..
//                    if (checkNeedFilter(ti) || !filtered.getChildren().isEmpty()) {
//                        filteredParent.getChildren().add(filtered);
//                    }
//                }
//            }
//        }
//
//        if (!filteredParent.getChildren().isEmpty()) {
//            filteredParent.setExpanded(true);
//            return filteredParent;
//        } else {
//            return null;
//        }
//    }
//
//    private boolean checkNeedFilter(TreeItem item) {
//        return item.getValue() instanceof DBElement;
//    }
//
//    private TreeItem makeCopyOfTreeItem(TreeItem source) {
//        TreeItem ti = new TreeItem(source.getValue(), source.getGraphic());
//        ti.setExpanded(source.isExpanded());
//
//        return ti;
//    }

    @Override
    public void refreshTree(boolean alertDatabaseCount) {
        TreeItem selected = (TreeItem) treeView.getSelectionModel().getSelectedItem();
        
        if (selected == treeView.getRoot()) {
            
            addRootChildren(true);
            
        } else {

            List expandedPath;
            if (selected != null) {
                expandedPath = new ArrayList();
                while (selected != null) {
                    if (selected != treeView.getRoot()) {
                        expandedPath.add(selected.getValue());
                    }

                    selected = selected.getParent();
                }

                Collections.reverse(expandedPath);
            } else {
                expandedPath = null;
            }

            Database selectedDatabse = getConnectionParams().getSelectedDatabase();
            if (selectedDatabse == null && alertDatabaseCount && getConnectionParams().getDatabases() != null && getConnectionParams().getDatabases().size() >= 20) {
                Platform.runLater(() -> {
                    DialogHelper.showInfo(getController(), "Info", "The Refresh may take more than one minute.\nPlease refresh individual database.", null);
                });            
            }

            if (selectedDatabse != null) {
                // delete old cache file
                try {
                    File f = new File("caches/" + getConnectionParams().getId() + "_" + selectedDatabse.getName() + ".json");
                    f.delete();
                } catch (Throwable th) {
                    log.error("Error", th);
                }

                try {
                    log.debug("Start syncDatabase");
                    selectedDatabse = DatabaseCache.getInstance().syncDatabase(selectedDatabse.getName(), getConnectionParams().cacheKey(), (AbstractDBService) getDbService(), true);
                    log.debug("Finish syncDatabase");
                } catch (SQLException ex) {
                    log.error("Error", ex);
                }

            }

            log.debug("Start create database tree structure");
            TreeItem ti = loadDatabaseTreeItem(selectedDatabse);
            loadDatabaseChildrenType(ti);
            log.debug("Finish create database tree structure");
            log.debug("Start rebuild tags for database");
            QueryTabModule queryTabModule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
            Collection<QueryAreaWrapper> list = queryTabModule.getAllCodeAreas();
            RsSyntaxCompletionHelper.getInstance().rebuildTags(getConnectionParams(), list);
            log.debug("Finish rebuild tags for database");

            // I try to replace only changed
    //        addRootChildren(selectedDatabse == null);

            String databaseName = selectedDatabse.getName();
            Platform.runLater(() -> {
                log.debug("Start prepare UI part");
                int i = 0;
                for (TreeItem ti0: ((FilterTreeItem)treeView.getRoot()).getInternalChildren()) {
                    if (((Database)ti0.getValue()).getName().equals(databaseName)) {
                        break;
                    }
                    i++;
                }

                treeView.getSelectionModel().clearSelection();

                treeView.getSelectionModel().selectedItemProperty().removeListener(ConnectionTabContentController.this);
                ((FilterTreeItem)treeView.getRoot()).getInternalChildren().set(i, ti);

                if (expandedPath != null) {
                    expandPath(((FilterTreeItem)treeView.getRoot()).getInternalChildren(), 0, expandedPath);
                }

                treeView.getSelectionModel().selectedItemProperty().addListener(ConnectionTabContentController.this);
                log.debug("Finish prepare UI part");
            });
        }
    }
    
    private void expandPath(List<TreeItem> items, int index, List expandedPath) {
        
        if (index < expandedPath.size()) {
            Object obj = expandedPath.get(index);

            for (TreeItem ti: items) {
                if (obj instanceof DBElement && ti.getValue() instanceof DBElement) {
                    
                    DBElement b0 = (DBElement)ti.getValue();
                    DBElement b1 = (DBElement)obj;
                    
                    if (b0 != null && b0.getClass().equals(b1.getClass()) && Objects.equals(b0.getName(), b1.getName())) {
                        ti.setExpanded(true);
                        if (index == expandedPath.size() - 1) {
                            Platform.runLater(() -> {
                               treeView.getSelectionModel().select(ti); 
                            });
                        }
                        expandPath(ti.getChildren(), ++index, expandedPath);
                        break;
                    }
                    
                } else if (obj.equals(ti.getValue())) {
                    ti.setExpanded(true);
                    if (index == expandedPath.size() - 1) {
                        Platform.runLater(() -> {
                            treeView.getSelectionModel().select(ti); 
                        });
                    }
                    expandPath(ti.getChildren(), ++index, expandedPath);
                }
            }            
        }
    }

    @Override
    public void showLeftPane() {
        if (!leftPaneState.get()) {
            mainSplitter.getItems().add(0, leftPane);
            mainSplitter.setDividerPositions(0.24);
            toggleLeftPaneState();
        }
    }

    @Override
    public void hideLeftPane() {
        if (leftPaneState.get()) {
            mainSplitter.getItems().remove(leftPane);
            toggleLeftPaneState();
        }
    }

    private void toggleLeftPaneState() {
        leftPaneState.set(!leftPaneState.get());
        
    }
    
    /**
     * if its true it means left pane is showing
     * @return 
     */ 
    @Override
    public ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty(){
        return ReadOnlyBooleanProperty.readOnlyBooleanProperty(leftPaneState);
    }

    /**
     * Adds database treeitem inside the database treenode
     */
    private void addRootChildren(boolean loadAll, String...loadDatabaseName) {
        
        try {
            
            List<TreeItem> treeItems = new ArrayList();
                        
            int size = getConnectionParams().getDatabase() != null ? getConnectionParams().getDatabase().size() : 0;

            // workarround for old connection
            if (size == 1 && getConnectionParams().getDatabase().get(0).trim().isEmpty()) {
                size = 0;
            }
            
            List<Database> list = getDbService().getDatabases();
            
//            if (loadAll || list.size() < 10) {
//                DatabaseCache.getInstance().syncDatabases(getDbService().loadDBStructure(), getConnectionParams(), (AbstractDBService) getDbService());
//            }
            
            if (getDbService() instanceof MysqlDBService) {
            for (Object o : list) {
                Database db = (Database) o;
                
                String dbName = db.getName();
                                
                Map<String, Object> m = ((AbstractDBService)getDbService()).loadDatabaseStructure(dbName);
                if (m != null) {
                    DatabaseCache.getInstance().loadDatabase(dbName, m, getConnectionParams().cacheKey(),
                            (AbstractDBService) getDbService(), false);
                }                
                
                if (size > 0 && getConnectionParams().getDatabase().contains(dbName) && getConnectionParams().getDatabases() != null) {
                    // if exist in connection params - use it
                    for (Database connectionDB : getConnectionParams().getDatabases()) {
                        if (connectionDB.getName().equals(dbName)) {
                            connectionDB.setCharset(db.getCharset());
                            connectionDB.setCollate(db.getCollate());
                            db = connectionDB;
                            break;
                        }
                    }
                }

                if (size == 0 || getConnectionParams().getDatabase().contains(dbName)) {                                        
                    treeItems.add(loadDatabaseTreeItem(db));
                }
            }              
            } else if (getDbService() instanceof PostgresDBService) {
                for (Object o : list) {
                    PgDatabase db = (PgDatabase) o;
                    treeItems.add(loadDatabaseTreeItem(db));
                }
            }

            getConnectionParams().setDatabases(list);
            
            Recycler.addWorkProc(listenBackground);
            
            Platform.runLater(() -> {
                treeView.getSelectionModel().selectedItemProperty().removeListener(ConnectionTabContentController.this);
                treeView.getSelectionModel().selectedItemProperty().addListener(ConnectionTabContentController.this);
                ((FilterTreeItem)treeView.getRoot()).getInternalChildren().clear();
                ((FilterTreeItem)treeView.getRoot()).getInternalChildren().addAll(treeItems);
                treeView.getSelectionModel().select(treeView.getRoot());
                isFocusedNow();
            });
                        
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error adding root node", ex);
        }
    }

    private TreeItem loadDatabaseTreeItem(Database db) {
        
        TreeItem<Database> treeItem = new AnimatableTreeItem<>();
        treeItem.setValue(db);
        if (databaseImage == null) {
            databaseImage = SmartImageProvider.getInstance().getDatabase_image();
        }
        treeItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(databaseImage), 20, 20));

        ChangeListener listener = (ChangeListener<Boolean>) (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean expanded) -> {
            if (expanded && !filtering) {
                onExpanding = true;
                treeView.getSelectionModel().select(treeItem);
                onExpanding = false;
            }
        };

        treeItem.expandedProperty().addListener(listener);

        if (folderImage == null) {
            folderImage = SmartImageProvider.getInstance().getFolder_image();
            folderFilterImage = SmartImageProvider.getInstance().getFolderFilter_image();

            tablesImage = SmartImageProvider.getInstance().getNodeTables_image();
            viewsImage = SmartImageProvider.getInstance().getNodeViews_image();
            functionsImage = SmartImageProvider.getInstance().getNodeFunctions_image();
            triggersImage = SmartImageProvider.getInstance().getNodeTriggers_image();
            proceduresImage = SmartImageProvider.getInstance().getNodeProcs_image();
            eventsImage = SmartImageProvider.getInstance().getNodeEvents_image();
        }

        for (DatabaseChildrenType children : DatabaseChildrenType.values()) {
            FilterTreeItem treeItem0 = new FilterTreeItem(children);
            treeItem0.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(folderImage), 16, 16));                        

            treeItem.getChildren().add(treeItem0);
            treeItem.setExpanded(false);

            switch (children) {
                case VIEWS:
                    treeItem0.getInternalChildren().add(new TreeItem());

                    treeItem0.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null && newValue && selectedDatabase != null) {
                            makeBusy(true, 15000, "Collecting Objects details from Metadata. It is a one-time job. Usually, it is a little bit slow if there are many tables.");
                            Thread th = new Thread(() -> {
                                try {
                                    List<View> views = getDbService().getViews(selectedDatabase);
                                    treeItem.getValue().setViews(views);
                                    DatabaseCache.getInstance().updateViewsCompletionData(getConnectionParams().cacheKey(), selectedDatabase);
                                    RsSyntaxCompletionHelper.getInstance().updateCompletionsListForViews(getConnectionParams(), selectedDatabase);
                                    Platform.runLater(() -> {
                                        loadViews(treeItem0, views);
                                        makeBusy(false);
                                    });
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            });
                            th.setDaemon(true);
                            th.start();
                        }
                    });
                    break;

                case FUNCTIONS:
                    treeItem0.getInternalChildren().add(new TreeItem());

                    treeItem0.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null && newValue && selectedDatabase != null) {
                            makeBusy(true, 15000, "Collecting Objects details from Metadata. It is a one-time job. Usually, it is a little bit slow if there are many tables.");
                            Thread th = new Thread(() -> {
                                try {
                                    List<Function> functions = getDbService().getFunctions(selectedDatabase);
                                    treeItem.getValue().setFunctions(functions);
                                    DatabaseCache.getInstance().updateFunctionsCompletionData(getConnectionParams().cacheKey(), selectedDatabase);
                                    RsSyntaxCompletionHelper.getInstance().updateCompletionsListForFunctions(getConnectionParams(), selectedDatabase);
                                    Platform.runLater(() -> {
                                        loadFunctions(treeItem0, functions);
                                        makeBusy(false);
                                    });
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            });
                            th.setDaemon(true);
                            th.start();
                        }
                    });
                    break;

                case TRIGGERS:
                    treeItem0.getInternalChildren().add(new TreeItem());

                    treeItem0.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null && newValue && selectedDatabase != null) {
                            makeBusy(true, 15000, "Collecting Objects details from Metadata. It is a one-time job. Usually, it is a little bit slow if there are many tables.");
                            Thread th = new Thread(() -> {
                                try {
                                    List<Trigger> triggers = getDbService().getTriggers(selectedDatabase);
                                    treeItem.getValue().setTriggers(triggers);
                                    DatabaseCache.getInstance().updateTriggersCompletionData(getConnectionParams().cacheKey(), selectedDatabase);
                                    RsSyntaxCompletionHelper.getInstance().updateCompletionsListForTriggers(getConnectionParams(), selectedDatabase);
                                    Platform.runLater(() -> {
                                        loadTriggers(treeItem0, triggers);
                                        makeBusy(false);
                                    });
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            });
                            th.setDaemon(true);
                            th.start();
                        }
                    });
                    break;

                case STORED_PROCS:
                    treeItem0.getInternalChildren().add(new TreeItem());

                    treeItem0.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null && newValue && selectedDatabase != null) {
                            makeBusy(true, 15000, "Collecting Objects details from Metadata. It is a one-time job. Usually, it is a little bit slow if there are many tables.");
                            Thread th = new Thread(() -> {
                                try {
                                    List<StoredProc> storedProcedures = getDbService().getStoredProcs(selectedDatabase);
                                    treeItem.getValue().setStoredProc(storedProcedures);
                                    DatabaseCache.getInstance().updateProceduresCompletionData(getConnectionParams().cacheKey(), selectedDatabase);
                                    RsSyntaxCompletionHelper.getInstance().updateCompletionsListForProcedures(getConnectionParams(), selectedDatabase);
                                    Platform.runLater(() -> {
                                        loadStoredProcs(treeItem0, storedProcedures);
                                        makeBusy(false);
                                    });
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            });
                            th.setDaemon(true);
                            th.start();
                        }
                    });
                    break;

                case EVENTS:
                    treeItem0.getInternalChildren().add(new TreeItem());
                    
                    treeItem0.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null && newValue && selectedDatabase != null) {
                            makeBusy(true, 15000, "Collecting Objects details from Metadata. It is a one-time job. Usually, it is a little bit slow if there are many tables.");
                            Thread th = new Thread(() -> {
                                try {
                                    List<Event> events = getDbService().getEvents(selectedDatabase);
                                    treeItem.getValue().setEvents(events);

                                    DatabaseCache.getInstance().updateEventsCompletionData(getConnectionParams().cacheKey(), selectedDatabase);
//                                                RsSyntaxCompletionHelper.getInstance().rebuildTags(getConnectionParams(), controller);

                                    Platform.runLater(() -> {
                                        loadEvents(treeItem0, events);
                                        makeBusy(false);
                                    });
                                } catch (SQLException ex) {
                                    log.error("Error", ex);
                                }
                            });
                            th.setDaemon(true);
                            th.start();
                        }
                    });
                    break;
            }
        }

        // if db not in base dbs need add SchemaDiagramm folder
        if (db.getName() != null && !BASE_DBS.contains(db.getName().toLowerCase())) {
            treeItem.getChildren().add(new ErrDiagramTreeItem(db));
        }

//                    if (needLoad) {
        loadDatabaseChildrenType(treeItem);
        
        return treeItem;
    }
    
    
    /**
     * Process all the expand action on the tree item.
     * <p>
     * @param item this is the enum valued treenode
     * @param children
     */
    private void loadChildrenTree(FilterTreeItem item, DatabaseChildrenType children, TreeItem<Database> dbItem) {
        
        switch (children) {
            case TABLES: 
                item.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(tablesImage), 18, 18));
                List<DBTable> tables = dbItem.getValue().getTables();                                
                loadTables(item, tables);
                break;            
            
            case VIEWS:
                item.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(viewsImage), 18, 18));
                List<View> views = dbItem.getValue().getViews();
                if (views != null && !views.isEmpty()) {
                    loadViews(item, views);
                }
                break;
                
            case FUNCTIONS:
                item.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(functionsImage), 18, 18));
                List<Function> functions = dbItem.getValue().getFunctions();
                if (functions != null && !functions.isEmpty()) {
                    loadFunctions(item, functions);
                }
                break;
                
            case TRIGGERS:
                item.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(triggersImage), 18, 18));
                List<Trigger> triggers = dbItem.getValue().getTriggers();
                if (triggers != null && !triggers.isEmpty()) {
                    loadTriggers(item, triggers);
                }
                break;

            case STORED_PROCS:
                item.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(proceduresImage), 18, 18));
                List<StoredProc> storedProcedures = dbItem.getValue().getStoredProc();
                if (storedProcedures != null && !storedProcedures.isEmpty()) {
                    loadStoredProcs(item, storedProcedures);
                }
                break;
                
            case EVENTS:
                item.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(eventsImage), 18, 18));
                List<Event> events = dbItem.getValue().getEvents();
                if (events != null && !events.isEmpty()) {
                    loadEvents(item, events);
                }
                break;

        }
    }


    /**
     * Loads the Table listing
     * <p>
     * @param item parent node where the table tree node must be added
     * @param tables
     */
    private void loadTables(FilterTreeItem item, List<DBTable> tables) {
        item.getInternalChildren().clear();
        if (tables != null) {
            for (DBTable table : tables) {
                createTable(item, table, getDbService());
            }
        }
    }
    
    public void createTable(FilterTreeItem tablesItem, DBTable table, DBService service) {
        FilterTreeItem tableItem = new FilterTreeItem(table);

        if (tableImage == null) {
            tableImage = SmartImageProvider.getInstance().getTableTab_image();
        }
        tableItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(tableImage), 18, 18));

        FilterTreeItem treeItem = new FilterTreeItem("Columns");
        treeItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(folderImage), 16, 16));
        tableItem.getInternalChildren().add(treeItem);
        tableItem.setExpanded(false);

        loadColumns(treeItem, new ArrayList(table.getColumns()));


        FilterTreeItem indexesTreeItem = new FilterTreeItem("Indexes");
        indexesTreeItem.getInternalChildren().add(new AnimatableTreeItem());
        indexesTreeItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(folderImage), 16, 16));
        tableItem.getInternalChildren().add(indexesTreeItem);

        indexesTreeItem.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                try {
                    service.setIndexes(table);
                } catch (SQLException ex) {}

                loadIndexes(indexesTreeItem, table.getIndexes(), table.getForeignKeys());
            }
        });

        indexesTreeItem.getInternalChildren().add(new FilterTreeItem());

        tablesItem.getInternalChildren().add(tableItem);
    }

    private static void loadColumns(FilterTreeItem item, List<Column> columns) {
        item.getInternalChildren().clear();
        for (Column column : columns) {
            TreeItem<Column> columnItem = new AnimatableTreeItem<>(column);
            
            if (columnImage == null) {
                columnImage = SmartImageProvider.getInstance().getColumnTab_image();
            }
            if (column.isPrimaryKey()) {
                if (primaryKeyImage == null) {
                    primaryKeyImage = SmartImageProvider.getInstance().getPrimaryKey_image();
                }
                columnItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(primaryKeyImage), 16, 16));
            } else {
                columnItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(columnImage), 16, 16));
            }

            //System.out.println("table name : " + columnItem.getValue().toString());

            item.getInternalChildren().add(columnItem);
        }
    }
    
    private static void loadIndexes(FilterTreeItem item, List<Index> indexes, List<ForeignKey> foreignKeys) {
        item.getInternalChildren().clear();
        
        if (indexes != null) {
            for (Index index : indexes) {
                TreeItem<Index> indexItem = new AnimatableTreeItem<>(index);

                if (indexImage == null) {
                    indexImage = SmartImageProvider.getInstance().getIndexTab_image();
                }
                if (index.getType() == IndexType.PRIMARY) {
                    if (primaryKeyImage == null) {
                        primaryKeyImage = SmartImageProvider.getInstance().getPrimaryKey_image();
                    }
                    indexItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(primaryKeyImage), 16, 16));
                } else {
                    indexItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(indexImage), 16, 16));
                }

                item.getInternalChildren().add(indexItem);
            }
        }
        
        if (foreignKeys != null) {
            for (ForeignKey fk : foreignKeys) {
                TreeItem<ForeignKey> fkItem = new AnimatableTreeItem<>(fk);

                if (indexImage == null) {
                    indexImage = SmartImageProvider.getInstance().getIndexTab_image();
                }
                
                fkItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(indexImage), 16, 16));
                item.getInternalChildren().add(fkItem);
            }
        }
    }

    private void loadViews(FilterTreeItem item, List<View> views) {
        item.getInternalChildren().clear();
        for (View view : views) {
            TreeItem<View> viewItem = new AnimatableTreeItem<>(view);

            if (viewImage == null) {
                viewImage = SmartImageProvider.getInstance().getViewTab_image();
            }
            viewItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(viewImage), 16, 16));
//            System.out.println("view name : " + viewItem.getValue().toString());

            item.getInternalChildren().add(viewItem);
        }
    }

    private void loadStoredProcs(FilterTreeItem item, List<StoredProc> storedProcs) {
        item.getInternalChildren().clear();
        for (StoredProc storedProc : storedProcs) {
            TreeItem<StoredProc> storedProcsItem = new AnimatableTreeItem<>(storedProc);

            if (storedProcImage == null) {
                storedProcImage = SmartImageProvider.getInstance().getStoredProcTab_image();
            }
            storedProcsItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(storedProcImage), 16, 16));
//            System.out.println("stored procedure name : " + storedProcsItem.getValue().toString());

            item.getInternalChildren().add(storedProcsItem);
        }
    }

    private void loadEvents(FilterTreeItem item, List<Event> events) {
        item.getInternalChildren().clear();
        for (Event event : events) {
            TreeItem<Event> eventItem = new AnimatableTreeItem<>(event);

            if (eventImage == null) {
                eventImage = SmartImageProvider.getInstance().getEventTab_image();
            }
            eventItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(eventImage), 16, 16));
            item.getInternalChildren().add(eventItem);
        }
    }

    private void loadFunctions(FilterTreeItem item, List<Function> functions) {
        item.getInternalChildren().clear();
        for (Function function : functions) {
            TreeItem<Function> functionItem = new AnimatableTreeItem<>(function);

            if (functionImage == null) {
                functionImage = SmartImageProvider.getInstance().getFunctionTab_image();
            }
            functionItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(functionImage), 16, 16));
//            System.out.println("function name : " + functionItem.getValue().toString());

            item.getInternalChildren().add(functionItem);
        }
    }

    private void loadTriggers(FilterTreeItem item, List<Trigger> triggers) {
        item.getInternalChildren().clear();
        for (Trigger trigger : triggers) {
            TreeItem<Trigger> triggerItem = new AnimatableTreeItem<>(trigger);

            if (triggerImage == null) {
                triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
            }
            triggerItem.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(triggerImage), 16, 16));
//            System.out.println("trigger name : " + triggerItem.getValue().toString());

            item.getInternalChildren().add(triggerItem);
        }
    }

    /**
     * Adds Tables,Triggers,Function,StoredProc,Events section on Database
     * Treeitem.
     * <p>
     * @param dbTreeItem
     */
    private void loadDatabaseChildrenType(TreeItem dbTreeItem) {
        
        for (Object treeItem: dbTreeItem.getChildren()) {
            if (treeItem instanceof FilterTreeItem && ((FilterTreeItem)treeItem).getValue() instanceof DatabaseChildrenType) {
                loadChildrenTree((FilterTreeItem)treeItem, (DatabaseChildrenType) ((FilterTreeItem)treeItem).getValue(), dbTreeItem);
            } else if (!(treeItem instanceof ErrDiagramTreeItem)) {
                log.error("Found some ISSUE with load database children type!!!");
            }
        }
    }

    @Override
    public void focusLeftTree() {
        Platform.runLater(() -> treeView.requestFocus());
    }
    
    

    /**
     * This function helps to open new connection and populate databases
     * initially.
     */
    @Override
    public void openConnection(ConnectionParams params) throws SQLException {
        
        log.debug("START openConnection");
        
        params.setMultiQueries(true);
        
        currentRoot = new FilterTreeItem();
        currentRoot.setExpanded(false);
        currentRoot.setValue(getConnectionParams());
        
        if (serverImage == null) {
            serverImage = SmartImageProvider.getInstance().getServer_image();
            serverFilterImage = SmartImageProvider.getInstance().getServerFilter_image();
        }
        
        currentRoot.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(serverImage), 18, 18));
        SimpleObjectProperty<Object> bindors = new SimpleObjectProperty<>();
        bindors.addListener(new ChangeListener<Object>() {
            
            boolean shouldSave = false;
            
            @Override
            public void changed(ObservableValue<? extends Object> observable,
                    Object oldValue, Object newValue) {

                Object[] res = (Object[]) newValue;
                if (res[3] == null) {
                    return;
                }

                if ((boolean) res[3]) {
                    getConnectionParams().setForegroundColor(null);
                    getConnectionParams().setBackgroundColor(null);
                    return;
                }
                
                String sFore = "#" + res[0].toString().substring(2),
                        sBack = "#" + res[1].toString().substring(2);
                
                String background = getBackgroundColor(getConnectionParams());
                String foreground = getForegroundColor(getConnectionParams());
                
                if (foreground == null || !foreground.equals(sFore)) {
                    getConnectionParams().setForegroundColor(sFore);
                }

                if (background == null || !background.equals(sBack)) {
                    getConnectionParams().setBackgroundColor(sBack);
                }
                
            }
        });
        
        String fore = getForegroundColor(getConnectionParams());
        String back = getBackgroundColor(getConnectionParams());
                
        Object[] p;
        if (fore != null && back != null) {
            p = new Object[]{Color.web(fore), Color.web(back), "", false};
        } else {
            //true here means default. since everybody is null,true is set to indicate
            //colors should be default.
            p = new Object[]{null, null, "", true};
        }
        
        bindors.setValue(p);
        Platform.runLater(() -> {
            treeView.setRoot(currentRoot);
            ContextMenu contextMenu = new ContextMenu();
            treeView.setContextMenu(contextMenu);
            treeView.setCellFactory(new Callback<TreeView, TreeCell>() {
                @Override
                public TreeCell call(TreeView p) {
                    
                    final TextFieldTreeCellImpl impl = 
                            new TextFieldTreeCellImpl(ovcl, getHooks(), contextMenu) {
                        @Override
                        public void updateItem(Object item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            Font font = getFont();
                            if (currentFont != null) {
                                font = currentFont;
                            }
                            try {
                                if (item != null && item.equals(selectedDatabase)) {
                                    setFont(Font.font(font.getFamily(), FontWeight.BOLD, font.getSize()));
                                } else {
                                    setFont(font);
                                }
                            } catch (ClassCastException cce) {
                                cce.printStackTrace();
                            }
                        }  
                    };                    
                    impl.disclosureNodeProperty().addListener((ObservableValue<? extends Node> ov, Node t, Node t1) -> {
                        if (t != null) {
                            disClosureButtons.remove(t1);
                        }
                        if (t1 != null) {
                            disClosureButtons.add(t1);
                        }
                    });            
                    
                    
                    impl.setOnDragDetected((MouseEvent event) -> {
                        
                        Object obj = impl.getTreeView().getSelectionModel().getSelectedItem().getValue();
                        if (obj instanceof DBTable || obj instanceof Column || obj instanceof View || obj instanceof StoredProc || obj instanceof Function) {
                            Dragboard dragBoard = impl.startDragAndDrop(TransferMode.MOVE);
                            ClipboardContent content = new ClipboardContent();
                            
                            // NOTICE!!! Text must be like 'type|name|query'
                            String name = obj.toString();
                            if (obj instanceof DBTable) {                                
                                String database = ((DBTable)obj).getDatabase().getName();                                
                                content.put(DataFormat.PLAIN_TEXT, String.format(DRAG_CONTENT__TABLE_OR_VIEW, name, database, name, name, name)); 
                                
                            } else if (obj instanceof View) {                                
                                String database = ((View)obj).getDatabase().getName();
                                content.put(DataFormat.PLAIN_TEXT, String.format(DRAG_CONTENT__TABLE_OR_VIEW, name, database, name, name, name)); 
                                
                            }else if (obj instanceof Column) {                                
                                String database = ((Column)obj).getTable().getDatabase().getName();                       
                                String table = ((Column)obj).getTable().getName();
                                name = ((Column)obj).getName();
                                content.put(DataFormat.PLAIN_TEXT, String.format(DRAG_CONTENT__COLUMN, name, table, database, name, table)); 
                                
                            } else {
                                content.put(DataFormat.PLAIN_TEXT, String.format(DRAG_CONTENT__FUNC_OR_PROC, name, name)); 
                            }
                            
                            
                            Label label = new Label(name, impl.getTreeView().getSelectionModel().getSelectedItem().getGraphic());
                            new Scene(label);
                            
                            dragBoard.setDragView(label.snapshot(null, null));
                            
                            dragBoard.setContent(content);
                        }
                        event.consume();
                    });
                    
                    impl.setOnDragOver((DragEvent event) -> {
                        Dragboard db = event.getDragboard();
                        if (db.hasString()) {
                            event.acceptTransferModes(TransferMode.MOVE);
                        }
                        event.consume(); 
                    });
                    
                    return impl;
                }
            });   
            
            EventHandler<MouseEvent> eventStopper = new EventHandler<MouseEvent>() {
                
                @Override
                public void handle(MouseEvent t) {
                    EventTarget et = t.getTarget();
                    if (disClosureButtons.contains(((Node) et).getParent())) {
                        if(t.getEventType() == MouseEvent.MOUSE_CLICKED){
                            TreeCell ic = (TreeCell)((Node) et).getParent().getParent();
                            TreeItem ti = ic.getTreeItem();
                            if(ti != null){
                                if (ti.isExpanded()) {
                                    ti.setExpanded(false);
                                }else{
                                    ti.setExpanded(true);
                                }                               
                            }                            
                        }
                        t.consume();
                    }else if (t.getEventType() == MouseEvent.MOUSE_CLICKED
                            && t.getClickCount() > 1) {
                        TreeItem ti = null;
                        if (et instanceof TreeCell) {
                            ti = ((TreeCell) et).getTreeItem();
                        } else if (et instanceof LabeledText) {
                            ti = ((TreeCell) ((LabeledText) et).getParent()).getTreeItem();
                        }
                        if (ti != null) {
                            ti.setExpanded(false);
                        }
                    }                   
                }
            };
            
            treeView.addEventFilter(MouseEvent.MOUSE_PRESSED, eventStopper);
            treeView.addEventFilter(MouseEvent.MOUSE_PRESSED, eventStopper);
            treeView.addEventFilter(MouseEvent.MOUSE_CLICKED, eventStopper);            
            treeView.setUserData(bindors);
        });
        
        getDbService().connect(getConnectionParams());
        
        log.debug("loading root children");
        addRootChildren(false, getConnectionParams().getListOfLastUsedDatabases());
        log.debug("loaded root children");
        
        DatabaseCache.getInstance().updateCompletionData(getConnectionParams().cacheKey(),
                getConnectionParams().getDatabase(), (AbstractDBService) getDbService(), null);
        
        Platform.runLater(()-> currentRoot.setExpanded(true));
    }
    
    @Override
    public Object getTreeViewUserData() {
        Object o = treeView.getUserData();
        if (o == null) {
            return new Object();
        }
        return o;
    }
    
    public TreeView getLeftTreeView(){
        return treeView;
    }

    @Override
    public void addTab(Tab t) {
        centertabPane.getTabs().add(centertabPane.getTabs().size() - 1, t);
    }

    public void prevTab() {
        if (centertabPane.getTabs().size() > 1) {
            int current = centertabPane.getSelectionModel().getSelectedIndex();
            centertabPane.getSelectionModel().select(current > 0 ? current - 1 : 0);
        }
    }

    public void nextTab() {
        if (centertabPane.getTabs().size() > 1) {
            int current = centertabPane.getSelectionModel().getSelectedIndex();
            int size = centertabPane.getTabs().size();
            centertabPane.getSelectionModel().select(current < size - 2 ? current + 1 : size - 2);
        }
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
        return centertabPane.getSelectionModel().selectedItemProperty();
    }

    @Override
    public Tab getTab(String id, String text) {
        for (Tab t: centertabPane.getTabs()) {
            if (id.equals(t.getId()) && text.equals(t.getText())) {
                return t;
            }
        }
        
        return null;
    }
    
    
    @Override
    public void showTab(Tab t) {
        
        if (centertabPane.getTabs().contains(t)) {
            centertabPane.getSelectionModel().select(t);
            
        } else if (bottomtabPane.getTabs().contains(t)) {
            bottomtabPane.getSelectionModel().select(t);
        }
    }
    
    public void setSelectedBottomTabId(String selectedBottomTabId) {
        this.selectedBottomTabId = selectedBottomTabId;
        for (Tab t: bottomtabPane.getTabs()) {
            if (selectedBottomTabId.equals(t.getId())) {
                bottomtabPane.getSelectionModel().select(t);
                break;
            }
        }
    }

    @Override
    public void addTabToBottomPane(Tab... tab) {
        bottomtabPane.getTabs().addAll(tab);
        if (selectedBottomTabId != null) {
            for (Tab t: tab) {
                if (selectedBottomTabId.equals(t.getId())) {
                    bottomtabPane.getSelectionModel().select(t);
                    break;
                }
            }
        }
    }
        
    @Override
    public void removeTabFromBottomPane(Tab... tab) {
        bottomtabPane.getTabs().removeAll(tab);
    }

    @Override
    public List<Tab> getBottamTabs() {
        return bottomtabPane.getTabs();
    }

    
    private double oldBottomWidth = 0.0;
    public void setBottomPaneVisibility(boolean bool) {
        //bottomtabPane.setVisible(bool);
        boolean contains = subSplitter.getItems().contains(bottomPane);
        if (bool) {
            if (!contains) {
                
                subSplitter.getItems().add(bottomPane);
                bottomtabPane.prefWidth(oldBottomWidth > 0 ? oldBottomWidth : centerPane.getWidth());
            }
        } else {
            // TODO Need check this comments
//            if (contains) {
                oldBottomWidth = bottomPane.getWidth();
                subSplitter.getItems().remove(bottomPane);
//            }
        }
    }
    
    public void setContentPaneVisibility(boolean bool){
        boolean contains = subSplitter.getItems().contains(centerPane);
        if (bool) {
            if (!contains) {
                subSplitter.getItems().add(0, centerPane);
            }
        } else {
            if (contains) {
                subSplitter.getItems().remove(centerPane);
            }
        }
    }

    private void initTabPane() {
        logger.addListener(this);
        bottomtabPane = new TabPane();
        bottomtabPane.prefWidth(centerPane.getWidth());
        bottomtabPane.setTabMaxWidth(200);
        bottomPane.getChildren().add(bottomtabPane);
        centertabPane = new TabPane();
        centertabPane.setId("top-function-tab-pane");
        bottomtabPane.setId("bottom-function-tab-pane");
        
        bottomtabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            selectedTabs.put(centertabPane.getSelectionModel().getSelectedItem(), newValue);
        });
        
        
        Tab addTab = new Tab("");
        addTab.setClosable(false);
        addTab.setId("newTabs");
        centertabPane.getTabs().add(addTab);
        
        centertabPane.prefWidth(centerPane.getWidth());
        centerPane.getChildren().add(centertabPane);
        np.com.ngopal.fx.utils.LayoutUtils.layoutAnchorPane(centertabPane, 0, 0, 0, 0);
        np.com.ngopal.fx.utils.LayoutUtils.layoutAnchorPane(bottomtabPane, 0, 0, 0, 0);
        
        /**
         * So we can be able to detect if it is a query Tab - in turn needs the bottomPane
         * or any other Tab, which doesn't need the bottomPane eg; the History tab
         * 
         */
        centertabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
                
            private Label queriesLabel = (Label)((MainController)getController()).getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_QUERIES); 

            private void changeMenuItemsSizes(List<MenuItem> items) {
                if (items != null) {
                    // change image size to standart sizes
                    for (MenuItem mi : items) {
                        ImageView iv = (ImageView) mi.getGraphic();
                        if (iv != null) {
                            iv.setFitHeight(18);
                            iv.setFitWidth(18);
                        }

                        if (mi instanceof Menu) {
                            changeMenuItemsSizes(((Menu) mi).getItems());
                        }
                    }
                }
            }
                
            Node tabNode = null;
            {
                ContextMenu cm = new ContextMenu();
                QueryTabModule queryTabModule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
                MenuItem history = new MenuItem("History",
                        FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getHistory_image()), 16, 16));
                history.setOnAction((ActionEvent t) -> {
                    ((MainController)getController()).reachHistoryTab();
                });

                MenuItem feq = new MenuItem("Frequency Executed Queries",
                        FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getFeg_image()), 16, 16));
                feq.setOnAction((ActionEvent t) -> {
                    ((MainController)getController()).reachFeqTab();
                });
                cm.getItems().addAll(queryTabModule.getQueryEditor(), queryTabModule.getQueryBuilder(), queryTabModule.getSchemaDesigner(), queryTabModule.getQueryAnalyzerMenuItem(),
                        new SeparatorMenuItem(), history, feq);
                changeMenuItemsSizes(cm.getItems());
                addTab.setContextMenu(cm);
            }

            @Override
            public void changed(ObservableValue<? extends Tab> ov, Tab t, Tab t1) {

                QueryTabModule queryTabModule = ((QueryTabModule)getController().getModuleByClassName(QueryTabModule.class.getName()));
                queryTabModule.hideFindReplaceDialog();
                
                WorkProc<Tab[]> itsBackground = new WorkProc<Tab[]>() {
                    ArrayList<Tab> itemsToRemove = new ArrayList();
                    ArrayList<Tab> tabContentHolderForBottomTabs = new ArrayList();

                    @Override
                    public void updateUI() {
                        if (getAddData()[0] != null) {
                            tabContentHolderForBottomTabs = (ArrayList<Tab>) getAddData()[0].getUserData();
                            if (tabContentHolderForBottomTabs != null && !tabContentHolderForBottomTabs.isEmpty()) {
                                tabContentHolderForBottomTabs.get(0).getTabPane().getTabs()
                                        .removeAll(tabContentHolderForBottomTabs);
                            }
                        }

                        if (getAddData()[1] != null) {
                            tabContentHolderForBottomTabs = (ArrayList<Tab>) getAddData()[1].getUserData();
                            if (tabContentHolderForBottomTabs != null && !tabContentHolderForBottomTabs.isEmpty()) {
                                addTabToBottomPane(
                                        tabContentHolderForBottomTabs.toArray(new Tab[tabContentHolderForBottomTabs.size()]));
                            }

                            Platform.runLater(()-> setBottomPaneVisibility(true) ); //to solve bug comming from tab that using workproc alot
                        } else {
                            setBottomPaneVisibility(false);
                        }
                        
                        bottomtabPane.getSelectionModel().select(selectedTabs.get(t1));

                        ((MainController)getController()).centerTabForQueryTabBooleanProperty().invalidate();
                        ((MainController)getController()).centerTabForOnlyQueryTabBooleanProperty().invalidate();
                        
                        getAddData()[0] = null;
                        getAddData()[1] = null;
                        
                        if (queriesLabel.textProperty().isBound()) {
                            queriesLabel.textProperty().unbind();
                        }
                        
                        SimpleStringProperty prop = queryTabModule.sessionLastRunnedQueryCountProperty(t1);
                        if (prop != null) {
                            queriesLabel.textProperty().bind(prop);
                        }
                    }

                    @Override
                    public void run() {
                        ((MainController)getController()).centerTabForQueryTabBooleanProperty().invalidate();
                        ((MainController)getController()).centerTabForOnlyQueryTabBooleanProperty().invalidate();
                        String o;
                        if (getAddData()[0] != null) {
                            o = getAddData()[0].getId();
                            if (o != null && ( (o instanceof String) && (((String)o).endsWith("Tab")))) {
                                tabContentHolderForBottomTabs = (ArrayList<Tab>) getAddData()[0].getUserData();
                                if (tabContentHolderForBottomTabs != null) {
                                    for (Tab t : tabContentHolderForBottomTabs) {
                                        if (t.getTabPane() == null) {//it has been closed                                
                                            itemsToRemove.add(t);
                                        }
                                    }
                                    tabContentHolderForBottomTabs.removeAll(itemsToRemove);
                                }
                                itemsToRemove.clear();
                            } else {
                                getAddData()[0] = null;
                            }
                        }
                        if (getAddData()[1] != null) {
                            o = getAddData()[1].getId();
                            // Issue from Sudheer:  DB objects alter/create view, SP, function, event and triggers are not showing bottom grid 
                            //  || (!((String)o).endsWith(QueryTabModule.TAB_USER_DATA__QUERY_TAB))
                            if ( !(o instanceof String) || ((String)o).endsWith(QueryTabModule.TAB_USER_DATA__TABLE_TAB)) {
                                getAddData()[1] = null;
                            }
                        }
                        callUpdate = true;

                    }
                };
                
                
                // check click on add tab
                if (addTab == t1) {
                    Platform.runLater(() -> {
                        if (addTab.getContextMenu().isShowing()) {
                            addTab.getContextMenu().hide();
                        }
                        if (tabNode == null) {
                            tabNode = centertabPane.lookup("#newTabs");
                        }
                         Point2D point = Utils.pointRelativeTo(tabNode,
                                 tabNode.getBoundsInLocal().getWidth(),
                                 tabNode.getBoundsInLocal().getHeight(),
                                 HPos.RIGHT, VPos.BOTTOM, tabNode.getBoundsInLocal().getMinX(),
                                 tabNode.getBoundsInLocal().getMinY(), true);
                         
                        addTab.getContextMenu().show(tabNode, point.getX(), point.getY());                                
                    });
                    centertabPane.getSelectionModel().select(t);
                    return;
                }

                ovcl.setValue(null);
                itsBackground.setAdditionalData(new Tab[] {t, t1});
                Recycler.addWorkProc(itsBackground);                        
            }
        });
        
        bottomtabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            
            private Label rowsLabel = (Label)((MainController)getController()).getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_ROWS); 
            
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                
                if (newValue != null && newValue.getUserData() instanceof ResultTabController) {
                    
                    if (rowsLabel.textProperty().isBound()) {
                        rowsLabel.textProperty().unbind();
                    }
                    
                    rowsLabel.textProperty().bind(((ResultTabController)newValue.getUserData()).totalRowsProperty());
                } else {
                    if (!rowsLabel.textProperty().isBound()) {
                        rowsLabel.setText("0 row(s)");
                    }
                }
            }
        });
    }

    public TreeItem getTableTreeItem(DBTable table) {
        
        for (TreeItem database: (List<TreeItem>)currentRoot.getChildren()) {
            Database db = (Database) database.getValue();
            if (db.getName().equals(table.getDatabase().getName()) && database.getChildren() != null) {
                
                for (TreeItem node: (List<TreeItem>)database.getChildren()) {
                    if (node.getValue() == DatabaseChildrenType.TABLES) {

                        for (TreeItem t: (List<TreeItem>)node.getChildren()) {
                            DBTable t0 = (DBTable)t.getValue();
                            if (t0.getName().equals(table.getName())) {
                                return t;
                            }
                        }
                    }
                }
            }
        }
        
        return null;
    }
    
    @Getter
    @AllArgsConstructor
    public enum DatabaseChildrenType implements Displayable {

        TABLES("Tables"), VIEWS("Views"), FUNCTIONS("Functions"), TRIGGERS("Triggers"), STORED_PROCS("Stored Procs"), EVENTS("Events");

        private String displayValue;

    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem() {
        return treeView.getSelectionModel().selectedItemProperty();
    }
    
    private void processNodeDBClick(TreeItem item) {
        if (item != null) {
            QueryTabModule queryTabModule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
            if (queryTabModule != null)
            {
                 ReadOnlyObjectProperty<Tab> selectedTab = (ReadOnlyObjectProperty<Tab>) 
                         getController().getSelectedConnectionTab(getController().getSelectedConnectionSession().get());
                // if opened query tab append in it
                if (selectedTab.get() != null && QueryTabModule.TAB_USER_DATA__QUERY_TAB.equals(selectedTab.get().getId())) {

                    Object value = item.getValue();
                    if (value instanceof Database) {
                        queryTabModule.appendToTab(selectedTab.get(), ((Database)value).getName() + " ");
                    } else if (value instanceof DBTable) {
                        DBTable tableau = (DBTable) value;
                        queryTabModule.appendToTab(selectedTab.get(), tableau.getDatabase().getName() + "." + tableau.getName() + " ");
                    } else if (value instanceof View) {
                        View view = (View) value;
                         queryTabModule.appendToTab(selectedTab.get(), view.getDatabase().getName() + "." + view.getName() + " ");
                    }else if(value instanceof Column){
                        queryTabModule.appendToTab(selectedTab.get(),((Column)value).getName() +"");
                    } 
                }
            }
        }
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return true;
    }

    @Override
    public String getTabName() {
        return getConnectionParams().getName();
    }

    public List<String> getKeyWords() {
        List<String> keywords = new ArrayList();
        for (Database database : getConnectionParams().getDatabases()) {
            System.out.println("database namess :" + database.getName());
            keywords.add(database.getName());
            for (DBTable table : database.getTables()) {
                keywords.add(table.getName());
            }
        }
        return keywords;
    }

    public void setKeyWordsToFile() {

        File file = new File(TabContentController.class.getResource("/np/com/ngopal/smart/sql/ui/images/d.txt").toExternalForm());
        String content = "";
        for (String keyword : getKeyWords()) {
            content += keyword + "\n";
        }

        try (FileOutputStream fop = new FileOutputStream(file)) {

            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
            DialogHelper.showError(getController(), "Error", "Error seting keywords to file", e);
        }
    }

    private void changeDatabase(String db) {
        // Save only if database was changed
        // Need check this code
        if (db != null) {
            if (selectedDatabase == null || /*db == null ||*/ getConnectionParams().getSelectedDatabase() == null
                    || !db.equals(getConnectionParams().getSelectedDatabase().getName())) {

                Recycler.addWorkProc(new WorkProc() {

                    boolean databaseLoaded = false;
                    SQLException exception;

                    @Override
                    public void updateUI() {
                        if (exception != null) {
                            DialogHelper.showError(getController(), "Error", "Error changing database", exception);
                        } else {
                            TreeItem databaseTreeItem = selectedDatabase != null ? retrieve(selectedDatabase) : null;
                            if (databaseTreeItem != null) {
                                ((MainController) getController()).notifyAllModulesOnTreeItem(databaseTreeItem);
                            }

                            if (databaseLoaded) {
//                                TreeItem ti = selectedItem;
//                                while (ti != null && !(ti.getValue() instanceof Database)) {
//                                    ti = ti.getParent();
//                                }

                                if (databaseTreeItem != null) {
                                    databaseTreeItem.setValue(selectedDatabase);
                                    loadDatabaseChildrenType(databaseTreeItem);
                                    QueryTabModule queryTabModule = (QueryTabModule) ((MainController) getController())
                                            .getModuleByClassName(QueryTabModule.class.getName());
                                    Collection<QueryAreaWrapper> list = queryTabModule.getAllCodeAreas();
                                    RsSyntaxCompletionHelper.getInstance().rebuildTags(getConnectionParams(), list);
                                    treeView.getSelectionModel().clearSelection();
                                    treeView.getSelectionModel().select(databaseTreeItem);
                                }
                            }
                        }

                        makeBusy(false);
                    }

                    @Override
                    public void run() {
                        try {
                            if (getDbService() instanceof MysqlDBService) {
                                databaseLoaded = applyMysqlChangeDatabaseMechanics(db);
                            } else if (getDbService() instanceof PostgresDBService) {
                                databaseLoaded = applyPostgresqlChangeDatabaseMechanics(db);
                            }
                            
                            ((AbstractDBService) getDbService()).changeDatabase(selectedDatabase);
                            getConnectionParams().addUsedDatabase(db);
                            service.save(getConnectionParams());

                        } catch (SQLException ex) {
                            exception = ex;
                        }
                        callUpdate = true;
                    }
                });
            }
            log.debug("Changed Db:: {}", selectedDatabase);
        }
    }
    
    private boolean applyMysqlChangeDatabaseMechanics(String db) throws SQLException {
        // check if database alredy from cache or need load it and caching
        boolean databaseLoaded = false;
        Database database = DatabaseCache.getInstance().getDatabase(getConnectionParams().cacheKey(), db);
        selectedDatabase = getConnectionParams().selectDatabase(db);

        if (database != selectedDatabase) {
            databaseLoaded = true;

            if (database == null) {
                database = DatabaseCache.getInstance().syncDatabase(db, getConnectionParams().cacheKey(), (AbstractDBService) getDbService());
            }

            if (getConnectionParams().getDatabases() == null) {
                getConnectionParams().setDatabases(new ArrayList<>());
            }

            int index = getConnectionParams().getDatabases().indexOf(selectedDatabase);
            getConnectionParams().getDatabases().set(index, database);

            index = getConnectionParams().getDatabases().indexOf(selectedDatabase);
            getConnectionParams().getDatabases().set(index, database);

            selectedDatabase = database;
        }

        DatabaseCache.getInstance().updateCompletionData(getConnectionParams().cacheKey(),
                getConnectionParams().getDatabase(), (AbstractDBService) getDbService(), null);

        return databaseLoaded;
    }

    private boolean applyPostgresqlChangeDatabaseMechanics(String db) throws SQLException {
        boolean databaseLoaded = false;

        PgDatabase database = PgDatabasesCache.getInstance().getDatabase(getConnectionParams().cacheKey(), db);
        selectedDatabase = getConnectionParams().selectDatabase(db);

        if (database != selectedDatabase) {
            databaseLoaded = true;

            if (database == null) {
                database = PgDatabasesCache.getInstance().syncDatabase(db, getConnectionParams().cacheKey(), (PostgresDBService) getDbService());
            }

            if (getConnectionParams().getDatabases() == null) {
                getConnectionParams().setDatabases(new ArrayList<>());
            }

            int index = getConnectionParams().getDatabases().indexOf(selectedDatabase);
            getConnectionParams().getDatabases().set(index, database);

            index = getConnectionParams().getDatabases().indexOf(selectedDatabase);
            if(index > -1)
                getConnectionParams().getDatabases().set(index, database);

            selectedDatabase = database;
        }

        return databaseLoaded;
    }

    private TreeItem retrieve(Database t1) {
        if (treeView != null && treeView.getRoot() != null && 
                ( treeView.getRoot() instanceof FilterTreeItem && ((FilterTreeItem)treeView.getRoot()).getInternalChildren() != null)) {
            for (TreeItem ti : (List<TreeItem>)((FilterTreeItem)treeView.getRoot()).getInternalChildren()) {
                if (ti.getValue() instanceof Database && ((Database) ti.getValue()).getName().equals(t1.getName())) {
                    return ti;
                }
            }
        }
        return null;
    }

    @Override
    public void listen(Connection connection, String logMsg, boolean executedByUser) {
        if (executedByUser && logMsg != null && logMsg.toLowerCase().startsWith("use")) {
            logMsg = logMsg.toLowerCase().replace("use", "").replace(";", "");
            String selectedDb = logMsg.replace("`", "").trim();
            changeDatabase(selectedDb);
            Recycler.addWorkProc(listenBackground);
        }
    }
    
    private final WorkProc listenBackground = new WorkProc() {        
        Database theDatabase = null;
        {
            callUpdate = true;
        }
        @Override
        public void updateUI() { 
            ((MainController)getController()).databaseComboBox.getSelectionModel()
                    .select(theDatabase);
        }
        @Override
        public void run() { 
            theDatabase = null;
            if (selectedDatabase != null) {
                for(Database data : getConnectionParams().getDatabases()){
                    if(data.getName().equals(selectedDatabase.getName())){
                        theDatabase = data;
                        break;
                    }
                }
            }
        }
    };
    
    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {                 
        TreeItem selectedItem = (TreeItem) newValue;
        if (selectedItem != null){            
            Database db = getDatabaseFromTreeItem(selectedItem);            
            if (db != null) {
                changeDatabase(db.getName());
            } else if (selectedItem == currentRoot) {
                changeDatabase(null);
            }
        }  
    }

    private Database getDatabaseFromTreeItem(TreeItem selectedItem){
        Database db = null;
         if (selectedItem.getValue() instanceof DatabaseChildren) {
            db = ((DatabaseChildren) selectedItem.getValue()).getDatabase();
        } else if (selectedItem.getValue() instanceof DatabaseChildrenType) {
            db = (Database) selectedItem.getParent().getValue();
        } else if (selectedItem.getValue() instanceof Database) {
            db = (Database) selectedItem.getValue();
        } else if (selectedItem.getValue() instanceof String) {
            // TODO Change this
            if (TREE_NODE__ERDIAGRAM.equals(selectedItem.getValue().toString())) {
                db = (Database) selectedItem.getParent().getValue();
            } else {
                db = ((DBTable)selectedItem.getParent().getValue()).getDatabase();
            }
        } else if (selectedItem.getValue() instanceof Column || selectedItem.getValue() instanceof Index) {
            db = ((DBTable)selectedItem.getParent().getParent().getValue()).getDatabase();
        }
         
        return db;
    }
    
    private String getBackgroundColor(ConnectionParams params) {
        String backColor = params.getBackgroundColor();
        if (backColor != null) {
            if (!backColor.startsWith("#")) {
                backColor = "#" + backColor;
            }
        }
        
        return backColor;
    }
    
    private String getForegroundColor(ConnectionParams params) {
        String foregColor = params.getForegroundColor();
        if (foregColor != null) {
            if (!foregColor.startsWith("#")) {
                foregColor = "#" + foregColor;
            }
        }
        
        return foregColor;
    }
    
    @Override
    public void isFocusedNow() {
        MainController controller = (MainController) getController();
        controller.databaseComboBox.getSelectionModel().selectedItemProperty().
                removeListener((ChangeListener)controller.databaseComboBox.getUserData());
        controller.databaseComboBox.getSelectionModel().clearSelection();        
        controller.databaseComboBox.getItems().clear();
        controller.databaseComboBox.getItems().addAll(getConnectionParams().getDatabases());
        controller.databaseComboBox.getSelectionModel().select(selectedDatabase);
        controller.databaseComboBox.getSelectionModel().selectedItemProperty().
                addListener((ChangeListener)controller.databaseComboBox.getUserData());
    }
    
    public TreeView getTreeView(){
        return treeView;
    }
    
    @FunctionalInterface
    public interface TreeItemPredicate {

        boolean test(TreeItem parent, Object value);

        static TreeItemPredicate create(Predicate predicate) {
            return (parent, value) -> predicate.test(value);
        }

    }
    
    @Getter
    @Setter
    private class ErrDiagramTreeItem extends TreeItem {
        
        private Database database;
        
        public ErrDiagramTreeItem(Database database) {
            super(TREE_NODE__ERDIAGRAM);
            
            this.database = database;
            setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getSchemaDesigner_image()), 16, 16));
        }
    }
    
    
    @Getter
    @Setter
    public class FilterTreeItem extends AnimatableTreeItem<Object> {
        
        private ObservableList<TreeItem> sourceList;
        private FilteredList<TreeItem> filteredList;
    
        private boolean needFilter = false;
        private String filterText;
        
        
        private List<String> filterKeys = new ArrayList<>();
        
        
        public FilterTreeItem() {
            this(null);
        }
        
        public FilterTreeItem(Object obj) {
            super(obj);
            
            this.sourceList = FXCollections.observableArrayList();
            this.filteredList = new FilteredList<>(this.sourceList);
            
            setHiddenFieldChildren(this.filteredList); 
        }
        
        private Predicate<TreeItem> createPredicate() {
            return (TreeItem t) -> {
                
                if (!checkContains(t)) {
                    
                    boolean result = checkIfChildrenContains(t instanceof FilterTreeItem ? ((FilterTreeItem)t).sourceList : t.getChildren());
                    if (!result && t instanceof FilterTreeItem) {
                        ((FilterTreeItem)t).filterKeys.clear();
                    }
                    return result;
                }

                return true;
            };
        }
        
        private boolean checkContains(TreeItem ti) {
            if (ti.getValue() != null) {
                
                // Index need show always
                if (ti.getValue() instanceof String && "Indexes".equalsIgnoreCase(ti.getValue().toString()) || (ti.getValue() instanceof Column && !filterColumns.isSelected())) {
                    return true;
                }
                
                String text = ti.getValue().toString().toLowerCase();
                if (!text.isEmpty()) {
                    if (filterText != null && !filterText.isEmpty() && !text.toLowerCase().contains(filterText.toLowerCase())) {
                        return false;
                    }

                    for (String k: filterKeys) {
                        if (!text.contains(k)) {
                            return false;
                        }
                    }
                }

                return true;
            } else {
                return false;
            }
        }
        
        public void setFilterText(String text) {
            
            String oldFilter = filterText;
            this.filterText = text;
            
            if (!(getValue() instanceof ConnectionParams)) {
                addParentFilterText(getInternalChildren(), filterText.toLowerCase(), oldFilter);
            }
            
            boolean oldNeedFilter = needFilter;
            needFilter = filterText != null && !filterText.isEmpty();
            
            if (needFilter != oldNeedFilter) {
                if (this == currentRoot) {
                    setGraphic(FunctionHelper.constructImageViewWithSize(needFilter ? new ImageView(serverFilterImage) : new ImageView(serverImage), 18, 18));
                } else {
                    setGraphic(FunctionHelper.constructImageViewWithSize(needFilter ? new ImageView(folderFilterImage) : new ImageView(folderImage), 16, 16));
                }
            }
            
            
            this.filteredList.setPredicate(createPredicate());
        }
        
        public void addParentFilterText(List<TreeItem> children, String newValue, String oldValue) {
            if (children != null) {
                for (TreeItem ti: children) {
                    if (ti instanceof FilterTreeItem) {
                        
                        if (ti.getValue() instanceof String && "Indexes".equalsIgnoreCase(ti.getValue().toString())) {
                            continue;
                        }
                        
                        ((FilterTreeItem) ti).filterKeys.remove(oldValue);

                        if (!newValue.isEmpty()) {
                            ((FilterTreeItem) ti).filterKeys.add(newValue);
                        }

                        ((FilterTreeItem) ti).filteredList.setPredicate(((FilterTreeItem) ti).createPredicate());
                    } 
                    addParentFilterText(ti instanceof FilterTreeItem ? ((FilterTreeItem)ti).getInternalChildren() : ti.getChildren(), newValue, oldValue);
                }
            }
        }
        
        
        private boolean checkIfChildrenContains(List<TreeItem> children) {
            if (children != null) {
                
                boolean contains = false;
                for (TreeItem ti: children) {
                    
                    if (ti.getValue() instanceof Database ||
                        ti.getValue() instanceof DBTable ||
                        ti.getValue() instanceof View ||
                        ti.getValue() instanceof Trigger ||
                        ti.getValue() instanceof StoredProc ||
                        ti.getValue() instanceof Event || 
                        ti.getValue() instanceof Column) {
                        
                         contains = checkContains(ti);
                    }
                    
                    if (contains) {
                        return true;
                    }
                    
                    if (!(ti.getValue() instanceof Column) &&
                        !(ti.getValue() instanceof Index) &&
                        !(ti.getValue() instanceof String && !filterColumns.isSelected())) {
                        
                        contains = checkIfChildrenContains(ti instanceof FilterTreeItem ? ((FilterTreeItem)ti).sourceList : ti.getChildren());
                    }
                                        
                    if (contains) {
                        return true;
                    }
                }
            }
            
            return false;
        }
        
        protected void setHiddenFieldChildren(ObservableList<TreeItem> list) {
            try {
                Field childrenField = TreeItem.class.getDeclaredField("children"); //$NON-NLS-1$
                childrenField.setAccessible(true);
                childrenField.set(this, list);

                Field declaredField = TreeItem.class.getDeclaredField("childrenListener"); //$NON-NLS-1$
                declaredField.setAccessible(true);
                list.addListener((ListChangeListener<? super TreeItem>) declaredField.get(this));
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException("Could not set TreeItem.children", e); //$NON-NLS-1$
            }
        }

        public ObservableList<TreeItem> getInternalChildren() {
            return this.sourceList;
        }
    }
    
    public List<Tab> findControllerTabs(Class controllerClass) {
        List result = new ArrayList();
        for (Tab t: centertabPane.getTabs()) {
            if (t.getUserData() != null && t.getUserData().getClass().isAssignableFrom(controllerClass)) {
                result.add(t);
            }
        }
        
        for (Tab t: bottomtabPane.getTabs()) {
            if (t.getUserData() != null && t.getUserData().getClass().isAssignableFrom(controllerClass)) {
                result.add(t);
            }
        }
        
        return result;
    }
    
}
