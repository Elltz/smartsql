/*
 * (C) Copyright 2012 JavaFXGraph (http://code.google.com/p/javafxgraph/).
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3.0 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package np.com.ngopal.smart.sql.ui.fxgraph;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TimelineBuilder;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.util.Duration;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerImagePane;
import np.com.ngopal.smart.sql.ui.controller.designer.DesignerLayerTittledPane;

public class FXGraphSelectionTool extends FXTool {

    static final double SELECTION_Z_OFFSET = 20;

    private final Pane owningControl;
    private final FXGraphModel model;
    private final FXGraphZoomHandler zoomHandler;

    private FXNode currentSelection;
    private final Rectangle currentSelectionRectangle = new Rectangle();
    private final Rectangle currentSelectionTopLeftRectangle = new Rectangle();
    private final Rectangle currentSelectionTopMiddleRectangle = new Rectangle();
    private final Rectangle currentSelectionTopRightRectangle = new Rectangle();
    private final Rectangle currentSelectionCenterLeftRectangle = new Rectangle();
    private final Rectangle currentSelectionCenterRightRectangle = new Rectangle();
    private final Rectangle currentSelectionBottomLeftRectangle = new Rectangle();
    private final Rectangle currentSelectionBottomMiddleRectangle = new Rectangle();
    private final Rectangle currentSelectionBottomRightRectangle = new Rectangle();
    
    
    private Rectangle interactiveSelectionRectangle;
    private Timeline interactiveSelectionTimeline;

    private boolean dragging;
    private double lastDragX;
    private double lastDragY;
    private boolean mousePressedOnNodeOrSelection;
    private FXEdgeWayPoint pressedWaypoint;
    
    private FXNode.ResizeType resizeType;
    
    private final SimpleDoubleProperty widthProperty = new SimpleDoubleProperty(-1);
    private final SimpleDoubleProperty heightProperty = new SimpleDoubleProperty(-1);

    private final SimpleObjectProperty<FXNode> selectedNodeProperty = new SimpleObjectProperty<>();
    
    private final SimpleObjectProperty<FXNode> movedProperty = new SimpleObjectProperty<>();
    private final SimpleObjectProperty<FXNode> resizedProperty = new SimpleObjectProperty<>();
    
    FXGraphSelectionTool(Pane aOwningControl, FXGraphModel aModel, FXGraphZoomHandler aZoomHandler) {
        owningControl = aOwningControl;
        model = aModel;
        zoomHandler = aZoomHandler;
        
        
        initSelectionRectangles();
    }

    public void resetSelection() {
        currentSelection = null;
        unbind();
        
        selectedNodeProperty.set(null);
    }
    
    protected SimpleObjectProperty<FXNode> selectedNodeProperty() {
        return selectedNodeProperty;
    }
    
    public void select(FXNode aNode) {
        
        unbind();
        
        widthProperty.set(aNode.wrappedNode.getBoundsInLocal().getWidth());
        heightProperty.set(aNode.wrappedNode.getBoundsInLocal().getHeight());
        
        aNode.wrappedNode.boundsInLocalProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) -> {
            if (newValue != null) {
                widthProperty.set(newValue.getWidth());
                heightProperty.set(newValue.getHeight());
            }
        });
        
        currentSelection = aNode;
        
        
        if (aNode.wrappedNode instanceof DesignerImagePane) {
            // image to front
            aNode.wrappedNode.toFront();
            
        } else if (aNode.wrappedNode instanceof DesignerLayerTittledPane) {
            
            owningControl.getChildren().remove(aNode.wrappedNode);
            int index = 0;
            for (Node n: owningControl.getChildren()) {
                if (!(n instanceof DesignerLayerTittledPane) && !(n instanceof Rectangle)) {
                    break;
                }
                
                index++;
            }
            
            owningControl.getChildren().add(index > owningControl.getChildren().size() ? owningControl.getChildren().size() : index, aNode.wrappedNode);
            
        } else {
            owningControl.getChildren().remove(aNode.wrappedNode);
            
            int index = 0;
            for (Node n: owningControl.getChildren()) {
                if (n instanceof DesignerImagePane) {
                    break;
                }
                
                index++;
            }
            
            
            owningControl.getChildren().add(index > owningControl.getChildren().size() ? owningControl.getChildren().size() : index, aNode.wrappedNode);
        }
        
        currentSelectionRectangle.toFront();
        currentSelectionTopLeftRectangle.toFront();
        currentSelectionTopMiddleRectangle.toFront();
        currentSelectionTopRightRectangle.toFront();
        currentSelectionCenterLeftRectangle.toFront();
        currentSelectionCenterRightRectangle.toFront();
        currentSelectionBottomLeftRectangle.toFront();
        currentSelectionBottomMiddleRectangle.toFront();
        currentSelectionBottomRightRectangle.toFront();
                
        bind();
        
        selectedNodeProperty.set(currentSelection);
    }        
    
    protected void showCorners() {
        if (currentSelectionTopLeftRectangle.isDisable()) {
            currentSelectionTopLeftRectangle.setDisable(false);
            currentSelectionTopLeftRectangle.setFill(Color.WHITE);
        }
        
        if (currentSelectionTopRightRectangle.isDisable()) {
            currentSelectionTopRightRectangle.setDisable(false);
            currentSelectionTopRightRectangle.setFill(Color.WHITE);
        }
        
        if (currentSelectionBottomLeftRectangle.isDisable()) {
            currentSelectionBottomLeftRectangle.setDisable(false);
            currentSelectionBottomLeftRectangle.setFill(Color.WHITE);
        }
        
        if (currentSelectionBottomRightRectangle.isDisable()) {
            currentSelectionBottomRightRectangle.setDisable(false);
            currentSelectionBottomRightRectangle.setFill(Color.WHITE);
        }
    }
    
    protected void hideCorners() {
        if (!currentSelectionTopLeftRectangle.isDisable()) {
            currentSelectionTopLeftRectangle.setDisable(true);
            currentSelectionTopLeftRectangle.setFill(Color.GRAY);
        }
        
        if (!currentSelectionTopRightRectangle.isDisable()) {
            currentSelectionTopRightRectangle.setDisable(true);
            currentSelectionTopRightRectangle.setFill(Color.GRAY);
        }
        
        if (!currentSelectionBottomLeftRectangle.isDisable()) {
            currentSelectionBottomLeftRectangle.setDisable(true);
            currentSelectionBottomLeftRectangle.setFill(Color.GRAY);
        }
        
        if (!currentSelectionBottomRightRectangle.isDisable()) {
            currentSelectionBottomRightRectangle.setDisable(true);
            currentSelectionBottomRightRectangle.setFill(Color.GRAY);
        }
    }
    
    private void unbind() {
        unbindRectangle(currentSelectionRectangle);
        unbindRectangle(currentSelectionTopLeftRectangle);
        unbindRectangle(currentSelectionTopMiddleRectangle);
        unbindRectangle(currentSelectionTopRightRectangle);
        unbindRectangle(currentSelectionCenterLeftRectangle);
        unbindRectangle(currentSelectionCenterRightRectangle);
        unbindRectangle(currentSelectionBottomLeftRectangle);
        unbindRectangle(currentSelectionBottomMiddleRectangle);
        unbindRectangle(currentSelectionBottomRightRectangle);
    }
    
    private void bind() {                
        
        bindRectangle(currentSelectionRectangle, 
            currentSelection.wrappedNode.layoutXProperty(), 
            currentSelection.wrappedNode.layoutYProperty(), 
            widthProperty, 
            heightProperty);
        
        bindRectangle(currentSelectionTopLeftRectangle, 
            currentSelection.wrappedNode.layoutXProperty().subtract(2), 
            currentSelection.wrappedNode.layoutYProperty().subtract(2), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionTopMiddleRectangle, 
            Bindings.add(currentSelection.wrappedNode.layoutXProperty(), widthProperty.divide(2)).subtract(5), 
            currentSelection.wrappedNode.layoutYProperty().subtract(2), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionTopRightRectangle, 
            Bindings.add(currentSelection.wrappedNode.layoutXProperty(), widthProperty).subtract(8), 
            currentSelection.wrappedNode.layoutYProperty().subtract(2), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionCenterLeftRectangle, 
            currentSelection.wrappedNode.layoutXProperty().subtract(2),
            Bindings.add(currentSelection.wrappedNode.layoutYProperty(), heightProperty.divide(2)).subtract(5), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionCenterRightRectangle, 
            currentSelection.wrappedNode.layoutXProperty().add(widthProperty).subtract(8),
            Bindings.add(currentSelection.wrappedNode.layoutYProperty(), heightProperty.divide(2)).subtract(5), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionBottomLeftRectangle, 
            currentSelection.wrappedNode.layoutXProperty().subtract(2),
            Bindings.add(currentSelection.wrappedNode.layoutYProperty(), heightProperty).subtract(8), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionBottomMiddleRectangle, 
            Bindings.add(currentSelection.wrappedNode.layoutXProperty(), widthProperty.divide(2)).subtract(5),
            Bindings.add(currentSelection.wrappedNode.layoutYProperty(), heightProperty).subtract(8), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        bindRectangle(currentSelectionBottomRightRectangle, 
            Bindings.add(currentSelection.wrappedNode.layoutXProperty(), widthProperty).subtract(8),
            Bindings.add(currentSelection.wrappedNode.layoutYProperty(), heightProperty).subtract(8), 
            new SimpleDoubleProperty(10), 
            new SimpleDoubleProperty(10));
        
        if (currentSelection.canChangeCorners != null) {
            if (currentSelection.canChangeCorners.apply(currentSelection.wrappedNode)) {                
                showCorners();
            } else {
                hideCorners();
            }
        }
    }
    
    private void unbindRectangle(Rectangle rectangle) {
        rectangle.xProperty().unbind();
        rectangle.yProperty().unbind();
        rectangle.widthProperty().unbind();
        rectangle.heightProperty().unbind();
        
        rectangle.setVisible(false);
    }
    
    private void bindRectangle(Rectangle rectangle, ObservableValue xProperty, ObservableValue yProperty, ObservableValue widthProperty, ObservableValue heightProperty) {
        rectangle.xProperty().bind(xProperty);
        rectangle.yProperty().bind(yProperty);
        rectangle.widthProperty().bind(widthProperty);
        rectangle.heightProperty().bind(heightProperty);
        
        rectangle.setVisible(true);
    }
    
    
    private void initSelectionRectangles() {
        
        
        EventHandler<MouseEvent> resizeHandler = (MouseEvent event) -> {
            
            mouseReleased(event);
            
            resizeType = null;
            
            resizedProperty.set(null);
            resizedProperty.set(currentSelection);
        
            event.consume();
        };
        
        EventHandler<MouseEvent> mouseDraggedd = (MouseEvent event) -> {
            mouseDragged(event);
            event.consume();
        };
        
        currentSelectionRectangle.setStrokeWidth(1);
        currentSelectionRectangle.setStroke(Color.web("#c1c6c8"));
        currentSelectionRectangle.setFill(Color.TRANSPARENT);
        currentSelectionRectangle.setMouseTransparent(true);
        currentSelectionRectangle.setTranslateZ(SELECTION_Z_OFFSET);

        currentSelectionTopLeftRectangle.setStrokeWidth(1);
        currentSelectionTopLeftRectangle.setStroke(Color.BLACK);
        currentSelectionTopLeftRectangle.setFill(Color.WHITE);
        currentSelectionTopLeftRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionTopLeftRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.TOP_LEFT;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        
        currentSelectionTopLeftRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionTopLeftRectangle.setOnMouseDragged((MouseEvent event) -> {
            mouseDragged(event);
            event.consume();
        });
        

        currentSelectionTopMiddleRectangle.setStrokeWidth(1);
        currentSelectionTopMiddleRectangle.setStroke(Color.BLACK);
        currentSelectionTopMiddleRectangle.setFill(Color.WHITE);
        currentSelectionTopMiddleRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionTopMiddleRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.TOP_MIDDLE;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionTopMiddleRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionTopMiddleRectangle.setOnMouseDragged(mouseDraggedd);

        currentSelectionTopRightRectangle.setStrokeWidth(1);
        currentSelectionTopRightRectangle.setStroke(Color.BLACK);
        currentSelectionTopRightRectangle.setFill(Color.WHITE);
        currentSelectionTopRightRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionTopRightRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.TOP_RIGHT;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionTopRightRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionTopRightRectangle.setOnMouseDragged(mouseDraggedd);

        currentSelectionCenterLeftRectangle.setStrokeWidth(1);
        currentSelectionCenterLeftRectangle.setStroke(Color.BLACK);
        currentSelectionCenterLeftRectangle.setFill(Color.WHITE);
        currentSelectionCenterLeftRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionCenterLeftRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.CENTER_LEFT;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionCenterLeftRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionCenterLeftRectangle.setOnMouseDragged(mouseDraggedd);


        currentSelectionCenterRightRectangle.setStrokeWidth(1);
        currentSelectionCenterRightRectangle.setStroke(Color.BLACK);
        currentSelectionCenterRightRectangle.setFill(Color.WHITE);
        currentSelectionCenterRightRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionCenterRightRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.CENTER_RIGHT;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionCenterRightRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionCenterRightRectangle.setOnMouseDragged(mouseDraggedd);

        currentSelectionBottomLeftRectangle.setStrokeWidth(1);
        currentSelectionBottomLeftRectangle.setStroke(Color.BLACK);
        currentSelectionBottomLeftRectangle.setFill(Color.WHITE);
        currentSelectionBottomLeftRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionBottomLeftRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.BOTTOM_LEFT;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionBottomLeftRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionBottomLeftRectangle.setOnMouseDragged(mouseDraggedd);

        currentSelectionBottomMiddleRectangle.setStrokeWidth(1);
        currentSelectionBottomMiddleRectangle.setStroke(Color.BLACK);
        currentSelectionBottomMiddleRectangle.setFill(Color.WHITE);
        currentSelectionBottomMiddleRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionBottomMiddleRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.BOTTOM_MIDDLE;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionBottomMiddleRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionBottomMiddleRectangle.setOnMouseDragged(mouseDraggedd);

        currentSelectionBottomRightRectangle.setStrokeWidth(1);
        currentSelectionBottomRightRectangle.setStroke(Color.BLACK);
        currentSelectionBottomRightRectangle.setFill(Color.WHITE);
        currentSelectionBottomRightRectangle.setTranslateZ(SELECTION_Z_OFFSET + 1);
        currentSelectionBottomRightRectangle.setOnMousePressed((MouseEvent event) -> {
            resizeType = FXNode.ResizeType.BOTTOM_RIGHT;
            mousePressedOnNode(event, currentSelection);
            event.consume();
        });
        currentSelectionBottomRightRectangle.setOnMouseReleased(resizeHandler);
        currentSelectionBottomRightRectangle.setOnMouseDragged(mouseDraggedd);

        owningControl.getChildren().addAll(
            currentSelectionRectangle,
            currentSelectionTopLeftRectangle,
            currentSelectionTopMiddleRectangle,
            currentSelectionTopRightRectangle,
            currentSelectionCenterLeftRectangle,
            currentSelectionCenterRightRectangle,
            currentSelectionBottomLeftRectangle,
            currentSelectionBottomMiddleRectangle,
            currentSelectionBottomRightRectangle
        );
    }
    public FXNode getCurrentSelection() {
        return currentSelection;
    }

    public Rectangle getCurrentSelectionRectangle() {
        return currentSelectionRectangle;
    }

//    public boolean contains(FXNode aNode) {
//        return currentSelection.contains(aNode);
//    }
//
//    public void remove(FXNode aNode) {
//        currentSelection.remove(aNode);
//    }

    public boolean isSelectionMode() {
        return interactiveSelectionRectangle != null;
    }

    public void startSelectionAt(double aSceneX, double aSceneY) {
        RectangleBuilder theBuilder = RectangleBuilder.create();
        theBuilder.x(aSceneX).y(aSceneY).width(1).height(1).strokeWidth(1).stroke(Color.BLACK).strokeDashArray(3.0, 7.0, 3.0, 7.0).fill(Color.TRANSPARENT).mouseTransparent(true);

        interactiveSelectionRectangle = theBuilder.build();
        interactiveSelectionRectangle.setTranslateZ(SELECTION_Z_OFFSET);

        owningControl.getChildren().add(interactiveSelectionRectangle);

        Duration theDuration = Duration.millis(1000 / 25);
        KeyFrame theOneFrame = new KeyFrame(theDuration, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                interactiveSelectionRectangle.setStrokeDashOffset(interactiveSelectionRectangle.getStrokeDashOffset() + 1);
            }
        });

        interactiveSelectionTimeline = TimelineBuilder.create().cycleCount(Animation.INDEFINITE).keyFrames(theOneFrame).build();
        interactiveSelectionTimeline.play();

    }

    public void enhanceSelectionTo(double aSceneX, double aSceneY) {
        double width = aSceneX - interactiveSelectionRectangle.getX();
        double height = aSceneY - interactiveSelectionRectangle.getY();

        interactiveSelectionRectangle.setWidth(width);
        interactiveSelectionRectangle.setHeight(height);
    }

    public void endSelection() {

        for (FXNode theNode : model.getNodes()) {
            if (interactiveSelectionRectangle.intersects(theNode.wrappedNode.getBoundsInParent())) {
                select(theNode);
            }
        }

        interactiveSelectionTimeline.stop();
        interactiveSelectionTimeline = null;

        owningControl.getChildren().remove(interactiveSelectionRectangle);
        interactiveSelectionRectangle = null;

        //updateSelectionInScene();
    }

    @Override
    public void mousePressedOnNode(MouseEvent aEvent, FXNode aNode) {

        mousePressedOnNodeOrSelection = true;
        pressedWaypoint = null;

//        if (!(aEvent.isControlDown() || aEvent.isShiftDown())) {
            resetSelection();
            select(aNode);
//        } else {
//            if (contains(aNode)) {
//                remove(aNode);
//            } else {
//                select(aNode);
//            }
//        }
//        updateSelectionInScene();
    }

    @Override
    public void mousePressedOnEdge(MouseEvent aEvent, FXEdge aEdge) {

        mousePressedOnNodeOrSelection = true;
        pressedWaypoint = null;

//        if (aEvent.isShiftDown()) {
//            aEdge.addWayPoint(new FXEdgeWayPoint(aEdge, aEvent.getSceneX() / zoomHandler.currentZoomLevel, aEvent.getSceneY() / zoomHandler.currentZoomLevel));
//        }
        resetSelection();
//        updateSelectionInScene();
    }

    @Override
    public void mousePressedOnEdgeWayPoint(MouseEvent aEvent, FXEdgeWayPoint aWayPoint) {
        mousePressedOnNodeOrSelection = false;
        pressedWaypoint = aWayPoint;
    }

    @Override
    public void mousePressed(MouseEvent aEvent) {
        
        mousePressedOnNodeOrSelection = false;
        pressedWaypoint = null;

        Rectangle theSelection = getCurrentSelectionRectangle();
        if (theSelection != null) {
            if (theSelection.contains(aEvent.getSceneX(), aEvent.getSceneY())) {
                mousePressedOnNodeOrSelection = true;
            } else {
                resetSelection();
//                updateSelectionInScene();
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent aEvent) {
        if (mousePressedOnNodeOrSelection || pressedWaypoint != null) {
            if (!dragging) {
                dragging = true;
            } else {
                double movementX = aEvent.getSceneX() - lastDragX;
                double movementY = aEvent.getSceneY() - lastDragY;

                if (pressedWaypoint == null) {
                    if (currentSelection != null) {
                        currentSelection.translatePosition(movementX, movementY, zoomHandler.currentZoomLevel, resizeType);
                    }
                } else {
                    pressedWaypoint.translatePosition(movementX / zoomHandler.currentZoomLevel, movementY / zoomHandler.currentZoomLevel, zoomHandler.currentZoomLevel);
                }
            }
            lastDragX = aEvent.getSceneX();
            lastDragY = aEvent.getSceneY();

        } else {

            if (!isSelectionMode()) {

                if (!(aEvent.isShiftDown() || aEvent.isControlDown())) {
                    resetSelection();
                }

                startSelectionAt(aEvent.getSceneX(), aEvent.getSceneY());
            } else {
                enhanceSelectionTo(aEvent.getSceneX(), aEvent.getSceneY());
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent aEvent) {
        if (dragging) {
            dragging = false;
            
            if (resizeType == null) {
                movedProperty.set(null);
                movedProperty.set(currentSelection);
            }
        }   
        
        if (isSelectionMode()) {
            endSelection();
        }
    }
    
    public SimpleObjectProperty<FXNode> movedProperty() {
        return movedProperty;
    }
    public SimpleObjectProperty<FXNode> resizedProperty() {
        return resizedProperty;
    }
    
}