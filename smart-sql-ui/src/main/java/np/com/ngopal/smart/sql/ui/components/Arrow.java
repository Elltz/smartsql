package np.com.ngopal.smart.sql.ui.components;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class Arrow extends Path {
    
    private static final double DEFAULT_SIZE = 10.0;
    
    private class InnerArrow {
        private MoveTo moveTo = new MoveTo();
        private LineTo line1 = new LineTo();
        private LineTo line2 = new LineTo();
    }
    
    private MoveTo startTo;
    private LineTo endTo;
    
    private List<InnerArrow> innerArrows;
    
    
    public Arrow(boolean withArrow) {
        this(withArrow, DEFAULT_SIZE);
    }
    
    public Arrow(boolean withArrow, double arrowHeadSize) {
        super();
        
        strokeProperty().bind(fillProperty());
        setFill(Color.BLACK);
        
        startTo = new MoveTo();
        endTo = new LineTo();
        
        getElements().add(startTo);
        getElements().add(endTo);
        
        if (withArrow) {
            
            innerArrows = new ArrayList<>();

            ChangeListener<Number> listener = (ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {

                double startX = startTo.getX();
                double startY = startTo.getY();

                double endX = endTo.getX();
                double endY = endTo.getY();

                for (InnerArrow ia: innerArrows) {
                    getElements().remove(ia.moveTo);
                    getElements().remove(ia.line1);
                    getElements().remove(ia.line2);
                }
                
                innerArrows.clear();
                
                
                // Arrow
                int steps = (int) (Math.sqrt((startX - endX) * (startX - endX) + (startY - endY) * (startY - endY)) / (DEFAULT_SIZE * 2));
                
                double angle = Math.atan2((endY - startY), (endX - startX)) - Math.PI / 2.0;
                double sin = Math.sin(angle);
                double cos = Math.cos(angle);
                    
                for (int i = 0; i < 10; i++) {
                    
                    InnerArrow innerArrow = new InnerArrow();
                    innerArrows.add(innerArrow);
                                
                    angle = Math.atan2((endY - startY), (endX - startX)) - Math.PI / 2.0;
                    sin = Math.sin(angle);
                    cos = Math.cos(angle);

                    double x1 = (- 1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * arrowHeadSize + endX;
                    double y1 = (- 1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * arrowHeadSize + endY;

                    double x2 = (1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * arrowHeadSize + endX;
                    double y2 = (1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * arrowHeadSize + endY;

                    innerArrow.moveTo.setX(x1);
                    innerArrow.moveTo.setY(y1);

                    innerArrow.line1.setX(x2);
                    innerArrow.line1.setY(y2);

                    innerArrow.line2.setX(endX);
                    innerArrow.line2.setY(endY);

                    for (InnerArrow ia: innerArrows) {
                        getElements().add(ia.moveTo);
                        getElements().add(ia.line1);
                        getElements().add(ia.line2);
                    }
                    
                    if (startX > endX) {
                        endX = x1;
                    } else {
                        endX -= x2;
                    }
                    
                    if (startY > endY) {
                        endY = y1 + Math.abs(y1 - y2) / 2;
                    } else {
                        endY -= y2 + Math.abs(y1 - y2) / 2;
                    }
                }
            };

            startTo.xProperty().addListener(listener);
            startTo.yProperty().addListener(listener);

            endTo.xProperty().addListener(listener);
            endTo.yProperty().addListener(listener);
        }
    }
    
//    public final void setArrowStroke(Paint value) {
//        setS
//    }
    
    public final DoubleProperty startXProperty() {
        return startTo.xProperty();
    }
    
    public final DoubleProperty startYProperty() {
        return startTo.yProperty();
    }
    
    public final DoubleProperty endXProperty() {
        return endTo.xProperty();
    }
    
    public final DoubleProperty endYProperty() {
        return endTo.yProperty();
    }
}
