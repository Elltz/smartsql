package np.com.ngopal.smart.sql.modules;

import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SlowQueryAnalyzerModule extends AbstractStandardModule {
        
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 200;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {return true;}

    @Override
    public boolean install() {
        
        registrateServiceChangeListener();
        
        MenuItem slowAnalyzeItem = new MenuItem("MySQL Slow Log Query Analyzer", new ImageView(StandardDefaultImageProvider.getInstance().getSlow_analyzer_image()));
        slowAnalyzeItem.setOnAction((ActionEvent event) -> {
            showSlowQueryAnalyzer(
                getMainController(), 
                getMainController().getSecondProfilerService(), 
                getMainController().getPreferenceService(),
                getMainController().getRecommendationService(),
                getMainController().getColumnCardinalityService(),
                getMainController().getPerformanceTuningService(),
                getMainController().getDb(),
                getMainController().getUsageService()
            );
        });    
        
        slowAnalyzeItem.disableProperty().bind(busy());
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.POWERTOOLS, new MenuItem[] {slowAnalyzeItem});        
        
        
        return true;
    }
        
    @Override
    public boolean uninstall() {
        return true;
    }
    
    @Override
    public String getInfo() { 
        return "Slow Analyze Queries";
    }
}