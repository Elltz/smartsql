/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import np.com.ngopal.smart.sql.ui.components.SqlBrowserTab;
import np.com.ngopal.smart.sql.structure.utils.ConstantWorkProcedures;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class SqlBroswerTabOffHandWorker<T> extends ConstantWorkProcedures<T> {

    private final ArrayList<SqlBrowserTab> tabs = new ArrayList();
    private volatile boolean shouldRestart = false;

    public SqlBroswerTabOffHandWorker() {
        workingProcedure = "SqlBrowserTabWork";
        startInitiation();
    }

    
    public void addTab(SqlBrowserTab sbt) {
        if (sbt != null) {            
            tabs.add(sbt);
        }
    }

    @Override
    public void run() {
        super.run();
        /**
         * shouldRestart flag should be set to true when run is called, so that
         * while loop can run. it is set to false before checkAvailability is called, 
         * to prevent it from calling it more than once.
         * 
         * if  checkAvailability is run and tab is closed shouldRestart will be true
         * and current runing loop in checkAvailability will break, it will re-run
         * while(shouldRestart) loop again because shouldRestart is true;
         */
        shouldRestart = true;
        while (shouldRestart) {
            shouldRestart = false;
            synchronized (tabs) {
                checkAvailability();
            }
        }
        lastVerifier();
    }

    @Override
    public void updateUI() {
        super.updateUI();
    }

    private void checkAvailability() {
        SqlBrowserTab stRem = null;
        for (SqlBrowserTab st : tabs) {
            if (shouldRestart) { break; }
            if(st.getTabPane() == null){
                shouldRestart = true;
                stRem = st;
                break;
            }
            try {
                // we must check only if last activuty is too old
                if (new Date().getTime() - st.getSession().getService().getLastActivity()
                        > st.getSession().getConnectionParam().getSessionIdleTimeout() * 1000) {
                    boolean closed = st.getSession().getService().isClosed();
                    if (st.isClosedConnection() != closed) {
                        st.setClosedConnection(closed);
                    }
                } else if (st.isClosedConnection() == true) {
                    if (!st.getSession().getService().isClosed()) {
                        st.setClosedConnection(false);
                    }
                }
            } catch (SQLException e) { }
        }
        tabs.remove(stRem);
    }

}
