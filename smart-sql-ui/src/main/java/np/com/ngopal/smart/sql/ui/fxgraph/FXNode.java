/*
 * (C) Copyright 2012 JavaFXGraph (http://code.google.com/p/javafxgraph/).
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 3.0 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-3.0.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package np.com.ngopal.smart.sql.ui.fxgraph;

import java.util.function.Function;
import javafx.scene.Node;
import javafx.scene.layout.Region;

public class FXNode {

    public enum ResizeType {
        TOP_LEFT,
        TOP_MIDDLE,
        TOP_RIGHT,
        CENTER_LEFT,
        CENTER_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_MIDDLE,
        BOTTOM_RIGHT
    }
    
    Region wrappedNode;

    FXGraph owner;

    double positionX;

    double positionY;
    
    Function<Node, Boolean> canChangeHeight;
    Function<Node, Boolean> canChangeCorners;
    Function<Node, String> color;

    FXNode(FXGraph aOwner, Region aNode, Function<Node, Boolean> canChageHeight, Function<Node, Boolean> canChangeCorners, Function<Node, String> color) {
        wrappedNode = aNode;
        owner = aOwner;
        this.canChangeHeight = canChageHeight;
        this.canChangeCorners = canChangeCorners;
        this.color = color;
        
        aNode.setUserData(this);
    }
    
    public Object getWrapedNode() {
        return wrappedNode;
    }

    public double getPositionX() {
        return positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    
    
    public void setPosition(double aPositionX, double aPositionY) {        
        wrappedNode.relocate(aPositionX, aPositionY);
        positionX = aPositionX;
        positionY = aPositionY;

        owner.updateEdgeNodesFor(this);
    }


    
    public void translatePosition(double aMovementX, double aMovementY, double aZoomLevel, ResizeType resizeType) {
        if (wrappedNode instanceof Region) {
                        
            if (resizeType != null) {
                if (canChangeCorners == null || 
                        
                        resizeType == ResizeType.TOP_MIDDLE || 
                        resizeType == ResizeType.CENTER_LEFT ||
                        resizeType == ResizeType.CENTER_RIGHT ||
                        resizeType == ResizeType.BOTTOM_MIDDLE ||
                        
                        canChangeCorners.apply(wrappedNode)) {
                    
                    double x = wrappedNode.getLayoutX();
                    double width = ((Region)wrappedNode).getWidth();
                    double minWidth = ((Region)wrappedNode).getMinWidth();

                    double y = wrappedNode.getLayoutY();
                    double height = ((Region)wrappedNode).getHeight();
                    double minHeight = ((Region)wrappedNode).getMinHeight();

                    switch (resizeType) {
                        case TOP_LEFT:
                            if (width - aMovementX > minWidth) {
                                x = wrappedNode.getLayoutX() + aMovementX;
                                width -= aMovementX;
                            }

                            if (height - aMovementY > minHeight) {
                                if (canChangeHeight != null && canChangeHeight.apply(wrappedNode)) {
                                    height -= aMovementY;
                                    y = wrappedNode.getLayoutY() + aMovementY;
                                }
                            }
                            break;

                        case TOP_MIDDLE:

                            if (height - aMovementY > minHeight) {
                                if (canChangeHeight != null && canChangeHeight.apply(wrappedNode)) {
                                    height -= aMovementY;
                                    y = wrappedNode.getLayoutY() + aMovementY;
                                }
                            }
                            break;

                        case TOP_RIGHT:
                            width += aMovementX;
                            if (height - aMovementY > minHeight) {
                                if (canChangeHeight != null && canChangeHeight.apply(wrappedNode)) {
                                    height -= aMovementY;
                                    y = wrappedNode.getLayoutY() + aMovementY;
                                }
                            }
                            break;

                        case CENTER_LEFT:
                            if (width - aMovementX > minWidth) {
                                x = wrappedNode.getLayoutX() + aMovementX;
                                width -= aMovementX;
                            }
                            break;

                        case CENTER_RIGHT:
                            width += aMovementX;
                            break;

                        case BOTTOM_LEFT:

                            if (width - aMovementX > minWidth) {
                                x = wrappedNode.getLayoutX() + aMovementX;
                                width -= aMovementX;
                            }

                            if (height + aMovementY > minHeight) {
                                if (canChangeHeight != null && canChangeHeight.apply(wrappedNode)) {
                                    height += aMovementY;
                                }

                            }
                            break;

                        case BOTTOM_MIDDLE:
                            if (height + aMovementY > minHeight) {
                                if (canChangeHeight != null && canChangeHeight.apply(wrappedNode)) {
                                    height += aMovementY;
                                }
                            }
                            break;

                        case BOTTOM_RIGHT:

                            if (width + aMovementX > minWidth) {
                                width += aMovementX;
                            }

                            if (height + aMovementY > minHeight) {
                                if (canChangeHeight != null && canChangeHeight.apply(wrappedNode)) {
                                    height += aMovementY;
                                }
                            }
                            break;
                    }                

                    ((Region)wrappedNode).setPrefSize(width, height);
                    ((Region)wrappedNode).resizeRelocate(x, y, width, height);
                }              
            } else {
                wrappedNode.setLayoutX(wrappedNode.getLayoutX() + aMovementX);
                wrappedNode.setLayoutY(wrappedNode.getLayoutY() + aMovementY);
            }
        } else {
            wrappedNode.setLayoutX(wrappedNode.getLayoutX() + aMovementX);
            wrappedNode.setLayoutY(wrappedNode.getLayoutY() + aMovementY);
        }

        
        positionX += aMovementX / aZoomLevel;
        positionY += aMovementY / aZoomLevel;

        owner.updateEdgeNodesFor(this);
    }

    public void setZoomLevel(double aZoomLevel) {
        wrappedNode.setLayoutX(positionX * aZoomLevel);
        wrappedNode.setLayoutY(positionY * aZoomLevel);
        wrappedNode.setScaleX(aZoomLevel);
        wrappedNode.setScaleY(aZoomLevel);

        owner.updateEdgeNodesFor(this, aZoomLevel);
    }
}
