
import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.folding.FoldParserManager;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class CodeAreaPerformance {
    
    public static void main(String[] args) {
        
        JFrame frame = new JFrame();
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Add custom FoldParser for collapsing functionality with '(' and ')'
        FoldParserManager.get().addFoldParserMapping(FoldParserManager.SYNTAX_STYLE_SQL, new SmartSQLCurlyFoldParser());

        RSyntaxTextArea textArea = new RSyntaxTextArea();

        // Enabling folding and set SQL style edit
        textArea.setCodeFoldingEnabled(true);
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);

        textArea.setMatchedBracketBorderColor(java.awt.Color.yellow);
        textArea.setMatchedBracketBGColor(java.awt.Color.yellow);

        RTextScrollPane sp = new RTextScrollPane(textArea);
        sp.setIconRowHeaderEnabled(true);

        DefaultCompletionProvider provider = new DefaultCompletionProvider();
        provider.setAutoActivationRules(true, ".");

        provider.addCompletion(new BasicCompletion(provider, "select"));
        provider.addCompletion(new BasicCompletion(provider, "select1"));
        provider.addCompletion(new BasicCompletion(provider, "select2"));
        provider.addCompletion(new BasicCompletion(provider, "from"));
        provider.addCompletion(new BasicCompletion(provider, "where"));
        provider.addCompletion(new BasicCompletion(provider, "order by"));
        provider.addCompletion(new BasicCompletion(provider, "group by"));
        provider.addCompletion(new BasicCompletion(provider, "limit"));

        AutoCompletion ac = new AutoCompletion(provider) {


            @Override
            protected int refreshPopupWindow() {

                int i = super.refreshPopupWindow();


                return i;
            }

        };
        ac.setAutoActivationEnabled(true);
        ac.setShowDescWindow(true);
        ac.install(textArea);

        textArea.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                changed();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                changed();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                changed();
            }

            // TODO Change to background threads
            private void changed() {
                
            }                
        });
        
        frame.setContentPane(sp);
        frame.setVisible(true);
    }
}
