/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ReplaceString {

    
    
    public static void main(String[] args) {
        String str = "select\n" +
"  if(\n" +
"       isnull(\n" +
"                `performance_schema`.`threads`.`PROCESSLIST_ID`\n" +
"             ),\n" +
"       substring_index(\n" +
"                         `performance_schema`.`threads`.`NAME`,\n" +
"                         '/',\n" +
"                         -(\n" +
"                             1\n" +
"                          )\n" +
"                      ),\n" +
"       concat(\n" +
"                `performance_schema`.`threads`.`PROCESSLIST_USER`,\n" +
"                '@',\n" +
"                `performance_schema`.`threads`.`PROCESSLIST_HOST`\n" +
"             )\n" +
"    ) AS `User`\n" +
"    ,\n" +
" \n" +
"    \n" +
"  sum(`performance_schema`.`events_waits_summary_by_thread_by_event_name`.`COUNT_STAR`) AS `Total`,\n" +
"  `sys`.`format_time`(\n" +
"    sum(`performance_schema`.`events_waits_summary_by_thread_by_event_name`.`SUM_TIMER_WAIT`)\n" +
") AS `Total Latency`,\n" +
"  `sys`.`format_time`(\n" +
"    min(`performance_schema`.`events_waits_summary_by_thread_by_event_name`.`MIN_TIMER_WAIT`)\n" +
") AS `MIN Latency`,\n" +
"  `sys`.`format_time`(\n" +
"    avg(`performance_schema`.`events_waits_summary_by_thread_by_event_name`.`AVG_TIMER_WAIT`)\n" +
") AS `AVG Latency`,\n" +
"  `sys`.`format_time`(\n" +
"    max(`performance_schema`.`events_waits_summary_by_thread_by_event_name`.`MAX_TIMER_WAIT`)\n" +
") AS `MAX Latency`  \n" +
"from\n" +
"  (\n" +
"    `performance_schema`.`events_waits_summary_by_thread_by_event_name` \n" +
"    left join `performance_schema`.`threads` \n" +
"      on(\n" +
"        (\n" +
"          `performance_schema`.`events_waits_summary_by_thread_by_event_name`.`THREAD_ID` = `performance_schema`.`threads`.`THREAD_ID`\n" +
")\n" +
")\n" +
") \n" +
"where (\n" +
"  (\n" +
"    `performance_schema`.`events_waits_summary_by_thread_by_event_name`.`EVENT_NAME` like 'wait/io/file/%'\n" +
") \n" +
"and (\n" +
"    `performance_schema`.`events_waits_summary_by_thread_by_event_name`.`SUM_TIMER_WAIT` > 0\n" +
")\n" +
") \n" +
"group by  \n" +
"  `user` \n" +
"order by sum(`performance_schema`.`events_waits_summary_by_thread_by_event_name`.`SUM_TIMER_WAIT`)";
        
        
        System.out.println(str.replaceAll("\\s+", " "));
    }
}
