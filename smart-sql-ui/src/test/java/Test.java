/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.github.sarxos.winreg.HKey;
import com.github.sarxos.winreg.RegistryException;
import com.github.sarxos.winreg.WindowsRegistry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class Test {
    public String changeNewName(String name) {

        String[] splitted = name.split("_");
        if (splitted.length == 1) {
            System.out.println("not found");
            return name + "_1";
        }
        String s1 = splitted[0];
        String s2 = splitted[1];
        System.out.println("s1" + s1);
        System.out.println("s2" + s2);
        for (int i = 1; i < 10; i++) {
            if (i <= Integer.parseInt(s2)) {
                System.out.println("i continued");
                continue;
            }
            return s1 + "_" + i;
        }
        return null;
    }

    public List<String> separateWithComma(String databases) {
//        List arr = new ArrayList<String>();
//        arr.add(Arrays.asList(databases.split(";")));
//        String[] s = databases.split(";");
        List<String> myList = new ArrayList<String>(Arrays.asList(databases.split(";")));
        return myList;

    }

    public String reverseSeparateWithComma(List<String> databaseList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < databaseList.size(); i++) {
            stringBuilder.append(databaseList.get(i));
            if (i == databaseList.size() - 1) {
                continue;
            }
            stringBuilder.append(';');
        }

        return stringBuilder.toString();

    }

    public static void main(String[] args) throws RegistryException {
        
        
        // TESTING
        // Modify registry
        
        //HKEY_CLASSES_ROOT\sml_auto_file
        
        
        WindowsRegistry reg = WindowsRegistry.getInstance();
        reg.createKey(HKey.HKCR, ".sll");
        reg.writeStringValue(HKey.HKCR, ".sll", "", "SmartMySQL");
        
        reg.createKey(HKey.HKCR, "SmartMySQL");
        reg.writeStringValue(HKey.HKCR, "SmartMySQL", "", "SQL Text File");
        
        reg.createKey(HKey.HKCR, "SmartMySQL\\DefaultIcon");
        reg.writeStringValue(HKey.HKCR, "SmartMySQL\\DefaultIcon", "", "C:\\Program Files\\SmartMySQLQB\\SmartMySQLQB_QA.ico,0");

        reg.createKey(HKey.HKCR, "SmartMySQL\\Shell");
        
        reg.createKey(HKey.HKCR, "SmartMySQL\\Shell\\Open");
        
        reg.createKey(HKey.HKCR, "SmartMySQL\\Shell\\Open\\Command");
        reg.writeStringValue(HKey.HKCR, "SmartMySQL\\Shell\\Open\\Command", "", "\"C:\\Program Files\\SmartMySQLQB\\SmartMySQLQB_QA.exe\"" + " \"%1\"");
        
//        List<String> str = new ArrayList<>();
//        str.add("razee");
//        str.add("razee2");
//        str.add("razee3");
//        List<String> s = new Test().separateWithComma("razee;ra;r");
//        System.out.println("size : " + s.size());
//        System.out.println(new Test().reverseSeparateWithComma(s));
//        ArrayList<String> s = (ArrayList<String>)new Test().separateWithComma("razee;ra;r");
//        String[] s = {"Art", "Dan", "Jen"};
//        for (String st : s) {
//            System.out.println(st + " ");
//        }
    }
}
