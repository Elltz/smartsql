
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class EncryptTest {

    public static void main(String[] args) throws Throwable {
        new EncryptTest().test();
        new EncryptTest().test1();
    }
    
    private void test() throws Throwable {
        try (FileOutputStream fs = new FileOutputStream("<file>")) {
            
            String encryptedString = null;            
            Cipher cipher = Cipher.getInstance("DESede");
            
            DESedeKeySpec ks = new DESedeKeySpec("<key>".getBytes("UTF8"));
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
            
            cipher.init(Cipher.ENCRYPT_MODE, skf.generateSecret(ks));
            
            byte[] plainText = "<text>".getBytes("UTF-8");
            byte[] encryptedText = cipher.doFinal(plainText);
                
            encryptedString = new String(Base64.encodeBase64(encryptedText));
            
            fs.write(encryptedString.getBytes("UTF-8"));
        }
                
    }
    
    
    private void test1() throws Throwable {
        StringBuilder result = new StringBuilder();
        Files.lines(new File("<file>").toPath()).forEach(line -> result.append(line));

        System.out.println("encrypt result: " + result.toString()); 
        
        Cipher cipher = Cipher.getInstance("DESede");

        DESedeKeySpec ks = new DESedeKeySpec("<key>".getBytes("UTF-8"));
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");

        cipher.init(Cipher.DECRYPT_MODE, skf.generateSecret(ks));
        byte[] encryptedText = Base64.decodeBase64(result.toString());
        byte[] plainText = cipher.doFinal(encryptedText);

        System.out.println("result: " + new String(plainText, "UTF-8"));            
    }
}
