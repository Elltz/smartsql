
import np.com.ngopal.smart.sql.model.utils.SqlUtils;
import org.junit.Assert;



/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SqlUtilsTest {

    @org.junit.Test
    public void parseGetSelectQueryInsideParents()
    {
        String query0 = "select * from b where a in ()";
        
        Assert.assertEquals(query0, SqlUtils.getQueryInsideParents(query0, 7, 7).get("query"));
        Assert.assertEquals("", SqlUtils.getQueryInsideParents(query0, 28, 28).get("query"));
        Assert.assertEquals(query0, SqlUtils.getQueryInsideParents(query0, 27, 27).get("query"));
        Assert.assertEquals(query0, SqlUtils.getQueryInsideParents(query0, 29, 29).get("query"));
        
        String query1 = "select * from b where a in (select * from a)";
        String query11 = "select * from a";
        
        Assert.assertEquals(query1, SqlUtils.getQueryInsideParents(query1, 7, 7).get("query"));
        Assert.assertEquals(query11, SqlUtils.getQueryInsideParents(query1, 30, 30).get("query"));
        Assert.assertEquals(query1, SqlUtils.getQueryInsideParents(query1, 27, 27).get("query"));
        Assert.assertEquals(query11, SqlUtils.getQueryInsideParents(query1, 29, 29).get("query"));
        
        String query2 = "select * from b where a in (a, b, s)";
        
        Assert.assertEquals(query2, SqlUtils.getQueryInsideParents(query2, 7, 7).get("query"));
        Assert.assertEquals(query2, SqlUtils.getQueryInsideParents(query2, 30, 30).get("query"));
        Assert.assertEquals(query2, SqlUtils.getQueryInsideParents(query2, 27, 27).get("query"));
        Assert.assertEquals(query2, SqlUtils.getQueryInsideParents(query2, 29, 29).get("query"));
    }
    
}
