
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.utils.MemoryUsageUtils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class TabPerformance extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        
        TabPane tabPane = new TabPane();
        tabPane.setCache(true);
        BorderPane root = new BorderPane(tabPane);
        Scene scene = new Scene(root, 400, 600);
        
        Button addALl = new Button("add tabs");
        addALl.setOnAction((ActionEvent event) -> {
            
            tabPane.getTabs().clear();
            List<Tab> tabs = new ArrayList<>();
            MemoryUsageUtils.printMemoryUsage("BEGIN");
            for (int i = 0; i < 200; i++) {
                
                Tab tab = new Tab();
                
//                tab.setGraphic(new ImageView(ImageProvider.QUERY_TAB_MODULE__IMAGE__RESULT_TAB));
                tab.setContent(new HBox(new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button(), new Button()));
                tabs.add(tab);
            }
            
            tabPane.getTabs().addAll(tabs);
            
            tabPane.getSelectionModel().selectLast();
            MemoryUsageUtils.printMemoryUsage("END");
        });
        
        Button clearAll = new Button("clear tabs");
        clearAll.setOnAction((ActionEvent event) -> {
            
            MemoryUsageUtils.printMemoryUsage("BEGIN");
            tabPane.getTabs().clear();
            MemoryUsageUtils.printMemoryUsage("END");
        });
        
        root.setTop(new HBox(10, addALl, clearAll));
        
        
        primaryStage.setScene(scene);        
        primaryStage.show();
    }

    
    public static void main(String[] args) {
        launch(args);
    }
}
