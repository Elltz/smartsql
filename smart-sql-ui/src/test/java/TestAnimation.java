
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.ui.components.TextBanner;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class TestAnimation extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestAnimation.launch(args);
    }

    private int line = 0;
    
    @Override
    public void start(Stage stage) {
                
        TextBanner banner = new TextBanner();
        banner.init("Test banner !!!\nHi again !!!\nToo tooo !!!");
        BorderPane root = new BorderPane(banner);
        root.setStyle("-fx-border-color: green");
        root.setPrefSize(500, 70);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Scrolling Text");
        stage.show();
        
        banner.start(1000);

    }
}
