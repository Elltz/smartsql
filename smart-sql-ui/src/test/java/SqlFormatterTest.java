
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.model.utils.SqlUtils;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import org.junit.Assert;
import tools.java.pats.models.SqlFormatter;



/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class SqlFormatterTest {

    @org.junit.Test
    public void parseUsingJoinQuery()
    {
        String[] result = SqlUtils.splitByUnion("select * frOm b UNION select * FROM c Union All select * from d Union select * from F");
        Assert.assertEquals(4, result.length);
        Assert.assertEquals("select * frOm b", result[0]);
        Assert.assertEquals("select * FROM c", result[1]);
        Assert.assertEquals("select * from d", result[2]);
        Assert.assertEquals("select * from F", result[3]);
    }
    
    @org.junit.Test
    public void findQuery() {
        
        String query = "SELECT * FROM ((SELECT information_schema.`COLUMNS`.table_schema `db_name`, information_schema.`COLUMNS`.table_name `table_name` ,information_schema.TABLES.ENGINE ENGINE , CONCAT(        column_name,        '$$$$',        column_type ,                 '$$$$' , if(information_schema.COLUMNS.IS_NULLABLE,'no', 'NOT NULL'), '$$$$', IF(information_schema.COLUMNS.`EXTRA`='auto_increment', 'auto_increment', ''), '$$$$' , IFNULL(information_schema.COLUMNS.COLUMN_DEFAULT,'')  ,        '$$$$' , IFNULL(information_schema.COLUMNS.CHARACTER_SET_NAME,'') ,        '$$$$' , IFNULL(information_schema.COLUMNS.COLLATION_NAME,'')                ) `column_name`, 1 `type`     FROM      information_schema.`COLUMNS`     JOIN information_schema.`TABLES`       ON information_schema.`COLUMNS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME`         AND information_schema.`COLUMNS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA`   WHERE information_schema.`TABLES`.`TABLE_TYPE` !='VIEW' ORDER BY information_schema.`COLUMNS`.`ORDINAL_POSITION`) UNION ALL(SELECT information_schema.`STATISTICS`.table_schema `db_name`, information_schema.`STATISTICS`.table_name `table_name`, '' ENGINE, CONCAT(INDEX_TYPE, ' ', index_name, '(', GROUP_CONCAT(column_name ORDER BY seq_in_index ), ')' ) `column_name`, 2 `type` FROM information_schema.`STATISTICS` JOIN information_schema.`TABLES` ON information_schema.`STATISTICS`.`TABLE_NAME`=information_schema.`TABLES`.`TABLE_NAME` AND information_schema.`STATISTICS`.`TABLE_SCHEMA`=information_schema.`TABLES`.`TABLE_SCHEMA` WHERE information_schema.`TABLES`.`TABLE_TYPE` !='VIEW' GROUP BY information_schema.`STATISTICS`.table_schema, information_schema.`STATISTICS`.table_name, index_name) UNION ALL (SELECT  table_schema `db_name`, table_name `table_name` , '' ENGINE, CONCAT('CREATE\n" +
"VIEW ', table_name, '\n" +
"AS\n" +
"', `VIEW_DEFINITION`) `column_name`, 3 `type` FROM  information_schema.`VIEWS`) UNION ALL (SELECT  routine_schema `db_name`, routine_name `table_name` , '' ENGINE, '' `column_name`, IF(    Routine_type='PROCEDURE',    4,    5  ) `type` FROM  information_schema.`ROUTINES`) UNION ALL (SELECT  trigger_schema `db_name`, trigger_name `table_name`,'' ENGINE,CONCAT('CREATE TRIGGER ', trigger_name, '\n" +
"  ', action_timing, ' ', event_manipulation, '\n" +
"  ON ', event_object_table, '\n" +
"  FOR EACH ROW\n" +
"', action_statement) `column_name`, 6 `type` FROM  information_schema.`TRIGGERS`) UNION ALL(SELECT  event_schema `db_name`, event_name `table_name`,'' ENGINE,CONCAT('CREATE EVENT ', `EVENT_NAME`, '\n" +
"  ON SCHEDULE ', IF(`EVENT_TYPE` = 'ONE TIME', CONCAT('AT ''', `EXECUTE_AT`, '''\n" +
"  '), CONCAT('EVERY ''', `INTERVAL_VALUE`, ''' ', `INTERVAL_FIELD`, '\n" +
"  STARTS ''', `STARTS`, '''\n" +
"  ', IF (`ENDS` is not null, CONCAT('ENDS ''', `ENDS`, '''\n" +
"  '), ''))), 'DO\n" +
"', `EVENT_DEFINITION`) `column_name`, 7 `type` FROM  information_schema.`EVENTS`) UNION ALL(SELECT  information_schema.`SCHEMATA`.`SCHEMA_NAME` `db_name`,'' `table_name`,'' ENGINE,'' `column_name`, 0 TYPE FROM  information_schema.`SCHEMATA`) UNION ALL (SELECT information_schema.`KEY_COLUMN_USAGE`.CONSTRAINT_SCHEMA db_name, information_schema.`KEY_COLUMN_USAGE`.TABLE_NAME table_name, '' ENGINE, CONCAT('CONSTRAINT `', information_schema.KEY_COLUMN_USAGE.CONSTRAINT_NAME, '` FOREIGN KEY (`', GROUP_CONCAT(information_schema.KEY_COLUMN_USAGE.COLUMN_NAME ORDER BY information_schema.`KEY_COLUMN_USAGE`.`ORDINAL_POSITION` ), '`) REFERENCES `', information_schema.KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME, '`(`', information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME, '`)' , CONCAT( IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='CASCADE',' ON UPDATE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.update_rule='RESTRICT',' ON UPDATE RESTRICT', '')), IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='CASCADE',' ON DELETE CASCADE',IF(information_schema.`REFERENTIAL_CONSTRAINTS`.delete_rule='RESTRICT',' ON DELETE RESTRICT', '')))) column_name, 8 TYPE FROM information_schema.KEY_COLUMN_USAGE LEFT JOIN information_schema.`REFERENTIAL_CONSTRAINTS` ON information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_NAME` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_NAME` AND information_schema.KEY_COLUMN_USAGE.`CONSTRAINT_SCHEMA` =information_schema.`REFERENTIAL_CONSTRAINTS`.`CONSTRAINT_SCHEMA` WHERE information_schema.KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME IS NOT NULL GROUP BY information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_SCHEMA`, information_schema.`KEY_COLUMN_USAGE`.`CONSTRAINT_NAME`) UNION ALL (SELECT T.table_schema `db_name`, T.table_name `table_name`, '' ENGINE, CONCAT( IFNULL(T.engine,''), '$$$$', IFNULL(CCSA.CHARACTER_SET_NAME,''), '$$$$', IFNULL(T.TABLE_COLLATION,''), '$$$$', IFNULL(T.`AUTO_INCREMENT`, ''), '$$$$', IFNULL(T.`AVG_ROW_LENGTH`,'')) `column_name`, 9 TYPE FROM information_schema.`TABLES` T JOIN information_schema.COLLATION_CHARACTER_SET_APPLICABILITY AS CCSA  ON (T.TABLE_COLLATION = CCSA.COLLATION_NAME) )) a ORDER BY 1, 2";
    
        QueryTabModule.FindQueryResult qr = QueryTabModule.findQuery(query, 100);
        
        Assert.assertNotNull(qr);
        Assert.assertEquals(qr.getStart(), 0);
        Assert.assertEquals(qr.getEnd(), query.length());
    }
    
    @org.junit.Test
    public void findQUery() {
        String query = "\n" +
            "\n" +
            "\n" +
            "DELIMITER ;;\n" + 
            "CREATE TRIGGER `ins_film` AFTER INSERT ON `film` FOR EACH ROW BEGIN\n" +
            "    INSERT INTO film_text (film_id, title, description)\n" +
            "        VALUES (new.film_id, new.title, new.description);\n" +
            "  END;;\n" +
            "\n" +
            "\n" +
            "CREATE TRIGGER `upd_film` AFTER UPDATE ON `film` FOR EACH ROW BEGIN\n" +
            "    IF (old.title != new.title) OR (old.description != new.description) OR (old.film_id != new.film_id)\n" +
            "    THEN\n" +
            "        UPDATE film_text\n" +
            "            SET title=new.title,\n" +
            "                description=new.description,\n" +
            "                film_id=new.film_id\n" +
            "        WHERE film_id=old.film_id;\n" +
            "    END IF;\n" +
            "  END;;\n" +
            "\n" +
            "\n" +
            "CREATE TRIGGER `del_film` AFTER DELETE ON `film` FOR EACH ROW BEGIN\n" +
            "    DELETE FROM film_text WHERE film_id = old.film_id;\n" +
            "  END;;\n" +
            "DELIMITER ;\n" + 
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "CREATE TABLE inventory (\n" +
            "  inventory_id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,\n" +
            "  film_id SMALLINT UNSIGNED NOT NULL,\n" +
            "  store_id TINYINT UNSIGNED NOT NULL,\n" +
            "  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n" +
            "  PRIMARY KEY  (inventory_id),\n" +
            "  KEY idx_fk_film_id (film_id),\n" +
            "  KEY idx_store_id_film_id (store_id,film_id),\n" +
            "  CONSTRAINT fk_inventory_store FOREIGN KEY (store_id) REFERENCES store (store_id) ON DELETE RESTRICT ON UPDATE CASCADE,\n" +
            "  CONSTRAINT fk_inventory_film FOREIGN KEY (film_id) REFERENCES film (film_id) ON DELETE RESTRICT ON UPDATE CASCADE\n" +
            ")ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        
        ScriptRunner sr = new ScriptRunner(null, true);
        try {
            Map<Integer, List<String>> result = sr.findQueries(new StringReader(query));
            Assert.assertEquals(4, result.size());
        } catch (IOException ex) {
            Assert.fail();
        }
        
        
    }
    
    @org.junit.Test
    public void testFormat() {
        String query = "/* select#1 */ select `smartsqlBugs`.`helpdesk_tickets`.`id` AS `id`,`smartsqlBugs`.`helpdesk_tickets`.`description` AS `description`,`smartsqlBugs`.`helpdesk_tickets`.`requester_id` AS `requester_id`,`smartsqlBugs`.`helpdesk_tickets`.`responder_id` AS `responder_id`,`smartsqlBugs`.`helpdesk_tickets`.`status` AS `status`,`smartsqlBugs`.`helpdesk_tickets`.`urgent` AS `urgent`,`smartsqlBugs`.`helpdesk_tickets`.`source` AS `source`,`smartsqlBugs`.`helpdesk_tickets`.`spam` AS `spam`,`smartsqlBugs`.`helpdesk_tickets`.`deleted` AS `deleted`,`smartsqlBugs`.`helpdesk_tickets`.`created_at` AS `created_at`,`smartsqlBugs`.`helpdesk_tickets`.`updated_at` AS `updated_at`,`smartsqlBugs`.`helpdesk_tickets`.`trained` AS `trained`,`smartsqlBugs`.`helpdesk_tickets`.`account_id` AS `account_id`,`smartsqlBugs`.`helpdesk_tickets`.`subject` AS `subject`,`smartsqlBugs`.`helpdesk_tickets`.`display_id` AS `display_id`,`smartsqlBugs`.`helpdesk_tickets`.`owner_id` AS `owner_id`,`smartsqlBugs`.`helpdesk_tickets`.`group_id` AS `group_id`,`smartsqlBugs`.`helpdesk_tickets`.`due_by` AS `due_by`,`smartsqlBugs`.`helpdesk_tickets`.`frDueBy` AS `frDueBy`,`smartsqlBugs`.`helpdesk_tickets`.`isescalated` AS `isescalated`,`smartsqlBugs`.`helpdesk_tickets`.`priority` AS `priority`,`smartsqlBugs`.`helpdesk_tickets`.`fr_escalated` AS `fr_escalated`,`smartsqlBugs`.`helpdesk_tickets`.`to_email` AS `to_email`,`smartsqlBugs`.`helpdesk_tickets`.`email_config_id` AS `email_config_id`,`smartsqlBugs`.`helpdesk_tickets`.`cc_email` AS `cc_email`,`smartsqlBugs`.`helpdesk_tickets`.`delta` AS `delta`,`smartsqlBugs`.`helpdesk_tickets`.`import_id` AS `import_id`,`smartsqlBugs`.`helpdesk_tickets`.`ticket_type` AS `ticket_type`,`smartsqlBugs`.`helpdesk_tickets`.`description_html` AS `description_html`,`smartsqlBugs`.`helpdesk_tickets`.`parent_ticket_id` AS `parent_ticket_id`,`smartsqlBugs`.`helpdesk_tickets`.`dirty` AS `dirty`,`smartsqlBugs`.`helpdesk_tickets`.`sl_product_id` AS `sl_product_id`,`smartsqlBugs`.`helpdesk_tickets`.`sl_sla_policy_id` AS `sl_sla_policy_id`,`smartsqlBugs`.`helpdesk_tickets`.`sl_merge_parent_ticket` AS `sl_merge_parent_ticket`,`smartsqlBugs`.`helpdesk_tickets`.`sl_skill_id` AS `sl_skill_id`,`smartsqlBugs`.`helpdesk_tickets`.`st_survey_rating` AS `st_survey_rating`,`smartsqlBugs`.`helpdesk_tickets`.`sl_escalation_level` AS `sl_escalation_level`,`smartsqlBugs`.`helpdesk_tickets`.`sl_manual_dueby` AS `sl_manual_dueby`,`smartsqlBugs`.`helpdesk_tickets`.`internal_group_id` AS `internal_group_id`,`smartsqlBugs`.`helpdesk_tickets`.`internal_agent_id` AS `internal_agent_id`,`smartsqlBugs`.`helpdesk_tickets`.`association_type` AS `association_type`,`smartsqlBugs`.`helpdesk_tickets`.`associates_rdb` AS `associates_rdb`,`smartsqlBugs`.`helpdesk_tickets`.`sla_state` AS `sla_state` from `smartsqlBugs`.`helpdesk_tickets` join `smartsqlBugs`.`helpdesk_ticket_states` join `smartsqlBugs`.`groups` where ((`smartsqlBugs`.`helpdesk_ticket_states`.`ticket_id` = `smartsqlBugs`.`helpdesk_tickets`.`id`) and (`smartsqlBugs`.`helpdesk_tickets`.`status` = 2) and (`smartsqlBugs`.`helpdesk_ticket_states`.`group_escalated` = 0) and (`smartsqlBugs`.`helpdesk_tickets`.`deleted` = 0) and (`smartsqlBugs`.`helpdesk_tickets`.`spam` = 0) and (`smartsqlBugs`.`helpdesk_tickets`.`account_id` = 178244) and (`smartsqlBugs`.`helpdesk_ticket_states`.`account_id` = 178244) and (`smartsqlBugs`.`groups`.`account_id` = 178244) and ((`smartsqlBugs`.`helpdesk_tickets`.`created_at` + interval `smartsqlBugs`.`groups`.`assign_time` second) <= '2017-11-07 02:58:25') and isnull(`smartsqlBugs`.`helpdesk_ticket_states`.`first_assigned_at`) and (`smartsqlBugs`.`helpdesk_tickets`.`updated_at` > '2017-09-07 02:58:25') and (`smartsqlBugs`.`groups`.`id` = `smartsqlBugs`.`helpdesk_tickets`.`group_id`)) order by `smartsqlBugs`.`helpdesk_tickets`.`id` limit 1000";
        String result = new SqlFormatter().formatSql(query, "", "2", "block");
        Assert.assertNotNull(result);
    }
    
    
    @org.junit.Test
    public void testTableNames() {
        String query = "SELECT\n" +
            "  node0_.id                   AS id1_29_,\n" +
            "  node0_.adhoc_call_supported AS adhoc_ca2_29_,\n" +
            "  node0_.bot_version_id       AS bot_vers3_29_,\n" +
            "  node0_.client_id            AS client_i4_29_,\n" +
            "  node0_.confirm_default      AS confirm_5_29_,\n" +
            "  node0_.content_type         AS content_6_29_,\n" +
            "  node0_.create_date          AS create_d7_29_,\n" +
            "  node0_.default_value        AS default_8_29_,\n" +
            "  node0_.display_order        AS display_9_29_,\n" +
            "  node0_.position             AS positio10_29_,\n" +
            "  node0_.followup_text        AS followu11_29_,\n" +
            "  node0_.input_metadata       AS input_m12_29_,\n" +
            "  node0_.intent               AS intent13_29_,\n" +
            "  node0_.label                AS label14_29_,\n" +
            "  node0_.mandatory            AS mandato15_29_,\n" +
            "  node0_.node_group_id        AS node_gr20_29_,\n" +
            "  node0_.public               AS public16_29_,\n" +
            "  node0_.restrict_input       AS restric17_29_,\n" +
            "  node0_.text                 AS text18_29_,\n" +
            "  node0_.value_src            AS value_s19_29_\n" +
            "FROM\n" +
            "  node node0_\n" +
            "  CROSS JOIN node_group nodegroup1_\n" +
            "WHERE\n" +
            "      node0_.client_id = 5\n" +
            "  AND ( node0_.active = 1 )\n" +
            "  AND node0_.node_group_id=nodegroup1_.id\n" +
            "  AND 1=1\n" +
            "  AND nodegroup1_.name='SlashCommand'\n" +
            "  AND node0_.intent='Ello'\n" +
            "ORDER BY\n" +
            "  node0_.display_order asc;";
        
        Map<String, String> tableNames = QueryFormat.getTableNamesFromQuery(query);
        
        Assert.assertEquals(2, tableNames.size());
        Assert.assertNotNull(tableNames.get("node"));
        Assert.assertNotNull(tableNames.get("node_group"));
    }
}
