<!DOCTYPE html>     
<html>  
<head>  
    <style>
a.button4{
    display:inline-block;
    padding:0.3em 1.2em;
    margin:0 0.1em 0.1em 0;
    border:0.16em solid rgba(255,255,255,0);
    border-radius:2em;
    box-sizing: border-box;
    text-decoration:none;
    font-family:'Roboto',sans-serif;
    font-weight:300;
    color:#FFFFFF;
    text-shadow: 0 0.04em 0.04em rgba(0,0,0,0.35);
    text-align:center;
    transition: all 0.2s;
}
a.button4:hover{
    border-color: rgba(255,255,255,1);
}

    </style>
    <title>Query Optimizer API</title>  
</head>  
<h1 style="color: white">Query Optimizer API</h1>  
<body style="background-color: rgb(51, 49, 49); text-align: center;">
    <div>
        <a href="qo/testAPI" class="button4" style="background-color:#9a4ef1">Click here for Test API</a>  
    </div>
</body>
</html>
