<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>  
<html>  
<head>  
    <style>
a.button4{
    display:inline-block;
    padding:0.3em 1.2em;
    margin:0 0.1em 0.1em 0;
    border:0.16em solid rgba(255,255,255,0);
    border-radius:2em;
    box-sizing: border-box;
    text-decoration:none;
    font-family:'Roboto',sans-serif;
    font-weight:300;
    color:#FFFFFF;
    text-shadow: 0 0.04em 0.04em rgba(0,0,0,0.35);
    text-align:center;
    transition: all 0.2s;
}
a.button4:hover{
    border-color: rgba(255,255,255,1);
}
    </style>
    <title>Query Optimizer API</title>  
</head>  
<h1 style="color: white">Query Optimizer Result:</h1>  
<body style="background-color: rgb(51, 49, 49); text-align: center;">  
    <div style="text-align: center;">
        <div style="width: 50%; display: inline-block;">
            <table>
                <c:if test="${resultModel.hasIndexes}">
                    <tr>
                        <td style="font-size: 16px; text-align: left;">
                            <span style="font-weight: bold; color: #6aa84f">Recommending Index:</span>
                            <br>
                            <c:forEach var="index" items="${resultModel.indexes}">
                                <span style="font-size: 14px; font-weight: normal; color: #ffffff"><c:out value="${index}"/></span>
                                <br>                    
                            </c:forEach>
                        </td>
                    </tr>
                </c:if>
                    <tr>
                    <td style="font-size: 16px; color: #6aa84f; padding-top: 20px; text-align: left;">
                        <span style="font-weight: bold;">Recommendations:</span>
                        <br>
                        <c:forEach var="recommendation" items="${resultModel.recommendations}">
                            <span style="font-size: 14px; font-weight: normal; color: #ffffff"><c:out value="${recommendation.data}"/></span>
                            <br>
                            <c:forEach var="other" items="${recommendation.others}">
                                <span style="font-size: 14px; font-weight: bold; color: #ffffff"><c:out value="${other}"/></span>
                                <br>
                            </c:forEach>
                        </c:forEach>
                        <c:if test="${resultModel.noRecommendations}">

                            <c:if test="${resultModel.emptyWithOr}">
                                <span style="font-size: 14px; font-weight: normal; color: #ffffff">
                                1. The MySQL optimiser could not use any index because the outer logical<br>                        
                                operator is "OR" and having more than two filter conditions. You may get good performance if you<br>
                                rewrite two query without "OR" and merge result with "UNION" then check performance<br>
                                difference.  Please contact to SMS(SmartMySQL) support team if you have any doubts.
                                </span>
                            </c:if>

                            <c:if test="${!resultModel.isEmptyWithOr}">
                                <span style="font-size: 14px; font-weight: normal; color: #ffffff">
                                1. The SmartMySQL could not identified any improvements.<br>
                                Please contact to TopDBs DB experts support to tune this query. TopDBs DB expert can improve performance with 100% gurantee incase if they<br>
                                could not improve then amount will be return back to you. Please contact TopDBs DB expert support  www.topdbs.in/consultancyService
                                </span>
                            </c:if>
                        </c:if>
                    </td>
                </tr>

            </table>
        </div>
    </div>
    <br>
    <a class="button4" style="background-color:#9a4ef1" href="testAPI">Back</a>    
</body>
</html>
