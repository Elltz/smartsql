package np.com.ngopal.smart.sql.web;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@XmlRootElement
public class Recommendation {
    private String data;
    private List<String> others;

    public Recommendation() {
    }

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }

    public List<String> getOthers() {
        return others;
    }

    public void setOthers(List<String> others) {
        this.others = others;
    }
}
