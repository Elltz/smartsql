package np.com.ngopal.smart.sql.web;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@XmlRootElement
public class ResultModel {

    private List<String> indexes = new ArrayList<>();
    
    private List<Recommendation> recommendations = new ArrayList<>();
    
    private boolean hasIndexes = false;
    private boolean noRecommendations = true;
    private boolean emptyWithOr = false;
    
    
    public ResultModel() {
    }

    public List<String> getIndexes() {
        return indexes;
    }
    public void setIndexes(List<String> indexes) {
        this.indexes = indexes;
    }
    

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }
    public void setRecommendations(List<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public boolean isHasIndexes() {
        return hasIndexes;
    }
    public void setHasIndexes(boolean hasIndexes) {
        this.hasIndexes = hasIndexes;
    }

    public boolean isNoRecommendations() {
        return noRecommendations;
    }
    public void setNoRecommendations(boolean noRecommendations) {
        this.noRecommendations = noRecommendations;
    }

    public boolean isEmptyWithOr() {
        return emptyWithOr;
    }
    public void setEmptyWithOr(boolean emptyWithOr) {
        this.emptyWithOr = emptyWithOr;
    }
        
}
