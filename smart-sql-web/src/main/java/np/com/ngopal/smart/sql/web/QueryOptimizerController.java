package np.com.ngopal.smart.sql.web;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import np.com.ngopal.smart.sql.queryoptimizer.QueryOptimizer;
import np.com.ngopal.smart.sql.queryoptimizer.data.AnalyzerResult;
import np.com.ngopal.smart.sql.queryoptimizer.data.QOIndexRecommendation;
import np.com.ngopal.smart.sql.queryoptimizer.data.QORecommendation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */

@RequestMapping("/qo")
@Controller
@ControllerAdvice
public class QueryOptimizerController {
    
    @Autowired
    @Qualifier("testDataSource")
    DataSource testDataSource;

    @RequestMapping(value = "/qoResult", method = RequestMethod.POST)
    public String optimize(
            @ModelAttribute("optimizeModel") OptimizeModel res,
            BindingResult result, ModelMap model) {
        
        if (result.hasErrors()) {
            return "error";
        }
                
        if (res.getQuery() == null || res.getQuery().trim().isEmpty()) {
            return "optimize";
        }
        
        ResultModel resultModel = new ResultModel();
        
        try {
            
            QORecommendation recommendation = QueryOptimizer.analyzeQuery(
                new QODataProvider(testDataSource.getConnection(), res.getDatabase()),
                res.getDatabase(),
                res.getQuery()
            );
            
            if (recommendation != null) {
                
                resultModel.setHasIndexes(!isEmpty(recommendation));
                
                List<String> indexes = new ArrayList<>();
                resultModel.setIndexes(indexes);
                for (List<QOIndexRecommendation> list: recommendation.getIndexes().values()) {
                    for (QOIndexRecommendation rr: list) {
                        if (rr.getIndexQuery() != null) {
                            indexes.add(rr.getIndexQuery());
                        }
                    }
                }
                
                List<Recommendation> recommendations = new ArrayList<>();
                resultModel.setRecommendations(recommendations);
                
                boolean hasRecomendation = false;
                int i = 1;
                for (String query: recommendation.getRecommendations().keySet()) {
                    List<String> list = recommendation.getRecommendations().get(query);
                    if (list != null && !list.isEmpty()) {
                        for (String rr: list) {
                            
                            String[] r = rr.split("\n");                        
                            
                            Recommendation rec = new Recommendation();
                            recommendations.add(rec);
                            
                            rec.setData(i + ". " + r[0]);
                            
                            List<String> others = new ArrayList<>();
                            rec.setOthers(others);
                            for (int j = 1; j < r.length; j++) {
                                others.add(r[j]);
                            }
                            i++;
                        } 

                        hasRecomendation = true;
                    }
                }
                resultModel.setNoRecommendations(!hasRecomendation);
                
                
            } else {
                resultModel.setNoRecommendations(true);
                resultModel.setHasIndexes(false);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(QueryOptimizerController.class.getName()).log(Level.SEVERE, null, ex);
            return "error";
        }
        
        model.addAttribute("resultModel", resultModel);
        return "result";
    }
    
    
    @RequestMapping("/testAPI") 
    public String testAPI(Model model){                
        
        model.addAttribute("optimizeModel", new OptimizeModel());

        return "optimize";  
    }
    
    private boolean isEmpty(QORecommendation recommends) {
        boolean empty = true;
        for (List<String> list: recommends.getRecommendations().values()) {
            for (String s: list) {
                empty = s.isEmpty() || AnalyzerResult.NO_IMPROVEMENTS.equals(s) || AnalyzerResult.EMPTY_OR.equals(s);
                if (!empty) {
                    break;
                }
            }
        }
        
        if (empty) {
            if (!recommends.getIndexes().isEmpty()) {
                empty = false;
            }
        }
        
        return empty;
    }
}
