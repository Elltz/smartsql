package np.com.ngopal.smart.sql.web;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import np.com.ngopal.smart.sql.model.Advanced;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataType;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataTypes;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;
import np.com.ngopal.smart.sql.queryoptimizer.providers.impl.AbstractEmptyDataProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class QODataProvider extends AbstractEmptyDataProvider {

    public static final String COLUMN_NAME__DB         = "db_name";
    public static final String COLUMN_NAME__TABLE      = "table_name";
    public static final String COLUMN_NAME__COLUMN     = "column_name";
    public static final String COLUMN_NAME__INFO       = "info";
    public static final String COLUMN_NAME__TYPE       = "type";
    public static final String COLUMN_NAME__COLUMNS    = "columns";
    public static final String COLUMN_NAME__INDEXES    = "indexes";
    public static final String COLUMN_NAME__FOREIGNS   = "foreigns";
    public static final String COLUMN_NAME__TABLES     = "tables";
    public static final String COLUMN_NAME__VIEWS      = "views";
    public static final String COLUMN_NAME__PROCEDURES = "procedures";
    public static final String COLUMN_NAME__FUNCTIONS  = "functions";
    public static final String COLUMN_NAME__TRIGGERS   = "triggers";
    public static final String COLUMN_NAME__EVENTS     = "events";
    
    private final Map<String, Map<String, Database>> databasesCache = new HashMap<>();
    private final Map<String, Map<String, Map<String, DBTable>>> tablesCache = new HashMap<>();
    
    private static final Pattern PATTERN__FOREIGN_KEY = Pattern.compile("CONSTRAINT `(?<name>.*?)` FOREIGN KEY \\(`(?<columns>.*?)`\\) REFERENCES `(?<refTable>.*?)`\\(`(?<refColumns>.*?)`\\)(?<updatCascade> ON UPDATE CASCADE)?(?<updateRestrict> ON UPDATE RESTRICT)?(?<deleteCascade> ON DELETE CASCADE)?(?<deleteRstrict> ON DELETE RESTRICT)?");
    
    private static final String PARAMS_KEY = "webTestParamsKey";
    
    private int mySQLVersionMaj = 0;
    private int mySQLVersionMin = 0;
    private int mySQLVersionMic = 0;
    
    private Connection connection;
    
    public QODataProvider(Connection connection, String database) throws SQLException {
        this.connection = connection;
        
        // get version
        try (ResultSet rs = (ResultSet) execute(QueryProvider.getInstance().getQuery(
                QueryProvider.DB__MYSQL, QueryProvider.QUERY__MYSQL_VERSION))) {

            if (rs.next()) {

                String s = rs.getString(1);

                if (s.contains("-")) {
                    s = s.split("-")[0];
                }

                String[] ver = s.split("\\.");
                if (ver.length > 0) {
                    try {mySQLVersionMaj = Integer.valueOf(ver[0]);} catch (NumberFormatException ex) {}                                
                } 

                if (ver.length > 1) {
                    try {mySQLVersionMin = Integer.valueOf(ver[1]);} catch (NumberFormatException ex) {}                                
                } 

                if (ver.length > 2) {
                    try {mySQLVersionMic = Integer.valueOf(ver[2]);} catch (NumberFormatException ex) {}                                
                }                  
            }
        }        
        
        // set database
        execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__CHANGE_DATABASE), database));
        
        // read database structure
        Map<String, Object> map = loadDBStructure(database);
        loadDatabase(database, map);
    }
        
    @Override
    public Object execute(String sql) throws SQLException {
        return connection.createStatement().executeQuery(sql);
    }

    @Override
    public Object execute(String sql, QueryResult result) throws SQLException {
        
        if (result != null) {
            result.setStartDuration(System.currentTimeMillis());
            result.setQuery(sql);
        }

        Statement stmt = connection.createStatement();
        boolean executeIndicator = stmt.execute(sql, Statement.RETURN_GENERATED_KEYS);
        if (result != null) {
            result.setExecutionDuration(System.currentTimeMillis());
        }

        Object res;
        if (executeIndicator) {

            res = stmt.getResultSet();
            if (result != null) {

                result.addResult(res);
                while (true) {
                    if (stmt.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                        result.addResult(stmt.getResultSet());
                    } else if (stmt.getUpdateCount() >= 0){
                        result.addResult(stmt.getUpdateCount());
                    } else {
                        break;
                    }
                }

                result.setEndDuration(System.currentTimeMillis());
                result.setResultSet(true);
            }
        } else {
            res = stmt.getUpdateCount();
            if (result != null) {
                result.addResult(res);

                while (true) {
                    if (stmt.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                        result.addResult(stmt.getResultSet());
                    } else if (stmt.getUpdateCount() >= 0){
                        result.addResult(stmt.getUpdateCount());
                    } else {
                        break;
                    }
                }
            }          
        }

        return res;
    }

    @Override
    public DBTable getCachedTable(String database, String table) {
        Map<String, Map<String, DBTable>> m = tablesCache.get(PARAMS_KEY);
        
        if (m != null) {
            Map<String, DBTable> map = m.get(database.toLowerCase());
            
            if (map != null && map.containsKey(table.toLowerCase())) {
                DBTable t = new DBTable();
                DBTable t0 = map.get(table.toLowerCase());
                t.copy(t0);
                if (t0.getColumns() != null) {
                    for (Column c: t0.getColumns()) {
                        Column c0 = new Column();
                        c0.copy(c);
                        t.addColumn(c0);
                    }
                }
                return t;
            }
        }
        
        return null;
    }

    @Override
    public Database getCachedDatabase(String database) {
        Map<String, Database> m = databasesCache.get(PARAMS_KEY);
        return m != null ? m.get(database.toLowerCase()) : null;
    }

    @Override
    public boolean canExplainExtended() {
        return mySQLVersionMaj <= 5;
    }

    @Override
    public boolean hasCredits() {
        return true;
    }
    
    
    public Map<String, Object> loadDBStructure(String database) throws SQLException {
        
        Map<String, Object> databaseData = new LinkedHashMap<>();
        
        databaseData.put(COLUMN_NAME__TABLES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__VIEWS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__PROCEDURES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__FUNCTIONS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__TRIGGERS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__EVENTS, new LinkedHashMap<>());
                
        boolean oldVersion = false;
        try {
            oldVersion = connection.getMetaData().getDatabaseProductVersion().startsWith("5.0");
        } catch (SQLException ex) {
            Logger.getLogger(QODataProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASE_INFO_5_0);
        if (!oldVersion) {
            query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASE_INFO);
        }
        
        String multiQuery = "SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';";
        multiQuery += String.format(query, database, database, database, database, database, database, database, database, database);
        
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery = multiQuery + ";";
        }
        
        if (!oldVersion) {
            multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_EVENTS), database);
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_FUNCTIONS), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_STORED_PROCEDURES), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_TRIGGERS), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_VIEWS), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }
        
        multiQuery += "SET SQL_MODE=@OLD_SQL_MODE;";
                
        try {
            QueryResult qr = new QueryResult();
            execute(multiQuery, qr);
            
            int i = 1;
            ResultSet rs = (ResultSet) qr.getResult().get(i);
            i++;
            boolean load = false;
            if (rs != null) {
                while (rs.next()) {
                    loadDatabaseInfo(rs, database, databaseData);
                    load = true;
                }
                
                rs.close();
            }
            
            if (!oldVersion) {
                rs = (ResultSet) qr.getResult().get(i);
                i++;
                while (rs.next()) {
                    String eventName = rs.getString(1);
                    Map<String, Map<String, List<String>>> events = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__EVENTS);
                    Map<String, List<String>> event = events.get(eventName);
                    if (event == null) {
                        event = new LinkedHashMap<>();
                        event.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                        events.put(eventName, event);
                    }
                }
                rs.close();
            }

            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String functionName = rs.getString(1);
                Map<String, Map<String, List<String>>> functions = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__FUNCTIONS);
                Map<String, List<String>> function = functions.get(functionName);
                if (function == null) {
                    function = new LinkedHashMap<>();
                    function.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    functions.put(functionName, function);
                }
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String tableName = rs.getString(1);
                Map<String, Map<String, List<String>>> propcedures = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__PROCEDURES);
                Map<String, List<String>> propcedure = propcedures.get(tableName);
                if (propcedure == null) {
                    propcedure = new LinkedHashMap<>();
                    propcedure.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    propcedures.put(tableName, propcedure);
                }
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String triggerName = rs.getString(1);
                Map<String, Map<String, List<String>>> triggers = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__TRIGGERS);
                Map<String, List<String>> trigger = triggers.get(triggerName);
                if (trigger == null) {
                    trigger = new LinkedHashMap<>();
                    trigger.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    triggers.put(triggerName, trigger);
                }
            }
            rs.close();
            
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String viewName = rs.getString(1);
                Map<String, Map<String, List<String>>> views = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__VIEWS);
                Map<String, List<String>> view = views.get(viewName);
                if (view == null) {
                    view = new LinkedHashMap<>();
                    view.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    views.put(viewName, view);
                }
            }
            rs.close();
            
            if (!load) {
                return null;
            }
            
        } catch (SQLException ex) {
            databaseData = null;
            throw ex;
        }
        
        return databaseData;
    }
    

    private void loadDatabaseInfo(ResultSet rs, String databaseName, Map<String, Object> databaseData) throws SQLException {
        String tableName = rs.getString(COLUMN_NAME__TABLE);
        String columnName = rs.getString(COLUMN_NAME__COLUMN);

        int type = rs.getInt(COLUMN_NAME__TYPE);

        //log.debug("001 - " + type);
        switch (type) {                            
            case 1:
            case 2:    
            case 8:
            case 9:

                Map<String, Map<String, List<String>>> tables = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__TABLES);
                Map<String, List<String>> table = tables.get(tableName);
                if (table == null) {
                    table = new LinkedHashMap<>();
                    table.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());
                    table.put(COLUMN_NAME__INDEXES, new ArrayList<String>());
                    table.put(COLUMN_NAME__FOREIGNS, new ArrayList<String>());
                    table.put(COLUMN_NAME__INFO, new ArrayList<String>());

                    tables.put(tableName, table);
                }

                switch (type) {
                    case 1:
                        table.get(COLUMN_NAME__COLUMNS).add(columnName);
                        break;

                    case 2:
                        table.get(COLUMN_NAME__INDEXES).add(columnName);
                        break;

                    case 8:
                        table.get(COLUMN_NAME__FOREIGNS).add(columnName);
                        break;

                    case 9:
                        table.get(COLUMN_NAME__INFO).add(columnName);
                        break;
                }
                break;

            case 3:                            
                Map<String, Map<String, List<String>>> views = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__VIEWS);
                Map<String, List<String>> view = views.get(tableName);
                if (view == null) {
                    view = new LinkedHashMap<>();
                    view.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    views.put(tableName, view);
                }

                view.get(COLUMN_NAME__COLUMNS).add(columnName);
                break;

            case 4:
                Map<String, Map<String, List<String>>> propcedures = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__PROCEDURES);
                Map<String, List<String>> propcedure = propcedures.get(tableName);
                if (propcedure == null) {
                    propcedure = new LinkedHashMap<>();
                    propcedure.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    propcedures.put(tableName, propcedure);
                }
                break;

            case 5:            
                Map<String, Map<String, List<String>>> functions = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__FUNCTIONS);
                Map<String, List<String>> function = functions.get(tableName);
                if (function == null) {
                    function = new LinkedHashMap<>();
                    function.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    functions.put(tableName, function);
                }
                break;

            case 6:    
                Map<String, Map<String, List<String>>> triggers = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__TRIGGERS);
                Map<String, List<String>> trigger = triggers.get(tableName);
                if (trigger == null) {
                    trigger = new LinkedHashMap<>();
                    trigger.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    triggers.put(tableName, trigger);
                }

                trigger.get(COLUMN_NAME__COLUMNS).add(columnName);
                break;

            case 7:  
                Map<String, Map<String, List<String>>> events = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__EVENTS);
                Map<String, List<String>> event = events.get(tableName);
                if (event == null) {
                    event = new LinkedHashMap<>();
                    event.put(COLUMN_NAME__COLUMNS, new ArrayList<String>());

                    events.put(tableName, event);
                }

                event.get(COLUMN_NAME__COLUMNS).add(columnName);
                break;

        }        
    }
    
    
    public void loadDatabase(String database, Map<String, Object> databaseStructure) {
        
        //log.debug("Start caching database");
        if (databaseStructure != null) {
            Database databaseRef =  new Database();
            databaseRef.setName(database);
//            databaseRef.setParams(params);

            Map<String, Database> map = databasesCache.get(PARAMS_KEY);
            if (map == null) {
                map = new LinkedHashMap<>();
                databasesCache.put(PARAMS_KEY, map);
            }

            map.put(database.toLowerCase(), databaseRef);

            Map<String, Map<String, List<String>>> tables = (Map<String, Map<String, List<String>>>) databaseStructure.get(COLUMN_NAME__TABLES);
            if (tables != null) {

                Map<String, Map<String, DBTable>> dbMap = tablesCache.get(PARAMS_KEY);
                if (dbMap == null) {
                    dbMap = new LinkedHashMap<>();
                    tablesCache.put(PARAMS_KEY, dbMap);
                }

                Map<String, DBTable> tableMap = dbMap.get(database.toLowerCase());
                if (tableMap == null) {
                    tableMap = new LinkedHashMap<>();
                    dbMap.put(database.toLowerCase(), tableMap);
                }

                List<DBTable> dbtables = new ArrayList<>();
                for (String table: tables.keySet()) {

                    DBTable tableRef = new DBTable();
                    tableRef.setName(table);
                    tableRef.setDatabase(databaseRef);

                    dbtables.add(tableRef);

                    tableMap.put(table.toLowerCase(), tableRef);

                    Map<String, List<String>> tableData = tables.get(table);

                    List<String> infoList = tableData.get(COLUMN_NAME__INFO);
                    if (infoList != null && !infoList.isEmpty()) {
                        String[] info = infoList.get(0).split("\\$\\$\\$\\$");

                        tableRef.setEngineType(new MySQLEngineTypes().getTypeByName(info[0]));
                        tableRef.setCharacterSet(info[1]);
                        tableRef.setCollation(info[2]);
                        if (tableRef.getAdvanced() == null) {
                            tableRef.setAdvanced(new Advanced());
                        }
                        tableRef.getAdvanced().setAvgRowLength(Integer.valueOf(info[4]));
                    }


                    List<String> columns = tableData.get(COLUMN_NAME__COLUMNS);
                    if (columns != null) {

                        int i = 0;
                        List<Column> tableColumns = new ArrayList<>();
                        for (String column: columns) {

                            Column columnRef = new Column();
                            columnRef.setTable(tableRef);

                            columnRef.setOrderIndex(i);
                            i++;

                            String[] columnInfo = column.split("\\$\\$\\$\\$");
                            int length = columnInfo.length;

                            if (length > 0) {
                                columnRef.setName(columnInfo[0]);
                                columnRef.setOriginalName(columnInfo[0]);
                            }

                            if (length > 1) {
                                columnRef.setDataType(getDatatype(columnInfo[1]));
                                String[] strs = columnInfo[1].split(" ");
                                if (strs.length > 1 && "unsigned".equalsIgnoreCase(strs[strs.length - 1])) {
                                    columnRef.setUnsigned(true);
                                }
                            }

                            if (length > 2) {
                                columnRef.setNotNull("NOT NULL".equalsIgnoreCase(columnInfo[2]));
                            }

                            if (length > 3) {
                                columnRef.setAutoIncrement("auto_increment".equalsIgnoreCase(columnInfo[3].trim()));
                            }

                            if (length > 4) {
                                columnRef.setDefaults(columnInfo[4]);
                            }

                            if (length > 5) {
                                columnRef.setCharacterSet(columnInfo[5]);
                            }

                            if (length > 6) {
                                columnRef.setCollation(columnInfo[6]);
                            }

                            if (length > 7) {
                                columnRef.setComment(columnInfo[7]);
                            }

                            tableColumns.add(columnRef);
                        }

                        tableRef.setColumns(tableColumns);
                    }

                    List<String> indexes = tableData.get(COLUMN_NAME__INDEXES);
                    if (indexes != null) {

                        List<Index> tableIndexes = new ArrayList<>();
                        for (String index: indexes) {

                            // its just an index
                            if (index.startsWith("BTREE ")) {
                                index = index.substring(6);

                            }

                            boolean primary = index.startsWith("PRIMARY(");

                            Index indexRef = new Index();
                            indexRef.setTable(tableRef);
                            indexRef.setType(primary ? IndexType.PRIMARY : null);

                            if (primary) {

                                indexRef.setName("PRIMARY");

                                String primaryName = index.substring(8, index.length() - 1);
                                String[] cols = primaryName.split(",");
                                Column[] indexColumns = new Column[cols.length];
                                int i = 0;
                                for (String c: cols) {
                                    
                                    if (c.contains("(")) {
                                        String strLength = c.substring(c.indexOf("(") + 1, c.indexOf(")"));
                                        
                                        c = c.substring(0, c.indexOf("(")).trim();
                                        
                                        try {
                                            indexRef.setColumnLength(c, Integer.valueOf(strLength));
                                        } catch (NumberFormatException ex) {
                                            Logger.getLogger(QODataProvider.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    
                                    Column primaryColumn = tableRef.getColumnByName(c.trim());

                                    if (primaryColumn != null) {
                                        primaryColumn.setPrimaryKey(true);
                                    }
                                    indexColumns[i] = primaryColumn;
                                    i++;
                                }

                                indexRef.setColumns(Arrays.asList(indexColumns));

                            } else {

                                if (index.startsWith("FULLTEXT ")) {
                                    indexRef.setType(IndexType.FULL_TEXT);
                                    index = index.substring(9);
                                } else if (index.startsWith("SPATIAL ")) {
                                    indexRef.setType(IndexType.SPATIAL);
                                    index = index.substring(8);
                                } else if (index.startsWith("UNIQUE ")) {
                                    indexRef.setType(IndexType.UNIQUE_TEXT);
                                    index = index.substring(7);
                                }

                                int startIndex = index.indexOf("(");
                                indexRef.setName(index.substring(0, startIndex));


                                String[] idxColumns = index.substring(startIndex + 1, index.length() - 1).split(",");
                                if (idxColumns != null) {

                                    List<Column> columnsList = new ArrayList<>();
                                    for (String idxColumn: idxColumns) {
                                        
                                        if (idxColumn.contains("(")) {
                                            String strLength = idxColumn.substring(idxColumn.indexOf("(") + 1, idxColumn.indexOf(")"));

                                            idxColumn = idxColumn.substring(0, idxColumn.indexOf("(")).trim();

                                            try {
                                                indexRef.setColumnLength(idxColumn, Integer.valueOf(strLength));
                                            } catch (NumberFormatException ex) {
                                                Logger.getLogger(QODataProvider.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }
                                        
                                        Column c = tableRef.getColumnByName(idxColumn.trim());
                                        if (c != null) {
                                            columnsList.add(c);
                                        }
                                    }

                                    indexRef.setColumns(columnsList);
                                }
                            }
                            if (primary) {
                                tableIndexes.add(0, indexRef);
                            } else {
                                tableIndexes.add(indexRef);
                            }
                        }

                        tableRef.setIndexes(tableIndexes);
                    }
                }

                // After collecting all tables - need collect foreign keys
                for (String table: tables.keySet()) {

                    Map<String, List<String>> tableData = tables.get(table);

                    DBTable sourceTable = tableMap.get(table.toLowerCase());    

                    List<String> foreignkeys = tableData.get(COLUMN_NAME__FOREIGNS);
                    if (foreignkeys != null) {

                        List<ForeignKey> tableForeignKeys = new ArrayList<>();
                        for (String foreignkey: foreignkeys) {

                            Matcher m = PATTERN__FOREIGN_KEY.matcher(foreignkey);
                            if (m.find()) {

                                ForeignKey fk = new ForeignKey();
                                fk.setReferenceDatabase(database);
                                fk.setName(m.group("name"));

                                String[] cols = m.group("columns").split(",");
                                Column[] fkColumns = new Column[cols.length];
                                int i = 0;
                                for (String c: cols) {
                                    fkColumns[i] = sourceTable.getColumnByName(c.trim());
                                    i++;
                                }

                                fk.setColumns(Arrays.asList(fkColumns));

                                String refTableName = m.group("refTable").toLowerCase();
                                DBTable refTable = tableMap.get(refTableName);                            
                                fk.setReferenceTable(refTableName);


                                String[] refCols = m.group("refColumns").split(",");
                                Column[] fkRefColumns = new Column[refCols.length];
                                i = 0;
                                for (String c: refCols) {
                                    Column col = refTable != null ? refTable.getColumnByName(c.trim()) : null;
                                    if (col == null) {
                                        col = new Column();
                                        col.setName(c.trim());
                                        col.setOriginalName(c.trim());
                                    }

                                    fkRefColumns[i] = col;
                                    i++;
                                }
                                fk.setReferenceColumns(Arrays.asList(fkRefColumns));

                                tableForeignKeys.add(fk);
                            } else {
                                Logger.getLogger(QODataProvider.class.getName()).log(Level.WARNING, "CAN NOT PARSE FOREIGN KEY - need check!!!: " + foreignkey);
                            }                        
                        }

                        sourceTable.setForeignKeys(tableForeignKeys);
                    }
                }

                databaseRef.setTables(dbtables);
            }
        }
    }
    
    public DataType getDatatype(String type) {
        Integer size = null;
        Integer precision = null;
        Set elements = null;

        if (type != null && type.contains("(")) {
            if (type.toLowerCase().startsWith("set") || type.toLowerCase().startsWith("enum")) {
                String metric = type.substring(type.indexOf("(") + 1, type.lastIndexOf(")"));
                String[] ele = (metric.split(","));
                elements = new TreeSet<>();
                for (String s: ele) {
                    s = s.trim();
                    elements.add(s.startsWith("'") && s.endsWith("'") ? s.substring(1, s.length() - 1) : s);
                }
            } else if (type.contains(",")) {
                String metric = type.substring(type.indexOf("(") + 1, type.lastIndexOf(")"));
                size = Integer.parseInt(metric.split(",")[0].trim());
                precision = Integer.parseInt(metric.split(",")[1].trim());
            } else {
                size = Integer.parseInt(type.substring(type.indexOf("(") + 1, type.lastIndexOf(")")).trim());
            }

        }
        
        DataType dataType = null;
        
        if (type != null) {
            
            MySQLDataTypes types = new MySQLDataTypes();
                         
            type = type.substring(0, (type.contains("(") ? type.indexOf("(") : type.length())).trim();
            if (type.contains(" ")) {
                type = type.split(" ")[0].trim();
            }
            dataType = types.getTypeByName(type);
            if (dataType != null) {
                ((MySQLDataType)dataType).setLength(size);
                ((MySQLDataType)dataType).setPrecision(precision);
                ((MySQLDataType)dataType).setList(elements);
            }
        }
        
        return dataType;
    }
}
