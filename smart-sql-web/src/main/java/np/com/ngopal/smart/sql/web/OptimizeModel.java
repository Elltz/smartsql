package np.com.ngopal.smart.sql.web;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@XmlRootElement
public class OptimizeModel {

    private String database = "smartmysql_testing";
    private String query = "SELECT smartmysql_tasks.frDueBy,smartmysql_tasks.source,\n" +
"smartmysql_tasks.group_id,smartmysql_tasks.isescalated, smartmysql_tasks.type,smartmysql_tasks.email_config_id,\n" +
"smartmysql_tasks.owner_id, smartmysql_tasks.association_type\n" +
"FROM\n" +
"  smartmysql_tasks\n" +
"WHERE\n" +
"  smartmysql_tasks.client_id = 206014\n" +
"    AND\n" +
"  ( smartmysql_tasks.created_at > ''\n" +
"    AND\n" +
"  smartmysql_tasks.status\n" +
"        IN  ('3','7')\n" +
"    AND\n" +
"  smartmysql_tasks.spam = 0\n" +
"    AND\n" +
"  smartmysql_tasks.deleted = 0 )\n" +
"ORDER BY\n" +
"  smartmysql_tasks.group_id DESC LIMIT 30;";
    
    public OptimizeModel() {
    }

    public String getDatabase() {
        return database;
    }
    public void setDatabase(String database) {
        this.database = database;
    }
    
    

    public String getQuery() {
        return query;
    }
    public void setQuery(String query) {
        this.query = query;
    }
    
    
}
