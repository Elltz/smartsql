package np.com.ngopal.smart.sql.debuganalyzer.controller;

import com.google.common.base.Objects;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.AppSettingsService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.debuganalyzer.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.TechnologyType;
import np.com.ngopal.smart.sql.model.provider.ConnectionParamsProvider;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.modules.ConnectionModuleInterface;
import np.com.ngopal.smart.sql.structure.modules.ProfilerModule;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class NewConnectionController extends BaseController implements
        Initializable {
    
    @FXML
    private TabPane tabPane;

    @FXML
    private Tab mySqlTab;
    @FXML
    private Tab httpTab;
    @FXML
    private Tab sshTab;
    @FXML
    private Tab osTab;
    @FXML
    private Tab sslTab;
    @FXML
    private Tab advancedTab;

    @FXML
    private BorderPane paramBorderPane;

    @FXML
    private BorderPane sshParamBorderPane;

    @FXML
    private Button deleteBttn;

    @FXML
    private TextField database;

    @FXML
    private TextField address;

    @FXML
    private Button cloneBttn;

    @FXML
    private HBox compressProtocolPane;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private PasswordField pass;

    @FXML
    private TextField port;

    @FXML
    private TitledPane compressedTitledPane;

    @FXML
    private CheckBox useSshTunnelingCheckBox;
    @FXML
    private GridPane sshTunnelingPane;
    @FXML
    private TextField sshHost;
    @FXML
    private TextField sshPort;
    @FXML
    private TextField sshUser;
    @FXML
    private TextField sshPassword;
    @FXML
    private CheckBox sshSavePasswordCheckBox;
    @FXML
    private Label sshPasswordLabel;
    @FXML
    private RadioButton sshPasswordTypeRadioButton;
    @FXML
    private RadioButton sshPrivateKeyTypeRadioButton;
    @FXML
    private TextField sshPrivateKeyField;
    @FXML
    private Label sshPrivateKeyLabel;
    @FXML
    private Button sshOpenPrivateKeyButton;

    @FXML
    private Button connectBttn;
    @FXML
    private Button testBttn;
    @FXML
    private Button cancelBttn;

    @FXML
    private Button saveBttn;

    @FXML
    private Button renameBttn;

    @FXML
    private TextField user;

    @FXML
    private Button newBttn;

    @FXML
    private CheckBox savePasswordCheckBox;

    @FXML
    private TextField sessionIdleField;
    @FXML
    private RadioButton sessionIdleDefault;
    @FXML
    private RadioButton sessionIdleRadioButton;

    @FXML
    private CheckBox useCompressedProtocolCheckBox;

    @FXML
    private ComboBox<ConnectionParams> savedConnectionComboBox;

    private ObservableList<ConnectionParams> connectionParamsList;

    private ObjectProperty<ConnectionParams> selectedConnParam;

    private boolean hasChanges = false;
    
    private boolean cancelable = true;

    @FXML
    public void connectBttnAction(ActionEvent event) {
        if (validateFields()) {
            makeBusy(true);
            Platform.runLater(() -> {
                try {
                    if (selectedConnParam.get() != null) {

                        if (hasChanges) {

                            DialogResponce dr = DialogHelper.showConfirm(getController(), "Changes Action", "Would you like to save your new changes?", getStage(), false);
                            if (dr == DialogResponce.OK_YES) {

                                Long id = saveConnection();
                                if (id == null) {
                                    DialogHelper.showError(getController(), "SAVE ERROR", "Could not save detected changes \n Please Try again with the save button", null, getStage());
                                    return;
                                }
                            } else if (dr == DialogResponce.CANCEL) {
                                return;
                            }
                        }

                        WorkProc<ConnectionParams> openConnectProc = new WorkProc<ConnectionParams>() {
                            @Override
                            public void updateUI() { }

                            @Override
                            public void run() {
                                try{                                    
                                    makeBusy(true);
                                    ((MainController) getController()).createNewConnectionSession(selectedConnParam.get());
                                }catch(Exception e){
                                    e.printStackTrace();
                                    DialogHelper.showError(getController(), "ERROR OPENNING CONNECTION", e.getMessage(), e);
                                }finally{
                                    makeBusy(false);                                    
                                }
                            }
                        };

                        Recycler.addWorkProc(openConnectProc);
                    }
                } finally {
                    makeBusy(false);
                }
            });
        }
    }

    @FXML
    void testBttnAction(ActionEvent event) {

        if (validateFields()) {
            makeBusy(true);
            if (hasChanges) {
                DialogResponce dr = DialogHelper.showConfirm(getController(),
                        "Changes Action", "Would you like to save your new changes?", getStage(), false);
                if (dr == DialogResponce.OK_YES) {
                    saveConnection();
                } else if (dr == DialogResponce.CANCEL) {
                    makeBusy(false);
                    return;
                }
            }

            WorkProc<ConnectionParams> testConnection = new WorkProc<ConnectionParams>() {

                SQLException exception;
                String productInfo;

                BigDecimal totalMemory;
                BigDecimal totalDisk;

                @Override
                public void updateUI() {
                    if (exception != null) {
                        DialogHelper.showError(getController(), "Error", "Error on test", exception, getStage());
                    } else {
                        DialogHelper.showInfo(getController(), "Info", "Connetion successful!\n" + productInfo, null, getStage());
                    }
                    makeBusy(false);
                }

                @Override
                public void run() {
                    try {
                        productInfo = ServiceEntityProviders.getInstance().getGeneralMysqlDBService().testConnection(selectedConnParam.get());
                    } catch (SQLException ex) {
                        exception = ex;
                    }
                    callUpdate = true;
                }
            };
            Recycler.addWorkProc(testConnection);
        }
    }

    public Long saveConnection() {
        if (selectedConnParam.get() != null) {
            if (!savePasswordCheckBox.isSelected()) {
                selectedConnParam.get().setPassword("");
            }

            if (!sshSavePasswordCheckBox.isSelected()) {
                selectedConnParam.get().setSshPassword("");
            }

            setHasChanges(false);

            /**
             * here we are trying to get the new changes into the reference we
             * hold
             */
            ConnectionParams connectionParams = ServiceEntityProviders.getInstance().getDb().save(selectedConnParam.get());
            //selectedConnParam.set(connectionParams);
            //we are re-selecting an already selected item in the combo box because it is binded 
            // to the selectionConnParam
            savedConnectionComboBox.getSelectionModel().select(null);//trying to trigger the changeListener
            savedConnectionComboBox.getSelectionModel().select(connectionParams);

            /**
             * here we change the connectionParam that is stored with the
             * currently selected connectionSession. we check if its null first,
             * incase of user first creating for the first time he use app.
             */
            // This is very very bad idea, we didnt check that current connection is edited
            // !!!!! DO NOT UNCOMMENT WITHOUT REFACTORING
//            if (controller.getSelectedConnectionSession().get() != null) {
//                controller.getSelectedConnectionSession().get().setConnectionParam(connectionParams);
//            }

            return connectionParams.getId();
        }
        return null;
    }

    @FXML
    void saveActionBttn(ActionEvent event) {
        if (saveConnection() != null) {
            DialogHelper.showInfo(getController(), "Info", "Successfully saved", null, getStage());
        }
    }

    @FXML
    void newBttnAction(ActionEvent event) {

        String result = DialogHelper.showInput(getController(), "New Connection", "Enter New Connection Name", "Name:", "", getStage());

        if (result != null && !result.isEmpty()) {

            if (ServiceEntityProviders.getInstance().getDb().checkConnectionParamsName(result)) {
                DialogHelper.showError(getController(), "Error", "the name already exists! Please enter other name!", null, getStage());
                return;
            }

            ConnectionParams p = ServiceEntityProviders.getInstance().getNewConnectionParams();
            p.setName(result);
            addConnectionNameList(p);
            comboBoxChangeSelection(p);
        }

    }

    @FXML
    void openPrivateKey(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PuTTY Private Key Files", "*.ppk"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showOpenDialog(getStage());
        if (file != null && sshTab.isSelected()) {
            sshPrivateKeyField.setText(file.getAbsolutePath());
        }
    }

    private boolean validateFields() {
        ConnectionParams params = selectedConnParam.get();
        if (params != null) {

            // for now check only Connection and SSH Module
            if (params.getSelectedModuleClassName() == null || "np.com.ngopal.smart.sql.core.modules.ConnectionModule".equals(params.getSelectedModuleClassName())) {
                if (isStringEmpty(params.getAddress())) {
                    DialogHelper.showError(getController(), "Error", "Please enter Host Address", null, getStage());
                    return false;
                }

                boolean sshEnabled = params.isUseSshTunneling();//!isStringEmpty(params.getSshHost()) || !isStringEmpty(params.getSshUser()) || !isStringEmpty(params.getSshPassword());
                if (sshEnabled) {
                    if (isStringEmpty(params.getSshHost())) {
                        DialogHelper.showError(getController(), "Error", "Please enter SSH Host", null, getStage());
                        return false;
                    }

                    if (isStringEmpty(params.getSshUser())) {
                        DialogHelper.showError(getController(), "Error", "Please enter SSH User", null, getStage());
                        return false;
                    }

                    if (!params.isUseSshPrivateKey() && isStringEmpty(params.getSshPassword())) {
                        DialogHelper.showError(getController(), "Error", "Please enter SSH Password", null, getStage());
                        return false;
                    }
                }
            } else if ("np.com.ngopal.smart.sql.modules.SSHModule".equals(params.getSelectedModuleClassName())) {
                if (isStringEmpty(params.getSshHost())) {
                    DialogHelper.showError(getController(), "Error", "Please enter SSH Host", null, getStage());
                    return false;
                }

                if (isStringEmpty(params.getSshUser())) {
                    DialogHelper.showError(getController(), "Error", "Please enter SSH User", null, getStage());
                    return false;
                }

                if (isStringEmpty(params.getSshPassword())) {
                    DialogHelper.showError(getController(), "Error", "Please enter SSH Password", null, getStage());
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private boolean isStringEmpty(String text) {
        return text == null || text.isEmpty();
    }

    @FXML
    void cloneBttnAction(ActionEvent event) {

        ConnectionParams connectionParams = selectedConnParam.get();
        if (connectionParams != null) {

            String result = DialogHelper.showInput(getController(), "New Connection", "Enter New Connection Name", "Name:", connectionParams.getName() + "_New", getStage());
            if (result != null && !result.isEmpty()) {

                if (ServiceEntityProviders.getInstance().getDb().checkConnectionParamsName(result)) {
                    DialogHelper.showError(getController(), "Error", "the name already exists! Please enter other name!!", null, getStage());
                    return;
                }

                ConnectionParams p = connectionParams.copyOnlyParams();
                p.setName(result);

                addConnectionNameList(p);
                comboBoxChangeSelection(p);
            }
        }
    }

    @FXML
    void renameBttnAction(ActionEvent event) {

        ConnectionParams params = selectedConnParam.get();
        if (params != null) {
            String result = DialogHelper.showInput(getController(), "Rename Connection", "Enter New Connection Name", "Name:", params.getName(), getStage());
            if (result != null && !result.isEmpty()) {

                ConnectionParams cp = ServiceEntityProviders.getInstance().getDb().findConenctionParamsByName(result);
                if (cp != null && !cp.getId().equals(params.getId())) {
                    DialogHelper.showError(getController(), "Error", "the name already exists! Please enter other name!!", null, getStage());
                    return;
                }

                params.setName(result);
                //db.save(params);

                int index = connectionParamsList.indexOf(params);
                connectionParamsList.remove(params);
                connectionParamsList.add(index, params);
                Collections.sort(connectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
                    return o1.getName().compareTo(o2.getName());
                });

                comboBoxChangeSelection(params);
            }
        }
    }

    @FXML
    void deleteBttnAction(ActionEvent event) {

        DialogResponce result = DialogHelper.showConfirm(getController(), "Delete connection", "Do you realy want to delete the connection detail?", getStage(), false);

        if (result == DialogResponce.OK_YES) {
            ConnectionParams params = selectedConnParam.get();
            if (params != null && ServiceEntityProviders.getInstance().getDb().delete(params)) {
                connectionParamsList.remove(params);
                DialogHelper.showInfo(getController(), "Info", "Successfully deleted '" + params.getName() + "'", null, getStage());
            }
        }
    }

    @FXML
    void cancelBttnAction(ActionEvent event) {
        if(cancelable){
            getStage().close();
        }else{
            DialogHelper.showError(getController(), "CONNECTION ALERT", "You need to create connection", null, getStage());
        }
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        ((ImageView)cancelBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCancelButton_image());
        ((ImageView)testBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getTestConnectionButton_image());
        ((ImageView)connectBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getConnectionButton_image());
        ((ImageView)deleteBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getDeleteButton_image());
        ((ImageView)renameBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRenameButton_image());
        ((ImageView)saveBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getSaveButton_image());
        ((ImageView)cloneBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCloneButton_image());
        ((ImageView)newBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getNewButton_image());
        
        tabPane.getTabs().remove(osTab);
        
        init();

        compressedTitledPane.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            getStage().setHeight(getStage().getHeight() + (newValue != null && newValue ? 30 : - 30));
        });

        compressProtocolPane.managedProperty().bind(compressProtocolPane.visibleProperty());
        compressProtocolPane.visibleProperty().bind(useCompressedProtocolCheckBox.selectedProperty());

        ConnectionParams selected = null;
        String value = ServiceEntityProviders.getInstance().getSettignsService()
                .getSettingValue(AppSettingsService.SETTING_NAME__LAST_OPENED_CONNECTION);
        if (value != null) {
            try {
                selected = ServiceEntityProviders.getInstance().getDb().get(Long.valueOf(value));
            } catch (Throwable th) {
            }
        }

        if (selected == null) {
            selected = connectionParamsList != null && !connectionParamsList.isEmpty() ? connectionParamsList.get(0) : null;
        }
        comboBoxChangeSelection(selected);
        
        cloneBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        saveBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        renameBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        deleteBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        paramBorderPane.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        sshParamBorderPane.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());

        newBttn.defaultButtonProperty().bind(newBttn.focusedProperty());
        cloneBttn.defaultButtonProperty().bind(cloneBttn.focusedProperty());
        saveBttn.defaultButtonProperty().bind(saveBttn.focusedProperty());
        renameBttn.defaultButtonProperty().bind(renameBttn.focusedProperty());
        deleteBttn.defaultButtonProperty().bind(deleteBttn.focusedProperty());

        sessionIdleField.disableProperty().bind(sessionIdleDefault.selectedProperty());

        sshTunnelingPane.disableProperty().bind(useSshTunnelingCheckBox.selectedProperty().not());
        sshPrivateKeyField.disableProperty().bind(sshPasswordTypeRadioButton.selectedProperty());
        sshPrivateKeyLabel.disableProperty().bind(sshPasswordTypeRadioButton.selectedProperty());
        sshOpenPrivateKeyButton.disableProperty().bind(sshPasswordTypeRadioButton.selectedProperty());

        connectBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        testBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        
        connectBttn.defaultButtonProperty().bind(connectBttn.focusedProperty());
        testBttn.defaultButtonProperty().bind(testBttn.focusedProperty());
        cancelBttn.defaultButtonProperty().bind(cancelBttn.focusedProperty());

        mainUI.sceneProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                if(!cancelable){ return; }
                
                if (newValue != null && newValue instanceof Scene) {                    
                    ((Scene) newValue).windowProperty().addListener(this);
                }else if (newValue != null && newValue instanceof Window) {
                    ((Window) newValue).getScene().windowProperty().removeListener(this);
                    
                    ((Window) newValue).setOnShowing(new EventHandler<WindowEvent>() {
                        @Override
                        public void handle(WindowEvent event) {
                            double constant = 0.0;
                            double stageWidtht = ((Stage) newValue).getOwner().getWidth();
                            double stageHeight = ((Stage) newValue).getOwner().getHeight();

                            if (stageWidtht > mainUI.getPrefWidth()) {
                                constant = Math.abs((stageWidtht - mainUI.getPrefWidth()) / 2);
                                ((Stage) newValue).setX(((Stage) newValue).getOwner().getX() + constant);
                            } else {
                                ((Stage) newValue).setX(((Stage) newValue).getOwner().getX());
                            }

                            if (stageHeight > mainUI.getPrefHeight()) {
                                constant = Math.abs((stageHeight - mainUI.getPrefHeight()) / 2);
                                ((Stage) newValue).setY(((Stage) newValue).getOwner().getY() + constant);
                            } else {
                                ((Stage) newValue).setY(((Stage) newValue).getOwner().getY());
                            }
                        }
                    });
                }
            }
        });

        tabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue == osTab && getConnectionParams().isRemote() && !getConnectionParams().hasOSDetails()) {
                DialogHelper.showInfo(getController(), "Info", "Please fill OS details or OS Metrics for remote connection", null);
            }
        });
    }

    private void updateFields(ConnectionParams params) {

        useCompressedProtocolCheckBox.setSelected(params != null ? params.isUseCompressedProtocol() : true);
        address.setText(params != null ? params.getAddress() : "");
        user.setText(params != null ? params.getUsername() : "");
        pass.setText(params != null ? params.getPassword() : "");
        port.setText(params != null ? params.getPort() + "" : "");
        database.setText(reverseSeparateWithComma(params != null ? params.getDatabase() : null));

        sessionIdleField.setText(params != null && params.getSessionIdleTimeout() > 0 ? params.getSessionIdleTimeout() + "" : "28800");
        sessionIdleRadioButton.setSelected(params != null && params.getSessionIdleTimeout() != 28800);
        sessionIdleDefault.setSelected(params == null || params.getSessionIdleTimeout() == 0 || params.getSessionIdleTimeout() == 28800);

        // Ssh module
        sshHost.setText(params != null ? params.getSshHost() : "");
        sshPort.setText(params != null ? params.getSshPort() + "" : "");
        sshUser.setText(params != null ? params.getSshUser() : "");
        sshPassword.setText(params != null ? params.getSshPassword() : "");
        useSshTunnelingCheckBox.setSelected(params != null ? params.isUseSshTunneling() : false);
        sshPrivateKeyTypeRadioButton.setSelected(params != null && params.isUseSshPrivateKey());
        sshPasswordTypeRadioButton.setSelected(params == null || !params.isUseSshPrivateKey());
        sshPrivateKeyField.setText(params != null ? params.getSshPrivateKey() : "");
    }

    public ListCell<ConnectionParams> getConnectionParamsListCell() {
        return new ListCell<ConnectionParams>() {

            @Override
            protected void updateItem(ConnectionParams item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (item != null) {
                    setText(item.getName());
                }
            }

        };
    }

    public void init() {
        selectedConnParam = new SimpleObjectProperty<ConnectionParams>() {
            @Override
            protected void invalidated() {
                super.invalidated();
                get();
            }

        };
        selectedConnParam.bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty());

        connectionParamsList = FXCollections.observableArrayList();
        connectionParamsList.addAll(ServiceEntityProviders.getInstance().getDb().getAll());

        Collections.sort(connectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
            return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
        });

        savePasswordCheckBox.setSelected(true);
        sshSavePasswordCheckBox.setSelected(true);

        savedConnectionComboBox.setButtonCell(getConnectionParamsListCell());
        savedConnectionComboBox.setCellFactory(new Callback<ListView<ConnectionParams>, ListCell<ConnectionParams>>() {

            @Override
            public ListCell<ConnectionParams> call(ListView<ConnectionParams> param) {
                return getConnectionParamsListCell();
            }

        });

        new AutoCompleteComboBoxListener<>(savedConnectionComboBox);
        savedConnectionComboBox.setConverter(new StringConverter<ConnectionParams>() {
            @Override
            public String toString(ConnectionParams object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public ConnectionParams fromString(String string) {
                for (ConnectionParams cp : connectionParamsList) {
                    if (cp.getName().equals(string)) {
                        return cp;
                    }
                }

                return null;
            }
        });

        savedConnectionComboBox.setItems(connectionParamsList);
        Platform.runLater(() -> savedConnectionComboBox.getEditor().selectAll());
        propertyListeners();
    }

    public void selectSavedConnection(final ConnectionParams params) {
        savedConnectionComboBox.getSelectionModel().select(params);
    }

//    public void selectNewConnection(final String name) {
//        ConnectionParams p = provider.get();
//        p.setName(name);
//        addConnectionNameList(p);
//        comboBoxChangeSelection(p);
//        
//        savedConnectionComboBox.getSelectionModel().select(p);        
//    }
    public void propertyListeners() {

        selectedConnParam.addListener(
                (ObservableValue<? extends ConnectionParams> observable, ConnectionParams oldValue, ConnectionParams newValue) -> {
                    updateFields(newValue);
                    setHasChanges(false);
                });

        useCompressedProtocolCheckBox.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isUseCompressedProtocol(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setUseCompressedProtocol(newValue);
                        }
                    }
                });

        address.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getAddress(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setAddress(newValue);
                    }
                });
        user.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getUsername(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setUsername(newValue);
                    }
                });

        pass.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {

                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getPassword(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setPassword(newValue);
                    }
                    if (newValue == null) {
                        savePasswordCheckBox.setSelected(false);
                    } else {
                        savePasswordCheckBox.setSelected(true);
                    }
                });

        port.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.matches("\\d*")) {
                try {
                    int value = Integer.parseInt(newValue);
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getPort(), value)) {
                        setHasChanges(true);
                        selectedConnParam.get().setPort(value);
                    }
                } catch (NumberFormatException ex) {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getPort(), 0)) {
                        setHasChanges(true);
                        selectedConnParam.get().setPort(0);
                    }
                }
            } else {
                port.setText(oldValue);
            }
        });

        sessionIdleField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.matches("\\d*")) {
                try {
                    int value = Integer.parseInt(newValue);
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSessionIdleTimeout(), value)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSessionIdleTimeout(value);
                    }
                } catch (NumberFormatException ex) {
                }
            } else {
                sessionIdleField.setText(oldValue);
            }
        });

        database.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && (selectedConnParam.get().getDatabase() == null || !Objects.equal(String.join(";", selectedConnParam.get().getDatabase()), newValue))) {
                        setHasChanges(true);
                        selectedConnParam.get().setDatabase(separateWithComma(newValue));
                    }
                });

        sshHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshHost(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshHost(newValue);
                    }
                });

        sshPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (newValue.matches("\\d*")) {
                        try {
                            int port = Integer.parseInt(newValue);
                            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPort(), port)) {
                                setHasChanges(true);
                                selectedConnParam.get().setSshPort(port);
                            }
                        } catch (NumberFormatException ex) {
                            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPort(), 0)) {
                                setHasChanges(true);
                                selectedConnParam.get().setSshPort(0);
                            }
                        }
                    } else {
                        sshPort.setText(oldValue);
                    }
                });

        sshUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshUser(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshUser(newValue);
                    }
                });

        sshPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPassword(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshPassword(newValue);
                    }

                    sshSavePasswordCheckBox.setSelected(pass != null);
                });

        useSshTunnelingCheckBox.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isUseSshTunneling(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setUseSshTunneling(newValue);
                        }
                    }
                });

        sshPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPrivateKey(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshPrivateKey(newValue);
                    }
                });

        sshPrivateKeyTypeRadioButton.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isUseSshPrivateKey(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setUseSshPrivateKey(newValue);
                        }
                    }
                });

        sshPasswordTypeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            sshPasswordLabel.setText(newValue != null && newValue ? "Password" : "Passphrase");
            sshSavePasswordCheckBox.setText(newValue != null && newValue ? "Save Password" : "Save Passphrase");
        });

        sessionIdleDefault.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                sessionIdleField.setText("28800");
            }
        });

    }

    public void setHasChanges(boolean f) {
        hasChanges = f;
    }

    public void comboBoxChangeSelection(ConnectionParams params) {
        if (connectionParamsList.size() > 0) {
            savedConnectionComboBox.getSelectionModel().select(params);
        }

    }

    public void addConnectionNameList(ConnectionParams connectionParam) {
        connectionParam.setType(TechnologyType.MYSQL);

        connectionParamsList.add(connectionParam);
        Collections.sort(connectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
            return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
        });

        savedConnectionComboBox.getSelectionModel().select(connectionParam);
    }

    public List<String> separateWithComma(String databases) {
        if (!databases.trim().isEmpty()) {

            String[] result = databases.split(";");

            List<String> l = new ArrayList<>();
            for (String r : result) {
                l.add(r.trim());
            }

            return l;
        } else {
            return new ArrayList<>();
        }

    }

    public String reverseSeparateWithComma(List<String> databaseList) {
        if (databaseList == null) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < databaseList.size(); i++) {
            stringBuilder.append(databaseList.get(i));
            if (i == databaseList.size() - 1) {
                continue;
            }
            stringBuilder.append(';');
        }

        return stringBuilder.toString();
    }

    @Override
    public Stage getStage() {
        if (database.getScene() == null) {
            return null;
        }
        return (Stage) database.getScene().getWindow(); //To change body of generated methods, choose Tools | Templates.
    }

    public void addTab(Tab tab) {
        tabPane.getTabs().add(tab);
    }

    public void addTab(Tab tab, int index) {
        tabPane.getTabs().add(index, tab);
    }

    public ConnectionParams getConnectionParams() {
        return selectedConnParam.get();
    }

    public void addConnectionChangeListener(ChangeListener<ConnectionParams> c) {
        selectedConnParam.addListener(c);
    }
    
    public void selectConnectionParam(String paramId) {
        for (ConnectionParams cp : connectionParamsList) {
            if (String.valueOf(cp.getId()).equals(paramId)) {
                comboBoxChangeSelection(cp);
                break;
            }
        }
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }
    
}
