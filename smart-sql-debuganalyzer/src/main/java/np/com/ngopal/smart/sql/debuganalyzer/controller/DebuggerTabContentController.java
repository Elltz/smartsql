/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.debuganalyzer.controller;

import com.sun.javafx.util.Utils;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.debuganalyzer.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class DebuggerTabContentController extends TabContentController {
    
    @FXML
    private TabPane tabpane;
    
    @FXML
    private Tab mainNew;
    
    @FXML
    private AnchorPane mainUI;
    
    private Node tabNode = null;
    
    private ContextMenu newConnectionContexMenu = new ContextMenu();

    @Override
    public void focusLeftTree() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openConnection(ConnectionParams params) throws SQLException {
        MysqlDBService dbService = ServiceEntityProviders.getInstance().getNewMysqlDBService();
        dbService.connect(params);
        setDbService(dbService);
        getSession().setService(dbService);
    }

    @Override
    public void addTab(Tab t) {
        tabpane.getTabs().add(tabpane.getTabs().size() -1, t);
    }

    @Override
    public void showTab(Tab t) {
        tabpane.getSelectionModel().select(t);
    }

    @Override
    public Tab getTab(String id, String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTabToBottomPane(Tab... tab) {
        
    }

    @Override
    public void removeTabFromBottomPane(Tab... tab) {
        
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
        return tabpane.getSelectionModel().selectedItemProperty();
    }

    @Override
    public List<Tab> getBottamTabs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return true;
    }

    @Override
    public String getTabName() {
        return "DebugConnection " + getSession().getId();
    }

    @Override
    public DisconnectPermission destroyController(DisconnectPermission permission) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void refreshTree(boolean alertkDatabaseCount) {
        
    }

    @Override
    public Object getTreeViewUserData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void isFocusedNow() {
        if(tabpane.getTabs().size() <= 1){
            createNewDebuggerConsole();
        }
    }

    @Override
    public void hideLeftPane() {
        
    }

    @Override
    public void showLeftPane() {
        
    }

    @Override
    public void addOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        
    }

    @Override
    public void removeOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        
    }

    @Override
    public ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeFont(String font) {
        
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        MenuItem openNewDebugger = new MenuItem("Open new debugger from current settings",
                FunctionHelper.constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getNew_image()), 18,18));
        openNewDebugger.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
            }
        });
        
        MenuItem debuggerMenuItem = new MenuItem("Open new debugger", 
                FunctionHelper.constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getDebugger_image()), 18,18));
        debuggerMenuItem.setOnAction((ActionEvent event) -> {
            createNewDebuggerConsole();
        });
        
        MenuItem queryAnalyzerMenuItem = new MenuItem("Open MySQL Slow Log Query Analyzer", 
                FunctionHelper.constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getSlow_analyzer_image()),18,18));
        queryAnalyzerMenuItem.setOnAction((ActionEvent event) -> {
            FunctionHelper.showSlowQueryAnalyzer(getController(), ServiceEntityProviders.getInstance().getH2ProfilerService(),
                    ServiceEntityProviders.getInstance().getPrefDataService(),ServiceEntityProviders.getInstance().getQARecommendationService(),
                    ServiceEntityProviders.getInstance().getColumnCardinalityService(), ServiceEntityProviders.getInstance().getQAPerformanceTuningService(),
                    ServiceEntityProviders.getInstance().getDb(), ServiceEntityProviders.getInstance().getUsageService());
        });
        
        newConnectionContexMenu.getItems().addAll(openNewDebugger, debuggerMenuItem, queryAnalyzerMenuItem);        
        mainNew.setContextMenu(newConnectionContexMenu);
        
        tabpane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                if (newValue != null) {
                    if (newValue == mainNew) {
                        if (tabNode == null) { tabNode = tabpane.lookup("#mainNew"); }
                        Point2D point = Utils.pointRelativeTo(tabNode,
                                tabNode.getBoundsInLocal().getWidth(),
                                tabNode.getBoundsInLocal().getHeight(),
                                HPos.RIGHT, VPos.BOTTOM, tabNode.getBoundsInLocal().getMinX(),
                                tabNode.getBoundsInLocal().getMinY(), true);

                        newConnectionContexMenu.show(tabNode, point.getX(), point.getY());
                        if (tabpane.getTabs().size() > 1) {
                            tabpane.getSelectionModel().select(oldValue);
                        } else {
                            tabpane.getSelectionModel().clearSelection();
                        }
                    }
                }
            }
        });
    }
    
    private void createNewDebuggerConsole() {
        //dont know why just there to solve NPE
        try { ServiceEntityProviders.getInstance().getProfilerChartService().getAll(""); }
        catch (Exception e) { e.printStackTrace(); }
        
        FunctionHelper.showDebugger(getController(), null, ServiceEntityProviders.getInstance().getProfilerChartService(),
                ServiceEntityProviders.getInstance().getH2ProfilerService(), ServiceEntityProviders.getInstance().getPrefDataService(),
                ServiceEntityProviders.getInstance().getDeadlockService(), ServiceEntityProviders.getInstance().getQARecommendationService(),
                ServiceEntityProviders.getInstance().getColumnCardinalityService(), ServiceEntityProviders.getInstance().getQAPerformanceTuningService(),
                ServiceEntityProviders.getInstance().getProfilerSettingsService(), ServiceEntityProviders.getInstance().getDb(),
                ServiceEntityProviders.getInstance().getDebuggerProfilerService(), ServiceEntityProviders.getInstance().getUsageService());
    }

}
