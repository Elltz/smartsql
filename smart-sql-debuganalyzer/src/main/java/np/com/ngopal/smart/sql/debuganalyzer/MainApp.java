package np.com.ngopal.smart.sql.debuganalyzer;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.debuganalyzer.controller.MainController;
import np.com.ngopal.smart.sql.debuganalyzer.provider.DebugAnalyzerBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.SynchorniseWorkProccessor;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;


public class MainApp extends Application {

    public static MainController controller;
    private static SynchorniseWorkProccessor snyProccessor;
    
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("SMART DEBUGGER & ANALYZER");
        controller = new DebugAnalyzerBaseControllerProvider<MainController>(null).get("Main");
        Scene scene = new Scene(controller.getUI());
        scene.getStylesheets().addAll(StandardResourceProvider.getInstance().getDependenceStylesheet(),
                getClass().getResource("/styles/Styles.css").toExternalForm());        
        stage.setScene(scene);
        
        stage.show();
        snyProccessor.start();
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                //controller.initLicenseManager();
            }
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StandardApplicationManager.getInstance().handleApplicatEntryArguments(args);
        snyProccessor = new SynchorniseWorkProccessor();
        snyProccessor.makeMain();
        StandardApplicationManager.getInstance().initializeDatabaseDriver("smart-debuganalyzer.db",
                "np/com/ngopal/smart/sql/debuganalyzer/smart-debuganalyzer.changelog.xml");
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if(controller != null){
            
            controller = null;
        }
        snyProccessor.destroy();
        snyProccessor = null;
    }

}
