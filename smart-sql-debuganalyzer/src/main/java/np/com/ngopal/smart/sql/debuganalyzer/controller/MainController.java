/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.debuganalyzer.controller;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.debuganalyzer.provider.DebugAnalyzerBaseControllerProvider;
import np.com.ngopal.smart.sql.debuganalyzer.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.LicenseManager;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.SmartPropertiesData;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class MainController extends BaseController implements UIController, Initializable  {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TabPane mainTabpane;
    
    @FXML
    private MenuItem closeMenuItem;
    
    @FXML
    private HBox toolBarContainer;
    
    @FXML
    private ToggleButton emptyTab;
    
    @FXML
    private HBox toolbarButtonsContainer;
    
    @FXML
    private ToolBar additionalButtonContainer;
    
    private Node tabNode = null;
    
    private NewConnectionController connectionController;
    
    private Stage connectionStage;
    
    private SimpleObjectProperty<ConnectionSession> selectedSession = new SimpleObjectProperty<ConnectionSession>();

    private final Map<Integer, ConnectionSession> connectionSessions = new LinkedHashMap<>();
    
    private final Map<ConnectionSession, Tab> tabSessions = new LinkedHashMap<>();
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void addMenuItems(Class depencence, MenuCategory category, MenuItem[] items) {
        
    }

    @Override
    public void clearMenuItems(Class depencence, MenuCategory category) {
        
    }

    @Override
    public void addToolbarContainerTabListener(Consumer<MenuCategory> listener) {
        
    }

    @Override
    public void removeToolbarContainerTabListener(Consumer<MenuCategory> listener) {
        
    }

    @Override
    public void addCustomKeyCombinations(KeyCombination key, Consumer action) {
        
    }

    @Override
    public void nextTab() {
        mainTabpane.getSelectionModel().selectNext();
    }

    @Override
    public void prevTab() {
        mainTabpane.getSelectionModel().selectPrevious();
    }

    @Override
    public void selectTab(int index) {
         mainTabpane.getSelectionModel().select(index);
    }

    @Override
    public void selectLast() {
        mainTabpane.getSelectionModel().selectLast();
    }

    @Override
    public void addTab(Tab tab, ConnectionSession session) {
        Platform.runLater(() -> {
            DebuggerTabContentController contentController = (DebuggerTabContentController) tabSessions.get(session).getUserData();
            contentController.addTab(tab);
        });
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedConnectionTab(ConnectionSession session) {
        DebuggerTabContentController debuggerTabContentController = (DebuggerTabContentController) tabSessions.get(session).getUserData();
        return debuggerTabContentController.getSelectedTab();
    }

    @Override
    public TabContentController getTabControllerBySession(ConnectionSession session) {
        return (TabContentController) tabSessions.get(session).getUserData();
    }

    @Override
    public void showTab(Tab tab, ConnectionSession session) {
        Platform.runLater(() -> {
            DebuggerTabContentController contentController = (DebuggerTabContentController) tabSessions.get(session).getUserData();
            contentController.showTab(tab);
        });
    }

    @Override
    public Tab getTab(String id, String text, ConnectionSession session) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem(ConnectionSession session) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void refreshTree(ConnectionSession session, boolean alertDatabaseCount) {
        
    }

    @Override
    public void toggleLeftPane() {
        
    }

    @Override
    public void hideContentPane(ConnectionSession session, boolean visibility) {
        
    }

    @Override
    public void hideResultPane(ConnectionSession session, boolean visibility) {
        
    }

    @Override
    public ReadOnlyObjectProperty<ConnectionSession> getSelectedConnectionSession() {
        return selectedSession;
    }

    @Override
    public ModuleController getModuleByClassName(String moduleClassname) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTabInConnection(Tab tab, int i) {
        tabSessions.put(connectionSessions.get(i), tab);
        Platform.runLater(() -> {
            mainTabpane.getTabs().add(tab);
            mainTabpane.getSelectionModel().select(tab);
        });
        
        if(i == 1){
            Platform.runLater(() -> resizeApplicationWithScreenSpecs() );
        }
    }

    @Override
    public void closeAllTabs() {
        
    }

    @Override
    public void closeTab(TabContentController tcc) {
        
    }

    @Override
    public void addTabToBottomPane(ConnectionSession session, Tab... tab) {
        
    }

    @Override
    public void removeTabFromBottomPane(ConnectionSession session, Tab... tab) {
        
    }

    @Override
    public DisconnectPermission disconnect(ConnectionSession session, DisconnectPermission permission) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BooleanBinding centerTabForQueryTabBooleanProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BooleanBinding centerTabForOnlyQueryTabBooleanProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjectBinding selectedTabProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reachHistoryTab() {
        
    }

    @Override
    public void reachFeqTab() {
        
    }

    @Override
    public void saveSessions() {
        
    }

    @Override
    public void saveSession(ConnectionSession session) {
        
    }

    @Override
    public void saveSessionAs(ConnectionSession session) {
        
    }

    @Override
    public void saveSessionTab(ConnectionSession session, String tab) {
        
    }

    @Override
    public void openSessionPoint() {
        
    }

    @Override
    public void endSession(ConnectionSession session) {
        
    }

    @Override
    public void aboutPage() {
        
    }

    @Override
    public void addBackgroundWork(WorkProc wp) {
        Recycler.addWorkProc(wp);
    }

    @Override
    public void togleTabPaneLayerToRight(boolean bool) {
        
    }

    @Override
    public HBox getApplicationFooter() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hideApplicationFooter() {
        
    }

    @Override
    public void showApplicationFooter() {
        
    }

    @Override
    public SmartPropertiesData getSmartProperties() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        mainTabpane.getTabs().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                Platform.runLater(()-> {
                    if(mainTabpane.getTabs().size() > 0){
                        mainUI.getChildren().remove(connectionController.getUI());                        
                    }else{
                        mainUI.getChildren().add(connectionController.getUI());
                    }
                });
            }
        });
        
        mainTabpane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                if (newValue != null) {
                    for (Entry<ConnectionSession, Tab> en : tabSessions.entrySet()) {
                        if (en.getValue() == newValue) {
                            selectedSession.set(en.getKey());
                            break;
                        }
                    }
                    Platform.runLater(() -> {
                        ((DebuggerTabContentController) newValue.getUserData()).isFocusedNow();
                    });
                }
            }
        });
        
        connectionController = new DebugAnalyzerBaseControllerProvider<NewConnectionController>(MainController.this).get("NewConnection");
        connectionController.setCancelable(false);
        
        AnchorPane.setTopAnchor(connectionController.getUI(), 0.0);
        AnchorPane.setLeftAnchor(connectionController.getUI(), 0.0);
        AnchorPane.setRightAnchor(connectionController.getUI(), 0.0);
        AnchorPane.setBottomAnchor(connectionController.getUI(), 0.0);
        
        mainUI.getChildren().add(connectionController.getUI());
    }
    
    void createNewConnectionSession(ConnectionParams params) throws Exception {
        ConnectionSession session = new ConnectionSession();
        session.setId(connectionSessions.size() + 1);
        session.setConnectionParam(params);

        DebuggerTabContentController debuggerTabContentController
                = new DebugAnalyzerBaseControllerProvider<DebuggerTabContentController>(this).get("DebuggerTabContent");
        debuggerTabContentController.setSession(session);
        debuggerTabContentController.openConnection(params);

        Tab debugConnectionTab = new Tab(debuggerTabContentController.getTabName());
        //debugConnectionTab.setGraphic(new Label(debuggerTabContentController.getTabName()));
        debugConnectionTab.setUserData(debuggerTabContentController);
        debugConnectionTab.setContent(debuggerTabContentController.getUI());

        session.setOpen(true);
        connectionSessions.put(session.getId(), session);
        addTabInConnection(debugConnectionTab, session.getId());
    }
    
    private void createConnectionDialog() {
        if (connectionStage == null) {
            connectionStage = new Stage(StageStyle.UTILITY);
            connectionStage.initOwner(getStage());
            connectionStage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(connectionController.getUI(), 600, 500);
            connectionStage.setScene(scene);
        }
    }
    
    private void resizeApplicationWithScreenSpecs(){
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        double w = screenBounds.getWidth() * 0.88;
        double h = screenBounds.getHeight() * 0.92;
        
        Stage s = getStage();
        s.setX((screenBounds.getWidth() - w)/2);
        s.setY((screenBounds.getHeight() - h)/2);        
        s.setHeight(h);
        s.setWidth(w);
    }
   
    public void initLicenseManager(){
        LicenseManager.getInstance().init(ServiceEntityProviders.getInstance().getLicenseDataService(),
                ServiceEntityProviders.getInstance().getLicenseDataUsageService(),
                StandardApplicationManager.getInstance().getSmartsqlSavedData().getEmailId(),
                StandardApplicationManager.getInstance().getSmartsqlSavedData().getLKey());
    }
}
