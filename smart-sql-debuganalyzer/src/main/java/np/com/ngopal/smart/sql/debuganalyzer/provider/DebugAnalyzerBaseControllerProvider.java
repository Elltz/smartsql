/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.debuganalyzer.provider;

import np.com.ngopal.smart.sql.debuganalyzer.MainApp;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.provider.BaseControllerProvider;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class DebugAnalyzerBaseControllerProvider <T extends BaseController> extends BaseControllerProvider<T>{

    public DebugAnalyzerBaseControllerProvider(UIController controller) {
        super(controller);
    }

    @Override
    protected String getPreface() {
        return "/np/com/ngopal/smart/sql/debuganalyzer/ui/";
    }
    
}
