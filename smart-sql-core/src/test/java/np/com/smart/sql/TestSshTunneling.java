
package np.com.smart.sql;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class TestSshTunneling {
    
    public static void main(String[] args) {
        
        sshTunneling();
        
    }
    
    private static  void sshTunneling() {
        
        String user = "root";
        String host = "some.host";
        int port = 11000;
        
        try {
            JSch jsch = new JSch();

            Session session = jsch.getSession(user, host, port);
            session.setPassword("some.pswd");
            session.setConfig("StrictHostKeyChecking", "no");

            session.connect();

            int assingedPort = session.setPortForwardingL("0.0.0.0", 1234, "localhost", 3306);

            System.out.println("localhost: " + assingedPort + " -> localhost:3306");
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
