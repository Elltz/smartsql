
package np.com.smart.sql;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.TechnologyType;
import np.com.ngopal.smart.sql.utils.ImportUtils;


/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public class ImportUtilsTest{
    
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(DBService.class).to(MysqlDBService.class);
            }
        });
        
        DBService service = injector.getInstance(DBService.class);
        ConnectionParams p = new ConnectionParams("localhost", TechnologyType.MYSQL, "localhost", "root", "secret", 3306);
        try {
            Connection c = service.connect(p);
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

//        ImportUtils.importDelimiter(service, "G:\\work\\import\\actor.csv", "test33", "actor_test", new ImportUtils.ImportStatusCallback() {
//            @Override
//            public void error(String errorMessage, Throwable th) {
//                log.error(errorMessage, th);
//            }
//
//            @Override
//            public void success() {
//                log.info("Import CSV success");
//            }
//        });
//        
//        ImportUtils.importHTML(service, "G:\\work\\import\\actor.htm", "test33", "actor_html", new ImportUtils.ImportStatusCallback() {
//            @Override
//            public void error(String errorMessage, Throwable th) {
//                log.error(errorMessage, th);
//            }
//
//            @Override
//            public void success() {
//                log.info("Import HTML success");
//            }
//        });
        
//        ImportUtils.importXLS(service, "G:\\work\\import\\actor2.xls", "test33", "actor_xls", new ImportUtils.ImportStatusCallback() {
//            @Override
//            public void error(String errorMessage, Throwable th) {
//                log.error(errorMessage, th);
//            }
//
//            @Override
//            public void success() {
//                log.info("Import XLS success");
//            }
//        });
        
//        
//        ImportUtils.importOtherRDBMS(service, "com.mysql.jdbc.Driver", "jdbc:mysql://localhost/test33", "root", "secret", "actor_html", "test33", "actor_otherrdbms", new ImportUtils.ImportStatusCallback() {
//            @Override
//            public void error(String errorMessage, Throwable th) {
//                log.error(errorMessage, th);
//            }
//
//            @Override
//            public void success() {
//                log.info("Import Other RDBMS success");
//            }
//        });

            ImportUtils.importXML(service, "G:\\work\\import\\actor3.xml", "test33", "actor_xml", new ImportUtils.ImportStatusCallback() {
                @Override
                public boolean isNeedStop() {
                    return false;
                }

                @Override
                public void progress(double progress) {
                    log.info("Import progress: " + progress);
                }
                
                @Override
                public void error(String errorMessage, Throwable th) {
                    log.error(errorMessage, th);
                }

                @Override
                public void success(QueryResult queryResult, Map<String, Object> map) {
                    log.info("Import XML success");
                }
            });
    }
}
