
package np.com.ngopal.smart.sql;

import java.util.List;
import javafx.scene.control.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@AllArgsConstructor
@Getter
@Setter
public class HookKey {
    
    // if we have more than one key with same group - then their items will be merged
    private String group;
    private List<MenuItem> items;
            
}
