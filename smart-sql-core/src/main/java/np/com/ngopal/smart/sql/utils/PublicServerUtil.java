package np.com.ngopal.smart.sql.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.TechnologyType;
import org.apache.commons.codec.binary.Base64;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class PublicServerUtil {

    public static MysqlDBService createPublishingService(String serverData) {
        MysqlDBService publishingService = new MysqlDBService(null);
        
        // TODO Maybe move this to some settings
        ConnectionParams params = createParams(serverData);
        params.setUseCompressedProtocol(false);
        publishingService.setParam(params);
        
        return publishingService;
    }
    
    
    
    public static ConnectionParams createParams(String serverData) {
        try {
            String result = serverData != null ? serverData : new BufferedReader(new InputStreamReader(PublicServerUtil.class.getResourceAsStream("/info"))).lines().collect(Collectors.joining("\n"));

            Cipher cipher = Cipher.getInstance("DESede");

            // @TODO Need get this key from some input like webservice
            DESedeKeySpec ks = new DESedeKeySpec("ganFITKDEfasjdONUASD&Y*F*".getBytes("UTF-8"));
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");

            cipher.init(Cipher.DECRYPT_MODE, skf.generateSecret(ks));
            byte[] encryptedText = Base64.decodeBase64(result);
            byte[] plainText = cipher.doFinal(encryptedText);

            String[] res = new String(plainText, "UTF-8").split("\\$\\$");
            
            Integer port = 3306;
            try {
                port = Integer.valueOf(res[1]);
            } catch (Throwable th) {}
            
            ConnectionParams cp = new ConnectionParams("PublishConnection", TechnologyType.MYSQL, res[0], res[2], res[3], port);
            if (!res[4].isEmpty()) {
                cp.setDatabase(Arrays.asList(new String[]{res[4]}));
            }
            
            return cp;
        } catch (Throwable ex) {
            log.error(ex.getMessage(), ex);
        }
        
        return null;
    }
}
