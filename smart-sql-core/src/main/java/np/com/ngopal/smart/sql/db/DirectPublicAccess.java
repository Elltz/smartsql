package np.com.ngopal.smart.sql.db;

import com.google.gson.Gson;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.DataBindingException;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.utils.PublicServerUtil;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DirectPublicAccess implements PublicAccess {

    private MysqlDBService publicService;
    public DirectPublicAccess(String serverData) {
        publicService = PublicServerUtil.createPublishingService(serverData);
    }

    @Override
    public boolean login(String login, String password) throws PublicDataException {
        // for direct always true at now
        return true;
    }
    
    @Override
    public List<ErrDiagram> loadAllErrDiagrams() throws PublicDataException {
        ResultSet rs = null;
        try {
            
            PreparedStatement ps = publicService.getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__LOAD_ALL_ERR_DIAGRAMS)); 
                
            rs = ps.executeQuery();
            if (rs != null) {
                
                List<ErrDiagram> list = new ArrayList<>();
                while (rs.next()) {
                    ErrDiagram errDiagram = new ErrDiagram();
                    errDiagram.setPublicId(rs.getLong("id"));
                    errDiagram.setName(rs.getString("name"));
                    errDiagram.setDatabase(rs.getString("database_name"));
                    errDiagram.setDiagramData(rs.getString("diagram_data"));
                    errDiagram.setDescription(rs.getString("description"));
                    errDiagram.setDomainId(rs.getLong("domain_id"));
                    errDiagram.setDomainName(rs.getString("domain_name"));
                    errDiagram.setSubDomainId(rs.getLong("subdomain_id"));
                    errDiagram.setSubDomainName(rs.getString("subdomain_name"));
                    
                    Map<String, Object> map = new Gson().fromJson(errDiagram.getDiagramData(), HashMap.class);
                    List tables = (List) map.get("tables");
                    errDiagram.setNumberOfTables(tables != null ? tables.size() : 0);
                    
                    list.add(errDiagram);
                }
                
                return list;
            }            
        } catch (SQLException ex) {
           throw new DataBindingException("Error searching public schemas", ex);
           
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
        }
        
        return null;
    }

    @Override
    public List<ErrDiagram> searchErrDiagram(String searchText) throws PublicDataException {
        ResultSet rs = null;
        try {
            PreparedStatement ps = publicService.getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__SEARCH_ERR_DIAGRAM)); 
            ps.setString(1, searchText);
            ps.setString(2, searchText);
            ps.setString(3, searchText);
            ps.setString(4, searchText);

            rs = ps.executeQuery();
            if (rs != null) {
                
                List<ErrDiagram> data = new ArrayList<>();
                
                while (rs.next()) {
                    ErrDiagram errDiagram = new ErrDiagram();
                    errDiagram.setPublicId(rs.getLong("id"));
                    errDiagram.setUserId(rs.getString("user_id"));
                    errDiagram.setName(rs.getString("name"));
                    errDiagram.setDatabase(rs.getString("database_name"));
                    errDiagram.setDiagramData(rs.getString("diagram_data"));
                    errDiagram.setDescription(rs.getString("description"));
                    errDiagram.setDomainId(rs.getLong("domain_id"));
                    errDiagram.setDomainName(rs.getString("domain_name"));
                    errDiagram.setSubDomainId(rs.getLong("subdomain_id"));
                    errDiagram.setSubDomainName(rs.getString("subdomain_name"));

                    Map<String, Object> map = new Gson().fromJson(errDiagram.getDiagramData(), HashMap.class);
                    List tables = (List) map.get("tables");
                    errDiagram.setNumberOfTables(tables != null ? tables.size() : 0);

                    data.add(errDiagram);
                }
                
                return data;
            }  
        } catch (SQLException ex) {
           throw new PublicDataException("Error searching public schemas", ex);
           
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
        }
        
        return null;
    }

    @Override
    public boolean deleteErrDiagram(Long id) throws PublicDataException {
        try {
            PreparedStatement ps = publicService.getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__DELETE_ERR_DIAGRAM));
            ps.setLong(1, id);

            ps.execute();
            
            return true;
        } catch (SQLException ex) {
            throw new PublicDataException("Delete error", ex);
        }
    }

    @Override
    public boolean updateErrDiagram(String schema, String diagramData, Long domainId, Long subDomainId, String description, Long directPublicId) throws PublicDataException {
        try {
            PreparedStatement ps = publicService.getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__UPDATE_ERR_DIAGRAM));
            ps.setString(1, schema);
            ps.setString(2, diagramData);
            ps.setLong(3, domainId);
            ps.setLong(4, subDomainId);
            ps.setString(5, description);
            ps.setLong(6, directPublicId);

            return ps.executeUpdate() > 0;

        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
    }

    @Override
    public Long insertErrDiagram(String schema, String diagramData, String database, Long domainId, Long subDomainId, String description, String emailId) throws PublicDataException {
        try {
            PreparedStatement ps = publicService.getDB().prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__INSERT_ERR_DIAGRAM), Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, schema);
            ps.setString(2, diagramData);
            ps.setString(3, database);
            ps.setLong(4, domainId);
            ps.setLong(5, subDomainId);
            ps.setString(6, description);
            ps.setString(7, emailId);

            ps.execute();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                Long id = rs.getLong(1);
                rs.close();
                
                return id;
            }

        } catch (SQLException ex) {

            if (ex instanceof SQLIntegrityConstraintViolationException) {
                throw new PublicDataException("The name already used, please specify another name.", null);
            } else {
                throw new PublicDataException("Publish error", ex);
            }
        }
        
        return null;
    }

    @Override
    public List<Map<String, Object>> loadDomains() throws PublicDataException {
        try 
            (ResultSet rs = (ResultSet)publicService.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__LOAD_DOMAINS))) {
            
            if (rs != null) {
                
                List<Map<String, Object>> result = new ArrayList<>();
                while (rs.next()) {
                    Map<String, Object> m = new HashMap<>();
                    m.put("id", rs.getLong("id"));
                    m.put("name", rs.getString("name"));
                    result.add(m);
                }
                
                return result;
            }
        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
        
        return null;
    }

    @Override
    public List<Map<String, Object>> loadSubDomains() throws PublicDataException {
        try 
            (ResultSet rs = (ResultSet)publicService.execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__LOAD_SUBDOMAINS))) {
            
            if (rs != null) {
                
                List<Map<String, Object>> result = new ArrayList<>();
                while (rs.next()) {
                    Map<String, Object> m = new HashMap<>();
                    m.put("id", rs.getLong("id"));
                    m.put("name", rs.getString("name"));
                    result.add(m);                    
                }
                
                return result;
            }
        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
        
        return null;
    }

    @Override
    public Long getIdByDomain(String domain) throws PublicDataException {
        try 
            (ResultSet rs = (ResultSet)publicService.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__GET_ID_BY_DOMAIN), domain))) {
            
            if (rs != null && rs.next()) {
                return rs.getLong("id");
            }
        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
        
        return null;        
    }

    @Override
    public Long insertDomain(String domain) throws PublicDataException {
        try {
            QueryResult queryResult = new QueryResult();
            publicService.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__INSERT_DOMAIN), domain), queryResult);

            ResultSet rs0 = queryResult.getGeneratedKey();
            if (rs0 != null && rs0.next()) {
                return rs0.getLong(1);
            }
        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
        
        return null;
    }

    @Override
    public Long getIdBySubDomain(String subDomain) throws PublicDataException {
        try 
            (ResultSet rs = (ResultSet)publicService.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__GET_ID_BY_SUBDOMAIN), subDomain))) {
            
            if (rs != null && rs.next()) {
                return rs.getLong("id");
            }
        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
        
        return null;
    }

    @Override
    public Long insertSubDomain(String subDomain) throws PublicDataException {
        try {            
            QueryResult queryResult = new QueryResult();
            publicService.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__INSERT_SUBDOMAIN), subDomain), queryResult);

            ResultSet rs0 = queryResult.getGeneratedKey();
            if (rs0 != null && rs0.next()) {
                return rs0.getLong(1);
            }
        } catch (SQLException ex) {
            throw new PublicDataException("Publish error", ex);
        }
        
        return null;
    }

    @Override
    public Timestamp lastUpdateUExpire(String email, String type, String expired) throws PublicDataException {
        try (ResultSet rs = (ResultSet) publicService.execute("SELECT last_update FROM SmartMySQL.UExpire WHERE email = '" + email + "' and type = " + type + " and ExpireDate = '" + expired + "'")) {
            if (rs != null && rs.next()) {
                return rs.getTimestamp(1);
            }
        } catch (Throwable th) {
            throw new PublicDataException(th.getMessage(), th);
        }
        
        return null;
    }

    @Override
    public void insertUExpire(String email, int type, int usedCredits, int usedFullUsage, String expired, String lastUpdate) throws PublicDataException {
        try {
            publicService.execute(
                "INSERT INTO SmartMySQL.UExpire(email, type, expireCredits, fullUsage, ExpireDate, last_update) VALUES("
                    + "'" + email + "', "
                    + type + ", " 
                    + usedCredits + ", " 
                    + usedFullUsage + ", '" 
                    + expired + "', "
                    + "'" + lastUpdate +"') \n" +
                "ON DUPLICATE KEY UPDATE "
                    + "expireCredits = expireCredits + " + usedCredits + ", "
                    + "fullUsage = fullUsage + " + usedFullUsage + ", "
                    + "last_update = '" + lastUpdate + "'");
        } catch (Throwable th) {
            throw new PublicDataException(th.getMessage(), th);
        }
    }

    @Override
    public String userLicense(String email) throws PublicDataException {
        try (ResultSet rs = (ResultSet) publicService.execute("SELECT `SmartMySQL`.`user_licence_type`('" + email + "');")) {
            if (rs != null && rs.next()) {
                return rs.getString(1);
            }
        } catch (Throwable th) {
            throw new PublicDataException(th.getMessage(), th);
        }
        
        return null;
    }

    @Override
    public boolean userValidation(String email, String license) throws PublicDataException {
        
        String database = "SmartMySQL";
        if (publicService.getParam() != null && publicService.getParam().getDatabase() != null && !publicService.getParam().getDatabase().isEmpty()) {
            database = publicService.getParam().getDatabase().get(0);
        }

        try (ResultSet rs = (ResultSet) publicService.execute("SELECT `" + database + "`.user_validation('" + (email) + "', '" + (license) + "')")){

            boolean valid = true;
            if (rs != null && rs.next()) {
                valid = rs.getInt(1) == 1;
            }

            return valid;

        } catch (Exception ex) {
            throw new PublicDataException(ex.getMessage(), ex, true);
        }
    }

    @Override
    public void userInstallUpdates(String userEmailId, String os, String macAddress, String ipAddress, String date) throws PublicDataException {
        String database = "SmartMySQL";
        if (publicService.getParam() != null && publicService.getParam().getDatabase() != null && !publicService.getParam().getDatabase().isEmpty()) {
            database = publicService.getParam().getDatabase().get(0);
        }
        
        String query = "INSERT INTO `" + database + "`.UserIUpdates(uEmail, OS, MACAdd, PublicIP, installDate) "
                + "VALUES('" + userEmailId + "', '" + os + "', '" + macAddress + "', '" + ipAddress + "', '" + date + "')";
        
        try {
            publicService.execute(query);
        } catch (Exception ex) {
            throw new PublicDataException(ex.getMessage(), ex);
        }
    }

    @Override
    public void userCloseApp(String userEmailId, String os, String macAddress, String ipAddress, String dateOpen, String dateClose) throws PublicDataException {
        String database = "SmartMySQL";
        if (publicService.getParam() != null && publicService.getParam().getDatabase() != null && !publicService.getParam().getDatabase().isEmpty()) {
            database = publicService.getParam().getDatabase().get(0);
        }
        
        String query = "INSERT INTO `" + database + "`.UserClosApp(uEmail, OS, MACAdd, PublicIP, OpenTime, CloseTime) "
                + "VALUES('" + userEmailId + "', '" + os + "', '" + macAddress + "', '" + ipAddress + "', '" + dateOpen + "', '" + dateClose + "')";
        
        try {
            publicService.execute(query);
        } catch (Exception ex) {
            throw new PublicDataException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long selectUserSystemId(String userEmailId, String localIP, String globalIP) throws PublicDataException {
        String database = "SmartMySQL";
        if (publicService.getParam() != null && publicService.getParam().getDatabase() != null && !publicService.getParam().getDatabase().isEmpty()) {
            database = publicService.getParam().getDatabase().get(0);
        }
        
        Long userSystemId = null;
        try (ResultSet rs = publicService.executeQuery(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__SELECT_USER_SYSTEM_ID), database, userEmailId, localIP, globalIP))){
            if (rs != null && rs.next()) {
                userSystemId = rs.getLong(1);
            }
        } catch (Exception ex) {
            throw new PublicDataException(ex.getMessage(), ex);
        }
        
        return userSystemId;
    }

    @Override
    public Long insertUserSystem(String userEmailId, String localIP, String globalIP) throws PublicDataException {
        String database = "SmartMySQL";
        if (publicService.getParam() != null && publicService.getParam().getDatabase() != null && !publicService.getParam().getDatabase().isEmpty()) {
            database = publicService.getParam().getDatabase().get(0);
        }
        
        Long userSystemId = null;
        try {
            QueryResult qr = new QueryResult();
            publicService.execute(String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__INSERT_USER_SYSTEM), database, userEmailId, localIP, globalIP), qr);
            if (qr.getGeneratedKey() != null && qr.getGeneratedKey().next()) {
                userSystemId = qr.getGeneratedKey().getLong(1);
            }
        } catch (Exception ex) {
            throw new PublicDataException(ex.getMessage(), ex);
        }
        
        return userSystemId;
    }

    @Override
    public boolean syncUserUsageStatistic(int productId, List<Map<String, Object>> data) throws PublicDataException {
        try {            
            String selectFeatureIdQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__SELECT_FEATURE_ID);
            String insertFeatureQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__INSERT_FEATURE);
            String insertPUsageQuery = QueryProvider.getInstance().getQuery(QueryProvider.DB__PUBLIC, QueryProvider.QUERY__PUBLIC__INSERT_PUSAGE);

            String database = "SmartMySQL";
            if (publicService.getParam() != null && publicService.getParam().getDatabase() != null && !publicService.getParam().getDatabase().isEmpty()) {
                database = publicService.getParam().getDatabase().get(0);
            }
            
            for (Map<String, Object> m: data) {       
                
                // find feature id
                Long featureId = null;
                try (ResultSet rs = publicService.executeQuery(String.format(selectFeatureIdQuery, database, m.get(PUsageFeatures.KEY__FEATURE), productId))) {
                    if (rs != null && rs.next()) {
                        featureId = rs.getLong(1);
                    }
                }
                
                // if null need insert feature for product
                if (featureId == null) {
                    publicService.execute(String.format(insertFeatureQuery, database, productId, m.get(PUsageFeatures.KEY__FEATURE), 1));
                    
                    try (ResultSet rs = publicService.executeQuery(String.format(selectFeatureIdQuery, database, m.get(PUsageFeatures.KEY__FEATURE), productId))) {
                        if (rs != null && rs.next()) {
                            featureId = rs.getLong(1);
                        }
                    }
                }
                
                // insert PUsage
                publicService.execute(String.format(insertPUsageQuery, database, featureId, m.get(PUsageFeatures.KEY__USER_SYSTEM_ID), m.get(PUsageFeatures.KEY__STATUS)));
            }

            return true;

        } catch (SQLException ex) {
            log.error("Error on sync with public server", ex);
        }
        
        return false;
    }

}
