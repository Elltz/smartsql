package np.com.ngopal.smart.sql.db;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import np.com.ngopal.smart.sql.model.ErrDiagram;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class PublicDataAccess implements PublicAccess {

    private static PublicDataAccess instance;
    public static void init(String login, String password, String serverData) throws PublicDataException {
        if (instance == null) {
            instance = new PublicDataAccess(serverData);
        }
        
        instance.login(login, password);
    }
    
    public static PublicDataAccess getInstanсe() {
        if (instance == null) {
            throw new RuntimeException("You must first call \"init\" method");
        }
        
        return instance;
    }
    
    private DirectPublicAccess directAccess;
    private WebApiPublicAccess webApiAccess;
    
    private PublicDataAccess(String serverData) {
        this.directAccess = new DirectPublicAccess(serverData);
        this.webApiAccess = new WebApiPublicAccess(serverData);
    }

    @Override
    public boolean login(String login, String password) throws PublicDataException {
        try {
            return webApiAccess.login(login, password);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.login(login, password);
            } else {
                throw th;
            }
        }
    }
    
    
    @Override
    public List<ErrDiagram> loadAllErrDiagrams() throws PublicDataException {
        try {
            return webApiAccess.loadAllErrDiagrams();
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.loadAllErrDiagrams();
            } else {
                throw th;
            }
        }
    }

    @Override
    public List<ErrDiagram> searchErrDiagram(String text) throws PublicDataException {
        try {
            return webApiAccess.searchErrDiagram(text);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.searchErrDiagram(text);
            } else {
                throw th;
            }
        }
    }

    @Override
    public boolean deleteErrDiagram(Long id) throws PublicDataException {
        try {
            return webApiAccess.deleteErrDiagram(id);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.deleteErrDiagram(id);
            } else {
                throw th;
            }
        }
    }

    @Override
    public boolean updateErrDiagram(String schema, String diagramData, Long domainId, Long subDomainId, String description, Long directPublicId) throws PublicDataException {
        try {
            return webApiAccess.updateErrDiagram(schema, diagramData, domainId, subDomainId, description, directPublicId);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.updateErrDiagram(schema, diagramData, domainId, subDomainId, description, directPublicId);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long insertErrDiagram(String schema, String diagramData, String database, Long domainId, Long subDomainId, String description, String emailId) throws PublicDataException {
        try {
            return webApiAccess.insertErrDiagram(schema, diagramData, database, domainId, subDomainId, description, emailId);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.insertErrDiagram(schema, diagramData, database, domainId, subDomainId, description, emailId);
            } else {
                throw th;
            }
        }
    }

    @Override
    public List<Map<String, Object>> loadDomains() throws PublicDataException {
        try {
            return webApiAccess.loadDomains();
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.loadDomains();
            } else {
                throw th;
            }
        }
    }

    @Override
    public List<Map<String, Object>> loadSubDomains() throws PublicDataException {
        try {
            return webApiAccess.loadSubDomains();
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.loadSubDomains();
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long getIdByDomain(String domain) throws PublicDataException {
        try {
            return webApiAccess.getIdByDomain(domain);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.getIdByDomain(domain);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long insertDomain(String domain) throws PublicDataException {
        try {
            return webApiAccess.insertDomain(domain);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.insertDomain(domain);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long getIdBySubDomain(String subDomain) throws PublicDataException {
        try {
            return webApiAccess.getIdBySubDomain(subDomain);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.getIdBySubDomain(subDomain);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long insertSubDomain(String domain) throws PublicDataException {
        try {
            return webApiAccess.insertSubDomain(domain);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.insertSubDomain(domain);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Timestamp lastUpdateUExpire(String email, String type, String expired) throws PublicDataException {
        try {
            return webApiAccess.lastUpdateUExpire(email, type, expired);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.lastUpdateUExpire(email, type, expired);
            } else {
                throw th;
            }
        }
    }

    @Override
    public void insertUExpire(String email, int type, int usedCredits, int usedFullUsage, String expired, String lastUpdate) throws PublicDataException {
        try {
            webApiAccess.insertUExpire(email, type, usedCredits, usedFullUsage, expired, lastUpdate);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                directAccess.insertUExpire(email, type, usedCredits, usedFullUsage, expired, lastUpdate);
            } else {
                throw th;
            }
        }
    }

    @Override
    public String userLicense(String email) throws PublicDataException {
        try {
            return webApiAccess.userLicense(email);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.userLicense(email);
            } else {
                throw th;
            }
        }
    }

    @Override
    public boolean userValidation(String email, String license) throws PublicDataException {
        try {
            return webApiAccess.userValidation(email, license);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.userValidation(email, license);
            } else {
                throw th;
            }
        }
    }

    @Override
    public void userInstallUpdates(String userEmailId, String os, String macAddress, String ipAddress, String date) throws PublicDataException {
        try {
            webApiAccess.userInstallUpdates(userEmailId, os, macAddress, ipAddress, date);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                directAccess.userInstallUpdates(userEmailId, os, macAddress, ipAddress, date);
            } else {
                throw th;
            }
        }
    }

    @Override
    public void userCloseApp(String userEmailId, String os, String macAddress, String ipAddress, String dateOpen, String dateClose) throws PublicDataException {
        try {
            webApiAccess.userCloseApp(userEmailId, os, macAddress, ipAddress, dateOpen, dateClose);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                directAccess.userCloseApp(userEmailId, os, macAddress, ipAddress, dateOpen, dateClose);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long selectUserSystemId(String userEmailId, String localIP, String globalIP) throws PublicDataException {
        try {
            return webApiAccess.selectUserSystemId(userEmailId, localIP, globalIP);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.selectUserSystemId(userEmailId, localIP, globalIP);
            } else {
                throw th;
            }
        }
    }

    @Override
    public Long insertUserSystem(String userEmailId, String localIP, String globalIP) throws PublicDataException {
        try {
            return webApiAccess.insertUserSystem(userEmailId, localIP, globalIP);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.insertUserSystem(userEmailId, localIP, globalIP);
            } else {
                throw th;
            }
        }
    }

    @Override
    public boolean syncUserUsageStatistic(int productId, List<Map<String, Object>> data) throws PublicDataException {
        try {
            return webApiAccess.syncUserUsageStatistic(productId, data);
        } catch (PublicDataException th) {
            if (th.isCriticalError()) {
                return directAccess.syncUserUsageStatistic(productId, data);
            } else {
                throw th;
            }
        }
    }

}
