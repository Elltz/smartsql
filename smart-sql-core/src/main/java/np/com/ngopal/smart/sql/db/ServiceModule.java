/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import com.google.inject.AbstractModule;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class ServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(DBService.class).to(AbstractDBService.class);
//        bind(AbstractDBService.class).to(MysqlDBService.class);
    }

//    @Provides
//    public MysqlTechnology provideMysqlTechnology() {
//        return new MysqlTechnology();
//    }
}
