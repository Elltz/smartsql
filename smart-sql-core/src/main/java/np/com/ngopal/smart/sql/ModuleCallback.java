/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql;

import java.util.List;
import java.util.Map;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;

/**
 * Every module will be executed from this Callback which makes the module's
 * inner method get executed on different targets of application life cycle.
 * <p>
 * This class is only implemented by main application for the module purpose.
 * <p>
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public interface ModuleCallback extends Comparable<ModuleCallback>{

    /**
     * Provides the class of the Module.
     * <p>
     * @return
     */
    Class getModuleClass();

    /**
     * Check to see if the module is from the core package.
     * <p>
     * @return
     */
    boolean isSystemModule();

    /**
     * This method will execute the block of module's postConnection and other
     * methods sequentially.
     * <p>
     * @param param
     * @param tab
     */
    void call(ConnectionSession param, Tab tab);

    /**
     * This method will be executed after the normal call.
     * <p>
     * @param param
     * @return
     */
    Map<HookKey, Hookable> getHooks(ConnectionSession param);

    Class getDependencyModuleClass();

    int order();

    
    @Override
    default public int compareTo(ModuleCallback o)
    {
        return order() - o.order();
    }
    
    
}
