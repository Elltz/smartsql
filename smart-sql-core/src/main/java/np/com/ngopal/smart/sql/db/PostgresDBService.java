/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.db;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import lombok.extern.slf4j.Slf4j;
import static np.com.ngopal.smart.sql.db.DBService.COLUMN_NAME__EVENTS;
import static np.com.ngopal.smart.sql.db.DBService.COLUMN_NAME__FUNCTIONS;
import static np.com.ngopal.smart.sql.db.DBService.COLUMN_NAME__PROCEDURES;
import static np.com.ngopal.smart.sql.db.DBService.COLUMN_NAME__TABLES;
import static np.com.ngopal.smart.sql.db.DBService.COLUMN_NAME__TRIGGERS;
import static np.com.ngopal.smart.sql.db.DBService.COLUMN_NAME__VIEWS;
import static np.com.ngopal.smart.sql.db.MysqlDBService.createConnectionUrl;
import static np.com.ngopal.smart.sql.db.MysqlDBService.sshTunneling;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PgDatabase;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class PostgresDBService extends AbstractDBService {
        
    private static int SSH_TUNNELING_PORT = 5432;
    
    private final List<MysqlDBService.ConnectionListener> connectionListeners = new ArrayList<>();
         
    public void addConnectinListener(AbstractDBService.ConnectionListener listener) {
        if (!connectionListeners.contains(listener)) {
            connectionListeners.add(listener);
        }
    }
    
    public void removeConnectionListener(AbstractDBService.ConnectionListener listener) {
        connectionListeners.remove(listener);
    }

    @Override
    protected TechnologyType getTechType() {
        return TechnologyType.POSTGRESQL;
    }
       
    private static String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__POSTGRESQL, queryId);
    }
    
    private static String getFormatedQuery(String queryId, Object...values) {
        return String.format(getQuery(queryId), values);
    }
    

    @Override
    protected String getChangeDBSQL(Database database) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Column> getDescribeColumn(String databaseName, String tableName) throws SQLException {
        List<Column> columns = new ArrayList<>();
        try (ResultSet rs = (ResultSet) executeQuery(getDescribeColumnSQL(databaseName, tableName))) {
            int order = 0;
            while (rs.next()) {
                Column column = new Column();
                column.setOrderIndex(order);
                column.setName(rs.getString("column_name"));
                if (rs.getBoolean("has_default")) {
                    column.setDefaults(rs.getString("default"));
                }
                columns.add(column);
                order++;
            }
        }

        //log.debug("size of column : {}", columns.size());
        return columns;
    }

    @Override
    public List<DBTable> getTables(Database d) throws SQLException {
         List<DBTable> tables = new ArrayList<>();
        try (ResultSet rs = executeQuery(getTablesSQL(d))) {
            while (rs.next()) {
                String tableName = rs.getString("name");
                DBTable t = new DBTable(tableName, null, null, null, null, d, null, null, null, null);
                List<Column> columns = getDescribeColumn(d.getName(), ("'"+tableName+"'"));
                for(Column column : columns){ column.setTable(t); }
                t.setColumns(columns);                
                tables.add(t);
            }
        }
        //log.debug("Total Tables: {}", tables.size());
        return tables;
    }

    @Override
    public List<Database> getDatabases() throws SQLException {
        List<Database> db = null;

        try (ResultSet rs = executeQuery(getDatabasesSQL())) {
            db = new ArrayList<>();
            while (rs.next()) {
                PgDatabase pgDatabase = new PgDatabase();
                pgDatabase.setName(rs.getString("database"));
                pgDatabase.setCanCreate(rs.getString("cancreate").charAt(0) == 't');
                pgDatabase.setID(rs.getInt("id"));
                pgDatabase.setServerEncoding(rs.getString("serverencoding"));
                pgDatabase.setTableSpace(rs.getInt("dattablespace"));
                pgDatabase.setLastSysId(rs.getInt( "datlastsysoid"));
                db.add(pgDatabase);
            }
        }

        db.sort(new Comparator<Database>() {
            @Override
            public int compare(Database o1, Database o2) {
                return Integer.compare( ((PgDatabase)o1).getID(), ((PgDatabase)o2).getID() );
            }
        });

        return db;
    }

    @Override
    public Database changeDatabase(Database d) throws SQLException {
        Database old = database;
        database = d;
        return old;
    }


    @Override
    protected String getDatabasesSQL() {
        return getQuery(QueryProvider.QUERY__SHOW_DATABASES);
    }

    @Override
    public String getDatabaseCharsetAndCollateSQL(String databaseName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getViewsSQL(Database database) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getTriggersSQL(Database database) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getFunctionsSQL(Database database) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getEventsSQL(Database database) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getStoredProcsSQL(Database database) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getTablesSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_FULL_TABLES, getParam().getPgSchemaId());
    }

    @Override
    protected String getCharsetSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getCollationSQL(String charset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getEngineSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getColumnInformationSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getRenameTableSQL(String databaseName, String oldTableName, String newTableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getChangeTableTypeSQL(String databaseName, String tableName, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getIndexSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getForeignKeysSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getKeysSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getProfilesStartSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getProfilesShowSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getProfilesStopSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getDescribeColumnSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__DESCRIBE_COLUMN, tableName);
    }

    @Override
    protected String getShowRecordsCountForTableSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    private String createConnectionUrl(ConnectionParams params, Session session, String dbs, TechnologyType type) {
        
        int port = (session != null || params.getPort() < 1) ? SSH_TUNNELING_PORT : params.getPort();
        String address = session != null ? "localhost" : params.getAddress();
        
        String tz = TimeZone.getDefault().getID();
        if (tz == null || tz.trim().isEmpty()) { tz = "UTC"; }
        
        StringBuilder jdbcUrlBuilder = new StringBuilder("jdbc:");
        jdbcUrlBuilder.append(type.getPrefix())
                .append(address).append(':').append(port);
        
        if(params.getDatabase() != null){
            jdbcUrlBuilder.append('/').append(params.getDatabase().get(0));
        }else{ jdbcUrlBuilder.append("/postgres"); }
        
        System.out.println(jdbcUrlBuilder.toString());
        
        boolean hasSessionVariable = false;
//        
//        if (params.isMultiQueries()) {
//            jdbcUrl += "&allowMultiQueries=true";
//        }
//        
//        if (params.getSessionIdleTimeout() > 0) {
//            jdbcUrl += "&sessionVariables=wait_timeout=" + params.getSessionIdleTimeout();
//            hasSessionVariable = true;
//        }
//
//        if (params.isEnabledCustomSqlMode()) {
//            if (hasSessionVariable) {
//                jdbcUrl += ",sql_mode='" + params.getCustomSqlMode() + "'";
//            } else {
//                jdbcUrl += "&sessionVariables=sql_mode='" + params.getCustomSqlMode() + "'";
//                hasSessionVariable = true;
//            }
//        }
//
//        if (params.isUseCompressedProtocol()) {
//            jdbcUrl += "&useCompression=true";
//        }
        
        Properties props = new Properties();
        if (params.getUsername() != null) {
            props.put("user", params.getUsername());
        }
        if (params.getPassword() != null) {
            props.put("password", params.getPassword());
        }
                
        return jdbcUrlBuilder.toString();
    }
    
    /**
     *  Test connection and return product name and version
     * @param params conneciton params
     * @return return product name and version
     * @throws SQLException if any errors
     */
    @Override
    public String testConnection(ConnectionParams params) throws SQLException {
        
        Session session = null;
        try {
            String dbs = "";
            if (params.getDatabase() != null && !params.getDatabase().isEmpty()) {
                for (String d : params.getDatabase()) {
                    dbs += d + ",";
                }
                dbs = dbs.substring(0, dbs.length() - 1);
            }
            
            System.out.println("test connection for database : " + dbs);

            // if connection params has ssh info
            // than need create ssh tunel
            // method return ssh tunel port or current connectionparams port
            session = sshTunneling(params, false);

            Connection conn = DriverManager.getConnection(createConnectionUrl(params, session, "", getTechType()), params.getUsername(), params.getPassword());

            executeInitCommands(conn, params);
            
            return conn.getMetaData().getDatabaseProductName() + " " + conn.getMetaData().getDatabaseProductVersion();

        } catch (JSchException ex) {
            throw new SQLException(ex.getMessage(), ex);

        } finally {
            if (session != null) {
                session.disconnect();
            }
        }
    }

    @Override
    public Connection connect(ConnectionParams params) throws SQLException {
        try {
            if (params != null) {                
                setParam(params);
                // if connection params has ssh info
                // than need create ssh tunel
                // method return ssh tunel session
                Session session = sshTunneling(params, true);

                connection = DriverManager.getConnection(createConnectionUrl(params, session, "", 
                        getTechType()), params.getUsername(), params.getPassword());
                
                executeInitCommands(connection, params);
                
                boolean oldValue = isServiceBusy();
                
                if (!oldValue) {
                    setServiceBusy(true);
                }
                
                for (AbstractDBService.ConnectionListener l: connectionListeners.toArray(new AbstractDBService.ConnectionListener[0])) {
                    l.connected();
                }
                
                if (!oldValue) {
                    setServiceBusy(false);
                }
                
                dbSQLVersionMaj = getDB().getMetaData().getDatabaseMajorVersion();
                dbSQLVersionMin = getDB().getMetaData().getDatabaseMinorVersion();
                dbSQLVersion = getDB().getMetaData().getDatabaseProductVersion();
                
                String query = getQuery(QueryProvider.QUERY__POSTGRESQL_SCHEMA_KIT);
                try (ResultSet rs = (ResultSet) execute(query)) {                    
                    if (rs.next()) {
                        if(rs.getString("db_support").charAt(0) == 't'){
                            params.setPgSchemaId(rs.getInt("schema_id"));
                        }
                    }
                }
                
                return connection;
            }
        } catch (JSchException ex) {
            throw new SQLException(ex.getMessage(), ex);
        }
        
        return null;
    }

    @Override
    public Map<String, Map<String, Object>> loadDBStructure() throws SQLException {
        if (connection == null) {
            connect(param);
        }
        Map<String, Map<String, Object>> data = new LinkedHashMap<>();
        
        List<Database> databases = getParam().getDatabases().isEmpty() ? getDatabases() : getParam().getDatabases();
        for(Database d : databases){
            String databaseName = d.getName();
            data.put(databaseName.toLowerCase(), loadDBStructure(d, true));
        }
        
        return data;
    }

    @Override
    public Map<String, Object> loadDBStructure(String database, boolean force) throws SQLException { 
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }       
    
        
    public Map<String, Object> loadDBStructure(Database database, boolean force) throws SQLException {        
         
        Map<String, Object> databaseData = null;
        if (!force) {
            databaseData = loadDatabaseStructure(database.getName());
            if (databaseData != null) {
                return databaseData;
            }
        }
        
        databaseData = new LinkedHashMap<>();
        
        Map<String, DBTable> tablesMap = new LinkedHashMap<>();
        database.setTables(getTables(database));
        for(DBTable table : database.getTables()){
            tablesMap.put(table.getName().toLowerCase(), table);
        }
        databaseData.put(COLUMN_NAME__TABLES, tablesMap);
                
        databaseData.put(COLUMN_NAME__VIEWS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__PROCEDURES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__FUNCTIONS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__TRIGGERS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__EVENTS, new LinkedHashMap<>());
        
        
        //saveDatabaseStructure(databaseData, database.getName());
        
        return databaseData;        
    }

    @Override
    public Map<String, List<String>> loadDBStructure(String database, String tableName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, List<String>> loadDBStructure(String database, String tableName, boolean force) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSQLOfColumnInformation(Column column, String oldColumnName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropIndexSQL(String database, String table, String index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropColumnSQL(String database, String table, String column) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getForeignKeySQL(String referencingDatabase, String referencingTable, String constraintName, List<String> referencingColumns, String referencedDabatase, String referencedTable, List<String> referencedColumns, String onUpdate, String onDelete) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropDatabaseSQL(String databaseName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateDatabaseSQL(String databaseName, String character, String collate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAlterDatabaseSQL(String databaseName, String character, String collate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropTableSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTruncateTableSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowCreateTableSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getInsertTableSQL(String databaseName, DBTable table) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUpdateTableSQL(String databaseName, DBTable table) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDeleteTableSQL(String databaseName, DBTable table) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSelectTableSQL(String databaseName, DBTable table) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowTableStatusSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateStoredProcedureTemplateSQL(String databaseName, String storedProcedureName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateStoredProcedureWithCursorTemplateSQL(String databaseName, String storedProcedureName, String cursorName, String cursorQuery) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAlterStoredProcedureSQL(String databaseName, String storedProcedureName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropStoredProcedureSQL(String databaseName, String storedProcedureName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowCreateStoredProcedureSQL(String databaseName, String storedProcedureName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateFunctionTemplateSQL(String databaseName, String functionName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAlterFunctionSQL(String databaseName, String functionName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropFunctionSQL(String databaseName, String functionName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowCreateFunctionSQL(String databaseName, String functionName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateTriggerTemplateSQL(String databaseName, String triggerName, String beforeAfter, String triggerEvent, String triggerTable, String comment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateAuditTriggerTemplateSQL(String databaseName, String triggerName, String beforeAfter, String triggerEvent, String triggerTable, String triggerBody, String comment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowTriggersSQL(String database, String table) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAlterTriggerSQL(String databaseName, String storedProcedureName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropTriggerSQL(String databaseName, String triggerName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowCreateTriggerSQL(String databaseName, String triggerName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTriggerDefinitionSQL(String databaseName, String triggerName, String triggerBody) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateEventTemplateSQL(String databaseName, String eventName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropEventSQL(String databaseName, String eventName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowCreateEventSQL(String databaseName, String eventName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAlterEventSQL(String databaseName, String eventName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getRenameEventTemplateSQL(String databaseName, String eventName, String newName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getClearForeignKeyCheckSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateViewTemplateSQL(String databaseName, String viewName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getViewDefinitionSQL(String databaseName, String viewName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCreateViewSQL(String databaseName, String viewName, String viewDefinition) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getAlterViewSQL(String databaseName, String viewName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDropViewSQL(String databaseName, String viewName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowCreateViewSQL(String databaseName, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUsersAccountSQL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUserGlobalPrivSQL(String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDeleteUserSQL(String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShowGrantsSQL(String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isClosed() throws SQLException {
        lastActivity = new Date().getTime();
        return connection == null;
    }
}
