/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import np.com.ngopal.smart.sql.model.*;

/**
 * This is the core services of any kind of database which helps to perform the
 * database query and many other database related actions.
 * <p>
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * <p>
 */
public interface DBService<Technology> {

    public static final String COLUMN_NAME__DB         = "db_name";
    public static final String COLUMN_NAME__TABLE      = "table_name";
    public static final String COLUMN_NAME__COLUMN     = "column_name";
    public static final String COLUMN_NAME__INFO       = "info";
    public static final String COLUMN_NAME__TYPE       = "type";
    public static final String COLUMN_NAME__COLUMNS    = "columns";
    public static final String COLUMN_NAME__INDEXES    = "indexes";
    public static final String COLUMN_NAME__FOREIGNS   = "foreigns";
    public static final String COLUMN_NAME__TABLES     = "tables";
    public static final String COLUMN_NAME__VIEWS      = "views";
    public static final String COLUMN_NAME__PROCEDURES = "procedures";
    public static final String COLUMN_NAME__FUNCTIONS  = "functions";
    public static final String COLUMN_NAME__TRIGGERS   = "triggers";
    public static final String COLUMN_NAME__EVENTS     = "events";
    
    /**
     * Provides the current opened Connection of Database.
     * <p>
     * @return Connection
     * @throws SQLException exception of database connection
     */
    Connection getDB() throws SQLException;

    /**
     * Connects the database with specific ConnectionParameter.
     * <p>
     * @param params ConnectionParams
     * @return Connection
     * @throws SQLException exception of database connection
     */
    Connection connect(ConnectionParams params) throws SQLException;

    
    public boolean isClosed() throws SQLException;
    
    public long getLastActivity();
    
    public boolean isPerformanceSchemaOn();
    
    public String getDbSQLVersion();
    
    public abstract int getDbSQLVersionMaj();
    public abstract int getDbSQLVersionMin();
    public abstract int getDbSQLVersionMic();
    
            
    /**
     * Executes any SQL commands with respective to the opened Connection
     * thread.
     * <p>
     * @param sql String
     * @return returns ResultSet if it is query based other wise integer.
     * @throws SQLException exception of database execute
     */
    Object execute(String sql) throws SQLException;

    Object execute(String sql, boolean needLog) throws SQLException;
    
    /**
     * Executes any SQL commands with respective to the opened Connection
     * thread.
     * <p>
     * @param sql String
     * @return returns ResultSet if it is query based other wise integer.
     * @throws SQLException exception of database execute
     */
    Object execute(String sql, QueryResult result) throws SQLException;
    
    Object execute(String sql, QueryResult result, boolean withWarnings, boolean needLog) throws SQLException;

    
    Map<String, Map<String, Object>> loadDBStructure() throws SQLException;
    
    Map<String, Object> loadDBStructure(String database, boolean force) throws SQLException;
    
    Map<String, List<String>> loadDBStructure(String database, String tableName) throws SQLException;
    Map<String, List<String>> loadDBStructure(String database, String tableName, boolean force) throws SQLException;
    
    /**
     * Provides the List of Database from the existing connection.
     * <p>
     * @return List<Database> list of Database type
     * @throws SQLException sql exception of database
     */
    List<Database> getDatabases() throws SQLException;

    /**
     * Provides the List of Table of Database from the existing connection.
     * <p>
     * @param database Database
     * @return List<Table> list of Table type
     * @throws SQLException sql exception of database
     */
    List<DBTable> getTables(Database d) throws SQLException;

    List<String> getTablesNames(Database database) throws SQLException;
    /**
     * Provides List of View of Database from the existing connection.
     * <p>
     * @param database Database
     * @return List<View> list of View type
     * @throws SQLException sql exception of database
     */
    List<View> getViews(Database d) throws SQLException;

    /**
     * Provides List of Trigger of Database from the existing connection .
     * <p>
     * @param database Database
     * @return List<Trigger>
     * @throws SQLException sql exception of database
     */
    List<Trigger> getTriggers(Database database) throws SQLException;

    /**
     * Provides List of Function of Database from the existing connection .
     * <p>
     * @param database Database
     * @return List<Function>
     * @throws SQLException sql exception of database
     */
    List<Function> getFunctions(Database database) throws SQLException;

    /**
     * Provides List of StoredProc of Database from the existing connection .
     * <p>
     * @param database Database
     * @return List<StoredProc>
     * @throws SQLException sql exception of database
     */
    List<StoredProc> getStoredProcs(Database database) throws SQLException;

    /**
     * Provides List of Event of Database from the existing connection .
     * <p>
     * @param database Database
     * @return Event
     * @throws SQLException sql exception of database
     */
    List<Event> getEvents(Database database) throws SQLException;

    /**
     * Provides the list of charset supported by database.
     * <p>
     * @return List<String>
     */
    List<String> getCharSet() throws SQLException;

    /**
     * Provides the list of collation supported by database.
     * <p>
     * @param charset String
     * @return List<String>
     */
    List<String> getCollation(String charset) throws SQLException;

    /**
     * Provides the list of engines supported by database.
     * <p>
     * @return List<String>
     */
    List<String> getEngine() throws SQLException;

    /**
     * Gives the full information of the relational table with schema
     * definition.
     * <p>
     * @param databaseName String
     * @param tableName    String
     * @return Table
     */
    DBTable getTableSchemaInformation(String databaseName, String tableName) throws SQLException;

    /**
     * Provides the full information of the specified Table 's columns
     * <p>
     * @param databaseName String
     * @param tableName    String
     * @return Set<Column>
     */
    List<Column> getColumnInformation(String databaseName, String tableName) throws SQLException;

    ResultSet getColumnInformationResultSet(String databaseName, String tableName) throws SQLException;

    //int alterTable(String databaseName, String tableName, Map<String, List<String>> columnAndValuesMap) throws SQLException;

    Object renameTable(String databaseName, String oldTableName, String newTableName) throws SQLException;
    Object changeTableType(String databaseName, String tableName, String type) throws SQLException;

    String getCharsetFromCollation(String collation) throws SQLException;

    List<String> getListOfCollationFromSameCollationMember(String Collation) throws SQLException;

    String getSQLOfColumnInformation(Column column, String oldColumnName);

//    String getChangesSQL(Column original, Column updated);
//    String getNewSQL(Column prevColumn, Column newColumn);
    String getDropIndexSQL(String database, String table, String index);
    String getDropColumnSQL(String database, String table, String column);
    //String getCreateSQL(Column column);

//    String getChangesSQL(DBTable original, DBTable updated);
//    String getChangesSQL(DBTable original, DBTable updated, boolean byName);
//    String getNewSQL(DBTable newTable);

    void setIndexes(DBTable table) throws SQLException;
    void setForeignKeys(DBTable table) throws SQLException;

//    String getPrimaryKeySQL(List<Column> original, List<Column> updated);

//    String getIndexSQL(DBTable original, DBTable updated, IndexType type, String indexKeyword);

    String getForeignKeySQL(String referencingDatabase, String referencingTable, String constraintName,
              List<String> referencingColumns, String referencedDabatase,
              String referencedTable, List<String> referencedColumns, String onUpdate, String onDelete);

    Double getExecutionTime(String sql) throws SQLException;

//    String getKeysSQL(String databaseName, String tableName);
    ResultSet getKeys(String databaseName, String tableName) throws SQLException;

    List<Column> getDescribeColumn(String databaseName, String tableName) throws SQLException;

    String getDropDatabaseSQL(String databaseName);

    String getCreateDatabaseSQL(String databaseName, String character, String collate);
    
    String getAlterDatabaseSQL(String databaseName, String character, String collate);

    Object dropDatabase(Database database) throws SQLException;
    
    Object dropIndex(String database, String table, String index) throws SQLException;
    
    Object dropColumn(Column column) throws SQLException;
    
    Object createDatabase(String databaseName, String character, String collate) throws SQLException;
    
    Object alterDatabase(String databaseName, String character, String collate) throws SQLException;

    Object copyTable(String databaseName, String sourceTable, String destinationTable, List<Column> columns, boolean withIndexes, boolean onlyStructure, boolean withTriggers) throws SQLException;
        
    String getDropTableSQL(String databaseName, String tableName);
    
    String getTruncateTableSQL(String databaseName, String tableName);
    
    String getShowCreateTableSQL(String databaseName, String tableName);
    
    String getInsertTableSQL(String databaseName, DBTable table) throws SQLException;
    
    String getUpdateTableSQL(String databaseName, DBTable table) throws SQLException;
    
    String getDeleteTableSQL(String databaseName, DBTable table) throws SQLException;
    
    String getSelectTableSQL(String databaseName, DBTable table) throws SQLException;
    
    String getShowTableStatusSQL(String databaseName, String tableName);

    Object dropTable(DBTable table) throws SQLException;
    Object truncateTable(DBTable table) throws SQLException;

    String getCreateStoredProcedureTemplateSQL(String databaseName, String storedProcedureName);
    
    String getCreateStoredProcedureWithCursorTemplateSQL(String databaseName, String storedProcedureName, String cursorName, String cursorQuery) throws SQLException;

    String getAlterStoredProcedureSQL(String databaseName, String storedProcedureName) throws SQLException;
    
    String getDropStoredProcedureSQL(String databaseName, String storedProcedureName);

    Object dropStoredProcedure(StoredProc storedProcedure) throws SQLException;

    String getShowCreateStoredProcedureSQL(String databaseName, String storedProcedureName);
    
    String getCreateFunctionTemplateSQL(String databaseName, String functionName);

    String getAlterFunctionSQL(String databaseName, String functionName) throws SQLException;
    
    String getDropFunctionSQL(String databaseName, String functionName);

    Object dropFunction(Function function) throws SQLException;
    
    String getShowCreateFunctionSQL(String databaseName, String functionName);

    String getCreateTriggerTemplateSQL(String databaseName, String triggerName, String beforeAfter, String triggerEvent, String triggerTable, String comment);
    String getCreateAuditTriggerTemplateSQL(String databaseName, String triggerName, String beforeAfter, String triggerEvent, String triggerTable, String triggerBody, String comment);

    String getShowTriggersSQL(String database, String table);
    
    String getAlterTriggerSQL(String databaseName, String storedProcedureName) throws SQLException;
    
    String getDropTriggerSQL(String databaseName, String triggerName);
    
    String getShowCreateTriggerSQL(String databaseName, String triggerName);

    Object dropTrigger(Trigger trigger) throws SQLException;
    
    String getRenameTriggerSQL(Trigger trigger, String newName) throws SQLException;
    
    String getTriggerDefinitionSQL(String databaseName, String triggerName, String triggerBody);

    String getCreateEventTemplateSQL(String databaseName, String eventName);

    String getDropEventSQL(String databaseName, String eventName);
    
    String getShowCreateEventSQL(String databaseName, String eventName);

    String getAlterEventSQL(String databaseName, String eventName) throws SQLException;
    
    String getRenameEventTemplateSQL(String databaseName, String eventName, String newName);

    Object renameEvent(Event event, String newName) throws SQLException;

    Object dropEvent(Event event) throws SQLException;

    String getClearForeignKeyCheckSQL();

    String getCreateViewTemplateSQL(String databaseName, String viewName);

    String getViewDefinitionSQL(String databaseName, String viewName);

    String getCreateViewSQL(String databaseName, String viewName, String viewDefinition);
    
    String getAlterViewSQL(String databaseName, String viewName) throws SQLException;

    String getDropViewSQL(String databaseName, String viewName);
    
    String getShowCreateViewSQL(String databaseName, String tableName);

    Object renameView(View view, String newName) throws SQLException;

    Object dropView(View view) throws SQLException;    
    
    String getLoadDataInFileSQL(String file, String databaseName, String tableName, String charset, String priorityType, String dubplicateAction, String fieldsTerminated, String fieldsEnclosed, boolean enclosedOptionally, String fieldsEscaped, String lineTerminated, Integer ignoreLines, List<String> columns);

    String getUsersAccountSQL();
    
    String getUserGlobalPrivSQL(String user);
    
    String getDeleteUserSQL(String user);
    
    String getShowGrantsSQL(String user);
}
