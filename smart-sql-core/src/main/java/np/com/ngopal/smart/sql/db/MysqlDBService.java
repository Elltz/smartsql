package np.com.ngopal.smart.sql.db;

import np.com.ngopal.smart.sql.model.QueryProvider;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.google.inject.Inject;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.TechnologyType;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class MysqlDBService extends AbstractDBService {

    private static int SSH_TUNNELING_PORT = 1234;
    
    private final List<AbstractDBService.ConnectionListener> connectionListeners = new ArrayList<>();
    
    //private TechnologyType technology = TechnologyType.MYSQL;
    @Inject
    public MysqlDBService(SQLLogger logger) {
    }
    
    public void addConnectinListener(AbstractDBService.ConnectionListener listener) {
        if (!connectionListeners.contains(listener)) {
            connectionListeners.add(listener);
        }
    }
    
    public void removeConnectionListener(AbstractDBService.ConnectionListener listener) {
        connectionListeners.remove(listener);
    }

    /**
     *  Test connection and return product name and version
     * @param params conneciton params
     * @return return product name and version
     * @throws SQLException if any errors
     */
    @Override
    public String testConnection(ConnectionParams params) throws SQLException {
        
        Session session = null;
        try {
            String dbs = "";
            if (params.getDatabase() != null && !params.getDatabase().isEmpty()) {
                for (String d : params.getDatabase()) {
                    dbs += d + ",";
                }
                dbs = dbs.substring(0, dbs.length() - 1);
            }
            
            System.out.println("test connection for database : " + dbs);

            // if connection params has ssh info
            // than need create ssh tunel
            // method return ssh tunel port or current connectionparams port
            session = sshTunneling(params, false);

            Connection conn = DriverManager.getConnection(createConnectionUrl(params, session, "", getTechType()), params.getUsername(), params.getPassword());

            executeInitCommands(conn, params);
            
            return conn.getMetaData().getDatabaseProductName() + " " + conn.getMetaData().getDatabaseProductVersion();

        } catch (JSchException ex) {
            throw new SQLException(ex.getMessage(), ex);

        } finally {
            if (session != null) {
                session.disconnect();
            }
        }
    }
        
    
    @Override
    public Connection connect(ConnectionParams params) throws SQLException {
        try {
            if (params != null) {
                
                setParam(params);
                //for database connection selective
//                String dbs = "";
//
//                if (params.getDatabase() != null && !params.getDatabase().isEmpty()) {
//                    for (String d : params.getDatabase()) {
//                        dbs += d + ",";
//                    }
//                    dbs = dbs.substring(0, dbs.length() - 1);
//                }

                // if connection params has ssh info
                // than need create ssh tunel
                // method return ssh tunel session
                Session session = sshTunneling(params, true);

                connection = DriverManager.getConnection(createConnectionUrl(params, session, "", getTechType()), params.getUsername(), params.getPassword());
                
                execute("set @@session.sql_mode = @@global.sql_mode");
                
                executeInitCommands(connection, params);
                
                boolean oldValue = isServiceBusy();
                
                if (!oldValue) {
                    setServiceBusy(true);
                }
                
                for (AbstractDBService.ConnectionListener l: connectionListeners.toArray(new AbstractDBService.ConnectionListener[0])) {
                    l.connected();
                }
                
                if (!oldValue) {
                    setServiceBusy(false);
                }
                
                try (ResultSet rs = (ResultSet) execute(QueryProvider.getInstance().getQuery(
                        QueryProvider.DB__MYSQL, QueryProvider.QUERY__MYSQL_VERSION))) {
                    
                    if (rs.next()) {

                        String s = rs.getString(1);

                        if (s.contains("-")) {
                            s = s.split("-")[0];
                        }
                            
                        String[] ver = s.split("\\.");
                        if (ver.length > 0) {
                            try {dbSQLVersionMaj = Integer.valueOf(ver[0]);} catch (NumberFormatException ex) {}                                
                        } 

                        if (ver.length > 1) {
                            try {dbSQLVersionMin = Integer.valueOf(ver[1]);} catch (NumberFormatException ex) {}                                
                        } 

                        if (ver.length > 2) {
                            try {dbSQLVersionMic = Integer.valueOf(ver[2]);} catch (NumberFormatException ex) {}                                
                        }

                        dbSQLVersion = s;                    
                    }
                }
                
                return connection;
            }
        } catch (JSchException ex) {
            throw new SQLException(ex.getMessage(), ex);
        }
        
        return null;
    }    
    
    @Override
    public Map<String, Object> loadDBStructure(String database, boolean force) throws SQLException {
        
        if (connection == null) {
            connect(param);
        }
        
        log.debug("Start loadDBStructure");
        Map<String, Object> databaseData = null;
        if (!force) {
            databaseData = loadDatabaseStructure(database);
            if (databaseData != null) {
                return databaseData;
            }
        }
        
        databaseData = new LinkedHashMap<>();
        
        databaseData.put(COLUMN_NAME__TABLES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__VIEWS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__PROCEDURES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__FUNCTIONS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__TRIGGERS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__EVENTS, new LinkedHashMap<>());
                
        boolean oldVersion = false;
        try {
            oldVersion = connection.getMetaData().getDatabaseProductVersion().startsWith("5.0");
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        
        String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASE_INFO_5_0);
        if (!oldVersion) {
            query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASE_INFO);
        }
        
        String multiQuery = "SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';";
        multiQuery += String.format(query, database, database, database, database, database, database, database, database, database);
        
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery = multiQuery + ";";
        }
        
        if (!oldVersion) {
            multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_EVENTS), database);
            if (!multiQuery.trim().endsWith(";")) {
                multiQuery += ";";
            }
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_FUNCTIONS), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_STORED_PROCEDURES), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_TRIGGERS), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }

        multiQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_VIEWS), database);
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }
        
        multiQuery += "SET SQL_MODE=@OLD_SQL_MODE;";
                
        try {
            log.debug("Start execute queries");
            QueryResult qr = new QueryResult();
            execute(multiQuery, qr);
            log.debug("Finish execute queries");
            
            int i = 1;
            log.debug("Start collect data DATABASE");
            ResultSet rs = (ResultSet) qr.getResult().get(i);
            i++;
            boolean load = false;
            if (rs != null) {
                while (rs.next()) {
                    loadDatabaseInfo(rs, database, databaseData);
                    load = true;
                }
                
                rs.close();
            }
            log.debug("Finish collect data DATABASE");
            
//            if (force) {
                
            if (!oldVersion) {
                log.debug("Start collect data EVENTS");
                rs = (ResultSet) qr.getResult().get(i);
                i++;
                while (rs.next()) {
                    String eventName = rs.getString(1);
                    Map<String, Map<String, List<String>>> events = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__EVENTS);
                    Map<String, List<String>> event = events.get(eventName);
                    if (event == null) {
                        event = new LinkedHashMap<>();
                        event.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                        events.put(eventName, event);
                    }
                }
                rs.close();
                log.debug("Finish collect data EVENTS");
            }

            log.debug("Start collect data FUNCTIONS");
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String functionName = rs.getString(1);
                Map<String, Map<String, List<String>>> functions = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__FUNCTIONS);
                Map<String, List<String>> function = functions.get(functionName);
                if (function == null) {
                    function = new LinkedHashMap<>();
                    function.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    functions.put(functionName, function);
                }
            }
            rs.close();
            log.debug("Finish collect data FUNCTIONS");

            log.debug("Start collect data PROCEDURES");
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String tableName = rs.getString(1);
                Map<String, Map<String, List<String>>> propcedures = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__PROCEDURES);
                Map<String, List<String>> propcedure = propcedures.get(tableName);
                if (propcedure == null) {
                    propcedure = new LinkedHashMap<>();
                    propcedure.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    propcedures.put(tableName, propcedure);
                }
            }
            rs.close();
            log.debug("Finish collect data PROCEDURES");

            log.debug("Start collect data TRIGGERS");
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String triggerName = rs.getString(1);
                Map<String, Map<String, List<String>>> triggers = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__TRIGGERS);
                Map<String, List<String>> trigger = triggers.get(triggerName);
                if (trigger == null) {
                    trigger = new LinkedHashMap<>();
                    trigger.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    triggers.put(triggerName, trigger);
                }
            }
            rs.close();
            log.debug("Finish collect data TRIGGERS");

            log.debug("Start collect data VIEWS");
            rs = (ResultSet) qr.getResult().get(i);
            i++;
            while (rs.next()) {
                String viewName = rs.getString(1);
                Map<String, Map<String, List<String>>> views = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__VIEWS);
                Map<String, List<String>> view = views.get(viewName);
                if (view == null) {
                    view = new LinkedHashMap<>();
                    view.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    views.put(viewName, view);
                }
            }
            rs.close();
            log.debug("Finish collect data VIEWS");
//            }
            
            
            if (!load) {
                return null;
            }
            
        } catch (SQLException ex) {
            databaseData = null;
            throw ex;
        }
        
        saveDatabaseStructure(databaseData, database);
        
        return databaseData;
    }
    
    
    @Override
    public Map<String, List<String>> loadDBStructure(String database, String tableName) throws SQLException {
        return loadDBStructure(database, tableName, false);
    }
    
    @Override
    public Map<String, List<String>> loadDBStructure(String database, String tableName, boolean force) throws SQLException {
        
        Map<String, Object> databaseData = force ? null : loadDatabaseStructure(database);
        if (databaseData != null) {
            return ((Map<String, Map<String, List<String>>>)databaseData.get(COLUMN_NAME__TABLES)).get(tableName.toLowerCase());
        }
        
        databaseData = new LinkedHashMap<>();
        
        databaseData.put(COLUMN_NAME__TABLES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__VIEWS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__PROCEDURES, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__FUNCTIONS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__TRIGGERS, new LinkedHashMap<>());
        databaseData.put(COLUMN_NAME__EVENTS, new LinkedHashMap<>());
        
        String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASE_INFO_5_0);
        try {
            if (!connection.getMetaData().getDatabaseProductVersion().startsWith("5.0")) {
                query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DATABASE_INFO);
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        
        try {
            execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=''");            
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        
        try 
            (ResultSet rs = (ResultSet) execute(String.format(query, database, database, database, database, database, database, database, database, database))) {
            while (rs.next()) {
                //if (tableName.equals(rs.getString(COLUMN_NAME__TABLE))) {
                    loadDatabaseInfo(rs, database, databaseData);
                //}
            }
        } catch (SQLException ex) {
            databaseData = null;
            throw ex;
        } finally {
            try {
                execute("SET SQL_MODE=@OLD_SQL_MODE");            
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        
        saveDatabaseStructure(databaseData, database);
        
        return ((Map<String, Map<String, List<String>>>)databaseData.get(COLUMN_NAME__TABLES)).get(tableName.toLowerCase());
    }
    
    @Override
    public Map<String, Map<String, Object>> loadDBStructure() {
        Map<String, Map<String, Object>> data = new LinkedHashMap<>();
        
        String query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DB_INFO_5_0);
        try {
            if (!connection.getMetaData().getDatabaseProductVersion().startsWith("5.0")) {
                query = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SELECT_DB_INFO);
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        
        try {
            execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=''");            
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        
        try 
            (ResultSet rs = (ResultSet) execute(query)) {

            while (rs.next()) {
                String databaseName = rs.getString(COLUMN_NAME__DB);
                
                Map<String, Object> databaseData = data.get(databaseName);
                if (databaseData == null) {
                    databaseData = new LinkedHashMap<>();

                    databaseData.put(COLUMN_NAME__TABLES, new LinkedHashMap<>());
                    databaseData.put(COLUMN_NAME__VIEWS, new LinkedHashMap<>());
                    databaseData.put(COLUMN_NAME__PROCEDURES, new LinkedHashMap<>());
                    databaseData.put(COLUMN_NAME__FUNCTIONS, new LinkedHashMap<>());
                    databaseData.put(COLUMN_NAME__TRIGGERS, new LinkedHashMap<>());
                    databaseData.put(COLUMN_NAME__EVENTS, new LinkedHashMap<>());

                    data.put(databaseName, databaseData);
                }

                loadDatabaseInfo(rs, databaseName, databaseData);
            }
        } catch (SQLException ex) {
            data = null;
        } finally {
            try {
                execute("SET SQL_MODE=@OLD_SQL_MODE");            
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        
        if (data != null) {
            for (String database: data.keySet()) {
                saveDatabaseStructure(data.get(database), database);
            }
        }
        
        return data;
    }
    
    
    private void loadDatabaseInfo(ResultSet rs, String databaseName, Map<String, Object> databaseData) throws SQLException {
        String tableName = rs.getString(COLUMN_NAME__TABLE);
        String columnName = rs.getString(COLUMN_NAME__COLUMN);

        int type = rs.getInt(COLUMN_NAME__TYPE);

        //log.debug("001 - " + type);
        switch (type) {                            
            case 1:
            case 2:    
            case 8:
            case 9:

                Map<String, Map<String, List<String>>> tables = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__TABLES);
                Map<String, List<String>> table = tables.get(tableName);
                if (table == null) {
                    table = new LinkedHashMap<>();
                    table.put(COLUMN_NAME__COLUMNS, new ArrayList<>());
                    table.put(COLUMN_NAME__INDEXES, new ArrayList<>());
                    table.put(COLUMN_NAME__FOREIGNS, new ArrayList<>());
                    table.put(COLUMN_NAME__INFO, new ArrayList<>());

                    tables.put(tableName, table);
                }

                switch (type) {
                    case 1:
                        table.get(COLUMN_NAME__COLUMNS).add(columnName);
                        break;

                    case 2:
                        table.get(COLUMN_NAME__INDEXES).add(columnName);
                        break;

                    case 8:
                        table.get(COLUMN_NAME__FOREIGNS).add(columnName);
                        break;

                    case 9:
                        table.get(COLUMN_NAME__INFO).add(columnName);
                        break;
                }
                break;

            case 3:                            
                Map<String, Map<String, List<String>>> views = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__VIEWS);
                Map<String, List<String>> view = views.get(tableName);
                if (view == null) {
                    view = new LinkedHashMap<>();
                    view.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    views.put(tableName, view);
                }

                view.get(COLUMN_NAME__COLUMNS).add(columnName);
                break;

            case 4:
                Map<String, Map<String, List<String>>> propcedures = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__PROCEDURES);
                Map<String, List<String>> propcedure = propcedures.get(tableName);
                if (propcedure == null) {
                    propcedure = new LinkedHashMap<>();
                    propcedure.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    propcedures.put(tableName, propcedure);
                }
                break;

            case 5:            
                Map<String, Map<String, List<String>>> functions = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__FUNCTIONS);
                Map<String, List<String>> function = functions.get(tableName);
                if (function == null) {
                    function = new LinkedHashMap<>();
                    function.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    functions.put(tableName, function);
                }
                break;

            case 6:    
                Map<String, Map<String, List<String>>> triggers = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__TRIGGERS);
                Map<String, List<String>> trigger = triggers.get(tableName);
                if (trigger == null) {
                    trigger = new LinkedHashMap<>();
                    trigger.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    triggers.put(tableName, trigger);
                }

                trigger.get(COLUMN_NAME__COLUMNS).add(columnName);
                break;

            case 7:  
                Map<String, Map<String, List<String>>> events = (Map<String, Map<String, List<String>>>) databaseData.get(COLUMN_NAME__EVENTS);
                Map<String, List<String>> event = events.get(tableName);
                if (event == null) {
                    event = new LinkedHashMap<>();
                    event.put(COLUMN_NAME__COLUMNS, new ArrayList<>());

                    events.put(tableName, event);
                }

                event.get(COLUMN_NAME__COLUMNS).add(columnName);
                break;

        }
        
        //log.debug("002");
    }
    
    
    public static String createConnectionUrl(ConnectionParams params, Session session, String dbs, TechnologyType type) {
        
        int port = session != null ? SSH_TUNNELING_PORT : params.getPort();
        String address = session != null ? "0.0.0.0" : params.getAddress();
        
        String tz = TimeZone.getDefault().getID();
        if (tz == null || tz.trim().isEmpty()) {
            tz = "UTC";
        }
        
        String jdbcUrl = String.format("jdbc:%s%s:%d/%s?zeroDateTimeBehavior=convertToNull&serverTimezone=" + tz + "&useLocalSessionState=true", type.getPrefix(),
            address,
            port <= 0 ? type.getDefaultPort() : port, dbs);
        
        boolean hasSessionVariable = false;
        
        if (params.isMultiQueries()) {
            jdbcUrl += "&allowMultiQueries=true";
        }
        
        if (params.getSessionIdleTimeout() > 0) {
            jdbcUrl += "&sessionVariables=wait_timeout=" + params.getSessionIdleTimeout();
            hasSessionVariable = true;
        }

        if (params.isEnabledCustomSqlMode()) {
            if (hasSessionVariable) {
                jdbcUrl += ",sql_mode='" + params.getCustomSqlMode() + "'";
            } else {
                jdbcUrl += "&sessionVariables=sql_mode='" + params.getCustomSqlMode() + "'";
                hasSessionVariable = true;
            }
        }

        if (params.isUseCompressedProtocol()) {
            jdbcUrl += "&useCompression=true";
        }
        
        Properties props = new Properties();
        if (params.getUsername() != null) {
            props.put("user", params.getUsername());
        }
        if (params.getPassword() != null) {
            props.put("password", params.getPassword());
        }
                
        return jdbcUrl;
    }
        
    public static Session sshTunneling(ConnectionParams params, boolean increasePort) throws JSchException {
        
        if (params.isUseSshTunneling() && params.getSshUser() != null && !params.getSshUser().isEmpty() &&
                params.getSshHost() != null && !params.getSshHost().isEmpty()) {
            
            JSch jsch = new JSch();
            Session session;
                    
            if (params.isUseSshPrivateKey()) {
                jsch.addIdentity(params.getSshPrivateKey(), params.getSshPassword());
                session = jsch.getSession(params.getSshUser(), params.getSshHost(), params.getSshPort());
            } else {
                session = jsch.getSession(params.getSshUser(), params.getSshHost(), params.getSshPort());
                session.setPassword(params.getSshPassword());
            }
            
            session.setConfig("StrictHostKeyChecking", "no");

            session.setDaemonThread(true);
            session.connect();

            SSH_TUNNELING_PORT++;
            
            session.setPortForwardingL("0.0.0.0", SSH_TUNNELING_PORT, params.getAddress(), params.getPort());
            
            if (!increasePort) {
                SSH_TUNNELING_PORT--;
            }
            
            
            return session;
        }
        
        return null;
    }        
    
    protected void reconnect() throws SQLException {
        connection = connect(param);
    }

    private static String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, queryId);
    }
    
    private static String getFormatedQuery(String queryId, Object...values) {
        return String.format(getQuery(queryId), values);
    }
    
    
    @Override
    protected String getDatabasesSQL() {
        return getQuery(QueryProvider.QUERY__SHOW_DATABASES);//"SHOW DATABASES";
    }
    
    @Override
    public String getDatabaseCharsetAndCollateSQL(String databaseName) {
        return getFormatedQuery(QueryProvider.QUERY__DEFAULT_CHARSET_AND_COLLATION, databaseName);//"SELECT DEFAULT_CHARACTER_SET_NAME, DEFAULT_COLLATION_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + databaseName + "'";
    }
    

    @Override
    protected String getTablesSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_FULL_TABLES, database.getName());//"SHOW FULL TABLES FROM `" + database.getName() + "` WHERE table_type = 'BASE TABLE'";
    }

    @Override
    protected String getChangeDBSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__CHANGE_DATABASE, database.getName());//"USE " + database.getName();
    }
    
    @Override
    protected String getShowRecordsCountForTableSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_RECORDS_COUNT_FOR_TABLE, databaseName, tableName);
    }

    @Override
    protected String getViewsSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_VIEWS, database.getName());//"SELECT `TABLE_NAME` FROM `INFORMATION_SCHEMA`.`TABLES` WHERE `TABLE_SCHEMA` = '" + database.getName() + "' AND `TABLE_TYPE` = 'VIEW';";
    }

    @Override
    protected String getTriggersSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_TRIGGERS, database.getName());//"SHOW TRIGGERS FROM `" + database.getName() + "`; ";
    }
    
    @Override
    public String getShowTriggersSQL(String database, String table) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_TRIGGERS_FOR_TABLE, database, table);//"SHOW TRIGGERS FROM `" + database + "` LIKE '" + table + "';";
    }

    @Override
    public String getTriggerDefinitionSQL(String databaseName, String triggerName, String triggerBody) {
        return getFormatedQuery(QueryProvider.QUERY__ALTER_TRIGGER, databaseName, triggerName, triggerBody);
    }
    
    @Override
    protected String getFunctionsSQL(Database database) {
//        return "SELECT `SPECIFIC_NAME` FROM `INFORMATION_SCHEMA`.`ROUTINES` WHERE `ROUTINE_SCHEMA` = '" + database.getName() + "' AND ROUTINE_TYPE = `FUNCTION`; ";
        return getFormatedQuery(QueryProvider.QUERY__SHOW_FUNCTIONS, database.getName());//"SELECT `SPECIFIC_NAME` FROM `INFORMATION_SCHEMA`.`ROUTINES` WHERE `ROUTINE_SCHEMA` = '" + database.getName() + "'AND `ROUTINE_TYPE` = 'FUNCTION'";
    }

    @Override
    protected String getEventsSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_EVENTS, database.getName());//"SELECT `EVENT_NAME` FROM `INFORMATION_SCHEMA`.`EVENTS` WHERE `EVENT_SCHEMA` = '" + database.getName() + "' ORDER BY EVENT_NAME; ";
    }

    @Override
    protected String getStoredProcsSQL(Database database) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_STORED_PROCEDURES, database.getName());//"SELECT `SPECIFIC_NAME` FROM `INFORMATION_SCHEMA`.`ROUTINES` WHERE `ROUTINE_SCHEMA` = '" + database.getName() + "' AND ROUTINE_TYPE = 'PROCEDURE';";
    }

    @Override
    public String getCharsetSQL() {
        return getQuery(QueryProvider.QUERY__SHOW_CHARSET);//"SHOW CHARSET;";
    }

    @Override
    protected String getCollationSQL(String charset) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_COLLATION_FOR_CHARSET, charset);//"SHOW COLLATION WHERE `Charset` LIKE '" + charset + "';";
//        return " SHOW COLLATION WHERE `Charset` LIKE 'latin1';";
    }

    @Override
    protected String getEngineSQL() {
        return getQuery(QueryProvider.QUERY__SHOW_ENGINES);//"SHOW ENGINES;";
    }

    // Duplicate
//    @Override
//    protected String getTableStatusSQL(String databaseName, String tableName) {
//        return getFormatedQuery(QueryProvider.QUERY__SHOW_TABLE_STATUS, databaseName, tableName);//"SHOW TABLE STATUS FROM `" + databaseName + "` LIKE '" + tableName + "'; ";
//    }

    @Override
    protected String getColumnInformationSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_FULL_FIELDS, databaseName, tableName);//"SHOW FULL FIELDS FROM `" + databaseName + "`.`" + tableName + "`; ";
    }

    // @TODO Not user, need remove
//    @Override
//    protected String getAlterTableSQL(String databaseName, String tableName,
//              Map<String, List<String>> columnAndValuesMap) {
//        StringBuilder sqlBuilder = new StringBuilder();
//        sqlBuilder.append("ALTER TABLE `" + databaseName + "`.`" + tableName + "`");
//        Iterator iterator = columnAndValuesMap.entrySet().iterator();
//        int i = 1;
//        while (iterator.hasNext()) {
//            Map.Entry mapEntry = (Map.Entry)iterator.next();
//            System.out.println("The altettable key is: " + mapEntry.getKey()
//                      + ", altettable value is :" + mapEntry.getValue());
//            if (mapEntry.getValue() == null) {
//                sqlBuilder.append(" " + mapEntry.getKey() + " ");
//            } else {
//                String values = "";
//                List<String> sdfv = (List<String>)mapEntry.getValue();
//                for (String s : sdfv) {
//                    values += s + " ";
//                }
//                System.out.println("sizess:" + sdfv.size() + " " + values);
//
//                sqlBuilder.append(" " + mapEntry.getKey() + " = " + values);
//            }
//
//            if (columnAndValuesMap.size() != i) {
//                sqlBuilder.append(" , ");
//            } else {
//                sqlBuilder.append(";");
//            }
//            i++;
//        }
//        return sqlBuilder.toString();
//    }

    @Override
    protected String getRenameTableSQL(String databaseName, String oldTableName, String newTableName) {
        return getFormatedQuery(QueryProvider.QUERY__RENAME_TABLE, databaseName, oldTableName, databaseName, newTableName);//"RENAME TABLE `" + databaseName + "`.`" + oldTableName + "` TO `" + databaseName + "`.`" + newTableName + "`;";
    }
    
    @Override
    protected String getChangeTableTypeSQL(String databaseName, String tableName, String type) {
        return getFormatedQuery(QueryProvider.QUERY__CHANGE_TABLE_TYPE, databaseName, tableName, type);
    }

    @Override
    public String getSQLOfColumnInformation(Column column, String oldColumnName) {

        if (oldColumnName == null) {
            oldColumnName = column.getName();
        }
        String sql = "CHANGE `" + oldColumnName + "` `" + column.getName() + "` ";
        if (column.getDataType() != null) {
            sql += column.getDataType().getName().toUpperCase() + " ";
            System.out.println("has length :" + column.getDataType().hasLength());
            if (column.getDataType().hasLength()) {
                sql += "(" + column.getDataType().getLength() + ") ";
            }
        }
        if (column.isUnsigned()) {
            sql += "UNSIGNED ";
        }

        sql += getColumnDefaultValue(column);
        
        if (column.isZeroFill()) {
            sql += "ZEROFILL ";
        }
        
        // TODO Need check this
        if (column.isOnUpdate() && column.getDefaults() != null && column.getDefaults().length() > 0) {
            sql += "ON UPDATE " + column.getDefaults() + " ";
        }
        if (column.isNotNull()) {
            sql += "NOT NULL ";
        }

        if (column.isAutoIncrement()) {
            sql += "AUTO_INCREMENT ";
        }

        if (column.getCharacterSet() != null) {
            sql += "CHARACTER SET " + column.getCharacterSet() + " ";
        }
        if (column.getCollation() != null) {
            sql += "COLLATE " + column.getCollation() + " ";
        }

        if (column.getComment() != null && column.getComment().length() > 0) {
            sql += "COMMENT '" + column.getComment() + "' ";
        }

        return sql;
    }
    
    public long getRowCount(String database, String table) {
        try
            (ResultSet rs = (ResultSet) execute(getShowRecordsCountForTableSQL(database, table))) {
            
            if (rs != null && rs.next()) {
                return rs.getLong(1);
            }
        } catch (SQLException ex) {            
        }
        
        return 0;
    }

    public String primaryKeySQL(Column col, boolean selected) {
        String sql = "";
//                            col.setPrimaryKey(new_val);
        int primaryKeyExists = 0;

        List<String> names = new ArrayList();
        for (Column sc : col.getTable().getColumns()) {
            if (sc.isPrimaryKey()) {
                if (!names.contains(sc.getName())) {
                    if (col.getName().equals(sc.getName())) {
                        continue;
                    }
                    primaryKeyExists++;
                    System.out.println("true pr:" + sc.getName());
                    names.add(sc.getName());

                }

            }

        }
        if (selected) {
            if (primaryKeyExists > 0) {
                sql = " DROP PRIMARY KEY, ADD PRIMARY KEY (";
                for (String name : names) {
                    sql += "`" + name + "`,";

                }
                col.setPrimaryKey(true);
                primaryKeyExists++;
                sql += "`" + col.getName() + "`);";
            } else {
                col.setPrimaryKey(true);
                primaryKeyExists++;
                sql = " ADD PRIMARY KEY (`" + col.getName() + "`)";
            }
        } else {
            col.setPrimaryKey(false);
            if (primaryKeyExists > 0) {
                sql = " DROP PRIMARY KEY, ADD PRIMARY KEY (";
                for (int i = 0; i < names.size(); i++) {
                    sql += "`" + names.get(i) + "`";
                    if (i == names.size() - 1) {
                        sql += ");";
                    } else {
                        sql += ",";
                    }

                }
            } else {
                sql = " DROP PRIMARY KEY;";
            }
        }
        System.out.println("SQl :" + sql);
        return sql;
    }

    public static String getChangesSQL(Column original, Column updated) {

        String originalDefaults = original.getDefaults() != null ? original.getDefaults() : "";
        if (original.getExtra() != null) {
            originalDefaults += " " + original.getExtra();
        }
        
        String updatedDefaults = updated.getDefaults() != null ? updated.getDefaults() : "";
        if (updated.getExtra() != null) {
            updatedDefaults += " " + updated.getExtra();
        }
                
        if (updated.getUpdateIndex() == -1
            && (original.getName() == null ? updated.getName() == null : original.getName().equals(updated.getName()))
            && (original.getDataType() == updated.getDataType() || original.getDataType().equals(updated.getDataType()))
            && original.isUnsigned() == updated.isUnsigned() 
            && originalDefaults.toUpperCase().trim().equals(updatedDefaults.toUpperCase().trim())
            && original.isOnUpdate() == updated.isOnUpdate() 
            && (updated.isPrimaryKey() || original.isNotNull() == updated.isNotNull())
            && original.isAutoIncrement() == updated.isAutoIncrement() 
            && (original.getCharacterSet() == null ? updated.getCharacterSet() == null || !isHasCharset(original.getDataType().getName()) : original.getCharacterSet().equals(updated.getCharacterSet()))
            && (original.getCollation() == null ? updated.getCollation() == null || !isHasCharset(original.getDataType().getName()) : original.getCollation().equals(updated.getCollation())) 
            && (original.getComment() == null ? updated.getComment() == null : original.getComment().equals(updated.getComment()))) {
            return "";
        }
        
        // TODO strange equals...
        if (!original.equals(updated)) {
            
            // TODO need refactor ordering
            String orderSQL = "";
            if (updated.getUpdateIndex() > -1) {
                if (updated.getUpdateIndex() == 0) {
                    orderSQL += "FIRST";
                    
                } else {
                    for (Column column : updated.getTable().getColumns()) {
                        log.debug("column: {} name:{}", column.getOrderIndex(), column.getName());
                        if (column.getOrderIndex() == updated.getOrderIndex() - 1) {
                            orderSQL += "AFTER " + "`" + column.getName() + "`";
                            break;
                        }
                    }
                }
            }
            
            return "CHANGE `" + original.getName() + "` " + columnSQL(updated, "%s %s%s%s%s%s%s%s%s%s%s%s") + (orderSQL.isEmpty() ? "" : " " + orderSQL);            
        } else {
            return "";
        }
    }
    
    private static String enumOrSetToString(Set<String> set) {
        if (set != null) {
            String result = "";
            for (String s: set) {
                result += "'" + s + "', ";
            }

            return "(" + result.substring(0, result.length() - 2) + ")";
        }
        
        return "";
    }
    
    private static String getColumnDefaultValue(Column column) {
        if (column.getDefaults() != null && !column.getDefaults().isEmpty()) {
                
            switch (column.getDataType().getName()) {
                case "BIGINT":
                case "DECIMAL":
                case "DOUBLE":
                case "FLOAT":
                case "INT":
                case "NUMERIC":
                case "BIT":
                case "BOOL":
                case "BOOLEAN":
                case "SMALLINT":
                case "TINYINT":
                    return String.format("DEFAULT %s", column.getDefaults().trim());

                default:
                    
                    // TODO Check this
                    if (column.getDefaults().toUpperCase().contains("CURRENT_TIMESTAMP")) {
                        return String.format("DEFAULT %s", column.getDefaults().trim());
                    } else {
                        return String.format("DEFAULT '%s'", column.getDefaults().trim());
                    }
            }
        }
        
        return "";
    }
    
    public static String getNewSQL(Column prevColumn, Column newColumn) {
                
        //mandatory to write this sql part of column
        String orderSQL = "";
        if (prevColumn == null) {
            System.out.println("first");
            orderSQL += "FIRST";
        } else {
            orderSQL += "AFTER `" + prevColumn.getName() + "`";
        }
        
        String columnSQL = columnSQL(newColumn, "ADD COLUMN %s %s%s%s%s%s%s%s%s%s%s%s");
        
        return columnSQL + (orderSQL.isEmpty() ? "" : " " + orderSQL);
    }
    
    

    @Override
    protected TechnologyType getTechType() {
        return TechnologyType.MYSQL;
    }

    
    private static String columnSQL(Column column, String format) {
        String colNameSQL = String.format("`%s`", column.getName());
        String dataTypeSQL = String.format("%s", column.getDataType() != null ? column.getDataType().getName() : null);
        String lengthSQL;
        
        if (column.getDataType() != null) {
            
            if (column.getDataType().isEnumOrSet()) {
                lengthSQL = enumOrSetToString(column.getDataType().getList());
            } else {
                
                if (column.getDataType().getLength() != null && column.getDataType().getLength() > 0) {
                    lengthSQL = column.getDataType().getPrecision() == null ? String.format("(%d)", column.getDataType().getLength()) : String.format("(%d, %d)", column.getDataType().getLength(), column.getDataType().getPrecision());
                } else if ("VARCHAR".equalsIgnoreCase(column.getDataType().getName())){
                    lengthSQL = "(255)";
                } else {
                    lengthSQL = "";
                }
                    
            }
        } else {
            lengthSQL = "";
        }
        
        boolean hasCharset = (column.getDataType() != null ? !isHasCharset(column.getDataType().getName()) : false);
        
        String unsignedSQL = column.isUnsigned() ? "UNSIGNED" : "";
        String zeroFillSQL = column.isZeroFill() ? "ZEROFILL " : "";
        String onUpdateSQL = column.isOnUpdate() ? "ON UPDATE CURRENT_TIMESTAMP" : "";
        String autoIncrementSQL = column.isAutoIncrement() ? "AUTO_INCREMENT" : "";
        String charsetSQL = hasCharset || column.getCharacterSet() == null || column.getCharacterSet().isEmpty() || "[default]".equals(column.getCharacterSet()) ? "" : String.format("CHARACTER SET %s", column.getCharacterSet());
        String collationSQL = hasCharset || column.getCollation() == null || column.getCollation().isEmpty() || "[default]".equals(column.getCollation()) ? "" : String.format("COLLATE %s", column.getCollation());
        String defaultSQL = getColumnDefaultValue(column);
        String notNullSQL = column.isNotNull() ? "NOT NULL" : "NULL";

        String commentSQL = column.getComment() == null ? "" : String.format("COMMENT '%s'", column.getComment());
        
        return String.format(format, 
            colNameSQL, dataTypeSQL, lengthSQL, 
            unsignedSQL.isEmpty() ? "" :  " " + unsignedSQL,
            zeroFillSQL.isEmpty() ? "" :  " " + zeroFillSQL, 
            onUpdateSQL.isEmpty() ? "" :  " " + onUpdateSQL, 
            autoIncrementSQL.isEmpty() ? "" :  " " + autoIncrementSQL, 
            charsetSQL.isEmpty() ? "" :  " " + charsetSQL, 
            collationSQL.isEmpty() ? "" :  " " + collationSQL,
            defaultSQL.isEmpty() ? "" :  " " + defaultSQL, 
            notNullSQL.isEmpty() ? "" :  " " + notNullSQL, 
            commentSQL.isEmpty() ? "" :  " " + commentSQL);
    }
    
    
    public static String columnSQLForAuditLog(Column column, String format) {
        String colNameSQL = String.format("`%s`", column.getName());
        String dataTypeSQL = String.format("%s", column.getDataType() != null ? column.getDataType().getName() : null);
        String lengthSQL;
        
        if (column.getDataType() != null) {
            
            if (column.getDataType().isEnumOrSet()) {
                lengthSQL = enumOrSetToString(column.getDataType().getList());
            } else {
                
                if (column.getDataType().getLength() != null && column.getDataType().getLength() > 0) {
                    lengthSQL = column.getDataType().getPrecision() == null ? String.format("(%d)", column.getDataType().getLength()) : String.format("(%d, %d)", column.getDataType().getLength(), column.getDataType().getPrecision());
                } else if ("VARCHAR".equalsIgnoreCase(column.getDataType().getName())){
                    lengthSQL = "(255)";
                } else {
                    lengthSQL = "";
                }
                    
            }
        } else {
            lengthSQL = "";
        }
        
        boolean hasCharset = (column.getDataType() != null ? !isHasCharset(column.getDataType().getName()) : false);
        
        String unsignedSQL = column.isUnsigned() ? "UNSIGNED" : "";
        String zeroFillSQL = column.isZeroFill() ? "ZEROFILL " : "";
        String onUpdateSQL = "";
        String autoIncrementSQL = "";
        String charsetSQL = hasCharset || column.getCharacterSet() == null || column.getCharacterSet().isEmpty() || "[default]".equals(column.getCharacterSet()) ? "" : String.format("CHARACTER SET %s", column.getCharacterSet());
        String collationSQL = hasCharset || column.getCollation() == null || column.getCollation().isEmpty() || "[default]".equals(column.getCollation()) ? "" : String.format("COLLATE %s", column.getCollation());
        String defaultSQL = "";
        String notNullSQL = column.isNotNull() ? "NOT NULL" : "NULL";

        String commentSQL = column.getComment() == null ? "" : String.format("COMMENT '%s'", column.getComment());
        
        return String.format(format, 
            colNameSQL, dataTypeSQL, lengthSQL, 
            unsignedSQL.isEmpty() ? "" :  " " + unsignedSQL,
            zeroFillSQL.isEmpty() ? "" :  " " + zeroFillSQL, 
            onUpdateSQL.isEmpty() ? "" :  " " + onUpdateSQL, 
            autoIncrementSQL.isEmpty() ? "" :  " " + autoIncrementSQL, 
            charsetSQL.isEmpty() ? "" :  " " + charsetSQL, 
            collationSQL.isEmpty() ? "" :  " " + collationSQL,
            defaultSQL.isEmpty() ? "" :  " " + defaultSQL, 
            notNullSQL.isEmpty() ? "" :  " " + notNullSQL, 
            commentSQL.isEmpty() ? "" :  " " + commentSQL);
    }
    
    private static Map<Index, List<String>> getIndexesData(DBTable newTable, IndexType type) {
        Map<Index, List<String>> indexes = new LinkedHashMap<>();
        
        for (Index index : newTable.getIndexes()) {
            if (index.getType() == type && index.getColumns() != null) {                
                List<String> columns = new ArrayList<>();
                for (Column col : index.getColumns()) {
                    columns.add(col.getName());
                }
                
                indexes.put(index, columns);
            }
        }
        
        return indexes;
    }
    
    public static String getNewSQL(DBTable newTable, boolean withDatabase) {
        String sql = getNewSQL(newTable);
        if (!withDatabase) {
            sql = sql.replace("`" + newTable.getDatabase().getName() + "`.", "");
        }
        
        return sql;
    }
            
    public static String getNewSQL(DBTable newTable) {
     
        List<String> primaryColumns = new ArrayList<>();
        
        List<Column> columnsList = newTable.getColumns().stream().sorted((Column o1, Column o2) -> {
            return o1.getOrderIndex() - o2.getOrderIndex();
        }).collect(Collectors.toList());
        
        String columnsSQL = "";
        for (Column col: columnsList) {
            // ignore empty columns
            if (col.getName() != null && !col.getName().trim().isEmpty()) {
                String sql = columnSQL(col, "    %s %s%s%s%s%s%s%s%s%s%s%s");

                if (!sql.isEmpty()) {
                    columnsSQL += sql + ",\n";
                }

                if (col.isPrimaryKey()) {
                    primaryColumns.add(col.getName());
                }
            }
        }
        
        if (columnsSQL.endsWith(",\n")) {
            columnsSQL = columnsSQL.substring(0, columnsSQL.length() - 2);
        }
        
        String primarysql = "";
        if (!primaryColumns.isEmpty()) {
            primarysql += "    PRIMARY KEY (" + getCommaSeparated(primaryColumns) + ")";
        }

        
        String engineSQL = newTable.getEngineType() == null ? "" : String.format("ENGINE=%s", newTable.getEngineType()) + "\n";
        String collationSQL = newTable.getCollation() == null ? "" : String.format("COLLATE=%s", newTable.getCollation()) + "\n";
        String charsetSQL = newTable.getCharacterSet() == null ? "" : String.format("CHARACTER SET=%s", newTable.getCharacterSet()) + "\n";
        
        String advancedSQL = "";        
       
        if (newTable.getAdvanced() != null) {
           
            String autoIncrementSQL = 
                    newTable.getAdvanced().getAutoIncrement() == -1 ? "" : String.format("AUTO_INCREMENT=%d", newTable.getAdvanced().getAutoIncrement()) + "\n";
            String checksumSQL = 
                    newTable.getAdvanced().getChecksum() == null ? "" : String.format("CHECKSUM=%d", newTable.getAdvanced().getChecksum().ordinal()) + "\n";
            String avgRowLengthSQL = 
                    newTable.getAdvanced().getAvgRowLength() == -1 ? "" : String.format("AVG_ROW_LENGTH=%d", newTable.getAdvanced().getAvgRowLength()) + "\n";
            String commentSQL = 
                    newTable.getAdvanced().getComment() == null ? "" : String.format("COMMENT='%s'", newTable.getAdvanced().getComment()) + "\n";
            String maxRowsSQL = 
                    newTable.getAdvanced().getMaxRows() == -1 ? "" : String.format("MAX_ROWS=%d", newTable.getAdvanced().getMaxRows()) + "\n";
            String minRowsSQL = 
                    newTable.getAdvanced().getMinRows() == -1 ? "" : String.format("MIN_ROWS=%d", newTable.getAdvanced().getMinRows()) + "\n";
            String delayKeyWriteSQL = 
                    newTable.getAdvanced().getDelayKeyWrite() == null ? "" : String.format("DELAY_KEY_WRITE=%d", newTable.getAdvanced().getDelayKeyWrite().ordinal()) + "\n";
            String rowFormatSQL = 
                    newTable.getAdvanced().getRowformat() == null ? "" : String.format("ROW_FORMAT=%s", newTable.getAdvanced().getRowformat().getDisplayValue()) + "\n";
            
            if (!checksumSQL.isEmpty() || !autoIncrementSQL.isEmpty() || !avgRowLengthSQL.isEmpty() || 
                    !maxRowsSQL.isEmpty() || !minRowsSQL.isEmpty() || !minRowsSQL.isEmpty() || 
                    !delayKeyWriteSQL.isEmpty() || !autoIncrementSQL.isEmpty()) {
                
                advancedSQL = String.format("%s %s %s %s %s %s %s %s", checksumSQL, autoIncrementSQL,
                              avgRowLengthSQL,
                              commentSQL, maxRowsSQL, minRowsSQL, delayKeyWriteSQL, rowFormatSQL);
            }
        }
        

        
        Map<Index, List<String>> uniqueIndexes = getIndexesData(newTable, IndexType.UNIQUE_TEXT);        
        
        String uniqueSQL = "";
        for (Index updateIndex: uniqueIndexes.keySet()) {
            
            List<String> columns = uniqueIndexes.get(updateIndex);            
            if (columns != null && !columns.isEmpty()) {
                uniqueSQL += "    UNIQUE INDEX `" + indexName(updateIndex) + "`(" + getCommaSeparated(updateIndex, columns) + "),\n";
            }
        }
        
        if (uniqueSQL.endsWith(",\n")) {
            uniqueSQL = uniqueSQL.substring(0, uniqueSQL.length() - 2);
        }
        
        
        Map<Index, List<String>> fulltextIndexes = getIndexesData(newTable, IndexType.FULL_TEXT);
        
        String fulltextSQL = "";
        for (Index updateIndex: fulltextIndexes.keySet()) {
            
            List<String> columns = fulltextIndexes.get(updateIndex);            
            if (columns != null && !columns.isEmpty()) {
                fulltextSQL += "    FULLTEXT INDEX `" + indexName(updateIndex) + "`(" + getCommaSeparated(updateIndex, columns) + "),\n";
            }
        }
        
        if (fulltextSQL.endsWith(",\n")) {
            fulltextSQL = fulltextSQL.substring(0, fulltextSQL.length() - 2);
        }
        
        Map<Index, List<String>> indexes = getIndexesData(newTable, null);
        
        
        Map<Index, List<String>> spatialIndexes = getIndexesData(newTable, IndexType.SPATIAL);        
        
        String spatialSQL = "";
        for (Index updateIndex: spatialIndexes.keySet()) {
            
            List<String> columns = spatialIndexes.get(updateIndex);            
            if (columns != null && !columns.isEmpty()) {
                spatialSQL += "    SPATIAL INDEX `" + indexName(updateIndex) + "`(" + getCommaSeparated(updateIndex, columns) + "),\n";
            }
        }
        
        if (spatialSQL.endsWith(",\n")) {
            spatialSQL = spatialSQL.substring(0, spatialSQL.length() - 2);
        }
        
        String indexSQL = "";
        for (Index updateIndex: indexes.keySet()) {
            
            List<String> columns = indexes.get(updateIndex);            
            if (columns != null && !columns.isEmpty()) {
                indexSQL += "    INDEX `" + indexName(updateIndex) + "`(" + getCommaSeparated(updateIndex, columns) + "),\n";
            }
        }
        
        if (indexSQL.endsWith(",\n")) {
            indexSQL = indexSQL.substring(0, indexSQL.length() - 2);
        }
        
        
        String foreignKeysSQL = "";
        for (ForeignKey fk: newTable.getForeignKeys()) {            
            if (fk.getColumns() != null) {
                List<String> columns = new ArrayList<>();
                for (Column col : fk.getColumns()) {
                    columns.add(col.getName());
                }

                List<String> refColumns = new ArrayList<>();
                if (fk.getReferenceColumns() != null) {
                    for (Column col : fk.getReferenceColumns()) {
                        refColumns.add(col.getName());
                    }
                }

                foreignKeysSQL += fk.getName() != null && fk.getName().isEmpty() ? "" : "    CONSTRAINT `" + fk.getName() + "` FOREIGN KEY (" + getCommaSeparated(columns) + ") "
                    + "REFERENCES  `" + fk.getReferenceTable() + "`("
                    + getCommaSeparated(refColumns) + ") " 
                    + (fk.getOnUpdate() == null ||fk.getOnUpdate().isEmpty() ? "" : "ON UPDATE " + fk.getOnUpdate() + " ") 
                    + (fk.getOnDelete() == null || fk.getOnDelete().isEmpty() ? "" : "ON DELETE " + fk.getOnDelete()) + ",\n";
            }
        }
        
        if (!foreignKeysSQL.isEmpty()) {
            foreignKeysSQL = foreignKeysSQL.substring(0, foreignKeysSQL.length() - 2);
        }
        
       
        String otherSql = columnsSQL + (columnsSQL.isEmpty() ? "" : ",\n") + primarysql + (primarysql.isEmpty() ? "" : ",\n") + 
                indexSQL + (indexSQL.isEmpty() ? "" : ",\n") +
                spatialSQL + (spatialSQL.isEmpty() ? "" : ",\n") +
                foreignKeysSQL + (foreignKeysSQL.isEmpty() ? "" : ",\n") + 
                uniqueSQL + (uniqueSQL.isEmpty() ? "" : ",\n") + 
                fulltextSQL;
        
        otherSql = otherSql.trim();
        
        if (otherSql.endsWith(",")) {
            otherSql = otherSql.substring(0, otherSql.length() - 1);
        }
        return getFormatedQuery(QueryProvider.QUERY__CREATE_TABLE, 
                    newTable.getDatabase().getName(),
                    newTable.getName(), 
                    "    " + otherSql, 
                    engineSQL.isEmpty() ? "" : "\n" + engineSQL, 
                    charsetSQL.isEmpty() ? "" : charsetSQL, 
                    collationSQL.isEmpty() ? "" : collationSQL, 
                    advancedSQL.isEmpty() ? "" : advancedSQL.trim());
    }
    
    
    public static String getChangesSQL(DBTable original, DBTable updated) {

        String columnsSQL = "";
        
        Column prevColumn = null;
        Map<String, Column> updatedOldColumnsNames = new HashMap<>();
        
        List<Column> updatedList = new ArrayList<>(updated.getColumns());
        Collections.sort(updatedList, (Column o1, Column o2) -> {
            return o1.getOrderIndex() - o2.getOrderIndex();
        });
        
        for (Column col : updatedList) {
            if (col.getName() != null && col.getDataType() != null) {
                Column origColumn = original.getColumnByName(col.getOriginalName());
                String sql = origColumn != null ? getChangesSQL(origColumn, col) : getNewSQL(prevColumn, col);

                if (!sql.isEmpty()) {
                    columnsSQL += sql + ", ";
                }
                prevColumn = col;

                updatedOldColumnsNames.put(col.getOriginalName(), col);                
            }
        }

        String dropedSQl = "";
        // check removed columns
        for (Column c: original.getColumns()) {
            
            Column column = updatedOldColumnsNames.get(c.getName());
            // if column didnt present - than it was deleted
            if (column == null) {
                String sql = "DROP COLUMN `" + c.getName() + "`";

                if (!sql.equals("")) {
                    dropedSQl += sql + ", ";
                }
            }
        }
        
        columnsSQL = dropedSQl + columnsSQL;
        
        if (columnsSQL.endsWith(", ")) {
            columnsSQL = columnsSQL.substring(0, columnsSQL.length() - 2);
        }
        
        String tableSQL = "";
         String tableNameSQL = "";
        
        if (original.isChanged(updated)) {            
           
            String engineSQL = "";
            String collationSQL = "";
            String charsetSQL = "";
            String autoIncrementSQL = "";
            String checksumSQL = "";
            String avgRowLengthSQL = "";
            String commentSQL = "";
            String maxRowsSQL = "";
            String minRowsSQL = "";
            String delayKeyWriteSQL = "";
            String rowFormatSQL = "";
            String advancedSQL = "";
            
            if (!original.getName().equals(updated.getName())) {
                tableNameSQL = updated.getName() == null ? "" : String.format("RENAME TABLE `%s`.`%s` TO `%s`.`%s`;",
                          original.getDatabase().getName(), original.getName(), updated.getDatabase().getName(),
                          updated.getName());
            }
            
            if (!Objects.equals(original.getEngineType(), updated.getEngineType())) {
                engineSQL = updated.getEngineType() == null ? "" : String.format("ENGINE=%s",
                          updated.getEngineType());
            }
            
            if (original.getCollation() != null && updated.getCollation() != null) {
                if (!original.getCollation().equals(updated.getCollation())) {

                    collationSQL = updated.getCollation() == null  || "[default]".equals(updated.getCollation()) ? "" : String.format("COLLATE=%s",
                              updated.getCollation());
                }
            }

            if (original.getCharacterSet() != null) {

                if (!original.getCharacterSet().equals(updated.getCharacterSet())) {
                    charsetSQL = updated.getCharacterSet() == null || "[default]".equals(updated.getCharacterSet()) ? "" : String.format("CHARSET=%s",
                              updated.getCharacterSet());
                }
            } else {
                if (updated.getCharacterSet() != null) {
                    charsetSQL = updated.getCharacterSet() == null   || "[default]".equals(updated.getCharacterSet()) ? "" : String.format("CHARSET=%s",
                              updated.getCharacterSet());
                }
            }
            
            if (original.getAdvanced() != null && updated.getAdvanced() != null) {
                if (!original.getAdvanced().equals(updated.getAdvanced())) {

                    System.out.println("change in advance");
                    
                    if (!Objects.equals(original.getAdvanced().getAutoIncrement(), updated.getAdvanced().getAutoIncrement()) && updated.getAdvanced().getAutoIncrement() >= 0) {
                        autoIncrementSQL = String.format("AUTO_INCREMENT=%d", updated.getAdvanced().getAutoIncrement());
                    }
                    if (!Objects.equals(original.getAdvanced().getChecksum(), updated.getAdvanced().getChecksum())) {
                        checksumSQL = String.format("CHECKSUM=%d", updated.getAdvanced().getChecksum().ordinal());

                    }
                    if (!Objects.equals(original.getAdvanced().getAvgRowLength(), updated.getAdvanced().getAvgRowLength()) && updated.getAdvanced().getAvgRowLength() >= 0) {
                        avgRowLengthSQL = String.format("AVG_ROW_LENGTH=%d", updated.getAdvanced().getAvgRowLength());
                    }
                    if (!Objects.equals(original.getAdvanced().getComment(), updated.getAdvanced().getComment())) {
                        System.out.println("commentchanged");
                        commentSQL = String.format("COMMENT='%s'", updated.getAdvanced().getComment());

                    }
                    if (!Objects.equals(original.getAdvanced().getMaxRows(), updated.getAdvanced().getMaxRows()) && updated.getAdvanced().getAvgRowLength() >= 0) {
                        maxRowsSQL = String.format("MAX_ROWS=%d", updated.getAdvanced().getMaxRows());
                    }
                    if (!Objects.equals(original.getAdvanced().getMinRows(), updated.getAdvanced().getMinRows()) && updated.getAdvanced().getAvgRowLength() >= 0) {
                        minRowsSQL = String.format("MIN_ROWS=%d", updated.getAdvanced().getMinRows());
                    }
                    if (!Objects.equals(original.getAdvanced().getDelayKeyWrite(), updated.getAdvanced().getDelayKeyWrite())) {
                        delayKeyWriteSQL = String.format("DELAY_KEY_WRITE=%d",
                                  updated.getAdvanced().getDelayKeyWrite().ordinal());

                    }
                    
                    if (!Objects.equals(original.getAdvanced().getRowformat(), updated.getAdvanced().getRowformat())) {
                        rowFormatSQL = String.format("ROW_FORMAT=%s",
                                  updated.getAdvanced().getRowformat().getDisplayValue());

                    }
                    
                    advancedSQL = String.format("%s %s %s %s %s %s %s %s", checksumSQL, autoIncrementSQL,
                              avgRowLengthSQL,
                              commentSQL, maxRowsSQL, minRowsSQL, delayKeyWriteSQL, rowFormatSQL);
                }
            }
            
            tableSQL = (tableNameSQL.isEmpty() ? "" : tableNameSQL + " ") + String.format(
                      "ALTER TABLE `%s`.`%s`%s%s%s%s%s",
                      updated.getDatabase().getName(),
                      updated.getName(), 
                      "%s", 
                      engineSQL.isEmpty() ? "" : " " + engineSQL, 
                      charsetSQL.isEmpty() ? "" : " " + charsetSQL, 
                      collationSQL.isEmpty() ? "" : " " + collationSQL, 
                      advancedSQL.isEmpty() ? "" : " " + advancedSQL);
        }
        
        
        List<Column> orgCol = new ArrayList();
        List<Column> orgUpd = new ArrayList();
        orgCol.addAll(findPrimaryColumnsFromIndexOrAllTable(original));
        orgUpd.addAll(updated.getColumns());
            
        String primarySQL = getPrimaryKeySQL(original, updated, orgCol, orgUpd, true);
        String primaryDropSQL = getPrimaryKeySQL(original, updated, orgCol, orgUpd, false);
        
        String fulltextSQL = getIndexSQL(original, updated, IndexType.FULL_TEXT, "FULLTEXT", true);
        String fulltextDropSQL = getIndexSQL(original, updated, IndexType.FULL_TEXT, "FULLTEXT", false);
        
        String uniqueSQL = getIndexSQL(original, updated, IndexType.UNIQUE_TEXT, "UNIQUE", true);
        String uniqueDropSQL = getIndexSQL(original, updated, IndexType.UNIQUE_TEXT, "UNIQUE", false);
        
        String spatialSQL = getIndexSQL(original, updated, IndexType.SPATIAL, "SPATIAL", true);
        String spatialDropSQL = getIndexSQL(original, updated, IndexType.SPATIAL, "SPATIAL", false);
        
        String keyIndexSQL = getIndexSQL(original, updated, null, "", true);
        String keyIndexDropSQL = getIndexSQL(original, updated, null, "", false);
        
        
        List<ForeignKey> originalFks = new ArrayList<>(original.getForeignKeys());
        List<ForeignKey> updatedFks =  new ArrayList<>(updated.getForeignKeys());
        
        // find for chages for droping
        Map<String, List<String>> originalFksMap = new LinkedHashMap<>();
        Map<String, List<String>> updatedFksMap = new LinkedHashMap<>();
        
        Map<String, List<String>> originalRefFksMap = new LinkedHashMap<>();
        Map<String, List<String>> updatedRefFksMap = new LinkedHashMap<>();
        
        Map<String, ForeignKey> updatedFksCacheMap = new LinkedHashMap<>();
        Map<String, ForeignKey> originalFksCacheMap = new LinkedHashMap<>();
        
        List<ForeignKey> newForeignKeys = new ArrayList<>();
        for (ForeignKey fk: updatedFks) {   
            
            if (fk.getName() == null || fk.getName().isEmpty()) {
                newForeignKeys.add(fk);
            } else {
                List<String> columns = new ArrayList<>();
                if (fk.getColumns() != null) {
                    for (Column col : fk.getColumns()) {
                        columns.add(col.getName());
                    }
                }

                updatedFksMap.put(fk.getName(), columns);
                
                List<String> refColumns = new ArrayList<>();
                if (fk.getReferenceColumns() != null) {
                    for (Column col : fk.getReferenceColumns()) {
                        refColumns.add(col.getName());
                    }
                }

                updatedRefFksMap.put(fk.getName(), refColumns); 
                
                updatedFksCacheMap.put(fk.getName(), fk);
            }
        }
        
        for (ForeignKey fk: originalFks) {          
            List<String> columns = new ArrayList<>();
            for (Column col : fk.getColumns()) {
                columns.add(col.getName());
            }

            originalFksMap.put(fk.getName(), columns);
            
            List<String> refColumns = new ArrayList<>();
            if (fk.getReferenceColumns() != null) {
                for (Column col : fk.getReferenceColumns()) {
                    refColumns.add(col.getName());
                }
            }

            originalRefFksMap.put(fk.getName(), refColumns);
            originalFksCacheMap.put(fk.getName(), fk);
        }
        
        String dropForeignKeysSQL = "";
        String foreignKeysSQL = "";
        for (String updateFKName: updatedFksMap.keySet()) {
            
            ForeignKey updatedFk = updatedFksCacheMap.get(updateFKName);
            ForeignKey originalFk = originalFksCacheMap.get(updateFKName);
            
            List<String> updatedColumns = updatedFksMap.get(updateFKName);
            List<String> originalColumns = originalFksMap.remove(updateFKName);
            
            List<String> updatedRefColumns = updatedRefFksMap.get(updateFKName);
            List<String> originalRefColumns = originalRefFksMap.remove(updateFKName);
            
            // if null or not equals we ned drop old (if null) and add new
            if (originalColumns == null || originalFk == null || !Arrays.equals(updatedColumns.toArray(), originalColumns.toArray())
                    || !Arrays.equals(updatedRefColumns.toArray(), originalRefColumns.toArray())
                    || !Objects.equals(originalFk.getReferenceDatabase(), updatedFk.getReferenceDatabase())
                    || !Objects.equals(originalFk.getReferenceTable(), updatedFk.getReferenceTable())
                    || !Objects.equals(originalFk.getOnUpdate(), updatedFk.getOnUpdate())
                    || !Objects.equals(originalFk.getOnDelete(), updatedFk.getOnDelete())) {
                
                if (originalColumns != null || originalFk != null) {
                    dropForeignKeysSQL += "DROP FOREIGN KEY `" + updateFKName + "`, ";
                }
                
                foreignKeysSQL += "ADD CONSTRAINT `" + updateFKName + "` FOREIGN KEY (" + getCommaSeparated(updatedColumns) + ") "
                        + "REFERENCES  `" + updatedFk.getReferenceDatabase() + "`.`" + updatedFk.getReferenceTable() + "`("
                        + getCommaSeparated(updatedRefColumns) + ") " 
                        + (updatedFk.getOnUpdate() == null ||updatedFk.getOnUpdate().isEmpty() ? "" : "ON UPDATE " + updatedFk.getOnUpdate() + " ") 
                        + (updatedFk.getOnDelete() == null || updatedFk.getOnDelete().isEmpty() ? "" : "ON DELETE " + updatedFk.getOnDelete()) + ", ";
            }
        }
        
        // check originalFks if exists some entries - we must remove them
        for (String updateFKName: originalFksMap.keySet()) {
            dropForeignKeysSQL += "DROP FOREIGN KEY `" + updateFKName + "`, ";
        }
        
        // add new FK
        for (ForeignKey fk: newForeignKeys) {
            
            List<String> columns = new ArrayList<>();
            if (fk.getColumns() != null) {
                for (Column col : fk.getColumns()) {
                    columns.add(col.getName());
                }
            }
            
            List<String> refColumns = new ArrayList<>();
            if (fk.getReferenceColumns() != null) {
                for (Column col : fk.getReferenceColumns()) {
                    refColumns.add(col.getName());
                }
            }
            
            foreignKeysSQL += "ADD FOREIGN KEY (" + getCommaSeparated(columns) + ") "
                        + "REFERENCES  `" + fk.getReferenceDatabase() + "`.`" + fk.getReferenceTable() + "`("
                    + getCommaSeparated(refColumns) + ") " 
                    + (fk.getOnUpdate() == null || fk.getOnUpdate().isEmpty() ? "" : "ON UPDATE " + fk.getOnUpdate() + " ") 
                    + (fk.getOnDelete() == null || fk.getOnDelete().isEmpty() ? "" : "ON DELETE " + fk.getOnDelete()) + ", ";
        }
        
        if (!dropForeignKeysSQL.isEmpty()) {
            dropForeignKeysSQL = dropForeignKeysSQL.substring(0, dropForeignKeysSQL.length() - 2);
        }
        
        if (!foreignKeysSQL.isEmpty()) {
            foreignKeysSQL = foreignKeysSQL.substring(0, foreignKeysSQL.length() - 2);
        }
        
        if (!dropForeignKeysSQL.isEmpty()) {
            dropForeignKeysSQL = String.format("ALTER TABLE `%s`.`%s` %s;", updated.getDatabase().getName(),  updated.getName(), dropForeignKeysSQL);
        }
        
        
        String otherSql = columnsSQL + (columnsSQL.isEmpty() ? "" : ", ") + 
            primaryDropSQL + (primaryDropSQL.isEmpty() ? "" : ", ") + 
            uniqueDropSQL + (uniqueDropSQL.isEmpty() ? "" : ", ") + 
            fulltextDropSQL + (fulltextDropSQL.isEmpty() ? "" : ",") +
            spatialDropSQL + (spatialDropSQL.isEmpty() ? "" : ",") +
            keyIndexDropSQL + (keyIndexDropSQL.isEmpty() ? "" : ", ") +
            primarySQL + (primarySQL.isEmpty() ? "" : ", ") + 
            foreignKeysSQL + (foreignKeysSQL.isEmpty() ? "" : ", ") + 
            uniqueSQL + (uniqueSQL.isEmpty() ? "" : ", ") + 
            fulltextSQL + (fulltextSQL.isEmpty() ? "" : ",") +
            spatialSQL + (spatialSQL.isEmpty() ? "" : ",") +
            keyIndexSQL + (keyIndexSQL.isEmpty() ? "" : ", ");
        
        String query = "";
        
        
        if (!otherSql.trim().isEmpty() && tableSQL.isEmpty()) {
            if (!otherSql.trim().isEmpty()) {
                query = String.format("ALTER TABLE `%s`.`%s`%s", updated.getDatabase().getName(), updated.getName(), " " + otherSql);
            }
        } else {
            query = otherSql.trim().isEmpty() ? "" : String.format(tableSQL, " " + otherSql);            
        }
        
        if (query.trim().isEmpty() && !tableNameSQL.isEmpty()) {
            query = tableNameSQL;
        }
        
        query = dropForeignKeysSQL + query;        
        query = query.trim();
        return query.endsWith(",") ? query.substring(0, query.length() - 1) : query;
    }
    
    private static List<Column> findPrimaryColumnsFromIndexOrAllTable(DBTable table) {
        if (table.getIndexes() != null) {
            for (Index idx: table.getIndexes()) {
                if (idx.getType() == IndexType.PRIMARY) {
                    return idx.getColumns();
                }
            }
        } 
        return table.getColumns();
    }

    @Override
    protected String getIndexSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_INDEX_FOR_TABLE, databaseName, tableName);//"SHOW INDEXES FROM `" + databaseName + "`.`" + tableName + "`; ";
    }

    @Override
    protected String getForeignKeysSQL(String databaseName, String tableName) {
        
        return getFormatedQuery(QueryProvider.QUERY__GET_FOREIGN_KEYS_FOR_TABLE, databaseName, tableName);
//                "SELECT "
//                + "a.constraint_name as contraintName, "
//                + "a.column_name as columnName, "
//                + "a.REFERENCED_TABLE_SCHEMA as referenceDatabase, "
//                + "a.referenced_table_name as referenceTable, "
//                + "a.referenced_column_name as referenceColumn, "
//                + "b.UPDATE_RULE as onUpdate, "
//                + "b.DELETE_RULE as onDelete "
//            + "FROM INFORMATION_SCHEMA.`KEY_COLUMN_USAGE` a "
//                + "JOIN INFORMATION_SCHEMA.`REFERENTIAL_CONSTRAINTS` b ON "
//                    + "a.constraint_schema = b.constraint_schema AND a.constraint_name =b.constraint_name AND a.table_name = b.table_name "
//            + "WHERE a.TABLE_SCHEMA='" + databaseName + "' AND a.table_name = '" + tableName + "' AND referenced_column_name IS NOT NULL";
    }
    
    @Override
    public String getKeysSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_KEYS_FOR_TABLE, databaseName, tableName);// "SHOW KEYS FROM `" + databaseName + "`.`" + tableName + "`; ";
    }

    public static String getPrimaryKeySQL(DBTable originalTable, DBTable updatedTable, List<Column> original, List<Column> updated, boolean add) {
        
        String primarySQL = "";
        
        Index originalIndex = null;
        if (originalTable.getIndexes() != null) {
            for (Index idx: originalTable.getIndexes()) {
                if (idx.getType() == IndexType.PRIMARY) {
                    originalIndex = idx;
                    break;
                }
            }
        }
        
        Index updatedIndex = null;
        if (updatedTable.getIndexes() != null) {
            for (Index idx: updatedTable.getIndexes()) {
                if (idx.getType() == IndexType.PRIMARY) {
                    updatedIndex = idx;
                    break;
                }
            }
        }
        
        
        List<String> primaryKeyColumnUpdated = new ArrayList();
        List<String> primaryKeyColumnOriginal = new ArrayList();
        
        boolean hasInc = false;
        for (Column column : updated) {
            if (column.isPrimaryKey()) {
                if (column.isAutoIncrement()) {
                    hasInc = true;
                    primaryKeyColumnUpdated.add(0, column.getName());
                } else {
                    primaryKeyColumnUpdated.add(column.getName());
                }
            }
        }
        
        for (Column column : original) {
            if (column.isPrimaryKey()) {
                primaryKeyColumnOriginal.add(column.getName());
            }
        }
        
        boolean needUpdate = false;
        if (primaryKeyColumnOriginal.size() == primaryKeyColumnUpdated.size()) {
            // first that columns the same
            for (String s: primaryKeyColumnOriginal) {
                if (!primaryKeyColumnUpdated.contains(s)) {
                    needUpdate = true;
                    break;
                }
            }
            // then if we have column with autoinc - must be at first position
            if (!needUpdate && hasInc) {
                for (int i = 0; i < primaryKeyColumnOriginal.size(); i++) {
                    if (!primaryKeyColumnUpdated.get(i).equals(primaryKeyColumnOriginal.get(i))) {
                        needUpdate = true;
                        break;
                    }
                }
            }
            
            if (!needUpdate) {
                for (String idx: primaryKeyColumnUpdated) {
                    int originalLength = originalIndex != null ? originalIndex.getColumnLength(idx) : 0;
                    int updatedLength = updatedIndex != null ? updatedIndex.getColumnLength(idx) : 0;
                 
                    if (originalLength != updatedLength) {
                        needUpdate = true;
                        break;
                    }
                }
            }
        } else {
            needUpdate = true;
        }
        
        // if not equals we need drop old (if empty) and add new
        if (needUpdate) {                

            if (!add && !primaryKeyColumnOriginal.isEmpty()) {
                primarySQL += "DROP PRIMARY KEY, ";
            }

            if (add && !primaryKeyColumnUpdated.isEmpty()) {
                primarySQL += "ADD PRIMARY KEY (" + getCommaSeparated(updatedIndex, primaryKeyColumnUpdated) + "), ";
            }
        }
        
        if (!primarySQL.isEmpty()) {
            primarySQL = primarySQL.substring(0, primarySQL.length() - 2);
        }
       
        return primarySQL;
    }

    private static String indexName(Index index) {
        String indexName = index.getName();
        if (indexName == null || indexName.trim().isEmpty()) {
            
            indexName = "idx";
            if (index.getColumns() != null && !index.getColumns().isEmpty()) {
                
                for (Column c: index.getColumns()) {
                    indexName += "_" + c.getName().toLowerCase();
                }
            }
        }
        
        return indexName;
    }
    
    public static String getIndexSQL(DBTable original, DBTable updated, IndexType type, String indexKeyword, boolean add) {
        
        Map<Index, List<String>> originalIndexes = new LinkedHashMap<>();
        Map<Index, List<String>> updatedIndexes = new LinkedHashMap<>();
        
        for (Index index : updated.getIndexes()) {
            if (index.getType() == type) {                
                List<String> columns = new ArrayList<>();
                if (index.getColumns() != null) {
                    for (Column col : index.getColumns()) {
                        columns.add(col.getName());
                    }
                }
                                
                updatedIndexes.put(index, columns);
            }
        }
        
        for (Index index : original.getIndexes()) {
            if (index.getType() == type) {
                List<String> columns = new ArrayList<>();
                if (index.getColumns() != null) {
                    for (Column col : index.getColumns()) {
                        columns.add(col.getName());
                    }
                }
                
                originalIndexes.put(index, columns);
            }
        }
        
        String uniqueSQL = "";
        
        for (Index updateIndex: updatedIndexes.keySet()) {
            
            String indexName = indexName(updateIndex);
            
            List<String> updatedColumns = updatedIndexes.get(updateIndex);
            
            Index originIndex = null;
            List<String> originalColumns = null;
            for (Index idx: originalIndexes.keySet())  {
                if (indexName.equals(indexName(idx))) {
                    originIndex = idx;
                    originalColumns = originalIndexes.remove(idx);
                    break;
                }
            }
            
            boolean changed = false;
            if (updatedColumns != null && originalColumns != null && Arrays.equals(updatedColumns.toArray(), originalColumns.toArray())) {
                
                for (int i = 0; i < updatedColumns.size(); i++) {
                    int originalLength = originIndex != null ? originIndex.getColumnLength(originalColumns.get(i)) : 0;
                    int updatedLength = updateIndex != null ? updateIndex.getColumnLength(updatedColumns.get(i)) : 0;
                 
                    if (originalLength != updatedLength) {
                        changed = true;
                        break;
                    }
                }                
                
            } else {
                changed = true;
            }            
            
            // if null or not equals we ned drop old (if null) and add new
            if (changed) {                
                
                if (!add && originalColumns != null) {
                    uniqueSQL += "DROP INDEX `" + indexName + "`, ";
                }
                
                if (add) {
                    uniqueSQL += "ADD " + indexKeyword + " INDEX `" + indexName + "` (" + getCommaSeparated(updateIndex, updatedColumns) + "), ";
                }
            }
        }
        
        if (!add) {
            // check originalIndexes if exists some entries - we must remove them
            for (Index originalIndex: originalIndexes.keySet()) {
                uniqueSQL += "DROP INDEX `" + indexName(originalIndex) + "`, ";
            }
        }
        
        if (!uniqueSQL.isEmpty()) {
            uniqueSQL = uniqueSQL.substring(0, uniqueSQL.length() - 2);
        }
        
        return uniqueSQL;
    }
    
    public String primaryKeySQL(List<Column> data, Column col, Boolean selected) {
        String sql = "";
//                            col.setPrimaryKey(new_val);
        int primaryKeyExists = 0;

        List<String> names = new ArrayList();
        for (Column sc : data) {
            if (sc.isPrimaryKey()) {
                if (!names.contains(sc.getName())) {
                    if (col.getName().equals(sc.getName())) {
                        continue;
                    }
                    primaryKeyExists++;
                    System.out.println("true pr:" + sc.getName());
                    names.add(sc.getName());

                }

            }

        }
        if (selected) {
            if (primaryKeyExists > 0) {
                sql = " DROP PRIMARY KEY, ADD PRIMARY KEY (";
                for (String name : names) {
                    sql += "`" + name + "`,";

                }
                col.setPrimaryKey(true);
                primaryKeyExists++;
                sql += "`" + col.getName() + "`);";
            } else {
                col.setPrimaryKey(true);
                primaryKeyExists++;
                sql = " ADD PRIMARY KEY (`" + col.getName() + "`)";
            }
        } else {
            col.setPrimaryKey(false);
            if (primaryKeyExists > 0) {
                sql = " DROP PRIMARY KEY, ADD PRIMARY KEY (";
                for (int i = 0; i < names.size(); i++) {
                    sql += "`" + names.get(i) + "`";
                    if (i == names.size() - 1) {
                        sql += ");";
                    } else {
                        sql += ",";
                    }

                }
            } else {
                sql = " DROP PRIMARY KEY;";
            }
        }
        System.out.println("SQl :" + sql);
        return sql;
    }

    @Override
    public String getForeignKeySQL(String referencingDatabase, String referencingTable, String constraintName,
              List<String> referencingColumns, String referencedDabatase,
              String referencedTable, List<String> referencedColumns, String onUpdate, String onDelete) {
        String referencingColumnsSQL = "";
        String referencedColumnsSQL = "";
        return "ALTER TABLE  `" + referencingDatabase + "`.`" + referencingTable + "` ADD FOREIGN KEY (  `" + referencingColumnsSQL + "` ) REFERENCES  `" + referencedDabatase + "`.`" + referencedTable + "` ("
                  + "`" + referencedColumnsSQL + "`) " + onUpdate + " " + onDelete
                  + ";";
    }

    @Override
    protected String getProfilesStartSQL() {
        return getQuery(QueryProvider.QUERY__ENABLE_PROFILING);
    }

    @Override
    protected String getProfilesShowSQL() {
        return getQuery(QueryProvider.QUERY__SHOW_PROFILES);
    }

    @Override
    protected String getProfilesStopSQL() {
        return getQuery(QueryProvider.QUERY__DISABLE_PROFILING);
    }

    @Override
    protected String getDescribeColumnSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__DESCRIBE_COLUMN, databaseName, tableName);//"DESCRIBE `" + databaseName + "`.`" + tableName + "`";
    }

    @Override
    public String getDropDatabaseSQL(String databaseName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_DATABASE, databaseName);//"DROP DATABASE IF EXISTS `" + databaseName + "`";
    }

    @Override
    public String getCreateDatabaseSQL(String databaseName, String character, String collate) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_DATABASE, databaseName, character != null ? " CHARACTER SET " + character + (collate != null ? " COLLATE " + collate : "") : "");//"CREATE DATABASE `" + databaseName + "`" + (character != null ? " CHARACTER SET " + character + (collate != null ? " COLLATE " + collate : "") : "");
    }
    
    @Override
    public String getAlterDatabaseSQL(String databaseName, String character, String collate) {
        return getFormatedQuery(QueryProvider.QUERY__ALTER_DATABASE, databaseName, character != null ? " CHARACTER SET " + character + (collate != null ? " COLLATE " + collate : "") : "");//"ALTER DATABASE `" + databaseName + "`" + (character != null ? " CHARACTER SET " + character + (collate != null ? " COLLATE " + collate : "") : "");
    }
        
    @Override
    public String getDropTriggerSQL(String databaseName, String triggerName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_TRIGGER, (databaseName != null ? databaseName + "`.`" : "") + triggerName);//"DROP TRIGGER IF EXISTS `" + (databaseName != null ? databaseName + "`.`" : "") + triggerName + "`";
    }
    
    @Override
    public String getShowCreateTriggerSQL(String databaseName, String triggerName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_TRIGGER, (databaseName != null ? databaseName + "`.`" : "") + triggerName);//"SHOW CREATE TRIGGER `" + (databaseName != null ? databaseName + "`.`" : "") + triggerName + "`";
    }
    
    @Override
    public String getAlterTriggerSQL(String databaseName, String triggerName) throws SQLException {
        
        Object obj = execute(getShowCreateTriggerSQL(databaseName, triggerName));
        if (obj instanceof ResultSet) {
            
            try (ResultSet rs = (ResultSet)obj) {
                if (rs.next()) {
                    String query = rs.getString("SQL Original Statement");
                    return query != null ? getTriggerDefinitionSQL(databaseName, triggerName, query) : null;
                }
            }
        }
        
        return null;
    }

    @Override
    public String getAlterFunctionSQL(String databaseName, String functionName) throws SQLException {
        
        Object obj = execute(getShowCreateFunctionSQL(databaseName, functionName));
        if (obj instanceof ResultSet) {
            
            try (ResultSet rs = (ResultSet)obj) {
                if (rs.next()) {
                    String query = rs.getString("Create Function");
                    return query != null ? getFormatedQuery(QueryProvider.QUERY__ALTER_FUNCTIONS, databaseName, functionName, query) : null;
                }
            }
        }
        
        return "";
    }
    
    @Override
    public String getDropFunctionSQL(String databaseName, String functionName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_FUNCTION, (databaseName != null ? databaseName + "`.`" : "") + functionName);//"DROP FUNCTION IF EXISTS `" + (databaseName != null ? databaseName + "`.`" : "") + functionName + "`";
    }
    
    @Override
    public String getShowCreateFunctionSQL(String databaseName, String functionName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_FUNCTIONS, (databaseName != null ? databaseName + "`.`" : "") + functionName);//"SHOW CREATE FUNCTION `" + (databaseName != null ? databaseName + "`.`" : "") + functionName + "`";
    }

    @Override
    public String getDropStoredProcedureSQL(String databaseName, String storedProcedureName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_STORED_PROCEDURE, (databaseName != null ? databaseName + "`.`" : "") + storedProcedureName);//"DROP PROCEDURE IF EXISTS `" + (databaseName != null ? databaseName + "`.`" : "") + storedProcedureName + "`";
    }
    
    @Override
    public String getShowCreateStoredProcedureSQL(String databaseName, String storedProcedureName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_STORED_PROCEDURE, (databaseName != null ? databaseName + "`.`" : "") + storedProcedureName);//"SHOW CREATE PROCEDURE `" + (databaseName != null ? databaseName + "`.`" : "") + storedProcedureName + "`";
    }

    @Override
    public String getAlterStoredProcedureSQL(String databaseName, String storedProcedureName) throws SQLException {
        
        Object obj = execute(getShowCreateStoredProcedureSQL(databaseName, storedProcedureName));
        if (obj instanceof ResultSet) {
            
            try (ResultSet rs = (ResultSet)obj) {
                if (rs.next()) {
                    String query = rs.getString("Create Procedure");
                    return query != null ? getFormatedQuery(QueryProvider.QUERY__ALTER_STORED_PROCEDURE, databaseName, storedProcedureName, query) : null;
//                    String result = "DELIMITER $$ \n\n";
//                    result += "USE `" + databaseName + "`$$\n\n";
//                    result += "DROP PROCEDURE IF EXISTS `" + storedProcedureName + "`$$\n\n";
//                    
//                    return  result + rs.getString("Create Procedure") + "$$\n\nDELIMITER ;";
                }
            }
        }
        
        return "";
    }
    
    @Override
    public String getDropTableSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_TABLE, (databaseName != null ? databaseName + "`.`" : "") + tableName);// "DROP TABLE IF EXISTS `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "`";
    }
    
    @Override
    public String getTruncateTableSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__TRUNCATE_TABLE, (databaseName != null ? databaseName + "`.`" : "") + tableName);//"TRUNCATE TABLE `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "`";
    }
    
    @Override
    public String getShowCreateTableSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_CREATE_TABLE, (databaseName != null ? databaseName + "`.`" : "") + tableName);//"SHOW CREATE TABLE `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "`";
    }
    
    @Override
    public String getInsertTableSQL(String databaseName, DBTable table) throws SQLException {
        List<Column> columns = table.getColumns();
        if (table.getColumns() == null) {
            columns = getColumnInformation(databaseName, table.getName());
        }
        
        String columnsInto = "";
        String columnsValues = "";
        
        for (Column c: columns) {
            columnsInto += "`" + c.getName() + "`, ";
            columnsValues += "'" + c.getName() + "', ";
        }
        
        if (!columnsInto.isEmpty()) {
            columnsInto = columnsInto.substring(0, columnsInto.length() - 2);
            columnsValues = columnsValues.substring(0, columnsValues.length() - 2);
        }
        
        return getFormatedQuery(QueryProvider.QUERY__INSERT_INTO_TABLE, (databaseName != null ? databaseName + "`.`" : "") + table.getName(), columnsInto, columnsValues);//"INSERT INTO `" + (databaseName != null ? databaseName + "`.`" : "") + table.getName() + "` (" + columnsInto + ")\n VALUES (" + columnsValues + ");";
    }
    
    @Override
    public String getSelectTableSQL(String databaseName, DBTable table) throws SQLException {
        List<Column> columns = table.getColumns();
        if (table.getColumns() == null) {
            columns = getColumnInformation(databaseName, table.getName());
        }
        
        String columnsSelect = "";
        
        for (Column c: columns) {
            columnsSelect += "`" + c.getName() + "`, ";
        }
        
        if (!columnsSelect.isEmpty()) {
            columnsSelect = columnsSelect.substring(0, columnsSelect.length() - 2);
        }
        
        return getFormatedQuery(QueryProvider.QUERY__SELECT_FROM_TABLE, columnsSelect, (databaseName != null ? databaseName + "`.`" : "") + table.getName());//"SELECT\n" + columnsSelect + "\nFROM `" + (databaseName != null ? databaseName + "`.`" : "") + table.getName() + "` LIMIT 0, 1000;";
    }
    
    @Override
    public String getUpdateTableSQL(String databaseName, DBTable table) throws SQLException {
        List<Column> columns = table.getColumns();
        if (table.getColumns() == null) {
            columns = getColumnInformation(databaseName, table.getName());
        }
        
        String columnsSet = "";
        String columnsPrimary = "";
        
        for (Column c: columns) {
            columnsSet += "`" + c.getName() + "` = '" + c.getName() + "', ";
            if (c.isPrimaryKey()) {
                columnsPrimary += "`" + c.getName() + "` = '" + c.getName() + "' AND ";
            }
        }
        
        if (!columnsSet.isEmpty()) {
            columnsSet = columnsSet.substring(0, columnsSet.length() - 2);
        }
        
        if (!columnsPrimary.isEmpty()) {
            columnsPrimary = columnsPrimary.substring(0, columnsPrimary.length() - 5);
        }
        
        return getFormatedQuery(QueryProvider.QUERY__UPDATE_IN_TABLE, (databaseName != null ? databaseName + "`.`" : "") + table.getName(), columnsSet, columnsPrimary);// "UPDATE `" + (databaseName != null ? databaseName + "`.`" : "") + table.getName() + "`\nSET " + columnsSet + "\nWHERE " + columnsPrimary + ";";
    }
    
    @Override
    public String getDeleteTableSQL(String databaseName, DBTable table) throws SQLException {
        List<Column> columns = table.getColumns();
        if (table.getColumns() == null) {
            columns = getColumnInformation(databaseName, table.getName());
        }
        
        String columnsPrimary = "";
        
        for (Column c: columns) {
            if (c.isPrimaryKey()) {
                columnsPrimary += "`" + c.getName() + "` = '" + c.getName() + "' AND ";
            }
        }
        
        if (!columnsPrimary.isEmpty()) {
            columnsPrimary = columnsPrimary.substring(0, columnsPrimary.length() - 5);
        }
        
        return getFormatedQuery(QueryProvider.QUERY__DELETE_FROM_TABLE, (databaseName != null ? databaseName + "`.`" : "") + table.getName(), columnsPrimary);//"DELETE FROM `" + (databaseName != null ? databaseName + "`.`" : "") + table.getName() + "`\nWHERE " + columnsPrimary + ";";
    }
   
    @Override
    public String getShowTableStatusSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_TABLE_STATUS, databaseName, tableName);//return "SHOW TABLE STATUS FROM `" + databaseName + "`" + " LIKE '"  + tableName + "'";
    }

    @Override
    public String getRenameEventTemplateSQL(String databaseName, String eventName, String newName) {
        return getFormatedQuery(QueryProvider.QUERY__RENAME_EVENT, databaseName, eventName, databaseName, newName);// "ALTER EVENT `" + databaseName + "`.`" + eventName + "` RENAME TO `" + databaseName + "`.`" + newName + "`";
    }

    @Override
    public String getDropEventSQL(String databaseName, String eventName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_EVENT, (databaseName != null ? databaseName + "`.`" : "") + eventName);//"DROP EVENT IF EXISTS `" + (databaseName != null ? databaseName + "`.`" : "") + eventName + "`";
    }
    
    @Override
    public String getShowCreateEventSQL(String databaseName, String eventName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_EVENT, (databaseName != null ? databaseName + "`.`" : "") + eventName);//"SHOW CREATE EVENT `" + (databaseName != null ? databaseName + "`.`" : "") + triggerName + "`";
    }

    @Override
    public String getAlterEventSQL(String databaseName, String eventName) throws SQLException {
        
        Object obj = execute(getShowCreateEventSQL(databaseName, eventName));
        if (obj instanceof ResultSet) {
            
            ResultSet rs = (ResultSet)obj;
            try {
                if (rs.next()) {
                    String query = rs.getString("Create Event");
                    return query != null ? getFormatedQuery(QueryProvider.QUERY__ALTER_EVENT, query.replace("CREATE", "ALTER")) : null;//"DELIMITER $$ \n\n" + rs.getString("Create Event").replace("CREATE", "ALTER") + "$$\n\nDELIMITER ;";
                }
            } finally {
                rs.close();
            }
        }
        
        return null;
    }
    
    @Override
    public String getClearForeignKeyCheckSQL() {
        return getQuery(QueryProvider.QUERY__CLEAN_FOREIGN_KEY_CHECKS);//"SET FOREIGN_KEY_CHECKS=1";
    }

    @Override
    public String getViewDefinitionSQL(String databaseName, String viewName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_VIEW_DEFINITION, databaseName, viewName);//"SELECT VIEW_DEFINITION FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '" + databaseName + "' AND TABLE_NAME = '" + viewName + "'";
    }

    @Override
    public String getCreateViewSQL(String databaseName, String viewName, String viewDefinition) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_VIEW, databaseName, viewName, viewDefinition);//"CREATE VIEW `" + databaseName + "`.`" + viewName + "` AS " + viewDefinition;
    }

    @Override
    public String getAlterViewSQL(String databaseName, String viewName) throws SQLException {
        
        Object obj = execute(getShowCreateTableSQL(databaseName, viewName));
        if (obj instanceof ResultSet) {
            try (ResultSet rs = (ResultSet)obj ) {
                if (rs.next()) {
                    String query = rs.getString("Create View");
                    return query != null ? getFormatedQuery(QueryProvider.QUERY__ALTER_VIEW_0, query.replace("CREATE ALGORITHM=", "ALTER ALGORITHM=")) : null;
                }
            }
        } else {
            //try (ResultSet rs = (ResultSet) execute("SELECT `IS_UPDATABLE`, `DEFINER`, `SECURITY_TYPE`, `VIEW_DEFINITION` FROM information_schema.`VIEWS` WHERE `TABLE_SCHEMA` = '" + databaseName + "' AND `TABLE_NAME` = '" + viewName + "';")) {
            try (ResultSet rs = (ResultSet) execute(getFormatedQuery(QueryProvider.QUERY__SELECT_VIEW_DEFINITION, databaseName, viewName ))) {
                
                if (rs.next()) {
                    String definer = rs.getString("DEFINER");

                    Pattern p = Pattern.compile("(.*)@(.*)");
                    Matcher m = p.matcher(definer);
                    if (m.find()) {
                        definer = "`" + m.group(1) + "`@`" + m.group(2) + "`";
                    }
                    
                    String query = rs.getString("VIEW_DEFINITION");
                    return query != null ? getFormatedQuery(QueryProvider.QUERY__ALTER_VIEW_1, definer, rs.getString("SECURITY_TYPE"), viewName, query) : null;
                }
            }
        }
        
        return null;
    }
    
    @Override
    public String getDropViewSQL(String databaseName, String viewName) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_VIEW, (databaseName != null ? databaseName + "`.`" : "") + viewName);//"DROP VIEW IF EXISTS `" + (databaseName != null ? databaseName + "`.`" : "") + viewName + "`";
    }
    
    @Override
    public String getShowCreateViewSQL(String databaseName, String tableName) {
        return getFormatedQuery(QueryProvider.QUERY__SHOW_CRAETE_VIEW, (databaseName != null ? databaseName + "`.`" : "") + tableName);//"SHOW CREATE VIEW `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "`";
    }

    @Override
    public String getCreateViewTemplateSQL(String databaseName, String viewName) {
        return getFormatedQuery(QueryProvider.QUERY__CRAETE_VIEW_TEMPLATE, databaseName, viewName);
//                "CREATE\n"
//                  + "    /*[ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]\n"
//                  + "    [DEFINER = { user | CURRENT_USER }]\n"
//                  + "    [SQL SECURITY { DEFINER | INVOKER }]*/\n"
//                  + "    VIEW `" + databaseName + "`.`" + viewName + "` \n"
//                  + "    AS\n"
//                  + "(SELECT * FROM ...);";
    }

    @Override
    public String getCreateStoredProcedureTemplateSQL(String databaseName, String storedProcedureName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_STORED_PROCEDURE_TEMPLATE, databaseName, storedProcedureName);
//                "CREATE\n"
//                  + "    /*[DEFINER = { user | CURRENT_USER }]*/\n"
//                  + "    PROCEDURE `" + databaseName + "`.`" + storedProcedureName + "`()\n"
//                  + "    /*LANGUAGE SQL\n"
//                  + "    | [NOT] DETERMINISTIC\n"
//                  + "    | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }\n"
//                  + "    | SQL SECURITY { DEFINER | INVOKER }\n"
//                  + "    | COMMENT 'string'*/\n"
//                  + "    BEGIN\n"
//                  + "\n"
//                  + "    END;";
    }

    
    @Override
    public String getCreateStoredProcedureWithCursorTemplateSQL(String databaseName, String storedProcedureName, String cursorName, String cursorQuery) throws SQLException {
        
        cursorQuery = cursorQuery.trim();
        if (cursorQuery.endsWith(";")) {
            cursorQuery = cursorQuery.substring(0, cursorQuery.length() - 1);
        }
        
        String variableDeclaration = "";
        String columnsList = "";
        try (ResultSet rs = executeQuery(cursorQuery)) {
            if (rs != null) {
                
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    
                    String name = "v_" + rs.getMetaData().getColumnName(i);
                    String type = rs.getMetaData().getColumnTypeName(i);
                    int length = rs.getMetaData().getColumnDisplaySize(i);
                    
                    DataType dataType = getDatatype(type + "(" + length + ")");
                    if (dataType != null) {
                        type = dataType.calcDataTypeStr();
                    } else {
                        type = "VARCHAR (45)";
                    }
                    
                    variableDeclaration += " DECLARE " + name + " " + type + ";\n";
                    
                    if (!columnsList.isEmpty()) {
                        columnsList += ", ";
                    }
                    columnsList += name;
                }
            }
        }
        
        return getFormatedQuery(QueryProvider.QUERY__CREATE_STORED_PROCEDURE_WITH_CURSOR_TEMPLATE, storedProcedureName, variableDeclaration, cursorName, cursorQuery, cursorName, cursorName, columnsList, columnsList, cursorName);
    }

    
    @Override
    public String getCreateFunctionTemplateSQL(String databaseName, String functionName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_FUNCTIONS_TEMPLATE, databaseName, functionName);
//                "CREATE\n"
//                  + "    /*[DEFINER = { user | CURRENT_USER }]*/\n"
//                  + "    FUNCTION `" + databaseName + "`.`" + functionName + "`()\n"
//                  + "    RETURNS TYPE\n"
//                  + "    /*LANGUAGE SQL\n"
//                  + "    | [NOT] DETERMINISTIC\n"
//                  + "    | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }\n"
//                  + "    | SQL SECURITY { DEFINER | INVOKER }\n"
//                  + "    | COMMENT 'string'*/\n"
//                  + "    BEGIN\n"
//                  + "\n"
//                  + "    END;";
    }

    @Override
    public String getCreateTriggerTemplateSQL(String databaseName, String triggerName, String beforeAfter, String triggerEvent, String triggerTable, String comment) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_TRIGGER_TEMPLATE, comment, databaseName, triggerName, beforeAfter, triggerEvent, databaseName, triggerTable);
//                "CREATE\n"
//                  + "    /*[DEFINER = { user | CURRENT_USER }]*/\n"
//                  + "    TRIGGER `" + databaseName + "`.`" + triggerName + "` BEFORE/AFTER INSERT/UPDATE/DELETE\n"
//                  + "    ON `" + databaseName + "`.`<Table Name>`\n"
//                  + "    FOR EACH ROW BEGIN\n"
//                  + "\n"
//                  + "    END;";
    }
    
    @Override
    public String getCreateAuditTriggerTemplateSQL(String databaseName, String triggerName, String beforeAfter, String triggerEvent, String triggerTable, String triggerBody, String comment) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_AUDIT_TRIGGER_TEMPLATE, databaseName, triggerName, beforeAfter, triggerEvent, databaseName, triggerTable, triggerBody, comment);
    }

    @Override
    public String getCreateEventTemplateSQL(String databaseName, String eventName) {
        return getFormatedQuery(QueryProvider.QUERY__CREATE_EVENT_TEMPLATE, databaseName, eventName);
//                "/* SET GLOBAL event_scheduler = ON$$      required for event to execute but not create*/    \n"
//                  + "\n"
//                  + "CREATE	/*[DEFINER = { user | CURRENT_USER }]*/	EVENT `" + databaseName + "`.`" + eventName + "`\n"
//                  + "\n"
//                  + "ON SCHEDULE\n"
//                  + "	 /* uncomment the example below you want to use */\n"
//                  + "\n"
//                  + "	/* scheduleexample 1: run once*/\n"
//                  + "\n"
//                  + "	   /*  AT 'YYYY-MM-DD HH:MM.SS'/CURRENT_TIMESTAMP { + INTERVAL 1 [HOUR|MONTH|WEEK|DAY|MINUTE|...] }*/\n"
//                  + "\n"
//                  + "	/* scheduleexample 2: run at intervals forever after creation*/\n"
//                  + "\n"
//                  + "	   /* EVERY 1 [HOUR|MONTH|WEEK|DAY|MINUTE|...]*/\n"
//                  + "\n"
//                  + "	/* scheduleexample 3: specified start time, end time and interval for execution*/\n"
//                  + "	   /*EVERY 1  [HOUR|MONTH|WEEK|DAY|MINUTE|...]\n"
//                  + "\n"
//                  + "	   STARTS CURRENT_TIMESTAMP/'YYYY-MM-DD HH:MM.SS' { + INTERVAL 1[HOUR|MONTH|WEEK|DAY|MINUTE|...] }\n"
//                  + "\n"
//                  + "	   ENDS CURRENT_TIMESTAMP/'YYYY-MM-DD HH:MM.SS' { + INTERVAL 1 [HOUR|MONTH|WEEK|DAY|MINUTE|...] } */\n"
//                  + "\n"
//                  + "/*[ON COMPLETION [NOT] PRESERVE]\n"
//                  + "[ENABLE | DISABLE]\n"
//                  + "[COMMENT 'comment']*/\n"
//                  + "\n"
//                  + "DO\n"
//                  + "	BEGIN\n"
//                  + "	    (sql_statements)\n"
//                  + "	END;";
    }
    
    @Override
    public String getUsersAccountSQL(){
        return getQuery(QueryProvider.QUERY__USERS_ACCOUNTS);
    }
    
    @Override
    public String getUserGlobalPrivSQL(String user){
        return getFormatedQuery(QueryProvider.QUERY__GLOBAL_USER_PRIV,user);
    }
    
    @Override
    public String getDeleteUserSQL(String user){
        return getFormatedQuery(QueryProvider.QUERY__DELETE_USER,user);
    }
    
    @Override
    public String getShowGrantsSQL(String user){
        return getFormatedQuery(QueryProvider.QUERY__SHOW_GRANTS,user);
    }
    
    @Override
    public String getDropIndexSQL(String database, String table, String index){
        return getFormatedQuery(QueryProvider.QUERY__DROP_INDEX, database, table, index);
    }
 
    @Override
    public String getDropColumnSQL(String database, String table, String column) {
        return getFormatedQuery(QueryProvider.QUERY__DROP_COLUMN, database, table, column);
    }    
}
