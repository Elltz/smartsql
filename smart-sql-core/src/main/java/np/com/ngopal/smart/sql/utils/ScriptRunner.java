package np.com.ngopal.smart.sql.utils;


import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.QueryResult;
import org.apache.commons.lang.StringUtils;

/**
 * Tool to run database scripts
 */
@Slf4j
public class ScriptRunner {

    private static final String DEFAULT_DELIMITER = ";";
    /**
     * regex to detect delimiter.
     * ignores spaces, allows delimiter in comment, allows an equals-sign
     */
    public static final Pattern delimP = Pattern.compile("^\\s*(--)?\\s*delimiter\\s*=?\\s*([^\\s]+)+\\s*.*$", Pattern.CASE_INSENSITIVE);
    
    private static final String COMMENT__START = "/*";
    private static final String COMMENT__END = "*/";
    private static final String SINGLE_QUOTE = "'";
    
    private static final String NEW_LINE_SEPARATOR = "  "; // 2 space for \r\n for correct calculating position of query
    private static final String DEFAULT_LINE_SEPARATOR = "\r\n";
    
    private final Connection connection;

    private final boolean stopOnError;
    
    private boolean forceStop = false;

    private String delimiter = DEFAULT_DELIMITER;
    private boolean fullLineDelimiter = false;

    private Statement runnedStatement;
    
    
    private Reader preparedReader;
    private ScriptRunnerCallback preparedCallback;
    
    /**
     * Default constructor
     * 
     * @param connection - DB connection
     * @param autoCommit - auto commit flag
     * @param stopOnError - stopping execution if error
     */
    public ScriptRunner(Connection connection, boolean stopOnError) {
        this.connection = connection;
        this.stopOnError = stopOnError;
    }

    public void setDelimiter(String delimiter, boolean fullLineDelimiter) {
        this.delimiter = delimiter;
        this.fullLineDelimiter = fullLineDelimiter;
    }
    
    
    public void forceStop() {
        forceStop = true;
        try {
            if (runnedStatement != null) {
                runnedStatement.cancel();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ScriptRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void prepareScript(Reader reader, ScriptRunnerCallback callback) {
        this.preparedReader = reader;
        this.preparedCallback = callback;
    }
    
    public void runScript(boolean needQueryResults) throws IOException, SQLException {
        if (!forceStop) {
            runScript(preparedReader, preparedCallback, needQueryResults);
        }
    }

    public void runScript() throws IOException, SQLException {
        runScript(true);
    }
    /**
     * Runs an SQL script (read in using the Reader parameter)
     *
     * @param reader - the source of the script
     * @param callback - the callback to know status
     * @param needQueryResults - need return query results
     * 
     * @throws SQLException if any SQL errors occur
     * @throws IOException if there is an error reading from the Reader
     */
    public void runScript(Reader reader, ScriptRunnerCallback callback, boolean needQueryResults) throws IOException, SQLException {
        try {
            runScript(connection, reader, -1, null,  callback, needQueryResults);            
        } catch (IOException | SQLException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error running script.  Cause: " + e, e);
        }
    }
    
    public void runScript(Reader reader, ScriptRunnerCallback callback) throws IOException, SQLException {
        runScript(reader, callback, true);
    }
    
    /**
     * Runs an SQL script (read in using the Reader parameter)
     *
     * @param reader - the source of the script
     * @param queryPosition - position of query for run in script
     * @param selectLimit - limit for select queries, if null - without limit
     * @param callback - the callback to know status
     * @param needQueryResults - need return query results
     * 
     * @throws SQLException if any SQL errors occur
     * @throws IOException if there is an error reading from the Reader
     */
    public void runScript(Reader reader, int queryPosition, Integer selectLimit, ScriptRunnerCallback callback, boolean needQueryResults) throws IOException, SQLException {
        try {
            runScript(connection, reader, queryPosition, selectLimit, callback, needQueryResults);
        } catch (IOException | SQLException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error running script.  Cause: " + e, e);
        }
    }
    
    public void runScript(Reader reader, int queryPosition, Integer selectLimit, ScriptRunnerCallback callback) throws IOException, SQLException {
        runScript(reader, queryPosition, selectLimit, callback, true);
    }
    
    public void runScript(Reader reader, int queryPosition, ScriptRunnerCallback callback) throws IOException, SQLException {
        runScript(reader, queryPosition, null, callback);
    }
    
    public void runBulkScript(Reader reader, ScriptRunnerCallback callback) throws IOException, SQLException {
        StringBuffer data = new StringBuffer(fullRead(reader, DEFAULT_LINE_SEPARATOR, 0).getKey());
            
        Map<Integer, List<String>> queries = new HashMap<>();
        queries.put(0, Arrays.asList(new String[] {data.toString()}));
        
        MemoryUsageUtils.printMemoryUsage("COLLECTED");
        
        run(data, queries, connection, -1, true, null, callback);
    }

    /**
     * Runs an SQL script (read in using the Reader parameter) using the
     * connection passed in
     *
     * @param conn - the connection to use for the script
     * @param reader - the source of the script
     * @param queryPosition - position of query for run in script
     * @param selectLimit - limit for select queries, if null - without limit
     * @param callback - the callback to know status
     * 
     * @throws SQLException if any SQL errors occur
     * @throws IOException if there is an error reading from the Reader
     */
    private void runScript(Connection conn, Reader reader, int queryPosition, Integer selectLimit, ScriptRunnerCallback callback, boolean needQueryResults) throws IOException,
            SQLException {
        
        Pair<String, Integer> value = fullRead(reader, DEFAULT_LINE_SEPARATOR, queryPosition);
        queryPosition += value.getValue();
        
        StringBuffer data = new StringBuffer(value.getKey());
            
        Map<Integer, List<String>> queries = collectQueries(data.toString(), false, callback);
        
        MemoryUsageUtils.printMemoryUsage("COLLECTED");
        
        run(data, queries, conn, queryPosition, needQueryResults, selectLimit, callback);
    }
    
    private void run(StringBuffer data, Map<Integer, List<String>> queries, Connection conn, int queryPosition, boolean needQueryResults, Integer selectLimit, ScriptRunnerCallback callback) throws SQLException {
        if (queries != null && !queries.isEmpty()) {
            
//            String mainText = data.toString();
            
            int totalSize = 0;
            for (List<String> list: queries.values()) {
                totalSize += list.size();
            }
            
            if (callback != null) {
                callback.started(conn, queryPosition > -1 ? 1 : totalSize);
            }
            
            
            try {
                int index = 0;
                int queryLength = 0;  
                int queryIndex = -1;
//                int replacedCount = 0;
                boolean hasRN = data.toString().contains("\r\n");
                for (Map.Entry<Integer, List<String>> entry: queries.entrySet()) {

                    if (entry.getValue() != null) {
                        for (String query: entry.getValue()) {
                            
//                            String mainQuery = query;
                            
                            
                            if (hasRN && !query.contains("\r\n")) {
                                String queryStr = query.replaceAll("\n", "\r\n");
//                                replacedCount += queryStr.length() - query.length();
                                query = queryStr;
                            }
                            
                            
//                            int queryShift = 0;
                            String simpleQuery = query;
//                            int idx = 0;
//                            while (data.charAt(idx) == '\r' || data.charAt(idx) == '\n') {
//                                if (data.charAt(idx) == '\r') {
//                                    data = data.replace(idx, idx + 1, "");
//                                } else {
//                                    idx++;
//                                }
//                            }
                            queryIndex = data.indexOf(query);
                            int shift = queryIndex + query.length(); // + queryShift
                            
                            queryLength += shift;
//                            System.out.println("QUERY: " + query);
//                            System.out.println("WRONG: " + queryLength);
//                            System.out.println("CORRECT: " + (mainText.indexOf(query) + query.length()));
                            
                            data = data.delete(0, shift);
                            
                            if (query.isEmpty()) {
                                continue;
                            }
                            
                            index++;
                            
                            // if query not negative we must run only needed query
                            if (totalSize > 1 && totalSize != index && queryPosition >= 0 && queryPosition > queryLength) {
                                continue;
                            }
                                
                            if (forceStop || callback != null && callback.isNeedStop()) {
                                return;
                            }
                            
                            QueryResult queryResult = needQueryResults ? new QueryResult() : null;
                            
                            try {
                                runnedStatement = conn.createStatement();
                                
                                if (needQueryResults) {
                                    queryResult.setStartDuration(System.currentTimeMillis());
                                }
                                
                                query = addLimitIfNeed(query, selectLimit);
                                
                                if (needQueryResults) {
                                    queryResult.setQuery(query);
                                    
                                    simpleQuery = simpleQuery.replaceAll("\r", "");
                                    simpleQuery = simpleQuery.replaceAll("\\s", " ").trim();
                                    if (simpleQuery.endsWith(";")) {
                                        simpleQuery = simpleQuery.substring(0, simpleQuery.length() - 1);
                                    }
                                    queryResult.setSimpleQuery(simpleQuery);
                                }
                                
                                if (callback != null) {
                                    callback.queryExecuting(conn, queryResult);
                                }
                                
                                
                                if (forceStop || callback != null && callback.isNeedStop()) {
                                    return;
                                }
                                
                                if (callback != null) {
                                    query = callback.prepareQuery(queryResult, query);
                                }
                                
                                boolean executeIndicator = runnedStatement.execute(query, Statement.RETURN_GENERATED_KEYS);
                                
                                if (needQueryResults) {
                                    queryResult.setExecutionDuration(System.currentTimeMillis());
                                }
                                
                                
                                if (forceStop || callback != null && callback.isNeedStop()) {
                                    return;
                                }
                                
                                if (needQueryResults) {
                                    if (executeIndicator) {
                                        queryResult.addResult(runnedStatement.getResultSet());
                                        
                                        while (true) {
                                            if (runnedStatement.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                                                queryResult.addResult(runnedStatement.getResultSet());
                                            } else if (runnedStatement.getUpdateCount() >= 0){
                                                queryResult.addResult(runnedStatement.getUpdateCount());
                                            } else {
                                                break;
                                            }
                                        }
                                        
                                        queryResult.setEndDuration(System.currentTimeMillis());
                                        queryResult.setResultSet(true);
                                    } else {
                                        int i = runnedStatement.getUpdateCount();
                                        queryResult.setEndDuration(System.currentTimeMillis());
                                        queryResult.addResult(i);
                                        queryResult.setGeneratedKey(runnedStatement.getGeneratedKeys());
                                        queryResult.setResultSet(false);
                                        
                                        while (true) {
                                            if (runnedStatement.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                                                queryResult.addResult(runnedStatement.getResultSet());
                                            } else if (runnedStatement.getUpdateCount() >= 0){
                                                queryResult.addResult(runnedStatement.getUpdateCount());
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                SQLWarning warn = runnedStatement.getWarnings();
                                while (queryResult != null && warn != null) {
                                    queryResult.addWarningCode(String.valueOf(warn.getErrorCode()));
                                    queryResult.addWarningMessage(warn.getMessage());
                                    warn = warn.getNextWarning();
                                }
                                                                
                                if (callback != null) {
                                    callback.queryExecuted(conn, queryResult);
                                }
                            } catch (SQLException e) {
                               
                                if (needQueryResults) {
                                    queryResult.setErrorCode(e.getErrorCode());
                                    queryResult.setErrorMessage(e.getMessage());
                                }
                                
                                if (callback != null) {
                                    callback.queryError(conn, queryResult, e);
                                }
                                
                                if (stopOnError) {
                                    throw e;
                                }
                            }
                            
                            if (queryPosition >= 0) {
                                return;
                            }
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {}
                        }
                    }
                }                
            } finally {                
                if (callback != null) {
                    callback.finished(conn);
                }
            }
        } else {
            if (callback != null) {
                callback.finished(conn); 
            }
        }
    }
    
    private static Pattern INTO_PATTERN = Pattern.compile("\\s+(INTO)\\s+.*");
    private static Pattern FOR_PATTERN = Pattern.compile("\\s+(FOR\\s+(UPDATE|SHARE))\\s+.*");
    private static Pattern LOCK_PATTERN = Pattern.compile("\\s+(LOCK\\s+IN)\\s+.*");
    
    public static String addLimitIfNeed(String query, Integer selectLimit) {
        if (selectLimit != null && !query.toUpperCase().contains("LIMIT") && query.toUpperCase().startsWith("SELECT")) {
            if (selectLimit == 0) {
                selectLimit = 1000;
            }
            
            String queryStr = hideStrings(query);
            
            int index = -1;
            
            Matcher m = INTO_PATTERN.matcher(queryStr);
            if (m.find()) {
                index = m.start();
            }
            
            if (index == -1) {
                m = FOR_PATTERN.matcher(queryStr);
                if (m.find()) {
                    index = m.start();
                }
            }
            
            if (index == -1) {
                m = LOCK_PATTERN.matcher(queryStr);
                if (m.find()) {
                    index = m.start();
                }
            }
            
            if (query.endsWith(";")) {
                query = query.substring(0, query.length() - 1);
            }
            
            if (index == -1) {
                index = query.length();
            }

            query = query.substring(0, index) + " LIMIT 0, " + selectLimit + " " + query.substring(index) +  ";";
        }
        
        return query;
    }
        
    public Map<Integer, List<String>> findQueries(Reader reader) throws IOException {
        Pair<String, Integer> res = fullRead(reader, DEFAULT_LINE_SEPARATOR, 0);
        return collectQueries(res.getKey(), false, null);
    }
    
    
    private Pair<String, Integer> fullRead(Reader reader, String lineSeparator, int position) throws IOException {
        LineNumberReader lineReader = new LineNumberReader(reader);
        StringBuilder fullLine = new StringBuilder();

        int total = 0;
        int shift = 0;
        String data;
        while ((data = lineReader.readLine()) != null) {            
            fullLine.append(data).append(lineSeparator);
            
            total += data.length() + 1;
            if (position > total) {
                shift += 1;
            }            
        }
        
        return new Pair(fullLine.toString(), shift);
    }
    
    private Map<Integer, List<String>> collectQueries(String data, boolean withComments, ScriptRunnerCallback callback) throws IOException {
        Map<Integer, List<String>> queries = new LinkedHashMap<>();
        StringBuffer command = null;
        int queryLine = -1;
        try {
            LineNumberReader lineReader = new LineNumberReader(new StringReader(data));
            String line;
            while ((line = lineReader.readLine()) != null) {
                
                if (callback != null && callback.isNeedStop()) {
                    return null;
                }
                
                if (command == null) {
                    command = new StringBuffer();
                    queryLine = lineReader.getLineNumber();
                }
                
                String trimmedLine = line.trim();
                //lengthDiff -= line.length() - trimmedLine.length();
                final Matcher delimMatch = delimP.matcher(trimmedLine);
                if (trimmedLine.length() < 1
                        || trimmedLine.startsWith("//")) {
                    if (command.length() == 0) {
                        command = null;
                    } else {
                        command.append(line);
                    }
                    
                } else if (delimMatch.matches()) {
                    setDelimiter(delimMatch.group(2), false);
                    //lengthDiff += trimmedLine.length();

                } else if (trimmedLine.startsWith("--")) {
                     // Do nothing
                     //lengthDiff += trimmedLine.length();
                     if (withComments) {
                         command.append(line);
                     }
                } else if (trimmedLine.length() < 1
                        || trimmedLine.startsWith("--")) {
                    // Do nothing
                    //lengthDiff += trimmedLine.length();
                } else if (!fullLineDelimiter && trimmedLine.contains(getDelimiter())) {
                    
                    //String allLine = command.toString() + trimmedLine;
                    String allLine = hideStrings(line);
                    if (countOfMatches(allLine, "'") % 2 == 0 && !insideComment(getDelimiter(), allLine) && notInsideString(getDelimiter(), allLine)
                        || fullLineDelimiter
                        && trimmedLine.equals(getDelimiter())) {

                        Object[] result = collectQueriesByDelimiter(command, queries, line, queryLine, lineReader);

                        command = (StringBuffer) result[0];
                        queryLine = (int) result[1];
                        
                    } else {
                        command.append(line);
                    }
                } else {
                    command.append(line);
                }
                
                if (command != null && command.length() > 0) {
                    command.append("\n");
                }
            }
        } catch (IOException e) {
            throw new IOException(String.format("Error making queries '%s': %s", command, e.getMessage()), e);
        }
        
        if (command != null) {
            List<String> list = queries.get(queryLine);
            if (list == null) {
                list = new ArrayList<>();
                queries.put(queryLine, list);
            }

            if (!command.toString().trim().isEmpty()) {
                list.add(command.toString().trim());
            }
        }
        
        
        return queries;
    }
    
    
    public static int countOfMatches(String text, String regex) {
        return StringUtils.countMatches(text, regex) - StringUtils.countMatches(text, "\\" + regex) + StringUtils.countMatches(text, "\\\\" + regex);
    }
    
    public static boolean insideComment(String word, String text) {
        
        text = replaceString(text);
        text = replaceComment(text);
                
        return !text.contains(word);
    }
    
    public static String hideStrings(String text) {
        text = text.replace("\\\\", "##");

        boolean singleQuote = false;
        boolean doubleQuote = false;
        StringBuilder converted = new StringBuilder();
        String prev = "";
        for (int i = 0; i < text.length(); i++) {

            String c = text.substring(i, i + 1);            

            if ("'".equals(c) && !"\\".equals(prev) && !doubleQuote) {
                if (!singleQuote && !doubleQuote) {
                    singleQuote = true;
                } else if (singleQuote) {
                    singleQuote = false;
                }

                converted.append("'");
                prev = c;
                continue;

            } else if ("\"".equals(c) && !"\\".equals(prev) && !singleQuote) {
                if (!singleQuote && !doubleQuote) {
                    doubleQuote = true;

                } else if (doubleQuote) {
                    doubleQuote = false;
                }

                converted.append("'");
                prev = c;
                continue;
            } else {
                if (doubleQuote || singleQuote) {
                    converted.append("#");
                } else {
                    converted.append(c);
                }
            }

            prev = c;
        }

        return converted.toString();
    }
    
    public static boolean notInsideString(String word, String text) {        
        text = hideStrings(text);
        return text.indexOf(word) != -1;
    }
    
    public static String replaceString(String text) {
        text = text.replace("\\\\", "");
        text = text.replace("\\'", "");
        StringBuilder sb = new StringBuilder(text);
        
        int start = sb.indexOf(SINGLE_QUOTE);
        int end = sb.indexOf(SINGLE_QUOTE, start + 1);
        
        while(start > -1 && end > -1) {            
            sb = sb.delete(start, end + 1);            
            start = sb.indexOf(SINGLE_QUOTE);
            end = sb.indexOf(SINGLE_QUOTE, start + 1);
        }
        
        if (start > -1) {
            sb = sb.delete(start, sb.length());
            end = sb.indexOf(SINGLE_QUOTE);
        }
        
        if (end > 0) {
            sb = sb.delete(0, end + 1);
        }
        
        return sb.toString();
    }
    
    
    public static String replaceComment(String text) {
        
        StringBuilder sb = new StringBuilder(text);
        int start = sb.indexOf(COMMENT__START);
        int end = sb.indexOf(COMMENT__END);
        while(start > -1 && end > -1 && start < end) {
//            text = text.substring(0, start) + text.substring(end + 2);
            
            sb = sb.delete(start, end + 2);
            
            start = sb.indexOf(COMMENT__START);
            end = sb.indexOf(COMMENT__END);
        }
        
        if (start > -1) {
//            text = text.substring(0, start);
            sb = sb.delete(start, sb.length());
            end = sb.indexOf(COMMENT__END);
        }
        
        if (end > 0) {
//            text = text.substring(end + 2);
            sb = sb.delete(0, end + 2);
        }
        
        return sb.toString();
    }
    
    public static String replaceComment(String text, char c) {
        
        StringBuilder sb = new StringBuilder(text);
        int start = sb.indexOf(COMMENT__START);
        int end = sb.indexOf(COMMENT__END);
        
        while(start > -1 && end > -1 && start < end) {
            
            String replace = "";
            for (int i = start; i < end + 2; i++) {
                replace += String.valueOf(c);
            }

            sb = sb.replace(start, end + 2, replace);            
            
            start = sb.indexOf(COMMENT__START);
            end = sb.indexOf(COMMENT__END);
        }
        
        if (start > -1) {
            
            String replace = "";
            for (int i = start; i < sb.length(); i++) {
                replace += String.valueOf(c);
            }
            
            sb = sb.replace(start, sb.length(), replace);
            end = sb.indexOf(COMMENT__END);
        }
        
        if (end > 0) {
            String replace = "";
            for (int i = 0; i < end + 2; i++) {
                replace += String.valueOf(c);
            }
            
            sb = sb.replace(0, end + 2, replace);
        }
        
        return sb.toString();
    }
    
    
    //SELECT 
    //* from test; select * from test2; select * FROM 
    //test_copy ;
    private Object[] collectQueriesByDelimiter(StringBuffer command, Map<Integer, List<String>> queries, String text, int queryLine, LineNumberReader lineReader) {
                
        String line = hideStrings(text);
        
        String currentLine = "";
        String otherLine = "";
        String tempLine = line;
        boolean hasDelim = false;
        int count = 0;
        while (tempLine.contains(getDelimiter())) {
            int idx = tempLine.indexOf(getDelimiter());
            String s = tempLine.substring(0, idx);
            count += countOfMatches(s, "'");
            
            currentLine += text.substring(0, idx) + (getDelimiter().equals(";") ? ";" : "");
            
            idx = tempLine.indexOf(getDelimiter()) + getDelimiter().length();
            // then its delimeter
            if (count % 2 == 0) {
                otherLine = text.substring(idx);
                hasDelim = true;
                break;
            } else {
                tempLine = tempLine.substring(idx);
                text = text.substring(idx);
            }
        }
        
        if (!hasDelim) {
            currentLine += text;
        }
        
        String c = currentLine;
        command.append(c);
        
        List<String> list = queries.get(queryLine);
        if (list == null) {
            list = new ArrayList<>();
            queries.put(queryLine, list);
        }

        if (!command.toString().isEmpty()) {
            list.add(command.toString().trim());
        }
 
        if (!otherLine.trim().isEmpty()) {
        
            String hidenOtherLine = hideStrings(otherLine);
            
            command = new StringBuffer();
            queryLine = lineReader.getLineNumber();
            
            if (hidenOtherLine.contains(getDelimiter()) && 
                (StringUtils.countMatches(hidenOtherLine, "'") % 2 == 0 && StringUtils.countMatches(hidenOtherLine, "\"") % 2 == 0)) {
                
                Object[] result = collectQueriesByDelimiter(command, queries, otherLine, queryLine, lineReader);
                
                command = (StringBuffer) result[0];
                queryLine = (int) result[1];
            } else {
                command.append(otherLine);
            }            
        } else {
            command = null;
        }
        
        return new Object[]{command, queryLine};
    }
    
    
    public Pair<Integer, String> findQueryAtPosition(String text, int queryPosition, boolean withComments) {
        
        try {
            Map<Integer, List<String>> queries = collectQueries(text, withComments, null);
            
            if (queries != null && !queries.isEmpty()) {
                
                int totalSize = 0;
                for (List<String> list: queries.values()) {
                    totalSize += list.size();
                }
                
                int index = 0;
                int queryLength = 0;
                for (Map.Entry<Integer, List<String>> entry: queries.entrySet()) {
                    
                    if (entry.getValue() != null) {
                        for (String query: entry.getValue()) {
                            
                            queryLength += query.length();
                            
                            if (query.isEmpty()) {
                                continue;
                            }
                            
                            index++;
                            // if position not negative we must run only needed query
                            if (totalSize > 1 && totalSize != index && queryPosition >= 0 && queryPosition > queryLength) {
                                continue;
                            }
                            
                            return new Pair(entry.getKey(), query);
                        }
                    }
                }
            }
            
        } catch (IOException ex) {
            log.error("Error", ex);
        }
        
        return null;
    }
    
    private String getDelimiter() {
        return delimiter;
    }

    public interface ScriptRunnerCallback {        
        
        void started(Connection con, int queriesSize) throws SQLException;
        
        void queryExecuting(Connection con, QueryResult queryResult) throws SQLException;
        /**
         * Remember you must close ResultSet manually
         * in query result
         * @param queryResult query result
         */
        void queryExecuted(Connection con, QueryResult queryResult) throws SQLException;
        void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException;
        void finished(Connection con) throws SQLException;
        
        boolean isWithLimit(QueryResult queryResult);
        
        boolean isNeedStop();
        
        default String prepareQuery(QueryResult queryResult, String query) {
            return query;
        }
    }
}
