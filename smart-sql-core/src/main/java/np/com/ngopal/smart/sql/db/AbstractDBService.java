package np.com.ngopal.smart.sql.db;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.google.inject.Inject;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.text.Collator;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.model.Advanced;
import np.com.ngopal.smart.sql.model.Checksum;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DelayKeyWrite;
import np.com.ngopal.smart.sql.model.EngineType;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.IndexType;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.Rowformat;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Technology;
import np.com.ngopal.smart.sql.model.TechnologyType;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataType;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataTypes;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLEngineTypes;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public abstract class AbstractDBService implements DBService<Technology> {

    protected Connection connection;
    // TODO I think need remove in future
    // this flag used for inidicate that we are trying
    // to create connect, but if was an error
    // than no need try recreate or always error throwed
    protected boolean inited = false;

    @Getter
    protected volatile long lastActivity = 0l;

    // using for multiply query executions
    // some object make it busy and must unbusy
    private volatile boolean serviceBusy = false;

    private List<Consumer<String>> databaseChangeListeners = new ArrayList<>();

    private final BooleanProperty busy = new SimpleBooleanProperty(false);

    protected String dbSQLVersion = "";

    protected int dbSQLVersionMaj = 0;
    protected int dbSQLVersionMin = 0;
    protected int dbSQLVersionMic = 0;

    @Setter
    @Getter
    protected ConnectionParams param;

    @Inject
    protected SQLLogger logger;

    @Getter
    protected Database database;

    public void addDatabaseChangeListener(Consumer<String> listener) {
        databaseChangeListeners.add(listener);
    }

    public void removeDatabaseChangeListener(Consumer<String> listener) {
        databaseChangeListeners.remove(listener);
    }

    public void fireDatabaseChanged(String database) {
        for (Consumer<String> listener : databaseChangeListeners) {
            listener.accept(database);
        }
    }

    public BooleanProperty busy() {
        return busy;
    }

    public void setServiceBusy(boolean serviceBusy) {
        this.serviceBusy = serviceBusy;
        busy.set(serviceBusy);
    }

    public boolean isServiceBusy() {
        return serviceBusy;
    }

    @Override
    public String getDbSQLVersion() {
        return dbSQLVersion;
    }

    @Override
    public int getDbSQLVersionMaj() {
        return dbSQLVersionMaj;
    }

    @Override
    public int getDbSQLVersionMin() {
        return dbSQLVersionMin;
    }

    @Override
    public int getDbSQLVersionMic() {
        return dbSQLVersionMic;
    }

    @Override
    public boolean isPerformanceSchemaOn() {
        try (ResultSet rs = (ResultSet) execute("SHOW VARIABLES LIKE 'performance_schema'")) {
            return rs != null && rs.next() && "on".equalsIgnoreCase(rs.getString(2));
        } catch (SQLException ex) {
            log.error("Error", ex);
        }

        return false;
    }

    public static boolean isNumber(String type) {
        switch (type.toUpperCase()) {
            case "BIGINT":
            case "DECIMAL":
            case "DOUBLE":
            case "FLOAT":
            case "INT":
            case "MEDIUMINT":
            case "NUMERIC":
            case "SMALLINT":
            case "TINYINT":
                return true;
        }

        return false;
    }

    public static boolean isHasCharset(String type) {
        switch (type.toUpperCase()) {
            case "CHAR":
            case "VARCHAR":
            case "TINYTEXT":
            case "TEXT":
            case "MEDIUMTEXT":
            case "LONGTEXT":
            case "ENUM":
            case "SET":
                return true;
        }

        return false;
    }

    protected abstract TechnologyType getTechType();

    protected abstract String getChangeDBSQL(Database database);

    protected abstract String getDatabasesSQL();

    public abstract String getDatabaseCharsetAndCollateSQL(String databaseName);

    protected abstract String getViewsSQL(Database database);

    protected abstract String getTriggersSQL(Database database);

    protected abstract String getFunctionsSQL(Database database);

    protected abstract String getEventsSQL(Database database);

    protected abstract String getStoredProcsSQL(Database database);

    protected abstract String getTablesSQL(Database database);

    protected abstract String getCharsetSQL();

    protected abstract String getCollationSQL(String charset);

    protected abstract String getEngineSQL();
    
    public abstract String testConnection(ConnectionParams params) throws SQLException;

    // Duplicate
//    protected abstract String getTableStatusSQL(String databaseName, String tableName);
    protected abstract String getColumnInformationSQL(String databaseName, String tableName);

    // @TODO Not user, need remove
//    protected abstract String getAlterTableSQL(String databaseName, String tableName,
//              Map<String, List<String>> columnAndValues);
    protected abstract String getRenameTableSQL(String databaseName, String oldTableName, String newTableName);

    protected abstract String getChangeTableTypeSQL(String databaseName, String tableName, String type);

    protected abstract String getIndexSQL(String databaseName, String tableName);

    protected abstract String getForeignKeysSQL(String databaseName, String tableName);

    protected abstract String getKeysSQL(String databaseName, String tableName);

    protected abstract String getProfilesStartSQL();

    protected abstract String getProfilesShowSQL();

    protected abstract String getProfilesStopSQL();

    protected abstract String getDescribeColumnSQL(String databaseName, String tableName);

    protected abstract String getShowRecordsCountForTableSQL(String databaseName, String tableName);

    @Override
    public boolean isClosed() throws SQLException {
        lastActivity = new Date().getTime();

        // @TODO: Not sure need this comment or not, need check later
//        if (isServiceBusy() && connection != null) {
//            return false;
//        }
        boolean closed = connection == null || !connection.isValid(1);

//        if (!closed) {
//            try {
//                Statement stmt = connection.createStatement();
//                stmt.execute("SELECT 1");
//            } catch (SQLException ex) {
//                return true;
//            }
//        }
        return closed;
    }

    @Override
    public List<Database> getDatabases() throws SQLException {
        List<Database> db = null;

        try (ResultSet rs = executeQuery(getDatabasesSQL())) {
            db = new ArrayList<>();
            while (rs.next()) {

//                String databaseName = rs.getString(1);
                db.add(new Database(null, rs.getString(1), null, null, null, null, null, null, null, null, null));
//                try (ResultSet rs0 = executeQuery(getDatabaseCharsetAndCollateSQL(databaseName))) {
//                    if (rs0.next()) {
//                        db.add(new Database(null, databaseName, rs0.getString(1), rs0.getString(2), null, null, null, null, null, null));
//                    } else {
//                        db.add(new Database(null, databaseName, null, null, null, null, null, null, null, null));
//                    }
//                }
            }
        }

        db.sort(new Comparator<Database>() {
            @Override
            public int compare(Database o1, Database o2) {
                return Collator.getInstance().compare(o1.getName(), o2.getName());
            }
        });

        return db;
    }

    protected ResultSet executeQuery(String sql) throws SQLException {
        Object obj = execute(sql);
        if (!(obj instanceof ResultSet)) {
            throw new SQLException("Integer cannot be converted to ResultSet");
        }
        return (ResultSet) obj;
    }

    @Override
    public Object execute(String sql) throws SQLException {
        return execute(sql, null, false, false);
    }

    @Override
    public Object execute(String sql, boolean needLog) throws SQLException {
        return execute(sql, null, false, needLog);
    }

    @Override
    public Object execute(String sql, QueryResult result) throws SQLException {
        return execute(sql, result, false, false);
    }

    @Override
    public Object execute(String sql, QueryResult result, boolean withWarnings, boolean needLog) throws SQLException {
        return execute(sql, result, withWarnings, needLog, 0);
    }

    private Object execute(String sql, QueryResult result, boolean withWarnings, boolean needLog, int step) throws SQLException {

        busy.set(true);
        try {
            lastActivity = new Date().getTime();

            if (result != null) {
                result.setStartDuration(System.currentTimeMillis());
                result.setQuery(sql);
            }

            if (needLog) {
                logger.log(getDB(), sql);
            }

            Statement stmt = getDB().createStatement();
            boolean executeIndicator = stmt.execute(sql/*, Statement.RETURN_GENERATED_KEYS*/);//supports more that just mysql servers now
            if (result != null) {
                result.setExecutionDuration(System.currentTimeMillis());
            }

            //@TODO SIR ANDRY
            // you can use this flag getDB().getMetaData().supportsGetGeneratedKeys() to check whether
            //current query can return generated keys etc.
            
            Object res;
            if (executeIndicator) {

                res = stmt.getResultSet();
                if (result != null) {

                    result.addResult(res);
                    while (true) {
                        if (stmt.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                            result.addResult(stmt.getResultSet());
                        } else if (stmt.getUpdateCount() >= 0) {
                            result.addResult(stmt.getUpdateCount());
                        } else {
                            break;
                        }
                    }

                    result.setEndDuration(System.currentTimeMillis());
                    result.setResultSet(true);
                }
            } else {
                res = stmt.getUpdateCount();
                if (result != null) {
                    result.addResult(res);

                    while (true) {
                        if (stmt.getMoreResults(Statement.KEEP_CURRENT_RESULT)) {
                            result.addResult(stmt.getResultSet());
                        } else if (stmt.getUpdateCount() >= 0) {
                            result.addResult(stmt.getUpdateCount());
                        } else {
                            break;
                        }
                    }

//                    result.setEndDuration(System.currentTimeMillis());
//                    result.setGeneratedKey(stmt.getGeneratedKeys());
                }
            }

            if (result != null && withWarnings) {
                SQLWarning warn = stmt.getWarnings();
                while (warn != null) {
                    result.addWarningCode(String.valueOf(warn.getErrorCode()));
                    result.addWarningMessage(warn.getMessage());
                    warn = warn.getNextWarning();
                }
            }

//            // fire that database is changed if query USE database
//            if (sql.trim().toLowerCase().startsWith("USE ")) {
//                String database = sql.trim().substring(4).trim();
//                if (database.endsWith(";")) {
//                    database = database.substring(0, database.length() - 1);
//                }
//                
//                fireDatabaseChanged(database);
//            }
            return res;
        } catch (SQLException ex) {

            // Try recreate 3 time connetion
            if (step < 3 && ex instanceof CommunicationsException) {
                connection = null;
                return execute(sql, result, withWarnings, needLog, step + 1);
            }

            throw ex;
        } finally {

            if (!serviceBusy) {
                busy.set(false);
            }
        }
    }

    @Override
    public Double getExecutionTime(String sql) throws SQLException {
        //System.out.println("getexecution");

        double duration;

        Statement s = getDB().createStatement();
        s.executeUpdate(getProfilesStartSQL());
        s.execute(sql);
        try (ResultSet rs = s.executeQuery(getProfilesShowSQL())) {
            rs.next();
            duration = rs.getDouble("Duration");
            System.out.println(String.format(
                    "     Statement: %s\nExecution time: %f seconds.",
                    rs.getString("Query"),
                    duration));

            s.executeUpdate(getProfilesStopSQL());
        }
        return duration;
    }

    @Override
    public List<String> getTablesNames(Database database) throws SQLException {
        List<String> tables = new ArrayList<>();
        try (ResultSet rs = executeQuery(getTablesSQL(database))) {
            while (rs.next()) {
                tables.add(rs.getString(1));
            }
        }
        //log.debug("Total Tables: {}", tables.size());
        return tables;
    }

    @Override
    public List<DBTable> getTables(Database d) throws SQLException {
//        changeDatabase(d);
        List<DBTable> tables = new ArrayList<>();
        try (ResultSet rs = executeQuery(getTablesSQL(d))) {
            while (rs.next()) {
                String tableName = rs.getString(1);
                DBTable t = new DBTable(tableName, null, null, null, null, d, null, null, null, null);
                List<Column> columns = getDescribeColumn(d.getName(), tableName);
                t.setColumns(columns);

                setIndexes(t);
//                setForeignKeys(t);

                tables.add(t);
            }
        }
        //log.debug("Total Tables: {}", tables.size());
        return tables;
    }

    @Override
    public List<View> getViews(Database d) throws SQLException {

        //changeDatabase(d);
        List<View> views = new ArrayList<>();
        ResultSet rs = executeQuery(getViewsSQL(d));
        while (rs.next()) {
            String viewName = rs.getString(1);
//            System.out.println("view name : " + viewName);            
            //View v = new View(viewName, null, d, getDescribeColumn(d.getName(), viewName), null);
            View v = new View(viewName, null, d, null, null);
            views.add(v);
        }
        //log.debug("Total Views: {}", views.size());
        return views;
    }

    public Database changeDatabase(Database d) throws SQLException {
        Database old = database;
        database = d;
        if (d != null) {
            execute(getChangeDBSQL(d));
        }

        return old;
    }

    @Override
    public List<Trigger> getTriggers(Database d) throws SQLException {
//        changeDatabase(d);
        List<Trigger> triggers = new ArrayList<>();
        ResultSet rs = executeQuery(getTriggersSQL(d));
        while (rs.next()) {
            String triggerName = rs.getString(1);
            Trigger t = new Trigger(triggerName, d, null);
            triggers.add(t);
        }
        //log.debug("Total Triggers: {}", triggers.size());
        return triggers;
    }

    @Override
    public List<Function> getFunctions(Database d) throws SQLException {
//        changeDatabase(d);
        List<Function> functions = new ArrayList<>();
        ResultSet rs = executeQuery(getFunctionsSQL(d));
        while (rs.next()) {
            String functionName = rs.getString(1);
            Function f = new Function(functionName, d, null);
            functions.add(f);
        }
        //log.debug("Total functions: {}", functions.size());
        return functions;
    }

    @Override
    public List<StoredProc> getStoredProcs(Database d) throws SQLException {
//        changeDatabase(d);
        List<StoredProc> storedProcs = new ArrayList<>();
        ResultSet rs = executeQuery(getStoredProcsSQL(d));
        while (rs.next()) {
            String tableName = rs.getString(1);
            StoredProc sp = new StoredProc(tableName, d, null);
            storedProcs.add(sp);
        }
        //log.debug("Total StoredProcs: {}", storedProcs.size());
        return storedProcs;
    }

    @Override
    public List<Event> getEvents(Database d) throws SQLException {
//        changeDatabase(d);

        List<Event> events = new ArrayList<>();

        if (getDbSQLVersionMaj() == 5 && getDbSQLVersionMin() == 0) {
            return events;
        }

        ResultSet rs = executeQuery(getEventsSQL(d));
        while (rs.next()) {
            String eventName = rs.getString(1);
            Event e = new Event(eventName, d, null);
            events.add(e);
        }
        return events;
    }

    @Override
    public List<String> getCharSet() throws SQLException {
        List<String> charsets = new ArrayList();

        try (ResultSet rs = (ResultSet) executeQuery(getCharsetSQL())) {
            while (rs.next()) {
                charsets.add(rs.getString(1));
            }
        }

        return charsets;
    }

    @Override
    public List<String> getCollation(String charset) throws SQLException {
        List<String> collations = new ArrayList();
        try (ResultSet rs = (ResultSet) executeQuery(getCollationSQL(charset))) {

            System.out.println("charset s : " + charset);
            while (rs.next()) {
                collations.add(rs.getString(1));
            }
        }

        //log.debug("size of collation : {}", collations.size());
        return collations;
    }

    @Override
    public List<String> getEngine() throws SQLException {
        List<String> engines = new ArrayList();
        try (ResultSet rs = (ResultSet) executeQuery(getEngineSQL())) {
            while (rs.next()) {
                engines.add(rs.getString(1));
            }
        }

        //log.debug("size of engines : {}" + engines.size());
        return engines;
    }

    @Override
    public DBTable getTableSchemaInformation(String databaseName, String tableName) throws SQLException {

        if (databaseName == null) {
            databaseName = param.getSelectedDatabase() != null ? param.getSelectedDatabase().getName() : (param.getDatabase() != null && !param.getDatabase().isEmpty() ? param.getDatabase().get(0) : null);
        }

        DBTable table = new DBTable();
        try (ResultSet rs = (ResultSet) executeQuery(getShowTableStatusSQL(databaseName, tableName))) {
            if (rs.next()) {
//                System.out.println("hello");
                String tableEngine = rs.getString("Engine");
                String tableCollation = rs.getString("Collation");
                String comment = rs.getString("Comment");
//                int autoIncrement = (rs.getString("Auto_increment")!=null)?Integer.parseInt(rs.getString("Auto_increment"):0);
                int autoIncrement = (rs.getString("Auto_increment") != null) ? Integer.parseInt(rs.getString(
                        "Auto_increment")) : 0;

                String createOptions = rs.getString("Create_options");
                System.out.println("createoptions:" + createOptions);
                Advanced advanced = null;
                if (createOptions != null) {
                    String[] parseCreateOptions = createOptions.split(" ");
                    int minrows = 0;
                    int maxrows = 0;
                    int avgRowLength = 0;
                    int checksum = 0;
                    int delayKeyWrite = 0;
                    String rowformat = "";
                    for (String options : parseCreateOptions) {
                        if (options.contains("min_rows")) {
                            minrows = Integer.parseInt(options.substring(
                                    options.indexOf(
                                            "=") + 1, options.length()));
                        }
//                        minrows = Integer.parseInt(String.format("%s", options.contains("min_rows") ? options.substring(
//                                  options.indexOf(
                        if (options.contains("max_rows")) {
                            maxrows = Integer.parseInt(options.substring(
                                    options.indexOf(
                                            "=") + 1, options.length()));
                        }
//                        System.out.println("options:" + options);
//                        maxrows = Integer.parseInt(String.format("%s",
//                                  options.contains("max_rows") ? options.substring(
//                                                      options.indexOf(
//                                                                "=") + 1, options.length()) : "0"));
                        if (options.contains("avg_row_length")) {
                            avgRowLength = Integer.parseInt(options.substring(
                                    options.indexOf(
                                            "=") + 1, options.length()));
                        }
//                        avgRowLength = Integer.parseInt(String.format("%s",
//                                  options.contains("avg_row_length") ? options.substring(
//                                                      options.indexOf(
//                                                                "=") + 1, options.length()) : "0"));
                        if (options.contains("checksum")) {
                            checksum = Integer.parseInt(String.format(options.substring(options.indexOf(
                                    "=") + 1, options.length())));
                        }
                        if (options.contains("row_format")) {
                            rowformat = String.format(options.substring(options.indexOf(
                                    "=") + 1, options.length()));
                        }
//                        checksum = Integer.parseInt(String.format("%s",
//                                  options.contains("checksum") ? options.substring(options.indexOf(
//                                                                "=") + 1, options.length()) : "0"));
                        if (options.contains("delay_ley_write")) {
                            delayKeyWrite = Integer.parseInt(options.substring(
                                    options.indexOf(
                                            "=") + 1, options.length()));
                        }
//                        delayKeyWrite = Integer.parseInt(String.format("%s",
//                                  options.contains("delay_ley_write") ? options.substring(
//                                                      options.indexOf(
//                                                                "=") + 1, options.length()) : "0"));
                    }

                    log.debug("Row Format: {}", rowformat.toUpperCase());

                    advanced = new Advanced(comment, autoIncrement, avgRowLength, maxrows, minrows,
                            (rowformat.isEmpty() ? null : Rowformat.valueOf(rowformat.toUpperCase())),
                            Checksum.values()[checksum],
                            DelayKeyWrite.values()[delayKeyWrite]);
                }
                if (tableCollation != null) {
                    String charset = getCharsetFromCollation(tableCollation);
                    table.setCollation(tableCollation);
                    table.setCharacterSet(charset);
                }
                table.setDatabase(new Database(null, databaseName, null, null, null, null, null, null, null, null, null));
                table.setColumns(getColumnInformation(databaseName, tableName));
                for (Column col : table.getColumns()) {
                    col.setTable(table);
                }

                table.setDatabase(new Database(null, databaseName, null, null, null, null, null, null, null, null, null));
                table.setAdvanced(advanced);

                log.debug("advanced : {}", advanced);
//                System.out.println("tableengine : " + tableEngine);
//                System.out.println("tableCollation : " + tableCollation);

                EngineType engineType = null;
                MySQLEngineTypes types = new MySQLEngineTypes();
                switch (getTechType()) {
                    case MYSQL:
                        engineType = types.getTypeByName(tableEngine);
                        break;
                }
                table.setEngineType(engineType);

                table.setName(tableName);

//                System.out.println("column size  : " + table.getColumns().size());
                setIndexes(table);
                setForeignKeys(table);

                log.debug("table :{}", table.getCollation());
                return table;
            }
        }

        return null;
    }

    @Override
    public List<Column> getColumnInformation(String databaseName, String tableName) throws SQLException {
        List<Column> columns = new ArrayList<>();
        //System.out.println("databasename:" + databaseName + " table name :" + tableName);

        try (ResultSet rs = (ResultSet) executeQuery(getColumnInformationSQL(databaseName, tableName))) {
//            Long id = (long)1;
            while (rs.next()) {
                String field = rs.getString(1);
                String type = rs.getString(2);
                String collation = rs.getString(3);
                String nulls = rs.getString(4);
                String key = rs.getString(5);
                String defaults = rs.getString(6);
                String extra = rs.getString(7);
                String privileges = rs.getString(8);
                String comment = rs.getString(9);

                Column column = new Column(field, null, getDatatype(type), defaults,
                        false, false, false,
                        false,
                        false, false, null, null, collation);

                column.setKey(key);
                column.setExtra(extra);
                column.setPrivileges(privileges);

                if (key.equals("PRI")) {
                    column.setPrimaryKey(true);
                }
                if (nulls.equals("NO")) {
                    column.setNotNull(true);
                }
                if (extra.contains("auto_increment")) {
                    column.setAutoIncrement(true);
                }
                if (type.contains("unsigned")) {
                    column.setUnsigned(true);
                }
                if (type.contains("zerofill")) {
                    System.out.println("zerofill xa");
                    column.setZeroFill(true);
                }
                if (extra.contains("on update")) {
                    column.setOnUpdate(true);
                }
                if (collation != null) {
                    column.setCharacterSet(getCharsetFromCollation(collation));
                }
                if (comment.length() > 0) {
                    column.setComment(comment);
                }
                //log.debug("{}", column);
                columns.add(column);
//                id++;
            }
        }

        return columns;
    }

    @Override
    public ResultSet getColumnInformationResultSet(String databaseName, String tableName) throws SQLException {
        return (ResultSet) executeQuery(getColumnInformationSQL(databaseName, tableName));
    }

    public DataType getDatatype(String type) {
        Integer size = null;
        Integer precision = null;
        Set elements = null;

        if (type != null && type.contains("(")) {
            if (type.toLowerCase().startsWith("set") || type.toLowerCase().startsWith("enum")) {
                String metric = type.substring(type.indexOf("(") + 1, type.lastIndexOf(")"));
                String[] ele = (metric.split(","));
                elements = new TreeSet<>();
                for (String s : ele) {
                    s = s.trim();
                    elements.add(s.startsWith("'") && s.endsWith("'") ? s.substring(1, s.length() - 1) : s);
                }
            } else if (type.contains(",")) {
                String metric = type.substring(type.indexOf("(") + 1, type.lastIndexOf(")"));
                size = Integer.parseInt(metric.split(",")[0].trim());
                precision = Integer.parseInt(metric.split(",")[1].trim());
            } else {
                size = Integer.parseInt(type.substring(type.indexOf("(") + 1, type.lastIndexOf(")")).trim());
            }

        }

        DataType dataType = null;

        if (type != null) {

            MySQLDataTypes types = new MySQLDataTypes();
            switch (getTechType()) {

                case MYSQL:
                    type = type.substring(0, (type.contains("(") ? type.indexOf("(") : type.length())).trim();
                    if (type.contains(" ")) {
                        type = type.split(" ")[0].trim();
                    }
                    dataType = types.getTypeByName(type);
                    if (dataType != null) {
                        ((MySQLDataType) dataType).setLength(size);
                        ((MySQLDataType) dataType).setPrecision(precision);
                        ((MySQLDataType) dataType).setList(elements);
                    }
                    break;
            }
        }

        return dataType;
    }

    // @TODO Not user, need remove
//    @Override
//    public int alterTable(String databaseName, String tableName, Map<String, List<String>> columnsAndValues) throws SQLException {
//        String sql = getAlterTableSQL(databaseName, tableName, columnsAndValues);
//        System.out.println("alter table sql : " + sql);
//        return (int)execute(sql);
//    }
    @Override
    public List<String> getListOfCollationFromSameCollationMember(String Collation) throws SQLException {
        String charset = getCharsetFromCollation(Collation);
        List<String> collations = getCollation(charset);
        if (collations.size() <= 0) {
            return null;
        }
        return collations;
    }

    @Override
    public Object renameTable(String databaseName, String oldTableName, String newTableName) throws SQLException {
        String sql = getRenameTableSQL(databaseName, oldTableName, newTableName);
        System.out.println("sql : " + sql);
        return execute(sql);
    }

    @Override
    public Object changeTableType(String databaseName, String tableName, String type) throws SQLException {
        return execute(getChangeTableTypeSQL(databaseName, tableName, type));
    }

    @Override
    public String getCharsetFromCollation(String collation) throws SQLException {
        for (String s : getCharSet()) {
            if (collation.contains(s)) {
                return s;
            }
        }
        return null;
    }

    @Override
    public void setForeignKeys(DBTable table) throws SQLException {
        List<ForeignKey> foreignKeys = new ArrayList();

        String sql = getForeignKeysSQL(table.getDatabase().getName(), table.getName());
        try (ResultSet rs = executeQuery(sql)) {

            Map<String, ForeignKey> foreignKeysMap = new LinkedHashMap<>();
            Map<String, List<Column>> tableColumnsMap = new HashMap<>();

            while (rs.next()) {

                String name = rs.getString("contraintName");
                String column = rs.getString("columnName");

                String onUpdate = rs.getString("onUpdate");
                String onDelete = rs.getString("onDelete");

                String referenceDatabase = rs.getString("referenceDatabase");
                String referenceTable = rs.getString("referenceTable");
                String referenceColumn = rs.getString("referenceColumn");

                ForeignKey fk = foreignKeysMap.get(name);
                if (fk == null) {

                    fk = new ForeignKey(name, new ArrayList<>(), referenceDatabase, referenceTable, new ArrayList<>(), onUpdate, onDelete);
                    foreignKeysMap.put(name, fk);
                    foreignKeys.add(fk);
                }

                Column col = table.getColumnByName(column);
                if (col != null && !fk.getColumns().contains(col)) {
                    fk.getColumns().add(col);
                }

                List<Column> tableColumns = tableColumnsMap.get(referenceDatabase + "_" + referenceTable);
                if (tableColumns == null) {
                    tableColumns = new ArrayList<>(getColumnInformation(referenceDatabase, referenceTable));
                    tableColumnsMap.put(referenceDatabase + "_" + referenceTable, tableColumns);
                }

                Column refCol = null;
                for (Column c : tableColumns) {
                    if (c.getName().equals(referenceColumn)) {
                        refCol = c;
                        break;
                    }
                }
                if (refCol != null && !fk.getReferenceColumns().contains(refCol)) {
                    fk.getReferenceColumns().add(refCol);
                }
            }

            table.setForeignKeys(foreignKeys);
        }
    }

    public static void readIndexs(DBTable table, ResultSet rs) throws SQLException {
        List<Index> indexes = new ArrayList();

        List<Column> primaryColumns = new ArrayList();
        Map<String, List<Column>> uniqueIndexes = new LinkedHashMap<>();
        Map<String, List<Column>> fullTextIndexes = new LinkedHashMap<>();
        Map<String, List<Column>> keyIndexes = new LinkedHashMap<>();

        Map<String, Long> seqInIndexMap = new HashMap<>();
        Map<String, Long> cardinalityInIndexMap = new HashMap<>();
        Map<String, Integer> columnLengthMap = new HashMap<>();

        while (rs.next()) {

            String keyName = rs.getString("Key_name");
            String indexType = rs.getString("Index_type");
            String columnName = rs.getString("Column_name");

            Long nonUnique = rs.getLong("Non_unique");

            seqInIndexMap.put(columnName, rs.getLong("Seq_in_index"));
            cardinalityInIndexMap.put(columnName, rs.getLong("Cardinality"));
            columnLengthMap.put(columnName, rs.getInt("Sub_part"));

            Column column = table.getColumnByName(columnName);

            if ("PRIMARY".equals(keyName)) {
                primaryColumns.add(column);

            } else if ("FULLTEXT".equals(indexType)) {
                List<Column> fulltextColumns = fullTextIndexes.get(keyName);
                if (fulltextColumns == null) {
                    fulltextColumns = new ArrayList<>();
                    fullTextIndexes.put(keyName, fulltextColumns);
                }

                fulltextColumns.add(column);

            } else if (nonUnique == 0) {
                // its unique columns
                List<Column> uniqueColumns = uniqueIndexes.get(keyName);
                if (uniqueColumns == null) {
                    uniqueColumns = new ArrayList<>();
                    uniqueIndexes.put(keyName, uniqueColumns);
                }

                uniqueColumns.add(column);
            } else {
                List<Column> keyColumns = keyIndexes.get(keyName);
                if (keyColumns == null) {
                    keyColumns = new ArrayList<>();
                    keyIndexes.put(keyName, keyColumns);
                }

                keyColumns.add(column);
            }
        }

        if (!primaryColumns.isEmpty()) {
            indexes.add(new Index(IndexType.PRIMARY.getDisplayValue().toUpperCase(), table, primaryColumns, IndexType.PRIMARY, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
        }

        if (!uniqueIndexes.isEmpty()) {
            for (Map.Entry<String, List<Column>> entry : uniqueIndexes.entrySet()) {
                indexes.add(new Index(entry.getKey(), table, entry.getValue(), IndexType.UNIQUE_TEXT, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
            }
        }

        if (!fullTextIndexes.isEmpty()) {
            for (Map.Entry<String, List<Column>> entry : fullTextIndexes.entrySet()) {
                indexes.add(new Index(entry.getKey(), table, entry.getValue(), IndexType.FULL_TEXT, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
            }
        }

        if (!keyIndexes.isEmpty()) {
            for (Map.Entry<String, List<Column>> entry : keyIndexes.entrySet()) {
                indexes.add(new Index(entry.getKey(), table, entry.getValue(), null, seqInIndexMap, cardinalityInIndexMap, columnLengthMap));
            }
        }

        table.setIndexes(indexes);
    }

    @Override
    public void setIndexes(DBTable table) throws SQLException {

        String sql = getIndexSQL(table.getDatabase().getName(), table.getName());
        try (ResultSet rs = executeQuery(sql)) {
            readIndexs(table, rs);
        }
    }

    @Override
    public ResultSet getKeys(String databaseName, String tableName) throws SQLException {
        return (ResultSet) executeQuery(getKeysSQL(databaseName, tableName));
    }

    @Override
    public List<Column> getDescribeColumn(String databaseName, String tableName) throws SQLException {
        List<Column> columns = new ArrayList<>();
        try (ResultSet rs = (ResultSet) executeQuery(getDescribeColumnSQL(databaseName, tableName))) {

            int order = 0;
            while (rs.next()) {
                String type = rs.getString("Type");
                String columnName = rs.getString("Field");
                String primary = rs.getString("Key");
                DataType dataType = getDatatype(type);

                Column column = new Column(columnName, columnName, order, -1, null, dataType, null, false,
                        primary.equals("PRI"), false, false,
                        false,
                        false, null, null, null, null, null, null, null, null, null, null, null, null, true);
                order++;

                columns.add(column);
            }
        }

        //log.debug("size of column : {}", columns.size());
        return columns;
    }

    @Override
    public Object dropDatabase(Database database) throws SQLException {
        return execute(getDropDatabaseSQL(database.getName()));
    }

    @Override
    public Object dropIndex(String database, String table, String index) throws SQLException {
        return execute(getDropIndexSQL(database, table, index));
    }

    @Override
    public Object dropColumn(Column column) throws SQLException {
        return execute(getDropColumnSQL(column.getTable().getDatabase().getName(), column.getTable().getName(), column.getName()));
    }

    @Override
    public Object createDatabase(String databaseName, String character, String collate) throws SQLException {
        return execute(getCreateDatabaseSQL(databaseName, character, collate));
    }

    @Override
    public Object alterDatabase(String databaseName, String character, String collate) throws SQLException {
        return execute(getAlterDatabaseSQL(databaseName, character, collate));
    }

    @Override
    public Object copyTable(String databaseName, String sourceTable, String destinationTable, List<Column> columns, boolean withIndexes, boolean onlyStructure, boolean withTriggers) throws SQLException {

        List<String> strColumns = columns.stream().map(c -> c.getName()).collect(Collectors.toList());

        String indexes = "";
        if (withIndexes) {
            try (ResultSet rs = getKeys(databaseName, sourceTable)) {

                List<String> primaryIndexes = new ArrayList<>();
                Map<String, String> keyIndexes = new LinkedHashMap<>();
                Map<String, List<String>> uniqueIndexes = new HashMap<>();

                // read all indexes
                while (rs.next()) {
                    String key = rs.getString("Key_name");
                    int nonUnique = rs.getInt("Non_unique");
                    String columnName = rs.getString("Column_name");

                    // its primary key
                    if ("PRIMARY".equals(key)) {
                        if (strColumns.contains(columnName)) {
                            primaryIndexes.add(columnName);
                        }
                    } // is unique index
                    else if (nonUnique == 0) {
                        List<String> indx = uniqueIndexes.get(key);
                        if (indx == null) {
                            indx = new ArrayList<>();
                            uniqueIndexes.put(key, indx);
                        }

                        indx.add(columnName);
                    } else {
                        keyIndexes.put(key, columnName);
                    }
                }

                // Make primary key query
                if (!primaryIndexes.isEmpty()) {
                    indexes += "PRIMARY KEY(";
                }
                for (String primaryColumn : primaryIndexes) {
                    indexes += "`" + primaryColumn + "`, ";
                }

                if (!primaryIndexes.isEmpty()) {
                    indexes = indexes.substring(0, indexes.length() - 2) + ")";
                }

                // Make unique query
                if (!indexes.isEmpty() && !uniqueIndexes.isEmpty() && !keyIndexes.isEmpty()) {
                    indexes += ", ";
                }

                for (String uniqueName : uniqueIndexes.keySet()) {

                    List<String> uniqueColumns = uniqueIndexes.get(uniqueName);

                    // check if any columns are not present in needed columns -
                    // than unique index no need add - because may be an violation of the uniqueness
                    boolean needContinue = false;
                    for (String s : uniqueColumns) {
                        if (!strColumns.contains(s)) {
                            needContinue = true;
                            break;
                        }
                    }

                    if (needContinue) {
                        continue;
                    }

                    if (uniqueColumns != null && !uniqueColumns.isEmpty()) {
                        indexes += "UNIQUE `" + uniqueName + "`(";

                        for (String uniqueColumn : uniqueColumns) {
                            indexes += "`" + uniqueColumn + "`, ";
                        }

                        indexes = indexes.substring(0, indexes.length() - 2) + "), ";
                    }
                }

                if (!uniqueIndexes.isEmpty()) {
                    indexes = indexes.substring(0, indexes.length() - 2);
                }

                // Make unique query
                if (!indexes.isEmpty() && !keyIndexes.isEmpty()) {
                    indexes += ", ";
                }

                for (String key : keyIndexes.keySet()) {
                    indexes += "KEY `" + key + "` (`" + keyIndexes.get(key) + "`), ";
                }

                if (!keyIndexes.isEmpty()) {
                    indexes = indexes.substring(0, indexes.length() - 2);
                }
            }
        }

        DBTable table = getTableSchemaInformation(databaseName, sourceTable);

        String query = "CREATE TABLE `" + (databaseName != null ? databaseName + "`.`" : "") + destinationTable + "` "
                + (!indexes.isEmpty() ? "(" + indexes + ")" : "")
                + " ENGINE = " + (table.getEngineType() != null ? table.getEngineType().getName() : "")
                + " COLLATE = " + table.getCollation()
                + " COMMENT = '" + (table.getAdvanced() != null ? table.getAdvanced().getComment() : "") + "' ";

        query += "SELECT ";
        for (String column : strColumns) {
            query += "`" + column + "`, ";
        }

        // remove last ', '
        query = query.substring(0, query.length() - 2);

        query += " FROM `" + (databaseName != null ? databaseName + "`.`" : "") + sourceTable + "`";

        if (onlyStructure) {
            query += "  WHERE 1 = 0";
        }

        int result = (int) execute(query);
        if (result == -1) {
            return -1;
        }

        if (withTriggers) {
            List<String> triggersQueries = new ArrayList<>();
            try (ResultSet rs = executeQuery(getShowTriggersSQL(databaseName, sourceTable))) {
                while (rs.next()) {
                    triggersQueries.add(
                            "CREATE TRIGGER `" + rs.getString("Trigger") + "_" + destinationTable + "` "
                            + rs.getString("Timing") + " " + rs.getString("Event") + " ON `" + destinationTable + "` "
                            + "FOR EACH ROW " + rs.getString("Statement")
                    );
                }
            }

            if (!triggersQueries.isEmpty()) {
                for (String s : triggersQueries) {
                    result = (int) execute(s);
                    if (result == -1) {
                        return -1;
                    }
                }
            }
        }

        return result;
    }

    @Override
    public Object dropTable(DBTable table) throws SQLException {
        return execute(getDropTableSQL(table.getDatabase().getName(), table.getName()));
    }

    @Override
    public Object truncateTable(DBTable table) throws SQLException {
        return execute(getTruncateTableSQL(table.getDatabase().getName(), table.getName()));
    }

    @Override
    public Object dropStoredProcedure(StoredProc storedProcedure) throws SQLException {
        return execute(getDropStoredProcedureSQL(storedProcedure.getDatabase().getName(), storedProcedure.getName()));
    }

    @Override
    public Object dropFunction(Function function) throws SQLException {
        return execute(getDropFunctionSQL(function.getDatabase().getName(), function.getName()));
    }

    @Override
    public Object dropTrigger(Trigger trigger) throws SQLException {
        return execute(getDropTriggerSQL(trigger.getDatabase().getName(), trigger.getName()));
    }

    @Override
    public Object dropEvent(Event event) throws SQLException {
        return execute(getDropEventSQL(event.getDatabase().getName(), event.getName()));
    }

    @Override
    public Object renameEvent(Event event, String newName) throws SQLException {
        return execute(getRenameEventTemplateSQL(event.getDatabase().getName(), event.getName(), newName));
    }

    @Override
    public Object renameView(View view, String newName) throws SQLException {

        try (ResultSet result = (ResultSet) execute(getViewDefinitionSQL(view.getDatabase().getName(), view.getName()))) {

            String viewDefinition = null;

            if (result != null && result.next()) {
                viewDefinition = result.getString(1);
            }

            if (viewDefinition != null) {
                // create new view
                execute(getCreateViewSQL(view.getDatabase().getName(), newName, viewDefinition));
                // drop old view
                return execute(getDropViewSQL(view.getDatabase().getName(), view.getName()));
            }
        }

        return null;
    }

    @Override
    public String getRenameTriggerSQL(Trigger trigger, String newName) throws SQLException {

        String newTrigger = null;

        Object obj = execute(getShowCreateTriggerSQL(trigger.getDatabase().getName(), trigger.getName()));
        if (obj instanceof ResultSet) {

            try (ResultSet rs = (ResultSet) obj) {
                if (rs.next()) {
                    newTrigger = getTriggerDefinitionSQL(trigger.getDatabase().getName(), trigger.getName(), rs.getString("SQL Original Statement").replace(trigger.getName(), newName));
                }
            }
        }

        return newTrigger;
    }

    @Override
    public Object dropView(View view) throws SQLException {
        return execute(getDropViewSQL(view.getDatabase().getName(), view.getName()));
    }

    @Override
    public String getLoadDataInFileSQL(String file, String databaseName, String tableName, String charset,
            String priorityType, String dubplicateAction, String fieldsTerminated, String fieldsEnclosed, boolean enclosedOptionally, String fieldsEscaped,
            String lineTerminated, Integer ignoreLines, List<String> columns) {

        String fields = "";
        if (fieldsTerminated != null && !fieldsTerminated.isEmpty()) {
            fields += " FIELDS TERMINATED BY '" + fieldsTerminated + "'";
        }

        if (fieldsEnclosed != null && !fieldsEnclosed.isEmpty()) {
            fields += (fields.isEmpty() ? " FIELDS " : " ") + (enclosedOptionally ? "OPTIONALLY " : "") + "ENCLOSED BY '" + fieldsEnclosed + "'";
        }

        if (fieldsEscaped != null && !fieldsEscaped.isEmpty()) {
            fields += (fields.isEmpty() ? " FIELDS " : " ") + "ESCAPED BY '" + fieldsEscaped + "'";
        }

        String lines = "";
        if (lineTerminated != null && !lineTerminated.isEmpty()) {
            lines += " LINES TERMINATED BY '" + lineTerminated + "'";
        }

        return "LOAD DATA" + (priorityType != null && priorityType.isEmpty() ? " " + priorityType : "")
                + " LOCAL INFILE " + "'" + file + "'"
                + (dubplicateAction != null && dubplicateAction.isEmpty() ? " " + dubplicateAction : "")
                + " INTO TABLE `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "`"
                + (charset != null && !charset.isEmpty() ? " CHARACTER SET '" + charset + "'" : "") + fields + lines
                + (ignoreLines != null ? " IGNORE " + ignoreLines + " LINES" : "")
                + (columns != null && !columns.isEmpty() ? " (" + getCommaSeparated(columns) + ")" : "");
    }

    protected static String getCommaSeparated(List<String> primaryKeyColumnUpdated) {
        String sql = "";
        int i = 1;
        for (String updCol : primaryKeyColumnUpdated) {
            sql += "`" + updCol + "`";
            if (i < primaryKeyColumnUpdated.size()) {
                sql += ",";
            }
            i++;

        }
        return sql;
    }

    protected static String getCommaSeparated(Index index, List<String> primaryKeyColumnUpdated) {
        String sql = "";
        int i = 1;
        for (String updCol : primaryKeyColumnUpdated) {
            sql += "`" + updCol + "`";

            int length = index.getColumnLength(updCol);
            if (length > 0) {
                sql += "(" + length + ")";
            }

            if (i < primaryKeyColumnUpdated.size()) {
                sql += ",";
            }
            i++;

        }
        return sql;
    }

    // TODO Need move them to some abstraction
    private File getApplicationPublicFilesAndStorageDirectory(String subDirectoryOrFile) {
        StringBuilder sb = new StringBuilder(
                System.getProperty("user.home"))
                .append(File.separator)
                .append("Documents")
                .append(File.separator)
                .append(getApplicationBinaryName());

        File finalFile = new File(sb.toString());

        if (subDirectoryOrFile != null && !subDirectoryOrFile.isEmpty()) {
            finalFile = new File(sb.toString(), subDirectoryOrFile);
        }

        return finalFile;
    }

    private String getApplicationBinaryName() {
        String templateAppName = System.getProperty("name");
        if (templateAppName == null || templateAppName.isEmpty()) {
            templateAppName = System.getProperty("project_name");
            templateAppName = templateAppName.substring(templateAppName.lastIndexOf('_') + 1).toUpperCase();
            if (templateAppName.equalsIgnoreCase("UI")) {
                templateAppName = "QB";
            }
            if ("2".equals(System.getProperty("buildType", "2"))) {
                templateAppName = templateAppName + "_QA";
            }
            templateAppName = "SmartMySQL" + templateAppName;
        }
        return templateAppName;
    }

    protected void saveDatabaseStructure(Map<String, Object> databaseData, String database) {
        File dir = getApplicationPublicFilesAndStorageDirectory("caches");
        dir.mkdirs();

        try (
                FileWriter fw = new FileWriter(dir.getAbsolutePath() + param.cacheKey() + "_" + database + ".json");
                JsonWriter jw = new JsonWriter(fw);) {

            new Gson().toJson(databaseData, HashMap.class, jw);

        } catch (IOException ex) {
            log.error("Error", ex);
        }
    }

    public Map<String, Object> loadDatabaseStructure(String database) {
        File dir = getApplicationPublicFilesAndStorageDirectory("caches");
        File f = new File(dir.getAbsolutePath() + param.cacheKey() + "_" + database + ".json");
        if (f.exists()) {
            try (FileReader fr = new FileReader(f)) {

                return (Map<String, Object>) new Gson().fromJson(fr, HashMap.class);

            } catch (IOException ex) {
                log.error("Error", ex);
            }
        }

        return null;
    }

    protected void executeInitCommands(Connection connection, ConnectionParams params) throws SQLException {
        // try run init commands
        // in future may be we can change this for some param
        if (params.getInitCommands() != null && !params.getInitCommands().trim().isEmpty()) {
            String[] initCommands = params.getInitCommands().split(";");
            for (String initCommand : initCommands) {
                SQLException error = null;
                try {
                    connection.createStatement().execute(initCommand);
                } catch (SQLException ex) {
                    error = ex;
                } finally {
                    if (error != null) {
                        connection.close();
                        connection = null;
                        throw error;
                    }
                }
            }
        }
    }
        
    @Override
    public Connection getDB() throws SQLException {
        if (connection == null && !inited || isClosed()) {
            inited = true;
            connection = connect(param);
            if (getDatabase() != null) {
                changeDatabase(getDatabase());
            }
        }
        return connection;
    }
    
    public interface ConnectionListener { void connected(); }
}
