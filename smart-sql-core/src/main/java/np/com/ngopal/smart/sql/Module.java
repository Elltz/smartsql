/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public interface Module {

    /**
     * This method gets executed during the initialization of the application.
     * <p>
     * @return boolean installation either true or false.
     */
    public abstract boolean install();

    /**
     * When user clicks on save connection this will get executed.
     * <p>
     * @param param
     * @return
     * @throws Exception
     */
    public ConnectionParams saveConnection(ConnectionParams param) throws Exception;

    /**
     * When different Connection is selected by the user it will get executed.
     * <p>
     * @param service ConnectionSession
     * @param tab     Tab
     * @throws Exception
     */
    public ConnectionParams connectionChanged(ConnectionParams param) throws Exception;

    /**
     * When any connection gets opened with NewConnection this will get executed
     * on appropriate module.
     * <p>
     * @param service ConnectionSession
     * @param tab     Tab
     * @throws Exception
     */
    public void postConnection(ConnectionSession service, Tab tab) throws Exception;

    /**
     * This method will be executed but in future purpose.
     * <p>
     * @return
     */
    public abstract boolean uninstall();

    /**
     * Provides the name of Module with respective to the class name removing
     * "Module" string from the class name.
     * <p>
     * @return class name of Module with excluded "Module" string.
     */
    public String getName();

    public int getOrder();
    
    public String getInfo();
}
