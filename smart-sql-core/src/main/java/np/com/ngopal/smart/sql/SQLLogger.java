/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql;

import com.google.inject.Singleton;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Singleton
@Getter
public class SQLLogger {

    private List<SQLListener> listeners;

    public SQLLogger() {
        listeners = new ArrayList<>();
    }

    public void addListener(SQLListener listener) {
        listeners.add(listener);
    }

    public void removeListener(SQLListener listener) {
        listeners.remove(listener);
    }

    public void log(Connection connection, String sql) {
        this.log(connection, sql, false);
    }
    
    public void log(Connection connection, String sql, boolean executedByUser) {
        for (SQLListener listener : listeners) {
            listener.listen(connection, sql, executedByUser);
        }
    }

}
