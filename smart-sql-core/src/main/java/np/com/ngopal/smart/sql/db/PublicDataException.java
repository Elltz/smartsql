package np.com.ngopal.smart.sql.db;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class PublicDataException extends Throwable {

    private boolean criticalError = false;
    public PublicDataException(String message, Throwable th, boolean criticalError) {
        super(message, th);
        this.criticalError = criticalError;
    }
    
    public PublicDataException(String message, Throwable th) {
        super(message, th);
    }

    public boolean isCriticalError() {
        return criticalError;
    }
    
}
