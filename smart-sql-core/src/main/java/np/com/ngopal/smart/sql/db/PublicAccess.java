package np.com.ngopal.smart.sql.db;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import np.com.ngopal.smart.sql.model.ErrDiagram;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public interface PublicAccess {
    
    public boolean login(String login, String password) throws PublicDataException;
    
    public List<ErrDiagram> loadAllErrDiagrams() throws PublicDataException;    
    public List<ErrDiagram> searchErrDiagram(String text) throws PublicDataException;
    public boolean deleteErrDiagram(Long id) throws PublicDataException;
    public boolean updateErrDiagram(String schema, String diagramData, Long domainId, Long subDomainId, String description, Long directPublicId) throws PublicDataException;
    public Long insertErrDiagram(String schema, String diagramData, String database, Long domainId, Long subDomainId, String description, String emailId) throws PublicDataException;
    public List<Map<String, Object>> loadDomains() throws PublicDataException;
    public List<Map<String, Object>> loadSubDomains() throws PublicDataException;
    public Long getIdByDomain(String domain) throws PublicDataException;
    public Long insertDomain(String domain) throws PublicDataException;
    public Long getIdBySubDomain(String subDomain) throws PublicDataException;
    public Long insertSubDomain(String domain) throws PublicDataException;
    
    
    public Timestamp lastUpdateUExpire(String email, String type, String expired) throws PublicDataException;
    public void insertUExpire(String email, int type, int usedCredits, int usedFullUsage, String expired, String lastUpdate) throws PublicDataException;
    public String userLicense(String email) throws PublicDataException;
    public boolean userValidation(String email, String license) throws PublicDataException;
    
    
    public void userInstallUpdates(String userEmailId, String os, String macAddress, String ipAddress, String date) throws PublicDataException;
    public void userCloseApp(String userEmailId, String os, String macAddress, String ipAddress, String dateOpen, String dateClose) throws PublicDataException;    
    public Long selectUserSystemId(String userEmailId, String localIP, String globalIP) throws PublicDataException;
    public Long insertUserSystem(String userEmailId, String localIP, String globalIP) throws PublicDataException;
    public boolean syncUserUsageStatistic(int productId, List<Map<String, Object>> data) throws PublicDataException;
}
