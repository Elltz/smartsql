/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql;

import java.sql.Connection;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public interface SQLListener {

    void listen(Connection connection, String logMsg, boolean exectedByUser);
}
