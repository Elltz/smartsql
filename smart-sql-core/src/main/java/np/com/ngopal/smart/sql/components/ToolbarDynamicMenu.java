package np.com.ngopal.smart.sql.components;

import java.util.function.BiConsumer;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ToolbarDynamicMenu extends Menu {

    private final BiConsumer<MenuButton, EventHandler<Event>> onShowingConsumer;
    
    public ToolbarDynamicMenu(BiConsumer<MenuButton, EventHandler<Event>> onShowingConsumer, String name, Node graphic) {
        super(name, graphic);
        this.onShowingConsumer = onShowingConsumer;
    }
    
    public void onShowing(MenuButton menuButton, EventHandler<Event> returnToDefaultTopMenuHandler) {
        onShowingConsumer.accept(menuButton, returnToDefaultTopMenuHandler);
    }
}
