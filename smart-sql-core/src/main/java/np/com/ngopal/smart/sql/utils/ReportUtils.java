package np.com.ngopal.smart.sql.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.ScheduleReport;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public final class ReportUtils {
    
    private ReportUtils() {
    }
    
    public interface ReportStatusCallback {
        
        void error(String errorMessage, Throwable th);
        void success(List<String> messages);
    }
    
    public static void doReport(DBService dbService, ScheduleReport settings, ReportUtils.ReportStatusCallback callback) {
        
        FileWriter logWriter = null;
        if (settings.getLogFile() != null) {
            try {
                logWriter = new FileWriter(settings.getLogFile(), true);
            } catch (IOException ex) {
                log.error("Error", ex);
                callback.error("Error", ex);
                if (settings.isExitImmediatly()) {
                    return;
                }
            }
        }
        try {
            dbService.connect(settings.toConnectionParams());
            
            dbService.execute("USE " + settings.getDatabase());
            
            // getting queries for report
            String queries[] = null;
            if (settings.isSelectFile()) {
                try {
                    byte[] data = Files.readAllBytes(new File(settings.getSelectFilePath()).toPath());

                    queries = new String(data, "UTF-8").split(";");
                } catch (Throwable th) {
                    logMessage(logWriter, "Error encountered when running report scheduler", th);

                    if (settings.isExitImmediatly()) {
                        callback.error("Error encountered when running report scheduler", th);
                        return;
                    }
                }
            } else {
                queries = settings.getQuery().split(";");
            }
            
            
            if (queries != null) {
                List<String> messages = new ArrayList<>();                
                for (String query: queries) {
                    if (!query.trim().isEmpty())
                    {
                        try {
                            // Execute query
                            Statement st = dbService.getDB().createStatement();
                            
                            boolean executeIndicator = st.execute(query, Statement.RETURN_GENERATED_KEYS);
                            if (executeIndicator) {
                                ResultSet result = st.getResultSet();

                                // If result set - if need send email - need get aall data
                                if (settings.isSendResultAnEmail()) {
                                    messages.add(makeEmailMessage(settings, query, result));
                                }

                                ((ResultSet)result).close();
                                // if integer than just return value
                            } else {
                                Integer r = st.getUpdateCount();
                                if (r >= 0) {
                                    if (settings.isSendResultAnEmail()) {
                                        messages.add(makeEmailMessage(settings, query, r));
                                    }
                                }                        
                            }
                        } catch (SQLException ex) {
                            logMessage(logWriter, "Error encountered when running report scheduler", ex);
                            if (settings.isExitImmediatly()) {
                                callback.error("Error encountered when running report scheduler", ex);
                                return;
                            } else {
                                messages.add(makeEmailMessage(settings, query, ex));
                            }
                        }
                    }
                }
                
                callback.success(messages);
            }
        } catch (SQLException ex) {
            logMessage(logWriter, "Error encountered when running report scheduler", ex);
            if (settings.isExitImmediatly()) {
                callback.error("Error encountered when running report scheduler", ex);
            }
        }
    }
    
    
    private static String makeEmailMessage(ScheduleReport settings, String query, Object result) throws SQLException {
        
        StringBuilder sb = new StringBuilder();
        sb.append("<div bgcolor=\"white\">");
            sb.append("<p style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;font-style:normal;font-weight:normal;color:#000000;text-decoration:none\">");
                if (settings.isSendResultAnEmail() && settings.isIncludeOriginalQuery()) {
                    sb.append("Query: ").append(query).append("<br><br>");
                }
                
                if (result instanceof Integer) {
                    
                    sb.append("Result: ").append(result.toString());
                    
                } else if (result instanceof Throwable){
                    
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    ((Throwable)result).printStackTrace(pw);

                    sb.append(sw.toString().replaceAll("\n", "<br>"));
                } else {
                    
                    sb.append("<table border=\"1\" width=\"100%\" cellspacing=\"0\" style=\"font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;border:1px #999999 solid\"><tbody>");
                    
                    // for result set we must create table
                    ResultSet rs = (ResultSet)result;                    
                    ResultSetMetaData rsmd = rs.getMetaData();
                    
                    // add row with columns
                    sb.append("<tr style=\"font-weight:bold;background-color:#d8d8d8;height:auto;width:auto\">");
                        for (int i = 0; i < rsmd.getColumnCount(); i++) {
                            sb.append("<td>").append(rsmd.getColumnName(i + 1)).append("</td>");
                        }
                    sb.append("</tr>");
 
                    int size = 0;
                    // add result set data
                    while(rs.next()) {
                        sb.append("<tr style=\"background-color:#f8f8f8;height:auto;width:auto\">");
                        for (int i = 0; i < rsmd.getColumnCount(); i++) {
                            Object obj = rs.getObject(i + 1);
                            sb.append("<td>").append(obj != null ? obj.toString() : "NULL").append("</td>");
                        }
                        sb.append("</tr>");
                        
                        size++;
                    }                    
                    sb.append("</tbody></table>");
                    
                    if (settings.isSendResultAnEmail() && settings.isIncludeMessagesFromServer()) {
                        sb.append("<br><font face=\"Verdana\" size=\"2\">(").append(size).append(" row(s) returned)</font><br><br><p></p><p></p>");
                    }
                }
            sb.append("</p>");
        sb.append("</div>");
        
        return sb.toString();
    }
    
    
    private static void logMessage(FileWriter logWriter, String message, Throwable th) {
        if (th != null) {
            log.error(message, th);
        }
        
        if (logWriter != null) {
            try {
                logWriter.append(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss: ").format(new Date()) + message);
                logWriter.append("\r\n");
                
                if (th != null) {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    th.printStackTrace(pw);

                    logWriter.append(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss: ").format(new Date()) + sw.toString());
                    logWriter.append("\r\n");
                }                
                
                logWriter.append("\r\n\r\n");
            } catch (IOException ex) {
                log.error("Error", ex);
            }
        }        
    }
}
