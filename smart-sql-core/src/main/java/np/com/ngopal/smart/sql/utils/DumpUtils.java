package np.com.ngopal.smart.sql.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.ScheduleParams;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringEscapeUtils;

/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public final class DumpUtils {
    
    private DumpUtils() {}
    
    public interface DumpStatusCallback {
        
        void error(String errorMessage, Throwable th);
        void success();
        
        void progress(int progress, String objectLevel, String currentDumpedObject);
    }
    
    public interface CopyStatusCallback {
        
        void error(String errorMessage, Throwable th);
        void success(String summary);
        
        void progress(int progress);
        void info(String info);
    }
    
    private static String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, queryId);
    }
    
    private static String getFormatedQuery(String queryId, Object...values) {
        return String.format(getQuery(queryId), values);
    }
    
    private static String wrapMultiQuery(String multiQuery, String newQuery) {
        multiQuery += newQuery;
        if (!multiQuery.trim().endsWith(";")) {
            multiQuery += ";";
        }
        
        return multiQuery;
    }
    
    public static boolean dumpAllDatabase(DBService dbService, ScheduleParams settings, DumpStatusCallback callback) {
        
        String exportPath = settings.getFilesFileField();
        
        FileWriter logWriter = null;
        if (settings.getLogFile() != null) {
            try {
                File logFile = new File(settings.getLogFile());
                if (!logFile.exists()) {
                    logFile.getParentFile().mkdirs();
                    logFile.createNewFile();
                    logWriter = new FileWriter(settings.getLogFile());
                } else {
                    logWriter = new FileWriter(settings.getLogFile(), true);
                }
            } catch (IOException ex) {
                log.error("Error", ex);
                callback.error("Error", ex);
                if (settings.isAbortOnError()) {
                    return false;
                }
            }
        }
        
        
        try {
            if (!settings.isZiping() && settings.isSubFolderWithTimestamp()) {
                String dir = exportPath.substring(0, exportPath.lastIndexOf(File.separatorChar) + 1);
                String file = exportPath.substring(exportPath.lastIndexOf(File.separatorChar) + 1);

                String newDir = dir + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
                exportPath = newDir + file;

                new File(newDir).mkdirs();
            }

            if (!settings.isZiping() && settings.isPrefixWithTimestamp()) {
                String dir = exportPath.substring(0, exportPath.lastIndexOf(File.separatorChar) + 1);
                String file = exportPath.substring(exportPath.lastIndexOf(File.separatorChar) + 1);

                exportPath = dir + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) + file;
            }

            String zipPath = null;
            if (settings.isZiping()) {

                String filepath = exportPath.substring(0, exportPath.lastIndexOf("."));            
                String filename = filepath.substring(filepath.lastIndexOf(File.separatorChar) + 1);

                zipPath = exportPath;
                try {
                    exportPath = File.createTempFile(filename + ".sql", "").getAbsolutePath();
                } catch (IOException ex) {

                    logMessage(logWriter, "Error", ex);

                    if (settings.isAbortOnError()) {
                        return false;
                    }
                }
            }

            boolean error = false;

            try (
                    FileWriter fw = new FileWriter(exportPath, !settings.isFilesPrefixTimestampOverride());
//                    BufferedWriter buff = new BufferedWriter(fw);
                ) {
                
                int index = 0;
                
                // COLLECT QUERIES                
                String multiQuery = wrapMultiQuery("", getFormatedQuery(QueryProvider.QUERY__CHANGE_DATABASE, settings.getDatabase()));
                index++;
                
                if (settings.isSingleTransaction() || settings.isLockTables()) {
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__SET_SESSION_TRANSACTION));//"SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ");
                    index++;
                    
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__START_TRANSACTION));//"START TRANSACTION /*!40100 WITH CONSISTENT SNAPSHOT */");
                    index++;
                }
                
                // execute if option selected
                if (settings.isFlushMasterLog()) {
                    // FLUSH MASTER - deprecated
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__RESET_MASTER));//"RESET MASTER");
                    index++;
                } 
                
                // execute if option selected
                if (settings.isFlushSlaveLog()) {
                    // FLUSH SLAVE - deprecated
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__RESET_SLAVE));//"RESET SLAVE");
                    index++;
                }

                // execute if option selected
                if (settings.isFlushLogs()) {
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__FLUSH_LOGS));//"FLUSH LOGS");
                    index++;
                }
                
                // TABLES
                String[] tables = settings.getSelectedData().get("TABLES") != null ? settings.getSelectedData().get("TABLES").split(":") : null;
                if (tables != null && tables.length > 0) {

                    for (String t: tables) {

                        if (!settings.isExportOnlyData()) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateTableSQL(settings.getDatabase(), t));
                        }
                        
                        if (!settings.isExportOnlyStructure()) {
                            multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, settings.getDatabase(), t));
                            
                            multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SHOW_COLUMNS, settings.getDatabase(), t));
                        }
                    }
                }
                
                if (!settings.isExportOnlyData()) {
                    // TRIGGERS
                    String[] triggers = settings.getSelectedData().get("TRIGGERS") != null ? settings.getSelectedData().get("TRIGGERS").split(":") : null;
                    if (triggers != null) {                
                        for (String trigger: triggers) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateTriggerSQL(settings.getDatabase(), trigger));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // EVENTS
                    String[] events = settings.getSelectedData().get("EVENTS") != null ? settings.getSelectedData().get("EVENTS").split(":") : null;
                    if (events != null) {
                        for (String event: events) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateEventSQL(settings.getDatabase(), event));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // FUNCTIONS
                    String[] functions = settings.getSelectedData().get("FUNCTIONS") != null ? settings.getSelectedData().get("FUNCTIONS").split(":") : null;
                    if (functions != null) {
                        for (String function: functions) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateFunctionSQL(settings.getDatabase(), function));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // STORED PROCEDURES
                    String[] procedures = settings.getSelectedData().get("STORED_PROCS") != null ? settings.getSelectedData().get("STORED_PROCS").split(":") : null;
                    if (procedures != null) {
                        for (String procedure: procedures) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateStoredProcedureSQL(settings.getDatabase(), procedure));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // VIEWS
                    String[] views = settings.getSelectedData().get("VIEWS") != null ? settings.getSelectedData().get("VIEWS").split(":") : null;
                    if (views != null) {                
                        for (String view: views) {                            
                            multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SHOW_VIEW_FIELDS, settings.getDatabase(), view));
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateViewSQL(settings.getDatabase(), view));
                        }
                    }
                    
                    if (settings.isStopExport()) {
                        return false;
                    }
                }

//                StringBuilder sb = new StringBuilder();

                // DUMP TITLE
                dumpTitle(dbService, fw, settings);

//                buff.append(sb);
//                sb = new StringBuilder();

                // EXECUTE MULTI QUERY
                QueryResult qr = new QueryResult();
                dbService.execute(multiQuery, qr);
                

                // DUMP TABLES
                if (tables != null && tables.length > 0) {

                    int tableStep = 70 / tables.length;

                    int status = 0;

                    for (String t: tables) {

                        ResultSet showCreateTableResultSet = null; 
                        ResultSet selectAllResultSet = null; 
                        ResultSet showColumnsResultSet = null; 
                        
                        if (!settings.isExportOnlyData()) {
                            showCreateTableResultSet = (ResultSet) qr.getResult().get(index);
                            index++;
                        }
                        
                        if (!settings.isExportOnlyStructure()) {
                            selectAllResultSet = (ResultSet) qr.getResult().get(index);
                            index++;
                            
                            showColumnsResultSet = (ResultSet) qr.getResult().get(index);
                            index++;
                        }
                        
                        dumpTable(dbService, showCreateTableResultSet, selectAllResultSet, showColumnsResultSet, fw, t, settings);

//                        buff.append(sb);
//                        sb = new StringBuilder();
                        // call gc after dump table data - it may be large
                        System.gc();

                        if (settings.isStopExport()) {
                            return false;
                        }

                        status += tableStep;
                        callback.progress(status, "TABLES", t);
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }

                callback.progress(70, null,null);

                if (!settings.isExportOnlyData()) {
                    // DUMP TRIGGERS
                    String[] triggers = settings.getSelectedData().get("TRIGGERS") != null ? settings.getSelectedData().get("TRIGGERS").split(":") : null;
                    if (triggers != null) {                
                        for (String trigger: triggers) {
                            
                            ResultSet rs = (ResultSet) qr.getResult().get(index);
                            index++;
                            
                            dumpTrigger(dbService, rs, fw, trigger, settings);
                            callback.progress(76,"TRIGGER",trigger);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }

//                        buff.append(sb);
//                        sb = new StringBuilder();
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(76,null,null);

                    // DUMP EVENTS
                    String[] events = settings.getSelectedData().get("EVENTS") != null ? settings.getSelectedData().get("EVENTS").split(":") : null;
                    if (events != null) {

                        fw.append("\n");
                        fw.append("/*!50106 set global event_scheduler = 1*/;\n");

                        for (String event: events) {
                            ResultSet rs = (ResultSet) qr.getResult().get(index);
                            index++;
                            
                            dumpEvent(dbService, rs, fw, event, settings);
                            callback.progress(82,"EVENT",event);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }

//                        buff.append(sb.toString());
//                        sb = new StringBuilder();

                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(82,null,null);

                    // DUMP FUNCTIONS
                    String[] functions = settings.getSelectedData().get("FUNCTIONS") != null ? settings.getSelectedData().get("FUNCTIONS").split(":") : null;
                    if (functions != null) {
                        for (String function: functions) {
                            ResultSet rs = (ResultSet) qr.getResult().get(index);
                            index++;
                            
                            dumpFunction(dbService, rs, fw, function, settings);
                            callback.progress(88,"FUNCTION",function);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }

//                        buff.append(sb.toString());
//                        sb = new StringBuilder();
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(88,null,null);

                    // DUMP STORED PROCEDURES
                    String[] procedures = settings.getSelectedData().get("STORED_PROCS") != null ? settings.getSelectedData().get("STORED_PROCS").split(":") : null;
                    if (procedures != null) {
                        for (String procedure: procedures) {
                            ResultSet rs = (ResultSet) qr.getResult().get(index);
                            index++;
                            
                            dumpSoredProcedure(dbService, rs, fw, procedure, settings);
                            callback.progress(94,"STORED PROCEDURE",procedure);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }

//                        buff.append(sb.toString());
//                        sb = new StringBuilder();
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(94,null,null);

                    // DUMP VIEWS
                    String[] views = settings.getSelectedData().get("VIEWS") != null ? settings.getSelectedData().get("VIEWS").split(":") : null;
                    if (views != null) {                
                        for (String view: views) {
                            
//                            buff.append(sb.toString());
//                            sb = new StringBuilder();
                        
                            try {
                                ResultSet rs0 = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                ResultSet rs = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                dumpView(dbService, rs0, rs, fw, view, settings);
                                callback.progress(98, "VIEW", view);
                            } catch (Throwable ex) {
                                if (settings.isAbortOnError()) {
                                    throw new SQLException(ex);
                                }
                                
//                                sb = new StringBuilder();
                            }

                            if (settings.isStopExport()) {
                                return false;
                            }
                        }
                    }
                    
//                    buff.append(sb.toString());
//                    sb = new StringBuilder();

                    if (settings.isStopExport()) {
                        return false;
                    }
                }
                
                callback.progress(98,null,null);

                dumpFooter(fw);

//                buff.append(sb);

                fw.flush();
//                buff.close();
                
            } catch (Throwable th) {
                logMessage(logWriter, "Error when running dump all object", th);
                callback.error("Error when running dump all object", th);

                error = true;
                if (settings.isAbortOnError()) {
                    return false;
                }
            } finally {

                try {
                    if (settings.isSingleTransaction() || settings.isLockTables()) {
                        dbService.execute(getQuery(QueryProvider.QUERY__UNLOCK_TABLES));//"UNLOCK TABLES");
                    }
                } catch (SQLException ex) {
                    logMessage(logWriter, "Error when set autocommit true", ex);
                    callback.error("Error when set autocommit true", ex);
                    if (settings.isAbortOnError()) {
                        return false;
                    }
                }
            }

            if (!error) { 
                if (settings.isZiping()) {

                    if (settings.isPrefixWithTimestamp()) {
                        String dir = zipPath.substring(0, zipPath.lastIndexOf(File.separatorChar) + 1);
                        String file = zipPath.substring(zipPath.lastIndexOf(File.separatorChar) + 1);

                        zipPath = dir + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) + file;
                    }
                    File file = new File(exportPath);

                    try (
                            FileOutputStream fos = new FileOutputStream(zipPath);
                            ZipOutputStream zip = new ZipOutputStream(fos);
                            FileInputStream fileInputStream = new FileInputStream(file);
                        ){

                        String name = exportPath.substring(exportPath.lastIndexOf(File.separatorChar) + 1, exportPath.lastIndexOf(".sql"));
                        ZipEntry ze = new ZipEntry(name + ".sql");
                        zip.putNextEntry(ze);


                        byte[] buf = new byte[1024];
                        int bytesRead;

                        while ((bytesRead = fileInputStream.read(buf)) > 0) {
                            zip.write(buf, 0, bytesRead);
                        }

                        zip.closeEntry();

                    } catch (IOException ex) {
                        logMessage(logWriter, "Error", ex);
                        callback.error("Error", ex);
                        if (settings.isAbortOnError()) {
                            return false;
                        }
                    }

                    // delete sql dump
                    file.delete();
                    logMessage(logWriter, "dump complete!", null);
                } else {
                    logMessage(logWriter, "dump complete!", null);
                }
                
                callback.success();
            }

            callback.progress(100,null,null);
        } finally {
            if (logWriter != null) {
                try {
                    logWriter.flush();
                    logWriter.close();
                } catch (IOException ex) {
                    log.error("Error", ex);
                }
            }
        }
        
        return true;
    }
    
    public static boolean dumpPerObjectDatabase(DBService dbService, ScheduleParams settings, DumpStatusCallback callback) {
        
        String exportPath = settings.getFilesFileField();
        FileWriter logWriter = null;
        if (settings.getLogFile() != null) {
            try {
                logWriter = new FileWriter(settings.getLogFile(), true);
            } catch (IOException ex) {
                log.error("Error", ex);
                callback.error("Error", ex);
                if (settings.isAbortOnError()) {
                    return false;
                }
            }
        }
        
        try {
            boolean error = false;

            if (!settings.isZiping()) {
                File dirFile = new File(exportPath);
                if (!dirFile.exists() || !dirFile.isDirectory()) {
                    callback.error("The selected file must be a directory and must exists", null);
                    return false;
                }

                exportPath = dirFile.getAbsolutePath();
            }

            if (!settings.isZiping() && settings.isSubFolderWithTimestamp()) {
                exportPath += File.separatorChar + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
                new File(exportPath).mkdirs();
            }

            String zipPath = null;
            List<String> filesFprZiping = null;
            if (settings.isZiping()) {

                filesFprZiping = new ArrayList<>();                
                String filename = exportPath.substring(exportPath.lastIndexOf(File.separatorChar) + 1);

                zipPath = exportPath;
                try {
                    exportPath = Files.createTempDirectory(filename + "_schedule").toString();
                } catch (IOException ex) {
                    logMessage(logWriter, "Error", ex);
                    callback.error("Error", ex);
                    if (settings.isAbortOnError()) {
                        return false;
                    }
                }
            }

            try {

                int index = 0;
                
                String multiQuery = "";
                
                if (settings.isSingleTransaction() || settings.isLockTables()) {
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__SET_SESSION_TRANSACTION));//"SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ");
                    index++;
                    
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__START_TRANSACTION));//"START TRANSACTION /*!40100 WITH CONSISTENT SNAPSHOT */");
                    index++;
                }

                // execute if option selected
                if (settings.isFlushLogs()) {
                    multiQuery = wrapMultiQuery(multiQuery, getQuery(QueryProvider.QUERY__FLUSH_LOGS));//"FLUSH LOGS");
                    index++;
                }


//                StringBuilder sb = new StringBuilder();

                // DUMP TABLES
                String[] tables = settings.getSelectedData().get("TABLES") != null ? settings.getSelectedData().get("TABLES").split(":") : null;
                if (tables != null && tables.length > 0) {
                    
                    for (String t: tables) {
                        if (!settings.isExportOnlyData()) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateTableSQL(settings.getDatabase(), t));
                        }
                        
                        if (!settings.isExportOnlyStructure()) {
                            multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, settings.getDatabase(), t));
                            
                            multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SHOW_COLUMNS, settings.getDatabase(), t));
                        }
                    }
                }
                
                if (!settings.isExportOnlyData()) {
                    // TRIGGERS
                    String[] triggers = settings.getSelectedData().get("TRIGGERS") != null ? settings.getSelectedData().get("TRIGGERS").split(":") : null;
                    if (triggers != null) {                
                        for (String trigger: triggers) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateTriggerSQL(settings.getDatabase(), trigger));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // EVENTS
                    String[] events = settings.getSelectedData().get("EVENTS") != null ? settings.getSelectedData().get("EVENTS").split(":") : null;
                    if (events != null) {
                        for (String event: events) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateEventSQL(settings.getDatabase(), event));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // FUNCTIONS
                    String[] functions = settings.getSelectedData().get("FUNCTIONS") != null ? settings.getSelectedData().get("FUNCTIONS").split(":") : null;
                    if (functions != null) {
                        for (String function: functions) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateFunctionSQL(settings.getDatabase(), function));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // STORED PROCEDURES
                    String[] procedures = settings.getSelectedData().get("STORED_PROCS") != null ? settings.getSelectedData().get("STORED_PROCS").split(":") : null;
                    if (procedures != null) {
                        for (String procedure: procedures) {
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateStoredProcedureSQL(settings.getDatabase(), procedure));
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    // VIEWS
                    String[] views = settings.getSelectedData().get("VIEWS") != null ? settings.getSelectedData().get("VIEWS").split(":") : null;
                    if (views != null) {                
                        for (String view: views) {                            
                            multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SHOW_VIEW_FIELDS, settings.getDatabase(), view));
                            multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateViewSQL(settings.getDatabase(), view));
                        }
                    }
                    
                    if (settings.isStopExport()) {
                        return false;
                    }
                }

                QueryResult qr = new QueryResult();
                dbService.execute(multiQuery, qr);
                
                if (tables != null && tables.length > 0) {
                    int tableStep = 70 / tables.length;                
                    int status = 0;

                    for (String t: tables) {

                        String path = exportPath + File.separatorChar + 
                                    (!settings.isZiping() && settings.isPrefixWithTimestamp() ? new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) : "") +
                                    "table_" + t + ".sql";

                        if (settings.isZiping()) {
                            filesFprZiping.add(path);
                        }

                        try (
                            FileWriter fw = new FileWriter(
                                path, 
                                !settings.isFilesPrefixTimestampOverride());
//                            BufferedWriter buff = new BufferedWriter(fw); 
                                ) {

                            // DUMP TITLE
                            dumpTitle(dbService, fw, settings);
                            
                            ResultSet showCreateTableResultSet = null; 
                            ResultSet selectAllResultSet = null; 
                            ResultSet showColumnsResultSet = null; 

                            if (!settings.isExportOnlyData()) {
                                showCreateTableResultSet = (ResultSet) qr.getResult().get(index);
                                index++;
                            }

                            if (!settings.isExportOnlyStructure()) {
                                selectAllResultSet = (ResultSet) qr.getResult().get(index);
                                index++;

                                showColumnsResultSet = (ResultSet) qr.getResult().get(index);
                                index++;
                            }

                            dumpTable(dbService, showCreateTableResultSet, selectAllResultSet, showColumnsResultSet, fw, t, settings);

                            dumpFooter(fw);

//                            buff.append(sb);
//                            sb = new StringBuilder();

                            // call gc after dump table data - it may be large
                            System.gc();

                            fw.flush();
//                            buff.close();

                            status += tableStep;
                            callback.progress(status,"TABLES", t);
                        }

                        if (settings.isStopExport()) {
                            return false;
                        }
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }

                callback.progress(70,"TABLE",null);

                if (!settings.isExportOnlyData()) {
                    // DUMP TRIGGERS
                    String[] triggers = settings.getSelectedData().get("TRIGGERS") != null ? settings.getSelectedData().get("TRIGGERS").split(":") : null;
                    if (triggers != null) {                
                        for (String trigger: triggers) {

                            String path = exportPath + File.separatorChar + 
                                (!settings.isZiping() && settings.isPrefixWithTimestamp() ? new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) : "") +
                                "trigger_" + trigger + ".sql";

                            if (settings.isZiping()) {
                                filesFprZiping.add(path);
                            }

                            try (
                                FileWriter fw = new FileWriter(
                                    path, 
                                    !settings.isFilesPrefixTimestampOverride());
//                                BufferedWriter buff = new BufferedWriter(fw); 
                                    ) {

                                // DUMP TITLE
                                dumpTitle(dbService, fw, settings);
                                
                                ResultSet rs = (ResultSet) qr.getResult().get(index);
                                index++;

                                dumpTrigger(dbService, rs, fw, trigger, settings);

                                dumpFooter(fw);

//                                buff.append(sb);

                                fw.flush();
//                                buff.close();

//                                sb = new StringBuilder();
                            }
                            callback.progress(76,"TRIGGER",trigger);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(76,"TRIGGER",null);

                    // DUMP EVENTS
                    String[] events = settings.getSelectedData().get("EVENTS") != null ? settings.getSelectedData().get("EVENTS").split(":") : null;
                    if (events != null) {

                        for (String event: events) {

                            String path = exportPath + File.separatorChar + 
                                (!settings.isZiping() && settings.isPrefixWithTimestamp() ? new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) : "") + 
                                "event_" + event + ".sql";

                            if (settings.isZiping()) {
                                filesFprZiping.add(path);
                            }

                            try (
                                FileWriter fw = new FileWriter(
                                    path, 
                                    !settings.isFilesPrefixTimestampOverride());
//                                BufferedWriter buff = new BufferedWriter(fw); 
                                    ) {

                                // DUMP TITLE
                                dumpTitle(dbService, fw, settings);

                                fw.append("\n");
                                fw.append("/*!50106 set global event_scheduler = 1*/;\n");

                                ResultSet rs = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                dumpEvent(dbService, rs, fw, event, settings);

                                dumpFooter(fw);

//                                buff.append(sb);

                                fw.flush();
//                                buff.close();

//                                sb = new StringBuilder();
                            }
                            callback.progress(82,"EVENTS",event);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(82,"EVENTS",null);

                    // DUMP FUNCTIONS
                    String[] functions = settings.getSelectedData().get("FUNCTIONS") != null ? settings.getSelectedData().get("FUNCTIONS").split(":") : null;
                    if (functions != null) {
                        for (String function: functions) {

                            String path = exportPath + File.separatorChar + 
                                (!settings.isZiping() && settings.isPrefixWithTimestamp() ? new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) : "") +
                                "function_" + function + ".sql";

                            if (settings.isZiping()) {
                                filesFprZiping.add(path);
                            }

                            try (
                                FileWriter fw = new FileWriter(
                                    path, 
                                    !settings.isFilesPrefixTimestampOverride());
//                                BufferedWriter buff = new BufferedWriter(fw); 
                                    ) {

                                // DUMP TITLE
                                dumpTitle(dbService, fw, settings);

                                ResultSet rs = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                dumpFunction(dbService, rs, fw, function, settings);

                                dumpFooter(fw);

//                                buff.append(sb);

                                fw.flush();
//                                buff.close();

//                                sb = new StringBuilder();
                            }
                            callback.progress(88,"FUNCTIONS",function);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }
                    }

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(88,"FUNCTIONS",null);

                    // DUMP STORED PROCEDURES
                    String[] procedures = settings.getSelectedData().get("STORED_PROCS") != null ? settings.getSelectedData().get("STORED_PROCS").split(":") : null;
                    if (procedures != null) {
                        for (String procedure: procedures) {

                            String path = exportPath + File.separatorChar +
                                (!settings.isZiping() && settings.isPrefixWithTimestamp() ? new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) : "") + 
                                "procedure_" + procedure + ".sql";

                            if (settings.isZiping()) {
                                filesFprZiping.add(path);
                            }

                            try (
                                FileWriter fw = new FileWriter(
                                    path, 
                                    !settings.isFilesPrefixTimestampOverride());
//                                BufferedWriter buff = new BufferedWriter(fw);
                                    ) {

                                // DUMP TITLE
                                dumpTitle(dbService, fw, settings);

                                ResultSet rs = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                dumpSoredProcedure(dbService, rs, fw, procedure, settings);

                                dumpFooter(fw);

//                                buff.append(sb);

                                fw.flush();
//                                buff.close();

//                                sb = new StringBuilder();
                            }
                            callback.progress(94,"STORED PROCEDURE",procedure);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }
                    }            

                    if (settings.isStopExport()) {
                        return false;
                    }

                    callback.progress(94,"STORED PROCEDURE",null);

                    // DUMP VIEWS
                    String[] views = settings.getSelectedData().get("VIEWS") != null ? settings.getSelectedData().get("VIEWS").split(":") : null;
                    if (views != null) {                
                        for (String view: views) {

                            String path = exportPath + File.separatorChar + 
                                (!settings.isZiping() && settings.isPrefixWithTimestamp() ? new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) : "") + 
                                "view_" + view + ".sql";

                            if (settings.isZiping()) {
                                filesFprZiping.add(path);
                            }

                            try (
                                FileWriter fw = new FileWriter(
                                    path, 
                                    !settings.isFilesPrefixTimestampOverride());
//                                BufferedWriter buff = new BufferedWriter(fw); 
                                    ) {

                                // DUMP TITLE
                                dumpTitle(dbService, fw, settings);

                                ResultSet rs0 = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                ResultSet rs = (ResultSet) qr.getResult().get(index);
                                index++;
                                
                                dumpView(dbService, rs0, rs, fw, view, settings);

                                dumpFooter(fw);

//                                buff.append(sb);

                                fw.flush();
//                                buff.close();

//                                sb = new StringBuilder();
                            }
                            callback.progress(98,"VIEWS", view);
                            if (settings.isStopExport()) {
                                return false;
                            }
                        }
                    }
                }

                callback.progress(100,null,null);
            } catch (Throwable th) {
                logMessage(logWriter, "Error when running dump per object", th);

                callback.error("Error when running dump all object", th);
                error = true;
                if (settings.isAbortOnError()) {
                    return false;
                }
            } finally {

                try {
                    if (settings.isSingleTransaction() || settings.isLockTables()) {
                        dbService.execute(getQuery(QueryProvider.QUERY__UNLOCK_TABLES));//"UNLOCK TABLES");                    
                    }
                } catch (SQLException ex) {
                    logMessage(logWriter, "Error", ex);
                    callback.error("Error", ex);
                    if (settings.isAbortOnError()) {
                        return false;
                    }
                }
            }

            if (!error) { 
                if (settings.isZiping()) {

                    if (settings.isPrefixWithTimestamp()) {
                        String dir = zipPath.substring(0, zipPath.lastIndexOf(File.separatorChar) + 1);
                        String file = zipPath.substring(zipPath.lastIndexOf(File.separatorChar) + 1);

                        zipPath = dir + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss_").format(new Date()) + file;
                    }


                    try (
                            FileOutputStream fos = new FileOutputStream(zipPath);
                            ZipOutputStream zip = new ZipOutputStream(fos);
                        ){

                        for (String file: filesFprZiping) {

                            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                                String name = file.substring(file.lastIndexOf(File.separatorChar) + 1, file.lastIndexOf(".sql"));
                                ZipEntry ze = new ZipEntry(name + ".sql");
                                zip.putNextEntry(ze);


                                byte[] buf = new byte[1024];
                                int bytesRead;

                                while ((bytesRead = fileInputStream.read(buf)) > 0) {
                                    zip.write(buf, 0, bytesRead);
                                }

                                zip.closeEntry();
                            }

                            // delete temp file
                            new File(file).delete();
                        }
                        
                        logMessage(logWriter, "dump complete!", null);
                    } catch (IOException ex) {
                        logMessage(logWriter, "Error", ex);
                        callback.error("Error", ex);
                    }
                } else {
                    logMessage(logWriter, "dump complete!", null);
                }

                callback.success();
            }
        } finally {
            if (logWriter != null) {
                try {
                    logWriter.flush();
                    logWriter.close();
                } catch (IOException ex) {
                    log.error("Error", ex);
                }
            }
        }
        
        return true;
    }
    
    
    public static boolean copyDatabaseTo(DBService dbService, ConnectionParams destination, ScheduleParams settings, String destinationDatabase, boolean copyAssociatedTriggers, CopyStatusCallback callback) {
                    
        boolean error = false;
        MemoryUsageUtils.printMemoryUsage("copyDatabaseTo");
        
        File tempFile = null;
        try {
            tempFile = File.createTempFile("smartsql_copy_database_to_", ".sql");
        } catch (IOException ex) {
            // it cant be in normal
            log.error("Error", ex);
        }
        
        try (
                FileWriter fw = new FileWriter(tempFile);
//                BufferedWriter buff = new BufferedWriter(sw);
            ) {

            final StringBuilder summary = new StringBuilder();
            
//            StringBuilder sb = new StringBuilder();

            fw.append("CREATE DATABASE /*!32312 IF NOT EXISTS*/ `").append(destinationDatabase).append("` /*!40100 DEFAULT CHARACTER SET utf8 */;\n\n");                
            fw.append("USE `").append(destinationDatabase).append("`;\n\n");                

            fw.append("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n");
            
//            buff.append(sb);
//            sb = new StringBuilder();

            String multiQuery = "";
            String[] tables = settings.getSelectedData().get("TABLES") != null ? settings.getSelectedData().get("TABLES").split(":") : null;
            if (tables != null && tables.length > 0) {

                for (String t: tables) {

                    if (!settings.isExportOnlyData()) {
                        multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateTableSQL(settings.getDatabase(), t));
                    }

                    if (!settings.isExportOnlyStructure()) {
                        multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, settings.getDatabase(), t));

                        multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SHOW_COLUMNS, settings.getDatabase(), t));
                    }
                }
            }
            
            if (!settings.isExportOnlyData()) {
                // TRIGGERS
                String[] triggers = settings.getSelectedData().get("TRIGGERS") != null ? settings.getSelectedData().get("TRIGGERS").split(":") : null;
                if (triggers != null) {                
                    for (String trigger: triggers) {
                        multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateTriggerSQL(settings.getDatabase(), trigger));
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }

                // EVENTS
                String[] events = settings.getSelectedData().get("EVENTS") != null ? settings.getSelectedData().get("EVENTS").split(":") : null;
                if (events != null) {
                    for (String event: events) {
                        multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateEventSQL(settings.getDatabase(), event));
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }

                // FUNCTIONS
                String[] functions = settings.getSelectedData().get("FUNCTIONS") != null ? settings.getSelectedData().get("FUNCTIONS").split(":") : null;
                if (functions != null) {
                    for (String function: functions) {
                        multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateFunctionSQL(settings.getDatabase(), function));
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }

                // STORED PROCEDURES
                String[] procedures = settings.getSelectedData().get("STORED_PROCS") != null ? settings.getSelectedData().get("STORED_PROCS").split(":") : null;
                if (procedures != null) {
                    for (String procedure: procedures) {
                        multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateStoredProcedureSQL(settings.getDatabase(), procedure));
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }

                // VIEWS
                String[] views = settings.getSelectedData().get("VIEWS") != null ? settings.getSelectedData().get("VIEWS").split(":") : null;
                if (views != null) {                
                    for (String view: views) {                            
                        multiQuery = wrapMultiQuery(multiQuery, getFormatedQuery(QueryProvider.QUERY__SHOW_VIEW_FIELDS, settings.getDatabase(), view));
                        multiQuery = wrapMultiQuery(multiQuery, dbService.getShowCreateViewSQL(settings.getDatabase(), view));
                    }
                }

                if (settings.isStopExport()) {
                    return false;
                }
            }
            
            if (copyAssociatedTriggers && tables != null && tables.length > 0) {
                for (String table: tables) {
                    multiQuery = wrapMultiQuery(multiQuery, dbService.getShowTriggersSQL(settings.getDatabase(), table));
                }
            }
            
            MemoryUsageUtils.printMemoryUsage("Before execute");
            // EXECUTE MULTI QUERY
            QueryResult qr = new QueryResult();
            dbService.execute(multiQuery, qr);
            
            MemoryUsageUtils.printMemoryUsage("After execute");

            int index = 0;
            
            // DUMP TABLES
            if (tables != null && tables.length > 0) {

                int tableStep = 70 / tables.length;

                int status = 0;

                for (String t: tables) {

                    ResultSet showCreateTableResultSet = null; 
                    ResultSet selectAllResultSet = null; 
                    ResultSet showColumnsResultSet = null; 

                    if (!settings.isExportOnlyData()) {
                        showCreateTableResultSet = (ResultSet) qr.getResult().get(index);
                        index++;
                    }

                    if (!settings.isExportOnlyStructure()) {
                        selectAllResultSet = (ResultSet) qr.getResult().get(index);
                        index++;

                        showColumnsResultSet = (ResultSet) qr.getResult().get(index);
                        index++;
                    }

                    dumpTable(dbService, showCreateTableResultSet, selectAllResultSet, showColumnsResultSet, fw, t, settings);

//                    buff.append(sb);
//                    sb = new StringBuilder();
                    // call gc after dump table data - it may be large
                    System.gc();

                    if (settings.isStopExport()) {
                        return false;
                    }

                    status += tableStep;
                    callback.progress(status);
                    
                    summary.append("Table '").append(t).append("' copied\n");
                }
                
                summary.append(String.valueOf(tables.length)).append(" table(s) copied\n\n");
            }

            if (settings.isStopExport()) {
                return false;
            }

            callback.progress(70);
            String[] triggers = null;
            if (!settings.isExportOnlyStructure()) {
                // DUMP TRIGGERS
                triggers = settings.getSelectedData().get("TRIGGERS") != null ? settings.getSelectedData().get("TRIGGERS").split(":") : null;
                if (triggers != null) {                
                    for (String trigger: triggers) {
                        
                        ResultSet rs = (ResultSet) qr.getResult().get(index);
                        index++;
                            
                        dumpTrigger(dbService, rs, fw, trigger, settings);

                        if (settings.isStopExport()) {
                            return false;
                        }
                        
                        summary.append("Trigger '").append(trigger).append("' copied\n");
                    }

                    summary.append(String.valueOf(triggers.length)).append(" trigger(s) copied\n\n");
                    
//                    buff.append(sb);
//                    sb = new StringBuilder();
                }

                if (settings.isStopExport()) {
                    return false;
                }

                callback.progress(76);

                // DUMP EVENTS
                String[] events = settings.getSelectedData().get("EVENTS") != null ? settings.getSelectedData().get("EVENTS").split(":") : null;
                if (events != null) {

                    fw.append("\n");
                    fw.append("/*!50106 set global event_scheduler = 1*/;\n");

                    for (String event: events) {
                        
                        ResultSet rs = (ResultSet) qr.getResult().get(index);
                        index++;
                            
                        dumpEvent(dbService, rs, fw, event, settings);

                        if (settings.isStopExport()) {
                            return false;
                        }
                        
                        summary.append("Event '").append(event).append("' copied\n");
                    }

                    summary.append(String.valueOf(events.length)).append(" event(s) copied\n\n");
                    
//                    buff.append(sb.toString());
//                    sb = new StringBuilder();

                }

                if (settings.isStopExport()) {
                    return false;
                }

                callback.progress(82);

                // DUMP FUNCTIONS
                String[] functions = settings.getSelectedData().get("FUNCTIONS") != null ? settings.getSelectedData().get("FUNCTIONS").split(":") : null;
                if (functions != null) {
                    for (String function: functions) {
                        
                        ResultSet rs = (ResultSet) qr.getResult().get(index);
                        index++;
                            
                        dumpFunction(dbService, rs, fw, function, settings);

                        if (settings.isStopExport()) {
                            return false;
                        }
                        
                        summary.append("Function '").append(function).append("' copied\n");
                    }
                    
                    summary.append(String.valueOf(functions.length)).append(" function(s) copied\n\n");

//                    buff.append(sb.toString());
//                    sb = new StringBuilder();
                }

                if (settings.isStopExport()) {
                    return false;
                }

                callback.progress(88);

                // DUMP STORED PROCEDURES
                String[] procedures = settings.getSelectedData().get("STORED_PROCS") != null ? settings.getSelectedData().get("STORED_PROCS").split(":") : null;
                if (procedures != null) {
                    for (String procedure: procedures) {
                        
                        ResultSet rs = (ResultSet) qr.getResult().get(index);
                        index++;
                            
                        dumpSoredProcedure(dbService, rs, fw, procedure, settings);

                        if (settings.isStopExport()) {
                            return false;
                        }
                        
                        summary.append("Procedure '").append(procedure).append("' copied\n");
                    }
                    
                    summary.append(String.valueOf(procedures.length)).append(" procedure(s) copied\n\n");

//                    buff.append(sb.toString());
//                    sb = new StringBuilder();
                }

                if (settings.isStopExport()) {
                    return false;
                }

                callback.progress(94);

                // DUMP VIEWS
                String[] views = settings.getSelectedData().get("VIEWS") != null ? settings.getSelectedData().get("VIEWS").split(":") : null;
                if (views != null) {                
                    for (String view: views) {

//                        buff.append(sb.toString());
//                        sb = new StringBuilder();

                        ResultSet rs0 = (ResultSet) qr.getResult().get(index);
                        index++;
                        
                        ResultSet rs = (ResultSet) qr.getResult().get(index);
                        index++;
                            
                        dumpView(dbService, rs0, rs, fw, view, settings);
                            
                        summary.append("View '").append(view).append("' copied\n");

                        if (settings.isStopExport()) {
                            return false;
                        }
                    }
                    
                    summary.append(String.valueOf(views.length)).append(" view(s) copied\n\n");

//                    buff.append(sb.toString());
//                    sb = new StringBuilder();
                }

                if (settings.isStopExport()) {
                    return false;
                }
            }
            
            if (copyAssociatedTriggers && tables != null && tables.length > 0) {
                List<String> list = triggers == null ? null : Arrays.asList(triggers);
                
                int count = 0;
                for (String table: tables) {
                    ResultSet rs = (ResultSet) qr.getResult().get(index);
                    index++;
                    
                    while(rs.next()) {
                        String triggerName = rs.getString("Trigger");
                        if (list == null || !list.contains(triggerName)) {
                            
                            try (ResultSet rs0 = (ResultSet) dbService.execute(dbService.getShowCreateTriggerSQL(settings.getDatabase(), triggerName))) {
                                
                                dumpTrigger(dbService, rs0, fw, triggerName, settings);

                                summary.append("Associated trigger '").append(triggerName).append("' copied\n");
                                count++;

//                                buff.append(sb);
//                                sb = new StringBuilder();
                            }
                        }
                    }
                }
                
                summary.append(String.valueOf(count)).append(" associated trigger(s) copied\n\n");
            }
            
            fw.append("/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n");
            
//            buff.append(sb);

            fw.flush();
            fw.close();
            
            MysqlDBService targetService = new MysqlDBService(null);
            targetService.connect(destination);
            
            new ScriptRunner(targetService.getDB(), true).runScript(new FileReader(tempFile), new ScriptRunner.ScriptRunnerCallback() {
                
                boolean error = false;
                @Override
                public void started(Connection con, int queriesSize) throws SQLException {
                    con.setAutoCommit(false);
                }
                
                @Override
                public void queryExecuting(Connection con, QueryResult queryResult) {
                }

                @Override
                public void queryExecuted(Connection con, QueryResult queryResult) {
                }

                @Override
                public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {
                    error = true;
                    con.rollback();
                }
                
                @Override
                public boolean isWithLimit(QueryResult queryResult) {
                    return false;
                }

                @Override
                public void finished(Connection con) throws SQLException{
                    
                    con.commit();
                    
                    callback.progress(100);
                    if (!error) {
                        callback.success(summary.toString());
                    }
                }                

                @Override
                public boolean isNeedStop() {
                    return false;
                }
            }, false);

        } catch (Throwable th) {
            callback.error(th.getMessage(), th);

            error = true;
            if (settings.isAbortOnError()) {
                return false;
            }
        }
        
        return true;
    }
      
    private static String getColumnsString(DBService dbService, ResultSet showColumnsResultSet, ScheduleParams settings, String tableName) throws SQLException {
        String result;
        ResultSet cols = showColumnsResultSet;//"SHOW COLUMNS FROM `" + tableName + "`")) {

        List<String> list = new ArrayList<>();
        while (cols.next()) {
            list.add(cols.getString(1));
        }

        List<String> columns = new ArrayList<>();
        for(String s: list){
            columns.add("`" + s + "`");
        }

        result = String.join(",", columns);
        
        
        return result;
    }

    
    private static String getNormalInsertsForTable(DBService dbService, FileWriter sb, ResultSet selectAllResultSet, ResultSet showColumnsResultSet, String tableName, ScheduleParams settings) throws SQLException, IOException {
//        StringBuilder sb = new StringBuilder();
        
        boolean dataExists = false;
        
        String columns = getColumnsString(dbService, showColumnsResultSet, settings, tableName);
        
        try (ResultSet rs = selectAllResultSet) {//"SELECT * FROM `" + tableName + "`")) {
            
            while (rs.next()) {
                dataExists = true;
                
                int colCount = rs.getMetaData().getColumnCount();
                if (colCount > 0) {
                    
                    sb.append("INSERT ").append(settings.isCreateDelayedInsert() ? "DELAYED " : "").append("INTO ").append("`").append(tableName).append("`").append("(").append(columns).append(") VALUES (");
                    for (int i = 0; i < colCount; i++) {
                        if (i > 0) {
                            sb.append(",");
                        }
                        try {
                            getColumnValue(sb, rs, i + 1);
//                            Object obj = rs.getObject(i + 1);
//                            if (obj != null) {
//                                if (obj instanceof Boolean) {
//                                    s += ((Boolean)obj).toString();
//                                } else if (obj instanceof byte[]){
//                                    s += "'" + StringEscapeUtils.escapeSql(new String((byte[])obj)) + "'";
//                                } else {
//                                    s += "'" + StringEscapeUtils.escapeSql(obj.toString()) + "'";
//                                }
//                            } else {
//                                s += "NULL";
//                            }
                        } catch (Exception e) {
                            sb.append("''");
                            if (settings.isAbortOnError()) {
                                throw new SQLException("Error create insert", e);
                            }
                        }
                        
                    }
                    sb.append(");\n");
                }
                
                if (settings.isStopExport()) {
                    return "";
                }
            }
        }
        
        return (dataExists ? "\n" : "") + sb.toString();
    }

    
    private static final String ENTER = "\n";
    private static final String N_INSERT = "\nINSERT ";
    private static final String DELAYED = "DELAYED";
    private static final String EMPTY = "";
    private static final String EMPTY_0 = "''";
    private static final String INTO = "INTO `";
    private static final String BRACER_LEFT = "`(";
    private static final String VALUES = ") VALUES ";
    private static final String COMMA = ",";
    private static final String COMMA_0 = ",\n";
    private static final String BRACER_LEFT_0 = "(";
    private static final String BRACER_RIGHT_0 = ")";
    private static final String SEMICOLON_0 = ";\n";
    
    private static void getBulkInsertsForTable(DBService dbService, FileWriter sb, ResultSet selectAllResultSet, ResultSet showColumnsResultSet, String tableName, ScheduleParams settings) throws SQLException, IOException{
//        StringBuilder sb = new StringBuilder();
        
        MemoryUsageUtils.printMemoryUsage("BEFORE Bulk Insert");
        try (ResultSet rs = selectAllResultSet) {//"SELECT * FROM `" + tableName + "`")) {
            // get rows count and if need INSERT TEXT
            rs.last();
            int numberOfRows = rs.getRow();
            
            rs.beforeFirst();
            
            int index = 0;
            while(index < numberOfRows) {
            
                if (numberOfRows > 0) {
                    sb.append(N_INSERT).append(settings.isCreateDelayedInsert() ? DELAYED : EMPTY).append(INTO).append(tableName).append(BRACER_LEFT).append(getColumnsString(dbService, showColumnsResultSet, settings, tableName)).append(VALUES);
                }

                int colCount = rs.getMetaData().getColumnCount();                

                // get all table data as bulk inserts
                if (index % 100000 == 0) {
                    sb.flush();
                    MemoryUsageUtils.printMemoryUsage("Before index = " + index);
                }
                
                boolean first = true;
                while (rs.next()) {
                    if (!first) {
                        sb.append(settings.isOneRowPerLine() ? COMMA_0 : COMMA);
                    } else {
                        first = false;
                        
                        if (settings.isOneRowPerLine()) {
                           sb.append(ENTER);
                        }
                    }
                                        
                    sb.append(BRACER_LEFT_0);
                    
                    boolean first0 = true;
                    for (int i = 0; i < colCount; i++) {
                        
                        if(!first0) {
                            sb.append(COMMA);
                        } else {
                            first0 = false;
                        }

                        try {
                            getColumnValue(sb, rs, i + 1);
                        } catch (Exception e) {
                            sb.append(EMPTY_0);
                            if (settings.isAbortOnError()) {
                                throw new SQLException("Error create insert", e);
                            }
                        }
                    }

                    sb.append(BRACER_RIGHT_0);
                    
                    if (settings.isStopExport()) {
                        return;
                    }
                    
                    index++;
                    if (settings.isServerDefaultSize() && index % 1000 == 0) {
                        break;
                    }
                }

                if (!first) {
                    sb.append(SEMICOLON_0);
                }
            }
            
            MemoryUsageUtils.printMemoryUsage("DONE");
            
            showColumnsResultSet.close();
        }
        
//        return sb.toString();
    }
    
    private static final String DATE_0 = "'00'";
    private static final String DATE_1 = "'0000-00-00'";
    private static final String TIME_0 = "'00:00:00'";
    private static final String TIMESTAMP_0 = "'0000-00-00 00:00:00'";
    private static final String NULL = "NULL";
    private static final String X_QUOTE = "x'";
    private static final String QUOTE = "'";
    
    private static void getColumnValue(FileWriter sb, ResultSet rs, int column) throws SQLException, IOException {
        ResultSetMetaData rsm = rs.getMetaData();

        int columnType = rsm.getColumnType(column);
        int precision = rsm.getPrecision(column);
                
        String value = rs.getString(column);
        if (value == null) {
            if (rsm.isNullable(column) == 0) {                
                switch (columnType) {
                    case Types.DATE:                        
                        switch (precision) {
                            case 2:
                                sb.append(DATE_0);
                                return;
                            case 4:
                                sb.append(DATE_0);
                                return;
                            default:
                                sb.append(DATE_1);
                                return;
                        }
                                                
                    case Types.TIME:
                    case Types.TIME_WITH_TIMEZONE:
                        sb.append(TIME_0);
                        return;
                        
                    case Types.TIMESTAMP:                
                    case Types.TIMESTAMP_WITH_TIMEZONE:
                        sb.append(TIMESTAMP_0);
                        return;
                }
            }
            sb.append(NULL);
            return;
            
        } else {
            switch (columnType) {
                case Types.BIGINT:
                case Types.BOOLEAN:
                case Types.DECIMAL:
                case Types.DOUBLE:
                case Types.NUMERIC:
                case Types.FLOAT:
                case Types.INTEGER:
                case Types.BIT:
                    sb.append(value);
                    return;
                
                case Types.BLOB:
                case Types.BINARY:
                case Types.VARBINARY:
                case Types.LONGVARBINARY:
                {
                    sb.append(X_QUOTE).append(Hex.encodeHexString(rs.getBytes(column))).append(QUOTE);
                    return;
                }
                
                case Types.DATE:
                    switch (precision) {
                        case 2:
                        case 4:
                            sb.append(QUOTE).append(rs.getString(column)).append(QUOTE);
                            return;
                        default:
                            sb.append(QUOTE).append(StringEscapeUtils.escapeSql(value)).append(QUOTE);
                            return;
                    }
                // TODO Fix other types if need
                default:
                    sb.append(QUOTE).append(StringEscapeUtils.escapeSql(value)).append(QUOTE);
                    return;
            }
        }
    }
    
    private static void dumpTitle(DBService dbService, FileWriter sb, ScheduleParams settings) throws SQLException, IOException {
        DatabaseMetaData metadata = dbService.getDB().getMetaData();
            
        // Title
        sb.append("/*********************************************\n")
            .append("   Smart SQL\n")
            .append("   MySQL - ").append(metadata.getDatabaseProductVersion()).append(" : Database - ").append(settings.getDatabase()).append("\n")
            .append("       Created on ")
            .append(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date())).append("\n")
            .append("**********************************************/\n\n");

        // Static data
        sb.append("/*!40101 SET NAMES ").append(settings.getCharset() == null || "default".equals(settings.getCharset()) ? "utf8" : settings.getCharset()).append(" */;\n");
        sb.append("\n");
        sb.append("/*!40101 SET SQL_MODE=''*/;\n");
        sb.append("\n");
        sb.append("/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;\n");
        // apend only if option selected
        if (settings.isSetForeignKeyChecks()) {
            sb.append("/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n");
        }
        sb.append("/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;\n");
        sb.append("/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;\n");

        // apend only if option selected
        if (settings.isIncludeCreateDatabase()) {
            sb.append("CREATE DATABASE /*!32312 IF NOT EXISTS*/ `").append(settings.getDatabase()).append("` /*!40100 DEFAULT CHARACTER SET utf8 */;\n\n");                
        }

        // apend only if option selected
        if (settings.isIncludeUseDatabase()) {
            sb.append("USE `").append(settings.getDatabase()).append("`;\n\n");                
        }
    }
    
    private static void dumpTable(
            DBService dbService, 
            
            ResultSet showCreateTableResultSet, 
            ResultSet selectAllResultSet, 
            ResultSet showColumnsResultSet, 
            
            FileWriter sb, String table, ScheduleParams settings) throws SQLException, IOException {        

        MemoryUsageUtils.printMemoryUsage("TABLE");
        if (!settings.isExportOnlyData()) {
            sb.append("\n");
            sb.append("/* Table structure for table `").append(table).append("` */\n");

            
            // apend only if option selected
            if (settings.isIncludeDrop()) {
                sb.append("\n").append(dbService.getDropTableSQL(null, table)).append(";\n");
            }

            try (ResultSet rs = showCreateTableResultSet)
            {
                rs.next();
                String tableCreateData = rs.getString("Create Table") + ";";
                sb.append("\n").append(tableCreateData).append("\n");
            }
        }

        if (!settings.isExportOnlyStructure()) {
            sb.append("\n");
            sb.append("/* Data for the table `").append(table).append("` */\n");
            sb.append("\n");

            
            if (settings.isKeysArroundInsert()) {
                sb.append("/*!40000 ALTER TABLE `").append(table).append("` DISABLE KEYS */;\n");
                sb.append("\n");
            }
            
            // apend only if option selected
            if (settings.isAddLockArroundInsert()) {
                sb.append("LOCK TABLES `").append(table).append("` WRITE;\n");
                sb.append("\n");
            }
            
            if (settings.isAutocommit()) {
                sb.append("set autocommit=0;\n");
                sb.append("\n");
            }

            // check bulk option for inserts
            if (settings.isCreateBulkInsert() || !settings.isCreateCompleteInsert()) {
               getBulkInsertsForTable(dbService, sb, selectAllResultSet, showColumnsResultSet, table, settings);
            } else {
                getNormalInsertsForTable(dbService, sb, selectAllResultSet, showColumnsResultSet, table, settings);
            }
            
            // apend only if option selected
            if (settings.isAddLockArroundInsert()) {
                sb.append("\nUNLOCK TABLES;\n");
                sb.append("\n");
            }

            if (settings.isKeysArroundInsert()) {
                sb.append("/*!40000 ALTER TABLE `").append(table).append("` ENABLE KEYS */;\n");
                sb.append("\n");
            }

            if (settings.isAutocommit()) {
                sb.append("COMMIT;\n");
                sb.append("\n");
            }
        }
    }
    
    private static void dumpTrigger(DBService dbService, ResultSet showCreateTriggerResultSet, FileWriter sb, String trigger, ScheduleParams settings) throws SQLException, IOException {        

        try (ResultSet rs = showCreateTriggerResultSet)
        {
            rs.next();
            String triggerCreateData = rs.getString("SQL Original Statement");
            if (triggerCreateData != null) {
                
                sb.append("\n");
                sb.append("/* Trigger structure for table `").append(trigger).append("` */\n");
                sb.append("\n");
                if (settings.isIncludeDrop()) {
                    sb.append("/*!50003 ").append(dbService.getDropTriggerSQL(null, trigger)).append(" */;\n");
                    sb.append("\n");
                }
                sb.append("DELIMITER $$\n\n");
        
                triggerCreateData = triggerCreateData.replaceAll("`" + settings.getDatabase() + "`.", "");

                // apend only if option selected
                if (settings.isIgnoreDefiner()) {
                    triggerCreateData = triggerCreateData.replaceAll(" DEFINER=`.+?`@`.+?`", "");
                }                        
                sb.append("").append(triggerCreateData).append("$$").append("\n");
                
                sb.append("\n").append("DELIMITER ;\n");
            }
        }
    }
    
    private static void dumpEvent(DBService dbService, ResultSet showCreateEventResultSet, FileWriter sb, String event, ScheduleParams settings) throws SQLException, IOException {

        try (ResultSet rs = showCreateEventResultSet)
        {
            rs.next();
            String eventCreateData = rs.getString("Create Event");
            if (eventCreateData != null) {
                
                sb.append("\n");
                sb.append("/* Event structure for event `").append(event).append("` */\n");
                sb.append("\n");

                if (settings.isIncludeDrop()) {
                    sb.append("/*!50106 ").append(dbService.getDropEventSQL(null, event)).append(" */;\n");
                    sb.append("\n");
                }

                sb.append("DELIMITER $$\n\n");
        
                eventCreateData = eventCreateData.replaceAll("`" + settings.getDatabase() + "`.", "");

                // apend only if option selected
                if (settings.isIgnoreDefiner()) {
                    eventCreateData = eventCreateData.replaceAll(" DEFINER=`.+?`@`.+?`", "");
                }                        
                sb.append("").append(eventCreateData).append("$$").append("\n");
                
                sb.append("\n").append("DELIMITER ;\n");
            }
        }
    }
    
    private static void dumpFunction(DBService dbService, ResultSet showCreateFunctionResultSet, FileWriter sb, String function, ScheduleParams settings) throws SQLException, IOException {
        
        try (ResultSet rs = showCreateFunctionResultSet)
        {
            rs.next();
            String functionCreateData = rs.getString("Create Function");
            if (functionCreateData != null) {
                
                sb.append("\n");
                sb.append("/* Function structure for function `").append(function).append("` */\n");
                sb.append("\n");

                if (settings.isIncludeDrop()) {
                    sb.append("/*!50003 ").append(dbService.getDropFunctionSQL(null, function)).append(" */;\n");
                    sb.append("\n");
                }

                sb.append("DELIMITER $$\n\n");

                functionCreateData = functionCreateData.replaceAll("`" + settings.getDatabase() + "`.", "");

                // apend only if option selected
                if (settings.isIgnoreDefiner()) {
                    functionCreateData = functionCreateData.replaceAll(" DEFINER=`.+?`@`.+?`", "");
                }                        
                sb.append("").append(functionCreateData).append("$$").append("\n");
                
                sb.append("\n").append("DELIMITER ;\n");
            }
        }                    
    }
    
    private static void dumpSoredProcedure(DBService dbService, ResultSet showCreateStoredProcedureResultSet, FileWriter sb, String procedure, ScheduleParams settings) throws SQLException, IOException {
        
        try (ResultSet rs = showCreateStoredProcedureResultSet)
        {
            rs.next();
            String procedureCreateData = rs.getString("Create Procedure");
            if (procedureCreateData != null) {
                
                sb.append("\n");
                sb.append("/* Procedure structure for procedure `").append(procedure).append("` */\n");
                sb.append("\n");

                if (settings.isIncludeDrop()) {
                    sb.append("/*!50003 ").append(dbService.getDropStoredProcedureSQL(null, procedure)).append(" */;\n");
                    sb.append("\n");
                }

                sb.append("DELIMITER $$\n\n");
        
                procedureCreateData = procedureCreateData.replaceAll("`" + settings.getDatabase() + "`.", "");

                // apend only if option selected
                if (settings.isIgnoreDefiner()) {
                    procedureCreateData = procedureCreateData.replaceAll(" DEFINER=`.+?`@`.+?`", "");
                }                        
                sb.append("").append(procedureCreateData).append("$$").append("\n");
                
                sb.append("\n").append("DELIMITER ;\n");
            }
        }
    }
    
    private static void dumpView(DBService dbService, ResultSet showViewFields, ResultSet showCreateViewResultSet, FileWriter sb, String view, ScheduleParams settings) throws SQLException, IOException {
        
        sb.append("\n");
        sb.append("/*Table structure for table `").append(view).append("` */\n");
        sb.append("\n").append(dbService.getDropTableSQL(null, view)).append(";\n");
        
        if (settings.isIncludeDrop()) {
            sb.append("\n").append("/*!50001 ").append(dbService.getDropViewSQL(null, view)).append("*/;\n");
            sb.append("/*!50001 ").append(dbService.getDropTableSQL(null, view)).append("*/;\n");
        }
        
        sb.append("\n").append("/*!50001 CREATE TABLE `").append(view).append("`(\n");
        try (ResultSet rs = showViewFields) //"SHOW FIELDS FROM `" + settings.getDatabase() + "`.`" + view + "`"))
        {
            boolean first = true;
            while(rs.next()) {
                if (!first) {
                    sb.append(",\n");
                } else {
                    first = false;
                }
                sb.append("`").append(rs.getString("Field")).append("` ");
                sb.append(rs.getString("Type"));
            }
            
        } catch (ClassCastException ex) {
            throw new SQLException("Can not show fields from view '" + view + "'", ex);
        }
        sb.append(")*/;\n");
               
        sb.append("\n");
        sb.append("/* View structure for view `").append(view).append("` */\n");
        
        sb.append("\n").append("/*!50001 ").append(dbService.getDropTableSQL(null, view)).append("*/;\n");        

        if (settings.isIncludeDrop()) {
            sb.append("/*!50001 ").append(dbService.getDropViewSQL(null, view)).append("*/;\n");
        }

        try (ResultSet rs = showCreateViewResultSet)
        {
            rs.next();
            String tableCreateData = rs.getString("Create View");
            if (tableCreateData != null) {
                tableCreateData = tableCreateData.replaceAll("`" + settings.getDatabase() + "`.", "") + "*/;";

                // apend only if option selected
                if (settings.isIgnoreDefiner()) {
                    tableCreateData = tableCreateData.replaceAll(" DEFINER=`.+?`@`.+?`", "");
                }                        
                sb.append("/*!50001 ").append(tableCreateData).append("\n");
            }
        }
    }
    
    private static void dumpFooter(FileWriter sb) throws IOException {
        sb.append("\n/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;\n");
        sb.append("/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n");
        sb.append("/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;\n");
        sb.append("/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;\n");
    }
    
    
    private static void logMessage(FileWriter logWriter, String message, Throwable th) {
        if (th != null) {
            log.error(message, th);
        }
        
        if (logWriter != null) {
            try {
                logWriter.append(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss: ").format(new Date()) + message);
                logWriter.append("\r\n");
                
                if (th != null) {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    th.printStackTrace(pw);

                    logWriter.append(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss: ").format(new Date()) + sw.toString());
                    logWriter.append("\r\n");
                }                
                
                logWriter.append("\r\n\r\n");
            } catch (IOException ex) {
                log.error("Error", ex);
            }
        }        
    }
}
