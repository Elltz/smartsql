package np.com.ngopal.smart.sql.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.TableProvider;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;


/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public class ExportUtils {

    public static final int MAX_ROWS_COUNT = 65535;
    
    public interface ExportStatusCallback {
        
        void error(String errorMessage, Throwable th);
        void success();
    }
    
    private ExportUtils() {}
        
    public static void exportXLSTemplate(DBService dbService, String xlsFilePath, String databaseName, String tableName, ExportStatusCallback callback) {
                    
        // create sub dirs
        File f = new File(xlsFilePath);
        f.getParentFile().mkdirs();
        
        try (
                FileOutputStream fout = new FileOutputStream(xlsFilePath);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ){
            
            // read table structure
            DBTable t = dbService.getTableSchemaInformation(databaseName, tableName);
            Map<String, DataType> tableStructure = new LinkedHashMap<>();
            for (Column c: t.getColumns()) {
                tableStructure.put(c.getName(), c.getDataType());
            }
            
            HSSFWorkbook wb = new HSSFWorkbook();
            
            // Create the spreadsheet
            HSSFSheet spreadSheet = wb.createSheet(tableName);
            
            // Create the first row and fill it by columns names
            HSSFRow headerRow = spreadSheet.createRow((short) 0);
            // Create second row and fill it bu columns types
            HSSFRow typesRow = spreadSheet.createRow((short) 1);
            
            int columnIndex = 0;
            for (Column c: t.getColumns()) {                
                HSSFCell headerCell  = headerRow.createCell(columnIndex);
                headerCell.setCellValue(c.getName());
                
                HSSFCell typeCell  = typesRow.createCell(columnIndex);
                typeCell.setCellValue(c.getDataType().getDisplayValue());
                
                columnIndex++;
            }
            

            wb.write(outputStream);
            outputStream.writeTo(fout);
  
            callback.success();
        } catch (Throwable ex) {
            callback.error("Error XLS Templatge export", ex);
        }
    }
    
    /**
     *  Export into xls file
     * 
     * @param excelFileName  output xls file
     * @param data  list of column values
     * @param columns  columns, where map value - need or no export,
     *  because data has all columns values
     * @param maxTextSizeLimit maximum text size of string cell
     * @param decimalPlaces decimal places count of decimal cell
     * @param charset text char set
     * @param callback  export callback
     */
    public static void exportXLS(String excelFileName, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, Integer maxTextSizeLimit, Integer decimalPlaces, String charset, ExportStatusCallback callback) {
        exportXLS(excelFileName, data, columns, null, maxTextSizeLimit, decimalPlaces, charset, callback);
    }
    
    
    private static void exportXLS(HSSFWorkbook wb, HSSFSheet sheet, HSSFFont defaultFont, short rowIndex, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, List<Integer> columnWidths, Integer maxTextSizeLimit, Integer decimalPlaces, String charset, ExportStatusCallback callback) {
        HSSFRow headerRow = sheet.createRow(rowIndex);
            
        CellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFont(defaultFont);

        int columnIndex = 0;
        for (Map.Entry<String, Boolean> column: columns.entrySet()) {

            if (column.getValue()) {
                HSSFCell headerCell  = headerRow.createCell(columnIndex);
                headerCell.setCellValue(column.getKey().trim());
                headerCell.setCellStyle(headerStyle);

                columnIndex++;
            }
        }

        CellStyle timestampStyle = wb.createCellStyle();
        CellStyle dateStyle = wb.createCellStyle();
        CellStyle timeStyle = wb.createCellStyle();


        CreationHelper createHelper = wb.getCreationHelper();
        timestampStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd"));
        timeStyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));

        for (int r = 0; r < data.size(); r++) {
            HSSFRow row = sheet.createRow(rowIndex + r + 1);

            int dataIndex = 0;
            columnIndex = 0;
            //iterating c number of columns
            for (Map.Entry<String, Boolean> column: columns.entrySet()) {
                if (column.getValue()) {
                    HSSFCell cell = row.createCell(columnIndex);

                    Object value = data.get(r).get(dataIndex);
                    if (value instanceof Number) {
                        if (value instanceof BigDecimal && decimalPlaces != null) {
                            value = ((BigDecimal)value).setScale(decimalPlaces, RoundingMode.HALF_UP);
                        }
                        cell.setCellValue(((Number)value).doubleValue());
                    } else if (value instanceof Date) {

                        cell.setCellValue((Date)value);

                        if (value instanceof Time) {
                            cell.setCellStyle(timeStyle);
                        } else if (value instanceof Timestamp) {
                            cell.setCellStyle(timestampStyle);
                        } else {
                            cell.setCellStyle(dateStyle);
                        }
                    } else if (value instanceof Boolean) {
                        cell.setCellType(CellType.NUMERIC);
                        cell.setCellValue((Boolean)value);

                    } else if (value instanceof byte[]) {

                        Drawing drawing = sheet.createDrawingPatriarch();
                        ClientAnchor anchor = createHelper.createClientAnchor();

                        int pictureIndex = wb.addPicture((byte[]) value, Workbook.PICTURE_TYPE_PNG);
                        
                        anchor.setCol1(columnIndex);
                        anchor.setRow1(r + 1);
                        anchor.setCol2(columnIndex);
                        anchor.setRow2(r + 1);                            

                        Picture p = drawing.createPicture(anchor, pictureIndex);
                        p.resize(1.0, 1.0);
                        
                    } else {

                        if (value == null) {
                            value = "";
                        } else if (value instanceof byte[]){
                            value = "x'" + Hex.encodeHexString((byte[])value) + "'";
                        } else {
                            value = value.toString();
                            if (maxTextSizeLimit != null && ((String)value).length() > maxTextSizeLimit) {
                                value = ((String)value).substring(0, maxTextSizeLimit);
                            }
                        }

                        // TODO: Need check this, may be its not work
                        String encodedText = (String)value;
                        if (charset != null && !charset.isEmpty()) {
                            try {
                                encodedText = new String(encodedText.getBytes(charset), charset);
                            } catch (Throwable th) {
                            }
                        }
                        cell.setCellValue(new HSSFRichTextString(encodedText));
                    }

                    columnIndex++;
                }

                dataIndex++;   
            }
        }


        if (columnWidths != null) {
            for (int i = 0; i < columnWidths.size(); i++) {
                Integer width = columnWidths.get(i) / 7;
                if (width > 255) {
                    width = 255;
                }
                sheet.setColumnWidth(i, width * 256);
            }
        } else {
            columnIndex = 0;
            for (Map.Entry<String, Boolean> column: columns.entrySet()) {            
                if (column.getValue()) {
                    sheet.autoSizeColumn(columnIndex);
                    columnIndex++;
                }
            }
        }
    }
    
    public static void exportXLS(String excelFileName, List<byte[]> headerImages, List<Integer> imagesHeights, List<Integer> imagesColspan, List<String> sheets, List<ObservableList<ObservableList>> data, List<LinkedHashMap<String, Boolean>> columns, List<List<Integer>> columnWidths, Integer maxTextSizeLimit, Integer decimalPlaces, String charset, ExportStatusCallback callback) {
        try {
            // create sub dirs
            File f = new File(excelFileName);
            if (f.getParentFile() != null) {
                f.getParentFile().mkdirs();
            }
            
            HSSFWorkbook wb = new HSSFWorkbook();
            
            for (int i = 0; i < sheets.size(); i++) {

                HSSFSheet sheet = wb.createSheet(sheets.get(i));

                HSSFFont defaultFont = wb.createFont();
                defaultFont.setFontHeightInPoints((short)10);
                defaultFont.setFontName("Arial");
                defaultFont.setColor(IndexedColors.BLUE.getIndex());
                defaultFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

                int row = 0;
                byte[] image = headerImages.get(i);
                if (image != null) {
                    Drawing drawing = sheet.createDrawingPatriarch();
                    ClientAnchor anchor = wb.getCreationHelper().createClientAnchor();

                    int pictureIndex = wb.addPicture(image, Workbook.PICTURE_TYPE_PNG);

                    anchor.setCol1(row);
                    anchor.setRow1(0);
                    Integer span = imagesColspan.get(i);
                    anchor.setCol2(span != null ? span : 0);
                    anchor.setRow2(row);                            

                    drawing.createPicture(anchor, pictureIndex).resize(1.0, 1.0);
                    Integer height = imagesHeights.get(i);
                    if (height != null) {
                        sheet.createRow(0).setHeight((short)(height * 256));
                    }
                    row += 2;
                }
                
                exportXLS(wb, sheet, defaultFont, (short) row, data.get(i), columns.get(i), columnWidths.get(i), maxTextSizeLimit, decimalPlaces, charset, callback);
            }
            
            try {            
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                wb.write(bo);
                Files.write(f.toPath(), bo.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (Throwable ex) {
                callback.error("Error exporting xls", ex);
                return;
            }
        }
        catch (Throwable th) {
            callback.error(th.getMessage(), th);
        }
        
        callback.success();
    }
    
    public static void exportXLS(String excelFileName, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, List<Integer> columnWidths, Integer maxTextSizeLimit, Integer decimalPlaces, String charset, ExportStatusCallback callback) {
        
        try {
            // create sub dirs
            File f = new File(excelFileName);
            if (f.getParentFile() != null) {
                f.getParentFile().mkdirs();
            }
            
            HSSFWorkbook wb = new HSSFWorkbook();
            String sheetName = excelFileName;
            if (sheetName.contains(File.separator)) {
                sheetName = sheetName.substring(sheetName.lastIndexOf(File.separator) + 1);
            }
            
            if (sheetName.contains(".")) {
                sheetName = sheetName.substring(0, sheetName.lastIndexOf("."));
            }
            
            HSSFSheet sheet = null;
            if (wb.getSheet(sheetName) == null) {
                sheet = wb.createSheet(sheetName);
            }

            HSSFFont defaultFont = wb.createFont();
            defaultFont.setFontHeightInPoints((short)12);
            defaultFont.setFontName("Arial");
            defaultFont.setColor(IndexedColors.BLUE.getIndex());
            defaultFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            exportXLS(wb, sheet, defaultFont, (short) 0, data, columns, columnWidths, maxTextSizeLimit, decimalPlaces, charset, callback);
            
            try {            
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                wb.write(bo);
                Files.write(f.toPath(), bo.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (Throwable ex) {
                callback.error("Error exporting xls", ex);
                return;
            }
        }
        catch (Throwable th) {
            callback.error(th.getMessage(), th);
        }
        
        callback.success();
    }
  
    public static void exportDelimiter(String fileName, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, 
        String charset, String fieldsTerminated, String fieldsEnclosed, String lineTerminated, String nullReplaced, boolean withColumns,
        ExportStatusCallback callback) {
                
        // create sub dirs
        File f = new File(fileName);
        if (f.getParentFile() != null) {
            f.getParentFile().mkdirs();
        }
        
        try (
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName(charset != null ? charset : "UTF-8").newEncoder());
            ) {
            
            convertDataForDelimiterWriter(osw, data, columns, charset, fieldsTerminated, fieldsEnclosed, lineTerminated, nullReplaced, withColumns);            
            
            osw.flush();
            
            Files.write(f.toPath(), bos.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            
        } catch (Throwable ex) {
            callback.error("Error", ex);
            return;
        }
        
        callback.success();
    }
    
    public static void convertDataForDelimiterWriter(Writer writer, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, 
        String charset, String fieldsTerminated, String fieldsEnclosed, String lineTerminated, String nullReplaced, boolean withColumns) throws IOException {
        
        List<String> columnNames = new ArrayList<>();
        for (Map.Entry<String, Boolean> column: columns.entrySet()) {
            if (column.getValue()) {
                columnNames.add(column.getKey());
            }
        }
        
        if (withColumns) {
            writer.append(String.join(StringEscapeUtils.unescapeJava(fieldsTerminated), columnNames) + StringEscapeUtils.unescapeJava(lineTerminated));
        }
        
        for (ObservableList ob : data) {

            List<String> arr = new ArrayList<>();
            
            int columnIndex = 0;
            //iterating c number of columns
            for (Map.Entry<String, Boolean> column: columns.entrySet()) {

                if (column.getValue()) { 
                    Object value = ob.get(columnIndex);
                    if (value == null) {
                        value = nullReplaced;
                    } else if (value instanceof byte[]){
                        
                        value = StringEscapeUtils.unescapeJava(fieldsEnclosed) 
                               + new String((byte[])value, "UTF-8") + 
                            StringEscapeUtils.unescapeJava(fieldsEnclosed);
                    } else {
                        
                        if (value.toString().startsWith("x'")) {
                            try {
                                value = Hex.decodeHex(value.toString().substring(2, value.toString().length() - 1).toCharArray());
                                
                                value = StringEscapeUtils.unescapeJava(fieldsEnclosed) 
                                    + new String((byte[])value, "UTF-8") + 
                                 StringEscapeUtils.unescapeJava(fieldsEnclosed);
                                
                            } catch (DecoderException ex) {
                                throw new IOException(ex);
                            }
                        } else {

                            value = StringEscapeUtils.unescapeJava(fieldsEnclosed) 
                                + new String(value.toString().getBytes(), charset != null ? charset : "UTF-8") + 
                                StringEscapeUtils.unescapeJava(fieldsEnclosed);
                        }
                    }

                    arr.add((String)value);                        
                }

                columnIndex++;
            }
            
            writer.append(String.join(StringEscapeUtils.unescapeJava(fieldsTerminated), arr) + StringEscapeUtils.unescapeJava(lineTerminated));
        }
    }
    
    public static void exportHTML(String fileName, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, ExportStatusCallback callback) {
        
        try {
            
            // create sub dirs
            File f = new File(fileName);
            if (f.getParentFile() != null) {
                f.getParentFile().mkdirs();
            }
            
            StringBuilder sb = new StringBuilder();

            // add HTML header
            sb.append("<html>\n");
            sb.append("<head>\n");
            sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>Export data</title>\n");
            sb.append("<style type=\"text/css\"> <!--\n");
            sb.append(".normal {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; color: #000000}\n");
            sb.append(".medium {  font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; font-weight: bold; color: #000000; text-decoration: none}\n");
            sb.append("--></style>\n");
            sb.append("</head>\n");
            sb.append("<body>\n");
            sb.append("<h3>Export data</h3>\n\n");

            sb.append("<table border=1>\n");

            // table header
            sb.append("<tr>\n");
            for (Map.Entry<String, Boolean> column: columns.entrySet()) {            
                if (column.getValue()) {
                    sb.append("<td bgcolor=silver class='medium'>");
                    sb.append(column.getKey());
                    sb.append("</td>\n");
                }
            }
            sb.append("</tr>\n");

            // add rows
            for (List row: data) {
                sb.append("<tr>\n");

                int columnIndex = 0;
                for (Map.Entry<String, Boolean> column: columns.entrySet()) {            
                    if (column.getValue()) {

                        Object value = row.get(columnIndex);
                        if (value == null) {
                            value = "(NULL)";
                        } else if (value instanceof byte[]){
                            value = "x'" + Hex.encodeHexString((byte[])value) + "'";
                        } else {
                            value = value.toString();
                        }

                        sb.append("<td class='normal' valign='top'>");
                        sb.append(value);
                        sb.append("</td>\n");
                    }

                    columnIndex++;
                }

                sb.append("</tr>\n");
            }

            // end
            sb.append("</table>\n");
            sb.append("</body></html>");

            Files.write(f.toPath(), sb.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            
            callback.success();
        } catch (IOException ex) {
            callback.error("Error", ex);
        }
    }
    
    public static void exportXML(String fileName, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, ExportStatusCallback callback) {
        
        // create sub dirs
        File f = new File(fileName);
        if (f.getParentFile() != null) {
            f.getParentFile().mkdirs();
        }
        
        try (
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(outputStream, "utf-8");
            ) {

            XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(osw);
            
            out.writeStartDocument();
                out.writeStartElement("data");

                for (ObservableList row: data) {
                    
                    out.writeStartElement("row");
                    
                    int columnIndex = 0;
                    for (Map.Entry<String, Boolean> entry: columns.entrySet()) {
                        
                        if (entry.getValue()) {
                            Object value = row.get(columnIndex);
                            if (value == null) {
                                value = "(NULL)";
                            } else if (value instanceof byte[]){
                                value = "x'" + Hex.encodeHexString((byte[])value) + "'";
                            } else {
                                value = value.toString();
                            }
                            
                            out.writeStartElement(entry.getKey());
                                out.writeCharacters((String)value);
                            out.writeEndElement();
                        }
                        
                        columnIndex++;
                    }
                    
                    out.writeEndElement();
                }
                

                out.writeEndElement();
            out.writeEndDocument();
            out.close();
            
            Files.write(f.toPath(), outputStream.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            
            
        } catch (Throwable th) {
            callback.error("Error exporting XML file", th);
            return;
        }
        
        callback.success();
    }
    
    
    public static void exportPDF(String fileName, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, ExportStatusCallback callback) {
        
        // create sub dirs
        File f = new File(fileName);
        if (f.getParentFile() != null) {
            f.getParentFile().mkdirs();
        }
            
        try {
            
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
            Document document = new Document();
            PdfWriter.getInstance(document, bos);
            
            document.open();
            
            // calc column count
            int columnCount = 0;
            for (Map.Entry<String, Boolean> entry: columns.entrySet()) {
                if (entry.getValue()) {
                    columnCount++;
                }
            }
            
            // create table
            PdfPTable table = new PdfPTable(columnCount);
            
            Font headerFont = new Font(FontFamily.HELVETICA, 8, Font.BOLD);
            Font dataFont = new Font(FontFamily.HELVETICA, 6, Font.NORMAL);
            
            // create header
            for (Map.Entry<String, Boolean> entry: columns.entrySet()) {
                if (entry.getValue()) {
                    
                    Phrase phrase = new Phrase(entry.getKey(), headerFont);
                    
                    PdfPCell cell = new PdfPCell(phrase);                    
                    cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    table.addCell(cell);
                }
            }
            
            // create data rows
            for (ObservableList row: data) {
                
                int columnIndex = 0;
                
                for (Map.Entry<String, Boolean> entry: columns.entrySet()) {                    
                    if (entry.getValue()) {
                        
                        Object value = row.get(columnIndex);
                        if (value == null) {
                            value = "(NULL)";
                        } else if (value instanceof byte[]){
                            value = "x'" + Hex.encodeHexString((byte[])value) + "'";
                        } else {
                            value = value.toString();
                        }
                            
                        Phrase phrase = new Phrase((String)value, dataFont);

                        PdfPCell cell = new PdfPCell(phrase);
                        table.addCell(cell);
                    }
                    columnIndex++;
                }
            }
            
            document.add(table);
            
            document.close();
            
            Files.write(f.toPath(), bos.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            
        } catch (Throwable th) {
            callback.error("Error exporting PDF file", th);
            return;
        }
        
        callback.success();
    }
    
    public enum ExportSQLType {
        ONLY_DATA,
        ONLY_STRUCTURE,
        STUCTURE_AND_DATA
    }
    
    public static void exportSQL(String fileName, TableProvider table, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns, ExportSQLType type, String version, Long bulkSize, Long insertBreakRows, ExportStatusCallback callback){

        // create sub dirs
        File f = new File(fileName);
        if (f.getParentFile() != null) {
            f.getParentFile().mkdirs();
        }
            
        try {
            StringBuilder sb = new StringBuilder();

            if (version != null) {
                sb.append("/*********************************************\n")
                    .append("   Smart SQL\n")
                    .append("   MySQL - ").append(version).append("\n")
                    .append("       Created on ")                    
                    .append(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date())).append("\n")
                    .append("**********************************************/\n\n");

            }
            
            sb.append("/*!40101 SET NAMES utf8 */;\n\n");
            
            if (type != ExportSQLType.ONLY_DATA) {
                sb.append("\n");            
                sb.append("create table `").append(table.getName()).append("` (\n");

                for (Column c: table.getColumns()) {
                    // only if column need export
                    Boolean v = columns.get(c.getName());
                    if (v != null && v) {
                        
                        String length = "";
                        if (c.getDataType().hasLength() && c.getDataType().getLength() != null && c.getDataType().getLength() >= 0) {
                            length = " (" + c.getDataType().getLength() + ")";
                        } else if (c.getDataType().getList() != null && !c.getDataType().getList().isEmpty()) {
                            length = " (";
                            for (String s: c.getDataType().getList()) {
                                length += "'" + s + "', ";
                            }
                            length = length.substring(0, length.length() - 2);
                            length += ")";
                        }
                        
                        sb.append("\t`").append(c.getName()).append("` ").append(c.getDataType().getName()).
                            append(length).append(",\n");
                    }
                }

                sb.delete(sb.length() - 2, sb.length());
                sb.append("\n);\n");
            }

            if (type != ExportSQLType.ONLY_STRUCTURE) {

                // make insert template
                String insertTemplate = "insert into `" + table.getName() + "` (";
                for (Column c: table.getColumns()) {
                    // only if column need export
                    Boolean v = columns.get(c.getName());
                    if (v != null && v) {
                        insertTemplate += "`" + c.getName() + "`, ";
                    }
                }

                insertTemplate = insertTemplate.substring(0, insertTemplate.length() - 2);            
                insertTemplate += ") values ";

                Long index = 0l;
                Long size = 0l;
                // create data rows
                for (ObservableList row: data) {

                    if (bulkSize != null && size > bulkSize || insertBreakRows != null && index >= insertBreakRows) {
                        sb.append(";\n");
                        index = 0l;
                        size = 0l;
                    }
                    
                    if (index > 0) {
                        sb.append(", ");
                        size += 2;
                    } else {
                        sb.append(insertTemplate);
                    }
                    
                    sb.append("(");
                    size += 1;
                    
                    int columnIndex = 0;

                    for (Map.Entry<String, Boolean> entry: columns.entrySet()) {                    
                        if (entry.getValue()) {

                            Object obj = row.get(columnIndex);
                            if (obj != null) {
                                if (obj instanceof Boolean) {
                                    obj = ((Boolean)obj).toString();
                                } else if (obj instanceof byte[]){
                                    obj = "x'" + Hex.encodeHexString((byte[])obj) + "'" ;
                                } else {
                                    obj = "'" + StringEscapeUtils.escapeSql(obj.toString()) + "'";
                                }
                            } else {
                                obj = "NULL";
                            }

                            size += (((String)obj).length() + 2);
                            sb.append((String)obj).append(", ");
                        }
                        
                        columnIndex++;
                    }

                    sb.delete(sb.length() - 2, sb.length());

                    sb.append(")");
                    size += 1;
                    
                    index++;
                }
                
                sb.append(";");
            }
            
            Files.write(f.toPath(), sb.toString().getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            callback.success();
            
        } catch (Throwable th) {
            callback.error("Error", th);
        }
    }

    public static void exportJSON(String fileName, Charset charset, ObservableList<ObservableList> data, LinkedHashMap<String, Boolean> columns,ExportStatusCallback callback){
        try {
            File f = new File(fileName);
            if (f.getParentFile() != null) { f.getParentFile().mkdirs(); }
            
            Gson gson = new Gson();
            JsonArray jsonArray = new JsonArray();
            //we are looping through the rows
            for (ObservableList ob : data) {
                int columnIndex = 0;
                JsonObject jsonObject = new JsonObject();
                // we are now looping through the columns to get an index row columns
                for (Map.Entry<String, Boolean> column : columns.entrySet()) {
                    if (column.getValue()) {

                        Object value = ob.get(columnIndex);
                        if (value == null) {
                            value = "NULL";
                        } else if (value instanceof byte[]) {
                            value = new String((byte[]) value, charset);
                        } else if (value.toString().startsWith("x'")) {
                            try {
                                value = Hex.decodeHex(value.toString().substring(2, value.toString().length() - 1).toCharArray());
                                value = new String((byte[]) value, charset);
                            } catch (DecoderException ex) {
                                throw new IOException(ex);
                            }
                        } else {
                            value = new String(value.toString().getBytes(), charset);
                        }

                        jsonObject.addProperty(column.getKey(), (String) value);
                    }
                    columnIndex++;
                }
                jsonArray.add(jsonObject);
            }

            String jsonString = gson.toJson(jsonArray);            
            Files.write(f.toPath(), jsonString.getBytes(charset), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

            if (callback != null) { callback.success(); }
        } catch (Throwable e) {
            if (callback != null) {
                callback.error(e.getMessage(), e);
            }
        }
    }
}
