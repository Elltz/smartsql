package np.com.ngopal.smart.sql.utils;

import com.sun.mail.smtp.SMTPTransport;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import lombok.extern.log4j.Log4j;

/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public final class EmailUtils {
 
    
    /**
     *  Send email
     * 
     * @param fromName sender name
     * @param fromEmail sender email
     * @param replyEmail sender reply email, can be <code>null</code>
     * @param toEmail target email
     * @param cc cc email, can be <code>null</code>
     * @param bcc bcc email, can be <code>null</code>
     * @param host sender host
     * @param port sender host port
     * @param encryption 0 - none, 1 - TLS, 2 - SSL
     * @param userName user name for authentificatin, can be <code>null</code>
     * @param userPassword user password for authentificatin, can be <code>null</code>
     * @param subject message subject
     * @param text message text
     * 
     * @throws javax.mail.MessagingException
     * @throws java.io.UnsupportedEncodingException
     */
    public static final void sendEmail(String fromName, String fromEmail, String replyEmail,
                                        String toEmail, String cc, String bcc,
                                        String host, String port, int encryption,
                                        String userName, String userPassword,
                                        String subject, String text) throws MessagingException, UnsupportedEncodingException {
        
        
        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        
        switch (encryption) {
            // NONE
            case 0:
                // do nothing
                break;
            
            // TLS
            case 1:
                props.put("mail.smtp.starttls.enable", "true");
                break;
                
            // SSL
            case 2:
                props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                //props.put("mail.smtp.socketFactory.port", port);
                break;
        }
        
        if (userName != null) {
            props.put("mail.smtp.auth", "true");
        }
        
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", "true");
        
        props.put("mail.smtp.ssl.trust", "*");

        // Get a Session object
        Session session = Session.getInstance(props, null);
        
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(fromEmail, fromName));

        if (replyEmail != null) {
            msg.setReplyTo(new Address[]{new InternetAddress(replyEmail)});
        }

        msg.setRecipients(Message.RecipientType.TO,  InternetAddress.parse(toEmail, false));
        if (cc != null) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));
        }
        if (bcc != null) {
            msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc, false));
        }

        msg.setSubject(subject);
        msg.setText(text, "utf-8", "html");
        
        msg.setSentDate(new Date());

        SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
//        t.setStartTLS(true);
        try {
            if (userName != null) {
                t.connect(host, userName, userPassword);
            } else {
                t.connect();
            }
            t.sendMessage(msg, msg.getAllRecipients());
        } finally {
            t.close();
        }          
    }
    
    
    
    public static void main(String[] args) throws MessagingException, UnsupportedEncodingException {

        sendEmail("SmartSQL", "notification.smartsql@gmail.com", "a.tsepushel@gmail.com",
                "a.tsepushel@gmail.com", "a.tsepushel@gmail.com", "a.tsepushel@gmail.com", 
                "smtp.gmail.com", "587", 1, 
                "notification.smartsql", "smartsql.notification",
                "SmartSQL Schedule Buckup Notification", "Database 'test' successfully dumped");
    }
}
