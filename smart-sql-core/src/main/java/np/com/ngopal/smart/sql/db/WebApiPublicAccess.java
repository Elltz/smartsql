package np.com.ngopal.smart.sql.db;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.utils.PublicServerUtil;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class WebApiPublicAccess implements PublicAccess {

    private static String WEB_API_URL = "http://45.33.28.245:8080/";
    
    private static SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        
    private String accessToken;
    private Date expiresIn;
    private String login;
    private String password;
    
    private ConnectionParams params;
    public WebApiPublicAccess(String serverData) {
        this.params = PublicServerUtil.createParams(serverData);
    }
    
    private String executePost(CloseableHttpClient client, String url, Map data) throws IOException, PublicDataException {
        
        // check maybe token expire
        if (expiresIn == null || expiresIn.compareTo(new Date()) <= 0) {
            if (!login(login, password)) {
                throw new PublicDataException("Can not login to web api", null);
            }
        }
        
        HttpPost post = new HttpPost(WEB_API_URL + url);
        post.addHeader("Authorization", "Bearer " + accessToken);

        StringEntity entityStr = new StringEntity(new Gson().toJson(data), "UTF-8");
        post.addHeader("content-type", "application/json");
        post.setEntity(entityStr);
        
        CloseableHttpResponse response = client.execute(post);
        if (response.getStatusLine().getStatusCode() == 200) {
            return EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
        }
        
        throw new PublicDataException("Status: " + response.getStatusLine().getStatusCode() + "; Reason: " + response.getStatusLine().getReasonPhrase(), null);
    }
    
    @Override
    public synchronized boolean login(String login, String password) throws PublicDataException {
        
        accessToken = null;
        expiresIn = null;
        
        this.login = login;
        this.password = password;                
        
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            HttpPost post = new HttpPost(WEB_API_URL + "oauth/token");

            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(params.getUsername(), params.getPassword());
            post.addHeader(new BasicScheme().authenticate(creds, post, null));
            
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", login));
            params.add(new BasicNameValuePair("password", password));
            params.add(new BasicNameValuePair("grant_type", "password"));
            post.setEntity(new UrlEncodedFormEntity(params));
            
            CloseableHttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == 200) {
                
                HttpEntity entity = response.getEntity();
                String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
                
                Map<String, Object> result = new Gson().fromJson(json, HashMap.class);
                accessToken = (String) result.get("access_token");
                expiresIn = new Date(new Date().getTime() + ((Double) result.get("expires_in")).longValue() * 1000);
                
                return true;
                
            } else {
                return false;
            }
            
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } catch (AuthenticationException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    private List<ErrDiagram> convertToErrDiagrams(List<Map<String, Object>> map) {
        List<ErrDiagram> result = new ArrayList<>();
        if (map != null) {
            
            for (Map<String, Object> m: map) {
                ErrDiagram ed = new ErrDiagram();
                ed.setPublicId((Long) m.get("id"));
                ed.setName((String) m.get("name"));
                ed.setDiagramData((String) m.get("diagramData"));
                ed.setDatabase((String) m.get("databaseName"));
                ed.setDescription((String)m.get("description"));
                ed.setUserId((String)m.get("userId"));
                
                Map<String, Object> domainMap = (Map<String, Object>) m.get("domain");
                ed.setDomainId((Long) domainMap.get("id"));
                ed.setDomainName((String) domainMap.get("name"));
                
                Map<String, Object> subDomainMap = (Map<String, Object>) m.get("subDomain");
                ed.setSubDomainId((Long) subDomainMap.get("id"));
                ed.setSubDomainName((String) subDomainMap.get("name"));
                
                Map<String, Object> m0 = new Gson().fromJson(ed.getDiagramData(), HashMap.class);
                List tables = (List) m0.get("tables");
                ed.setNumberOfTables(tables != null ? tables.size() : 0);
                
                result.add(ed);
            }
        }
        
        return result;
    }
    
    @Override
    public synchronized List<ErrDiagram> loadAllErrDiagrams() throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, Object> data = new HashMap<>();
                    
            String value = executePost(client, "err-diagram/loadAllErrDiagrams", data);
            try {
                return convertToErrDiagrams(new Gson().fromJson(value, ArrayList.class));
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            }
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized List<ErrDiagram> searchErrDiagram(String text) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, Object> data = new HashMap<>();
            data.put("searchText", text);
                    
            String value = executePost(client, "err-diagram/searchErrDiagram", data);
            try {
                return convertToErrDiagrams(new Gson().fromJson(value, ArrayList.class));
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            }
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized boolean deleteErrDiagram(Long id) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {
            
            executePost(client, "err-diagram/deleteErrDiagram/" + id, new HashMap<>());
            
            return true;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized boolean updateErrDiagram(String schema, String diagramData, Long domainId, Long subDomainId, String description, Long directPublicId) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, Object> data = new HashMap<>();
            data.put("name", schema);
            data.put("diagram_data", diagramData);
            data.put("domain_id", domainId);
            data.put("subdomain_id", subDomainId);
            data.put("description", description);
            data.put("id", directPublicId);
                    
            executePost(client, "err-diagram/updateErrDiagram", data);
            
            return true;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long insertErrDiagram(String schema, String diagramData, String database, Long domainId, Long subDomainId, String description, String emailId) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, Object> data = new HashMap<>();
            data.put("name", schema);
            data.put("diagram_data", diagramData);
            data.put("database_name", database);
            data.put("domain_id", domainId);
            data.put("subdomain_id", subDomainId);
            data.put("description", description);
            data.put("email", emailId);
                    
            String value = executePost(client, "err-diagram/insertErrDiagram", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized List<Map<String, Object>> loadDomains() throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
                    
            String value = executePost(client, "err-diagram/loadDomains", data);
            
            try {
                return new Gson().fromJson(value, ArrayList.class);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized List<Map<String, Object>> loadSubDomains() throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
                    
            String value = executePost(client, "err-diagram/loadSubDomains", data);
            
            try {
                return new Gson().fromJson(value, ArrayList.class);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long getIdByDomain(String domain) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("domain", domain);
                    
            String value = executePost(client, "err-diagram/getIdByDomain", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long insertDomain(String domain) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("domain", domain);
                    
            String value = executePost(client, "err-diagram/insertDomain", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long getIdBySubDomain(String subDomain) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("subDomain", subDomain);
                    
            String value = executePost(client, "err-diagram/getIdBySubDomain", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long insertSubDomain(String subDomain) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("subDomain", subDomain);
                    
            String value = executePost(client, "err-diagram/insertSubDomain", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Timestamp lastUpdateUExpire(String email, String type, String expired) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmail", email);
            data.put("type", type);
            data.put("expiredDate", expired);
                    
            String value = executePost(client, "license/lastUpdateUExpire", data);
            try {
                return new Timestamp(DATETIME_FORMAT.parse(value).getTime());
            } catch (Throwable th) {
            }
            
            return null;
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized void insertUExpire(String email, int type, int usedCredits, int usedFullUsage, String expired, String lastUpdate) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, Object> data = new HashMap<>();
            data.put("userEmail", email);
            data.put("type", type);
            data.put("expiredDate", expired);
            data.put("expireCredits", usedCredits);
            data.put("fullUsage", usedFullUsage);
            data.put("lastUpdate", lastUpdate);
                    
            executePost(client, "license/insertUExpire", data);
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized String userLicense(String email) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmail", email);
                    
            return executePost(client, "license/userLicense", data);
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized boolean userValidation(String email, String license) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmail", email);
            data.put("userLicense", license);
                    
            String value = executePost(client, "license/userValidation", data);
            try {
                return Boolean.valueOf(value);
            } catch (Throwable th) {
                return false;
            }
            
        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized void userInstallUpdates(String userEmailId, String os, String macAddress, String ipAddress, String date) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmail", userEmailId);
            data.put("os", os);
            data.put("macAddress", macAddress);
            data.put("ipAddress", ipAddress);
            data.put("installDate", date);
                    
            executePost(client, "statistic/userInstallUpdates", data);

        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized void userCloseApp(String userEmailId, String os, String macAddress, String ipAddress, String dateOpen, String dateClose) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmail", userEmailId);
            data.put("os", os);
            data.put("macAddress", macAddress);
            data.put("ipAddress", ipAddress);
            data.put("openDate", dateOpen);
            data.put("closeDate", dateClose);
                    
            executePost(client, "statistic/userCloseApp", data);

        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long selectUserSystemId(String userEmailId, String localIP, String globalIP) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmailId", userEmailId);
            data.put("localIP", localIP);
            data.put("globalIP", globalIP);
                    
            String value = executePost(client, "statistic/selectUserSystemId", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
                return null;
            }

        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized Long insertUserSystem(String userEmailId, String localIP, String globalIP) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, String> data = new HashMap<>();
            data.put("userEmailId", userEmailId);
            data.put("localIP", localIP);
            data.put("globalIP", globalIP);
                    
            String value = executePost(client, "statistic/insertUserSystem", data);
            try {
                return Long.valueOf(value);
            } catch (Throwable th) {
                return null;
            }

        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public synchronized boolean syncUserUsageStatistic(int productId, List<Map<String, Object>> list) throws PublicDataException {
        CloseableHttpClient client = HttpClients.createDefault();
        try {

            Map<String, Object> data = new HashMap<>();
            data.put("productId", productId);
            data.put("usage", list);
                    
            String value = executePost(client, "statistic/syncUserUsageStatistic", data);
            
            try {
                return Boolean.valueOf(value);
            } catch (Throwable th) {
                return false;
            }

        } catch (UnsupportedEncodingException ex) {
            throw new PublicDataException(ex.getMessage(), ex);
            
        } catch (IOException ex) {
            // in this case its critical and we need try use direct
            throw new PublicDataException(ex.getMessage(), ex, true);
            
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
            }
        }
    }

}
