package np.com.ngopal.smart.sql.utils;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author PC
 */
@Slf4j
public final class MemoryUsageUtils {
    
    private static final long MEMORY_1_MB = 1024 * 1024;
    
    
    public static void printMemoryUsage(String label, boolean useGC) {
        
        if (useGC) {
            System.gc();
        }
        
        Runtime runtime = Runtime.getRuntime();
        long total = runtime.totalMemory();
        long used  = total - runtime.freeMemory();
        
        total /= MEMORY_1_MB;
        used /= MEMORY_1_MB;
        
        log.debug(label + " # Total memory: " + total + " MB, used: " + used + " MB");
    }
    
    public static void printMemoryUsage(String label) {        
        printMemoryUsage(label, false);        
    }
}
