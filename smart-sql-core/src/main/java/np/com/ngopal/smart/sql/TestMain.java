/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql;

import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.util.uri.ClassURI;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class TestMain {

    public static void main(String[] args) {
        PluginManager pm = PluginManagerFactory.createPluginManager();

        // and then one or more of these ...
        pm.addPluginsFrom(ClassURI.CLASSPATH);

    }
}
