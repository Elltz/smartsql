/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Getter
@Setter
public class ConnectionSession {

    private int id;

    private int connectionId;
    
    private String name;
    private boolean open = true;

    private ObjectProperty<ConnectionParams> connectionParam;

    private DBService service;

    public ConnectionSession(int id, int connectionId, ConnectionParams param, DBService service) {
        this();
        this.id = id;
        this.connectionId = connectionId;
        this.service = service;

    }

    public ConnectionSession() {
        connectionParam = new SimpleObjectProperty<>();
    }

    public ObjectProperty<ConnectionParams> connectionParamProperty() {
        return connectionParam;
    }

    public ConnectionParams getConnectionParam() {
        return connectionParam.get();
    }

    public void setConnectionParam(ConnectionParams param) {
        if (param != null) {
            connectionParam.set(param);
        }

    }

}
