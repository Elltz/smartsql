package np.com.ngopal.smart.sql.utils;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import lombok.extern.log4j.Log4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.DBTable;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Log4j
public final class ImportUtils {
    
    public interface ImportStatusCallback {
        
        void error(String errorMessage, Throwable th);
        void success(QueryResult queryResult, Map<String, Object> map);
        void progress(double progress);
        boolean isNeedStop();
    }
    
    private ImportUtils() {}
    
    private static final String NULL_VALUE = "(NULL)";
    
    public static final String DELIMITER__FIELDS_TERMINATED   = ",";
    public static final String DELIMITER__FIELDS_ESCAPED      = "\\\\";
    public static final String DELIMITER__FIELDS_ENCLOSED     = "\"";
    public static final String DELIMITER__LINE_TERMINATED     = "\\r\\n";
    
    public static void importDelimiter(DBService dbService, String csvPath, String databaseName, String tableName, ImportStatusCallback callback) {
        importDelimiter(dbService, csvPath, databaseName, tableName, null, null, null, DELIMITER__FIELDS_TERMINATED, DELIMITER__FIELDS_ENCLOSED, true, DELIMITER__FIELDS_ESCAPED, DELIMITER__LINE_TERMINATED, null, null, callback);
    }
    
    public static void importDelimiter(DBService dbService, String csvPath, String databaseName, String tableName, 
            String charset, String priorityType, String dubplicateAction, 
            String fieldsTerminated, String fieldsEnclosed, boolean enclosedOptionally, String fieldsEscaped, String lineTerminated, 
            Integer ignoreLines, List<String> columns, ImportStatusCallback callback) {
        
        try {
            
            callback.progress(-1);
            
            QueryResult qr = new QueryResult();
            // For csv just execute LOAD file query
            int result = (int) dbService.execute(
                    dbService.getLoadDataInFileSQL(csvPath.replace("\\", "\\\\"), databaseName, tableName, charset, priorityType, dubplicateAction, 
                            fieldsTerminated, fieldsEnclosed, enclosedOptionally, fieldsEscaped, lineTerminated,
                            ignoreLines, columns), qr);
            if (result >= 0) {
                callback.success(qr, null);
            } else {
                callback.error("Error CSV import", null);
            }
        } catch (SQLException ex) {
            callback.error("Error CSV import", ex);
        }
    }
    
    
    public static void importHTML(DBService dbService, String htmlLink, String databaseName, String tableName, ImportStatusCallback callback) {
        
        String importQuery = "INSERT INTO `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "` ";
        
        try {
            Document doc = null;
            try {
                // try connect to url
                doc = Jsoup.connect(htmlLink).get();
            } catch (IllegalArgumentException ex) {
                // if not url read as file
                doc = Jsoup.parse(new File(htmlLink), "UTF-8");
            }
            
            if (callback.isNeedStop()) {
                return;
            }
            
            callback.progress(0.1);

            // select table element
            Elements tableElements = doc.select("table");

            // select rows element exculed header rows
            Elements tableRowElements = tableElements.select(":not(thead) tr");
            
            double progress = 0.1;
            double step = 0.7 / tableRowElements.size();
            
            for (int i = 0; i < tableRowElements.size(); i++) {
                Element row = tableRowElements.get(i);
                
                importQuery += "(";
                
                // select columns for each row and add them to import query
                // 1st row must be db columns names
                Elements rowItems = row.select("td");
                for (int j = 0; j < rowItems.size(); j++) {
                    String value = rowItems.get(j).text();
                    if (NULL_VALUE.equalsIgnoreCase(value)) {
                        value = "NULL";
                    } else {
                        boolean hexData = value != null && value.startsWith("x'");
                        
                        value = (i == 0 ? "`" : (hexData ? "" : "'")) + value + (i == 0 ? "`" : (hexData ? "" : "'"));
                    }
                    
                    importQuery += value + ", ";
                }
                
                if (importQuery.endsWith(", ")) {
                    importQuery = importQuery.substring(0, importQuery.length() - 2);
                }
                
                if (i == 0) {
                    importQuery += ") VALUES ";
                } else {
                    importQuery += "), ";
                }
                
                progress += step;
                callback.progress(progress);
                
                if (callback.isNeedStop()) {
                    return;
                }
            }
            
            if (importQuery.endsWith(", ")) {
                importQuery = importQuery.substring(0, importQuery.length() - 2);
            }
        
            callback.progress(0.8);
   
            QueryResult qr = new QueryResult();
            int result = (int) dbService.execute(importQuery, qr);
            if (result >= 0) {
                callback.success(qr, null);
            } else {
                callback.error("Error HTML import", null);
            }
        
        } catch (Throwable e) {
            callback.error("Error HTML import", e);
            return;
        }
    }
    
    public static void importXLS(DBService dbService, String xlsFilePath, String databaseName, String tableName, ImportStatusCallback callback) {
        
        String importQuery = "INSERT INTO `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "` ";
            
        try {
            
            callback.progress(0.1);
            
            // read table structure
            DBTable t = dbService.getTableSchemaInformation(databaseName, tableName);
            Map<String, DataType> tableStructure = new LinkedHashMap<>();
            for (Column c: t.getColumns()) {
                tableStructure.put(c.getName(), c.getDataType());
            }
            
            // Reading xls file using WorkbookFactory
            // and use interface Workbook, so no need
            // known HSSF or XSSF workbook
            Workbook wb = WorkbookFactory.create(new File(xlsFilePath));
            
            Sheet sheet = wb.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            
            Map<Integer, String> existsColumn = new HashMap<>();
            
            if (callback.isNeedStop()) {
                return;
            }
            
            boolean headerRow = true;
            while (rows.hasNext()) {
                Row row = rows.next();
                
                importQuery += "(";
                
                // reading cell for each row
                // 1st row is db columns names
                Iterator<Cell> cells = row.cellIterator();
                while (cells.hasNext()) {
                    
                    if (callback.isNeedStop()) {
                        return;
                    }
                    
                    Cell cell = cells.next();
                    
                    String value = "";
                    if (headerRow) {
                        String strValue = cell.getStringCellValue();
                        // work only with columns that exists in destination table
                        if (tableStructure.containsKey(strValue)) {
                            value = "`" + strValue + "`";                            
                            existsColumn.put(cell.getColumnIndex(), strValue);
                        } else {
                            continue;
                        }
                    } else {
                        // if column exist in destination table
                        String columnName = existsColumn.get(cell.getColumnIndex());
                        if (columnName != null) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_STRING:
                                    
                                    String strValue = cell.getStringCellValue();
                                    if (NULL_VALUE.equalsIgnoreCase(strValue)) {
                                        value = "NULL";
                                    } else {
                                        if (strValue.isEmpty()) {
                                            DataType dt = tableStructure.get(columnName);
                                            switch (dt.getName()) {
                                                case "CHAR":
                                                case "LONGTEXT":
                                                case "MEDIUMTEXT":
                                                case "TEXT":
                                                case "VARCHAR":
                                                case "TINYTEXT":
                                                    value =  "''";
                                                    break;

                                                default:
                                                    value = "NULL";
                                            }
                                        } else {
                                            
                                            boolean hexData = strValue.startsWith("x'");
                                            
                                            value = hexData ? strValue : "'" + strValue + "'";
                                        }
                                    }

                                    break;

                                case Cell.CELL_TYPE_NUMERIC:
                                    // some cell value can has numeric type, but they are date
                                    if (HSSFDateUtil.isCellDateFormatted(cell)) {

                                        DataType dt = tableStructure.get(columnName);
                                        switch (dt.getName()) {                                            
                                            case "TIME":
                                                value = "'" + timeFormat.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue())) + "'";
                                                break;

                                            case "DATE":
                                                value = "'" + dateFormat.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue())) + "'";
                                                break;

                                            default:
                                                value = "'" + timestampFormat.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue())) + "'";
                                        }
                                    } else {
                                        value = "'" + cell.getNumericCellValue() + "'";
                                    }
                                    break;

                                case Cell.CELL_TYPE_BOOLEAN:
                                    value = cell.getBooleanCellValue() + "";
                                    break;
                            }
                        } else {
                            continue;
                        }
                    }
                    
                    importQuery += value + ", ";
                }
                
                if (importQuery.endsWith(", ")) {
                    importQuery = importQuery.substring(0, importQuery.length() - 2);
                }
                
                if (headerRow) {
                    headerRow = false;
                    importQuery += ") VALUES ";
                } else {
                    importQuery += "), ";
                }
            }
            
            callback.progress(0.7);
            
            if (importQuery.endsWith(", ")) {
                importQuery = importQuery.substring(0, importQuery.length() - 2);
            }
            
            if (callback.isNeedStop()) {
                return;
            }            
         
            QueryResult qr = new QueryResult();
            int result = (int) dbService.execute(importQuery, qr);
            if (result >= 0) {
                callback.success(qr, null);
            } else {
                callback.error("Error XLS import", null);
            }
        
        } catch (Throwable ex) {
            callback.error("Error XLS import", ex);
        }
    }
    
    
    public static void importOtherRDBMS(DBService dbService, String driverClassName, String connectionUrl, String connectionUser, String connectionPassword, String otherTableName, String databaseName, String tableName, ImportStatusCallback callback) {
        
        try {
            Class.forName(driverClassName);            
        
            String importQuery = "INSERT INTO `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "` ";
            boolean empty = true;
            
            callback.progress(0.1);

            // Connect to other RDBMS connection
            try (Connection connection = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword)) {
                
                // select all data from inputed table
                Statement st = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                try (ResultSet rs = st.executeQuery("SELECT * FROM " + otherTableName)) {
                    
                    importQuery += "(";
                    
                    // read info (columns names) from columns
                    ResultSetMetaData rsmd = rs.getMetaData();
                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        importQuery += "`" + rsmd.getColumnName(i) +  "`, ";
                    }
                    
                    if (importQuery.endsWith(", ")) {
                        importQuery = importQuery.substring(0, importQuery.length() - 2);
                    }
                    
                    importQuery += ") VALUES ";
                    
                    rs.last();
                    int size = rs.getRow();
                    rs.beforeFirst();
                    
                    double progress = 0.2;
                    double step = 0.6 / size;
                    
                    if (callback.isNeedStop()) {
                        return;
                    }
                    
                    callback.progress(progress + step);
                    while (rs.next()) {
                        empty = false;
                        importQuery += "(";
                        
                        if (callback.isNeedStop()) {
                            return;
                        }
                        
                        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                            
                            int columnType = rsmd.getColumnType(i);
                            
                            // convert column value to string insertable value
                            String value = rs.getString(i);
                            if (value == null) {
                                if (rsmd.isNullable(i) == 0) {                
                                    switch (columnType) {
                                        case Types.DATE:
                                            importQuery += "'0000-00-00', ";
                                            break;

                                        case Types.TIME:
                                        case Types.TIME_WITH_TIMEZONE:
                                            importQuery += "'00:00:00', ";
                                            break;

                                        case Types.TIMESTAMP:                
                                        case Types.TIMESTAMP_WITH_TIMEZONE:
                                            importQuery += "'0000-00-00 00:00:00', ";
                                            break;
                                    }
                                } else {
                                    importQuery += "NULL, ";
                                }
                            } else {
                                switch (columnType) {
                                    case Types.BIGINT:
                                    case Types.BOOLEAN:
                                    case Types.DECIMAL:
                                    case Types.DOUBLE:
                                    case Types.NUMERIC:
                                    case Types.FLOAT:
                                    case Types.INTEGER:
                                    case Types.BIT:
                                        importQuery += value + ", ";
                                        break;

                                    default:
                                        boolean hexData = value.startsWith("x'");
                                        importQuery += hexData ? value : "'" + value + "', ";
                                }
                            }
                        }
                        
                        if (importQuery.endsWith(", ")) {
                            importQuery = importQuery.substring(0, importQuery.length() - 2);
                        }
                        
                        importQuery += "), ";
                    }
                    
                    if (importQuery.endsWith(", ")) {
                        importQuery = importQuery.substring(0, importQuery.length() - 2);
                    }
                }
                
                callback.progress(0.8);
                
                if (callback.isNeedStop()) {
                    return;
                }
                
                if (!empty) {
                    QueryResult qr = new QueryResult();
                    int result = (int) dbService.execute(importQuery, qr);
                    if (result >= 0) {
                        callback.success(qr, null);
                    } else {
                        callback.error("Error Other RDBMS import", null);
                    }
                }
            } catch (SQLException ex) {
                callback.error("Error Other RDBMS import: using connection", ex);
            }            

        } catch (ClassNotFoundException ex) {
            callback.error("Other RDBMS import: Driver '" + driverClassName + "' not supported, include driver into class path", ex);
        }
    }
    
    public static void importSQL(DBService dbService, String sqlFilePath, String databaseName, ImportStatusCallback callback) {
        try {
            
            String query = new String(Files.readAllBytes(new File(sqlFilePath).toPath()), "UTF-8");
            dbService.execute(query);
            callback.success(null, null);
//            
//            // For now we just run script runner for exeuting SQL script
//            new ScriptRunner(dbService.getDB(), true).runScript(new FileReader(sqlFilePath), new ScriptRunner.ScriptRunnerCallback() {
//                
//                private boolean wasError = false;
//                private double step = 0;
//                private double progress = 0.1;
//                @Override
//                public void started(Connection con, int queriesSize) throws SQLException {
//                    
//                    con.setAutoCommit(false);
//                    
//                    callback.progress(progress);
//                    step = 0.8 / queriesSize;
//                }
//
//                @Override
//                public void queryExecuting(Connection con, QueryResult queryResult) {
//                }
//                
//                @Override
//                public void queryExecuted(Connection con, QueryResult queryResult) {
//                    progress += step;
//                    callback.progress(progress);
//                }
//
//                @Override
//                public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {
//                    con.rollback();
//                    wasError = true;
//                    callback.error("Error SQL import", th);
//                }
//
//                @Override
//                public boolean isWithLimit(QueryResult queryResult) {
//                    return false;
//                }
//                
//                @Override
//                public void finished(Connection con) throws SQLException {
//                    con.commit();
//                    if (!wasError) {
//                        callback.success(null, null);
//                    }
//                }
//
//                @Override
//                public boolean isNeedStop() {
//                    return callback.isNeedStop();
//                }
//            }, false);
        } catch (Throwable th) {
            callback.error("Error SQL import", th);
        }
    }
    
    
    public static void importXML(DBService dbService, String xmlFilePath, String databaseName, String tableName, ImportStatusCallback callback) {
        
        try {
            // parse XMl using SAX
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware(true);
            SAXParser saxParser = spf.newSAXParser();
            XMLReader parser = saxParser.getXMLReader();
            
            XMLSQLHandler xmlHandler = new XMLSQLHandler(databaseName, tableName);

            parser.setContentHandler(xmlHandler);
            parser.setEntityResolver(xmlHandler);
            parser.setDTDHandler(xmlHandler);
            parser.setErrorHandler(xmlHandler);

            callback.progress(0.2);
            parser.parse(new File(xmlFilePath).toURI().toURL().toString());
            
            if (callback.isNeedStop()) {
                return;
            }
            
            callback.progress(0.7);
            QueryResult qr = new QueryResult();
            int result = (int) dbService.execute(xmlHandler.parsedSQL, false);
            if (result >= 0) {
                callback.success(qr, null);
            } else {
                callback.error("Error XML import", null);
            }
        } catch (Throwable ex) {
            callback.error("Error XML import", ex);
        }
    }
    
    
    private static class XMLSQLHandler extends DefaultHandler {
        
        private static final String DATA_TAG = "data";
        private static final String ROW_TAG = "row";
        private String parsedSQL = "";
        
        private boolean columnsCollected = false;
        private List<String> columnsNames;
        private Map<String, String> columnsValues;
        
        private String currentColumn;
        
        public XMLSQLHandler(String databaseName, String tableName) {
            parsedSQL = "INSERT INTO `" + (databaseName != null ? databaseName + "`.`" : "") + tableName + "` (";
        }
        
        @Override
        public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException
        {
            if (DATA_TAG.equalsIgnoreCase(qName)) {
                columnsNames = new ArrayList<>();
                
            } else  if (ROW_TAG.equalsIgnoreCase(qName)) {
                columnsValues = new HashMap<>();
                
            } else {
                if (!columnsCollected) {
                    parsedSQL += qName + ", ";
                    columnsNames.add(qName);
                }
                
                currentColumn = qName;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException
        {
            if (ROW_TAG.equalsIgnoreCase(qName)) {
                if (!columnsCollected) {
                    columnsCollected = true;
                    
                    if (parsedSQL.endsWith(", ")) {
                        parsedSQL = parsedSQL.substring(0, parsedSQL.length() - 2);
                    }
                    
                    parsedSQL += ") VALUES ";
                }
                
                parsedSQL += "(";
                for (String c: columnsNames) {
                    parsedSQL += columnsValues.get(c) + ", ";
                }
                
                if (parsedSQL.endsWith(", ")) {
                    parsedSQL = parsedSQL.substring(0, parsedSQL.length() - 2);
                }
                
                parsedSQL += "), ";
            } else if (DATA_TAG.equalsIgnoreCase(qName)) {
                if (parsedSQL.endsWith(", ")) {
                    parsedSQL = parsedSQL.substring(0, parsedSQL.length() - 2);
                }
            }
        }
        
        @Override
        public void characters(char ch[], int start, int length) throws SAXException
        {
            String value = String.copyValueOf(ch, start, length);
            if (NULL_VALUE.equalsIgnoreCase(value)) {
                value = "NULL";
            } else {
                boolean hexData = value.startsWith("x'");
                value = hexData ? value : "'" + value + "'";
            }
            columnsValues.put(currentColumn, value);
        }
    }
}
