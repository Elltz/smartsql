/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.profiler.provider;

import javafx.scene.Parent;
import np.com.ngopal.smart.sql.profiler.MainApp;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.provider.BaseControllerProvider;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class ProfilerBaseControllerProvider <T extends BaseController> extends BaseControllerProvider<T>{

    public ProfilerBaseControllerProvider(UIController iController) {
        super(iController, null);
    }
    
    public ProfilerBaseControllerProvider() {
        super(MainApp.controller, null);
    }

    public ProfilerBaseControllerProvider(Class<T> meccalss) {
        super(MainApp.controller, meccalss);
    }

    @Override
    protected String getPreface() {
        return "/np/com/ngopal/smart/sql/profiler/ui/";
    }
    
}
