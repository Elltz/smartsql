package np.com.ngopal.smart.sql.profiler;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.profiler.controller.MainController;
import np.com.ngopal.smart.sql.profiler.modules.SmartProfilerModule;
import np.com.ngopal.smart.sql.profiler.provider.ProfilerBaseControllerProvider;
import np.com.ngopal.smart.sql.profiler.provider.ProfilerImageProvider;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.utils.SynchorniseWorkProccessor;
import org.apache.log4j.Logger;
import static javafx.application.Application.launch;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;


public class MainApp extends Application {
    
    public static MainController controller;
    private static SynchorniseWorkProccessor snyProccessor;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("SmartMYSQL PROFILER");        
        
        controller = new ProfilerBaseControllerProvider<MainController>(controller).get("Main");
        modulesConstruction();
        Scene scene = new Scene(controller.getUI(), 1000, 700);
        scene.getStylesheets().addAll(StandardResourceProvider.getInstance().getDependenceStylesheet()/*,
                getClass().getResource("ui/styles/style.css").toExternalForm()*/);
        stage.getIcons().add(ProfilerImageProvider.getInstance().getProfiler_logo_bg_image());
        stage.setScene(scene);
        stage.show();
        snyProccessor.start();
        controller.triggerUpdateMechanism();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        controller.shutdown();
        snyProccessor.destroy();
        snyProccessor = null;
        controller = null;
    }
    

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StandardApplicationManager.getInstance().handleApplicatEntryArguments(args);
        try {
            StandardApplicationManager.getInstance().initializeDatabaseDriver("smart-profiler.db",
                    "np/com/ngopal/smart/sql/profiler/smart-profiler.changelog.xml");
            snyProccessor = new SynchorniseWorkProccessor();
            snyProccessor.makeMain();            
            launch(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void modulesConstruction(){
        controller.registerProfilerModule(new SmartProfilerModule(controller));
    }
}
