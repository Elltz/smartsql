/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.profiler.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.Getter;
import lombok.Setter;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.profiler.provider.ProfilerBaseControllerProvider;
import np.com.ngopal.smart.sql.profiler.provider.ProfilerImageProvider;
import np.com.ngopal.smart.sql.profiler.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ConnectionCardsController extends BaseController implements Initializable {

    @FXML
    private Label paramLabel;

    @FXML
    private HBox downButtonsHolder;

    @FXML
    private Circle statusIndicator;

    @FXML
    private AnchorPane mainUI;

    @Setter
    @Getter
    private ConnectionParams params;
    
    @Getter
    private ConnectionSession session;

    private SimpleBooleanProperty connected = new SimpleBooleanProperty();
    
    private ContextMenu contextMenu;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        getUI().setUserData(this);
        
        getUI().setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                getUI().setEffect(new DropShadow());                
            }
        });
        
        getUI().setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                getUI().setEffect(null);                
            }
        });
        
        connected.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                Platform.runLater(()->{
                    if (newValue) {
                        statusIndicator.setFill(Color.GREEN);
                    } else {
                        statusIndicator.setFill(Color.GOLD);
                    }                    
                });
            }
        });
        
        ImageView imageView = new ImageView(StandardDefaultImageProvider.getInstance().getConnectionButton_image());
        imageView.setFitWidth(16);
        imageView.setFitHeight(16);
        
        MenuItem connectMenuItem = new MenuItem("Connect", imageView);
        connectMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(connected.get()){
                    getController().selectTab(session.getId());
                }else{   
                    getUI().setDisable(true);
                    makeBusy(true);
                    Recycler.addWorkProc(new WorkProc() {
                        @Override
                        public void updateUI() { }

                        @Override
                        public void run() {
                            updateSession(((MainController) getController()).createConnectionSession(params));
                            Platform.runLater(() -> {                                
                                getUI().setDisable(false);
                                makeBusy(false);
                            });
                        }
                    });                    
                }
            }
        });
        
        imageView = new ImageView(ProfilerImageProvider.getInstance().getEdit_connection_image());
        imageView.setFitWidth(16);
        imageView.setFitHeight(16);
        MenuItem editMenuItem = new MenuItem("View/Edit Connection", imageView);
        editMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                NewConnectionController ncc = new ProfilerBaseControllerProvider<NewConnectionController>().get("NewConnection");
                ncc.setAndUpdateParams(params);
                Stage s = new Stage(StageStyle.UTILITY);
                s.setTitle(params.getName() + " details");
                s.initOwner(getStage());
                s.initModality(Modality.APPLICATION_MODAL);
                s.setScene(new Scene(ncc.getUI(), 600, 500));
                s.showAndWait();
                init();
            }
        });
        
        imageView = new ImageView(StandardDefaultImageProvider.getInstance().getDeleteButton_image());
        imageView.setFitWidth(16);
        imageView.setFitHeight(16);
        MenuItem removeMenuItem = new MenuItem("Remove Connection", imageView);
        removeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                boolean a = ServiceEntityProviders.getInstance().getDb().delete(params);
                if(a){
                    ((FlowPane)getUI().getParent()).getChildren().remove(getUI());
                }
            }
        });
        
        contextMenu = new ContextMenu(connectMenuItem, new SeparatorMenuItem(), editMenuItem, removeMenuItem);

        getUI().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                contextMenu.show(getUI(), event.getScreenX(), event.getScreenY());
            }
        });
    }

    public void init() {
        if (params == null) {
            throw new RuntimeException("ConnectionParams not set");
        }
        StringBuilder sb = new StringBuilder(params.getName().toUpperCase())
                .append("\n  ")
                .append("User : ")
                .append(params.getUsername())
                .append("\n  ")
                .append("Addr : ")
                .append(params.getAddress());
        paramLabel.setText(sb.toString());
    }

    public void setConnected(boolean isConnected) {
        connected.set(isConnected);
    }
    
    public void updateSession(ConnectionSession connectionSession){
        session = connectionSession;
        connected.set(session != null && session.isOpen());
        if(session != null && session.getConnectionParam() != null){
            params = session.getConnectionParam();
        }
    }
}
