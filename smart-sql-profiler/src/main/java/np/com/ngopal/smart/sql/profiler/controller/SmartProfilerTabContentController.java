/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.profiler.controller;

import com.sun.javafx.event.EventDispatchChainImpl;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import lombok.AccessLevel;
import lombok.Setter;
import np.com.ngopal.smart.sql.components.DynamicMenuItem;
import np.com.ngopal.smart.sql.components.IconedMenuItem;
import np.com.ngopal.smart.sql.components.TextMenu;
import np.com.ngopal.smart.sql.components.TextMenuItem;
import np.com.ngopal.smart.sql.components.ToolbarDynamicMenu;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.profiler.provider.ServiceEntityProviders;
import np.com.ngopal.smart.sql.structure.controller.ProfilerTabContentController;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.checkExclusion;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.hasCoordinate;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class SmartProfilerTabContentController extends ProfilerTabContentController {

    @Setter(AccessLevel.PUBLIC)
    private Map<KeyCombination, List<Object>> keyCombinations;

    @Setter(AccessLevel.PUBLIC)
    private Map<KeyCombination, Consumer> customKeyCombinations;
    
    @Setter(AccessLevel.PUBLIC)
    private Map<MenuCategory, List<MenuItem>> categories;

    @FXML
    private AnchorPane toolbarParentContainer;

    @FXML
    private HBox toolBarContainer;

    @FXML
    private ToggleButton emptyTab;

    @FXML
    private ToggleGroup toolBarButtonGroup;

    @FXML
    private HBox toolbarButtonsContainer;

    @FXML
    private ToolBar additionalButtonContainer;

    @Override
    public void isFocusedNow() {
        super.isFocusedNow();
        Platform.runLater(() -> {
            toolBarButtonGroup.selectToggle(emptyTab);
        });
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
                
        toolBarButtonGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle unselected, Toggle selected) {
                if (selected != null) {
                    additionalButtonContainer.getItems().clear();
                    List<Node> list = (List<Node>) selected.getUserData();
                    if (list != null) {
                        for (Node n : list) {
                            if (n.visibleProperty().get()) {
                                additionalButtonContainer.getItems().add(n);
                            }
                        }
                    }
                }
            }
        });

        toolBarButtonGroup.selectToggle(null);

        getUI().sceneProperty().addListener(new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
                getUI().sceneProperty().removeListener(this);
                addSceneListeners(newValue);
            }
        });

        setDbProfilerService(ServiceEntityProviders.getInstance().getProfilerService());
        setChartService(ServiceEntityProviders.getInstance().getProfilerChartService());
        setSettingsService(ServiceEntityProviders.getInstance().getProfilerSettingsService());
        setPreferenceDataService(ServiceEntityProviders.getInstance().getPrefDataService());
        setQARecommendationService(ServiceEntityProviders.getInstance().getQARecommendationService());
        setAPerformanceTuningService(ServiceEntityProviders.getInstance().getQAPerformanceTuningService());
        setColumnCardinalityService(ServiceEntityProviders.getInstance().getColumnCardinalityService());
        setConnectionParamService(ServiceEntityProviders.getInstance().getDb());
        setProfilerDataManager(ServiceEntityProviders.getInstance().getProfilerService());
        setUsageService(ServiceEntityProviders.getInstance().getUsageService());
        
        updateSettings();
    }

    private void addSceneListeners(Scene scene) {
        scene.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {

            boolean f0 = hasCoordinate(toolBarContainer, event.getX(), event.getY());
            boolean f1 = hasCoordinate(toolbarButtonsContainer, event.getX(), event.getY());
            if (!f0 && !f1) {
                returnToDefaultTopMenuHandler.handle(event);
            }
        });

        scene.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {

//            log.error("KEY_PRESSED: " + event);
            for (KeyCombination keyCom : customKeyCombinations.keySet()) {
                if (keyCom.match(event)) {

                    Consumer mi = customKeyCombinations.get(keyCom);
                    mi.accept(null);
                    event.consume();

                    break;
                }
            }

            // TODO 
            // This is a hack, tableview handling "F2" key inside and this KeyCombinaton not working
            // for now just add exclusion for it            
            Event e = null;
            if (checkExclusion(event)) {
                e = event;
            } else {
                try {
                    e = ((Node) event.getTarget()).getEventDispatcher().dispatchEvent(event, new EventDispatchChainImpl());
                } catch (Exception exception) {
                }
            }

            if (e != null) {
                for (KeyCombination keyCom : keyCombinations.keySet()) {
                    if (keyCom.match(event)) {
                        List<Object> list = keyCombinations.get(keyCom);
                        for (Object node : list) {
                            if (node instanceof MenuItem) {
                                if (!((MenuItem) node).isDisable()) {
                                    ((MenuItem) node).fire();
                                    event.consume();
                                    break;
                                }
                            } else if (!((Button) node).isDisable()) {
                                ((Button) node).getOnMouseClicked().handle(null);
                                event.consume();
                                break;
                            }
                        }
                    }
                }
            } else {
                event.consume();
            }
        });
    }

    private final EventHandler<Event> returnToDefaultTopMenuHandler = (Event event) -> {
        if (toolBarContainer.getChildren().size() > 1 && Objects.equals(getController().getSelectedConnectionSession().get(), getSession())) {
            toolBarButtonGroup.selectToggle(emptyTab);
        }
    };

    private void decorateUserInterface() {
        for (Map.Entry<MenuCategory, List<MenuItem>> en : categories.entrySet()) {

            MenuCategory mc = en.getKey();
            ArrayList<Node> nodes = new ArrayList(en.getValue().size());

            ToggleButton tog = null;
            if (mc == null) {
                tog = emptyTab;
            } else {
                tog = new ToggleButton(mc.getDisplayValue());
                tog.setToggleGroup(toolBarButtonGroup);
                tog.getStyleClass().add("top-toolbar-toggle");
                ToggleButton t = tog;
                Platform.runLater(() -> {
                    toolBarContainer.getChildren().add(t);
                });
            }
            tog.setUserData(nodes);

            for (MenuItem mi : en.getValue()) {
                Labeled node;

                if (mi instanceof Menu) {
                    node = new MenuButton();
                    node.setMaxWidth(Label.USE_COMPUTED_SIZE);
                    if (mc != null) {
                        node.textProperty().bind(mi.textProperty());
                    }
                    node.setId("tollbar-menu-button");

                    if (mi instanceof ToolbarDynamicMenu) {
                        ((MenuButton) node).showingProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                            if (newValue != null && newValue) {
                                ((ToolbarDynamicMenu) mi).onShowing((MenuButton) node, returnToDefaultTopMenuHandler);
                            }
                        });
                    }

                    Menu subMenu = (Menu) mi;
                    ((MenuButton) node).getItems().addAll(subMenu.getItems());
                } else if (mi instanceof DynamicMenuItem) {
                    //categories.get(category).add(((DynamicMenuItem) mi).createNode());
                    continue;
                } else {
                    node = new Button();
                    node.setId("tollbarlabel");
                    node.setWrapText(true);
                    //node.setMinHeight(30);
                    node.setPrefHeight(15);
                    node.setMaxHeight(25);
                    node.setUserData(mi.getAccelerator());
                    if (!(mi instanceof IconedMenuItem)) {
                        node.textProperty().bind(mi.textProperty());
                    }

                    node.setOnMouseClicked((MouseEvent event) -> {
                        returnToDefaultTopMenuHandler.handle(event);
                        if (mi.getOnAction() != null) {
                            mi.fire();
                        }
                    });

                }

                //sir Sudheer requirement - 25/03/2018
                node.setContentDisplay(ContentDisplay.LEFT);
                node.setGraphicTextGap(5);
                node.setMaxWidth(Double.MAX_VALUE);

                node.disableProperty().bind(mi.disableProperty());
                node.visibleProperty().bind(mi.visibleProperty());
                node.setFocusTraversable(false);

                mi.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (mi.getUserData() == null) {
                        String toolTip = newValue != null ? newValue : "";
                        if (mi.getAccelerator() != null) {
                            toolTip += " (" + mi.getAccelerator().getDisplayText() + ")";
                        }
                        Tooltip tt = new Tooltip(toolTip);
                        node.setTooltip(tt);
                    }
                });

                String toolTip = mi.getUserData() != null ? mi.getUserData().toString() : mi.getText();
                if (mi.getAccelerator() != null) {
                    toolTip += " (" + mi.getAccelerator().getDisplayText() + ")";
                }

                Tooltip tt = new Tooltip(toolTip);
                node.setTooltip(tt);

                //hackTooltipStartTiming(node.getTooltip());
                node.setCursor(Cursor.HAND);

                if ((mi instanceof TextMenuItem) || (mi instanceof TextMenu)) {
                    node.setContentDisplay(ContentDisplay.RIGHT);
                }

                if (mi.getGraphic() != null) {

                    ImageView im0 = (ImageView) mi.getGraphic();
                    ImageView nodeGraphic = new ImageView(im0.getImage());
                    nodeGraphic.setFitHeight(im0.getFitHeight());
                    nodeGraphic.setFitWidth(im0.getFitWidth());
                    node.setGraphic(nodeGraphic);

                    mi.graphicProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                        if (newValue instanceof ImageView) {
                            ImageView imageview = new ImageView(((ImageView) newValue).getImage());
                            imageview.setFitHeight(((ImageView) newValue).getFitHeight());
                            imageview.setFitWidth(((ImageView) newValue).getFitWidth());
                            node.setGraphic(imageview);
                        }
                    });

                    //it ends here
                    if (mc == null) {
                        node.textProperty().unbind();
                        node.setText("");
                    }
                }

                node.setWrapText(false);
                if (mi.getStyleClass().contains("time") || (mi instanceof TextMenu)) {
                    node.setMaxSize(Double.MAX_VALUE, 30);
                    node.setGraphicTextGap(0);
                }

                node.setTextAlignment(TextAlignment.CENTER);
                node.setPadding(new Insets(0, 5, 0, 5));

                HBox.setMargin(node, new Insets(0, 2, 0, 2));

                nodes.add(new Group(node));
            }
        }
    }

    @Override
    public void openConnection(ConnectionParams params) throws SQLException {
        // enable multy query execution
        params.setMultiQueries(true);
        
        super.openConnection(params);
        decorateUserInterface();
    }
    
    
}
