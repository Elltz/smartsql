/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.profiler.provider;

import java.sql.SQLException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.db.AppSettingsService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import np.com.ngopal.smart.sql.db.DeadlockService;
import np.com.ngopal.smart.sql.db.H2ProfilerStorageProvider;
import np.com.ngopal.smart.sql.db.LastOsProfilerServerSpeculationsService;
import np.com.ngopal.smart.sql.db.MySQLProfilerStorageProvider;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.MysqlTechnology;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.provider.DefaultProfilerDataManager;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
@Getter(AccessLevel.PUBLIC)
public class ServiceEntityProviders {

    private String persistenceUnit = "smart-profiler-persistence-unit";

    private static ServiceEntityProviders serviceEntityProviders;

    public static ServiceEntityProviders getInstance() {
        if (serviceEntityProviders == null) {
            serviceEntityProviders = new ServiceEntityProviders();
        }
        return serviceEntityProviders;
    }
    
    public static void destroyCurrentEntityProvider(){
        if(serviceEntityProviders != null){
            serviceEntityProviders.destroyEntity();
        }
        serviceEntityProviders = null;
        System.gc();
    }

    private ConnectionParamService db;
    private UserUsageStatisticService usageService;
    private MysqlDBService generalMysqlDBService;
    private AppSettingsService settignsService;
    private ProfilerChartService profilerChartService;
    private ProfilerSettingsService profilerSettingsService;
    private LastOsProfilerServerSpeculationsService lastOsProfilerServerSpeculationsService;
    private SQLLogger qlLog;
    private MysqlTechnology mysqlTechnology;
    private ProfilerDataManager profilerService;
    private PreferenceDataService prefDataService;
    private QAPerformanceTuningService qAPerformanceTuningService;
    private QARecommendationService qARecommendationService;
    private DeadlockService deadlockService;
    private ColumnCardinalityService columnCardinalityService;
    private DBDebuggerProfilerService debuggerProfilerService;

    private ServiceEntityProviders() {
        try {
            lastOsProfilerServerSpeculationsService = new LastOsProfilerServerSpeculationsService(persistenceUnit);
            db = new ConnectionParamService(persistenceUnit);
            usageService = new UserUsageStatisticService(persistenceUnit);
            profilerChartService = new ProfilerChartService(persistenceUnit);
            profilerChartService.setDefImportChartScript(FunctionHelper.streamToString(
                    StandardResourceProvider.getInstance().getDefaultImportChartScript()));
            // init if need charts
            profilerChartService.getAll(null);
            profilerSettingsService = new ProfilerSettingsService(persistenceUnit);
            qlLog = new SQLLogger();
            mysqlTechnology = new MysqlTechnology();
            generalMysqlDBService = new MysqlDBService(qlLog);
            prefDataService = new PreferenceDataService(persistenceUnit);
            qARecommendationService = new QARecommendationService(persistenceUnit);
            qAPerformanceTuningService = new QAPerformanceTuningService(persistenceUnit);
            deadlockService = new DeadlockService(persistenceUnit);
            columnCardinalityService = new ColumnCardinalityService(persistenceUnit);
            settignsService = new AppSettingsService(persistenceUnit);
            
            profilerService = new DefaultProfilerDataManager();
            applyProviders(profilerService);
            
            debuggerProfilerService = new DBDebuggerProfilerService();
            applyProviders(debuggerProfilerService);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void applyProviders(ProfilerDataManager dataManager) {
        dataManager.addStorageProvider(QueryProvider.DB__H2, new H2ProfilerStorageProvider(persistenceUnit));
        dataManager.addStorageProvider(QueryProvider.DB__MYSQL, new MySQLProfilerStorageProvider());
        
        ProfilerSettings ps = profilerSettingsService.getProfilerSettings();
        
        if (ps == null || ps.getStorageType() == 0) {
            dataManager.activateStorage(QueryProvider.DB__H2);
            
        } else if (ps != null && ps.getStorageType() == 1) {
                        
            MySQLProfilerStorageProvider storage = (MySQLProfilerStorageProvider) dataManager.activateStorage(QueryProvider.DB__MYSQL);
            try {
                storage.init(db.get(ps.getConnectionId()), ps.getDatabaseName());
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }
    
    public ConnectionParams getNewConnectionParams() {
        return new ConnectionParams();
    }

    public MysqlDBService getNewMysqlDBService() {
        return new MysqlDBService(qlLog);
    }
    
    public void destroyEntity() {
        lastOsProfilerServerSpeculationsService = null;
        db = null;
        usageService = null;
        profilerChartService = null;
        profilerSettingsService = null;
        qlLog = null;
        mysqlTechnology = null;
        generalMysqlDBService = null;
        profilerService = null;
        prefDataService = null;
        qARecommendationService = null;
        qAPerformanceTuningService = null;
    }
    
    
    //  qa
    public void usedQueryAnalyzer() {
        try {
            usageService.savePUsage(PUsageFeatures.QA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzer", th);
        }
    }

    // sqa
    public void usedSlowQueryAnalyzer() {
        try {
            usageService.savePUsage(PUsageFeatures.SQA);
        } catch (Throwable th) {
            log.error("Error on usedSlowQueryAnalyzer", th);
        }
    }

    // sqaqa
    public void usedQueryAnalyzerFromSlowQueryAnalyzer() {
        try {
            usageService.savePUsage(PUsageFeatures.SQAQA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzerFromSlowQueryAnalyzer", th);
        }
    }

    // debugger
    public void usedDebugger() {
        try {
            usageService.savePUsage(PUsageFeatures.DEBUGGER);
        } catch (Throwable th) {
            log.error("Error on usedDebugger", th);
        }
    }

    // dqa
    public void usedQueryAnalyzerFromDebugger() {
        try {
            usageService.savePUsage(PUsageFeatures.DQA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzerFromDebugger", th);
        }
    }

    // profiler
    public void usedProfiler() {
        try {
            usageService.savePUsage(PUsageFeatures.PROFILER);
        } catch (Throwable th) {
            log.error("Error on usedProfiler", th);
        }
    }

    // pqa
    public void usedQueryAnalyzerFromProfiler() {
        try {
            usageService.savePUsage(PUsageFeatures.PQA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzerFromProfiler", th);
        }
    }

    // mst
    public void usedMySqlTuner() {
        try {
            usageService.savePUsage(PUsageFeatures.MST);
        } catch (Throwable th) {
            log.error("Error on usedMySqlTuner", th);
        }
    }

    // lfe
    public void usedLockExecution() {
        try {
            usageService.savePUsage(PUsageFeatures.LFE);
        } catch (Throwable th) {
            log.error("Error on usedLockExecution", th);
        }
    }


}
