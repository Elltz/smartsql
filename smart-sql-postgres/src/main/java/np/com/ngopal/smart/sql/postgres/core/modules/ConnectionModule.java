package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.controller.NewConnectionController;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCode;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
public class ConnectionModule extends AbstractConnectionModule {
    
    @Inject
    private ProfilerDataManager profilerService;
    
    private BaseController controller;

    @Override
    public String getName() {
        return "SQL Browser";
    }

    @Override
    public boolean install() {
        super.install();
        registrateServiceChangeListener();

        MenuItem newConnection = getNewConnection();

        newConnection.disableProperty().bind(busy());
        
        MenuItem newConnectionUsingCurrent = new MenuItem("New Connection using Current Setting",
                FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getNewConnectionUsingCurrent_image()), 20, 20));

        newConnectionUsingCurrent.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
        newConnectionUsingCurrent.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MysqlDBService service = MainUI.injector.getInstance(MysqlDBService.class);
                getMainController().openConnection(service, getMainController().getSelectedConnectionSession().get().getConnectionParam(), true, null);
            }
        });

        MenuItem[] items = new MenuItem[]{newConnectionUsingCurrent, getNewConnection()};

        Menu menu = new Menu("Connection", FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getConnection_image()), 20, 20), items);

        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FILE, new MenuItem[]{menu});

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{
            newConnection});

        MenuItem clearCachedDataItem = new MenuItem("Clear cached profiler data", new ImageView(StandardDefaultImageProvider.getInstance().getClear_profiler_history_image()));
        clearCachedDataItem.setOnAction((ActionEvent event) -> {
            
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {
                tcc.makeBusy(true);
            }
            
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                    DialogHelper.showInfo(getController(), "Done", "Data cleared!", null);
                    tcc.makeBusy(false);
                }

                @Override
                public void run() {
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY_TEMPLATE_PQTABLE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQSTATE_STAT));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY_STATS));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQSTATE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQTABLE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQDB));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUERY_TEMPLATE));
                    profilerService.executeUpdate(QueryProvider.getInstance().getQuery(QueryProvider.DB__H2, QueryProvider.QUERY__DELETE_PQUSER_HOST));
                    
                    callUpdate = true;
                }
            });
        });
        
        getController().addMenuItems(ConnectionModule.class, MenuCategory.POWERTOOLS, new MenuItem[] {clearCachedDataItem});
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.TAB, KeyCombination.SHORTCUT_DOWN, KeyCodeCombination.SHIFT_DOWN), (Object obj) -> getController().prevTab());
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.TAB, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().nextTab());
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(0));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT2, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(1));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(2));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT4, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(3));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT5, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(4));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT6, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(5));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT7, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(6));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT8, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectTab(7));
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT9, KeyCombination.SHORTCUT_DOWN), (Object obj) -> getController().selectLast());
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.B, KeyCombination.SHORTCUT_DOWN), (Object obj) -> { 
            TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
            if (tcc != null) {
                tcc.focusLeftTree();
            }
        });
        
        
        return true;
    }

    private MenuItem getNewConnection() {
        MenuItem newConnection = new MenuItem("New Connection",
                FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getNewConnection_image()), 20, 20));

        newConnection.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHORTCUT_DOWN));
        newConnection.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getMainController().triggerHomeAction(true);
            }
        });

        return newConnection;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem obj) {return true; }

    public void addTab(Tab t) {
        ((NewConnectionController) controller).addTab(t);
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public boolean isConnection() {
        return true;
    }

    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {
    }

    @Override
    public Map<?, ?> getPersistingMap() {
        return super.getPersistingMap();
    }

    @Override
    public String getInfo() {
        return "Open a new SQL Connection..";
    }

    

    @Override
    protected void serviceBusyChanged(Boolean newValue) {
        super.serviceBusyChanged(newValue); //To change body of generated methods, choose Tools | Templates.
    }

    // TODO i think this no needed in this module
    // this method must say - can we open more then one module or no
//    @Override
//    public boolean canOpenConnection(ConnectionParams params) { 
//        exception = null;
//        try {
//            service.setServiceBusy(true);
//            if (service.getDB() == null) {
//                service.connect(params);
//            }
//            service.testConnection(params);
//            return true;
//        } catch (Exception e) {
//            exception = e;
//            return false;
//        }
//    }
    
}
