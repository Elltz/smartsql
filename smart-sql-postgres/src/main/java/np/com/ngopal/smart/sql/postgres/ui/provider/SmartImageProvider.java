/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.provider;

import javafx.scene.image.Image;
import lombok.AccessLevel;
import lombok.Getter;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Getter(AccessLevel.PUBLIC)
public class SmartImageProvider extends DefaultImageProvider {

    private static SmartImageProvider sip;

    public static SmartImageProvider getInstance() {
        if (sip == null) {
            sip = new SmartImageProvider();
        }
        return sip;
    }

    @Override
    public String getImagePathRoute() {
        return "/np/com/ngopal/smart/sql/ui/images/";
    }

    public SmartImageProvider() { }
    
    /**
     * //////////////////////////////////////////
     * 
     * CONNECTION FOLDER IMAGES
     * 
     * ///////////////////////////////////////////
     */
    
    
    public Image getSortEmpty_image() {
        return getNonDeclaredImage("sort_empty.png");
    }
    
    public Image getSortAsc_image() {
        return getNonDeclaredImage("sort_asc.png");
    }
    
    public Image getSortDesc_image() {
        return getNonDeclaredImage("sort_desc.png");
    }
    
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRefreshObjectBrowser_image(){
        return getNonDeclaredImage("connection/refresh_object_browser.png", 20, 20);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDisconnectAll_image(){
        return getNonDeclaredImage("connection/disconnet_all.png", 20, 20);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCopyTableData_image(){
        return getNonDeclaredImage("connection/copy_table_data.png", 20, 20);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getInsertNewRow_image(){
        return getNonDeclaredImage("connection/insert_new_row.png", 20, 20);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDuplicateCurrentRow_image(){
        return getNonDeclaredImage("connection/duplicate_current_row.png", 20, 20);
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFormView_image(){
        return getNonDeclaredImage("connection/form_view.png", 20, 20);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTextView_image(){
        return getNonDeclaredImage("connection/text_view.png", 20, 20);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getLeftArrow_small_image(){
        return getNonDeclaredImage("connection/left_arrow_small.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRightArrow_small_image(){
        return getNonDeclaredImage("connection/right_arrow_small.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPreviousLast_image(){
        return getNonDeclaredImage("connection/previouslast.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPrevious_image(){
        return getNonDeclaredImage("connection/previous.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNextLast_image(){
        return getNonDeclaredImage("connection/nextlast.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNext_image(){
        return getNonDeclaredImage("connection/next.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getResultTab_image(){
        return getNonDeclaredImage("connection/result_tab.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCloseTab_image() {
        return getNonDeclaredImage("connection/close_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getColor_image() {
        return getNonDeclaredImage("connection/color.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getClear_image() {
        return getNonDeclaredImage("connection/clear.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAbout_image() {
        return getNonDeclaredImage("connection/about.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAddFavorite_image() {
        return getNonDeclaredImage("connection/add_favorite.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAdvanced_image() {
        return getNonDeclaredImage("connection/advanced.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAlterEvent_image() {
        return getNonDeclaredImage("connection/alter_event.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAlterFunction_image() {
        return getNonDeclaredImage("connection/alter_function.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAlterStoredProcs_image() {
        return getNonDeclaredImage("connection/alter_storedprocs.png");
    }
    
    public Image getAlterStoredProcs_image(int width, int height) {
        return getNonDeclaredImage("connection/alter_storedprocs.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAlterTable_image() {
        return getNonDeclaredImage("connection/alter_table.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAlterTrigger_image() {
        return getNonDeclaredImage("connection/alter_trigger.png");
    }
    
    public Image getAlterTrigger_image(int width, int height) {
        return getNonDeclaredImage("connection/alter_trigger.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAlterView_image() {
        return getNonDeclaredImage("connection/alter_view.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getArrange_image() {
        return getNonDeclaredImage("connection/arrange.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getBackUpExport_image() {
        return getNonDeclaredImage("connection/backup_export.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getBrowser_image() {
        return getNonDeclaredImage("connection/browser.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCancelExecute_image() {
        return getNonDeclaredImage("connection/cancel_execute.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCascade_image() {
        return getNonDeclaredImage("connection/cascade.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCheckUpdate_image() {
        return getNonDeclaredImage("connection/check_update.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getColumnDrop_image() {
        return getNonDeclaredImage("connection/column_drop.png");
    }
    
    public Image getColumnDrop_image(int width, int height) {
        return getNonDeclaredImage("connection/column_drop.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getColumnManage_image() {
        return getNonDeclaredImage("connection/column_manage.png");
    }
    
    public Image getColumnManage_image(int width, int height) {
        return getNonDeclaredImage("connection/column_manage.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getColumnMenu_image() {
        return getNonDeclaredImage("connection/column_menu.png");
    }
    
    public Image getColumnMenu_image(int width, int height) {
        return getNonDeclaredImage("connection/column_menu.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getColumnTab_image() {
        return getNonDeclaredImage("connection/column_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getComment_image() {
        return getNonDeclaredImage("connection/comment.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getConnectionGreen_image() {
        return getNonDeclaredImage("connection/connection_green.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getConnectionWhite_image() {
        return getNonDeclaredImage("connection/connection_white.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getConnection_image() {
        return getNonDeclaredImage("connection/connection.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCopy_image() {
        return getNonDeclaredImage("connection/copy.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCopyIndex_image() {
        return getNonDeclaredImage("connection/copy_index.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreate_image() {
        return getNonDeclaredImage("connection/create.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateEvent_image() {
        return getNonDeclaredImage("connection/create_event.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateFunction_image() {
        return getNonDeclaredImage("connection/create_function.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateIndex_image() {
        return getNonDeclaredImage("connection/create_index.png");
    }
    
    public Image getCreateIndex_image(int width, int height) {
        return getNonDeclaredImage("connection/create_index.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateStoredProcs_image() {
        return getNonDeclaredImage("connection/create_storedprocs.png");
    }
    
    public Image getCreateStoredProcs_image(int width, int height) {
        return getNonDeclaredImage("connection/create_storedprocs.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateTable_image() {
        return getNonDeclaredImage("connection/create_table.png");
    }
    
    public Image getCreateTable_image(int width, int height) {
        return getNonDeclaredImage("connection/create_table.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateTrigger_image() {
        return getNonDeclaredImage("connection/create_trigger.png");
    }
    
    public Image getCreateTrigger_image(int width, int height) {
        return getNonDeclaredImage("connection/create_trigger.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCreateView_image() {
        return getNonDeclaredImage("connection/create_view.png");
    }
    
    public Image getCreateView_image(int width, int height) {
        return getNonDeclaredImage("connection/create_view.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getCut_image() {
        return getNonDeclaredImage("connection/cut.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_csv_image() {
        return getNonDeclaredImage("connection/data_export_csv.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_excel_image() {
        return getNonDeclaredImage("connection/data_export_excel.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_html_image() {
        return getNonDeclaredImage("connection/data_export_html.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_json_image() {
        return getNonDeclaredImage("connection/data_export_json.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image get() {
        return getNonDeclaredImage("connection/");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExportLogo_image() {
        return getNonDeclaredImage("connection/data_export_logo2.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_pdf_image() {
        return getNonDeclaredImage("connection/data_export_pdf.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_sql_image() {
        return getNonDeclaredImage("connection/data_export_sql.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_text_image() {
        return getNonDeclaredImage("connection/data_export_text.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataExport_xml_image() {
        return getNonDeclaredImage("connection/data_export_xml.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataVisual_image() {
        return getNonDeclaredImage("connection/data_visual.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabase_image() {
        return getNonDeclaredImage("connection/database.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseAlter_image() {
        return getNonDeclaredImage("connection/database_alter.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseCreate_image() {
        return getNonDeclaredImage("connection/database_create.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseDrop_image() {
        return getNonDeclaredImage("connection/database_drop.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseDump_image() {
        return getNonDeclaredImage("connection/database_dump.png");
    }
    
    public Image getDatabaseDump_image(int width, int height) {
        return getNonDeclaredImage("connection/database_dump.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseEmpty_image() {
        return getNonDeclaredImage("connection/database_empty.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseExecuteScript_image() {
        return getNonDeclaredImage("connection/database_execute_script.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseExport_image() {
        return getNonDeclaredImage("connection/database_export.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseHtml_image() {
        return getNonDeclaredImage("connection/database_html.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseInfo_image() {
        return getNonDeclaredImage("connection/database_info.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseSearch_image() {
        return getNonDeclaredImage("connection/database_search.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseStatus_image() {
        return getNonDeclaredImage("connection/database_status.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseSyncWizard_image() {
        return getNonDeclaredImage("connection/database_sync_wizard.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseTab_image() {
        return getNonDeclaredImage("connection/database_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseTruncate_image() {
        return getNonDeclaredImage("connection/database_truncate.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDebug_image() {
        return getNonDeclaredImage("connection/debug.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDebugTab_image() {
        return getNonDeclaredImage("connection/debug_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDelete_image() {
        return getNonDeclaredImage("connection/delete.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDisconnect_image() {
        return getNonDeclaredImage("connection/disconnect.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDropEvent_image() {
        return getNonDeclaredImage("connection/drop_event.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDropFunction_image() {
        return getNonDeclaredImage("connection/drop_function.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDropIndex_image() {
        return getNonDeclaredImage("connection/drop_index.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDropStoredProcs_image() {
        return getNonDeclaredImage("connection/drop_storedprocs.png");
    }
    
    public Image getDropStoredProcs_image(int width, int height) {
        return getNonDeclaredImage("connection/drop_storedprocs.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDropTrigger_image() {
        return getNonDeclaredImage("connection/drop_trigger.png");
    }
    
    public Image getDropTrigger_image(int width, int height) {
        return getNonDeclaredImage("connection/drop_trigger.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDropView_image() {
        return getNonDeclaredImage("connection/drop_view.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getEditIndex_image() {
        return getNonDeclaredImage("connection/edit_index.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getEndSession_image() {
        return getNonDeclaredImage("connection/end_session.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getEngine_image() {
        return getNonDeclaredImage("connection/engine.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getError_image() {
        return getNonDeclaredImage("connection/error.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getErrorDisable_image() {
        return getNonDeclaredImage("connection/error_disable.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getEvent_image() {
        return getNonDeclaredImage("connection/event.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getEventTab_image() {
        return getNonDeclaredImage("connection/event_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExecute_image() {
        return getNonDeclaredImage("connection/execute.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExecuteAll_image() {
        return getNonDeclaredImage("connection/execute_all.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExecuteBulk_image() {
        return getNonDeclaredImage("connection/execute_bulk.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExecuteEdit_image() {
        return getNonDeclaredImage("connection/execute_edit.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExecuteLock_image() {
        return getNonDeclaredImage("connection/execute_lock.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExecuteQueryMenu_image() {
        return getNonDeclaredImage("connection/execute_query_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExpiredIcon_image() {
        return getNonDeclaredImage("connection/expired_icon.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExplain_image() {
        return getNonDeclaredImage("connection/explain.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExplain_small_image() {
        return getNonDeclaredImage("connection/explain_small.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExplainQuery_image() {
        return getNonDeclaredImage("connection/explain_query.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExportConnectionDetails_image() {
        return getNonDeclaredImage("connection/export_con_details.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExportView_image() {
        return getNonDeclaredImage("connection/export_view.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFeg_image() {
        return getNonDeclaredImage("connection/feq.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFind_image() {
        return getNonDeclaredImage("connection/find.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFindMenu_image() {
        return getNonDeclaredImage("connection/find_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFlush_image() {
        return getNonDeclaredImage("connection/flush.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFolder_image() {
        return getNonDeclaredImage("connection/folder.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFolderFilter_image() {
        return getNonDeclaredImage("connection/folder_filter.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getForeignKey_image() {
        return getNonDeclaredImage("connection/foreign_key.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFullVersionNote_image() {
        return getNonDeclaredImage("connection/full_verion_note_icon.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getGoto_image() {
        return getNonDeclaredImage("connection/goto.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFunction_image() {
        return getNonDeclaredImage("connection/function.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFunctionTab_image() {
        return getNonDeclaredImage("connection/function_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHelp_image() {
        return getNonDeclaredImage("connection/help.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHide_image() {
        return getNonDeclaredImage("connection/hide.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHideOption_image() {
        return getNonDeclaredImage("connection/hide_option.png", 32, 32);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHide1_image() {
        return getNonDeclaredImage("connection/hide1.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHistory_image() {
        return getNonDeclaredImage("connection/history.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHistory_big_image() {
        return getNonDeclaredImage("connection/history_big.png", 32, 32);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getHistoryTab_image() {
        return getNonDeclaredImage("connection/history_tab.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getImport_image() {
        return getNonDeclaredImage("connection/import.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getImportConnectionDetails_image() {
        return getNonDeclaredImage("connection/import_con_details.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getImportCsv_image() {
        return getNonDeclaredImage("connection/import_csv.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getIndex_image() {
        return getNonDeclaredImage("connection/index.png");
    }
    
    public Image getIndex_image(int width, int height) {
        return getNonDeclaredImage("connection/index.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getIndexTab_image() {
        return getNonDeclaredImage("connection/index_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getInsert_image() {
        return getNonDeclaredImage("connection/insert.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getInsertFromFile_image() {
        return getNonDeclaredImage("connection/insert_from_file.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getInsertTemplate_image() {
        return getNonDeclaredImage("connection/insert_template.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getKeyboardShortcuts_image() {
        return getNonDeclaredImage("connection/keyboard_shortcuts.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getKeywordTab_image() {
        return getNonDeclaredImage("connection/keyword_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getKill_image() {
        return getNonDeclaredImage("connection/kill.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getKillAll_image() {
        return getNonDeclaredImage("connection/kill_all.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getLanguage_image() {
        return getNonDeclaredImage("connection/language.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getListAllTags_image() {
        return getNonDeclaredImage("connection/list_all_tags.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getListFunctionTags_image() {
        return getNonDeclaredImage("connection/list_function_tags.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getListMatchingTags_image() {
        return getNonDeclaredImage("connection/list_matching_tags.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getLoaderBike_image() {
        return getNonDeclaredImage("connection/loader_bike.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getLoaderScreen_image() {
        return getNonDeclaredImage("connection/loader_screen_time_up_icon.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getLowercase_image() {
        return getNonDeclaredImage("connection/lowercase.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getManage_image() {
        return getNonDeclaredImage("connection/manage.png");
    }
    
    public Image getManage_image(int width, int height) {
        return getNonDeclaredImage("connection/manage.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getModify_image() {
        return getNonDeclaredImage("connection/modify.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getMore_image() {
        return getNonDeclaredImage("connection/more.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNewConnection_image() {
        return getNonDeclaredImage("connection/new_connection.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNewConnection_big_image() {
        return getNonDeclaredImage("connection/new_connection_big.png", 64,64);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNewConnectionUsingCurrent_image() {
        return getNonDeclaredImage("connection/new_connection_using_current.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDataSearch_image() {
        return getNonDeclaredImage("connection/new_data_search.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNewEditor_image() {
        return getNonDeclaredImage("connection/new_editor.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNewEditor_RS_image() {
        return getNonDeclaredImage("connection/new_editor_rs.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSchemaDesigner_image() {
        return getNonDeclaredImage("connection/new_schema_designer.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNewsTab_image() {
        return getNonDeclaredImage("connection/news_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeEvents_image() {
        return getNonDeclaredImage("connection/node_events.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeFunctions_image() {
        return getNonDeclaredImage("connection/node_functions.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeProcs_image() {
        return getNonDeclaredImage("connection/node_procedures.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeTables_image() {
        return getNonDeclaredImage("connection/node_tables.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeTriggers_image() {
        return getNonDeclaredImage("connection/node_triggers.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeViews_image() {
        return getNonDeclaredImage("connection/node_views.png", 16, 16);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeCollapsed_image() {
        return getNonDeclaredImage("connection/node_collapsed.png", 9, 9);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getNodeExpanded_image() {
        return getNonDeclaredImage("connection/node_expanded.png", 9, 9);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOldEditor_image() {
        return getNonDeclaredImage("connection/old_new_editor.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOrganizeFavourite_image() {
        return getNonDeclaredImage("connection/organize_favorite.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOrganizeFavoriteTab_image() {
        return getNonDeclaredImage("connection/organize_favorite_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOsDebian_image() {
        return getNonDeclaredImage("connection/os_debian.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOSMac_image() {
        return getNonDeclaredImage("connection/os_mac.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOsOthers_image() {
        return getNonDeclaredImage("connection/os_others.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOsRedhat_image() {
        return getNonDeclaredImage("connection/os_redhat.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOsSuse_image() {
        return getNonDeclaredImage("connection/os_suse.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOsUbuntu_image() {
        return getNonDeclaredImage("connection/os_ubuntu.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOsWindows_image() {
        return getNonDeclaredImage("connection/os_windows.png", 24, 24);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOthers_image() {
        return getNonDeclaredImage("connection/others.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPaste_image() {
        return getNonDeclaredImage("connection/paste.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPdf_image() {
        return getNonDeclaredImage("connection/pdf.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPlus_image() {
        return getNonDeclaredImage("connection/plus.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPref_0_image() {
        return getNonDeclaredImage("connection/pref0.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPref_1_image() {
        return getNonDeclaredImage("connection/pref1.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPref_2_image() {
        return getNonDeclaredImage("connection/pref2.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPref_3_image() {
        return getNonDeclaredImage("connection/pref3.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPref_4_image() {
        return getNonDeclaredImage("connection/pref4.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPref_5_image() {
        return getNonDeclaredImage("connection/pref5.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPreferences_image() {
        return getNonDeclaredImage("connection/preferences.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPrimaryKey_image() {
        return getNonDeclaredImage("connection/primaryKey.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getProcessList_image() {
        return getNonDeclaredImage("connection/process_list.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getProfileTab_image() {
        return getNonDeclaredImage("connection/profile_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getProfiler_image() {
        return getNonDeclaredImage("connection/profiler.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getQuery_image() {
        return getNonDeclaredImage("connection/query.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getQueryBuilder_image() {
        return getNonDeclaredImage("connection/queryBuilder.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRebuildTags_image() {
        return getNonDeclaredImage("connection/rebuild_tags.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRecentMenu_image() {
        return getNonDeclaredImage("connection/recent_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRedo_image() {
        return getNonDeclaredImage("connection/redo.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRegistration_image() {
        return getNonDeclaredImage("connection/registration.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRenameEvent_image() {
        return getNonDeclaredImage("connection/rename_event.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRenameTab_image() {
        return getNonDeclaredImage("connection/rename_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRenameTrigger_image() {
        return getNonDeclaredImage("connection/rename_trigger.png");
    }
    
    public Image getRenameTrigger_image(int width, int height) {
        return getNonDeclaredImage("connection/rename_trigger.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRenameView_image() {
        return getNonDeclaredImage("connection/rename_view.png");
    }
    
    public Image getRenameView_image(int width, int height) {
        return getNonDeclaredImage("connection/rename_view.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getReplace_image() {
        return getNonDeclaredImage("connection/replace.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getScheduleBackUp_image() {
        return getNonDeclaredImage("connection/schedule_backup.png");
    }
    
    public Image getScheduleBackUp_image(int width, int height) {
        return getNonDeclaredImage("connection/schedule_backup.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getScheduleJobs_image() {
        return getNonDeclaredImage("connection/schedule_jobs.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getScheduleReport_image() {
        return getNonDeclaredImage("connection/schedule_report.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getScheduler_image() {
        return getNonDeclaredImage("connection/scheduler.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSchemaSync_image() {
        return getNonDeclaredImage("connection/schema_sync.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSelect_image() {
        return getNonDeclaredImage("connection/select.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSelectAll_image() {
        return getNonDeclaredImage("connection/select_all.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getServer_image() {
        return getNonDeclaredImage("connection/server.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getServerFilter_image() {
        return getNonDeclaredImage("connection/server_filter.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSessionMenu_image() {
        return getNonDeclaredImage("connection/session_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getShow_image() {
        return getNonDeclaredImage("connection/show.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getShowOption_Image() {
        return getNonDeclaredImage("connection/show_option.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getShowStatus_image() {
        return getNonDeclaredImage("connection/show_status.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getShowVariable_image() {
        return getNonDeclaredImage("connection/show_variable.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSlowAnalyze_image() {
        return getNonDeclaredImage("connection/slow_analyze.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSlowAnalyzeTab_image() {
        return getNonDeclaredImage("connection/slow_analyze_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSql_image() {
        return getNonDeclaredImage("connection/sql.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSqlFormatterMenu_image() {
        return getNonDeclaredImage("connection/sql_formatter_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getStatus_image() {
        return getNonDeclaredImage("connection/status.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getStoredProc_image() {
        return getNonDeclaredImage("connection/stored_procedure.png");
    }
    
    public Image getStoredProc_image(int width, int height) {
        return getNonDeclaredImage("connection/stored_procedure.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getStoredProcTab_image() {
        return getNonDeclaredImage("connection/storedprocs_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSubtract_image() {
        return getNonDeclaredImage("connection/subtract.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSwitchNext_image() {
        return getNonDeclaredImage("connection/switch_next.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSwitchPrev_image() {
        return getNonDeclaredImage("connection/switch_prev.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSync_image() {
        return getNonDeclaredImage("connection/sync.png");
    }
    
    public Image getSync_image(int width, int height) {
        return getNonDeclaredImage("connection/sync.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTable_image() {
        return getNonDeclaredImage("connection/table.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableDiagnostics_image() {
        return getNonDeclaredImage("connection/table_diagnostics.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableDrop_image() {
        return getNonDeclaredImage("connection/table_drop.png");
    }
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDeleteTable_image() {
        return getNonDeclaredImage("connection/delete_table.png");
    }
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getOpen_small_image() {
        return getNonDeclaredImage("connection/open_small.png");
    }
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableInsert_image() {
        return getNonDeclaredImage("connection/insert_table.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableDuplicate_image() {
        return getNonDeclaredImage("connection/table_duplicate.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableExport_image() {
        return getNonDeclaredImage("connection/table_export.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableProperties_image() {
        return getNonDeclaredImage("connection/table_properties.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableRename_image() {
        return getNonDeclaredImage("connection/table_rename.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableTab_image() {
        return getNonDeclaredImage("connection/table_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableTabDisabled_image() {
        return getNonDeclaredImage("connection/table_tab_disabled.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableTruncate_image() {
        return getNonDeclaredImage("connection/table_truncate.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTagsMenu_image() {
        return getNonDeclaredImage("connection/tags_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTile_image() {
        return getNonDeclaredImage("connection/tile.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTop_image() {
        return getNonDeclaredImage("connection/top.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTrigger_image() {
        return getNonDeclaredImage("connection/trigger.png");
    }
    
    public Image getTrigger_image(int width, int height) {
        return getNonDeclaredImage("connection/trigger.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTriggerTab_image() {
        return getNonDeclaredImage("connection/trigger_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTruncate_image() {
        return getNonDeclaredImage("connection/truncate.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getUnComment_image() {
        return getNonDeclaredImage("connection/uncomment.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getUndo_image() {
        return getNonDeclaredImage("connection/undo.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getUpdate_image() {
        return getNonDeclaredImage("connection/update.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getUppercase_image() {
        return getNonDeclaredImage("connection/uppercase.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getUser_image() {
        return getNonDeclaredImage("connection/user.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getAddUser_image() {
        return getNonDeclaredImage("connection/user_add.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getEditUser_image() {
        return getNonDeclaredImage("connection/user_edit.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getGlobalUser_image() {
        return getNonDeclaredImage("connection/user_global.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDeleteUser_image() {
        return getNonDeclaredImage("connection/user_delete.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getVariables_image() {
        return getNonDeclaredImage("connection/variables.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getView_image() {
        return getNonDeclaredImage("connection/view.png");
    }
    
    public Image getView_image(int width, int height) {
        return getNonDeclaredImage("connection/view.png", width, height);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getViewData_image() {
        return getNonDeclaredImage("connection/view_data.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getViewTab_image() {
        return getNonDeclaredImage("connection/view_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getVisualExplainTab_image() {
        return getNonDeclaredImage("connection/visualExplain_tab.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTrash_image() {
        return getNonDeclaredImage("connection/trash.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getXls_image() {
        return getNonDeclaredImage("connection/xls.png");
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFormatAll_image() {
        return getNonDeclaredImage("connection/format_all.png");
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFormatCurrent_image() {
        return getNonDeclaredImage("connection/format_current.png");
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getFormatSelected_image() {
        return getNonDeclaredImage("connection/format_selected.png");
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getTableExport_small_image() {
        return getNonDeclaredImage("connection/table_export_small.png",18,18);
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getQueryBuilder_small_image() {
        return getNonDeclaredImage("connection/query_builder_small.png",24,24);
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getSimpleClose_image() {
        return getNonDeclaredImage("connection/simple-close.png", 12, 12);
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseCopy_image() {
        return getNonDeclaredImage("connection/database_copy.png");
    }
    
    public Image getDatabaseCopy_image(int width, int height) {
        return getNonDeclaredImage("connection/database_copy.png", width, height);
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseCopy_small_image() {
        return getNonDeclaredImage("connection/databae_copy_small.png");
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getDatabaseExternalImport() {
        return getNonDeclaredImage("connection/database_external_import.png");
    }
        
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getPreview_image() {
        return getNonDeclaredImage("connection/preview.png");
    }
        
//    /**
//     * In Connection folder
//     * @return 
//     * image
//     */
//    public Image get() {
//        return getNonDeclaredImage("connection/.png");
//    }
        
    public Image getAdd_user2_image() {
        return getNonDeclaredImage("add-user.png");
    }
    
    public Image getAreaChart_image() {
        return getNonDeclaredImage("areaChart.png");
    }
    
    public Image getAreaChart_small_image() {
        return getNonDeclaredImage("areaChart_small.png");
    }
    
    public Image getEer_Diagram_image() {
        return getNonDeclaredImage("eer_diagram.png");
    }
    
    public Image getEmpty_image() {
        return getNonDeclaredImage("empty.png");
    }
    
    public Image getFilter_image() {
        return getNonDeclaredImage("connection/filter.png");
    }
    
    public Image getFilter_small_image() {
        return getNonDeclaredImage("filter_small.png");
    }
    
    public Image getFilterCancel_image() {
        return getNonDeclaredImage("connection/filter_cancel.png");
    }
     
    public Image getLineChart_image() {
        return getNonDeclaredImage("lineChart.png");
    }
    
    public Image getLineChart_small_image() {
        return getNonDeclaredImage("lineChart_small.png");
    }
    
    public Image getMoreTable_image() {
        return getNonDeclaredImage("more_table.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getRename_image() {
        return getNonDeclaredImage("connection/rename.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getQueryEditor_image() {
        return getNonDeclaredImage("connection/query_editor_add.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getQueryEditor_small_image() {
        return getNonDeclaredImage("connection/query_editor_add_small.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image getExplainMenu_image() {
        return getNonDeclaredImage("connection/explain_menu.png");
    }
    
    /**
     * In Connection folder
     * @return 
     * image
     */
    public Image get_image() {
        return getNonDeclaredImage("connection/.png");
    }
    
    /**
     * In Designer folder
     * @return 
     * image
     */
    public Image getPublishSearch_designer_image() {
        return getNonDeclaredImage("designer/publish_search.png");
    }
    
    /**
     * In Ssh folder
     * @return 
     * image
     */
    public Image getImport_ssh_image(int width, int height) {
        return getNonDeclaredImage("ssh/import.png", width, height);
    }
    
    /**
     * In Ssh folder
     * @return 
     * image
     */
    public Image getExport_ssh_image(int width, int height) {
        return getNonDeclaredImage("ssh/export.png", width, height);
    }
      
    public Image getFullTableScanningSQLQueries_image() {
        return getNonDeclaredImage("reports/full_table_query.png", 18, 18);
    }
    
    public Image getFileSortingSQLQueries_image() {
        return getNonDeclaredImage("reports/file_sort_query.png", 18, 18);
    }
    
    public Image getTempTableCreatingQueries_image() {
        return getNonDeclaredImage("reports/temp_table_query.png", 18, 18);
    }
    
    public Image getSQLQuery95PercentileResTime_image() {
        return getNonDeclaredImage("reports/percentile_query.png", 18, 18);
    }
    
        public Image getSQLStatements_image() {
        return getNonDeclaredImage("reports/sql_statement.png", 18, 18);
    }
//    
//    public Image get() {
//        return getNonDeclaredImage(imageFileName);
//    }
//    
    
    
        
    public Image getExit_image() {
        return getNonDeclaredImage("connection/exit.png", 16, 16);
    }
    
    
    
    
    
}
