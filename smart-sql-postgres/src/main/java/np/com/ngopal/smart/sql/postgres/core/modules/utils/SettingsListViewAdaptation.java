/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.concurrent.Callable;
import javafx.scene.Node;
import javafx.util.Callback;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public interface SettingsListViewAdaptation extends Callback<String, Boolean> {
    
    public Node getCanvas();
    public boolean isIsBoolean();
    public String getValue();
    public boolean isSelected();
    public String getRepresentation();
    
}
