package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class StoredProcedureWithCursorController extends DialogController {
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private TextField procedureNameField;
    @FXML
    private TextField cursorNameField;
    @FXML
    private TextArea cursorQueryField;
    
    @FXML
    private Button okButton;
    
    private DialogResponce responce = DialogResponce.CANCEL;
    
        
    @FXML
    public void okAction(ActionEvent event) {
        responce = DialogResponce.OK_YES;
        getStage().close();
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        responce = DialogResponce.CANCEL;
        getStage().close();
    }
   
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        okButton.disableProperty().bind(Bindings.isEmpty(procedureNameField.textProperty()).or(Bindings.isEmpty(cursorNameField.textProperty())).or(Bindings.isEmpty(cursorQueryField.textProperty())));
        
        EventHandler<KeyEvent> handler = (KeyEvent event) -> {
            if (!okButton.isDisable()) {
                switch (event.getCode()) {

                    case ENTER:
                        okAction(null);
                        break;

                    case ESCAPE:
                        cancelAction(null);
                        break;                    
                }
            }
        };
        
        procedureNameField.setOnKeyPressed(handler);
        cursorNameField.setOnKeyPressed(handler);
        cursorQueryField.setOnKeyPressed(handler);
    }
    
    public DialogResponce getResponce() {
        return responce;
    }
    
    public String getProcedureNameValue() {
        return procedureNameField.getText();
    }
    
    public String getCursorNameValue() {
        return cursorNameField.getText();
    }
    
    public String getCursorQueryValue() {
        return cursorQueryField.getText();
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }

    @Override
    public String getInputValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initDialog(Object... params) {
        if (getController() != null) {
            getUI().getStylesheets().add(getController().getStage().getScene().getStylesheets().get(0));
        }
    }
}

