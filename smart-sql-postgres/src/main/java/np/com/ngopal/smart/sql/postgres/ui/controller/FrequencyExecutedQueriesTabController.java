package np.com.ngopal.smart.sql.ui.controller;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.SQLListener;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.db.HistoryTemplateService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.HistoryTemplate;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class FrequencyExecutedQueriesTabController extends BaseController implements Initializable, SQLListener {

    @Inject
    private SQLLogger logger;
    
    @Inject
    private HistoryTemplateService historyTemplateService;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private PaginatorTableView<HistoryTemplate> historyTable;
    
    @FXML
    private ToolBar header;
    
    @FXML
    private VBox historyBox;
    
    @FXML
    private TextField filterTextField;
    @FXML
    private CheckBox regexCheckBox;
    @FXML
    private CheckBox notMatchCheckBox;
    
    @FXML
    private Button exportTableAsXlsButton;
    
    @FXML
    private Button refreshDataButton;
    
    @FXML
    private Button copyCellButton;
    
    private final ObservableList<HistoryTemplate> currentData = FXCollections.observableArrayList();
    
    private ConnectionSession session;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    private Font currentFont;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void listen(Connection connection, String logMsg, boolean exectedByUser) {
        if (exectedByUser && Objects.equal(session, getController().getSelectedConnectionSession().get())) {
            Recycler.addWorkProc(new WorkProc() {

                List<HistoryTemplate> data;

                @Override
                public void updateUI() {
                    historyTable.setItems(FXCollections.observableArrayList(data));
                    historyTable.getTableView().refresh();
                }

                @Override
                public void run() {
                    data = historyTemplateService.selectAllWithOrder(session.getConnectionParam().getId());
                    callUpdate = true;
                }
            });
        }
    }
    
    public void changeFont(String font) {
        currentFont = FontUtils.parseFXFont(font);
        historyTable.getTableView().refresh();
    }
    
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        
        logger.addListener(this);
        listen(null, null, true);
    }
    
    public void destory() {
        logger.removeListener(this);
    }
    
    
    @FXML
    public void refreshData(ActionEvent event) {
        listen(null, null, true);
    }
    
    
    @FXML
    void exportTableAsXls(ActionEvent event) {
        
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS files", "*.xls"));
        File xlsFile = fc.showSaveDialog(getStage());
        if (xlsFile != null) {
            
            String filename = xlsFile.getAbsolutePath();
            
            ExportUtils.ExportStatusCallback exportCallback = new ExportUtils.ExportStatusCallback() {
                @Override
                public void error(String errorMessage, Throwable th) {
                    DialogHelper.showError(getController(), "Error", errorMessage, th);
                }

                @Override
                public void success() {
                    DialogResponce resp = DialogHelper.showConfirm(getController(), "Information",
                        "Data exported Successfully, Would you like to open file?");

                    if (resp == DialogResponce.OK_YES) {
                        try {
                            Desktop.getDesktop().open(new File(filename));
                        } catch (IOException ex) {
                            Platform.runLater(() -> {
                                DialogHelper.showError(getController(), "Error", "There is an error while opening exported file", ex);
                            });
                        }
                    }
                }
            };


            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
            columns.put("Rank", true);
            columns.put("NoOfExecution's", true);
            columns.put("LastExecutedTime", true);
            columns.put("QueryTemplate", true);
            
            
            ObservableList<ObservableList> rowsData = FXCollections.observableArrayList();
            for (HistoryTemplate ht: historyTable.getFilteredItems()) {
                
                ObservableList row = FXCollections.observableArrayList();
                row.add(ht.getRank());
                row.add(ht.getNoOfOccurrence());       
                row.add(ht.getRecentlyExecutedString());
                row.add(ht.getQueryTemplate());
                
                rowsData.add(row);
            }            

            ExportUtils.exportXLS(filename, rowsData, columns, null, null, "UTF-8", exportCallback);
        }
    }
    
    public void copyCell(ActionEvent avent) {
        HistoryTemplate row = historyTable.getSelectionModel().getSelectedItem();
        if (row != null) {
            
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            
            List<TablePosition> list = historyTable.getSelectionModel().getSelectedCells();
            if (!list.isEmpty()) {
                
                int column = list.get(0).getColumn();
                switch (column) {
                    case 0:
                        content.putString("" + row.getRank());
                        break;
                        
                    case 1:
                        content.putString(row.getNoOfOccurrence() != null ? row.getNoOfOccurrence().toString() : "");
                        break;
                        
                    case 2:
                        content.putString(row.getRecentlyExecutedString());
                        break;

                    default:
                        content.putString(row.getQueryTemplate() != null ? row.getQueryTemplate() : "");
                        break;
                }

                clipboard.setContent(content);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ((ImageView)copyCellButton.getGraphic()).setImage(SmartImageProvider.getInstance().getCopy_image());
        ((ImageView)refreshDataButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportTableAsXlsButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXls_export_image());
        
        historyTable.setItems(currentData);
        historyTable.getSelectionModel().setCellSelectionEnabled(true);        
        
        historyTable.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if (event.isControlDown() && event.getCode() == KeyCode.C) {
                copyCell(null);
                event.consume();
            }
        });
        
        copyCellButton.disableProperty().bind(Bindings.isNull(historyTable.getSelectionModel().selectedItemProperty()));
        
        HBox paginatorBox = historyTable.removePaginator();
        header.getItems().add(paginatorBox);
        
        TableColumn<HistoryTemplate, Integer> rankColum = new TableColumn<>("Rank");
        rankColum.setCellValueFactory(new PropertyValueFactory<>("rank"));
        rankColum.setStyle( "-fx-alignment: center-right;");
        rankColum.setCellFactory(tc -> {
            TableCell<HistoryTemplate, Integer> cell = new TableCell<HistoryTemplate, Integer>() {

                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText("");
                    } else {
                        setText(item != null ? item.toString() : "");
                        setFont(currentFont != null ? currentFont : Font.getDefault());
                    }
                }
            };

            cell.getStyleClass().add("numeric");
            return cell ;
        });
                
        TableColumn<HistoryTemplate, Integer> noOfOccurenceColum = new TableColumn<>("NoOfExecution/s");
        noOfOccurenceColum.setCellValueFactory(new PropertyValueFactory<>("noOfOccurrence"));
        noOfOccurenceColum.setStyle( "-fx-alignment: center-right;");
        noOfOccurenceColum.setCellFactory(tc -> {
            TableCell<HistoryTemplate, Integer> cell = new TableCell<HistoryTemplate, Integer>() {

                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText("");
                    } else {
                        setText(item != null ? item.toString() : "");
                        setFont(currentFont != null ? currentFont : Font.getDefault());
                    }
                }
            };

            cell.getStyleClass().add("numeric");
            return cell ;
        });
        
        TableColumn<HistoryTemplate, String> lastExecutedTimeColumn = new TableColumn<>("LastExecutedTime");
        lastExecutedTimeColumn.setCellValueFactory(new PropertyValueFactory<>("recentlyExecutedString"));
        lastExecutedTimeColumn.setCellFactory(tc -> {
            TableCell<HistoryTemplate, String> cell = new TableCell<HistoryTemplate, String>() {

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText("");
                    } else {
                        setText(item != null ? item : "");
                        setFont(currentFont != null ? currentFont : Font.getDefault());
                    }
                }
            };

            cell.getStyleClass().add("numeric");
            return cell ;
        });
        
        TableColumn<HistoryTemplate, String> queryTemplateColumn = new TableColumn<>("QueryTemplate");
        queryTemplateColumn.setCellValueFactory(new PropertyValueFactory<>("queryTemplate"));
        queryTemplateColumn.setCellFactory(tc -> {
            TableCell<HistoryTemplate, String> cell = new TableCell<HistoryTemplate, String>() {

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText("");
                    } else {
                        setText(item != null ? item : "");
                        setFont(currentFont != null ? currentFont : Font.getDefault());
                    }
                }
            };

            cell.getStyleClass().add("numeric");
            return cell ;
        });
        
        historyTable.getColumns().addAll(rankColum, noOfOccurenceColum, lastExecutedTimeColumn, queryTemplateColumn);
        
        
        rankColum.setMaxWidth( 1f * Integer.MAX_VALUE * 5 ); // 5% width
        rankColum.setPrefWidth(50);
        
        noOfOccurenceColum.setMaxWidth( 1f * Integer.MAX_VALUE * 12 ); // 5% width
        noOfOccurenceColum.setPrefWidth(80);
        
        lastExecutedTimeColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 15 ); // 10% width
        lastExecutedTimeColumn.setPrefWidth(100);
        
        queryTemplateColumn.setMaxWidth( 1f * Integer.MAX_VALUE * 68 );
        
                
        filterTextField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            historyTable.setPredicate(this::acceptFilter);
        });
        
        regexCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            historyTable.setPredicate(this::acceptFilter);
        });
        
        notMatchCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            historyTable.setPredicate(this::acceptFilter);
        });
        
               
        ImageView img = new ImageView(SmartImageProvider.getInstance().getCopy_image());
        img.setFitHeight(16);
        img.setFitWidth(16);
        img.setPickOnBounds(true);
        
        MenuItem copyItem = new MenuItem("Copy", img);
        copyItem.setAccelerator(KeyCombination.keyCombination("Ctrl+C"));
        copyItem.setOnAction((ActionEvent t) -> {
            copyCell(null);
        });
        
        MenuItem copyRowItem = new MenuItem("Copy Row");
        copyRowItem.setOnAction((ActionEvent t) -> {
            
            HistoryTemplate row = historyTable.getSelectionModel().getSelectedItem();            
            String text = row.getPresentation();
            
            if (!text.isEmpty()) {
                final Clipboard clipboard = Clipboard.getSystemClipboard();
                final ClipboardContent content = new ClipboardContent();
                content.putString(text);
                clipboard.setContent(content);
            }
        });
        
        MenuItem copyAllItem = new MenuItem("Copy All");
        copyAllItem.setOnAction((ActionEvent t) -> {
            String text = "";
            for (HistoryTemplate row: historyTable.getItems()) {
                text += row.getPresentation();
            }
            
            if (!text.isEmpty()) {
                final Clipboard clipboard = Clipboard.getSystemClipboard();
                final ClipboardContent content = new ClipboardContent();
                content.putString(text);
                clipboard.setContent(content);
            }
        });
        
        MenuItem saveItem = new MenuItem("Save Frequency Executed Queries To File...");
        saveItem.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        saveItem.setOnAction((ActionEvent t) -> {            
            String text = "";
            for (HistoryTemplate row: historyTable.getItems()) {
                text += row.getPresentation() + "\n";
            }
            
            if (!text.isEmpty()) {
                FileChooser fc = new FileChooser();
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("SQL Files", "*.sql"),
                        new FileChooser.ExtensionFilter("All Files", "*.*"));
                
                File file = fc.showSaveDialog(getStage());
                if (file != null) {
                    try {
                        Files.write(file.toPath(), text.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                    } catch (Throwable ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        
        
        HBox b = new HBox(5);
        b.setStyle("-fx-padding: 3 3 0 3");

        TextField searchField = new TextField();
        searchField.setOnAction((ActionEvent event) -> {
            findNext(historyTable, searchField);
        });
        searchField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                searchField.setText("");
                historyBox.getChildren().remove(b);
            } else if (event.getCode() == KeyCode.F3) {
                findNext(historyTable, searchField);
                event.consume();
            }
        });
        
        Button next = new Button("Find");
        next.setPrefWidth(50);
        next.setOnAction((ActionEvent event) -> {
            findNext(historyTable, searchField);
        });
        
        Button close = new Button("Close");
        close.setPrefWidth(50);
        close.setOnAction((ActionEvent event) -> {
            searchField.setText("");
            historyBox.getChildren().remove(b);
        });
        
        b.getChildren().addAll(searchField, close, next);
        
        searchField.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(searchField, Priority.ALWAYS);
        

        MenuItem findItem = new MenuItem("Find...", new ImageView(SmartImageProvider.getInstance().getPreview_image()));
        findItem.setAccelerator(KeyCombination.keyCombination("Ctrl+F"));
        findItem.setOnAction((ActionEvent t) -> {
            showFind(historyBox, b, searchField);
        });
        
        MenuItem findNextItem = new MenuItem("Find Next");
        findNextItem.setAccelerator(KeyCombination.keyCombination("F3"));
        findNextItem.setOnAction((ActionEvent t) -> {
            findNext(historyTable, searchField);
        });
        
        ContextMenu menu = new ContextMenu();
        menu.getItems().add(copyItem);
        menu.getItems().add(copyRowItem);
        menu.getItems().add(copyAllItem);
        menu.getItems().add(saveItem);
        menu.getItems().add(new SeparatorMenuItem());
        menu.getItems().add(findItem);
        menu.getItems().add(findNextItem);
        
        historyTable.getTableView().setOnMouseClicked((MouseEvent mouse) -> {
            if (mouse.getButton() == MouseButton.SECONDARY) {
                menu.show(getStage(), mouse.getScreenX(), mouse.getScreenY());
            } else {
                menu.hide();
            }
        });
        
        historyTable.getTableView().setOnKeyPressed((KeyEvent event) -> {
            if (event.isControlDown() && event.getCode() == KeyCode.F) {
                showFind(historyBox, b, searchField);
                event.consume();
            } else if (event.getCode() == KeyCode.F3) {
                findNext(historyTable, searchField);
                event.consume();
            }
        });
        
        changeFont(preferenceService.getPreference().getFontsHistoryInfo());
    }
    
    
    private boolean acceptFilter(HistoryTemplate row) {
        String filterCondition = filterTextField.getText();
        if (!filterCondition.isEmpty()) {
            
            try {
                Pattern pattern = regexCheckBox.isSelected() ? Pattern.compile(filterCondition) : null;
                
                boolean result = false;
                
                for (String filterValue: row.getValuesFoFilter()) {

                    if (regexCheckBox.isSelected() && pattern != null) {
                        // if regex - need check using regex
                        result = pattern.matcher(filterValue).find();
                    } else {
                        // else just
                        result = filterValue.toLowerCase().contains(filterCondition.toLowerCase());
                    }

                    if (result) {
                        break;
                    }
                }

                if (notMatchCheckBox.isSelected()) {
                    result = !result;
                }

                return result;
            } catch (Throwable th) {
                // my be some errors in pattern compile - its ok just ignore and give user write full regex
            }
        }
        
        return true;
    }
    
    private void showFind(VBox historyBox, HBox searchContainer, TextField searchField) {
        
        if (!historyBox.getChildren().contains(searchContainer)) {            
            // last founded index
            searchField.setUserData(-1);
            
            historyBox.getChildren().add(0, searchContainer);            
        }
        
        Platform.runLater(() -> {
            searchField.requestFocus();
        });
    }
    
    private void findNext(PaginatorTableView<HistoryTemplate> tableView, TextField searchField) {
        int index = searchField.getUserData() != null ? (Integer)searchField.getUserData() : 0;
        if (index >= tableView.getItems().size()) {
            index = -1;
        }
        
        index++;
        for (int i = index; i < tableView.getItems().size(); i++) {
            HistoryTemplate row = tableView.getItems().get(i);
            if (row.getPresentation().toLowerCase().contains(searchField.getText().toLowerCase())) {
                Platform.runLater(() -> {
                    tableView.getSelectionModel().select(row);
                    tableView.scrollTo(row);
                });
                searchField.setUserData(i);
                return;
            }
        }
        
        // if not found try found from start
        searchField.setUserData(-1);
        index = 0;
        for (int i = index; i < tableView.getItems().size(); i++) {
            HistoryTemplate row = tableView.getItems().get(i);
            if (row.getPresentation().toLowerCase().contains(searchField.getText().toLowerCase())) {
                Platform.runLater(() -> {
                    tableView.getSelectionModel().select(row);
                    tableView.scrollTo(row);
                });
                searchField.setUserData(i);
                return;
            }
        }
        
        
    }
    
}
