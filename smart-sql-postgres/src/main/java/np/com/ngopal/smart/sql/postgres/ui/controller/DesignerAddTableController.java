package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle; 
import javafx.util.Callback;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.DBElementWrapperOnBuilder;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class DesignerAddTableController  extends BaseController implements Initializable {
    
    @FXML
    private Label dbName;
    
    @FXML
    private CheckBox selectdeselectAllCheckbox;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Button cancelButton;
    
    @FXML
    private Button add;
    
    @FXML
    private ListView<Holder> listview;
    
    public ArrayList<DBElementWrapperOnBuilder> tableau = new ArrayList();
    
    private class Holder{
        DBTable table;
        SimpleBooleanProperty selected = 
                new SimpleBooleanProperty(false);
        
        {
            selectdeselectAllCheckbox.selectedProperty().addListener(
                    new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean>
                        ov, Boolean t, Boolean t1) { 
                    selected.setValue(t1);                    
                }
            });
            
        }
    }
    
    

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        add.setOnAction((ActionEvent t) -> {
            for(Holder h : listview.getItems()){
                if(h.selected.get()){
                    tableau.add(new DBElementWrapperOnBuilder(0, 0, h.table));
                }
            }
            cancelButton.fire();
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                    setAddRequest.run();
                }

                @Override
                public void run() {
                    callUpdate = true;
                }
            });
        });
        
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialog.hide();
            }
        });
        
         listview.setCellFactory(new Callback<ListView<Holder>,
                ListCell<Holder>>() {
            @Override
            public ListCell<Holder> call(ListView<Holder> p) { 
                return new CheckBoxListCell<Holder>(new Callback<Holder,
                        ObservableValue<Boolean>>() {
                    @Override
                    public ObservableValue<Boolean> call(Holder p) {
                        return p.selected;
                    }
                },new StringConverter<Holder>() {
                    @Override
                    public String toString(Holder t) { 
                      return t.table.getName();
                    }

                    @Override
                    public Holder fromString(String string) { 
                        return null;
                    }
                });
            }
        });       
    }
    
    Stage dialog;
    public void init(String databaseName, Collection<DBTable> list, ConnectionSession session){
        this.session = session;
        dbName.setText(databaseName);
        for(DBTable tab : list){
            Holder hold = new Holder();
            hold.table = tab;
            listview.getItems().add(hold);        
        }
        if (dialog == null) {
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setResizable(false);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Add Tables");
            dialog.setScene(new Scene(getUI()));
        }
        tableau.clear();
        dialog.showAndWait();
    }
    
    private ConnectionSession session;    
    public Runnable setAddRequest;
    
}
