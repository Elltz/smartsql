/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import np.com.ngopal.smart.sql.core.modules.utils.SessionKey;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class SessionContainerController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private CheckBox selectorIndicator;
    
    @FXML
    private Label sessionName;
    
    @FXML
    private Label sessionStatus;
    
    @FXML
    private ImageView sessionStatusIcon;

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    public static SimpleObjectProperty<SessionContainerController>
            whosSelected = new SimpleObjectProperty();
    
    static {
        whosSelected.addListener(new ChangeListener<SessionContainerController>() {

            @Override
            public void changed(ObservableValue<? extends SessionContainerController> observable,
                    SessionContainerController oldValue, SessionContainerController newValue) { 
                System.out.println("change listener for sessionContainerController");
                if (oldValue != null) {
                    oldValue.toggleSelector(false);
                }
                if (newValue != null) {
                    newValue.toggleSelector(true);
                }
            }
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        getUI().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (selectorIndicator.isSelected()) {
                    whosSelected.set(null);
                } else {
                    SessionContainerController.whosSelected.set(
                            SessionContainerController.this);                    
                }
            }
        });
    }
    
    private void toggleSelector(boolean t){
        selectorIndicator.setSelected(t);
    }
    
    private Image red = new Image("/np/com/ngopal/smart/sql/ui/images/rating_red.png"),
            yellow = new Image("/np/com/ngopal/smart/sql/ui/images/rating_yellow.png"),
            green = new Image("/np/com/ngopal/smart/sql/ui/images/rating_green.png");
    
    public void addSessionSavePoint(SessionKey ssp){
        this.sk = ssp;
        refereshFields();
    }
    
    public void refereshFields(){
        sessionName.setText(sk.getName());
        if(sk.getStatus() == SessionKey.STATUS_INTERRUPTED){
            sessionStatus.setText("SESSION WAS INTERRUPTED");
            sessionStatusIcon.setImage(red);
        }else if(sk.getStatus() == SessionKey.STATUS_SYSTEM_SAVED){
            sessionStatus.setText("SESSION SAVED BY APP");
            sessionStatusIcon.setImage(yellow);
        }else if(sk.getStatus() == SessionKey.STATUS_USER_SAVED){
            sessionStatus.setText("SESSION SAVED");
            sessionStatusIcon.setImage(green);
        }else{
            sessionStatus.setText("SESSION CREATED");
            sessionStatusIcon.setImage(null);
        }
    }
    
    private SessionKey sk;
    
    public SessionKey getSessionsSavePoint(){
        return sk;
    }
    
}
