/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.MessagesTabModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class FlushController extends BaseController implements Initializable{
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private ListView listview;
   
    private CheckBox statusCheckbox = new CheckBox("STATUS"); 
    
    private  CheckBox tableWithReadLockCheckbox = new CheckBox("TABLE WITH READ LOCK"); 
    
    @FXML
    private Button flushButton;
    
    private CheckBox hostCheckbox = new CheckBox("HOSTS");

    private CheckBox privCheckbox= new CheckBox("PRIVILEGES");

    private CheckBox UseNoWriteToBinlogCheckbox= new CheckBox("USE NO_WRITE_TO_BIN_LOG");

    private CheckBox UserResourcesCheckbox= new CheckBox("USER_RESOURCES");

    private CheckBox tablesCheckbox= new CheckBox("TABLES");

    private CheckBox logsCheckbox= new CheckBox("LOGS");

    private CheckBox queryCacheCheckbox= new CheckBox("QUERY CACHE");

    @FXML
    private Button cancelButton;

    private CheckBox allCheckbox = new CheckBox("ALL OPTIONS");

    private CheckBox DesKeyFileCheckbox= new CheckBox("DES_KEY_FILE");
    
    private  ArrayList<String> stlist = new ArrayList();

    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    private ChangeListener<Boolean> selListener
            = new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov,
                        Boolean t, Boolean t1) {
                    if (ov instanceof BooleanProperty) {
                        BooleanProperty bp = (BooleanProperty) ov;
                        if (t1) {
                            CheckBox cb = (CheckBox) bp.getBean();
                            if(cb == allCheckbox){
                                stlist.clear();
                            }
                            stlist.add(cb.getText());
                        } else {
                            stlist.remove(((CheckBox) bp.getBean()).getText());
                        }
                    }
                }
            };

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        DesKeyFileCheckbox.selectedProperty().addListener(selListener);
        UseNoWriteToBinlogCheckbox.selectedProperty().addListener(selListener);
        UserResourcesCheckbox.selectedProperty().addListener(selListener);
        hostCheckbox.selectedProperty().addListener(selListener);
        logsCheckbox.selectedProperty().addListener(selListener);
        tablesCheckbox.selectedProperty().addListener(selListener);
        tableWithReadLockCheckbox.selectedProperty().addListener(selListener);
        statusCheckbox.selectedProperty().addListener(selListener);
        privCheckbox.selectedProperty().addListener(selListener);
        queryCacheCheckbox.selectedProperty().addListener(selListener);
        allCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            DesKeyFileCheckbox.setSelected(newValue);
            UserResourcesCheckbox.setSelected(newValue);
            hostCheckbox.setSelected(newValue);
            logsCheckbox.setSelected(newValue);
            privCheckbox.setSelected(newValue);
            queryCacheCheckbox.setSelected(newValue);
            statusCheckbox.setSelected(newValue);
            tablesCheckbox.setSelected(newValue);
            tableWithReadLockCheckbox.setSelected(newValue);
        });
        ///////////////////////////////////////////////////////////////////////
        DesKeyFileCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
//        UseNoWriteToBinlogCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        UserResourcesCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        hostCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        logsCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        tablesCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        tableWithReadLockCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        statusCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        privCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
        queryCacheCheckbox.disableProperty().bind(allCheckbox.selectedProperty());
                
        flushButton.disableProperty().bind(
                (DesKeyFileCheckbox.selectedProperty()
            .or(UserResourcesCheckbox.selectedProperty()).or(hostCheckbox.selectedProperty())
            .or(logsCheckbox.selectedProperty()).or(tablesCheckbox.selectedProperty())
            .or(tableWithReadLockCheckbox.selectedProperty()).or(statusCheckbox.selectedProperty())
            .or(privCheckbox.selectedProperty().or(queryCacheCheckbox.selectedProperty())
            .or(allCheckbox.selectedProperty()))).not());
        
        flushButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) { 
                
                ConnectionSession session = getController().getSelectedConnectionSession().get();
                
                getController().addBackgroundWork(new WorkProc() {
                    
                    SQLException ex = null;
                    String res = "";
                   
                    @Override
                    public void updateUI() { 
                        Tab tab = new Tab("FLUSH RESULTS");
                        tab.setGraphic(new ImageView(StandardDefaultImageProvider.getInstance().getMessageInformation_image()));
                        TextArea ta = new TextArea();
                        ta.setFont(Font.font("Monospace"));
                        if(ex != null){
                          ta.setText(ex.getMessage());
                        }else{
                           ta.setText("FLUSH OPTION :\n\r" +res+ 
                                   "\n\r successfully flushed");
                        }
                        tab.setContent(ta);
                        ((MessagesTabModule)((MainController)getController()).getModuleByClassName(
                                MessagesTabModule.class.getName())).addToMessageSector(session, tab, true);
                    }

                    @Override
                    public void run() {
                        boolean isLocal = true,hasTable = false,hasLock = false;
                        for(String s :stlist){
                            System.out.println(s);
                            if(s.equals(UseNoWriteToBinlogCheckbox.getText())){
                                isLocal = false;
                            }else if(s.equals(tablesCheckbox.getText())){
                                hasTable = true;
                            }else if(s.equals(tableWithReadLockCheckbox.getText())){
                                hasTable = true;
                                hasLock = true;
                            }
                        }
                        
                        StringBuilder sb = new StringBuilder();
                        if (!isLocal) {
                            stlist.remove(UseNoWriteToBinlogCheckbox.getText());
                        }
                        if(hasTable){
                            stlist.remove(tablesCheckbox.getText());
                            stlist.remove(tableWithReadLockCheckbox.getText());
                            sb.append("FLUSH ");
                            if (isLocal) {
                                sb.append("LOCAL ");
                            } else {                                
                                sb.append(UseNoWriteToBinlogCheckbox.getText());
                                sb.append(" ");
                            }
                            if (hasLock) {
                                sb.append(tableWithReadLockCheckbox.getText());
                                sb.append(";");
                            } else {
                                sb.append("TABLES;");
                            }                            
                            sb.append("\n\r");
                        }
                        if (!stlist.isEmpty()) {
                            sb.append("FLUSH ");
                            if (isLocal) {
                                sb.append("LOCAL ");
                            } else {                                
                                sb.append(UseNoWriteToBinlogCheckbox.getText());
                            }

                            for (int i = 0; i < stlist.size(); i++) {
                                sb.append(stlist.get(i));
                                if (i + 1 == stlist.size()) {
                                    sb.append(";");
                                } else {
                                    sb.append(", ");
                                }
                            }
                        }
                        
                        res = sb.toString();
                        System.out.println(res);
                        try {
                            getController().getSelectedConnectionSession()
                                    .get().getService().execute(res);
                            sb.delete(0, sb.length());
                            for (int i = 0; i < stlist.size(); i++) {
                                sb.append(stlist.get(i));
                                if (i + 1 == stlist.size()) {
                                    res = sb.toString();
                                } else {
                                    sb.append(", ");
                                }
                            }                        
                        } catch (SQLException e) {ex = e;}
                        callUpdate = true;
                    }
                });
                cancelButton.fire();
            }
        });    
        
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                dialog.hide();
            }
        });
        
        listview.getItems().addAll("GENERAL",allCheckbox,
                "LOG OPTION",UseNoWriteToBinlogCheckbox,"FLUSH OPTIONS"
        ,DesKeyFileCheckbox,UserResourcesCheckbox,hostCheckbox,
        logsCheckbox,privCheckbox,queryCacheCheckbox,statusCheckbox,
        tablesCheckbox,tableWithReadLockCheckbox);
        
        listview.setCellFactory(new Callback<ListView, ListCell>() {

            @Override
            public ListCell call(ListView p) { 
                return new ListCell(){

                    @Override
                    public void updateSelected(boolean bln) {
                        super.updateSelected(bln);
                    }

                    @Override
                    protected void updateItem(Object t, boolean bln) {
                        super.updateItem(t, bln);
                        if(bln){
                            setContentDisplay(ContentDisplay.TEXT_ONLY);
                                setText("");
                        }else{
                            if(t instanceof String){
                                setStyle("-fx-background-color: #e1ebf9;");
                                setContentDisplay(ContentDisplay.TEXT_ONLY);
                                setText(t.toString());                                
                            }else{
                                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                CheckBox cb = (CheckBox)t;
                                cb.setTextFill(Color.BLACK);
                                setGraphic(cb);
                                setStyle("-fx-background-color: white;");
                            }
                        }                        
                    }
                    
                };
            }
        });
        
    }
    
    private Stage dialog;
    
    public void init(){
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(400);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Flush");
        dialog.setScene(new Scene(getUI()));        
        dialog.showAndWait();
    }
    
}
