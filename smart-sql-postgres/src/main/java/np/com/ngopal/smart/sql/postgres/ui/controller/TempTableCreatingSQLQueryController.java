/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.control.TableColumn;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.models.FilterableData;
import np.com.ngopal.smart.sql.structure.models.TemTableCreatingSQLQueryData;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class TempTableCreatingSQLQueryController extends SQLStatementsController {

    @Override
    public String getName() {
        return "Temp Table Creating Queries";
    }

    @Override
    public List<? extends FilterableData> getData() {
        String query = "SELECT\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`DIGEST_TEXT`                                                                                                                                                 AS `query`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SCHEMA_NAME`                                                                                                                                                 AS `db`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`                                                                                                                                                  AS `exec_count`,\n"
                + "   (`performance_schema`.`events_statements_summary_by_digest`.`SUM_TIMER_WAIT`)                                                                                                                                              AS `total_latency`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_TABLES`                                                                                                                                      AS `memory_tmp_tables`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_DISK_TABLES`                                                                                                                                 AS `disk_tmp_tables`,\n"
                + "  round(ifnull((`performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_TABLES` / nullif(`performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`, 0)), 0), 0)                          AS `avg_tmp_tables_per_query`,\n"
                + "  round((ifnull((`performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_DISK_TABLES` / nullif(`performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_TABLES`, 0)), 0) * 100), 0) AS `tmp_tables_to_disk_pct`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`FIRST_SEEN`                                                                                                                                                  AS `first_seen`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`LAST_SEEN`                                                                                                                                                   AS `last_seen`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`DIGEST`                                                                                                                                                      AS `digest`\n"
                + "FROM\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`\n"
                + "WHERE\n"
                + "  (`performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_TABLES` > 0)\n"
                + "    and ( performance_schema.events_statements_summary_by_digest.SCHEMA_NAME not in ('mysql','sys','information_schema','performance_schema') )\n"
                + "ORDER BY\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_DISK_TABLES` desc,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_CREATED_TMP_TABLES` DESC;";

        List<TemTableCreatingSQLQueryData> data = new ArrayList(100);
        try {
            ResultSet rs = (ResultSet) getController().getSelectedConnectionSession().get().getService().execute(query);
            while (rs.next()) {
                data.add(new TemTableCreatingSQLQueryData(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                ));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        TableColumn queryColumn = createColumn("Query", "query");
        TableColumn dbColumn = createColumn("DB", "db");
        TableColumn execCountColumn = createColumn("Exec Count", "exec_count");
        TableColumn totalLatencyColumn = createColumn("Total latency", "total_latency");
        TableColumn memoryTmpTablesColumn = createColumn("Temp Memory Tables", "memory_tmp_tables");
        TableColumn diskTmpTablesColumn = createColumn("Temp Disk Tables", "disk_tmp_tables");
        TableColumn avgTmpTablesPerQueryColumn = createColumn("Avg Temp Tables/Query", "avg_tmp_tables_per_query");
        TableColumn tmpTablesToDiskPctColumn = createColumn("Temp Tables To Disk Pct", "tmp_tables_to_disk_pct");
        TableColumn firstSeenColumn = createColumn("First Seen", "first_seen");
        TableColumn lastSeenColumn = createColumn("Last Seen", "last_seen");
        TableColumn digestColumn = createColumn("Digest", "digest");
        
        tableView.setSearchEnabled(true);        
        tableView.addTableColumn(queryColumn, 170, 170, false);
        tableView.addTableColumn(dbColumn, 100, 100, false);
        tableView.addTableColumn(execCountColumn, 100, 100, false);
        tableView.addTableColumn(totalLatencyColumn, 100, 100, false);
        tableView.addTableColumn(memoryTmpTablesColumn, 100, 100, false);
        tableView.addTableColumn(diskTmpTablesColumn, 100, 100, false);
        tableView.addTableColumn(avgTmpTablesPerQueryColumn, 100, 100, false);
        tableView.addTableColumn(tmpTablesToDiskPctColumn, 100, 100, false);
        tableView.addTableColumn(firstSeenColumn, 100, 100, false);
        tableView.addTableColumn(lastSeenColumn, 100, 100, false);
        tableView.addTableColumn(digestColumn, 100, 100, false);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
}
