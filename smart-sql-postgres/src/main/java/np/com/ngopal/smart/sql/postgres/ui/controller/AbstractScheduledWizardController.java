/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javax.mail.AuthenticationFailedException;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.WindowsScheduleUtil;
import np.com.ngopal.smart.sql.core.modules.utils.EncryptiorValue;
import np.com.ngopal.smart.sql.core.modules.utils.RunFrequency;
import np.com.ngopal.smart.sql.core.modules.utils.WeeklyValue;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Schedule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.utils.EmailUtils;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public abstract class AbstractScheduledWizardController<T extends Schedule> extends BaseController implements Initializable {

    @Inject
    ConnectionParamService connectionParamService;           
        
    static final SimpleDateFormat DATETIME_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
    static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyy-MM-dd");
    
    T schedule;
    
    DBService dbService;
    
    @FXML
    AnchorPane mainUI;
    
    int currentWizardIndex;
        
    @FXML
    Button backButton;
    
    @FXML
    Button nextButton;

    @FXML
    RadioButton newJobRadioButton;
    
    @FXML
    TextField jobNameField;
    
    @FXML
    ComboBox<T> savedJobsComboBox;
    
    @FXML
    Button removeSchedultButton;
    
    @FXML
    ComboBox<T> connectionsComboBox;
    @FXML
    ComboBox<Database> databaseComboBox;
    
    @FXML
    TextField user;
    
    @FXML
    TextField address;
    
    @FXML
    PasswordField pass;
    
    @FXML
    TextField port;
    
    @FXML
    TextField sshHost;
    
    @FXML
    TextField sshPort;
    
    @FXML
    TextField sshUser;
    
    @FXML
    TextField sshPassword;
    
    @FXML
    CheckBox useCompressedProtocol;
    
    @FXML
    RadioButton sessionIdleTimeout;
    
    @FXML
    TextField sessionIdleTimeoutValue;
    
    @FXML
    CheckBox runImmediatly;
    
    @FXML
    CheckBox useWindowsSchedule;
    
    @FXML
    ComboBox<RunFrequency> runFrequency;  
    
    @FXML
    Spinner<Integer> hourSpinner;
    
    @FXML
    Spinner<Integer> minutesSpinner;
    
    @FXML
    FlowPane weeklyBox;
    
    @FXML
    CheckBox monCheckBox;
    
    @FXML
    CheckBox tueCheckBox;
    
    @FXML
    CheckBox wedCheckBox;
    
    @FXML
    CheckBox thuCheckBox;
    
    @FXML
    CheckBox friCheckBox;
    
    @FXML
    CheckBox satCheckBox;
    
    @FXML
    CheckBox sunCheckBox;
        
    @FXML
    TextField logFile;
    
    @FXML
    TextField emailFromName;
    
    @FXML
    TextField emailFromEmail;
    
    @FXML
    TextField emailReplyEmail;
    
    @FXML
    TextField emailToEmail;
    
    @FXML
    TextField emailCc;
    
    @FXML
    TextField emailBcc;
    
    @FXML
    TextField emailHost;
    @FXML
    TextField emailPort;
    @FXML
    ComboBox<EncryptiorValue> emailEncryptior;
    @FXML
    CheckBox emailAuth;
    @FXML
    Label emailAccountLabel;
    @FXML
    Label emailPsswdLabel;
    @FXML
    TextField emailAccountName;
    @FXML
    PasswordField emailPasswd;
    
    @FXML
    Button testEmailButton;
    
    protected List<AnchorPane> wizardPanes;

    private ConnectionParams currentConnectionParams;
    
    
    
    
    abstract List<T> loadSchedulers();
    
    abstract void deleteSchedule(T schedule);
    
    abstract boolean checkNextAction();
    abstract void checkBackAction();
    
    abstract boolean isNeedSendEmail();
    
    abstract void updateAllComponentsUsingSchedule(T schedule);
    
    abstract void fillWizardPanes(List<AnchorPane> wizardPanes);
    
    abstract T createSchedule();
    abstract T createScheduleFromConnectionParams(ConnectionParams params);
    
    abstract void fillScheduleCustom();
    abstract void finishAction();
    
    Database initDatabase;
    
    public void init(DBService dbService, ConnectionParams connectionParam, Database database) throws SQLException {
        
        this.dbService = dbService;
        currentConnectionParams = connectionParam;
        if (currentConnectionParams != null) {
            for (T sp: connectionsComboBox.getItems()) {
                if (sp.getParams().getId().equals(currentConnectionParams.getId())) {
                    connectionsComboBox.getSelectionModel().select(sp);
                    break;
                }
            }
        }
        
        savedJobsComboBox.setItems(FXCollections.observableList(loadSchedulers()));
        
        this.initDatabase = database;
                
        runFrequency.setItems(FXCollections.observableArrayList(RunFrequency.values()));
        runFrequency.getSelectionModel().select(0);
        
        String email = MainUI.getMainController().getSmartData().getEmailId();
        if (email != null) {
            emailFromEmail.setText(email);
            emailReplyEmail.setText(email);
            emailAccountName.setText(email);
        }
        
        String name = System.getProperty("user.name");
        if (name == null || name.isEmpty()) {
            name = email;
        }
        
        if (name != null) {
            emailFromName.setText(name);
        }
    }

    @FXML
    void emailTestSettings(ActionEvent event) {
        
        mainUI.setCursor(Cursor.WAIT);
        mainUI.setDisable(true);
        
        Thread thread = new Thread(() -> {
            try {
                EmailUtils.sendEmail(emailFromName.getText(), emailFromEmail.getText(), emailReplyEmail.getText(),
                        emailToEmail.getText(), emailCc.getText().isEmpty() ? null : emailCc.getText(), emailBcc.getText().isEmpty() ? null : emailBcc.getText(), 
                        emailHost.getText(), emailPort.getText(), emailEncryptior.getSelectionModel().getSelectedItem().id, 
                        emailAccountName.getText(), emailPasswd.getText(),
                        "SmartSQL Test Message", "This is an e-mail message sent automatically by SmartSQL Account Manager while testing the settings for your SMTP account.");

                Platform.runLater(() -> {
                    DialogHelper.showInfo(getController(), "Info", "Test email successfully sent", null, getStage());
                    mainUI.setCursor(Cursor.DEFAULT);
                    mainUI.setDisable(false);
                });
            } catch (Throwable th) {
                if (th instanceof AuthenticationFailedException)
                {
                    String[] errors = th.getMessage().split("\n");
                    Platform.runLater(() -> {
                        // for authetification error - show only last message
                        DialogHelper.showError(getController(),
                            "Error", 
                            "Error testing email",
                            new AuthenticationFailedException("Can not authenticate to SMTP server: " + errors[errors.length - 1]), 
                            getStage()
                        );
                        mainUI.setCursor(Cursor.DEFAULT);
                        mainUI.setDisable(false);
                    });
                } else {
                    Platform.runLater(() -> {
                        DialogHelper.showError(getController(), "Error", "Error testing email", th, getStage());
                        mainUI.setCursor(Cursor.DEFAULT);
                        mainUI.setDisable(false);
                    });
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
    
    
    @FXML
    void removeAction(ActionEvent event) {
        T params = savedJobsComboBox.getSelectionModel().getSelectedItem();
        if (params != null) {
            
            DialogResponce responce = DialogHelper.showConfirm(getController(), "Remove saved schedule", 
                "Do you what remove saved schedule '" + params.getName() + "'?");
            
            if (responce == DialogResponce.OK_YES) {
                deleteSchedule(params);
                savedJobsComboBox.getItems().remove(params);

                try {
                    WindowsScheduleUtil.removeWindowsScheduleJob(params);
                } catch (IOException | InterruptedException ex) {
                    DialogHelper.showError(getController(), "Error", "Error removing saved schedule", ex, getStage());
                }
            }
        }
    }
            
    @FXML
    void nextAction(ActionEvent event) {
        if (currentWizardIndex == wizardPanes.size() - 1) {
            cancelAction(null);
        } else {            
            
            int oldIndex = currentWizardIndex;
            
            if (checkNextAction()) {
            
                wizardPanes.get(oldIndex).setVisible(false);
                wizardPanes.get(++currentWizardIndex).setVisible(true);

                checkActions();
            }
        }
    }
    
    void updateAllWithScheduleParams() {
        
        connectionsComboBox.getSelectionModel().clearSelection();
        
        address.setText(schedule.getAddress());
        user.setText(schedule.getUsername());
        pass.setText(schedule.getPassword());
        
        sshHost.setText(schedule.getSshHost());
        sshPort.setText(""+schedule.getSshPort());
        
        sshUser.setText(schedule.getSshUser());
        sshPassword.setText(schedule.getSshPassword());
        
        port.setText("" + schedule.getPort());
        
        // email part
        if (isNeedSendEmail()) {
            
            emailFromName.setText(schedule.getEmailFromName());
            emailFromEmail.setText(schedule.getEmailFromEmail());
            emailReplyEmail.setText(schedule.getEmailReplyEmail());
            
            emailToEmail.setText(schedule.getEmailToEmail());
            
            emailCc.setText(schedule.getEmailCc() != null ? schedule.getEmailCc() : "");
            emailBcc.setText(schedule.getEmailBcc() != null ? schedule.getEmailBcc() : "");
            
            emailHost.setText(schedule.getEmailHost());
            emailPort.setText(""+schedule.getEmailPort());
            
            emailEncryptior.getSelectionModel().select(EncryptiorValue.toEnum(schedule.getEmailEncryptior()));
            
            if (schedule.isEmailAuth()) {
                emailAuth.setSelected(true);
                
                emailAccountName.setText(schedule.getEmailAccountName() != null ? schedule.getEmailAccountName() : "");
                emailPasswd.setText(schedule.getEmailPasswd() != null ? schedule.getEmailPasswd() : "");
            }
        }
        
        
        logFile.setText(schedule.getLogFile());
        
        useWindowsSchedule.setSelected(schedule.isUseWindowsSchedule());
        
        if (schedule.isUseWindowsSchedule()) {
            runFrequency.getSelectionModel().select(RunFrequency.toEnum(schedule.getRunFrequency()));

            
            hourSpinner.getValueFactory().setValue(schedule.getFrequencyHour());
            minutesSpinner.getValueFactory().setValue(schedule.getFrequencyMinutes());
            
            String weekly = schedule.getFrequencyWeekly();
            if (weekly != null && !weekly.isEmpty()) {
                
                for (String day: weekly.split(":")) {
                    
                    try {
                        switch (WeeklyValue.toEnum(Integer.valueOf(day))) {
                            case MONDAY:
                                monCheckBox.setSelected(true);
                                break;
                            case TUESDAY:
                                tueCheckBox.setSelected(true);
                                break;
                            case WEDNESDAY:
                                wedCheckBox.setSelected(true);
                                break;
                            case THURSDAY:
                                thuCheckBox.setSelected(true);
                                break;
                            case FRIDAY:
                                friCheckBox.setSelected(true);
                                break;
                            case SATURDAY:
                                satCheckBox.setSelected(true);
                                break;
                            case SUNDAY:
                                sunCheckBox.setSelected(true);
                                break;
                        }
                    } catch (NumberFormatException ex) {
                        DialogHelper.showError(getController(), "Error", "Error  encountered when  converting string  to  weekly value  ENUM data-type", ex, getStage());
                    }
                }
            }
        }
        
        
        
        updateAllComponentsUsingSchedule(schedule);
    }
    
    
    void sendEmail(T scheduleParams, String subject, String text) {
        try {
            EmailUtils.sendEmail(scheduleParams.getEmailFromName(), scheduleParams.getEmailFromEmail(), scheduleParams.getEmailReplyEmail(),
                    scheduleParams.getEmailToEmail(), scheduleParams.getEmailCc(), scheduleParams.getEmailBcc(), 
                    scheduleParams.getEmailHost(), ""+scheduleParams.getEmailPort(), scheduleParams.getEmailEncryptior(), 
                    scheduleParams.getEmailAccountName(), scheduleParams.getEmailPasswd(),
                    subject, text);
        } catch (Throwable e) {
            DialogHelper.showError(getController(), "Error", "Error sending message", e, getStage());
        }
    }
    
    @FXML
    void backAction(ActionEvent event) {
        int oldIndex = currentWizardIndex;
        
        checkBackAction();        
        
        wizardPanes.get(oldIndex).setVisible(false);
        wizardPanes.get(--currentWizardIndex).setVisible(true);
        
        checkActions();
    }
    
    @FXML
    public void refreshDatabaseAction(ActionEvent event) {
        
        Database database = databaseComboBox.getSelectionModel().getSelectedItem();
        if (database != null) {
            makeBusy(true);
            Thread th = new Thread(() -> {
                try {
                    
                    String cacheKey = schedule != null ? schedule.toConnectionParams().cacheKey() : null;
                    if (cacheKey == null) {
                        cacheKey = address.getText().replace(".", "_") + "_" + port.getText();
                    }

                    Database db = DatabaseCache.getInstance().syncDatabase(database.getName(), cacheKey, (AbstractDBService) dbService, true);
                    Platform.runLater(() -> {
                        databaseComboBox.getItems().set(databaseComboBox.getItems().indexOf(database), db);
                        makeBusy(false);
                        
                        databaseChanged(db);
                    });                
                } catch (SQLException ex) {
                    Platform.runLater(() -> {
                        DialogHelper.showError(getController(), "Error", "Error refreshing database", ex, getStage());
                        makeBusy(false);
                    });
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    private void checkActions() {
        backButton.setDisable(currentWizardIndex == 0 || currentWizardIndex == wizardPanes.size() - 1);
        nextButton.setText(currentWizardIndex == wizardPanes.size() - 1 ? "Done" : "Next >");
    }
    
    String getWeeklyString() {
        String weeklyDays = "";
        
        if (monCheckBox.isSelected()) {
            weeklyDays += "0:";
        }
        if (tueCheckBox.isSelected()) {
            weeklyDays += "1:";
        }
        if (wedCheckBox.isSelected()) {
            weeklyDays += "2:";
        }
        if (thuCheckBox.isSelected()) {
            weeklyDays += "3:";
        }
        if (friCheckBox.isSelected()) {
            weeklyDays += "4:";
        }
        if (satCheckBox.isSelected()) {
            weeklyDays += "5:";
        }
        if (sunCheckBox.isSelected()) {
            weeklyDays += "6:";
        }
        
        return weeklyDays.isEmpty() ? null : weeklyDays.substring(0, weeklyDays.length() - 1);
    }
    
    @FXML
    void cancelAction(ActionEvent event) {        
        try {
            if (currentConnectionParams != null) {
                dbService.connect(currentConnectionParams);        
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error canceling", ex, getStage());
        }
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
//    private void disableDatabaseCombobox() {
//        databaseComboBox.getItems().clear();
//        databaseComboBox.setDisable(true);
//    }

    
    abstract T fromString(String string);
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
//        databaseComboBox.setDisable(true);
        
        new AutoCompleteComboBoxListener<T>(connectionsComboBox);
        new AutoCompleteComboBoxListener<Database>(databaseComboBox);
        
        connectionsComboBox.setConverter(new StringConverter<T>() {
            @Override
            public String toString(T object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public T fromString(String string) {
                return AbstractScheduledWizardController.this.fromString(string);
            }
        });
        
        databaseComboBox.setConverter(new StringConverter<Database>() {
            @Override
            public String toString(Database object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public Database fromString(String string) {
                for (Database d: databaseComboBox.getItems()) {
                    if (d.getName().equals(string)) {
                        return d;
                    }
                }
                
                return null;
            }
        });
        
//        address.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        user.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        pass.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        port.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        sshHost.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        sshPort.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        sshUser.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        sshPassword.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
//        connectionsComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends T> observable, T oldValue, T newValue) -> {
//            disableDatabaseCombobox();
//        });
//        
        
        currentWizardIndex = 0;
        
        wizardPanes = new ArrayList<>();
        fillWizardPanes(wizardPanes);
                    
        databaseComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
            if (dbService != null && newValue != null) {
                try {                    
                    
                    ((MysqlDBService)dbService).changeDatabase(newValue);
                    
                    databaseChanged(newValue);
                    
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error connection to database", ex.getMessage(), ex, getStage());
                }
            }
        });
        
        // prepare connection combobox
//        connectionsComboBox.setButtonCell(getConnectionParamsListCell());
//        connectionsComboBox.setCellFactory((ListView<T> param) -> getConnectionParamsListCell());
        
        // load connection combobox data
        List<ConnectionParams> params = connectionParamService.getAll();
        if (params != null) {            
            connectionsComboBox.setItems(FXCollections.observableList(params.stream().map(c -> createScheduleFromConnectionParams(c)).collect(Collectors.toList())));
        }        
                
        hourSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23, 0));
        minutesSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59, 0));
        
        addListenerKeyChange(hourSpinner);
        addListenerKeyChange(minutesSpinner);
        
        
        emailEncryptior.setItems(FXCollections.observableArrayList(EncryptiorValue.values()));
        emailEncryptior.getSelectionModel().select(0);
        
        propertyListeners();
    }
    
    abstract public void databaseChanged(Database database);
    
    private void addListenerKeyChange(Spinner spinner) {
        spinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            commitEditorText(spinner);
        });
    }

    private void commitEditorText(Spinner spinner) {
        if (!spinner.isEditable()) return;
        String text = spinner.getEditor().getText();
        SpinnerValueFactory.IntegerSpinnerValueFactory valueFactory = (SpinnerValueFactory.IntegerSpinnerValueFactory) spinner.getValueFactory();
        if (valueFactory != null) {
            StringConverter<Integer> converter = valueFactory.getConverter();
            if (converter != null) {
                Integer value = converter.fromString(text);
                valueFactory.setValue(value);
            }
        }
    }
    
    public void propertyListeners() {
        newJobRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            savedJobsComboBox.setDisable(newValue);
            jobNameField.setDisable(!newValue);
        });
        
        savedJobsComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends T> observable, T oldValue, T newValue) -> {
            removeSchedultButton.setDisable(newValue == null);
        });
                
        connectionsComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends T> observable, T oldValue, T newValue) -> {
            if (newValue != null) {
                initWithSelectedConnection(newValue);
            }
        });
        
        sessionIdleTimeout.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            sessionIdleTimeoutValue.setDisable(newValue);
        });
              
        
        runFrequency.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends RunFrequency> observable, RunFrequency oldValue, RunFrequency newValue) -> {
            
            minutesSpinner.setDisable(newValue == null || !useWindowsSchedule.isSelected());
            hourSpinner.setDisable(newValue == null || !useWindowsSchedule.isSelected());
            
            if (newValue != null) {                
                switch (newValue) {
                    case DAILY:
                        weeklyBox.setDisable(true);
                        break;
                        
                    case WEEKLY:  
                        weeklyBox.setDisable(false);
                        break;
                }
            }
        });
        
        useWindowsSchedule.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            runFrequency.setDisable(!newValue);
            
            RunFrequency rf =  runFrequency.getSelectionModel().getSelectedItem();
            minutesSpinner.setDisable(!newValue || rf == null);
            hourSpinner.setDisable(!newValue || rf == null);
            weeklyBox.setDisable(!newValue || rf == null || rf == RunFrequency.DAILY);
        });
        
        
        emailAuth.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            emailAccountLabel.setDisable(!newValue);
            emailAccountName.setDisable(!newValue);
            emailPsswdLabel.setDisable(!newValue);
            emailPasswd.setDisable(!newValue);            
        });
        
        emailFromEmail.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            emailReplyEmail.setText(newValue);
        });
        
        emailEncryptior.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends EncryptiorValue> observable, EncryptiorValue oldValue, EncryptiorValue newValue) -> {
            if (newValue != null) {
                
                switch (newValue) {
                    
                    case NOENCRYPTION:
                        emailAuth.setSelected(false);
                        emailAccountName.setText("");
                        emailPasswd.setText("");
                        emailPort.setText("25");
                        break;
                        
                    case TLS:
                        emailAuth.setSelected(true);
                        emailAccountName.setText(emailFromEmail.getText());
                        emailPort.setText("25");
                        break;
                        
                    case SSL:
                        emailAuth.setSelected(true);
                        emailAccountName.setText(emailFromEmail.getText());
                        emailPort.setText("465");
                        break;
                }
                
            } else {
                emailAccountName.setText("");
                emailPasswd.setText("");
            }
        });
    }
    
        
    @FXML
    public void selectLogFile(ActionEvent event) {
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("LOG Files", "*.log"));

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            logFile.setText(file.getAbsolutePath());
        }
    }
    
    void fillScheduleWithDatabaseData() {
        if (schedule == null) {
            schedule = createSchedule();
        }
        
        Database database = databaseComboBox.getSelectionModel().getSelectedItem();
        schedule.setDatabase(database != null ? database.getName() : null);
        
        schedule.setAddress(address.getText());
        schedule.setUsername(user.getText());
        schedule.setPassword(pass.getText());
        
        try {
            schedule.setPort(Integer.parseInt(port.getText()));
        } catch (NumberFormatException ex) {
            DialogHelper.showError(getController(), "Error", "Error parsing port", ex, getStage());
        }
        
        schedule.setSessionIdleTimeout(sessionIdleTimeout.isSelected());
        try {
            schedule.setSessionIdleTimeoutValue(Long.parseLong(sessionIdleTimeoutValue.getText()));
        } catch (NumberFormatException ex) {
            DialogHelper.showError(getController(), "Error", "Error parsing idle timeout", ex, getStage());
        }
        
        schedule.setSshHost(sshHost.getText());
        schedule.setSshPassword(sshPassword.getText());
        schedule.setSshUser(sshUser.getText());
        schedule.setUseCompressedProtocol(useCompressedProtocol.isSelected());
    }
    
    void fillScheduleWithOtherData() {
        if (schedule == null) {
            schedule = createSchedule();
        }
        
        // fill schedule params
        if (schedule.getName() == null){
            schedule.setName(jobNameField.getText());
        }
        
        schedule.setUseWindowsSchedule(useWindowsSchedule.isSelected());
        
        RunFrequency rf = runFrequency.getSelectionModel().getSelectedItem();
        schedule.setRunFrequency(rf != null ? rf.id : -1);
        
        schedule.setFrequencyHour(hourSpinner.getValue());
        schedule.setFrequencyMinutes(minutesSpinner.getValue());
        
        
        schedule.setNotificationEmail(isNeedSendEmail());
        
        if (isNeedSendEmail()) {            
            schedule.setEmailFromName(emailFromName.getText());
            schedule.setEmailFromEmail(emailFromEmail.getText());
            schedule.setEmailReplyEmail(emailReplyEmail.getText().isEmpty() ? null : emailReplyEmail.getText());
            
            schedule.setEmailToEmail(emailToEmail.getText());
            schedule.setEmailCc(emailCc.getText().isEmpty() ? null : emailCc.getText());
            schedule.setEmailBcc(emailBcc.getText().isEmpty() ? null : emailBcc.getText());
            
            schedule.setEmailHost(emailHost.getText());
            try {
                schedule.setEmailPort(Integer.parseInt(emailPort.getText()));
            } catch (NumberFormatException ex) {
                DialogHelper.showError(getController(), "Error", "Error parsing email port", ex, getStage());
                
                // TODO
                schedule.setEmailPort(25);
            }
            
            schedule.setEmailEncryptior(emailEncryptior.getSelectionModel().getSelectedItem().id);
            
            schedule.setEmailAuth(emailAuth.isSelected());
            if (emailAuth.isSelected()) {
                schedule.setEmailAccountName(emailAccountName.getText());
                schedule.setEmailPasswd(emailPasswd.getText());
            } else {
                schedule.setEmailAccountName(null);
                schedule.setEmailPasswd(null);
            }
        }        
        
        schedule.setFrequencyWeekly(getWeeklyString());
        
        schedule.setLogFile(logFile.getText());
    }
    
    void doFinish() {
        
        fillScheduleWithDatabaseData();        
        fillScheduleWithOtherData();
        
        fillScheduleCustom();
        
        if (useWindowsSchedule.isSelected()) {
            runWindowsSchedule();
        }
        
        if (runImmediatly.isSelected()) {            
            
            new Thread(() -> {
                
                finishAction();
                
            }).start();
        }
    }
    
    void runWindowsSchedule() {
        try {
            Gson gson = new Gson();
            String json = gson.toJson(schedule);
            File configFile = new File(schedule.getName() + ".ssj");
            Files.write(configFile.toPath(), json.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

            WindowsScheduleUtil.createWindowsScheduleJob(schedule, configFile.getAbsolutePath());
        } catch (IOException | InterruptedException ex) {
            DialogHelper.showError(getController(), "Error", "Error in creating windows schedule file", ex, getStage());
        }
    }
    
    private void initWithSelectedConnection(T params) {
        address.setText(params == null ? "" : params.getAddress());
        user.setText(params == null ? "" : params.getUsername());
        pass.setText(params == null ? "" : params.getPassword());
        port.setText(params == null ? "" : params.getPort() + "");

        sshHost.setText(params == null ? "" : params.getSshHost());
        sshPort.setText(params == null ? "" : params.getSshPort() + "");
        sshUser.setText(params == null ? "" : params.getSshUser());
        sshPassword.setText(params == null ? "" : params.getSshPassword());
    }
    
    private ListCell<T> getConnectionParamsListCell() {
        return new ListCell<T>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (item != null) {
                    setText(item.getName());
                }
            }
        };
    }
    
    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
