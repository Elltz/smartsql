package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;


/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class MenuInIndexesModule extends AbstractStandardModule {

    private Image tableImage;
    
    private MenuItem manageIndex;
    private MenuItem createIndex;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 1;
    }
    
    @Override
    public boolean install() {
        super.install();
                
        manageIndex = getManageIndexesMenuItem(null);
        createIndex = getCreateIndexMenuItem(null);
        
        SeparatorMenuItem separate = new SeparatorMenuItem();
        Menu indexMenu = new Menu("Indexes",
            new ImageView(SmartImageProvider.getInstance().getIndex_image(18, 18)));
        indexMenu.getItems().addAll(createIndex, separate, manageIndex);
                
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        
        boolean isIndex = item.getValue() instanceof String && item.getValue().toString().equals("Indexes");
        
        manageIndex.setDisable(!isIndex);
        createIndex.setDisable(!isIndex);
        
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        SeparatorMenuItem separate = new SeparatorMenuItem();

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("indexes", new ArrayList<>(Arrays.asList(new MenuItem[]{createIndex, separate, manageIndex}))), Hooks.QueryBrowser.INDEXES);
        
        return map;
    }
    
    private MenuItem getCreateIndexMenuItem(ConnectionSession session) {
        MenuItem createIndexMenuItem = new MenuItem("Create Index",
                 new ImageView(SmartImageProvider.getInstance().getCreateIndex_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createIndexMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createIndexMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AlterTableController altertableController = showAlterTable(
                    session!= null? session : getController().getSelectedConnectionSession().get(),
                            true);
                    altertableController.manageIndexes();
                    altertableController.createIndex();
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }
            }
        });
        return createIndexMenuItem;
    }


    private MenuItem getManageIndexesMenuItem(ConnectionSession session) {
        MenuItem manageIndexesMenuItem = new MenuItem("Manage Indexes",
                 new ImageView(SmartImageProvider.getInstance().getManage_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        manageIndexesMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F7));
        manageIndexesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {            
                try {
                    AlterTableController altertableController = showAlterTable(
                    session!= null? session : getController().getSelectedConnectionSession().get(),
                            true);
                    altertableController.manageIndexes();
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }
            }
        });
        return manageIndexesMenuItem;
    }

    // @TODO Need make some singleton for methods like this
    private AlterTableController showAlterTable(final ConnectionSession session, boolean alterMode) throws SQLException {
        if (tableImage == null) {
            tableImage = SmartImageProvider.getInstance().getTableTab_image();
        }
        
        ConnectionSession session0 = session != null ? session : getController().getSelectedConnectionSession().get();
            
        
        ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(session0);
        

        DBTable table = DatabaseCache.getInstance().getTable(session0.getConnectionParam().cacheKey(), 
                object.get().getParent().getParent().getParent().getValue().toString(),
                object.get().getParent().getValue().toString());
        
        AlterTableController altertableController = null;
        Tab alterTab = null;

        ConnectionTabContentController connTab = (ConnectionTabContentController) getController().getTabControllerBySession(session0);
        List<Tab> tabs = connTab.findControllerTabs(AlterTableController.class);
        if (tabs != null && !tabs.isEmpty()) {

            for (Tab t: tabs) {
                if (((AlterTableController)t.getUserData()).getTable().equals(table)) {
                    altertableController = (AlterTableController)t.getUserData();
                    alterTab = t;
                    break;
                }
            }
        }

        if (altertableController != null) {
            getController().showTab(alterTab, session0);
        } else {
            
            altertableController = ((MainController)getController()).getTypedBaseController("AlterTable");
            altertableController.setService(session0.getService());

            alterTab = new Tab(table.toString());
            alterTab.setGraphic(new ImageView(tableImage));
            alterTab.setUserData(altertableController);

            Parent p = altertableController.getUI();
            alterTab.setContent(p);        

            altertableController.init(alterTab, table, session0, alterMode);

            getController().addTab(alterTab, session0);
            getController().showTab(alterTab, session0);
        }
        
        return altertableController;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}