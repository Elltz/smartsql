/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import java.util.ArrayList;
import javafx.scene.control.Tab;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.DBElementWrapperOnBuilder;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public interface ObjectViewClickListeners {
    
    /*
     * Since every Query, View query, Querybuilder,Schema builder
     * all listens to the the clicks of the object browser, they will
     * all implements this interface so as to able to respond the clicks
     * without me doing any crazy stuff, like using the Recycler.
     */
    
    
    /**
     * 
     * @param bm 
     * 
     * Simply responding to DBElements clicks. which is all.
     */
    void addViewOverseer(boolean addRelation, DBElementWrapperOnBuilder... bm);
    
    /**
     * this provides a way for the implementors to be able to set and get
     * the identifier for onclick listener in the treeview/treeitem.
     * @param t 
     * The particular tab that is to listen to these clicks; could be 
     * queryBuilder,Schema Designer etc etc
     */    
    void setIdentifier(Tab t);
    
    Tab getidentifier();
    
    default void addToSet(ConnectionSession cs){
        if(cs == null){return;}
        //DONT KNOW IF WE STILL USE THIS SO I ADD A BREAK POINT
        //TabContentController.ovclHash.putIfAbsent(cs, new ArrayList<ObjectViewClickListeners>());
        //TabContentController.ovclHash.get(cs).add(this);
    }
}
