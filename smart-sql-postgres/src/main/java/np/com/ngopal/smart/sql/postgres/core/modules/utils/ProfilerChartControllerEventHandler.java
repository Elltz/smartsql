/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.chart.Axis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public abstract class ProfilerChartControllerEventHandler implements EventHandler<MouseEvent> {

    private double startX = -1;
    private double finishX = -1;
    boolean showRect = false;
    double x = -1;
    double y = -1;
    public Rectangle rectangle;
    public XYChart chart;
    public Axis xAxis;

    @Override
    public void handle(MouseEvent event) {
        if (xAxis instanceof NumberAxis) {
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
                Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());
                startX = xAxis.sceneToLocal(mouseSceneCoords).getX();

                showRect = true;
                x = event.getX();
                y = event.getY();

            } else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
                Point2D mouseSceneCoords = new Point2D(event.getSceneX(), event.getSceneY());
                finishX = xAxis.sceneToLocal(mouseSceneCoords).getX();

                showRect = false;
                rectangle.setWidth(0);
                rectangle.setHeight(0);

                if (Math.abs(startX - finishX) > 30) {
                    zoomDragedAction(((NumberAxis)xAxis).getValueForDisplay(startX).longValue(), ((NumberAxis)xAxis).getValueForDisplay(finishX).longValue());
                }
            } else if (showRect && event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                if (chart.getHeight() > event.getY() && chart.getWidth() > event.getX()) {

                    rectangle.setFill(Color.web("d6ebff", 0.5d));
                    rectangle.setStrokeWidth(0.3d);
                    rectangle.setStroke(Color.BLACK);
                    rectangle.getStrokeDashArray().addAll(5d, 3d);
                    rectangle.setStrokeType(StrokeType.INSIDE);
                    rectangle.setX(x);
                    rectangle.setY(y);
                    rectangle.setWidth(event.getX() - x);
                    rectangle.setHeight(event.getY() - y);
                }
            }
        }
    }
    
    
    public abstract void zoomDragedAction(Long start, Long finish);

}
