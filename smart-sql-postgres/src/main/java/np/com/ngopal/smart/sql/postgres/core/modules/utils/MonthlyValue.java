package np.com.ngopal.smart.sql.core.modules.utils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public enum MonthlyValue {
    JANUARY(0, "January", "JAN"),
    FEBRUARY(1, "February", "FEB"),
    MARCH(2, "March", "MAR"),
    APRIL(3, "April", "APR"),
    MAY(4, "May", "MAY"),
    JUNE(5, "June", "JUN"),
    JULY(6, "July", "JUL"),
    AUGUST(7, "August", "AUG"),
    SEPTEMBER(8, "September", "SEP"),
    OCTOBER(9, "October", "OCT"),
    NOVEMBER(10, "November", "NOV"),
    DECEMBER(11, "December", "DEC");

    public final int id;
    final String name;
    final String scheduleTaskName;

    MonthlyValue(int id, String name, String scheduleTaskName) {
        this.id = id;
        this.name = name;
        this.scheduleTaskName = scheduleTaskName;
    }

    public static MonthlyValue toEnum(int id) {
        switch (id) {                
            case 0:
                return JANUARY;                    
            case 1:
                return FEBRUARY;                    
            case 2:
                return MARCH;
            case 3:
                return APRIL;
            case 4:
                return MAY;
            case 5:
                return JUNE;
            case 6:
                return JULY;
            case 7:
                return AUGUST;
            case 8:
                return SEPTEMBER;    
            case 9:
                return OCTOBER;
            case 10:
                return NOVEMBER;
            case 11:
                return DECEMBER;

            default:
                return JANUARY;
        }
    }

    public String getScheduleTaskName() {
        return scheduleTaskName;
    }

    @Override
    public String toString() {
        return name;
    }
};
