package np.com.ngopal.smart.sql.core.modules.utils;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public enum RunFrequency {
    DAILY(0, "Daily"), 
    WEEKLY(1, "Weekly");

    public final int id;
    final String name;
    RunFrequency(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static RunFrequency toEnum(int id) {
        switch (id) {                
            case 0:
                return DAILY;                    
            case 1:
                return WEEKLY;              
        }

        return null;
    }

    @Override
    public String toString() {
        return name;
    }
};
