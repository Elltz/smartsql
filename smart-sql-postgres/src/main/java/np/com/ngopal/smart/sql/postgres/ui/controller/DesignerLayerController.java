package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DesignerLayerController extends BaseController implements Initializable {
        
    @FXML
    private AnchorPane mainUI;
     
    @FXML
    private TextField layerNameField;
    @FXML
    private TextField colorField;    
    @FXML
    private ColorPicker colorPicker;
    
    private boolean dataChanged = false;

    private DesignerLayer layer;
    private DesignerLayer sourceLayer;
    private Tab tab;
    
    private SchemaDesignerTabContentController parent;
    
    public void init(Tab tab, SchemaDesignerTabContentController parent, DesignerLayer sourceLayer) {        
        
        this.tab = tab;
        this.sourceLayer = sourceLayer;
        this.parent = parent;
        
        this.layer = new DesignerLayer();
        this.layer.fromMap(sourceLayer.toMap());
                
        layerNameField.textProperty().bindBidirectional(layer.name());
        
//        colorField.textProperty().bindBidirectional(layer.color());
        colorField.setOnAction((ActionEvent event) -> {
            try {
                colorPicker.setValue(Color.web(colorField.getText()));
            } catch (Throwable th) {}
        });
        
        colorField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue && !newValue) {
                try {
                    Color c = Color.web(colorField.getText());
                    if (c != null && !c.equals(colorPicker.getValue())) {
                        colorPicker.setValue(c);
                    }
                } catch (Throwable th) {}
            }
        });
        
        colorField.setText(layer.color().get());
        
        try {
            colorPicker.setValue(Color.web(layer.color().get()));
        } catch (Throwable th) {}
        
        colorPicker.valueProperty().addListener((ObservableValue<? extends Color> observable, Color oldValue, Color newValue) -> {
            
            int r = (int)Math.round(newValue.getRed() * 255.0);
            int g = (int)Math.round(newValue.getGreen() * 255.0);
            int b = (int)Math.round(newValue.getBlue() * 255.0);

            colorField.setText(String.format("#%02X%02X%02X", r, g, b));
            layer.color().set(colorField.getText());
        });
    }
    
    
    @FXML
    public void saveAction(ActionEvent event) {
        
        sourceLayer.fromMap(layer.toMap());
        parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + layer.name().get() + "'", parent.getCurrentDiagram());
        
        closeAction(null);
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        module.makeCanSave();
    }
    
    @FXML
    public void closeAction(ActionEvent event) {
        if (tab != null && tab.getTabPane() != null) {
            tab.getTabPane().getTabs().remove(tab);
        }
    }
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
}
