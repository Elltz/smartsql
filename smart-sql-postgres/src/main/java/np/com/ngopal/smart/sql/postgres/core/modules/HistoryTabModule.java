package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.ui.controller.HistoryTabController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;
import org.w3c.dom.Element;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class HistoryTabModule extends AbstractStandardModule {
    
    public static final String Tab_History = "historyTab";
    private HashMap<ConnectionSession, Tab> tabHolder = new HashMap<>();
    private HashMap<ConnectionSession, HistoryTabController> controllerHolder = new HashMap<>();

    @Inject
    private PreferenceDataService preferenceService;
     
    private Image historyImage = null;

    
    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public boolean install() {
//        getController()
        return true;
    }
    
    public Tab getTab(ConnectionSession session) {
        return tabHolder.get(session);
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem obj) {return true; }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postConnection(ConnectionSession session, Tab intab) throws Exception {
        if (historyImage == null) {
            historyImage = SmartImageProvider.getInstance().getHistory_image();
        }

        HistoryTabController tabController = getTypedBaseController("HistoryTab");
        tabController.init(session);

        Tab tab = new Tab("History");
        tab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(historyImage), 16, 16));
        tab.setUserData(Tab_History);

        ChangeListener<Object> listener = (ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {
            if (tab == newValue) {
                tabController.refreshData(null);
            }
        };

        tab.onClosedProperty().addListener((ObservableValue<? extends EventHandler<Event>> observable, EventHandler<Event> oldValue, EventHandler<Event> newValue) -> {
            getController().selectedTabProperty().removeListener(listener);
            tabController.destory();
        });

        getController().selectedTabProperty().addListener(listener);

        tab.setContent(tabController.getUI());

        tabHolder.put(session, tab);
        controllerHolder.put(session, tabController);
        
        tab.setOnCloseRequest((Event t) -> {
            tabHolder.remove(session);
            controllerHolder.remove(session);
        });
        
        Platform.runLater(() ->  {
            Tab t = reopenTab(session);
            if (t != null) {
                t.getTabPane().getSelectionModel().select(t);
            }
        });
    }
    
    public Tab reopenTab(ConnectionSession session) {
        Tab tab = tabHolder.get(session);
        if (tab != null) {
            
            if (tab.getTabPane() != null) {
                tab.getTabPane().getTabs().remove(tab);
            }
            
            PreferenceData pd = preferenceService.getPreference();
            if (pd.isPositioningHistory()) {
                getController().addTabToBottomPane(session, tab);
            } else {
                getController().addTab(tab, session);
            }
        }
        
        return tab;
    }
           
    public void changeAllFont(String font) {
        for (HistoryTabController tabController: controllerHolder.values()) {
            tabController.changeFont(font);
        }
    }

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }

}
