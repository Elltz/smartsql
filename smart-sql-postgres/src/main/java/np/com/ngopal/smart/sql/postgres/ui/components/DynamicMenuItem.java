package np.com.ngopal.smart.sql.ui.components;

import javafx.scene.Node;
import javafx.scene.control.MenuItem;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public abstract class DynamicMenuItem extends MenuItem{

    public DynamicMenuItem(String text) {
        super(text);
    }
    
    public abstract Node createNode();
}
