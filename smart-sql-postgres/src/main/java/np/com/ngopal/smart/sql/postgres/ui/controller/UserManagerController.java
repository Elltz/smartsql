/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import com.sun.javafx.scene.control.skin.TableViewSkinBase;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.UserManagerModule;
import np.com.ngopal.smart.sql.core.modules.utils.Flags;
import np.com.ngopal.smart.sql.core.modules.utils.MysqlUser;
import np.com.ngopal.smart.sql.core.modules.utils.MysqlVersionHandler;
import np.com.ngopal.smart.sql.core.modules.utils.PrivsLevelWrapper;
import np.com.ngopal.smart.sql.core.modules.utils.ProcStateManipulator;
import np.com.ngopal.smart.sql.core.modules.utils.User;
import np.com.ngopal.smart.sql.core.modules.utils.UserManagerTreeItemPrivilegesLabels;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.components.RoundToggleButton;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah laweh (rumorsapp@gmail.com)
 */
public class UserManagerController extends BaseController implements Initializable {

    @FXML
    private TextField usernameField;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button cancelChangesButton;

    @FXML
    private Button addNewUser;

    @FXML
    private Button closeButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Label passwordAlertLabel;

    @FXML
    private TextField maxQueriesField;

    @FXML
    private TextField maxUpdatesField;

    @FXML
    private TextField maxConnField;

    @FXML
    private TextField conConnField;

    @FXML
    private Button deleteUser;

    @FXML
    private Button saveChangesButton;

    @FXML
    private PasswordField rePasswordField;

    @FXML
    private ComboBox<String> hostChooser;

    @FXML
    private Button unlockAccountButton;

    @FXML
    private Button lockAccountButton;

    @FXML
    private ComboBox<String> passwordExpiry;

    @FXML
    private Button convertToQueryButton;

    @FXML
    private Label instanceText;

    @FXML
    private Button expirePasswordButton;

    @FXML
    private TableView<SimplePrivsTypeValuesMatcher> globalRoleTemplateTableview;

    @FXML
    private TableView<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>> globalPrivsSelectorTable;

    @FXML
    private TableView<SimplePrivsTypeValuesMatcher> administrativeSchemaPrivilegsTableview;

    @FXML
    private Button revokAllGlobalLevelPrivsButton;

    @FXML
    private Button revokAllSchemaLevelPrivsButton;

    @FXML
    private Button schemaDeleteEntryButton;

    @FXML
    private Button schemaAddEntryButton;

    @FXML
    private CheckBox checkboxSelect;

    @FXML
    private CheckBox checkboxInsert;

    @FXML
    private CheckBox checkboxUpdate;

    @FXML
    private CheckBox checkboxDelete;

    @FXML
    private CheckBox checkboxExecute;

    @FXML
    private CheckBox checkboxShowView;

    @FXML
    private CheckBox checkboxCreate;

    @FXML
    private CheckBox checkboxAlter;

    @FXML
    private CheckBox checkboxPreferences;

    @FXML
    private CheckBox checkboxIndex;

    @FXML
    private CheckBox checkboxCreateView;

    @FXML
    private CheckBox checkboxCreateRoutine;

    @FXML
    private CheckBox checkboxAlterRoutine;

    @FXML
    private CheckBox checkboxEvent;

    @FXML
    private CheckBox checkboxDrop;

    @FXML
    private CheckBox checkboxTrigger;

    @FXML
    private CheckBox checkboxGrant;

    @FXML
    private CheckBox checkboxCreateTemporaryTable;

    @FXML
    private CheckBox checkboxLockTables;

    @FXML
    private Button schemaUnselectAllButton;

    @FXML
    private Button schemaSelectAllButton;

    @FXML
    private VBox userSectionContainer;

    @FXML
    private TableView<MysqlUser> usersTableview;

    @FXML
    private SplitPane contentSplitPane;

    @FXML
    private TabPane tabPane;

    @FXML
    private VBox administrationSectionContainer;

    @FXML
    private Button refreshUsers;

    @FXML
    private Label tabPaneHeader;

    @FXML
    private HBox administrativeSchemaCheckablePaneContainer;

    @FXML
    private VBox box;

    @FXML
    private VBox box1;

    @FXML
    private VBox box2;

    @FXML
    private Label schemaLevelInfoLabel;

    @FXML
    private GridPane loginGrid;

    @FXML
    private Label passowrdExpiryLabel;

    @FXML
    private HBox header;

    @FXML
    private ImageView headerImageView;

    @FXML
    private VBox innerHeader;

    @FXML
    private Label schemaPrivInfoLabel;

    private ProgressIndicator smallProgressIndicator;

    private Tab userManagerTab;

    private HashMap<MysqlUser, Boolean> keepUserCustomState = new HashMap();

    private MysqlVersionHandler mvh;
    
    private WorkProc specsProc;

    private BooleanBinding unSavedUserEdits = new BooleanBinding() {
        @Override
        protected boolean computeValue() {
            return getActiveMysqlUser() != null && (getActiveMysqlUser().isNewUser()
                    || passwordField.getText().length() > 0
                    || getActiveMysqlUser().isGenInformationModified()
                    || !getActiveMysqlUser().isUserHavingEmptyEdittingPrivs());
        }
    };

    private SimpleBooleanProperty initializeSuccess = new SimpleBooleanProperty();

    private ImageView tabImage = FunctionHelper.constructImageViewWithSize(new ImageView(UserManagerModule.userDefaultImage), 16, 16);

    private ArrayList<SimplePrivsTypeValuesMatcher> defaultGlobalPrivsTemplates = new ArrayList(20);

    private SimplePrivsTypeValuesMatcher customRole;

    private Integer validate_password_special_char_count;

    private Integer validate_password_number_count;

    private Integer validate_password_mixed_case_count;

    private int validate_password_length;

    private String validate_password_policy;

    private boolean isSystemEdit = false;

    private ChangeListener<Boolean> focusListener;
    private ChangeListener<Boolean> conneciontabSelectionListener;
    private ChangeListener<Bounds> boundListener;
    private ChangeListener<Boolean> disableListener;
    private ChangeListener<Boolean> tabSelectionListener;
    private ChangeListener<Boolean> unSavedUserEditListener;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    private void globalListenersCreator() {
        unSavedUserEditListener = new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov,
                    Boolean t, Boolean t1) {
                tabImage.setImage(t1 ? UserManagerModule.userDefaultEditted
                        : UserManagerModule.userDefaultImage);
            }
        };

        tabSelectionListener = new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    ((MainController) getController()).performHideLeftPane();
                } else if (newValue != null && Objects.equals(newValue, Boolean.FALSE)) {
                    ((MainController) getController()).performShowLeftPane();
                }
            }
        };

        focusListener = new ChangeListener<Boolean>() {
            long lostFocustTime = 0l;

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (lostFocustTime == 0l || !newValue) {
                    lostFocustTime = System.currentTimeMillis();
                } else if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lostFocustTime) > 15) {
                    Platform.runLater(() -> refreshUsersKeepingChanges());
                }
            }
        };

        conneciontabSelectionListener = new ChangeListener<Boolean>() {
            long lastActiveTime = 0l;

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                System.out.println("connection tab selection listener");
                if (newValue != null && newValue && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastActiveTime) > 3) {
                    refreshUsersKeepingChanges();
                } else {
                    lastActiveTime = System.currentTimeMillis();
                }
            }
        };

        boundListener = new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
                calculateUiSpecs();
            }
        };

        disableListener = new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov,
                    Boolean t, Boolean t1) {
                if (t1) {
                    userManagerTab.setGraphic(smallProgressIndicator);
                } else {
                    userManagerTab.setGraphic(tabImage);
                }
                makeBusy(t1);
                forceTriggerSavedUserEditsFlags();
            }
        };
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        smallProgressIndicator = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
        smallProgressIndicator.setPrefSize(15, 15);

        tabPane.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue != null) {
                    switch (newValue.intValue()) {
                        case 0:
                            onLoginTab();
                            break;
                        case 1:
                            onLimitsTab();
                            break;
                        case 2:
                            onAdministrativeTab();
                            break;
                        case 3:
                            onSchemaPrivsTab();
                            break;
                        default:
                            break;
                    }
                }
            }
        });

        globalListenersCreator();

        usersTableviewWorks();
        administrativeWork();
        passwordExpiryWork();
        hostChooserWork();
        clickListeners();
    }

    /**
     *
     * ============================================================================================================================================================
     *
     * ADMINISTRATIVE SECTION CODES START HERE
     *
     * ============================================================================================================================================================
     *
     */
    private TableCell<SimplePrivsTypeValuesMatcher, String> getStringValColumnCell() {
        return new TableCell<SimplePrivsTypeValuesMatcher, String>() {

            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setAlignment(Pos.CENTER_LEFT);
                    setText(item);
                }
            }

        };
    }

    private void administrativeWork() {

        /**
         * ADMINISTRATIVE ALL GLOBAL PRIVS TABLE VIEW
         */
        TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean> checkyColumn
                = (TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean>) globalPrivsSelectorTable.getColumns().get(0);

        checkyColumn.setCellFactory(new Callback<TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean>, TableCell<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean>>() {
            @Override
            public TableCell<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean> call(TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean> param) {
                return new TableCell<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean>() {
                    {
                        final PrivsLevelWrapper pl = new PrivsLevelWrapper(mvh.getVersion());
                        CheckBox checkbox = new CheckBox();
                        checkbox.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                boolean flag = ((CheckBox) event.getSource()).isSelected();
                                Entry<SimpleBooleanProperty, SimpleStringProperty> en = (Entry<SimpleBooleanProperty, SimpleStringProperty>) getTableRow().getItem();
                                Recycler.addWorkProc(new WorkProc() {
                                    @Override
                                    public void run() {
                                        //try {Thread.sleep(25); }catch(Exception e){}
                                        validateSelectableCheckBoxStateSchemaPrivsOnActiveUser(flag, getActiveMysqlUser(), pl, en.getValue().get());
                                        refreshGlobalPrivsTemplates();
                                        forceTriggerSavedUserEditsFlags();
                                    }
                                });
                            }
                        });
                        setGraphic(checkbox);
                        setAlignment(Pos.CENTER);

                    }

                    @Override
                    protected void updateItem(Boolean item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setContentDisplay(ContentDisplay.TEXT_ONLY);
                        } else {
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            ((CheckBox) getGraphic()).setSelected(item);
                        }
                    }
                };
            }
        });

        checkyColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, Boolean> param) {
                return param.getValue().getKey();
            }
        });

        TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String> valueColumn
                = (TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String>) globalPrivsSelectorTable.getColumns().get(1);

        valueColumn.setCellFactory(new Callback<TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String>, TableCell<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String>>() {
            @Override
            public TableCell<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String> call(TableColumn<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String> param) {
                return new TableCell<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText("");
                        } else {
                            setAlignment(Pos.CENTER_LEFT);
                            setText(item);
                        }
                    }
                };
            }
        });

        valueColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<AbstractMap.Entry<SimpleBooleanProperty, SimpleStringProperty>, String> param) {
                return param.getValue().getValue();
            }
        });
        valueColumn.setMinWidth(222);

        globalPrivsSelectorTable.setMinWidth(270);

        /**
         * ADMINISTRATIVE GENERAL TABLE VIEW
         */
        TableColumn<SimplePrivsTypeValuesMatcher, Boolean> checkyColumn1
                = (TableColumn<SimplePrivsTypeValuesMatcher, Boolean>) globalRoleTemplateTableview.getColumns().get(0);

        checkyColumn1.setCellFactory(new Callback<TableColumn<SimplePrivsTypeValuesMatcher, Boolean>, TableCell<SimplePrivsTypeValuesMatcher, Boolean>>() {
            @Override
            public TableCell<SimplePrivsTypeValuesMatcher, Boolean> call(TableColumn<SimplePrivsTypeValuesMatcher, Boolean> param) {
                return new TableCell<SimplePrivsTypeValuesMatcher, Boolean>() {
                    {
                        CheckBox checkbox = new CheckBox();
                        checkbox.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                SimplePrivsTypeValuesMatcher matcher = (SimplePrivsTypeValuesMatcher) getTableRow().getItem();
                                boolean flag = ((CheckBox) event.getSource()).isSelected();
                                keepUserCustomState.put(getActiveMysqlUser(), customRole == matcher ? flag : null);
                                Recycler.addWorkProc(new WorkProc() {
                                    @Override
                                    public void run() {
                                        System.out.println(new Exception().getStackTrace()[0].getMethodName());
                                        long before = System.currentTimeMillis();
                                        String[] stringarray = new String[]{};
                                        stringarray = matcher.levelWrapper.getRawPrivileges(true).toArray(stringarray);
                                        validateSelectableCheckBoxStateSchemaPrivsOnActiveUser(flag, getActiveMysqlUser(),
                                                matcher.levelWrapper, stringarray);
                                        refreshGlobalPhaseAministratorPrivs();
                                        long after = System.currentTimeMillis();
                                        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
                                    }
                                });
                            }
                        });
                        setGraphic(checkbox);
                        setAlignment(Pos.CENTER);

                    }

                    @Override
                    protected void updateItem(Boolean item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setContentDisplay(ContentDisplay.TEXT_ONLY);
                        } else {
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            ((CheckBox) getGraphic()).setSelected(item);
                        }
                    }
                };
            }
        });

        checkyColumn1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, Boolean>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, Boolean> param) {
                return param.getValue().bool;
            }
        });

        TableColumn<SimplePrivsTypeValuesMatcher, String> roleValueColumn
                = (TableColumn<SimplePrivsTypeValuesMatcher, String>) globalRoleTemplateTableview.getColumns().get(1);

        roleValueColumn.setCellFactory(new Callback<TableColumn<SimplePrivsTypeValuesMatcher, String>, TableCell<SimplePrivsTypeValuesMatcher, String>>() {
            @Override
            public TableCell<SimplePrivsTypeValuesMatcher, String> call(TableColumn<SimplePrivsTypeValuesMatcher, String> param) {
                return getStringValColumnCell();
            }
        });

        roleValueColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String> param) {
                return param.getValue().values[0];
            }
        });

        TableColumn<SimplePrivsTypeValuesMatcher, String> desValueColumn
                = (TableColumn<SimplePrivsTypeValuesMatcher, String>) globalRoleTemplateTableview.getColumns().get(2);

        desValueColumn.setCellFactory(new Callback<TableColumn<SimplePrivsTypeValuesMatcher, String>, TableCell<SimplePrivsTypeValuesMatcher, String>>() {
            @Override
            public TableCell<SimplePrivsTypeValuesMatcher, String> call(TableColumn<SimplePrivsTypeValuesMatcher, String> param) {
                return getStringValColumnCell();
            }
        });

        desValueColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String> param) {
                return param.getValue().values[1];
            }
        });

        desValueColumn.prefWidthProperty().bind(globalRoleTemplateTableview.widthProperty().subtract(roleValueColumn.widthProperty()).subtract(checkyColumn1.widthProperty()).subtract(5));

        /**
         * ADMINISTARTION SCHEMA
         */
        TableColumn<SimplePrivsTypeValuesMatcher, String> schemaValueColumn
                = (TableColumn<SimplePrivsTypeValuesMatcher, String>) administrativeSchemaPrivilegsTableview.getColumns().get(0);

        schemaValueColumn.setCellFactory(new Callback<TableColumn<SimplePrivsTypeValuesMatcher, String>, TableCell<SimplePrivsTypeValuesMatcher, String>>() {
            @Override
            public TableCell<SimplePrivsTypeValuesMatcher, String> call(TableColumn<SimplePrivsTypeValuesMatcher, String> param) {
                return getStringValColumnCell();
            }
        });

        schemaValueColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String> param) {
                param.getValue().values[0].setValue(param.getValue().levelWrapper.convertPrivsLevelToSqlStandard());
                return param.getValue().values[0];
            }
        });

        TableColumn<SimplePrivsTypeValuesMatcher, String> privsValueColumn
                = (TableColumn<SimplePrivsTypeValuesMatcher, String>) administrativeSchemaPrivilegsTableview.getColumns().get(1);

        privsValueColumn.setCellFactory(new Callback<TableColumn<SimplePrivsTypeValuesMatcher, String>, TableCell<SimplePrivsTypeValuesMatcher, String>>() {
            @Override
            public TableCell<SimplePrivsTypeValuesMatcher, String> call(TableColumn<SimplePrivsTypeValuesMatcher, String> param) {
                return getStringValColumnCell();
            }
        });

        privsValueColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<SimplePrivsTypeValuesMatcher, String> param) {
                return param.getValue().values[1];
            }
        });

        privsValueColumn.prefWidthProperty().bind(administrativeSchemaPrivilegsTableview.widthProperty().subtract(schemaValueColumn.widthProperty()).subtract(2));

        administrativeSchemaPrivilegsTableview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SimplePrivsTypeValuesMatcher>() {
            @Override
            public void changed(ObservableValue<? extends SimplePrivsTypeValuesMatcher> observable, SimplePrivsTypeValuesMatcher oldValue, SimplePrivsTypeValuesMatcher newValue) {
                Platform.runLater(() -> {
                    onNewSchemaLevelEntrySelected(newValue);
                });
            }
        });

        EventHandler<ActionEvent> checkAction = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void run() {
                        System.out.println(new Exception().getStackTrace()[0].getMethodName());
                        long before = System.currentTimeMillis();
                        SimplePrivsTypeValuesMatcher matcher = administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem();
                        validateSelectableCheckBoxStateSchemaPrivsOnActiveUser((CheckBox) event.getSource(),
                                getActiveMysqlUser(), matcher.levelWrapper);
                        Platform.runLater(() -> {
                            refreshSchemaTemplate(matcher);
                            forceTriggerSavedUserEditsFlags();
                            administrativeSchemaPrivilegsTableview.getProperties().put(TableViewSkinBase.REFRESH, Boolean.TRUE);
                        });
                        long after = System.currentTimeMillis();
                        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
                    }
                });
            }
        };

        for (CheckBox cb : getAllSelectableSchemaLevelPrivileges()) {
            cb.setOnAction(checkAction);
        }
    }

    private void installDefaultOnGenPrivsTemplate() {
        nullifyCustomRole();
        globalRoleTemplateTableview.getItems().addAll(getDefaultGlobalPrivsTemplates());
    }

    private void nullifyCustomRole() {
        if (customRole != null) {
            Platform.runLater(() -> {
                //nullify custom role here
                globalRoleTemplateTableview.getItems().remove(customRole);
                //customRole = null;
            });
        }
        System.gc();
    }

    private ArrayList<SimplePrivsTypeValuesMatcher> getDefaultGlobalPrivsTemplates() {
        if (defaultGlobalPrivsTemplates.isEmpty()) {
            PrivsLevelWrapper levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.makeSystem();
            levelWrapper = levelWrapper.clone(true);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_ADM__ALL);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("DBA"), new SimpleStringProperty("Grants the rights to perform all tasks")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SUPER);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DBS__EVENT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__RELOAD);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SHOW_DATABASE);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SHUTDOWN);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("Maintenance Admin"), new SimpleStringProperty("Grants rights needed to maintain server")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__RELOAD);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SUPER);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("Process Admin"), new SimpleStringProperty("Grants rights needed to assess, monitor and kill any user process running in server")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__CREA_USER);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__RELOAD);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("User Admin"), new SimpleStringProperty("Grants rights to create users logins and reset passwords")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__CREA_USER);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__RELOAD);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SHOW_DATABASE);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("Security Admin"), new SimpleStringProperty("Grants rights to manage logins and grant and revoke server and database level permission")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__PROCESS);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("Monitor Admin"), new SimpleStringProperty("Grants minimum set of rights needed to monitor server")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TAB__ALTER);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_SDR__ALT_ROUT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DTI__CREATE);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_SDR__CREA_ROUT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TAB__CREA_TEMP);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_VIW__CREA_VIEW);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DBS__EVENT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DTV__DROP);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DBS__LOCK_TABLES);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TAB__DEL);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TAB__INDEX);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TaC__INSERT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TAB__TRIGGER);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_VIW__SHOW_VIEW);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SHOW_DATABASE);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TaC__UPDATE);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TaC__SELECT);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("DB Manger"), new SimpleStringProperty("Grants full rights on all databases")));

            levelWrapper = levelWrapper.clone(true);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_TAB__CREA_TEMP);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_TaC__INSERT);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_DBS__EVENT);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_DTV__DROP);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_DBS__LOCK_TABLES);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_TAB__DEL);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_TaC__UPDATE);
            levelWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_TaC__SELECT);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("DB designer"), new SimpleStringProperty("Grants rights to create and reverse engineer any database schema")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__REPL_CLIENT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__REPL_SLAVE);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SUPER);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("Replication Admin"), new SimpleStringProperty("Grants rights needed to setup and manage replication")));

            levelWrapper = new PrivsLevelWrapper(mvh.getVersion());
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DBS__EVENT);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_DBS__LOCK_TABLES);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_ADM__SHOW_DATABASE);
            levelWrapper.modifyPrivilege(true, Flags.CONTEXT_PRIV_TaC__SELECT);
            defaultGlobalPrivsTemplates.add(new SimplePrivsTypeValuesMatcher(levelWrapper,
                    new SimpleStringProperty("BackupAdmin"), new SimpleStringProperty("Grants minimal rights neede to backup any database")));
        }

        return defaultGlobalPrivsTemplates;
    }

    private boolean hasCustomGlobalPrivsTemplate() {
        boolean bool = globalRoleTemplateTableview.getItems().size() > getDefaultGlobalPrivsTemplates().size();
        return bool;
    }

    private void populatePrivsSelectorTable() {
        for (String s : getDefaultGlobalPrivsTemplates().get(0).levelWrapper.getRawPrivileges(true)) {
            globalPrivsSelectorTable.getItems()
                    .add(new AbstractMap.SimpleEntry<SimpleBooleanProperty, SimpleStringProperty>(
                            new SimpleBooleanProperty(false), new SimpleStringProperty(s)));
        }
    }

    private void refreshAdministratorPrvis() {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        refreshGlobalPhaseAministratorPrivs();
        refreshSchemaPrivsTemplates();
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void refreshGlobalPhaseAministratorPrivs() {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        refreshGlobalPrivsTemplates();
        refreshGlobalPrivsSelectorTable();
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void refreshGlobalPrivsSelectorTable() {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        ArrayList<String> privs = getActiveMysqlUser().getAssembleFinalPrivsFromWrappers(
                getDefaultGlobalPrivsTemplates().get(0).levelWrapper).getRawPrivileges(true);
        for (Map.Entry<SimpleBooleanProperty, SimpleStringProperty> en : globalPrivsSelectorTable.getItems()) {
            final boolean state = privs.contains(en.getValue().get());
            Platform.runLater(() -> en.getKey().set(state));
        }
        Platform.runLater(() -> {
            forceTriggerSavedUserEditsFlags();
        });
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void refreshGlobalPrivsTemplates() {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        PrivsLevelWrapper template = getDefaultGlobalPrivsTemplates().get(0).levelWrapper;
        PrivsLevelWrapper globalUserPrivs = PrivsLevelWrapper.loopWrappersForLevelEquality(getActiveMysqlUser().getPrivs(), template);
        PrivsLevelWrapper globalUserEditPrivs = PrivsLevelWrapper.loopWrappersForLevelEquality(getActiveMysqlUser().getEditPrivs(), template);

        //Assembled user privileges, this is the final privileges to be given to the user should changes be persisted
        PrivsLevelWrapper assembledUserPrivWrapper = getActiveMysqlUser().getAssembleFinalPrivsFromWrappers(template);

        final LinkedHashMap<SimplePrivsTypeValuesMatcher, Boolean> matcherTable = new LinkedHashMap(24);

        /**
         *
         * THIS SECTION IS FOR CONFIGURATION OF DEFAULT TEMPLATES SELECTION
         *
         *
         */
        for (SimplePrivsTypeValuesMatcher matcher : getDefaultGlobalPrivsTemplates()) {
            boolean state = matcher.levelWrapper.isSubSetPrivsOf(assembledUserPrivWrapper);
            matcherTable.put(matcher, state);
        }

        /**
         *
         * THIS SECTION IS FOR CONFIGURATION OF CUSTOM TEMPLATES SELECTION
         *
         */
        PrivsLevelWrapper onlyCustomPrivs = null;
        onlyCustomPrivs = PrivsLevelWrapper.getDifferenceInPrivsWrapper(globalUserPrivs, assembledUserPrivWrapper);
        if (onlyCustomPrivs != null) {
            Boolean bool = keepUserCustomState.get(getActiveMysqlUser());
            Boolean customRoleBooleanState = (bool == null ? onlyCustomPrivs.isSubSetPrivsOf(PrivsLevelWrapper.assembleFinalPrivsFromWrappers(
                    globalUserPrivs, globalUserEditPrivs, template, mvh.getVersion())) : bool);
            if (customRole == null) {
                customRole = new SimplePrivsTypeValuesMatcher(onlyCustomPrivs, new SimpleStringProperty("Custom"),
                        new SimpleStringProperty("custom roles"));
            } else {
                customRole.levelWrapper = onlyCustomPrivs;
            }
            matcherTable.put(customRole, customRoleBooleanState);
        }

        //HERE WE BIND IT TO THE UI SO WE CAN GET THE CHANGES
        Platform.runLater(() -> {
            for (Entry<SimplePrivsTypeValuesMatcher, Boolean> en : matcherTable.entrySet()) {
                if (en.getKey() == customRole && !hasCustomGlobalPrivsTemplate()) {
                    System.out.println("about to add custom role");
                    globalRoleTemplateTableview.getItems().add(customRole);
                }
                en.getKey().bool.setValue(en.getValue());
            }

            if (matcherTable.size() == getDefaultGlobalPrivsTemplates().size()) {
                globalRoleTemplateTableview.getItems().remove(customRole);
            }
        });
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void concatSchemaPrivsTemplates(SimplePrivsTypeValuesMatcher matcher) {
        List<String> ls = matcher.levelWrapper.getRawPrivileges(true);
        if (ls.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : ls) {
                sb.append(s);
                sb.append(',');
            }
            sb.delete(sb.length() - 1, sb.length());
            matcher.values[1].setValue(sb.toString());
        } else {
            matcher.values[1].setValue("none");
        }
    }

    private void refreshSchemaTemplate(SimplePrivsTypeValuesMatcher matcher) {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        PrivsLevelWrapper wrapper = getActiveMysqlUser().getAssembleFinalPrivsFromWrappers(matcher.levelWrapper);
        if (wrapper != null) {
            matcher.levelWrapper = wrapper;
            concatSchemaPrivsTemplates(matcher);
        }
        System.gc();
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }
    
    private void refreshSchemaPrivsTemplates() {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        
        Platform.runLater(() -> administrativeSchemaPrivilegsTableview.getItems().clear() );
        
        String selectedSchemaName = null;
        if (administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem() != null) {
            selectedSchemaName = administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem().values[0].getValue();
        }
        boolean checkSelection = true;
        //ArrayList<PrivsLevelWrapper> schemaPrivsList = new ArrayList(30);
        ArrayList<PrivsLevelWrapper> userPrivsList = new ArrayList(30);        
        userPrivsList.addAll(getActiveMysqlUser().getPrivs());
        // The ones in editting prvs but not user privs
        // This is also true when user privs is empty by default        
        userPrivsList.addAll(getActiveMysqlUser().getEditPrivs());
        
        for (PrivsLevelWrapper userpl : userPrivsList) {
            if (userpl.isGlobalPrivilege()) { continue; }
            if (selectedSchemaName != null && checkSelection) {
                if(Objects.equals(userpl.convertPrivsLevelToSqlStandard(), selectedSchemaName)){
                    checkSelection = false;
                    addNewSchemaLevelPrivilegeEntry(userpl, true);
                    continue;
                }
            }
            addNewSchemaLevelPrivilegeEntry(userpl, false);
        }
        
        //wipe userPrivsList
        userPrivsList.clear();
        
        Platform.runLater(() -> {
            forceTriggerSavedUserEditsFlags();
        });
        
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void addNewSchemaLevelPrivilegeEntry(PrivsLevelWrapper pl, boolean select) {
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        SimplePrivsTypeValuesMatcher matcher = null;

        for (SimplePrivsTypeValuesMatcher m : administrativeSchemaPrivilegsTableview.getItems()) {
            if (m.levelWrapper.equals(pl)) {
                matcher = m;
                break;
            }
        }

        if (matcher == null) {
            matcher = new SimplePrivsTypeValuesMatcher();
            matcher.levelWrapper = pl;
            refreshSchemaTemplate(matcher);
        }

        SimplePrivsTypeValuesMatcher fmatcher = matcher;
        Platform.runLater(() -> {
            administrativeSchemaPrivilegsTableview.getItems().add(fmatcher);
            if (select) {
                administrativeSchemaPrivilegsTableview.getSelectionModel().select(fmatcher);
            }
        });
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void deleteSchemaLevelPrivilegeEntry(SimplePrivsTypeValuesMatcher matcherToRemove) {

        if (administrativeSchemaPrivilegsTableview.getItems().remove(matcherToRemove)) {
            for (CheckBox cb : getAllSelectableSchemaLevelPrivileges()) {
                cb.setSelected(false);
            }

            if (matcherToRemove == administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem()) {
                administrativeSchemaPrivilegsTableview.getSelectionModel().select(null);
            }
        }

        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
            }

            @Override
            public void run() {
                System.out.println("delete entry workproc running");
                for (String prvs : matcherToRemove.levelWrapper.getRawPrivileges(true)) {
                    getActiveMysqlUser().revokePrivilege(matcherToRemove.levelWrapper, prvs);
                }
                System.out.println("delete entry workproc done");
            }
        });
    }

    private List<CheckBox> getAllSelectableSchemaLevelPrivileges() {
        return Arrays.asList(checkboxSelect, checkboxInsert, checkboxUpdate, checkboxDelete, checkboxExecute, checkboxShowView,
                checkboxCreate, checkboxAlter, checkboxPreferences, checkboxIndex, checkboxCreateView, checkboxCreateRoutine,
                checkboxAlterRoutine, checkboxEvent, checkboxDrop, checkboxTrigger, checkboxGrant, checkboxCreateTemporaryTable,
                checkboxLockTables);
    }

    private void validateSelectableCheckBoxStateSchemaPrivsOnActiveUser(CheckBox cb, MysqlUser user, PrivsLevelWrapper pl) {
        validateSelectableCheckBoxStateSchemaPrivsOnActiveUser(cb.isSelected(), user, pl, cb.getText().trim());
    }

    private void validateSelectableCheckBoxStateSchemaPrivsOnActiveUser(boolean state, MysqlUser user, PrivsLevelWrapper pl, String... priv) {        
        System.out.println(new Exception().getStackTrace()[0].getMethodName());
        long before = System.currentTimeMillis();
        if (state) {
            user.grantPrivilege(pl, priv);
        } else {
            user.revokePrivilege(pl, priv);
        }
        long after = System.currentTimeMillis();
        System.out.println("TIME TAKEN = " + String.valueOf(after - before));
    }

    private void toggleAllSelectableSchemaLevelPrivs(boolean state) {
        SimplePrivsTypeValuesMatcher matcher = administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem();
        ArrayList<String> prvs = new ArrayList(21);
        for (CheckBox c : getAllSelectableSchemaLevelPrivileges()) {
            if (!Objects.equals(c.isSelected(), state)) {
                prvs.add(c.getText().trim());
                c.setSelected(state);
            }
        }
        String[] stringarray = new String[]{};
        stringarray = prvs.toArray(stringarray);
        validateSelectableCheckBoxStateSchemaPrivsOnActiveUser(state, getActiveMysqlUser(), matcher.levelWrapper, stringarray);
        refreshSchemaTemplate(matcher);
        forceTriggerSavedUserEditsFlags();
        administrativeSchemaPrivilegsTableview.getProperties().put(TableViewSkinBase.REFRESH, Boolean.TRUE);
    }

    private void selectAllSelectableSchemaLevelPrivs() {
        toggleAllSelectableSchemaLevelPrivs(true);
    }

    private void deSelectAllSelectableSchemaLevelPrivs() {
        toggleAllSelectableSchemaLevelPrivs(false);
    }

    private void showSchemaLevelPrivilegeEntryCreator() {
        if (getActiveMysqlUser() == null) {
            return;
        }
        try {
            Stage s = new Stage(StageStyle.DECORATED);
            s.initModality(Modality.APPLICATION_MODAL);
            s.initOwner(getStage());
            s.setWidth(800);
            s.setHeight(600);
            s.setResizable(false);
            FXMLLoader loader = new FXMLLoader(MainUI.class.getResource("UserManagerSchemaPrvDefinition.fxml").toURI().toURL());
            loader.load();
            AnchorPane ap = loader.getRoot();
            Label genLabel = (Label) ap.lookup("#topic");
            genLabel.setText(new StringBuilder("Select the Schema fo which the user ")
                    .append(getActiveMysqlUser().getOnlyName())
                    .append(" will have the privileges you want to define").toString());

            genLabel = (Label) ap.lookup("#matchingText");
            genLabel.setText(new StringBuilder("This rule will apply to schemas that match the given name or pattern.")
                    .append(System.lineSeparator())
                    .append("You may use _ and % as wildcards in a pattern")
                    .append(System.lineSeparator())
                    .append("Escape these character with '\' in case you wan their literal value").toString());

            TextField tf = (TextField) ap.lookup("#matchingField");

            ArrayList<String> ls = new ArrayList(100);
            for (Object o : getController().getSelectedConnectionSession().get().getConnectionParam().getDatabases()) {
                ls.add(o.toString());
            }
            final ChoiceBox<String> databases = (ChoiceBox) ap.lookup("#databaseChoicebox");
            databases.getItems().addAll(ls);

            final RadioButton allSchema = (RadioButton) ap.lookup("#radioAllSchema");
            final RadioButton selectedSchema = (RadioButton) ap.lookup("#radioSelectedSchema");
            final RadioButton matchPattern = (RadioButton) ap.lookup("#radioMatchPattern");

            ToggleGroup tg = new ToggleGroup();
            allSchema.setToggleGroup(tg);
            matchPattern.setToggleGroup(tg);
            selectedSchema.setToggleGroup(tg);

            tf.disableProperty().bind(matchPattern.selectedProperty().not());
            databases.disableProperty().bind(selectedSchema.selectedProperty().not());

            Button okayButton = (Button) ap.lookup("#okayButton");
            Button cancelButon = (Button) ap.lookup("#cancelButon");

            EventHandler<ActionEvent> onAction = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (event.getSource() == okayButton) {
                        Recycler.addWorkProc(new WorkProc() {
                            @Override
                            public void run() {
                                PrivsLevelWrapper pl = new PrivsLevelWrapper(null, selectedSchema.isSelected()
                                        ? databases.getSelectionModel().getSelectedItem()
                                        : (allSchema.isSelected() ? "%" : tf.getText().trim()), mvh.getVersion());
                                if (PrivsLevelWrapper.loopWrappersForLevelEquality(getActiveMysqlUser().getEditPrivs(), pl) == null) {
                                    getActiveMysqlUser().grantPrivilege(pl, new String[]{null});
                                }
                                addNewSchemaLevelPrivilegeEntry(pl, true);
                            }
                        });
                    }
                    s.close();
                }
            };

            okayButton.setOnAction(onAction);
            cancelButon.setOnAction(onAction);

            s.setScene(new Scene(ap));
            s.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onNewSchemaLevelEntrySelected(SimplePrivsTypeValuesMatcher valuesMatcher) {
        /**
         * Good code wise i need call this first to disable all checked but for
         * performance wise i comment and re-disable them in the loop below
         * deSelectAllSelectableSchemaLevelPrivs();
         *
         */
        if (valuesMatcher == null) {
            return;
        }

        StringBuilder sb = new StringBuilder(100);
        sb.append("The user '")
                .append(getActiveMysqlUser().toString())
                .append("' will have the following rights to the schema '")
                .append(valuesMatcher.levelWrapper.convertPrivsLevelToSqlStandard())
                .append("':");
        schemaPrivInfoLabel.setText(sb.toString());

        List<String> list = valuesMatcher.levelWrapper.getRawPrivileges(true);
        for (CheckBox cb : getAllSelectableSchemaLevelPrivileges()) {
            //boolean isGrantCheck = cb.getText().trim().equals(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            cb.setSelected(list.contains(cb.getText().trim()));
        }
    }

    /**
     *
     * ============================================================================================================================================================
     *
     * ADMINISTRATIVE SECTION CODES END HERE
     *
     * ============================================================================================================================================================
     *
     */
    private void passwordExpiryWork() {
        if (passwordExpiry == null) {
            passwordExpiry = new ComboBox<>();
        }
        passwordExpiry.getSelectionModel().select(Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE);
        passwordExpiry.getSelectionModel().selectedItemProperty().
                addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                        if (t1 == null) {
                            return;
                        }
                        if (t1.equals(Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE_INTERVAL_N_DAY)) {
                            String s = ((MainController) getController()).showInput("PASSWORD DURATION", "ENTER DURATION OF DAYS", null, "100", getStage());
                            StringBuilder sb = new StringBuilder();
                            sb.append("PASSWORD EXPIRE INTERVAL");
                            sb.append(s);
                            sb.append("DAY");
                            passwordExpiry.getSelectionModel().select(sb.toString());
                        } else if (t1.equals(Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE_DEFAULT) && mvh.passwordExpiresAfter1yearOnDefault()) {
                            ((MainController) getController()).showInfo("MYSQL VERSION NOTICE", Flags.MYSQL_ACCOUNT_PASSWORD_EXPIRE_DEFAULT
                                    + " on your current mysql version is 1 year valid, which means you have to reset your password after 365 days.", null);
                        }
                    }
                });
    }

    private void hostChooserWork() {
        hostChooser.setPromptText("<enter host>");
        hostChooser.setEditable(true);
        hostChooser.setFocusTraversable(true);
        hostChooser.setConverter(new StringConverter<String>() {
            @Override
            public String toString(String t) {
                return t;
            }

            @Override
            public String fromString(String string) {
                return string;
            }
        });
        hostChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> ov,
                    String t, String t1) {
                if (t1 == null) {
                    return;
                }
                final boolean wasSystemEdit = isSystemEdit;
                Recycler.addWorkProc(new WorkProc<String>(t1) {
                    @Override
                    public void updateUI() {
                    }

                    @Override
                    public void run() {

                        if (!wasSystemEdit) {
                            getActiveMysqlUser().modifyHost(getAddData());
                            if (getActiveMysqlUser().isNewUser()) {
                                Platform.runLater(() -> usersTableview.refresh());
                            }
                            Platform.runLater(() -> forceTriggerSavedUserEditsFlags());
                        }

                        for (String s : hostChooser.getItems()) {
                            if (s.equals(getAddData())) {
                                return;
                            }
                        }

                        Platform.runLater(() -> {
                            hostChooser.getItems().add(0, getAddData());
                            hostChooser.getSelectionModel().select(getAddData());
                        });
                    }
                });

            }
        });
    }

    public Tab getTab() {
        return userManagerTab;
    }

    private void onNewUserSelectionFromTableview(MysqlUser newMysqlUserSelected) {
        if (newMysqlUserSelected == null) { return; }
        tabPaneHeader.setText("Details for account " + newMysqlUserSelected.getUserAccountName());
        fillUpGenDataUiScreen(newMysqlUserSelected);
        forceTriggerSavedUserEditsFlags();
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void run() {
                //System.out.println("started onNewSelection from " + Thread.currentThread().getName());
                try { Thread.sleep(50); } catch (Exception e) {}
                refreshAdministratorPrvis();
            }
        });
    }

    private void onLoginTab() {
    }

    private void onLimitsTab() {
    }

    private void onAdministrativeTab() {
    }

    private void onSchemaPrivsTab() {
    }

    public void init(Tab t) {
        if (t != userManagerTab) {
            userManagerTab = t;
        }
        t.setContent(getUI());
        t.setGraphic(tabImage);
        userManagerTab.selectedProperty().addListener(tabSelectionListener);

        Platform.runLater(() -> usermanagerEssentialInitializer());
    }

    public void closeController() {
        userManagerTab.selectedProperty().removeListener(tabSelectionListener);
        ((MainController) getController()).getCurrentConnectionParentTab().selectedProperty().removeListener(conneciontabSelectionListener);
        getStage().focusedProperty().removeListener(focusListener);
        getUI().boundsInLocalProperty().removeListener(boundListener);
        getUI().disableProperty().removeListener(disableListener);
        unSavedUserEdits.removeListener(unSavedUserEditListener);
        getUsers = null;
        userManagerTab = null;

        System.gc();
        System.gc();
        System.gc();
        System.gc();
    }

    private void onDoneInitializeOnBackground() {
        fillUpUserList();
        getUI().boundsInLocalProperty().addListener(boundListener);
        getStage().focusedProperty().addListener(focusListener);
        getUI().disableProperty().addListener(disableListener);
        unSavedUserEdits.addListener(unSavedUserEditListener);
        ((MainController) getController()).getCurrentConnectionParentTab().selectedProperty().addListener(conneciontabSelectionListener);
        schemaLevelInfoLabel.setText("Schema and Host fields may use % and _ wildcards. \nThe Server will match specific entries before wildcarded ones.");
        passwordExpiry.disableProperty().bind(mvh.getPasswordExpirable().not());
        passwordExpiry.getItems().addAll(mvh.getPassWordExpireOptions());
        installDefaultOnGenPrivsTemplate();
        populatePrivsSelectorTable();
        initializeSuccess.set(true);
    }

    private void usermanagerEssentialInitializer() {
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
                onDoneInitializeOnBackground();
                String name = ((MainController)getController()).getTabControllerBySession(getController().getSelectedConnectionSession().get()).getTabName();
                instanceText.setText(name.toLowerCase());
            }

            @Override
            public void run() {
                try {
//                    if(true){
//                        throw new RuntimeException("FRIENDLY ERROR");
//                    }
                    DBService service = getController().getSelectedConnectionSession().get().getService();
                    String query = "SHOW VARIABLES WHERE (`Variable_name` LIKE 'validate_password%') OR (`Variable_name` = 'version');";
                    ResultSet resultset = (ResultSet) service.execute(query);
                    String mysqlVersionNummber = "5.7";
                    while (resultset.next()) {
                        String variableName = resultset.getString(1);
                        String variableValue = resultset.getString(2);

                        if (variableName.equalsIgnoreCase("version")) {
                            int index = variableValue.indexOf('-');
                            if (index > -1) {
                                mysqlVersionNummber = variableValue.substring(0, index);
                            } else {
                                mysqlVersionNummber = variableValue;
                            }
                        } else if (variableName.equalsIgnoreCase("validate_password_length")) {
                            validate_password_length = Integer.valueOf(variableValue);
                        } else if (variableName.equalsIgnoreCase("validate_password_policy")) {
                            validate_password_policy = variableValue;
                        } else if (variableName.equalsIgnoreCase("validate_password_mixed_case_count")) {
                            validate_password_mixed_case_count = Integer.valueOf(variableValue);
                        } else if (variableName.equalsIgnoreCase("validate_password_number_count")) {
                            validate_password_number_count = Integer.valueOf(variableValue);
                        } else if (variableName.equalsIgnoreCase("validate_password_special_char_count")) {
                            validate_password_special_char_count = Integer.valueOf(variableValue);
                        }
                    }
                    mvh = new MysqlVersionHandler(mysqlVersionNummber);
                    callUpdate = true;
                } catch (Exception exception) {
                    exception.printStackTrace();
                    ((MainController) getController()).showError("USER MANAGER ERROR", "USER MANAGER failed to initialize \n\n \t Please restart", exception);
                }
            }
        });
    }

    /**
     *
     * ====================================================================================================================================================
     * MYSQLUSER ORIENTTED CODE STARTS HERE
     * ====================================================================================================================================================
     *
     */
    private void usersTableviewWorks() {

        usersTableview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MysqlUser>() {
            @Override
            public void changed(ObservableValue<? extends MysqlUser> observable, MysqlUser oldValue, MysqlUser newMysqlUserSelected) {
                onNewUserSelectionFromTableview(newMysqlUserSelected);
                tabPane.getSelectionModel().selectFirst();
            }
        });

        TableColumn<MysqlUser, String> nameColumn = (TableColumn<MysqlUser, String>) usersTableview.getColumns().get(0);
        nameColumn.setCellFactory(new Callback<TableColumn<MysqlUser, String>, TableCell<MysqlUser, String>>() {
            @Override
            public TableCell<MysqlUser, String> call(TableColumn<MysqlUser, String> param) {
                return new TableCell<MysqlUser, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText("");
                        } else {
                            setText(item);
                            setAlignment(Pos.CENTER_LEFT);
                            setMaxWidth(Double.MAX_VALUE);
                        }
                    }
                };
            }
        });

        nameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MysqlUser, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MysqlUser, String> param) {
                return new SimpleStringProperty(param.getValue().getOnlyName());
            }
        });

        TableColumn<MysqlUser, String> hostColumn = (TableColumn<MysqlUser, String>) usersTableview.getColumns().get(1);
        hostColumn.setCellFactory(new Callback<TableColumn<MysqlUser, String>, TableCell<MysqlUser, String>>() {
            @Override
            public TableCell<MysqlUser, String> call(TableColumn<MysqlUser, String> param) {
                return new TableCell<MysqlUser, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText("");
                        } else {
                            setText(item);
                            setAlignment(Pos.CENTER_LEFT);
                            setMaxWidth(Double.MAX_VALUE);
                        }
                    }
                };
            }
        });

        hostColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MysqlUser, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MysqlUser, String> param) {
                return new SimpleStringProperty(param.getValue().getHost());
            }
        });
    }

    private boolean hasSelectedUser() {
        return getActiveMysqlUser() != null;
    }

    private MysqlUser getActiveMysqlUser() {
        return usersTableview.getSelectionModel().getSelectedItem();
    }

    private WorkProc<Boolean> mergeOldNewUsersProc = new WorkProc<Boolean>(Boolean.FALSE) {

        Integer selectedIndex = -1;
        Integer tabSelectedIndex = -1;

        @Override
        public void updateUI() {
        }

        @Override
        public void run() {
            ChangeListener<Number> tempIndexSelectionListener = new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    selectedIndex = newValue.intValue();
                }
            };
            usersTableview.getSelectionModel().selectedIndexProperty().addListener(tempIndexSelectionListener);
            selectedIndex = usersTableview.getSelectionModel().getSelectedIndex();
            tabSelectedIndex = tabPane.getSelectionModel().getSelectedIndex();

            System.out.println("starting background refresh");
            //get unsaved users
            ArrayList<MysqlUser> unsavedUsers = new ArrayList(20);
            for (MysqlUser mu : usersTableview.getItems()) {
                if (mu.isNewUser() || mu.isGenInformationModified() || !mu.isUserHavingEmptyEdittingPrivs()) {
                    unsavedUsers.add(mu);
                }
            }

            getUsers.run();

            if (getUsers.callUpdate) {
                for (MysqlUser mu : unsavedUsers) {
                    if (mu.isNewUser()) {
                        getUsers.getAddData().add(mu);
                    } else {
                        for (MysqlUser msu : getUsers.getAddData()) {
                            if (MysqlUser.isSameUser(mu, msu)) {
                                MysqlUser.mergeUsersTogether(msu, mu);
                                break;
                            }
                        }
                    }
                }

                Platform.runLater(() -> {
                    getUsers.updateUI();
                    usersTableview.getSelectionModel().select(selectedIndex);
                    tabPane.getSelectionModel().select(tabSelectedIndex);
                    setAdditionalData(Boolean.FALSE);
                });
            } else {
                setAdditionalData(Boolean.FALSE);
            }

            usersTableview.getSelectionModel().selectedIndexProperty().removeListener(tempIndexSelectionListener);
            System.gc();

            System.out.println("finished background refresh");
        }
    };

    private WorkProc<ArrayList<MysqlUser>> getUsers = new WorkProc<ArrayList<MysqlUser>>(new ArrayList(20)) {

        @Override
        public void updateUI() {
            synchronized (getUsers) {
                usersTableview.getItems().clear();
                keepUserCustomState.clear();
                usersTableview.getSelectionModel().select(-1);
                usersTableview.getSelectionModel().clearSelection();
                usersTableview.getItems().addAll(getAddData());
                getAddData().clear();
                System.gc();
                System.gc();
            }
            getUI().setDisable(false);
            installHostsFromUsersAvailable();
            System.gc();
            System.gc();
        }

        @Override
        public void run() {
            synchronized (getUsers) {
                getAddData().clear();
                DBService service = getController().getSelectedConnectionSession().get().getService();
                MysqlUser.mysqlVersionHandler = mvh;
                try {
                    ResultSet rs = (ResultSet) service.execute(service.getUsersAccountSQL());
                    while (rs.next()) {
                        getAddData().add(new MysqlUser(service, rs.getString(1), "", rs.getString(2)).prepareUser());
                    }
                    rs.close();
                    callUpdate = true;
                } catch (SQLException ex) {
                    callUpdate = false;
                    org.slf4j.LoggerFactory.getLogger(UserManagerController.class).error(ex.getLocalizedMessage(), ex);
                    Platform.runLater(() -> {
                        ((MainController) getController()).showError("USER MANAGEMENT ERROR", "You do not have the right privileges for this operation."
                                + "\n contact your database administrator for help", ex);
                    });
                }
            }
        }
    };

    //here we fill up userChooser combo box with the users available in mysql server
    private void fillUpUserList() {
        getUI().setDisable(true);
        Recycler.addWorkProc(getUsers);
    }

    private void installHostsFromUsersAvailable() {
        if (hostChooser.getItems().isEmpty()) {
            for (MysqlUser mu : usersTableview.getItems()) {
                if (!hostChooser.getItems().contains(mu.getHost())) {
                    hostChooser.getItems().add(mu.getHost());
                }
            }
        }
    }

    private void revokeAllUserPrivs(MysqlUser user) {
        getUI().setDisable(true);
        Recycler.addWorkProc(new WorkProc() {

            @Override
            public void updateUI() {
                getUI().setDisable(false);
                onNewUserSelectionFromTableview(user);
            }

            @Override
            public void run() {
                callUpdate = true;
                user.refreshUser();
            }
        });
    }

    /**
     *
     * =======================================================================================================================================
     * MYSQL USER ORIENTED CODE ENDS HERE
     * =======================================================================================================================================
     *
     */
    public void destroyController() {
    }

    private void clearInformationScreen() {
        passwordField.setStyle(null);
        rePasswordField.setStyle(null);
        usernameField.setText("");
        maxQueriesField.setText("0");
        maxConnField.setText("0");
        maxUpdatesField.setText("0");
        conConnField.setText("0");
        passwordField.setText("");
        rePasswordField.setText("");
        passwordField.setPromptText("**********");
        passwordField.setStyle(".textfield{}");
        hostChooser.getSelectionModel().select(null);
    }

    private void fillUpGenDataUiScreen(MysqlUser selectedItem) {
        //System.out.println("fillUpGenData " + Thread.currentThread().getName());
        if (selectedItem == null) {
            return;
        }
        isSystemEdit = true;
        clearInformationScreen();
        hostChooser.getSelectionModel().select(selectedItem.getHost());
        usernameField.setText(selectedItem.getOnlyName());
//        if (selectedItem.isAccountLocked()) {
//            lockLabel.setVisible(true);
//            unlockAccountButton.setVisible(true);
//            coreScreen.setDisable(true);
//        } else {
//            lockLabel.setVisible(false);
//            unlockAccountButton.setVisible(false);
//            coreScreen.setDisable(false);
//        }
        if (mvh.canSetResourceLimit()) {
            maxQueriesField.setText(selectedItem.getPropsValue(Flags.MAX_QUERIES_PER_HOUR));
            maxUpdatesField.setText(selectedItem.getPropsValue(Flags.MAX_UPDATES_PER_HOUR));
            maxConnField.setText(selectedItem.getPropsValue(Flags.MAX_CONNECTIONS_PER_HOUR));
            conConnField.setText(selectedItem.getPropsValue(Flags.MAX_USER_CONNECTIONS));
        }

        passwordField.setText(selectedItem.getPassPhrase());
        passowrdExpiryLabel.setVisible(selectedItem.getGenInfoValue(Flags.MYSQL_ACCOUNT_PASSWORD, "N").equals("Y"));

        isSystemEdit = false;
    }

    private void forceTriggerSavedUserEditsFlags() {
        System.gc();
        System.gc();
        System.gc();
        System.gc();
        unSavedUserEdits.invalidate();
        unSavedUserEdits.get();
    }

    private void cancelChanges() {
        if (hasSelectedUser()) {
            MysqlUser lastKnownUser = getActiveMysqlUser();
            keepUserCustomState.put(lastKnownUser, null);
            lastKnownUser.resetInState();
            usersTableview.getSelectionModel().select(null);
            Platform.runLater(() -> usersTableview.getSelectionModel().select(lastKnownUser));
        }
    }

    private final void clickListeners() {
        expirePasswordButton.disableProperty().bind(initializeSuccess.not().or(Bindings.createBooleanBinding(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return getActiveMysqlUser() == null || getActiveMysqlUser().isNewUser();
            }
        }, usersTableview.getSelectionModel().selectedItemProperty())).or(passowrdExpiryLabel.visibleProperty()));
        addNewUser.disableProperty().bind(initializeSuccess.not());
        refreshUsers.disableProperty().bind(addNewUser.disableProperty());

        cancelChangesButton.disableProperty().bind(initializeSuccess.not().or(unSavedUserEdits.not()));
        saveChangesButton.disableProperty().bind(cancelChangesButton.disableProperty());
        revokAllSchemaLevelPrivsButton.disableProperty().bind(cancelChangesButton.disableProperty());
        deleteUser.disableProperty().bind(usersTableview.getSelectionModel().selectedItemProperty().isNull());
        for (Tab t : tabPane.getTabs()) {
            t.disableProperty().bind(deleteUser.disableProperty());
        }

        schemaDeleteEntryButton.disableProperty().bind(administrativeSchemaPrivilegsTableview.getSelectionModel().selectedItemProperty().isNull());
        schemaPrivInfoLabel.visibleProperty().bind(schemaDeleteEntryButton.disableProperty().not());
        box.disableProperty().bind(schemaDeleteEntryButton.disableProperty());
        box1.disableProperty().bind(schemaDeleteEntryButton.disableProperty());
        box2.disableProperty().bind(schemaDeleteEntryButton.disableProperty());
        schemaSelectAllButton.disableProperty().bind(schemaDeleteEntryButton.disableProperty());
        schemaUnselectAllButton.disableProperty().bind(schemaDeleteEntryButton.disableProperty());

        //addNewUser
        EventHandler<ActionEvent> action = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (t.getSource() == addNewUser) {
                    addUser();
                } else if (t.getSource() == deleteUser) {
                    DialogResponce dr = ((MainController) getController()).showConfirm("DELETE DIALOG", "ARE YOU SURE YOU WANT TO DELETE "
                            + getActiveMysqlUser().getOnlyName().toUpperCase());
                    if (dr == DialogResponce.OK_YES || dr == DialogResponce.YES_TO_ALL) {
                        if (getActiveMysqlUser().deleteMe()) {
                            usersTableview.getItems().remove(getActiveMysqlUser());
                            flushPrivileges();
                            Platform.runLater(() -> usersTableview.getSelectionModel().select(null));
                        }
                    }
                } else if (t.getSource() == cancelChangesButton) {
                    cancelChanges();
                } else if (t.getSource() == saveChangesButton) {
                    saveChanges(true);
                } else if (t.getSource() == unlockAccountButton) {
                    toggleAccountStatus(false);
                } else if (t.getSource() == lockAccountButton) {
                    toggleAccountStatus(true);
                } else if (t.getSource() == schemaAddEntryButton) {
                    showSchemaLevelPrivilegeEntryCreator();
                } else if (t.getSource() == schemaDeleteEntryButton) {
                    deleteSchemaLevelPrivilegeEntry(administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem());
                } else if (t.getSource() == schemaSelectAllButton) {
                    selectAllSelectableSchemaLevelPrivs();
                } else if (t.getSource() == schemaUnselectAllButton) {
                    deSelectAllSelectableSchemaLevelPrivs();
                } else if (t.getSource() == revokAllGlobalLevelPrivsButton
                        || t.getSource() == revokAllSchemaLevelPrivsButton) {
                    revokeAllUserPrivs(getActiveMysqlUser());
                    //} else if (t.getSource() == revokAllSchemaLevelPrivsButton) {
                    //revokeAllSchemaPrivs(administrativeSchemaPrivilegsTableview.getSelectionModel().getSelectedItem());
                } else if (t.getSource() == refreshUsers) {
                    fillUpUserList();
                } else if (t.getSource() == expirePasswordButton) {

                    expirePasswordButton.setGraphic(smallProgressIndicator);
                    Recycler.addWorkProc(new WorkProc() {

                        boolean expiryState = false;

                        @Override
                        public void updateUI() {
                            expirePasswordButton.setGraphic(null);
                            if (expiryState) {
                                onNewUserSelectionFromTableview(getActiveMysqlUser());
                            } else {
                                ((MainController) getController()).showError("USER MANAGER ALERT", "Could not EXPIRE Password", null);
                            }

                        }

                        @Override
                        public void run() {
                            callUpdate = true;
                            expiryState = getActiveMysqlUser().expireUserPassWord();
                            if (expiryState) {
                                getActiveMysqlUser().prepareUser();
                            }
                        }
                    });
                }
                forceTriggerSavedUserEditsFlags();
            }
        };
        //unlockAccountButton.setOnAction(action);
        //lockAccountButton.setOnAction(action);
        addNewUser.setOnAction(action);
        deleteUser.setOnAction(action);
        cancelChangesButton.setOnAction(action);
        saveChangesButton.setOnAction(action);
        refreshUsers.setOnAction(action);
        revokAllGlobalLevelPrivsButton.setOnAction(action);
        revokAllSchemaLevelPrivsButton.setOnAction(action);
        schemaUnselectAllButton.setOnAction(action);
        schemaSelectAllButton.setOnAction(action);
        schemaDeleteEntryButton.setOnAction(action);
        schemaAddEntryButton.setOnAction(action);
        expirePasswordButton.setOnAction(action);

        passwordFieldWorks();
        accountLimitsFieldWorks();

        usernameField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);

                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    getActiveMysqlUser().modifyUserName(usernameField.getText());
                    if (getActiveMysqlUser().isNewUser()) {
                        Platform.runLater(() -> usersTableview.refresh());
                    }
                    forceTriggerSavedUserEditsFlags();

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);
                }

            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!isSystemEdit && newValue != null) {
                    if (!waiter.getAddData().isQueuedInBackground()) {
                        waiter.getAddData().setQueuedInBackground(true);
                        Recycler.addWorkProc(waiter);
                    }

                    waiter.getAddData().setWaitInBackground(true);
                }
            }
        });

    }

    private void persistRequirablesOntoUser(MysqlUser user) {
        if (Objects.equals(rePasswordField.getText(), passwordField.getText()) && !Objects.equals("", passwordField.getText())) {
            user.modifyPassword(passwordField.getText());
        }

        if (!Objects.equals("", usernameField.getText())) {
            user.modifyUserName(usernameField.getText());
        }

        if (!Objects.equals(user.getHost(), hostChooser.getSelectionModel().getSelectedItem())) {
            user.modifyHost(hostChooser.getSelectionModel().getSelectedItem());
        }

        user.modifyPassWordExpiry(passwordExpiry.getSelectionModel().getSelectedItem());

        if (!Objects.equals(user.getPropsValue(Flags.MAX_QUERIES_PER_HOUR), maxQueriesField.getText())) {
            user.modifyMaxes(Flags.MAX_QUERIES_PER_HOUR, maxQueriesField.getText());
        }

        if (!Objects.equals(user.getPropsValue(Flags.MAX_UPDATES_PER_HOUR), maxUpdatesField.getText())) {
            user.modifyMaxes(Flags.MAX_UPDATES_PER_HOUR, maxUpdatesField.getText());
        }

        if (!Objects.equals(user.getPropsValue(Flags.MAX_CONNECTIONS_PER_HOUR), maxConnField.getText())) {
            user.modifyMaxes(Flags.MAX_CONNECTIONS_PER_HOUR, maxConnField.getText());
        }

        if (!Objects.equals(user.getPropsValue(Flags.MAX_USER_CONNECTIONS), conConnField.getText())) {
            user.modifyMaxes(Flags.MAX_USER_CONNECTIONS, conConnField.getText());
        }
    }

    private void refreshUsersKeepingChanges() {
        if (!mergeOldNewUsersProc.getAddData()) {
            mergeOldNewUsersProc.setAdditionalData(Boolean.TRUE);
            Recycler.addWorkProc(mergeOldNewUsersProc);
        }
    }

    private void saveChanges(final boolean prepareOnSave) {
        final MysqlUser userToSave = getActiveMysqlUser();
        if (!Objects.equals(rePasswordField.getText(), passwordField.getText()) || usernameField.getText().isEmpty()
                || hostChooser.getSelectionModel().getSelectedItem() == null) {
            ((MainController) getController()).showInfo("USER INFORMATION", "you need to input NAME, HOST and CONFIRM PASSWORD", null);
            return;
        }

        if (maxConnField.getText().isEmpty() || maxQueriesField.getText().isEmpty()
                || maxUpdatesField.getText().isEmpty() || conConnField.getText().isEmpty()) {
            ((MainController) getController()).showInfo("USER INFORMATION", "Please fill in all ACCOUNTS LIMITS", null);
            return;
        }

        if (passwordField.getBorder() == null ? userToSave.isNewUser() : passwordField.getBorder().getStrokes().get(0).getBottomStroke().equals(Color.TOMATO)) {
            ((MainController) getController()).showInfo("USER INFORMATION", "\tPASSWORD ERROR.. \n\rplease fufill password requirements on both fields\t\n\r",
                    null);
            return;
        }

        //persistRequirablesOntoUser(userToSave);
        //if (userToSave.isGenInformationModified()) {
        getUI().setDisable(true);

        Recycler.addWorkProc(new WorkProc() {
            boolean hasError = false;
            String msg = "";

            @Override
            public void updateUI() {
                if (hasError) {
                    ((MainController) getController()).showError("ERROR NOTIFICATION", msg, null);
                    getUI().setDisable(false);
                    hasError = false;
                    msg = "";
                } else {
                    getUI().setDisable(false);
                    keepUserCustomState.put(userToSave, null);
                    onNewUserSelectionFromTableview(userToSave);
                }
            }

            @Override
            public void run() {
                callUpdate = true;
                for (int i = 0; i < usersTableview.getItems().size(); i++) {
                    MysqlUser mu = usersTableview.getItems().get(i);
                    if (mu.getUserAccountName().equals(userToSave.getUserAccountName())
                            && mu != userToSave) {
                        hasError = true;
                        if (userToSave.isNewUser()) {
                            msg = "User already exist in your database";
                        } else {
                            msg = "User details match another user in database"
                                    + "\n"
                                    + "Please check user at row " + String.valueOf(i + 1);
                        }
                        return;
                    }
                }

                if (userToSave.getOnlyName().contains("<")
                        | userToSave.getOnlyName().contains(">")) {
                    hasError = true;
                    msg = "User name should not contain \"<\" or \">\"";
                    return;
                }

                if (userToSave.saveModifiedChanges()) {
                    flushPrivileges();
                    if (prepareOnSave) {
                        userToSave.refreshUser();
                    }
                } else {
                    hasError = true;
                    msg = userToSave.getLatestErrorMessage();
                }
            }
        });
        //}
    }

    private boolean hasUnSavedUser() {
        for (MysqlUser mu : usersTableview.getItems()) {
            if (mu.isNewUser() || mu.isGenInformationModified() || !mu.isUserHavingEmptyEdittingPrivs()) {
                return true;
            }
        }
        return false;
    }

    private void addUser() {
        final MysqlUser freshUser = new MysqlUser(getController().getSelectedConnectionSession().get().getService());
        usersTableview.getItems().add(freshUser);

        Recycler.addWorkProc(new WorkProc() {

            MysqlUser getFuser() {
                for (MysqlUser mu : usersTableview.getItems()) {
                    if (mu.isNewUser()) {
                        return mu;
                    }
                }
                return null;
            }

            @Override
            public void run() {
                MysqlUser fuser = getFuser();

                if (fuser != freshUser) {
                    Platform.runLater(() -> {
                        usersTableview.getSelectionModel().select(fuser);
                        usersTableview.getItems().remove(freshUser);
                    });
                }
            }
        });
    }

    public void toggleAccountStatus(boolean lock) {
        DialogResponce dp = null;
//        if (!saveChangesButton.isDisable()) {
//            dp = showConfirm("ACCOUNT STATUS NOTE", "All Changes will be discarded. "
//                    + "Do you want to save changes first ?");
//            if (dp != DialogResponce.OK_YES || dp != DialogResponce.YES_TO_ALL) {
//                saveChanges(false);
//            }
//        }
        cancelChanges();
        dp = ((MainController) getController()).showConfirm("ACCOUNT STATUS NOTE", "Are you sure you wante to "
                + (lock ? "LOCK " : "UNLOCK") + "?");
        if (dp == DialogResponce.OK_YES || dp == DialogResponce.YES_TO_ALL) {
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                    fillUpGenDataUiScreen(getActiveMysqlUser());
                    getUI().setDisable(false);
                }

                @Override
                public void run() {
                    callUpdate = true;
                    getActiveMysqlUser().modifyAccountStatus(lock);
                    getActiveMysqlUser().prepareUser();
                    System.gc();
                }
            });
            getUI().setDisable(true);
        }
    }

    private void flushPrivileges() {
        try {
            getController().getSelectedConnectionSession().get().getService().execute(Flags.FLUSH_PRIVILEGE_FOR_USER_MANAGER);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void accountLimitsFieldWorks() {

        EventHandler<KeyEvent> numberFieldTextChecker = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if ((t.getEventType() == KeyEvent.KEY_TYPED)
                        ? !Character.isDigit(t.getCharacter().charAt(0))
                        : !t.getCode().isDigitKey()) {
                    t.consume();

                }
            }
        };
        maxConnField.addEventFilter(KeyEvent.ANY, numberFieldTextChecker);
        conConnField.addEventFilter(KeyEvent.ANY, numberFieldTextChecker);
        maxQueriesField.addEventFilter(KeyEvent.ANY, numberFieldTextChecker);
        maxUpdatesField.addEventFilter(KeyEvent.ANY, numberFieldTextChecker);

        EventHandler<MouseEvent> clickSelectAllField = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ((TextField) event.getSource()).selectAll();
            }
        };
        maxConnField.setOnMouseClicked(clickSelectAllField);
        conConnField.setOnMouseClicked(clickSelectAllField);
        maxQueriesField.setOnMouseClicked(clickSelectAllField);
        maxUpdatesField.setOnMouseClicked(clickSelectAllField);

        conConnField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);

                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    getActiveMysqlUser().modifyMaxes(Flags.MAX_USER_CONNECTIONS, conConnField.getText());
                    forceTriggerSavedUserEditsFlags();

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);
                }

            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!isSystemEdit) {
                    if (newValue == null || newValue.isEmpty()) {
                        return;
                    }

                    if (!waiter.getAddData().isQueuedInBackground()) {
                        waiter.getAddData().setQueuedInBackground(true);
                        Recycler.addWorkProc(waiter);
                    }

                    waiter.getAddData().setWaitInBackground(true);
                }
            }
        });
        maxConnField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);

                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    getActiveMysqlUser().modifyMaxes(Flags.MAX_CONNECTIONS_PER_HOUR, maxConnField.getText());
                    forceTriggerSavedUserEditsFlags();

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);
                }

            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!isSystemEdit) {
                    if (newValue == null || newValue.isEmpty()) {
                        return;
                    }

                    if (!waiter.getAddData().isQueuedInBackground()) {
                        waiter.getAddData().setQueuedInBackground(true);
                        Recycler.addWorkProc(waiter);
                    }

                    waiter.getAddData().setWaitInBackground(true);
                }
            }
        });
        maxQueriesField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);

                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    getActiveMysqlUser().modifyMaxes(Flags.MAX_QUERIES_PER_HOUR, maxQueriesField.getText());
                    forceTriggerSavedUserEditsFlags();

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);
                }

            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!isSystemEdit) {
                    if (newValue == null || newValue.isEmpty()) {
                        return;
                    }

                    if (!waiter.getAddData().isQueuedInBackground()) {
                        waiter.getAddData().setQueuedInBackground(true);
                        Recycler.addWorkProc(waiter);
                    }

                    waiter.getAddData().setWaitInBackground(true);
                }
            }
        });
        maxUpdatesField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);

                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    getActiveMysqlUser().modifyMaxes(Flags.MAX_UPDATES_PER_HOUR, maxUpdatesField.getText());
                    forceTriggerSavedUserEditsFlags();

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);
                }

            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!isSystemEdit) {
                    if (newValue == null || newValue.isEmpty()) {
                        return;
                    }

                    if (!waiter.getAddData().isQueuedInBackground()) {
                        waiter.getAddData().setQueuedInBackground(true);
                        Recycler.addWorkProc(waiter);
                    }

                    waiter.getAddData().setWaitInBackground(true);
                }
            }
        });
    }

    private void passwordFieldWorks() {

        passwordField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {
                String style = ".textfield{}";
                String errorText;

                @Override
                public void updateUI() {
                    passwordField.setStyle(style);
                    style = ".textfield{}";

                    if (errorText == null) {
                        GridPane.setColumnIndex(passwordAlertLabel, 0);
                        GridPane.setColumnSpan(passwordAlertLabel, GridPane.REMAINING);
                        passwordAlertLabel.setMaxWidth(Double.MAX_VALUE);
                        passwordAlertLabel.setText("Consider using a password with 8 or more character with mixed case letters, numbers and punctionate marks.");
                        passwordAlertLabel.setTextFill(Color.BLACK);
                        getActiveMysqlUser().modifyPassword(passwordField.getText());
                        forceTriggerSavedUserEditsFlags();
                    } else {
                        GridPane.setColumnIndex(passwordAlertLabel, 1);
                        GridPane.setColumnSpan(passwordAlertLabel, 2);
                        //passwordAlertLabel.setMaxWidth(Label.USE_COMPUTED_SIZE);
                        passwordAlertLabel.setText(errorText);
                        passwordAlertLabel.setTextFill(Color.TOMATO);
                        errorText = null;
                    }

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);
                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);

                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    callUpdate = true;
                    int len = Math.max(4, validate_password_length);
                    boolean matchPasswordCode = passwordField.getText().length() > len;

                    if (!matchPasswordCode) {
                        errorText = "Your password need to be least " + String.valueOf(len) + " long";
                    }

                    if (matchPasswordCode && validate_password_policy != null) {

                        String password = passwordField.getText();
                        if (validate_password_policy.charAt(0) == 'M' || validate_password_policy.charAt(0) == 'S') {
                            int nonAlphaNumbericCount = 0;
                            int digitCount = 0;
                            int upperCaseCount = 0;
                            int lowerCaseCount = 0;

                            for (char c : password.toCharArray()) {

                                if (Character.isUpperCase(c)) {
                                    upperCaseCount++;
                                }

                                if (Character.isLowerCase(c)) {
                                    lowerCaseCount++;
                                }

                                if (!Character.isLetterOrDigit(c)) {
                                    nonAlphaNumbericCount++;
                                }

                                if (Character.isDigit(c)) {
                                    digitCount++;
                                }
                            }

                            if (nonAlphaNumbericCount < validate_password_special_char_count) {
                                errorText = "You need at least " + String.valueOf(validate_password_special_char_count) + " non-alphanumeric character(s) in your password";
                                matchPasswordCode = false;
                            }
                            if ((upperCaseCount + lowerCaseCount) < validate_password_mixed_case_count) {
                                errorText = "You need at least " + String.valueOf(validate_password_mixed_case_count) + " different cases(capital & small letters) in your password";
                                matchPasswordCode = false;
                            }
                            if (digitCount < validate_password_number_count) {
                                errorText = "You need at least " + String.valueOf(validate_password_number_count) + " digit(s) in your password";
                                matchPasswordCode = false;
                            }

                        }

                    }

                    style = matchPasswordCode
                            ? "-fx-border-color: green; -fx-border-width: 1.5;"
                            : "-fx-border-color: tomato; -fx-border-width: 1.5;";
                }

            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.length() < 2) {
                    return;
                }

                if (!waiter.getAddData().isQueuedInBackground()) {
                    waiter.getAddData().setQueuedInBackground(true);
                    Recycler.addWorkProc(waiter);
                }

                waiter.getAddData().setWaitInBackground(true);
            }
        });

        rePasswordField.textProperty().addListener(new ChangeListener<String>() {

            final WorkProc<ProcStateManipulator> waiter = new WorkProc<ProcStateManipulator>(
                    new ProcStateManipulator(Boolean.FALSE, Boolean.FALSE)) {

                boolean matchPasswordCode;

                @Override
                public void updateUI() {

                    if (matchPasswordCode) {
                        GridPane.setColumnIndex(passwordAlertLabel, 0);
                        GridPane.setColumnSpan(passwordAlertLabel, GridPane.REMAINING);
                        passwordAlertLabel.setMaxWidth(Double.MAX_VALUE);
                        passwordAlertLabel.setText("Consider using a password with 8 or more character with mixed case letters, numbers and punctionate marks.");
                        passwordAlertLabel.setTextFill(Color.BLACK);
                    } else {
                        GridPane.setColumnIndex(passwordAlertLabel, 1);
                        GridPane.setColumnSpan(passwordAlertLabel, 2);
                        passwordAlertLabel.setText("Password Mismatch");
                        passwordAlertLabel.setTextFill(Color.TOMATO);
                    }

                    rePasswordField.setStyle(matchPasswordCode ? "-fx-border-color: green; -fx-border-width: 1.5;"
                            : "-fx-border-color: tomato; -fx-border-width: 1.5;");

                    getAddData().setQueuedInBackground(false);
                    getAddData().setWaitInBackground(false);

                }

                @Override
                public void run() {
                    getAddData().setWaitInBackground(true);
                    callUpdate = true;
                    while (getAddData().isWaitingInBackground()) {
                        getAddData().setWaitInBackground(false);
                        synchronized (waiter) {
                            try {
                                waiter.wait(400);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }

                    matchPasswordCode = rePasswordField.getText().equals(passwordField.getText());
                }
            };

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.length() < 2) {
                    return;
                }

                if (!waiter.getAddData().isQueuedInBackground()) {
                    waiter.getAddData().setQueuedInBackground(true);
                    Recycler.addWorkProc(waiter);
                }

                waiter.getAddData().setWaitInBackground(true);
            }
        });

    }

    private class SimplePrivsTypeValuesMatcher {

        SimpleBooleanProperty bool;
        PrivsLevelWrapper levelWrapper;

        SimpleStringProperty[] values;

        public SimplePrivsTypeValuesMatcher() {
            this(new SimpleStringProperty(), new SimpleStringProperty());
        }

        public SimplePrivsTypeValuesMatcher(SimpleStringProperty... values) {
            this(null, values);
        }

        public SimplePrivsTypeValuesMatcher(PrivsLevelWrapper levelWrapper, SimpleStringProperty... values) {
            this.bool = new SimpleBooleanProperty();
            this.levelWrapper = /*(levelWrapper == null) ? new PrivsLevelWrapper() : */ levelWrapper;
            this.values = values;
        }

    }

    private void calculateUiSpecs() {
        if (specsProc == null) {
            specsProc = new WorkProc() {

                double headerHeight = 0.0;
                boolean addInnerHeader = false;

                @Override
                public void run() {
                    addInnerHeader = false;
                    try { Thread.sleep(150); } catch (Exception e) { }
                    double needinHeight = Flags.USER_MANAGER_UI_MINIMUM_INTERNAL_HEIGHT - mainUI.getHeight();
                    if (needinHeight > 0) {
                        headerHeight = 10.0;
                        if (needinHeight < 35) {
                            headerHeight = 45 - needinHeight;
                            if (innerHeader.getParent() == null) {
                                addInnerHeader = true;
                            }
                        }
                    }
                    
                    //here we calculate tableviews width and divider position
                    double width = contentSplitPane.getPrefWidth();
                    double templateTableviewWidth = globalRoleTemplateTableview.getPrefWidth();
                    Insets templateTableviewPadding = globalRoleTemplateTableview.getPadding();
                    double templateTableviewConstantColumnsWidth = templateTableviewPadding.getLeft() + templateTableviewPadding.getRight();
                    double templateTableviewDesColumnWidth;
                    for(int i =0; i < 1; i++){
                        templateTableviewConstantColumnsWidth += globalRoleTemplateTableview.getColumns().get(1).getPrefWidth();
                    }
                    templateTableviewDesColumnWidth = templateTableviewWidth - templateTableviewConstantColumnsWidth;
                    globalRoleTemplateTableview.getColumns().get(2).setPrefWidth(templateTableviewDesColumnWidth);
                    
                    
                    System.gc(); System.gc(); System.gc(); System.gc();
                }

                @Override
                public void updateUI() {
                    super.updateUI();
                    
                    if (headerHeight != header.getPrefHeight()) {
                        if (addInnerHeader) {
                            header.getChildren().add(innerHeader);
                        } else {
                            header.getChildren().remove(innerHeader);
                        }
                        header.setPrefHeight(headerHeight);
                        headerImageView.setFitHeight(headerHeight / 2.3);
                        AnchorPane.setTopAnchor(contentSplitPane, headerHeight + 1);
                    }
                    
                    callUpdate = false;
                }
            };
        }

        //here i am using callUpdate boolean flag as a check whether method is executing or done executing
        if (!specsProc.callUpdate) {
            specsProc.callUpdate = true;
            Recycler.addWorkProc(specsProc);
        }
    }
    
}
