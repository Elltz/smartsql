/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javafx.beans.property.BooleanProperty;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.query.QueryValidation;
import np.com.ngopal.smart.sql.structure.queryutils.RSyntaxTextAreaBuilder;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import org.apache.commons.lang.StringUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class CodeAreaWrapper {

    private static volatile boolean needExplain = false;
//    
//    private static ChangeListener<Object> closingListener = new ChangeListener<Object>() {
//        @Override
//        public void changed(ObservableValue<? extends Object> ov,
//                Object t, Object t1) {
//            CodeArea textArea = (CodeArea) ((ReadOnlyObjectProperty) ov).getBean();
//            CodeAreaWrapperClosingData codeAreaWrapperData = (CodeAreaWrapperClosingData) textArea.getUserData();
//            if (codeAreaWrapperData == null) {
//                return;
//            }
//
//            if (t1 == null && !codeAreaWrapperData.alreadyCalledClose) {
//                codeAreaWrapperData.alreadyCalledClose = true;
//                codeAreaWrapperData.alreadyCalledOpen = false;
//                textArea.removeEventFilter(KeyEvent.KEY_PRESSED, codeAreaWrapperData.getMenuAcceleratorHandler());
//                if(t instanceof Scene){
//                    ((Scene)t).removeEventFilter(KeyEvent.KEY_PRESSED, codeAreaWrapperData.getOnKeyEnteredFliterHandler());
//                }
//                textArea.setOnDragOver(null);
//                textArea.setOnDragDropped(null);
//                //textArea.behavior().get().setScrollHandler(null);
//                textArea.textProperty().removeListener(codeAreaWrapperData.getTextInvalidationListener());
//                textArea.caretPositionProperty().removeListener(codeAreaWrapperData.getCaretPositionListener());
//                textArea.currentParagraphProperty().removeListener(codeAreaWrapperData.getParagraphListener());
//                textArea.caretColumnProperty().removeListener(codeAreaWrapperData.getCaretColumnListener());
//                textArea.removeEventHandler(KeyEvent.KEY_RELEASED, codeAreaWrapperData.getAutoCompleteHandler());                
//                textArea.removeEventHandler(MouseEvent.MOUSE_PRESSED, codeAreaWrapperData.getWordSelectorHandler());
//                textArea.setContextMenu(null);
//                codeAreaWrapperData.finalize();
//            } else if (t1 != null && !codeAreaWrapperData.alreadyCalledOpen) {
//                codeAreaWrapperData.alreadyCalledOpen = true;
//                codeAreaWrapperData.alreadyCalledClose = false;
//                textArea.addEventFilter(KeyEvent.KEY_PRESSED, codeAreaWrapperData.getMenuAcceleratorHandler());
//                textArea.getScene().addEventFilter(KeyEvent.KEY_PRESSED, codeAreaWrapperData.getOnKeyEnteredFliterHandler());
//                textArea.setOnDragOver(codeAreaWrapperData.getDraggedOverHandler());
//                textArea.setOnDragDropped(codeAreaWrapperData.getDraggedDroppedHandler());
//                textArea.textProperty().addListener(codeAreaWrapperData.getTextInvalidationListener());
//                textArea.caretPositionProperty().addListener(codeAreaWrapperData.getCaretPositionListener());
//                //textArea.currentParagraphProperty().addListener(codeAreaWrapperData.getParagraphListener());
//                //textArea.caretColumnProperty().addListener(codeAreaWrapperData.getCaretColumnListener());
//                textArea.addEventHandler(KeyEvent.KEY_RELEASED, codeAreaWrapperData.getAutoCompleteHandler());
//                textArea.addEventFilter(MouseEvent.MOUSE_PRESSED, codeAreaWrapperData.getWordSelectorHandler());
//                // invalidate listeners
//                Platform.runLater(() -> {
//                    if (!textArea.getText().isEmpty()) {
//                        textArea.appendText("");
//                        ((CodeAreaWrapperClosingData) textArea.getUserData()).checkOnComputeHightLightHacks(textArea);
//                    }
//                });
//            }
//        }
//    };
//    
//    public static final char[] selectionCatchPharses = new char[]{'.', ' ', ';', '(', ')',',','\'','"'};
//
//    /**
//     * This method returns an offset from the current caret postion as of the
//     * time it was called. it gives you how many chars to the left and right of
//     * the current caret postion
//     *
//     * @param text The text to examine
//     * @param currentPos the current postion of the caret
//     * @param delimiters the delimiters to break or call a word on
//     * @return returns an array of offset of length to the right and left of the
//     * caret
//     */
//    private static int[] getOffsetOnPos(String text, int currentPos, boolean needLeft, boolean needRight, char... delimiters) {
//        int[] index = new int[]{-1, -1};
//        if (needLeft) {
//            //left
//            leftLoop:
//            for (int i = currentPos; i > -1 && i < text.length(); --i) {
//                for (char c : delimiters) {
//                    if (c == text.charAt(i)) {
//                        index[0] = currentPos - (i + 1);
//                        break leftLoop;
//                    }
//                }
//            }
//            //left point insurance
//            if (index[0] == -1) {
//                index[0] = currentPos;
//            }
//        }
//        if (needRight) {
//            //right
//            rightLoop:
//            for (int i = currentPos; i < text.length(); ++i) {
//                for (char c : delimiters) {
//                    if (c == text.charAt(i)) {
//                        index[1] = i - currentPos;
//                        break rightLoop;
//                    }
//                }
//            }
//            //right point insurance
//            if (index[1] == -1) {
//                index[1] = text.length() - currentPos;
//            }
//        }
//        return index;
//    }
//
//    public static QueryAreaWrapper getCodeArea(final ConnectionSession session, final String context, MenuItem[] items,
//            BooleanProperty needValidateQuery, QueryTabModule queryTabModule, MainController controller, PreferenceDataService preferenceService,
//            ConnectionParamService connectionParamService, Callback<File, Void> openFileCallback,
//            Callback<String, Void> openQueryAnalyzerCallback, Callback<List<Object>, Integer> tryDragWithJoinCallback) {
//
//        final CodeArea textArea = new CodeArea();
//        QueryAreaWrapper wrapper = new QueryAreaWrapper(textArea,items);
//        
//        CodeAreaWrapperClosingData codeAreaWrapperData = new CodeAreaWrapperClosingData();
//
//        codeAreaWrapperData.setMenuAcceleratorHandler(new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent event) {
//                needExplain = ";".equals(event.getText());
//                
//                for (MenuItem mi : textArea.getContextMenu().getItems()) {
//                    if (mi.getAccelerator() != null && mi.getAccelerator().match(event)) {
//                        mi.fire();
//                        event.consume();
//                        break;
//                    }
//                }
//            }
//        });
//
//        codeAreaWrapperData.setDraggedOverHandler(new EventHandler<DragEvent>() {
//            @Override
//            public void handle(DragEvent event) {
//                Dragboard db = event.getDragboard();
//                if (db.hasString() || db.hasFiles()) {
//                    event.acceptTransferModes(TransferMode.MOVE);
//                }
//                event.consume();
//            }
//        });
//
//        codeAreaWrapperData.setDraggedDroppedHandler(new EventHandler<DragEvent>() {
//            @Override
//            public void handle(DragEvent event) {
//                Dragboard db = event.getDragboard();
//                if (db.hasString()) {
//                    String[] data = db.getString().split("\\|");
//                    Recycler.addWorkProc(new WorkProc() {
//                        Integer pos;
//
//                        @Override
//                        public void updateUI() {
//                            if (pos == null) {
//                                pos = textArea.getCaretPosition();
//                                textArea.insertText(textArea.getCaretPosition(), data[3]);
//                            }
//
//                            formatCurrentQuery(controller, preferenceService, controller.getSelectedConnectionSession().get(), wrapper, pos);
//                        }
//
//                        @Override
//                        public void run() {
//                            callUpdate = true;
//                            if ("tableOrView".equals(data[0])) {
//                                List<String> names = highlightGreenMap.get(session);
//                                if (names == null) {
//                                    names = new ArrayList<>();
//                                    highlightGreenMap.put(session, names);
//                                }
//
//                                if (!names.contains(data[1].toLowerCase())) {
//                                    names.add(data[1].toLowerCase());
//                                }
//
//                                if (!names.contains(data[2].toLowerCase())) {
//                                    names.add(data[2].toLowerCase());
//                                }
//
//                            } else if ("functionOrProcudure".equals(data[0])) {
//                                List<String> names = highlightMaroonMap.get(session);
//                                if (names == null) {
//                                    names = new ArrayList<>();
//                                    highlightMaroonMap.put(session, names);
//                                }
//
//                                if (!names.contains(data[1].toLowerCase())) {
//                                    names.add(data[1].toLowerCase());
//                                }
//                            }
//
//                            // exist only for table
//                            String database = data[2];
//
//                            pos = tryDragWithJoinCallback.call(Arrays.asList(session, database, data[1], wrapper, event.getX(), event.getY()));
//                        }
//                    });
//                } else if (db.hasFiles()) {
//                    List<File> files = db.getFiles();
//                    if (files != null) {
//                        for (File f : files) {
//                            openFileCallback.call(f);
//                        }
//                    }
//                }
//                event.setDropCompleted(true);
//                event.consume();
//            }
//        });
//
//        codeAreaWrapperData.setValidateQueryListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
//                textArea.appendText("");
//            }
//        });
//        
//        codeAreaWrapperData.setOnKeyEnteredFliterHandler(new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent event) { 
//                //this is to not force scroll listener from being called when user typing
//                codeAreaWrapperData.setDontTriggerScroll(true);
//            }
//        });
//        
//        codeAreaWrapperData.setWordSelectorHandler(new EventHandler<MouseEvent>() {
//            
//            @Override
//            public void handle(MouseEvent t) { 
//                if (t.getClickCount() > 1) {
//                    int caretParaCol = textArea.getCaretColumn();
//                    int caretPosInText = textArea.getCaretPosition();
//                    String text = textArea.getText(textArea.getCurrentParagraph());
//                    if (text.isEmpty()) {  return; }
//                    int[] offset = getOffsetOnPos(text, caretParaCol,true,true, selectionCatchPharses);
//                    textArea.selectRange(caretPosInText - offset[0], caretPosInText + offset[1]);
//                    t.consume();
//                }
//            }
//        });
//
//        codeAreaWrapperData.setLineNumberFactory((LineNumberFactory) LineNumberFactory.get(textArea,
//               (Label node, String lineContent, LineNumberFactory factory) -> {}));
//        
//        /**
//         * The Boolean explanation [0] stands for wait duration. When
//         * wait_split_organiser starts when an invalidation listener is
//         * triggered, it will run once, but when user is for example typing then
//         * wait_split_organiser will wait. In invalidation listener [0] is
//         * triggered to true, whiles [0] true, wait_split_organiser will wait.
//         *
//         * [1] = means wait_split_organiser is doing hard work so some operation
//         * like, caret Position listeener should not run and etc.
//         *
//         * need re-check logic here
//         */
//        WorkProc<Boolean[]> wait_split_organiser = new WorkProc<Boolean[]>(
//                new Boolean[]{Boolean.FALSE, Boolean.FALSE}) {
//
//                    final Object lock = new Object();                    
//                    Label l = (Label) controller.getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA);
//                    WorkProc stylingWorkProc = stylingWorkProc = getSylingWorkProc(textArea, session, controller);
//                    volatile byte pauseLoopCounter = 0;
//
//                    WorkProc<String> queryCheckingWorkProc = new WorkProc() {
//
//                            Map<Integer, String> warningEmptyKeyResult = new HashMap<>();
//                            Map<Integer, String> warningMoreThen10kRowsResult = new HashMap<>();
//                            Map<Integer, String> errorResult = new HashMap<>();
//                            Integer[] paraposition = new Integer[2];
//
//                            @Override
//                            public void updateUI() {
//                                codeAreaWrapperData.getLineNumberFactory().clear(paraposition[0],paraposition[1]);
//                                codeAreaWrapperData.getLineNumberFactory().checkMinimize();
//
//                                for (Integer row : warningEmptyKeyResult.keySet()) {
//                                    Label node = codeAreaWrapperData.getLineNumberFactory().lookupNumberLabel(row - 1);
//                                    if (node != null) {
//                                        if (warningEmptyKeyResult.get(row) != null) {
//                                            codeAreaWrapperData.getLineNumberFactory().showWarning(node);
//                                            node.setTooltip(new Tooltip("Query having null in \"Key\" column of EXPLAIN output"));
//                                            node.setOnMouseClicked((MouseEvent event) -> {
//                                                openQueryAnalyzerCallback.call(warningEmptyKeyResult.get(row));
//                                            });
//                                        }
//                                    }
//                                }
//
//                                for (Integer row : warningMoreThen10kRowsResult.keySet()) {
//                                    Label node = codeAreaWrapperData.getLineNumberFactory().lookupNumberLabel(row - 1);
//                                    if (node != null) {
//                                        if (warningMoreThen10kRowsResult.get(row) != null) {
//                                            codeAreaWrapperData.getLineNumberFactory().showWarning(node);
//                                            node.setTooltip(new Tooltip("The query is using more than 10k records scan. You can use SQL Optimizer module to tune the Query"));
//                                            node.setOnMouseClicked((MouseEvent event) -> {
//                                                openQueryAnalyzerCallback.call(warningMoreThen10kRowsResult.get(row));
//                                            });
//                                        }
//                                    }
//                                }
//
//                                for (Integer row : errorResult.keySet()) {
//                                    Label node = codeAreaWrapperData.getLineNumberFactory().lookupNumberLabel(row - 1);
//
//                                    String errorMsg = errorResult.get(row);
//                                    if (node != null && !errorMsg.isEmpty()) {
//                                        codeAreaWrapperData.getLineNumberFactory().showError(node);
//                                        node.setTooltip(new Tooltip(errorMsg));
//                                    }
//                                }
//                            }
//
//                            @Override
//                            public void run() {
//                                if (queryTabModule.canValidate(wrapper)) {
//                                    callUpdate = true;
//                                    try {
//                                        if (needValidateQuery.get()) {
//                                            Integer[] positioning = new Integer[2];
//                                            positioning[0] = textArea.behavior().get().visibleTextPosition()[0];
//                                            positioning[1] = textArea.behavior().get().visibleTextPosition()[1];
//
//                                            positioning[0] = (positioning[0] - ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL()
//                                                    > 0) ? positioning[0] - ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL()
//                                                            : positioning[0];
//
//                                            positioning[1] = (positioning[1] + ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL()
//                                                    > textArea.getText().length()) ? positioning[1]
//                                                            : positioning[1] + ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL();
//
//                                            //retrieve paragraph lines for line number clearance
//                                            paraposition[0] = textArea.behavior().get().visibleParagraphIndexes()[0];
//                                            paraposition[1] = textArea.behavior().get().visibleParagraphIndexes()[1];
//
//                                            warningEmptyKeyResult.clear();
//                                            warningMoreThen10kRowsResult.clear();
//                                            errorResult.clear();
//                                            checkQueries(textArea, textArea.getCaretPosition(), textArea.getText(positioning[0],positioning[1]), controller, session, needValidateQuery, 0, warningEmptyKeyResult,
//                                                    warningMoreThen10kRowsResult, errorResult);
//                                        }
//                                    } catch (Exception e) { }                                
//                                }
//                            }
//                        };
//
//                    @Override
//                    public void updateUI() {
//                        if (stylingWorkProc != null && stylingWorkProc.callUpdate) {
//                            stylingWorkProc.updateUI();
//                        }
//                        l.setText("Lin: " + String.valueOf(textArea.getCurrentParagraph() + 1)
//                                + ", Col: " + String.valueOf(textArea.getCaretColumn() + 1));
//                        getAddData()[1] = false;
//                        Platform.runLater(() -> {
//                             codeAreaWrapperData.setDontTriggerScroll(false);
//                        });
//                    }
//
//                    @Override
//                    public void run() {
//                        callUpdate = false;
//                        getAddData()[1] = true;
//                        getAddData()[0] = true;                        
//                        while (getAddData()[0]) {
//                            getAddData()[0] = false;
//                            synchronized (lock) {
//                                try { lock.wait(QueryTabModule.HIGHLIGHT_DELAY); } catch (InterruptedException ie) { }
//                            }
//                        }
//                        
//                        if (textArea.getText().isEmpty()) {
//                            getAddData()[1] = false;
//                            Platform.runLater(() -> {
//                                codeAreaWrapperData.getLineNumberFactory().clearAll();
//                                 codeAreaWrapperData.setDontTriggerCaret(false);
//                                 codeAreaWrapperData.setDontTriggerScroll(false);
//                            });
//                            return;
//                        }
//                        if(codeAreaWrapperData.getOFFSET_FOR_SCROLL() == 0){
//                            codeAreaWrapperData.setOFFSET_FOR_SCROLL(500);
//                        }
//                        int len = textArea.getText().length();
//                        if(len < codeAreaWrapperData.getOFFSET_FOR_SCROLL() * 2){
//                            int temp = codeAreaWrapperData.getOFFSET_FOR_SCROLL();
//                            while(temp > 0){
//                                temp-= 100;
//                                if(len > temp * 2){
//                                    codeAreaWrapperData.setOFFSET_FOR_SCROLL(temp);
//                                    break;
//                                }
//                            }
//                        }
//                        
//                        if (((CodeAreaWrapperClosingData) textArea.getUserData()).isOffsetInComputedRegionList(
//                                textArea.behavior().get().visibleTextPosition()[0],
//                                textArea.behavior().get().visibleTextPosition()[1])) {
//                        
//                            ((CodeAreaWrapperClosingData) textArea.getUserData()).removeOffsetFromComputeRegionList(
//                                    textArea.behavior().get().visibleTextPosition()[0],
//                                    textArea.behavior().get().visibleTextPosition()[1]);
//                        }
//                        
//                        Recycler.addWorkProc(queryCheckingWorkProc);
//                        
//                        stylingWorkProc.run();
//                        callUpdate = true;
//                    }
//                    
//
//                };
//
//        codeAreaWrapperData.setTextInvalidationListener(new InvalidationListener() {
//            @Override
//            public void invalidated(Observable o) { 
//                codeAreaWrapperData.setDontTriggerScroll(true);
//                textArea.clearSelectCustomRange();
//                if (!wait_split_organiser.getAddData()[1]) {
//                    Recycler.addWorkProc(wait_split_organiser);
//                }else{
//                    wait_split_organiser.getAddData()[0] = Boolean.TRUE;
//                }
//            }
//        });
//
//        codeAreaWrapperData.setCaretPositionListener(new ChangeListener<Integer>() {
//
//            int[] oldParenPos = new int[]{-1,-1};
//            volatile String lastKnownStyleClass = null;
//            WorkProc<Boolean> carePosWorkproc = new WorkProc<Boolean>(false) {
//
//                int[] openParenPos = new int[2];
//                Label l = (Label) controller.getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA);
//                char[] yellowHighlightChars = new char[]{'(', ')', '<', '>', '{', '}', '[', ']'};
//                boolean lastTrigPosWasEven = false;
//                
//                private String getDefaultSytleOfChar(char c){
//                    if(c == yellowHighlightChars[0] || c == yellowHighlightChars[1]){
//                        return "paren";
//                    }else if(c == yellowHighlightChars[2] || c == yellowHighlightChars[3]){
//                        return "operator";
//                    }else if(c == yellowHighlightChars[4] || c == yellowHighlightChars[5]){
//                        return "brace";
//                    }else if(c == yellowHighlightChars[6] || c == yellowHighlightChars[7]){
//                        return "bracket";
//                    }
//                    return null;
//                }
//                
//                private boolean isValidBreaker(char c){
//                        for(char cars : yellowHighlightChars){
//                            if(c == cars){
//                                return true;
//                            }
//                        }
//                    return false;
//                }
//                
//                private char getNextChar(char c){
//                    for(int i = 0; i < yellowHighlightChars.length; i++){
//                        if(c == yellowHighlightChars[i]){
//                            lastTrigPosWasEven = i%2 ==0;
//                            return yellowHighlightChars[(lastTrigPosWasEven ? i+1 : i-1)];
//                        }
//                    }
//                    return c;
//                }
//                
//                private final void computeParenHighlights(String text, int currentPosition) {
//                    char characterToLook = '?', characterToSkip = '?';
//                    openParenPos[0] = -1;
//                    openParenPos[1] = -1;
//                    int indexPos = currentPosition - 1;
//                    if (indexPos > -1 && isValidBreaker(text.charAt(indexPos))) {
//                        openParenPos[0] = indexPos;
//                        characterToSkip = text.charAt(indexPos);
//                        characterToLook = getNextChar(characterToSkip);
//                    } else if (currentPosition == text.length()) {
//                        return;
//                    }/** else if (isValidBreaker(text.charAt(currentPosition))) {
//                        openParenPos[0] = currentPosition;
//                        characterToSkip = text.charAt(currentPosition);
//                        characterToLook = getNextChar(characterToSkip);
//                    } */else {
//                        return;
//                    }
//
//                    if (!lastTrigPosWasEven) {
//                        openParenPos[1] = openParenPos[0];
//                        openParenPos[0] = -1;
//                    }
//
//                    //we move to the left here
//                    if (openParenPos[0] == -1) {
//                        int sameOccurenceCount = 0;
//                        //we start of from the known offset
//                        for (int i = (openParenPos[1] - 1); i > -1; i--) {
//                            char c = text.charAt(i);
//                            if (c == characterToSkip) {
//                                sameOccurenceCount++;
//                            } else if (c == characterToLook) {
//                                if (sameOccurenceCount > 0) {
//                                    sameOccurenceCount--;
//                                } else {
//                                    openParenPos[0] = i;
//                                    break;
//                                }
//                            }
//                        }
//                    } //we move to the right here
//                    else if (openParenPos[1] == -1) {
//                        int sameOccurenceCount = 0;
//                        //we start of from the known offset
//                        for (int i = (openParenPos[0] + 1); i < text.length(); i++) {
//                            char c = text.charAt(i);
//                            if (c == characterToSkip) {
//                                sameOccurenceCount++;
//                            } else if (c == characterToLook) {
//                                if (sameOccurenceCount > 0) {
//                                    sameOccurenceCount--;
//                                } else {
//                                    openParenPos[1] = i;
//                                    break;
//                                }
//                            }
//                        }
//                    }
//                    lastKnownStyleClass = getDefaultSytleOfChar(characterToLook);
//                }
//                
//                @Override
//                public void updateUI() {
//                    if (!textArea.getText().isEmpty()) {                         
//                        try {        
//                            if (openParenPos[0] == -1 || openParenPos[1] == -1) {
//                                int pos = (openParenPos[0] == -1) ? openParenPos[1] : openParenPos[0];
//                                if(pos > -1){
//                                    textArea.setStyleClass(pos, pos + 1, "highlightNonExistParen");
//                                }
//                            } else {
//                                textArea.setStyleClass(openParenPos[0], openParenPos[0] + 1, "highlightExistParen");
//                                textArea.setStyleClass(openParenPos[1], openParenPos[1] + 1, "highlightExistParen");
//                            }
//                            //set new to old
//                            oldParenPos[0] = openParenPos[0];
//                            oldParenPos[1] = openParenPos[1];
//                            //reset al
//                            openParenPos[0] = -1;
//                            openParenPos[1] = -1;
//                        } catch (Exception e) { e.printStackTrace(); }
//                        
//                        Platform.runLater(() -> { 
//                            codeAreaWrapperData.setCaretScroll(false);   
//                            //codeAreaWrapperData.setDontTriggerScroll(false);
//                        });
//                    }
//                    
//                    Integer[] i = (Integer[]) l.getUserData();
//                    i[0] = textArea.getCurrentParagraph() + 1;
//                    i[1] = textArea.getCaretColumn() + 1;
//                    l.setText("Lin: " + String.valueOf(i[0]) + ", Col: " + String.valueOf(i[1]));
//                    
//                    setAdditionalData(false);
//                }
//
//                @Override
//                public void run() {
//                    callUpdate = true;
//                    setAdditionalData(true);                    
//                    String text = textArea.getText();
//                    computeParenHighlights(text ,textArea.getCaretPosition());
//                }
//            };
//            
//            @Override
//            public void changed(ObservableValue<? extends Integer> ov, Integer t, Integer t1) {
//                
//                if (lastKnownStyleClass != null) {
//                    if (oldParenPos[0] > -1 && oldParenPos[1] > -1) {
//                        int old0 = oldParenPos[0];
//                        int old1 = oldParenPos[1];
//                        String temp = lastKnownStyleClass;
//                        Platform.runLater(() -> {
//                            try {
//                                textArea.setStyleClass(old0, old0 + 1, temp);
//                                textArea.setStyleClass(old1, old1 + 1, temp);
//                            } catch (Exception e) { e.printStackTrace(); }
//                        });                        
//                    } else {
//                        int pos = (oldParenPos[0] == -1) ? oldParenPos[1] : oldParenPos[0];
//                        String temp = lastKnownStyleClass;
//                        if (pos > -1) {
//                            Platform.runLater(() -> {
//                                try {
//                                    textArea.setStyleClass(pos, pos + 1, temp);
//                                } catch (Exception e) { e.printStackTrace(); }
//                            });
//                        }
//                    }
//                    oldParenPos[0] = -1;
//                    oldParenPos[1] = -1;
//                    lastKnownStyleClass = null;
//                    System.out.println("resetting to old style with oldParen");
//                }
//                
//                if (wait_split_organiser.getAddData()[1] ) {return;}
//                
//                if (codeAreaWrapperData.isDontTriggerCaret()) {
//                    codeAreaWrapperData.setDontTriggerCaret(false);
//                    return;
//                }
//                
//                if (carePosWorkproc.getAddData()) { return; }
//                
//                codeAreaWrapperData.setCaretScroll(true);
//                
//                Recycler.addWorkProc(carePosWorkproc);
//            }
//        });
//
//        codeAreaWrapperData.setParagraphListener(new ChangeListener<Integer>() {
//            Label l = (Label) controller.getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA);
//            @Override
//            public void changed(ObservableValue<? extends Integer> ov, Integer t, Integer t1) {
//                if (wait_split_organiser.getAddData()[1]) {
//                    return;
//                }
//                Integer[] i = (Integer[]) l.getUserData();
//                i[0] = t1;
//                l.setText("Lin: " + String.valueOf(i[0] + 1) + ", Col: " + String.valueOf(i[1] + 1));
//            }
//        });
//
//        codeAreaWrapperData.setCaretColumnListener(new ChangeListener<Integer>() {
//            Label l = (Label) controller.getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA);
//            @Override
//            public void changed(ObservableValue<? extends Integer> ov, Integer t, Integer t1) {
//                if (wait_split_organiser.getAddData()[1]) {
//                    return;
//                }
//                Integer[] i = (Integer[]) l.getUserData();
//                i[1] = t1;
//                l.setText("Lin: " + String.valueOf(i[0] + 1) + ", Col: " + String.valueOf(i[1] + 1));
//            }
//        });
//        
//        /**
//         * Initially i used the setCodeAreaMotionHandler to track when the scroller has been
//         * scroll to compute highlighting of then off-screen queries, but, that brought about
//         * alot of complications which had me implement the below code which checks or listens
//         * to the change boolean value of the thumbNode property to detect when the user just
//         * came out of scorlling or initiating a scrolling. - (forget the grammer & spelling here:))
//         * 
//         * if true it means user just initiated a scrolling
//         * if false it means user just came out immediately out of scrolling,
//         *          * 
//         */
//        codeAreaWrapperData.setCodeAreaMotionHandler(new EventHandler<ScrollEvent>() {
//            @Override
//            public void handle(ScrollEvent t) {
//                if(!codeAreaWrapperData.isComputeHighlightCodeAlreadyRunning()){
//                    Recycler.addWorkProc(getSylingWorkProc(textArea, session, controller));
//                }
//            }
//        });        
//        codeAreaWrapperData.setAfterScrollCode(new Runnable() {
//            @Override
//            public void run() { 
//                Recycler.addWorkProc(getSylingWorkProc(textArea, session, controller)); 
//            }
//        });
//
//        codeAreaWrapperData.setAutoCompleteHandler(new AutoCompleteQueryListener(controller.getStage(),
//                connectionParamService, textArea, session, highlightGreenMap, highlightMaroonMap));
//        needValidateQuery.addListener(codeAreaWrapperData.getValidateQueryListener());
//        
//        textArea.setParagraphGraphicFactory(codeAreaWrapperData.getLineNumberFactory());
//        textArea.getStylesheets().add(QueryTabModule.class.getResource(
//                "css/java-keywords.css").toExternalForm());
//        textArea.appendText(context);
//
//        textArea.sceneProperty().addListener(closingListener);
//        //textArea.parentProperty().addListener(closingListener);
//        textArea.setUserData(codeAreaWrapperData);
//
//        return wrapper;
//    }
//
//    private static final WorkProc getSylingWorkProc(CodeArea textArea,ConnectionSession session,MainController controller){
//        return new WorkProc() {
//                        StyleSpans<Collection<String>> ss;
//                        Integer[] positioning = new Integer[2],paraposition = new Integer[2];
//                        
//                        @Override
//                        public void updateUI() {
//                            ((CodeAreaWrapperClosingData) textArea.getUserData()).setDontTriggerScroll(true);
//                            textArea.clearSelectCustomRange();
//                            if (ss != null && !textArea.getText().isEmpty()) {
//                                try {
//                                    textArea.setStyleSpans(positioning[0], ss);
//                                } catch (Exception e) { e.printStackTrace(); }
//                                ss = null;
//                                if (((CodeAreaWrapperClosingData) textArea.getUserData()).wasTriggeredFromScroll()) {
//                                    ((CodeAreaWrapperClosingData) textArea.getUserData()).setDontTriggerCaret(true);
//                                    System.out.println("changing caret");
//                                    ((CodeAreaWrapperClosingData) textArea.getUserData()).setScrollBarPositionToLastKnown();
//                                    textArea.moveTo((textArea.behavior().get().visibleTextPosition()[0] 
//                                            + textArea.behavior().get().visibleTextPosition()[1])/2);
//                                }
//                                
//                                ((CodeAreaWrapperClosingData) textArea.getUserData()).setComputeHighlightCodeAlreadyRunning(false);
//                                System.out.println("compute hightlight toggled false bcos of update ui");
//                            } else {
//                                textArea.clearStyle(0, textArea.getLength());
//                                ((CodeAreaWrapperClosingData) textArea.getUserData()).getComputed_cordinates().clear();
//                            }
//                            ((CodeAreaWrapperClosingData) textArea.getUserData()).getLineNumberFactory().checkMinimize();
//                            Platform.runLater(() -> {
//                                ((CodeAreaWrapperClosingData) textArea.getUserData()).setDontTriggerScroll(false);
//                            });                            
//                        }
//
//                        @Override
//                        public void run() {
//                            ((CodeAreaWrapperClosingData)textArea.getUserData()).checkOnComputeHightLightHacks(textArea);
//                            if(((CodeAreaWrapperClosingData)textArea.getUserData()).shouldContinueWithHighLighting() == false 
//                                    ||
//                                    ((CodeAreaWrapperClosingData)textArea.getUserData()).isComputeHighlightCodeAlreadyRunning()){
//                                callUpdate = false;
//                                return;
//                            }
//                            ((CodeAreaWrapperClosingData)textArea.getUserData()).setComputeHighlightCodeAlreadyRunning(true);
//                            //System.out.println("compute hightlight toggled true");
//                            positioning[0] = textArea.behavior().get().visibleTextPosition()[0];
//                            positioning[1] = textArea.behavior().get().visibleTextPosition()[1];
//                            
//                            paraposition[0] = textArea.behavior().get().visibleParagraphIndexes()[0];
//                            paraposition[1] = textArea.behavior().get().visibleParagraphIndexes()[1];
//                            
//                            //this part creates offset around the computing range to, help remove constant computation
//                            positioning[0] = (positioning[0] - ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL()
//                                    > 0) ? positioning[0] - ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL()
//                                            : positioning[0];
//
//                            positioning[1] = (positioning[1] + ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL()
//                                    > textArea.getText().length()) ? positioning[1]
//                                            : positioning[1] + ((CodeAreaWrapperClosingData) textArea.getUserData()).getOFFSET_FOR_SCROLL();
//                            
//                            if (((CodeAreaWrapperClosingData) textArea.getUserData()).
//                                    isOffsetInComputedRegionList(positioning[0], positioning[1])) {
//                                callUpdate = false;
//                                ((CodeAreaWrapperClosingData)textArea.getUserData()).setComputeHighlightCodeAlreadyRunning(false);
//                                System.out.println("compute hightlight toggled false bcs of offset");
//                                return;
//                            } else {
//                                ((CodeAreaWrapperClosingData) textArea.getUserData()).addOffsetToComputeRegionList(positioning);
//                                callUpdate = true;
//                        }
//                                                                                    
//                            System.out.println("about to compute hightlight");
//                            ss = computeHighlighting(controller, textArea,positioning[1]/2 ,
//                                    textArea.getText(positioning[0], positioning[1]), positioning[0], positioning[1], session);
//                        }
//                    };
//    }
//    
//    
    public static void checkQueries(RSyntaxTextArea area, int position, String text, MainController controller, ConnectionSession session, BooleanProperty needValidateQuery, int rowDiff, Map<Integer, String> warningEmptyKeyResult,
            Map<Integer, String> warningMoreThen10kRowsResult, Map<Integer, String> errorResult) throws IOException {

        Pair<Integer, String> result = new ScriptRunner(null, true).findQueryAtPosition(text, position > 0 ? position - 1 : 0, false);
        if (result == null) {
            return;
        }
        
        
        String q = result.getValue();
        int row = result.getKey();

        //QueryTabModule.disabledValidatingAreas.put(area, false);

        String query = q.replaceAll("\\s+", " ").trim();

        int size = query.split(" ").length;

        String key = query.trim().toUpperCase();

        String errorMessage = QueryTabModule.validatedQueries.get(key);
        if (errorMessage != null && !errorMessage.isEmpty() && needValidateQuery.get()) {
            errorResult.put(row + rowDiff, errorMessage);
        }

        if (QueryTabModule.warningEmptyKeyQueries.contains(key) && needValidateQuery.get()) {
            warningEmptyKeyResult.put(row + rowDiff, key);
        }

        if (QueryTabModule.warningMoreThen10kRowsQueries.contains(key) && needValidateQuery.get()) {
            warningMoreThen10kRowsResult.put(row + rowDiff, key);
        }

        if (errorMessage == null || errorMessage.isEmpty()) {

            try {

                if (!query.startsWith("//") && !query.startsWith("#") && !query.startsWith("--")) {

                    // TODO at now need igonre not simple queries
                    
                    //String replacedStr = q.replaceAll("\\s", " ");
                    
                    // check maybe query is FUNCTION
//                    Matcher m = QueryTabModule.QUERY_FUNCTION_PATTERN.matcher(replacedStr);
                    if (false) {//m.find()) {

//                        int rowCount = q.substring(0, m.start(1)).split("\\n").length - 2; // first + last = 2                                
//                        checkQueries(area, position, q.substring(m.start(1), m.end(1)), controller, session, needValidateQuery, rowDiff + row + rowCount, warningEmptyKeyResult, warningMoreThen10kRowsResult, errorResult);

                    } else {

                        //m = QueryTabModule.QUERY_PROCEDURE_PATTERN.matcher(replacedStr);
                        if (false) {//m.find()) {

//                            int rowCount = q.substring(0, m.start(1)).split("\\n").length - 2; // first + last = 2
//                            checkQueries(area, position, q.substring(m.start(1), m.end(1)), controller, session, needValidateQuery, rowDiff + row + rowCount, warningEmptyKeyResult, warningMoreThen10kRowsResult, errorResult);

                        } else {

                            //m = QueryTabModule.QUERY_TRIGGER_PATTERN.matcher(replacedStr);
                            if (false) {//m.find()) {

//                                int rowCount = q.substring(0, m.start(1)).split("\\n").length - 2; // first + last = 2
//                                checkQueries(area, position, q.substring(m.start(1), m.end(1)), controller, session, needValidateQuery, rowDiff + row + rowCount, warningEmptyKeyResult, warningMoreThen10kRowsResult, errorResult);

                            } else {

                                //m = QueryTabModule.QUERY_EVENT_PATTERN.matcher(replacedStr);
                                if (false) {//m.find()) {

//                                    int rowCount = q.substring(0, m.start(1)).split("\\n").length - 2; // first + last = 2
//                                    checkQueries(area, position, q.substring(m.start(1), m.end(1)), controller, session, needValidateQuery, rowDiff + row + rowCount, warningEmptyKeyResult, warningMoreThen10kRowsResult, errorResult);

                                } else {

                                    if (size >= 3) {

                                        // vaidate query
                                        if (needValidateQuery.get()) {
                                            List<String> list = QueryTabModule.successfullyExecutedQueries.get(controller.getSelectedConnectionSession().get());

                                            String queryStr = q.trim();
                                            if (queryStr.endsWith(";")) {
                                                queryStr = queryStr.substring(0, queryStr.length() - 1).trim();
                                            }

                                            if (list == null || !list.contains(queryStr)) {

                                                if (/*needIgnore || */QueryValidation.validateQuery(queryStr)) {
                                                    return;
                                                }
                                            }

                                            RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder(area);
                                            if (builder.getNeedExplain()) {
                                                builder.setNeedExplain(false);
                                                
                                                boolean hasError = false;

                                                warningEmptyKeyResult.put(row, null);
                                                // if query valid - check explain extended
                                                // if text less then 100kb
                                                queryStr = key;
                                                if (queryStr.endsWith(";") && queryStr.startsWith("SELECT")) {
                                                    
                                                    String queryTmp;
                                                    
                                                    if (session.getService().getDbSQLVersionMaj() > 5) {
                                                        queryTmp = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), query);
                                                    } else {
                                                        queryTmp = String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN_EXTENDED), query);
                                                    }
                                                    
                                                    try (ResultSet rs = (ResultSet) session.getService().execute(queryTmp)) {
                                                        if (rs != null) {
                                                            long totalRows = 1l;
                                                            while (rs.next()) {
                                                                String table = rs.getString("table");
                                                                if (table != null) {
                                                                    if (table.equalsIgnoreCase("mysql") || table.equalsIgnoreCase("information_schema") || table.equalsIgnoreCase("performance_schema") || table.equalsIgnoreCase("sys")) {
                                                                        continue;
                                                                    }
                                                                }

                                                                String extra = rs.getString("Extra");
                                                                boolean needCheckWarning = extra != null && (extra.toLowerCase().contains("using filesort") || extra.toLowerCase().contains("where") || extra.toLowerCase().contains("using temporary"));
                                                                if (rs.getString("key") == null && needCheckWarning) {
                                                                    warningEmptyKeyResult.put(row, query.trim());
                                                                    QueryTabModule.warningEmptyKeyQueries.add(key);                                                                                            
                                                                }           
                                                                long rows = rs.getLong("rows");
                                                                if (rows > 0) {
                                                                    totalRows *= rows;
                                                                }
                                                            }

                                                            if (totalRows > 10000) {
                                                                warningMoreThen10kRowsResult.put(row, query.trim());
                                                                QueryTabModule.warningMoreThen10kRowsQueries.add(key);
                                                            }
                                                        }
                                                    } catch (SQLException ex) {
                                                        errorResult.put(row + rowDiff, ex.getMessage());
                                                        QueryTabModule.validatedQueries.put(key, ex.getMessage());
                                                        hasError = true;
                                                    }
                                                }

                                                if (!hasError) {
                                                    QueryTabModule.validatedQueries.put(key, StringUtils.EMPTY);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable ex) {
                // if exception - invalid
                errorResult.put(row + rowDiff, ex.getMessage());
                QueryTabModule.validatedQueries.put(query.trim().toUpperCase(), ex.getMessage());
                //log.debug("Error", ex);
            }
        }
    }
}
