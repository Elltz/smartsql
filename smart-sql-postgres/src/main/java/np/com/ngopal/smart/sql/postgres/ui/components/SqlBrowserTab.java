
package np.com.ngopal.smart.sql.ui.components;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.SqlBroswerTabOffHandWorker;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class SqlBrowserTab extends Tab {
    
    private static SqlBroswerTabOffHandWorker sqlWorker;
    
    public static Image greeImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/connection_green.png");
    public static Image whiteImage = new Image("/np/com/ngopal/smart/sql/ui/images/connection/connection_white.png");
    
    @Getter
    private boolean closedConnection = false;    
    
    @Getter
    @Setter
    private ConnectionSession session;
    
    public SqlBrowserTab(ConnectionSession session) {
        this.session = session;
        if (sqlWorker == null) {
            sqlWorker = new SqlBroswerTabOffHandWorker();
        }
        sqlWorker.addTab(this);

        graphicProperty().addListener(new ChangeListener<Node>() {
            @Override
            public void changed(ObservableValue<? extends Node> ov, Node t, Node newValue) {
                if (newValue != null) {
                    ((Label) getGraphic()).setGraphic(new ImageView(closedConnection ? whiteImage : greeImage));
                    graphicProperty().removeListener(this);
                }
            }
        });

    }
    
    
    @Override
    public boolean equals(Object tab) {
        Label tabLabel = (Label) getGraphic();
        return tab instanceof Tab && tabLabel != null && tabLabel.getText() != null
                && ((Tab)tab).getGraphic() instanceof Label && tabLabel.getText().equals(
                        ((Label) ((Tab) tab).getGraphic()).getText());
        // return tab instanceof Tab && getText() != null && getText().equals(((Tab) tab).getText());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    
    public void setClosedConnection(boolean closedConnection) {
        this.closedConnection = closedConnection;
        ((ImageView)((Label)getGraphic()).getGraphic()).setImage(closedConnection ? whiteImage : greeImage);
    }

}
