package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TreeView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Pair;
import javax.swing.SwingUtilities;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.utils.FavoritesUtils;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.model.Favourites;
import np.com.ngopal.smart.sql.ui.components.TreeTextFieldEditor;
import np.com.ngopal.smart.sql.ui.provider.SmartBaseControllerProvider;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 *
 * @author Terah laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class OrganiseFavouriteController extends BaseController implements Initializable{
    
    @FXML
    private AnchorPane mainUI;  
    
    @FXML
    private Button closeButton;
    
    @FXML
    private TextField nameTextfield;
    
    @FXML
    private TreeView<Favourites> treeview;
    
    @FXML
    private Button createFolderButton;
            
    @FXML
    private Button moveToFolderButton;
            
    @FXML
    private Button renameButton;
            
    @FXML
    private Button deleteButton;      
    
    @FXML
    private Button newButton;
    
    @FXML
    private Button saveButton;
    
    @FXML
    private VBox container;
    
    private RSyntaxTextArea textArea;    
                
    @Inject
    private FavouritesService favServ;
    
    @Inject
    public SmartBaseControllerProvider provider;
    
    private TreeItem<Favourites> root;
            
    private Stage dialog;
    
    private QueryTabModule querymodule;
    
    
    public void init(Window win, String query){
        
        RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), null);
        SwingUtilities.invokeLater(() -> {
            builder.init();
            textArea = builder.getTextArea();
            textArea.setText(query);

            textArea.setEnabled(false);
            Platform.runLater(() -> {
                container.getChildren().add(builder.getSwingNode());
            });
            
            VBox.setVgrow(builder.getSwingNode(), Priority.ALWAYS);
        });
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(555);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setResizable(false);
        dialog.setTitle("Organise Favorites");
        dialog.setScene(new Scene(getUI()));      
        
        treeview.getSelectionModel().select(root);
        
        dialog.showAndWait();
    }
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }    

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        
        querymodule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
        
        root = new TreeItem(new Favourites(null, true, FavoritesUtils.FAVORITES_ROOT_NAME, null, null, false), new ImageView(FavoritesUtils.IMAGE__FAVORITES));

        treeview.setRoot(root);
        root.setExpanded(true);
        
        treeview.setEditable(true);
        treeview.setCellFactory((TreeView<Favourites> player) -> new TreeTextFieldEditor());
        treeview.setOnEditCommit((TreeView.EditEvent<Favourites> event) -> {
            
            Favourites fav = event.getNewValue();
            if (favServ.find(fav.getParent(), fav.getTempName(), fav.isFolder()) == null) {
                fav.setName(fav.getTempName());
                favServ.save(fav);
            } else {
                ((MainController)getController()).showError("Error", "Cannot create Favorite Folder", null, dialog);
            }
            
            Platform.runLater(() -> treeview.requestFocus());
        });
        
        treeview.setOnEditCancel((TreeView.EditEvent<Favourites> event) -> {
            Platform.runLater(() -> treeview.requestFocus());
        });
        
        treeview.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        treeview.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TreeItem<Favourites>> observable, TreeItem<Favourites> oldValue, TreeItem<Favourites> newValue) -> {

            // always clear
            if (newValue != null) {
                
                Favourites fav = newValue.getValue();
                if (fav != null) {
                    // if not folder - append query text to code area
                    if (!fav.isFolder()) {
                        newButton.setDisable(true);
                        saveButton.setDisable(false);
                        nameTextfield.setDisable(true);
                        if (fav.getQuery() != null) {
                            textArea.setText(fav.getQuery());
                            textArea.setEnabled(true);
                            nameTextfield.setText(fav.getName());
                        }
                    } else {
                        newButton.setDisable(false);
                        saveButton.setDisable(true);
                        nameTextfield.setDisable(false);
                        nameTextfield.setText("");
                    }
                }
            } else {
                textArea.setText("");
                textArea.setEnabled(false);
                newButton.setDisable(true);
                saveButton.setDisable(true);
                nameTextfield.setDisable(true);
                nameTextfield.setText("");
            }
            
            // dieable buttons if selected node is root
            renameButton.setDisable(newValue == root);
            moveToFolderButton.setDisable(newValue == root);
            deleteButton.setDisable(newValue == root);
        });        
       
        
        createFolderButton.setOnAction(onAction);
        deleteButton.setOnAction(onAction);
        moveToFolderButton.setOnAction(onAction);
        renameButton.setOnAction(onAction);
        closeButton.setOnAction(onAction);
        
        FavoritesUtils.refreshTree(favServ, root);
    }
    
    private final EventHandler onAction = new EventHandler<Event>() {
        @Override
        public void handle(Event event) {
            if (event.getSource() == createFolderButton) {
               FavoritesUtils.createFolder(getController(), dialog, favServ, treeview.getSelectionModel().getSelectedItem(), root);
               
            } else if (event.getSource() == deleteButton) {
                
                TreeItem<Favourites> selectedItem = treeview.getSelectionModel().getSelectedItem();
                if(selectedItem.getValue() != null){
                    
                    DialogResponce dr = ((MainController)getController()).showConfirm("Delete", "Do you really want to delete '" + selectedItem.getValue().getName() +"'?", dialog, false);
                    if(dr == DialogResponce.OK_YES){
                        
                        FavoritesUtils.deleteTreeItem(favServ, selectedItem);                        
                        FavoritesUtils.refreshTree(favServ, root);
                    }
                }
                
            } else if (event.getSource() == moveToFolderButton) {
                
                TreeItem<Favourites> selectedItem = treeview.getSelectionModel().getSelectedItem();
                Favourites selected = selectedItem.getValue();
                if(selected != null){
                    provider.setInput("MoveToFolderFavourite");

                    MoveToFolderFavouriteController controller = (MoveToFolderFavouriteController) provider.get();
                    controller.init(dialog);

                    Favourites toFolderFav = controller.getSelected();
                    if (toFolderFav != null) {
                        
                        if (toFolderFav.getId() != null) {
                            if (!checkIfCanMove(selectedItem, toFolderFav)) {
                                ((MainController)getController()).showError("Error", "You can not move '" + selected.getName() + "' to itself.", null, dialog);
                                return;
                            }
                            
                            selected.setParent(toFolderFav.getId());
                        } else {
                            selected.setParent(null);
                        }
                        
                        favServ.save(selected);
                        FavoritesUtils.refreshTree(favServ, root);
                    }
                }
                
            } else if (event.getSource() == renameButton) {
                treeview.edit(treeview.getSelectionModel().getSelectedItem());
                
            } else if (event.getSource() == closeButton) {
                dialog.close();
            }
        }

    };    
    
    @FXML
    public void saveAction(ActionEvent event) {
        Favourites favorite = treeview.getSelectionModel().getSelectedItem().getValue();
        if (favorite != null) {
            
            favorite.setQuery(textArea.getText());
            
            favServ.save(favorite);
            
            RsSyntaxCompletionHelper.getInstance().refreshFavorites();
        }
    }
    
    @FXML
    public void newAction(ActionEvent event) {
        String res = nameTextfield.getText();
        
        if (res == null || res.isEmpty()) {
            ((MainController)getController()).showError("Error", "Please enter a reference name for the query", null, dialog);
            return;
        }
        
        Favourites favorite = null;
        Favourites parent = treeview.getSelectionModel().getSelectedItem().getValue();
        if (parent.isFolder()) {
            
            Favourites exist = favServ.find(parent.getId(), res, false);
            if (exist != null) {
                
                DialogResponce responce = ((MainController)getController()).showConfirm("Add Favorite", "The name you have entered for the shortcut already exist \nin Favorites menu. Would you like to overwrite it?", dialog, false);
                if (responce == DialogResponce.OK_YES) {
                    favorite = exist;
                    
                } else {
                    favorite = null;
                }                
            } else {
                
                favorite = new Favourites();
                favorite.setName(res);
                if (!parent.equals(root.getValue())) {
                    favorite.setParent(parent.getId());
                }                            
            }            
        }
        
        if (favorite != null) {
            QueryAreaWrapper area = querymodule.getSessionCodeArea();
            String query = area.getSelectedText();

            // find query by position
            if (query == null || query.isEmpty()) {
                Pair<Integer, String> result = new ScriptRunner(null, true).findQueryAtPosition(area.getText(), area.getCaretPosition(CARET_POSITION_FROM_START), false);
                query = result != null ? result.getValue() : null;
            }

            // if still empty - need show error
            if (query == null || query.isEmpty()) {
                ((MainController)getController()).showError("Error", "the Query editor is empty! Please enter some query/queries in the editor.", null, dialog);
                return;

            }

            favorite.setQuery(query);
            favServ.save(favorite);
            RsSyntaxCompletionHelper.getInstance().refreshFavorites();
            
            FavoritesUtils.refreshTree(favServ, root);
        }
    }
    
    private boolean checkIfCanMove(TreeItem<Favourites> selectedItem, Favourites fav) {
        if (selectedItem != null && selectedItem.getValue() != null) {
            
            if (fav.getId().equals(selectedItem.getValue().getId())) {
                return false;
            }
            
            if (selectedItem.getChildren() != null) {
                for (TreeItem<Favourites> item: selectedItem.getChildren()) {
                    if (!checkIfCanMove(item, fav)) {
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
}
