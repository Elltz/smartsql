package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.DatabaseCompareUtils;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.ui.provider.SmartBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;


/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaSynchronizerTabContentController extends TabContentController {

    @Inject
    public SmartBaseControllerProvider provider;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    @FXML
    private AnchorPane mainUI;
    @FXML
    private BorderPane centerContainer;
    
    
    private final ObjectProperty<Tab> nullTab = new SimpleObjectProperty<>();
    private final ObjectProperty<TreeItem> nullTreeItem = new SimpleObjectProperty<>();
    
  
    public SchemaSynchronizerTabContentController() {
    }
    
    @Override
    public void changeFont(String font) {
    }
    
    @Override
    public void addOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }
    
    @Override
    public void removeOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }
    
    public void init(SyncCompareInfo compareInfo) {
            
        SyncCompareResult result = DatabaseCompareUtils.compare(compareInfo);         


        Platform.runLater(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/np/com/ngopal/smart/sql/ui/SynchronizationDiffComponent.fxml"));
                loader.load();

                SynchronizationDiffComponent component = ((MainController)getController()).getTypedBaseController("SynchronizationDiffComponent");
                component.init(this, preferenceService, result, compareInfo);
                centerContainer.setCenter(component.getUI());
            } catch (IOException ex) {
                throw new RuntimeException("Unable to initialize SynchronizationDiffComponent", ex);
            }
        });
    }
        
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    @Override
    public void openConnection(ConnectionParams params) throws SQLException {
        Platform.runLater(() -> {
            
            BorderPane busyPane = new BorderPane();
            ProgressIndicator indicator = new ProgressIndicator(-1);
            indicator.setPrefSize(50, 50);
            busyPane.setCenter(new Group(indicator));
            busyPane.getStyleClass().add("busyPane");
            
            centerContainer.setCenter(busyPane);
            
            SchemaSynchronizerWizardController syncController = ((MainController)getController()).getTypedBaseController("SchemaSynchronizerWizard");
            syncController.show(getConnectionParams()); 
            
            SyncCompareInfo selectedComapredInfo =  syncController.getSelectedComapreInfo();
            if (selectedComapredInfo != null) {
                Recycler.addWorkProc(new WorkProc() {
                    @Override
                    public void updateUI() {}

                    @Override
                    public void run() {
                        init(selectedComapredInfo);
                    }
                });
            }
        });
    }

    @Override
    public void focusLeftTree() {
        // do nothing
    }
    
    @Override
    public void addTab(Tab t) {
    }

    @Override
    public Tab getTab(String id, String text) {
        return null;
    }
    
    @Override
    public void showTab(Tab t) {
    }

    @Override
    public void addTabToBottomPane(Tab... tab) {
    }
    
    @Override
    public void removeTabFromBottomPane(Tab... tab) {
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
        return nullTab;
    }

    @Override
    public List<Tab> getBottamTabs() {
        return null;
    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem() {
        return nullTreeItem;
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return true;
    }

    @Override
    public String getTabName() {
        return "Schema Synchronizer";
    }

    @Override
    public DisconnectPermission destroyController(DisconnectPermission permission) {
        return permission;
    }

    @Override
    public void refreshTree(boolean alertDatabaseCount) {
    }
    
    @Override
    public Object getTreeViewUserData() {
        return null;
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void isFocusedNow() {}
    
    @Override
    public void showLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hideLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty(){
        return null;
    }
}
