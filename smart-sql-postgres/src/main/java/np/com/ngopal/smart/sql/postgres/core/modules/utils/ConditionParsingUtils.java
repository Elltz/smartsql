
package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class ConditionParsingUtils {

    private Map<Integer, String> conditions = new HashMap<>();
    private Map<Integer, String> functions = new HashMap<>();
    private Integer conditionIndex = 0;
    private Integer functionIndex = 0;
    
    public static final void main(String[] args) {
        new ConditionParsingUtils().parse("i>3 and count(i)>1 or (r<4 or (s=3 and i>5))");
    }

    public void parse(String query ){
        String parsedQuery = parseConditions(query);
        
        
        
        System.out.println("Query: " + query);
        System.out.println("Parsed query: " + parsedQuery);
        
        for (int i = 0; i < conditionIndex; i++) {
            System.out.println("Sub condition ^" + i + ": " + conditions.get(i));
        }
        
        for (int i = 0; i < functionIndex; i++) {
            System.out.println("Sub functions $" + i + ": " + functions.get(i));
        }
    }
 
    private String parseConditions(String query) {
        
        // find '( ... )' block            
        int brachesDiff = 0;
        int start = -1;
        String prefText = "";
        boolean function = false;
        for (int i = 0; i < query.length(); i++)
        {
            switch (query.charAt(i))
            {
                case '(':
                    if (start == -1) {
                        start = i;
                        
                        if (!prefText.endsWith(" ") && !prefText.toUpperCase().endsWith(" AND") && !prefText.toUpperCase().endsWith(")AND") &&
                                !prefText.toUpperCase().endsWith(" OR") && !prefText.toUpperCase().endsWith(")OR")) {
                            start = prefText.lastIndexOf(" ");
                            function = true;
                        }
                    }
                    
                    brachesDiff++;
                    break;

                case ')':
                    brachesDiff--;
                    break;
            }

            // we find end of select query
            if (brachesDiff == 0 && start > -1) {
                if (function) {
                    functions.put(functionIndex, query.substring(start + 1, i + 1));
                    query = query.substring(0, start + 1) + "$" + functionIndex + query.substring(i + 1);
                    functionIndex++;
                } else {
                    conditions.put(conditionIndex, query.substring(start + 1, i));
                    query = query.substring(0, start) + "^" + conditionIndex + query.substring(i + 1);
                    conditionIndex++;
                }
                
                query = parseConditions(query);
                break;
            }
            
            prefText += query.charAt(i);
        }
        
        
        return query;
    }    
}
