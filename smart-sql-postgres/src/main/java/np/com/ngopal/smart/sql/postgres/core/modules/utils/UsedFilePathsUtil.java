package np.com.ngopal.smart.sql.core.modules.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public final class UsedFilePathsUtil {
    
    private UsedFilePathsUtil() {}
    
    
    private static final Object lock = new Object();
    
    public static final String TYPE__EXPORT_TABLE = "exportTable.dat";
    public static final String TYPE__EXPORT_AS_PDF_TABLE = "exportAsPdfTable.dat";
    public static final String TYPE__EXPORT_AS_XLS_TABLE = "exportAsXlsTable.dat";
    public static final String TYPE__IMPORT_TABLE = "importTable.dat";
    
    
    
    public static List<String> loadFilePaths(String type) {
        synchronized (lock) {
            try {
                return Files.readAllLines(new File(type).toPath());
            } catch (IOException ex) {}

            return new ArrayList<>();
        }
    }
    
    public static void addFilePaths(String type, String newPath) {
        synchronized (lock) {
            try {
                Files.write(new File(type).toPath(), (newPath + "\n").getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
