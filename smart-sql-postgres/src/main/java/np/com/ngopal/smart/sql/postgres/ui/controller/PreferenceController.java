package np.com.ngopal.smart.sql.ui.controller;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.FrequencyExecutedQueriesModule;
import np.com.ngopal.smart.sql.core.modules.HistoryTabModule;
import np.com.ngopal.smart.sql.core.modules.InformationModule;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.TabledataTabModule;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import org.controlsfx.dialog.FontSelectorDialog;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import tools.java.pats.models.SqlFormatter;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class PreferenceController extends BaseController implements Initializable {
    
    @Inject
    PreferenceDataService preferenceService;

    private static final String TAB_USER_DATA__SQL_FORMATER = "SqlFormater";
    private static final String TAB_USER_DATA__GENERAL = "General";
    private static final String TAB_USER_DATA__POWER_TOOLS = "PowerTools";
    private static final String TAB_USER_DATA__FONTS = "Fonts";
    private static final String TAB_USER_DATA__OTHERS = "Others";
    private static final String TAB_USER_DATA__QUERY_OPTIMIZER = "QueryOptimizer";
    private static final String TAB_USER_DATA__QUERY_BROWSER = "QueryBrowser";
    private static final String TAB_USER_DATA__DEBUGGER = "Debugger";
  
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TabPane tabPane;

    @FXML
    private RadioButton blockStyle;
    @FXML
    private RadioButton expandedStyle;
    @FXML
    private CheckBox indentElements;
    @FXML
    private TextField indentationField;
    @FXML
    private BorderPane previewAreaPane;
    private RSyntaxTextArea previewArea;
    
    @FXML
    private Tab settingsTab;
    
    private ConfigurableQueryBinderController configQueryBinder;
    
    private ObjectProperty<PreferenceData> preferenceData;
    
    @FXML
    private AnchorPane settingsPane;
    
    
    @FXML
    private CheckBox queriesUsingBackquote;
    @FXML
    private CheckBox keepFocusOnEditor;
    @FXML
    private CheckBox promtUnsavedTab;
    @FXML
    private CheckBox pasteNameOnDoubleClick;
    @FXML
    private CheckBox wordWrapInEditor;
    @FXML
    private CheckBox transactionSupport;
    @FXML
    private CheckBox showWarnings;
    @FXML
    private CheckBox haltExecutionOnError;
    
    @FXML
    private CheckBox exportOptionsServerDefault;
    @FXML
    private TextField exportOptionsCustomSize;
    @FXML
    private CheckBox exportOptionsDontBreakIntoChunks;
    @FXML
    private TextField exportOptionsChunkSize;
    
    @FXML
    private CheckBox positioningTableData;
    @FXML
    private CheckBox positioningInfoData;
    @FXML
    private CheckBox positioningHistory;
    
    
    @FXML
    private CheckBox enableAutocomplete;
    @FXML
    private CheckBox showAutocompleteHelp;
    
    @FXML
    private CheckBox enableQueryProfiler;
    @FXML
    private CheckBox showProfile;
    @FXML
    private CheckBox explainResult;
    @FXML
    private CheckBox statusVariables;
    @FXML
    private CheckBox explainExtended;
    
    
    @FXML
    private Label fontsSqlEditor;
    @FXML
    private Label fontsHistoryInfo;
    @FXML
    private Label fontsBlobViewer;
    @FXML
    private Label fontsObjectBrowser;
    @FXML
    private Label fontsOthers;
    
    
    @FXML
    private ComboBox<String> caseKeywords;
    @FXML
    private ComboBox<String> caseFunctions;
    
    @FXML
    private TextField tabsSize;
    @FXML
    private RadioButton tabsInsertSpaces;
    @FXML
    private RadioButton tabsKeepTabs;
    
    @FXML
    private TextField paggingNumberRows;
    @FXML
    private CheckBox miscRestoreSession;
    
    @FXML
    private TextField debuggerRefreshInterval;
    @FXML
    private CheckBox debuggerEnableBlink;
    @FXML
    private TextField debuggerRQSlowField;
    @FXML
    private TextField debuggerRQMediumField;
    @FXML
    private TextField debuggerRQNormalField;
    @FXML
    private TextField debuggerDeadlockHistoryField;
    @FXML
    private TextField debuggerDeadlockAlertField;
    
    @FXML
    private CheckBox debuggerAlertRQ;
    @FXML
    private CheckBox debuggerAlertTR;
    @FXML
    private CheckBox debuggerAlertDL;
    @FXML
    private CheckBox debuggerAlertQL;
    @FXML
    private CheckBox debuggerAlertConn;
    @FXML
    private CheckBox debuggerAlertDBLoad;
    @FXML
    private CheckBox debuggerAlertConfig;
    @FXML
    private CheckBox debuggerAlertRL;
    @FXML
    private CheckBox debuggerAlertRM;
    @FXML
    private CheckBox debuggerAlertFK;
    @FXML
    private CheckBox debuggerAlertOT;
    @FXML
    private CheckBox debuggerAlertBP;
    @FXML
    private CheckBox debuggerAlertRO;
    @FXML
    private CheckBox debuggerAlertInnoDBStatus;
    @FXML
    private CheckBox debuggerAlertMyISAMStatus;
    @FXML
    private CheckBox debuggerAlertDBSize;
    @FXML
    private CheckBox debuggerAlertUI;
    @FXML
    private CheckBox debuggerAlertDI;
    @FXML
    private CheckBox debuggerAlertSQA;
    @FXML
    private CheckBox debuggerAlertEL;
    
    
    @FXML
    private TextField indexMaxNoColumns;
    @FXML
    private TextField indexSubstrLength;
    
    @FXML
    private CheckBox otherUseCache;
    
    @FXML
    private CheckBox qbActivateAutoComplete;
    @FXML
    private CheckBox qbColumns;
    @FXML
    private CheckBox qbTablesViews;
    @FXML
    private CheckBox qbDatabase;
    @FXML
    private CheckBox qbVariables;
    @FXML
    private CheckBox qbStoredProcedures;
    @FXML
    private CheckBox qbHistory;
    @FXML
    private CheckBox qbFavorites;
    @FXML
    private CheckBox qbFeq;
    
    @FXML
    private CheckBox qbEnableAnimation;
    @FXML
    private CheckBox qbFlipAnimation;
    @FXML
    private CheckBox qbExpandAnimation;
    
    
    private boolean tableDataOld = false;
    private boolean historyOld = false;
    private boolean infoDataOld = false;
    
    private boolean enableAutocompleteOld = false;
    private boolean enableShowAutocompleteHelpOld = false;
    
    private String fontsSqlEditorOld;
    private String fontsHistoryInfoOld;
    private String fontsBlobViewerOld;
    private String fontsObjectBrowserOld;
    private String fontsOthersOld;
    
    private String tabsSizeOld;
    private boolean tabsInsertSpacesOld;
    
    private boolean qbActivateAutoCompleteOld;
    private boolean qbColumnsOld;
    private boolean qbTablesViewsOld;
    private boolean qbDatabaseOld;
    private boolean qbVariablesOld;
    private boolean qbStoredProceduresOld;
    private boolean qbHistoryOld;
    private boolean qbFavoritesOld;
    private boolean qbFeqOld;
        
    private static final String TEMP_QUERY = "SELECT t1.column1 AS c1, t1.column2 AS c2 FROM table1 t1, table2 t2 WHERE t1.column1 = t2.column1 GROUP BY c1, c2 ORDER BY c1";

    public void selectTab(int tab) {
        tabPane.getSelectionModel().select(tab);
    }
    
    private String changeFont(String font) {
        
        Font f = FontUtils.parseFXFont(font);
        
        FontSelectorDialog dialog = new FontSelectorDialog(f);
        Optional<Font> result = dialog.showAndWait();
        
        if (result != null && result.isPresent() && result.get() != null) {
            f = result.get();
            return f.getFamily() + ", " + f.getStyle() + ", " + f.getSize();
        }
        
        return font;
    }
    
    @FXML
    public void changeSqlEditor(ActionEvent event) {
        String result = changeFont(preferenceData.get().getFontsSqlEditor());
        fontsSqlEditor.setText(result != null ? result : "");
        
        preferenceData.get().setFontsSqlEditor(result);
        
    }
    
    @FXML
    public void changeHistoryInfo(ActionEvent event) {
        
        String result = changeFont(preferenceData.get().getFontsHistoryInfo());
        fontsHistoryInfo.setText(result != null ? result : "");
        
        preferenceData.get().setFontsHistoryInfo(result);        
    }
    
    @FXML
    public void changeBlobViewer(ActionEvent event) {
        String result = changeFont(preferenceData.get().getFontsBlobViewer());
        fontsBlobViewer.setText(result != null ? result : "");
        
        preferenceData.get().setFontsBlobViewer(result);        
    }
    
    @FXML
    public void changeObjectBrowser(ActionEvent event) {        
        String result = changeFont(preferenceData.get().getFontsObjectBrowser());
        fontsObjectBrowser.setText(result != null ? result : "");
        
        preferenceData.get().setFontsObjectBrowser(result);        
    }
    
    @FXML
    public void changeOthers(ActionEvent event) {
        String result = changeFont(preferenceData.get().getFontsOthers());
        fontsOthers.setText(result != null ? result : "");
        
        preferenceData.get().setFontsOthers(result);
    }
    
    private void makePreview()
    {
        String formated;
        
        SqlFormatter formatter = new SqlFormatter();
        if (preferenceData.get() != null) {
            formated = formatter.formatSql(TEMP_QUERY, preferenceData.get().getTab(), preferenceData.get().getIndentString(), preferenceData.get().getStyle());
        } else {
            formated = formatter.formatSql(TEMP_QUERY, "", "2", "block");
        }
        
        SwingUtilities.invokeLater(() -> {
            previewArea.setText(formated);
        });
    }
    
    @FXML
    public void saveAction(ActionEvent event) {
        preferenceService.save(preferenceData.get());
        
        if (tableDataOld != preferenceData.get().isPositioningTableData()) {
            TabledataTabModule module = (TabledataTabModule) ((MainController)getController()).getModuleByClassName(TabledataTabModule.class.getName());
            for (ConnectionSession cs: ((MainController)getController()).getSessionsMap().values()) {
                module.reopenTab(cs);
            }
        }
        
        if (historyOld != preferenceData.get().isPositioningHistory()) {
            HistoryTabModule module = (HistoryTabModule) ((MainController)getController()).getModuleByClassName(HistoryTabModule.class.getName());
            for (ConnectionSession cs: ((MainController)getController()).getSessionsMap().values()) {
                module.reopenTab(cs);
            }
        }
        
        if (infoDataOld != preferenceData.get().isPositioningInfoData()) {
            InformationModule module = (InformationModule) ((MainController)getController()).getModuleByClassName(InformationModule.class.getName());
            for (ConnectionSession cs: ((MainController)getController()).getSessionsMap().values()) {
                module.reopenTab(cs);
            }
        }
        
        if (enableAutocompleteOld != preferenceData.get().isAutocompleteEnable() || enableShowAutocompleteHelpOld != preferenceData.get().isAutocompleteShowHelp()) {
            RSyntaxTextAreaBuilder.hideAllAutocomplete(!preferenceData.get().isAutocompleteEnable() || !preferenceData.get().isAutocompleteShowHelp());
        }
        
        
        if (!Objects.equal(fontsSqlEditorOld, preferenceData.get().getFontsSqlEditor())) {
            RSyntaxTextAreaBuilder.changeAllFont(preferenceData.get().getFontsSqlEditor());
        }
        
        if (!Objects.equal(fontsHistoryInfoOld, preferenceData.get().getFontsHistoryInfo())) {
            HistoryTabModule module0 = (HistoryTabModule) ((MainController)getController()).getModuleByClassName(HistoryTabModule.class.getName());
            module0.changeAllFont(preferenceData.get().getFontsHistoryInfo());
            
            FrequencyExecutedQueriesModule module1 = (FrequencyExecutedQueriesModule) ((MainController)getController()).getModuleByClassName(FrequencyExecutedQueriesModule.class.getName());
            module1.changeAllFont(preferenceData.get().getFontsHistoryInfo());
        }
        
        if (!Objects.equal(fontsBlobViewerOld, preferenceData.get().getFontsBlobViewer())) {
            // no need
        }
        
        if (!Objects.equal(fontsObjectBrowserOld, preferenceData.get().getFontsObjectBrowser())) {
            ((MainController)getController()).changeObjectFont(preferenceData.get().getFontsObjectBrowser());
        }
        
        if (!Objects.equal(fontsOthersOld, preferenceData.get().getFontsOthers())) {
            QueryTabModule module = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());            
            module.updateResultTabsFont(preferenceData.get().getFontsOthers());
        }
        
        if (!Objects.equal(tabsSizeOld, preferenceData.get().getTabsSize()) || tabsInsertSpacesOld != preferenceData.get().isTabsInsertSpaces()) {
            RSyntaxTextAreaBuilder.changeAllTabSettings(preferenceData.get().getTabsSize(), preferenceData.get().isTabsInsertSpaces());
        }
        
        if (qbActivateAutoCompleteOld != preferenceData.get().isQbActivateAutoComplete()) {
            RSyntaxTextAreaBuilder.updateAllAutoComplete();
        }
        
        if (qbColumnsOld != preferenceData.get().isQbColumns() ||
                qbTablesViewsOld != preferenceData.get().isQbTablesViews() ||
                qbDatabaseOld != preferenceData.get().isQbDatabase() ||
                qbVariablesOld != preferenceData.get().isQbVariables() ||
                qbStoredProceduresOld != preferenceData.get().isQbStoredProcedures() ||
                qbHistoryOld != preferenceData.get().isQbHistory() ||
                qbFavoritesOld != preferenceData.get().isQbFavorites() ||
                qbFeqOld != preferenceData.get().isQbFeq()) {
            
            ((MainController)getController()).rebuildTags();
        }
                
        getStage().close();
    }
    
    @FXML
    public void restoreAllDefaultAction(ActionEvent event) {
        preferenceData.get().restoreAllDefaults();
        updateFields(preferenceData.get());
    }
    
    @FXML
    public void restoreTabDefaultAction(ActionEvent event) {
        
        Tab selectedTab = tabPane.getSelectionModel().getSelectedItem();
        if (selectedTab != null && selectedTab.getUserData() != null)
        {
            switch (selectedTab.getUserData().toString())
            {
                case TAB_USER_DATA__SQL_FORMATER:
                    preferenceData.get().restoreSqlFormatDefaults();
                    updateFields(preferenceData.get());
                    break;
                    
                case TAB_USER_DATA__GENERAL:
                    preferenceData.get().restoreGeneralDefaults();
                    updateFields(preferenceData.get());
                    break;
                    
                case TAB_USER_DATA__POWER_TOOLS:
                    preferenceData.get().restorePowerToolsDefaults();
                    updateFields(preferenceData.get());
                    break;
                    
                case TAB_USER_DATA__FONTS:
                    preferenceData.get().restoreFontsDefaults();
                    updateFields(preferenceData.get());
                    break;
                
                case TAB_USER_DATA__OTHERS:
                    preferenceData.get().restoreOthersDefaults();
                    updateFields(preferenceData.get());
                    break;
                    
                case TAB_USER_DATA__QUERY_OPTIMIZER:
                    preferenceData.get().restoreQADefaults();
                    updateFields(preferenceData.get());
                    break;
                    
                case TAB_USER_DATA__QUERY_BROWSER:
                    preferenceData.get().restoreQueryBrowserDefaults();
                    updateFields(preferenceData.get());
                    break;
                    
                case TAB_USER_DATA__DEBUGGER:
                    preferenceData.get().restoreDebuggerDefaults();
                    updateFields(preferenceData.get());
                    break;
            }
        }
    }

    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        exportOptionsCustomSize.disableProperty().bind(exportOptionsServerDefault.selectedProperty());
        exportOptionsChunkSize.disableProperty().bind(exportOptionsDontBreakIntoChunks.selectedProperty());
        
        showAutocompleteHelp.disableProperty().bind(enableAutocomplete.selectedProperty().not());
        
        indentationField.disableProperty().bind(indentElements.selectedProperty().not());
        
        qbColumns.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbTablesViews.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbDatabase.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbVariables.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbStoredProcedures.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbHistory.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbFavorites.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        qbFeq.disableProperty().bind(qbActivateAutoComplete.selectedProperty().not());
        
        qbFlipAnimation.disableProperty().bind(qbEnableAnimation.selectedProperty().not());
        qbExpandAnimation.disableProperty().bind(qbEnableAnimation.selectedProperty().not());
        
        RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), 
                ((MainController)getController()).getSelectedConnectionSession().get());
        
        SwingUtilities.invokeLater(() -> {
            builder.init();
            previewArea = builder.getTextArea();
            previewArea.setEditable(false);
            Platform.runLater(() -> {
                previewAreaPane.setCenter(builder.getSwingNode());
            });
        });
        
                
        preferenceData = new SimpleObjectProperty<>();

        final List<PreferenceData> list = preferenceService.getAll();
        if (list == null || list.isEmpty()) {
            preferenceData.set(new PreferenceData());
        } else {
            preferenceData.set(list.get(0));
        }
        
        tableDataOld = preferenceData.get().isPositioningTableData();
        historyOld = preferenceData.get().isPositioningHistory();
        infoDataOld = preferenceData.get().isPositioningInfoData();
        
        enableAutocompleteOld = preferenceData.get().isAutocompleteEnable();
        enableShowAutocompleteHelpOld = preferenceData.get().isAutocompleteShowHelp();
        
        fontsSqlEditorOld = preferenceData.get().getFontsSqlEditor();
        fontsHistoryInfoOld = preferenceData.get().getFontsHistoryInfo();
        fontsBlobViewerOld = preferenceData.get().getFontsBlobViewer();
        fontsObjectBrowserOld = preferenceData.get().getFontsObjectBrowser();
        fontsOthersOld = preferenceData.get().getFontsOthers();
        
        tabsSizeOld = preferenceData.get().getTabsSize();
        tabsInsertSpacesOld = preferenceData.get().isTabsInsertSpaces();
        
        qbActivateAutoCompleteOld = preferenceData.get().isQbActivateAutoComplete();
        qbColumnsOld = preferenceData.get().isQbColumns();
        qbTablesViewsOld = preferenceData.get().isQbTablesViews();
        qbDatabaseOld = preferenceData.get().isQbDatabase();
        qbVariablesOld = preferenceData.get().isQbVariables();
        qbStoredProceduresOld = preferenceData.get().isQbStoredProcedures();
        qbHistoryOld = preferenceData.get().isQbHistory();
        qbFavoritesOld = preferenceData.get().isQbFavorites();
        qbFeqOld = preferenceData.get().isQbFeq();
        
        updateFields(preferenceData.get());
        propertyListeners();
        
        makePreview();
    }

    private void updateFields(PreferenceData preferenceData) {
        
        if (preferenceData.isBlockStyle()) {
            blockStyle.setSelected(true);
        } else {
            expandedStyle.setSelected(true);
        }
        
        indentElements.setSelected(preferenceData.isIndentEnabled());        
        indentationField.setText(String.valueOf(preferenceData.getIdentation()));    
        
        queriesUsingBackquote.setSelected(preferenceData.isQueriesUsingBackquote());
        keepFocusOnEditor.setSelected(preferenceData.isKeepFocusOnEditor());
        promtUnsavedTab.setSelected(preferenceData.isPromtUnsavedTab());
        pasteNameOnDoubleClick.setSelected(preferenceData.isPasteNameOnDoubleClick());
        wordWrapInEditor.setSelected(preferenceData.isWordWrapInEditor());
        transactionSupport.setSelected(preferenceData.isTransactionSupport());
        showWarnings.setSelected(preferenceData.isShowWarnings());
        haltExecutionOnError.setSelected(preferenceData.isHaltExecutionOnError());
        exportOptionsServerDefault.setSelected(preferenceData.isExportOptionsServerDefault());
        exportOptionsCustomSize.setText(preferenceData.getExportOptionsCustomSize());
        exportOptionsDontBreakIntoChunks.setSelected(preferenceData.isExportOptionsDontBreakIntoChunks());
        exportOptionsChunkSize.setText(preferenceData.getExportOptionsChunkSize());
        positioningTableData.setSelected(preferenceData.isPositioningTableData());
        positioningInfoData.setSelected(preferenceData.isPositioningInfoData());
        positioningHistory.setSelected(preferenceData.isPositioningHistory());
        
        
        enableAutocomplete.setSelected(preferenceData.isAutocompleteEnable());
        showAutocompleteHelp.setSelected(preferenceData.isAutocompleteShowHelp());
        
        enableQueryProfiler.setSelected(preferenceData.isProfillerQueryEnable());
        showProfile.setSelected(preferenceData.isProfillerShowProfile());
        explainResult.setSelected(preferenceData.isProfillerExplainResult());
        statusVariables.setSelected(preferenceData.isProfillerStatusVariables());
        explainExtended.setSelected(preferenceData.isProfillerExplainExtended());
        
        
        fontsSqlEditor.setText(preferenceData.getFontsSqlEditor());
        fontsHistoryInfo.setText(preferenceData.getFontsHistoryInfo());
        fontsBlobViewer.setText(preferenceData.getFontsBlobViewer());
        fontsObjectBrowser.setText(preferenceData.getFontsObjectBrowser());
        fontsOthers.setText(preferenceData.getFontsOthers());
        
        tabsSize.setText(preferenceData.getTabsSize());
        if (preferenceData.isTabsInsertSpaces()) {
            tabsInsertSpaces.setSelected(true);
        } else {
            tabsKeepTabs.setSelected(true);
        }
        
        miscRestoreSession.setSelected(preferenceData.isMiscRestoreSession());
        paggingNumberRows.setText(preferenceData.getPaggingNumberRows());
        
        debuggerRefreshInterval.setText(preferenceData.getDebuggerRefreshInterval());
        debuggerEnableBlink.setSelected(preferenceData.isDebuggerEnableBlink());
        
        debuggerRQSlowField.setText(preferenceData.getDebuggerRQSlow());
        debuggerRQMediumField.setText(preferenceData.getDebuggerRQMedium());
        debuggerRQNormalField.setText(preferenceData.getDebuggerRQNormal());
        
        debuggerDeadlockHistoryField.setText(preferenceData.getDebuggerDeadlockHistory());
        debuggerDeadlockAlertField.setText(preferenceData.getDebuggerDeadlockAlert());
        
        debuggerAlertRQ.setSelected(preferenceData.isDebuggerAlertRQ());
        debuggerAlertTR.setSelected(preferenceData.isDebuggerAlertTR());
        debuggerAlertDL.setSelected(preferenceData.isDebuggerAlertDL());
        debuggerAlertQL.setSelected(preferenceData.isDebuggerAlertQL());
        debuggerAlertConn.setSelected(preferenceData.isDebuggerAlertConn());
        debuggerAlertDBLoad.setSelected(preferenceData.isDebuggerAlertDBLoad());
        debuggerAlertConfig.setSelected(preferenceData.isDebuggerAlertConfig());
        debuggerAlertRL.setSelected(preferenceData.isDebuggerAlertRL());
        debuggerAlertRM.setSelected(preferenceData.isDebuggerAlertRM());
        debuggerAlertFK.setSelected(preferenceData.isDebuggerAlertFK());
        debuggerAlertOT.setSelected(preferenceData.isDebuggerAlertOT());
        debuggerAlertBP.setSelected(preferenceData.isDebuggerAlertBP());
        debuggerAlertRO.setSelected(preferenceData.isDebuggerAlertRO());
        debuggerAlertInnoDBStatus.setSelected(preferenceData.isDebuggerAlertInnoDBStatus());
        debuggerAlertMyISAMStatus.setSelected(preferenceData.isDebuggerAlertMyISAMStatus());
        debuggerAlertDBSize.setSelected(preferenceData.isDebuggerAlertDBSize());
        debuggerAlertUI.setSelected(preferenceData.isDebuggerAlertUI());
        debuggerAlertDI.setSelected(preferenceData.isDebuggerAlertDI());
        debuggerAlertSQA.setSelected(preferenceData.isDebuggerAlertSQA());
        debuggerAlertEL.setSelected(preferenceData.isDebuggerAlertEL());
        
        indexMaxNoColumns.setText(preferenceData.getQaIndexMaxNoColumns());
        indexSubstrLength.setText(preferenceData.getQaIndexSubstrLength());
        otherUseCache.setSelected(preferenceData.isQaOtherUseCache());
        
        
        qbActivateAutoComplete.setSelected(preferenceData.isQbActivateAutoComplete());
        qbColumns.setSelected(preferenceData.isQbColumns());
        qbTablesViews.setSelected(preferenceData.isQbTablesViews());
        qbDatabase.setSelected(preferenceData.isQbDatabase());
        qbVariables.setSelected(preferenceData.isQbVariables());
        qbStoredProcedures.setSelected(preferenceData.isQbStoredProcedures());
        qbHistory.setSelected(preferenceData.isQbHistory());
        qbFavorites.setSelected(preferenceData.isQbFavorites());
        qbFeq.setSelected(preferenceData.isQbFeq());
        
        qbEnableAnimation.setSelected(preferenceData.isQbEnableAnimation());
        qbFlipAnimation.setSelected(preferenceData.isQbFlipAnimation());
        qbExpandAnimation.setSelected(preferenceData.isQbExpandAnimation());    
    }

    public void propertyListeners() {
        blockStyle.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setBlockStyle(newValue);
            makePreview();
        });
        
        indentationField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                preferenceData.get().setIdentation(Integer.parseInt(newValue));                
            } catch (NumberFormatException ex) {
                preferenceData.get().setIdentation(0);
            }
            
            makePreview();
        });
        
        queriesUsingBackquote.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQueriesUsingBackquote(newValue);
        });
        keepFocusOnEditor.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setKeepFocusOnEditor(newValue);
        });
        promtUnsavedTab.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setPromtUnsavedTab(newValue);
        });
        pasteNameOnDoubleClick.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setPasteNameOnDoubleClick(newValue);
        });
        wordWrapInEditor.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setWordWrapInEditor(newValue);
        });
        transactionSupport.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setTransactionSupport(newValue);
        });
        showWarnings.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setShowWarnings(newValue);
        });
        haltExecutionOnError.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setHaltExecutionOnError(newValue);
        });
        exportOptionsServerDefault.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setExportOptionsServerDefault(newValue);
        });
        exportOptionsCustomSize.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setExportOptionsCustomSize(newValue);
        });
        exportOptionsDontBreakIntoChunks.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setExportOptionsDontBreakIntoChunks(newValue);
        });
        exportOptionsChunkSize.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setExportOptionsChunkSize(newValue);
        });
        positioningTableData.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setPositioningTableData(newValue);
        });
        positioningInfoData.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setPositioningInfoData(newValue);
        });
        positioningHistory.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setPositioningHistory(newValue);
        });
        
        
        enableAutocomplete.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setAutocompleteEnable(newValue);
        });
        showAutocompleteHelp.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setAutocompleteShowHelp(newValue);
        });
        
        enableQueryProfiler.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setProfillerQueryEnable(newValue);
        });
        showProfile.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setProfillerShowProfile(newValue);
        });
        explainResult.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setProfillerExplainResult(newValue);
        });
        statusVariables.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setProfillerStatusVariables(newValue);
        });
        explainExtended.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setProfillerExplainExtended(newValue);
        });
        
        tabsSize.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setTabsSize(newValue);
        });
        
        tabsInsertSpaces.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setTabsInsertSpaces(newValue);
        });
        
        
        paggingNumberRows.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setPaggingNumberRows(newValue);
        });
        
        miscRestoreSession.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setMiscRestoreSession(newValue);
        });
        
        
        debuggerRefreshInterval.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setDebuggerRefreshInterval(newValue);
        });
        
        debuggerRQSlowField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setDebuggerRQSlow(newValue);
        });
        
        debuggerRQMediumField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setDebuggerRQMedium(newValue);
        });
        
        debuggerRQNormalField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setDebuggerRQNormal(newValue);
        });
        
        debuggerDeadlockAlertField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            
            try {
                int v = Integer.valueOf(newValue);
                if (v < 1) {
                    debuggerDeadlockAlertField.setText("1");
                } else if (v > 60) {
                    debuggerDeadlockAlertField.setText("60");
                }
            } catch (Throwable th) {}
            
            preferenceData.get().setDebuggerDeadlockAlert(newValue);
        });
        
        debuggerDeadlockHistoryField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            
            try {
                int v = Integer.valueOf(newValue);
                if (v < 1) {
                    debuggerDeadlockHistoryField.setText("1");
                } else if (v > 30) {
                    debuggerDeadlockHistoryField.setText("30");
                }
            } catch (Throwable th) {}
                    
            preferenceData.get().setDebuggerDeadlockHistory(newValue);
        });
        
        
        miscRestoreSession.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setMiscRestoreSession(newValue);
        });
        
        debuggerAlertRQ.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertRQ(newValue);
        });
        debuggerAlertTR.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertTR(newValue);
        });
        debuggerAlertDL.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertDL(newValue);
        });
        debuggerAlertQL.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertQL(newValue);
        });
        debuggerAlertConn.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertConn(newValue);
        });
        debuggerAlertDBLoad.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertDBLoad(newValue);
        });
        debuggerAlertConfig.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertConfig(newValue);
        });
        debuggerAlertRL.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertRL(newValue);
        });
        debuggerAlertRM.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertRM(newValue);
        });
        debuggerAlertFK.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertFK(newValue);
        });
        debuggerAlertOT.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertOT(newValue);
        });
        debuggerAlertBP.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertBP(newValue);
        });
        debuggerAlertRO.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertRO(newValue);
        });
        debuggerAlertInnoDBStatus.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertInnoDBStatus(newValue);
        });
        debuggerAlertMyISAMStatus.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertMyISAMStatus(newValue);
        });
        debuggerAlertDBSize.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertDBSize(newValue);
        });
        debuggerAlertUI.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertUI(newValue);
        });
        debuggerAlertDI.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertDI(newValue);
        });
        debuggerAlertSQA.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertSQA(newValue);
        });
        debuggerAlertEL.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerAlertEL(newValue);
        });
        
        debuggerEnableBlink.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setDebuggerEnableBlink(newValue);
        });
        
        indexMaxNoColumns.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setQaIndexMaxNoColumns(newValue);
        });
        indexSubstrLength.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            preferenceData.get().setQaIndexSubstrLength(newValue);
        });
        otherUseCache.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQaOtherUseCache(newValue);
        });
        
        
        qbActivateAutoComplete.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbActivateAutoComplete(newValue);
        });
        
        qbColumns.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbColumns(newValue);
        });
        
        qbTablesViews.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbTablesViews(newValue);
        });
        
        qbDatabase.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbDatabase(newValue);
        });
        
        qbVariables.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbVariables(newValue);
        });
        
        qbStoredProcedures.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbStoredProcedures(newValue);
        });
        
        qbHistory.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbHistory(newValue);
        });
        
        qbFavorites.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbFavorites(newValue);
        });
        
        qbFeq.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbFeq(newValue);
        });
        
        
        qbEnableAnimation.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbEnableAnimation(newValue);
        });
        
        qbFlipAnimation.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbFlipAnimation(newValue);
        });
        
        qbExpandAnimation.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            preferenceData.get().setQbExpandAnimation(newValue);
        });
           
    }

    @Override
    public Stage getStage() {
        Stage s = (Stage) mainUI.getScene().getWindow();
        if (s.getOnHiding() == null) {
            QueryTabModule queryModule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
            s.setOnHiding((WindowEvent event) -> {
                if(configQueryBinder != null){ configQueryBinder.closureCode(); }
                for (QueryAreaWrapper qaw : queryModule.getAllCodeAreas()) {
                    qaw.refreshQueryArea();
                }
            });
        }
        return s;
    }

}