package np.com.ngopal.smart.sql.core.modules.utils;

import com.sun.javafx.collections.ObservableListWrapper;
import java.util.Collection;
import java.util.List;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ObservableListWithLastNonDataItem<T> extends ObservableListWrapper<T> {

    private T lastNonDataRow;
    
    public ObservableListWithLastNonDataItem(List<T> list) {
        super(list);
    }
    
    public void removeLastNonDataRow() {
        if (lastNonDataRow != null) {
            remove(lastNonDataRow);
            lastNonDataRow = null;
        }
    }
    
    
    public void initLastNonDataRow(T row) {
        if (lastNonDataRow != null) {
            remove(lastNonDataRow);
        }
        
        lastNonDataRow = row;
        if (lastNonDataRow != null && !contains(lastNonDataRow)) {
            super.add(lastNonDataRow);
        }
    }
    
    public void changeLastNonDataRow(T row) {
        lastNonDataRow = row;
        if (lastNonDataRow != null && !contains(lastNonDataRow)) {
            super.add(lastNonDataRow);
        }
    }

    @Override
    public boolean add(T e) {
        
        if (lastNonDataRow != null) {
            remove(lastNonDataRow);
        }
        
        boolean f = super.add(e);
        if (lastNonDataRow != null && !contains(lastNonDataRow)) {
            super.add(lastNonDataRow);
        }
        
        return f;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        
        if (lastNonDataRow != null) {
            remove(lastNonDataRow);
        }
        
        boolean f = super.addAll(c);
        if (lastNonDataRow != null && !contains(lastNonDataRow)) {
            super.add(lastNonDataRow);
        }
        
        return f;
    }

}
