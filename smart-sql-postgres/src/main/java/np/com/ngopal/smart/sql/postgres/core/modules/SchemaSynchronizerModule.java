package np.com.ngopal.smart.sql.modules;

import java.util.Map;
import javafx.scene.control.TreeItem;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.AbstractConnectionModule;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaSynchronizerModule extends AbstractConnectionModule {
    
    
    @Override
    public int getOrder() {
        return 30;
    }    
    
    @Override
    public boolean isConnection() {
        return true;
    }
    
    @Override
    public Class getDependencyModuleClass() {
        return SchemaSynchronizerModule.class;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {return true; }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public String getInfo() { 
        return "Schema Synchronizer";
    }
}
