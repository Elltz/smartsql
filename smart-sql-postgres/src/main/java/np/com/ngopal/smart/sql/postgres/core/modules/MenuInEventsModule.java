package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInEventsModule extends AbstractStandardModule {

    private Image eventImage;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public int getOrder() {
        return 6;
    }
    
    MenuItem createEvent,alterEvent,dropEvent,renameEvent;
    Menu eventMenu;

    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();
        
        SeparatorMenuItem separate = new SeparatorMenuItem();
        eventMenu = new Menu("Events",
            new ImageView(SmartImageProvider.getInstance().getEvent_image()));
        
        createEvent = getCreateEventMenuItem(null);
        alterEvent = getAlterEventMenuItem(null);
        dropEvent = getDropEventMenuItem(null);
        renameEvent = getRenameEventMenuItem(null);        
        
        eventMenu.disableProperty().bind(busy());
        createEvent.disableProperty().bind(busy());
        alterEvent.disableProperty().bind(busy());
        dropEvent.disableProperty().bind(busy());
        renameEvent.disableProperty().bind(busy());
        
        eventMenu.getItems().addAll(createEvent, alterEvent, dropEvent, separate, renameEvent);
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{eventMenu});
        
        return true;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        boolean isEvent = item.getValue() instanceof Event;
        
        alterEvent.setDisable(!isEvent);
        dropEvent.setDisable(!isEvent);
        renameEvent.setDisable(!isEvent);
        
        createEvent.setDisable(!(isEvent || item.getValue().equals(ConnectionTabContentController.DatabaseChildrenType.EVENTS)));
        return true;
    }
    
    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private MenuItem getCreateEventMenuItem(ConnectionSession session){
          
        MenuItem createEventMenuItem = new MenuItem("Create Event...",
            new ImageView(SmartImageProvider.getInstance().getCreateEvent_image()));
        createEventMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createEventMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createEvent();
            }
        });
        
        return createEventMenuItem;
    }
    
    private MenuItem getDropEventMenuItem(ConnectionSession session) {
        
        MenuItem dropEventMenuItem = new MenuItem("Drop Event...",
            new ImageView(SmartImageProvider.getInstance().getDropEvent_image()));
        dropEventMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropEventMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                ConnectionSession session0 = session != null ? session : getController().getSelectedConnectionSession().get();
                
                Event event = (Event) ((ReadOnlyObjectProperty<TreeItem>) getController().getSelectedTreeItem(session0)).get().getValue();

                DialogResponce responce = DialogHelper.showConfirm(getController(), "Drop event", 
                    "Do you really want to drop the event '" + event.getName() + "'?");
                
                if (responce == DialogResponce.OK_YES) {
                    try {
                        // drop fucntion
                        session0.getService().dropEvent(event);
                        LeftTreeHelper.eventDroped(session0.getConnectionParam(), ((ReadOnlyObjectProperty<TreeItem>) getController().getSelectedTreeItem(session0)).get());
                    } catch (SQLException ex) {
                        DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        
        return dropEventMenuItem;
    }
    
    private MenuItem getAlterEventMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem alterEventMenuItem = new MenuItem("Alter Event..",
            new ImageView(SmartImageProvider.getInstance().getAlterEvent_image()));
        alterEventMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        alterEventMenuItem.setOnAction((ActionEvent e) -> {
            alterEvent();
        });
        
        return alterEventMenuItem;
    }
    
    public void alterEvent() {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        ConnectionSession session0 = getController().getSelectedConnectionSession().get();
        Event event = (Event) ((ReadOnlyObjectProperty<TreeItem>) getController().getSelectedTreeItem(session0)).get().getValue();
        if (eventImage == null) {
            eventImage = SmartImageProvider.getInstance().getEventTab_image();
        }
        try {
            String alterEvent = session0.getService().getAlterEventSQL(event.getDatabase().getName(), event.getName());

            if (alterEvent == null) {
                DialogHelper.showError(getController(), "Access denied", "Access denied for altering event", null);
            } else {
                queryTabModule.addNamedTabWithContext(
                        session0,
                        event.getName(),
                        alterEvent,
                        QueryTabModule.TAB_USER_DATA__EVENT_TAB,
                        null
                );
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
        }
    }
        
    private MenuItem getRenameEventMenuItem(ConnectionSession session){
          
        MenuItem renameEventMenuItem = new MenuItem("Rename Event",
            new ImageView(SmartImageProvider.getInstance().getRenameEvent_image()));
        renameEventMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F2));
        renameEventMenuItem.setOnAction((ActionEvent e) -> {
            renameEvent();
        });

        
        return renameEventMenuItem;
    }
    
    public void renameEvent() {
        ConnectionSession session0 = getController().getSelectedConnectionSession().get();            
        TreeItem ti = ((ReadOnlyObjectProperty<TreeItem>)getController().getSelectedTreeItem(session0)).get();
        Event event = (Event) ti.getValue();
        String newName = DialogHelper.showInput(getController(), "Rename event", "Enter new name of event:", "", event.getName());
        if (newName != null && !newName.isEmpty()){
            try {
                // rename event
                session0.getService().renameEvent(event, newName);                                       

                getController().refreshTree(session0, false);
            } catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);        
        
        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("events0", new ArrayList<>(Arrays.asList(new MenuItem[]{getCreateEventMenuItem(session)}))), Hooks.QueryBrowser.EVENTS);
        map.put(new HookKey("eventItems0", new ArrayList<>(Arrays.asList(new MenuItem[]{getCreateEventMenuItem(session),
            getAlterEventMenuItem(session), getDropEventMenuItem(session)}))), Hooks.QueryBrowser.EVENT_ITEM);
        map.put(new HookKey("eventItems1", new ArrayList<>(Arrays.asList(new MenuItem[]{getRenameEventMenuItem(session)}))), Hooks.QueryBrowser.EVENT_ITEM);

        return map;
    }

    public void createEvent()
    {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null)
        {
            Database db = session.getConnectionParam().getSelectedDatabase();

            if (eventImage == null) {
                eventImage = SmartImageProvider.getInstance().getEventTab_image();
            }
            queryTabModule.addTemplatedQueryTab(
                session, 
                "Create event", 
                "Enter new event name:", 
                new java.util.function.Function<String, String>() {
                    @Override
                    public String apply(String t) {
                        return session.getService().getCreateEventTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                    }
                },QueryTabModule.TAB_USER_DATA__EVENT_TAB );
        }
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
