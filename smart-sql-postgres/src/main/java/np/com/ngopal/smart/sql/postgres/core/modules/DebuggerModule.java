package np.com.ngopal.smart.sql.modules;

import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DebuggerModule extends AbstractStandardModule {
        
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 300;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {return true;}

    @Override
    public boolean install() {               
        registrateServiceChangeListener();
        
        MenuItem debuggerItem = new MenuItem("Debugger", new ImageView(SmartImageProvider.getInstance().getDebug_image()));
        debuggerItem.setOnAction((ActionEvent event) -> {
            showDebugger(getController(), getMainController().getDatabaseComboBox().getSelectionModel().selectedItemProperty(),
                    getMainController().getChartService(), getMainController().getSecondProfilerService(), getMainController().getPreferenceService(),
                    getMainController().getDeadlockService(), getMainController().getRecommendationService(),
                    getMainController().getColumnCardinalityService(), getMainController().getPerformanceTuningService(),
                    getMainController().getSettingsService(), getMainController().getDb(), getMainController().getDebuggerProfilerService(),
                    getMainController().getUsageService());
        });
        
        debuggerItem.disableProperty().bind(busy());
                
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.POWERTOOLS, new MenuItem[] {debuggerItem});
                
        return true;
    }
        
    @Override
    public boolean uninstall() {
        return true;
    }
    
    @Override
    public String getInfo() { 
        return "Slow Analyze Queries";
    }
}