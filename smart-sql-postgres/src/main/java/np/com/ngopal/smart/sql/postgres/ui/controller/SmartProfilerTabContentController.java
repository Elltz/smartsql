/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import np.com.ngopal.smart.sql.structure.controller.ProfilerTabContentController;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class SmartProfilerTabContentController extends ProfilerTabContentController {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        MainController mainController = (MainController) getController();
        chartService = mainController.getChartService();
        dbProfilerService = mainController.getProfilerService();
        profilerService = mainController.getSecondProfilerService();
        preferenceDataService = mainController.getPreferenceService();
        settingsService = mainController.getSettingsService();
        qARecommendationService = mainController.getRecommendationService();
        aPerformanceTuningService = mainController.getPerformanceTuningService();
        columnCardinalityService = mainController.getColumnCardinalityService();
        connectionParamService = mainController.getDb();
        profilerDataManager = mainController.getProfilerService();
        usageService = mainController.getUsageService();
        
        updateSettings();
    }
    
}
