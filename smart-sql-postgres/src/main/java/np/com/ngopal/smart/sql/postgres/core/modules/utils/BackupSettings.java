package np.com.ngopal.smart.sql.core.modules.utils;

import lombok.Setter;

/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Setter
public class BackupSettings {
    
    String charset;
    
    String logFile;
    
    boolean prefixWithTimestamp = false;
    boolean singleTransaction = false;
    boolean lockTables = false;
    boolean flushLogs = false;
    boolean exportOnlyData = false;
    boolean setForeignKeyChecks = false;
    boolean includeCreateDatabase = false;
    boolean includeUseDatabase = false;
    boolean includeDrop = false;
    boolean exportOnlyStructure = false;
    boolean addLockArroundInsert = false;
    boolean createBulkInsert = false;
    boolean ignoreDefiner = false;
    boolean oneRowPerLine = false;
    
    boolean ziping = false;
    boolean subFolderWithTimestamp = false;
    boolean filesPrefixTimestampOverride = true;
    boolean createCompleteInsert = false;
    boolean createDelayedInsert = false;
    boolean flushMasterLog = false;
    boolean flushSlaveLog = false;
    boolean autocommit = false;
    boolean keysArroundInsert = false;
    boolean includeVersionInformation = false;
    
    boolean serverDefaultSize = true;
    
    boolean abortOnError = false;
    
    boolean stopExport = false;
}
