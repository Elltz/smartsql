/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import lombok.AllArgsConstructor;
import lombok.Getter;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author terah laweh (rumorsapp@gmail.com)
 */
public class ShowWarningsController extends BaseController implements Initializable {
    
    @AllArgsConstructor
    @Getter
    public class Warning {
        private String level;
        private String code;
        private String message;
    }
    
    @FXML
    private AnchorPane mainUI;
        
    @FXML
    private TableView<Warning> tableView;
    @FXML
    private TableColumn<Warning, String> levelColumn;
    @FXML
    private TableColumn<Warning, String> codeColumn;
    @FXML
    private TableColumn<Warning, String> messageColumn;
    
    private Stage dialog;
    
    private QueryResult queryResult;
    
        
    @FXML
    public void okAction(ActionEvent event) {
        dialog.close();
    }
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        levelColumn.setCellValueFactory(new PropertyValueFactory<>("level"));
        codeColumn.setCellValueFactory(new PropertyValueFactory<>("code"));
        messageColumn.setCellValueFactory(new PropertyValueFactory<>("message"));
    }
    
    public void init(QueryResult queryResult, Window win){
        
        this.queryResult = queryResult;
        
        if (queryResult.getWarningCodes() != null) {
            for (int i = 0; i < queryResult.getWarningCodes().size(); i++) {
                tableView.getItems().add(new Warning("Warning", queryResult.getWarningCodes().get(i), queryResult.getWarningMessages().get(i)));
            }
        }
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("CSV Import Result");        
        dialog.setScene(new Scene(getUI()));
        dialog.showAndWait();        
    }
    
}
