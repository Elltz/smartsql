package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.ExportTableController;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import tools.java.pats.models.SqlFormatter;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInViewModule extends AbstractStandardModule {
    
    private Image viewImage;
    
    private MenuItem createViewItem;
    private MenuItem alterViewItem;
    private MenuItem dropViewItem;
    private MenuItem renameViewItem;
    private MenuItem exportViewItem;
    private MenuItem viewDataItem;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 2;
    }
    
    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();
        
        SeparatorMenuItem separate = new SeparatorMenuItem();
        Menu viewMenu = new Menu("Views",
            new ImageView(SmartImageProvider.getInstance().getView_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        
        alterViewItem = getAlterViewMenuItem(null);
        dropViewItem = getDropViewMenuItem(null);
        renameViewItem = getRenameViewMenuItem(null);
        exportViewItem = getExportViewMenuItem(null);
        viewDataItem = getViewDataMenuItem(null);
        
        createViewItem = getCreateViewMenuItem(null);
        
        viewMenu.getItems().addAll(createViewItem, alterViewItem, dropViewItem, separate, renameViewItem, exportViewItem, viewDataItem);
              
        alterViewItem.disableProperty().bind(busy());
        dropViewItem.disableProperty().bind(busy());
        renameViewItem.disableProperty().bind(busy());
        exportViewItem.disableProperty().bind(busy());
        viewDataItem.disableProperty().bind(busy());
        createViewItem.disableProperty().bind(busy());
        viewMenu.disableProperty().bind(busy());
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{viewMenu});
        
        if (viewImage == null) {
            viewImage = SmartImageProvider.getInstance().getViewTab_image();
        }
        
        return true;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        
        boolean isView = item.getValue() instanceof View;
        
        alterViewItem.setDisable(!isView);
        dropViewItem.setDisable(!isView);
        renameViewItem.setDisable(!isView);
        
        createViewItem.setDisable(!(isView || item.getValue().equals(ConnectionTabContentController.DatabaseChildrenType.VIEWS)));
        return true;
    }
    
    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    private MenuItem getCreateViewMenuItem(ConnectionSession session) {

        MenuItem createViewMenuItem = new MenuItem("Create View...",
                new ImageView(SmartImageProvider.getInstance().getCreateView_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createViewMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createViewMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createView();
            }
        });

        return createViewMenuItem;
    }
    
    
    private MenuItem getRenameViewMenuItem(ConnectionSession session) {

        MenuItem renameViewMenuItem = new MenuItem("Rename View",
                new ImageView(SmartImageProvider.getInstance().getRenameView_image(DefaultImageProvider.IMAGE_SIZE_WIDTH,DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        renameViewMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F2));
        renameViewMenuItem.setOnAction((ActionEvent event) -> {
            renameView();
        });


        return renameViewMenuItem;
    }
    
    public void renameView() {
        ConnectionSession sessionTemp = getController().getSelectedConnectionSession().get();

        View view = (View) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();

        String newName = showInput("Rename view", "Enter new name of view:", view.getName());
        if (newName != null && !newName.isEmpty()) {

            try {
                sessionTemp.getService().renameView(view, newName);
                getController().refreshTree(sessionTemp, false);
            } catch (SQLException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }
    }
    
    private MenuItem getDropViewMenuItem(ConnectionSession session) {

        MenuItem dropViewMenuItem = new MenuItem("Drop View...", new ImageView(SmartImageProvider.getInstance().getDropView_image()));
        dropViewMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropViewMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession sessionTemp = session!= null? session : getController().getSelectedConnectionSession().get();
                
                View view = (View) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();

                DialogResponce responce = showConfirm("Drop view", 
                    "Do you really want to drop the view '" + view.getName() + "'?");
                
                if (responce == DialogResponce.OK_YES) {
                    try {
                        // drop view
                        sessionTemp.getService().dropView(view);
                        LeftTreeHelper.viewDroped(sessionTemp.getConnectionParam(), getController().getSelectedTreeItem(sessionTemp).get());
                    } catch (SQLException ex) {
                        showError("Error", ex.getMessage(), ex);
                    }
                }
            }
        });

        return dropViewMenuItem;
    }
    
    private MenuItem getViewDataMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem viewDataMenuItem = new MenuItem("View Data", new ImageView(SmartImageProvider.getInstance().getViewData_image()));
        viewDataMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConnectionSession ses = session!= null? session : getController().getSelectedConnectionSession().get();
            
                QueryTabModule qtm = (QueryTabModule)
                        getController().getModuleByClassName(QueryTabModule.class.getName());

                ReadOnlyObjectProperty<Tab> selectedTab = (ReadOnlyObjectProperty<Tab>) getController().getSelectedConnectionTab(ses);

                Tab tab;
                if (selectedTab != null && selectedTab.get() != null && QueryTabModule.TAB_USER_DATA__QUERY_TAB.equals(selectedTab.get().getId())) {
                    tab = selectedTab.get();
                } else {
                    tab = qtm.addQueryTab(ses, null, "");
                }

                TabContentController tcc  = getMainController().getTabControllerBySession(ses);
                if(tcc instanceof ConnectionTabContentController){
                    ConnectionTabContentController ctcc = (ConnectionTabContentController)tcc;
                    ctcc.setSelectedBottomTabId(TabledataTabModule.TableData_ID);
                }
            }
        });
        
        return viewDataMenuItem;
    }
    
    private MenuItem getExportViewMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem exportViewMenuItem = new MenuItem("Export View", new ImageView(SmartImageProvider.getInstance().getExportView_image()));
        exportViewMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConnectionSession currentSession = session != null ? session : getController().getSelectedConnectionSession().get();
                
                TableProvider table = null;
                ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(currentSession);
                if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {            
                    if (selectedTreeItem.getValue().getValue() instanceof TableProvider) {
                        table = (TableProvider)selectedTreeItem.getValue().getValue();
                    }
                }
                if (table != null) {
                    ExportTableController exportTableController = getTypedBaseController("ExportTable");
                    
                    Scene scene = new Scene(exportTableController.getUI());
                    ObservableList<ObservableList> datas = FXCollections.observableArrayList();
                    ObservableList<String> columnsData = FXCollections.observableArrayList();

                    try {
                        ((View)table).setColumns(session.getService().getColumnInformation(table.getDatabase().getName(), table.getName()));
                        if (table.getColumns() != null) {
                            for (Column c: table.getColumns()) {
                                columnsData.add(c.getName());
                            }
                        }
                    } catch (Throwable th) {
                        log.error("Error", th);
                    }
                    
                    String countQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_COUNT, currentSession.getConnectionParam().getSelectedDatabase(), table.getName());
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(countQuery)) { //"SELECT count(*) FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "`")) {
                        if (rs.next()) {
                            if (rs.getLong(1) > ExportUtils.MAX_ROWS_COUNT) {
                                showInfo("Info", "The total rows count is more than " + ExportUtils.MAX_ROWS_COUNT + ",\n only " + ExportUtils.MAX_ROWS_COUNT + " rows can be exported", null);
                            }
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                        
                    String selectAllQuery = getFormatedQuery(QueryProvider.QUERY__SELECT_ALL, currentSession.getConnectionParam().getSelectedDatabase(), table.getName()) + " LIMIT " + ExportUtils.MAX_ROWS_COUNT;
                    try (ResultSet rs = (ResultSet) currentSession.getService().execute(selectAllQuery)) {//"SELECT * FROM `" + currentSession.getConnectionParam().getSelectedDatabase() + "`.`" + table.getName() + "` LIMIT " + ExportUtils.MAX_ROWS_COUNT)) {
                        while (rs.next()) {
                            
                            ObservableList row = FXCollections.observableArrayList();
                            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                                row.add(rs.getObject(i));
                            }
                            
                            datas.add(row);
                        }
                    } catch (SQLException ex) {
                        showError("Error", "Error loading export data", ex);
                        return;
                    }
                    
                    try {
                        exportTableController.init(currentSession.getService(), table, datas, columnsData);

                        Stage stage = new Stage();
                        stage.setTitle("Export As");
                        stage.initModality(Modality.WINDOW_MODAL);
                        stage.initOwner(getController().getStage());
                        stage.setScene(scene);

                        stage.showAndWait();
                    } catch (SQLException ex) {
                        showError("Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        
        return exportViewMenuItem;
    }
    
    private String getQuery(String queryId) {
        return QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, queryId);
    }
    
    private String getFormatedQuery(String queryId, Object...values) {
        return String.format(getQuery(queryId), values);
    }
    
    private MenuItem getAlterViewMenuItem(ConnectionSession session) {

        MenuItem alterViewMenuItem = new MenuItem("Alter View..", new ImageView(SmartImageProvider.getInstance().getAlterView_image()));
        alterViewMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        alterViewMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alterView();
            }
        });

        return alterViewMenuItem;
    }
    
    public void alterView() {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        ConnectionSession sessionTemp = getController().getSelectedConnectionSession().get();

        View view = (View) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();

        try {
            String alterView = sessionTemp.getService().getAlterViewSQL(view.getDatabase().getName(), view.getName());
            if (alterView == null) {
                showError("Access denied", "Access denied for altering view", null);
            } else {

                try {
                    SqlFormatter formatter = new SqlFormatter();

                    PreferenceData pd = preferenceService.getPreference();

                    ScriptRunner scriptRunner = new ScriptRunner(sessionTemp.getService().getDB(), pd.isHaltExecutionOnError());
                    Pair<Integer, String> res = scriptRunner.findQueryAtPosition(alterView, 0, false);
                    if (res.getValue() != null) {
                        alterView = res.getValue();
                    }

                    if (!pd.isQueriesUsingBackquote()) {
                        alterView = alterView.replaceAll("`", "");
                    }

                    alterView = formatter.formatSql(alterView, pd.getTab(), pd.getIndentString(), pd.getStyle());

                    // after formating we can loose DELIMETERS, need return them
                    // DELIMITER $$ \n\n%s$$\n\nDELIMITER ;
                    if (!alterView.trim().startsWith("DELIMITER")) {
                        alterView = "DELIMITER $$ \n\n" + alterView + "$$\n\nDELIMITER";
                    }
                } catch (Throwable th) {
                    log.error("Error", th);
                }

                queryTabModule.addNamedTabWithContext(
                    sessionTemp,
                    view.getName(),
                    alterView,
                    QueryTabModule.TAB_USER_DATA__VIEW_TAB,
                    null);
            }
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);                

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("views0", new ArrayList<>(Arrays.asList(new MenuItem[]{getCreateViewMenuItem(session)}))),
                Hooks.QueryBrowser.VIEWS);
        map.put(new HookKey("viewItems0", new ArrayList<>(Arrays.asList(new MenuItem[]{getCreateViewMenuItem(session),
            getAlterViewMenuItem(session), getDropViewMenuItem(session)}))), Hooks.QueryBrowser.VIEW_ITEM);
        map.put(new HookKey("viewItems1", new ArrayList<>(Arrays.asList(new MenuItem[]{getRenameViewMenuItem(session),
            getExportViewMenuItem(session), getViewDataMenuItem(session)}))), Hooks.QueryBrowser.VIEW_ITEM);
        return map;
    }

    
    public void createView()
    {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null)
        {
            Database db = session.getConnectionParam().getSelectedDatabase();
            
            queryTabModule.addTemplatedQueryTab(
                session, 
                "Create view", 
                "Enter new view name:", 
                new java.util.function.Function<String, String>() {
                    @Override
                    public String apply(String t) {
                        return session.getService().getCreateViewTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                    }
                }, QueryTabModule.TAB_USER_DATA__VIEW_TAB );
        }
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
