package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ViewTablePropertiesController extends BaseController implements Initializable {
 
    @FXML
    private AnchorPane mainUI;
    @FXML
    private TableView<TableStatus> propertiesTable;
    @FXML
    private TableColumn<TableStatus, String> variableColumn;
    @FXML
    private TableColumn<TableStatus, String> valuesColumn;
    
    @FXML
    public void closeAction(ActionEvent event) {
        getStage().close();
    }
    
    public void init(DBService dbService, DBTable table) {
        try (ResultSet rs = (ResultSet) dbService.execute(dbService.getShowTableStatusSQL(table.getDatabase().getName(), table.getName()))) {
            if (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                if (rsmd != null) {
                    for (int i = 0; i < rsmd.getColumnCount(); i++) {
                        Object value = rs.getObject(i + 1);
                        propertiesTable.getItems().add(new TableStatus(rsmd.getColumnName(i + 1), value != null ? value.toString() : "NULL"));
                    }
                }
            }
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "There is an error while loading tables", ex);
        }
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        variableColumn.setCellValueFactory(new PropertyValueFactory<>("variable"));
        valuesColumn.setCellValueFactory(new PropertyValueFactory<>("values"));
    }
    
    @Setter
    @Getter
    @AllArgsConstructor
    public class TableStatus {
        private String variable;
        private String values;
    }
    
    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
