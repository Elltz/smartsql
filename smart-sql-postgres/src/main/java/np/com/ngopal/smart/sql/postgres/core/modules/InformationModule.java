package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.util.HashMap;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.controller.InfoPartitionsController;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class InformationModule extends AbstractStandardModule {
    
    @Inject
    private PreferenceDataService preferenceService;
    
    Image infoImage = SmartImageProvider.getInstance().getDatabaseInfo_image();
    
    MenuItem info;
    
    private HashMap<ConnectionSession, Tab> infoTabsInSession = new HashMap();

    @Override
    public ConnectionParams connectionChanged(ConnectionParams param) throws Exception {
        return super.connectionChanged(param);
    }

    @Override
    public int getOrder() {
        return 4;
    }

    
    
    @Override
    public boolean install() {
        info = new MenuItem("Info", new ImageView(infoImage));
        info.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.SHORTCUT_DOWN,KeyCombination.SHIFT_DOWN));
        
        info.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                final ConnectionSession session = getController().getSelectedConnectionSession().get();
                Tab tab = infoTabsInSession.get(session);
                if (tab == null) {
                    tab = reopenTab(session);
                }
                
                if (tab != null) {
                    getController().showTab(tab, session);
                }
            }
        });
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{info});
        
        return true;
    }
    
    private Tab createInfoTab(ConnectionSession session) {
        Tab infoTab = new Tab("Info");
        infoTab.setClosable(false);
        ImageView imv = new ImageView(infoImage);
        imv.setFitWidth(16);
        imv.setFitHeight(16);
        infoTab.setGraphic(imv);
        
        final InfoPartitionsController ipc = ((MainController)getController()).getTypedBaseController("InfoPartitions");
        
        TreeItem ti = getController().getSelectedTreeItem(session).get();
        if (ti != null) {
            Object value = ti.getValue();

            if (ti.getValue() instanceof String) {
                value = ti.getParent().getValue();
            } else if (ti.getValue() instanceof Column || ti.getValue() instanceof Index){
                value = ti.getParent().getParent().getValue();
            }
            
            ipc.init(value, findDatabaseName(ti));
        }
        
        infoTab.setContent(ipc.getUI());
        infoTab.setUserData(ipc);
        infoTab.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                showInfo(infoTab, getController().getSelectedTreeItem(session).get());
            }
        });
        
        return infoTab;
    }

    @Override
    public void postConnection(ConnectionSession session, Tab tab) throws Exception {        
        Platform.runLater(() -> {
            infoTabsInSession.putIfAbsent(session, createInfoTab(session));
            reopenTab(session);
        });
    }
    
    public Tab reopenTab(ConnectionSession session) {
        Tab tab = infoTabsInSession.get(session);
        if (tab != null) {
            
            if (tab.getTabPane() != null) {
                tab.getTabPane().getTabs().remove(tab);
            }
            
            PreferenceData pd = preferenceService.getPreference();
            if (pd.isPositioningInfoData()) {
                getController().addTabToBottomPane(session, tab);
            } else {
                getController().addTab(tab, session);
            }
        }
        
        return tab;
    }
    

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem ti) {
        Tab tab = infoTabsInSession.get(getController().getSelectedConnectionSession().get());
        if(tab.isSelected()){
            showInfo(tab, ti);
        }
        return true;
    }
    
    private void showInfo(Tab tab, TreeItem ti) {
        InfoPartitionsController ipc = (InfoPartitionsController) tab.getUserData();

        Object value = ti.getValue();

        if (ti.getValue() instanceof String) {
            value = ti.getParent().getValue();
        } else if (ti.getValue() instanceof Column || ti.getValue() instanceof Index){
            value = ti.getParent().getParent().getValue();
        }

        ipc.loadEntity(value, findDatabaseName(ti));
        ipc.requestFocus();
    }

    private String findDatabaseName(TreeItem ti) {
        if (ti != null) {
            if (ti.getValue() instanceof Database) {
                return ((Database)ti.getValue()).getName();
            } else {
                return findDatabaseName(ti.getParent());
            }
        }
        
        return null;
    }
    
    @Override
    public boolean uninstall() { 
        return false;
    }

    @Override
    public String getInfo() { 
        return "Retrieves Information about a selected base object";
    }
}
