package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Terah laweh (rumorsapp@gmail.com)
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class MenuInColumnModule extends AbstractStandardModule {

    private Image tableImage;
        
    private MenuItem dropIndex;
    private MenuItem manageIndex;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 0;
    }
    
    

    @Override
    public boolean install() {
        
        registrateServiceChangeListener();
        
        dropIndex = getDropColumnMenuItem(null);
        manageIndex = getManageColumnsMenuItem(null);
        
        SeparatorMenuItem separate = new SeparatorMenuItem();
        Menu columnMenu = new Menu("Columns", new ImageView(SmartImageProvider.getInstance().getColumnMenu_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        columnMenu.getItems().addAll(dropIndex, separate, manageIndex);
        
        dropIndex.disableProperty().bind(busy());
        manageIndex.disableProperty().bind(busy());
        columnMenu.disableProperty().bind(busy());
        /**
         * Toolbar other menuitem -column menu items are inactive.
         */
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{columnMenu});
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem obj) {
        boolean isTable = obj.getValue() instanceof DBTable;
        boolean isInsideTable = obj.getValue() instanceof String;
        boolean isColumn = obj.getValue() instanceof Column;
        
        manageIndex.setDisable(!isTable && !isInsideTable && !isColumn);
        dropIndex.setDisable(!isColumn);
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        SeparatorMenuItem separate = new SeparatorMenuItem();

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("column", new ArrayList<>(Arrays.asList(new MenuItem[]{getDropColumnMenuItem(null), separate, getManageColumnsMenuItem(null)}))), Hooks.QueryBrowser.COLUMN_ITEM);
        
        return map;
    }
    
    private MenuItem getDropColumnMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */

        MenuItem dropColumnMenuItem = new MenuItem("Drop Column",
        new ImageView(SmartImageProvider.getInstance().getColumnDrop_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        dropColumnMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropColumnMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                
                ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(
                        currentSession);
                
                Column c = (Column)object.get().getValue();
                if (c.getTable().getColumns().size() == 1) {
                    DialogHelper.showError(getController(), "Delete column", "You cannot delete all columns", null);
                    
                } else {
                    DialogResponce responce = DialogHelper.showConfirm(getController(), "Delete column", "Do you really want to drop column (" + c.getName() + ")?");
                    if (responce == DialogResponce.OK_YES) {
                        try {
                            currentSession.getService().dropColumn(c);                            
                            LeftTreeHelper.columnDroped(currentSession.getConnectionParam(), object.get());                            
                            DialogHelper.showInfo(getController(), "Delete column", "Column deleted successfully", null);
                        } catch (SQLException ex) {
                            DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                        }   
                    }
                }
                    
            }
        });
        return dropColumnMenuItem;
    }

    private MenuItem getManageColumnsMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */

        MenuItem manageColumnsMenuItem = new MenuItem("Manage Columns",
        new ImageView(SmartImageProvider.getInstance().getColumnManage_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        manageColumnsMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        manageColumnsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableImage == null) {
                    tableImage = SmartImageProvider.getInstance().getTableTab_image();
                }
                
                ConnectionSession currentSession = session!= null? session : getController().getSelectedConnectionSession().get();
                
                ReadOnlyObjectProperty<TreeItem> object = getController().getSelectedTreeItem(currentSession);
                
                Object obj = object.get().getValue();
                
                DBTable selectedTable = null;
                if (obj instanceof DBTable) {
                    selectedTable = (DBTable)obj;
                    
                } else if (obj instanceof Column) {                
                    selectedTable = (DBTable)((Column)obj).getTable();
                    
                } else if (obj instanceof String) {
                    selectedTable = (DBTable) (object.get().getParent()).getValue();
                }
                
                if (selectedTable != null) {
                    
                    AlterTableController altertableController = null;
                    Tab alterTab = null;
                            
                    ConnectionTabContentController connTab = (ConnectionTabContentController) getController().getTabControllerBySession(currentSession);
                    List<Tab> tabs = connTab.findControllerTabs(AlterTableController.class);
                    if (tabs != null && !tabs.isEmpty()) {
                        
                        for (Tab t: tabs) {
                            if (((AlterTableController)t.getUserData()).getTable().equals(selectedTable)) {
                                altertableController = (AlterTableController)t.getUserData();
                                alterTab = t;
                                break;
                            }
                        }
                        
                    }
                    
                    if (altertableController != null) {
                        
                        altertableController.manageColumns();
                        getController().showTab(alterTab, currentSession);
                        
                    } else {

                        alterTab = new Tab(selectedTable.toString());
                        alterTab.setGraphic(new ImageView(tableImage));

                        altertableController = ((MainController)getController()).getTypedBaseController("AlterTable");
                        altertableController.setService(currentSession.getService());

                        alterTab.setUserData(altertableController);

                        Parent p = altertableController.getUI();
                        alterTab.setContent(p);

                        DBTable table = DatabaseCache.getInstance().getTable(currentSession.getConnectionParam().cacheKey(), 
                            selectedTable.getDatabase().toString(),
                            selectedTable.toString());

                        altertableController.init(alterTab, table, currentSession, true);

                        getController().addTab(alterTab, currentSession);
                        getController().showTab(alterTab, currentSession);
                    }
                    
                } else {
                    log.error("Table is NULL");
                }
            }
        });
        
        return manageColumnsMenuItem;
    }

    @Override
    public String getInfo() { 
        return "";
    }
}
