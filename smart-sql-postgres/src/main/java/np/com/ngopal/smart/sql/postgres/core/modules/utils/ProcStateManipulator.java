/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class ProcStateManipulator {
    
    private Boolean queuedInBackground;
    private Boolean waitingInBackground;

    public ProcStateManipulator(Boolean queuedInBackground, Boolean waitInBackground) {
        this.queuedInBackground = queuedInBackground;
        this.waitingInBackground = waitInBackground;
    }

    public boolean isQueuedInBackground() {
        return queuedInBackground;
    }

    public void setQueuedInBackground(Boolean queuedInBackground) {
        this.queuedInBackground = queuedInBackground;
    }

    public boolean isWaitingInBackground() {
        return waitingInBackground;
    }

    public void setWaitInBackground(Boolean waitInBackground) {
        this.waitingInBackground = waitInBackground;
    }    
    
}
