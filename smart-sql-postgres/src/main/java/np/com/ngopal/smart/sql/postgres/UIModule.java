/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.MysqlTechnology;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.provider.SmartBaseControllerProvider;
import np.com.ngopal.smart.sql.ui.provider.MainControllerProvider;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.db.AppSettingsService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParam4RestorationService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import np.com.ngopal.smart.sql.db.DeadlockService;
import np.com.ngopal.smart.sql.db.DesignerErrDiagramService;
import np.com.ngopal.smart.sql.db.DesignerTableTemplateService;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.db.FindReplaceHistoryService;
import np.com.ngopal.smart.sql.db.H2ProfilerStorageProvider;
import np.com.ngopal.smart.sql.db.HistoryQueryService;
import np.com.ngopal.smart.sql.db.HistoryTemplateService;
import np.com.ngopal.smart.sql.db.LastOsProfilerServerSpeculationsService;
import np.com.ngopal.smart.sql.db.LicenseDataService;
import np.com.ngopal.smart.sql.db.LicenseDataUsageService;
import np.com.ngopal.smart.sql.db.MySQLProfilerStorageProvider;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.RecentFileService;
import np.com.ngopal.smart.sql.db.SSHHotKeyService;
import np.com.ngopal.smart.sql.db.ScheduleParamsService;
import np.com.ngopal.smart.sql.db.ScheduleReportsService;
import np.com.ngopal.smart.sql.db.SessionsSavePointService;
import np.com.ngopal.smart.sql.db.TableTemplateService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.ProfilerSettings;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.model.provider.StorageProvider;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.provider.DefaultProfilerDataManager;
import np.com.ngopal.smart.sql.structure.provider.H2ProfilerDataManager;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class UIModule extends AbstractModule {

    @Inject
    SmartBaseControllerProvider provider;
    
    public static final String persistenceUnit = "persistence-unit";
    public static final String persistenceUnitForProfiler = "profiler-persistence-unit";
    
    //private Injector injector;
    public UIModule() {
        super();
    }

    /**
     * Configuring th google guice UI module
     */
    @Override
    protected void configure() {
//        bind(SQLLogger.class).toProvider(SQLLoggerProvider.class).in(Singleton.class);
//        bind(BaseController.class).toProvider(SmartBaseControllerProvider.class);
//        bind(MainController.class).in(Scopes.SINGLETON);
//        bind(MainController.class).annotatedWith(Names.named("single")).toProvider(MainControllerProvider.class);
        bind(MainController.class).toProvider(MainControllerProvider.class);
        bind(SQLLogger.class).toInstance(provideSQLLogger());
                
        bind(AppSettingsService.class).toInstance(provideAppSettingsService());
        bind(ConnectionParam4RestorationService.class).toInstance(provideConnectionParam4RestorationService());
        bind(ConnectionParamService.class).toInstance(provideConnectionParamService());
        bind(DesignerErrDiagramService.class).toInstance(provideDesignerErrDiagramService());
        bind(DesignerTableTemplateService.class).toInstance(provideDesignerTableTemplateService());
        bind(FavouritesService.class).toInstance(provideFavouritesService());
        bind(HistoryQueryService.class).toInstance(provideHistoryQueryService());
        bind(HistoryTemplateService.class).toInstance(provideHistoryTemplateService());
        bind(PreferenceDataService.class).toInstance(providePreferenceDataService());
        bind(ProfilerChartService.class).toInstance(provideProfilerChartService());
        bind(ProfilerSettingsService.class).toInstance(provideProfilerSettingsService());
        bind(QAPerformanceTuningService.class).toInstance(provideQAPerformanceTuningService());
        bind(RecentFileService.class).toInstance(provideRecentFileService());
        bind(FindReplaceHistoryService.class).toInstance(provideFindReplaceHistoryService());
        bind(SSHHotKeyService.class).toInstance(provideSSHHotKeyService());
        bind(ScheduleParamsService.class).toInstance(provideScheduleParamsService());
        bind(ScheduleReportsService.class).toInstance(provideScheduleReportsService());
        bind(SessionsSavePointService.class).toInstance(provideSessionsSavePointService());
        bind(TableTemplateService.class).toInstance(provideTableTemplateService());
        bind(UserUsageStatisticService.class).toInstance(provideUserUsageStatisticService());
        bind(LastOsProfilerServerSpeculationsService.class).toInstance(LastOsProfilerServerSpeculationsService());
        bind(QARecommendationService.class).toInstance(provideQARecommendationService());
        bind(DeadlockService.class).toInstance(provideDeadlockService());
        bind(ColumnCardinalityService.class).toInstance(provideColumnCardinalityService());
        bind(LicenseDataService.class).toInstance(provideLicenseDataService());
        bind(LicenseDataUsageService.class).toInstance(provideLicenseDataUsageService());
        bind(ProfilerDataManager.class).toInstance(provideDBProfilerService());
        bind(H2ProfilerDataManager.class).toInstance(provideH2DBProfilerService());
        bind(DBDebuggerProfilerService.class).toInstance(provideDBDebuggerProfilerService());
    }

    @Provides
    public MysqlTechnology provideMysqlTechnology() {
        return new MysqlTechnology();
    }
    
    public AppSettingsService provideAppSettingsService() {
        return new AppSettingsService(persistenceUnit);
    }
    
    public ConnectionParam4RestorationService provideConnectionParam4RestorationService() {
        return new ConnectionParam4RestorationService(persistenceUnit);
    }
    
    public ConnectionParamService provideConnectionParamService() {
        return new ConnectionParamService(persistenceUnit);
    }
    
    public DesignerErrDiagramService provideDesignerErrDiagramService() {
        return new DesignerErrDiagramService(persistenceUnit);
    }
    
    public DesignerTableTemplateService provideDesignerTableTemplateService() {
        return new DesignerTableTemplateService(persistenceUnit);
    }
    
    public FavouritesService provideFavouritesService() {
        return new FavouritesService(persistenceUnit);
    }
    
    public HistoryQueryService provideHistoryQueryService() {
        return new HistoryQueryService(persistenceUnit);
    }
    
    public HistoryTemplateService provideHistoryTemplateService() {
        return new HistoryTemplateService(persistenceUnit);
    }
    
    public PreferenceDataService providePreferenceDataService() {
        return new PreferenceDataService(persistenceUnit);
    }
    
    public ProfilerChartService provideProfilerChartService() {
        ProfilerChartService pcs = new ProfilerChartService(persistenceUnitForProfiler);
        pcs.setDefImportChartScript(FunctionHelper.streamToString(
                    StandardResourceProvider.getInstance().getDefaultImportChartScript()));
        // init if need charts
        pcs.getAll(null);
        return pcs;
    }
    
    public ProfilerSettingsService provideProfilerSettingsService() {
        return new ProfilerSettingsService(persistenceUnitForProfiler);
    }
    
    private void checkClearPQ(StorageProvider storage, byte type) {
        File f = StandardApplicationManager.getInstance().getSmartDataDirectory(type);
        if (!f.exists()) {
            try {
                List<String> result = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/META-INF/scripts/clearPQTables.sql"))).lines().collect(Collectors.toList());
                if (result != null) {
                    for (String query: result) {
                        storage.beginTransaction();
                        storage.executeQuery(query, new Object[]{});                        
                        storage.commitTransaction();
                    }
                }

                Files.write(f.toPath(), "done".getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            } catch (Throwable ex) {
                storage.rollbackTransaction();
                Logger.getLogger(StorageProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void applyProviders(ProfilerDataManager dataManager) {
        
        H2ProfilerStorageProvider h2Provider = new H2ProfilerStorageProvider(persistenceUnitForProfiler);
        dataManager.addStorageProvider(QueryProvider.DB__H2, h2Provider);
        
        MySQLProfilerStorageProvider mysqlProvider = new MySQLProfilerStorageProvider();
        dataManager.addStorageProvider(QueryProvider.DB__MYSQL, mysqlProvider);
        
        checkClearPQ(h2Provider, Flags.CLEAR_H2_SQ_FILE);
        
        ProfilerSettingsService pss = provideProfilerSettingsService();
        ProfilerSettings ps = pss.getProfilerSettings();
        
        if (ps == null || ps.getStorageType() == 0) {
            dataManager.activateStorage(QueryProvider.DB__H2);
            
        } else if (ps != null && ps.getStorageType() == 1) {
            
            ConnectionParamService cps = provideConnectionParamService();
            
            MySQLProfilerStorageProvider storage = (MySQLProfilerStorageProvider) dataManager.activateStorage(QueryProvider.DB__MYSQL);
            try {
                storage.init(cps.get(ps.getConnectionId()), ps.getDatabaseName());
                checkClearPQ(storage, Flags.CLEAR_MYSQL_SQ_FILE);
            } catch (SQLException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }
    
    public DBDebuggerProfilerService provideDBDebuggerProfilerService() {
        DBDebuggerProfilerService service = new DBDebuggerProfilerService();
        service.addStorageProvider(QueryProvider.DB__H2, new H2ProfilerStorageProvider(persistenceUnitForProfiler));
        service.activateStorage(QueryProvider.DB__H2);
        
        return service;
    }

    public ProfilerDataManager provideDBProfilerService() {
        DefaultProfilerDataManager dataManager = new DefaultProfilerDataManager();
        applyProviders(dataManager);        
        return dataManager;
    }
    
    public H2ProfilerDataManager provideH2DBProfilerService() {
        H2ProfilerDataManager dataManager = new H2ProfilerDataManager();
        dataManager.addStorageProvider(QueryProvider.DB__H2, new H2ProfilerStorageProvider(persistenceUnitForProfiler));
        dataManager.activateStorage(QueryProvider.DB__H2);
        return dataManager;
    }

    public QAPerformanceTuningService provideQAPerformanceTuningService() {
        return new QAPerformanceTuningService(persistenceUnit);
    }
    
    public FindReplaceHistoryService provideFindReplaceHistoryService() {
        return new FindReplaceHistoryService(persistenceUnit);
    }
    
    public RecentFileService provideRecentFileService() {
        return new RecentFileService(persistenceUnit);
    }
    
    public SSHHotKeyService provideSSHHotKeyService() {
        return new SSHHotKeyService(persistenceUnit);
    }
    
    public ScheduleParamsService provideScheduleParamsService() {
        return new ScheduleParamsService(persistenceUnit);
    }
    
    public ScheduleReportsService provideScheduleReportsService() {
        return new ScheduleReportsService(persistenceUnit);
    }
    
    public SessionsSavePointService provideSessionsSavePointService() {
        return new SessionsSavePointService(persistenceUnit);
    }
    
    public TableTemplateService provideTableTemplateService() {
        return new TableTemplateService(persistenceUnit);
    }
    
    public UserUsageStatisticService provideUserUsageStatisticService() {
        return new UserUsageStatisticService(persistenceUnit);
    }
    
    public LastOsProfilerServerSpeculationsService LastOsProfilerServerSpeculationsService() {
        return new LastOsProfilerServerSpeculationsService(persistenceUnit);
    }
    
    public QARecommendationService provideQARecommendationService() {
        return new QARecommendationService(persistenceUnit);
    }
    
    public DeadlockService provideDeadlockService() {
        return new DeadlockService(persistenceUnit);
    }
    
    public ColumnCardinalityService provideColumnCardinalityService() {
        return new ColumnCardinalityService(persistenceUnit);
    }
    
    public LicenseDataService provideLicenseDataService() {
        return new LicenseDataService(persistenceUnit);
    }
    
    public LicenseDataUsageService provideLicenseDataUsageService() {
        return new LicenseDataUsageService(persistenceUnit);
    }
    
    public SQLLogger provideSQLLogger() {
        return new SQLLogger();
    }
//    public QueryBrowserController provideQueryBrowserController() {
//        try {
//            FXMLLoader loader = new FXMLLoader(MainUI.class.getResource("QueryBrowser.fxml").toURI().toURL());
//            loader.setControllerFactory(new Callback<Class<?>, Object>() {
//
//                @Override
//                public Object call(Class<?> type) {
//                    return injector.getInstance(type);
//                }
//            ;
//            });
//
//            loader.load();
//            return (QueryBrowserController) loader.getController();
//        } catch (Exception ex) {
//            Logger.getLogger(UIModule.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//
//    }
}
