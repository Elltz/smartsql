package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.ComboBoxAutoFilterListener;
import np.com.ngopal.smart.sql.core.modules.utils.UsedFilePathsUtil;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ExportTableController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private ImageView titleImageView;
    
    
    @FXML
    private TitledPane xlsOptions;
    @FXML
    private TextField xlsColumnSize;
    @FXML
    private TextField xlsDecimalPlaces;
    
    
    @FXML
    private TitledPane charsetOptions;
    @FXML
    private ComboBox<String> charsetCombobox;
    
    
    @FXML
    private ComboBox<String> exportPathField;
    
    
    @FXML
    private TitledPane csvOptionPane;
    @FXML
    private TextField fieldsTerminated;
    @FXML
    private TextField fieldsEnclosed;
    @FXML
    private TextField fieldsEscaped;
    @FXML
    private TextField lineTerminated;
    @FXML
    private TextField nullReplacedBy;
    @FXML
    private CheckBox namesOnTop;
    
    @FXML
    private TitledPane sqlOptions;
    @FXML
    private RadioButton sqlStructureOnly;
    @FXML
    private RadioButton sqlDataOnly;
    
    @FXML
    private RadioButton sqlDataAndStructure;
    
    @FXML
    private CheckBox sqlIncludeVersion;
    
    @FXML
    private ListView<String> columnsListView;
    
    @FXML
    private RadioButton csvOption;
    
    @FXML
    private RadioButton htmlOption;
    
    @FXML
    private RadioButton excelOption;
    
    @FXML
    private RadioButton xmlOption;
    
    @FXML
    private RadioButton pdfOption;
    
    @FXML
    private RadioButton delimiterOption;
    
    @FXML
    private RadioButton sqlOption;
  
    @FXML
    private ToggleGroup formatGroup;

    private ObservableList<String> columnsData;
    private ObservableList<ObservableList> rowsData;
    private TableProvider table;
    private  DBService service;

    public boolean response;

    private ComboBoxAutoFilterListener<String> autoFilterListener;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    
    public void setResponse(boolean response) {
        this.response = response;
    }

    public boolean getResponse() {
        return response;
    }

    public RadioButton getSelectedOption() {
        if (csvOption.isSelected()) {
            return csvOption;
        }
        if (htmlOption.isSelected()) {
            return htmlOption;
        }
        if (excelOption.isSelected()) {
            return excelOption;
        }
        if (xmlOption.isSelected()) {
            return xmlOption;
        }
        if (pdfOption.isSelected()) {
            return pdfOption;
        }
        if (delimiterOption.isSelected()) {
            return delimiterOption;
        }
        if (sqlOption.isSelected()) {
            return sqlOption;
        }
        return null;
    }
    
    @FXML
    void columnsSelectAll(ActionEvent event) {
        columnsListView.getSelectionModel().selectAll();
    }
    
    @FXML
    void columnsDeselectAll(ActionEvent event) {
        columnsListView.getSelectionModel().clearSelection();
    }
    

    public String getExtension() {
        RadioButton r = getSelectedOption();
        if (r != null) {
            String selectedItem = getSelectedOption().getText();
            switch (selectedItem) {
                case "Excel":
                    return "xls";

                case "Delimiter file":
                    return "*";

                default:
                    return selectedItem;
            }
        }
        return "*";
    }

    @FXML
    void chooseExportFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As");
        if (getSelectedOption() == null) {
            DialogHelper.showInfo(getController(), "Info", "Please select export option", null);
            return;
        }
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter(getSelectedOption().getText(), "*." + getExtension().toLowerCase()),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(mainUI.getScene().getWindow());
        if (file != null) {
            exportPathField.getEditor().setText(file.getAbsolutePath());
        }
    }

    /**
     * Init  controller with data
     * @param service db service
     * @param table if we export table - need input it for sql export, 
     * if null sql export will be disabled
     * @param rowsData data for export
     * @param columnsData columns of data
     */
    public void init(DBService service, TableProvider table, ObservableList<ObservableList> rowsData, ObservableList<String> columnsData) throws SQLException {
        this.rowsData = rowsData;
        this.columnsData = columnsData;
        this.table = table;
        this.service = service;
        
        charsetCombobox.setItems(FXCollections.observableArrayList(service.getCharSet()));
        charsetCombobox.getSelectionModel().select("utf8");
        if (columnsData != null) {
            columnsListView.setItems(columnsData);
        }
        columnsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        columnsListView.getSelectionModel().selectAll();
        
        exportPathField.getItems().setAll(UsedFilePathsUtil.loadFilePaths(UsedFilePathsUtil.TYPE__EXPORT_TABLE));
        exportPathField.getSelectionModel().selectLast();
        
        autoFilterListener = new ComboBoxAutoFilterListener<>(exportPathField, exportPathField.getItems());
        exportPathField.getEditor().textProperty().addListener(autoFilterListener);
        
        formatGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            if (formatGroup.getSelectedToggle() != null) {
                String filename = exportPathField.getEditor().getText();
                if (filename != null) {
                    if (filename.contains(".")) {
                        filename = filename.substring(0,
                                filename.lastIndexOf("."));
                        System.out.println("extension:" + getExtension());
                        filename += "." + getExtension().toLowerCase();
                        exportPathField.getEditor().setText(filename);
                    }
                }
            }
        });
        
        
        csvOptionPane.disableProperty().bind(csvOption.selectedProperty().or(delimiterOption.selectedProperty()).not());
        charsetOptions.disableProperty().bind(csvOption.selectedProperty().or(delimiterOption.selectedProperty()).or(excelOption.selectedProperty()).not());
        xlsOptions.disableProperty().bind(excelOption.selectedProperty().not());
        sqlOptions.disableProperty().bind(sqlOption.selectedProperty().not());
        
        sqlOption.setDisable(table == null);
        
        sqlOption.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                DialogHelper.showInfo(getController(), "SmartSQL", "This saves the data in most simple form of SQL statement.\nAdditional table information (indexes, charset, comments etc.) is not saved.\nIf you want to take a backup of your data please use \n'Backup table as SQL Dump' instead.", null, getStage());
            }
        });
    }

    @FXML
    void export(ActionEvent event) {

        if (getSelectedOption() == null) {
            DialogHelper.showError(getController(), "Error", "Please select export type", null);
            return;
        }
        
        String filename = exportPathField.getEditor().getText();
        if (filename == null || filename.equals("")) {
            DialogHelper.showError(getController(), "Error", "Please Specify filename", null);
            return;
        }
        
        if (columnsListView.getSelectionModel().getSelectedItems().isEmpty()) {
            DialogHelper.showError(getController(), "Error", "Please select at least one column", null);
            return;
        }
        
        if (new File(filename).exists()) {
            if (DialogHelper.showConfirm(getController(), "File alreay exist", "File already exist, do you want to replace it?", getStage(), false) != DialogResponce.OK_YES) {
                return;
            }
        }
        
        if (!exportPathField.getItems().contains(filename)) {
            
            autoFilterListener.addSourcePath(filename);
            
            UsedFilePathsUtil.addFilePaths(UsedFilePathsUtil.TYPE__EXPORT_TABLE, filename);
            exportPathField.getItems().add(filename);
        }
                
        ExportUtils.ExportStatusCallback exportCallback = new ExportUtils.ExportStatusCallback() {
            @Override
            public void error(String errorMessage, Throwable th) {
                log.error(errorMessage, th);
                DialogHelper.showError(getController(), "Error", errorMessage, th);
            }

            @Override
            public void success() {
                DialogResponce resp = DialogHelper.showConfirm(getController(), "Information",
                    "Data exported Successfully, Would you like to open file?");
                
                if (resp == DialogResponce.OK_YES) {
                    try {
                        Desktop.getDesktop().open(new File(filename));
                    } catch (IOException ex) {
                        Platform.runLater(() -> {
                            DialogHelper.showError(getController(), "Error", "There is an error while opening exported file", ex);
                        });
                    }
                }
            }
        };
        
        LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
        for (String c: columnsListView.getItems()) {
            columns.put(c, columnsListView.getSelectionModel().getSelectedItems().contains(c));
        }

        switch (getSelectedOption().getText()) {
            case "CSV":
            case "Delimiter file":
                ExportUtils.exportDelimiter(filename, rowsData, columns, 
                    charsetCombobox.getSelectionModel().getSelectedItem(), fieldsTerminated.getText(), fieldsEnclosed.getText(), lineTerminated.getText(), nullReplacedBy.getText(), namesOnTop.isSelected(), exportCallback);
                break;
                
            case "Excel":
                
                Integer maxTextSizeLimit = null;
                try {
                    maxTextSizeLimit = Integer.valueOf(xlsColumnSize.getText());
                } catch (NumberFormatException ex) {
                }
                
                Integer decimalPlaces = null;
                try {
                    decimalPlaces = Integer.valueOf(xlsDecimalPlaces.getText());
                } catch (NumberFormatException ex) {
                }
                
                ExportUtils.exportXLS(filename, rowsData, columns, maxTextSizeLimit, decimalPlaces, charsetCombobox.getSelectionModel().getSelectedItem(), exportCallback);
                break;
            
            case "PDF":
                ExportUtils.exportPDF(filename, rowsData, columns, exportCallback);
                break;
                
            case "HTML":
                ExportUtils.exportHTML(filename, rowsData, columns, exportCallback);
                break;
                
            case "XML":
                ExportUtils.exportXML(filename, rowsData, columns, exportCallback);
                break;

            case "SQL":
                ExportUtils.ExportSQLType type = ExportUtils.ExportSQLType.STUCTURE_AND_DATA;
                if (sqlDataOnly.isSelected()) {
                    type = ExportUtils.ExportSQLType.ONLY_DATA;
                } else if (sqlStructureOnly.isSelected()) {
                    type = ExportUtils.ExportSQLType.ONLY_STRUCTURE;
                }
                
                String version = null;
                try {
                    if (sqlIncludeVersion.isSelected()) {
                        DatabaseMetaData metadata = service.getDB().getMetaData();
                        version = metadata.getDatabaseProductVersion();
                    }
                } catch (SQLException ex) {
                    DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
                }
                
                PreferenceData pd = preferenceService.getPreference();
                                
                Long size = null;
                Long rows = null;

                if (!pd.isExportOptionsServerDefault()) {
                    try {
                        size = Long.valueOf(pd.getExportOptionsCustomSize()) * 1024;
                    } catch (Throwable th) {}
                }

                if (!pd.isExportOptionsDontBreakIntoChunks()) {
                    try {
                        rows = Long.valueOf(pd.getExportOptionsChunkSize());
                    } catch (Throwable th) {}
                }
                ExportUtils.exportSQL(filename, table, rowsData, columns, type, version, size, rows, exportCallback);
                break;
        }

        

    }

    @FXML
    void cancel(ActionEvent event) {
        Stage stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        titleImageView.setImage(SmartImageProvider.getInstance().getTableExport_image());
    }

}
