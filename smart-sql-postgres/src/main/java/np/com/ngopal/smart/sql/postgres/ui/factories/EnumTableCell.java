package np.com.ngopal.smart.sql.ui.factories;

import java.util.function.BiConsumer;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import lombok.Getter;


public class EnumTableCell<S, T> extends TableCell<S, T> {

    private final ComboBox<T> comboBox;

    private ObservableValue<T> ov;

    private String item;

    public ComboBox getComboBox() {
        return comboBox;
    }

    public EnumTableCell(ObservableList lists, BiConsumer<EnumTableCell.TableCellData, T> acceptFunction, BooleanBinding editable) {
        
        setStyle("-fx-padding: 0");
        
        this.comboBox = new ComboBox();
        comboBox.setId("enumTableCombobox");
        
        comboBox.setItems(lists);
        comboBox.setMaxWidth(Double.MAX_VALUE);
        comboBox.valueProperty().addListener((ObservableValue<? extends T> observable, T oldValue, T newValue) -> {
            
            this.item = newValue != null ? newValue.toString() : "(NULL)";            
            setItem((T)newValue);
        
            TableCellData data = new TableCellData();
            data.row = getIndex();
            data.column = getTableView().getColumns().indexOf(getTableColumn());
            
            acceptFunction.accept(data, newValue);
        });
        
//        setAlignment(Pos.LEFT);
    }

    public String item() {
        return item;
    }

    @Override
    public void startEdit() {
        if (! isEditable() || ! getTableView().isEditable() || ! getTableColumn().isEditable()) {
            return;
        }
        
        comboBox.getSelectionModel().select(getItem());

        super.startEdit();
        setText(null);
        setGraphic(comboBox);
    }
    
    @Override 
    public void cancelEdit() {
        super.cancelEdit();

        setText(item);
        setGraphic(null);
    }

    
    

    @Override 
    public void updateItem(T item, boolean empty) {

        super.updateItem(item, empty);
        if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
            this.item = (String)item;
            
            if (isEditing()) {
                comboBox.getSelectionModel().select(item);
                setGraphic(comboBox);
            } else {
                setText(item != null ? item.toString() : "(NULL)");
            }
        } else {
            setGraphic(null);
        }
    }
    
    @Getter
    public class TableCellData {
        int row;
        int column;
    }
}
