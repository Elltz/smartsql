package np.com.ngopal.smart.sql.core.modules.utils;

import javafx.scene.control.TreeItem;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.structure.utils.*;


/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class LeftTreeHelper {

    public static final void tableAltered(ConnectionTabContentController contollers, TreeItem tableItem, DBTable table) {
        if (tableItem != null) {
            tableItem.setValue(table);
        }
    }
    
    public static final void columnDroped(ConnectionParams params, TreeItem columnItem) {
        TreeItem parent = columnItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(columnItem);
        } else {
            parent.getChildren().remove(columnItem);
        }
        
        ((Column)columnItem.getValue()).getTable().getColumns().remove((Column)columnItem.getValue());
        
        DatabaseCache.getInstance().columnDroped(params.cacheKey(), (Column)columnItem.getValue());
    }
    
    public static final void indexDroped(ConnectionParams params, TreeItem indexItem) {
        TreeItem parent = indexItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(indexItem);
        } else {
            parent.getChildren().remove(indexItem);
        }
        
        ((Index)indexItem.getValue()).getTable().getIndexes().remove((Index)indexItem.getValue());
        
        DatabaseCache.getInstance().indexDroped(params.cacheKey(), (Index)indexItem.getValue());
    }
    
    public static final void tableDroped(ConnectionParams params, TreeItem tableItem) {
        TreeItem parent = tableItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(tableItem);
        } else {
            parent.getChildren().remove(tableItem);
        }
        
        ((DBTable)tableItem.getValue()).getDatabase().getTables().remove((DBTable)tableItem.getValue());
        
        DatabaseCache.getInstance().tableDroped(params.cacheKey(), (DBTable)tableItem.getValue());
    }
    
    public static final void databaseDroped(ConnectionParams params, TreeItem databaseItem) {
        TreeItem parent = databaseItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(databaseItem);
        } else {
            parent.getChildren().remove(databaseItem);
        }
        
        DatabaseCache.getInstance().databaseDroped(params.cacheKey(), (Database)databaseItem.getValue());
    }
    
    public static final void eventDroped(ConnectionParams params, TreeItem eventItem) {
        TreeItem parent = eventItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(eventItem);
        } else {
            parent.getChildren().remove(eventItem);
        }
        
        ((Event)eventItem.getValue()).getDatabase().getEvents().remove((Event)eventItem.getValue());
        
        DatabaseCache.getInstance().eventDroped(params.cacheKey(), (Event)eventItem.getValue());
    }
    
    public static final void functionDroped(ConnectionParams params, TreeItem functionItem) {
        TreeItem parent = functionItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(functionItem);
        } else {
            parent.getChildren().remove(functionItem);
        }
        
        ((Function)functionItem.getValue()).getDatabase().getFunctions().remove((Function)functionItem.getValue());
        
        DatabaseCache.getInstance().functionDroped(params.cacheKey(), (Function)functionItem.getValue());
    }
    
    public static final void storedProcDroped(ConnectionParams params, TreeItem storedProcItem) {
        TreeItem parent = storedProcItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(storedProcItem);
        } else {
            parent.getChildren().remove(storedProcItem);
        }
        
        ((StoredProc)storedProcItem.getValue()).getDatabase().getStoredProc().remove((StoredProc)storedProcItem.getValue());
        
        DatabaseCache.getInstance().storedProcDroped(params.cacheKey(), (StoredProc)storedProcItem.getValue());
    }
    
    public static final void triggerDroped(ConnectionParams params, TreeItem triggerItem) {
        TreeItem parent = triggerItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(triggerItem);
        } else {
            parent.getChildren().remove(triggerItem);
        }
        
        ((Trigger)triggerItem.getValue()).getDatabase().getTriggers().remove((Trigger)triggerItem.getValue());
        
        DatabaseCache.getInstance().triggerDroped(params.cacheKey(), (Trigger)triggerItem.getValue());
    }
    
    public static final void viewDroped(ConnectionParams params, TreeItem viewItem) {
        TreeItem parent = viewItem.getParent();
        if (parent instanceof ConnectionTabContentController.FilterTreeItem) {
            ((ConnectionTabContentController.FilterTreeItem)parent).getSourceList().remove(viewItem);
        } else {
            parent.getChildren().remove(viewItem);
        }
        
        ((View)viewItem.getValue()).getDatabase().getViews().remove((View)viewItem.getValue());
        
        DatabaseCache.getInstance().viewDroped(params.cacheKey(), (View)viewItem.getValue());
    }
}
