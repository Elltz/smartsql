package np.com.ngopal.smart.sql.ui.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.Base64;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.ui.fxgraph.FXGraph;
import np.com.ngopal.smart.sql.ui.fxgraph.FXNode;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DesignerImageController extends BaseController implements Initializable {
    @FXML
    private AnchorPane mainUI;
     
    @FXML
    private TextField fileNameField;
    @FXML
    private ImageView imageView;
    @FXML
    private TextField widthField;
    @FXML
    private TextField heightField;
    @FXML
    private CheckBox aspectRatioCheckBox;
    
    private DesignerImage image;
    
    private SchemaDesignerTabContentController parent;
        
    private DesignerImage sourceImage;
    private Tab tab;
    
    @FXML
    public void selectFileName(ActionEvent event) {
        String fileName = null;
        
        FileChooser fc = new FileChooser();
        File f = fc.showOpenDialog(getStage());
        if (f != null) {
            fileName = f.getName();
        }
        
        if (fileName != null && !fileName.trim().isEmpty()) {
            image.filename().set(fileName);
            
            try {
                Image img = new Image(new FileInputStream(f));
                
                image.imageData().set(Base64.getEncoder().encodeToString(Files.readAllBytes(f.toPath())));
                image.width().set(img.getWidth());
                image.height().set(img.getHeight());

                parent.getDesignerDiagramHistory().putHistoryPoint("Select New Data '" + image.name().get() + "'", parent.getCurrentDiagram());                
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
    
    @FXML
    public void resetSize(ActionEvent event) {
        image.width().set(image.imageDefaultWidth().get());
        image.height().set(image.imageDefaultHeight().get());
        
        parent.getDesignerDiagramHistory().putHistoryPoint("Resize '" + image.name().get() + "'", parent.getCurrentDiagram());
    }
    
    

    public void init(Tab tab, SchemaDesignerTabContentController parent, FXGraph graph, FXNode node, DesignerImage sourceImage) {        
        
        this.parent = parent;
        this.tab = tab;
        this.sourceImage = sourceImage;
        
        this.image = new DesignerImage();
        image.fromMap(sourceImage.toMap());
        
        try {
            Image img = new Image(new ByteArrayInputStream(Base64.getDecoder().decode(image.imageData().get())));
            imageView.setImage(img);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        
        image.imageData().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                Image img = new Image(new ByteArrayInputStream(Base64.getDecoder().decode(image.imageData().get())));
                imageView.setImage(img);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        });
        
        fileNameField.textProperty().bindBidirectional(image.filename());          
        
        widthField.setText(String.valueOf(image.width().get()));
        
        image.width().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            widthField.setText(String.valueOf(image.width().get()));
        });
        
        widthField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                image.width().set(Double.parseDouble(newValue));
            } catch (NumberFormatException ex) {
                log.warn(ex.getMessage(), ex);
            }
        });
        
        heightField.setText(String.valueOf(image.height().get()));
        
        image.height().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            heightField.setText(String.valueOf(image.height().get()));
        });
        
        heightField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                image.height().set(Double.parseDouble(newValue));
            } catch (NumberFormatException ex) {
                log.warn(ex.getMessage(), ex);
            }
        });
                
        
        aspectRatioCheckBox.selectedProperty().bindBidirectional(image.aspectRatio());
        
//        aspectRatioCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
//            graph.selectNode(node);
//            
//            if (aspectRatioCheckBox.isFocused()) {
//                parent.getDesignerDiagramHistory().putHistoryPoint("Toggle '" + image.name().get() + "' Aspect Ratio", parent.getCurrentDiagram());
//            }
//        });
    }
    
    
    @FXML
    public void saveAction(ActionEvent event) {
        
        sourceImage.fromMap(image.toMap());
        parent.getDesignerDiagramHistory().putHistoryPoint("Change '" + image.name().get() + "'", parent.getCurrentDiagram());
        
        closeAction(null);
        
        SchemaDesignerModule module = (SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName());
        module.makeCanSave();
    }
    
    @FXML
    public void closeAction(ActionEvent event) {
        if (tab != null && tab.getTabPane() != null) {
            tab.getTabPane().getTabs().remove(tab);
        }
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
}
