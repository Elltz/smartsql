package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.IndexRange;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.fx.utils.LayoutUtils;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.modules.SSHModule;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.ui.factories.TextFieldTreeCellImpl;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SSHTabContentController extends TabContentController {

    @Inject
    private SQLLogger logger;

    @Getter
    @Inject
    ConnectionParamService db;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private SplitPane mainSplitter;

    @FXML
    private AnchorPane leftPane;

    @FXML
    private AnchorPane centerPane;

    @FXML
    private TextField filterField;
    
    @FXML
    private VBox searchContainer;
    
    @FXML
    private ImageView clearImageView;
    
    
    @FXML
    private TreeView treeView;

    private ObjectProperty<TreeItem> selectedTreeItem;

    private final SimpleBooleanProperty serverItemSelected = new SimpleBooleanProperty(false);

    private final SimpleBooleanProperty tabSelected = new SimpleBooleanProperty(false);

    private final Image folderImage = new Image(getClass().getResource("/np/com/ngopal/smart/sql/ui/images/folder.png").toExternalForm());

    private final Image serverImage = new Image(getClass().getResource("/np/com/ngopal/smart/sql/ui/images/server.png").toExternalForm());

    private TabPane centertabPane;

    private String plinkPath;

    private TreeItem currentRoot;
    private TreeItem firstFilterdItem;

    private boolean needFilter = true;
    private boolean needAutocomlete = true;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    private Font currentFont;
    
    @Override
    public void changeFont(String font) {
        currentFont = FontUtils.parseFXFont(font);
        treeView.refresh();
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void refreshTree(boolean alertDatabaseCount) {
        // TODO
    }

    @Override
    public void addOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }
    
    @Override
    public void removeOnDoubleMouseClickedOnLeftTree(Consumer listener) {
        // implement if need
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        currentFont = FontUtils.parseFXFont(preferenceService.getPreference().getFontsObjectBrowser());
        
        treeView.backgroundProperty().addListener(
                new ChangeListener<Background>() {
                    
            Background back;                    
            @Override
            public void changed(ObservableValue<? extends Background> observable,
                    Background oldValue, Background newValue) { 
                if (newValue != null && getConnectionParams().getBackgroundColor() != null) {
                    if (newValue.getFills().get(0).getFill().equals(Color.web(getConnectionParams().getBackgroundColor()))) {
                        back = newValue;
                    } else {
                        if (oldValue!= null && 
                                oldValue.getFills().get(0).getFill().equals(Color.web(getConnectionParams().getBackgroundColor()))) {
                            back = oldValue;
                        }
                    }
                }
                if (back != null) {
                    searchContainer.setBackground(back);
                }
            }
        });
        
        selectedTreeItem = new SimpleObjectProperty();
        selectedTreeItem.addListener((ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {
            log.debug("Selected: {}", newValue);
        });

        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            try {
                File f = File.createTempFile("plink", ".exe");
                Files.copy(getClass().getResourceAsStream("/plink.exe"), f.toPath(), StandardCopyOption.REPLACE_EXISTING);

                plinkPath = f.getAbsolutePath();
            } catch (IOException ex) {
                log.error("Error while unpack plink to temp dir", ex);

                plinkPath = "plink";
            }
        } else {
            plinkPath = "plink";
        }

        getStage().setOnCloseRequest((WindowEvent event) -> {
            destroyController(DisconnectPermission.NONE);
        });
        
        getStage().addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if (event.isControlDown() && event.isShiftDown() && event.getCode() == KeyCode.B) {
                filterField.requestFocus();
                event.consume();
            }            
        });

        filterField.getStyleClass().add("clearableTextField");
        filterField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {

                    if (needFilter) {
                        // make visible clear view if text non empty
                        clearImageView.setVisible(newValue != null && !newValue.isEmpty());

                        filterNodes(newValue);
                    }
                });

        filterField.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {

            switch (event.getCode()) {
                case ESCAPE:
                    filterField.setText("");
                    event.consume();
                    break;

                case ENTER:
                    filterField.positionCaret(filterField.getLength());
                    filterNodes(filterField.getText());

                    if (firstFilterdItem != null) {
                        treeView.getSelectionModel().select(firstFilterdItem);
                    }
                    event.consume();
                    break;

                case LEFT:
                    checkFilterFieldKey(event, filterField.getCaretPosition() - 1);
                    break;

                case RIGHT:
                    checkFilterFieldKey(event, filterField.getCaretPosition() + 1);
                    break;

                case END:
                    checkFilterFieldKey(event, filterField.getLength());
                    break;

                case HOME:
                    checkFilterFieldKey(event, 0);
                    break;

                case DELETE:
                case BACK_SPACE:
                    IndexRange selectedRange = filterField.getSelection();
                    // if selected text exist and was pressed DELETE or BACKSAPCE
                    // thah remove selected text
                    if (selectedRange != null && selectedRange.getLength() > 0) {
                        needFilter = false;
                        filterField.deleteText(selectedRange);
                        needFilter = true;
                    } else {
                        needAutocomlete = false;
                        if (event.getCode() == KeyCode.DELETE) {
                            filterField.deleteNextChar();
                        } else {
                            filterField.deletePreviousChar();
                        }
                        needAutocomlete = true;
                    }
                    event.consume();
                    break;
            }
        });

        clearImageView.setOnMouseClicked((MouseEvent event) -> {
            filterField.setText("");
        });
    }
    
    
    
    
    
    private void checkFilterFieldKey(KeyEvent event, int currentPosition) {
        IndexRange selectedRange = filterField.getSelection();
        if (selectedRange != null && selectedRange.getLength() > 0) {
            needAutocomlete = false;
            filterField.positionCaret(currentPosition);
            filterNodes(filterField.getText());
            needAutocomlete = true;

            event.consume();
        }
    }

    private void filterNodes(String text) {
        firstFilterdItem = null;

        // filter tree and return first equals text
        filterConnectionTree(text);

        // no need add autocomplete if we remove chars
        if (firstFilterdItem != null && needAutocomlete) {
            needFilter = false;

            String autocompleteText = firstFilterdItem.getValue().toString().toLowerCase().replace(text.toLowerCase(),
                    "");
            filterField.appendText(autocompleteText);

            Platform.runLater(() -> filterField.selectPositionCaret(text.length()));

            needFilter = true;
        }
    }
    
    private void filterConnectionTree(String filterText) {
        // if filtered text is emty - just set current root
        if (filterText.isEmpty()) {
            treeView.setRoot(currentRoot);
        } else {
            // find filtered nodes
            treeView.setRoot(foundFilterItem(currentRoot, filterText));
        }
    }

    /**
     * Recursion function for find all filtered nodes
     * <p>
     * @param parent Parent node
     * @param filterText Text for filtering
     * @return TreeItem filtered TreeItem, null if non found
     */
    private TreeItem foundFilterItem(TreeItem parent, String filterText) {
        TreeItem filteredParent = makeCopyOfTreeItem(parent);
        for (Object n : parent.getChildren()) {
            TreeItem ti = (TreeItem) n;
            if (ti.getValue() != null) {

                TreeItem filtered = null;

                // if not start - try found in children
                if (!ti.getValue().toString().toLowerCase().contains(filterText.toLowerCase())) {
                    filtered = foundFilterItem(ti, filterText);
                } else {
                    filtered = makeCopyOfTreeItem(ti);

                    if (firstFilterdItem == null && checkNeedFilter(ti)) {
                        // save first filtered text for autocomplete
                        firstFilterdItem = filtered;
                    }

                    // find inside item that already start with filtered text
                    TreeItem temp = foundFilterItem(ti, filterText);
                    if (temp != null) {
                        filtered.getChildren().setAll(temp.getChildren());
                        filtered.setExpanded(true);
                    }
                }

                if (filtered != null) {
                    // check node - we filter only: db, tables, columns, functions and etc..
                    if (checkNeedFilter(ti) || !filtered.getChildren().isEmpty()) {
                        filteredParent.getChildren().add(filtered);
                    }
                }
            }
        }

        if (!filteredParent.getChildren().isEmpty()) {
            filteredParent.setExpanded(true);
            return filteredParent;
        } else {
            return null;
        }
    }

    private boolean checkNeedFilter(TreeItem item) {
        return item.getValue() instanceof SshHostItem;
    }

    private TreeItem makeCopyOfTreeItem(TreeItem source) {
        TreeItem ti = new TreeItem(source.getValue(), source.getGraphic());
        ti.setExpanded(source.isExpanded());

        return ti;
    }
    
    @Override
    public DisconnectPermission destroyController(DisconnectPermission permission) {
        for (Tab tab : commands.keySet()) {
            centertabPane.getTabs().remove(tab);

            // fire that tab realy was closed
            final EventHandler handler = tab.getOnClosed();
            handler.handle(null);
        }
        
        return permission;
    }

    public void updateServers() {
        Platform.runLater(() -> {
            // Fill tree with ssh module connections
            final List<ConnectionParams> connections = db.getAll();
            if (connections != null) {
                currentRoot = new TreeItem<>();

                TreeItem productionItem = new TreeItem("Production", new ImageView(folderImage));
                TreeItem testItem = new TreeItem("Test", new ImageView(folderImage));
                TreeItem qaItem = new TreeItem("QA", new ImageView(folderImage));
                TreeItem developmentItem = new TreeItem("Development", new ImageView(folderImage));
                
                currentRoot.getChildren().addAll(productionItem, testItem, qaItem, developmentItem);
                
                connections.stream()
                        .filter(c -> SSHModule.class.getName().equals(c.getSelectedModuleClassName()))
                        .forEach(c -> {
                            TreeItem<?> connectionItem = testItem;
                            if (c.getConnectionType() != null) {
                                switch (c.getConnectionType()) {
                                    case "Production":
                                        connectionItem = productionItem;
                                        break;

                                    case "QA":
                                        connectionItem = qaItem;
                                        break;

                                    case "Development":
                                        connectionItem = developmentItem;
                                        break;
                                }
                            }
                            
                            connectionItem.getChildren().add(new TreeItem(new SshHostItem(c), new ImageView(serverImage)));
                        });

                treeView.setRoot(currentRoot);
                treeView.requestLayout();
            }
        });
    }
    
    public class SshHostItem {

        private final ConnectionParams params;

        public SshHostItem(ConnectionParams params) {
            this.params = params;
        }

        @Override
        public String toString() {
            return params != null ? params.getOsHost() : "";
        }

        public ConnectionParams getParams() {
            return params;
        }

    }

    @Override
    public void openConnection(ConnectionParams params) {
        updateServers();

        SimpleObjectProperty<Object> bindors = new SimpleObjectProperty<>();
        bindors.addListener((ObservableValue<? extends Object> observable, Object oldValue, Object newValue) -> {
            Object[] res = (Object[]) newValue;
            if (res[3] == null) {
                return;
            }
            
            if ((boolean) res[3]) {
                getConnectionParams().setForegroundColor(null);
                getConnectionParams().setBackgroundColor(null);
                return;
            }
            
            getConnectionParams().setForegroundColor(res[0].toString().substring(2));
            getConnectionParams().setBackgroundColor(res[1].toString().substring(2));
        });
        
        String fore = getConnectionParams().getForegroundColor();
        String back = getConnectionParams().getBackgroundColor();
        
        Object[] p;
        if (fore != null && back != null) {
            p = new Object[]{Color.web(fore), Color.web(back), "", false};
        } else {
            //true here means default. since everybody is null,true is set to indicate
            //colors should be default.
            p = new Object[]{null, null, "", true};
        }
        bindors.setValue(p);
        
        initTabPane();
        
        treeView.setUserData(bindors);
        
        Platform.runLater(() -> addTabWithParams(getConnectionParams()));
    }
    
    @Override
    public Object getTreeViewUserData() {
        Object o = treeView.getUserData();
        if (o == null) {
            return new Object();
        }
        return o;
    }

    private Map<Tab, String> commands = new HashMap<>();

    private Map<Tab, Integer> positions = new HashMap<>();

    public void addTabWithParams(final ConnectionParams params) {
        if (params.getOsPassword() == null || params.getOsPassword().isEmpty()) {

            final String pswd = DialogHelper.showPassword(getController(), "Password input", "Enter password:", "", "");
            if (pswd != null && !pswd.isEmpty()) {
                params.setOsPassword(pswd);
            } else {
                return;
            }
        }

        final Tab tab = new Tab(params.getOsHost());
        tab.setUserData(params);

        commands.put(tab, "");

        final SSHConnection sshConnection = new SSHConnection(plinkPath);
        tab.setOnClosed((Event event) -> {
            sshConnection.disconnect();
        });

        tab.setGraphic(new ImageView(serverImage));

        final TextArea area = new TextArea("");
        area.getStyleClass().setAll("sshArea");
        tab.setContent(area);

        tab.setOnSelectionChanged((Event event) -> {
            if (event.getSource() == tab) {
                Platform.runLater(() -> {
                    area.end();
                    area.requestFocus();
                });
            }
        });

        area.setOnKeyReleased((KeyEvent event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                log.debug("send command: " + commands.get(tab));

                sshConnection.sendCommand(commands.get(tab));
                commands.put(tab, "");
            } else {
                commands.put(tab, commands.get(tab) + event.getText());
            }
        });

        final EventDispatcher initial = area.getEventDispatcher();
        area.setEventDispatcher((Event event, EventDispatchChain tail) -> {
            final Integer currentPosition = positions.get(tab);
            if (event.getEventType() == KeyEvent.KEY_PRESSED
                    || event.getEventType() == KeyEvent.KEY_RELEASED
                    || event.getEventType() == KeyEvent.KEY_TYPED) {
                if (((KeyEvent) event).getCode() == KeyCode.BACK_SPACE
                        || ((KeyEvent) event).getCode() == KeyCode.LEFT) {
                    if (area.getCaretPosition() <= currentPosition) {
                        area.positionCaret(currentPosition);
                        event.consume();
                        return null;
                    }
                } else if (((KeyEvent) event).getCode() == KeyCode.DELETE
                        || (((KeyEvent) event).getCode() == KeyCode.V && ((KeyEvent) event).isControlDown())
                        || (((KeyEvent) event).getCode() == KeyCode.INSERT && ((KeyEvent) event).isShiftDown())) {
                    if (area.getCaretPosition() < currentPosition) {
                        area.positionCaret(currentPosition);
                        event.consume();
                        return null;
                    }
                } else if (((KeyEvent) event).getCode() == KeyCode.HOME) {
                    area.positionCaret(currentPosition);
                    event.consume();
                    return null;
                }
            }

            return initial.dispatchEvent(event, tail);
        });

        area.addEventHandler(EventType.ROOT, (Event event) -> {
            final Integer currentPosition = positions.get(tab);
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED
                    || event.getEventType() == MouseEvent.MOUSE_PRESSED
                    || event.getEventType() == MouseEvent.MOUSE_RELEASED) {
                if (area.getCaretPosition() < currentPosition) {
                    area.positionCaret(currentPosition);
                }
            } else if (event.getEventType() == KeyEvent.KEY_PRESSED
                    || event.getEventType() == KeyEvent.KEY_RELEASED
                    || event.getEventType() == KeyEvent.KEY_TYPED) {
                if (((KeyEvent) event).getCode() == KeyCode.PAGE_UP
                        || ((KeyEvent) event).getCode() == KeyCode.PAGE_DOWN
                        || ((KeyEvent) event).getCode() == KeyCode.UP) {
                    if (area.getCaretPosition() < currentPosition) {
                        area.positionCaret(currentPosition);
                    }
                }
            }
        });

        addTab(tab);
        positions.put(tab, -1);

        sshConnection.resultProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (t1 != null) {
                // TODO Change this
                if (t1.endsWith("password: ")) {
                    Platform.runLater(() -> {

                        final String pswd = DialogHelper.showPassword(getController(), "Password input", "Enter password:", "", "");
                        if (pswd != null && !pswd.isEmpty()) {
                            params.setSshPassword(pswd);
                            sshConnection.connect(params);
                        } else {
                            return;
                        }
                    });
                } else {
                    area.appendText(t1);
                    positions.put(tab, area.getText().length());
                }
            }
        });

        sshConnection.connect(params);

        Platform.runLater(area::requestFocus);
    }

    @Override
    public List<Tab> getBottamTabs() {
        return null;
    }
    
    @Override
    public void focusLeftTree() {
        Platform.runLater(() -> treeView.requestFocus());
    }

    @Override
    public void addTab(Tab t) {
        Platform.runLater(() -> {
            centertabPane.getTabs().add(t);
            centertabPane.getSelectionModel().select(t);
        });
    }

    @Override
    public ReadOnlyObjectProperty<Tab> getSelectedTab() {
        return centertabPane.getSelectionModel().selectedItemProperty();
    }

    @Override
    public void addTabToBottomPane(Tab... tab) {
        // TODO Not used yet
    }
    
    @Override
    public void removeTabFromBottomPane(Tab... tab) {
        // Do nothing
    }

    public void removeSelectedServer() {
        Platform.runLater(() -> {
            final TreeItem item = (TreeItem) getSelectedTreeItem().get();
            if (item != null && item.getValue() instanceof SshHostItem) {
                
                DialogResponce responce = DialogHelper.showConfirm(getController(), "Remove selected server", "Are you sure you want to delete server?");
                if (responce == DialogResponce.OK_YES) {
                    
                    ConnectionParams params = ((SshHostItem) item.getValue()).getParams();
                    params.setOsHost("");
                    params.setOsName("");
                    params.setOsPassword("");
                    params.setOsPort(22);
                    params.setOsUser("");
                    
                    db.save(params);                    
                    updateServers();
                }
            }
        });
    }

    public void nextTab() {
        int currentTabIndex = centertabPane.getSelectionModel().getSelectedIndex();
        if (currentTabIndex == centertabPane.getTabs().size() - 1) {
            currentTabIndex = 0;
        } else {
            currentTabIndex++;
        }
        
        centertabPane.getSelectionModel().select(currentTabIndex);
    }
    
    public void prevTab() {
        int currentTabIndex = centertabPane.getSelectionModel().getSelectedIndex();
        if (currentTabIndex == 0) {
            currentTabIndex = centertabPane.getTabs().size() - 1;
        } else {
            currentTabIndex--;
        }
        
        centertabPane.getSelectionModel().select(currentTabIndex);
    }
    
    
    public void duplicateCurrentTab() {
        final Tab currentTab = centertabPane.getSelectionModel().getSelectedItem();
        if (currentTab != null) {
            addTabWithParams((ConnectionParams) currentTab.getUserData());
        }
    }

    public void openSelectedServer() {
        final TreeItem item = (TreeItem) treeView.getSelectionModel().getSelectedItem();
        if (item != null && item.getValue() instanceof SshHostItem) {
            System.out.println("Selected connection: " + item.getValue());

            addTabWithParams(((SshHostItem) item.getValue()).getParams());
        }
    }

    public ConnectionParams getSelectedServerParams() {
        final TreeItem item = (TreeItem) treeView.getSelectionModel().getSelectedItem();
        if (item != null && item.getValue() instanceof SshHostItem) {
            return ((SshHostItem) item.getValue()).getParams();
        }

        return null;
    }

    private void initTabPane() {
        if (centertabPane == null) {
            centertabPane = new TabPane();
            centertabPane.prefWidth(centerPane.getWidth());
            centerPane.getChildren().add(centertabPane);

            treeView.setOnMouseClicked((MouseEvent mouseEvent) -> {
                if (mouseEvent.getClickCount() == 2) {
                    openSelectedServer();
                }
            });
            
            treeView.setCellFactory(new Callback<TreeView, TreeCell>() {
                @Override
                public TreeCell call(TreeView p) {
                    TextFieldTreeCellImpl impl = 
                            new TextFieldTreeCellImpl(null, getHooks(), new ContextMenu()) {
                        @Override
                        public void updateItem(Object item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            if (currentFont != null) {
                                setFont(currentFont);
                            }
                        }                                
                    };
                    
                    return impl;
                }
            }); 
            
            selectedTreeItem.bind(treeView.getSelectionModel().selectedItemProperty());

            treeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
                serverItemSelected.set(newValue instanceof TreeItem && ((TreeItem) newValue).getValue() instanceof SshHostItem);
            });

            centertabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
                tabSelected.set(newValue != null);
            });

            LayoutUtils.layoutAnchorPane(centertabPane, 0, 0, 0, 0);
        }
    }

    public ReadOnlyBooleanProperty serverSelectedProperty() {
        return serverItemSelected;
    }

    public ReadOnlyBooleanProperty tabSelectedProperty() {
        return tabSelected;
    }

    @Override
    public ObjectProperty<TreeItem> getSelectedTreeItem() {
        return selectedTreeItem;
    }

    @Override
    public boolean isNeedOpenInNewTab() {
        return false;
    }

    @Override
    public String getTabName() {
        return "SSH";
    }
    
    @Override
    public Tab getTab(String id, String text) {
        return null;
    }

    @Override
    public void showTab(Tab t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void isFocusedNow() {}

    @Override
    public void showLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hideLeftPane() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public ReadOnlyBooleanProperty getLeftPaneVisibiltyProperty(){
        return null;
    }
}
