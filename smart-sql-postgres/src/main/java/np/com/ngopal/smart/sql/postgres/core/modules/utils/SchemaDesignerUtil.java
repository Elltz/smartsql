package np.com.ngopal.smart.sql.core.modules.utils;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import javafx.application.Platform;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public final class SchemaDesignerUtil {

    public static void forwardEngeen(DBService service, UIController controller, String database, DesignerErrDiagram diagram, BiConsumer<String, Double> statusCallback, Consumer<Void> finishedCallback, Function<Void, Double> currentProgressCallback) throws SQLException {        
        if (diagram != null) {

            // all alter need make in transaction

            String footer = 
                "/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;\n" +
                "/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n" +
                "/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;\n" +
                "/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;\n" +
                "USE `" + database + "`;\n";

            String query = "";
            for (DesignerTable dt: diagram.tables()) {

                dt.schema().set(database);

                // find database in DB
                DBTable dbTable = service.getTableSchemaInformation(database, dt.name().get());
                DBTable designerTable = SchemaDesignerModule.convertDesignerTableToTable(service, dt);

                String script = "";
                // if table NULL - just create new table
                if (dbTable == null) {
                    script = MysqlDBService.getNewSQL(designerTable);
                } else {
                    script = MysqlDBService.getChangesSQL(dbTable, designerTable);
                }

                if (!script.trim().isEmpty()) {
                    query += script.trim() + ";\n";
                }
            } 

            if (!query.isEmpty()) {
                query = footer + query;
                query +=
                    "/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;\n" +
                    "/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n" +
                    "/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;\n" +
                    "/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;";
            }

            if (statusCallback != null) {
                statusCallback.accept("Execute alters", 0.3);
            }

            if (!query.isEmpty()) {
                try {
                    ScriptRunner scriptRunner = new ScriptRunner(service.getDB(), true);
                    scriptRunner.runScript(new StringReader(query), new ScriptRunner.ScriptRunnerCallback() {

                        double step = 0;
                        boolean error = false;

                        @Override
                        public void started(Connection con, int queriesSize) throws SQLException {
                            
                            con.setAutoCommit(false);
                            
                            step = 0.7 / queriesSize;
                        }

                        @Override
                        public void queryExecuting(Connection con, QueryResult queryResult) {
                        }

                        @Override
                        public void queryExecuted(Connection con, QueryResult queryResult) {
                            if (statusCallback != null) {
                                statusCallback.accept("Executing queries", (currentProgressCallback != null ? currentProgressCallback.apply(null) : 0.0) + step);
                            }
                        }

                        @Override
                        public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {
                            
                            con.rollback();
                            
                            error = true;
                            if (statusCallback != null) {
                                statusCallback.accept("Error executing query", 0.0);
                            }

                            Platform.runLater(() -> {
                                DialogHelper.showError(controller, "Error forward engineer", th.getMessage(), null, controller.getStage());
                            });
                        }

                        @Override
                        public boolean isWithLimit(QueryResult queryResult) {
                            return false;
                        }

                        @Override
                        public void finished(Connection con) {
                            if (!error) {
                                if (statusCallback != null) {
                                    statusCallback.accept("Done", 1.0);
                                }
                                
                                if (finishedCallback != null) {
                                    finishedCallback.accept(null);
                                }
                            }
                        }

                        @Override
                        public boolean isNeedStop() {
                            return false;
                        }
                    }, false);
                } catch (Throwable th) {
                    log.error("Error", th);
                }
            } else {
                if (statusCallback != null) {
                    statusCallback.accept("No changes", 1.0);
                }
            }
        }
    }
}
