package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import np.com.ngopal.smart.sql.ui.controller.BackupDatabaseAsSQLDumpController;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.DatabaseCopierController;
import np.com.ngopal.smart.sql.ui.controller.ScheduledBackupWizardController;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 */
public class MenuInTablesModule extends AbstractStandardModule {

    private Image tableImage = null;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public boolean install() {
        return true;
    }

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {return true; }
    
    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void createTable() {
        if (tableImage == null) {
            tableImage = SmartImageProvider.getInstance().getTableTab_image();
        }

        ConnectionSession sessionTemp = getController().getSelectedConnectionSession().get();

        Database db = sessionTemp.getConnectionParam().getSelectedDatabase();

        AlterTableController altertableController = getTypedBaseController("AlterTable");
        altertableController.setService(sessionTemp.getService());

        Tab alterTab = new Tab("New table");
        alterTab.setGraphic(new ImageView(tableImage));
        alterTab.setUserData(altertableController);

        Parent p = altertableController.getUI();
        alterTab.setContent(p);

        DBTable table = new DBTable("", null, null, null, new ArrayList<>(), db, null, null, new ArrayList<>(), new ArrayList<>());

        altertableController.init(alterTab, table, sessionTemp, false);

        getController().addTab(alterTab, sessionTemp);
        getController().showTab(alterTab, sessionTemp);
    }
    
    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {

        MenuItem createTableItem = new MenuItem("Create Table", 
                new ImageView(SmartImageProvider.getInstance().getCreateTable_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createTableItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createTableItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createTable();
            }
        });
        
        MenuItem copyTableToDifferentItem = new MenuItem("Copy Table(s)To Different Host/Database...", 
                new ImageView(SmartImageProvider.getInstance().getDatabaseCopy_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        copyTableToDifferentItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                
                final ConnectionSession currentSession = sessionTemp;
                DatabaseCopierController copierCon = getTypedBaseController("DatabaseCopier");
                
                Database db = currentSession.getConnectionParam().getSelectedDatabase();
                        
                List<DBTable> tablesList = DatabaseCache.getInstance().getTables(currentSession.getConnectionParam().cacheKey(), db.getName());

                Map<String, String> selectedData = new HashMap<>();
                String tables = "";
                for (DBTable t : tablesList) {
                    tables += t.getName() + ":";
                }

                if (!tables.isEmpty()) {
                    selectedData.put(ConnectionTabContentController.DatabaseChildrenType.TABLES.name(),
                            tables.substring(0, tables.length() - 1));
                }
                copierCon.insertSession(currentSession, db.getName(), selectedData);

                if (copierCon.showNow(getController().getStage())) {
                    getController().refreshTree(currentSession, false);
                }       
            }
        });

        MenuItem scheduledBackupsItem = new MenuItem("Scheduled Backups...",
                new ImageView(SmartImageProvider.getInstance().getScheduleBackUp_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        scheduledBackupsItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        scheduledBackupsItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showScheduledBackupsDialog(session);
            }
        });
        MenuItem backupTableAsSQLDumpItem = new MenuItem("Backup Table(s) As SQL Dump",
                new ImageView(SmartImageProvider.getInstance().getDatabaseDump_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        backupTableAsSQLDumpItem.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN,KeyCombination.ALT_DOWN));
        backupTableAsSQLDumpItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showBackupDatabaseAsSQLDumpDialog(session);
            }
        });
        
        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("tables0", new ArrayList<>(Arrays.asList(new MenuItem[]{createTableItem, copyTableToDifferentItem}))), Hooks.QueryBrowser.TABLES);
        map.put(new HookKey("tables1", new ArrayList<>(Arrays.asList(new MenuItem[]{scheduledBackupsItem, backupTableAsSQLDumpItem}))), Hooks.QueryBrowser.TABLES);

        return map;
    }
    
    private void showBackupDatabaseAsSQLDumpDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        BackupDatabaseAsSQLDumpController controller = getTypedBaseController("BackupDatabaseAsSQLDump");        
        Database database = null;
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session);
        Map<String, String> selectedData = new HashMap<>();
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getParent().getValue();            
            if (itemValue instanceof Database) {
                database = (Database)itemValue;
            }            
            
            selectedData.put(ConnectionTabContentController.DatabaseChildrenType.TABLES.name(), "*");
        }
        
        
        
        controller.init(session.getService(), session.getConnectionParam(), database, selectedData);
        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("SQL Dump");

        dialog.showAndWait();
    }
    
    public void showScheduledBackupsDialog(ConnectionSession session) {
        final Stage dialog = new Stage();
        ScheduledBackupWizardController controller = getTypedBaseController("ScheduledBackupWizard");        
        Database database = null;
        
        ReadOnlyObjectProperty<TreeItem> selectedTreeItem = getController().getSelectedTreeItem(session);
        if (selectedTreeItem != null && selectedTreeItem.getValue() != null) {
            
            Object itemValue = selectedTreeItem.getValue().getParent().getValue();            
            if (itemValue instanceof Database) {
                database = (Database)itemValue;
            }
        }
        
        try {
            controller.init(session.getService(), session.getConnectionParam(), database);
            final Parent p = controller.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getController().getStage());
            dialog.setTitle("Export Data As Batch Script Wizard");

            dialog.showAndWait();
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }
        
    @Override
    public String getInfo() { 
        return "";
    }
}
