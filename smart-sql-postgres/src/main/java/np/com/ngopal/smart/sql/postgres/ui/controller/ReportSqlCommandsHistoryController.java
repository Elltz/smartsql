package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportSqlCommandsHistoryController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
        
    @FXML
    private Button refreshButton;
    
    @FXML
    private Button exportButton;
    
    private volatile boolean needStop = false;
    
    private ConnectionSession session;
    
    private boolean checkVersion(String version) {
        String[] versions = version.split("\\.");
        if (versions.length > 0) {
            try {
                int v0 = Integer.valueOf(versions[0]);
                if (v0 > 5) {
                    return true;
                    
                } else if (v0 == 5 && versions.length > 1) {
                    int v1 = Integer.valueOf(versions[1]);
                    if (v1 >= 6) {
                        return true;
                    }
                }
            } catch (Throwable th) {
            }
        }
        
        return false;
    }
    
    @FXML
    public void refreshAction(ActionEvent event) {
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            if (session != null) {
                
                if (checkVersion(session.getService().getDbSQLVersion())) {
                
                    try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_SQL_COMMANDS_HISTORY_0))) {
                        if (rs != null && rs.next()) {
                            int value = rs.getInt(1);
                            if (value == 0) {
                                CountDownLatch cdl = new CountDownLatch(1);
                                Platform.runLater(() -> {
                                    
                                    EnablingPerformanceSchemaController c = ((MainController)getController()).getTypedBaseController("EnablingPerformanceSchema");
                                    
                                    Stage dialog = new Stage(StageStyle.UTILITY);
                                    dialog.initModality(Modality.WINDOW_MODAL);
                                    dialog.initOwner(getController().getStage());
                                    dialog.setTitle("Message");        
                                    dialog.setScene(new Scene(c.getUI()));
                                    dialog.showAndWait();
                                    
                                    if (c.getResponce() == DialogResponce.CANCEL) {
                                        needStop = true;
                                        
                                    } else {
                                        String[] queries = QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_SQL_COMMANDS_HISTORY_1).split(";");
                                        try  {
                                            for (String query: queries) {
                                                session.getService().execute(query);                                                
                                            }
                                        } catch (SQLException ex) {
                                            Platform.runLater(() -> ((MainController)getController()).showError("Error", ex.getMessage(), null));
                                            needStop = true;
                                        }
                                    }
                                    cdl.countDown();
                                });
                                try {
                                    cdl.await();
                                } catch (Throwable ex) {
                                }
                            }
                        }
                    } catch (SQLException ex) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", ex.getMessage(), null));
                        needStop = true;
                    }
                            
                    if (!needStop) {
                        try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_SQL_COMMANDS_HISTORY_2))) {
                            if (rs != null) {
                                List<ReportSqlCommandsHistory> list = new ArrayList<>();
                                while (rs.next()) {

                                    ReportSqlCommandsHistory rid = new ReportSqlCommandsHistory(
                                        rs.getString(1), 
                                        rs.getString(2),
                                        rs.getString(3), 
                                        rs.getLong(4),
                                        rs.getLong(5),
                                        rs.getLong(6),
                                        rs.getLong(7),
                                        rs.getLong(8),
                                        rs.getLong(9),
                                        rs.getLong(10),
                                        rs.getLong(11)
                                    );  

                                    list.add(rid);
                                }

                                tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                            }
                        } catch (SQLException ex) {
                            ((MainController)getController()).showError("Error", ex.getMessage(), null);
                        }
                    }
                    
                } else {
                    Platform.runLater(() -> ((MainController)getController()).showError("Not working", "This report works only in 5.6 or greater MySQL version", null));
                }
            }
            
            Platform.runLater(() -> makeBusy(false));
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportSqlCommandsHistory rsch: (List<ReportSqlCommandsHistory>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rsch.getSchemaName());
                        row.add(rsch.getStatement());
                        row.add(rsch.getSqlText());
                        row.add(rsch.getSumTimerWait());
                        row.add(rsch.getMinTimerWait());
                        row.add(rsch.getAvgTimerWait());
                        row.add(rsch.getCountStar());
                        row.add(rsch.getSumRowsSent());
                        row.add(rsch.getSumRowsExamined());
                        row.add(rsch.getSumCreatedTmpDiskTables());
                        row.add(rsch.getSumCreatedTmpTables());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    columns.put("Schema name", true);
                    columnWidths.add(100);

                    columns.put("Statement", true);
                    columnWidths.add(100);

                    columns.put("SQL Ttext", true);
                    columnWidths.add(150);

                    columns.put("Sum Timer Wait", true);
                    columnWidths.add(150);

                    columns.put("Min Timer Wait", true);
                    columnWidths.add(150);
                    
                    columns.put("Avg Timer Wait", true);
                    columnWidths.add(150);
                    
                    columns.put("Count Star", true);
                    columnWidths.add(150);
                    
                    columns.put("Sum Rows Sent", true);
                    columnWidths.add(150);
                    
                    columns.put("Sum Rows Examined", true);
                    columnWidths.add(150);
                    
                    columns.put("Sum Created Tmp Disk Tables", true);
                    columnWidths.add(150);
                    
                    columns.put("Sum Created Tmp Tables", true);
                    columnWidths.add(150);
                                        
                    sheets.add("SQL Commands history report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        
        TableColumn databaseColumn = createColumn("Schema name", "schemaName");        
        TableColumn statementColumn = createColumn("Statement", "statement");
        statementColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "Statement"));
            }
        });
        
        TableColumn sqlTextColumn = createColumn("SQL Ttext", "sqlText");
        sqlTextColumn.setCellFactory(new Callback<TableColumn<DebugRunningQueryData, String>, TableCell<DebugRunningQueryData, String>>() {
            @Override
            public TableCell<DebugRunningQueryData, String> call(TableColumn<DebugRunningQueryData, String> param) {
                return new QueryTableCell<>(0, (item) -> calcBlobLength(item), (QueryTableCell.QueryTableCellData data, String text) -> showSlowQueryData(data, text, "SQL Ttext"));
            }
        });
        
        TableColumn sumTimerWaitColumn = createColumn("Sum Timer Wait", "sumTimerWait");
        TableColumn minTimerWaitColumn = createColumn("Min Timer Wait", "minTimerWait");
        TableColumn avgTimerWaitColumn = createColumn("Avg Timer Wait", "avgTimerWait");
        TableColumn countStarColumn = createColumn("Count Star", "countStar");
        TableColumn sumRowsSentColumn = createColumn("Sum Rows Sent", "sumRowsSent");
        TableColumn sumRowsExaminedColumn = createColumn("Sum Rows Examined", "sumRowsExamined");
        TableColumn sumCreatedTmpDiskTablesColumn = createColumn("Sum Created Tmp Disk Tables", "sumCreatedTmpDiskTables");
        TableColumn sumCreatedTmpTablesColumn = createColumn("Sum Created Tmp Tables", "sumCreatedTmpTables");
        
        tableView.setSearchEnabled(true);
        
        tableView.addTableColumn(databaseColumn, 100, 100, false);
        tableView.addTableColumn(statementColumn, 170, 170, false);
        tableView.addTableColumn(sqlTextColumn, 170, 170, false);
        tableView.addTableColumn(sumTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(minTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(avgTimerWaitColumn, 100, 100, false);
        tableView.addTableColumn(countStarColumn, 100, 100, false);
        tableView.addTableColumn(sumRowsSentColumn, 100, 100, false);
        tableView.addTableColumn(sumRowsExaminedColumn, 100, 100, false);
        tableView.addTableColumn(sumCreatedTmpDiskTablesColumn, 100, 100, false);
        tableView.addTableColumn(sumCreatedTmpTablesColumn, 100, 100, false);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }
    
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }    
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {

        final Stage dialog = new Stage();
        SlowQueryTemplateCellController cellController = ((MainController)getController()).getTypedBaseController("SlowQueryTemplateCell");
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    private String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    log.error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }
}
