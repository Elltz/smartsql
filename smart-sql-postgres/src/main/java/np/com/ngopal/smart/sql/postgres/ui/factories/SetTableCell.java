package np.com.ngopal.smart.sql.ui.factories;

import java.util.function.BiConsumer;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import lombok.Getter;
import org.controlsfx.control.CheckComboBox;


public class SetTableCell<S> extends TableCell<S, String> {

    private final CheckComboBox<String> comboBox;

    private ObservableValue<String> ov;

    private String item;
    private volatile boolean canCheck = true;
    
    public CheckComboBox getComboBox() {
        return comboBox;
    }

    public SetTableCell(ObservableList lists, BiConsumer<SetTableCell.TableCellData, String> acceptFunction, BooleanBinding editable) {
        
        setStyle("-fx-padding: 0");
                
        this.comboBox = new CheckComboBox<>();
        comboBox.setId("setTableCombobox");
//        comboBox.disableProperty().bind(editable.not());
        
        comboBox.getItems().addAll(lists);
        comboBox.setMaxWidth(Double.MAX_VALUE);
        comboBox.getCheckModel().getCheckedItems().addListener((ListChangeListener.Change<? extends String> c) -> {
            
            if (canCheck) { 
                
                
                String result = "";
                for (int i = 0; i < comboBox.getItems().size(); i++) {
                    if (comboBox.getCheckModel().isChecked(i)) {
                        if (!result.isEmpty()) {
                            result += ",";
                        }

                        result += comboBox.getCheckModel().getItem(i);
                    }
                }

                TableCellData data = new TableCellData();
                data.row = getIndex();
                data.column = getTableView().getColumns().indexOf(getTableColumn());

                this.item = result.isEmpty() ? "(NULL)" : result;
                
                acceptFunction.accept(data, item);  
            }
        });
        
//        setAlignment(Pos.CENTER);
//        setGraphic(comboBox);
    }

    public String item() {
        return item;
    }
    
    @Override
    public void startEdit() {
        if (! isEditable() || ! getTableView().isEditable() || ! getTableColumn().isEditable()) {
            return;
        }
        
        initCheckValue();

        super.startEdit();
        setText(null);
        setGraphic(comboBox);
    }
    
    private void initCheckValue() {
        String[] sets = item != null ? item.split(",") : null;
        if (sets != null) {

            canCheck = false;

            for (String s: sets) {
                comboBox.getCheckModel().check(s.trim());
            }

            canCheck = true;
        }
    }
    
    
    @Override 
    public void cancelEdit() {
        super.cancelEdit();

        setText(item);
        setGraphic(null);
    }

    @Override 
    public void updateItem(String item, boolean empty) {

        super.updateItem(item, empty);
        if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
            this.item = item;
            
            if (isEditing()) {                
                initCheckValue();
                setGraphic(comboBox);
                
            } else {
                setText(item);
            }

        } else {
            setGraphic(null);
        }
    }
    
    @Getter
    public class TableCellData {
        int row;
        int column;
    }
}
