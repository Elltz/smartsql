/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules;

import java.util.HashMap;
import java.util.Map;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.controller.UserManagerController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class UserManagerModule extends AbstractStandardModule {
    
    public static final Image userDefaultImage = SmartImageProvider.getInstance().getUser_image();
    public static final Image userDefaultEditted = SmartImageProvider.getInstance().getEditUser_image();
    public static final Image userDefaultSaved = SmartImageProvider.getInstance().getGlobalUser_image();
    
    /**
     * userManagers is a collection of all opened UserManager Functionality in
     * different connections in application. userManagers allows only one
     * UserManager window opened for one connection or different connection with
     * same mysql server. That is one userManager function will be opened for a
     * connection to localhost mysql server.
     */
    private Map<ConnectionSession, UserManagerController> userManagers = new HashMap<ConnectionSession, UserManagerController>()/* {
        @Override
        public UserManagerController putIfAbsent(ConnectionSession key, UserManagerController value) {
            UserManagerController umc = null;
            for (ConnectionSession cs : keySet()) {
                if (cs.getConnectionParam().equals(key.getConnectionParam())
                        || (cs.getConnectionParam().getAddress().equalsIgnoreCase(key.getConnectionParam().getAddress()))
                        && cs.getConnectionParam().getPort() == key.getConnectionParam().getPort()) {
                    umc = get(cs);
                    break;
                }
            }

            if (umc == null) {
                umc = super.putIfAbsent(key, value);//umc will still be null here
            }

            return umc;
        }

        @Override
        public UserManagerController remove(Object key) {
            UserManagerController umc = super.remove(key);
            if (umc != null) {
                umc.destroyController();
            }
            return umc;
        }

        @Override
        public UserManagerController put(ConnectionSession key, UserManagerController value) {
            return putIfAbsent(key, value);
        }

    }*/;

    /**
     * here is where we get notify that the left tree has been clicked hence,
     * object level privileges needs to check and play along. It runs in the
     * background.
     *
     * @param item The selected object { can be database, table ,stored procs }
     * etc
     * @return 
     */
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        return true;
    }
    
    public static String getStringVersionOfSelectedTreeItemForUserMananger(TreeItem item) {
        String val = null;
        if (item.getValue() instanceof DBTable) {
            DBTable dbt = ((DBTable) item.getValue());
            val = "TABLE=" + dbt.getDatabase().getName() + "." + dbt.getName() + ".*";
        } else if (item.getValue() instanceof StoredProc) {
            StoredProc sp = ((StoredProc) item.getValue());
            val = "STOREDPROC=" + sp.getDatabase() + "." + sp.getName() + ".*";
        } else if (item.getValue() instanceof Trigger) {
            Trigger t = ((Trigger) item.getValue());
            val = "TRIGGER=" + t.getDatabase().getName() + t.getName() + ".*";
        } else if (item.getValue() instanceof np.com.ngopal.smart.sql.model.Function) {
            Function f = ((Function) item.getValue());
            val = "FUNCTION=" + f.getDatabase().getName() + f.getName() + ".*";
        } else if (item.getValue() instanceof Column) {
            Column c = ((Column) item.getValue());
            val = "COLUMN=" + ((c.getTable() == null) ? c.getView() : c.getTable()).getDatabase().getName() + "."
                    + ((c.getTable() == null) ? c.getView() : c.getTable()).getName() + "." + c.getName();
        } else if (item.getValue() instanceof View) {
            View v = ((View) item.getValue());
            val = "VIEW=" + v.getDatabase().getName() + "." + v.getName() + ".*";
        } else if (item.getValue() instanceof Index) {
            val = null;//((Index) item.getValue()).getName();
        } else if (item.getValue() instanceof ConnectionTabContentController.DatabaseChildrenType) {
            val = null;//"^=" +item.getValue().toString();
        } else if (item.getValue() instanceof String) {
            
        } else if (item.getValue() instanceof Database) {
            val = "DATABASE=" + ((Database) item.getValue()).getName() + ".*.*";
        } else if (item.getValue() instanceof ConnectionParams) {
            val = "*=*.*.*";
        }
        
        return val;
    }

    @Override
    public boolean uninstall() {
        return false;
    }

    @Override
    public String getInfo() {
        return "Functionality to create/modify MYSQL SERVER user privileges";
    }

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {
        super.postConnection(service, tab);
    }

    @Override
    public String getName() {
        return "User Manager";
    }

    @Override
    public boolean isConnection() {
        return false;
    }
    
    public void openUserManager(ConnectionSession service, Tab tab) {

        UserManagerController returnedUMC = userManagers.get(service);
        if (returnedUMC != null) { //red alert
            if (DialogHelper.showConfirm(getController(), "DUPLICATE USER MANAGERS MYSQL SERVER",
                    "You are tring to open two USER MANAGERS on one MYSQL SERVER, this might "
                    + "cause privileges being overriden simultaneously. \n "
                    + "DO YOU WANT TO GO BACK TO EXISTING MANAGER ?") == DialogResponce.OK_YES) {
                //here we relocate
                getController().showTab(returnedUMC.getTab(), service);
            }
        } else {
            tab = new Tab(getName());
            tab.setGraphic(new Label());
            tab.setOnCloseRequest(new EventHandler<Event>() {
                @Override
                public void handle(Event t) {
                    UserManagerController returnedUMC = userManagers.get(service);
                    if (returnedUMC != null) {
                        returnedUMC.closeController();
                    }
                    userManagers.remove(service);
                    getMainController().performShowLeftPane();
                }
            });
            UserManagerController umc = ((MainController)getController()).getTypedBaseController("UserManagerInteface");
            userManagers.put(service, umc);
            getController().addTab(tab, service);
            umc.init(tab);
            getController().showTab(tab, service);
        }

    }

}
