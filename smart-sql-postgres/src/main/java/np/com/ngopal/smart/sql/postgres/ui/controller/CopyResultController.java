package np.com.ngopal.smart.sql.ui.controller;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.utils.ExportUtils;

/**
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class CopyResultController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
        
    @FXML
    private TextField fieldsTerminated;
    @FXML
    private TextField fieldsEnclosed;
    @FXML
    private TextField fieldsEscaped;
    @FXML
    private TextField lineTerminated;
    
    private ObservableList<ObservableList> rowsData;
    private ObservableList<String> columnsData;

   

    public void init(ObservableList<ObservableList> rowsData, ObservableList<String> columnsData) {
        this.rowsData = rowsData;
        this.columnsData = columnsData;
    }

    @FXML
    void export(ActionEvent event) {
        
        try {
            LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();
            for (String c: columnsData) {
                columns.put(c, true);
            }

            StringWriter sw = new StringWriter();
            // convert to delimiter rows
            ExportUtils.convertDataForDelimiterWriter(sw, rowsData, columns,
                    "UTF-8", fieldsTerminated.getText(), fieldsEnclosed.getText(), lineTerminated.getText(), "NULL", false);
            
            Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
            Clipboard systemClipboard = defaultToolkit.getSystemClipboard();
            
            // add to clipboard
            systemClipboard.setContents(new StringSelection(sw.toString()), null);
            
            close();
            
        } catch (IOException ex) {
            DialogHelper.showError(getController(), "Error", "Error exporting", ex);
        }       
    }

    @FXML
    void cancel(ActionEvent event) {
        close();
    }
    
    private void close() {
        Stage stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

}
