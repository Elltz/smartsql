/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.control.TableColumn;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.models.FileSortingSQLQueryData;
import np.com.ngopal.smart.sql.structure.models.FilterableData;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
public class FileSortingSQLQueryController extends SQLStatementsController {

    @Override
    public String getName() {
        return "File Sorting SQL Queries";
    }

    @Override
    public List<? extends FilterableData> getData() {
        String query = "SELECT\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`DIGEST_TEXT`                                                                                                                       AS `query`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SCHEMA_NAME`                                                                                                                       AS `db`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`                                                                                                                        AS `exec_count`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_TIMER_WAIT`                                                                                                                    AS `total_latency`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_MERGE_PASSES`                                                                                                             AS `sort_merge_passes`,\n"
                + "  round(ifnull((`performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_MERGE_PASSES` / nullif(`performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`, 0)), 0), 0) AS `avg_sort_merges`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_SCAN`                                                                                                                     AS `sorts_using_scans`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_RANGE`                                                                                                                    AS `sort_using_range`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_ROWS`                                                                                                                     AS `rows_sorted`,\n"
                + "  round(ifnull((`performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_ROWS` / nullif(`performance_schema`.`events_statements_summary_by_digest`.`COUNT_STAR`, 0)), 0), 0)         AS `avg_rows_sorted`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`FIRST_SEEN`                                                                                                                        AS `first_seen`,\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`LAST_SEEN`                                                                                                                         AS `last_seen` \n"
                + "  FROM\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`\n"
                + "WHERE\n"
                + "  (`performance_schema`.`events_statements_summary_by_digest`.`SUM_SORT_ROWS` > 0)\n"
                + "  and ( performance_schema.events_statements_summary_by_digest.SCHEMA_NAME not in ('mysql','sys','information_schema','performance_schema') )\n"
                + "ORDER BY\n"
                + "  `performance_schema`.`events_statements_summary_by_digest`.`SUM_TIMER_WAIT` DESC";

        List<FileSortingSQLQueryData> data = new ArrayList(100);
        try {
            ResultSet rs = (ResultSet) getController().getSelectedConnectionSession().get().getService().execute(query);
            while (rs.next()) {
                data.add(new FileSortingSQLQueryData(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12)
                ));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        TableColumn queryColumn = createColumn("Query", "query");
        TableColumn dbColumn = createColumn("Db", "db");
        TableColumn execCountColumn = createColumn("Exec Count", "exec_count");
        TableColumn totalLatencyColumn = createColumn("Total Latency", "total_latency");
        TableColumn sortMergePassesColumn = createColumn("Sort Merge Passes", "sort_merge_passes");
        TableColumn avgSortMergesColumn = createColumn("Avg Sort Merges", "avg_sort_merges");
        TableColumn sortsUsingScansColumn = createColumn("Sort Using Scans", "sorts_using_scans");
        TableColumn sortUsingRangeColumn = createColumn("Sort Using Range", "sort_using_range");
        TableColumn rowsSortedColumn = createColumn("Rows Sorted", "rows_sorted");
        TableColumn avgRowsSortedColumn = createColumn("Avg Rows Sorted", "avg_rows_sorted");
        TableColumn firstSeenColumn = createColumn("First Seen", "first_seen");
        TableColumn lastSeenColumn = createColumn("Last Seen", "last_seen");
        
        tableView.setSearchEnabled(true);        
        tableView.addTableColumn(queryColumn, 170, 170, false);
        tableView.addTableColumn(dbColumn, 100, 100, false);
        tableView.addTableColumn(execCountColumn, 100, 100, false);
        tableView.addTableColumn(totalLatencyColumn, 100, 100, false);
        tableView.addTableColumn(sortMergePassesColumn, 100, 100, false);
        tableView.addTableColumn(avgSortMergesColumn, 100, 100, false);
        tableView.addTableColumn(sortsUsingScansColumn, 100, 100, false);
        tableView.addTableColumn(sortUsingRangeColumn, 100, 100, false);
        tableView.addTableColumn(rowsSortedColumn, 100, 100, false);
        tableView.addTableColumn(avgRowsSortedColumn, 100, 100, false);
        tableView.addTableColumn(firstSeenColumn, 100, 100, false);
        tableView.addTableColumn(lastSeenColumn, 100, 100, false);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
    }

}
