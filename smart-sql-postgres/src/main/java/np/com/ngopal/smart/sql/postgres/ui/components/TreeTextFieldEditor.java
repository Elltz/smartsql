package np.com.ngopal.smart.sql.ui.components;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import np.com.ngopal.smart.sql.model.Favourites;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class TreeTextFieldEditor extends TreeCell<Favourites> {

    private TextField textField; 

    @Override
    public void cancelEdit() {
        setText(getString());
        setGraphic(getTreeItem().getGraphic());
            
        super.cancelEdit();
    }

    @Override
    public void startEdit() {
        super.startEdit();

        if (getItem() != null && getItem().getId() != null) {
            createTextField();
            setText(null);
            setGraphic(textField);
            Platform.runLater(() -> {
                textField.selectAll();
                textField.requestFocus();
            });
        }
    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setOnKeyReleased((KeyEvent e) -> {
            if (e.getCode() == KeyCode.ENTER) {
                getItem().setTempName(textField.getText());
                commitEdit(getItem());
            } else if (e.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });
        textField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (oldValue != null && oldValue && (newValue == null || !newValue)) {
                cancelEdit();
            }
        });
    }
    

    private String getString() {
        return getItem() == null ? "" : getItem().getName();
    }

    @Override
    protected void updateItem(Favourites value, boolean empty) {
        super.updateItem(value, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else if (isEditing()) {
            if (textField != null) {
                textField.setText(getString());
            }
            setText(null);
            setGraphic(textField);
        } else {
            setText(getString());
            setGraphic(getTreeItem().getGraphic());
        }
    }
}
