package np.com.ngopal.smart.sql.ui.controller;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.db.AppSettingsService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.TechnologyType;
import np.com.ngopal.smart.sql.model.provider.ConnectionParamsProvider;
import np.com.ngopal.smart.sql.modules.SSHModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.modules.ConnectionModuleInterface;
import np.com.ngopal.smart.sql.structure.modules.ProfilerModule;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class NewConnectionController extends BaseController implements
        Initializable {

    private static final String OS__WINDOWS = "Windows";
    private static final String OS__UBUNTU = "Ubuntu";
    private static final String OS__MAC = "Mac";
    private static final String OS__REDHAT = "RedHat / CentOS";
    private static final String OS__DEBIAN = "Debian";
    private static final String OS__SUSE = "SUSE Linux / Open Suse";
    private static final String OS__OTHERS = "Other Linux distributions";
    private static final String OS__AMAZON_S3 = "Aws S3";
    private static final String OS__GOOGLE_CLOUD = "google cloud";

    private static final ColorItem FOREGROUND_DEFAULT_ITEM = new ColorItem("Default", "000000");
    private static final ColorItem FOREGROUND_AUTOCONTRAST_ITEM = new ColorItem("Auto Contrast", "000000");
    private static final ColorItem FOREGROUND_COLOR_ITEM = new ColorItem("ffffff");

    private static final ColorItem BACKGROUND_COLOR_ITEM = new ColorItem("000000");

    @Inject
    MysqlDBService service;

    @Inject
    AppSettingsService settignsService;

    @Inject
    MainController controller;

    @Inject
    ConnectionParamService db;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab mySqlTab;
    @FXML
    private Tab httpTab;
    @FXML
    private Tab sshTab;
    @FXML
    private Tab osTab;
    @FXML
    private Tab sslTab;
    @FXML
    private Tab advancedTab;

    @FXML
    private BorderPane paramBorderPane;

    @FXML
    private BorderPane sshParamBorderPane;

    @FXML
    private Button deleteBttn;

    @FXML
    private TextField database;

    @FXML
    private TextField address;

    @FXML
    private Button cloneBttn;

    @FXML
    private HBox compressProtocolPane;

    @FXML
    private AnchorPane mainUI;

    @FXML
    private PasswordField pass;

    @FXML
    private TextField port;

    @FXML
    private TitledPane compressedTitledPane;

    @FXML
    private CheckBox useSshTunnelingCheckBox;
    @FXML
    private GridPane sshTunnelingPane;
    @FXML
    private TextField sshHost;
    @FXML
    private TextField sshPort;
    @FXML
    private TextField sshUser;
    @FXML
    private TextField sshPassword;
    @FXML
    private CheckBox sshSavePasswordCheckBox;
    @FXML
    private Label sshPasswordLabel;
    @FXML
    private RadioButton sshPasswordTypeRadioButton;
    @FXML
    private RadioButton sshPrivateKeyTypeRadioButton;
    @FXML
    private TextField sshPrivateKeyField;
    @FXML
    private Label sshPrivateKeyLabel;
    @FXML
    private Button sshOpenPrivateKeyButton;

    @FXML
    private RadioButton customSqlModeRadioButton;
    @FXML
    private TextField customSqlModeField;
    @FXML
    private TextField initCommandsField;

    @FXML
    private Button connectBttn;
    @FXML
    private Button testBttn;
    @FXML
    private Button cancelBttn;

    @FXML
    private Button saveBttn;

    @FXML
    private Button renameBttn;

    @FXML
    private TextField user;

    @FXML
    private Button newBttn;

    @FXML
    private ComboBox<ModuleController> moduleComboBox;

    @FXML
    private CheckBox savePasswordCheckBox;

    @FXML
    private ComboBox<ColorItem> backgroundColorComboBox;

    @FXML
    private ComboBox<ColorItem> foregroundColorComboBox;

    @FXML
    private TextField sessionIdleField;
    @FXML
    private RadioButton sessionIdleDefault;
    @FXML
    private RadioButton sessionIdleRadioButton;

    @FXML
    private CheckBox useCompressedProtocolCheckBox;

    @Inject
    private ConnectionParamsProvider provider;

    @FXML
    private ComboBox<ConnectionParams> savedConnectionComboBox;

    private ObservableList<ConnectionParams> connectionParamsList;

    private ObjectProperty<ConnectionParams> selectedConnParam;

    @FXML
    private ToggleButton osWindowsButton;
    @FXML
    private ToggleButton osUbuntuButton;
    @FXML
    private ToggleButton osMacButton;
    @FXML
    private ToggleButton osRedHatButton;
    @FXML
    private ToggleButton osDebianButton;
    @FXML
    private ToggleButton osSuseButton;
    @FXML
    private ToggleButton osOthersButton;

    @FXML
    private ComboBox<String> connectionTypeComboBox;

    @FXML
    private TextField osHost;
    @FXML
    private TextField osPort;
    @FXML
    private TextField osUser;
    @FXML
    private PasswordField osPassword;

    @FXML
    private CheckBox osSavePasswordCheckBox;

    @FXML
    private CheckBox osUsePasswordCheckbox;

    @FXML
    private CheckBox osUsePrivateKeyCheckbox;

    @FXML
    private TextField osPrivateKeyField;

    @FXML
    private Button osOpenPrivateKeyButton;

    @FXML
    private TextField osDiskSizeField;
    @FXML
    private TextField osRamSizeField;

    private boolean hasChanges = false;
    
    private boolean cancelable = true;
    
    @Getter
    private boolean connected = false;

    @FXML
    public void connectBttnAction(ActionEvent event) {
        if (validateFields()) {
            makeBusy(true);
            Platform.runLater(() -> {
                try {
                    if (selectedConnParam.get() != null) {

                        if (hasChanges) {

                            DialogResponce dr = DialogHelper.showConfirm(getController(), "Changes Action", "Would you like to save your new changes?", getStage(), false);
                            if (dr == DialogResponce.OK_YES) {

                                Long id = saveConnection();
                                if (id == null) {
                                    DialogHelper.showError(getController(), "SAVE ERROR", "Could not save detected changes \n Please Try again with the save button", null, getStage());
                                    return;
                                }
                            } else if (dr == DialogResponce.CANCEL) {
                                return;
                            }
                        }

                        WorkProc<ConnectionParams> openConnectProc = new WorkProc<ConnectionParams>() {
                            @Override
                            public void updateUI() { }

                            @Override
                            public void run() {
                                // if selected module is NULL
                                // select as default ConnectionModule
                                String selectedModuleClass = selectedConnParam.get().getSelectedModuleClassName();
                                if (selectedModuleClass == null) {
                                    selectedModuleClass = ConnectionModule.class.getName();
                                }
                                
                                ModuleController mc = controller.getModuleByClassName(selectedModuleClass);                                
                                if(mc instanceof ConnectionModuleInterface){
                                    ((ConnectionModuleInterface) mc).connectToConnection(selectedConnParam.get());
                                }else{
                                    
                                }
                            }
                        };

                        Recycler.addWorkProc(openConnectProc);
                        connected = true;
                        if(cancelable){
                            getStage().close();
                        }
                    }
                } finally {
                    makeBusy(false);
                }
            });
        }
    }

    @FXML
    void testBttnAction(ActionEvent event) {

        if (validateFields()) {
            makeBusy(true);
            if (hasChanges) {
                DialogResponce dr = DialogHelper.showConfirm(getController(),
                        "Changes Action", "Would you like to save your new changes?", getStage(), false);
                if (dr == DialogResponce.OK_YES) {
                    saveConnection();
                } else if (dr == DialogResponce.CANCEL) {
                    makeBusy(false);
                    return;
                }
            }

            WorkProc<ConnectionParams> testConnection = new WorkProc<ConnectionParams>() {

                SQLException exception;
                String productInfo;

                BigDecimal totalMemory;
                BigDecimal totalDisk;

                @Override
                public void updateUI() {

                    if (exception != null) {
                        DialogHelper.showError(getController(), "Error on test connection", exception.getMessage(), exception, getStage());
                    } else {
                        boolean isOsTab = tabPane.getSelectionModel().getSelectedItem() == osTab;

                        if (isOsTab && totalMemory != null && totalMemory.compareTo(BigDecimal.ZERO) > 0
                                && totalDisk != null && totalDisk.compareTo(BigDecimal.ZERO) > 0) {

                            osDiskSizeField.setText(totalDisk.toString());
                            osRamSizeField.setText(totalMemory.toString());

                            DialogHelper.showInfo(getController(), "Info", "Connetion successful and remote metrics are updated!\n" + productInfo, null, getStage());
                        } else if (isOsTab && selectedConnParam.get().isRemote() && selectedConnParam.get().hasOSDetails()) {
                            DialogHelper.showInfo(getController(), "Info", "Connetion successful, but can't get os metrics with current OS details!\n" + productInfo, null, getStage());
                        } else {
                            DialogHelper.showInfo(getController(), "Info", "Connetion successful!\n" + productInfo, null, getStage());
                        }
                    }
                    makeBusy(false);
                }

                @Override
                public void run() {
                    try {
                        productInfo = service.testConnection(selectedConnParam.get());
                    } catch (SQLException ex) {
                        exception = ex;
                    }
                    callUpdate = true;
                }
            };
            Recycler.addWorkProc(testConnection);
        }
    }

    public Long saveConnection() {
        if (selectedConnParam.get() != null) {
            
            selectedConnParam.get().setSavePassword(savePasswordCheckBox.isSelected());
            
//            if (!savePasswordCheckBox.isSelected()) {
//                selectedConnParam.get().setPassword("");
//            }

            if (!sshSavePasswordCheckBox.isSelected()) {
                selectedConnParam.get().setSshPassword("");
            }

            if (!osSavePasswordCheckBox.isSelected()) {
                selectedConnParam.get().setOsPassword("");
            }

            setHasChanges(false);

            /**
             * here we are trying to get the new changes into the reference we
             * hold
             */
            
            String password = selectedConnParam.get().getPassword();
            if (!savePasswordCheckBox.isSelected()) {
                selectedConnParam.get().setPassword("");
            }
            
            ConnectionParams connectionParams = db.save(selectedConnParam.get());
            
            connectionParams.setPassword(password);
            
            //selectedConnParam.set(connectionParams);
            //we are re-selecting an already selected item in the combo box because it is binded 
            // to the selectionConnParam
            savedConnectionComboBox.getSelectionModel().select(null);//trying to trigger the changeListener
            savedConnectionComboBox.getSelectionModel().select(connectionParams);

            /**
             * here we change the connectionParam that is stored with the
             * currently selected connectionSession. we check if its null first,
             * incase of user first creating for the first time he use app.
             */
            // This is very very bad idea, we didnt check that current connection is edited
            // !!!!! DO NOT UNCOMMENT WITHOUT REFACTORING
//            if (controller.getSelectedConnectionSession().get() != null) {
//                controller.getSelectedConnectionSession().get().setConnectionParam(connectionParams);
//            }

            return connectionParams.getId();
        }
        return null;
    }

    @FXML
    void saveActionBttn(ActionEvent event) {
        if (saveConnection() != null) {
            DialogHelper.showInfo(getController(), "Info", "Successfully saved", null, getStage());
        }
    }

    @FXML
    void newBttnAction(ActionEvent event) {

        String result = DialogHelper.showInput(getController(), "New Connection", "Enter New Connection Name", "Name:", "", getStage());

        if (result != null && !result.isEmpty()) {

            if (db.checkConnectionParamsName(result)) {
                DialogHelper.showError(getController(), "Error", "the name already exists! Please enter other name!", null, getStage());
                return;
            }

            ConnectionParams p = provider.get();
            p.setName(result);

            addConnectionNameList(p);
            comboBoxChangeSelection(p);

            moduleComboBox.getSelectionModel().select(0);
        }

    }

    @FXML
    void openPrivateKey(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PuTTY Private Key Files", "*.ppk"),
                new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showOpenDialog(getStage());
        if (file != null && sshTab.isSelected()) {
            sshPrivateKeyField.setText(file.getAbsolutePath());
        } else if (file != null && osTab.isSelected()) {
            osPrivateKeyField.setText(file.getAbsolutePath());
        }
    }

    private boolean validateFields() {
        ConnectionParams params = selectedConnParam.get();
        if (params != null) {

            // for now check only Connection and SSH Module
            if (params.getSelectedModuleClassName() == null || "np.com.ngopal.smart.sql.core.modules.ConnectionModule".equals(params.getSelectedModuleClassName())) {
                if (isStringEmpty(params.getAddress())) {
                    DialogHelper.showError(getController(), "Error", "Please enter Host Address", null, getStage());
                    return false;
                }

                boolean sshEnabled = params.isUseSshTunneling();//!isStringEmpty(params.getSshHost()) || !isStringEmpty(params.getSshUser()) || !isStringEmpty(params.getSshPassword());
                if (sshEnabled) {
                    if (isStringEmpty(params.getSshHost())) {
                        DialogHelper.showError(getController(), "Error", "Please enter SSH Host", null, getStage());
                        return false;
                    }

                    if (isStringEmpty(params.getSshUser())) {
                        DialogHelper.showError(getController(), "Error", "Please enter SSH User", null, getStage());
                        return false;
                    }

                    if (!params.isUseSshPrivateKey() && isStringEmpty(params.getSshPassword())) {
                        DialogHelper.showError(getController(), "Error", "Please enter SSH Password", null, getStage());
                        return false;
                    }
                }
            } else if ("np.com.ngopal.smart.sql.modules.SSHModule".equals(params.getSelectedModuleClassName())) {
                if (isStringEmpty(params.getSshHost())) {
                    DialogHelper.showError(getController(), "Error", "Please enter SSH Host", null, getStage());
                    return false;
                }

                if (isStringEmpty(params.getSshUser())) {
                    DialogHelper.showError(getController(), "Error", "Please enter SSH User", null, getStage());
                    return false;
                }

                if (isStringEmpty(params.getSshPassword())) {
                    DialogHelper.showError(getController(), "Error", "Please enter SSH Password", null, getStage());
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private boolean isStringEmpty(String text) {
        return text == null || text.isEmpty();
    }

    @FXML
    void cloneBttnAction(ActionEvent event) {

        ConnectionParams connectionParams = selectedConnParam.get();
        if (connectionParams != null) {

            String result = DialogHelper.showInput(getController(), "New Connection", "Enter New Connection Name", "Name:", connectionParams.getName() + "_New", getStage());
            if (result != null && !result.isEmpty()) {

                if (db.checkConnectionParamsName(result)) {
                    DialogHelper.showError(getController(), "Error", "the name already exists! Please enter other name!!", null, getStage());
                    return;
                }

                ConnectionParams p = connectionParams.copyOnlyParams();
                p.setName(result);

                addConnectionNameList(p);
                comboBoxChangeSelection(p);
            }
        }
    }

    @FXML
    void renameBttnAction(ActionEvent event) {

        ConnectionParams params = selectedConnParam.get();
        if (params != null) {
            String result = DialogHelper.showInput(getController(), "Rename Connection", "Enter New Connection Name", "Name:", params.getName(), getStage());
            if (result != null && !result.isEmpty()) {

                ConnectionParams cp = db.findConenctionParamsByName(result);
                if (cp != null && !cp.getId().equals(params.getId())) {
                    DialogHelper.showError(getController(), "Error", "the name already exists! Please enter other name!!", null, getStage());
                    return;
                }

                params.setName(result);
                //db.save(params);

                int index = connectionParamsList.indexOf(params);
                connectionParamsList.remove(params);
                connectionParamsList.add(index, params);
                Collections.sort(connectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
                    return o1.getName().compareTo(o2.getName());
                });

                comboBoxChangeSelection(params);
            }
        }
    }

    @FXML
    void deleteBttnAction(ActionEvent event) {

        DialogResponce result = DialogHelper.showConfirm(getController(), "Delete connection", "Do you realy want to delete the connection detail?", getStage(), false);

        if (result == DialogResponce.OK_YES) {
            ConnectionParams params = selectedConnParam.get();
            if (params != null && db.delete(params)) {
                connectionParamsList.remove(params);
                DialogHelper.showInfo(getController(), "Info", "Successfully deleted '" + params.getName() + "'", null, getStage());
            }
        }
    }

    @FXML
    void cancelBttnAction(ActionEvent event) {
        if(cancelable){
            getStage().close();
        }else{
            DialogHelper.showError(getController(), "CONNECTION ALERT", "You need to create connection", null, getStage());
        }
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        ((ImageView)cancelBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCancelButton_image());
        ((ImageView)testBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getTestConnectionButton_image());
        ((ImageView)connectBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getConnectionButton_image());
        ((ImageView)deleteBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getDeleteButton_image());
        ((ImageView)renameBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRenameButton_image());
        ((ImageView)saveBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getSaveButton_image());
        ((ImageView)cloneBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getCloneButton_image());
        ((ImageView)newBttn.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getNewButton_image());
                
        init();

        compressedTitledPane.expandedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            getStage().setHeight(getStage().getHeight() + (newValue != null && newValue ? 80 : -80));
        });

        compressProtocolPane.managedProperty().bind(compressProtocolPane.visibleProperty());
        compressProtocolPane.visibleProperty().bind(useCompressedProtocolCheckBox.selectedProperty());

        ConnectionParams selected = null;
        String value = settignsService.getSettingValue(AppSettingsService.SETTING_NAME__LAST_OPENED_CONNECTION);
        if (value != null) {
            try {
                selected = db.get(Long.valueOf(value));
            } catch (Throwable th) {
            }
        }

        if (selected == null) {
            selected = connectionParamsList != null && !connectionParamsList.isEmpty() ? connectionParamsList.get(0) : null;
        }
        comboBoxChangeSelection(selected);
        
        cloneBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        saveBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        renameBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        deleteBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        paramBorderPane.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        sshParamBorderPane.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());

        newBttn.defaultButtonProperty().bind(newBttn.focusedProperty());
        cloneBttn.defaultButtonProperty().bind(cloneBttn.focusedProperty());
        saveBttn.defaultButtonProperty().bind(saveBttn.focusedProperty());
        renameBttn.defaultButtonProperty().bind(renameBttn.focusedProperty());
        deleteBttn.defaultButtonProperty().bind(deleteBttn.focusedProperty());

        sessionIdleField.disableProperty().bind(sessionIdleDefault.selectedProperty());

        sshTunnelingPane.disableProperty().bind(useSshTunnelingCheckBox.selectedProperty().not());
        sshPrivateKeyField.disableProperty().bind(sshPasswordTypeRadioButton.selectedProperty());
        sshPrivateKeyLabel.disableProperty().bind(sshPasswordTypeRadioButton.selectedProperty());
        sshOpenPrivateKeyButton.disableProperty().bind(sshPasswordTypeRadioButton.selectedProperty());

        customSqlModeField.disableProperty().bind(customSqlModeRadioButton.selectedProperty().not());

        connectBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        testBttn.disableProperty().bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty().isNull());
        
        connectBttn.defaultButtonProperty().bind(connectBttn.focusedProperty());
        testBttn.defaultButtonProperty().bind(testBttn.focusedProperty());
        cancelBttn.defaultButtonProperty().bind(cancelBttn.focusedProperty());

        osPrivateKeyField.disableProperty().bind(osUsePrivateKeyCheckbox.selectedProperty().not());
        osOpenPrivateKeyButton.disableProperty().bind(osUsePrivateKeyCheckbox.selectedProperty().not());

        osPassword.disableProperty().bind(osUsePasswordCheckbox.selectedProperty().not());

        mainUI.sceneProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                if (newValue != null && newValue instanceof Scene) {
                    mainUI.sceneProperty().removeListener(this);
                    ((Scene) newValue).windowProperty().addListener(this);
                } else if (newValue != null && newValue instanceof Window) {
                    ((Window) newValue).getScene().windowProperty().removeListener(this);
                    ((Window) newValue).setOnShown(new EventHandler<WindowEvent>() {
                        @Override
                        public void handle(WindowEvent event) {
                            double constant = 0.0;
                            double stageWidtht = ((Stage) newValue).getOwner().getWidth();
                            double stageHeight = ((Stage) newValue).getOwner().getHeight();

                            if (stageWidtht > mainUI.getWidth()) {
                                constant = Math.abs((stageWidtht - mainUI.getWidth()) / 2);
                                ((Stage) newValue).setX(((Stage) newValue).getOwner().getX() + constant);
                            } else {
                                ((Stage) newValue).setX(((Stage) newValue).getOwner().getX());
                            }

                            if (stageHeight > mainUI.getHeight()) {
                                constant = Math.abs((stageHeight - mainUI.getHeight()) / 2);
                                ((Stage) newValue).setY(((Stage) newValue).getOwner().getY() + constant);
                            } else {
                                ((Stage) newValue).setY(((Stage) newValue).getOwner().getY());
                            }
                        }
                    });
                }
            }
        });

        tabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue == osTab && getConnectionParams().isRemote() && !getConnectionParams().hasOSDetails()) {
                DialogHelper.showInfo(getController(), "Info", "Please fill OS details or OS Metrics for remote connection", null);
            }
        });
    }

    private void selectModuleByClassname(String className) {
        moduleComboBox.getSelectionModel().clearSelection();
        for (ModuleController c : moduleComboBox.getItems()) {
            if (c.getClass().getName().equals(className)) {
                moduleComboBox.getSelectionModel().select(c);
                break;
            }
        }
    }

    private void updateFields(ConnectionParams params) {

        if (params != null && params.getConnectionType() != null) {
            connectionTypeComboBox.getSelectionModel().select(params.getConnectionType());
        } else {
            connectionTypeComboBox.getSelectionModel().select(0);
        }

        if (params != null && params.getOsName() != null) {
            switch (params.getOsName()) {
                case OS__WINDOWS:
                    osWindowsButton.setSelected(true);
                    break;

                case OS__UBUNTU:
                    osUbuntuButton.setSelected(true);
                    break;

                case OS__MAC:
                    osMacButton.setSelected(true);
                    break;

                case OS__DEBIAN:
                    osDebianButton.setSelected(true);
                    break;

                case OS__REDHAT:
                    osRedHatButton.setSelected(true);
                    break;

                case OS__SUSE:
                    osSuseButton.setSelected(true);
                    break;

                case OS__OTHERS:
                    osOthersButton.setSelected(true);
                    break;
            }
        }

        useCompressedProtocolCheckBox.setSelected(params != null ? params.isUseCompressedProtocol() : true);
        address.setText(params != null ? params.getAddress() : "");
        user.setText(params != null ? params.getUsername() : "");
        savePasswordCheckBox.setSelected(params != null ? params.isSavePassword() : true);
        pass.setText(params != null ? params.getPassword() : "");
        port.setText(params != null ? params.getPort() + "" : "");
        selectModuleByClassname(params != null ? params.getSelectedModuleClassName() : "");
        database.setText(reverseSeparateWithComma(params != null ? params.getDatabase() : null));

        sessionIdleField.setText(params != null && params.getSessionIdleTimeout() > 0 ? params.getSessionIdleTimeout() + "" : "28800");
        sessionIdleRadioButton.setSelected(params != null && params.getSessionIdleTimeout() != 28800);
        sessionIdleDefault.setSelected(params == null || params.getSessionIdleTimeout() == 0 || params.getSessionIdleTimeout() == 28800);

        // Ssh module
        sshHost.setText(params != null ? params.getSshHost() : "");
        sshPort.setText(params != null ? params.getSshPort() + "" : "");
        sshUser.setText(params != null ? params.getSshUser() : "");
        sshPassword.setText(params != null ? params.getSshPassword() : "");
        useSshTunnelingCheckBox.setSelected(params != null ? params.isUseSshTunneling() : false);
        sshPrivateKeyTypeRadioButton.setSelected(params != null && params.isUseSshPrivateKey());
        sshPasswordTypeRadioButton.setSelected(params == null || !params.isUseSshPrivateKey());
        sshPrivateKeyField.setText(params != null ? params.getSshPrivateKey() : "");

        // OS part
        osHost.setText(params != null ? params.getOsHost() : "");
        osPort.setText(params != null ? params.getOsPort() + "" : "");
        osUser.setText(params != null ? params.getOsUser() : "");
        osPassword.setText(params != null ? params.getOsPassword() : "");
        osUsePasswordCheckbox.setSelected(params != null && params.isOsUsePassword());
        osUsePrivateKeyCheckbox.setSelected(params != null && !params.isOsUsePassword());
        osPrivateKeyField.setText(params != null ? params.getOsPrivateKey() : "");

        osRamSizeField.setText(params != null ? params.getOsMetricRamSize() : "");
        osDiskSizeField.setText(params != null ? params.getOsMetricDiskSize() : "");

        // advanced
        if (params != null) {

            boolean found = false;
            for (ColorItem n : backgroundColorComboBox.getItems()) {
                if (n.color != null && n.color.equals(params.getBackgroundColor())) {
                    backgroundColorComboBox.getSelectionModel().select(n);
                    found = true;
                    break;
                }
            }

            // need set color to OTHER COLORS
            if (!found && params.getBackgroundColor() != null) {
                BACKGROUND_COLOR_ITEM.setColor(params.getBackgroundColor());
                backgroundColorComboBox.getSelectionModel().select(BACKGROUND_COLOR_ITEM);
            }

            found = false;
            for (ColorItem n : foregroundColorComboBox.getItems()) {
                if (n.color != null && n.color.equals(params.getForegroundColor())) {
                    foregroundColorComboBox.getSelectionModel().select(n);
                    found = true;
                    break;
                }
            }

            if (!found && params.getForegroundColor() != null) {
                FOREGROUND_COLOR_ITEM.setColor(params.getForegroundColor());
                foregroundColorComboBox.getSelectionModel().select(FOREGROUND_COLOR_ITEM);
            }
        }

        if (params != null && params.isEnabledCustomSqlMode()) {
            customSqlModeRadioButton.setSelected(true);
        }

        customSqlModeField.setText(params != null ? params.getCustomSqlMode() : "");
        initCommandsField.setText(params != null ? params.getInitCommands() : "");

    }

    public ListCell<ConnectionParams> getConnectionParamsListCell() {
        return new ListCell<ConnectionParams>() {

            @Override
            protected void updateItem(ConnectionParams item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (item != null) {
                    setText(item.getName());
                }
            }

        };
    }

    public void init() {
        // show only connection modules
        List<ModuleController> connectionModules = controller.getModules();
        if (connectionModules != null) {
            connectionModules = connectionModules.stream().filter(ModuleController::isConnection).collect(Collectors.toList());
        }
        moduleComboBox.setItems(FXCollections.observableArrayList(connectionModules));
        moduleComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ModuleController> observable, ModuleController oldValue, ModuleController newValue) -> {
            if (newValue instanceof SSHModule) {
                osRedHatButton.setSelected(true);
                tabPane.getSelectionModel().select(sshTab);
            }            
            osWindowsButton.setSelected(true);
            tabPane.getSelectionModel().select(mySqlTab);

            osMacButton.setDisable(false);
            osRedHatButton.setDisable(false);
            osDebianButton.setDisable(false);
            osSuseButton.setDisable(false);
        });

        new AutoCompleteComboBoxListener<>(moduleComboBox);
        moduleComboBox.setConverter(new StringConverter<ModuleController>() {
            @Override
            public String toString(ModuleController object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public ModuleController fromString(String string) {
                for (ModuleController cp : controller.getModules()) {
                    if (cp.getName().equals(string)) {
                        return cp;
                    }
                }

                return null;
            }
        });

        selectedConnParam = new SimpleObjectProperty<ConnectionParams>() {

            @Override
            protected void invalidated() {
                super.invalidated();
                get();
            }

        };
        selectedConnParam.bind(savedConnectionComboBox.getSelectionModel().selectedItemProperty());

        connectionParamsList = FXCollections.observableArrayList();
        connectionParamsList.addAll(db.getAll());

        Collections.sort(connectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
            return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
        });

        savePasswordCheckBox.setSelected(true);
        sshSavePasswordCheckBox.setSelected(true);
        osSavePasswordCheckBox.setSelected(true);

        savedConnectionComboBox.setButtonCell(getConnectionParamsListCell());
        savedConnectionComboBox.setCellFactory(new Callback<ListView<ConnectionParams>, ListCell<ConnectionParams>>() {

            @Override
            public ListCell<ConnectionParams> call(ListView<ConnectionParams> param) {
                return getConnectionParamsListCell();
            }

        });

        new AutoCompleteComboBoxListener<>(savedConnectionComboBox);
        savedConnectionComboBox.setConverter(new StringConverter<ConnectionParams>() {
            @Override
            public String toString(ConnectionParams object) {
                return object != null ? object.getName() : "";
            }

            @Override
            public ConnectionParams fromString(String string) {
                for (ConnectionParams cp : connectionParamsList) {
                    if (cp.getName().equals(string)) {
                        return cp;
                    }
                }

                return null;
            }
        });

        savedConnectionComboBox.setItems(connectionParamsList);
        Platform.runLater(() -> savedConnectionComboBox.getEditor().selectAll());

        backgroundColorComboBox.setItems(defaultBackgroundColors());
        backgroundColorComboBox.setCellFactory(new Callback<ListView<ColorItem>, ListCell<ColorItem>>() {
            @Override
            public ListCell<ColorItem> call(ListView<ColorItem> list) {
                return new ColorRectCell(backgroundColorComboBox, BACKGROUND_COLOR_ITEM);
            }
        });

        backgroundColorComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
            if (newValue != null) {
                Color color = Color.web(newValue.color).invert();
                FOREGROUND_AUTOCONTRAST_ITEM.setColor(color.toString().substring(2, 8).toUpperCase());

                if (foregroundColorComboBox.getSelectionModel().getSelectedItem() != FOREGROUND_COLOR_ITEM) {
                    foregroundColorComboBox.getSelectionModel().clearSelection();
                }
                foregroundColorComboBox.getItems().remove(FOREGROUND_AUTOCONTRAST_ITEM);
                foregroundColorComboBox.getItems().add(1, FOREGROUND_AUTOCONTRAST_ITEM);
            }
        });

        foregroundColorComboBox.setItems(defaultForegroundColors());
        foregroundColorComboBox.setCellFactory(new Callback<ListView<ColorItem>, ListCell<ColorItem>>() {
            @Override
            public ListCell<ColorItem> call(ListView<ColorItem> list) {
                return new ColorRectCell(foregroundColorComboBox, FOREGROUND_COLOR_ITEM);
            }
        });

        connectionTypeComboBox.setItems(FXCollections.observableArrayList(new String[]{"Production", "Test", "QA", "Development"}));
        new AutoCompleteComboBoxListener<>(connectionTypeComboBox);
        connectionTypeComboBox.setConverter(new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return object;
            }

            @Override
            public String fromString(String string) {
                return string;
            }
        });

        connectionTypeComboBox.getSelectionModel().select(0);
        propertyListeners();
    }

    static class ColorItem extends Label {

        String color;
        String text;

        ColorItem(String text, String color) {
            this.color = color;
            this.text = text;
            setMaxWidth(Double.MAX_VALUE);
            if (color != null) {
                if (!color.startsWith("#")) {
                    color = "#" + color;
                }
                setStyle("-fx-background-color: " + color);
            }
        }

        ColorItem(String color) {
            this(null, color);
        }

        public void setColor(String color) {
            this.color = color;
            if (color != null) {
                if (!color.startsWith("#")) {
                    color = "#" + color;
                }
                setStyle("-fx-background-color: " + color);
            } else {
                setStyle("");
            }
        }
    }

    static class ColorRectCell extends ListCell<ColorItem> {

        private final ColorItem otherColors;
        private final ComboBox<ColorItem> combobox;

        public ColorRectCell(ComboBox<ColorItem> combobox, ColorItem otherColors) {
            this.combobox = combobox;
            this.otherColors = otherColors;
        }

        @Override
        public void updateItem(ColorItem item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                if (item != null && otherColors != item) {
                    if (item.text != null) {
                        Rectangle rect = new Rectangle(50, 20);
                        rect.setFill(Color.web(item.color));

                        HBox h = new HBox(rect, new Label(item.text));
                        h.setAlignment(Pos.CENTER_LEFT);
                        h.setSpacing(10);
                        setGraphic(h);
                    } else {
                        Rectangle rect = new Rectangle(getListView().getWidth() - 50, 20);
                        rect.setFill(Color.web(item.color));
                        setGraphic(rect);
                    }
                } else {
                    ColorPicker l = new ColorPicker(Color.web(otherColors.color));
                    l.getStyleClass().add(ColorPicker.STYLE_CLASS_BUTTON);
                    l.setPrefSize(getListView().getWidth() - 50, 20);
                    l.setOnMousePressed((MouseEvent event) -> {
                        combobox.getSelectionModel().select(otherColors);
                    });
                    l.setOnAction((ActionEvent event) -> {
                        otherColors.setColor(l.getValue().toString().substring(2, 8).toUpperCase());
                        combobox.getItems().remove(otherColors);
                        combobox.getItems().add(otherColors);
                        combobox.getSelectionModel().select(otherColors);
                    });

                    setGraphic(l);
                }
            }
        }
    }

    public void selectSavedConnection(final ConnectionParams params) {
        savedConnectionComboBox.getSelectionModel().select(params);
    }

//    public void selectNewConnection(final String name) {
//        ConnectionParams p = provider.get();
//        p.setName(name);
//        addConnectionNameList(p);
//        comboBoxChangeSelection(p);
//        
//        savedConnectionComboBox.getSelectionModel().select(p);        
//    }
    public void propertyListeners() {

        selectedConnParam.addListener(
                (ObservableValue<? extends ConnectionParams> observable, ConnectionParams oldValue, ConnectionParams newValue) -> {
                    updateFields(newValue);
                    setHasChanges(false);
                });
        moduleComboBox.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends ModuleController> observable, ModuleController oldValue, ModuleController newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSelectedModuleClassName(), newValue.getClass().getName())) {
                            setHasChanges(true);
                            selectedConnParam.get().setSelectedModuleClassName(newValue.getClass().getName());
                        }
                    }
                });
        
        savePasswordCheckBox.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isSavePassword(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setSavePassword(newValue);
                        }
                    }
                });

        useCompressedProtocolCheckBox.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isUseCompressedProtocol(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setUseCompressedProtocol(newValue);
                        }
                    }
                });

        connectionTypeComboBox.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getConnectionType(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setConnectionType(newValue);
                    }
                });

        address.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getAddress(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setAddress(newValue);
                    }
                });
        user.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getUsername(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setUsername(newValue);
                    }
                });

        pass.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {

                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getPassword(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setPassword(newValue);
                    }
//                    if (newValue == null) {
//                        savePasswordCheckBox.setSelected(false);
//                    } else {
//                        savePasswordCheckBox.setSelected(true);
//                    }
                });

        port.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.matches("\\d*")) {
                try {
                    int value = Integer.parseInt(newValue);
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getPort(), value)) {
                        setHasChanges(true);
                        selectedConnParam.get().setPort(value);
                    }
                } catch (NumberFormatException ex) {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getPort(), 0)) {
                        setHasChanges(true);
                        selectedConnParam.get().setPort(0);
                    }
                }
            } else {
                port.setText(oldValue);
            }
        });

        sessionIdleField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.matches("\\d*")) {
                try {
                    int value = Integer.parseInt(newValue);
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSessionIdleTimeout(), value)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSessionIdleTimeout(value);
                    }
                } catch (NumberFormatException ex) {
                }
            } else {
                sessionIdleField.setText(oldValue);
            }
        });

        database.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && (selectedConnParam.get().getDatabase() == null || !Objects.equal(String.join(";", selectedConnParam.get().getDatabase()), newValue))) {
                        setHasChanges(true);
                        selectedConnParam.get().setDatabase(separateWithComma(newValue));
                    }
                });

        backgroundColorComboBox.valueProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getBackgroundColor(), newValue.color)) {
                setHasChanges(true);
                selectedConnParam.get().setBackgroundColor(newValue.color);
            }
        });

        foregroundColorComboBox.valueProperty().addListener((ObservableValue<? extends ColorItem> observable, ColorItem oldValue, ColorItem newValue) -> {
            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getForegroundColor(), newValue.color)) {
                setHasChanges(true);
                selectedConnParam.get().setForegroundColor(newValue.color);
            }
        });

        sshHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshHost(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshHost(newValue);
                    }
                });

        sshPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (newValue.matches("\\d*")) {
                        try {
                            int port = Integer.parseInt(newValue);
                            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPort(), port)) {
                                setHasChanges(true);
                                selectedConnParam.get().setSshPort(port);
                            }
                        } catch (NumberFormatException ex) {
                            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPort(), 0)) {
                                setHasChanges(true);
                                selectedConnParam.get().setSshPort(0);
                            }
                        }
                    } else {
                        sshPort.setText(oldValue);
                    }
                });

        sshUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshUser(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshUser(newValue);
                    }
                });

        sshPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPassword(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshPassword(newValue);
                    }

                    sshSavePasswordCheckBox.setSelected(pass != null);
                });

        useSshTunnelingCheckBox.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isUseSshTunneling(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setUseSshTunneling(newValue);
                        }
                    }
                });

        sshPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getSshPrivateKey(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setSshPrivateKey(newValue);
                    }
                });

        sshPrivateKeyTypeRadioButton.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue != null) {
                        if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isUseSshPrivateKey(), newValue)) {
                            setHasChanges(true);
                            selectedConnParam.get().setUseSshPrivateKey(newValue);
                        }
                    }
                });

        sshPasswordTypeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            sshPasswordLabel.setText(newValue != null && newValue ? "Password" : "Passphrase");
            sshSavePasswordCheckBox.setText(newValue != null && newValue ? "Save Password" : "Save Passphrase");
        });

        sessionIdleDefault.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                sessionIdleField.setText("28800");
            }
        });

        osWindowsButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                if (moduleComboBox.getSelectionModel().getSelectedItem() instanceof ProfilerModule && osTab.isSelected()) {
                    DialogHelper.showError(getController(), "MONITORING ERROR", "the WINDOWS OS/SERVERS are currently not supported!!", null);
                    osWindowsButton.setSelected(false);
                    return;
                }
                selectedConnParam.get().setOsName(OS__WINDOWS);
            }
        });

        osUbuntuButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                selectedConnParam.get().setOsName(OS__UBUNTU);
            }
        });

        osMacButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                selectedConnParam.get().setOsName(OS__MAC);
            }
        });

        osRedHatButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                selectedConnParam.get().setOsName(OS__REDHAT);
            }
        });

        osDebianButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                selectedConnParam.get().setOsName(OS__DEBIAN);
            }
        });

        osSuseButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                setHasChanges(true);
                selectedConnParam.get().setOsName(OS__SUSE);
            }
        });

        osOthersButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && newValue != null && newValue) {
                selectedConnParam.get().setOsName(OS__OTHERS);
            }
        });

        osHost.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsHost(), newValue)) {
                        selectedConnParam.get().setOsHost(newValue);
                    }
                });

        osPort.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (newValue.matches("\\d*")) {
                        try {
                            int port = Integer.parseInt(newValue);
                            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsPort(), port)) {
                                setHasChanges(true);
                                selectedConnParam.get().setOsPort(port);
                            }
                        } catch (NumberFormatException ex) {
                            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsPort(), 0)) {
                                setHasChanges(true);
                                selectedConnParam.get().setOsPort(0);
                            }
                        }
                    } else {
                        osPort.setText(oldValue);
                    }
                });

        osUser.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsUser(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setOsUser(newValue);
                    }
                });

        osDiskSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsMetricDiskSize(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setOsMetricDiskSize(newValue);
                    }
                });

        osRamSizeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsMetricRamSize(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setOsMetricRamSize(newValue);
                    }
                });

        osPassword.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsPassword(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setOsPassword(newValue);
                    }

                    osSavePasswordCheckBox.setSelected(pass != null);
                });

        osUsePasswordCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePrivateKeyCheckbox.setSelected(false);
                }
                if (selectedConnParam.get() != null && newValue != null) {
                    setHasChanges(true);
                    selectedConnParam.get().setOsUsePassword(newValue);
                }
            }
        });

        osUsePrivateKeyCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    osUsePasswordCheckbox.setSelected(false);
                }
            }
        });

        osPrivateKeyField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getOsPrivateKey(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setOsPrivateKey(newValue);
                    }
                });

        customSqlModeField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getCustomSqlMode(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setCustomSqlMode(newValue);
                    }
                });

        initCommandsField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().getInitCommands(), newValue)) {
                        setHasChanges(true);
                        selectedConnParam.get().setInitCommands(newValue);
                    }
                });

        customSqlModeRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (selectedConnParam.get() != null && !Objects.equal(selectedConnParam.get().isEnabledCustomSqlMode(), newValue)) {
                setHasChanges(true);
                selectedConnParam.get().setEnabledCustomSqlMode(newValue);
            }
        });

    }

    public void setHasChanges(boolean f) {
        hasChanges = f;
    }

    public void comboBoxChangeSelection(ConnectionParams params) {
        if (connectionParamsList.size() > 0) {
            savedConnectionComboBox.getSelectionModel().select(params);
        }

    }

    public void addConnectionNameList(ConnectionParams connectionParam) {
        connectionParam.setType(TechnologyType.MYSQL);

        connectionParamsList.add(connectionParam);
        Collections.sort(connectionParamsList, (ConnectionParams o1, ConnectionParams o2) -> {
            return o1.getName() != null && o2.getName() != null ? o1.getName().compareTo(o2.getName()) : -1;
        });

        savedConnectionComboBox.getSelectionModel().select(connectionParam);
    }

    public List<String> separateWithComma(String databases) {
        if (!databases.trim().isEmpty()) {

            String[] result = databases.split(";");

            List<String> l = new ArrayList<>();
            for (String r : result) {
                l.add(r.trim());
            }

            return l;
        } else {
            return new ArrayList<>();
        }

    }

    public String reverseSeparateWithComma(List<String> databaseList) {
        if (databaseList == null) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < databaseList.size(); i++) {
            stringBuilder.append(databaseList.get(i));
            if (i == databaseList.size() - 1) {
                continue;
            }
            stringBuilder.append(';');
        }

        return stringBuilder.toString();
    }

    private ObservableList<ColorItem> defaultBackgroundColors() {
        ObservableList<ColorItem> colorItems = FXCollections.observableArrayList();
        colorItems.add(new ColorItem("ffffff"));
        colorItems.add(new ColorItem("ffe3e3"));
        colorItems.add(new ColorItem("f0fff0"));
        colorItems.add(new ColorItem("e0ecff"));
        colorItems.add(new ColorItem("fffacd"));
        colorItems.add(new ColorItem("f4ecff"));
        colorItems.add(new ColorItem("f9ffef"));
        colorItems.add(new ColorItem("ffe6ff"));
        colorItems.add(new ColorItem("cffafe"));
        colorItems.add(new ColorItem("eaeaae"));
        colorItems.add(new ColorItem("fff0f5"));
        colorItems.add(BACKGROUND_COLOR_ITEM);

        return colorItems;
    }

    private ObservableList<ColorItem> defaultForegroundColors() {
        ObservableList<ColorItem> colorItems = FXCollections.observableArrayList();
        colorItems.add(FOREGROUND_DEFAULT_ITEM);
        colorItems.add(FOREGROUND_AUTOCONTRAST_ITEM);
        colorItems.add(FOREGROUND_COLOR_ITEM);

        return colorItems;
    }

    @Override
    public Stage getStage() {
        if (database.getScene() == null) {
            return null;
        }
        return (Stage) database.getScene().getWindow(); //To change body of generated methods, choose Tools | Templates.
    }

    public void addTab(Tab tab) {
        tabPane.getTabs().add(tab);
    }

    public void addTab(Tab tab, int index) {
        tabPane.getTabs().add(index, tab);
    }

    public ConnectionParams getConnectionParams() {
        return selectedConnParam.get();
    }

    public void addConnectionChangeListener(ChangeListener<ConnectionParams> c) {
        selectedConnParam.addListener(c);
    }

    public void selectModule(ModuleController mc) {
        selectModule(mc.getClass());
    }

    public final void selectModule(Class moduleClass) {
        if (moduleClass != null) {
            for (ModuleController md : moduleComboBox.getItems()) {
                if (md.getClass() == moduleClass) {
                    moduleComboBox.getSelectionModel().select(md);
                }
            }
        }
    }

    public void selectConnectionType(String contype) {
        connectionTypeComboBox.getSelectionModel().select(contype);
    }

    public void selectConnectionParam(String paramId) {
        for (ConnectionParams cp : connectionParamsList) {
            if (String.valueOf(cp.getId()).equals(paramId)) {
                comboBoxChangeSelection(cp);
                break;
            }
        }
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }
    
}
