/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.components;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public class RoundToggleButton extends ToggleButton {

    private Circle circle;
    private ChangeListener<Boolean> selectedItemListener;
    private boolean isGestureEnabled = true;
    
    public class RoundToggleDataHolder {
        private ChangeListener<Boolean> pingedListener;
        private Object miscelaneousAddition;
        private boolean cameSelected;
        
        public RoundToggleDataHolder(){ }
        
        public RoundToggleDataHolder(ChangeListener<Boolean> pingedListener, Object miscelaneousAddition) {
            this.pingedListener = pingedListener;
            this.miscelaneousAddition = miscelaneousAddition;
        }

        public RoundToggleDataHolder(ChangeListener<Boolean> pingedListener) {
            this.pingedListener = pingedListener;
        }

        public ChangeListener<Boolean> getPingedListener() {
            return pingedListener;
        }

        public void setPingedListener(ChangeListener<Boolean> pingedListener) {
            this.pingedListener = pingedListener;
        }

        public Object getMiscelaneousAddition() {
            return miscelaneousAddition;
        }

        public void setMiscelaneousAddition(Object miscelaneousAddition) {
            this.miscelaneousAddition = miscelaneousAddition;
        }        

        public boolean itCameSelected() {
            return cameSelected;
        }

        public void setCameSelected(boolean cameSelected) {
            this.cameSelected = cameSelected;
        }        
    }

    public RoundToggleButton() { init(); }

    public RoundToggleButton(String string) {
        super(string);
        init();
    }

    public RoundToggleButton(String string, Node node) {
        super(string, node);
        init();
    }
    
    private final void init(){
                getStyleClass().addAll("beauty-button", "settings-toggle");
        setPrefSize(ToggleButton.USE_COMPUTED_SIZE, ToggleButton.USE_COMPUTED_SIZE);
        circle = new Circle(8, Color.TOMATO);
        selectedItemListener = new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov,
                    Boolean t, Boolean t1) {
                setText(t1 ? " YES " : " NO ");
                circle.setFill(t1 ? Color.WHITE : Color.TOMATO);
                setContentDisplay(t1 ? ContentDisplay.RIGHT : ContentDisplay.LEFT);
            }
        };
        setGraphic(circle);
        selectedProperty().addListener(selectedItemListener);

        addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (!isGestureEnabled) {
                    t.consume();
                }
            }
        });
        
        addEventFilter(TouchEvent.TOUCH_PRESSED, new EventHandler<TouchEvent>() {
            @Override
            public void handle(TouchEvent t) {
                if (!isGestureEnabled) {
                    t.consume();
                }
            }
        });
        setUserData(new RoundToggleDataHolder());
    }

    public void toggleSelection() {
        setSelected(!isSelected());
    }

    public void shouldReceiveGestures(boolean flag) {
        isGestureEnabled = flag;
    }
    
    public RoundToggleDataHolder getToggleDataHolder(){
        return (RoundToggleDataHolder) getUserData();
    }

}
