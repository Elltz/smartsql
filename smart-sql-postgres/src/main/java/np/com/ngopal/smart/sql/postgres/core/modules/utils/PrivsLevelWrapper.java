/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class PrivsLevelWrapper {
    
    private String column;
    private String table;
    private String database;
    private double version;
    
    private ArrayList<String> privileges;
    
    private char subEntity = '*';

    
    public PrivsLevelWrapper(String table, String database, double vNum) {
        this(null, table, database, vNum, null);
    }

    public PrivsLevelWrapper(String table, String database, double vNum, ArrayList<String> privileges) {
        this(null, table, database, vNum, privileges);
    }

    public PrivsLevelWrapper(double vNum, ArrayList<String> privileges) {
        this(null, null, vNum, privileges);
    }
    
    public PrivsLevelWrapper(double vNum) {
        this(null, null, vNum);
    }
    
    public PrivsLevelWrapper(String item, double vNum) {
        String[] s = ((String) item).split("=");
        String sub = s[0];
        s = s[1].split("\\.");
        initiator(s[2], s[1], s[0], sub, vNum, null);
    }
    public PrivsLevelWrapper(String column, String table, String database, double vNum, List<String> priv) {
        this(column, table, database, null, vNum, priv);
    }
    
    public PrivsLevelWrapper(String column, String table, String database,String subEntity, double vNum, List<String> priv) {
        initiator(column, table, database, subEntity, vNum, priv);
    }
    
    private void initiator(String column, String table, String database,String subEntity, double vNum, List<String> privsList) {
        this.column = ((column != null && column.equals("*")) ? null : column);
        this.table = ((table != null && table.equals("*")) ? null : table);
        this.version = vNum;
        this.database = ((database != null && database.equals("*")) ? null : database);
        if(subEntity != null && !subEntity.isEmpty()){
            this.subEntity = subEntity.charAt(0);
        }
        
        privileges = new ArrayList();
        if (privsList != null) {
            if (privsList.contains(Flags.CONTEXT_PRIV_ADM__ALL)) {
                PrivsLevelWrapper tempWrapper = new PrivsLevelWrapper(table, database, vNum);
                tempWrapper.makeSystem();
                
                privileges.addAll(tempWrapper.getRawPrivileges(true));
                privileges.remove(Flags.CONTEXT_PRIV_ADM__ALL);
                
                if (!privsList.contains(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION)) {
                    privileges.remove(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
                }
            }else{
                privileges.addAll(privsList);
                
                /**
                 * MySQL returns USAGE privilege to stand for empty privileges at object level.
                 * 
                 * So we remove it from the list of privileges, if exist.
                 * After removal privileges list will be empty or have only GRANT OPTION.
                 */
                privileges.remove(Flags.CONTEXT_PRIV_ADM__USAGE);
            }
        }
    }

    
    public boolean isGlobalPrivilege(){
        return table == null && database == null;
    }
    /**
     * Checks whether this privilege is a database privilege 
     * @return 
     * returns true if its a database
     */
    public boolean isDatabasePrivilege(){
        //this is what it looked like initially, then i change it
        //database != null && table == null && subEntity == 'D';
        return database != null && table == null;
    }
    
    /**
     * Checks wheterh this privilege is a table privilege
     * @return 
     * returns true if its a database
     */
    public boolean isTablePrivilege(){
        return database != null && table != null && column == null;
    }
    
    /**
     * Checks whether this privilege is a column privilege
     * @return 
     * returns true if it is a column 
     */
    public boolean isColumnPrivilege(){
        return !isTablePrivilege() && column != null;
    }

   /**
    * Creates a copy of this privilege wrapper
    * @param addPrivs
    * Flag indicating whether to add the privileges of this object entity wrapper,
    * if true, adds privileges.
    * @return 
    * returns a copy-clone of this object
    */
    public PrivsLevelWrapper clone(boolean addPrivs) {
        System.gc();
        return new PrivsLevelWrapper(column, table, database, 
                new String(new char[]{subEntity}), version, (addPrivs ? getRawPrivileges(true) : null));
    }

    public String getColumn() {
        return column;
    }

    public String getTable() {
        return table;
    }

    public String getDatabase() {
        return database;
    }
    
    public ArrayList<String> getRawPrivileges(boolean withGrantOption) {
        ArrayList<String> ls = new ArrayList(privileges);
        if (withGrantOption) {
            return ls;
        } else {
            ls.remove(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            return ls;
        }
    }
     
    public ArrayList<String> getScriptablePrivileges(boolean withGrantOption) {
        ArrayList<String> ls = getRawPrivileges(withGrantOption);
        if (isSystem) { return ls; }

        PrivsLevelWrapper tempWrapper = clone(false);
        tempWrapper.makeSystem();
        
        if (!ls.contains(Flags.CONTEXT_PRIV_ADM__ALL) && tempWrapper.isSubSetPrivsOf(this)) {
            ls.clear();
            if (withGrantOption && privileges.contains(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION)) {
                ls.add(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            }
            ls.add(Flags.CONTEXT_PRIV_ADM__ALL);
        }
        
        if (ls.isEmpty() || (ls.size() == 1 && ls.contains(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION))) {
            ls.add(Flags.CONTEXT_PRIV_ADM__USAGE);
        }

        return ls;
    }
    
    public boolean hasGrantsOnPrivilegs(){
       return privileges.contains(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
    }
    
    public void emptyPrivs(){        
        //privileges.removeAll(getRawPrivileges(false));
        privileges.clear();
    }
    
    /**
     * The methods adds or removes privileges from the privilege holder of the object level priv
     * @param add_remove
     * flag indicating whether to add or remove
     * @param priv 
     * the privilege to add or remove from the list
     */
    public synchronized void modifyPrivilege(boolean add_remove, String priv){
        if(isSystem || priv == null){return ;}
        if(add_remove){            
            privileges.add(priv);
        }else{
            privileges.remove(priv);
        }
    }
    
    public void editPrivilege(boolean grant_revoke, String priv){
        editPrivilege(grant_revoke, priv, false);
    }
    
    public void editPrivilege(boolean grant_revoke, String priv, boolean removeIfContain) {
        boolean contains = false;
        for (String s : privileges) {
            if (s.trim().equals(priv.trim())) {
                contains = true;
                break;
            }
        }
        if (grant_revoke && !contains) {
            modifyPrivilege(true, priv);
        }else if (grant_revoke && removeIfContain) {
            modifyPrivilege(false, priv);
        } else if (!grant_revoke) {
            modifyPrivilege(!contains, priv);
        }
    }

    @Override
    public boolean equals(Object object) {
        PrivsLevelWrapper obj = null;
        if(object instanceof PrivsLevelWrapper){
            obj = (PrivsLevelWrapper) object;
        }else if(object instanceof String) {
            obj = new PrivsLevelWrapper((String)object, version);
        }else{
            return false;
        }
        return obj.isGlobalPrivilege() && isGlobalPrivilege() || (
                ((obj.isDatabasePrivilege() && isDatabasePrivilege()) && obj.getDatabase().equalsIgnoreCase(getDatabase())) ||
                ((obj.isTablePrivilege() && isTablePrivilege()) && obj.getTable().equalsIgnoreCase(getTable())) ||
                ((obj.isColumnPrivilege() && isColumnPrivilege()) && (obj.getColumn().equalsIgnoreCase(getColumn()) /*||
                obj.isSameSubEntity(subEntity)*/)));
    }
    /**
     * Checks to see whether pl has/ contains  all the privileges of this object
     * 
     * does take [ALL] or [ALL PRIVILEGES] into account 
     * 
     * @param pl
     * The parent PrivsLevelWrapper to check subset
     * @return 
     * returns Flags for equality or a subset of pl
     */
    public boolean isSubSetPrivsOf(PrivsLevelWrapper pl){
        List<String> plString = pl.getRawPrivileges(true);    
        List<String> pString = getRawPrivileges(true);
        if(pString.size() > plString.size()){ return false; }
        for (String s : pString) {
            if(!plString.contains(s)){ return false; }
        }
        return true;
    }
    
    public static PrivsLevelWrapper loopWrappersForLevelEquality(List<PrivsLevelWrapper> ls, PrivsLevelWrapper paramWrapper){
        for(PrivsLevelWrapper pl : ls){
            if(pl.equals(paramWrapper)){
                return pl;
            }
        }
        return null;
    }
    
    public static  PrivsLevelWrapper assembleFinalPrivsFromWrappers(PrivsLevelWrapper userPrivs, PrivsLevelWrapper userEditPrivs, PrivsLevelWrapper paramsWrapper, double version){
                
        PrivsLevelWrapper finalPrivsWrapper = (userPrivs == null) ? paramsWrapper.clone(false) : userPrivs.clone(true);

        if (userEditPrivs != null) {
            List<String> userEditPrivsList = userEditPrivs.getRawPrivileges(true);
            for (String prv : userEditPrivsList) {
                finalPrivsWrapper.editPrivilege(true, prv);
            }
        }
        
        //finalPrivsWrapper.modifyPrivilege(false, Flags.CONTEXT_PRIV_ADM__USAGE);//technically this is not suppose to trigger any effect
        System.gc();
        return finalPrivsWrapper;
    }
        
    public static PrivsLevelWrapper getDifferenceInPrivsWrapper(PrivsLevelWrapper subject, PrivsLevelWrapper predicate) {
        PrivsLevelWrapper diffPrivWrapper = null;
        if (subject != null) {
            ArrayList<String> subjectPrivs = subject.getRawPrivileges(true);
            if (predicate != null) {
                subjectPrivs.removeAll(predicate.getRawPrivileges(true));
            }
            if (subjectPrivs.size() > 0) {
                diffPrivWrapper = new PrivsLevelWrapper(subject.getTable(), subject.getDatabase(), subject.version, subjectPrivs);
            }
        }
        return diffPrivWrapper;
    }
    
    public static PrivsLevelWrapper[] getPrivsLevelKeyFromString(String input, double mysqlVersionNumber) {
        /**
         * The reason the starting offset is 6 is because of GRANT = 5 + 1 - the
         * whitespace character
         * 
         * The whitespace prefixing and postfixing is very crucial to parsing 
         * queries
         */
        String priv = input.substring(6, input.indexOf(" ON ")).trim();
        String privLevel = input.substring(input.indexOf(" ON ") + 3,
                input.indexOf(" TO ")).trim().replace("`", "");
        boolean hasGrantOption = input.contains("WITH ");
        String[] table_database = (privLevel.contains(".") ? privLevel.split("\\.") : privLevel.split("@"));
        String[] privArray = priv.split(",");
        ArrayList<PrivsLevelWrapper> results = new ArrayList();

        PrivsLevelWrapper origPlw = null;
        
        for (String s : privArray) {
            s = s.trim();
            if (s.contains("(")) { //is column
                PrivsLevelWrapper plw = null;
                String column = s.substring(s.indexOf('(')+1, s.indexOf(')')).trim();
                for (PrivsLevelWrapper item : results) {
                    if (item.getColumn() != null && item.getColumn().equals(column)) {
                        plw = item;
                        break;
                    }
                }

                if (plw == null) {
                    plw = new PrivsLevelWrapper(column, table_database[1], table_database[0], mysqlVersionNumber,
                            Arrays.asList(s.substring(0,s.indexOf('('))));
                     plw.modifyPrivilege(hasGrantOption, Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
                    results.add(plw);
                }else{
                    plw.modifyPrivilege(true, s);
                }                               
            } else {
                if (origPlw == null) {
                    origPlw = new PrivsLevelWrapper(null, table_database[1], table_database[0], mysqlVersionNumber,
                            Arrays.asList(s));
                     origPlw.modifyPrivilege(hasGrantOption, Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
                    results.add(origPlw);
                } else {
                    origPlw.modifyPrivilege(true, s);
                }
            }
        }
        
        //check for duplicate
        boolean hasDuplicate = false;
        int len = results.size();
        for (int i =0; i < len; i++) {
            PrivsLevelWrapper s = results.get(i);
            for (int k =0; k < len; k++) {
                PrivsLevelWrapper p = results.get(k);
                if(s.equals(p) && i != k){
                    hasDuplicate = true;
                }
            }
        }
        System.out.println("has Duplicate ? " + (hasDuplicate ? "YES" : "NO"));
        PrivsLevelWrapper[] plw = new PrivsLevelWrapper[results.size()];
        return results.toArray(plw);
    }
    
    public boolean isStoredProcedure(){
        return subEntity == 'S';
    }
    
    public boolean isFunction(){
        return subEntity == 'F';
    }
    
    public boolean isIndex(){
        return subEntity == 'I';
    }
    
    public boolean isTrigger(){
        return subEntity == 'T' && !isTablePrivilege();
    }
    
    public boolean isView(){
        return subEntity == 'V';
    }
    
    public boolean isSameSubEntity(char c){
        return c == subEntity;
    }
    
    private boolean isSystem = false;
    
    public void makeSystem() {
        if (isSystem) { return; }
        isSystem = true;
        privileges.clear();
        if (isGlobalPrivilege()) {
            privileges.add(Flags.CONTEXT_PRIV_ADM__ALL);
            privileges.add(Flags.CONTEXT_PRIV_TAB__ALTER);
            privileges.add(Flags.CONTEXT_PRIV_SDR__ALT_ROUT);
            privileges.add(Flags.CONTEXT_PRIV_DTI__CREATE);
            privileges.add(Flags.CONTEXT_PRIV_SDR__CREA_ROUT);
            if(version > 5.1){ privileges.add(Flags.CONTEXT_PRIV_ADM__CREA_TABLESPACE); }
            privileges.add(Flags.CONTEXT_PRIV_TAB__CREA_TEMP);
            privileges.add(Flags.CONTEXT_PRIV_ADM__CREA_USER);
            privileges.add(Flags.CONTEXT_PRIV_VIW__CREA_VIEW);
            privileges.add(Flags.CONTEXT_PRIV_TAB__DEL);
            privileges.add(Flags.CONTEXT_PRIV_DTV__DROP);
             if(version > 5.0) { privileges.add(Flags.CONTEXT_PRIV_DBS__EVENT); }
            privileges.add(Flags.CONTEXT_PRIV_SDR__EXECUTE);
            privileges.add(Flags.CONTEXT_PRIV_FAS__FILE);
            privileges.add(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            privileges.add(Flags.CONTEXT_PRIV_TAB__INDEX);
            privileges.add(Flags.CONTEXT_PRIV_TaC__INSERT);
            privileges.add(Flags.CONTEXT_PRIV_DBS__LOCK_TABLES);
            privileges.add(Flags.CONTEXT_PRIV_ADM__PROCESS);
            //privileges.add(Flags.CONTEXT_PRIV_ADM__PROXY);
             if(version > 5.6) { privileges.add(Flags.CONTEXT_PRIV_DaT__REF); }
            privileges.add(Flags.CONTEXT_PRIV_ADM__RELOAD);
            privileges.add(Flags.CONTEXT_PRIV_ADM__REPL_CLIENT);
            privileges.add(Flags.CONTEXT_PRIV_ADM__REPL_SLAVE);
            privileges.add(Flags.CONTEXT_PRIV_TaC__SELECT);
            privileges.add(Flags.CONTEXT_PRIV_ADM__SHOW_DATABASE);
            privileges.add(Flags.CONTEXT_PRIV_VIW__SHOW_VIEW);  
            privileges.add(Flags.CONTEXT_PRIV_ADM__SHUTDOWN);
            privileges.add(Flags.CONTEXT_PRIV_ADM__SUPER);
            if(version > 5.0) { privileges.add(Flags.CONTEXT_PRIV_TAB__TRIGGER); }
            privileges.add(Flags.CONTEXT_PRIV_TaC__UPDATE);  
            //privileges.add(Flags.CONTEXT_PRIV_ADM__USAGE);
        }else if(isDatabasePrivilege()){
            privileges.add(Flags.CONTEXT_PRIV_TAB__ALTER);
            privileges.add(Flags.CONTEXT_PRIV_SDR__ALT_ROUT);
            privileges.add(Flags.CONTEXT_PRIV_DTI__CREATE);
            privileges.add(Flags.CONTEXT_PRIV_SDR__CREA_ROUT);
            privileges.add(Flags.CONTEXT_PRIV_TAB__CREA_TEMP);
            privileges.add(Flags.CONTEXT_PRIV_VIW__CREA_VIEW);
            privileges.add(Flags.CONTEXT_PRIV_TAB__DEL);
            privileges.add(Flags.CONTEXT_PRIV_DTV__DROP);            
             if(version > 5.0) { privileges.add(Flags.CONTEXT_PRIV_DBS__EVENT); }
            privileges.add(Flags.CONTEXT_PRIV_SDR__EXECUTE);
            privileges.add(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            privileges.add(Flags.CONTEXT_PRIV_TAB__INDEX);
            privileges.add(Flags.CONTEXT_PRIV_TaC__INSERT);            
            privileges.add(Flags.CONTEXT_PRIV_DBS__LOCK_TABLES);
            privileges.add(Flags.CONTEXT_PRIV_DaT__REF);
            privileges.add(Flags.CONTEXT_PRIV_TaC__SELECT);
            privileges.add(Flags.CONTEXT_PRIV_VIW__SHOW_VIEW);
             if(version > 5.0) { privileges.add(Flags.CONTEXT_PRIV_TAB__TRIGGER); }           
            privileges.add(Flags.CONTEXT_PRIV_TaC__UPDATE);            
        }else if(isTablePrivilege()){
            privileges.add(Flags.CONTEXT_PRIV_TAB__ALTER);
            privileges.add(Flags.CONTEXT_PRIV_DTI__CREATE);
            privileges.add(Flags.CONTEXT_PRIV_TAB__CREA_TEMP);
            privileges.add(Flags.CONTEXT_PRIV_TAB__DEL);
            privileges.add(Flags.CONTEXT_PRIV_DTV__DROP);
            privileges.add(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);
            privileges.add(Flags.CONTEXT_PRIV_TAB__INDEX);
            privileges.add(Flags.CONTEXT_PRIV_TaC__INSERT);
            privileges.add(Flags.CONTEXT_PRIV_DaT__REF);
            privileges.add(Flags.CONTEXT_PRIV_TaC__SELECT);            
             if(version > 5.0) { privileges.add(Flags.CONTEXT_PRIV_TAB__TRIGGER); }
            privileges.add(Flags.CONTEXT_PRIV_TaC__UPDATE);
        }else if(isView()){
            privileges.add(Flags.CONTEXT_PRIV_VIW__CREA_VIEW);privileges.add(Flags.CONTEXT_PRIV_DTV__DROP);
            privileges.add(Flags.CONTEXT_PRIV_VIW__SHOW_VIEW);
        }else if(isFunction()){
            privileges.add(Flags.CONTEXT_PRIV_FAS__FILE);
        }else if(isStoredProcedure()){
             privileges.add(Flags.CONTEXT_PRIV_SDR__ALT_ROUT);
             privileges.add(Flags.CONTEXT_PRIV_SDR__CREA_ROUT);
             if(version > 5.0) { privileges.add(Flags.CONTEXT_PRIV_DBS__EVENT); }
             privileges.add(Flags.CONTEXT_PRIV_SDR__EXECUTE);
             privileges.add(Flags.CONTEXT_PRIV_DTS__GRANT_OPTION);                        
        }else if(isTrigger()){ }else if(isIndex()){ }else if(isColumnPrivilege()){
            privileges.add(Flags.CONTEXT_PRIV_TaC__INSERT);privileges.add(Flags.CONTEXT_PRIV_TaC__SELECT);
            privileges.add(Flags.CONTEXT_PRIV_TaC__UPDATE);
        }else{
            throw new UnsupportedOperationException("Could not detect what type of privlege object");
        }
    }
    
    public static PrivsLevelWrapper findSubset(PrivsLevelWrapper first, PrivsLevelWrapper second) {
        if (first.isGlobalPrivilege() && !second.isGlobalPrivilege()) {
            return first;
        } else if (second.isGlobalPrivilege() && !first.isGlobalPrivilege()) {
            return second;
        } else if (second.isGlobalPrivilege() && first.isGlobalPrivilege()) {
            return null;
        } else if (first.isDatabasePrivilege() && !second.isDatabasePrivilege()) {
            return first;
        } else if (second.isDatabasePrivilege() && !first.isDatabasePrivilege()) {
            return second;
        } else if (first.isDatabasePrivilege() && second.isDatabasePrivilege()) {
            return null;
        } else if (first.isTablePrivilege() && !second.isTablePrivilege()) {
            return first;
        } else if (second.isTablePrivilege() && !first.isTablePrivilege()) {
            return second;
        } else if (first.isTablePrivilege() && second.isTablePrivilege()) {
            return null;
        } else if (first.isView() && !second.isView()) {
            return first;
        } else if (second.isView() && !first.isView()) {
            return second;
        } else if (first.isColumnPrivilege()) {
            return second;
        } else if (second.isColumnPrivilege()) {
            return first;
        } else {
            return null;
        }
    }

    public String convertPrivsLevelToSqlStandard(){
        if(isColumnPrivilege()){
            return "*.*";
        }else if(isDatabasePrivilege()){
            return getDatabase();
        }else if(isTablePrivilege() || isView()){
            return getDatabase() + "."+ getTable();
        }else if(isColumnPrivilege()){
            return getDatabase() + "."+ getTable() + "(" + getColumn() +")";
        }else if(isFunction() || isIndex() || isStoredProcedure() || isTrigger()){
            
        }
        return "";
    }
}
