package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.utils.ComboBoxAutoFilterListener;
import np.com.ngopal.smart.sql.core.modules.utils.UsedFilePathsUtil;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import np.com.ngopal.smart.sql.utils.ImportUtils;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ImportTableController extends BaseController implements Initializable {
    
    private final ObservableList<RDBMSDriver> rdbmsDrivers = FXCollections.observableArrayList();
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private RadioButton csvOption;
    @FXML
    private RadioButton htmlOption;
    @FXML
    private RadioButton excelOption;
    @FXML
    private RadioButton sqlOption;
    @FXML
    private RadioButton rdbmsOption;
    @FXML
    private RadioButton xmlOption;
    @FXML
    private RadioButton delimiterOption;
    
    @FXML
    private TitledPane csvOptionPane;
    @FXML
    private TextField fieldsTerminated;
    @FXML
    private TextField fieldsEnclosed;
    @FXML
    private CheckBox enclosedOptionally;
    @FXML
    private TextField fieldsEscaped;
    @FXML
    private TextField lineTerminated;
    
    @FXML
    private TitledPane rdbmsOptionPane;
    @FXML
    private ComboBox<RDBMSDriver> driversCombobox;
    @FXML
    private TextField urlField;
    @FXML
    private TextField tableField;
    @FXML
    private TextField userField;
    @FXML
    private PasswordField passwordField;
    
    @FXML 
    private ComboBox<String> importFileField;
    @FXML
    private ComboBox<Database> databasesCombobox;
    @FXML
    private ComboBox<DBTable> tablesCombobox;
    
    @FXML
    private Button importButton;
    @FXML
    private AnchorPane mainPane;
    @FXML
    private ProgressBar progressBar;
    
    @FXML
    private ImageView titleImageView;
    
    private ConnectionSession session;
    
    private ComboBoxAutoFilterListener<String> autoFilterListener;
    
    private volatile boolean needStop = false;
    
    public void init(ConnectionSession session, DBTable table) {
        
        this.session = session;
        
        databasesCombobox.setItems(FXCollections.observableArrayList(session.getConnectionParam().getDatabases()));
        databasesCombobox.getSelectionModel().select(session.getConnectionParam().getSelectedDatabase());
        
        tablesCombobox.getSelectionModel().select(table);
        
        importFileField.getItems().setAll(UsedFilePathsUtil.loadFilePaths(UsedFilePathsUtil.TYPE__IMPORT_TABLE));
        importFileField.getSelectionModel().selectLast();
        
        autoFilterListener = new ComboBoxAutoFilterListener<>(importFileField, importFileField.getItems());
        importFileField.getEditor().textProperty().addListener(autoFilterListener);
    }
    
    @FXML
    public void chooseImportFile(ActionEvent event) {        
        FileChooser fileChooser = new FileChooser();
        
        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            importFileField.getEditor().setText(file.getAbsolutePath());
        }
    }
    
    
    @FXML
    public void closeAction(ActionEvent event) {
        needStop = true;
        getStage().close();
    }
    
    @FXML
    public void importAction(ActionEvent event) {

        mainPane.setDisable(true);
        importButton.setDisable(true);
        
        Thread th = new Thread(() -> {
            String importFilePath = importFileField.getEditor().getText();
            if (!rdbmsOption.isSelected()) {
                if (importFilePath == null || importFilePath.isEmpty()) {
                    DialogHelper.showInfo(getController(), "Info", "Please select import file!", null, getStage());
                    mainPane.setDisable(false);
                    importButton.setDisable(false);
                    return;
                }
            }

            final Database database = databasesCombobox.getSelectionModel().getSelectedItem();
            if (database == null) {
                DialogHelper.showInfo(getController(), "Info", "Please select database for import!", null, getStage());
                mainPane.setDisable(false);
                importButton.setDisable(false);
                return;
            }

            final DBTable table = tablesCombobox.getSelectionModel().getSelectedItem();
            if (!sqlOption.isSelected() && table == null) {
                DialogHelper.showInfo(getController(), "Info", "Please select table for import!", null, getStage());
                mainPane.setDisable(false);
                importButton.setDisable(false);
                return;
            }

            if (!importFileField.getItems().contains(importFilePath)) {

                autoFilterListener.addSourcePath(importFilePath);

                UsedFilePathsUtil.addFilePaths(UsedFilePathsUtil.TYPE__IMPORT_TABLE, importFilePath);
                importFileField.getItems().add(importFilePath);
            }

            ImportUtils.ImportStatusCallback importCallback = new ImportUtils.ImportStatusCallback() {
                
                @Override
                public boolean isNeedStop() {
                    return needStop;
                }
                
                
                @Override
                public void progress(double progress) {
                    Platform.runLater(() -> {
                        progressBar.setProgress(progress);
                    });
                }
                                
                @Override
                public void error(String errorMessage, Throwable th) {
                    Platform.runLater(() -> {

                        DialogHelper.showError(getController(), "Error", errorMessage, th);

                        if (excelOption.isSelected()) {

                            DialogResponce responce = DialogHelper.showConfirm(getController(), "Template excel file", "Would you like to receive excel template file for the selected table?");
                            if (responce == DialogResponce.OK_YES) {

                                // show file chooser for saving template
                                FileChooser fileChooser = new FileChooser();
                                fileChooser.getExtensionFilters().addAll(
                                    new FileChooser.ExtensionFilter("XLS Files", "*.xls"));

                                File file = fileChooser.showSaveDialog(getStage());
                                if (file != null) {
                                    ExportUtils.exportXLSTemplate(session.getService(), file.getAbsolutePath(), database.getName(), table.getName(), new ExportUtils.ExportStatusCallback() {
                                        @Override
                                        public void error(String errorMessage, Throwable th) {
                                            DialogHelper.showError(getController(), "Error", errorMessage, th);
                                        }

                                        @Override
                                        public void success() {
                                            DialogHelper.showInfo(getController(), "Info", "Excel template successfully generated!", null, getStage());
                                        }
                                    });
                                }
                            }
                        }
                        
                        mainPane.setDisable(false);
                        importButton.setDisable(false);
                        progressBar.setProgress(1);
                    });
                }

                @Override
                public void success(QueryResult queryResult, Map<String, Object> map) {
                    Platform.runLater(() -> {
                        
                        progressBar.setProgress(1);
                        
                        DialogHelper.showInfo(getController(), "Info", "Import successfully!", null, getStage());
                        mainPane.setDisable(false);
                        importButton.setDisable(false);
                    });
                }
            };

            if (csvOption.isSelected() || delimiterOption.isSelected()) {

                ImportUtils.importDelimiter(
                    session.getService(), 
                    importFilePath, 
                    database.getName(), table.getName(),
                    null, null, null,
                    fieldsTerminated.getText(), fieldsEnclosed.getText(), enclosedOptionally.isSelected(), fieldsEscaped.getText(), lineTerminated.getText(), null, null, importCallback);

            } else if (htmlOption.isSelected()) {

                ImportUtils.importHTML(
                        session.getService(), 
                        importFilePath, 
                        database.getName(), table.getName(), importCallback);

            } else if (excelOption.isSelected()) {

                ImportUtils.importXLS(
                    session.getService(), 
                    importFilePath, 
                    database.getName(), table.getName(), importCallback);

            } else if (sqlOption.isSelected()) {

                ImportUtils.importSQL(
                    session.getService(), 
                    importFilePath, 
                    database.getName(), importCallback);

            } else if (rdbmsOption.isSelected()) {

                RDBMSDriver driver = driversCombobox.getSelectionModel().getSelectedItem();
                if (driver == null) {
                    DialogHelper.showInfo(getController(), "Info", "Please select driver for rdbms import!", null, getStage());
                    return;
                }

                String rdbmsTable = tableField.getText();
                if (rdbmsTable == null || rdbmsTable.isEmpty()) {
                    DialogHelper.showInfo(getController(), "Info", "Input table name for rdbms import!", null, getStage());
                    return;
                }

                String rdbmsUrl = urlField.getText();
                if (rdbmsUrl == null || rdbmsUrl.isEmpty()) {
                    DialogHelper.showInfo(getController(), "Info", "Input url for rdbms import!", null, getStage());
                    return;
                }

                String rdbmsUser = userField.getText();
                if (rdbmsUser == null || rdbmsUser.isEmpty()) {
                    DialogHelper.showInfo(getController(), "Info", "Input user name for rdbms import!", null, getStage());
                    return;
                }

                String rdbmsPassword = passwordField.getText();
                if (rdbmsPassword == null || rdbmsPassword.isEmpty()) {
                    DialogHelper.showInfo(getController(), "Info", "Input user password for rdbms import!", null, getStage());
                    return;
                }

                ImportUtils.importOtherRDBMS(
                    session.getService(), 
                    driver.className,
                    rdbmsUrl, rdbmsUser, rdbmsPassword, rdbmsTable,                     
                    database.getName(), table.getName(), importCallback);

            } else if (xmlOption.isSelected()) {

                ImportUtils.importXML(
                    session.getService(), 
                    importFilePath, 
                    database.getName(), table.getName(), importCallback);

            }            
        });
        th.setDaemon(true);
        th.start();
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        // bind change listeners
        csvOptionPane.disableProperty().bind(csvOption.selectedProperty().or(delimiterOption.selectedProperty()).not());
        rdbmsOptionPane.disableProperty().bind(rdbmsOption.selectedProperty().not());        
        importFileField.disableProperty().bind(rdbmsOption.selectedProperty());
        
        importFileField.getEditor().textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue != null) {
                if (newValue.endsWith(".csv")) {
                    csvOption.setSelected(true);
                } else if (newValue.endsWith(".htm") || newValue.endsWith(".html")) {
                    htmlOption.setSelected(true);
                } else if (newValue.endsWith(".xls") || newValue.endsWith(".xlsx")) {
                    excelOption.setSelected(true);
                } else if (newValue.endsWith(".sql")) {
                    sqlOption.setSelected(true);
                } else if (newValue.endsWith(".xml")) {
                    xmlOption.setSelected(true);
                } else {
                    delimiterOption.setSelected(true);
                }
            }
        });
        
        
        databasesCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
            if (newValue != null) {
                List<DBTable> tables = DatabaseCache.getInstance().getTables(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), newValue.getName());
                if (tables != null) {
                    tablesCombobox.setItems(FXCollections.observableArrayList(tables));
                }
            } else {
                tablesCombobox.getItems().clear();
            }
        });
        
        driversCombobox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends RDBMSDriver> observable, RDBMSDriver oldValue, RDBMSDriver newValue) -> {
            
            urlField.clear();
            tableField.clear();
            userField.clear();
            passwordField.clear();
            
            if (newValue != null) {
                urlField.setText(newValue.urlTemplate);
            }
        });
        
        rdbmsDrivers.add(new RDBMSDriver("Access", "net.ucanaccess.jdbc.UcanaccessDriver", "jdbc:ucanaccess://<database>"));
        rdbmsDrivers.add(new RDBMSDriver("MySQL", "com.mysql.jdbc.Driver", "jdbc:mysql://<host>:<port3306>/<database>"));        
        rdbmsDrivers.add(new RDBMSDriver("Oracle", "oracle.jdbc.driver.OracleDriver", " jdbc:oracle:thin:@<host>:<port1521>:<database>"));
        rdbmsDrivers.add(new RDBMSDriver("PostgreSQL", "org.postgresql.Driver", "jdbc:postgresql://<host>:<port5432>/<database>"));
        
        driversCombobox.setItems(rdbmsDrivers);
        
        titleImageView.setImage(SmartImageProvider.getInstance().getTableExport_image());
    }
    
    
    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
    
    // TODO Maybe collect drivers using reflections
    private class RDBMSDriver {
        
        private final String name;
        private final String className;
        private final String urlTemplate;
        
        public RDBMSDriver(String name, String className, String urlTemplate) {
            this.name = name;
            this.className = className;
            this.urlTemplate = urlTemplate;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
}
