/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.modules;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.structure.modules.ProfilerModule;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.controller.MainController;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */
@Slf4j
public class SmartProfilerModule extends ProfilerModule {

    @Override
    public synchronized boolean canOpenConnection(ConnectionParams params) {
        // we must denid open connection for the same address
//        if (params != null) {
//            for (ConnectionParams p: openedParams) {
//                if (p.getAddress() != null && p.getAddress().equals(params.getAddress())) {
//                    if (getCurrentProfilerController() != null && getCurrentProfilerController().isProfilingMysqlDatabase() && getCurrentProfilerController().isProfilingOsMetrics()) {
//                        Platform.runLater(() -> {
//                            showError("Error", "Connections already opened", null);
//                        });
//                        return false;
//                    } else {
//                        break;
//                    }
//                }
//            }
//        }

        // Task: We shold not open profielr for two DB servers
        return openedParams.isEmpty();
    }

    @Override
    public boolean install() {
        super.install();
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FILE, new MenuItem[]{newItem, openItem, saveItem, saveAsItem, exportItem, exitItem, monitoringStatusItem});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{startItem, stopItem});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.PROFILER, new MenuItem[]{settingsItem, clearCachedDataItem});

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{startItem, stopItem, exportItem, scrollBackwardItem,
            scrollForwardItem, chartEditItem, zoomAlt, /*zoomText, zoomIn, zoomOut,*/
            tTime, timeElapsedItem, monitoringStatusItem, debuggerItem, slowQueryAnalyzer, tunerItem});

        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.RIGHT_MENU, new MenuItem[]{zoomOut, zoomText, zoomIn, selectTime});

        //activateHideMenuItemsWhenDisabled();
        setChartService(((MainController) getController()).getChartService());
        setColumnCardinalityService(((MainController) getController()).getColumnCardinalityService());
        setDeadlockService(((MainController) getController()).getDeadlockService());
        setPerformanceTuningService(((MainController) getController()).getPerformanceTuningService());
        setPreferenceDataService(((MainController) getController()).getPreferenceService());
        setProfilerService(((MainController) getController()).getProfilerService());
        setDbProfilerService(((MainController) getController()).getSecondProfilerService());
        setRecommendationService(((MainController) getController()).getRecommendationService());
        setSettingsService(((MainController) getController()).getSettingsService());
        setParamService(((MainController) getController()).getDb());
        setDebuggerProfilerService(((MainController) getController()).getDebuggerProfilerService());
        setUsageService(((MainController) getController()).getUsageService());
        
        return true;
    }

    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {
        log.info("post connection called in profilerModule on THREAD " + Thread.currentThread().getName());
        for (ConnectionSession cs : profilerControllers.keySet()) {
            if (cs.getConnectionParam().getAddress().equals(service.getConnectionParam().getAddress())) {
                DialogHelper.showError(getController(), "Host busy", "Profiler host '" + service.getConnectionParam().getAddress() + "' already used", null);
                return;
            }
        }
        super.postConnection(service, tab);
    }

    /**
     *
     * Since this class does not directly extends AbstractStandardModule class,
     * i need put functions & methods in AbstractStandardModule class here.
     *
     * TODO i need refractor & put AbstractStandardModule class somewhere
     * central, so i not need do this
     */
    protected void registrateServiceChangeListener() {
        serviceListener.changed(null, false, true);

        getController().getSelectedConnectionSession().addListener((ObservableValue<? extends ConnectionSession> observable, ConnectionSession oldValue, ConnectionSession newValue) -> {

            if (oldValue != null) {
                ((AbstractDBService) oldValue.getService()).busy().removeListener(serviceListener);
            }

            if (newValue != null) {
                ((AbstractDBService) newValue.getService()).busy().addListener(serviceListener);

                boolean busy = ((AbstractDBService) newValue.getService()).isServiceBusy();
                serviceListener.changed(null, !busy, busy);
            }
        });
    }

    public void connectToConnection(ConnectionParams cp) {
        try {
            MysqlDBService service = ((MainController)getController()).getInjector().getInstance(MysqlDBService.class);
            service.setServiceBusy(true);
            ((MainController)getController()).openConnection(service, cp, true, (ConnectionSession t) -> {
                getController().saveSessions();
            });
        } catch (Exception e) {
            exception = e;
        }
    }

    @Override
    public void connectionFailedOpening(String error) { }

    @Override
    public void connectionOpened(ConnectionParams params) {
        super.connectionOpened(params);
        if(((MainController) getController()).getDatabaseComboBox() != null){
            setDatabaseSelectedItemProperty(((MainController) getController()).getDatabaseComboBox()
                    .getSelectionModel().selectedItemProperty());
        }
    }    
}
