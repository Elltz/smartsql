package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaDesignerImportTablesController  extends BaseController implements Initializable {
    
    @FXML
    private ComboBox<Database> databasesComboBox;
    @FXML
    private CheckBox selectAllCheckBox;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Button cancelButton;
    
    @FXML
    private Button add;
    
    @FXML
    private ListView<Holder> listView;
    
    private List<String> tables = new ArrayList<>();
    
    private class Holder{
        String table;
        SimpleBooleanProperty selected = new SimpleBooleanProperty(false);
        
        public Holder(String table) {
            this.table = table;
        }
    }
    
    public String getSelectedDatabase() {
        return databasesComboBox.getSelectionModel().getSelectedItem() != null ? databasesComboBox.getSelectionModel().getSelectedItem().getName() : null;
    }
    
    public List<String> getImportTables() {
        return tables;
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        add.setOnAction((ActionEvent t) -> {
            for (Holder h: listView.getItems()) {
                if (h.selected.get()) {
                    tables.add(h.table);
                }
            }
            dialog.hide();
        });
        
        cancelButton.setOnAction((ActionEvent t) -> {
            tables.clear();
            dialog.hide();
        });
        
        listView.setCellFactory((ListView<Holder> p) -> new CheckBoxListCell<>((Holder p1) -> p1.selected,new StringConverter<Holder>() {
             @Override
             public String toString(Holder t) {
                 return t.table;
             }
             
             @Override
             public Holder fromString(String string) {
                 return null;
             }
         }));
         
        selectAllCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {            
            for (Holder h: listView.getItems()) {
                h.selected.setValue(t1);
            }
        });
    }
    
    Stage dialog;
    public void init(DBService dbService, Collection<Database> list, String selected) {
        
        databasesComboBox.setItems(FXCollections.observableArrayList(list));
        
        databasesComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Database>() {
            @Override
            public void changed(ObservableValue<? extends Database> observable, Database oldValue, Database newValue) {
                
                listView.getItems().clear();
                selectAllCheckBox.setSelected(false);
                
                if (newValue != null) {
                    try {
                        List<String> list = dbService.getTablesNames(newValue);
                        for (String t: list) {        
                            listView.getItems().add(new Holder(t));
                        }
                    } catch (SQLException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                }
            }
        });
        
        if (selected != null) {
            for (Database database : list) {
                if (database.getName().equals(selected)) {
                    databasesComboBox.getSelectionModel().select(database);
                }
            }
        }
        
        if (dialog == null) {
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setResizable(false);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getStage());
            dialog.setTitle("Import Tables");
            dialog.setScene(new Scene(getUI()));
        }
        
        tables.clear();
        dialog.showAndWait();
    }
    
}
