package np.com.ngopal.smart.sql.ui.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.filechooser.FileSystemView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaSynchronizerComparingController extends BaseController implements Initializable {
    
    private SyncCompareResult compareResult;
    

    private Stage dialog;
    
    @FXML
    private AnchorPane mainUI;
    
  
    @FXML
    private Label targetAdressLabel;
    @FXML
    private Label targetDatabaseLabel;
    @FXML
    private Label sourceAdressLabel;
    @FXML
    private Label sourceDatabaseLabel;

    
    @FXML
    private RadioButton openInEditorRadioButton;
    @FXML
    private RadioButton saveToFileRadioButton;
    @FXML
    private RadioButton executeScriptRadioButton;
    
    @FXML
    private TextField saveToFileField;
    @FXML
    private Button saveToFileButton;
    @FXML
    private CheckBox savetoFIleOpenInEditorCheckBox;
    
    @FXML
    private CheckBox executeScriptRefreshCheckBox;


    private SynchronizationDiffComponent diffComponent;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    
    public void show(SynchronizationDiffComponent diffComponent, SyncCompareResult compareResult) {
        
        this.diffComponent = diffComponent;
        
        sourceAdressLabel.setText(compareResult.getSourceHost());
        sourceDatabaseLabel.setText(compareResult.getSourceDatabase().getName());
        
        targetAdressLabel.setText(compareResult.getTargetHost());
        targetDatabaseLabel.setText(compareResult.getTargetDatabase().getName());
        
        dialog.showAndWait();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
        checkFieldsDisable();
        
        openInEditorRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                checkFieldsDisable();
            }
        });
        
        saveToFileRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                checkFieldsDisable();
            }
        });
        
        executeScriptRadioButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue != null && newValue) {
                checkFieldsDisable();
            }
        });
        
        saveToFileField.setText(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + File.separatorChar + "smart-sql-synchronization.sql");
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(getStage());
        dialog.setResizable(false);
        dialog.setScene(new Scene(mainUI));
        dialog.setTitle("Schema Synchronization");
    }
    
    @FXML
    public void saveToFileAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("SQL Files", "*.sql"));

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            saveToFileField.setText(file.getAbsolutePath());
        }
    }
    
    @FXML
    public void synchronizeAction(ActionEvent event) {
        
        if (openInEditorRadioButton.isSelected()) {
            String script = diffComponent.generateScript();
            try {
                File tempFile = File.createTempFile("smart-sql-synchronization-", ".sql");
                Files.write(tempFile.toPath(), script.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                Desktop.getDesktop().open(tempFile);
                
                dialog.close();
            } catch (IOException ex) {
                ((MainController)getController()).showError("Error", ex.getMessage(), ex, dialog);
            }
            
        } else if (saveToFileRadioButton.isSelected()) {
            String script = diffComponent.generateScript();
            
            String fileName = saveToFileField.getText().trim();
            if (!fileName.isEmpty()) {
                if (fileName.endsWith(".sql")) {
                    fileName += ".sql";
                }
                
                try {
                    File file = new File(fileName);
                    if (file.exists()) {
                        if (((MainController)getController()).showConfirm("File alreay exist", "File already exist, do you want to replace it?", getStage(), false) != DialogResponce.OK_YES) {
                            return;
                        }
                    }
                    Files.write(file.toPath(), script.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                    if (savetoFIleOpenInEditorCheckBox.isSelected()) {
                        Desktop.getDesktop().open(file);
                    }
                    
                    dialog.close();
                } catch (IOException ex) {
                    ((MainController)getController()).showError("Error", ex.getMessage(), ex, dialog);
                }
            } else {
                ((MainController)getController()).showInfo("Select file", "Please select output file", null, dialog);
            }
            
        } else {
            diffComponent.importInTarget(executeScriptRefreshCheckBox.isSelected());
            dialog.close();
        }
    }
    
    @FXML
    public void closeAction(ActionEvent event) {
        dialog.close();
    }
    
    
    private void checkFieldsDisable() {
        saveToFileField.setDisable(!saveToFileRadioButton.isSelected());
        saveToFileButton.setDisable(!saveToFileRadioButton.isSelected());
        savetoFIleOpenInEditorCheckBox.setDisable(!saveToFileRadioButton.isSelected());
        
        executeScriptRefreshCheckBox.setDisable(!executeScriptRadioButton.isSelected());
    }

}
