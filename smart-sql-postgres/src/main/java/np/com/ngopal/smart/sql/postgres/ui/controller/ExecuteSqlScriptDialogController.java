/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.ui.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com);
 */
@Slf4j
public class ExecuteSqlScriptDialogController extends BaseController implements Initializable {

    DBService dbService;
    
    @FXML
    private ImageView titleImageView;
    
    @FXML 
    private Button executeButton;
    @FXML 
    private Button cancelButton;
    
    @FXML
    private TextField scriptField;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private CheckBox abortOnError;
    
    @FXML
    private Label querySizeLabel;
    @FXML
    private Label statusLabel;
    @FXML
    private Label dataSizeLabel;
    
    @FXML
    private Label databaseLabel;
    
    @FXML
    private AnchorPane mainUI;
    
    private volatile boolean needStop = false;
    private volatile boolean scriptRunned = false;
    
    @FXML
    void cancelAction(ActionEvent event) {
        getStage().close();
    }
    
    public void setDbService(DBService dbService) {
        this.dbService = dbService;
        
        Database database = ((MysqlDBService)dbService).getDatabase();
        databaseLabel.setText("Current database: " + (database != null ? database.getName() : "<not selected>"));
    }

    @FXML
    void executeScript(ActionEvent event) {
        if(scriptField.getText().isEmpty()){
            DialogHelper.showInfo(getController(), "Path Locator", "Please enter a filename",null);
            return;
        }
        
        if (scriptRunned) {
            needStop = true;
        } else {
            
            Database database = ((MysqlDBService)dbService).getDatabase();
            String msg = "The current database context is `" + (database != null ? database.getName() : "<not selected>") + "`. SQL " +
                         "statements in the file will be executed here, unless one or " + 
                         "more USE statement(s) specifying another database context is " +
                         "in the file. Do you want to contimue?";
            
            DialogResponce responce = DialogHelper.showConfirm(getController(), "SmartMySQL", msg);
            if (responce != DialogResponce.OK_YES && responce != DialogResponce.YES_TO_ALL) {
                return;
            }
            
            scriptRunned = true;
            needStop = false;
            executeButton.setText("Stop");
            Thread th = new Thread(() -> 
            {
                try {

                    long totalSize = Files.size(new File(scriptField.getText()).toPath());

                    ScriptRunner runner = new ScriptRunner(dbService.getDB(), abortOnError.isSelected());
                    ScriptRunner.ScriptRunnerCallback callBack = new ScriptRunner.ScriptRunnerCallback() {

                        private int queriesSize = 0;
                        private int queryIndex = 0;

                        private boolean hasErrors = false;

                        @Override
                        public boolean isNeedStop() {
                            return needStop;
                        }
                        
                        @Override
                        public void queryExecuting(Connection con, QueryResult queryResult) {
                        }


                        @Override
                        public void started(Connection con, int queriesSize) throws SQLException {
                            con.setAutoCommit(false);
                            Platform.runLater(() -> {
                                this.queriesSize = queriesSize;
                                this.queryIndex = 0;

                                progressBar.setProgress(0);
                                statusLabel.setText("");
                                querySizeLabel.setText("");
                                dataSizeLabel.setText("Data size: " + totalSize / 1024 + " KB");
                            });
                        }

                        @Override
                        public void queryExecuted(Connection con, QueryResult queryResult) {
                            Platform.runLater(() -> {
                                queryIndex++;
                                progressBar.setProgress((double)queryIndex / (double)queriesSize);

                                querySizeLabel.setText("Query(s) Executed: " + queryIndex);
                            });
                        }

                        @Override
                        public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {
                            con.rollback();
                            Platform.runLater(() -> {
                                statusLabel.setText("Import has some errors");
                                hasErrors = true;
                            });
                        }
                        
                        @Override
                        public boolean isWithLimit(QueryResult queryResult) {
                            return false;
                        }

                        @Override
                        public void finished(Connection con) throws SQLException {
                            con.commit();
                            Platform.runLater(() -> {
                                if (!hasErrors) {
                                    statusLabel.setText(isNeedStop() ? "Import stopped" : "Import successful");
                                }
                                cancelButton.setText("Done");
                                executeButton.setText("Execute");

                                scriptRunned = false;
                            });
                        }
                    };

                    makeBusy(true);
                    try (FileInputStream fstream = new FileInputStream(scriptField.getText())) {
                        runner.runScript(new BufferedReader(new InputStreamReader(fstream)), callBack, false);
                    }
                } catch (Throwable ex) {
                    DialogHelper.showError(getController(), "Error", "Error executing script", ex, null, getStage());
                } finally {
                    makeBusy(false);
                }
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    @FXML
    void chooseScript(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("SQL files", "*.sql"),
            new FileChooser.ExtensionFilter("All files", "*.*"));
        
        File file = fileChooser.showOpenDialog(getStage());
        if (file != null) {
            scriptField.setText(file.getAbsolutePath());
        }
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        titleImageView.setImage(StandardDefaultImageProvider.getInstance().getExecuteQuery_image());
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
