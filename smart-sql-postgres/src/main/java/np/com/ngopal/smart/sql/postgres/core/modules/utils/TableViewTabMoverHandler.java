package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class TableViewTabMoverHandler implements EventHandler<KeyEvent> {

    private final TableView<?> tableView;
    private int startColumn = 0;
    private boolean edit = false;
    
    public TableViewTabMoverHandler(TableView<?> tableView, int startColumn) {
        this(tableView, startColumn, false);
    }
    
    public TableViewTabMoverHandler(TableView<?> tableView, int startColumn, boolean edit) {
        this.tableView = tableView;
        this.startColumn = startColumn;
        this.edit = edit;
    }
    
    
    @Override
    public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.TAB) {
                
            List<TablePosition> cells = tableView.getSelectionModel().getSelectedCells();
            if (cells != null && !cells.isEmpty()) {

                int row = tableView.getSelectionModel().getSelectedIndex();
                int columnsSize = 0;
                for (TableColumn tc: tableView.getColumns()) {
                    if (tc.isVisible()) {
                        columnsSize++;
                    }
                }

                int cell = cells.get(0).getColumn();
                if (event.isShiftDown()) {
                    cell--;
                } else {
                    cell++;
                }

                TableColumn tc = tableView.getColumns().get(startColumn);
                if (cell < startColumn) {

                    if (row > 0) {
                        row--;

                        cell = columnsSize - 1;
                        tc = tableView.getColumns().get(cell);
                        while (cell > 0 && !tc.isVisible()) {
                            cell--;
                            tc = tableView.getColumns().get(cell);
                        }
                    }
                } else if (cell >= columnsSize){

                    if (row < columnsSize - 1) {
                        row++;
                        cell = startColumn;

                        tc = tableView.getColumns().get(cell);
                        while (cell > 0 && !tc.isVisible()) {
                            cell++;
                            tc = tableView.getColumns().get(cell);
                        }
                    }
                } else {

                    tc = tableView.getColumns().get(cell);
                    while (cell > 0 && !tc.isVisible()) {
                        cell++;
                        tc = tableView.getColumns().get(cell);
                    }
                }

                tableView.getSelectionModel().select(row, tc);

                event.consume();
            }
        } else if (edit && !event.getText().isEmpty()) {
            
            List<TablePosition> cells = tableView.getSelectionModel().getSelectedCells();
            if (cells != null && !cells.isEmpty()) {
                
                int row = tableView.getSelectionModel().getSelectedIndex();
                int cell = cells.get(0).getColumn();
                
                TableColumn tc = tableView.getColumns().get(cell);
                
                tableView.edit(row, tc);
            }
            
        }
    }

}
