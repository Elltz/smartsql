package np.com.ngopal.smart.sql.ui.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.ReportsContract;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportRamConfigurationController extends BaseController implements Initializable, ReportsContract {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private Label keyBufferSizeLabel;
    @FXML
    private Label queryCacheSizeLabel;
    @FXML
    private Label tmpTableSizeLabel;
    @FXML
    private Label innodbBufferPoolSizeLabel;
    @FXML
    private Label innodbAdditionalMemPoolSizeLabel;
    @FXML
    private Label innodbLogBufferSizeLabel;
    @FXML
    private Label maxConnectionsLabel;
    @FXML
    private Label sortBufferSizeLabel;
    @FXML
    private Label readBufferSizeLabel;
    @FXML
    private Label readRndBufferSizeLabel;
    @FXML
    private Label joinBufferSizeLabel;
    @FXML
    private Label threadStackLabel;
    @FXML
    private Label binlogCacheSizeLabel;
    @FXML
    private Label totalLabel;
    @FXML
    private Button exportButton;
    @FXML
    private Button refreshButton;
    
    @FXML
    private Label keyBufferSizeLabel1;
    @FXML
    private Label queryCacheSizeLabel1;
    @FXML
    private Label tmpTableSizeLabel1;
    @FXML
    private Label innodbBufferPoolSizeLabel1;
    @FXML
    private Label innodbAdditionalMemPoolSizeLabel1;
    @FXML
    private Label innodbLogBufferSizeLabel1;
    @FXML
    private Label maxConnectionsLabel1;
    @FXML
    private Label sortBufferSizeLabel1;
    @FXML
    private Label readBufferSizeLabel1;
    @FXML
    private Label readRndBufferSizeLabel1;
    @FXML
    private Label joinBufferSizeLabel1;
    @FXML
    private Label threadStackLabel1;
    @FXML
    private Label binlogCacheSizeLabel1;
    @FXML
    private Label totalLabel1;
    
    @FXML
    private Label defKeyBufferSizeLabel;
    @FXML
    private Label defQueryCacheSizeLabel;
    @FXML
    private Label defTmpTableSizeLabel;
    @FXML
    private Label defInnodbBufferPoolSizeLabel;
    @FXML
    private Label defInnodbAdditionalMemPoolSizeLabel;
    @FXML
    private Label defInnodbLogBufferSizeLabel;
    @FXML
    private Label defMaxConnectionsLabel;
    @FXML
    private Label defSortBufferSizeLabel;
    @FXML
    private Label defReadBufferSizeLabel;
    @FXML
    private Label defReadRndBufferSizeLabel;
    @FXML
    private Label defJoinBufferSizeLabel;
    @FXML
    private Label defThreadStackLabel;
    @FXML
    private Label defBinlogCacheSizeLabel;
    @FXML
    private Label defTotalLabel;
    
    
    private final Map<String, Integer> DEFAULTS = new HashMap<>();
    
    private ConnectionSession session;
    
    
    @FXML
    public void refreshAction(ActionEvent event) {
        try (ResultSet rs = (ResultSet) session.getService().execute(validateQuery(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_CONFIGURATION_0)))) {
            if (rs != null && rs.next()) {
                keyBufferSizeLabel.setText(convertToMB(rs.getDouble("@@key_buffer_size")));
                queryCacheSizeLabel.setText(convertToMB(getColumnOnFailSafe(rs, "@@query_cache_size")));
                tmpTableSizeLabel.setText(convertToMB(rs.getDouble("@@tmp_table_size")));
                innodbBufferPoolSizeLabel.setText(convertToMB(rs.getDouble("@@innodb_buffer_pool_size")));
                innodbLogBufferSizeLabel.setText(convertToMB(rs.getDouble("@@innodb_log_buffer_size")));
                maxConnectionsLabel.setText(rs.getString("@@max_connections"));
                sortBufferSizeLabel.setText(convertToMB(rs.getDouble("@@sort_buffer_size")));
                readBufferSizeLabel.setText(convertToMB(rs.getDouble("@@read_buffer_size")));
                readRndBufferSizeLabel.setText(convertToMB(rs.getDouble("@@read_rnd_buffer_size")));
                joinBufferSizeLabel.setText(convertToMB(rs.getDouble("@@join_buffer_size")));
                threadStackLabel.setText(convertToMB(rs.getDouble("@@thread_stack")));
                binlogCacheSizeLabel.setText(convertToMB(rs.getDouble("@@binlog_cache_size")));
                
                keyBufferSizeLabel1.setText(convertToMB(rs.getDouble("@@key_buffer_size")));
                queryCacheSizeLabel1.setText(convertToMB(getColumnOnFailSafe(rs, "@@query_cache_size")));
                tmpTableSizeLabel1.setText(convertToMB(rs.getDouble("@@tmp_table_size")));
                innodbBufferPoolSizeLabel1.setText(convertToMB(rs.getDouble("@@innodb_buffer_pool_size")));
                innodbLogBufferSizeLabel1.setText(convertToMB(rs.getDouble("@@innodb_log_buffer_size")));
                maxConnectionsLabel1.setText(rs.getString("@@max_connections"));
                sortBufferSizeLabel1.setText(convertToMB(rs.getDouble("@@sort_buffer_size")));
                readBufferSizeLabel1.setText(convertToMB(rs.getDouble("@@read_buffer_size")));
                readRndBufferSizeLabel1.setText(convertToMB(rs.getDouble("@@read_rnd_buffer_size")));
                joinBufferSizeLabel1.setText(convertToMB(rs.getDouble("@@join_buffer_size")));
                threadStackLabel1.setText(convertToMB(rs.getDouble("@@thread_stack")));
                binlogCacheSizeLabel1.setText(convertToMB(rs.getDouble("@@binlog_cache_size")));
                
            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), null);
        }
        
        try (ResultSet rs = (ResultSet) session.getService().execute("show GLOBAL status like 'Connections'")) {
            if (rs != null && rs.next()) {
                maxConnectionsLabel1.setText(rs.getString(2));
            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), null);
        }
        
        try (ResultSet rs = (ResultSet) session.getService().execute(String.format(validateQuery(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_CONFIGURATION_1)), maxConnectionsLabel.getText()))) {
            if (rs != null && rs.next()) {
                totalLabel.setText(new BigDecimal(rs.getDouble(1)).setScale(2, RoundingMode.HALF_UP) + " GB");
            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), null);
        }
        
        try (ResultSet rs = (ResultSet) session.getService().execute(String.format(validateQuery(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_RAM_CONFIGURATION_1)), maxConnectionsLabel1.getText()))) {
            if (rs != null && rs.next()) {
                totalLabel1.setText(new BigDecimal(rs.getDouble(1)).setScale(2, RoundingMode.HALF_UP) + " GB");
            }
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), null);
        }
    }
    
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Thread th = new Thread(() -> {
                
                HSSFWorkbook wb = new HSSFWorkbook();
            
                HSSFSheet sheet = wb.createSheet("Ram configuration");
                sheet.setColumnWidth(1, 35 * 256);
                sheet.setColumnWidth(2, 18 * 256);
                sheet.setColumnWidth(3, 18 * 256);
                sheet.setColumnWidth(4, 18 * 256);

                HSSFPalette palette = wb.getCustomPalette();
                
                HSSFFont defaultFont = wb.createFont();
                defaultFont.setFontHeightInPoints((short)10);
                defaultFont.setFontName("Arial");
                
                HSSFFont headertFont = wb.createFont();
                headertFont.setFontHeightInPoints((short)10);
                headertFont.setFontName("Arial");
                headertFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                
                HSSFFont footerFont = wb.createFont();
                footerFont.setFontHeightInPoints((short)18);
                footerFont.setFontName("Arial");
                footerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

                CellStyle headerStyle0 = wb.createCellStyle();
                headerStyle0.setFont(headertFont);
                CellStyle headerStyle1 = wb.createCellStyle();
                headerStyle1.setFont(headertFont);
                CellStyle headerStyle2 = wb.createCellStyle();
                headerStyle2.setFont(headertFont);
                
                CellStyle style0 = wb.createCellStyle();
                style0.setFont(defaultFont);
                try {
                    HSSFColor hssfColor = palette.findColor((byte)0xDE, (byte)0xDE, (byte)0xDE); 
                    if (hssfColor == null ) {
                        palette.setColorAtIndex(HSSFColor.LAVENDER.index, (byte)0xDE, (byte)0xDE, (byte)0xDE);
                        hssfColor = palette.getColor(HSSFColor.LAVENDER.index);
                        style0.setFillForegroundColor(hssfColor.getIndex());
                        style0.setFillPattern(CellStyle.SOLID_FOREGROUND);
                    }
                } catch (Exception e) {
                }
                
                CellStyle style1 = wb.createCellStyle();
                style1.setFont(defaultFont);
                style1.setAlignment(HorizontalAlignment.RIGHT);
                try {
                    HSSFColor hssfColor = palette.findColor((byte)0xEF, (byte)0xEF, (byte)0xEF); 
                    if (hssfColor == null ) {
                        palette.setColorAtIndex(HSSFColor.AQUA.index, (byte)0xEF, (byte)0xEF, (byte)0xEF);
                        hssfColor = palette.getColor(HSSFColor.AQUA.index);
                        style1.setFillForegroundColor(hssfColor.getIndex());
                        style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
                    }
                } catch (Exception e) {
                }
                
                CellStyle style2 = wb.createCellStyle();
                style2.setFont(defaultFont);
                style2.setAlignment(HorizontalAlignment.RIGHT);
                try {
                    HSSFColor hssfColor = palette.findColor((byte)0xF7, (byte)0xAE, (byte)0x65); 
                    if (hssfColor == null ) {
                        palette.setColorAtIndex(HSSFColor.CORAL.index, (byte)0xF7, (byte)0xAE, (byte)0x65);
                        hssfColor = palette.getColor(HSSFColor.CORAL.index);
                        style2.setFillForegroundColor(hssfColor.getIndex());
                        style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
                    }
                } catch (Exception e) {
                }
                
                CellStyle footerStyle = wb.createCellStyle();
                footerStyle.setFont(footerFont);
                footerStyle.setAlignment(HorizontalAlignment.CENTER);
                
                // header 
                HSSFRow row = sheet.createRow(1);
                
                HSSFCell cell = row.createCell(1);
                cell.setCellValue("Parameter");
                cell.setCellStyle(headerStyle0);
                
                cell = row.createCell(2);
                cell.setCellValue("MySQL Default");
                cell.setCellStyle(headerStyle1);
                
                cell = row.createCell(3);
                cell.setCellValue("RAM configured");
                cell.setCellStyle(headerStyle2);
                
                cell = row.createCell(4);
                cell.setCellValue("Current RAM Allocation");
                cell.setCellStyle(headerStyle2);
                
                
                row = sheet.createRow(2);
                
                cell = row.createCell(1);
                cell.setCellValue("     key_buffer_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defKeyBufferSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(keyBufferSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(keyBufferSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(3);
                
                cell = row.createCell(1);
                cell.setCellValue("+  query_cache_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defQueryCacheSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(queryCacheSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(queryCacheSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(4);
                
                cell = row.createCell(1);
                cell.setCellValue("+  tmp_table_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defTmpTableSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(tmpTableSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(tmpTableSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(5);
                
                cell = row.createCell(1);
                cell.setCellValue("+  innodb_buffer_pool_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defInnodbBufferPoolSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(innodbBufferPoolSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(innodbBufferPoolSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(6);
                
                cell = row.createCell(1);
                cell.setCellValue("+  innodb_additional_mem_pool_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defInnodbAdditionalMemPoolSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(innodbAdditionalMemPoolSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(innodbAdditionalMemPoolSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(7);
                
                cell = row.createCell(1);
                cell.setCellValue("+  innodb_log_buffer_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defInnodbLogBufferSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(innodbLogBufferSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(innodbLogBufferSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(8);
                
                cell = row.createCell(1);
                cell.setCellValue("+  max_connections");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defMaxConnectionsLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(maxConnectionsLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(maxConnectionsLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(9);
                
                cell = row.createCell(1);
                cell.setCellValue("     ×");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue("");
                cell.setCellStyle(style0);
                
                cell = row.createCell(3);
                cell.setCellValue("");
                cell.setCellStyle(style0);
                
                cell = row.createCell(4);
                cell.setCellValue("");
                cell.setCellStyle(style0);
                
                
                row = sheet.createRow(10);
                
                cell = row.createCell(1);
                cell.setCellValue("         sort_buffer_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defSortBufferSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(sortBufferSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(sortBufferSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(11);
                
                cell = row.createCell(1);
                cell.setCellValue("     +  read_buffer_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defReadBufferSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(readBufferSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(readBufferSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(12);
                
                cell = row.createCell(1);
                cell.setCellValue("     +  read_rnd_buffer_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defReadRndBufferSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(readRndBufferSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(readRndBufferSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(13);
                
                cell = row.createCell(1);
                cell.setCellValue("     +  join_buffer_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defJoinBufferSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(joinBufferSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(joinBufferSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(14);
                
                cell = row.createCell(1);
                cell.setCellValue("     +  thread_stack");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defThreadStackLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(threadStackLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(threadStackLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(15);
                
                cell = row.createCell(1);
                cell.setCellValue("     +  binlog_cache_size");
                cell.setCellStyle(style0);
                
                cell = row.createCell(2);
                cell.setCellValue(defBinlogCacheSizeLabel.getText());
                cell.setCellStyle(style1);
                
                cell = row.createCell(3);
                cell.setCellValue(binlogCacheSizeLabel.getText());
                cell.setCellStyle(style2);
                
                cell = row.createCell(4);
                cell.setCellValue(binlogCacheSizeLabel1.getText());
                cell.setCellStyle(style2);
                
                
                row = sheet.createRow(16);
                
                cell = row.createCell(1);
                cell.setCellValue("Totals:");
                cell.setCellStyle(footerStyle);
                
                cell = row.createCell(2);
                cell.setCellValue(defTotalLabel.getText());
                cell.setCellStyle(footerStyle);
                
                cell = row.createCell(3);
                cell.setCellValue(totalLabel.getText());
                cell.setCellStyle(footerStyle);
                
                cell = row.createCell(4);
                cell.setCellValue(totalLabel1.getText());
                cell.setCellStyle(footerStyle);
                
                
                try {            
                    ByteArrayOutputStream bo = new ByteArrayOutputStream();
                    wb.write(bo);
                    Files.write(file.toPath(), bo.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                    
                    Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                } catch (Throwable ex) {
                    Platform.runLater(() -> ((MainController)getController()).showError("Error", ex.getMessage(), ex, getStage()));
                }
                
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;       
        
        String version = session.getService().getDbSQLVersion();
        if (version.startsWith("5.5")) {
            version = "_5_5";
        } else if (version.startsWith("5.6")) {
            version = "_5_6";
        } else {
            version = "_5_7";
        }
        
        defKeyBufferSizeLabel.setText(convertToMB(DEFAULTS.get("key_buffer_size" + version).doubleValue()));
        defQueryCacheSizeLabel.setText(convertToMB(DEFAULTS.get("query_cache_size" + version).doubleValue()));
        defTmpTableSizeLabel.setText(convertToMB(DEFAULTS.get("key_buffer_size" + version).doubleValue()));
        defInnodbBufferPoolSizeLabel.setText(convertToMB(DEFAULTS.get("innodb_buffer_pool_size" + version).doubleValue()));
        defInnodbLogBufferSizeLabel.setText(convertToMB(DEFAULTS.get("innodb_log_buffer_size" + version).doubleValue()));
        defMaxConnectionsLabel.setText(DEFAULTS.get("max_connections" + version).toString());
        defSortBufferSizeLabel.setText(convertToMB(DEFAULTS.get("sort_buffer_size" + version).doubleValue()));
        defReadBufferSizeLabel.setText(convertToMB(DEFAULTS.get("read_buffer_size" + version).doubleValue()));
        defReadRndBufferSizeLabel.setText(convertToMB(DEFAULTS.get("read_rnd_buffer_size" + version).doubleValue()));
        defJoinBufferSizeLabel.setText(convertToMB(DEFAULTS.get("join_buffer_size" + version).doubleValue()));
        defThreadStackLabel.setText(convertToMB(DEFAULTS.get("key_buffer_size" + version).doubleValue()));
        defBinlogCacheSizeLabel.setText(convertToMB(DEFAULTS.get("binlog_cache_size" + version).doubleValue()));
        defTotalLabel.setText(convertToGB(DEFAULTS.get("total" + version).doubleValue()));
        
        refreshAction(null);
    }
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        DEFAULTS.put("key_buffer_size_5_5", 8388608);
        DEFAULTS.put("query_cache_size_5_5", 0);
        DEFAULTS.put("innodb_buffer_pool_size_5_5", 134217728);
        DEFAULTS.put("innodb_log_buffer_size_5_5", 134217728);
        DEFAULTS.put("max_connections_5_5", 151);
        DEFAULTS.put("read_buffer_size_5_5", 131072);
        DEFAULTS.put("read_rnd_buffer_size_5_5", 262144);
        DEFAULTS.put("sort_buffer_size_5_5", 2097144);
        DEFAULTS.put("join_buffer_size_5_5", 131072);
        DEFAULTS.put("binlog_cache_size_5_5", 32768);
        DEFAULTS.put("thread_stack_5_5", 262144);
        DEFAULTS.put("tmp_table_size_5_5", 16777216);
        DEFAULTS.put("total_5_5", (8388608 + 0 + 16777216 + 134217728 + 134217728 + 151 * (2097144 + 131072 + 262144 + 131072 + 262144 + 32768)));
        
        DEFAULTS.put("key_buffer_size_5_6", 8388608);
        DEFAULTS.put("query_cache_size_5_6", 1048576);
        DEFAULTS.put("innodb_buffer_pool_size_5_6", 134217728);
        DEFAULTS.put("innodb_log_buffer_size_5_6", 134217728);
        DEFAULTS.put("max_connections_5_6", 151);
        DEFAULTS.put("read_buffer_size_5_6", 131072);
        DEFAULTS.put("read_rnd_buffer_size_5_6", 262144);
        DEFAULTS.put("sort_buffer_size_5_6", 2097144);
        DEFAULTS.put("join_buffer_size_5_6", 131072);
        DEFAULTS.put("binlog_cache_size_5_6", 32768);
        DEFAULTS.put("thread_stack_5_6", 262144);
        DEFAULTS.put("tmp_table_size_5_6", 262144);
        DEFAULTS.put("total_5_6", (8388608 + 1048576 + 262144 + 134217728 + 134217728 + 151 * (2097144 + 131072 + 262144 + 131072 + 262144 + 32768)));
        
        DEFAULTS.put("key_buffer_size_5_7", 8388608);
        DEFAULTS.put("query_cache_size_5_7", 1048576);
        DEFAULTS.put("innodb_buffer_pool_size_5_7", 134217728);
        DEFAULTS.put("innodb_log_buffer_size_5_7", 134217728);
        DEFAULTS.put("max_connections_5_7", 151);
        DEFAULTS.put("read_buffer_size_5_7", 131072);
        DEFAULTS.put("read_rnd_buffer_size_5_7", 262144);
        DEFAULTS.put("sort_buffer_size_5_7", 262144);
        DEFAULTS.put("join_buffer_size_5_7", 262144);
        DEFAULTS.put("binlog_cache_size_5_7", 32768);
        DEFAULTS.put("thread_stack_5_7", 262144);
        DEFAULTS.put("tmp_table_size_5_7", 262144);
        DEFAULTS.put("total_5_7", (8388608 + 1048576 + 262144 + 134217728 + 134217728 + 151 * (262144 + 131072 + 262144 + 262144 + 262144 + 32768)));
    }
    
    private Boolean keepQueryCacheSize = null;
    
    private String validateQuery(String query) {
        if (keepQueryCacheSize == null) {
            try (ResultSet rs = (ResultSet) session.getService().execute("SHOW VARIABLES like 'query_cache_size';")) {
                keepQueryCacheSize = rs != null && rs.next();
            } catch (SQLException ex) { ex.printStackTrace(); }
        }
        
        if (keepQueryCacheSize != null && !keepQueryCacheSize) {
            if (query.charAt(7) == '@') {
                query = query.replace(" @@query_cache_size,", "");
            } else {
                query = query.replace(" @@query_cache_size +", "");
            }
        }
        
        return query;
    }
    
    private double getColumnOnFailSafe(ResultSet rs, String column){
        double val = 0.0;
        try{
            val = rs.getDouble(column);
        }catch(Exception e){ e.printStackTrace(); }
        
        return val;
    }
    
}
