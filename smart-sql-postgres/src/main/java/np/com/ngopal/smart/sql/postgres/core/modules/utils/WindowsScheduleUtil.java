
package np.com.ngopal.smart.sql.core.modules.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.Schedule;

/** 
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class WindowsScheduleUtil {
    
    // TODO Change this
    private static final String SSJ_PATH = ".." + File.separatorChar + "ssj" + File.separatorChar + "ssj-1.0.jar";
    
    public static final void createWindowsScheduleJob(Schedule params, String configPath) throws IOException, InterruptedException {
        
        // delete old task
        removeWindowsScheduleJob(params);
        
        // after create new
        List<String> createCommands = new ArrayList<>();
        createCommands.add("schtasks.exe");
        createCommands.add("/CREATE");
        createCommands.add("/TN");
        createCommands.add("\"" + params.getName() + "\"");
        createCommands.add("/TR");
        createCommands.add("\"javaw -jar \'" + new File(SSJ_PATH).getAbsolutePath() + "\' \'" + configPath + "\'\"");
        createCommands.add("/SC");
        
        switch (RunFrequency.toEnum(params.getRunFrequency())) {
            case DAILY:
                createCommands.add("DAILY");
                createCommands.add("/ST");
                createCommands.add(String.format("%02d:%02d:00", params.getFrequencyHour(), params.getFrequencyMinutes()));
                break;
                
            case WEEKLY:
                createCommands.add("WEEKLY");
                createCommands.add("/D");
                
                String scheduleWeekly = "";
                String weekly = params.getFrequencyWeekly();
                if (weekly != null && !weekly.isEmpty()) {
                   for (String w: weekly.split(":"))
                   {
                       scheduleWeekly += WeeklyValue.toEnum(Integer.valueOf(w)).getScheduleTaskName() + ", ";
                   }
                }
                if (!scheduleWeekly.isEmpty()) {
                    createCommands.add(scheduleWeekly.substring(0, scheduleWeekly.length() - 2));
                }
                createCommands.add("/ST");
                createCommands.add(String.format("%02d:%02d:00", params.getFrequencyHour(), params.getFrequencyMinutes()));
                break;
                
            default:
                createCommands.add("ONCE");
        }

        Process p = new ProcessBuilder(createCommands).start();
        System.out.println("result: " + p.waitFor());
        System.out.println(String.join(" ", createCommands));
    }
    
    public static final void removeWindowsScheduleJob(Schedule params) throws IOException, InterruptedException {
        
        // delete old task
        List<String> deleteCommands = new ArrayList<>();
        deleteCommands.add("schtasks.exe");
        deleteCommands.add("/DELETE");
        deleteCommands.add("/TN");
        deleteCommands.add("\"" + params.getName() + "\"");
        deleteCommands.add("/F");

        Process deleteProcess = new ProcessBuilder(deleteCommands).start();
        deleteProcess.waitFor();

        System.out.println(String.join(" ", deleteCommands));
    }
    
    
    public static final void main(String[] args) throws IOException, InterruptedException {
        
        // schtasks /create /tn "HowToTask" /tr c:\temp\test.cmd /sc once /st 00:00:00 /sd 2022/01/01 /ru username /rp password

        List<String> commands = new ArrayList<>();

        commands.add("schtasks.exe");
        commands.add("/CREATE");
        commands.add("/TN");
        commands.add("\"WindowsScheduleTest\"");
        commands.add("/TR");
        commands.add("\"javaw -jar G:\\work\\smart-sql\\smart-sql-ui\\target\\smart-sql-ui-1.0-jar-with-dependencies.jar\"");
        commands.add("/SC");
        commands.add("once");
        commands.add("/ST");
        commands.add("20:51:00");
        commands.add("/SD");
        commands.add("26/10/2015");

        ProcessBuilder builder = new ProcessBuilder(commands);
        Process p = builder.start();
        p.waitFor();
        System.out.println(String.join(" ", commands) + ": " + p.exitValue()); // 0 : OK
    }
}
