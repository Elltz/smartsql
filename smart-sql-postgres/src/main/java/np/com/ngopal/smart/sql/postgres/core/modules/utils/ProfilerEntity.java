///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package np.com.ngopal.smart.sql.core.modules.utils;
//
//import java.sql.SQLException;
//import java.util.LinkedHashMap;
//import java.util.List;
//import javafx.beans.property.ObjectProperty;
//import javafx.beans.property.ReadOnlyBooleanProperty;
//import javafx.beans.property.ReadOnlyObjectProperty;
//import javafx.collections.ObservableList;
//import javafx.scene.control.Tab;
//import javafx.scene.control.TreeItem;
//import np.com.ngopal.smart.sql.core.modules.model.ProfilerData;
//import np.com.ngopal.smart.sql.model.ConnectionParams;
//import np.com.ngopal.smart.sql.model.OsServerSpecs;
//import np.com.ngopal.smart.sql.ui.controller.ProfilerTabContentController;
//
///**
// *
// * @author Terah Laweh(rumorsapp@gmail.com);
// */
//public interface ProfilerEntity {
//    
//    void showUserCharts();
//    ReadOnlyBooleanProperty profilerStarted();
//    void showSettings();
//    void zoomInAction();
//    void clearDraggedData();
//    void zoomDragedAction(Long start, Long finish);
//    void zoomOutAction();
//    void showAllAction();
//    void updateCharts();
//    void startProfiler();
//    void clearProfilerData();
//    void stopProfiler();
//    boolean isValid();
//    void destroy();
//    void promptRemainingMetric();
//    void createConnection() throws SQLException ;
//    ConnectionParams getConnectionParams();
//    ObjectProperty<TreeItem> getSelectedTreeItem();
//    ReadOnlyObjectProperty<Tab> getSelectedTab();
//    void finalizeConnectionCreation();
//    OsServerSpecs getCurrentServerSpecs();
//    
//    public void saveProfillerData(ProfilerData pd);
//    public void openData(ProfilerData data);
//    
//    public void exportToXls(
//            List<String> sheets, 
//            List<ObservableList<ObservableList>> totalData,
//            List<LinkedHashMap<String, Boolean>> totalColumns,
//            List<List<Integer>> totalColumnWidths,
//            List<byte[]> images,
//            List<Integer> heights,
//            List<Integer> colspans);
//    
//    public ProfilerTabContentController getParentController();
//}
