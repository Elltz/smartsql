/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Setter;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.queryutils.QueryAreaWrapper;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
public class GoToDialogController extends BaseController implements Initializable {
    
    @Setter
    private QueryAreaWrapper codeArea;
    
    @FXML
    private Button goToButton;
    
    @FXML
    private Button cancelButton;

    @FXML
    private TextField lineNumberField;
    
    @FXML
    private AnchorPane mainUI;
        
    @FXML
    public void goToLineAction(ActionEvent event) {
        
        String lineNumber = lineNumberField.getText();
        try {            
            Integer number = Integer.valueOf(lineNumber);
            codeArea.moveToRow(number);                
            getStage().close();
        } catch (Throwable th) {
            DialogHelper.showInfo(getController(), "Info", "Enter valid line number!", null);
        }
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
