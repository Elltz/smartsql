package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReverseEngineerDialogController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private ComboBox<Database> databaseComboBox;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label statusFieldLabel;
    @FXML
    private Button importButton;
    
    private volatile boolean importRunned = false;
    private volatile boolean stopImport = false;
    
    private DBService service;
    private SchemaDesignerTabContentController designerController;
    
    public void init(DBService service, SchemaDesignerTabContentController designerController, String selectedDatabase) {
        this.service = service;
        this.designerController = designerController;
        
        databaseComboBox.getItems().clear();
        progressBar.setProgress(0);
        
        importButton.setText("Import");
        importButton.setDisable(true);
        
        stopImport = false;
        importRunned = false;
        
        try {    
            databaseComboBox.getItems().setAll(service.getDatabases());
            databaseComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
                importButton.setDisable(newValue == null);
            });
        } catch (SQLException ex) {
            applyError("Error on loading databases list");
            log.error("Error on loading databases list", ex);
        }
        
        if (selectedDatabase != null) {
            for (Database db: databaseComboBox.getItems()) {
                if (selectedDatabase.equals(db.getName())) {
                    databaseComboBox.getSelectionModel().select(db);
                    break;
                }
            }
                
            importAction(null);
        }
    }
    
    
    private void reverseEngeen() {
        
        Database database = databaseComboBox.getSelectionModel().getSelectedItem();
        
        // 1. Load all tables
        applyStatus("Reading data", 0.1);
        List<DBTable> tables = new ArrayList<>();
        try {            
            List<String> list = service.getTablesNames(database);
            double step = 0.7 / list.size();
            
            // return if need
            if (stopImport) {
                return;
            }
            
            for (String t: list) {
                tables.add(service.getTableSchemaInformation(database.getName(), t));
                
                applyStatus("Reading data", progressBar.getProgress() + step);
                
                // return if need
                if (stopImport) {
                    return;
                }
            }
        } catch (SQLException ex) {
            applyError("Error loading tables list");
            log.error("Error loading tables list", ex);
            return;
        }
        
        // return if need
        if (stopImport) {
            return;
        }
            
        // 2. Convert db table to designer table
        Map<String, DesignerTable> designerTablesMap = new HashMap<>();
        List<DesignerTable> designerTables = new ArrayList<>();
        
        double step = 0.2 / tables.size();
        for (DBTable t: tables) {
            try {
                DesignerTable dt = SchemaDesignerModule.convertDBTableToDesignerTableWithoutRelations(t, database.getName());
                designerTables.add(dt);
                
                applyStatus("Reading data", progressBar.getProgress() + step);
                
                designerTablesMap.put(dt.name().get(), dt);
            } catch (SQLException ex) {
                applyError("Error covnerting table '" + t.getName() + "'");
                log.error("Error covnerting table '" + t.getName() + "'", ex);
                return;
            }
            
            // return if need
            if (stopImport) {
                return;
            }
        }

        applyStatus("Show data in designer", progressBar.getProgress() + 0.01);        
        
        // load relations
        for (DBTable t: tables) {
            SchemaDesignerModule.loadTableRelations(t, designerTablesMap);
        }
        
        // 3. Show data in designer
        Platform.runLater(() -> {
            designerController.importTables(designerTables);
            
            designerController.getCurrentDiagram().database().set(database.getName());
            
            applyStatus("Done", 1.0);
            
            stopImport = true;
            importButton.setDisable(false);
        });
    }
    
    
    
    private void applyStatus(String statusText, double progress) {
        Platform.runLater(() -> {
            statusFieldLabel.setText(statusText);
            statusFieldLabel.setStyle("-fx-text-fill: black");
            progressBar.setProgress(progress);
        });
    }
    
    private void applyError(String errorText) {
        Platform.runLater(() -> {
            statusFieldLabel.setText(errorText);
            statusFieldLabel.setStyle("-fx-text-fill: red");
        });
        
        importRunned = false;
        stopImport = true;
    }
    
    @FXML
    public synchronized void importAction(ActionEvent event) {
        if (importRunned) {
            if (stopImport) {
                cancelAction(event);
            }
        } else {
            importRunned = true;
            stopImport = false;
            
            databaseComboBox.setDisable(true);
                    
            importButton.setText("Done");
            importButton.setDisable(true);
            progressBar.setProgress(-1);
            
            
            applyStatus("Starting", 0.1);
            Thread th = new Thread(() -> reverseEngeen());
            th.setDaemon(true);
            th.start();
        }
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        stopImport = true;
        getStage().close();
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
