/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.queryutils.QueryAreaWrapper;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.MainUI;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class ConfigurableQueryBinderController extends BaseController implements Initializable {
    
    @FXML
    private TextField filepathTextField;
    
    @FXML
    private Button openFileButton;
    
    @FXML
    private CheckBox activateCompletionCheckBox;
    
    @FXML
    private ColorPicker fontColorPicker;
    
    @FXML
    private ColorPicker backgroundColorPicker;
    
    @FXML
    private ColorPicker selectionColorPicker;
    
    @FXML
    private Slider fontSlider;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private CheckBox tablesCheckbox;
    
    @FXML
    private CheckBox useCacheForQACheckbox;
    
    @FXML
    private CheckBox databaseCheckbox;
    
    @FXML
    private CheckBox variableCheckbox;
    
    @FXML
    private CheckBox storedprcCheckbox;
    
    @FXML
    private CheckBox historyCheckbox;
    
    @FXML
    private CheckBox favoritesCheckbox;
    
    @FXML
    private CheckBox feqCheckbox;
    
    @FXML
    private CheckBox ColumnsCheckbox;
    
    @FXML
    private TextField dictionarypathTextfield;
    
    @FXML
    private Button openUserDictionaryButton;
    
    @FXML
    private CheckBox notifyUserWhenTabUnsavedChangesExit;
    
    @FXML
    private CheckBox pasteObjectNameOnDoubleClick;
    
    @FXML
    private CheckBox autoRebuildTags;
    
    @FXML
    private CheckBox enableAnimationCheckbox;
    
    @FXML
    private CheckBox expandAnimCheckbx;
    
    @FXML
    private CheckBox flipAnimCheckbox;
    
    @FXML
    private Button exitButton;
    
    private  Stage s;
    
    private SimpleBooleanProperty activateAutoComplete,autoRebuildTagsOnStartup,activateVariablesCompletion,
            activateStoreProc_FunctionCompletion,activateTablesCompletion,activateDatabaseCompletion,
            pasteObjectOnDoubleClick,notifyUnsavedTabChanges,enableAnimation,activateColumnsCompletion,
            menuButton_flipAnim,menuButton_expndAnim, activateHistoryCompletion, activateFavoritesCompletion, activateFEQCompletion;
    private SimpleStringProperty fontColor, backgroundColor,queryAssociateFile,
            selectionColor;
    private SimpleDoubleProperty fontSize;
    
    public void showConfigurableQueryBindingDialog() {        
        s.showAndWait();
    }

    public ConfigurableQueryBinderController() {
        activateAutoComplete = new SimpleBooleanProperty();
        this.fontColor = new SimpleStringProperty();
        this.backgroundColor = new SimpleStringProperty();
        this.selectionColor = new SimpleStringProperty();
        this.fontSize = new SimpleDoubleProperty();
        queryAssociateFile = new SimpleStringProperty();
        autoRebuildTagsOnStartup = new SimpleBooleanProperty();
        activateColumnsCompletion = new SimpleBooleanProperty();
        activateDatabaseCompletion = new SimpleBooleanProperty();
        activateStoreProc_FunctionCompletion = new SimpleBooleanProperty();
        activateHistoryCompletion = new SimpleBooleanProperty();
        activateFavoritesCompletion = new SimpleBooleanProperty();
        activateFEQCompletion = new SimpleBooleanProperty();
        activateTablesCompletion = new SimpleBooleanProperty();
        activateVariablesCompletion = new SimpleBooleanProperty();
        pasteObjectOnDoubleClick = new SimpleBooleanProperty();
        enableAnimation= new SimpleBooleanProperty();
        notifyUnsavedTabChanges = new SimpleBooleanProperty();
        menuButton_flipAnim = new SimpleBooleanProperty();
        menuButton_expndAnim = new SimpleBooleanProperty();
    }

    public Stage init(QueryAreaWrapper area){
        filepathTextField.getParent().setDisable(area == null);
        
        if (area != null) {
            queryAssociateFile.setValue(area.getRelativePath());
            queryAssociateFile.addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable,
                        String oldValue, String newValue) { 
                    area.setRelativePath(newValue);
                }
            });
        }
        
        // font size
        fontSlider.setValue(MainUI.getMainController().getSmartProperties().getFontSize().get());
                
        //font color
        fontColorPicker.setValue(getColor(MainUI.getMainController().getSmartProperties().getFontColor().get(), Color.BLACK));
        
        //background color
        backgroundColorPicker.setValue(getColor(MainUI.getMainController().getSmartProperties().getBackgroundColor().get(),null));
                
        //selection color
        selectionColorPicker.setValue(getColor(MainUI.getMainController().getSmartProperties().getSelectionColor().get(),null));
         
        //notify user
        notifyUserWhenTabUnsavedChangesExit.setSelected(MainUI.getMainController().getSmartProperties().getNotifyUnsavedTabChanges().get()); 
                
        //columns checkbox
//        ColumnsCheckbox.setSelected(controller.getSmartProperties().getActivateColumnsCompletion().get()); 
//        
//        //database checkbox
//        databaseCheckbox.setSelected(controller.getSmartProperties().getActivateDatabaseCompletion().get()); 
//        
//        //variables checkbox
//        variableCheckbox.setSelected(controller.getSmartProperties().getActivateVariablesCompletion().get()); 
//        
//        //tables checkbox
//        tablesCheckbox.setSelected(controller.getSmartProperties().getActivateTablesCompletion().get()); 
//        
//        //stored proc func checkbox
//        storedprcCheckbox.setSelected(controller.getSmartProperties().getActivateStoreProc_FunctionCompletion().get()); 
//        
//        //history checkbox
//        historyCheckbox.setSelected(controller.getSmartProperties().getActivateHistoryCompletion().get()); 
//        
//        //favorites checkbox
//        favoritesCheckbox.setSelected(controller.getSmartProperties().getActivateFavoritesCompletion().get()); 
//        
//        //feq checkbox
//        feqCheckbox.setSelected(controller.getSmartProperties().getActivateFEQCompletion().get()); 
        
        //auto rebuild tags
        autoRebuildTags.setSelected(MainUI.getMainController().getSmartProperties().getAutoRebuildTagsOnStartup().get()); 
        
//        //enable animation
//        enableAnimationCheckbox.setSelected(controller.getSmartProperties().getEnableAnimation().get()); 
        
        //auto complete
//        activateCompletionCheckBox.setSelected(controller.getSmartProperties().getActivateAutoComplete().get()); 
        
//        //expand animation
//        expandAnimCheckbx.setSelected(controller.getSmartProperties().getMenuButton_expandAnimation().get()); 
//        
//        //flip animation
//        flipAnimCheckbox.setSelected(controller.getSmartProperties().getMenuButton_flipAnimation().get()); 
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
//                controller.getSmartProperties().getActivateAutoComplete().bindBidirectional(activateAutoComplete);
                MainUI.getMainController().getSmartProperties().getFontColor().bindBidirectional(fontColor);
                MainUI.getMainController().getSmartProperties().getBackgroundColor().bindBidirectional(backgroundColor);
                MainUI.getMainController().getSmartProperties().getSelectionColor().bindBidirectional(selectionColor);
                MainUI.getMainController().getSmartProperties().getFontSize().bindBidirectional(fontSize);
//                controller.getSmartProperties().getActivateColumnsCompletion().bindBidirectional(activateColumnsCompletion);
//                controller.getSmartProperties().getActivateDatabaseCompletion().bindBidirectional(activateDatabaseCompletion);
//                controller.getSmartProperties().getActivateStoreProc_FunctionCompletion().bindBidirectional(activateStoreProc_FunctionCompletion);
//                controller.getSmartProperties().getActivateHistoryCompletion().bindBidirectional(activateHistoryCompletion);
//                controller.getSmartProperties().getActivateFavoritesCompletion().bindBidirectional(activateFavoritesCompletion);
//                controller.getSmartProperties().getActivateFEQCompletion().bindBidirectional(activateFEQCompletion);
//                controller.getSmartProperties().getActivateTablesCompletion().bindBidirectional(activateTablesCompletion);
//                controller.getSmartProperties().getActivateVariablesCompletion().bindBidirectional(activateVariablesCompletion);
                MainUI.getMainController().getSmartProperties().getAutoRebuildTagsOnStartup().bindBidirectional(autoRebuildTagsOnStartup);
                MainUI.getMainController().getSmartProperties().getNotifyUnsavedTabChanges().bindBidirectional(notifyUnsavedTabChanges);
                MainUI.getMainController().getSmartProperties().getPasteObjectOnDoubleClick().bindBidirectional(pasteObjectOnDoubleClick);
//                controller.getSmartProperties().getEnableAnimation().bindBidirectional(enableAnimation);
//                controller.getSmartProperties().getMenuButton_flipAnimation().bindBidirectional(menuButton_flipAnim);
//                controller.getSmartProperties().getMenuButton_expandAnimation().bindBidirectional(menuButton_expndAnim);
            }
        });
        
        if (s == null) {
            s = new Stage(StageStyle.UTILITY);
            s.initModality(Modality.WINDOW_MODAL);
            s.initOwner(Stage.impl_getWindows().next());
            s.setWidth(403);
            s.setResizable(false);
            s.setScene(new Scene(getUI()));
            s.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) { 
                    closureCode();
                }
            });
            s.setTitle("SETTINGS");
        }
        
        return s;
    }    
    
    private javafx.scene.paint.Color getColor(String a, javafx.scene.paint.Color d) {
        if (a == null) {
            if (d == null) {
                return javafx.scene.paint.Color.WHITE;
            } else {
                return d;
            }
        }
        return javafx.scene.paint.Color.web(a);
    }
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }
    
    private EventHandler<ActionEvent> allActionEventHandler = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) { 
            if(event.getSource() == exitButton){
                s.close();
            }else if(event.getSource() == openFileButton){
                FileChooser openFileChooser = new FileChooser();
                openFileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("SmartSQL Files", "*.sql"),
                        new FileChooser.ExtensionFilter("All Files", "*.*"));
                File userFile = openFileChooser.showOpenDialog(s);
                if (userFile != null) {
                    filepathTextField.setText(userFile.getPath());
                }
            }else if(event.getSource() == openUserDictionaryButton){
                FileChooser openFileChooser = new FileChooser();
                openFileChooser.getExtensionFilters().addAll(
                         new FileChooser.ExtensionFilter("TXT FILES", "*.txt"),
                        new FileChooser.ExtensionFilter("All Files", "*.*"));
                File userFile = openFileChooser.showOpenDialog(s);
                if (userFile != null) {
                    dictionarypathTextfield.setText(userFile.getPath());
                }
            }
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        exitButton.setOnAction(allActionEventHandler);
        openUserDictionaryButton.setOnAction(allActionEventHandler);
        
        fontSize.bind(fontSlider.valueProperty());
        fontColorPicker.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable,
                    Color oldValue, Color newValue) { 
                if(newValue != null){
                    fontColor.setValue("#"+newValue.toString().
                            substring(2,newValue.toString().length()-2));
                }
            }
        });
        
        backgroundColorPicker.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable,
                    Color oldValue, Color newValue) { 
                if(newValue != null){
                    backgroundColor.setValue("#"+newValue.toString().substring(
                    2,newValue.toString().length()-2));
                }
            }
        });
        
        selectionColorPicker.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable,
                    Color oldValue, Color newValue) { 
                if(newValue != null){
                    selectionColor.setValue("#"+newValue.toString().
                            substring(2,newValue.toString().length()-2));
                }
            }
        });
        
        ChangeListener<Boolean> menuButtonsAnimsCheckboxChangeListener = new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                    if (((ReadOnlyBooleanProperty) observable).getBean() == expandAnimCheckbx) {
                        flipAnimCheckbox.setSelected(!newValue);
                    } else {
                        expandAnimCheckbx.setSelected(!newValue);
                    }                
            }
        };
        
        flipAnimCheckbox.selectedProperty().addListener(menuButtonsAnimsCheckboxChangeListener);
        expandAnimCheckbx.selectedProperty().addListener(menuButtonsAnimsCheckboxChangeListener);
        
        activateAutoComplete.bind(activateCompletionCheckBox.selectedProperty());
        activateColumnsCompletion.bind(ColumnsCheckbox.selectedProperty());
        activateDatabaseCompletion.bind(databaseCheckbox.selectedProperty());
        activateTablesCompletion.bind(tablesCheckbox.selectedProperty());
        activateStoreProc_FunctionCompletion.bind(storedprcCheckbox.selectedProperty());
        activateHistoryCompletion.bind(historyCheckbox.selectedProperty());
        activateFavoritesCompletion.bind(favoritesCheckbox.selectedProperty());
        activateFEQCompletion.bind(feqCheckbox.selectedProperty());
        activateVariablesCompletion.bind(variableCheckbox.selectedProperty());
        autoRebuildTagsOnStartup.bind(autoRebuildTags.selectedProperty());
        pasteObjectOnDoubleClick.bind(pasteObjectNameOnDoubleClick.selectedProperty());
        enableAnimation.bind(enableAnimationCheckbox.selectedProperty());
        notifyUnsavedTabChanges.bind(notifyUserWhenTabUnsavedChangesExit.selectedProperty());
        menuButton_expndAnim.bind(expandAnimCheckbx.selectedProperty());
        menuButton_flipAnim.bind(flipAnimCheckbox.selectedProperty());
        
        //file path text
        filepathTextField.textProperty().addListener(new ChangeListener<String>() {
            
            private boolean isRealPathWithAccess(String path) {
                try {
                    java.io.File f = new java.io.File(path);
                    return f.exists() && f.canWrite();
                } finally {
                    return false;
                }
            }
            
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) { 
                boolean hadLegitOldValue  =  isRealPathWithAccess(oldValue);
                boolean newValueLegit = isRealPathWithAccess(newValue);
                if(newValueLegit && !hadLegitOldValue){
                    queryAssociateFile.setValue(newValue);
                }else if(!newValueLegit && hadLegitOldValue){
                    filepathTextField.textProperty().removeListener(this);
                    filepathTextField.setText(oldValue);
                    filepathTextField.textProperty().addListener(this);
                    DialogHelper.showError(getController(), "FILE PATH ERROR", "Specified location does not exist or it's invalid", null);
                }
            }
        });
        
        filepathTextField.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {            
            KeyCodeCombination kcc;            
            @Override
            public void handle(KeyEvent event) { 
                if(kcc == null){
                    kcc = new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN);
                }
                if(!kcc.match(event)){
                    DialogHelper.showError(getController(), "ILLEGAL OPERATION", "You can not manually enter file path.\n Select by using button on the right", null);
                    event.consume();
                }
            }
        });
        
        //open file 
        openFileButton.setOnAction(allActionEventHandler);
        
    }    
       public void closureCode() {
//        controller.getSmartProperties().getActivateAutoComplete().unbindBidirectional(activateAutoComplete);
        MainUI.getMainController().getSmartProperties().getFontColor().unbindBidirectional(fontColor);
        MainUI.getMainController().getSmartProperties().getBackgroundColor().unbindBidirectional(backgroundColor);
        MainUI.getMainController().getSmartProperties().getSelectionColor().unbindBidirectional(selectionColor);
        MainUI.getMainController().getSmartProperties().getFontSize().unbindBidirectional(fontSize);
//        controller.getSmartProperties().getActivateColumnsCompletion().unbindBidirectional(activateColumnsCompletion);
//        controller.getSmartProperties().getActivateDatabaseCompletion().unbindBidirectional(activateDatabaseCompletion);
//        controller.getSmartProperties().getActivateStoreProc_FunctionCompletion().unbindBidirectional(activateStoreProc_FunctionCompletion);
//        controller.getSmartProperties().getActivateHistoryCompletion().unbindBidirectional(activateHistoryCompletion);
//        controller.getSmartProperties().getActivateFavoritesCompletion().unbindBidirectional(activateFavoritesCompletion);
//        controller.getSmartProperties().getActivateFEQCompletion().unbindBidirectional(activateFEQCompletion);
//        controller.getSmartProperties().getActivateTablesCompletion().unbindBidirectional(activateTablesCompletion);
//        controller.getSmartProperties().getActivateVariablesCompletion().unbindBidirectional(activateVariablesCompletion);
        MainUI.getMainController().getSmartProperties().getAutoRebuildTagsOnStartup().unbindBidirectional(autoRebuildTagsOnStartup);
        MainUI.getMainController().getSmartProperties().getNotifyUnsavedTabChanges().unbindBidirectional(notifyUnsavedTabChanges);
        MainUI.getMainController().getSmartProperties().getPasteObjectOnDoubleClick().unbindBidirectional(pasteObjectOnDoubleClick);
//        controller.getSmartProperties().getEnableAnimation().unbindBidirectional(enableAnimation);
//        controller.getSmartProperties().getMenuButton_flipAnimation().unbindBidirectional(menuButton_flipAnim);
//        controller.getSmartProperties().getMenuButton_expandAnimation().unbindBidirectional(menuButton_expndAnim);
    }

}
