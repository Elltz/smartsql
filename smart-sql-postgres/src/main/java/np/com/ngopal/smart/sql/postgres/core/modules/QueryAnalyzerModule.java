package np.com.ngopal.smart.sql.modules;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class QueryAnalyzerModule extends AbstractStandardModule {
    
    private Image queryAnalyzerImage;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 100;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {return true;}

    @Override
    public boolean install() {
        
        registrateServiceChangeListener();
        
        MenuItem analyzeItem = new MenuItem("SQL Optimizer", new ImageView(StandardDefaultImageProvider.getInstance().getAnalyzer_image()));
        analyzeItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (queryAnalyzerImage == null) {
                    queryAnalyzerImage = StandardDefaultImageProvider.getInstance().getQueryAnalyzeTab_image();
                }
                final QueryAnalyzerTabController controller = getTypedBaseController("QueryAnalyzerTab");
                controller.setService(getMainController().getSelectedConnectionSession().get().getService());
                controller.setPerformanceTuningService(getMainController().getPerformanceTuningService());
                controller.setPreferenceService(getMainController().getPreferenceService());
                controller.setQars(getMainController().getRecommendationService());
                controller.setColumnCardinalityService(getMainController().getColumnCardinalityService());
                controller.setUsageService(getMainController().getUsageService());
                
                Tab alterTab = new Tab("SQL Optimizer");
                alterTab.setGraphic(new ImageView(queryAnalyzerImage));

                Parent p = controller.getUI();
                alterTab.setContent(p);

                getController().addTab(alterTab, getMainController().getSelectedConnectionSession().get());
                getController().showTab(alterTab, getMainController().getSelectedConnectionSession().get());
            }
        });
                
        analyzeItem.disableProperty().bind(busy());
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.POWERTOOLS, new MenuItem[] {analyzeItem});
        //getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[] {analyzeItem});
        
        return true;
    }
    
    

    @Override
    public boolean uninstall() {
        return true;
    }
    
    @Override
    public String getInfo() { 
        return "Analyze Queries";
    }
}