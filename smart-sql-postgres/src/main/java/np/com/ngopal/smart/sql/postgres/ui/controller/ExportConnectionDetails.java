package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.inject.Inject;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 *
 * @author Terah laweh (rumorsapp@gmail.com)
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class ExportConnectionDetails extends BaseController implements Initializable{
    
    
    
    @Inject
    private ConnectionParamService connectionService;
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private TableView<ConnectionParams> tableview;
    
    @FXML
    private TableColumn<ConnectionParams, Boolean> selectColumn;
    
    @FXML
    private TableColumn connectionNameTable;
    
    @FXML
    private TableColumn hostTable;
    
    @FXML
    private TableColumn usernameTable;
    
    @FXML
    private TableColumn portTable;
    
    @FXML
    private TableColumn connectionTypeTable;
    
    @FXML
    private Button exportButton;
        
    @FXML
    private TextField directoryPathField;
    
    @FXML
    private CheckBox selectDesectCheckbox;

    
    
    
    public void export(ActionEvent event) {
        
        final Gson gson = new Gson();
        
        List<ConnectionParams> forExport = new ArrayList<>();
        for (ConnectionParams p: tableview.getItems()) {
            if (p.getSelected() != null && p.getSelected()) {
                forExport.add(p.copyOnlyParams());
            }
        }
        
        final String json = gson.toJson(forExport);

        try {
            // encrypt data
            final DESKeySpec desKeySpec = new DESKeySpec(MainController.desspec.getBytes());
            final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            final SecretKey secretKey = keyFactory.generateSecret(desKeySpec);

            final Cipher encryptCipher = Cipher.getInstance("DES");
            encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] bytes = encryptCipher.doFinal(json.getBytes("UTF-8"));

            File f = new File(directoryPathField.getText());
            if (f.exists()) {
                if (DialogHelper.showConfirm(getController(), "File alreay exist", "File already exist, do you want to replace it?", getStage(), false) != DialogResponce.OK_YES) {
                    return;
                }
            }
            Files.write(f.toPath(), bytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (Exception ex) {
            DialogHelper.showError(getController(), "Error export", "Error occurred while exporting connections", ex);
        }
        
        DialogHelper.showInfo(getController(), "Done", "Connection(s) exported successfully", null);
        dialog.hide();
    }
    
    public void selectExportFile(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select path to save exported connections");
        fileChooser.setInitialFileName("exportConnections.ssc");
        
        final File exportFile = fileChooser.showSaveDialog(dialog);
        directoryPathField.setText(exportFile != null ? exportFile.getAbsolutePath() : "");
    }
    
    public void cancel(ActionEvent event) {
        dialog.hide();
    }
    
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        selectColumn.setCellValueFactory(new PropertyValueFactory<>("selected"));
        selectColumn.setCellFactory((TableColumn<ConnectionParams, Boolean> p) -> {
            TableCell<ConnectionParams, Boolean> cell = new TableCell<ConnectionParams, Boolean>() {

                private CheckBox checkBox;
                @Override
                public void updateItem(Boolean item, boolean empty) {

                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {                        
                        if (checkBox == null) {
                            this.checkBox = new CheckBox();
                            this.checkBox.setFocusTraversable(false);
                            this.checkBox.setAlignment(Pos.CENTER);
                            setAlignment(Pos.CENTER);
                            
                            checkBox.setOnAction((ActionEvent event) -> {
                                ConnectionParams col = tableview.getItems().get(getIndex());
                                col.setSelected(checkBox.isSelected());
                            });                            
                        }

                        checkBox.setSelected(item != null ? item : false);

                        setGraphic(checkBox);
                    } else {
                        setGraphic(null);
                    }
                }
            };
            return cell;
        });
        connectionNameTable.setCellValueFactory(new PropertyValueFactory<>("name"));
        hostTable.setCellValueFactory(new PropertyValueFactory<>("address"));
        usernameTable.setCellValueFactory(new PropertyValueFactory<>("username"));
        portTable.setCellValueFactory(new PropertyValueFactory<>("port"));
        connectionTypeTable.setCellValueFactory(new PropertyValueFactory<>("connectionType"));
        
        tableview.getItems().addAll(connectionService.getAll());
        
        
        selectDesectCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (ConnectionParams p: tableview.getItems()) {
                p.setSelected(newValue);
            }
            
            selectColumn.setVisible(false);
            selectColumn.setVisible(true);
        });
        
        directoryPathField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            exportButton.setDisable(false);
        });
        
        selectDesectCheckbox.setSelected(true);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    
    private Stage dialog;
    
    public void init(Window win){
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(600);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);
        dialog.setTitle("Export Connection Details");
        dialog.setScene(new Scene(getUI()));        
        dialog.showAndWait();
    }
    
}
