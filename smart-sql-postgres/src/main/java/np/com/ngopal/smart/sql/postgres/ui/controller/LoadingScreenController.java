/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.util.AbstractMap;
import java.util.Properties;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.Flags;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class LoadingScreenController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;

    @FXML
    private StackPane smartsqlPage;

    @FXML
    private StackPane timeUpScreen;

    @FXML
    private Label loaderIcon;

    @FXML
    private Button paynow;

    @FXML
    private Button exit;

    @FXML
    private Label trialNoticeIcon;

    @FXML
    private Label resetPeriod;

    @FXML
    private ProgressIndicator progressLoader;

    @FXML
    private Label sessionLoaderNotice;

    @FXML
    private Button cancel;
    
    private boolean isLoad = false;
    
    private boolean isHide = false;

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainUI.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            KeyCode[] resetKeys = {KeyCode.D, KeyCode.E, KeyCode.V, KeyCode.S,
                KeyCode.MINUS, KeyCode.R, KeyCode.E, KeyCode.S,
                KeyCode.E, KeyCode.T}, entry = new KeyCode[10];
            byte pos = 0;

            @Override
            public void handle(KeyEvent t) {
                if (t.getCode().equals(KeyCode.ENTER)) {
                    //evaluate
                    boolean allMatch = resetKeys[0].equals(entry[0]) && resetKeys[1].equals(entry[1])
                            && resetKeys[2].equals(entry[2]) && resetKeys[3].equals(entry[3])
                            && resetKeys[4].equals(entry[4]) && resetKeys[5].equals(entry[5])
                            && resetKeys[6].equals(entry[6]) && resetKeys[7].equals(entry[7])
                            && resetKeys[8].equals(entry[8]) && resetKeys[9].equals(entry[9]);
                    for (int i = 0; i < 10; i++) {
                        entry[i] = null;
                    }
                    pos = 0;
                    if (allMatch) {
                        resetExpiry(true);
                    }
                } else {
                    if (pos == 10) {
                        pos = 0;
                    }
                    entry[pos] = t.getCode();
                    pos++;
                }
            }
        });

        timeUpScreen.setVisible(false);
        smartsqlPage.setVisible(false);

        mainUI.setPrefHeight(Region.USE_COMPUTED_SIZE);
    }

    private void resetExpiry(boolean isEscape) {
        resetPeriod.setVisible(true);
        getController().addBackgroundWork(new WorkProc() {

            @Override
            public void updateUI() {
                closeLoader();
                //controller.fileHelperController.gotoAboutPage();
            }

            @Override
            public void run() {
                callUpdate = true;
                synchronized (((MainController)getController()).getSmartDataLocker()) {
                    if (isEscape) {
                        Properties props = new Properties();
                        props.setProperty(Flags.USER_KEYTYPE, "free");//"developerKey"
                        ((MainController)getController()).getSmartData().updateProperties(props);
                    } else { }
                }
                getController().saveSessions();
            }
        });
    }
    
    private Label onAddSessionLoaderNoticeToFotter(boolean isAddingToFooter){
        if(isAddingToFooter){
            sessionLoaderNotice.setUserData(sessionLoaderNotice.getGraphic());
            progressLoader.setPrefSize(15,15);
            sessionLoaderNotice.setGraphic(progressLoader);
            sessionLoaderNotice.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent t) { 
                    isHide = false;
                    startLoader();
                }
            });
        }else{
            sessionLoaderNotice.setGraphic((Node)sessionLoaderNotice.getUserData());
            sessionLoaderNotice.setUserData(null);
            progressLoader.setPrefSize(25,25);
            sessionLoaderNotice.setOnMouseClicked(null);
        }
        return sessionLoaderNotice;
    }
    
    public void hideLoader() {
        if (isLoad && !isHide) {
            popUp.close();
            smartsqlPage.getChildren().remove(sessionLoaderNotice);
            addMessageToFooter(onAddSessionLoaderNoticeToFotter(true));
            isHide = true;
        }
    }
    
    public void closeLoader() {
        if (isLoad) {
            popUp.close();
            removeMessageFromFooter(onAddSessionLoaderNoticeToFotter(false));
            isHide = isLoad = false;
            timeUpScreen.setVisible(false);
            smartsqlPage.setVisible(false);
        }
    }
    
    public void addMessageToFooter(Label l){
        if(((HBox)((MainController)getController()).getFooter(Flags.FOOTER_ELEMENT_NOTIFICATION_SECTION)).getChildren().
                contains(l))return;
        HBox.setHgrow(l, Priority.SOMETIMES);
        ((HBox)((MainController)getController()).getFooter(Flags.FOOTER_ELEMENT_NOTIFICATION_SECTION)).getChildren().add(l);
    }
    
    public void removeMessageFromFooter(Node n){
            HBox.clearConstraints(n);
            ((HBox)((MainController)getController()).getFooter(Flags.FOOTER_ELEMENT_NOTIFICATION_SECTION))
                    .getChildren().remove(n);
    }

    public boolean isLoading() {        
        return (popUp != null) && isLoad;
    }

    private Stage popUp;

    public void startLoader() {
        Platform.runLater(() -> {
            if (popUp == null) {
                popUp = new Stage();
                popUp.setScene(new Scene(mainUI, Color.TRANSPARENT));
                popUp.initModality(Modality.APPLICATION_MODAL);
                popUp.initOwner(getController().getStage());
                popUp.initStyle(StageStyle.TRANSPARENT);
                popUp.getScene().getRoot().setStyle("-fx-background-color: transparent;");
            } else {
                if (!smartsqlPage.getChildren().contains(sessionLoaderNotice)) {
                    smartsqlPage.getChildren().add(sessionLoaderNotice);
                }
            }

            popUp.show();
            isLoad = true;
            if (!isHide) {
                Platform.runLater(() -> {
                    sizeUpLoader();
                });
            }
        });
        if (!isHide) {
            setTimeUp();
            setPaidLevel();
        }
    }

    private double denum = 0.0,// or max
            rate = 0.0;

    public void setDenum(double de) {
        denum = de;
    }

    public void setRate(double numerator) {
        rate = numerator;
    }

    public void setTimeUp() {
        if (((MainController)getController()).getSmartData() == null) {
            return;
        }
        if (((MainController)getController()).getSmartData().isEvaluationTimeOver()) {
            timeUpScreen.setVisible(true);
            exit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    closeLoader();
                    getStage().close();
                }
            });

            paynow.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    //currently does nothing remember to call reset days

                }
            });
            AbstractMap.SimpleEntry<Boolean, String> keyValue = ((MainController)getController()).getSmartData().getEvaluationStatus_Message();
            resetPeriod.setText(keyValue.getValue());
            resetPeriod.setVisible(true);
            resetPeriod.setContentDisplay((keyValue.getKey() ? ContentDisplay.LEFT : ContentDisplay.TEXT_ONLY));
        } else {
            Platform.runLater(() -> {
                smartsqlPage.setVisible(true);
            });
        }
    }

    public void setPaidLevel() {
        synchronized (((MainController)getController()).getSmartDataLocker()) {
            if (((MainController)getController()).getSmartData() == null) {
                return;
            }
            trialNoticeIcon.setVisible(((MainController)getController()).getSmartData().isPaid());
        }
    }

    public void notifyBadPress(int what, String message, EventHandler<ActionEvent> eh) {
        
        Platform.runLater(() -> {
            if(cancel.getTooltip() == null){
                cancel.setTooltip(new Tooltip());
            }
            cancel.getTooltip().setText(message);
            cancel.setOnAction(eh);
        });
    }

    private void sizeUpLoader() {
        Stage stage = getController().getStage();
        if (stage != null) {
            popUp.setHeight(stage.getHeight());
            popUp.setWidth(stage.getWidth());
            popUp.setX(stage.getX());
            popUp.setY(stage.getY());
        }
//        AnchorPane.setTopAnchor(smartsqlPage, 380.0);
//        AnchorPane.setLeftAnchor(smartsqlPage,
//                (controller.getStage().getWidth() / 1.5) - (smartsqlPage.prefWidth(-1) / 2));
    }
}
