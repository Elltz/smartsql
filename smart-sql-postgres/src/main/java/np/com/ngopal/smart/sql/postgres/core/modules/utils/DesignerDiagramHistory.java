package np.com.ngopal.smart.sql.core.modules.utils;

import java.util.Map;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
public class DesignerDiagramHistory {
    
    private final SimpleObjectProperty<HistoryPoint> disabledHistoryPoint = new SimpleObjectProperty<HistoryPoint>() {
        @Override
        protected void invalidated() {
            // if value is null need make all point enabled
            if (disabledHistoryPoint.get() == null) {
                for (HistoryPoint hp: historyPoints) {
                    hp.setEnabled(true);
                }
            } else {
                int index = historyPoints.indexOf(disabledHistoryPoint.get());
                for (int i = 0; i < historyPoints.size(); i++) {
                    
                    HistoryPoint hp = historyPoints.get(i);
                    hp.setEnabled(i < index);
                }
            }
        }
        
    };
    
    private final ObservableList<HistoryPoint> historyPoints = FXCollections.observableArrayList();
   
    private final HistoryPoint initHistoryPoint;
    public DesignerDiagramHistory(DesignerErrDiagram currentDiagram) {
        initHistoryPoint = new HistoryPoint("Init", currentDiagram.toMap());
    }
    
   
    /**
     *  Create new history point and putting to all history,
     * diagram will be cloned
     * 
     * @param pointText text of new history point
     * @param diagram diagram to be cloned to history point
     */
    public void putHistoryPoint(String pointText, DesignerErrDiagram diagram) {
        if (disabledHistoryPoint.get() != null) {
            
            // remove all no needed history points
            historyPoints.remove(historyPoints.indexOf(disabledHistoryPoint.get()), historyPoints.size());
            historyPoints.add(new HistoryPoint(pointText, diagram.toMap()));
            
            // clear property
            disabledHistoryPoint.set(null);
        } else {
            historyPoints.add(new HistoryPoint(pointText, diagram.toMap()));
        }
    }
    
    /**
     *  Disable / Enable selected history point
     * 
     * @param historyPoint history point for disable / enable
     */
    public void selectHistoryPoint(HistoryPoint historyPoint) {
        
        if (historyPoint.isEnabled()) {
            disabledHistoryPoint.set(historyPoint);
            
        } else {
            
            int index = historyPoints.indexOf(historyPoint);
            index++;
            
            disabledHistoryPoint.set(historyPoints.size() > index ? historyPoints.get(index) : null);
        }
    }
    
    
    /**
     *  Return last enabled history point
     * @return last enabled history point
     */
    public HistoryPoint getLastEnabledHistoryPoint() {
        HistoryPoint historyPoint = initHistoryPoint;
        for (HistoryPoint hp: historyPoints) {
            if (hp.isEnabled()) {
                historyPoint = hp;
            } else {
                break;
            }
        }
        
        return historyPoint;
    }
    
    /**
     *  Return property of first disabled history point,
     * by default value is null
     * 
     * @return value of disabled history point
     */
    public SimpleObjectProperty<HistoryPoint> disabledHistoryPointProperty() {
        return disabledHistoryPoint;
    }
    
    public ObservableList<HistoryPoint> getHistoryPoints() {
        return historyPoints;
    }
    
    /**
     * Clear designer diagram history
     */    
    public void clearHistory() {
        historyPoints.clear();
        disabledHistoryPoint.set(null);
    }
    
    public class HistoryPoint {
        
        private final Map<String, Object> diagramMap;
        private final String pointText;
        
        private boolean enabled = true;
        
        public HistoryPoint(String pointText, Map<String, Object> diagramMap) {
            this.diagramMap = diagramMap;
            this.pointText = pointText;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
        public boolean isEnabled() {
            return enabled;
        }
        
        public Map<String, Object> getDiagram() {
            return diagramMap;
        }        
        
        @Override
        public String toString() {
            return pointText;
        }
    }
    
    
    
}
