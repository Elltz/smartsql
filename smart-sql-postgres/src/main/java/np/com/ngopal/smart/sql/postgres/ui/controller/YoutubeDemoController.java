package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.structure.controller.BaseController;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class YoutubeDemoController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private WebView youtubeView;
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
    }
    
    public void init(String youtubeLink) {
        Platform.runLater(() -> {
            try {
                youtubeView.getEngine().load("http://www.youtube.com/embed/" + youtubeLink + "?fs=0&rel=0&showinfo=0&autoplay=1");
            } catch (Throwable th) {
                log.error(th.getMessage(), th);
            }
        });
    }
    
    public void stop() {
        youtubeView.getEngine().loadContent("");
    }
    
    @FXML
    public void closeAction() {
        stop();
        getStage().close();
    }
}
