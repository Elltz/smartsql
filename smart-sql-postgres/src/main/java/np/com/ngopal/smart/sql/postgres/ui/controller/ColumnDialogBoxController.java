/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.ui.factories.EditingCell;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class ColumnDialogBoxController extends BaseController implements Initializable {

    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private CheckBox checkAllCheckBox;
    
    @FXML
    private TableView<SelectionColumn> columnTable;

    @FXML
    private TableColumn<SelectionColumn, String> columnCol;    
    @FXML
    private TableColumn<SelectionColumn, Boolean> checkBoxcol;
    @FXML
    private TableColumn<SelectionColumn, DataType> datatypeCol;    
    @FXML
    private TableColumn<SelectionColumn, Integer> lengthCol;

    @FXML
    private Button upButton;
    @FXML
    private Button downButton;

    private boolean response;

    private ObservableList<SelectionColumn> columnsList;

    private Index index;
    private List<SelectionColumn> selectionColumns;

    private Stage stage;

    @FXML
    public void checkAll(ActionEvent event) {
        if (selectionColumns != null) {
            for (SelectionColumn c: selectionColumns) {
                c.selected().set(checkAllCheckBox.isSelected());
            }
        }
    }
    
    
    @FXML
    void up(ActionEvent event) {

    }
    
    @FXML
    void down(ActionEvent event) {

    }

    @FXML
    void cancel(ActionEvent event) {
        setResponse(false);
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();
    }

    

    @FXML
    void ok(ActionEvent event) {
        setResponse(true);
        stage = (Stage)mainUI.getScene().getWindow();
        stage.close();

    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public List<Column> selectedColumns() {
        return selectionColumns.stream().filter(t -> t.selected().get()).collect(Collectors.toList());
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public boolean getResponse() {
        return response;
    }
    
    public Index getIndex() {
        return index;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        checkBoxcol.setId("checkbox");
    }

    public void init(Index index, List<SelectionColumn> selectionColumns) {

        this.index = index;
        this.selectionColumns = selectionColumns;

        Callback<TableColumn<SelectionColumn, String>, TableCell<SelectionColumn, String>> cellFactory = new Callback<TableColumn<SelectionColumn, String>, TableCell<SelectionColumn, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };
        
        columnsList = FXCollections.observableArrayList(selectionColumns);
        columnTable.setItems(columnsList);
        columnTable.setEditable(true);
        
        columnCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnCol.setCellFactory(cellFactory);
        columnCol.setEditable(false);

        datatypeCol.setCellValueFactory(new PropertyValueFactory<>("dataType"));
        datatypeCol.setEditable(false);
        datatypeCol.setCellFactory((TableColumn<SelectionColumn, DataType> param) -> {
            TableCell<SelectionColumn, DataType> cell = new TableCell<SelectionColumn, DataType>() {

                private Label label;
                
                @Override
                protected void updateItem(DataType item, boolean empty) {
                    
                    super.updateItem(item, empty);
                    
                    if (item != null) {
                        if (label == null) {
                            label = new Label(item.getDisplayValue());
                        }
                        
                        setGraphic(label);
                    }                    
                }                
            };
            
            return cell;
        });

        if (index != null) {
            
            lengthCol.setCellFactory(new Callback<TableColumn<SelectionColumn, Integer>, TableCell<SelectionColumn, Integer>>() {
                @Override
                public TableCell call(TableColumn p) {
                    EditingCell cell = new EditingCell() {
                        @Override
                        protected Integer convertFromString(String item) {
                            return Integer.valueOf(item);
                        }

                        @Override
                        public void commitEdit(Object newValue) {
                            super.commitEdit(newValue);                            
                            
                            index.setColumnLength(((SelectionColumn)getTableRow().getItem()).getName(), (Integer)newValue);
                        }
                        
                    };
                    cell.setAlignment(Pos.CENTER_RIGHT);
                    return cell;
                }
            });
        } else {
            lengthCol.setCellFactory(TextFieldTableCell.<SelectionColumn, Integer>forTableColumn(new IntegerStringConverter()));
        }
        
        
        lengthCol.setCellValueFactory((CellDataFeatures<SelectionColumn, Integer> param) -> {
            Integer i = param.getValue().getDataType() != null ? param.getValue().getDataType().getLength() : null;
            if (index != null) {
                i = index.getColumnLength(param.getValue().getName());
            }
            
            SimpleObjectProperty obj = new SimpleObjectProperty<>();
            if (i != null) {
                obj.set(i);
            }
            return obj;
        });
        
        checkBoxcol.setCellValueFactory(cellData -> ((SelectionColumn)cellData.getValue()).selected());
        checkBoxcol.setCellFactory((TableColumn<SelectionColumn, Boolean> p) -> {
            TableCell<SelectionColumn, Boolean> cell = new TableCell<SelectionColumn, Boolean>() {

                private CheckBox checkBox;

                @Override
                public void updateItem(Boolean item, boolean empty) {

                    super.updateItem(item, empty);
                    
                    if (getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {                        
                        if (checkBox == null) {
                            this.checkBox = new CheckBox();
                            this.checkBox.setFocusTraversable(false);
                            this.checkBox.setAlignment(Pos.CENTER);
                            setAlignment(Pos.CENTER);
                            
                            checkBox.setOnAction((ActionEvent event) -> {
                                SelectionColumn col = selectionColumns.get(getIndex());
                                col.selected().set(checkBox.isSelected());
                            });                            
                        }

                        checkBox.setSelected(item != null ? item : false);

                        setGraphic(checkBox);

                    } else {
                        setGraphic(null);
                    }

                }

            };
            return cell;
        });
        
        if (mainUI.getScene() == null) {
            mainUI.sceneProperty().addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
                if (newValue != null) {
                    if (!selectionColumns.isEmpty()) {
                        columnTable.getSelectionModel().select(0);
                    }
                    columnTable.requestFocus();
                }
            });
        } else {
            if (!selectionColumns.isEmpty()) {
                columnTable.getSelectionModel().select(0);
            }
            columnTable.requestFocus();
        }
    }

}
