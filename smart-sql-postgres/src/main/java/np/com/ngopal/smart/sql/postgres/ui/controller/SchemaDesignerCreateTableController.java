/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
public class SchemaDesignerCreateTableController extends BaseController implements Initializable{
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private ComboBox databaseChooser;

    @FXML
    private TextField tablenameTextfield;

    @FXML
    private ComboBox engineCombobox;

    @FXML
    private ComboBox<String> characterSetCombobox;

    @FXML
    private ComboBox collationCombobox;
    
    @FXML
    private TabPane tabPane;
    
    @FXML
    private Button saveButton;
    
    @FXML
    private Button revertButton;
    
    @FXML
    private Button cancelButton;
    
    @FXML
    private TextArea commentInput;
    
    @FXML
    private TextField autoIncrementInput;
    
    @FXML
    private TextField avgRowLengthInput;
    
    @FXML
    private TextField maxRowsInput;
    
    @FXML
    private TextField minRowsInput;
    
    @FXML
    private ComboBox rowFormartChooser;
    
    @FXML
    private ComboBox delayChooser;
    
    @FXML
    private ComboBox checksumChooser;
    
    @FXML
    private Button delButton;
    
    @FXML
    private Button addButton;
    
    @FXML
    private Button moveUpButton;
    
    @FXML
    private Button moveDownButton;
    
    @FXML
    private HBox tabContentHeader;
    
    @FXML
    private CheckBox hideLanCheckbox;
    
    @FXML
    private TableView columnTableview;
    
    @FXML
    private TableView indexTableview;
    
    @FXML
    private TableColumn columnNameColumn;
    
    @FXML
    private TableColumn notNullColumn;
    
    @FXML
    private TableColumn dataTypeColumn;
    
    @FXML
    private TableColumn defaultColumn;;
    
    @FXML
    private TableColumn lengthColumn;
    
    @FXML
    private TableColumn pkColumn;
    
    @FXML
    private TableColumn unsignedColumn;
    
    @FXML
    private TableColumn autoIncreaseColumn;
    
    @FXML
    private TableColumn zeroFillColumn;
    
    @FXML
    private TableColumn onUpdateColumn;
    
    @FXML
    private TableColumn commentColumn;
    
    @FXML
    private TableColumn indexNameColumn;
    
    @FXML
    private TableColumn columnColumn;
    
    @FXML
    private TableColumn indexTypeColumn;
    
    @FXML
    private TableColumn constraintNameColumn;
    
    @FXML
    private TableColumn refColumn;
    
    @FXML
    private TableColumn refDatabase;
    
    @FXML
    private TableColumn refTable;
    
    @FXML
    private TableColumn refedColumn;
    
    @FXML
    private TableColumn onUpdateColumnFK;
    
    @FXML
    private TableColumn onDelColumn;
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //me and my crazy recyclings :)
        tabPane.getSelectionModel().selectedIndexProperty().
                addListener(new ChangeListener<Number>() {
                    
                    @Override
                    public void changed(ObservableValue<? extends Number> ov,
                            Number t, Number t1) {
                        switch (t1.intValue()) {
                            case 0: //column 
                                ((VBox)tabContentHeader.getParent()).getChildren().remove(tabContentHeader);
                                ((VBox)tabPane.getTabs().get(0).getContent()).getChildren().add(0,tabContentHeader);
                                for(Node n :tabContentHeader.getChildren()){
                                    n.setVisible(true);
                                }
                                break;
                            case 1:// index
                                ((VBox)tabContentHeader.getParent()).getChildren().remove(tabContentHeader);
                                ((VBox)tabPane.getTabs().get(1).getContent()).getChildren().add(0,tabContentHeader);
                                for(Node n :tabContentHeader.getChildren()){
                                    if(n == addButton | n == delButton){
                                        continue;
                                    }
                                    n.setVisible(false);
                                }
                                break;
                            case 2: // foreign keys
                                ((VBox)tabContentHeader.getParent()).getChildren().remove(tabContentHeader);
                                ((VBox)tabPane.getTabs().get(2).getContent()).getChildren().add(0,tabContentHeader);
                                for(Node n :tabContentHeader.getChildren()){
                                    if(n == addButton | n == delButton){
                                        continue;
                                    }
                                    n.setVisible(false);
                                }
                                break;
                            case 3:
                                break;
                            case 4:
                                break;
                        }
                    }
                });
        
    }
    
    Database data;
    Stage dialog;
    // @TODO IT NOT USED, so no need any multi query call, I think need remove this class
    public void init(Database database, ConnectionSession session,boolean isAlter,
            int tab2select,String tableName){
        data = database;        
        
        try {
            databaseChooser.getItems().addAll(session.getService().getDatabases());
            if(isAlter){
                databaseChooser.setDisable(true);
            }
            characterSetCombobox.getItems().addAll(session.getService().getCharSet());
            engineCombobox.getItems().addAll(session.getService().getEngine());
            characterSetCombobox.getSelectionModel().selectedItemProperty()
                    .addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> ov, String t, String t1) { 
                    if(t1 != null){
                        try {
                            collationCombobox.getItems().clear();
                            collationCombobox.getItems().addAll(session.getService().getCollation(t1));
                        } catch (SQLException ex) {
                            collationCombobox.getItems().add("[default]");
                        }
                    }
                }
            });
        } catch (SQLException ex) {
            return;
        }
        if (dialog == null) {
            dialog = new Stage(StageStyle.UTILITY);
            dialog.setWidth(605);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getStage());
            if(isAlter){
                dialog.setTitle("Alter Table");
            }else{
                dialog.setTitle("Create Table");
            }
            dialog.setScene(new Scene(getUI()));
//        dialog.setOnCloseRequest(new EventHandler<WindowEvent>() {
//
//            @Override
//            public void handle(WindowEvent t) {
//                dialog.getScene().setRoot(null);
//                dialog.setScene(null);
//                dialog = null;
//            }
//        });
        }
        if(isAlter){
            tabPane.getSelectionModel().select(tab2select);
        }
        tablenameTextfield.setText(tableName);
        dialog.showAndWait();
    }
    
}
