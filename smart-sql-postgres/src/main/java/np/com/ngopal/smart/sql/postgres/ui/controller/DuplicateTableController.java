package np.com.ngopal.smart.sql.ui.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class DuplicateTableController extends BaseController implements Initializable {

    private ConnectionSession session;
    private DBTable table;
    
    @FXML
    private AnchorPane mainUI;
    @FXML
    private Label tableLabel;
    @FXML
    private TextField tableField;
    @FXML
    private CheckBox withAllFields;
    @FXML
    private CheckBox withIndexes;
    @FXML
    private CheckBox withTriggers;
    @FXML
    private RadioButton structureAndData;

    @FXML
    private ListView<Column> columnsListView;
    
    private List<Column> selectedColumns;
    
    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }
    
    @FXML
    public void copyAction(ActionEvent event) {
        try {
            session.getService().copyTable(table.getDatabase().getName(), table.getName(), tableField.getText(), selectedColumns, withIndexes.isSelected(), !structureAndData.isSelected(), withTriggers.isSelected());
            getController().refreshTree(session, false);

            getStage().close();
        } catch (SQLException ex) {
            DialogHelper.showError(getController(), "Error", "Error coping table", ex);
        }
    }

    public void init(ConnectionSession session, DBTable table) throws SQLException {
        
        this.session = session;
        this.table = table;
        
        tableLabel.setText(String.format("Copy '%s' to new table", table.getName()));
        tableField.setText(table.getName() + "_copy");
        
        List<Column> columns = session.getService().getColumnInformation(table.getDatabase().getName(), table.getName());
        if (columns != null) {
            columnsListView.setItems(FXCollections.observableArrayList(columns));
            selectedColumns = new ArrayList<>(columns);
        }
        
        columnsListView.setCellFactory(CheckBoxListCell.forListView((Column item) -> {
            BooleanProperty observable = new SimpleBooleanProperty(true);
            observable.addListener((obs, wasSelected, isNowSelected) -> 
            {
                if (isNowSelected) {
                    if (!selectedColumns.contains(item)) {
                        selectedColumns.add(item);
                    }
                } else {
                    selectedColumns.remove(item);
                }
            });
            return observable ;
        }));
        
        columnsListView.setDisable(true);
        
        bindListeners();
    }
    
    private void bindListeners() {
        withAllFields.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            columnsListView.setDisable(newValue);
        });
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
