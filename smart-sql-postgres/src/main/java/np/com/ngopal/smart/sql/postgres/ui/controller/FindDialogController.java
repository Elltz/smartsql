package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.FindReplaceHistoryService;
import np.com.ngopal.smart.sql.model.FindReplaceHistory;
import np.com.ngopal.smart.sql.ui.components.AutoCompleteComboBoxListener;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.queryutils.FindCodeAreaData;
import np.com.ngopal.smart.sql.structure.queryutils.FindCodeAreaUtil;
import np.com.ngopal.smart.sql.structure.queryutils.FindObject;
import np.com.ngopal.smart.sql.structure.utils.*;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;

/**
 *
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class FindDialogController extends BaseController implements Initializable {
    
    private Map<FindObject, FindCodeAreaData> codeareasFindDada;
    
    @FXML
    private Button findNextButton;

    @FXML
    private Button cancelButton;

    @FXML
    private ComboBox<String> findField;

    @FXML
    private CheckBox matchWholeWordCheckBox;

    @FXML
    private CheckBox matchCaseCheckBox;
    
    @FXML
    private RadioButton upRadioButton;

    @FXML
    private RadioButton downRadioButton;

    @FXML
    private AnchorPane mainUI;

    @Getter
    private FindCodeAreaData findData;
        
    @Inject
    private FindReplaceHistoryService findReplaceService;
    
    private TableView<RowData> resultGrid;
    
    private FindObject findObject;

    public void initFindObject(Map<FindObject, FindCodeAreaData> codeareasFindDada, FindObject findObject) {
        this.codeareasFindDada = codeareasFindDada;
        this.findObject = findObject;
        codeareasFindDada.remove(getFindObject());
    }
    
    public void initResultGrid(TableView<RowData> resultGrid) {
        this.resultGrid = resultGrid;
    }
    
    private FindObject getFindObject() {
        return findObject;
    }
    
    
    @FXML
    public void findNextAction(ActionEvent event) {
        if (codeareasFindDada != null) {
            FindObject codeArea = getFindObject();
            makeBusy(true);
            Thread th = new Thread(() -> {
                String text = findField.getEditor().getText();
                if (!text.trim().isEmpty() && findReplaceService.isNeedSave(text)) {
                    findReplaceService.save(new FindReplaceHistory(text));
                    Platform.runLater(() -> {
                       updateUsage(); 
                    });
                }

//                FindCodeAreaData old = codeareasFindDada.get(codeArea);

                int position = codeArea.getCaretPosition(CARET_POSITION_FROM_START);


                findData = new FindCodeAreaData(
                    text, 
                    downRadioButton.isSelected() ? FindCodeAreaData.FindDirection.DOWN : FindCodeAreaData.FindDirection.UP, 
                    matchCaseCheckBox.isSelected(), 
                    matchWholeWordCheckBox.isSelected(),
                    true,
                    position);

                FindCodeAreaUtil.findNext(codeArea, findData);
                
                if (findData.getLastPosition() == -1) {
                    Platform.runLater(() -> {
                        DialogHelper.showInfo(getController(), "Info", "Finishing searching the document. Cannot find \"" + findData.getFindText() + "\"", null);
                    });
                }

                codeareasFindDada.put(codeArea, findData);
                makeBusy(false);
            });
            th.setDaemon(true);
            th.start();
            
        } else if (resultGrid != null) {
            
            makeBusy(true);
            Thread th = new Thread(() -> {
                String text = findField.getEditor().getText();
                if (!text.trim().isEmpty() && findReplaceService.isNeedSave(text)) {
                    findReplaceService.save(new FindReplaceHistory(text));
                    Platform.runLater(() -> {
                       updateUsage(); 
                    });
                }

                findData = new FindCodeAreaData(
                    text, 
                    downRadioButton.isSelected() ? FindCodeAreaData.FindDirection.DOWN : FindCodeAreaData.FindDirection.UP, 
                    matchCaseCheckBox.isSelected(), 
                    matchWholeWordCheckBox.isSelected(),
                    true,
                    resultGrid.getSelectionModel().getSelectedCells() != null && !resultGrid.getSelectionModel().getSelectedCells().isEmpty() ? resultGrid.getSelectionModel().getSelectedCells().get(0) : null);

                FindCodeAreaUtil.findNext(resultGrid, findData);

                makeBusy(false);
            });
            th.setDaemon(true);
            th.start();
        }
    }

    private void updateUsage() {
        findField.getItems().clear();
        List<FindReplaceHistory> list = findReplaceService.getLastNames();
        if (list != null) {
            for (FindReplaceHistory f: list) {
                findField.getItems().add(f.getHistory());
            }
        }
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        getStage().close();
    }

    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateUsage();
        
//        findField.getEditor().addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
//            if (event.getCode() == KeyCode.ENTER) {
//                findNextAction(null);
//            }
//        });
//        
        Platform.runLater(() -> {
            new AutoCompleteComboBoxListener<>(findField);
            findField.getEditor().requestFocus();
        });
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}