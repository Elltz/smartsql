/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.stage.Window;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
/**
 *
 * @author Terah Laweh(rumorsapp@gmail.com);
 */

public abstract class AbstractStandardModule extends ModuleController {

    private BooleanBinding busy;
    
    protected BooleanBinding busy() {
        if (busy == null ) {
            busy = Bindings.or(selectedConnectionBusy(), getMainController().forceBusy());
        }
        
        return busy;
    }
    
     protected void registrateServiceChangeListener() {
        serviceListener.changed(null, false, true);

        getController().getSelectedConnectionSession().addListener((ObservableValue<? extends ConnectionSession> observable, ConnectionSession oldValue, ConnectionSession newValue) -> {

            if (oldValue != null) {
                ((AbstractDBService) oldValue.getService()).busy().removeListener(serviceListener);
            }

            if (newValue != null) {
                ((AbstractDBService) newValue.getService()).busy().addListener(serviceListener);

                boolean busy = ((AbstractDBService) newValue.getService()).isServiceBusy();
                serviceListener.changed(null, !busy, busy);
            }
        });
    }
    
     public MainController getMainController(){
         return ((MainController)getController());
     }
     
     public <T extends BaseController> T getTypedBaseController(String input){
        return getMainController().getTypedBaseController(input);
    }
    
    public <T extends TabContentController> T getTypedTabContentBaseController(ModuleController module, ConnectionSession session){        
        return getMainController().getTypedTabContentBaseController(module, session);
    }

    @Override
    public boolean isConnection() {
        return false;
    }
    
    public DialogResponce showInfo(String titleBar, String infoMessage, String detailMessage) {
        return getMainController().showInfo(titleBar, infoMessage, detailMessage);
    }
    
    public DialogResponce showInfo(String titleBar, String infoMessage, String detailMessage, Window owner) {
        return getMainController().showInfo(titleBar, infoMessage, detailMessage, owner);
    }

    public DialogResponce showConfirm(String titleBar, String infoMessage, Window owner, boolean hasMultiButtons) {
        return getMainController().showConfirm(titleBar, infoMessage, owner, hasMultiButtons);
    }
    
    public DialogResponce showConfirm(String titleBar, String infoMessage) {
        return getMainController().showConfirm(titleBar, infoMessage);
    }
    
    public String showInput(String titleBar, String message, String initialValue) {
        return getMainController().showInput(titleBar, message, initialValue);
    }
    
    public String showInput(String titleBar, String message, String label, String initialValue, Window owner) {
        return getMainController().showInput(titleBar, message, label, initialValue, owner);
    }

    public String showPassword(String titleBar, String message, String label, String initialValue, Window owner) {
        return getMainController().showPassword(titleBar, message, label, initialValue, owner);
    }

    public DialogResponce showError(String titleBar, String infoMessage, Throwable throwable, Window owner) {
        return getMainController().showError(titleBar, infoMessage, throwable, owner);
    }
    
    public DialogResponce showError(String titleBar, String infoMessage, Throwable throwable) {
        return getMainController().showError(titleBar, infoMessage, throwable);
    }    
}
