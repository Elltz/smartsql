/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.core.modules.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.controller.UserManagerController;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com);
 */
public abstract class User {
    
    
    /**
     * MYSQL Privileges 
     * 
     * The privileges are listed in the names after CONTEXT_PRIV_
     * lowercase 'a' means "and" all uppercases represents the 
     * context in which they affect.
     * 
     * Then name of the privilege comes after `__`.
     * 
     */
    
    public static final String CONTEXT_PRIV_DTI__CREATE = "CREATE";
    public static final String CONTEXT_PRIV_DTV__DROP = "DROP";
    public static final String CONTEXT_PRIV_DTS__GRANT_OPTION = "GRANT OPTION";
    public static final String CONTEXT_PRIV_DBS__LOCK_TABLES = "LOCK TABLES";
    public static final String CONTEXT_PRIV_DaT__REF = "REFERENCES";
    public static final String CONTEXT_PRIV_DBS__EVENT = "EVENT";
    public static final String CONTEXT_PRIV_TAB__ALTER = "ALTER";
    public static final String CONTEXT_PRIV_TAB__DEL = "DELETE";
    public static final String CONTEXT_PRIV_TAB__INDEX = "INDEX";
    public static final String CONTEXT_PRIV_TaC__INSERT = "INSERT";
    public static final String CONTEXT_PRIV_TaC__SELECT = "SELECT";
    public static final String CONTEXT_PRIV_TaC__UPDATE = "UPDATE";
    public static final String CONTEXT_PRIV_TAB__CREA_TEMP = 
            "CREATE TEMPORARY TABLES";
    public static final String CONTEXT_PRIV_TAB__TRIGGER = "TABLES";
    public static final String CONTEXT_PRIV_VIW__CREA_VIEW = "CREATE VIEW";
    public static final String CONTEXT_PRIV_VIW__SHOW_VIEW = "SHOW VIEW";
    public static final String CONTEXT_PRIV_SDR__ALT_ROUT = "ALTER ROUTINE";
    public static final String CONTEXT_PRIV_SDR__CREA_ROUT = "CREATE ROUTINE";
    public static final String CONTEXT_PRIV_SDR__EXECUTE = "EXECUTE";
    public static final String CONTEXT_PRIV_FAS__FILE ="FILE";
    public static final String CONTEXT_PRIV_ADM__CREA_TABLESPACE = 
            "CREATE TABLESPACE";
    public static final String CONTEXT_PRIV_ADM__CREA_USER = "CREATE USER";
    public static final String CONTEXT_PRIV_ADM__PROCESS = "PROCESS";
    public static final String CONTEXT_PRIV_ADM__PROXY = "PROXY";
    public static final String CONTEXT_PRIV_ADM__RELOAD = "RELOAD";
    public static final String CONTEXT_PRIV_ADM__REPL_CLIENT = 
            "REPLICATION CLIENT";
    public static final String CONTEXT_PRIV_ADM__REPL_SLAVE = 
            "REPLICATION_SLAVE";
    public static final String CONTEXT_PRIV_ADM__SHOW_DATABASE = 
            "SHOW DATABASES";
    public static final String CONTEXT_PRIV_ADM__SHUTDOWN = "SHUTDOWN";
    public static final String CONTEXT_PRIV_ADM__SUPER = "SUPER";
    public static final String CONTEXT_PRIV_ADM__ALL= "ALL PRIVILEGES";
    public static final String CONTEXT_PRIV_ADM__ALL_SHORTEN= "ALL";
    public static final String CONTEXT_PRIV_ADM__USAGE = "USAGE";
    
    public static final String MAX_QUERIES_PER_HOUR = " WITH MAX_QUERIES_PER_HOUR ";
    public static final String MAX_UPDATES_PER_HOUR = " WITH MAX_UPDATES_PER_HOUR ";
    public static final String MAX_CONNECTIONS_PER_HOUR = " WITH MAX_CONNECTIONS_PER_HOUR ";
    public static final String MAX_USER_CONNECTIONS = " WITH MAX_USER_CONNECTIONS ";
    public static final String PASSWORD = " IDENTIFIED BY ";
    
    public abstract void addWorkToBackground(WorkProc userBackWork);
    
    /**
     * This string will hold this users non privileges, this will be crucial
     * only if this user is not an administrator, and this user is equal to 
     * the currently logged in mysql user (current_user).
     */
    private String nonPrivs;
    
    private StringBuilder editingQuery = new StringBuilder(100);
    
    /**
     * edittedPrivs is my logic of retriving the selected and 
     * unselected items. This arraylist is populated when an item
     * is changed.
     * 
     */
    private HashMap<ObjectLevelAndGrants,ArrayList<String>> 
            edittedLevelPrivs = new HashMap(); 
    
    
    /**
     * This method gets edited privileges from the user manager
     * and uses it to write mysql query on the specific privilege
     * 
     * @param grantOrRevoke
     * indicate whether to revoke or grant. This works in two ways
     * firs it either grants or revokes the user's privileges on the
     * mysql system.
     * also the second funtionality is to remove the already grant
     * privilege from the list of privileges.
     * for example, you put in "grant select on sakila.actor;"
     * now you can undo it by removing this line from the query list
     * so it does not get executed, its different from revoke statement
     * in the second functionality.
     * @param database
     * the database to which this privilege belongs or *
     * @param table
     * the table to which this privilege belongs or *
     * @param privs 
     * The privs for these objects.
     */
    public void passOnEdittedPrivs(boolean grantOrRevoke,String database,
            String table, String[] privs,boolean withGrants){
        ObjectLevelAndGrants check = new ObjectLevelAndGrants(
            database,table, (withGrants ? "GRANT OPTION" : ""));
        
        if(grantOrRevoke){
            addEdittedPrivs(check, privs);
        }else{            
            boolean callremove = true;
            for(ObjectLevelAndGrants olag : edittedLevelPrivs.keySet()){
                if(check.equals(olag)){
                    callremove = false;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() { 
                            edittedLevelPrivs.remove(olag);
                        }
                    });
                    break;
                }
            }
            if(callremove){
                removeEdittedPrivs(check, privs);
            }
        }
    }
    
    /**
     * THis is for alter statement
     * @param privs 
     */
    public void passOnEdittedPrivs(String... privs) {
        ObjectLevelAndGrants check = new ObjectLevelAndGrants("*", "*", "");

        ArrayList<String> queries = new ArrayList(3);
        queries.add(NON_PRIV);
        if (privs != null) {
            for (String s : privs) {
                queries.add(s);
            }
        }
        edittedLevelPrivs.put(check, queries);
    }
    
    private final String NON_PRIV= "NP",GRANT = "GRANT",
            REVOKE = "REVOKE",ALTER_USER = "ALTER USER";
    
    
    private void addEdittedPrivs(ObjectLevelAndGrants check,
            String[] privs){
        ArrayList<String> queries = new ArrayList(privs.length+1);
        queries.add(GRANT);
        for(String s : privs){
             queries.add(s);
        }
        edittedLevelPrivs.put(check,queries);        
    }
    
    private void removeEdittedPrivs(ObjectLevelAndGrants check,
            String[] privs){
        ArrayList<String> queries = new ArrayList(privs.length);
        queries.add(REVOKE);
        for(String s : privs){
             queries.add(s);
        }
        edittedLevelPrivs.put(check,queries); 
    }
    
    private class ObjectLevelAndGrants {
        
        public boolean isGlobalPriv = false,
                hasGrants = false,
                isAllObjectPriv = false;
        public String database = "",table = "";  
        
        public ObjectLevelAndGrants(String... inputs){
            if(inputs != null){//for database
                if(inputs[0] != null){
                    database = inputs[0];
                    if(inputs[0].trim().equalsIgnoreCase("*")){
                        isGlobalPriv = true;
                    }
                }
                
                if(inputs.length > 2 && inputs[2] != null && 
                        inputs[2].equalsIgnoreCase("GRANT OPTION")){
                    hasGrants = true;
                }
                
                if(inputs[1] != null){//for table
                    table = inputs[1];
                    if(isGlobalPriv){
                        //it doesn't make sense to have something like
                        //this *.sakila
                        isAllObjectPriv = true;
                    }else{
                        if(inputs[1].trim().equalsIgnoreCase("*")){
                            isAllObjectPriv = true;
                        }
                    }
                }
            }
        }
        
        public boolean equals(boolean addGrants,Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ObjectLevelAndGrants other = (ObjectLevelAndGrants) obj;
                       
            return (this.database.toLowerCase().equals(other.database.toLowerCase())
                    && this.table.toLowerCase().equals(other.table.toLowerCase())
                    && (addGrants ? this.hasGrants == other.hasGrants : true));
        }
        
         @Override
        public boolean equals(Object obj) {
            return equals(true, obj);
        }
        
    }
    
    @Override
        public boolean equals(Object obj) {
            if (obj instanceof User) {
                User us = (User)obj;
                return  us.getName().trim().equalsIgnoreCase(this.getName().trim())
                        &&
                        (us.getHost().trim().equalsIgnoreCase(this.getHost().trim())
                        ||
                        (us.getHost().trim().equals("%") || this.getHost().trim().
                        equals("%")));
            }
            return false;
        }
        
        /**
         * This collection is in the format as key/value
         * The key is a class of the [database,table] and whether is a global
         * priv or objectlevel global priv.
         * 
         * The value is an another key/value collection.
         * The Key is the privilege whether select/insert etc
         * 
         * The value is the column the privilege consist of, that is
         * for example, we could have a priv statement like this 
         * Grant select(firstColumn,SecondColumn) , insert on Database.Table
         * 
         * this is where the value being a string array comes in for the value
         * of this hashmap.
         * if the priv specified has a column the the String array will not be 
         * empty, but if it doesn't then the string array value will be null.
         *
         */
        private HashMap<ObjectLevelAndGrants,HashMap<String,String[]>>
                userLevelsPriv = new HashMap();
        
        /**
         * This method is use to populate the user manager's for the end user
         * that is this is the method to call to get the user privs on a particular
         * object level.
         * 
         * For a database level which global object level privileges, the table
         * column section will be null. 
         * 
         * For a table level the database and table should be sepecified and the
         * column be null
         * 
         * For the column all should be specified.
         * 
         * For user global level privilege all privileges will be null. but 
         * this method does not check all parameters to be null, it only checks
         * the database section to be null.
         * 
         * @param DatabaseLevel
         * The database for which you want to retrieve
         * @param TableLevel
         * The table for which you want to retrieve 
         * @param ColumnLevel
         * the column for which you want to retrieve
         * @return 
         */
        public ArrayList<String> getUserLevelPrivileges(String DatabaseLevel, 
                String TableLevel,String ColumnLevel){
            String data = null,tab = null,column = null;
            if(DatabaseLevel == null){
                data = "*";
                tab = "*";
            }else{
                data = DatabaseLevel.trim();
                if(TableLevel == null){
                    tab = "*";
                }else {
                    tab = TableLevel;
                }
                
                if(ColumnLevel != null){
                    column = ColumnLevel;
                }
            }
            HashMap<String,String[]> map  = new HashMap();
            ObjectLevelAndGrants olag = new ObjectLevelAndGrants(data ,tab);
            for(ObjectLevelAndGrants o : userLevelsPriv.keySet()){
                if(olag.equals(false, o)){
                    olag = o;
                    map =userLevelsPriv.get(o);
                    break;
                }
            }
            
             ArrayList<String> toReturn = new ArrayList();
            
            if(olag.isGlobalPriv){ 
                /**
                 * here we convert them to list<string> and return them
                 */
                toReturn.addAll(map.keySet());
                synchronized(disablePrivsOnLevel){
                    disablePrivsOnLevel.clear();
                }
                if(olag.hasGrants){
                    toReturn.add(CONTEXT_PRIV_DTS__GRANT_OPTION);
                }
                return toReturn;
            }else if(olag.isAllObjectPriv){
                /**
                 * here we check global privs, the ones that has grants
                 * for a particular privilege is ignored here and oredered
                 * to be disabled
                 */
                synchronized(disablePrivsOnLevel){
                    disablePrivsOnLevel.clear();
                }
                
                fliterPrivs(map, toReturn,true);
                if(olag.hasGrants){
                    toReturn.add(CONTEXT_PRIV_DTS__GRANT_OPTION);
                }
                return toReturn;
            }else{
                
                /**
                 * here we combine the top two.
                 */
                synchronized(disablePrivsOnLevel){
                    disablePrivsOnLevel.clear();
                }
                
                HashMap<String,?> map2 = new HashMap();
                ObjectLevelAndGrants olag2 = new ObjectLevelAndGrants(
                olag.database,"*");
                
                for(ObjectLevelAndGrants o : userLevelsPriv.keySet()){
                    if(olag2.equals(false, o)){
                        olag2 = o;
                        map2 =userLevelsPriv.get(o);
                        break;
                    }
                }
                
                /**
                 * Here we are nesting or combining the two ifs method
                 * so instead of fliterPrivs taking the list to send to
                 * usermanager, it will take the disablePrivsOnLevel. 
                 */
                fliterPrivs(map2, disablePrivsOnLevel,true);                
                
                if (column != null) {

                } else {
                    fliterPrivs(map, toReturn, false);
                }
                 if(olag.hasGrants){
                    toReturn.add(CONTEXT_PRIV_DTS__GRANT_OPTION);
                }
                return toReturn;
            }
            
        }
        
    private final void fliterPrivs(HashMap<String, ?> map, ArrayList<String> toReturn,
            boolean iterateGlobalLevel) {

        if (iterateGlobalLevel) {
            for (ObjectLevelAndGrants o : userLevelsPriv.keySet()) {
                if (o.isGlobalPriv) {
                    HashMap<String, ?> all = userLevelsPriv.get(o);
                    disablePrivsOnLevel.addAll(all.keySet());
                }
            }
        }
        for (String s : map.keySet()) {
            if (!disablePrivsOnLevel.contains(s)) {
                toReturn.add(s);
            }
        }
    }
        
        private final ArrayList<String> disablePrivsOnLevel = new ArrayList();
        
        public ArrayList<String> getDisablePrivsOnLevel(){
            synchronized(disablePrivsOnLevel){
                return disablePrivsOnLevel;
            }
        }
        
        public void deleteMe(){
        try {
            dbService.execute(dbService.getDeleteUserSQL(toString()));
        } catch (SQLException ex) { System.out.println("cann not drop user"); }
        }
        
        public void undoChanges(){
            /**
             * we do not commit the changes until saveChanges we persit it
             * so undo just re-init this user from the database again.
             */
            userLevelsPriv.clear();
            edittedLevelPrivs.clear();
             addWorkToBackground(userBackWork); 
             editingQuery.delete(0, editingQuery.length());
             System.gc();
        }
        
        public void saveChanges(Runnable runs){
            //here we execute all the queries/string in editingQuery sb
            WorkProc<Boolean> saveBackWork = new WorkProc<Boolean>() {
                
                private void workOnEntity(Map.Entry<ObjectLevelAndGrants, ArrayList<String>> en) {
                    if (en.getValue().get(0).equals(NON_PRIV)) {
                        addNewQueryline(
                                (getAddData() ? "CREATE USER " : ALTER_USER) + User.this.toString(true)
                                + ((en.getValue().size() > 1) ? en.getValue().get(1)
                                        + en.getValue().get(2) : "") + ";");
                        setAdditionalData(false);
                    } else if (en.getValue().get(0).equals(GRANT) || en.getValue().get(0).equals(REVOKE)) {
                        editingQuery.append(en.getValue().get(0));
                        editingQuery.append(" ");
                        editingQuery.append(en.getValue().get(1));
                        editingQuery.append(" ON ");
                        editingQuery.append(en.getKey().database + "." + en.getKey().table);
                        addNewQueryline(" TO " + User.this.toString(true)
                                + (en.getKey().hasGrants ? " WITH GRANT OPTION;" : ";")
                        );
                    }
                }

                @Override
                public void updateUI() {
                
                runs.run();
                    
                }

                @Override
                public void run() {
                    ///////////////////// I ARRANGED THEM /////////////////////////
                    HashMap<ObjectLevelAndGrants, ArrayList<String>> arrangedEdittedLevelPrivs
                            = new HashMap();
                    for (Map.Entry<ObjectLevelAndGrants, ArrayList<String>> en : edittedLevelPrivs.entrySet()) {
                        if (en.getValue().get(0).equals(NON_PRIV)) {
                            arrangedEdittedLevelPrivs.put(en.getKey(), en.getValue());
                        }
                    }
                    
                    for(ObjectLevelAndGrants o : arrangedEdittedLevelPrivs.keySet()){
                        edittedLevelPrivs.remove(o);
                    }
                    
                    /**
                     * Here i am trying clean the non-privs statement and remove bogus queries
                     */
                    
                    ArrayList<ObjectLevelAndGrants> toremove = new ArrayList();
                    for (Map.Entry<ObjectLevelAndGrants, ArrayList<String>> en :
                            arrangedEdittedLevelPrivs.entrySet()) {
                        if(!(en.getValue().size() > 1 && en.getValue().size() % 2 == 1)){
                            toremove.add(en.getKey());
                        }
                    }
                    
                    if (toremove.size() != arrangedEdittedLevelPrivs.size()) {
                        for (ObjectLevelAndGrants og : toremove) {
                            arrangedEdittedLevelPrivs.remove(og);
                        }
                    }

                    toremove.clear();
                    toremove = null;
                    
                    /////////////////////////////////////////////////////////////////
                    for(Map.Entry<ObjectLevelAndGrants, ArrayList<String>> en :
                            arrangedEdittedLevelPrivs.entrySet()){
                        workOnEntity(en);
                    }
                    
                     for(Map.Entry<ObjectLevelAndGrants, ArrayList<String>> en :
                            edittedLevelPrivs.entrySet()){
                        workOnEntity(en);
                    }

                    System.out.println(editingQuery);
                    try {
                        /////////////////////execute here /////////
                        ResultSet rs = (ResultSet)dbService.execute(editingQuery.toString());
                        while(rs.next()){
                            System.out.println(rs.toString());
                        }
                    } catch (SQLException ex) { 
                        ex.printStackTrace();
                    }
                    ///////////////delete here////////
                    edittedLevelPrivs.clear();
                    userLevelsPriv.clear();
                    editingQuery.delete(0, editingQuery.length());
                    System.gc();
                    userBackWork.run();
                    callUpdate = true;
                }
            };
            saveBackWork.setAdditionalData(newUser);
            addWorkToBackground(saveBackWork);
            //last item only when succesfull
            newUser = false;
        }
        
        private void addNewQueryline(String s){
            editingQuery.append(s);
            editingQuery.append("\r\n");
        }
        
        private WorkProc userBackWork = 
                new WorkProc(){

            @Override
            public void updateUI() {}

            @Override
            public void run() {
                try {
                    ResultSet rs;
                    if(isMe){
                        rs = (ResultSet) dbService.execute(dbService.getShowGrantsSQL("CURRENT_USER()"));
                    }else{
                        rs = (ResultSet) dbService.execute(dbService.getShowGrantsSQL(
                        "'"+getName()+"'@'"+getHost()+"'"));
                    }
                    
                    while (rs.next()) {
                        String res = rs.getString(1);

                        /**
                         * Example of the res string sample is
                         *
                         * +--------------------------------------------+ |
                         * Grants for tester@localhost |
                         * +--------------------------------------------+ |
                         * GRANT USAGE ON *.* TO 'tester'@'localhost' |
                         * +--------------------------------------------+
                         *
                         * +------------------------------------------------------------------+
                         * | Grants for developer@% |
                         * +------------------------------------------------------------------+
                         * | GRANT ALL PRIVILEGES ON *.* TO 'developer'@'%' WITH
                         * GRANT OPTION |
                         * +------------------------------------------------------------------+
                         *
                         * +--------------------------------------------------------------------+
                         * | Grants for tester@localhost |
                         * +--------------------------------------------------------------------+
                         * | GRANT USAGE ON *.* TO 'tester'@'localhost' | |
                         * GRANT SELECT (NAME) ON `sakila`.`category` TO
                         * 'tester'@'localhost' | | GRANT SELECT ON
                         * `sakila`.`actor` TO 'tester'@'localhost' |
                         * +--------------------------------------------------------------------+
                         *
                         */
                        
                        
                        String[] key = getForDatabaseTable(
                                res.substring((res.toLowerCase().indexOf(" on ")+4),
                                res.toLowerCase().indexOf(" to ")));
                        
                        StringBuilder option = new StringBuilder();
                        for (int i = (res.toLowerCase().indexOf(" with ") + 6); i < res.length(); i++) {
                            option.append(res.charAt(i));
                        }
                        
                        key = new String[]{ key[0], key[1], option.toString()};
                        
                        userLevelsPriv.put(new ObjectLevelAndGrants(key),
                        getPrivs(res.substring(5, res.toLowerCase().indexOf(" on "))));
                        
                        workWithNonPrivs();
                    }
                } catch (Exception ex) {
                    Logger.getLogger(UserManagerController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            
        };
        
     /**
     * Gives you user resources bundled in a string array.
     *
     *
     * The reason i do 
     * toReturn[n] = ss[i+1]; 
     * where n is the index and i also
     * the loop index; is because the query is in the format
     ** MAX_QUERIES_PER_HOUR 20 MAX_UPDATES_PER_HOUR 10 
     * seperated with whitespaces hence, if i split them i 
     * have 8 items with the odd numbers being the digits. :)
     * i think i can formulate a formulae here, lol 
     *  if(n%2 != 0) we have a digit and the order will be in
     * there respective order.
     *
     * //CODE//// 
     * int k =0; 
     * for(int i =0; i < ss.length; i++){
     *      if(i%2 != 0){
     *          toReturn[k] = ss[i];
     *      } k++;
     * } 
     * this will make the code shorter and will reduce more ifs, and
     * more optimised. 
     * 
     * IMPLEMENT THIS RATHER!
     *
     * 
     * @return the order of return is [ number of queries, number of updates ,
     * number of connection times , number of simultaneous connections]
     */
        public String[] getNonPrivs(){
            String[] toReturn = new String[4];
            if(nonPrivs != null && nonPrivs.isEmpty()){
                System.out.println("NonPrivs is " + nonPrivs);
                String[] ss = nonPrivs.split("//s");
                for(int i =0; i < ss.length; i++){
                    String s = ss[i];                    
                    if(s.equals("MAX_QUERIES_PER_HOUR")){
                        toReturn[0] = ss[i+1];
                    }else if(s.equals("MAX_UPDATES_PER_HOUR")){
                        toReturn[1] = ss[i+1];
                    }else if(s.equals("MAX_CONNECTIONS_PER_HOUR")){
                        toReturn[2] = ss[i+1];
                    }if(s.equals("MAX_USER_CONNECTIONS")){
                        toReturn[3] = ss[i+1];
                    }
                }
            }
            return toReturn;
        }
        
        private void workWithNonPrivs() throws Exception {
            ResultSet rs = (ResultSet) dbService.execute(
                "SHOW CREATE USER CURRENT_USER();");
            rs.next();
            String privs = rs.getString(1);
                privs = privs.substring(privs.indexOf("REQUIRE "),
                        privs.indexOf("PASSWORD EXPIRE"));
                privs = privs.substring(privs.indexOf(" WITH")+6);
                nonPrivs = privs.toUpperCase();
                privs = null;            
        }
        
        private Boolean[] bools;

    /**
     * This method checks the userLevelPriv to see if the user has full admin
     * privileges, like for example "ALL ON *.*" . anything other than this
     * returns false for index[0], and index[1] verifies if the user has grants
     * option meaning he/she can grant other users the privileges it has.
     *
     * @return returns an array of {isFUllPrivileges,canGrantPrivileges};
     */
    public Boolean[] hasFullAdministratorWithGrantPriv() {
        if (bools == null) {
            bools = new Boolean[]{Boolean.FALSE, Boolean.FALSE};
            for (ObjectLevelAndGrants olag : userLevelsPriv.keySet()) {
                if (olag.isGlobalPriv) {
                    for (String s : userLevelsPriv.get(olag).keySet()) {
                        s = s.trim();
                        if (s.equalsIgnoreCase(CONTEXT_PRIV_ADM__ALL)
                                || s.equalsIgnoreCase(CONTEXT_PRIV_ADM__ALL_SHORTEN) ||
                                s.equalsIgnoreCase(CONTEXT_PRIV_ADM__CREA_USER)) {
                            bools[0] = true;
                            break;
                        }
                    }
                }
                if ((!bools[1])
                        && olag.hasGrants) {
                    bools[1] = true;
                    break;
                }
            }
        }
        return bools;
    }
        
        private HashMap<String,String[]> getPrivs(String input) {
            String[] columnsArray = null;
            HashMap<String,String[]> toReturn = new HashMap();
            StringBuilder grant = new StringBuilder(),
                    column = null;
            boolean startColumnSearch = false;
            int max = input.length();
            for (int i = 0; i < max; i++) {
                char c = input.charAt(i);

                if (startColumnSearch) {
                    if (c == 41) {//)
                        startColumnSearch = false;
                        columnsArray = column.toString().split(",");
                    } else {
                        column.append(c);
                    }
                } else {
                    if (c == 44) {//,
                        toReturn.put(grant.toString(), columnsArray);
                        grant.delete(0,grant.length());
                        columnsArray = null;
                    } else if (c == 40) {//(
                        startColumnSearch = true;
                        column = new StringBuilder();
                    } else {
                        grant.append(c);
                    }
                }
                if (i == (max-1) && !grant.toString().isEmpty()) {
                    toReturn.put(grant.toString().trim(),null);
                }
            }

            return toReturn;
        }
        
        public String[] getForDatabaseTable(String input){
            String[] toReturn = new String[2];
            StringBuilder object = new StringBuilder();
            for (int i = 0; i < input.length(); i++) {
                char c = input.charAt(i);
                if (c == '\'' || c == '`') { /*just skip*/} 
                else if (c == '.') {
                    toReturn[0] = object.toString().trim();
                    //moving to table
                    object.delete(0, i);
                } else {
                    object.append(c);
                }
            }
            if(!object.toString().isEmpty()){
                toReturn[1] = object.toString().trim();
            }                
           return toReturn; 
        }
        
        public String getName(){
            return name.get();
        }
        
        public String getPassword(){
            return password.get();
        }
        
        public String getHost(){
            return host.get();
        }
        
        public void setName(String nameI){
            name.set(nameI);
        }
        
        public void setHost(String hostI){
            host.set(hostI);
        }
        
        public void setPassword(String pass){
            password.set(pass);
        }
        
        public volatile SimpleStringProperty name, password,host;
        private volatile boolean newUser = false,
                isMe = false;
        
        public User(){
            name = new SimpleStringProperty("Enter name");
            password = new SimpleStringProperty("");
            host = new SimpleStringProperty("%");
            newUser = true;        
        }
        
        public User(boolean me,DBService service,String nameInput, String passInput,
                String hostInput){
            isMe = me;
            setDBService(service);
            name = new SimpleStringProperty(nameInput);
            password = new SimpleStringProperty(passInput);
            host = new SimpleStringProperty(hostInput);
            addWorkToBackground(userBackWork);
        }
        
        public User(DBService service,String nameInput, String passInput, String hostInput){
            this(false, service, nameInput, passInput, hostInput);
        }
        
        @Override
        public String toString() { 
            return toString(false);
        }

       
        public String toString(boolean addBackTicks) { 
            if (addBackTicks) {
                if (name != null && host != null) {
                    return "'"+ getName() + "'@'" + getHost()+"'";
                }
                return "";
            } else {
                if (name != null && host != null) {
                    return /*"'"+*/ getName() + "@" + getHost()/*+"'"*/;
                }
                return "";
            }
        }
        
        private DBService dbService;
        
        public final void setDBService(DBService service){
            dbService = service;
        }
        
    }