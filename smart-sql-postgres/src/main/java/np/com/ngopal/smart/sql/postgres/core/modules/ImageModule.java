/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.modules;

import java.util.Map;
import javafx.application.Platform;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class ImageModule extends AbstractStandardModule {

    @Override
    public boolean install() {
        return true;

    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) { return true;}

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postConnection(ConnectionSession service, Tab tab) throws Exception {
        Image image = new Image(getClass().getResource("../ui/images/view.png").toExternalForm());
        ImageView view = new ImageView(image);
        view.setFitHeight(200);
        view.setFitWidth(200);
        Platform.runLater(()-> tab.setContent(view) );

    }

    @Override
    public Class getDependencyModuleClass() {
        return ImageModule.class;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }

}
