package np.com.ngopal.smart.sql.ui.controller;

import java.io.File;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.utils.ExportUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.visualexplain.*;
import np.com.ngopal.smart.sql.structure.metrics.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardResourceProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;

/**
 *
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ReportIndexSummaryController extends BaseController implements Initializable, ReportsContract{
    
    @FXML
    private AnchorPane mainUI;
    
    @FXML
    private FilterableTableView tableView;
    
    @FXML
    private Button refreshButton;
    
    @FXML
    private Button exportButton;
    
    private ConnectionSession session;
    
    @FXML
    public void refreshAction(ActionEvent event) {
        
        makeBusy(true);
        
        Thread th = new Thread(() -> {
            if (session != null) {

                try (ResultSet rs = (ResultSet) session.getService().execute(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__REPORT_INDEX_SUMMARY))) {
                    if (rs != null) {
                        List<ReportIndexSummary> list = new ArrayList<>();
                        while (rs.next()) {
                            ReportIndexSummary rid = new ReportIndexSummary(
                                rs.getString(1), 
                                rs.getString(2),
                                rs.getString(3), 
                                rs.getLong(4),
                                rs.getString(5),
                                rs.getLong(6),
                                rs.getString(7),
                                rs.getLong(8),
                                rs.getString(9),
                                rs.getLong(10),
                                rs.getString(11)
                            );  

                            list.add(rid);
                        }

                        tableView.addAndRemoveItems(list, new ArrayList<>(tableView.getSourceData()));
                    }
                } catch (SQLException ex) {
                    ((MainController)getController()).showError("Error", ex.getMessage(), null);
                }
            }
            
            Platform.runLater(() -> makeBusy(false));
        });
        th.setDaemon(true);
        th.start();
    }
    
    @FXML
    public void exportAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export As Xls");
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("XLS", "*.xls"),
            new FileChooser.ExtensionFilter("All files", "*.*")
        );

        File file = fileChooser.showSaveDialog(getStage());
        if (file != null) {
            
            Thread th = new Thread(() -> {
                
                List<String> sheets = new ArrayList<>();
                List<ObservableList<ObservableList>> totalData = new ArrayList<>();
                List<LinkedHashMap<String, Boolean>> totalColumns = new ArrayList<>();
                List<List<Integer>> totalColumnWidths = new ArrayList<>();
                List<byte[]> images = new ArrayList<>();
                List<Integer> heights = new ArrayList<>();
                List<Integer> colspans = new ArrayList<>();
                
                {
                    ObservableList rowsData = FXCollections.observableArrayList();
                    for (ReportIndexSummary rds: (List<ReportIndexSummary>) (List) tableView.getCopyItems()) {
                        ObservableList row = FXCollections.observableArrayList();

                        row.add(rds.getDatabase());
                        row.add(rds.getTableName());
                        row.add(rds.getIndexName());
                        row.add(rds.getRowsSelected());
                        row.add(rds.getSelectLatency());
                        row.add(rds.getRowsInserted());
                        row.add(rds.getInsertLatency());
                        row.add(rds.getRowsUpdated());
                        row.add(rds.getUpdateLatency());
                        row.add(rds.getRowsDeleted());
                        row.add(rds.getDeleteLatency());

                        rowsData.add(row);
                    }

                    List<Integer> columnWidths = new ArrayList<>();
                    LinkedHashMap<String, Boolean> columns = new LinkedHashMap<>();

                    columns.put("Table Schema", true);
                    columnWidths.add(100);

                    columns.put("Table name", true);
                    columnWidths.add(100);

                    columns.put("Index Name", true);
                    columnWidths.add(100);

                    columns.put("Rows Selected", true);
                    columnWidths.add(60);

                    columns.put("SELECT Latency", true);
                    columnWidths.add(90);
                    
                    columns.put("Rows Inserted", true);
                    columnWidths.add(60);
                    
                    columns.put("INSERT Latency", true);
                    columnWidths.add(90);
                    
                    columns.put("Rows Updated", true);
                    columnWidths.add(60);
                    
                    columns.put("UPDATE Latency", true);
                    columnWidths.add(90);
                    
                    columns.put("Rows Deleted", true);
                    columnWidths.add(60);
                    
                    columns.put("DELETE Latency", true);
                    columnWidths.add(90);
                                        
                    sheets.add("Indexes  Summary Report");
                    totalData.add(rowsData);
                    totalColumns.add(columns);
                    totalColumnWidths.add(columnWidths);
                    images.add(null);
                    heights.add(null);
                    colspans.add(null);
                }                
                
                ExportUtils.exportXLS(file.getAbsolutePath(), images, heights, colspans, sheets, totalData, totalColumns, totalColumnWidths, null, null, "UTF-8", new ExportUtils.ExportStatusCallback() {
                    @Override
                    public void error(String errorMessage, Throwable th) {
                        Platform.runLater(() -> ((MainController)getController()).showError("Error", errorMessage, th, getStage()));
                    }

                    @Override
                    public void success() {
                        Platform.runLater(() -> ((MainController)getController()).showInfo("Success", "Data export seccessfull", null, getStage()));
                    }
                });
            });
            th.setDaemon(true);
            th.start();
        }
    }
    
    
    public void init(ConnectionSession session) {
        this.session = session;
        refreshAction(null);
    }
    
    
    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }
    
    @Override
    public BaseController getReportController() {
        return this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainUI.getStylesheets().clear();
        mainUI.getStylesheets().add(StandardResourceProvider.getInstance().getDependenceStylesheet());
        ((ImageView)refreshButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getRefresh_image());
        ((ImageView)exportButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getXlsSmall_image());
        TableColumn databaseColumn = createColumn("Table Schema", "database");        
        TableColumn tableNameColumn = createColumn("Table name", "tableName");        
        TableColumn indexNameColumn = createColumn("Index Name", "indexName");
        TableColumn rowsSelectedColumn = createColumn("Rows Selected", "rowsSelected");
        TableColumn selectLatencyColumn = createColumn("SELECT Latency", "selectLatency");        
        TableColumn rowsInsertedColumn = createColumn("Rows Inserted", "rowsInserted");
        TableColumn insertLatencyColumn = createColumn("INSERT Latency", "insertLatency");
        TableColumn rowsUpdatedColumn = createColumn("Rows Updated", "rowsUpdated");
        TableColumn updateLatencyColumn = createColumn("UPDATE Latency", "updateLatency");
        TableColumn rowsDeletedColumn = createColumn("Rows Deleted", "rowsDeleted");
        TableColumn deleteLatencyColumn = createColumn("DELETE Latency", "deleteLatency");
        
        tableView.addTableColumn(databaseColumn, 0.1);
        tableView.addTableColumn(tableNameColumn, 0.1);
        tableView.addTableColumn(indexNameColumn, 0.1);
        tableView.addTableColumn(rowsSelectedColumn, 0.07);
        tableView.addTableColumn(selectLatencyColumn, 0.08);
        tableView.addTableColumn(rowsInsertedColumn, 0.07);
        tableView.addTableColumn(insertLatencyColumn, 0.08);
        tableView.addTableColumn(rowsUpdatedColumn, 0.07);
        tableView.addTableColumn(updateLatencyColumn, 0.08);
        tableView.addTableColumn(rowsDeletedColumn, 0.07);
        tableView.addTableColumn(deleteLatencyColumn, 0.08);
        
        tableView.getTable().getSelectionModel().setCellSelectionEnabled(true);
        
        tableView.setSearchEnabled(true);
    }
    
    
    private TableColumn createColumn(String title, String property) {
        TableColumn column = new TableColumn();
        
        Label columnLabel = new Label(title);
        column.getStyleClass().add("analyzerHeaderColumn");
            
        columnLabel.setMaxWidth(Double.MAX_VALUE);
        columnLabel.setTooltip(new Tooltip(title));
        
        column.setGraphic(columnLabel);
        column.setCellValueFactory(new PropertyValueFactory<>(property));
        
        return column;
    }    
    
    private void showSlowQueryData(QueryTableCell.QueryTableCellData cellData, String text, String title) {

        final Stage dialog = new Stage();
        
        SlowQueryTemplateCellController cellController = ((MainController)getController()).getTypedBaseController("SlowQueryTemplateCell");
        cellController.init(text, false);

        final Parent p = cellController.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle(title);

        dialog.showAndWait();
    }
    
    private String calcBlobLength(String text) {
        
        int size = 0;
        if (text != null && !"(NULL)".equals(text)) {
            if (text.startsWith("x'") && text.endsWith("'")) {
                try {
                    size = Hex.decodeHex(text.substring(2, text.length() - 1).toCharArray()).length;
                } catch (DecoderException ex) {
                    log.error("Error", ex);
                }
            } else {
                size = text.getBytes().length;
            }       
        }
         
        if(size <= 0) {
            return "0B";
        }
        
        return NumberFormater.compactByteLength(size);
    }
}
