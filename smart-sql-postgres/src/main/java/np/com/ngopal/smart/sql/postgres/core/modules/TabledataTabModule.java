package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.ui.controller.ResultTabController;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Terah Laweh (rumorsapp@gmail.com);
 * @modify Andii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class TabledataTabModule extends AbstractStandardModule {
    
    @Inject
    private PreferenceDataService preferenceService;

    private Image tableImage = null;
    public static final String TableData_ID = "tableData_BP:2";
    private HashMap<ConnectionSession,Tab> tabHolder= new HashMap();

    
    @Override
    public int getOrder() {
        return 2;
    }
    
    @Override
    public boolean install() {
//        getController()
        return true;
    }
    
    private Object lastSelectedItemWhenTabWasNotSelected = null;
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        Object obj = item.getValue();
        lastSelectedItemWhenTabWasNotSelected = null;

        Tab t = tabHolder.get(getController().
                getSelectedConnectionSession().get());
        if (t.isSelected()) {                
            Platform.runLater(() -> {
                if (obj instanceof TableProvider) {
                    ((ResultTabController) t.getUserData()).resetData();
                    
                    ((ResultTabController) t.getUserData()).
                            respondToTreeViewClicks((TableProvider) obj);
                } else {
                    ((ResultTabController) t.getUserData()).clearGrid();
                }
            });
        } else {
            if (obj instanceof DBTable || obj instanceof View) {
                lastSelectedItemWhenTabWasNotSelected = obj;
            }
        }
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postConnection(ConnectionSession session, Tab t) throws Exception {
        Platform.runLater(() -> {
            ResultTabController resultController = getTypedBaseController("ResultTab");
            resultController.setService(session.getService());
            if (tableImage == null) {
                tableImage = SmartImageProvider.getInstance().getTableTab_image();
            }
            Tab tab = new Tab("Table Data");
            tab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(tableImage), 18, 18));
            tab.setId(TableData_ID);
            tab.setContent(resultController.getUI());
            tab.setClosable(false);
            tab.setUserData(resultController);
            tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov,
                        Boolean t, Boolean t1) {
                    if (t1 && lastSelectedItemWhenTabWasNotSelected != null) {
                        ((ResultTabController) tab.getUserData()).
                                respondToTreeViewClicks((TableProvider) lastSelectedItemWhenTabWasNotSelected);
                        lastSelectedItemWhenTabWasNotSelected = null;
                        
                        resultController.requestFocus();
                    }
                }
            });
            resultController.toggleForTab(false);
            resultController.init(null, null, "", session, false, true, session.getConnectionParam().isWithLimit());
            
            tabHolder.put(session, tab);
            
            reopenTab(session);
            
            resultController.peekTabpane(tab.getTabPane());
            log.debug("Added PostConnection from History module");
        });
    }
    
    public void reopenTab(ConnectionSession session) {
        Tab tab = tabHolder.get(session);
        if (tab != null) {
            
            if (tab.getTabPane() != null) {
                tab.getTabPane().getTabs().remove(tab);
            }
            
            PreferenceData pd = preferenceService.getPreference();
            if (pd.isPositioningTableData()) {
                getController().addTabToBottomPane(session, tab);
            } else {
                getController().addTab(tab, session);
            }
        }
        
    }

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
