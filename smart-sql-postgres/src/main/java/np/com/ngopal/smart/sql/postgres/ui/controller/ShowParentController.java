/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import com.mysql.jdbc.exceptions.jdbc4.MySQLQueryInterruptedException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.structure.components.TableViewCellCopyHandler;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;

/**
 *
 * @author Terah Laweh (rumorsapp@gmail.com)
 */
public class ShowParentController extends BaseController implements Initializable{

    @Override
    public AnchorPane getUI() { 
        return mainUI;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        tableview.getSelectionModel().setCellSelectionEnabled(true);
        
        new TableViewCellCopyHandler<ArrayList<String>>(tableview) {
            @Override
            protected String copyCell(ArrayList<String> row, TableColumn column) {
                switch (column.getText()) {
                    case "Id":
                        return row.get(0);
                    case "User":
                        return row.get(1);
                    case "Host":
                        return row.get(2);
                    case "db":
                        return row.get(3);
                    case "Command":
                        return row.get(4);
                    case "Time":
                        return row.get(5);
                    case "State":
                        return row.get(6);
                    case "Info":
                        return row.get(7);
                }
                return "";
            }
        };
        
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                dialog.hide();
            }
        });

        refreshButton.visibleProperty().bind(refreshButton.disabledProperty().not());
        
        
        killProcessButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                killProcessButton.setDisable(true);
                String id = tableview.getSelectionModel().getSelectedItem().get(0);
                Recycler.addWorkProc(new WorkProc() {
                    Exception ex;
                    @Override
                    public void updateUI() {
                        if (ex != null) {
                            if(ex instanceof MySQLQueryInterruptedException){
                                ((MainController)getController()).showError("PROCESS ERROR", "Can not kill main process...", ex);
                            }
                        } else {
                            refreshButton.fire();
                            killProcessButton.setDisable(false);                            
                        }
                    }
                    
                    @Override
                    public void run() { 
                        callUpdate = true;
                        try {
                            Integer integer = (Integer) getController().getSelectedConnectionSession().get()
                                    .getService().execute(
                                            "KILL " + id
                                    );                           
                        } catch (Exception e) {
                            ex = e;
                        }                        
                    }
                });
            }
        });

    }
    
    public static enum  ShowMenuItems {
        Variable, Processlist, Status;
    }

    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button refreshButton;

    @FXML
    private Button okButton;

    @FXML
    private Button killProcessButton;
    
    @FXML
    private TableView<ArrayList<String>> tableview;

    private Stage dialog;

    /**
     * 
     * @param win
     * @param showmenutype 
     * This will distinguish between sub menu items and how to react to them
     */
    

    public void init(Window win, ShowMenuItems showmenutype, ConnectionSession session) {
        
        dialog = new Stage(StageStyle.UTILITY);
        dialog.setWidth(550);
        dialog.setResizable(false);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(win);        
        dialog.setScene(new Scene(getUI()));        
        switch(showmenutype){
            case Variable:
                dialog.setTitle("SERVER VARIABLES");
                executionCode(session,"show variables;");
                killProcessButton.setVisible(false);
                break;
            case Processlist:
                dialog.setTitle("SERVER PROCESSLIST");
                executionCode(session, "show full processlist;");
                break;
            case Status:
                dialog.setTitle("SERVER STATUS");
                executionCode(session,"show status;");
                killProcessButton.setVisible(false);
                break;
            default:
                throw new AssertionError(showmenutype.name());
            
        }
        
        
        dialog.showAndWait();
    }
    
    private void executionCode(final ConnectionSession session, final String query){
        
        try {
            ResultSet rs = (ResultSet) session.getService().execute(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                String columnName = rsmd.getColumnName(i);
                TableColumn<ArrayList<String>,String> tc = new TableColumn();
                tc.setText(columnName);
                tc.setId(String.valueOf((i-1)));
                tc.setCellValueFactory(cellValue);               
                tableview.getColumns().add(tc);
            }
            int i;
            while (rs.next()) {
                ArrayList<String> list = new ArrayList<>(rsmd.getColumnCount());
                for (i = 1; i <= rsmd.getColumnCount(); i++) {
                    String str = rs.getString(i);
                    if (str != null) {
                        str = str.replace("\n", " ");
                        str = str.replace("\r", "");
                    }
                    list.add(str);
                }
                tableview.getItems().add(list);
            }
            tableview.refresh();
            
            rs.close();
            if (refreshButton.getOnAction() == null) {
                refreshButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        refreshButton.setDisable(true);
                        tableview.getItems().clear();
                        tableview.getColumns().clear();
                        System.gc();
                        executionCode(session, query);
                        refreshButton.setDisable(false);
                    }
                });
            }
        } catch (SQLException ex) {
            Logger.getLogger(ShowParentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private final Callback<TableColumn.CellDataFeatures<ArrayList<String>, String>, ObservableValue<String>> 
            cellValue = new Callback<TableColumn.CellDataFeatures<ArrayList<String>, String>, ObservableValue<String>>() {
        @Override
        public ObservableValue<String> call(TableColumn.CellDataFeatures<ArrayList<String>, String> p) {
            return new SimpleStringProperty(p.getValue().get(Integer.valueOf(p.getTableColumn().getId())));
        }
    };

}
