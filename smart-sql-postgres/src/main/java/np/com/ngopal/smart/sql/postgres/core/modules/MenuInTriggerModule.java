package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import np.com.ngopal.smart.sql.structure.dialogs.CreateTriggerDialogController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInTriggerModule extends AbstractStandardModule {

    private Image triggerImage;

    private MenuItem alterTrigger;
    private MenuItem dropTrigger;
    private MenuItem renameTrigger;
    private MenuItem createTrigger;
    private MenuItem createAuditTrigger;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    @Override
    public int getOrder() {
        return 5;
    }
    
    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();
        
        alterTrigger = getAlterTriggerMenuItem(null);
        dropTrigger = getDropTriggerMenuItem(null);
        renameTrigger = getRenameTriggerMenuItem(null);
        createTrigger = getCreateTriggerMenuItem(null);
        createAuditTrigger = getCreateAuditTriggerMenuItem(null);
        
        SeparatorMenuItem separate = new SeparatorMenuItem();
        Menu triggerMenu = new Menu("Triggers",
                 new ImageView(SmartImageProvider.getInstance().getTrigger_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        triggerMenu.getItems().addAll(createTrigger, createAuditTrigger, alterTrigger, dropTrigger,
                separate, renameTrigger);
     
        alterTrigger.disableProperty().bind(busy());
        dropTrigger.disableProperty().bind(busy());
        renameTrigger.disableProperty().bind(busy());
        createTrigger.disableProperty().bind(busy());
        createAuditTrigger.disableProperty().bind(busy());
        triggerMenu.disableProperty().bind(busy());
        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{triggerMenu});
        
        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        boolean isTrigger = item.getValue() instanceof Trigger;
        
        alterTrigger.setDisable(!isTrigger);
        dropTrigger.setDisable(!isTrigger);
        renameTrigger.setDisable(!isTrigger);
        
        createTrigger.setDisable(!(isTrigger || item.getValue().equals(ConnectionTabContentController.DatabaseChildrenType.TRIGGERS)));
        createAuditTrigger.setDisable(!(isTrigger || item.getValue().equals(ConnectionTabContentController.DatabaseChildrenType.TRIGGERS)));
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private MenuItem getCreateTriggerMenuItem(ConnectionSession session) {

        MenuItem createTriggerMenuItem = new MenuItem("Create Trigger...",
                new ImageView(SmartImageProvider.getInstance().getCreateTrigger_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createTriggerMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createTriggerMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createTrigger();
            }
        });
        
        return createTriggerMenuItem;
    }
    
    private MenuItem getCreateAuditTriggerMenuItem(ConnectionSession session) {

        MenuItem createAuditTriggerMenuItem = new MenuItem("Create Audit Log Trigger...",
                new ImageView(SmartImageProvider.getInstance().getCreateTrigger_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        createAuditTriggerMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createAuditTrigger();
            }
        });
        
        return createAuditTriggerMenuItem;
    }


    private MenuItem getAlterTriggerMenuItem(ConnectionSession session) {
        MenuItem alterTriggerMenuItem = new MenuItem("Alter Trigger...",
                new ImageView(SmartImageProvider.getInstance().getAlterTrigger_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        alterTriggerMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        alterTriggerMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                alterTrigger();
            }
        });
        
        return alterTriggerMenuItem;
    }
    
    public void alterTrigger() {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        ConnectionSession sessionTemp = getController().getSelectedConnectionSession().get();

        Trigger trigger = (Trigger) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();

        if (triggerImage == null) {
            triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
        }                        

        try {
            String alterTrigger = sessionTemp.getService().getAlterTriggerSQL(trigger.getDatabase().getName(), trigger.getName());
            if (alterTrigger == null) {
                showError("Access denied", "Access denied for altering trigger", null);
            } else {
                queryTabModule.addNamedTabWithContext(
                    sessionTemp, 
                    trigger.getName(), 
                    alterTrigger,
                    QueryTabModule.TAB_USER_DATA__TRIGGER_TAB,
                    null);
            }
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }

    private MenuItem getDropTriggerMenuItem(ConnectionSession session) {

        MenuItem dropTriggerMenuItem = new MenuItem("Drop Trigger...",
                new ImageView(SmartImageProvider.getInstance().getDropTrigger_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        dropTriggerMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropTriggerMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                ConnectionSession sessionTemp = session != null ? session : getController().getSelectedConnectionSession().get();
                
                Trigger trigger = (Trigger) (getController().getSelectedTreeItem(sessionTemp)).get().getValue();

                DialogResponce responce = showConfirm("Drop trigger", 
                    "Do you really want to drop the trigger '" + trigger.getName() + "'?");
                
                if (responce == DialogResponce.OK_YES) {
                    try {
                        // drop trigger
                        sessionTemp.getService().dropTrigger(trigger);
                        LeftTreeHelper.triggerDroped(sessionTemp.getConnectionParam(), getController().getSelectedTreeItem(sessionTemp).get());
                    } catch (SQLException ex) {
                        showError("Error", ex.getMessage(), ex);
                    }
                }
            }
        });
        
        return dropTriggerMenuItem;
    }

    private MenuItem getRenameTriggerMenuItem(ConnectionSession session) {
        /**
         * session should be check for a null reference and reference if needed.
         * Reason for this is because the toolbar menuitem uses this method also
         * and injects null as its session, as its not available by then
         */
        MenuItem renameTriggerMenuItem = new MenuItem("Rename Trigger...", 
                new ImageView(SmartImageProvider.getInstance().getRenameTrigger_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        renameTriggerMenuItem.setAccelerator(KeyCombination.keyCombination("F2"));
        renameTriggerMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                renameTrigger();
            }
        });
        
        return renameTriggerMenuItem;
    }
    
    public void renameTrigger() {
        ConnectionSession sessionTemp = getController().getSelectedConnectionSession().get();

        Trigger trigger = (Trigger) ( getController().getSelectedTreeItem(sessionTemp)).get().getValue();

        String newName = showInput("Rename trigger", "Enter new name of trigger:",trigger.getName());
        if (newName != null && !newName.isEmpty()) {

            try {

                String sql = sessionTemp.getService().getRenameTriggerSQL(trigger, newName);

                if (sql != null) {
                    PreferenceData pd = preferenceService.getPreference();
                    ScriptRunner scriptRunner = new ScriptRunner(sessionTemp.getService().getDB(), pd.isHaltExecutionOnError());
                    scriptRunner.runScript(
                        new StringReader(sql), 
                        -1, 
                        sessionTemp.getConnectionParam().getLimitResult(),
                        new ScriptRunner.ScriptRunnerCallback() {
                            @Override
                            public void started(Connection con, int queriesSize) {
                            }

                            @Override
                            public void queryExecuting(Connection con, QueryResult queryResult) {
                            }

                            @Override
                            public void queryExecuted(Connection con, QueryResult queryResult) {
                            }

                            @Override
                            public void queryError(Connection con, QueryResult queryResult, Throwable th) {
                                showError("Error", th.getMessage(), th);
                            }

                            @Override
                            public boolean isWithLimit(QueryResult queryResult) {
                                return false;
                            }

                            @Override
                            public void finished(Connection con) {
                                getController().refreshTree(sessionTemp, false);
                            }

                            @Override
                            public boolean isNeedStop() {
                                return false;
                            }
                        },
                        false
                    );
                }
            } catch (SQLException | IOException ex) {
                showError("Error", ex.getMessage(), ex);
            }
        }
    }

    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("triggers0", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getCreateTriggerMenuItem(session), getCreateAuditTriggerMenuItem(session)}))), Hooks.QueryBrowser.TRIGGERS);
        map.put(new HookKey("triggerItems0", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getCreateTriggerMenuItem(session),
            getCreateAuditTriggerMenuItem(session),
            getAlterTriggerMenuItem(session), getDropTriggerMenuItem(session)}))), 
                Hooks.QueryBrowser.TRIGGER_ITEM);
        map.put(new HookKey("triggerItems1", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getRenameTriggerMenuItem(session)}))), Hooks.QueryBrowser.TRIGGER_ITEM);
        return map;
    }

    public void createTrigger() {
        
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null) {

            if (triggerImage == null) {
                triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
            }

            Database db = session.getConnectionParam().getSelectedDatabase();

            String dbName = db != null ? db.getName() : "<db-name>";

            queryTabModule.addTemplatedQueryTab(session, (Void t) -> {
                CreateTriggerDialogController contr = StandardBaseControllerProvider.getController("CreateTrigger");
                contr.init(session.getConnectionParam(), db, null, false);

                DialogHelper.show("Create trigger", contr.getUI(), getMainController().getStage());

                if (contr.getResponce() == DialogResponce.OK_YES) {

                    String content = session.getService().getCreateTriggerTemplateSQL(
                        dbName,
                        contr.getTriggerNameValue(),
                        contr.getBeforeAfterValue(),
                        contr.getTriggerEventValue(),
                        contr.getTriggerTableValue(),
                        contr.getComment()
                    );

                    return new Pair<>(contr.getTriggerNameValue(), content);                        
                }                    

                return null;


            }, QueryTabModule.TAB_USER_DATA__TRIGGER_TAB );
        }
    }
    
    public void createAuditTrigger() {
        
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null) {

            if (triggerImage == null) {
                triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
            }

            Database db = session.getConnectionParam().getSelectedDatabase();

            String dbName = db != null ? db.getName() : "<db-name>";

            queryTabModule.addTemplatedQueryTab(session, (Void t) -> {
                CreateTriggerDialogController contr = StandardBaseControllerProvider.getController("CreateTrigger");
                contr.init(session.getConnectionParam(), db, null, true);

                DialogHelper.show("Create Trigger for Audit Logs", contr.getUI(), getMainController().getStage());

                if (contr.getResponce() == DialogResponce.OK_YES) {

                    String content = session.getService().getCreateAuditTriggerTemplateSQL(
                        dbName,
                        contr.getTriggerNameValue(),
                        contr.getBeforeAfterValue(),
                        contr.getTriggerEventValue(),
                        contr.getTriggerTableValue(),
                        contr.getTriggerBody(),
                        contr.getComment()
                    );

                    return new Pair<>(contr.getTriggerNameValue(), content);                        
                }                    

                return null;


            }, QueryTabModule.TAB_USER_DATA__TRIGGER_TAB );
        }
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
