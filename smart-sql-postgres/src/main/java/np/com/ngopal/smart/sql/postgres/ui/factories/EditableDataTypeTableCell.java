/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.factories;

import java.util.Set;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.model.DataType;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLDataType;
import np.com.ngopal.smart.sql.structure.misc.SelectionColumn;
import np.com.ngopal.smart.sql.ui.controller.AlterTableController;
import np.com.ngopal.smart.sql.ui.controller.EnumSetAlterController;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class EditableDataTypeTableCell<S, T> extends TableCell<S, T> {

    private TextField textField;
    private HBox textBox;
    
    private final AlterTableController parent;
    private final ObservableList<SelectionColumn> data;
    private final EnumSetAlterController controller;
    
    private Stage dialog;
    
    private boolean canceled = false;

    public EditableDataTypeTableCell(AlterTableController parent, ObservableList<SelectionColumn> d, EnumSetAlterController controller) {
        this.data = d;
        this.controller = controller;
        this.parent = parent;
        
        setStyle("-fx-padding: 0 0 0 0;");
    }

    @Override
    public void startEdit() {
        
        canceled = false;
                
        if (getItemObject() == null || !getItemObject().hasLength()
                || !isEditable()
                || !getTableView().isEditable()
                || !getTableColumn().isEditable()) {
            return;
        }

        
        if (getItemObject().isEnumOrSet()) {
            showEnumOrSetDialog(getItemObject());
        } else {
            super.startEdit();
            if (isEditing()) {
                if (textField == null) {
                    textField = new TextField();
                    textField.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
//                    textField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//                        System.out.println("TEXT - " + newValue);
//                    });
                    textField.setOnKeyPressed((KeyEvent t) -> {
                        if (t.getCode() == KeyCode.ENTER) {
                            accept();
                        } else if (t.getCode() == KeyCode.ESCAPE) {
                            canceled = true;
                            cancelEdit();
                        }
                    });
                    textField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (!canceled && newValue != null && !newValue) {
                            accept();
                        }
                    });
                    
                    textBox = new HBox(textField);
                    HBox.setHgrow(textField, Priority.ALWAYS);
                    textBox.setFillHeight(true);
                }

                textField.setText(currentTypeText());
                setGraphic(textBox);
                setText(null);

                textField.requestFocus();
                textField.selectAll();
                log.debug("::Select All");
            }
        }
    }
    
    private void accept() {
        DataType type = data.get(getIndex()).getDataType();
        // TODO: Need check
        try {
            
            String text = textField.getText().trim();
            if (text.contains(",")) {
                String[] strs = text.split(",");
                
                type.setLength(Integer.parseInt(strs[0].trim()));
                
                if (type.hasPrecisionInteger()) {
                    type.setPrecision(Integer.parseInt(strs[1].trim()));
                }
            } else {
                type.setLength(Integer.parseInt(text));
                if (type.hasPrecisionInteger()) {
                    type.setPrecision(null);
                }
            }
        } catch (NumberFormatException ex) {
            type.setLength(-1);
        }
        log.debug("Edited:: {}", type.getLength());

        commitEdit((T) type);
    }
    
    private void showEnumOrSetDialog(DataType currentDataType) {
        if (dialog == null) {
            dialog = new Stage();

            final Parent p = controller.getUI();
            dialog.initStyle(StageStyle.UTILITY);

            final Scene scene = new Scene(p);
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getScene().getWindow());
        }
        
        dialog.close();
        
        controller.init(currentDataType.getList());
        dialog.setTitle("SET".equals(currentDataType.getName()) ? "Set Value List" : "Enum Value List");
        
        dialog.showAndWait();
        Set<String> data = controller.getData();
        if (data != null) {
            currentDataType.setList(data);
            setText(currentTypeText());
            
            parent.dataChanged();
        }        
    }
    

    public DataType getItemObject() {
        return getTableRow().getIndex() < data.size() ? (DataType) data.get(getTableRow().getIndex()).getDataType() : null;
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        
        log.debug("Cancel:: {}", getItemObject().getLength());
        setText(currentTypeText());
        setGraphic(null);
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
                
        if (item != null && getTableRow() != null && getIndex() >= 0 && getTableRow().getIndex() < getTableView().getItems().size()) {
            DataType it = (DataType) item;
            String text = "";
            switch (it.getTech()) {
                case MYSQL:
                    MySQLDataType type = (MySQLDataType) item;
                    text = currentTypeText();
            }

            if (isEditing()) {
                if (textField != null) {
                    textField.setText(text);
                }
                setText(null);
                setGraphic(textField);
            } else {

                setText(text);
                setGraphic(null);
            }

        } else {
            setGraphic(null);
            setText("");
        }
    }
    
    private String currentTypeText() {
        if (getItemObject() == null) {
            return "";
        } else {
            
            if (getItemObject().isEnumOrSet()) {
                return enumOrSetToString(getItemObject().getList());
                
            } else if (getItemObject().hasPrecisionInteger()) {
                
                String length = getItemObject().getLength() != null && getItemObject().hasLength() && getItemObject().getLength() >= 0 ? "" + getItemObject().getLength() : "";
                if (!length.isEmpty() && getItemObject().getPrecision() != null) {
                    length += ", " + getItemObject().getPrecision();
                }
                
                return length;
                
            } else {
                return getItemObject().getLength() != null && getItemObject().hasLength() && getItemObject().getLength() >= 0 ? "" + getItemObject().getLength() : "";
            }
        }
    }
    
    private String enumOrSetToString(Set<String> set) {
        if (set != null && !set.isEmpty()) {
            String result = "";
            for (String s: set) {
                result += "'" + s + "', ";
            }

            return result.substring(0, result.length() - 2);
        }
        
        return "";
    }
}
