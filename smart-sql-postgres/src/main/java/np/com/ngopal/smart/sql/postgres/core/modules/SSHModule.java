package np.com.ngopal.smart.sql.modules;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Injector;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.AbstractConnectionModule;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.SSHHotKeyService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.SSHHotKeys;
import np.com.ngopal.smart.sql.ui.controller.NewConnectionController;
import np.com.ngopal.smart.sql.ui.controller.SSHHotKeysController;
import np.com.ngopal.smart.sql.ui.controller.SSHTabContentController;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.structure.dialogs.DialogController;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SSHModule extends AbstractConnectionModule {

    @Inject
    private SSHHotKeyService hotkeyService;

    @Inject
    Injector injector;

    private SSHTabContentController tabController;
    
    private final EventHandler<ActionEvent> addServerAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            showServersAction();
        }
    };

    private final EventHandler<ActionEvent> removeAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            tabController.removeSelectedServer();
        }
    };

    private final EventHandler<ActionEvent> connectAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            tabController.openSelectedServer();
        }
    };

    private final EventHandler<ActionEvent> propertiesAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            showServersAction(tabController.getSelectedServerParams());
        }
    };

    private final EventHandler<ActionEvent> importTreeAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            importConnections();
        }
    };

    private final EventHandler<ActionEvent> exportTreeAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            exportConnections();
        }
    };

    private final EventHandler<ActionEvent> connectToAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            showServersAction(null);
        }
    };

    private final EventHandler<ActionEvent> dublicateAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            tabController.duplicateCurrentTab();
        }
    };

    private final EventHandler<ActionEvent> hotkeysAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            showHotkeysAction();
        }
    };
    
    private final EventHandler<ActionEvent> nextTabAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            tabController.nextTab();
        }
    };
    
    private final EventHandler<ActionEvent> prevTabAction = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            tabController.prevTab();
        }
    };

    private MenuItem connectToMenuItem;
    private MenuItem exportTreeMenuItem;
    private MenuItem importTreeMenuItem;
    private MenuItem propertiesMenuItem;
    private MenuItem connectMenuItem;
    private MenuItem removeMenuItem;
    private MenuItem addServerMenuItem;
    private MenuItem dublicateMenuItem;
    private MenuItem hotkeysMenuItem;
    
    private MenuItem nextTabMenuItem;
    private MenuItem prevTabMenuItem;

    @Override
    public boolean install() {
        connectToMenuItem = createMenuItem("Connect to", "/np/com/ngopal/smart/sql/ui/images/ssh/connect_to.png", connectToAction);

        exportTreeMenuItem = createMenuItem("Export Tree", "/np/com/ngopal/smart/sql/ui/images/ssh/export.png", exportTreeAction);
        importTreeMenuItem = createMenuItem("Import Tree", "/np/com/ngopal/smart/sql/ui/images/ssh/import.png", importTreeAction);

        propertiesMenuItem = createMenuItem("Properties", "/np/com/ngopal/smart/sql/ui/images/ssh/properties.png", propertiesAction);
        propertiesMenuItem.setDisable(true);
        connectMenuItem = createMenuItem("Connect", "/np/com/ngopal/smart/sql/ui/images/ssh/connect.png", connectAction);
        connectMenuItem.setDisable(true);
        removeMenuItem = createMenuItem("Remove", "/np/com/ngopal/smart/sql/ui/images/ssh/remove.png", removeAction);
        removeMenuItem.setDisable(true);
        addServerMenuItem = createMenuItem("Add server", "/np/com/ngopal/smart/sql/ui/images/ssh/add_server.png", addServerAction);

        dublicateMenuItem = createMenuItem("Dublicate", "/np/com/ngopal/smart/sql/ui/images/ssh/duplicate.png", dublicateAction);
        dublicateMenuItem.setDisable(true);

        hotkeysMenuItem = createMenuItem("Hotkes", "/np/com/ngopal/smart/sql/ui/images/ssh/hotkeys.png", hotkeysAction);

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{connectToMenuItem});
        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{exportTreeMenuItem, importTreeMenuItem});
        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{propertiesMenuItem, connectMenuItem, removeMenuItem, addServerMenuItem});

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{dublicateMenuItem, hotkeysMenuItem});
        
        
        nextTabMenuItem = createMenuItem("Next tab", null, nextTabAction);
        prevTabMenuItem = createMenuItem("Previous tab", null, prevTabAction);
        
        return true;
    }
    
    private void checkHotkeys(KeyEvent event) {
        if (!connectToMenuItem.isDisable() && connectToMenuItem.getAccelerator().match(event)) {
            connectToMenuItem.fire();
            event.consume();
        }        
        if (!exportTreeMenuItem.isDisable() && exportTreeMenuItem.getAccelerator().match(event)) {
            exportTreeMenuItem.fire();
            event.consume();
        }        
        if (!importTreeMenuItem.isDisable() && importTreeMenuItem.getAccelerator().match(event)) {
            importTreeMenuItem.fire();
            event.consume();
        }
        
        if (!propertiesMenuItem.isDisable() && propertiesMenuItem.getAccelerator().match(event)) {
            propertiesMenuItem.fire();
            event.consume();
        }
        if (!connectMenuItem.isDisable() && connectMenuItem.getAccelerator().match(event)) {
            connectMenuItem.fire();
            event.consume();
        }
        if (!removeMenuItem.isDisable() && removeMenuItem.getAccelerator().match(event)) {
            removeMenuItem.fire();
            event.consume();
        }        
        if (!addServerMenuItem.isDisable() && addServerMenuItem.getAccelerator().match(event)) {
            addServerMenuItem.fire();
            event.consume();
        }
        
        if (!dublicateMenuItem.isDisable() && dublicateMenuItem.getAccelerator().match(event)) {
            dublicateMenuItem.fire();
            event.consume();
        }        
        if (!hotkeysMenuItem.isDisable() && hotkeysMenuItem.getAccelerator().match(event)) {
            hotkeysMenuItem.fire();
            event.consume();
        }
        
        if (!nextTabMenuItem.isDisable() && nextTabMenuItem.getAccelerator().match(event)) {
            nextTabMenuItem.fire();
            event.consume();
        }
        if (!prevTabMenuItem.isDisable() && prevTabMenuItem.getAccelerator().match(event)) {
            prevTabMenuItem.fire();
            event.consume();
        }
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) { return true;}
    
    public void updateHotKeys(final SSHHotKeys hotkeys) {
        applyHotKey(connectToMenuItem, hotkeys.getConnectTo());

        applyHotKey(exportTreeMenuItem, hotkeys.getExportTree());
        applyHotKey(importTreeMenuItem, hotkeys.getImportTree());

        applyHotKey(propertiesMenuItem, hotkeys.getPropertiesOfSelected());
        applyHotKey(connectMenuItem, hotkeys.getConnectToSelected());
        applyHotKey(removeMenuItem, hotkeys.getRemoveServer());
        applyHotKey(addServerMenuItem, hotkeys.getAddServer());

        applyHotKey(dublicateMenuItem, hotkeys.getDuplicateConnection());
        applyHotKey(hotkeysMenuItem, hotkeys.getRemapHotkeys());
        
        applyHotKey(nextTabMenuItem, hotkeys.getNextServerTab());
        applyHotKey(prevTabMenuItem, hotkeys.getPrevServerTab());
    }

    private void applyHotKey(final MenuItem menuitem, final String hotkey) {
        if (!hotkey.equalsIgnoreCase("No")) {
            final List<KeyCombination.Modifier> modifiers = new ArrayList<>();
            if (hotkey.contains("Shift + ")) {
                modifiers.add(KeyCombination.SHIFT_DOWN);
            }
            if (hotkey.contains("Ctrl + ")) {
                modifiers.add(KeyCombination.CONTROL_DOWN);
            }
            if (hotkey.contains("Alt + ")) {
                modifiers.add(KeyCombination.ALT_DOWN);
            }

            menuitem.setAccelerator(
                    new KeyCodeCombination(
                            KeyCode.valueOf(hotkey.substring(hotkey.lastIndexOf("+") + 1).trim().toUpperCase()),
                            modifiers.toArray(new KeyCombination.Modifier[]{})
                    )
            );
        }
    }
    
    private void initToollbar(){
                
        final List<SSHHotKeys> hotkeys = hotkeyService.getAll();
        if (hotkeys != null && !hotkeys.isEmpty()) {
            updateHotKeys(hotkeys.get(0));
        }
               
    }

    private MenuItem createMenuItem(final String name, String image, final EventHandler<ActionEvent> action) {
        final MenuItem menuItem = image != null ? new MenuItem(name, new ImageView(new Image(image))) : new MenuItem(name);
        menuItem.setOnAction(action);

        return menuItem;
    }

    private void showServersAction() {
        showServersAction(null);
    }

    private void showServersAction(final ConnectionParams params) {
        final Stage dialog = new Stage();
        
        final NewConnectionController controller = getTypedBaseController("NewConnection");
        if (params != null) {
            controller.selectSavedConnection(params);
        }
//        else {
//            controller.selectNewConnection("New connection");
//        }
        
        controller.selectModule(SSHModule.class);
        
        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Add new server");

        dialog.show();
    }

    private void showHotkeysAction() {
        final Stage dialog = new Stage();

        final SSHHotKeysController controller = getTypedBaseController("SSHHotKeys");
        controller.setSshModule(this);

        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Hot keys");

        dialog.show();
    }

    private final String desspec = "e34e7514-5738-11e5-885d-feff819cdc9f";

    private void exportConnections() {
        final ConnectionParamService db = injector.getInstance(ConnectionParamService.class);

        final Gson gson = new Gson();
        final List<ConnectionParams> params = db.getAll();
        
        List<ConnectionParams> forExport = new ArrayList<>();
        if (params != null) {
            for (ConnectionParams p: params) {
                forExport.add(p.copyOnlyParams());
            }
        }
        
        final String json = gson.toJson(forExport);

        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select path to save exported connections");
        fileChooser.setInitialFileName("exportConnections.ssc");

        final File exportFile = fileChooser.showSaveDialog(getController().getStage());
        if (exportFile != null) {
            try {
                // encrypt data
                final DESKeySpec desKeySpec = new DESKeySpec(desspec.getBytes());
                final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
                final SecretKey secretKey = keyFactory.generateSecret(desKeySpec);

                final Cipher encryptCipher = Cipher.getInstance("DES");
                encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);

                byte[] bytes = encryptCipher.doFinal(json.getBytes("UTF-8"));

                Files.write(exportFile.toPath(), bytes, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (Exception ex) {
                log.error("Error while exporting connections", ex);
            }
        }
    }

    private void importConnections() {
        final ConnectionParamService db = injector.getInstance(ConnectionParamService.class);

        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select exported connections for import");

        final File exportFile = fileChooser.showOpenDialog(getController().getStage());
        if (exportFile != null) {
            try {
                final byte[] encryptBytes = Files.readAllBytes(exportFile.toPath());

                // decrypt data
                final DESKeySpec desKeySpec = new DESKeySpec(desspec.getBytes());
                final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
                final SecretKey secretKey = keyFactory.generateSecret(desKeySpec);

                final Cipher decryptCipher = Cipher.getInstance("DES");
                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);

                final byte[] bytes = decryptCipher.doFinal(encryptBytes);

                final String json = new String(bytes, "UTF-8");
                final Gson gson = new Gson();

                final List<ConnectionParams> params = gson.fromJson(json, new TypeToken<List<ConnectionParams>>() {
                }.getType());
                if (params != null && !params.isEmpty()) {
                    
                    DialogResponce responce = showConfirm("Import", 
                        "Do you want remove the old connection?\nPresss 'Yes' to remove, 'No' to update old connection or 'Cancel' for cancel import");
                    
                    if (responce == DialogResponce.OK_YES) {
                        final List<ConnectionParams> oldParams = db.getAll();
                        if (oldParams != null) {
                            for (final ConnectionParams p : oldParams) {
                                db.delete(p);
                            }
                        }

                        save(db, params);
                    } else if (responce == DialogResponce.NO) {
                        save(db, params);
                    }
                }
            } catch (Exception ex) {
                showError("Error", "Error occurred while exporting connections", ex);
            }
        }
    }
    
    private void save(ConnectionParamService db, List<ConnectionParams> params) {
        for (final ConnectionParams p : params) {
            // Gson return HashTreeMap
            p.setModuleParams(new HashMap<>(p.getModuleParams()));
            db.save(p);
        }

        // need close or opened tab controllers
        getController().closeAllTabs();

        tabController = null;

        propertiesMenuItem.disableProperty().unbind();
        connectMenuItem.disableProperty().unbind();
        removeMenuItem.disableProperty().unbind();
        dublicateMenuItem.disableProperty().unbind();
//        connectButton.disableProperty().unbind();

        propertiesMenuItem.setDisable(true);
        connectMenuItem.setDisable(true);
        removeMenuItem.setDisable(true);
        dublicateMenuItem.setDisable(true);
//        connectButton.setDisable(true);
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postConnection(ConnectionSession service, Tab t) throws Exception {
        Platform.runLater(() -> {
            initToollbar();

            getController().getStage().addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    checkHotkeys(event);
                }
            });

            if (tabController == null) {
                tabController = (SSHTabContentController) t.getUserData();

                propertiesMenuItem.disableProperty().bind(tabController.serverSelectedProperty().not());
                connectMenuItem.disableProperty().bind(tabController.serverSelectedProperty().not());
                removeMenuItem.disableProperty().bind(tabController.serverSelectedProperty().not());
                dublicateMenuItem.disableProperty().bind(tabController.tabSelectedProperty().not());

//            connectButton.disableProperty().bind(tabController.serverSelectedProperty().not());
            }
        });
    }

    @Override
    public int getOrder() {
        return 5;
    }
    
    @Override
    public boolean isConnection() {
        return true;
    }
    
    @Override
    public Class getDependencyModuleClass() {
        return SSHModule.class;
    }
    
    @Override
    public Map<?, ?> getPersistingMap() { 
      throw new UnsupportedOperationException("Not supported yet.");
    }

//    @Override
//    public boolean onRestore(Map<?, ?> restoreImage) {
//        return false;
//    }
    
    @Override
    public String getInfo() { 
        return "I do not know how to describe this Sir";
    }
}
