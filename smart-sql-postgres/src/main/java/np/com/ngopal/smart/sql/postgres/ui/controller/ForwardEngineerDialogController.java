package np.com.ngopal.smart.sql.ui.controller;

import com.google.inject.Inject;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.SchemaDesignerUtil;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.structure.components.*;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.factories.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.*;

/**
 * @author Andry Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class ForwardEngineerDialogController extends BaseController implements Initializable {
    
    @FXML
    private AnchorPane mainUI;
    
    @Inject
    private PreferenceDataService preferenceService;
    
    @FXML
    private ComboBox<Database> databaseComboBox;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label statusFieldLabel;
    @FXML
    private Button importButton;
    
    private volatile boolean importRunned = false;
    private volatile boolean stopImport = false;
    
    private DBService service;
    private SchemaDesignerTabContentController designerController;
    
    public void init(DBService service, SchemaDesignerTabContentController designerController, String selectedDatabase) {
        this.service = service;
        this.designerController = designerController;
        
        databaseComboBox.getItems().clear();
        progressBar.setProgress(0);
        
        importButton.setText("Import");
        importButton.setDisable(true);
        
        stopImport = false;
        importRunned = false;
        
        try {    
            databaseComboBox.getItems().setAll(service.getDatabases());
            databaseComboBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Database> observable, Database oldValue, Database newValue) -> {
                importButton.setDisable(newValue == null);
            });
            
            DesignerErrDiagram diagram = designerController.getCurrentDiagram();
            if (diagram != null && diagram.database().get() != null)  {
                for (Database d: databaseComboBox.getItems()) {
                    if  (d.getName().equals(diagram.database().get())) {
                        databaseComboBox.getSelectionModel().select(d);
                    }
                }
            }
        } catch (SQLException ex) {
            applyError("Error on loading databases list");
            log.error("Error on loading databases list", ex);
        }
        
        if (selectedDatabase != null) {
            for (Database db: databaseComboBox.getItems()) {
                if (selectedDatabase.equals(db.getName())) {
                    databaseComboBox.getSelectionModel().select(db);
                    break;
                }
            }
                
            importAction(null);
        }
    }
    
    
    private void forwardEngeen() {
        try {
            applyStatus("Preparing data", 0.1);
            Database database = databaseComboBox.getSelectionModel().getSelectedItem();
            DesignerErrDiagram diagram = designerController.getCurrentDiagram();
            if (diagram != null) {
                
                SchemaDesignerUtil.forwardEngeen(
                    service, 
                    getController(), 
                    database.getName(), 
                    diagram, 
                    this::applyStatus,
                    v -> {
                        stopImport = true;
                        importButton.setDisable(false);
                    }, 
                    v -> progressBar.getProgress()
                );                
            }
        } catch (Throwable ex) {
            log.error(ex.getMessage(), ex);
//            Platform.runLater(() -> {
//               showError("Error forward engineer", "Error executing query: " + queryResult.getQuery(), ex); 
//            });
        }
    }
    
    
    
    private void applyStatus(String statusText, double progress) {
        Platform.runLater(() -> {
            statusFieldLabel.setText(statusText);
            statusFieldLabel.setStyle("-fx-text-fill: black");
            progressBar.setProgress(progress);
        });
    }
    
    private void applyError(String errorText) {
        Platform.runLater(() -> {
            statusFieldLabel.setText(errorText);
            statusFieldLabel.setStyle("-fx-text-fill: red");
        });
        
        importRunned = false;
        stopImport = true;
    }
    
    @FXML
    public synchronized void importAction(ActionEvent event) {
        if (importRunned) {
            if (stopImport) {
                cancelAction(event);
            }
        } else {
            importRunned = true;
            stopImport = false;
            
            databaseComboBox.setDisable(true);
                    
            importButton.setText("Done");
            importButton.setDisable(true);
            progressBar.setProgress(-1);
            
            
            applyStatus("Starting", 0.1);
            Thread th = new Thread(() -> forwardEngeen());
            th.setDaemon(true);
            th.start();
        }
    }
    
    @FXML
    public void cancelAction(ActionEvent event) {
        stopImport = true;
        getStage().close();
    }
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public Stage getStage() {
        return (Stage) mainUI.getScene().getWindow();
    }
}
