package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Consumer;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.Pair;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.utils.DBElementWrapperOnBuilder;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.TableProvider;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.ui.components.QueryBuilderRowData;
import np.com.ngopal.smart.sql.ui.factories.EditingCell;
import np.com.ngopal.smart.sql.ui.provider.SmartBaseControllerProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DatabaseCache;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * 
 * !!!! This class has many many bad code
 * !!!! Need refactoring all
 *
 * @author Terah Laweh(rumorsapp@gmail.com)
 */
@Slf4j
public class QueryBuilderController extends BaseController
        implements Initializable {

    private static final Pair<Integer, Integer>[] DEFAULT_POSITIONS = new Pair[]{new Pair(320, 150), new Pair(50, 30), new Pair(550, 30), new Pair(50, 270), new Pair(550, 270)};
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private Button addTableButton;
    
    @FXML
    private CheckBox showCanvasRB;

    @FXML
    private CheckBox showFieldRB;

    @FXML
    private CheckBox showQueryRB;

    @FXML
    private AnchorPane queryAnchor;

    @FXML
    private SplitPane mainSplit;

    @FXML
    private SplitPane splitHolder;

    @FXML
    private BorderPane queryScreenPane;
    private RSyntaxTextArea queryScreen;

    @FXML 
    private Button joinButton;
    
    @FXML
    private VBox footer;

    @FXML
    private ScrollPane canvasScrollPane;
    @FXML
    private Pane queryInUI;

    @FXML
    private HBox footerButtonHolder;

    @FXML
    private TableView<QueryBuilderRowData> tableview;

    @FXML
    private Label notice;

    @FXML
    private Button copyQueryToClipboard;

    @FXML
    private Button copyQueryToNewQueryTab;

    @FXML
    private Button createViewUsingCurrentQuery;
    
    @FXML
    private Button saveQueryBuilderButton;
    
    @FXML
    private Button openQueryBuilderButton;

    private MenuItem mCopyQueryToClipboard;

    private MenuItem mCopyQueryToSameQueryTab;

    private MenuItem mCopyQueryToNewQueryTab;

    private MenuItem mCreateViewUsingCurrentQuery;

    //menu item for the builder workspace
    private MenuItem mAddTables;

    private MenuItem mAddViews;

    private MenuItem mLoadQueryDesign;

    private MenuItem mRemoveAllTables;

    private MenuItem mUP, mDown;
    
    private ContextMenu lineContextMenu;
    private MenuItem deleteJoinItem;
    private Menu joinTypeItem;
    private CheckMenuItem innerItem;
    private CheckMenuItem leftItem;
    private CheckMenuItem rightItem;

    
    private MenuItem cAddTables;

    private MenuItem cAddViews;

    private MenuItem cLoadQueryDesign;
    
    private MenuItem cRemoveAllTables;
    
    @FXML
    private CheckBox useDatabaseNameCheckbox;

    @FXML
    private TableColumn<QueryBuilderRowData, ArrayList<SimpleStringProperty>> moreColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, Boolean> groupColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, String> ageFunctionColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, String> sortColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, Boolean> showColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, String> aliasColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, String> fieldColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, String> tableColumn;

    @FXML
    private TableColumn<QueryBuilderRowData, String> whereConditionColumn;

    @FXML
    private ScrollPane queryScreenScroll;

    @FXML
    private Button addColumnButton;

    /**
     * This is the database name with the text; Database: (databasename);
     */
    @FXML
    private Label databaseName;
    
    @FXML
    private StackPane controllerPane;

    public ConnectionSession session;
    
    
    @Inject
    private PreferenceDataService preferenceService;    
    @Inject
    public SmartBaseControllerProvider provider;

    private SimpleObjectProperty<Database> databaseInUse = new SimpleObjectProperty<Database>(null) {
        @Override
        protected void invalidated() {            
            databaseName.setText("Database: " + (get() == null ? "" : get().getName()));
        }
    };

    
//    private Map<TableProvider, List<TableProvider>> map = new HashMap<>();
    
    // List of displayed table
    private ObservableMap<TableProvider, BuilderViewController> viewControllers = FXCollections.observableMap(new LinkedHashMap<>());
    
    private List<Line> tableLines = new ArrayList<>();
    
    private Line selectedLine;
    
    private JoinMaker.JoinResult joinResult = JoinMaker.JoinResult.empty();
    
    public Database getDatabaseInUse() {
        return databaseInUse.get();
    }

    public QueryTabModule queryTabmodule;

    private Consumer dbClickConsumer = (Consumer) (Object obj) -> {

        // make it only for this QueryBuilder tab
        Tab tab = getController().getSelectedConnectionTab(session).get();
        if (tab != null && tab.getContent() == mainUI) {

            if (obj instanceof DBTable) {
                // if table need add table
                System.out.println("adding table");
                
                DBTable table = (DBTable) obj;                
                addTable(table, 0, 0);
            } else if (obj instanceof Column) {
                // if column:
                // 1. if table not exist - add table and select column
                // 2. if table exist - just add column
                System.out.println("adding column");
                addColumn((Column) obj);
            }
        }
    };

    private SimpleBooleanProperty pauseForAll = new SimpleBooleanProperty(false);

    
    @FXML
    public void addTableAction(ActionEvent event) {
        
        Database selectedDatabase = databaseInUse.get();
        if (selectedDatabase == null) {
            
            provider.setInput("ChooseDatabase");
            ChooseDatabaseController cdc =(ChooseDatabaseController) provider.get();
            
            selectedDatabase = cdc.show(session);
        }
        
        try {
            selectedDatabase = DatabaseCache.getInstance().syncDatabase(selectedDatabase.getName(), session.getConnectionParam().cacheKey(), (AbstractDBService) session.getService());
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), ex);
        }

        if (selectedDatabase == null) {
            return;
        }
        
        provider.setInput("DesignerAddTable");
        DesignerAddTableController sdac = (DesignerAddTableController)provider.get();
        
//        String dbName = selectedDatabase.getName();
        
        Database database = selectedDatabase;
        
        sdac.setAddRequest = () -> {            
            
            // its slow but need for getting foreignkeys
//            for (DBElementWrapperOnBuilder d: sdac.tableau) {
//                
//                try {
//                    d.dbe = session.getService().getTableSchemaInformation(dbName, d.dbe.getName());
//                } catch (SQLException ex) {
//                    log.error("Error", ex);
//                }
//            }
            
            List<DBElementWrapperOnBuilder> baseFounded = new ArrayList<>();
            
            List<DBElementWrapperOnBuilder> list = new ArrayList<>(sdac.tableau);
            for (DBElementWrapperOnBuilder b: list) {
                
                if (b.dbe instanceof DBTable && !baseFounded.contains(b)) {
                    
                    DBTable table = (DBTable)b.dbe;
                    Set<String> primarys = new TreeSet<>();
                    for (Column c: table.getColumns()) {
                        if (c.isPrimaryKey()) {
                            primarys.add(c.getName());
                        }
                    }
                    
                    for (DBElementWrapperOnBuilder b0: list) {
                        
                        if (b0.dbe instanceof DBTable && b0 != b && !baseFounded.contains(b0)) {
                            
                            DBTable dragedTable = (DBTable)b0.dbe;
                            
                            if (((DBTable)dragedTable).getForeignKeys() != null) {
                                // if not returned need check for draged table
                                for (ForeignKey fk: ((DBTable)dragedTable).getForeignKeys()) {

                                    // check database and table name
                                    if (Objects.equals(fk.getReferenceDatabase().toLowerCase(), table.getDatabase().getName().toLowerCase()) 
                                        && Objects.equals(fk.getReferenceTable().toLowerCase(), table.getName().toLowerCase())) {

                                        // check primary
                                        if (primarys.size() == fk.getColumns().size()) {
                                            boolean found = true;
                                            for (Column c: fk.getColumns()) {
                                                if (!primarys.contains(c.getName())) {
                                                    found = false;
                                                    break;
                                                }
                                            }

                                            if (found) {
                                               
                                                sdac.tableau.remove(b0);
                                                sdac.tableau.add(0, b0);
                                                
                                                baseFounded.add(b0);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            int size = sdac.tableau.size();
            for (int i = 0; i < size; i++) {
                
                DBElementWrapperOnBuilder d = sdac.tableau.get(i);
                ((DBTable)d.dbe).setDatabase(database);
                
                addTable((DBTable)d.dbe, d.point.getX(), d.point.getY(), i == size - 1);                
            }
        };
        
        sdac.init(selectedDatabase.getName(), DatabaseCache.getInstance().getTables(getController().getSelectedConnectionSession().get().getConnectionParam().cacheKey(), selectedDatabase.getName()), session);
    }   
    
    
    @FXML
    public void joinAction(ActionEvent event) {
        doFilter(viewControllers.values().toArray(new BuilderViewController[0])[1], null, true);
    }
    
    
    
    @FXML
    public void addViewAction(ActionEvent event) {
        
        Database selectedDatabase = databaseInUse.get();
        if (selectedDatabase == null) {
            
            provider.setInput("ChooseDatabase");
            ChooseDatabaseController cdc =(ChooseDatabaseController) provider.get();
            
            selectedDatabase = cdc.show(session);
            if (selectedDatabase == null) {
                return;
            }
        }
        
        String dbName = selectedDatabase.getName();
        Database database = selectedDatabase;
        
        provider.setInput("DesignerAddView");
        DesignerAddViewController sdac = (DesignerAddViewController)provider.get();
        sdac.setAddRequest = () -> {
            
            int size = sdac.views.size();
            for (int i = 0; i < size; i++) {
                                
                DBElementWrapperOnBuilder d = sdac.views.get(i);
                try {
                    DBTable table = session.getService().getTableSchemaInformation(dbName, d.dbe.getName());
                    table.setDatabase(database);
                    
                    addTable(table, d.point.getX(), d.point.getY(), i == size - 1);
                } catch (SQLException ex) {
                    log.error("Error", ex);
                }
            }
        };
        
        try {
            sdac.init(selectedDatabase.getName(), session.getService().getViews(selectedDatabase));
        } catch (SQLException ex) {
            log.error("Error", ex);
        }
    }
    
    @FXML
    public void openQueryBuilderAction(ActionEvent event) {
        
    }
    
    @FXML
    public void saveQueryBuilderAction(ActionEvent event) {
        
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("QueryBuilder Files (*.qb)", "*.qb"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        
        fc.setInitialFileName(".qb");
        File qbFile = fc.showSaveDialog(getStage());

        // create JSON data
        Map<String, Object> data = new HashMap<>();
        
        // database
        if (databaseInUse.get() != null) {
            data.put("database", databaseInUse.get().getName());
        }
        
        List<Map<String, Object>> tables = new ArrayList<>();
        for (TableProvider tp: viewControllers.keySet()) {
            
            BuilderViewController bvc = viewControllers.get(tp);
            Map<String, Object> table = new HashMap<>();
            
            table.put("name", tp.getName());
            table.put("alias", tp.getAlias());
            table.put("x", bvc.getX());
            table.put("y", bvc.getY());
            
            tables.add(table);
        }
        
        data.put("tables", tables);
        
        String jsonData = new Gson().toJson(data, HashMap.class);
        try {
            Files.write(qbFile.toPath(), jsonData.getBytes("UTF-8"));
        } catch (IOException ex) {
            ((MainController)getController()).showError("Error", ex.getMessage(), ex);
        }
    }
    
    @FXML
    public void clearAllAction(ActionEvent event) {
        Set<Node> nodes = queryInUI.lookupAll("#closeButton");
        
        if (!nodes.isEmpty()) {
            if (((MainController)getController()).showConfirm("Clear all", "Are you sure to clear Query Builder?", getStage(), false) == DialogResponce.OK_YES) {
                for (Node n: nodes) {
                    ((Button) n).fire();
                }

                databaseInUse.setValue(null);
            }
        }
    }
    
    
    
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    public void destroy() {
        // TODO
        freezeChanges();
        TabContentController tabController = getController().getTabControllerBySession(session);
        tabController.removeOnDoubleMouseClickedOnLeftTree(dbClickConsumer);
    }

    public void init(ConnectionSession session) {
        this.session = session;

        RSyntaxTextAreaBuilder builder = RSyntaxTextAreaBuilder.createSimple(UUID.randomUUID().toString(), session);
        SwingUtilities.invokeLater(() -> {
            builder.init();
            queryScreen = builder.getTextArea();
            queryScreen.setEditable(false);
            
            Platform.runLater(() -> {
                queryScreenPane.setCenter(builder.getSwingNode());
            });
        });
        
        TabContentController tabController = getController().getTabControllerBySession(session);
        tabController.addOnDoubleMouseClickedOnLeftTree(dbClickConsumer);
        
        ((MainController)getController()).usedQueryBuilder();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        ((ImageView)openQueryBuilderButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getOpen_image());
        ((ImageView)saveQueryBuilderButton.getGraphic()).setImage(StandardDefaultImageProvider.getInstance().getSave_image());
        
        tableview.setFixedCellSize(24);
        
        joinButton.disableProperty().bind(Bindings.greaterThan(Bindings.size(viewControllers), 1).not());
        
        queryTabmodule = (QueryTabModule) ((MainController)getController()).getModuleByClassName(QueryTabModule.class.getName());
        
        FadeTransition ft = new FadeTransition();
        ft.setNode(addTableButton);
        ft.setDuration(new Duration(300));
        ft.setFromValue(1.0);
        ft.setToValue(0.6);
        ft.setCycleCount(Animation.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();
        
        addTableButton.setOnMouseEntered(event -> { 
            ft.jumpTo(Duration.ZERO);
            ft.stop();
        });
                
        queryInUI.prefWidthProperty().bind(mainUI.prefWidthProperty());
        queryInUI.getChildren().addListener((ListChangeListener.Change<? extends Node> change) -> {
            while (change.next()) {
                if (change.wasRemoved()) {
                    if (change.getList().size() == 1) {
                        notice.setVisible(true);
                        footerButtonHolder.setDisable(true);
                        mRemoveAllTables.setDisable(true);
                        mUP.setDisable(true);
                        mDown.setDisable(true);
                        databaseInUse.setValue(null);
                    }
                } else if (change.wasAdded()) {
                    notice.setVisible(false);
                    footerButtonHolder.setDisable(false);
                    mRemoveAllTables.setDisable(false);
                }
            }
        });
        
        queryInUI.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            clearSelectedLine();
        });

        mAddTables = new MenuItem("Add Tables", new ImageView(SmartImageProvider.getInstance().getTableInsert_image()));
        mAddViews = new MenuItem("Add Views", new ImageView(SmartImageProvider.getInstance().getView_image()));
        mLoadQueryDesign = new MenuItem("Load Query Design", new ImageView(SmartImageProvider.getInstance().getOpen_small_image()));
        mRemoveAllTables = new MenuItem("Clear/Remove All Tables", new ImageView(SmartImageProvider.getInstance().getDeleteTable_image()));
        mUP = new MenuItem("Move Up", new ImageView(StandardDefaultImageProvider.getInstance().getMoveUp_image()));
        mDown = new MenuItem("Move Down", new ImageView(StandardDefaultImageProvider.getInstance().getMoveDown_image()));
        mRemoveAllTables.setDisable(true);
        mDown.setDisable(true);
        mUP.setDisable(true);

        ContextMenu cm0 = new ContextMenu();
        cm0.getItems().addAll(mUP, mDown,
                mAddTables, mAddViews, mLoadQueryDesign, mRemoveAllTables);
        tableview.setOnMouseClicked((MouseEvent event) -> {
            if (event.isPopupTrigger()) {
                cm0.show(getStage(), event.getScreenX(), event.getScreenY());
            }
        });
        
        deleteJoinItem = new MenuItem("Delete", new ImageView(SmartImageProvider.getInstance().getDropIndex_image()));
        deleteJoinItem.setOnAction((ActionEvent event) -> {
            if (selectedLine != null)  {
                joinResult.addWithNonJoinContion((JoinMaker.JoinCondition) selectedLine.getUserData());

                updateTableLines();
                tableViewChangesFunction();
            }
        });
        
        joinTypeItem = new Menu("Join Type");
        innerItem = new CheckMenuItem("Inner Join");
        innerItem.setOnAction((ActionEvent event) -> {
            if (selectedLine != null)  {
                ((JoinMaker.JoinCondition) selectedLine.getUserData()).setJoinType("INNER");
                tableViewChangesFunction();
            }
        });
        
        leftItem = new CheckMenuItem("Left Join");
        leftItem.setOnAction((ActionEvent event) -> {
            if (selectedLine != null)  {
                ((JoinMaker.JoinCondition) selectedLine.getUserData()).setJoinType("LEFT");
                tableViewChangesFunction();
            }
        });
        
        rightItem = new CheckMenuItem("Right Join");
        rightItem.setOnAction((ActionEvent event) -> {
            if (selectedLine != null)  {
                ((JoinMaker.JoinCondition) selectedLine.getUserData()).setJoinType("RIGHT");
                tableViewChangesFunction();
            }
        });
        
        
        lineContextMenu = new ContextMenu();
        joinTypeItem.getItems().addAll(innerItem, leftItem, rightItem);
        
        lineContextMenu.getItems().addAll(deleteJoinItem, joinTypeItem);
        
        cAddTables = new MenuItem("Add Tables", new ImageView(SmartImageProvider.getInstance().getTableInsert_image()));
        cAddViews = new MenuItem("Add Views", new ImageView(SmartImageProvider.getInstance().getView_image()));
        cLoadQueryDesign = new MenuItem("Load Query Design", new ImageView(SmartImageProvider.getInstance().getOpen_small_image()));
        cRemoveAllTables = new MenuItem("Clear/Remove All Tables", new ImageView(SmartImageProvider.getInstance().getDeleteTable_image()));
                
        ContextMenu canvasContextMenu = new ContextMenu();
        canvasContextMenu.getItems().addAll(cAddTables, cAddViews, cLoadQueryDesign, cRemoveAllTables);
        
        queryInUI.setOnMouseClicked((MouseEvent event) -> {
            
            if (event.isPopupTrigger()) {
                if (selectedLine != null) {
                    
                    innerItem.setSelected(false);
                    leftItem.setSelected(false);
                    rightItem.setSelected(false);
                    
                    String joinType = ((JoinMaker.JoinCondition) selectedLine.getUserData()).getJoinType();
                    if (joinType != null) {
                        switch (joinType) {
                            case "INNER":
                                innerItem.setSelected(true);
                                break;

                            case "LEFT":
                                leftItem.setSelected(true);
                                break;

                            case "RIGHT":
                                rightItem.setSelected(true);
                                break;
                        }
                    }
                    
                    lineContextMenu.show(getStage(), event.getSceneX(), event.getSceneY());
                } else {
                    canvasContextMenu.setX(event.getSceneX());
                    canvasContextMenu.setY(event.getSceneY());

                    canvasContextMenu.show(getStage());
                }
            }
        });
        

        mCopyQueryToClipboard = new MenuItem("Copy Query To Clipboard", new ImageView(
        SmartImageProvider.getInstance().getExplain_small_image()));
        mCopyQueryToNewQueryTab = new MenuItem("Copy Query To new Query Tab", new ImageView(
                SmartImageProvider.getInstance().getQueryEditor_small_image()));
        mCopyQueryToSameQueryTab = new MenuItem("Copy Query To this Query Tab", new ImageView(
                SmartImageProvider.getInstance().getExplainMenu_image()));
        mCreateViewUsingCurrentQuery = new MenuItem("Create View Using Current Query", new ImageView(
                SmartImageProvider.getInstance().getCreateView_image()));

        ContextMenu cm = new ContextMenu();
        cm.getItems().addAll(mCopyQueryToClipboard, mCopyQueryToNewQueryTab,
                mCopyQueryToSameQueryTab, mCreateViewUsingCurrentQuery);
        queryScreenScroll.setContextMenu(cm);

        mCopyQueryToClipboard.disableProperty().
                bind(copyQueryToClipboard.disableProperty());
        mCopyQueryToNewQueryTab.disableProperty().
                bind(copyQueryToNewQueryTab.disableProperty());

        mCreateViewUsingCurrentQuery.disableProperty().
                bind(createViewUsingCurrentQuery.disableProperty());

        copyQueryToClipboard.setOnAction(onAction);
        copyQueryToNewQueryTab.setOnAction(onAction);
        createViewUsingCurrentQuery.setOnAction(onAction);
        mCopyQueryToClipboard.setOnAction(onAction);
        mCopyQueryToNewQueryTab.setOnAction(onAction);

        mCreateViewUsingCurrentQuery.setOnAction(onAction);
        mLoadQueryDesign.setOnAction(onAction);
        mAddTables.setOnAction(onAction);
        mAddViews.setOnAction(onAction);
        mRemoveAllTables.setOnAction(onAction);
        mUP.setOnAction(onAction);
        mDown.setOnAction(onAction);
        
        cLoadQueryDesign.setOnAction(onAction);
        cAddTables.setOnAction(onAction);
        cAddViews.setOnAction(onAction);
        cRemoveAllTables.setOnAction(onAction);

        initColumnsForTable();
        addColumnButton.setOnAction(onAction);

        useDatabaseNameCheckbox.selectedProperty().addListener((Observable o) -> {
            SwingUtilities.invokeLater(() -> {
                queryScreen.setText("");
            });
            Recycler.addWorkProc(queryWriter);
        });

        tableview.getItems().addListener((Observable o) -> {
            mUP.setDisable(tableview.getItems().isEmpty());
            mDown.setDisable(tableview.getItems().isEmpty());
        });

        showCanvasRB.setOnAction(onAction);
        showFieldRB.setOnAction(onAction);
        showQueryRB.setOnAction(onAction);
        
        showCanvasRB.getStyleClass().add("query-checkbox");
        showFieldRB.getStyleClass().add("query-checkbox");
        showQueryRB.getStyleClass().add("query-checkbox");

//        addDebuggers();//comment this when done
        
       
        queryInUI.setOnDragOver((DragEvent event) -> {
            
            Dragboard db = event.getDragboard();
            if (db.hasString()) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });

        queryInUI.setOnDragDropped((DragEvent event) -> {
            
            Dragboard db = event.getDragboard();            
            boolean success = false;
            if (db.hasString()) {
                
                String[] data = db.getString().split("\\|");
                
                if ("tableOrView".equals(data[0])) {
                 
                    // exist only for table
                    String database = data[2];

                    // if we can add new table as join to old - then do this
                    // else just insert new select query
                    if (!database.isEmpty()) {
                        TableProvider table = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), database, data[1]);
                        if (table == null) {
                            table = DatabaseCache.getInstance().getView(session.getConnectionParam().cacheKey(), data[1]);
                            if (((View)table).getColumns() == null) {
                                try {
                                    ((View)table).setColumns(session.getService().getColumnInformation(database, data[1]));
                                } catch (SQLException ex) {
                                    ((MainController)getController()).showError("Error", "Error getting view columns", ex);
                                }
                            }
                        }
                        
                        addTable(table, event.getX(), event.getY());
                    }
                }
               
                success = true;
            }
            
            event.setDropCompleted(success);
            event.consume();
        });
    }
   
    
    private EventHandler<ActionEvent> onAction
            = new EventHandler<ActionEvent>() {               

                @Override
                public void handle(ActionEvent t) {
                    Object o = t.getSource();
                    if (o == copyQueryToClipboard || o == mCopyQueryToClipboard) {
                        ClipboardContent content = new ClipboardContent();
                        content.putString(queryScreen.getText());                        
                        Clipboard.getSystemClipboard().setContent(content);
                        
                    } else if (o == copyQueryToNewQueryTab || o == mCopyQueryToNewQueryTab) {
                        queryTabmodule.addQueryTab(session, queryScreen.getText());
                        
                    } else if (o == createViewUsingCurrentQuery || o == mCreateViewUsingCurrentQuery) {
                        
                        queryTabmodule.addTemplatedQueryTab(session, 
                            "Create view", 
                            "Enter new view name:",
                            (String t1) -> {
                                String text = session.getService().getCreateViewTemplateSQL(
                                    getDatabaseInUse() != null ? getDatabaseInUse().getName() : "<db-name>",
                                    t1
                                );                                
                                return text.replace("SELECT * FROM ...", queryScreen.getText().substring(0, queryScreen.getText().length() - 1));
                            },QueryTabModule.TAB_USER_DATA__VIEW_TAB );
                        
                    } else if (o == addColumnButton) {
                        columnAdditionCode();
                        
                    } else if (o == mRemoveAllTables || o == cRemoveAllTables) {
                        clearAllAction(null);
                        
                    } else if (o == mAddTables || o == cAddTables) {
                        addTableAction(null);
                        
                    } else if (o == mAddViews || o == cAddViews) {
                        addViewAction(null);
                        
                    } else if (o == mDown) {
                        int pos = tableview.getSelectionModel().getSelectedIndex(),
                        len = tableview.getItems().size() - 1;
                        QueryBuilderRowData qbr = tableview.getSelectionModel().getSelectedItem();
                        if (pos >= 0 && (pos + 1) < len) {
                            //no unneccessarily computations
                            tableview.getItems().removeListener(tableviewItemsListener);
                            tableview.getItems().remove(pos);
                            pos++;
                            //now compute
                            tableview.getItems().addListener(tableviewItemsListener);
                            tableview.getItems().add(pos, qbr);
                        }
                    } else if (o == mUP) {
                        int pos = tableview.getSelectionModel().getSelectedIndex();
                        QueryBuilderRowData qbr = tableview.getSelectionModel().getSelectedItem();
                        if (pos >= 1/* && (pos-1) > -1*/) {
                            //no unneccessarily computations
                            tableview.getItems().removeListener(tableviewItemsListener);
                            tableview.getItems().remove(pos);
                            pos--;
                            //now compute
                            tableview.getItems().addListener(tableviewItemsListener);
                            tableview.getItems().add(pos, qbr);
                        }

                    } else if (o == showCanvasRB) {
                        if (showCanvasRB.isSelected()) {
                            if (splitHolder.getItems().isEmpty()) {
                                mainSplit.getItems().add(0, splitHolder);
                            }
                            splitHolder.getItems().add(0, canvasScrollPane);
                        } else {
                            splitHolder.getItems().remove(canvasScrollPane);
                            if (splitHolder.getItems().isEmpty()) {
                                mainSplit.getItems().remove(splitHolder);
                            }
                        }

                        splitHolder.toFront();

                    } else if (o == showFieldRB) {
                        if (showFieldRB.isSelected()) {
                            if (splitHolder.getItems().isEmpty()) {
                                mainSplit.getItems().add(0, splitHolder);
                                splitHolder.getItems().add(0, tableview);
                            } else {
                                splitHolder.getItems().add(1, tableview);
                            }
                        } else {
                            splitHolder.getItems().remove(tableview);
                            if (splitHolder.getItems().isEmpty()) {
                                mainSplit.getItems().remove(splitHolder);
                            }
                        }

                        splitHolder.toFront();

                    } else if (o == showQueryRB) {

                        if (showQueryRB.isSelected()) {
                            if (mainSplit.getItems().isEmpty()) {
                                mainSplit.getItems().add(0, queryAnchor);
                            } else {
                                mainSplit.getItems().add(1, queryAnchor);
                                mainSplit.setDividerPosition(0, 0.7);
                            }
                        } else {
                            mainSplit.getItems().remove(queryAnchor);
                        }
                    }
                }

            };

    public void addTable(TableProvider table, double transX, double transY) {
        addTable(table, transX, transY, true);
    }
    
    public void addTable(TableProvider table, double transX, double transY, boolean needUpdate) {
        
        if (transX == 0 && transY == 0) {
            
            int index = viewControllers.size();
            if (index >= DEFAULT_POSITIONS.length) {
                index = index%DEFAULT_POSITIONS.length; 
            }
            
            if (index == 0 && !viewControllers.isEmpty()) {
                index++;
            }
            
            transX = DEFAULT_POSITIONS[index].getKey();
            transY = DEFAULT_POSITIONS[index].getValue();
        }
        
        if (getDatabaseInUse() == null) {
            if (table.getDatabase() == null) {
                ((MainController)getController()).showError("DATABASE RESOLUTION ERROR", "Could not resolve database on table. please try again", null);
                return;
            }
            databaseInUse.setValue(table.getDatabase());
            
        } else if (!databaseInUse.get().getName().equals(table.getDatabase().getName())) {
            ((MainController)getController()).showError("SELECTION ERROR", "the SmartMySQL's Query builder does currently"
                    + " does not support building of queries between two or more"
                    + " databases. Next Update will feature this.", null);
            return;
            
        } else if (isTableExist(table.getName())) {
            
        }

        BuilderViewController builderViewController = new BuilderViewController(this, table, transX, transY) {
            @Override
            public MainController getController() {
                return (MainController)QueryBuilderController.this.getController();
            }

            @Override
            public void onReflectChange() {
                tableViewChangesFunction();
            }
        };
        builderViewController.setTableView(tableview);
        queryInUI.getChildren().add(builderViewController.getUI());        
        viewControllers.put(table, builderViewController);
                    
        JoinMaker.makeJoin(((MainController)getController()), (MysqlDBService) getController().getSelectedConnectionSession().get().getService(), table, joinResult, false, false);

        if (needUpdate) {
            Platform.runLater(() -> {
                if (joinResult.getAllTables().contains(table)) {
                    updateTableLines();                
                    tableViewChangesFunction(); 
                } else {
                    removeTable(builderViewController.getLabel());
                }                    
            });
        }
    }
       
    
    /**
     * This is equivalent to select the column in the listview
     * 
     * @param col 
     * The column itself to select
     */
    public void addColumn(Column col) {
        addColumn(col.getTable().getName(), col);
    }
    

    /**
     * This is equivalent to select the column in the listview
     * 
     * @param table
     * THe name of the table the column belongs in
     * @param col 
     * The column itself to select
     */
    public void addColumn(String table, Column col) {
        for (Node n : queryInUI.getChildren()) {
            BuilderViewController bvc = (BuilderViewController) n.getUserData();
            if(bvc == null || !bvc.getLabel().equals(table)){continue;}
            bvc.toggleColumnState(col);
            n.toFront();
            return;
        }
        WorkProc<DBTable> backgroundTableAddition = new WorkProc<DBTable>() {
            
            @Override
            public void updateUI() { 
                addTable(getAddData(), 0, 0); 
            }

            @Override
            public void run() {
                
                setAdditionalData(DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), col.getTable().getDatabase().getName(), table));
//                try {
//                    List<DBTable> tbls;
//                    if (databaseInUse.get() == null) {
//                        List<Database> dbs = session.getService().getDatabases();
//                        databaseLoop:
//                        for (Database db : dbs) {
//                            tbls = session.getService().getTables(db);
//                            for (DBTable dbt : tbls) {
//                                if (dbt.getName().equals(table)) {
//                                    setAdditionalData(dbt);
//                                    break databaseLoop;
//                                }
//                            }
//                        }
//                    } else {
//                        tbls = session.getService().getTables(databaseInUse.get());
//                        for (DBTable dbt : tbls) {
//                            if (dbt.getName().equals(table)) {
//                                setAdditionalData(dbt);
//                                break;
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        };
        Recycler.addWorkProc(backgroundTableAddition);
        DialogResponce dr = ((MainController)getController()).showConfirm("TABLE NOT FOUND ON CANVAS",
                "Would you like to add table", getStage(), false);
        if (dr == DialogResponce.OK_YES || dr == DialogResponce.YES_TO_ALL) {
            backgroundTableAddition.updateUI();
        }
    }
    
    public void removeColumn(Column col){
        String table = col.getTable().getName();
        for (Node n : queryInUI.getChildren()) {
            
            Object obj = n.getUserData();
            if (obj == null) {
                continue;
            }
            if (obj instanceof BuilderViewController) {
                BuilderViewController bvc = (BuilderViewController) obj;
                if(!bvc.getLabel().equals(table)) {
                    continue;
                }
                
                bvc.removeColumn(col);
                n.toFront();
                break;
            }
        }
    }
    
    public void removeTable(String table) {
        for (Node n : queryInUI.getChildren()) {
            
            Object obj = n.getUserData();
            if (obj == null) {
                continue;
            }
            if (obj instanceof BuilderViewController) {
                BuilderViewController bvc = (BuilderViewController) obj;
                if(!bvc.getLabel().equals(table)) {
                    continue;
                }
            
                bvc.destroyController();

                viewControllers.remove(bvc.getTableProvider());

                joinResult.removeTable((MysqlDBService) getController().getSelectedConnectionSession().get().getService(), bvc.getTableProvider());

                updateTableLines();                
                tableViewChangesFunction();
                
                break;
            }
        }
    }
        

    public Pane getCanvas() {
        return queryInUI;
    }

    public void tableViewChangesFunction() {
        
        for (TableProvider table: viewControllers.keySet()) {
            Integer idx = joinResult.getSameTableIndexes().get(table);
            if (idx == null) {
                idx = 0;
            }
            
            viewControllers.get(table).setTableAlias(table.getName() + (idx > 0 ? idx : ""));
        }
        
        if (isChangesFrozen()) {
            return;
        }
        if (Platform.isFxApplicationThread()) {
            queryScreen.setText("");
            Recycler.addWorkProc(queryWriter);
        } else {
            Platform.runLater(() -> {
                queryScreen.setText("");
                Recycler.addWorkProc(queryWriter);
            });
        }
    }

    private void clearSelectedLine() {
        if (selectedLine != null) {
            selectedLine.setStroke(Color.web("#b1c6e1"));
            selectedLine.setStrokeWidth(6);
            selectedLine = null;
        }
    }
    
    private void updateTableLines() {
        
        for (Line l: tableLines) {
            queryInUI.getChildren().remove(l);
        }        
        tableLines.clear();
            
             
        for (TableProvider tp: joinResult.getJoinTables().keySet()) {
            
            List<JoinMaker.JoinCondition> list = joinResult.getTableJoinCondition(tp);
            if (list != null) {

                for (JoinMaker.JoinCondition jc: list) {
                    BuilderViewController bvc = viewControllers.get(jc.getFirst());
                    if (bvc != null) {

                        BuilderViewController bvc0 = viewControllers.get(jc.getSecond());
                        if (bvc0 != null) {                            
                            Line l = new Line();
                            l.setUserData(jc);
                            
                            l.setFocusTraversable(true);

                            l.setOnKeyPressed((KeyEvent event) -> {
                                if (event.getCode() == KeyCode.DELETE) {

                                    if (((MainController)getController()).showConfirm("Delete JOIN condition", "Do you realy want delete to deete JOIN condition?") == DialogResponce.OK_YES) {

                                        joinResult.addWithNonJoinContion(jc);

                                        updateTableLines();
                                        tableViewChangesFunction();
                                    }

                                    event.consume();
                                }
                            });

                            l.setOnMouseClicked((MouseEvent event) -> {

                                clearSelectedLine();

                                l.setStroke(Color.BLUE);
                                selectedLine = l;
                                selectedLine.requestFocus();

                                if (event.getClickCount() == 2) {                            
                                    doFilter(bvc0, jc, false);
                                }
                            });

                            l.setStroke(Color.web("#b1c6e1"));
                            l.setStrokeWidth(6);

                            l.startXProperty().bind(bvc.getUI().layoutXProperty().add(bvc.getUI().widthProperty().divide(2.0)));
                            l.startYProperty().bind(bvc.getUI().layoutYProperty().add(bvc.getUI().heightProperty().divide(2.0)));

                            l.endXProperty().bind(bvc0.getUI().layoutXProperty().add(bvc0.getUI().widthProperty().divide(2.0)));
                            l.endYProperty().bind(bvc0.getUI().layoutYProperty().add(bvc0.getUI().heightProperty().divide(2.0)));
                                
//                            Arrow arrow = new Arrow(true);
//                            arrow.startXProperty().bind(bvc.getUI().layoutXProperty().add(bvc.getUI().widthProperty().divide(2.0)));
//                            arrow.startYProperty().bind(bvc.getUI().layoutYProperty().add(bvc.getUI().heightProperty().divide(2.0)));
//
//                            arrow.endXProperty().bind(bvc0.getUI().layoutXProperty().add(bvc0.getUI().widthProperty().divide(2.0)));
//                            arrow.endYProperty().bind(bvc0.getUI().layoutYProperty().add(bvc0.getUI().heightProperty().divide(2.0)));
                            
                            tableLines.add(l);
                            queryInUI.getChildren().add(l);
                            l.toBack();
                            
//                            queryInUI.getChildren().add(arrow);
                        }
                    }
                }
            }
        }
    }
    
//    private BuilderViewController getTableViewController(TableProvider tp) {
//        for (TableProvider key: viewControllers.keySet()) {
//            if (key.getName().equalsIgnoreCase(tp.getName()) &&
//                    (key.getDatabase() == null && tp.getDatabase() == null 
//                        || key.getDatabase() != null && tp.getDatabase() != null && key.getDatabase().getName().equalsIgnoreCase(tp.getDatabase().getName()))) {
//                return viewControllers.get(key);
//            }
//        }
//        
//        return null;
//    }
        
    private void doFilter(BuilderViewController dragedTable, JoinMaker.JoinCondition joinCondition, boolean canChange) {
               
        if (joinCondition == null) {
            TableProvider table = null;
            for (TableProvider tp: joinResult.getAllTables()) {
                if (tp != dragedTable) {
                    table = tp;
                    break;
                }
            }
            joinCondition = JoinMaker.JoinCondition.create(dragedTable.getTableProvider(), table == null ? joinResult.getAllTables().get(0) : table, dragedTable.getTableProvider());
        }
                        
        if (JoinMaker.showJoinConditionDialog(((MainController)getController()), joinResult.getAllTables(), joinResult, joinCondition, canChange, canChange, false)) {            

            updateTableLines();
            tableViewChangesFunction();
        }
//        else {
//            removeTable(dragedTable.getLabel());
//        }
    }
    
    private volatile boolean running = false;

    private final WorkProc queryWriter = new WorkProc() {

        StringBuilder queryStringBuilder = new StringBuilder();
        
        private boolean empty = true;
        
        
        
        Random ran = new Random();

        @Override
        public void updateUI() {
            copyQueryToClipboard.setDisable(empty);
            copyQueryToNewQueryTab.setDisable(empty);
            createViewUsingCurrentQuery.setDisable(empty);
            if (!running) {
                PreferenceData pd = preferenceService.getPreference();
                if (!pd.isQueriesUsingBackquote()) {
                    queryScreen.append(queryStringBuilder.toString().replaceAll("`", ""));
                } else {
                    queryScreen.append(queryStringBuilder.toString());
                }
                
                QueryTabModule.formatCurrentQuery(((MainController)getController()), preferenceService, session, new QueryAreaWrapper(UUID.randomUUID().toString(), queryScreen, null), queryScreen.getText().length() - 1);
            }

            queryStringBuilder = new StringBuilder();
            
            if (ran.nextBoolean() && !showQueryRB.isSelected()) {
                showQueryRB.fire();
            }
        }

        @Override
        public void run() {
            callUpdate = false;
            running = true;
            
            boolean writeWithTableName = true;//useDatabaseNameCheckbox.isSelected();
            
            List<QueryBuilderRowData> source = new ArrayList(tableview.getItems());
            if (viewControllers.isEmpty()) {
                empty = callUpdate = true;
                return;
            }
            
            empty = false;
            
            Map<String, List<String>> columns = new LinkedHashMap<>();
            
            for (int x = 0; x < source.size(); x++) {
                QueryBuilderRowData qbr = source.get(x);
                
                String text = "";
                if (qbr.getShow().get()) {
                    
                    if (qbr.getAgeFunction().get().isEmpty()) {
                        if (writeWithTableName) {
                            text += qbr.getTable() + "." + qbr.getColumn().getName()+ " ";
                        } else {
                            text += qbr.getColumn().getName() + " ";
                        }                        
                    } else {
                        
                        text += qbr.getAgeFunction().get();
                        text += "(";

                        if (writeWithTableName) {
                            text += qbr.getTable() + "." + qbr.getColumn().getName() + " ";
                        } else {
                            text += qbr.getColumn().getName() + " ";
                        }

                        text += ") ";
                    }

                    if (!qbr.getAlias().get().isEmpty()) {
                        text += "AS ";
                        text +=  qbr.getAlias().get() + " ";
                    }
                }
                
                if (!text.isEmpty()) {
                    
                    List<String> list = columns.get(qbr.getFullTable());
                    if (list == null) {
                        list = new ArrayList<>();
                        columns.put(qbr.getFullTable(), list);
                    }
                    
                    list.add(text);
                }
            }
            
            joinResult.clearPresentation();
            String joinResultText = joinResult.toString();
            
            queryStringBuilder.append("SELECT ");
            
            String columnText = "";                        
            String othersTables = "";
            for (String table: columns.keySet()) {
                
                if (!columns.isEmpty()) {
                    
                    List<String> list = columns.get(table);
                    if (list != null && !list.isEmpty()) {
                        if (!columnText.isEmpty()) {
                            columnText += ", ";
                        }
                        columnText += String.join(", ", list);
                    }
                }
//                
//                if (joinText == null || !joinText.contains(" " + table + " ")) {
//                    
//                    if (!table.equals(tableText)) {
//                        if (!othersTables.isEmpty()) {
//                            othersTables += ", ";
//                        }
//                        othersTables += table;
//                    }
//                }
            }
            
            if (columns.isEmpty()) {
                columnText = joinResultText.substring(joinResultText.indexOf("SELECT ") + 7, joinResultText.indexOf(" FROM "));
            }
            
            queryStringBuilder.append(columnText);
            
            queryStringBuilder.append(" FROM ");
            
            if (joinResultText != null) {
                
                String fromText = joinResultText.substring(joinResultText.indexOf(" FROM ") + 6);
                if (!useDatabaseNameCheckbox.isSelected()) {
                    fromText = fromText.replaceAll(databaseInUse.get().getName() + "\\.", "");
                }
                
                queryStringBuilder.append(" ").append(fromText);
            }
            
            if (!othersTables.isEmpty()) {
                queryStringBuilder.append(", ").append(othersTables);
            }
            
            // where + having
            conditionSections(queryStringBuilder, source, writeWithTableName);


            if (queryStringBuilder.length() > 0) {
                ////query terminator//
                queryStringBuilder.append(";");
            }

            callUpdate = true;
            running = false;
        }
    };
    
        
    private void conditionSections(StringBuilder builder, List<QueryBuilderRowData> source, boolean writeWithTableName) {
        
        Map<Integer, List<String>> conditionsMap = new LinkedHashMap<>();
        Map<Integer, List<String>> functionsMap = new LinkedHashMap<>();
        
        
        for (QueryBuilderRowData qbr: source) {
              
            String columnName;
            String whereColumnName;
            if (writeWithTableName) {
                columnName = qbr.getAlias().isEmpty().get() ? qbr.getTable() + "." + qbr.getColumn().getName() : qbr.getAlias().get();                
                whereColumnName = qbr.getTable() + "." + qbr.getColumn().getName();
            } else {
                columnName = qbr.getAlias().isEmpty().get() ? qbr.getColumn().getName() : qbr.getAlias().get();
                whereColumnName = qbr.getColumn().getName();
            }
            
            int conditionIndex = 0;
            
            boolean hasAgeFunction = !qbr.getAgeFunction().get().isEmpty();
            boolean hasConditions = !qbr.getWhereCondition().get().isEmpty() || !qbr.getConditions().isEmpty();
            
            Map<Integer, List<String>> map = hasAgeFunction ? functionsMap : hasConditions ? conditionsMap : null;
            if (map != null) {
                
                // if we have where condition - add it
                if (!qbr.getWhereCondition().get().isEmpty()) {
                    List<String> conditions = map.get(conditionIndex);
                    if (conditions == null) {
                        conditions = new ArrayList<>();
                        map.put(conditionIndex, conditions);
                    }
                    
                    String text;
                    if (writeWithTableName) {
                        
                        if (hasAgeFunction) {
                           text = qbr.getAgeFunction().get() + "(" + whereColumnName + ") "
                                + qbr.getWhereCondition().get(); 
                        } else {
                            text = whereColumnName + " " + qbr.getWhereCondition().get();
                        }
                        
                    } else {
                        if (hasAgeFunction) {
                           text = qbr.getAgeFunction().get() + "(" + whereColumnName + ") "
                                + qbr.getWhereCondition().get(); 
                        } else {
                            text = whereColumnName + " "
                                + qbr.getWhereCondition().get();
                        }
                    }
                    
                    conditions.add(text);
                }
                
                // if we have or conditions - add them to next levels
                for (SimpleStringProperty con: qbr.getConditions()) {
                    conditionIndex++;
                    
                    if (!con.get().isEmpty()) {
                        List<String> conditions = map.get(conditionIndex);
                        if (conditions == null) {
                            conditions = new ArrayList<>();
                            map.put(conditionIndex, conditions);
                        }

                        String text;
                        if (writeWithTableName) {

                            if (hasAgeFunction) {
                               text = qbr.getAgeFunction().get() + "(" + whereColumnName + ") "
                                    + con.get(); 
                            } else {
                                text = whereColumnName + " " + con.get();
                            }

                        } else {
                            
                            if (hasAgeFunction) {
                               text = qbr.getAgeFunction().get() + "(" + whereColumnName + ") "
                                    + con.get(); 
                            } else {
                                text = whereColumnName + " " + con.get();
                            }
                        }
                        
                        conditions.add(text);
                    }
                }
            }
        }
        
        processConditionsMap("WHERE ", conditionsMap, builder);
        
        //for group
        boolean oneTime = false;
        for (int x = 0; x < source.size(); x++) {

            QueryBuilderRowData qbr = source.get(x);
            if (qbr.getGroupBy().get()) {

                if (!oneTime) {
                    if (builder.charAt(builder.length() - 1) != ' ') {
                        builder.append(" ");
                    }
                    builder.append("GROUP BY ");
                    oneTime = true;
                } else {
                    builder.append(", ");
                }

//                if (!qbr.getAlias().get().isEmpty()) {
//                    builder.append(qbr.getAlias().get()).append(" ");
//
//                } else {
                    builder.append(qbr.getTable()).append(".").append(qbr.getColumn().getName()).append(" ");
//                }
            }
        }
        
        processConditionsMap("HAVING ", functionsMap, builder);
         
        
        //for order
        oneTime = false;
        for (int x = 0; x < source.size(); x++) {
            QueryBuilderRowData qbr = source.get(x);
            if (!qbr.getSort().get().equals("(Not Sorted)")) {

                if (!oneTime) {
                    if (builder.charAt(builder.length() - 1) != ' ') {
                        builder.append(" ");
                    }
                    builder.append("ORDER BY ");
                    oneTime = true;
                } else {
                    builder.append(", ");
                }                        
                
                builder.append(qbr.getTable()).append(".").append(qbr.getColumn().getName()).append(" ").append(qbr.getSort().get());                
            }
        }
    }
    
    private void processConditionsMap(String type, Map<Integer, List<String>> conditionsMap, StringBuilder builder) {
        if (!conditionsMap.isEmpty()) {
            builder.append("\n").append(type);

            boolean prevEmpty = true;
            int index = 0;
            for (List<String> condtions: conditionsMap.values()) {
                
                if (!condtions.isEmpty()) {
                    
                    if (!prevEmpty) {
                        builder.append(" OR ");
                    }
                    prevEmpty = false;                    
                    
                    builder.append("(");

                    boolean firstCondition = true;
                    for (String condition: condtions) {

                        if (!firstCondition) {
                            builder.append(" AND ");
                        }

                        builder.append(condition);                        
                        firstCondition = false;
                    }

                    if (index < conditionsMap.size() - 1) {
                        builder.append(") \n");
                    } else {
                        builder.append(")");
                    }
                }
                
                index++;
            }
        }
    }
    

    private final ListChangeListener tableviewItemsListener = (ListChangeListener) (ListChangeListener.Change change) -> {
        tableViewChangesFunction();
    };

    
    private QueryBuilderRowData editRow;
    
    private void initColumnsForTable() {

        tableview.getItems().clear();
        tableview.getItems().addListener(tableviewItemsListener);
        tableColumn.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> new TableCell<QueryBuilderRowData, String>() {
            {
                setEditable(false);
                setStyle("-fx-text-fill: #293955;");
            }
            
            @Override
            protected void updateItem(String t, boolean bln) {
                super.updateItem(t, bln);
                if (bln) {
                    setText("");
                } else {
                    setText(t);
                }
            }
        });

        tableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<
               QueryBuilderRowData, String>, ObservableValue<String>>() {
            SimpleStringProperty theString
                    = new SimpleStringProperty();

            @Override
            public ObservableValue<String> call(
                    TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) {
                theString.set(p.getValue().getTable());
                return theString;
            }
        });

        fieldColumn.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> new TableCell<QueryBuilderRowData, String>() {
            {
                setEditable(false);
                setStyle("-fx-text-fill: #293955;");
            }
            
            @Override
            protected void updateItem(String t, boolean bln) {
                super.updateItem(t, bln);
                if (bln) {
                    setText("");
                } else {
                    setText(t);
                }
            }
        });

        fieldColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<
               QueryBuilderRowData, String>, ObservableValue<String>>() {
            SimpleStringProperty theString
                    = new SimpleStringProperty();

            @Override
            public ObservableValue<String> call(
                    TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) {
                theString.set(p.getValue().getColumn().getName());
                return theString;
            }
        });

        showColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryBuilderRowData, Boolean> p) -> p.getValue().getShow());

        showColumn.setCellFactory((TableColumn<QueryBuilderRowData, Boolean> p) -> new CheckBoxTableCell<>());

        groupColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryBuilderRowData, Boolean> p) -> p.getValue().getGroupBy());

        groupColumn.setCellFactory((TableColumn<QueryBuilderRowData, Boolean> p) -> new CheckBoxTableCell<>());

        ageFunctionColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) -> p.getValue().getAgeFunction());

        ageFunctionColumn.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> 
            new ComboBoxTableCell<QueryBuilderRowData, String>("", "avg", "count", "max", "min", "stddev", "sum", "variance") {
            {
                setEditable(true);
                setStyle("-fx-text-fill: #293955;");
                graphicProperty().addListener((ObservableValue<? extends Node> ov, Node t, Node t1) -> {
                    if (t1 != null) {
                        ((ComboBox) t1).setStyle(
                                "-fx-background-color: white;");
                        if (((ComboBox) t1).getSelectionModel().getSelectedItem() == null) {
                            ((ComboBox) t1).getSelectionModel().selectFirst();
                        }
                    }
                });
            }
        });

        sortColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) -> p.getValue().getSort());

        sortColumn.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> 
            new ComboBoxTableCell<QueryBuilderRowData, String>("(Not Sorted)", "ASC", "DESC") {
            {
                setEditable(true);
                setStyle("-fx-text-fill: #293955;");
                graphicProperty().addListener((ObservableValue<? extends Node> ov, Node t, Node t1) -> {
                    if (t1 != null) {
                        ((ComboBox) t1).setStyle("-fx-background-color: white;");
                        if (((ComboBox) t1).getSelectionModel().getSelectedItem() == null) {
                            ((ComboBox) t1).getSelectionModel().selectFirst();
                        }
                    }
                });
            }
        });
        
//        sortColumn.setOnEditStart((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
//            editRow = event.getRowValue();
//        });
//        sortColumn.setOnEditCommit((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
//            
//            QueryBuilderRowData row = editRow;
//            if (event.getTablePosition() != null) {
//                row = event.getRowValue();
//            }
//            row.getSort().set(event.getNewValue());
//        });

        aliasColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) -> p.getValue().getAlias());

        aliasColumn.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> 
            new EditingCell<QueryBuilderRowData, String>()
            {
                {
                    setStyle("-fx-text-fill: #293955;");
                    setEditable(true);
                }
            }
        );
        
        aliasColumn.setOnEditStart((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
            editRow = event.getRowValue();
        });
        aliasColumn.setOnEditCommit((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
            
            QueryBuilderRowData row = editRow;
            if (event.getTablePosition() != null) {
                row = event.getRowValue();
            }
            row.getAlias().set(event.getNewValue());
        });

        whereConditionColumn.setCellValueFactory((TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) -> p.getValue().getWhereCondition());

        whereConditionColumn.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> 
            new EditingCell<QueryBuilderRowData, String>()
            {
                {
                    setStyle("-fx-text-fill: #293955;");
                    setEditable(true);
                }
            }
        );
        whereConditionColumn.setOnEditStart((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
            editRow = event.getRowValue();
        });
        whereConditionColumn.setOnEditCommit((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
            
            QueryBuilderRowData row = editRow;
            if (event.getTablePosition() != null) {
                row = event.getRowValue();
            }
            row.getWhereCondition().set(event.getNewValue());
        });

        moreColumn.setCellFactory((TableColumn<QueryBuilderRowData, ArrayList<SimpleStringProperty>> p) -> new TableCell<QueryBuilderRowData, ArrayList<SimpleStringProperty>>() {
            @Override
            protected void updateItem(ArrayList<SimpleStringProperty> t,
                    boolean bln) {
                super.updateItem(t, bln);
                //setStyle("-fx-background-color: white;");
            }
        });

    }

    private void columnAdditionCode() {
        TableColumn<QueryBuilderRowData, String> tc = new TableColumn();
        tc.setText("or");
        tc.setId("querybuildercolumn");
        tc.setPrefWidth(100);
        /**
         * This place always confuses me. Note Each row which is backed by
         * QueryBuilderRowData, has an ArrayList of StringProperty which
         * corresponds to conditional-or in the query These StringProperties are
         * mapped to the table columns with title conditionals. 'The number of
         * conditionals columns corresponds to the number of available or
         * conditions, not every row will fill all these conditionals. the index
         * of these StringProperty are calculated starting from the first
         * conditional column, i.e, the current column - 8 the index of the
         * first conditional column
         *
         * Note; in the query writing when one condition is hit it checks other
         * row for existing conditions in the same conditional column.
         *
         *
         */
        tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<
                                    QueryBuilderRowData, String>, ObservableValue<String>>() {
            SimpleStringProperty conditionStatement;

            @Override
            public ObservableValue<String> call(
                    TableColumn.CellDataFeatures<QueryBuilderRowData, String> p) {
                conditionStatement = null;
                byte pos = (byte) (tableview.getColumns().indexOf(tc) - 8);
                if (pos < p.getValue().getConditions().size()) {
                    conditionStatement = p.getValue().getConditions().get(pos);
                }
                if (conditionStatement == null) {
                    conditionStatement = p.getValue().addNewCondition("");
                }
                return conditionStatement;
            }
        });

        tc.setCellFactory((TableColumn<QueryBuilderRowData, String> p) -> 
            new EditingCell<QueryBuilderRowData, String>()
            {
                {
                    setStyle("-fx-text-fill: #293955;");
                    setEditable(true);
                }
            }
        );
        tc.setOnEditStart((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
            editRow = event.getRowValue();
        });
        tc.setOnEditCommit((TableColumn.CellEditEvent<QueryBuilderRowData, String> event) -> {
            
            QueryBuilderRowData row = editRow;
            if (event.getTablePosition() != null) {
                row = event.getRowValue();
            }
            
            byte pos = (byte) (tableview.getColumns().indexOf(tc) - 8);
            if (pos < row.getConditions().size()) {
                row.getConditions().get(pos).set(event.getNewValue());
            }
        });

        
        tableview.getColumns().add((tableview.getColumns().size() - 1), tc);
    }

    public void alterTableRowItem(Column column, boolean show, String alias, String groupBy,
            String ageFunc, String sort, String whereCondition, ArrayList<SimpleStringProperty> conditions) {
        for (QueryBuilderRowData qbr : tableview.getItems()) {
            if (qbr.getColumn().getName().equals(column.getName()) && qbr.getColumn().equals(column)) {
                System.out.println("qbr found");
                freezeChanges();
                qbr.getSort().set(sort);
                qbr.getAgeFunction().set(ageFunc);
                qbr.getAlias().set(alias);
                qbr.getWhereCondition().set(whereCondition);
                qbr.getGroupBy().set(Boolean.valueOf(groupBy));
                qbr.getConditions().addAll(conditions);
                qbr.getShow().set(show);
                unFreezeChanges();
                tableViewChangesFunction();
                break;
            }
        }
    }

    public void freezeChanges() {
        pauseForAll.set(true);
    }

    public void unFreezeChanges() {
        pauseForAll.set(false);
    }

    public boolean isChangesFrozen() {
        return pauseForAll.get();
    }

    private boolean isTableExist(String name) { 
        for (Node n : queryInUI.getChildren()) {
            Object obj = n.getUserData();
            if (obj instanceof BuilderViewController) {
                BuilderViewController bvc = (BuilderViewController) obj;
                if(bvc == null || !bvc.getLabel().equals(name)){continue;}
                return true;
            }
            
        }
        return false;
    }

}
