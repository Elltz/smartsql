package np.com.ngopal.smart.sql.core.modules;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.Hooks;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import np.com.ngopal.smart.sql.core.modules.utils.LeftTreeHelper;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Rajya Laxmi Maharjan &lt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class MenuInFunctionModule extends AbstractStandardModule {

    private Image functionImage;
    
    private MenuItem alterFunction;
    private MenuItem dropFunction;
    private MenuItem createFunction;
    
    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }
    
    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public boolean install() {
        super.install();
        
        registrateServiceChangeListener();

        alterFunction = getAlterFunctionMenuItem(null);
        dropFunction = getDropFunctionMenuItem(null);
        createFunction = getCreateFunctionMenuItem(null);

        Menu functionMenu = new Menu("Functions",
                new ImageView(SmartImageProvider.getInstance().getFunction_image()));
        functionMenu.getItems().addAll(createFunction, alterFunction, dropFunction);
        
        alterFunction.disableProperty().bind(busy());
        dropFunction.disableProperty().bind(busy());
        createFunction.disableProperty().bind(busy());
        functionMenu.disableProperty().bind(busy());

        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{functionMenu});

        if (functionImage == null) {
            functionImage = SmartImageProvider.getInstance().getFunctionTab_image();
        }

        return true;
    }
    
    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        boolean isFunction = item.getValue() instanceof Function;
        
        alterFunction.setDisable(!isFunction);
        dropFunction.setDisable(!isFunction);
        
        createFunction.setDisable(!(isFunction || item.getValue().equals(ConnectionTabContentController.DatabaseChildrenType.FUNCTIONS)));
        return true;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private MenuItem getCreateFunctionMenuItem(ConnectionSession session) {

        MenuItem createFunctionMenuItem = new MenuItem("Create Function...", 
                new ImageView(SmartImageProvider.getInstance().getCreateFunction_image()));
        createFunctionMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        createFunctionMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                createFunction();
            }
        });

        return createFunctionMenuItem;
    }
    
    private MenuItem getAlterFunctionMenuItem(ConnectionSession session) {
        
        MenuItem alterFunctionMenuItem = new MenuItem("Alter Function...",
                new ImageView(SmartImageProvider.getInstance().getAlterFunction_image()));
        alterFunctionMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        alterFunctionMenuItem.setOnAction((ActionEvent e) -> {
            alterFunction();
        });
        
        if (session == null) {
            alterFunctionMenuItem.setDisable(true);
        }
        return alterFunctionMenuItem;
    }

    public void alterFunction() {
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        ConnectionSession session0 = getController().getSelectedConnectionSession().get();
        try {
            Function function = (Function) ((ReadOnlyObjectProperty<TreeItem>)getController().getSelectedTreeItem(session0)).get().getValue();

            String alterFunction = session0.getService().getAlterFunctionSQL(function.getDatabase().getName(), function.getName());
            if (alterFunction == null) {
                showError("Access denied", "Access denied for altering function", null);
            } else {
                queryTabModule.addNamedTabWithContext(
                        session0,
                        function.getName(), 
                        alterFunction,
                        QueryTabModule.TAB_USER_DATA__FUNCTION_TAB,
                        null
                );
            }
        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
        }
    }
    
    private MenuItem getDropFunctionMenuItem(ConnectionSession session) {

        MenuItem dropFunctionMenuItem = new MenuItem("Drop Function...",
                new ImageView(SmartImageProvider.getInstance().getDropFunction_image()));
        dropFunctionMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        dropFunctionMenuItem.setOnAction((ActionEvent event) -> {
            try {
                ConnectionSession session0 = session != null ? session : getController().getSelectedConnectionSession().get();
            
                Function function = (Function) ((ReadOnlyObjectProperty<TreeItem>) getController()
                        .getSelectedTreeItem(session0)).get().getValue();
                
                DialogResponce responce = DialogHelper.showConfirm(getController(), "Drop function", 
                        "Do you really want to drop the function '" + function.getName() + "'?");
                
                if (responce == DialogResponce.OK_YES) {
                    // drop fucntion
                    session0.getService().dropFunction(function);
                    LeftTreeHelper.functionDroped(session0.getConnectionParam(), ((ReadOnlyObjectProperty<TreeItem>) getController().getSelectedTreeItem(session0)).get());
                }
            } catch (SQLException ex) {
                DialogHelper.showError(getController(), "Error", ex.getMessage(), ex);
            }
        });
        if (session == null) {
            dropFunctionMenuItem.setDisable(true);
        }
        return dropFunctionMenuItem;
    }
    
    @Override
    public Map<HookKey, Hookable> getHooks(final ConnectionSession session) {
        Separator separator = new Separator();
        separator.setOrientation(Orientation.VERTICAL);

        Map<HookKey, Hookable> map = new LinkedHashMap<>();
        map.put(new HookKey("functions0", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getCreateFunctionMenuItem(session)}))), Hooks.QueryBrowser.FUNCTIONS);
        map.put(new HookKey("functionItem0", new ArrayList<>(Arrays.asList(new MenuItem[]{
            getCreateFunctionMenuItem(session), getAlterFunctionMenuItem(session), 
            getDropFunctionMenuItem(session)}))), Hooks.QueryBrowser.FUNCTION_ITEM);
        return map;
    }
    
    public void createFunction()
    {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        QueryTabModule queryTabModule = (QueryTabModule) getController().getModuleByClassName(QueryTabModule.class.getName());
        if (queryTabModule != null)
        {
            Database db = session.getConnectionParam().getSelectedDatabase();

            queryTabModule.addTemplatedQueryTab(
                session, 
                "Create function", 
                "Enter new function name:", 
                new java.util.function.Function<String, String>() {
                    @Override
                    public String apply(String t) {
                        return session.getService().getCreateFunctionTemplateSQL(db != null ? db.getName() : "<db-name>", t);
                    }
                },QueryTabModule.TAB_USER_DATA__FUNCTION_TAB );
        }
    }
    
    @Override
    public String getInfo() { 
        return "";
    }
}
