package np.com.ngopal.smart.sql.core.modules;

import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Pair;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.SQLLogger;
import np.com.ngopal.smart.sql.core.modules.utils.CodeAreaWrapper;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.model.QueryProvider;
import np.com.ngopal.smart.sql.db.RecentFileService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PreferenceData;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLKeywords;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.FindDialogController;
import np.com.ngopal.smart.sql.ui.controller.GoToDialogController;
import np.com.ngopal.smart.sql.ui.controller.ReplaceDialogController;
import np.com.ngopal.smart.sql.utils.ScriptRunner;
import np.com.ngopal.smart.sql.query.QueryFormat;
import np.com.ngopal.smart.sql.ui.controller.AddFavoriteController;
import np.com.ngopal.smart.sql.ui.controller.ExportConnectionDetails;
import np.com.ngopal.smart.sql.ui.controller.FlushController;
import np.com.ngopal.smart.sql.ui.controller.ImportConnectionDetails;
import np.com.ngopal.smart.sql.ui.controller.OrganiseFavouriteController;
import np.com.ngopal.smart.sql.ui.controller.QueryBuilderController;
import np.com.ngopal.smart.sql.ui.controller.ShowParentController;
import np.com.ngopal.smart.sql.ui.controller.TableDiagnosticsController;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.View;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.DBElement;
import np.com.ngopal.smart.sql.model.Favourites;
import np.com.ngopal.smart.sql.model.ForeignKey;
import np.com.ngopal.smart.sql.model.Function;
import np.com.ngopal.smart.sql.model.Index;
import np.com.ngopal.smart.sql.model.QueryResult;
import np.com.ngopal.smart.sql.model.RecentFiles;
import np.com.ngopal.smart.sql.model.StoredProc;
import np.com.ngopal.smart.sql.model.Trigger;
import np.com.ngopal.smart.sql.model.tech.mysql.MySQLFunctions;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.query.clauses.FromClause;
import np.com.ngopal.smart.sql.query.clauses.JoinClause;
import np.com.ngopal.smart.sql.query.clauses.QueryClause;
import np.com.ngopal.smart.sql.query.clauses.TableReferenceClause;
import np.com.ngopal.smart.sql.query.commands.SelectCommand;
import np.com.ngopal.smart.sql.ui.components.TextMenu;
import np.com.ngopal.smart.sql.ui.components.TextMenuItem;
import np.com.ngopal.smart.sql.ui.components.ToolbarDynamicMenu;
import np.com.ngopal.smart.sql.ui.controller.ColorDialogController;
import np.com.ngopal.smart.sql.ui.controller.ConfigurableQueryBinderController;
import np.com.ngopal.smart.sql.ui.controller.InsertTemplateController;
import np.com.ngopal.smart.sql.ui.controller.MainController;
import np.com.ngopal.smart.sql.ui.controller.OpenSchemaDesignerDialogController;
import np.com.ngopal.smart.sql.ui.controller.PreferenceController;
import np.com.ngopal.smart.sql.ui.controller.ResultTabController;
import np.com.ngopal.smart.sql.ui.controller.SearchPublicSchemaController;
import org.apache.commons.lang.StringUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import tools.java.pats.models.SqlFormatter;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.dialogs.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.structure.queryutils.RSyntaxTextAreaBuilder;
import np.com.ngopal.smart.sql.structure.queryutils.RsSyntaxCompletionHelper;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.misc.*;
import np.com.ngopal.smart.sql.structure.provider.StandardDefaultImageProvider;
import static np.com.ngopal.smart.sql.structure.utils.FunctionHelper.*;
import static np.com.ngopal.smart.sql.structure.utils.Flags.*;
import np.com.ngopal.smart.sql.core.modules.utils.Flags;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.utils.SqlUtils;
import np.com.ngopal.smart.sql.modules.SmartProfilerModule;
import np.com.ngopal.smart.sql.structure.modules.ConnectionModuleInterface;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class QueryTabModule extends AbstractStandardModule implements SessionPersistor {

    private static final int HIGHLIGHTING_MAX_LINE_LENGTH = 5000;
    public static final int HIGHLIGHT_DELAY = 500;

    //public static final Map<CodeArea, Boolean> disabledHighlightAreas = new HashMap<>();
    //public static final Map<CodeArea, Boolean> disabledValidatingAreas = new HashMap<>();
    public static final long MAX_OPEN_FILE_SIZE = 5 * 1024 * 1024;

    private static final String ROWS_AFFECTED = " row(s) affected";
    private static final String TOTAL = "Total: ";
    private static final String SEC = " sec";
    private static final String ERROR_CODE = "Error Code: ";
    private static final String SINGLE_QUOTE = "'";
    private static final String LIMIT = "LIMIT";
    private static final String ENTER = "\n";

//    private static final Paint VALID_SELECTION_PAINT    = Color.web("#e1ffe6");
    private static final Color INVALID_SELECTION_PAINT = Color.web("#ffb3b3");

    public static final Pattern QUERY_FUNCTION_PATTERN = Pattern.compile("CREATE.*?FUNCTION.*?BEGIN(.*?)END", Pattern.CASE_INSENSITIVE);
    public static final Pattern QUERY_PROCEDURE_PATTERN = Pattern.compile("CREATE.*?PROCEDURE.*?BEGIN(.*?)END", Pattern.CASE_INSENSITIVE);
    public static final Pattern QUERY_TRIGGER_PATTERN = Pattern.compile("CREATE.*?TRIGGER.*?BEGIN(.*?)END", Pattern.CASE_INSENSITIVE);
    public static final Pattern QUERY_EVENT_PATTERN = Pattern.compile("CREATE.*?EVENT.*?BEGIN(.*?)END", Pattern.CASE_INSENSITIVE);

    public static final Pattern QUERY_EMPTY_PREFIX = Pattern.compile("(\\s*).*");
    
    public static final long MAX_VALIDATION_SIZE = 100 * 1024; // 100kb
    public static final Pattern CLAUSES = Pattern.compile("(\\bSELECT\\b|\\bINSERT\\b|\\bUPDATE\\b|\\bDELETE\\b|\\bSET\\b|\\bSHOW\\b|\\bCREATE\\b|"
            + "\\bGRANT\\b|\\bCOMMIT\\b|\\bROLLBACK\\b|\\bSTART\\b|\\bALTER\\b|\\bDECLARE\\b|\\bDROP\\b|\\bRENAME\\b|\\bREPLACE\\b|\\bRETURN\\b|\\bTRUNCATE\\b)", Pattern.CASE_INSENSITIVE);

    @Inject
    private SQLLogger logger;

    @Inject
    private PreferenceDataService preferenceService;

    @Inject
    private RecentFileService recentFileService;

    @Inject
    private FavouritesService favServ;
    
    @Inject 
    private UserUsageStatisticService usageStatisticService;

    @Inject
    private ConnectionParamService service;

    private Image queryAnalyzerImage;

    private final Map<ConnectionSession, Tab> parentTabs = new HashMap<>();
    private final Map<Tab, QueryAreaWrapper> codeareas = new HashMap<>();
    private final Map<QueryAreaWrapper, Tab> changedTabs = new HashMap<>();
    private final HashMap<ConnectionSession, ArrayList<AbstractMap.SimpleEntry<Tab, QueryAreaWrapper>>> sessionsOpenedInterface
            = new HashMap();

    public static final Map<ConnectionSession, List<String>> highlightGreenMap = new HashMap<>();
    public static final Map<ConnectionSession, List<String>> highlightMaroonMap = new HashMap<>();

    public static final Map<ConnectionSession, Map<QueryResult, String>> errorsForHighlight = new HashMap<>();

    private final Map<FindObject, FindCodeAreaData> codeareasFindDada = new HashMap<>();

    private final BooleanProperty needValidateQuery = new SimpleBooleanProperty(true);

    private final Map<ConnectionSession, Boolean> executeButtonMap = new HashMap<>();
    private final Map<ConnectionSession, Boolean> executeLockButtonMap = new HashMap<>();
    private final Map<ConnectionSession, Boolean> executeAllButtonMap = new HashMap<>();
    private final Map<ConnectionSession, Boolean> executeWithEditButtonMap = new HashMap<>();
    private final Map<ConnectionSession, Boolean> executeBulkButtonMap = new HashMap<>();

    private final BooleanProperty executeButton = new SimpleBooleanProperty(false);
    private final BooleanProperty executeLockButton = new SimpleBooleanProperty(false);
    private final BooleanProperty executeAllButton = new SimpleBooleanProperty(false);
    private final BooleanProperty executeBulkButton = new SimpleBooleanProperty(false);
    private final BooleanProperty executeWithEditButton = new SimpleBooleanProperty(false);

    private final HashMap<ConnectionSession, CheckMenuItem> hideSQLEditorCollection = new HashMap();
    private final HashMap<ConnectionSession, CheckMenuItem> hideResultTabCollection = new HashMap();
    private final HashMap<ConnectionSession, Map<QueryAreaWrapper, List<Tab>>> resultTabs = new HashMap<>();

//    private final HashMap<ConnectionSession, List<Tab>> visualExplanTabs = new HashMap();
    
    private final HashMap<Tab, Consumer> dbClickListeners = new HashMap<>();

    private BooleanBinding queryTabNotSelected;
    private BooleanBinding onlyQueryTabNotSelected;

    public static final Map<ConnectionSession, Pair<Integer, Integer>> currentQueryPosition = new HashMap<>();
    public static final Map<ConnectionSession, List<String>> successfullyExecutedQueries = new HashMap<>();

    public static final HashMap<String, String> validatedQueries = new HashMap<>();
    public static final Set<String> warningEmptyKeyQueries = new HashSet<>();
    public static final Set<String> warningMoreThen10kRowsQueries = new HashSet<>();

    // on update and on delete its fake delimaters - we must check them and ignore
    private static final Pattern QUERY_POSITION_PATTERN = Pattern.compile("\\b(ON\\s+UPDATE|ON\\s+DELETE|DELIMITER|SELECT|CREATE|DROP|ALTER|TRUNCATE|INSERT|UPDATE|DELETE|SHOW|COMMIT|ROLLBACK|SAVEPOINT|DO|START)\\b", Pattern.CASE_INSENSITIVE);
    private static final String IGNORE_MATCH_STR = "ON\\s+UPDATE|ON\\s+DELETE";
    private static final String UNION_MATCH_STR = ".*?\\bunion\\b(\\s+\\ball\\b)?";

    @Override
    public int getOrder() {
        return 7;
    }
    
    private Map<Tab, SimpleStringProperty> runnedQueryCountMap = new HashMap<>();
    public SimpleStringProperty sessionLastRunnedQueryCountProperty(Tab t) {
        return runnedQueryCountMap.get(t);
    }

    MenuItem queryEditor, queryBuilder, schemaDesigner, closeTab, renameTab,
            disconnect, disconnectAll, open, save, saveAs, openSession, saveSession, saveSessionAs,
            endSession, switchToNextTab, paste, cut, copy, insertFromFile, goTo, copyWithNormalisedWhitespace, undo, redo,
            listAllTags, listMatchingTags, listFunctions, switchToPreviousTab, find, findNext, selectAll, replace, hideObjectBrowser/*, hideSqlEditor, hideResultPane*/,
            /*refreshFav,*/ organiseFav, /*addFav,*/ pref, pref0, pref1, pref2, pref3, pref4, pref5, pref6, pref7, exportCon, info, userM, history, feq, tableDiag, flush, executeSQL, exportAllRows, iconArrange, backupData, tile, cascade, exit, queryAnalyzerMenuItem, disableValidationMenuItem,
            exportConnectionDetails, importConnectionDetails, databaseMenuItem, refreshObjectBrowser, slowQueryAnalyzerMenuItem, debuggerMenuItem, profilerMenuItem ;

    private MenuItem publishSearchMi;

    private Menu advanced, explain, executeQuery, sqlFormatter, showitem, findItem, copyMenu, tagsMenu;

    private File openFileDir;

    public ReadOnlyBooleanProperty needValidateQuery() {
        return needValidateQuery;
    }

    public boolean hasChanges() {
        return !changedTabs.isEmpty();
    }

    public List<QueryAreaWrapper> getChangedCodeAreas() {
        return new ArrayList<>(changedTabs.keySet());
    }

    @Override
    public boolean install() {

        registrateServiceChangeListener();

        exit = new MenuItem("Exit", new ImageView(StandardDefaultImageProvider.getInstance().getExit_image()));
        exit.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.ALT_DOWN));
        exit.setOnAction((ActionEvent event) -> {
            Platform.exit();
        });

        queryEditor = getQueryEditor();

        queryBuilder = getQueryBuilder();

        schemaDesigner = getSchemaDesigner();

        databaseMenuItem = getComboxMenuItem();
        
        profilerMenuItem = getProfilerMenuItem();
        
        closeTab = new MenuItem("Close Tab", new ImageView(SmartImageProvider.getInstance().getCloseTab_image()));

        closeTab.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.ALT_DOWN));
        closeTab.setOnAction((ActionEvent event) -> {
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            Tab t = (Tab) ((ReadOnlyObjectProperty) getController().getSelectedConnectionTab(session)).get();
            t.getTabPane().getTabs().remove(t);
        });

        renameTab = new MenuItem("Rename Tab", new ImageView(SmartImageProvider.getInstance().getRenameTab_image()));
        renameTab.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.ALT_DOWN));
        renameTab.setOnAction((ActionEvent event) -> {
            renameTab();
        });

        disconnect = new MenuItem("Disconnet", new ImageView(SmartImageProvider.getInstance().getDisconnect_image()));
        disconnect.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCombination.SHORTCUT_DOWN));
        disconnect.setOnAction((ActionEvent event) -> {
            getController().disconnect(getController().getSelectedConnectionSession().get(), DisconnectPermission.NONE);
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.W, KeyCombination.SHORTCUT_DOWN), (Object obj) -> {
            getController().disconnect(getController().getSelectedConnectionSession().get(), DisconnectPermission.NONE);
            ((KeyEvent)obj).consume();
        });
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.F4, KeyCombination.SHORTCUT_DOWN), (Object obj) -> {
            getController().disconnect(getController().getSelectedConnectionSession().get(), DisconnectPermission.NONE);
            ((KeyEvent)obj).consume();
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.U, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN), (Object obj) -> {
            upperLowerSelectedText(true);
            ((KeyEvent)obj).consume();
        });
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.L, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN), (Object obj) -> {
            upperLowerSelectedText(false);
            ((KeyEvent)obj).consume();
        });

        disconnectAll = new MenuItem("Disconnect All", new ImageView(SmartImageProvider.getInstance().getDisconnect_image()));
        disconnectAll.setOnAction((ActionEvent event) -> {
            DisconnectPermission permission = DisconnectPermission.NONE;
            while (getController().getSelectedConnectionSession().get() != null) {
                permission = getController().disconnect(getController().getSelectedConnectionSession().get(), permission);
            }
        });

        open = new MenuItem("Open", new ImageView(StandardDefaultImageProvider.getInstance().getOpen_image()));
        open.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN));
        open.setOnAction((ActionEvent event) -> {
            FileChooser openFileChooser = new FileChooser();
            openFileChooser.setInitialDirectory(openFileDir);
            openFileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("SmartSQL Files", "*.sql"),
                    new FileChooser.ExtensionFilter("All Files", "*.*"));
            File userFile = openFileChooser.showOpenDialog(getController().getStage());
            if (userFile != null) {
                openFileDir = userFile.getParentFile();
                openFile(userFile);
            }
        });

        save = new MenuItem("Save", new ImageView(StandardDefaultImageProvider.getInstance().getSave_image()));
        save.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN));
        save.setOnAction((ActionEvent event) -> {
            saveScript();
        });

        saveAs = new MenuItem("Save As", new ImageView(StandardDefaultImageProvider.getInstance().getSaveAs_image()));
        saveAs.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper codeArea = getSessionCodeArea();
            if (codeArea != null) {
                FileChooser fc = new FileChooser();
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("SmartSQL Files", "*.sql"),
                        new FileChooser.ExtensionFilter("All Files", "*.*"));
                File userFile = fc.showSaveDialog(getController().getStage());
                if (userFile != null) {
                    try {
                        Files.write(userFile.toPath(), codeArea.getText().getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

                        Tab tab = changedTabs.remove(codeArea);
                        if (tab == null) {
                            for (Tab t: codeareas.keySet()) {
                                if (codeareas.get(t) == codeArea) {
                                    tab = t;
                                    break;
                                }
                            }
                        }
                        
                        RSyntaxTextAreaBuilder codeAreaWrapperData = (RSyntaxTextAreaBuilder) codeArea.getUserData();
                        if (codeAreaWrapperData != null) {
                            codeAreaWrapperData.setRelativePath(userFile.getAbsolutePath());
                        }
                        
                        if (tab != null) {
                            tab.setText(userFile.getName());
                        }

                    } catch (Throwable ex) {
                        DialogHelper.showError(getController(),"Error saveing script", ex.getMessage(), ex);
                    }
                }
            }
        });

        openSession = new MenuItem("Open Session Savepoint", new ImageView(StandardDefaultImageProvider.getInstance().getOpen_image()));
        openSession.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        openSession.setOnAction((ActionEvent event) -> {
            getController().openSessionPoint();
        });

        saveSession = new MenuItem("Save Session",
                new ImageView(StandardDefaultImageProvider.getInstance().getSave_image()));
        saveSession.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        saveSession.setOnAction((ActionEvent event) -> {
            getController().saveSession(getController().getSelectedConnectionSession().get());
        });

        saveSessionAs = new MenuItem("Save Session As", new ImageView(StandardDefaultImageProvider.getInstance().getSaveAs_image()));
        saveSessionAs.setOnAction((ActionEvent event) -> {
            getController().saveSessionAs(getController().getSelectedConnectionSession().get());
        });

        endSession = new MenuItem("End Session", new ImageView(SmartImageProvider.getInstance().getEndSession_image()));
        endSession.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        endSession.setOnAction((ActionEvent event) -> {
            getController().endSession(getController().getSelectedConnectionSession().get());
        });

        Menu recentFiles = new ToolbarDynamicMenu((MenuButton menu, EventHandler<Event> handler) -> {
            List<RecentFiles> files = recentFileService.getLastFiles();
            if (files != null && !files.isEmpty()) {

                menu.getItems().clear();

                int i = 0;
                for (RecentFiles rf : files) {
                    MenuItem mi = new MenuItem(++i + " " + rf.getRecentFile());
                    mi.setOnAction((ActionEvent event) -> {

                        menu.hide();

                        File file = new File(rf.getRecentFile());
                        if (file.exists()) {
                            try {
                                byte[] data = Files.readAllBytes(file.toPath());

                                if (data.length > MAX_OPEN_FILE_SIZE) {
                                    DialogHelper.showError(getController(),"Cannot open", "Cannot open file with size greater than 5mb", null);
                                    return;
                                }
                                addQueryTab(getController().getSelectedConnectionSession().get(), UUID.randomUUID().toString(), file.getAbsolutePath(), new String(data, "UTF-8"), file.getName(), TAB_USER_DATA__QUERY_TAB);
                            } catch (IOException ex) {
                                DialogHelper.showError(getController(),"Error reading recent file", ex.getMessage(), ex);
                            }
                        } else {
                            DialogHelper.showError(getController(), "Error", "File does not exist", null);
                        }
                    });

                    mi.addEventHandler(EventType.ROOT, handler);

                    menu.getItems().add(mi);
                }
            } else if (menu.getItems().isEmpty()) {
                menu.getItems().add(new MenuItem("No recent files"));
            }
        },
                "Recent Files",
                new ImageView(SmartImageProvider.getInstance().getRecentMenu_image())
        );

        /////////declaring menu items from array  ///////////////
        /**
         * I am equating the menuItems array returned from
         * initMenuAndMenuItems() the order in which they are returned is what
         * is enumerated as follows Counting is from last to first
         */
        MenuItem[] menuItemsForEditToolBar = initMenuAndMenuItems();
        advanced = (Menu) menuItemsForEditToolBar[25];
        switchToNextTab = menuItemsForEditToolBar[24];
        switchToPreviousTab = menuItemsForEditToolBar[23];
//        hideSqlEditor = (CheckMenuItem) menuItemsForEditToolBar[22];
//        hideResultPane = (CheckMenuItem) menuItemsForEditToolBar[21];
        hideObjectBrowser = menuItemsForEditToolBar[20];
        listMatchingTags = menuItemsForEditToolBar[19];
        listAllTags = menuItemsForEditToolBar[18];
        listFunctions = menuItemsForEditToolBar[26];
        goTo = menuItemsForEditToolBar[17];
        replace = menuItemsForEditToolBar[16];
        findNext = menuItemsForEditToolBar[15];
        find = menuItemsForEditToolBar[14];
        selectAll = menuItemsForEditToolBar[13];
        insertFromFile = menuItemsForEditToolBar[12];
        paste = menuItemsForEditToolBar[11];
        copyWithNormalisedWhitespace = menuItemsForEditToolBar[10];
        copy = menuItemsForEditToolBar[9];
        cut = menuItemsForEditToolBar[8];
        redo = menuItemsForEditToolBar[7];
        undo = menuItemsForEditToolBar[6];
        insertTemplates = menuItemsForEditToolBar[5];
        insertTemplates.setUserData("Insert Templates...");
        insertTemplates.setText("Insert");
        sqlFormatter = (Menu) menuItemsForEditToolBar[4];
        explain = (Menu) menuItemsForEditToolBar[3];
        executeQuery = (Menu) menuItemsForEditToolBar[2];
        MenuItem changeObjectBrowser = menuItemsForEditToolBar[1];
        refreshObjectBrowser = menuItemsForEditToolBar[0];

        queryAnalyzerMenuItem = menuItemsForEditToolBar[27];
        slowQueryAnalyzerMenuItem = menuItemsForEditToolBar[28];
        debuggerMenuItem = menuItemsForEditToolBar[29];

        MenuItem visualExplain = menuItemsForEditToolBar[30];

        /////////end of declaration ///////////
//        refreshFav = getRefreshFavouriteItem();
        organiseFav = getOrganiseFavouriteItem();
        //addFav = getAddToFavouriteItem();

        pref = getPreferenceItem();
        
        pref0 = getPreferenceGeneralItem();
        pref1 = getPreferencePowerToolsItem();
        pref2 = getPreferenceFontsItem();
        pref3 = getPreferenceFormaterItem();
        pref4 = getPreferenceOthersItem();
        pref5 = getPreferenceQueryBrowserItem();
        pref6 = getPreferenceQueryOptimizerItem();
        pref7 = getPreferenceDebuggerItem();
        
        
        exportCon = getImportExportConnectionDetailsItem();
        exportCon.setText("Import/Export");
        exportCon.setUserData("Import/Export Connection Details");

        //changeLan = getChangeLangueItem();
        showitem = (Menu) getShowItem();
        userM = getUserManagerItem();
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.U, KeyCombination.SHORTCUT_DOWN), (Object obj) -> {
            showUserManager();
            ((KeyEvent)obj).consume();
        });
        
        history = getHistoryItem();
        feq = getFeqItem();
        tableDiag = getTableDiagnosticsItem();
        flush = getFlushItem();

        iconArrange = getIconArrangeItem();
        tile = getTileWindowItem();
        cascade = getCascadeItem();
//        Menu menuSession = new Menu("Sessions",
//                new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/session_menu.png",
//                        18, 18, false, false)));

        disableValidationMenuItem = getDisableValidationItem();

        queryTabNotSelected = getController().centerTabForQueryTabBooleanProperty();
        onlyQueryTabNotSelected = getController().centerTabForOnlyQueryTabBooleanProperty();

        ObjectBinding selectedTab = getController().selectedTabProperty();

        closeTab.disableProperty().bind(selectedTab.isNull());
        renameTab.disableProperty().bind(selectedTab.isNull().or(queryTabNotSelected));

        switchToNextTab.disableProperty().bind(selectedTab.isNull());
        switchToPreviousTab.disableProperty().bind(selectedTab.isNull());

        refreshObjectBrowser.disableProperty().bind((queryTabNotSelected.or(busy())).and(executeButton.not()));

        MenuItem execute = executeQuery.getItems().get(0);
        execute.disableProperty().bind((onlyQueryTabNotSelected.or(busy())).and(executeButton.not()));

        executeButton.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            Platform.runLater(() -> {
                execute.setGraphic(new ImageView( ((newValue == null || !newValue) ? SmartImageProvider.getInstance().getExecute_image() : SmartImageProvider.getInstance().getCancelExecute_image())));
                execute.setText(newValue == null || !newValue ? "Execute Query" : "Stop executing");
            });
        });

        MenuItem executeAll = executeQuery.getItems().get(1);
        executeAll.disableProperty().bind((queryTabNotSelected.or(busy())).and(executeAllButton.not()));
        executeAllButton.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            Platform.runLater(() -> {
                executeAll.setGraphic(new ImageView( ((newValue == null || !newValue) ? SmartImageProvider.getInstance().getExecuteAll_image() : SmartImageProvider.getInstance().getCancelExecute_image())));
                executeAll.setText(newValue == null || !newValue ? "Execute All Queries" : "Stop executing");
            });
        });

        MenuItem executeWithEdit = executeQuery.getItems().get(2);
        executeWithEdit.disableProperty().bind((onlyQueryTabNotSelected.or(busy())).and(executeWithEditButton.not()));

        executeWithEditButton.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            Platform.runLater(() -> {
                executeWithEdit.setGraphic(new ImageView( ((newValue == null || !newValue) ? SmartImageProvider.getInstance().getExecuteEdit_image() : SmartImageProvider.getInstance().getCancelExecute_image())));
                executeWithEdit.setText(newValue == null || !newValue ? "Execute And Edit ResultSet" : "Stop executing");
            });
        });

        MenuItem executeLock = executeQuery.getItems().get(3);
        executeLock.disableProperty().bind((onlyQueryTabNotSelected.or(busy())).and(executeLockButton.not()));

        executeLockButton.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            Platform.runLater(() -> {
                executeLock.setGraphic(new ImageView( ((newValue == null || !newValue) ? SmartImageProvider.getInstance().getExecuteLock_image() : SmartImageProvider.getInstance().getCancelExecute_image())));
                executeLock.setText(newValue == null || !newValue ? "Lock Free Execution" : "Stop executing");
            });
        });
        
        MenuItem executeBulk = executeQuery.getItems().get(4);
        executeBulk.disableProperty().bind((onlyQueryTabNotSelected.or(busy())).and(executeBulkButton.not()));

        executeBulkButton.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            Platform.runLater(() -> {
                executeBulk.setGraphic(new ImageView( ((newValue == null || !newValue) ? SmartImageProvider.getInstance().getExecuteBulk_image() : SmartImageProvider.getInstance().getCancelExecute_image())));
                executeBulk.setText(newValue == null || !newValue ? "Bulk execution. Don’t give output. Useful for bulk inserts." : "Stop executing");
            });
        });

        publishSearchMi = new MenuItem("Search public schemas", new ImageView(SmartImageProvider.getInstance().getPublishSearch_designer_image()));
        publishSearchMi.setOnAction((ActionEvent event) -> {
            showPublishSearch();
        });

        explain.getItems().stream().forEach((mi) -> {
            mi.disableProperty().bind(queryTabNotSelected.or(busy()));
        });

        sqlFormatter.getItems().stream().forEach((mi) -> {
            mi.disableProperty().bind(queryTabNotSelected);
        });

        advanced.getItems().stream().forEach((mi) -> {
            mi.disableProperty().bind(queryTabNotSelected);
        });

        insertFromFile.disableProperty().bind(queryTabNotSelected);
        insertTemplates.disableProperty().bind(queryTabNotSelected);
        paste.disableProperty().bind(queryTabNotSelected);
        cut.disableProperty().bind(queryTabNotSelected);
        copy.disableProperty().bind(queryTabNotSelected);
        goTo.disableProperty().bind(queryTabNotSelected);
        copyWithNormalisedWhitespace.disableProperty().bind(queryTabNotSelected);
        undo.disableProperty().bind(queryTabNotSelected);
        redo.disableProperty().bind(queryTabNotSelected);
        find.disableProperty().bind(queryTabNotSelected);
        findNext.disableProperty().bind(queryTabNotSelected);
        selectAll.disableProperty().bind(queryTabNotSelected);
        replace.disableProperty().bind(queryTabNotSelected);

        listAllTags.disableProperty().bind(queryTabNotSelected);
        listMatchingTags.disableProperty().bind(queryTabNotSelected);

        visualExplain.disableProperty().bind(queryTabNotSelected.or(busy()));

        // Add itemd
        Menu newMenu = new Menu("New", new ImageView(StandardDefaultImageProvider.getInstance().getNew_image()), queryEditor, queryBuilder, schemaDesigner);
        Menu modifyMenu = new Menu("Modify", new ImageView(SmartImageProvider.getInstance().getModify_image()), closeTab, renameTab, disconnect, disconnectAll);
//        Menu queryMenu = new Menu("Query", new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/query.png")), open, save, saveAs);
        Menu sessionMenu = new Menu("Session", new ImageView(SmartImageProvider.getInstance().getSessionMenu_image()), openSession, saveSession, saveSessionAs, endSession);

        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FILE, new MenuItem[]{open, save, saveAs, newMenu, modifyMenu, sessionMenu, recentFiles, exit});

        Menu browserMenu = new Menu("Browser", new ImageView(SmartImageProvider.getInstance().getBrowser_image()), refreshObjectBrowser, changeObjectBrowser);
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{browserMenu, executeQuery, explain, sqlFormatter, insertTemplates});

        Menu editMenu = new Menu("Edit", new ImageView(SmartImageProvider.getInstance().getModify_image()), undo, redo, cut, copy, copyWithNormalisedWhitespace, paste, insertFromFile, selectAll);
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{editMenu});

        Menu findMenu = new Menu("Find", new ImageView(SmartImageProvider.getInstance().getFindMenu_image()), find, findNext, replace, goTo);
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{findMenu});

        Menu tagMenu = new Menu("Tags", new ImageView(SmartImageProvider.getInstance().getTagsMenu_image()), listAllTags, listMatchingTags, listFunctions);
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{tagMenu});

        Menu othersMenu = new Menu("Others", new ImageView(SmartImageProvider.getInstance().getOthers_image()), hideObjectBrowser/*, hideResultPane, hideSqlEditor*/, switchToPreviousTab, switchToNextTab);
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{othersMenu});

        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.EDIT, new MenuItem[]{advanced});

        getController().addToolbarContainerTabListener((MenuCategory t) -> {
            if (t == MenuCategory.FAVORITES) {
                updateFavorites();
            }
        });

        updateFavorites();
        //getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FAVORITES, new MenuItem[]{addFav, organiseFav, refreshFav});

        executeSQL = getexecuteSQLScriptItem();
        backupData = getBackUpDataAsSQLDumpItem();
        exportAllRows = getExportAllRowsTableItem();

        //getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{exportAllRows, backupData, executeSQL});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{flush, tableDiag, history, feq});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{userM, showitem});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.POWERTOOLS, new MenuItem[]{profilerMenuItem});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.TOOLS, new MenuItem[]{exportCon, pref});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.SETTINGS, new MenuItem[]{pref0, pref1, pref2, pref3, pref4
                , pref5, pref6, pref7});        
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.OTHERS, new MenuItem[]{visualExplain});
        //getController().addMenuItems(getDependencyModuleClass(), MenuCategory.WINDOW, new MenuItem[]{getCascadeItem(), getTileWindowItem(), getIconArrangeItem(), menuSession});
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.HELP, new MenuItem[]{getHelpItem(),
            getKeyboardShortcutItem(), getQuerySettingsItem(), getCheckForUpdatesItem(), getChangeRegistrationDetailsItem(), getAboutItem()});

        ///recreating them to use in the default part /////////
        menuItemsForEditToolBar = initMenuAndMenuItems();
        sqlFormatter = (Menu) menuItemsForEditToolBar[4];
        explain = (Menu) menuItemsForEditToolBar[3];
        executeQuery = (Menu) menuItemsForEditToolBar[2];

       // addFav.disableProperty().bind(queryTabNotSelected.or(busy()));

        executeQuery.getItems().stream().forEach((mi) -> {
            mi.disableProperty().bind(queryTabNotSelected.or(busy()));
        });
        sqlFormatter.getItems().stream().forEach((mi) -> {
            mi.disableProperty().bind(queryTabNotSelected);
        });

        queryEditor = getQueryEditor();

        exportConnectionDetails.disableProperty().bind(busy());
        importConnectionDetails.disableProperty().bind(busy());
        refreshObjectBrowser.disableProperty().bind(busy());
        open.disableProperty().bind(busy());
        save.disableProperty().bind(busy());
        saveAs.disableProperty().bind(busy());
        queryEditor.disableProperty().bind(busy());

        // TODO Need check this
        getController().getSelectedConnectionSession().addListener((ObservableValue<? extends ConnectionSession> observable, ConnectionSession oldValue, ConnectionSession newValue) -> {
            validatedQueries.clear();
            warningEmptyKeyQueries.clear();
        });

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{refreshObjectBrowser, organiseFav, save, saveAs, queryEditor, execute, executeAll, executeWithEdit, executeLock, executeBulk/*, addFav*/});

//        getController().getSelectedConnectionSession().addListener((ObservableValue<? extends ConnectionSession> ov, ConnectionSession t, ConnectionSession t1) -> {
//            if (t1 != null) {
////                if (hideSQLEditorCollection.get(t1) != null) {
////                    ((CheckMenuItem) hideSqlEditor).setSelected(
////                            hideSQLEditorCollection.get(t1).isSelected());
////                }
////                if (hideResultTabCollection.get(t1) != null) {
////                    ((CheckMenuItem) hideResultPane).setSelected(
////                            hideResultTabCollection.get(t1).isSelected());
////                }
//            }
//        });

        MenuItem userManagerItem = getUserManagerItem();
        MenuItem queryAnaluzerItem = getQueryAnalyzerMenuItem();
        MenuItem queryBuilderItem = getQueryBuilder();
        MenuItem schemaDesignerItem = getSchemaDesigner();

        databaseMenuItem.disableProperty().bind(busy());
        sqlFormatter.getItems().get(0).disableProperty().bind(busy());
        userManagerItem.disableProperty().bind(busy());
        queryAnaluzerItem.disableProperty().bind(busy());
        queryBuilderItem.disableProperty().bind(busy());
        schemaDesignerItem.disableProperty().bind(busy());
        disableValidationMenuItem.disableProperty().bind(busy());
        
        profilerMenuItem.disableProperty().bind(busy());
        debuggerMenuItem.disableProperty().bind(busy());
        slowQueryAnalyzerMenuItem.disableProperty().bind(busy());
        publishSearchMi.disableProperty().bind(busy());

        getController().addMenuItems(getDependencyModuleClass(), null, new MenuItem[]{databaseMenuItem,
            sqlFormatter.getItems().get(0), userManagerItem, queryAnaluzerItem, slowQueryAnalyzerMenuItem, debuggerMenuItem, queryBuilderItem, publishSearchMi,
            schemaDesignerItem, disableValidationMenuItem, profilerMenuItem});

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN), (Object obj) -> {

            if (getSessionCodeArea() != null) {
                getSessionCodeArea().requestFocus();
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN), (Object obj) -> {
            Tab t = getSelectedBottomTab();
            if (t != null) {

                Object o = t.getUserData();
                if (o instanceof BaseController) {
                    Platform.runLater(() -> ((BaseController) o).requestFocus());
                } else {
                    Platform.runLater(() -> t.getContent().requestFocus());
                }
                
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.L, KeyCombination.SHORTCUT_DOWN), (Object obj) -> {
            Tab t = getSelectedResultTab();
            if (t != null) {

                Object o = t.getUserData();
                if (o instanceof ResultTabController) {
                    if (((ResultTabController) o).isGridView()) {
                        Platform.runLater(() -> ((ResultTabController) o).toTextView());
                    } else {
                        Platform.runLater(() -> ((ResultTabController) o).toGridView());
                    }
                    ((KeyEvent)obj).consume();
                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN), (Object obj) -> {
            Tab t = getSelectedResultTab();
            if (t != null) {

                Object o = t.getUserData();
                if (o instanceof ResultTabController) {
                    Platform.runLater(() -> ((ResultTabController) o).exportTable(null));
                    ((KeyEvent)obj).consume();
                }
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN, KeyCodeCombination.SHIFT_DOWN), (Object obj) -> {

            if (getSessionCodeArea() != null) {
                getSessionCodeArea().commectSelection();
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN, KeyCodeCombination.SHIFT_DOWN), (Object obj) -> {

            Tab t = getSelectedResultTab();
            if (t != null) {

                Object o = t.getUserData();
                if (o instanceof ResultTabController) {
                    Platform.runLater(() -> ((ResultTabController) o).exportTable(null));
                    ((KeyEvent)obj).consume();
                }
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN, KeyCodeCombination.SHIFT_DOWN), (Object obj) -> {

            if (getSessionCodeArea() != null) {
                getSessionCodeArea().uncommectSelection();
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 0) {
                Tab t = tabs.get(0);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT2, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 1) {
                Tab t = tabs.get(1);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 2) {
                Tab t = tabs.get(2);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT4, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 3) {
                Tab t = tabs.get(3);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT5, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 4) {
                Tab t = tabs.get(4);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT6, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 5) {
                Tab t = tabs.get(5);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT7, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 6) {
                Tab t = tabs.get(6);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT8, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 7) {
                Tab t = tabs.get(7);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT9, KeyCombination.ALT_DOWN), (Object obj) -> {

            List<Tab> tabs = getResultTabs();
            if (tabs != null && tabs.size() > 0) {
                Tab t = tabs.get(tabs.size() - 1);
                Platform.runLater(() -> t.getTabPane().getSelectionModel().select(t));
                ((KeyEvent)obj).consume();
            }
        });

        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.F2, KeyCombination.ALT_DOWN), (Object obj) -> {

            if (getSessionCodeArea() != null) {
                for (Tab t : codeareas.keySet()) {
                    if (codeareas.get(t) == getSessionCodeArea()) {
                        String name = DialogHelper.showInput(getController(), "Rename query tab", "Enter query tab name:", "", t.getText());
                        if (name != null) {
                            t.setText(name);
                        }
                        ((KeyEvent)obj).consume();
                    }
                }
            }
        });
        
        return true;
    }
    
    public void hideFindReplaceDialog() {
        if (findReplacteDialog != null) {
            if (findReplacteDialog.isShowing()) {
                findReplacteDialog.close();
            }
            findReplacteDialog = null;
        }
    }
    
    private void upperLowerSelectedText(boolean upper) {
        String text = getSessionCodeArea().getSelectedText();
        if (!text.trim().isEmpty()) {
            text = upper? text.toUpperCase() : text.toLowerCase();
            
            int from = getSessionCodeArea().getSelectionStart();
            int to = getSessionCodeArea().getSelectionEnd();
            
            getSessionCodeArea().replaceText(from, to, text);
            getSessionCodeArea().selectRange(from, to);
        }
    }

    private Tab getSelectedResultTab() {
        Map<QueryAreaWrapper, List<Tab>> map = resultTabs.get(getController().getSelectedConnectionSession().get());
        if (map != null) {
            List<Tab> tabs = map.get(getSessionCodeArea());
            if (tabs != null) {
                for (Tab t : tabs) {
                    if (t.isSelected()) {
                        if (t.getUserData() instanceof ResultTabController) {
                            return t;
                        } else {
                            return null;
                        }
                    }
                }
            }
        }

        return null;
    }
    
    public void updateResultTabsFont(String font) {
        for (Map<QueryAreaWrapper, List<Tab>> map: resultTabs.values()) {
            if (map != null) {
                List<Tab> tabs = map.get(getSessionCodeArea());
                if (tabs != null) {
                    for (Tab t : tabs) {
                        if (t.getUserData() instanceof ResultTabController) {
                            ((ResultTabController)t.getUserData()).chageFont(font);
                        }
                    }
                }
            }
        }
    }

    private Tab getSelectedBottomTab() {
        TabContentController tcc = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
        if (tcc != null) {
            List<Tab> tabs = tcc.getBottamTabs();
            if (tabs != null) {
                for (Tab t : tabs) {
                    if (t.isSelected()) {
                        return t;
                    }
                }
            }
        }

        return null;
    }

    private List<Tab> getResultTabs() {
        List<Tab> list = new ArrayList<>();
        Map<QueryAreaWrapper, List<Tab>> map = resultTabs.get(getController().getSelectedConnectionSession().get());
        if (map != null) {
            List<Tab> tabs = map.get(getSessionCodeArea());
            if (tabs != null) {
                for (Tab t : tabs) {
                    if (t.getUserData() instanceof ResultTabController) {
                        list.add(t);
                    }
                }
            }
        }

        return list;
    }

    @Override
    protected void serviceBusyChanged(Boolean newValue) {
        super.serviceBusyChanged(newValue);

        if (!newValue) {
            executeButton.set(false);
            executeButtonMap.remove(getController().getSelectedConnectionSession().get());

            executeAllButton.set(false);
            executeAllButtonMap.remove(getController().getSelectedConnectionSession().get());

            executeWithEditButton.set(false);
            executeWithEditButtonMap.remove(getController().getSelectedConnectionSession().get());
 
            executeLockButton.set(false);
            executeLockButtonMap.remove(getController().getSelectedConnectionSession().get());
            
            executeBulkButton.set(false);
            executeBulkButtonMap.remove(getController().getSelectedConnectionSession().get());

        } else {
            Boolean v0 = executeButtonMap.get(getController().getSelectedConnectionSession().get());
            executeButton.set(v0 != null && v0);

            Boolean v1 = executeAllButtonMap.get(getController().getSelectedConnectionSession().get());
            executeAllButton.set(v1 != null && v1);

            Boolean v2 = executeWithEditButtonMap.get(getController().getSelectedConnectionSession().get());
            executeWithEditButton.set(v2 != null && v2);

            Boolean v3 = executeLockButtonMap.get(getController().getSelectedConnectionSession().get());
            executeLockButton.set(v3 != null && v3);
            
            Boolean v4 = executeBulkButtonMap.get(getController().getSelectedConnectionSession().get());
            executeBulkButton.set(v4 != null && v4);
        }
    }

    public void openFile(File userFile) {
        try {
            byte[] data = Files.readAllBytes(userFile.toPath());
//            if (data.length > MAX_OPEN_FILE_SIZE) {
//                showInfo("Cannot open", "Cannot open file with size greater than 5mb", null);
//                return;
//            }
//
            Platform.runLater(() -> {
                try {
                    addQueryTab(getController().getSelectedConnectionSession().get(), UUID.randomUUID().toString(), userFile.getAbsolutePath(), new String(data, "UTF-8"), userFile.getName(), TAB_USER_DATA__QUERY_TAB);
                } catch (UnsupportedEncodingException ex) {
                    log.error("Error", ex);
                }
            });

            if (recentFileService.isNeedSave(userFile.getAbsolutePath())) {
                recentFileService.save(new RecentFiles(userFile.getAbsolutePath()));
            }
        } catch (IOException ex) {
            DialogHelper.showError(getController(),"Error reading recent file", ex.getMessage(), ex);
        }
    }

    private void showPublishSearch() {
        SearchPublicSchemaController publicSchemaController = ((MainController)getController()).getTypedBaseController("SearchPublicSchema");
        publicSchemaController.init((SchemaDesignerModule) ((MainController)getController()).getModuleByClassName(SchemaDesignerModule.class.getName()), getController().getSelectedConnectionSession().get().getService());
    }

    private void saveScript() {
        QueryAreaWrapper codeArea = getSessionCodeArea();
        if (codeArea != null) {
            saveCodeAreaText(codeArea);
        }
    }

    public void saveCodeAreaText(QueryAreaWrapper codeArea) {
        File userFile;
        if (codeArea.getUserData() == null || ((RSyntaxTextAreaBuilder) codeArea.getUserData()).getRelativePath() == null || ((RSyntaxTextAreaBuilder) codeArea.getUserData()).getRelativePath().trim().isEmpty()) {
            FileChooser fc = new FileChooser();
            fc.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("SmartSQL Files", "*.sql"),
                    new FileChooser.ExtensionFilter("All Files", "*.*"));
            userFile = fc.showSaveDialog(getController().getStage());
        } else {
            userFile = new File(((RSyntaxTextAreaBuilder) codeArea.getUserData()).getRelativePath());
        }

        if (userFile != null) {
            try {
                Files.write(userFile.toPath(), codeArea.getText().getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                RSyntaxTextAreaBuilder codeAreaWrapperData = (RSyntaxTextAreaBuilder) codeArea.getUserData();
                codeAreaWrapperData.setRelativePath(userFile.getAbsolutePath());

                Tab tab = changedTabs.remove(codeArea);
                if (tab != null) {
                    tab.setText(userFile.getName());
                }
            } catch (Throwable ex) {
                DialogHelper.showError(getController(),"Error saveing script", ex.getMessage(), ex);
            }
        }
    }

    public MenuItem getComboxMenuItem() {
        CustomMenuItem cItem = new CustomMenuItem();
        ComboBox databaseComboBox = new ComboBox();
        databaseComboBox.getStyleClass().add("table-template-combo-box");

        databaseComboBox.setPromptText("No database selected");
        TextArea placeHolder = new TextArea("NO DATABASE FOUND");
        placeHolder.setStyle("-fx-background-color : #3c71b3;"
                + "-fx-text-fill: #293955;");
        placeHolder.setMinHeight(35.0);
        placeHolder.setEditable(false);
        databaseComboBox.setPlaceholder(placeHolder);
        databaseComboBox.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Database>() {
            {
                databaseComboBox.setUserData(this);
            }
            private WorkProc<Object[]> itsBackgroundWork
                    = new WorkProc<Object[]>() {

                TreeItem retrieve(Database t1) {

                    if (t1 != null) {
                        for (TreeItem ti : root.getChildren()) {
                            if (ti.getValue() instanceof Database
                                    && ((Database) ti.getValue()).getName().equals(t1.getName())) {
                                return ti;
                            }
                        }
                    }
                    return null;
                }

                volatile TreeItem<Object> finalTreeItem;
                volatile TreeItem<Object> root;

                ConnectionTabContentController tabController;
                TreeView tv;

                @Override
                public void updateUI() {
                    if (tv != null) {
                        Object v = finalTreeItem.getValue();
                        if (v instanceof Database) {
                            tv.getSelectionModel().select(finalTreeItem);
                        }
                        tv = null;
                    }
                    finalTreeItem = null;
                }

                @Override
                public void run() {
                    TabContentController tcc = ((MainController)getController()).getTabControllerBySession(
                            ((MainController)getController()).getSelectedConnectionSession().get());

                    if (tcc instanceof ConnectionTabContentController) {
                        tabController = (ConnectionTabContentController) tcc;

                        tv = tabController.getTreeView();

                        TreeItem ti = (TreeItem) getAddData()[0];
                        if (ti == null) {
                            ti = tv.getRoot();
                        }
                        while (ti.getParent() != null) {
                            ti = ti.getParent();
                        }

                        root = ti;
                        finalTreeItem = retrieve(((Database) getAddData()[1]));
                        callUpdate = finalTreeItem != null;
                        root = null;
                    }
                }
            };

            @Override
            public void changed(ObservableValue<? extends Database> ov,
                    Database t, Database t1) {

                ConnectionSession session = getController().getSelectedConnectionSession().get();
                RsSyntaxCompletionHelper.getInstance().setCompletions(session.getConnectionParam(), t1 == null ? null : t1.toString());
                
                if (t != null && t1 != null && (t.getName().equals(t1.getName()))) {
                    // TODO This is wrong condition - always TRUE
                    //                                || 
                    //                                t1.getName().equals(t1.getName()))){
                    return;
                }

                if (selectedTreeItemValueChangedBool) {
                    selectedTreeItemValueChangedBool = false;
                    return;
                }

                TreeItem ti = getController().getSelectedTreeItem(session).get();

                if (itsBackgroundWork.getAddData() == null) {
                    itsBackgroundWork.setAdditionalData(new Object[]{ti, t1});
                } else {
                    Object[] o = itsBackgroundWork.getAddData();
                    o[0] = ti;
                    o[1] = t1;
                }

                getController().addBackgroundWork(itsBackgroundWork);
            }
        });

            final Image databaseIcon = SmartImageProvider.getInstance().getDatabase_image();
            final Image noDatabase = SmartImageProvider.getInstance().getDatabaseDrop_image();
        databaseComboBox.setButtonCell(new ListCell<Database>() {

            {
                setFont(Font.font(Font.getDefault().getName(),
                        FontWeight.BOLD, Font.getDefault().getSize()));
                setContentDisplay(ContentDisplay.LEFT);
            }

            @Override
            protected void updateItem(Database t, boolean bln) {
                super.updateItem(t, bln);
                if (getGraphic() == null) {
                    ImageView iv = new ImageView();
                    iv.setFitWidth(16);
                    iv.setFitHeight(16);
                    setGraphic(iv);
                }
                if (bln || t == null) {
                    ((ImageView) getGraphic()).setImage(noDatabase);
                    setText("No Database Selected");
                    return;
                }
                ((ImageView) getGraphic()).setImage(databaseIcon);
                setText(t.toString());
            }
        });
        databaseComboBox.setCellFactory(new Callback<ListView<Database>, ListCell<Database>>() {
            @Override
            public ListCell<Database> call(ListView<Database> p) {
                return new ListCell<Database>() {

                    @Override
                    protected void updateItem(Database t, boolean bln) {
                        super.updateItem(t, bln);
                        if (bln) {
                            return;
                        }
                        setText(t != null ? t.toString() : "");
                        if (getGraphic() == null) {
                            ImageView iv = new ImageView();
                    iv.setFitWidth(16);
                    iv.setFitHeight(16);
                            setGraphic(iv);
                        }
                        ((ImageView) getGraphic()).setImage(databaseIcon);
                    }

                    @Override
                    public void updateSelected(boolean bln) {
                        super.updateSelected(bln);
                        setFont(Font.font(Font.getDefault().getName(),
                                bln ? FontWeight.BOLD : FontWeight.NORMAL,
                                Font.getDefault().getSize()));
                    }
                };
            }
        });
        cItem.setContent(databaseComboBox);
        return cItem;
    }

    private volatile boolean selectedTreeItemValueChangedBool = false;

    @Override
    public boolean selectedTreeItemChanged(TreeItem item) {
        selectedTreeItemValueChangedBool = true;
        ComboBox<Database> topComboBox = (ComboBox<Database>) ((CustomMenuItem) databaseMenuItem).getContent();

        Object obj = item.getValue();
        Database treeDatabase;
        Database comboDatabase = topComboBox.getSelectionModel().getSelectedItem();
        
        TreeItem rootItem = item;
        while (rootItem.getParent() != null) {
            rootItem = rootItem.getParent();
        }
        
        ConnectionParams cp = (ConnectionParams) rootItem.getValue();
        
        
        if (obj instanceof Database) {
            treeDatabase = (Database) obj;
            
            if (comboDatabase == null || !treeDatabase.equals(comboDatabase)) {
                Platform.runLater(() -> {
//                    log.debug("SELECT DATABASE: " + getController().getSelectedConnectionSession().get().getConnectionParam().toString());
//                    log.debug("DATABASE: " + cp.toString());
                    if (getController().getSelectedConnectionSession().get().getConnectionParam().equals(cp)) {
                        topComboBox.getSelectionModel().select(treeDatabase);
                    }

                    validatedQueries.clear();
                    warningEmptyKeyQueries.clear();
                });
            } else {
                selectedTreeItemValueChangedBool = false;
            }
        } else {
            if (obj instanceof DBTable) {
                treeDatabase = ((DBTable) obj).getDatabase();
            } else if (obj instanceof StoredProc) {
                treeDatabase = ((StoredProc) obj).getDatabase();
            } else if (obj instanceof Trigger) {
                treeDatabase = ((Trigger) obj).getDatabase();
            } else if (obj instanceof np.com.ngopal.smart.sql.model.Event) {
                treeDatabase = ((np.com.ngopal.smart.sql.model.Event) obj).getDatabase();
            } else if (obj instanceof np.com.ngopal.smart.sql.model.Function) {
                treeDatabase = ((np.com.ngopal.smart.sql.model.Function) obj).getDatabase();
            } else if (obj instanceof Column) {
                treeDatabase = ((Column) obj).getTable().getDatabase();
            } else if (obj instanceof View) {
                treeDatabase = ((View) obj).getDatabase();
            } else if (obj instanceof Index) {
                TreeItem parent = item.getParent().getParent();
                treeDatabase = ((DBTable) parent.getValue()).getDatabase();
            } else if (obj instanceof ConnectionTabContentController.DatabaseChildrenType) {
                TreeItem parent = item.getParent();
                treeDatabase = (Database) parent.getValue();
            } else if (obj instanceof String) {
                if ("Columns".equals(obj) || "Indexes".equals(obj)) {
                    TreeItem parent = item.getParent();
                    treeDatabase = ((DBTable) parent.getValue()).getDatabase();
                } else {
                    selectedTreeItemValueChangedBool = false;
                    return true;
                }
            } else {
                selectedTreeItemValueChangedBool = false;
                return true;
            }

            if (comboDatabase == null || !treeDatabase.getName().equals(comboDatabase.getName())) {
                Platform.runLater(() -> {
                    if (treeDatabase.equals(topComboBox.getSelectionModel().getSelectedItem())) {
                        selectedTreeItemValueChangedBool = false;
                    }
                    if (getController().getSelectedConnectionSession().get().getConnectionParam().equals(cp)) {
                        topComboBox.getSelectionModel().select(treeDatabase);
                    }

                    validatedQueries.clear();
                    warningEmptyKeyQueries.clear();
                });
            } else {
                selectedTreeItemValueChangedBool = false;
            }
        }
        return true;
    }

    /**
     * Below are the getters for getting the Help menuItem.
     *
     */
    private MenuItem getHelpItem() {

        MenuItem help = new MenuItem("Help",
                new ImageView(SmartImageProvider.getInstance().getHelp_image()));
        help.setAccelerator(new KeyCodeCombination(KeyCode.F1));
        help.setOnAction((ActionEvent event) -> {
            URLUtils.openUrl("http://www.smartmysql.com/smartmysql-documentation/");
        });
        return help;
    }

    private MenuItem getAboutItem() {
        MenuItem about = new MenuItem("About",
                new ImageView(SmartImageProvider.getInstance().getAbout_image()));
        about.setOnAction((ActionEvent t) -> {
            getController().aboutPage();
        });
        return about;
    }

    private MenuItem getKeyboardShortcutItem() {

        MenuItem keyboardShortcut = new MenuItem("Keyboard Shortcuts",
                new ImageView(SmartImageProvider.getInstance().getKeyboardShortcuts_image()));
        keyboardShortcut.setOnAction((ActionEvent event) -> {
            ((MainController)getController()).getTypedBaseController("KeyboardShortcuts");
        });
        return keyboardShortcut;
    }

    private MenuItem getChangeRegistrationDetailsItem() {

        MenuItem changeRegistrationDetails = new MenuItem("Change Registration Details",
                new ImageView(SmartImageProvider.getInstance().getRegistration_image()));
        changeRegistrationDetails.setOnAction((ActionEvent event) -> {
            LicenseController lc = StandardBaseControllerProvider.getController(getController(), "License");
            lc.prepareLicenseScreen(usageStatisticService, false);
        });
        return changeRegistrationDetails;
    }

    private MenuItem getCheckForUpdatesItem() {
        MenuItem checkForUpdates = new MenuItem("Check For Updates",
                new ImageView(SmartImageProvider.getInstance().getCheckUpdate_image()));
        checkForUpdates.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                CheckForUpdatesController cuc = StandardBaseControllerProvider.getController(getController(), "CheckForUpdates");
                Parent p = cuc.getUI();
                Scene scene = new Scene(p);
                scene.getStylesheets().add(QueryTabModule.class.getResource("css/dialog-style.css").toExternalForm());
                Stage dialog = new Stage();
                dialog.initStyle(StageStyle.UTILITY);
                dialog.setScene(scene);
                dialog.initModality(Modality.WINDOW_MODAL);
                dialog.initOwner(getController().getStage());
                dialog.setTitle("Check For Updates");
                
                cuc.init();
                
                dialog.showAndWait();
            }
        });
        return checkForUpdates;
    }

    private MenuItem getDisableValidationItem() {

        MenuItem disableValidation = new MenuItem("Disable/Enable validation",
                new ImageView(SmartImageProvider.getInstance().getError_image()));
        disableValidation.setOnAction((ActionEvent event) -> {
            needValidateQuery.set(!needValidateQuery.get());

            for (QueryAreaWrapper qaw : getAllCodeAreas()) {
                try {
                    checkAreaQueries(RSyntaxTextAreaBuilder.getRSyntaxTextAreaBuilder((RSyntaxTextArea) qaw.getWrappedObject()), getController().getSelectedConnectionSession().get());
                } catch (IOException ex) {
                }
            }

            disableValidation.graphicProperty().set(
                    needValidateQuery.get()
                            ? new ImageView(SmartImageProvider.getInstance().getError_image())
                            : new ImageView(SmartImageProvider.getInstance().getErrorDisable_image()));
        });
        return disableValidation;
    }

    /**
     * Below are the getters for getting the window menuItem.
     *
     */
    private MenuItem getCascadeItem() {

        MenuItem cascade = new MenuItem("Stack Window Top", new ImageView(SmartImageProvider.getInstance().getCascade_image()));
        cascade.setOnAction((ActionEvent event) -> {
            getController().togleTabPaneLayerToRight(false);
        });
        return cascade;
    }

    private MenuItem getTileWindowItem() {

        MenuItem tileWindow = new MenuItem("Stack Window Right", new ImageView(SmartImageProvider.getInstance().getTile_image()));
        tileWindow.setOnAction((ActionEvent event) -> {
            getController().togleTabPaneLayerToRight(true);
        });
        return tileWindow;
    }

    // i can't seem to see what this actually  does
    private MenuItem getIconArrangeItem() {
        MenuItem iconArrange = new MenuItem("Icon Arrange", new ImageView(SmartImageProvider.getInstance().getArrange_image()));
        iconArrange.setOnAction((ActionEvent event) -> {
            DialogHelper.showError(getController(),"Not implemented", "Not implemented", null);
        });
        return iconArrange;
    }

//    /**
//     * Below are the getters for getting the tools menuItem.
//     *
//     */
    private MenuItem getexecuteSQLScriptItem() {
        MenuItem executeSQLscript = new MenuItem("Execute SQL Script", new ImageView(SmartImageProvider.getInstance().getDatabaseExecuteScript_image()));

        return executeSQLscript;
    }

    private MenuItem getPreferenceItem() {
        MenuItem preference = new MenuItem("Settings / Preferences", new ImageView(SmartImageProvider.getInstance().getPreferences_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(0);
        });
        return preference;
    }
    
    
    private MenuItem getPreferenceGeneralItem() {
        MenuItem preference = new MenuItem("General", new ImageView(SmartImageProvider.getInstance().getPref_0_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(0);
        });
        return preference;
    }
    
    private MenuItem getPreferencePowerToolsItem() {
        MenuItem preference = new MenuItem("Power Tools", new ImageView(SmartImageProvider.getInstance().getPref_1_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(1);
        });
        return preference;
    }
    
    private MenuItem getPreferenceFontsItem() {
        MenuItem preference = new MenuItem("Fonts & Editor Settings", new ImageView(SmartImageProvider.getInstance().getPref_2_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(2);
        });
        return preference;
    }
    
    private MenuItem getPreferenceFormaterItem() {
        MenuItem preference = new MenuItem("SQL Formater", new ImageView(SmartImageProvider.getInstance().getPref_3_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(3);
        });
        return preference;
    }
    
    private MenuItem getPreferenceOthersItem() {
        MenuItem preference = new MenuItem("Others", new ImageView(SmartImageProvider.getInstance().getPref_4_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(4);
        });
        return preference;
    }
    
    private MenuItem getPreferenceQueryBrowserItem() {
        MenuItem preference = new MenuItem("Query Browser", new ImageView(SmartImageProvider.getInstance().getPref_5_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(5);
        });
        return preference;
    }
    
    private MenuItem getPreferenceQueryOptimizerItem() {
        MenuItem preference = new MenuItem("Query Optimizer", new ImageView(StandardDefaultImageProvider.getInstance().getAnalyzer_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(6);
        });
        return preference;
    }
    
    private MenuItem getPreferenceDebuggerItem() {
        MenuItem preference = new MenuItem("Debugger", new ImageView(StandardDefaultImageProvider.getInstance().getDebugger_image()));
        preference.setOnAction((ActionEvent event) -> {
            showPreferenceAction(7);
        });
        return preference;
    }
    

    private MenuItem getExportAllRowsTableItem() {
        MenuItem exportAllRows = new MenuItem("Export All Rows of Table Data",
                new ImageView(SmartImageProvider.getInstance().getTableExport_small_image()));

        return exportAllRows;
    }

    private MenuItem getBackUpDataAsSQLDumpItem() {
        MenuItem backup = new MenuItem("Back up Database as SQL Dump",
                new ImageView(SmartImageProvider.getInstance().getDatabaseDump_image()));
        backup.setAccelerator(KeyCombination.keyCombination("Ctrl+Alt+E"));
        return backup;
    }

    private MenuItem getFlushItem() {
        MenuItem flush = new MenuItem("Flush", new ImageView(SmartImageProvider.getInstance().getFlush_image()));
        flush.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN));
        flush.setOnAction((ActionEvent t) -> {
            FlushController fc = ((MainController)getController()).getTypedBaseController("Flush");
            fc.init();
        });

        return flush;
    }
    
    private MenuItem getProfilerMenuItem() {
        MenuItem profMenuItem = new MenuItem("Profiler", FunctionHelper.constructImageViewWithSize(
                new ImageView(StandardDefaultImageProvider.getInstance().getProfilerTabIcon_image()),
                20, 20));
        profMenuItem.setOnAction((ActionEvent event) -> {
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            ConnectionParams params = session.getConnectionParam();
            WorkProc<ConnectionParams> openConnectProc = new WorkProc<ConnectionParams>() {
                @Override
                public void run() {
                    ConnectionParams cp = params.copyOnlyParams();
                    cp.setId(params.getId());
                    cp.setSelectedModuleClassName(SmartProfilerModule.class.getName());
                    ModuleController mc = getMainController().getModuleByClassName(cp.getSelectedModuleClassName());
                    if (mc instanceof ConnectionModuleInterface) {
                        ((ConnectionModuleInterface) mc).connectToConnection(cp);
                    }
                }
            };

            getMainController().addWorkToBackground(openConnectProc);
        });
        return profMenuItem;
    }

    private MenuItem getTableDiagnosticsItem() {
        MenuItem tableDiagnostics = new MenuItem("Table Diagnostics", new ImageView(SmartImageProvider.getInstance().getTableDiagnostics_image()));
        tableDiagnostics.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN));
        tableDiagnostics.setOnAction((ActionEvent t) -> {
            TableDiagnosticsController td = ((MainController)getController()).getTypedBaseController("TableDiagnostics");
            try {
                td.init(getController().getSelectedConnectionSession().get(),
                        getController().getStage());
            } catch (SQLException ex) {
            }
        });

        return tableDiagnostics;
    }

    private MenuItem getHistoryItem() {
        MenuItem history = new MenuItem("History", new ImageView(SmartImageProvider.getInstance().getHistory_image()));
        history.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        history.setOnAction((ActionEvent t) -> {
            getController().reachHistoryTab();
        });
        return history;
    }

    private MenuItem getFeqItem() {
        MenuItem feq = new MenuItem("Frequency Executed Queries", new ImageView(SmartImageProvider.getInstance().getFeg_image()));
        feq.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        feq.setOnAction((ActionEvent t) -> {
            getController().reachFeqTab();
        });
        return feq;
    }

    private MenuItem getUserManagerItem() {
        MenuItem userManager = new MenuItem("User Manager", new ImageView(SmartImageProvider.getInstance().getUser_image()));
        userManager.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHORTCUT_DOWN));
        userManager.setOnAction((ActionEvent t) -> {
            showUserManager();
        });

        return userManager;
    }
    
    private void showUserManager() {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        try {
            ((UserManagerModule) getMainController().getModuleByClassName(UserManagerModule.class.getName()))
                    .openUserManager(session, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Menu getShowItem() {
        Menu showItem = new Menu("Show",
                new ImageView(SmartImageProvider.getInstance().getShow_image()));

        MenuItem variable = new MenuItem("Variables", new ImageView(SmartImageProvider.getInstance().getVariables_image()));
        variable.setOnAction((ActionEvent t) -> {
            ShowParentController controller1 = ((MainController)getController()).getTypedBaseController("ShowParent");
            controller1.init(getController().getStage(), ShowParentController.ShowMenuItems.Variable, getController().getSelectedConnectionSession().get());
        });

        MenuItem processlist = new MenuItem("ProcessList", new ImageView(SmartImageProvider.getInstance().getProcessList_image()));
        processlist.setOnAction((ActionEvent t) -> {
            ShowParentController controller1 = ((MainController)getController()).getTypedBaseController("ShowParent");
            controller1.init(getController().getStage(), ShowParentController.ShowMenuItems.Processlist, getController().getSelectedConnectionSession().get());
        });

        MenuItem status = new MenuItem("Status", new ImageView(SmartImageProvider.getInstance().getStatus_image()));
        status.setOnAction((ActionEvent t) -> {
            ShowParentController controller1 = ((MainController)getController()).getTypedBaseController("ShowParent");
            controller1.init(getController().getStage(), ShowParentController.ShowMenuItems.Status, getController().getSelectedConnectionSession().get());
        });

        showItem.getItems().addAll(variable, processlist, status);
        return showItem;
    }

    private MenuItem getChangeLangueItem() {
        MenuItem changeLanguageItem = new MenuItem("Change Language", new ImageView(SmartImageProvider.getInstance().getLanguage_image()));
        changeLanguageItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHIFT_DOWN, KeyCombination.ALT_DOWN));
        return changeLanguageItem;
    }

    private Menu getImportExportConnectionDetailsItem() {
        Menu importExportItem = new Menu("Import/Export Connection Details",
                new ImageView(SmartImageProvider.getInstance().getBackUpExport_image()));
        importConnectionDetails = new MenuItem("Import Connection Details", new ImageView(SmartImageProvider.getInstance().getImport_ssh_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        importConnectionDetails.setOnAction((ActionEvent event) -> {
            ImportConnectionDetails controller1 = ((MainController)getController()).getTypedBaseController("ImportConnectionDetails");
            controller1.init(getController().getStage());
        });

        exportConnectionDetails = new MenuItem("Export Connection Details",
                new ImageView(SmartImageProvider.getInstance().getExport_ssh_image(DefaultImageProvider.IMAGE_SIZE_WIDTH, DefaultImageProvider.IMAGE_SIZE_HEIGHT)));
        exportConnectionDetails.setOnAction((ActionEvent event) -> {
            ExportConnectionDetails controller1 = ((MainController)getController()).getTypedBaseController("ExportConnectionDetails");
            controller1.init(getController().getStage());
        });

        importExportItem.getItems().addAll(exportConnectionDetails, importConnectionDetails);

        return importExportItem;
    }

//    private void importConnections() {
//        final ConnectionParamService db = injector.getInstance(ConnectionParamService.class);
//
//        final FileChooser fileChooser = new FileChooser();
//        fileChooser.setTitle("Select exported connections for import");
//
//        final File exportFile = fileChooser.showOpenDialog(getController().getStage());
//        if (exportFile != null) {
//            try {
//                final byte[] encryptBytes = Files.readAllBytes(exportFile.toPath());
//
//                // decrypt data
//                final DESKeySpec desKeySpec = new DESKeySpec(desspec.getBytes());
//                final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
//                final SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
//
//                final Cipher decryptCipher = Cipher.getInstance("DES");
//                decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
//
//                final byte[] bytes = decryptCipher.doFinal(encryptBytes);
//
//                final String json = new String(bytes, "UTF-8");
//                final Gson gson = new Gson();
//
//                final List<ConnectionParams> params = gson.fromJson(json, new TypeToken<List<ConnectionParams>>() {
//                }.getType());
//                
//                if (params != null && !params.isEmpty()) {
//                    for (final ConnectionParams p : params) {
//                        // Gson return HashTreeMap
//                        p.setModuleParams(new HashMap<>(p.getModuleParams()));
//                        db.save(p);
//                    }
//
//                    // need close or opened tab controllers
//                    getController().closeAllTabs();
//                }
//            } catch (Exception ex) {
//                showError("Error", "Error occurred while exporting connections", ex);
//            }
//        }
//    }
//    
//    
    /**
     * Below are getters for getting the favourite menuitem.
     *
     *
     */
    private MenuItem getAddToFavouriteItem() {
        MenuItem addToFavourite = new MenuItem("Add To Favorites",
                new ImageView(SmartImageProvider.getInstance().getAddFavorite_image()));
        addToFavourite.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));

        addToFavourite.setOnAction((ActionEvent t) -> {
            AddFavoriteController controller1 = ((MainController)getController()).getTypedBaseController("AddFavourite");
            controller1.init(getController().getStage());
        });
        return addToFavourite;
    }

    private MenuItem getOrganiseFavouriteItem() {
        MenuItem organiseFavourite = new MenuItem("Organize Favorite",
                new ImageView(SmartImageProvider.getInstance().getOrganizeFavourite_image()));
        organiseFavourite.setOnAction((ActionEvent t) -> {
            OrganiseFavouriteController controller1 = ((MainController)getController()).getTypedBaseController("OrganiseFavourite");
            controller1.init(getController().getStage(), getSessionCodeArea() != null ? getSessionCodeArea().getSelectedText() : "");
        });

        return organiseFavourite;
    }

//    private MenuItem getRefreshFavouriteItem() {
//        MenuItem refreshFavourite = new MenuItem("Refresh Favorite",
//                new ImageView(new Image("/np/com/ngopal/smart/sql/ui/images/connection/refresh.png",DefaultImageProvider.IMAGE_SIZE_WIDTH,DefaultImageProvider.IMAGE_SIZE_HEIGHT,false,false)));
//        refreshFavourite.setOnAction((ActionEvent t) -> {
//            updateFavorites();
//        });
//
//        return refreshFavourite;
//    }
    private void updateFavorites() {
        List<MenuItem> menus = createMenus(null);

//        menus.add(0, refreshFav);
        menus.add(0, organiseFav);
//        menus.add(0, addFav);

        getController().clearMenuItems(getDependencyModuleClass(), MenuCategory.FAVORITES);
        getController().addMenuItems(getDependencyModuleClass(), MenuCategory.FAVORITES, menus.toArray(new MenuItem[0]));
    }

    private List<MenuItem> createMenus(Long parent) {
        List<MenuItem> items = new ArrayList<>();

        List<Favourites> list = favServ.findChildren(parent);
        if (list != null) {

            for (Favourites f : list) {
                if (f.isFolder()) {
                    Menu m = new TextMenu();
                    m.setMnemonicParsing(false);
                    m.setText(f.getName());
                    m.disableProperty().bind(queryTabNotSelected);
                    items.add(m);

                    m.getItems().addAll(createMenus(f.getId()));

                } else {
                    MenuItem mi = new TextMenuItem();
                    mi.setText(f.getName());
                    mi.setMnemonicParsing(false);
                    mi.disableProperty().bind(queryTabNotSelected);
                    items.add(mi);

                    mi.setOnAction((ActionEvent event) -> {
                        QueryAreaWrapper area = getSessionCodeArea();
                        if (area != null) {
                            area.insertText(area.getCaretPosition(CARET_POSITION_FROM_START), f.getQuery());
                        }
                    });
                }
            }
        }

        return items;
    }

    MenuItem insertTemplates;

    public MenuItem getQueryEditor() {
        MenuItem queryEditor = new MenuItem("New Query Editor", new ImageView(SmartImageProvider.getInstance().getNewEditor_image()));
        queryEditor.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHORTCUT_DOWN));
        queryEditor.setOnAction((ActionEvent event) -> {
            addQueryTab(getController().getSelectedConnectionSession().get(), null);
        });
        return queryEditor;
    }

    public MenuItem getOldQueryEditor() {
        MenuItem queryEditor = new MenuItem("Old Query Editor", new ImageView(SmartImageProvider.getInstance().getOldEditor_image()));
        queryEditor.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHIFT_DOWN, KeyCodeCombination.ALT_DOWN));
        queryEditor.setOnAction((ActionEvent event) -> {
            //addOldQueryAreaTab(getController().getSelectedConnectionSession().get(), null, "", "OldQueryEditor");
        });
        return queryEditor;
    }

    public MenuItem getQueryBuilder() {
        MenuItem queryBuilder = new MenuItem("New Query Builder", new ImageView(SmartImageProvider.getInstance().getQueryBuilder_image()));

        queryBuilder.setAccelerator(new KeyCodeCombination(KeyCode.K, KeyCombination.SHORTCUT_DOWN));
        queryBuilder.setOnAction((ActionEvent event) -> {
            QueryBuilderController qbc = ((MainController)getController()).getTypedBaseController("QueryBuilder");
            qbc.init(getController().getSelectedConnectionSession().get());
            Tab tab = new Tab("Query Builder");
            tab.setGraphic(new ImageView(SmartImageProvider.getInstance().getQueryBuilder_small_image()));
            tab.setContent(qbc.getUI());

            tab.setOnCloseRequest((Event event1) -> {
                qbc.destroy();
            });

            getController().addTab(tab, qbc.session);
            getController().showTab(tab, qbc.session);
        });

        return queryBuilder;
    }

    public MenuItem getSchemaDesigner() {
        MenuItem schemaDesigner = new MenuItem("New Schema Designer",
                new ImageView(SmartImageProvider.getInstance().getSchemaDesigner_image()));
        schemaDesigner.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN));
        schemaDesigner.setOnAction((ActionEvent event) -> {

            String selectedDatabase = null;
            String selectedSchemaName = null;
            // If user not selected any database then ask end user to selecte database or create new schemadiagram
            ConnectionSession s = getController().getSelectedConnectionSession().get();
            if (s != null && s.getConnectionParam() != null) {
                List<String> list = s.getConnectionParam().getDatabase();
                if (list != null && list.size() == 1) {
                    selectedDatabase = list.get(0);
                }

                // in others case need show selection dialog            
                if (selectedDatabase == null) {
                    OpenSchemaDesignerDialogController controller = ((MainController)getController()).getTypedBaseController("OpenSchemaDesignerDialog");
                    controller.init(s.getConnectionParam().getDatabases());

                    Parent p = controller.getUI();
                    Scene scene = new Scene(p);
                    scene.getStylesheets().add(QueryTabModule.class.getResource("css/dialog-style.css").toExternalForm());
                    Stage dialog = new Stage();
                    dialog.initStyle(StageStyle.UTILITY);
                    dialog.setScene(scene);
                    dialog.initModality(Modality.WINDOW_MODAL);
                    dialog.initOwner(getController().getStage());

                    dialog.setTitle("Select");
                    dialog.showAndWait();

                    selectedDatabase = controller.getSelectedDatabase();
                    selectedSchemaName = controller.getSchemaName();

                    if (selectedDatabase == null && selectedSchemaName == null) {
                        return;
                    }
                }

                // TODO Show Schema designer
                ConnectionParams cp = s.getConnectionParam().copyOnlyParams();
                cp.setSelectedModuleClassName(SchemaDesignerModule.class.getName());
                if (selectedDatabase != null) {
                    cp.setDatabase(Arrays.asList(new String[]{selectedDatabase}));
                    cp.setMakeBackwardEngine(true);
                } else {
                    cp.setNewSchemaName(selectedSchemaName);
                }

                ((MainController)getController()).openConnection(s.getService(), cp, true, null);
            }
        });

        return schemaDesigner;
    }

    @Override
    public boolean uninstall() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public static final String TAB_USER_DATA__QUERY_TAB = "QueryTab";
    public static final String TAB_USER_DATA__FUNCTION_TAB = "FunctionTab";
    public static final String TAB_USER_DATA__TABLE_TAB = "TableTab";
    public static final String TAB_USER_DATA__PROCEDURE_TAB = "ProcedureTab";
    public static final String TAB_USER_DATA__VIEW_TAB = "ViewTab";
    public static final String TAB_USER_DATA__INDEX_TAB = "IndexTab";
    public static final String TAB_USER_DATA__TRIGGER_TAB = "TriggerTab";
    public static final String TAB_USER_DATA__EVENT_TAB = "EventTab";
    public static final String TAB_USER_DATA__NAMED_QUERY_TAB = "NamedQueryTab";

    public static final String USER_DATA__QUERY_TABS = "queryTabs";

    private static final String STORE_DIVIDER = "<divider>";

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", MySQLKeywords.getTypesArray()) + ")\\b";

    private static final String FUNCTION_PATTERN = "\\b(" + String.join("|", MySQLFunctions.FUNCTIONS) + ")\\b";

    private static final String PAREN_PATTERN = "\\(|\\)";

    private static final String BRACE_PATTERN = "\\{|\\}";

    private static final String BRACKET_PATTERN = "\\[|\\]";

    private static final String SEMICOLON_PATTERN = "\\;";

    private static final String OPERATOR_PATTERN = "[-+/*><=!%]";

    private static final String STRING_PATTERN_0 = "\"(\\\\.|[^\"])*\"";
    private static final String STRING_PATTERN_1 = "'(\\\\.|[^'])*'";
    private static final String STRING_PATTERN_2 = "`(\\\\.|[^`])*`";

    private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";

    public static final Pattern PATTERN = Pattern.compile(
            "(?<STRING0>" + STRING_PATTERN_0 + ")"
            + "|(?<STRING1>" + STRING_PATTERN_1 + ")"
            + "|(?<STRING2>" + STRING_PATTERN_2 + ")"
            + "|(?<KEYWORD>" + KEYWORD_PATTERN + ")"
            + "|(?<FUNCTION>" + FUNCTION_PATTERN + ")"
            + "|(?<PAREN>" + PAREN_PATTERN + ")"
            + "|(?<BRACE>" + BRACE_PATTERN + ")"
            + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
            + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
            + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
            + "|(?<OPERATOR>" + OPERATOR_PATTERN + ")", Pattern.CASE_INSENSITIVE);

    private static final Pattern SELECT_PATTER = Pattern.compile("\\bselect\\b");
    private static final Pattern UPDATE_PATTER = Pattern.compile("\\bupdate\\b");
    private static final Pattern CREATE_PATTER = Pattern.compile("\\bcreate\\b");
    private static final Pattern ALTER_PATTER = Pattern.compile("\\balter\\b");
    private static final Pattern INSERT_PATTER = Pattern.compile("\\binsert\\b");
    private static final Pattern DELETE_PATTER = Pattern.compile("\\bdelete\\b");
    private static final Pattern REPLACE_PATTER = Pattern.compile("\\breplace\\b");
    private static final Pattern SHOW_PATTER = Pattern.compile("\\bshow\\b");
    private static final Pattern FLUSH_PATTER = Pattern.compile("\\bflush\\b");
    private static final Pattern DELIMITER_START_PATTER = Pattern.compile("\\delimiter\\b\\s+\\$\\$");
    private static final Pattern DELIMITER_END_PATTER = Pattern.compile("\\$\\$\\s*\\delimiter\\b\\s+;");

    @Override
    public void postConnection(final ConnectionSession session, Tab t) throws Exception {
        parentTabs.put(session, t);
        if (!Flags.inRestorationState()) {
            Platform.runLater(() -> addQueryTab(session, null));
        }
    }

    private void checkAreaQueries(RSyntaxTextAreaBuilder rSyntaxTextAreaBuilder, ConnectionSession session) throws IOException {
        Map<Integer, String> warningEmptyKeyResult = new HashMap<>();
        Map<Integer, String> warningMoreThen10kRowsResult = new HashMap<>();
        Map<Integer, String> errorResult = new HashMap<>();

        CodeAreaWrapper.checkQueries(
                rSyntaxTextAreaBuilder.getTextArea(),
                rSyntaxTextAreaBuilder.getTextArea().getCaretPosition(),
                rSyntaxTextAreaBuilder.getTextArea().getText(),
                ((MainController)getController()),
                session,
                needValidateQuery,
                0,
                warningEmptyKeyResult,
                warningMoreThen10kRowsResult,
                errorResult
        );

        rSyntaxTextAreaBuilder.setWarningEmptyKeyResult(warningEmptyKeyResult);
        rSyntaxTextAreaBuilder.setWarningMoreThen10kRowsResult(warningMoreThen10kRowsResult);
        
        int s = 0;
        for (Integer row : warningEmptyKeyResult.keySet()) {
            if (warningEmptyKeyResult.get(row) != null) {
                s++;
            }
        }
        for (Integer row : warningMoreThen10kRowsResult.keySet()) {
            if (warningMoreThen10kRowsResult.get(row) != null) {
                s++;
            }
        }
        
        if (s > 0) {
            ((MainController)getController()).usedWarnIcon(s);
        }
        
        Platform.runLater(() -> {
            try {
                rSyntaxTextAreaBuilder.getTextScrollPane().getGutter().removeAllTrackingIcons();
                for (Integer row : errorResult.keySet()) {
                    if (errorResult.get(row) != null) {
                        rSyntaxTextAreaBuilder.getTextScrollPane().getGutter().addLineTrackingIcon(
                                row - 1, StandardDefaultImageProvider.getInstance().getErrorIcon_image(), errorResult.get(row));
                    }
                }

                for (Integer row : warningEmptyKeyResult.keySet()) {
                    if (warningEmptyKeyResult.get(row) != null) {
                        rSyntaxTextAreaBuilder.getTextScrollPane().getGutter().addLineTrackingIcon(
                                row - 1, StandardDefaultImageProvider.getInstance().getWarningIcon_image(), "The query is not using Index");
                    }
                }

                for (Integer row : warningMoreThen10kRowsResult.keySet()) {
                    if (warningMoreThen10kRowsResult.get(row) != null) {
                        rSyntaxTextAreaBuilder.getTextScrollPane().getGutter().addLineTrackingIcon(
                                row - 1, StandardDefaultImageProvider.getInstance().getWarningIcon_image(), "The query needs to scan more than 10k rows to retrieve records. Please use SQL Optimizer to tune this query");
                    }
                }
            } catch (Throwable exception1) {
                log.error(exception1.getMessage(), exception1);
            }

//            if (documentChangedWorkProc != null) {
//                documentChangedWorkProc.setAdditionalData(Boolean.FALSE);
//            }
        });
    }

    public void addQueryTab(final ConnectionSession session, String relativePath) {
        addQueryTab(session, relativePath, "");
    }

    public Tab addQueryTab(final ConnectionSession session, String scriptPath, String text) {
        return addQueryTab(session, UUID.randomUUID().toString(), scriptPath, text, "Query", TAB_USER_DATA__QUERY_TAB);
    }

    public Tab addQueryTab(final ConnectionSession session, String uuid, String scriptPath, String text, String tabName, String type) {

        Tab tab = new Tab(tabName);
        runnedQueryCountMap.put(tab, new SimpleStringProperty("0 queries runned"));
        
//        tab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getNewEditor_image()), 18, 18));
        tab.setId(type);
        tab.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) { 
                if(newValue != null && newValue){
                    Platform.runLater(()-> {
                        getController().getTabControllerBySession(session).showLeftPane();
                    });
                }
            }
        });

        RSyntaxTextAreaBuilder rSyntaxTextAreaBuilder = new RSyntaxTextAreaBuilder(uuid, initMenuAndMenuItems(),
                session, preferenceService,
                new Callback<File, Void>() {
            public Void call(File p) {
                openFile(p);
                return null;
            }
        },
                new Callback<List<Object>, Integer>() {
            private String prevDatabase;

            public Integer call(List<Object> p) {
                if (prevDatabase == null) {
                    prevDatabase = (String) p.get(1);
                }
                Integer i = tryDragWithJoin(session, prevDatabase, (String) p.get(1), (String) p.get(2),
                        (QueryAreaWrapper) p.get(3), (Map<String, Object>)p.get(4), (Double) p.get(5), (Double) p.get(6));

                prevDatabase = (String) p.get(1);
                                
                ((MainController)getController()).usedQueryBuilder();
                return i;
            }
        }, ((MainController)getController()),
        ((MainController)getController()).getPerformanceTuningService(),
        ((MainController)getController()).getRecommendationService(),
        ((MainController)getController()).getColumnCardinalityService(),
        ((MainController)getController()).getUsageService());

        SwingUtilities.invokeLater(() -> {
            
//            log.debug("SwingUtilities START" + text);
            rSyntaxTextAreaBuilder.init(text);
        
            Label autocompleteLabel = new Label("Autocomplete: You can get history queries using [Ctrl+shift+H], Favourites: [Ctrl+Shift+F] and FEQ: [Ctrl+Shift+Q]");
            autocompleteLabel.setMaxWidth(Double.MAX_VALUE);
            autocompleteLabel.setStyle("-fx-font-size: 10; -fx-text-fill: #6b8ec1; -fx-padding: 5; ");
            autocompleteLabel.managedProperty().bind(autocompleteLabel.visibleProperty());
            autocompleteLabel.setContentDisplay(ContentDisplay.RIGHT);

            rSyntaxTextAreaBuilder.setAutocompeteLabel(autocompleteLabel);

//            log.debug("SwingUtilities 0");
            PreferenceData pd = preferenceService.getPreference();
            if (pd.isAutocompleteEnable() && pd.isAutocompleteShowHelp()) {
                rSyntaxTextAreaBuilder.hideAutocomplete(false);
            } else {
                rSyntaxTextAreaBuilder.hideAutocomplete(true);
            }

            rSyntaxTextAreaBuilder.changeFont(pd.getFontsSqlEditor());
            rSyntaxTextAreaBuilder.changeTabSettings(pd.getTabsSize(), pd.isTabsInsertSpaces());

            StackPane pane = new StackPane();
            pane.setMaxHeight(Double.MAX_VALUE);
            pane.getChildren().add(rSyntaxTextAreaBuilder.getSwingNode());
            VBox.setVgrow(pane, Priority.ALWAYS);

            HBox hb = new HBox(autocompleteLabel, rSyntaxTextAreaBuilder.getCloseAutocomplete());
            hb.setAlignment(Pos.CENTER);
            HBox.setHgrow(autocompleteLabel, Priority.ALWAYS);

//            log.debug("SwingUtilities 1");
            Platform.runLater(() -> {
                tab.setContent(new VBox(hb, pane));
            });
            tab.setOnSelectionChanged((Event event) -> {
                SwingUtilities.invokeLater(() -> {
                    rSyntaxTextAreaBuilder.getQueryWrapper().refreshQueryArea();
                    rSyntaxTextAreaBuilder.getQueryWrapper().requestFocus();
                });
            });

            ArrayList<Tab> tabContentHolderForBottomTabs = new ArrayList();
            tab.setUserData(tabContentHolderForBottomTabs);

            Platform.runLater(() -> {
                parseImageToTabFromString(type, tab);
            });

//            log.debug("SwingUtilities 2");

            TabContentController tabController = getController().getTabControllerBySession(session);
            addDBClickListener(tabController, tab, rSyntaxTextAreaBuilder.getQueryWrapper());

            tab.setOnCloseRequest((Event event) -> {

                PreferenceData pd0 = preferenceService.getPreference();

                if (pd0.isPromtUnsavedTab() && changedTabs.containsValue(tab)) {
                    DialogResponce responce = DialogHelper.showConfirm(getController(), "SmartSQL", "Do you want to save the changes?");
                    switch (responce) {
                        case OK_YES:
                            saveScript();
                            break;
                        case CANCEL:
                            event.consume();
                    }
                }

                if (!event.isConsumed()) {
                    queriesTabCloseHandlerForOpenedSession(tab, session);
                }

                Consumer c = dbClickListeners.remove(tab);
                if (c != null && tabController != null) {
                    tabController.removeOnDoubleMouseClickedOnLeftTree(c);
                }
                
                SimpleStringProperty prop = runnedQueryCountMap.remove(tab);
                if (prop.isBound()) {
                    prop.unbind();;
                }
            });

//            log.debug("SwingUtilities 3");
            MessagesTabModule messageTabModule = (MessagesTabModule) ((MainController)getController()).getModuleByClassName(MessagesTabModule.class.getName());
//            log.debug("SwingUtilities 31");
            ObservableMap<Tab, String> tab_stringMap = (ObservableMap<Tab, String>) messageTabModule.getSessionMessageBottomTabTextFlow(session).getUserData();
//            log.debug("SwingUtilities 32");
            tab_stringMap.put(tab, "");

            rSyntaxTextAreaBuilder.getTextArea().addCaretListener(new CaretListener() {
                Label l = (Label) ((MainController)getController()).getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA);

                @Override
                public void caretUpdate(CaretEvent e) {
                    Integer[] i = (Integer[]) l.getUserData();
                    i[0] = rSyntaxTextAreaBuilder.getTextArea().getCaretLineNumber();
                    i[1] = rSyntaxTextAreaBuilder.getTextArea().getCaretOffsetFromLineStart();
                    Platform.runLater(() -> {
                        l.setText("Lin: " + String.valueOf(i[0] + 1) + ", Col: " + String.valueOf(i[1]));
                    });
                }
            });

//            log.debug("SwingUtilities 34");
            rSyntaxTextAreaBuilder.getQueryWrapper().setRelativePath(scriptPath);
//            log.debug("SwingUtilities 35");


//            if (text != null && text.length() > 0) {
//                rSyntaxTextAreaBuilder.getQueryWrapper().appendText(text);
//                //rSyntaxTextAreaBuilder.getQueryWrapper().positionCaret(0);
//            }
            
//            log.debug("SwingUtilities 4");

            rSyntaxTextAreaBuilder.addDocumentListenerCodeExecutor(new Callable<Object>() {

                Thread saver;
                volatile boolean saved = false;
                volatile boolean first = true;
                volatile Long lastTime;

                Thread checkAreaQueries;
                volatile Long checkAreaQueriesTime;
                volatile boolean checkAreaQueriesRunned = false;

                @Override
                public Object call() throws Exception {

                    checkAreaQueriesTime = new Date().getTime();
                    if (!checkAreaQueriesRunned) {

                        checkAreaQueriesRunned = true;
                        checkAreaQueries = new Thread(() -> {

                            if (!changedTabs.containsKey(rSyntaxTextAreaBuilder.getQueryWrapper())) {
                                changedTabs.put(rSyntaxTextAreaBuilder.getQueryWrapper(), tab);
                            }

                            // wait 3 sec before checking
                            while (new Date().getTime() - checkAreaQueriesTime < 2000) {
                                try {
                                    Thread.sleep(500);
                                } catch (InterruptedException ex) {
                                }
                            }                        

                            try {
                                if (!changedTabs.containsKey(rSyntaxTextAreaBuilder.getQueryWrapper())) {
                                    changedTabs.put(rSyntaxTextAreaBuilder.getQueryWrapper(), tab);
                                }

                                checkAreaQueries(rSyntaxTextAreaBuilder, session);
                            } catch (Throwable exception) {
                                log.error(exception.getMessage(), exception);
                            }

                            checkAreaQueriesRunned = false;
                        });
                        checkAreaQueries.setDaemon(true);
                        checkAreaQueries.start();
                    }

                    if (session.isOpen()) { 
                        lastTime = new Date().getTime();

                        if (saver == null || saved) {

                            saved = false;
                            saver = new Thread(() -> {
                                try {
                                    while (new Date().getTime() - lastTime < 3000) {
                                        Thread.sleep(100);
                                    }
                                    if (session.isOpen()) {
                                        getController().saveSessionTab(session, rSyntaxTextAreaBuilder.getQueryWrapper().getUuid());
                                    }

                                } catch (Throwable th) {
                                } finally {
                                    saved = true;
                                }

                            });
                            saver.setDaemon(true);
                            if (!first) {
                                saver.start();
                            }
                        } else if (first) {
                            first = false;
                            saver.start();
                        }
                    }

                    return null;
                }
            });


            log.debug("SwingUtilities 5");
            codeareas.put(tab, rSyntaxTextAreaBuilder.getQueryWrapper());
            sessionsOpenedInterface.putIfAbsent(session, new ArrayList());
            sessionsOpenedInterface.get(session).add(new AbstractMap.SimpleEntry(tab, rSyntaxTextAreaBuilder.getQueryWrapper()));
            
            log.debug("SwingUtilities FINISH");
        });


        getController().addTab(tab, session);
        getController().showTab(tab, session);

        return tab;
    }
    
//    public Tab addOldQueryAreaTab(final ConnectionSession session, String scriptPath, String text, String tabName) {
//
//        final QueryAreaWrapper textArea = createCodeArea(session, text);
//        textArea.setRelativePath(scriptPath);
//                
//        Tab tab = new Tab(tabName);
//        tab.setGraphic(new ImageView(queryImage));
//        tab.setId(TAB_USER_DATA__QUERY_TAB);       
//        tab.setContent(new StackPane((CodeArea)((QueryAreaWrapper)textArea).getWrappedObject()));
//        ArrayList<Tab> tabContentHolderForBottomTabs = new ArrayList();
//        tab.setUserData(tabContentHolderForBottomTabs);
//                
//        parseImageToTabFromString(TAB_USER_DATA__QUERY_TAB, tab);
//        
//        TabContentController tabController = getController().getTabControllerBySession(session);
//        addDBClickListener(tabController, tab, textArea);        
//                
//        tab.setOnCloseRequest((Event event) -> {            
//                        
//            if (changedTabs.containsValue(tab)) {
//                DialogResponce responce = showConfirm("SmartSQL", "Do you want to save the changes?");
//                switch (responce) {
//                    case OK_YES:
//                        saveScript();
//                        break;
//                    case CANCEL:
//                        event.consume();
//                }
//            }
//            
//            if(!event.isConsumed()){
//                queriesTabCloseHandlerForOpenedSession(tab, session);
//            }
//            
//            Consumer c = dbClickListeners.remove(tab);
//            if (c != null && tabController != null) {
//                tabController.removeOnDoubleMouseClickedOnLeftTree(c);
//            }
//        });
//        
//        MessagesTabModule messageTabModule = (MessagesTabModule) getController().getModuleByClassName(MessagesTabModule.class.getName());
//        ObservableMap<Tab,String> tab_stringMap = (ObservableMap<Tab,String>) messageTabModule.getSessionMessageBottomTabTextFlow(session).getUserData();
//        tab_stringMap.put(tab, "");
//                 
//        ((CodeArea)((QueryAreaWrapper)textArea).getWrappedObject()).textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
//            if (!changedTabs.containsKey(textArea)) {
//                changedTabs.put(textArea, tab);
//            }
//        });        
//       
//        codeareas.put(tab, textArea);
//        sessionsOpenedInterface.putIfAbsent(session, new ArrayList());
//        sessionsOpenedInterface.get(session).add(new AbstractMap.SimpleEntry(tab, textArea));
//        getController().addTab(tab, session);
//        getController().showTab(tab, session);
//        
//        Platform.runLater(() -> ((CodeArea)((QueryAreaWrapper)textArea).getWrappedObject()).requestFocus());
//                
//        
//        return tab;
//    }
    public void addTemplatedQueryTab(final ConnectionSession session, final String confirmTitleText, final String confirmHeaderText, final java.util.function.Function<String, String> querySupplier, final String typeTab) {

        String result = DialogHelper.showInput(getController(), confirmTitleText, confirmHeaderText, "", "");
        if (result != null && !result.isEmpty()) {
            addNamedTabWithContext(session, UUID.randomUUID().toString(), result, querySupplier.apply(result), typeTab, null);
        }
    }

    public void addTemplatedQueryTab(final ConnectionSession session, final java.util.function.Function<Void, Pair<String, String>> querySupplier, final String typeTab) {

        Pair<String, String> result = querySupplier.apply(null);
        if (result != null) {
            addNamedTabWithContext(session, UUID.randomUUID().toString(), result.getKey(), result.getValue(), typeTab, null);
        }
    }

    public void addNamedTabWithContext(final ConnectionSession session, final String tabName, final String context, String typeTab, String relativePath) {
        addNamedTabWithContext(session, UUID.randomUUID().toString(), tabName, context, typeTab, relativePath);
    }
    
    public void addNamedTabWithContext(final ConnectionSession session, String uuid, final String tabName, final String context, String typeTab, String relativePath) {

        addQueryTab(session, uuid, relativePath, context, tabName, typeTab);

//        parseImageToTabFromString(typeTab, tab);

//        return codeareas.get(tab);
    }

    private void parseImageToTabFromString(String typeTab, Tab tab) {

        ImageView im = FunctionHelper.constructImageViewWithSize(new ImageView(), 18, 18);

        if (typeTab.startsWith("Proc")) {
            im.setImage(SmartImageProvider.getInstance().getStoredProcTab_image());
        } else if (typeTab.startsWith("Trig")) {
            im.setImage(SmartImageProvider.getInstance().getTriggerTab_image());
        } else if (typeTab.startsWith("Table")) {
            im.setImage(SmartImageProvider.getInstance().getTableTab_image());
        } else if (typeTab.startsWith("Index")) {
            //im.setImage(indexImage);
        } else if (typeTab.startsWith("View")) {
            im.setImage(SmartImageProvider.getInstance().getView_image());
        } else if (typeTab.startsWith("Query") || typeTab.startsWith("NamedQuery")) {
            im.setImage(SmartImageProvider.getInstance().getNewEditor_image());
        } else if (typeTab.startsWith("Func")) {
            im.setImage(SmartImageProvider.getInstance().getFunctionTab_image());
        } else if (typeTab.startsWith("Event")) {
            im.setImage(SmartImageProvider.getInstance().getEventTab_image());
        }

//        Platform.runLater(() -> {
            tab.setGraphic(im);
//        });
    }

    private void queriesTabCloseHandlerForOpenedSession(final Tab tab, final ConnectionSession session) {
        Recycler.addWorkProc(new WorkProc() {
            @Override
            public void updateUI() {
                Pane p = (Pane) tab.getContent();
                p.getChildren().clear();
                tab.setContent(null);
            }

            @Override
            public void run() {
                AbstractMap.SimpleEntry<Tab, QueryAreaWrapper> se = null;
                for (AbstractMap.SimpleEntry<Tab, QueryAreaWrapper> d : sessionsOpenedInterface.get(session)) {
                    if (d.getKey().equals(tab)) {
                        se = d;
                        break;
                    }
                }
                if (se != null) {
                    sessionsOpenedInterface.get(session).remove(se);
                    //se.getValue().setUserData(null);

                    changedTabs.remove(se.getValue());
                    codeareas.remove(se.getKey());
                    codeareasFindDada.remove(se.getValue());
                    dbClickListeners.remove(se.getKey());
//                    disabledHighlightAreas.remove(se.getValue());
//                    disabledValidatingAreas.remove(se.getValue());
                }
                callUpdate = true;
            }
        });

    }

    private void addDBClickListener(TabContentController tabController, Tab tab, QueryAreaWrapper textArea) {

        if (tabController != null) {

            Consumer c = (Consumer) (Object t) -> {
                
                PreferenceData pd = preferenceService.getPreference();
                if (pd.isPasteNameOnDoubleClick() && (t instanceof Database
                        || t instanceof DBTable
                        || t instanceof Column
                        || t instanceof View
                        || t instanceof StoredProc
                        || t instanceof Function
                        || t instanceof Trigger
                        || t instanceof np.com.ngopal.smart.sql.model.Event)) {

                    ConnectionSession session = getController().getSelectedConnectionSession().get();

                    if (tab == getController().getSelectedConnectionTab(session).get()) {

                        String text = ((DBElement) t).getName();
                        if (pd.isQueriesUsingBackquote()) {
                            text = "`" + text + "`";
                        }

                        textArea.insertText(textArea.getCaretPosition(CARET_POSITION_FROM_START), text);

                        if (t instanceof Database || t instanceof DBTable || t instanceof View) {
                            List<String> names = highlightGreenMap.get(session);
                            if (names == null) {
                                names = new ArrayList<>();
                                highlightGreenMap.put(session, names);
                            }

                            if (!names.contains(text.trim().toLowerCase())) {
                                names.add(text.trim().toLowerCase());
                            }

                        } else if (t instanceof StoredProc || t instanceof Function || t instanceof np.com.ngopal.smart.sql.model.Event || t instanceof Trigger) {
                            List<String> names = highlightMaroonMap.get(session);
                            if (names == null) {
                                names = new ArrayList<>();
                                highlightMaroonMap.put(session, names);
                            }

                            if (!names.contains(text.trim().toLowerCase())) {
                                names.add(text.trim().toLowerCase());
                            }
                        }
                    }
                }
            };

            tabController.addOnDoubleMouseClickedOnLeftTree(c);
            dbClickListeners.put(tab, c);
        }
    }

    public boolean canValidate(QueryAreaWrapper area) {
        for (Tab t : codeareas.keySet()) {
            if (codeareas.get(t).equals(area)) {
                return t.getId().equals(TAB_USER_DATA__QUERY_TAB) || t.getId().equals(TAB_USER_DATA__NAMED_QUERY_TAB);
            }
        }
        return false;
    }

    public void appendToTab(final Tab tab, final String text) {
        QueryAreaWrapper area = codeareas.get(tab);
        if (area != null) {
            if (area.getCaretPosition(CARET_POSITION_FROM_START) >= 0) {
                area.insertText(area.getCaretPosition(CARET_POSITION_FROM_START), text);
            } else {
                area.appendText(text);
            }
        }
    }

    public void insertToTab(final Tab tab, final String text) {
        QueryAreaWrapper area = codeareas.get(tab);
        if (area != null) {
            area.insertText(area.getCaretPosition(CARET_POSITION_FROM_START), text);
        }
    }

//    public QueryAreaWrapper createCodeArea(final ConnectionSession session, final String context) {
//        return CodeAreaWrapper.getCodeArea(session,context, initMenuAndMenuItems(), 
//            needValidateQuery, this, getMainController(), preferenceService, service,
//            new Callback<File,Void>() {
//            public Void call(File p) {  openFile(p); return null; } },
//            new Callback<String, Void>() {
//            public Void call(String p) { openQueryAnalyzer(p); return null; } },
//            
//            new Callback<List<Object>, Integer>() {
//                
//                private String prevDatabase;
//                public Integer call(List<Object> p) {
//                    if (prevDatabase == null) { prevDatabase = (String) p.get(1); }
//                    Integer i = tryDragWithJoin(session, prevDatabase, (String) p.get(1), (String) p.get(2),
//                            (QueryAreaWrapper) p.get(3), (Double) p.get(4), (Double) p.get(5));
//
//                    prevDatabase = (String) p.get(1);
//                    return i;
//                }
//            });
//    }
    private void updateErrorsList() {

        Map<QueryResult, String> errors = errorsForHighlight.get(getController().getSelectedConnectionSession().get());
        if (errors != null) {

            QueryAreaWrapper area = getSessionCodeArea();
            for (QueryResult qr : errors.keySet()) {

                String queryText = qr.getSimpleQuery().trim();
                String errorText = errors.get(qr);
                if (!errorText.trim().isEmpty()) {
                    String query = area.getText();

                    int index = 0;
                    int length = 0;
                    String[] rows = query.split("\n");
                    for (String s: rows) {
                        if (index >= qr.getErrorLine()) {
                            break;
                        }
                        length += s.length() + 1;
                        index++;
                    }

                    queryText = queryText.replaceAll("\n", " ").replaceAll("\r", "").replaceAll("\t", " ");                
                    errorText = errorText.replaceAll("\n", " ").replaceAll("\r", "").replaceAll("\t", " ");                
                    query = query.replaceAll("\r", "").replaceAll("\n", " ").replaceAll("\t", " ");

                    int start = query.indexOf(queryText);
                    if (start > -1) {

                        int errorIndex = queryText.indexOf(errorText);
                        if (errorIndex > -1) {

                            int i = queryText.indexOf(errorText, errorIndex + errorText.length());
                            if (i > -1) {
                                while (errorIndex < length) {
                                    i = queryText.indexOf(errorText, errorIndex + errorText.length());
                                    if (i > -1) {
                                        errorIndex = i;
                                    } else {
                                        break;
                                    }
                                }
                            }

                            start += errorIndex;
                            area.selectCustomRange(start, start + errorText.length(), INVALID_SELECTION_PAINT);
                        }
                    }
                }
            }
        }

    }
    
    private Integer tryDragWithJoin(ConnectionSession session, String prevDatabase, String dragedDatabase,
            String dragedTable, QueryAreaWrapper textAreaWrapper, Map<String, Object> queryData, double x, double y) {

        int position = textAreaWrapper.hitPosition(x, y);

//        Map<String, Pair<Integer, Integer>> queryPosition = findQueryPosition(textAreaWrapper, position);

        // first check if its SELECT query        
        String queryFromData = null;
        if (queryData != null) {
            
            queryFromData = (String)queryData.get("query");
            queryFromData = queryFromData.trim();
            if (!queryFromData.isEmpty() && queryFromData.toLowerCase().startsWith("select")) {
                
                QueryClause queryClause = QueryFormat.parseQuery(queryFromData);
                if (queryClause instanceof SelectCommand && ((SelectCommand) queryClause).getFromClause() != null) {
                    JoinMaker.JoinResult joinResult = JoinMaker.makeJoin(session, getMainController(),
                            (MysqlDBService) session.getService(), prevDatabase, DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(),
                                    dragedDatabase, dragedTable), queryFromData, true);

                    textAreaWrapper.replaceText((Integer)queryData.get("from"), (Integer)queryData.get("to"), formatCurrentQuery(preferenceService, joinResult.toString()));

                    return position;
                }
            }
        }
        
        
        int line = textAreaWrapper.getLineByPosition(position);
        
        String[] lines = textAreaWrapper.getText().split("\\n");
        if (lines.length > line) {
            try {
                String query = null;

                String lineStr = lines[line];
                if (!lineStr.trim().isEmpty()) {
                    if (SqlUtils.checkQuerySelect(lineStr)) {
                        query = session.getService().getSelectTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryInsert(lineStr)) {
                        query = session.getService().getInsertTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryUpdate(lineStr)) {
                        query = session.getService().getUpdateTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryDelete(lineStr)) {
                        query = session.getService().getDeleteTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryDrop(lineStr)) {
                        query = session.getService().getDropTableSQL(dragedDatabase, dragedTable);

                    } else if (SqlUtils.checkQueryTruncate(lineStr)) {
                        query = session.getService().getTruncateTableSQL(dragedDatabase, dragedTable);
                    }
                }

                if (query != null) {
                    int start = 0;
                    for (int i = 0; i < line; i++) {
                        start += lines[i].length() + 1;
                    }

                    int end = start + lineStr.length();

                    textAreaWrapper.replaceText(start, end, formatCurrentQuery(preferenceService, query));
                    
                    return start;
                }
                
                if (queryFromData != null && !queryFromData.isEmpty()) {
                    if (SqlUtils.checkQuerySelect(queryFromData)) {
                        query = session.getService().getSelectTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryInsert(queryFromData)) {
                        query = session.getService().getInsertTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryUpdate(queryFromData)) {
                        query = session.getService().getUpdateTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryDelete(queryFromData)) {
                        query = session.getService().getDeleteTableSQL(dragedDatabase, DatabaseCache.getInstance().getTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable));

                    } else if (SqlUtils.checkQueryDrop(queryFromData)) {
                        query = session.getService().getDropTableSQL(dragedDatabase, dragedTable);

                    } else if (SqlUtils.checkQueryTruncate(queryFromData)) {
                        query = session.getService().getTruncateTableSQL(dragedDatabase, dragedTable);
                    }

                    if (query != null) {
                        if (query.endsWith(";")) {
                            query = query.substring(0, query.length() - 1);
                        }
                        textAreaWrapper.replaceText((Integer)queryData.get("from") + 1, (Integer)queryData.get("to"), formatCurrentQuery(preferenceService, query));

                        return (Integer)queryData.get("from") + 1;
                    }
                }
            } catch (SQLException ex) {
                log.error("Error", ex);
            }
        }

        return null;
    }

    public static Map<String, Pair<String, String>> processJoins(MainController mainController, String currentQuery, ConnectionSession session, String dragedDatabase, String dragedTable, Consumer<String> aliasConsumer, BiConsumer<Boolean, String> joinConsumer, String joinCondition, BiFunction<DBTable, DBTable, List<FilterData>> showFilterConsumer) {

        if (joinCondition != null) {
            joinCondition = joinCondition.substring(joinCondition.indexOf("(") + 1, joinCondition.lastIndexOf(")"));
        }

        Map<String, Pair<String, String>> map = new HashMap<>();

        List<TableReferenceClause> tables = new ArrayList<>();
        List<TableReferenceClause> joinsTables = new ArrayList<>();

        String query = currentQuery.replaceAll("\\s+", " ").trim();
        QueryClause queryClause = QueryFormat.parseQuery(query);
        if (queryClause instanceof SelectCommand) {

            // check if from clause exist
            FromClause fromClause = ((SelectCommand) queryClause).getFromClause();
            if (fromClause != null) {
                List<TableReferenceClause> tableRefs = fromClause.getTables();
                if (tableRefs != null) {
                    for (TableReferenceClause t : tableRefs) {
                        tables.add(t);

                        // check joins
                        List<JoinClause> joins = t.getJoinClauses();
                        if (joins != null) {
                            for (JoinClause j : joins) {
                                joinsTables.add(j.getTable());
                            }
                        }
                    }
                }
            }

            // check joins
            List<JoinClause> joins = ((SelectCommand) queryClause).getJoinClauses();
            if (joins != null) {
                for (JoinClause j : joins) {
                    joinsTables.add(j.getTable());
                }
            }
        }

        joinsTables.addAll(tables);

        if (!tables.isEmpty()) {

            DBTable dragDbTable = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), dragedDatabase, dragedTable);

            // find if table aready exist - need add sufix
            String tableSufix = "";
            int index = 0;
            for (TableReferenceClause t : joinsTables) {

                String table = t.getName();
                if (table.contains(".")) {
                    String[] s = table.split("\\.");
                    table = s[1];
                }

                if (table.startsWith("`") && table.endsWith("`")) {
                    table = table.substring(1, table.length() - 1);
                }

                if (table.equalsIgnoreCase(dragedTable)) {

                    String indexStr = t.getAlias().replace(table, "");
                    if (indexStr.isEmpty()) {
                        index = 1;
                    } else if (indexStr.matches("\\d+")) {
                        index = Integer.valueOf(indexStr) + 1;
                    }
                }
            }

            if (index > 0) {
                tableSufix = "" + index;
            }

            {
                TableReferenceClause t = tables.get(0);

                String table = t.getName();
                String database = null;

                // check if table having fk
                if (table.contains(".")) {
                    String[] s = table.split("\\.");
                    database = s[0];
                    table = s[1];
                }

                if (table.startsWith("`") && table.endsWith("`")) {
                    table = table.substring(1, table.length() - 1);
                }

                if (database != null && database.startsWith("`") && database.endsWith("`")) {
                    database = database.substring(1, database.length() - 1);
                }

                DBTable dbTable = DatabaseCache.getInstance().getCopyOfTable(session.getConnectionParam().cacheKey(), database, table);

                String onQuery = "";
                if (dbTable != null) {
                    for (ForeignKey fk : dbTable.getForeignKeys()) {

                        // check database and table name
                        if (Objects.equals(fk.getReferenceDatabase().toLowerCase(), dragedDatabase.toLowerCase()) && Objects.equals(fk.getReferenceTable().toLowerCase(), dragedTable.toLowerCase())) {
                            for (int i = 0; i < fk.getColumns().size(); i++) {

                                if (!onQuery.isEmpty()) {
                                    onQuery += " AND ";
                                }

                                onQuery += dbTable.getName() + "." + fk.getColumns().get(i).getName() + " = " + dragedTable + tableSufix + "." + fk.getReferenceColumns().get(i).getName();
                            }
                        }
                    }

                    // if onquery empty need check for draged table
                    if (onQuery.isEmpty()) {
                        if (dragDbTable != null) {
                            for (ForeignKey fk : dragDbTable.getForeignKeys()) {

                                // check database and table name
                                if ((database == null || Objects.equals(fk.getReferenceDatabase().toLowerCase(), dragedDatabase.toLowerCase())) && Objects.equals(fk.getReferenceTable().toLowerCase(), table.toLowerCase())) {
                                    for (int i = 0; i < fk.getColumns().size(); i++) {

                                        if (!onQuery.isEmpty()) {
                                            onQuery += " AND ";
                                        }

                                        onQuery += dragDbTable + tableSufix + "." + fk.getColumns().get(i).getName() + " = " + table + "." + fk.getReferenceColumns().get(i).getName();
                                    }
                                }
                            }
                        }
                    }

                    if (onQuery.isEmpty()) {

                        if (dragDbTable != null) {

                            List<Column> primarys = new ArrayList<>();

                            // check first only NUMBER types
                            for (Column c : dragDbTable.getColumns()) {
                                if (c.isPrimaryKey()) {
                                    primarys.add(c);
                                }

                                if (AbstractDBService.isNumber(c.getDataType().getName())) {
                                    String name = c.getName().replaceAll("_", "");
                                    // try found the same colum names
                                    for (Column dc : dbTable.getColumns()) {

                                        if (AbstractDBService.isNumber(dc.getDataType().getName())) {
                                            if (name.equalsIgnoreCase(dc.getName().replaceAll("_", ""))) {
                                                if (!onQuery.isEmpty()) {
                                                    onQuery += " AND ";
                                                }

                                                onQuery += dragDbTable + tableSufix + "." + c.getName() + " = " + table + "." + dc.getName();
                                            }
                                        }
                                    }
                                }
                            }
                            if (onQuery.isEmpty()) {
                                if (joinConsumer != null) {
                                    if (joinCondition == null) {
                                        List<FilterData> filteredData = showFilterConsumer != null ? showFilterConsumer.apply(dbTable, dragDbTable) : null;
                                        if (filteredData != null && !filteredData.isEmpty()) {

                                            for (FilterData fd : filteredData) {
                                                if (!onQuery.isEmpty()) {
                                                    onQuery += " AND ";
                                                }

                                                // second table
                                                String firstColumn = fd.getValue();
                                                for (Column c : dragDbTable.getColumns()) {
                                                    if (c.getName().equalsIgnoreCase(firstColumn)) {
                                                        firstColumn = dragDbTable + tableSufix + "." + firstColumn;
                                                        break;
                                                    }
                                                }

                                                // first table
                                                String secondColumn = fd.getColumn();
                                                for (Column c : dbTable.getColumns()) {
                                                    if (c.getName().equalsIgnoreCase(secondColumn)) {
                                                        secondColumn = dbTable + "." + secondColumn;
                                                        break;
                                                    }
                                                }

                                                onQuery += firstColumn + " " + fd.getCondition() + " " + secondColumn;
                                            }
                                        } else {
                                            joinConsumer.accept(false, null);
                                        }
                                    } else if (!joinCondition.isEmpty()) {
                                        String[] conditions = joinCondition.split(" AND ");
                                        joinCondition = "";

                                        for (String condition : conditions) {

                                            if (!joinCondition.isEmpty()) {
                                                joinCondition += " AND ";
                                            }

                                            if (condition.matches(dragedTable + "\\d*\\..*?(=|>|<|LIKE).*")) {
                                                joinCondition += condition.replaceFirst(dragedTable + "\\d*", dragDbTable + tableSufix);
                                            } else {
                                                joinCondition += condition;
                                            }

//                                            joinCondition += dragDbTable + tableSufix + condition.substring(condition.indexOf("."));
                                        }
                                    }
                                } else // add custom join condition
                                if (onQuery.isEmpty() && !primarys.isEmpty()) {
                                    for (Column dc : primarys) {
                                        if (!onQuery.isEmpty()) {
                                            onQuery += " AND ";
                                        }

                                        onQuery += dragDbTable + tableSufix + "." + dc.getName() + " = _";
                                    }
                                }
                            }
                        }
                    }
                }

                if (!onQuery.isEmpty() || joinCondition != null) {

                    String fullQuery = " JOIN " + dragedDatabase + "." + dragedTable + " " + dragedTable + tableSufix
                            + " ON ("
                            + (joinCondition == null ? onQuery : joinCondition)
                            + ")";
                    map.put(t.toString(), new Pair(tableSufix, fullQuery));

                    if (joinConsumer != null) {
                        joinConsumer.accept(true, fullQuery);
                    }
                } else {
                    String fullQuery = ", " + dragedDatabase + "." + dragedTable + " " + dragedTable + tableSufix;
                    map.put(t.toString(), new Pair(tableSufix, fullQuery));
                }

                if (aliasConsumer != null) {
                    aliasConsumer.accept(dragedTable + tableSufix);
                }
            }
        }

        return map;
    }

    public QueryAreaWrapper getSessionCodeArea() {
        final ReadOnlyObjectProperty<Tab> tabProperty
                = (ReadOnlyObjectProperty<Tab>) getController().getSelectedConnectionTab(getController().
                        getSelectedConnectionSession().get());
        return codeareas.get(tabProperty.get());
    }

    public FindObject getSessionFindObject() {
        return getSessionCodeArea();
    }
    
    public Collection<QueryAreaWrapper> getAllCodeAreas() {
        return codeareas.values();
    }

    public boolean isAlterTab() {
        final ReadOnlyObjectProperty<Tab> tabProperty
                = (ReadOnlyObjectProperty<Tab>) getController().getSelectedConnectionTab(getController().
                        getSelectedConnectionSession().get());
        return tabProperty.get() != null && tabProperty.get().getId() != null && !tabProperty.get().getId().equals(TAB_USER_DATA__QUERY_TAB);
    }

    public MenuItem[] initMenuAndMenuItems() {

        MenuItem refreshObjectBrowserMenuItem = new MenuItem("Refresh Object Browser",
                new ImageView(StandardDefaultImageProvider.getInstance().getRefresh_image()));
        refreshObjectBrowserMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F5));

        refreshObjectBrowserMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            WorkProc refreshingWorkProc;

            @Override
            public void handle(ActionEvent t) {
                if (refreshingWorkProc == null) {
                    refreshingWorkProc = new WorkProc() {
                        public void updateUI() {
                        }

                        public void run() {
                            getController().refreshTree(getController().getSelectedConnectionSession().get(), true);
                        }
                    };
                }
                Recycler.addWorkProc(refreshingWorkProc);
            }
        });

        MenuItem changeObjectBrowserColorMenuItem = new MenuItem("Change Object Browser Color",
                new ImageView(SmartImageProvider.getInstance().getColor_image()));
        changeObjectBrowserColorMenuItem.setOnAction((ActionEvent event) -> {
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            
            ColorDialogController ccController = ((MainController)getController()).getTypedBaseController("ColorDialog");
            ccController.create();
            ConnectionParams cp = session.getConnectionParam();
            if (cp.getForegroundColor() != null && cp.getBackgroundColor() != null) {
                ccController.setPaintDefaults(Color.web(cp.getForegroundColor()),
                        Color.web(cp.getBackgroundColor()));
            }
            ccController.show_hide();
            Object[] res = ccController.getResults("tab" + String.valueOf(getController().getSelectedConnectionSession().get().getId()));
            if (res != null) {
                TabContentController tabController = getController().getTabControllerBySession(getController().getSelectedConnectionSession().get());
                if (tabController != null) {
                    ((SimpleObjectProperty)tabController.getTreeViewUserData()).setValue(res);
                }
            }
        });

        Menu executeQueryMenu = new Menu("Execute",
                new ImageView(SmartImageProvider.getInstance().getExecuteQueryMenu_image()));

        MenuItem executeQueryLineMenuItem = new MenuItem("Execute Query",
                new ImageView(SmartImageProvider.getInstance().getExecute_image()));
        executeQueryLineMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F9));
        executeQueryLineMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            // last runned script runner
            ScriptRunner scriptRunner;

            @Override
            public void handle(ActionEvent event) {
                if (executeButton.get()) {
                    if (scriptRunner != null) {
                        scriptRunner.forceStop();
                        scriptRunner = null;
                    }
                } else {
                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    QueryAreaWrapper textArea = getSessionCodeArea();
                    if (textArea != null) {

                        executeButton.set(true);
                        executeButtonMap.put(session, true);

                        final ReadOnlyObjectProperty<Tab> tabProperty
                                = (ReadOnlyObjectProperty<Tab>) getController().getSelectedConnectionTab(getController().
                                        getSelectedConnectionSession().get());

                        Thread th = new Thread(() -> {
                            // if we found seletion - need execute it
                            String selectedText = textArea.getSelectedText();
                            if (selectedText != null && !selectedText.isEmpty()) {
                                scriptRunner = executeLineActionPerformed(session, textArea, selectedText, -1, false);
                            } else if (tabProperty.get() != null && TAB_USER_DATA__QUERY_TAB.equals(tabProperty.get().getId())) {
                                scriptRunner = executeLineActionPerformed(session, textArea, textArea.getText(), textArea.getCaretPosition(CARET_POSITION_FROM_START), false);
                            } else {
                                scriptRunner = executeLineActionPerformed(session, textArea, textArea.getText(), -1, false);
                            }
                        });
                        th.setDaemon(true);
                        th.start();
                    }
                }
            }
        });

        MenuItem executeAllMenuItem = new MenuItem("Execute All Queries", new ImageView(SmartImageProvider.getInstance().getExecuteAll_image()));
        executeAllMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F9, KeyCombination.SHORTCUT_DOWN));
        executeAllMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            // last runned script runner
            ScriptRunner scriptRunner;

            @Override
            public void handle(ActionEvent event) {
                if (executeAllButton.get()) {
                    if (scriptRunner != null) {
                        scriptRunner.forceStop();
                        scriptRunner = null;
                    }
                } else {
                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    QueryAreaWrapper textArea = getSessionCodeArea();
                    if (textArea != null) {

                        executeAllButton.set(true);
                        executeAllButtonMap.put(session, true);
                        
                        String text = textArea.getSelectedText();
                        if (text == null || text.trim().isEmpty()) {
                            text = textArea.getText();
                        }
                        
                        scriptRunner = executeLineActionPerformed(session, textArea, text, -1, false);
                    }
                }
            }
        });

        MenuItem executeAndEditResultSetMenuItem = new MenuItem("Execute And Edit ResultSet",
                new ImageView(SmartImageProvider.getInstance().getExecuteEdit_image()));
        executeAndEditResultSetMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F8, KeyCombination.SHORTCUT_DOWN));
        executeAndEditResultSetMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            // last runned script runner
            ScriptRunner scriptRunner;

            @Override
            public void handle(ActionEvent event) {
                if (executeWithEditButton.get()) {
                    if (scriptRunner != null) {
                        scriptRunner.forceStop();
                        scriptRunner = null;
                    }
                } else {

                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    QueryAreaWrapper textArea = getSessionCodeArea();
                    if (textArea != null) {
                        executeWithEditButton.set(true);
                        executeWithEditButtonMap.put(session, true);

                        if (textArea.getSelectedText() != null && !textArea.getSelectedText().isEmpty()) {
                            scriptRunner = executeLineActionPerformed(session, textArea, textArea.getSelectedText(), -1, true);
                        } else {
                            scriptRunner = executeLineActionPerformed(session, textArea, textArea.getText(), textArea.getCaretPosition(CARET_POSITION_FROM_START), true);
                        }
                    }
                }
            }
        });

        MenuItem executeLockQueryMenuItem = new MenuItem("Lock Free Execution",
                new ImageView(SmartImageProvider.getInstance().getExecuteLock_image()));
        executeLockQueryMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            // last runned script runner
            ScriptRunner scriptRunner;

            @Override
            public void handle(ActionEvent event) {
                if (executeLockButton.get()) {
                    if (scriptRunner != null) {
                        scriptRunner.forceStop();
                        scriptRunner = null;
                    }
                } else {
                    
                    ((MainController)getController()).usedLockExecution();
                    
                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    QueryAreaWrapper textArea = getSessionCodeArea();
                    if (textArea != null) {

                        // if we found seletion - need execute it
                        if (textArea.getSelectedText() != null && !textArea.getSelectedText().isEmpty()) {

                            executeLockButton.set(true);
                            executeLockButtonMap.put(session, true);

                            scriptRunner = executeLineActionPerformed(session, textArea, textArea.getSelectedText(), -1, true);
                        } else {
                            DialogHelper.showError(getController(),"Info", "Please select first query", null);
                        }
                    }
                }
            }
        });
        
        MenuItem executeBulkQueryMenuItem = new MenuItem("Bulk execution. Don’t give output. Useful for bulk inserts.",
                new ImageView(SmartImageProvider.getInstance().getExecuteBulk_image()));
        executeBulkQueryMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            // last runned script runner
            ScriptRunner scriptRunner;

            @Override
            public void handle(ActionEvent event) {
                if (executeBulkButton.get()) {
                    if (scriptRunner != null) {
                        scriptRunner.forceStop();
                        scriptRunner = null;
                    }
                } else {
                     
                    DialogResponce dr = showConfirm("Bulk execution", 
                            "The bulk execution option executes all queries at one"
                            + "shot without any network delay with good performance"
                            + "and it don’t give output of the query. It is useful"
                            + "for bulk inserts. Are you sure you want to run the"
                            + "bulk execute all queries from the query tab.");
                    if (dr == DialogResponce.OK_YES) {
                        ConnectionSession session = getController().getSelectedConnectionSession().get();
                        QueryAreaWrapper textArea = getSessionCodeArea();
                        if (textArea != null) {

                            executeBulkButton.set(true);
                            executeBulkButtonMap.put(session, true);

                            scriptRunner = bulkExecute(session, textArea);
                        }
                    }
                }
            }
        });

        executeQueryMenu.getItems().addAll(executeQueryLineMenuItem, executeAllMenuItem, executeAndEditResultSetMenuItem, executeLockQueryMenuItem, executeBulkQueryMenuItem);

        Menu executeExplainMenu = new Menu("Explain",
                new ImageView(SmartImageProvider.getInstance().getExplain_image()));
        MenuItem explainQueryMenuItem = new MenuItem("EXPLAIN <Query>",
                new ImageView(SmartImageProvider.getInstance().getExplainQuery_image()));
        explainQueryMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                ConnectionSession session = getController().getSelectedConnectionSession().get();
                explainQuery(session, "EXPLAIN", textArea);
            }
        });

        MenuItem explainExtendedQueryMenuItem = new MenuItem("EXPLAIN EXTENDED <Query>",
                new ImageView(SmartImageProvider.getInstance().getExplainQuery_image()));
        explainExtendedQueryMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                ConnectionSession session = getController().getSelectedConnectionSession().get();
                explainQuery(session, "EXPLAIN EXTENDED", textArea);
            }
        });

        MenuItem explainVisualMenuItem = new MenuItem("Visual explain",
                new ImageView(SmartImageProvider.getInstance().getVisualExplainTab_image()));
        explainVisualMenuItem.setOnAction((ActionEvent e) -> {

            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {

                String query = textArea.getSelectedText();
                if (query == null || query.isEmpty()) {
                    try {
                        PreferenceData pd = preferenceService.getPreference();
                        ScriptRunner scriptRunner = new ScriptRunner(getController().getSelectedConnectionSession().get().getService().getDB(), pd.isHaltExecutionOnError());
                        Pair<Integer, String> res = scriptRunner.findQueryAtPosition(textArea.getText(), textArea.getCaretPosition(CARET_POSITION_FROM_START), false);
                        query = res != null ? res.getValue() : null;
                    } catch (Throwable th) {
                    }
                }

                if (query != null && !query.isEmpty()) {
                    ConnectionSession session = getController().getSelectedConnectionSession().get();
                    showVisualExplain(session.getConnectionParam().getSelectedDatabase(), query, getController(), 
                        ((MainController)getController()).getColumnCardinalityService(), 
                        ((MainController)getController()).getPreferenceService(),
                        ((MainController)getController()).getPerformanceTuningService(),
                        ((MainController)getController()).getRecommendationService(),
                        ((MainController)getController()).getUsageService(),
                        null);
//                    if (t != null) {
//                        List<Tab> tabs = visualExplanTabs.get(session);
//                        if (tabs == null) {
//                            tabs = new ArrayList<>();
//                            visualExplanTabs.put(session, tabs);
//                        }
//                        
//                        tabs.add(t);
//                    }
                } else {
                    DialogHelper.showError(getController(),"Info", "Please select query for visual explain", null);
                }
            }
        });

        executeExplainMenu.getItems().addAll(explainQueryMenuItem, explainExtendedQueryMenuItem, explainVisualMenuItem);
//        executeExplainMenu.getItems().addAll(explainQueryMenuItem, explainExtendedQueryMenuItem);

        Menu sqlFormatterMenu = new Menu("Formatter",
                new ImageView(SmartImageProvider.getInstance().getSqlFormatterMenu_image()));

        MenuItem formatCurrentQueryMenuItem = new MenuItem("Format Current Query",
                new ImageView(SmartImageProvider.getInstance().getFormatCurrent_image()));
        formatCurrentQueryMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F12));
        formatCurrentQueryMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {

                int index = textArea.getCaretPosition(CARET_POSITION_FROM_START);
                if (index >= 0) {
                    formatCurrentQuery(((MainController)getController()), preferenceService, getController().getSelectedConnectionSession().get(), textArea, index);
                }
            }
        });

        MenuItem formatSelectedQueryMenuItem = new MenuItem("Format Selected Query",
                new ImageView(SmartImageProvider.getInstance().getFormatSelected_image()));
        formatSelectedQueryMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F12, KeyCombination.SHORTCUT_DOWN));
        formatSelectedQueryMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {

                String selectedText = textArea.getSelectedText();
                if (selectedText != null && !selectedText.isEmpty()) {

                    String formatedText;
                    SqlFormatter formatter = new SqlFormatter();

                    List<PreferenceData> list = preferenceService.getAll();
                    if (!list.isEmpty()) {
                        formatedText = formatter.formatSql(selectedText, list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle());
                    } else {
                        formatedText = formatter.formatSql(selectedText, "", "2", "block");
                    }
                    
                    ((MainController)getController()).usedQueryFormat();

                    textArea.replaceText(textArea.getSelectionStart(), textArea.getSelectionEnd(), formatedText);
                    textArea.positionCaret(0);
                }
            }
        });

        MenuItem formatAllQueriesMenuItem = new MenuItem("Format All Queries",
                new ImageView(SmartImageProvider.getInstance().getFormatAll_image()));
        formatAllQueriesMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F12, KeyCombination.SHIFT_DOWN));
        formatAllQueriesMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {

                String text = textArea.getText();
                if (text != null && !text.isEmpty()) {

                    String formatedText;
                    SqlFormatter formatter = new SqlFormatter();

                    List<PreferenceData> list = preferenceService.getAll();
                    if (!list.isEmpty()) {
                        formatedText = formatter.formatSql(text, list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle());
                    } else {
                        formatedText = formatter.formatSql(text, "", "2", "block");
                    }
                    
                    ((MainController)getController()).usedQueryFormat();

                    textArea.clear();
                    textArea.appendText(formatedText);
                    textArea.positionCaret(0);
                }
            }
        });

        sqlFormatterMenu.getItems().addAll(formatCurrentQueryMenuItem, formatSelectedQueryMenuItem,
                formatAllQueriesMenuItem);

        MenuItem insertTemplatesMenuItem = new MenuItem("Insert Templates...",
                new ImageView(SmartImageProvider.getInstance().getInsertTemplate_image()));
        insertTemplatesMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        insertTemplatesMenuItem.setOnAction((ActionEvent event) -> {
            
            Stage dialogStage = new Stage(StageStyle.UTILITY);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(getController().getStage());
            dialogStage.setTitle("      SQL Template");
            
            InsertTemplateController insertTemplateController = ((MainController)getController()).getTypedBaseController("InsertSqlTemplates");
            insertTemplateController.insertQueryCodeArea(getSessionCodeArea());
            
            dialogStage.setScene(new Scene(insertTemplateController.getUI()));
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
        });

        MenuItem undoMenuItem = new MenuItem("Undo",
                new ImageView(SmartImageProvider.getInstance().getUndo_image()));
        undoMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.SHORTCUT_DOWN));
        undoMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.undo();
            }
        });

        MenuItem redoMenuItem = new MenuItem("Redo",
                new ImageView(SmartImageProvider.getInstance().getRedo_image()));
        redoMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.SHORTCUT_DOWN));
        redoMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.redo();
            }
        });

        MenuItem cutMenuItem = new MenuItem("Cut",
                new ImageView(SmartImageProvider.getInstance().getCut_image()));
        cutMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.SHORTCUT_DOWN));
        cutMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.cut();
            }
        });

        MenuItem copyMenuItem = new MenuItem("Copy",
                new ImageView(SmartImageProvider.getInstance().getCopy_image()));
        copyMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN));
        copyMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.copy();
            }
        });

        MenuItem copyWithNormalizedWhitespaceMenuItem = new MenuItem("Copy With Normalized Whitespace",
                new ImageView(SmartImageProvider.getInstance().getCopy_image()));
        copyWithNormalizedWhitespaceMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN));
        copyWithNormalizedWhitespaceMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                String selectedText = textArea.getSelectedText().replaceAll("\\s+", " ").trim();
                if (selectedText.length() > 0) {
                    ClipboardContent content = new ClipboardContent();
                    content.putString(selectedText);
                    Clipboard.getSystemClipboard().setContent(content);
                }
            }
        });

        MenuItem pasteMenuItem = new MenuItem("Paste",
                new ImageView(SmartImageProvider.getInstance().getPaste_image()));
        pasteMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN));
        pasteMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.paste();
            }
        });

        MenuItem insertFromFileMenuItem = new MenuItem("Insert From File",
                new ImageView(SmartImageProvider.getInstance().getInsertFromFile_image()));
        insertFromFileMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                String fileText = readFromUserFile();
                if (fileText != null) {
                    textArea.insertText(textArea.getCaretPosition(CARET_POSITION_FROM_START), fileText);
                }
            }
        });
        MenuItem selectAllMenuItem = new MenuItem("Select All",
                new ImageView(SmartImageProvider.getInstance().getSelectAll_image()));
        selectAllMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.SHORTCUT_DOWN));
        selectAllMenuItem.setOnAction((ActionEvent e) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.selectAll();
            }
        });

        MenuItem findMenuItem = new MenuItem("Find...",
                new ImageView(SmartImageProvider.getInstance().getFind_image()));
        findMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN));
        findMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                showFindDialog(textArea);
            }
        });

        MenuItem findNextMenuItem = new MenuItem("Find Next",
                new ImageView(SmartImageProvider.getInstance().getFind_image()));
        findNextMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.F3));
        findNextMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                FindCodeAreaData findData = codeareasFindDada.get(textArea);
                if (findData != null) {
                    FindCodeAreaUtil.findNext(textArea, findData);
                }
            }
        });

        MenuItem replaceMenuItem = new MenuItem("Replace...",
                new ImageView(SmartImageProvider.getInstance().getReplace_image()));
        replaceMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.H, KeyCombination.SHORTCUT_DOWN));
        replaceMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                showReplaceDialog(textArea);
            }
        });

        MenuItem gotoMenuItem = new MenuItem("Go To...", new ImageView(SmartImageProvider.getInstance().getGoto_image()));
        gotoMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.G, KeyCombination.SHORTCUT_DOWN));
        gotoMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                showGoToLineDialog(textArea);
            }
        });

        MenuItem listAllTagsMenuItem = new MenuItem("List All Tags", new ImageView(SmartImageProvider.getInstance().getListAllTags_image()));
        listAllTagsMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.SPACE, KeyCombination.SHORTCUT_DOWN));
        listAllTagsMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.listAllTags();
            }
        });

        MenuItem listMatchingTagsMenuItem = new MenuItem("List Matching Tags",
                new ImageView(SmartImageProvider.getInstance().getListMatchingTags_image()));
        listMatchingTagsMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.SHORTCUT_DOWN));
        listMatchingTagsMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                textArea.listMatchingTags();
            }
        });

        MenuItem listFunctionMenuItem = new MenuItem("List Function and Routine Parameters",
                new ImageView(SmartImageProvider.getInstance().getListFunctionTags_image()));
        listFunctionMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.SPACE, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        listFunctionMenuItem.setOnAction((ActionEvent event) -> {
            DialogHelper.showError(getController(),"Not implemented", "Not implemented", null);
        });

        MenuItem hideObjectBrowserMenuItem = new CheckMenuItem("Hide Object Browser",
                new ImageView(SmartImageProvider.getInstance().getHide_image()));
        hideObjectBrowserMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        hideObjectBrowserMenuItem.setOnAction((ActionEvent event) -> {            
            getController().toggleLeftPane();
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN), (Object obj) -> {
            getController().toggleLeftPane();
            ((KeyEvent)obj).consume();
        });

        CheckMenuItem hideResultPaneMenuItem = new CheckMenuItem("Hide Result Panel",
                new ImageView(SmartImageProvider.getInstance().getHide_image()));
        hideResultPaneMenuItem.setSelected(false);
        hideResultPaneMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DIGIT2, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        hideResultPaneMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            ConnectionSession cs;

            {
                cs = getController().getSelectedConnectionSession().get();
                if (cs != null) {
                    hideResultTabCollection.putIfAbsent(cs, hideResultPaneMenuItem);
                }
            }

            @Override
            public void handle(ActionEvent event) {
                if (cs == null) {
                    cs = getController().getSelectedConnectionSession().get();
                }

                if (hideResultPaneMenuItem.isSelected()) {
                    hideResultPaneMenuItem.setSelected(false);
                    getController().hideResultPane(cs, true);
                } else {
                    hideResultPaneMenuItem.setSelected(true);
                    getController().hideResultPane(cs, false);
                }

//                if (hideResultPaneMenuItem != hideResultPane) {
//                    ((CheckMenuItem) hideResultPane).setSelected(hideResultPaneMenuItem.isSelected());
//                } else {
                    hideResultTabCollection.get(cs).setSelected(hideResultPaneMenuItem.isSelected());
//                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.F4), (Object obj) -> {
            TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().get()).get();
            if (item != null) {
                Object value = item.getValue();
                if (value instanceof ConnectionTabContentController.DatabaseChildrenType) {
                    ConnectionTabContentController.DatabaseChildrenType typeValue = (ConnectionTabContentController.DatabaseChildrenType)value;
                    switch (typeValue) {
                        case TABLES:
                            ((MenuInTablesModule)getMainController().getModuleByClassName(MenuInTablesModule.class.getName())).createTable();
                            ((KeyEvent)obj).consume();
                            break;
                            
                        case VIEWS:
                            ((MenuInViewModule)getMainController().getModuleByClassName(MenuInViewModule.class.getName())).createView();
                            ((KeyEvent)obj).consume();
                            break;
                            
                        case FUNCTIONS:
                            ((MenuInFunctionModule)getMainController().getModuleByClassName(MenuInFunctionModule.class.getName())).createFunction();
                            ((KeyEvent)obj).consume();
                            break;
                            
                        case STORED_PROCS:
                            ((MenuInStoredProcedureModule)getMainController().getModuleByClassName(MenuInStoredProcedureModule.class.getName())).createStoredProcedure();
                            ((KeyEvent)obj).consume();
                            break;
                            
                        case TRIGGERS:
                            ((MenuInTriggerModule)getMainController().getModuleByClassName(MenuInTriggerModule.class.getName())).createTrigger();
                            ((KeyEvent)obj).consume();
                            break;
                            
                        case EVENTS:
                            ((MenuInEventsModule)getMainController().getModuleByClassName(MenuInEventsModule.class.getName())).createEvent();
                            ((KeyEvent)obj).consume();
                            break;
                    }
                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.F6), (Object obj) -> {
            TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().get()).get();
            if (item != null) {
                Object value = item.getValue();
                if (value instanceof DBTable) {
                    ((MenuInTableItemModule)getMainController().getModuleByClassName(MenuInTableItemModule.class.getName())).alterTable();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof View) {
                    ((MenuInViewModule)getMainController().getModuleByClassName(MenuInViewModule.class.getName())).alterView();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof Function) {
                    ((MenuInFunctionModule)getMainController().getModuleByClassName(MenuInFunctionModule.class.getName())).alterFunction();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof StoredProc) {
                    ((MenuInStoredProcedureModule)getMainController().getModuleByClassName(MenuInStoredProcedureModule.class.getName())).alterStoredProcedure();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof Trigger) {
                    ((MenuInTriggerModule)getMainController().getModuleByClassName(MenuInTriggerModule.class.getName())).alterTrigger();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof Event) {
                    ((MenuInEventsModule)getMainController().getModuleByClassName(MenuInEventsModule.class.getName())).alterEvent();
                    ((KeyEvent)obj).consume();
                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.F2), (Object obj) -> {
            TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().get()).get();
            if (item != null) {
                Object value = item.getValue();
                if (value instanceof DBTable) {
                    ((MenuInTableItemModule)getMainController().getModuleByClassName(MenuInTableItemModule.class.getName())).renameTable();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof View) {
                    ((MenuInViewModule)getMainController().getModuleByClassName(MenuInViewModule.class.getName())).renameView();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof Function) {
//                    ((MenuInFunctionModule)getMainController().getModuleByClassName(MenuInFunctionModule.class.getName())).renameFunction();
                    ((KeyEvent)obj).consume();
                
                } else if (value instanceof StoredProc) {
//                    ((MenuInStoredProcedureModule)getMainController().getModuleByClassName(MenuInStoredProcedureModule.class.getName())).renameStoredProcedure();
                     ((KeyEvent)obj).consume();
                            
                } else if (value instanceof Trigger) {
                    ((MenuInTriggerModule)getMainController().getModuleByClassName(MenuInTriggerModule.class.getName())).renameTrigger();
                    ((KeyEvent)obj).consume();
                
                } else if (value instanceof Event) {
                    ((MenuInEventsModule)getMainController().getModuleByClassName(MenuInEventsModule.class.getName())).renameEvent();
                    ((KeyEvent)obj).consume();
                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.SHIFT_DOWN), (Object obj) -> {
            TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().get()).get();
            if (item != null) {
                Object value = item.getValue();
                if (value instanceof Database) {
                    ((MenuInDatabaseItemModule)getMainController().getModuleByClassName(MenuInDatabaseItemModule.class.getName())).truncateDatabase();
                    ((KeyEvent)obj).consume();
                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DELETE), (Object obj) -> {
            if (getController().getStage().getScene().focusOwnerProperty().get() instanceof TreeView) {
                TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().get()).get();
                if (item != null) {
                    Object value = item.getValue();
                    if (value instanceof Database) {
                        ((MenuInDatabaseItemModule)getMainController().getModuleByClassName(MenuInDatabaseItemModule.class.getName())).dropDatabase();
                        ((KeyEvent)obj).consume();
                    }
                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.F7), (Object obj) -> {
            TreeItem item = getController().getSelectedTreeItem(getController().getSelectedConnectionSession().get()).get();
            if (item != null) {
                Object value = item.getValue();
                if (value instanceof DBTable) {
                    ((MenuInTableItemModule)getMainController().getModuleByClassName(MenuInTableItemModule.class.getName())).manageIndexes();
                    ((KeyEvent)obj).consume();
                    
                } else if (value instanceof Index) {
                    ((MenuInIndexModule)getMainController().getModuleByClassName(MenuInIndexModule.class.getName())).manageIndexes();
                    ((KeyEvent)obj).consume();
                }
            }
        });
        
//        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT1, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN), (Object obj) -> {
//            getController().toggleLeftPane();
//        });

        CheckMenuItem hideSQLEditorMenuItem = new CheckMenuItem("Hide Top Section",
                new ImageView(SmartImageProvider.getInstance().getHide1_image()));
        hideSQLEditorMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        hideSQLEditorMenuItem.setOnAction(new EventHandler<ActionEvent>() {

            ConnectionSession cs;

            {
                cs = getController().getSelectedConnectionSession().get();
                if (cs != null) {
                    hideSQLEditorCollection.putIfAbsent(cs, hideSQLEditorMenuItem);
                }
            }

            @Override
            public void handle(ActionEvent event) {
                if (cs == null) {
                    cs = getController().getSelectedConnectionSession().get();
                }

                if (hideSQLEditorMenuItem.isSelected()) {
                    hideSQLEditorMenuItem.setSelected(false);
                    getController().hideContentPane(cs, true);
                } else {
                    hideSQLEditorMenuItem.setSelected(true);
                    getController().hideContentPane(cs, false);
                }

//                if (hideSQLEditorMenuItem != hideSqlEditor) {
//                    ((CheckMenuItem) hideSqlEditor).setSelected(hideSQLEditorMenuItem.isSelected());
//                } else {
                    hideSQLEditorCollection.get(cs).setSelected(hideSQLEditorMenuItem.isSelected());
//                }
            }
        });
        
        getController().addCustomKeyCombinations(new KeyCodeCombination(KeyCode.DIGIT3, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN), (Object obj) -> {
            hideSQLEditorMenuItem.fire();
            ((KeyEvent)obj).consume();
        });

        MenuItem switchToPreviousTabMenuItem = new MenuItem("Switch To Previous Tab",
                new ImageView(SmartImageProvider.getInstance().getSwitchPrev_image()));
        switchToPreviousTabMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.PAGE_UP, KeyCombination.SHORTCUT_DOWN));
        switchToPreviousTabMenuItem.setOnAction((ActionEvent event) -> {
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            ((ConnectionTabContentController) parentTabs.get(session).getUserData()).prevTab();
        });

        MenuItem switchToNextTabMenuItem = new MenuItem("Switch To Next Tab",
                new ImageView(SmartImageProvider.getInstance().getSwitchNext_image()));
        switchToNextTabMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.PAGE_DOWN, KeyCombination.SHORTCUT_DOWN));
        switchToNextTabMenuItem.setOnAction((ActionEvent event) -> {
            ConnectionSession session = getController().getSelectedConnectionSession().get();
            ((ConnectionTabContentController) parentTabs.get(session).getUserData()).nextTab();
        });

        Menu advancedMenu = new Menu("Advanced", new ImageView(SmartImageProvider.getInstance().getAdvanced_image()));
        MenuItem makeASelectionUppercaseMenuItem = new MenuItem("Make a Selection Uppercase",
                new ImageView(SmartImageProvider.getInstance().getUppercase_image()));
        makeASelectionUppercaseMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        makeASelectionUppercaseMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                String selectionText = textArea.getSelectedText();
                textArea.replaceText(textArea.getSelectionStart(), textArea.getSelectionEnd(), selectionText.toUpperCase());
            }
        });

        MenuItem makeASelectionLowercaseMenuItem = new MenuItem("Make a Selection Lowercase",
                new ImageView(SmartImageProvider.getInstance().getLowercase_image()));
        makeASelectionLowercaseMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.L, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        makeASelectionLowercaseMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                String selectionText = textArea.getSelectedText();
                textArea.replaceText(textArea.getSelectionStart(), textArea.getSelectionEnd(), selectionText.toLowerCase());
            }
        });

        MenuItem commentSelectionMenuItem = new MenuItem("Comment Selection",
                new ImageView(SmartImageProvider.getInstance().getComment_image()));
        commentSelectionMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        commentSelectionMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                String selectionText = textArea.getSelectedText();
                textArea.replaceText(textArea.getSelectionStart(), textArea.getSelectionEnd(), "/*" + selectionText + "*/");
            }
        });

        MenuItem removeCommentFromSelectionMenuItem = new MenuItem("Remove Comment From Selection",
                new ImageView(SmartImageProvider.getInstance().getUnComment_image()));
        removeCommentFromSelectionMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
        removeCommentFromSelectionMenuItem.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            if (textArea != null) {
                String selectionText = textArea.getSelectedText();
                if (selectionText.startsWith("/*") && selectionText.endsWith("*/")) {
                    textArea.replaceText(textArea.getSelectionStart(), textArea.getSelectionEnd(), selectionText.substring(2, selectionText.length() - 2));
                }
            }
        });

        MenuItem queryAnalyzerMenuItem = getQueryAnalyzerMenuItem();
        MenuItem slowQueryAnalyzerMenuItem = getSlowQueryAnalyzerMenuItem();
        MenuItem debuggerrMenuItem = getDebuggerMenuItem();

        advancedMenu.getItems().addAll(makeASelectionUppercaseMenuItem, makeASelectionLowercaseMenuItem,
                commentSelectionMenuItem, removeCommentFromSelectionMenuItem);

        return new MenuItem[]{refreshObjectBrowserMenuItem, changeObjectBrowserColorMenuItem,
            executeQueryMenu, executeExplainMenu, sqlFormatterMenu, insertTemplatesMenuItem, undoMenuItem,
            redoMenuItem, cutMenuItem, copyMenuItem, copyWithNormalizedWhitespaceMenuItem, pasteMenuItem,
            insertFromFileMenuItem,
            selectAllMenuItem, findMenuItem, findNextMenuItem, replaceMenuItem, gotoMenuItem, listAllTagsMenuItem,
            listMatchingTagsMenuItem, hideObjectBrowserMenuItem, hideResultPaneMenuItem, hideSQLEditorMenuItem,
            switchToPreviousTabMenuItem, switchToNextTabMenuItem, advancedMenu, listFunctionMenuItem, queryAnalyzerMenuItem, slowQueryAnalyzerMenuItem, debuggerrMenuItem, explainVisualMenuItem};
    }

    public static String formatCurrentQuery(PreferenceDataService preferenceService, String query) {

        SqlFormatter formatter = new SqlFormatter();

        List<PreferenceData> list = preferenceService.getAll();
        if (!list.isEmpty()) {
            return formatter.formatSql(query, list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle());
        } else {
            return formatter.formatSql(query, "", "2", "block");
        }
    }
    
    public static void formatCurrentQuery(MainController controller, PreferenceDataService preferenceService, ConnectionSession session, QueryAreaWrapper textArea, int index) {

        try {
            String formatedText;
            SqlFormatter formatter = new SqlFormatter();
            
            // find current query
            //        FindQueryResult queryResult = findQuery(textArea.getText(), index);
            PreferenceData pd = preferenceService.getPreference();
            ScriptRunner scriptRunner = new ScriptRunner(session.getService().getDB(), pd.isHaltExecutionOnError());
            Pair<Integer, String> res = scriptRunner.findQueryAtPosition(textArea.getText(), index, false);
            if (res != null && res.getValue() != null) {

                List<PreferenceData> list = preferenceService.getAll();
                if (!list.isEmpty()) {
                    formatedText = formatter.formatSql(res.getValue(), list.get(0).getTab(), list.get(0).getIndentString(), list.get(0).getStyle());
                } else {
                    formatedText = formatter.formatSql(res.getValue(), "", "2", "block");
                }

                controller.usedQueryFormat();

            //            if (queryResult.start > 0) {
            //                formatedText = "\n" + formatedText;
            //            }

                int from = textArea.getText().indexOf(res.getValue());
                int to = from + res.getValue().length() + 1;
                if (to >= textArea.getText().length()) {
                    to = textArea.getText().length();
                }
                textArea.replaceText(from, to, formatedText);
                if (index >= textArea.getText().length()) {
                    index = textArea.getText().length();
                }
                textArea.positionCaret(index >= 0 ? index : 0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryTabModule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MenuItem getQueryAnalyzerMenuItem() {
        MenuItem queryAnalyzer = new MenuItem("SQL Optimizer", new ImageView(StandardDefaultImageProvider.getInstance().getAnalyzer_image()));
        queryAnalyzer.setOnAction((ActionEvent event) -> {
            QueryAreaWrapper textArea = getSessionCodeArea();
            String selectedText = textArea != null ? textArea.getSelectedText() : "";
            if (selectedText == null) {
                selectedText = "";
            }
            openQueryAnalyzer(selectedText, RunnedFrom.QA);
        });
        return queryAnalyzer;
    }

    public MenuItem getSlowQueryAnalyzerMenuItem() {
        MenuItem queryAnalyzer = new MenuItem("MySQL Slow Log Query Analyzer", new ImageView(StandardDefaultImageProvider.getInstance().getSlow_analyzer_image()));
        queryAnalyzer.setOnAction((ActionEvent event) -> {
            showSlowQueryAnalyzer(
                getMainController(), 
                getMainController().getSecondProfilerService(), 
                getMainController().getPreferenceService(),
                getMainController().getRecommendationService(),
                getMainController().getColumnCardinalityService(),
                getMainController().getPerformanceTuningService(),
                getMainController().getDb(),
                getMainController().getUsageService()
            );
        });
        return queryAnalyzer;
    }
    
    public MenuItem getDebuggerMenuItem() {
        MenuItem queryAnalyzer = new MenuItem("Debugger", new ImageView(StandardDefaultImageProvider.getInstance().getDebugger_image()));
        queryAnalyzer.setOnAction((ActionEvent event) -> {
            showDebugger(getController(), getMainController().getDatabaseComboBox().getSelectionModel().selectedItemProperty(),
                    getMainController().getChartService(), getMainController().getSecondProfilerService(), getMainController().getPreferenceService(),
                    getMainController().getDeadlockService(), getMainController().getRecommendationService(),
                    getMainController().getColumnCardinalityService(), getMainController().getPerformanceTuningService(),
                    getMainController().getSettingsService(),getMainController().getDb(), getMainController().getDebuggerProfilerService(),
                    getMainController().getUsageService());
        });
        return queryAnalyzer;
    }

    public void openQueryAnalyzer(String query, RunnedFrom runnedFrom) {
        if (queryAnalyzerImage == null) {
            queryAnalyzerImage = StandardDefaultImageProvider.getInstance().getQueryAnalyzeTab_image();
        }
        ConnectionSession s = getController().getSelectedConnectionSession().get();
//                
        QueryAnalyzerTabController queryAnaController = StandardBaseControllerProvider.getController(getController(), "QueryAnalyzerTab");
        queryAnaController.setService(s.getService());
        queryAnaController.setPerformanceTuningService(getMainController().getPerformanceTuningService());
        queryAnaController.setPreferenceService(getMainController().getPreferenceService());
        queryAnaController.setQars(getMainController().getRecommendationService());
        queryAnaController.setColumnCardinalityService(getMainController().getColumnCardinalityService());
        queryAnaController.setUsageService(getMainController().getUsageService());
        queryAnaController.init(query, s, runnedFrom);

        Tab alterTab = new Tab("SQL Optimizer");
        alterTab.setGraphic(new ImageView(queryAnalyzerImage));

        Parent p = queryAnaController.getUI();
        alterTab.setContent(p);

        getController().addTab(alterTab, s);
        getController().showTab(alterTab, s);
    }

    private void explainQuery(ConnectionSession session, String explainText, QueryAreaWrapper area) {

        try {
            //        boolean needExecute = false;
            ScriptRunner sr = new ScriptRunner(session.getService().getDB(), true);
            Pair<Integer, String> result = sr.findQueryAtPosition(area.getText(), area.getCaretPosition(CARET_POSITION_FROM_START), false);
            if (result != null) {
                String explainQuery = QueryFormat.convertToExplainQuery(explainText, result.getValue());
            //        if (explainQuery != null) {
            //            area.clear();
            //            area.appendText(explainQuery);
            //            needExecute = true;
            //        }

                if (explainQuery != null) {
                    executeLineActionPerformed(session, area, explainQuery, -1, false);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryTabModule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Stage findReplacteDialog = null;
    
    public synchronized void showFindDialog(FindObject area) {
        
        if (area != null) {
            if (!findReplaceOpened) {
            
                findReplaceOpened = true;
                findReplacteDialog = new Stage();
                findReplacteDialog.initStyle(StageStyle.UTILITY);

                TabPane tabPane = createFindReplaceNode(area);
                tabPane.getSelectionModel().select(0);

                Scene scene = new Scene(tabPane);
                findReplacteDialog.setScene(scene);
                findReplacteDialog.initOwner(getController().getStage());
                findReplacteDialog.setTitle("Find & Replace");

                findReplacteDialog.setOnHidden((WindowEvent event) -> {
                    findReplaceOpened = false;
                });
                
                findReplacteDialog.showAndWait();
            }
        }
    }

    public synchronized void showReplaceDialog(FindObject area) {
        if (!findReplaceOpened) {
            
            findReplaceOpened = true;
            
            findReplacteDialog = new Stage();
            findReplacteDialog.initStyle(StageStyle.UTILITY);

            TabPane tabPane = createFindReplaceNode(area);
            tabPane.getSelectionModel().select(1);

            Scene scene = new Scene(tabPane);
            findReplacteDialog.setScene(scene);
            findReplacteDialog.initOwner(getController().getStage());
            findReplacteDialog.setTitle("Find & Replace");
            
            findReplacteDialog.setOnHidden((WindowEvent event) -> {
                findReplaceOpened = false;
            });

            findReplacteDialog.showAndWait();
        }
    }
    
    private boolean findReplaceOpened = false;
    
    private TabPane createFindReplaceNode(FindObject area) {
        
        FindDialogController controller0 = ((MainController)getController()).getTypedBaseController("FindDialog");
        controller0.initFindObject(codeareasFindDada, area);

        Tab findTab = new Tab("Find");
        findTab.setContent(controller0.getUI());
        
        // TODO Why we need this?
        codeareasFindDada.put(area, controller0.getFindData());
        
        ReplaceDialogController controller1 = ((MainController)getController()).getTypedBaseController("ReplaceDialog");
        controller1.setFindObject(area);

        Tab replaceTab = new Tab("Replace");
        replaceTab.setContent(controller1.getUI());
        
        return new TabPane(findTab, replaceTab);
    }

    private void showGoToLineDialog(QueryAreaWrapper area) {
        final Stage dialog = new Stage();
        
        GoToDialogController controller = getTypedBaseController("GoToLineDialog");
        controller.setCodeArea(area);

        final Parent p = controller.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        final Scene scene = new Scene(p);
        dialog.setScene(scene);
//        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Go To");

        dialog.showAndWait();
    }

    private String readFromUserFile() {
        FileChooser fc = new FileChooser();
        File userFile = fc.showOpenDialog(getController().getStage());
        if (userFile != null) {
            try {
                byte[] data = Files.readAllBytes(userFile.toPath());
                return new String(data, "UTF-8");
            } catch (IOException ex) {
                DialogHelper.showError(getController(),"Error reading user template file", ex.getMessage(), ex);
            }
        }

        return null;
    }

    public String getCursorLineText(TextArea textarea, int cursor) {
        String[] splittedLines = textarea.getText().split("\n");
        int sumCarets = 0;
        for (String line : splittedLines) {
//            log.debug("sumcarets : " + sumCarets);
            if (splittedLines[0] == line && line.length() == 0) {
                continue;
            }
//            if (line.length() == 0) {
//                sumCarets++;
//                continue;
//            }
            sumCarets += line.length();
//            log.debug("sumcarets2 : " + sumCarets);
            if (cursor <= sumCarets) {
//                log.debug("Line is : " + line);
                return line;
            }
            sumCarets++;
        }
        return ";";
    }

    public ScriptRunner executeLineActionPerformed(ConnectionSession session, QueryAreaWrapper textArea, String lineText, int position, boolean lock) {
        return executeLineActionPerformed(session, textArea, lineText, position, false, lock);
    }

    public ScriptRunner executeLineActionPerformed(ConnectionSession session, QueryAreaWrapper textArea, String lineText, int position, boolean editMode, boolean lock) {

        try {
            
//            log.error("executeLineActionPerformed: " + lineText);
            AbstractDBService dbService = ((AbstractDBService) session.getService());
            PreferenceData pd = preferenceService.getPreference();
            ScriptRunner scriptRunner = new ScriptRunner(dbService.getDB(), pd.isHaltExecutionOnError());

            // I was removed addBackgroundWork + WorkProc
            // because we must run this method from many threads
            // if not remove - than next executing waiting finishing of prev
            // Terah : i am re-add addBackgroundWork + WorkProc
            // because i am fix synchronize Threads
            dbService.setServiceBusy(true);
//            dbService.changeDatabase(session.getConnectionParam().getSelectedDatabase());

//            List<Tab> tabs = visualExplanTabs.get(session);
//            if (tabs != null) {
//                for (Tab t: tabs) {
//                    QueryAnalyzerTabController.closeVisualExplain(getMainController(), t);
//                }
//                
//                tabs.clear();
//            }

//            log.error("addWorkProc: " + lineText);
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    try {
                        
//                        log.error("run work proc: " + lineText);
//                        MemoryUsageUtils.printMemoryUsage("Execute queries", true);

                        scriptRunner.runScript(new StringReader(lineText), position, session.getConnectionParam().getLimitResult(),
                                new ScriptRunner.ScriptRunnerCallback() {

                            private volatile int queriesSize;
                            private volatile int successCount;
                            private volatile int errorCount;
                            private volatile int warningCount;

                            private boolean canProfiling = false;
                            private boolean profiling = false;
                            private Map<String, Object> showStatusBefore;
                            private Map<String, Object> showStatusAfter;
//                            private Map<String, Double> showProfiles;
                            private List<ExplainResult> explainResult;
                            private Map<String, String> extendedResult;
                            
                            private boolean showProfiler = false;
                            
                            private BigDecimal overallTotalTime;

                            private List<QueryResult> queryResults;
                            final Tab connectionTab = getController().getSelectedConnectionTab(
                                    getController().getSelectedConnectionSession().get()).get();

                            private Node oldGraphicNode;

                            Tab resultSetTab;
                            Tab infoTab;

                            ResultTabController resultTabController;
                            
                            @Override
                            public void started(Connection con, int queriesSize) throws SQLException {
                                
//                                log.error("started: " + lineText);
                                
                                this.queriesSize = 0;
                                queryResults = new ArrayList<>();
                                successCount = 0;
                                errorCount = 0;
                                warningCount = 0;

                                resultSetTab = null;
                                infoTab = null;
                                
                                if (queriesSize == 1) {
                                    canProfiling = true;
                                }

                                Platform.runLater(() -> {
                                    
                                    Tab selectedTab = null;
                                    for (Tab t: codeareas.keySet()) {
                                        if (codeareas.get(t) == textArea) {
                                            selectedTab = t;
                                            break;
                                        }
                                    }
                                    
                                    SimpleStringProperty prop = runnedQueryCountMap.get(selectedTab);
                                    if (prop != null) {
                                        prop.set("0 queries runned");
                                    }
                                    
                                    ProgressIndicator pi = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
                                    pi.setPrefSize(15, 15);
                                    pi.setStyle("-fx-progress-color: white ;");
                                    oldGraphicNode = connectionTab.getGraphic();
                                    connectionTab.setGraphic(pi);

                                    Map<QueryAreaWrapper, List<Tab>> map = resultTabs.get(session);
                                    if (map == null) {
                                        map = new HashMap<>();
                                        resultTabs.put(session, map);
                                    }

                                    List<Tab> tabList = map.get(textArea);
                                    if (tabList == null) {
                                        tabList = new ArrayList<>();
                                        map.put(textArea, tabList);
                                    }

                                    int countResultSets = 0;
                                    if (queriesSize == 1) {
                                        
                                        for (Tab t : tabList) {
                                            if (t.getUserData() instanceof ResultTabController) {
                                                log.error("TAB :: " + t.getText());
                                                resultSetTab = t;
                                                
                                                countResultSets++;
                                                if (countResultSets > 1) {
                                                    break;
                                                }
                                            } else if (t.getUserData() instanceof ProfilerTabController) {
                                                infoTab = t;
                                            }
                                        }
                                    }

                                    if (countResultSets > 1 || queriesSize > 1) {

                                        resultSetTab = null;
                                        infoTab = null;

                                        for (Tab t : tabList) {
                                            ((MessagesTabModule) ((MainController)getController()).getModuleByClassName(
                                                    MessagesTabModule.class.getName())).removeFromMessageSector(session, t);

                                            Object data = t.getUserData();

                                            if (data instanceof ResultTabController) {
                                                ((ResultTabController) data).clearData();

                                                ((ResultTabController) data).destroyController();
                                            }

                                            ((ArrayList<Tab>) connectionTab.getUserData()).remove(t);
                                        }

                                        tabList.clear();
                                    }
                                });
                            }

                            boolean wasLockedSelect = false;

                            @Override
                            public void queryExecuting(Connection con, QueryResult queryResult) throws SQLException {
                                queriesSize++;

//                                log.error("executing: " + lineText);
                                // if lock first execute lock commands
//                                if (queryResult.getQuery().toLowerCase().trim().startsWith("select") && lock) {
//                                    con.prepareStatement("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;").execute();
//                                    wasLockedSelect = true;
//                                }

                                // check maybe need add profilling
//                                if (canProfiling) {
//                                    try {
//                                        SelectCommand command = QueryFormat.parseSelectQuery(queryResult.getSimpleQuery());
//                                        if (command != null) {
//
//                                            profiling = true;
//
//                                            // check if profiller enabled
//                                            ProfilerTabController.enableProfiling(dbService, logger);
//                                            PreferenceData pd = preferenceService.getPreference();
//                                            if (pd.isProfillerStatusVariables()) {
//                                                showStatusBefore = ProfilerTabController.executeShowStatus(dbService, logger);
//                                            } else {
//                                                showStatusBefore = null;
//                                            }
//                                        }
//                                    } catch (Throwable th) {
//                                        log.error(th.getMessage(), th);
//                                    }
//                                }
                            }                           

                            @Override
                            public String prepareQuery(QueryResult queryResult, String query) {
                                
                                String newQuery = "";
                                if (queryResult.getQuery().toLowerCase().trim().startsWith("select") && lock) {
                                    newQuery += "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;";
                                    wasLockedSelect = true;
                                }
                                
                                boolean canExplain = queryResult.getQuery().trim().toLowerCase().startsWith("select") ||
                                            queryResult.getQuery().trim().toLowerCase().startsWith("update") ||
                                            queryResult.getQuery().trim().toLowerCase().startsWith("insert") ||
                                            queryResult.getQuery().trim().toLowerCase().startsWith("delete");
                                
                                if (canProfiling && !(queryResult.getQuery() != null && canExplain)) {
                                    canProfiling = false;
                                }
                                
                                if (canProfiling) {
                                    try {
                                        QueryClause command = QueryFormat.parseQuery(queryResult.getSimpleQuery());
                                        if (command != null) {

                                            profiling = true;
                                            
                                            newQuery += "SET PROFILING = 1;";
                                            
                                            PreferenceData pd = preferenceService.getPreference();
                                            if (pd.isProfillerStatusVariables()) {
                                                newQuery += "SHOW STATUS;";
                                            }
                                        }
                                    } catch (Throwable th) {
                                        log.error(th.getMessage(), th);
                                    }
                                }
                                
                                newQuery += query;
                                if (!newQuery.trim().endsWith(";")) {
                                    newQuery += ";";
                                }
                                
                                if (wasLockedSelect && lock) {
                                    newQuery += "COMMIT;";
                                }
                                
                                if (profiling) {
                                    newQuery += "SHOW STATUS;";
                                   
                                    boolean wasExplain = false;
                                    if (queryResult.getQuery() != null && canExplain) {
                                        if (dbService.getDbSQLVersionMaj() > 5) {
                                            newQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), query);
                                        } else {
                                            newQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN_EXTENDED), query);
                                        }
                                        
                                        wasExplain = true;
                                    }
                                    
                                    if (pd.isProfillerExplainExtended()) {
                                        if (!wasExplain) {
                                            if (dbService.getDbSQLVersionMaj() > 5) {
                                                newQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN), query);
                                            } else {
                                                newQuery += String.format(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__EXPLAIN_EXTENDED), query);
                                            }
                                        }
                                        
                                        if (!newQuery.trim().endsWith(";")) {
                                            newQuery += ";";
                                        }
                                        
                                        newQuery += QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_WARNINGS);
                                    }
                                    

                                    if (!newQuery.trim().endsWith(";")) {
                                        newQuery += ";";
                                    }

                                    if (pd.isProfillerShowProfile()) {
    //                                    newQuery += "SHOW PROFILES;";
                                        // QUERY_ID = 2 - NOT 100% !!!!!!!!
                                        //newQuery += "SELECT state, ROUND(SUM(duration),5) AS `duration (summed) in sec` FROM information_schema.profiling WHERE query_id = 2 GROUP BY state ORDER BY `duration (summed) in sec` DESC;";
                                    }
                                    
                                    newQuery += "SET PROFILING = 0;";
                                }
                                
                                
                                
                                return newQuery;
                            }

                            
                            
                            
                            @Override
                            public boolean isWithLimit(QueryResult queryResult) {
                                boolean withLimit = true;
                                String latestQuery = session.getConnectionParam().getLatestQuery();
                                if (latestQuery != null && latestQuery.equalsIgnoreCase(queryResult.getSimpleQuery())) {
                                    withLimit = session.getConnectionParam().isWithLimit();
                                }

                                return withLimit;
                            }

                            @Override
                            public void queryExecuted(Connection con, QueryResult queryResult) throws SQLException {

//                                log.error("executed: " + lineText);

                                int resultIndex = 0;
                                
                                Object currentResult = queryResult.getResult().isEmpty() ? null : queryResult.getResult().get(resultIndex);
                                
                                if (queryResult.getWarningCodes() != null) {
                                    warningCount += queryResult.getWarningCodes().size();
                                }
                                        
                                if (wasLockedSelect && lock) {
//                                    con.prepareStatement("COMMIT;").execute();
                                    wasLockedSelect = false;
                                }

                                boolean canExplain = queryResult.getQuery().trim().toLowerCase().startsWith("select") ||
                                            queryResult.getQuery().trim().toLowerCase().startsWith("update") ||
                                            queryResult.getQuery().trim().toLowerCase().startsWith("insert") ||
                                            queryResult.getQuery().trim().toLowerCase().startsWith("delete");
                                
                                // check maybe profilling was enabled
                                if (profiling) {
                                    profiling = false;

                                    // ignore SET PROFILING = 1;
                                    resultIndex++;
                                    
                                    showStatusBefore = FunctionHelper.executeShowStatus((ResultSet) queryResult.getResult().get(resultIndex), true);
                                    resultIndex++;
                                    
                                    currentResult = queryResult.getResult().get(resultIndex);
                                    resultIndex++;
                                    
                                    PreferenceData pd = preferenceService.getPreference();
                                    if (pd.isProfillerStatusVariables()) {
                                        showStatusAfter = FunctionHelper.executeShowStatus((ResultSet) queryResult.getResult().get(resultIndex), true);
                                        resultIndex++;
                                    } else {
                                        showStatusAfter = null;
                                    }
                                    
                                    if (queryResult.getQuery() != null && canExplain) {
                                        if (pd.isProfillerExplainResult()) {
                                            try {
                                                explainResult = FunctionHelper.explainQuery((ResultSet) queryResult.getResult().get(resultIndex), true);                               
                                                resultIndex++;
                                            } catch (Throwable th) {
                                            }
                                        } else {
                                            explainResult = null;
                                        }
                                        showProfiler = true;
                                    }
                                    
                                    if (pd.isProfillerExplainExtended()) {
                                        try {
                                            extendedResult = FunctionHelper.explainExtendedQuery((ResultSet) queryResult.getResult().get(resultIndex), true); 
                                            resultIndex++;
                                        } catch (Throwable th) {
                                        }
                                    } else {
                                        extendedResult = null;
                                    }
                                    
//                                    String query = queryResult.getQuery();
//                                    if (query.endsWith(";")) {
//                                        query = query.substring(0, query.length() - 1);
//                                    }
                                    
                                    // NEED !!! MOVE TO PROFILER TAB ON SELECT
//                                    if (pd.isProfillerShowProfile()) {
//                                        showProfiles = ProfilerTabController.executeShowProfiles(dbService, query, logger);
//                                    } else {
//                                        showProfiles = null;
//                                    }

//                                    ProfilerTabController.disableProfiling(dbService, logger); 
                                }

                                List<String> queries = successfullyExecutedQueries.get(session);
                                if (queries == null) {
                                    queries = new ArrayList<>();
                                    successfullyExecutedQueries.put(session, queries);
                                }

                                String query = queryResult.getQuery().trim();
                                if (query.endsWith(";")) {
                                    query = query.substring(0, query.length() - 1).trim();
                                }

                                if (queryResult.getErrorMessage() == null) {
                                    if (!queries.contains(query)) {
                                        queries.add(query);
                                    }
                                } else {
                                    queries.remove(query);
                                }
                                
                                Object staticCurrentResult = currentResult;
                                
                                Platform.runLater(() -> {                                    
//                                    MemoryUsageUtils.printMemoryUsage("BEGIN");
//                                    int count = staticCurrentResult != null ? 1 : 0;
//                                    if (count >= 25) {
//                                        count = 25;
//                                    }

//                                    for (int i = 0; i < count; i++) {
                                    Object result = staticCurrentResult;

                                    queryResult.setMainResultSet(staticCurrentResult);
                                    
                                    if (canExplain) {
                                        
                                        Map<QueryAreaWrapper, List<Tab>> map = resultTabs.get(session);
                                        if (map == null) {
                                            map = new HashMap<>();
                                            resultTabs.put(session, map);
                                        }

                                        List<Tab> tabList = map.get(textArea);
                                        if (tabList == null) {
                                            tabList = new ArrayList<>();
                                            map.put(textArea, tabList);
                                        }
                                        
                                        if (showProfiler) {
                                            ProfilerTabController profilerTabController = StandardBaseControllerProvider.getController(getController(), "ProfilerTab");
                                            profilerTabController.init(showStatusBefore, showStatusAfter, explainResult, extendedResult);

                                            if (infoTab == null) {
                                                Tab profilerTab = new Tab("Profiler");                                                    
                                                profilerTab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(StandardDefaultImageProvider.getInstance().getProfilerTabIcon_image()),
                                                        18, 18));
                                                profilerTab.setUserData(profilerTabController);
                                                profilerTab.setOnClosed((Event event) -> {
                                                    ((ArrayList<Tab>) connectionTab.getUserData()).remove(profilerTab);

                                                    Map<QueryAreaWrapper, List<Tab>> map0 = resultTabs.get(session);
                                                    List<Tab> tabList0 = map0.get(textArea);
                                                    if (tabList0 != null) {
                                                        tabList0.remove(profilerTab);
                                                    }
                                                });
                                                tabList.add(profilerTab);

                                                profilerTab.setContent(profilerTabController.getUI());

                                                ((ArrayList<Tab>) connectionTab.getUserData()).add(profilerTab);

                                                if (getSessionCodeArea() == textArea) {                                                    
                                                    ((MessagesTabModule) getController().getModuleByClassName(
                                                            MessagesTabModule.class.getName())).addToMessageSector(session, profilerTab, true);
                                                }

                                                profilerTab.selectedProperty().addListener(new ChangeListener<Boolean>() {
                                                    @Override
                                                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                                                        if (newValue != null && newValue && !profilerTabController.isFullInited()) {
                                                            try {
                                                                profilerTabController.doFullInit(session.getService(), queryResult.getQuery(), pd.isProfillerShowProfile());
                                                            } catch (SQLException ex) {
                                                                log.error(ex.getMessage(), ex);
                                                            }
                                                        }
                                                    }
                                                });
                                            } else {
                                                infoTab.setContent(profilerTabController.getUI());
                                            }
                                        } else if (infoTab != null) {
                                            ((MessagesTabModule) ((MainController)getController()).getModuleByClassName(
                                                    MessagesTabModule.class.getName())).removeFromMessageSector(session, infoTab);
                                            ((ArrayList<Tab>) connectionTab.getUserData()).remove(infoTab);
                                        }
                                    }
                                
                                    if (ResultSet.class.isInstance(result)) {
                                
                                        if (!queryResults.contains(queryResult)) {
                                            successCount++;
                                            queryResults.add(queryResult);
                                        }
                                        queryResult.setResultSet(true);

                                        final int staticCount = successCount;

                                        try {
                                            Map<QueryAreaWrapper, List<Tab>> map = resultTabs.get(session);
                                            if (map == null) {
                                                map = new HashMap<>();
                                                resultTabs.put(session, map);
                                            }

                                            List<Tab> tabList = map.get(textArea);
                                            if (tabList == null) {
                                                tabList = new ArrayList<>();
                                                map.put(textArea, tabList);
                                            }
                                            
                                            ResultSet rs = (ResultSet) result;

                                            rs.last();
                                            queryResult.setRowAffected(rs.getRow() + ROWS_AFFECTED);

                                            boolean withLimit = true;
                                            String latestQuery = session.getConnectionParam().getLatestQuery();
                                            if (latestQuery != null && latestQuery.equalsIgnoreCase(queryResult.getQuery())) {
                                                withLimit = session.getConnectionParam().isWithLimit();
                                            }


                                            if (resultSetTab == null) {

                                                resultTabController = getTypedBaseController("ResultTab");
                                                resultTabController.setService(session.getService());
                                                resultTabController.init(con, rs, queryResult.getSimpleQuery(), session, editMode, true, withLimit);

                                                Parent p = resultTabController.getUI();

                                                Tab resultTab = new Tab("Result " + staticCount);
                                                resultTab.setGraphic(FunctionHelper.constructImageViewWithSize(new ImageView(SmartImageProvider.getInstance().getResultTab_image()), 18, 18));
                                                resultTab.setUserData(resultTabController);
                                                resultTab.setOnClosed((Event event) -> {
                                                    resultTabController.destroyController();
                                                    ((ArrayList<Tab>) connectionTab.getUserData()).remove(resultTab);

                                                    Map<QueryAreaWrapper, List<Tab>> map0 = resultTabs.get(session);
                                                    List<Tab> tabList0 = map0.get(textArea);
                                                    if (tabList0 != null) {
                                                        tabList0.remove(resultTab);
                                                    }
                                                });

                                                tabList.add(resultTab);

                                                resultTab.setContent(p);

                                                session.getConnectionParam().setLatestQuery(queryResult.getQuery());
                                                ((ArrayList<Tab>) connectionTab.getUserData()).add(resultTab);
                                                //if (getSessionCodeArea() == textArea) {
                                                    ((MessagesTabModule) getMainController().getModuleByClassName(
                                                            MessagesTabModule.class.getName())).addToMessageSector(session, resultTab, true);
                                                //}
                                            } else {

                                                resultTabController = (ResultTabController) resultSetTab.getUserData();
                                                resultTabController.resetData();
                                                resultTabController.init(con, rs, queryResult.getSimpleQuery(), session, editMode, true, withLimit);

                                                // if we at other QB then runned this query
                                                if (resultSetTab.getTabPane() != null) {
                                                    resultSetTab.getTabPane().getSelectionModel().select(resultSetTab);
                                                }
                                            }
                                        } catch (SQLException ex) {
                                            DialogHelper.showError(getController(),"Error", ex.getMessage(), ex);
                                        }

                                        queryResult.setEndDuration(System.currentTimeMillis());
                                    } else if (result.getClass() == Integer.class) {

                                        queryResult.setResultSet(false);
                                        queryResult.setRowAffected((Integer) result + ROWS_AFFECTED);

                                        if (!queryResults.contains(queryResult)) {
                                            successCount++;
                                            queryResults.add(queryResult);
                                        }
                                    }
//                                    }

                                    if (!isAlterTab()) {
                                        Recycler.addWorkProc(new WorkProc() {

                                            SQLException exception;

                                            @Override
                                            public void updateUI() {
                                                if (exception != null) {
                                                    DialogHelper.showError(getController(),"Error", exception.getMessage(), exception);
                                                }
                                            }

                                            @Override
                                            public void run() {
                                                try {
                                                    String msg = SqlUtils.makeQueryOneLine(queryResult.getQuery());
                                                    logger.log(session.getService().getDB(), msg, true);
                                                    
                                                    TabContentController tcc = getController().getTabControllerBySession(session);
                                                    if (tcc instanceof ConnectionTabContentController) {
                                                        ((ConnectionTabContentController)tcc).listen(session.getService().getDB(), msg, true);
                                                    }
                                                } catch (SQLException ex) {
                                                    exception = ex;
                                                    callUpdate = true;
                                                }
                                            }
                                        });
                                    }
//                                    MemoryUsageUtils.printMemoryUsage("END");

                                    Tab selectedTab = null;
                                    for (Tab t: codeareas.keySet()) {
                                        if (codeareas.get(t) == textArea) {
                                            selectedTab = t;
                                            break;
                                        }
                                    }
                                    
                                    SimpleStringProperty prop = runnedQueryCountMap.get(selectedTab);
                                    if (prop != null) {
                                        prop.setValue(successCount + warningCount + " queries runned");
                                    }
                                });
                            }

                            @Override
                            public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {

                                // No need we showing in message this error
                                //Platform.runLater(() -> showError("Error", th.getMessage(), th));
                                if (infoTab != null) {
                                    Platform.runLater(() -> {
                                        ((MessagesTabModule) ((MainController)getController()).getModuleByClassName(
                                                MessagesTabModule.class.getName())).removeFromMessageSector(session, infoTab);
                                        ((ArrayList<Tab>) connectionTab.getUserData()).remove(infoTab);
                                        
                                        Map<QueryAreaWrapper, List<Tab>> map0 = resultTabs.get(session);
                                        List<Tab> tabList0 = map0.get(textArea);
                                        if (tabList0 != null) {
                                            tabList0.remove(infoTab);
                                        }
                                    });
                                }

                                if (resultSetTab != null) {
                                    Platform.runLater(() -> {
                                        ((MessagesTabModule) ((MainController)getController()).getModuleByClassName(
                                                MessagesTabModule.class.getName())).removeFromMessageSector(session, resultSetTab);
                                        ((ArrayList<Tab>) connectionTab.getUserData()).remove(resultSetTab);
                                        
                                        
                                        ((ResultTabController) resultSetTab.getUserData()).clearData();
                                        ((ResultTabController) resultSetTab.getUserData()).destroyController();
                                        
                                        Map<QueryAreaWrapper, List<Tab>> map0 = resultTabs.get(session);
                                        List<Tab> tabList0 = map0.get(textArea);
                                        if (tabList0 != null) {
                                            tabList0.remove(resultSetTab);
                                        }
                                        
                                        resultSetTab = null;
                                    });
                                }

                                errorCount++;
                                queryResult.setRowAffected(th.getMessage());
                                queryResults.add(queryResult);

                                String query = queryResult.getQuery().toUpperCase();
                                if (query.contains("FOREIGN KEY") && query.contains("REFERENCES")) {
                                    try (ResultSet rs = con.prepareStatement(QueryProvider.getInstance().getQuery(QueryProvider.DB__MYSQL, QueryProvider.QUERY__SHOW_ENGINE_INNODB_STATUS)).executeQuery()) {
                                        if (rs.next()) {
                                            String status = rs.getString("Status");
                                            int from = status.indexOf("LATEST FOREIGN KEY ERROR") + 24;
                                            int to = status.indexOf("TRANSACTIONS") - 1;

                                            while (status.charAt(from) == '-' || status.charAt(from) == '\n') {
                                                from++;
                                            }

                                            while (status.charAt(to) == '-' || status.charAt(to) == '\n') {
                                                to--;
                                            }

                                            queryResult.setErrorMessage(queryResult.getErrorMessage() + "\n" + status.substring(from, to + 1));
                                        }
                                    }
                                }

                                if (wasLockedSelect && lock) {
                                    //con.prepareStatement("COMMIT;").execute();
                                    wasLockedSelect = false;
                                }
                            }

                            
                            
                            @Override
                            public void finished(Connection con) throws SQLException {

                                overallTotalTime = BigDecimal.ZERO;

                                Platform.runLater(() -> {

                                    Map<QueryResult, String> errors = errorsForHighlight.get(getController().getSelectedConnectionSession().get());
                                    if (errors == null) {
                                        errors = new LinkedHashMap<>();
                                        errorsForHighlight.put(getController().getSelectedConnectionSession().get(), errors);
                                    }

                                    errors.clear();

                                    if (queryResults != null) {

                                        MessageData md = new MessageData();
                                        md.setQueriesSize(queriesSize);
                                        md.setSuccessCount(successCount);
                                        md.setErrorsCount(errorCount);
                                        md.setWarningsCount(warningCount);

                                        MessagesTabModule.MessageText myTextFlow = (MessagesTabModule.MessageText) ((MessagesTabModule) ((MainController)getController()).getModuleByClassName(
                                                MessagesTabModule.class.getName())).getSessionMessageBottomTabTextFlow(session);
                                        
                                        boolean hasResultSet = false;
                                        for (QueryResult queryResult: queryResults) {

                                            addMessageItem(queryResult, errors, md);

                                            overallTotalTime = overallTotalTime.add(new BigDecimal(queryResult.getResponseTime()));
                                            if (!hasResultSet) {
                                                hasResultSet = queryResult.isResultSet();
                                            }
                                        }

                                        Tab selectedTab = null;
                                        for (Tab t: codeareas.keySet()) {
                                            if (codeareas.get(t) == textArea) {
                                                selectedTab = t;
                                                break;
                                            }
                                        }
                                        Object oldValue = ((ObservableMap<Tab, MessageData>) myTextFlow
                                                .getUserData()).put(selectedTab, md);

                                        if (oldValue instanceof MessageData) {
                                            ((MessageData) oldValue).getItems().clear();
                                        }
                                        
                                        if (!hasResultSet && textArea == getSessionCodeArea()) {
                                            ((MessagesTabModule) getMainController().getModuleByClassName(MessagesTabModule.class.getName())).messageTabToFront(session);
                                        }

                                        if (textArea == getSessionCodeArea()) {
                                            myTextFlow.init(md);
                                        }
                                    }

                                    connectionTab.setGraphic(oldGraphicNode);
                                    ((Label) getController().getApplicationFooter().
                                            getChildren().get(1)).setText(TOTAL + overallTotalTime.setScale(2, RoundingMode.HALF_UP) + SEC);

                                    QueryAreaWrapper textArea = getSessionCodeArea();
                                    //invalidating
                                    if (textArea != null) {
                                        textArea.appendText("");
                                    }
                                    updateErrorsList();
                                    
                                    if (queryResults != null) {
                                        queryResults.clear();
                                    }
                                    
//                                    if (resultTabController != null) {
//                                        PreferenceData pd = preferenceService.getPreference();
//                                        if (!pd.isKeepFocusOnEditor()) {                                                    
//                                            Thread th = new Thread(() -> {
////                                                try {
////                                                    Thread.sleep(1000);
////                                                } catch (InterruptedException ex) {
////                                                    Logger.getLogger(QueryTabModule.class.getName()).log(Level.SEVERE, null, ex);
////                                                }
//                                                Platform.runLater(() -> {
//                                                   resultTabController.requestFocus();
//                                                });
//                                            });
//                                            th.setDaemon(true);
//                                            th.start();
//                                        }
//                                    }
                                });

                                ((AbstractDBService) session.getService()).setServiceBusy(false);
                            }

                            @Override
                            public boolean isNeedStop() {
                                return false;
                            }
                        });
                    } catch (SQLException | IOException ex) {
                        DialogHelper.showError(getController(),"Error", ex.getMessage(), ex);
                        ((AbstractDBService) session.getService()).setServiceBusy(false);
                    }
                }
            });

            return scriptRunner;

        } catch (SQLException ex) {
            DialogHelper.showError(getController(),"Error", ex.getMessage(), ex);
            ((AbstractDBService) session.getService()).setServiceBusy(false);
            return null;
        }
    }
    
    private void addMessageItem(QueryResult queryResult, Map<QueryResult, String> errors, MessageData md) {
        int customRange0 = -1;
        int customRange1 = -1;

        String errMsg = queryResult.getErrorMessage();
        if (errMsg != null && StringUtils.countMatches(errMsg, SINGLE_QUOTE) > 1) {

            int i = errMsg.indexOf("at line ");
            try {
                int errorLine = Integer.parseInt(errMsg.substring(i + 8, errMsg.length()));
                queryResult.setErrorLine(errorLine);
            } catch (Throwable th) {
            }
            
            errMsg = errMsg.substring(errMsg.indexOf(SINGLE_QUOTE) + 1).trim();
            errMsg = errMsg.substring(0, errMsg.lastIndexOf(SINGLE_QUOTE)).trim();
                
            // try clear addintional queries
            String newErrMsg = errMsg;
            int idx = errMsg.indexOf(";");
            if (idx > -1) {
                newErrMsg = errMsg.substring(0, idx);
            }
            
            int index = newErrMsg.indexOf(LIMIT);
            if (index >= 0) {
                newErrMsg = newErrMsg.substring(0, index);
                newErrMsg = newErrMsg.substring(newErrMsg.indexOf(SINGLE_QUOTE) + 1).trim();
            }
            
            queryResult.setErrorMessage(queryResult.getErrorMessage().replace(errMsg, newErrMsg));
            errMsg = newErrMsg;

            errors.put(queryResult, errMsg);
            
            
            customRange0 = queryResult.getErrorMessage().indexOf(errMsg);
            customRange1 = customRange0 + errMsg.length();
        }

        md.addItem(
                SqlUtils.makeQueryOneLine(queryResult.getQuery()),
                queryResult.getErrorMessage() != null
                        ? ERROR_CODE + queryResult.getErrorCode() + ENTER + queryResult.getErrorMessage()
                        : queryResult.getRowAffected(),
                queryResult.getExecutionTime(),
                queryResult.getTransferTime(),
                queryResult.getResponseTime(),
                queryResult.getErrorMessage() != null,
                queryResult.getWarningCodes() != null,
                queryResult.getWarningCodes(),
                queryResult.getWarningMessages(),
                queryResult.isResultSet(),
                customRange0,
                customRange1);
    }
    
    public ScriptRunner bulkExecute(ConnectionSession session, QueryAreaWrapper textArea) {

        try {
            AbstractDBService dbService = ((AbstractDBService) session.getService());
            PreferenceData pd = preferenceService.getPreference();
            ScriptRunner scriptRunner = new ScriptRunner(dbService.getDB(), pd.isHaltExecutionOnError());

            String lineText = textArea.getSelectedText() != null && !textArea.getSelectedText().isEmpty() ? textArea.getSelectedText() : textArea.getText();
            
            dbService.setServiceBusy(true);
            
            Tab connectionTab = getController().getSelectedConnectionTab(
                                    getController().getSelectedConnectionSession().get()).get();
            
            ProgressIndicator pi = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
            pi.setPrefSize(15, 15);
            pi.setStyle("-fx-progress-color: white ;");
            Node oldGraphicNode = connectionTab.getGraphic();
            connectionTab.setGraphic(pi);
            
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void updateUI() {
                }

                @Override
                public void run() {
                    try {
                        
                        scriptRunner.runBulkScript(new StringReader(lineText), new ScriptRunner.ScriptRunnerCallback() {
                            
                            private boolean wasError = false;
                            
                            @Override
                            public void started(Connection con, int queriesSize) throws SQLException {
                                
                            }

                            @Override
                            public void queryExecuting(Connection con, QueryResult queryResult) throws SQLException {
                               
                            }                           

                            @Override
                            public String prepareQuery(QueryResult queryResult, String query) {
                                
                                return query;
                            }

                            
                            @Override
                            public boolean isWithLimit(QueryResult queryResult) {
                                return false;
                            }

                            @Override
                            public void queryExecuted(Connection con, QueryResult queryResult) throws SQLException {

                            }

                            @Override
                            public void queryError(Connection con, QueryResult queryResult, Throwable th) throws SQLException {
                                wasError = true;
                                Platform.runLater(() -> {
                                    connectionTab.setGraphic(oldGraphicNode);
                                    showError("Error", th.getMessage(), th); 
                                });
                            }

                            @Override
                            public void finished(Connection con) throws SQLException {
                                ((AbstractDBService) session.getService()).setServiceBusy(false);
                                if (!wasError) {
                                    Platform.runLater(() -> {
                                        connectionTab.setGraphic(oldGraphicNode);
                                        showInfo("Info", "Bulk execution done!", null); 
                                    });
                                }
                            }

                            @Override
                            public boolean isNeedStop() {
                                return false;
                            }
                        });
                    } catch (SQLException | IOException ex) {
                        showError("Error", ex.getMessage(), ex);
                        ((AbstractDBService) session.getService()).setServiceBusy(false);
                    }
                }
            });

            return scriptRunner;

        } catch (SQLException ex) {
            showError("Error", ex.getMessage(), ex);
            ((AbstractDBService) session.getService()).setServiceBusy(false);
            return null;
        }
    }

    public static FindQueryResult findQuery(String sql, int position) {

        FindQueryResult fqr = null;
        if (position >= 0) {

            if (position > sql.length()) {
                position = sql.length();
            }
            String before = sql.substring(0, position).toLowerCase();
            String after = sql.substring(position).toLowerCase();

            // find start query in before
            int startQuery = findQueryPosition(before, true);
            if (startQuery >= 0) {
                // find end query in after
                int endQuery = findQueryPosition(after, false);
                if (endQuery == -1 || endQuery == 0) {
                    endQuery = sql.length();
                } else {
                    endQuery += before.length();
                }

                String tempSql = sql.substring(startQuery, endQuery);
                Matcher m = QUERY_EMPTY_PREFIX.matcher(tempSql);
                if (m.find()) {
                    startQuery += m.group(1).length();
                }

                tempSql = tempSql.trim();

                fqr = new FindQueryResult();
                fqr.start = tempSql.isEmpty() ? 0 : startQuery;
                fqr.end = endQuery;
                fqr.query = tempSql.isEmpty() ? sql.trim() : tempSql;
            }
        }

        return fqr;
    }

    private static int findQueryPosition(String text, boolean before) {
        text = ScriptRunner.replaceComment(text, '*');
        
        int queryPosition = before ? text.lastIndexOf(";") : -1;
        if (queryPosition == -1) {
                        
            if (queryPosition == -1) {
                Matcher m = DELIMITER_END_PATTER.matcher(text);
                if (m.find()) {
                    queryPosition = m.start();
                }
            }
            
            // check for select
            Matcher matcher = SELECT_PATTER.matcher(text);
            while (matcher.find()) {
                queryPosition = matcher.start();
                if (queryPosition >= 0) {
                    // if found check may be it sub select
                    String beforeSelect = text.substring(0, queryPosition);
                    if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                        queryPosition = -1;
                    }
                }                                
                if (queryPosition >= 0) {
                    break;
                }
            }

            if (queryPosition == -1) {
                Matcher m = CREATE_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = ALTER_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = UPDATE_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = DELETE_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = INSERT_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = REPLACE_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = SHOW_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }

            if (queryPosition == -1) {
                Matcher m = FLUSH_PATTER.matcher(text);
                while (m.find()) {
                    queryPosition = m.start();
                    if (queryPosition >= 0) {
                        // if found check may be it sub select
                        String beforeSelect = text.substring(0, queryPosition);
                        if (beforeSelect.contains(",") || beforeSelect.contains("(")) {
                            queryPosition = -1;
                        }
                    }
                    if (queryPosition >= 0) {
                        break;
                    }
                }
            }
        } else {
            queryPosition++;
        }

        return queryPosition;
    }

    private MenuItem getQuerySettingsItem() {
        MenuItem querySettings = new MenuItem("Query Settings",
                new ImageView(StandardDefaultImageProvider.getInstance().getTestConnectionButton_image()));
        querySettings.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConfigurableQueryBinderController configQueryBinder = ((MainController)getController()).getTypedBaseController("ConfigurableQueryBinder");
                configQueryBinder.init(getSessionCodeArea()).setOnHiding(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        for (QueryAreaWrapper qaw : codeareas.values()) {
                            qaw.refreshQueryArea();
                        }
                    }
                });
                configQueryBinder.showConfigurableQueryBindingDialog();
            }
        });
        return querySettings;
    }

    @Getter
    public static class FindQueryResult {

        private int start;
        private int end;
        private String query;                
    }

    public boolean isUserKeyword(String s, ConnectionParams param) {
        for (Database d : param.getDatabases()) {
            if (s.equalsIgnoreCase(d.getName())) {
                return true;
            }
            
            if (DatabaseCache.getInstance().containsTable(param.cacheKey(), d.getName(), s)) {
                return true;
            }
        }

        return false;
    }

    // TODO May be need move this method somewhere
//    public static StyleSpans<Collection<String>> computeHighlighting(MainController controller, CodeArea textArea, int currentPosition, String text, int startMatching, int endMatching, ConnectionSession session) {
//
////        if (checkCanValidate(textArea, controller)) {
//            //System.out.println("computeHighlighting : { \n "+ text + "\n }");
//            String oldText = text;
////            if(endMatching >= text.length()){
////                endMatching = text.length() - 1;
////            }
////            if (endMatching > - 1) {
////                text = text.substring(startMatching, endMatching);
////            }
//
//            List<String> greenData = highlightGreenMap.get(session);
//            List<String> maroonData = highlightMaroonMap.get(session);
//            
//            boolean hasGreeData = greenData != null && !greenData.isEmpty();
//            boolean hasMaroonData = maroonData != null && !maroonData.isEmpty();
//            
//            Pattern dynamicPattern = Pattern.compile(
//                (hasGreeData ? "(?<GREENDATA>`?\\b(" + (greenData != null ? String.join("|", greenData) : "") + ")\\b`?)" : "") +
//                (hasMaroonData ? (hasGreeData ? "|" : "") + "(?<MAROONDATA>\\b(" + String.join("|", maroonData) + ")\\b)" : "") +
//                (hasGreeData || hasMaroonData ? "|" : "") + PATTERN.pattern(),
//                Pattern.CASE_INSENSITIVE);            
//            
//            Matcher matcher = dynamicPattern.matcher(text);
//            int lastKwEnd = 0;
//
//            StyleSpansBuilder<Collection<String>> spansBuilder
//                    = new StyleSpansBuilder<>();
//            TreeMap<Integer, Integer> hash = new TreeMap<>();
//            Map<Integer, String> hashStyle = new HashMap<>();
//
//            //added by Terah
//            boolean computeOpen_Close_Paren = (currentPosition >= startMatching) && (currentPosition <= endMatching);
//            boolean openParen =false;
//            boolean closeParen = false;
//            if (computeOpen_Close_Paren) {
//                if (currentPosition > oldText.length()) {
//                    currentPosition = (currentPosition * text.length()) / endMatching;
//                }
//                
//                String positionString = oldText.substring((currentPosition ==0)? currentPosition : currentPosition - 1, currentPosition);
//                openParen = computeOpen_Close_Paren && "(".equals(positionString);
//                closeParen = computeOpen_Close_Paren && ")".equals(positionString);
//                if (!openParen && !closeParen && (currentPosition + 1) < oldText.length()) {
//                    positionString = oldText.substring(currentPosition, currentPosition + 1);
//                    openParen = computeOpen_Close_Paren && "(".equals(positionString);
//                    closeParen = computeOpen_Close_Paren && ")".equals(positionString);
//                } else {
//                    currentPosition--;//nothing to lose if openParen & closeParen is false;
//                }
//            }
//            Map<Integer, String> parenHashStyle = new TreeMap<>();
//            while (matcher.find()) {
//                
//                String styleClass = null;
//                try {
//                    styleClass = matcher.group("GREENDATA") != null ? "greendata" : null;
//                } catch (IllegalArgumentException ex) {
//                    // do nothing - its okay
//                }
//                
//                if (styleClass == null) {
//                    try {
//                        styleClass = matcher.group("MAROONDATA") != null ? "maroondata" : null;
//                    } catch (IllegalArgumentException ex) {
//                        // do nothing - its okay
//                    }
//                }
//                
//                if (styleClass == null)  {
//                    styleClass = matcher.group("KEYWORD") != null ? "keyword"
//                        : matcher.group("FUNCTION") != null ? "function"
//                        : matcher.group("PAREN") != null ? "paren"
//                        : matcher.group("BRACE") != null ? "brace"
//                        : matcher.group("BRACKET") != null ? "bracket"
//                        : matcher.group("SEMICOLON") != null ? "semicolon"
//                        : matcher.group("STRING0") != null ? "string"
//                        : matcher.group("STRING1") != null ? "string"
//                        : matcher.group("STRING2") != null ? "string2"
//                        : matcher.group("COMMENT") != null ? "comment"
//                        : matcher.group("OPERATOR") != null ? "operator"
//                        : null;
//                }
//                
//                /* never happens */ assert styleClass != null;
//
//                int start = matcher.start();
//                int end =  matcher.end();
//                hash.put(start, end);
//
//                // collect parens if need for highlighting
//                if (openParen || closeParen) {
//                    if ("paren".equals(styleClass)) {
//                        String paren = matcher.group("PAREN");
//                        parenHashStyle.put(start, paren);
//                    }
//                }
//                hashStyle.put(start, styleClass);
//            }
//
//            // find positions for highlighting
//            if (openParen) {
//                hashStyle.put(currentPosition, "highlightNonExistParen");
//
//                int diff = 0;
//                for (Entry<Integer, String> entry : parenHashStyle.entrySet()) {
//                    if (entry.getKey() > currentPosition) {
//                        if ("(".equals(entry.getValue())) {
//                            diff++;
//                        } else if (diff == 0) {
//                            hashStyle.put(currentPosition, "highlightExistParen");
//                            hashStyle.put(entry.getKey(), "highlightExistParen");
//                            break;
//                        } else {
//                            diff--;
//                        }
//                    }
//                }
//            } else if (closeParen) {
//                hashStyle.put(currentPosition, "highlightNonExistParen");
//
//                List<Integer> list = new ArrayList(parenHashStyle.keySet());
//                Collections.reverse(list);
//
//                int diff = 0;
//                for (Integer position : list) {
//                    if (position < currentPosition) {
//                        if (")".equals(parenHashStyle.get(position))) {
//                            diff++;
//                        } else if (diff == 0) {
//                            hashStyle.put(currentPosition, "highlightExistParen");
//                            hashStyle.put(position, "highlightExistParen");
//                            break;
//                        } else {
//                            diff--;
//                        }
//                    }
//                }
//            }
//
//            lastKwEnd = 0;
//            //For the USER KEYWORDS
//
//            // @TODO it has VERY SLOW PERFORMANCE, if need - later need do something with its
//    //        if (param != null) {
//    //            Pattern USER_PATTERN = Pattern.compile(
//    //                    "(?<USERKEYWORD>" + "\\b(" + String.join("|", param.getKeywords()) + ")\\b" + ")",
//    //                    Pattern.CASE_INSENSITIVE);
//    //            Matcher matcher2 = USER_PATTERN.matcher(text);
//    //            while (matcher2.find()) {
//    //                String styleClass = matcher2.group("USERKEYWORD") != null ? "user-keyword" : null;
//    //                if (styleClass != null) {
//    //
//    //                    // TODO There is a bug - if matcher find inside exists area
//    //                    // throws exception, for example it find some mathces in big comment area,
//    //                    // at now I add method for non adding if result in exists area
//    //                    if (checkResultInExistsArea(hash, matcher2.start(), matcher2.end())) {
//    //                        hash.put(startMatching + matcher2.start(), startMatching + matcher2.end());
//    //                        hashStyle.put(startMatching + matcher2.start(), styleClass);
//    //                    }
//    //                }
//    //
//    //            }
//    //        }
//
//            for (Entry<Integer, Integer> entry : hash.entrySet()) {
//                
//                int emptyLength = entry.getKey() - lastKwEnd;
//                int keyLength = entry.getValue() - entry.getKey();
//
//                spansBuilder.add(Collections.emptyList(), emptyLength);
//                spansBuilder.add(Collections.singleton(hashStyle.get(entry.getKey())), keyLength);
//                
//                lastKwEnd = entry.getValue();
//            }
//
//            spansBuilder.add(Collections.emptyList(), oldText.length() - lastKwEnd);
//
//            return spansBuilder.create();    
////        }
//        
////        return null;
//    }
//    
//    
//    private static boolean checkCanValidate(CodeArea area, MainController controller) {
//        
//        for (Paragraph<?> p: area.getParagraphs()) {
//            if (p.fullLength() > HIGHLIGHTING_MAX_LINE_LENGTH) {
//                Boolean value = disabledHighlightAreas.get(area);
//                if (value == null || !value) {
//                    disabledHighlightAreas.put(area, true);
//                    controller.showInfo("Notice", "Highlighting was disabled", null);
//                }
//                return false;
//            }
//        }
//        
//        disabledHighlightAreas.put(area, false);
//        
//        return true;
//    }
    // Need check and refactor if need this methods
    private static String replaceQuotes(String text) {

        if (text.contains("'")) {
            text = text.replace("\\'", "##");

            Pattern quotePattern = Pattern.compile("'(.*?)'");
            Matcher matcher = quotePattern.matcher(text);
            while (matcher.find()) {
                String s = matcher.group();
                String replaceStr = "";
                for (int i = 0; i < s.length(); i++) {
                    replaceStr += "#";
                }

                text = text.substring(0, matcher.start()) + replaceStr + text.substring(matcher.end());
                matcher = quotePattern.matcher(text);
            }
        }

        if (text.contains("\"")) {
            text = text.replace("\\\"", "##");

            Pattern quotePattern = Pattern.compile("\"(.*?)\"");
            Matcher matcher = quotePattern.matcher(text);
            while (matcher.find()) {
                String s = matcher.group();
                String replaceStr = "";
                for (int i = 0; i < s.length(); i++) {
                    replaceStr += "#";
                }

                text = text.substring(0, matcher.start()) + replaceStr + text.substring(matcher.end());
                matcher = quotePattern.matcher(text);
            }
        }

        return text;
    }

    private static Map<String, Pair<Integer, Integer>> findQueryPosition(QueryAreaWrapper queryArea, int position) {

        String oldText = queryArea.getText();
        String text = replaceQuotes(queryArea.getText());

        int prevIndex = -1;
        int foundedIndex = -1;
        Matcher m = QUERY_POSITION_PATTERN.matcher(text);
        while (m.find()) {

            if (!m.group().toUpperCase().matches(IGNORE_MATCH_STR)) {
                if (prevIndex == -1 && m.start() >= position) {
                    break;
                }

                if (m.start() >= position) {
                    foundedIndex = prevIndex;
                }

                prevIndex = m.start();

                if (foundedIndex > -1) {
                    String query = text.substring(foundedIndex, prevIndex);
                    if (!query.trim().toLowerCase().matches(UNION_MATCH_STR)) {
                        break;
                    }
                }
            }
        }

        if (foundedIndex == -1) {
            foundedIndex = prevIndex;
        }

        String query = null;

        if (foundedIndex > -1) {

            int begin = foundedIndex;
            int end;
            if (prevIndex != foundedIndex) {
                query = text.substring(foundedIndex, prevIndex);
                end = prevIndex;

            } else {
                query = text.substring(foundedIndex);
                end = text.length();
            }

            int index = query.lastIndexOf(";");
            if (index > -1) {
                end = index + foundedIndex;
            }

            if (index == -1 || index >= position - foundedIndex || queryArea.getLineByPosition(index) == queryArea.getLineByPosition(position)) {
                Map<String, Pair<Integer, Integer>> map = new HashMap<>();
                map.put(oldText.substring(begin, end), new Pair<>(begin, end));

                return map;
            }
        }

        return null;
    }

    private Map<Pair<Integer, Integer>, String> findAllQueries(String text) {

        Map<Pair<Integer, Integer>, String> map = new HashMap<>();

        String oldText = text;
        text = replaceQuotes(text);

        int prevIndex = -1;
        int foundedIndex = -1;
        Matcher m = QUERY_POSITION_PATTERN.matcher(text);
        while (m.find()) {

            if (!m.group().toUpperCase().matches(IGNORE_MATCH_STR)) {

                foundedIndex = prevIndex;
                prevIndex = m.start();

                if (foundedIndex > -1) {
                    String query = text.substring(foundedIndex, prevIndex);

                    if (!query.trim().toLowerCase().matches(UNION_MATCH_STR)) {

                        int begin = foundedIndex;
                        int end;
                        if (prevIndex != foundedIndex) {
                            query = text.substring(foundedIndex, prevIndex);
                            end = prevIndex;

                        } else {
                            query = text.substring(foundedIndex);
                            end = text.length();
                        }

                        int index = query.lastIndexOf(";");
                        if (index > -1) {
                            end = index + foundedIndex;
                        }

                        map.put(new Pair<>(begin, end), oldText.substring(begin, end));
                    }
                }
            }
        }

        if (prevIndex > -1) {
            map.put(new Pair<>(prevIndex, oldText.length()), oldText.substring(prevIndex, oldText.length()));
        }

        return map;
    }

    public static boolean checkResultInExistsArea(TreeMap<Integer, Integer> hash, Integer start, Integer end) {

        if (hash != null && !hash.isEmpty()) {

            for (Integer s : hash.keySet()) {
                if (start >= s && end <= hash.get(s)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Class getDependencyModuleClass() {
        return ConnectionModule.class;
    }

    private void showPreferenceAction(int tab) {
        final Stage dialog = new Stage();
        
        PreferenceController pc = ((MainController)getController()).getTypedBaseController("PreferenceDialog");

        final Parent p = pc.getUI();
        dialog.initStyle(StageStyle.UTILITY);

        pc.selectTab(tab);
        
        final Scene scene = new Scene(p);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getController().getStage());
        dialog.setTitle("Settings / Preferences");
        scene.getStylesheets().addAll(((MainController)getController()).getStage().getScene().getStylesheets());
        dialog.setResizable(false);
        dialog.show();
    }

    private void renameTab() {
        ConnectionSession session = getController().getSelectedConnectionSession().get();
        Tab t = (Tab) ((ReadOnlyObjectProperty) getController().getSelectedConnectionTab(session)).get();
        String s = DialogHelper.showInput(getController(), "Rename Tab", "Enter new name for tab", "", "");
        if (s != null && !s.isEmpty()) {
            t.setText(s);
        }
    }
    
    public List<Map<String, String>> getPersistingMap(String connection, String name) {
        
        for (Map.Entry<ConnectionSession, ArrayList<AbstractMap.SimpleEntry<Tab, QueryAreaWrapper>>> en: sessionsOpenedInterface.entrySet()) {
            
            if (name != null && name.equals(en.getKey().getName()) || en.getKey().getName() == null) {

                Long id = en.getKey().getConnectionParam().getId();
                if (connection.equals(id.toString())) {
                    return getConnectionMap(en.getValue());
                }
            }
        }
        
        return null;
    }
    
    private List<Map<String, String>> getConnectionMap(ArrayList<AbstractMap.SimpleEntry<Tab, QueryAreaWrapper>> tabs) {
        
        List<Map<String, String>> list = new ArrayList<>();

        int i = 0;
        for (AbstractMap.SimpleEntry<Tab, QueryAreaWrapper> o : tabs) {
            Map<String, String> data = new HashMap<>();

            data.put("id", o.getKey().getId());
            data.put("uuid", o.getValue().getUuid());
            data.put("order", String.valueOf(i));
            data.put("rsynaxArea", String.valueOf(o.getValue().isRSyntaxTextArea()));
            data.put("name", o.getKey().getText());
            if (o.getValue() != null) {
                data.put("value", o.getValue().getText());
                data.put("filePath", o.getValue().isReferencingAFile() ? o.getValue().getRelativePath() : "");
            }
            list.add(data);
            
            i++;
        }
        
        return list;
    }
    

    @Override
    public Map<?, ?> getPersistingMap() {
        HashMap<String, Object> cacheImages = new HashMap();
        //for query tab stuff
        for (Map.Entry<ConnectionSession, ArrayList<AbstractMap.SimpleEntry<Tab, QueryAreaWrapper>>> en
                : sessionsOpenedInterface.entrySet()) {
            HashMap<String, Object> queryCacheImages = new HashMap();
            List<Map<String, String>> codeText = new ArrayList();
            queryCacheImages.put(USER_DATA__QUERY_TABS, codeText);
            
            List<Map<String, String>> data = getConnectionMap(en.getValue());
            if (data != null) {
                codeText.addAll(data);
            }

            //last item
            cacheImages.putIfAbsent(String.valueOf(en.getKey().getConnectionParam().getId()), queryCacheImages);
        }

        return cacheImages;
    }

    @Override
    public void onRestore(Map<?, ?> restoreImage, ConnectionSession session, Long connection, Consumer<ConnectionSession> callback) {
        if (restoreImage != null) {
            Map<String, Object> currentSessionImage = (Map<String, Object>) restoreImage;
    //        ConnectionSession session = getMainController().getSelectedConnectionSession().get();
            if (session.getConnectionParam().getSelectedModuleClassName() == null || session.getConnectionParam().getSelectedModuleClassName().equals(ConnectionModule.class.getName())) {

                List<Map<String, String>> tabs = (List<Map<String, String>>) currentSessionImage.get(USER_DATA__QUERY_TABS);
                int i = 0;
                for (Map<String, String> tab: tabs) {

                    if (i == 0) {
                        Tab t = ((ConnectionTabContentController) parentTabs.get(session).getUserData())
                                .getSelectedTab().get();
                        QueryAreaWrapper ca = codeareas.get(t);
                        Platform.runLater(() -> {
                            if (ca != null && ca.getText().isEmpty()) {
                                ca.appendText(tab.get("value"));
                                t.setText(tab.get("name"));
                                parseImageToTabFromString(tab.get("rsynaxArea"), t);
                                ca.setRelativePath(tab.get("filePath"));
                                ca.setUuid(tab.get("uuid"));
                            } else {
                                restoreQueryTabOpener(session, tab.get("uuid"), tab.get("name"), tab.get("value"), tab.get("id"), tab.get("filePath"));
                            }
                        });
                    } else {
                        Platform.runLater(() -> {
                            restoreQueryTabOpener(session, tab.get("uuid"), tab.get("name"), tab.get("value"), tab.get("id"), tab.get("filePath"));
                        });
                    }
                    i++;
                }            
            }
        }
    }

    private void restoreQueryTabOpener(ConnectionSession session, String uuid, String tabName, String context, String typeTab, String relativePath) {
        addNamedTabWithContext(session, uuid, tabName, context, typeTab, relativePath);
    }

    @Override
    public String getInfo() {
        return "";
    }

}
