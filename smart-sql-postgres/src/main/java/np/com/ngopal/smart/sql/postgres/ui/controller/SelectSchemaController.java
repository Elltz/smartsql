package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import java.io.StringReader;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.core.modules.utils.SchemaDesignerUtil;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.ErrDiagram;
import np.com.ngopal.smart.sql.structure.controller.*;
import np.com.ngopal.smart.sql.structure.models.*;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;
import np.com.ngopal.smart.sql.structure.utils.*;
/**
 *
 * @author terah
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SelectSchemaController extends BaseController implements Initializable {

    private static final String DEFAULT = "[default]";
    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private TableColumn<ErrDiagram, String> nameColumn;
    @FXML
    private TableColumn<ErrDiagram, String> domainColumn;
    @FXML
    private TableColumn<ErrDiagram, String> subdomainColumn;
    @FXML
    private TableColumn<ErrDiagram, String> numberOfTablesColumn;
    @FXML
    private TableColumn<ErrDiagram, String> descriptionColumn;
    
    @FXML
    private TableView<ErrDiagram> publicDiagramsTable;

    private Dialog<Void> dialog;

    private Button myApply;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        domainColumn.setCellValueFactory(new PropertyValueFactory<>("domainName"));
        subdomainColumn.setCellValueFactory(new PropertyValueFactory<>("subDomainName"));
        numberOfTablesColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfTables"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        
        publicDiagramsTable.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                myApply.fire();
            }
        });
    }

    @Override
    public Stage getStage() {
        return (Stage) dialog.getOwner();
    }

    public void show(String database, Consumer callback) {                
        select(database, callback);        
        dialog.showAndWait();
    }
    

    private void select(String database, Consumer callback) {
        dialog = new Dialog<>();
        dialog.initStyle(StageStyle.UNIFIED);
        dialog.setTitle("Select schema");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        myApply = ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK));
        myApply.setText("Select schema");
        dialog.getDialogPane().setContent(mainUI);

        myApply.setOnAction((ActionEvent event) -> {
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void run() {
                ErrDiagram ed = publicDiagramsTable.getSelectionModel().getSelectedItem();
                if (ed != null) {
                    Map<String, Object> map = new Gson().fromJson(new StringReader(ed.getDiagramData()), HashMap.class);

                    DesignerErrDiagram d = new DesignerErrDiagram();
                    d.fromMap(map);
                    d.domain().set(ed.getPublicId() != null);
                    d.publicId().set(ed.getPublicId());

                    try {
                        SchemaDesignerUtil.forwardEngeen(
                                dBService,
                                getController(),
                                database,
                                d,
                                null,
                                callback,
                                null
                        );
                    } catch (SQLException ex) {
                        ((MainController)getController()).showError("Error", ex.getMessage(), ex);
                    }
                }
                }
            });
        });

        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setOnAction((ActionEvent event) -> {
            dialog.close();
        });

        initData();
    }
    
    private void initData() {
        publicDiagramsTable.getItems().clear();
        
        try {
            List<ErrDiagram> list = PublicDataAccess.getInstanсe().loadAllErrDiagrams();
            if (list != null) {
                publicDiagramsTable.getItems().addAll(list);
            }
        } catch (PublicDataException ex) {
           ((MainController)getController()).showError("Error searching public schemas", ex.getMessage(), ex, dialog.getOwner());
        }
    }
    
    private DBService dBService;

    public void setDBService(DBService dBService) {
        this.dBService = dBService;
    }
}