/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.controller;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.model.Column;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.models.ExplainResult;
import np.com.ngopal.smart.sql.structure.provider.DefaultImageProvider;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 *
 * @author terah laweh
 * @modify Andrii Tsepushel (a.tsepushel@gmail.com)
 */
@Slf4j
public class SchemaCreationController extends BaseController implements Initializable {

    private static final String TABLE_NAME      = "\\$TABLE_NAME\\$";
    private static final String FIELDS          = "\\$FIELDS\\$";
    private static final String INDEXES         = "\\$INDEXES\\$";
    private static final String DATABASE_NAME   = "\\$DATABASE_NAME\\$";
    private static final String TABLES_LINKS    = "\\$TABLES_LINKS\\$";
    private static final String TABLES_TEMPLATE = "\\$TABLES_TEMPLATE\\$";
                    
    @FXML
    private AnchorPane mainUI;

    @FXML
    private ScrollPane tablesWrapper;

    @FXML
    private ListView<DBTable> tablesContainer;

    @FXML
    private Button createButton;

    @FXML
    private Button selectAllButton;

    @FXML
    private Button deselectAllButton;

    @FXML
    private Button cancelButton;
    @FXML
    private ImageView schemaIcon;

    
    private Stage stage;
    
    private String dBname = "";
    private List<DBTable> myTables;
    
    private ConnectionSession session;
    
    @Override
    public AnchorPane getUI() {
        return mainUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tablesContainer.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        schemaIcon.setImage(SmartImageProvider.getInstance().getTableExport_image());
        tablesContainer.setCellFactory(new Callback() {
            @Override
            public Object call(Object param) {
                return new ListCell<DBTable>() {
                    {
                        //setFont(Font.font(Font.getDefault().getFamily(), 10));
                        selectedProperty().addListener(new InvalidationListener() {
                            @Override
                            public void invalidated(Observable observable) {
                                updateIndex(getIndex());
                            }
                        });
                    }
                    Background blue = new Background(new BackgroundFill(Color.web("#0093ff"),
                            CornerRadii.EMPTY, Insets.EMPTY));

                    @Override
                    protected void updateItem(DBTable item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setBackground(Background.EMPTY);
                            setText("");
                        } else {
                            setText(item.getName());
                        }                        
                    }

                    @Override
                    public void updateIndex(int i) {
                        super.updateIndex(i);
                        if (isSelected()) {
                            setBackground(blue);
                            setTextFill(Color.WHITE);
                        } else {
                            setBackground(Background.EMPTY);
                            setTextFill(Color.BLACK);
                        }
                    }
                };
            }
        });        
        selectAllButton.setOnAction(clicks);
        deselectAllButton.setOnAction(clicks);
        cancelButton.setOnAction(clicks);
        createButton.setOnAction(clicks);
    }

    private final EventHandler<ActionEvent> clicks = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            if (selectAllButton == event.getSource()) {
                tablesContainer.getSelectionModel().selectAll();
            } else if (deselectAllButton == event.getSource()) {
                tablesContainer.getSelectionModel().clearSelection();
            } else if (cancelButton == event.getSource()) {
                stage.close();
            } else if (createButton == event.getSource()) {
                create();
            }
        }
    };

    private void create() {
        List<DBTable> tables = tablesContainer.getSelectionModel().getSelectedItems();
        
        if (tables.isEmpty()) {
            ((MainController)getController()).showInfo("Info", "Please select at least one tablee", null);
        } else {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("HTML Files", "*.htm;*.html"), 
                new FileChooser.ExtensionFilter("All Files", "*.*"));

            File file = fileChooser.showSaveDialog(getStage());
            if (file != null) {
                
                boolean success = false;
                try (FileWriter fw = new FileWriter(file)) {
                    
                    String tableTemplate = streamToString(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/core/modules/tableSchema.template"));
                    String tablesLinks = "";
                    String result = "";
                    for (DBTable table: tables) {
                        
                        String template = tableTemplate.replaceAll(TABLE_NAME, table.getName());
                        String fields = "";
                        String indexes = "";
                        
                        tablesLinks += "<li><a href=\"#" + table.getName() + "\"><p class=\"normal\">" + table.getName() + "</a></li>\n";
                        
                        List<Column> columns = session.getService().getColumnInformation(session.getConnectionParam().getSelectedDatabase().getName(), table.getName());
                        if (columns != null && !columns.isEmpty()) {
                            
                            // fields table
                            fields += "<table width=\"100%\" cellspacing=\"0\" cellapdding=\"2\" border=\"1\">\n";
                            
                            // fields table header
                            fields += 
                                "<tr>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Field</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Type</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Collation</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Null</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Key</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Default</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Extra</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Privileges</td>\n" +
                                "   <td align=\"center\" valign=\"top\" class=\"fieldcolumn\">Comment</td>\n" +
                                "</tr>\n";
                            
                            // add each column as table row
                            for (Column c: columns) {
                                fields += "<tr>\n";
                                
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getName() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getDataType().getName() + (c.getDataType().hasLength() ? "(" + c.getDataType().getLength() + ")" : "") + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getCollation() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + !c.isNotNull() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getKey() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getDefaults() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getExtra() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getPrivileges() + "</td>\n";
                                fields += "<td align=\"left\" valign=\"top\"><p class=\"normal\">" + c.getComment() + "</td>\n";
                                
                                fields += "</tr>\n";
                            }
                            
                            // end fields table
                            fields += "</table>";
                            
                        }
                        
                        try (ResultSet rs = session.getService().getKeys(session.getConnectionParam().getSelectedDatabase().getName(), table.getName())) {
                            if (rs.next()) {
                                rs.beforeFirst();
                                
                                // fields indexes
                                indexes += "<table width=\"100%\" cellspacing=\"0\" cellapdding=\"2\" border=\"1\">\n";
                                
                                indexes += 
                                    "<tr>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Table</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Non<br>unique</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Key<br>name</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Seq<br>in<br>index</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Column<br>name</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Collation</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Cardinality</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Sub<br>part</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Packed</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Null</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Index<br>type</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Comment</td>\n" +
                                    "	<td align=\"left\" valign=\"top\" class=\"fieldcolumn\">Index<br>comment</td>\n" +
                                    "</tr>";
                                
                                while(rs.next()) {
                                    indexes += "<tr>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Table") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getInt("Non_unique") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Key_name") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getInt("Seq_in_index") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Column_name") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Collation") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getInt("Cardinality") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Sub_part") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Packed") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getBoolean("Null") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Index_type") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Comment") + "</td>\n";
                                    indexes += "    <td align=\"left\" valign=\"top\"><p class=\"normal\">" + rs.getString("Index_comment") + "</td>\n";
                                    indexes += "</tr>\n";
                                }
                                
                                indexes += "</table>\n";
                            }
                        }
                        
                        template = template.replaceAll(FIELDS, fields);
                        template = template.replaceAll(INDEXES, indexes);
                        
                        result += template;
                    }
                    
                    String databaseTemplate = streamToString(getClass().getResourceAsStream("/np/com/ngopal/smart/sql/core/modules/databaseSchema.template"));
                    databaseTemplate = databaseTemplate.replaceAll(DATABASE_NAME, session.getConnectionParam().getSelectedDatabase().getName());
                    databaseTemplate = databaseTemplate.replaceAll(TABLES_LINKS, tablesLinks);
                    databaseTemplate = databaseTemplate.replaceAll(TABLES_TEMPLATE, result);
                    
                    fw.append(databaseTemplate);
                    fw.flush();
                    
                    success = true;
                } catch (Throwable ex) {
                    ((MainController)getController()).showError("Error", "Error generating HTML schema", ex);
                }
                
                if (success) {
                    ((MainController)getController()).showInfo("Info", "Schema generation successfully", null);                    
                    openInBrowser(file.toURI());
                    
                    stage.close();
                }
            }
        }
    }
    

    public void showNow(Window owner) {

        if (stage == null) {
            stage = new Stage(StageStyle.UTILITY);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setResizable(true);
            stage.setScene(new Scene(mainUI, 350, 310));
            stage.setTitle("Create Schema For - '" + dBname + "'");
            stage.setMinHeight(300);
            stage.setMinWidth(340);
            stage.setResizable(false);
        }
        if (owner != null) {
            stage.initOwner(owner);
        }
        tablesContainer.getSelectionModel().selectAll();
        stage.showAndWait();
    }

    public void insertSession(ConnectionSession session) {
        if (session == null) {
            return;
        }
        this.session = session;

        try {
            Database d = session.getConnectionParam().getSelectedDatabase();
            dBname = d.getName();
            myTables = session.getService().getTables(d);
        } catch (SQLException ex) {
            ((MainController)getController()).showError("Error", "Error inserting session", ex);
        }
        if (myTables != null) {
            tablesContainer.getItems().setAll(myTables);
        }
    }
    
    public String streamToString(final InputStream inputStream) throws Exception {
        try (
            final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))
        ) {
            return br.lines().parallel().collect(Collectors.joining("\n"));
        } catch (final IOException e) {
            ((MainController)getController()).showError("Error", "Error streaming to string", e);
        }
        
        return null;
    }
    
    
    public void openInBrowser(URI uri) {
        try {
            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;

            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(uri);
            }
        }
        catch (IOException e){
            ((MainController)getController()).showError("Error", "Error opening in browser", e);
        }
    }
}
