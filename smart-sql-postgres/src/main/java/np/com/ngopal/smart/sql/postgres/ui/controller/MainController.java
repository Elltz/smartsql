package np.com.ngopal.smart.sql.ui.controller;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.sun.javafx.PlatformUtil;
import com.sun.javafx.event.EventDispatchChainImpl;
import com.sun.javafx.scene.control.behavior.TabPaneBehavior;
import com.sun.javafx.scene.control.skin.TabPaneSkin;
import com.sun.javafx.util.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Side;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.HookKey;
import np.com.ngopal.smart.sql.Hookable;
import np.com.ngopal.smart.sql.ModuleCallback;
import np.com.ngopal.smart.sql.core.modules.AbstractStandardModule;
import np.com.ngopal.smart.sql.core.modules.ConnectionModule;
import np.com.ngopal.smart.sql.core.modules.FrequencyExecutedQueriesModule;
import np.com.ngopal.smart.sql.core.modules.HistoryTabModule;
import np.com.ngopal.smart.sql.core.modules.QueryTabModule;
import np.com.ngopal.smart.sql.core.modules.utils.Flags;
import np.com.ngopal.smart.sql.core.modules.utils.PersistenceAppSaver;
import np.com.ngopal.smart.sql.db.AbstractDBService;
import np.com.ngopal.smart.sql.db.ColumnCardinalityService;
import np.com.ngopal.smart.sql.db.ConnectionParamService;
import np.com.ngopal.smart.sql.structure.provider.DBDebuggerProfilerService;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.db.DeadlockService;
import np.com.ngopal.smart.sql.db.FavouritesService;
import np.com.ngopal.smart.sql.db.HistoryQueryService;
import np.com.ngopal.smart.sql.db.HistoryTemplateService;
import np.com.ngopal.smart.sql.db.LicenseDataService;
import np.com.ngopal.smart.sql.db.LicenseDataUsageService;
import np.com.ngopal.smart.sql.db.MysqlDBService;
import np.com.ngopal.smart.sql.db.PreferenceDataService;
import np.com.ngopal.smart.sql.db.ProfilerChartService;
import np.com.ngopal.smart.sql.db.ProfilerSettingsService;
import np.com.ngopal.smart.sql.db.QAPerformanceTuningService;
import np.com.ngopal.smart.sql.db.QARecommendationService;
import np.com.ngopal.smart.sql.db.SessionsSavePointService;
import np.com.ngopal.smart.sql.db.UserUsageStatisticService;
import np.com.ngopal.smart.sql.model.ConnectionParams;
import np.com.ngopal.smart.sql.model.DBTable;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.PUsageFeatures;
import np.com.ngopal.smart.sql.model.provider.ProfilerDataManager;
import np.com.ngopal.smart.sql.model.ui.MenuCategory;
import np.com.ngopal.smart.sql.modules.SSHModule;
import np.com.ngopal.smart.sql.modules.SchemaDesignerModule;
import np.com.ngopal.smart.sql.modules.SmartProfilerModule;
import np.com.ngopal.smart.sql.structure.controller.BaseController;
import np.com.ngopal.smart.sql.structure.controller.CheckForUpdatesController;
import np.com.ngopal.smart.sql.structure.controller.LicenseController;
import np.com.ngopal.smart.sql.structure.controller.ModuleController;
import np.com.ngopal.smart.sql.structure.controller.TabContentController;
import np.com.ngopal.smart.sql.structure.controller.UIController;
import np.com.ngopal.smart.sql.structure.dialogs.DialogResponce;
import np.com.ngopal.smart.sql.structure.executable.CommunicationProtocol;
import np.com.ngopal.smart.sql.structure.utils.DialogHelper;
import np.com.ngopal.smart.sql.structure.utils.DisconnectPermission;
import np.com.ngopal.smart.sql.structure.utils.FunctionHelper;
import np.com.ngopal.smart.sql.structure.utils.Recycler;
import np.com.ngopal.smart.sql.structure.utils.WorkProc;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.components.DynamicMenuItem;
import np.com.ngopal.smart.sql.ui.components.IconedMenuItem;
import np.com.ngopal.smart.sql.ui.components.SqlBrowserTab;
import np.com.ngopal.smart.sql.ui.components.TextBanner;
import np.com.ngopal.smart.sql.ui.components.TextMenu;
import np.com.ngopal.smart.sql.ui.components.TextMenuItem;
import np.com.ngopal.smart.sql.ui.components.ToolbarDynamicMenu;
import np.com.ngopal.smart.sql.ui.provider.*;
import org.reflections.Reflections;
import np.com.ngopal.smart.sql.structure.executable.StandardApplicationManager;
import np.com.ngopal.smart.sql.structure.modules.*;
import np.com.ngopal.smart.sql.db.PublicDataAccess;
import np.com.ngopal.smart.sql.db.PublicDataException;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.*;
import np.com.ngopal.smart.sql.structure.queryutils.*;
import static np.com.ngopal.smart.sql.ui.MainUI.controller;

/**
 * FXML Controller class for the main user interface of the application. It has
 * control over all the modules and different UI components and containers.
 * <p>
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
@Slf4j
public class MainController implements Initializable, UIController {

    private int connectionId;    
    private final String AUTOIMPORT_FILE = "autoimport";
    private final String YOG_LOCATION = "C:\\Users\\%s\\AppData\\Roaming\\SQLyog\\sqlyog.ini";

    @Inject
    @Getter
    private Injector injector;

    @Inject
    @Getter
    private PreferenceDataService preferenceService;

    @Inject
    @Getter
    ConnectionParamService db;

    @Inject
    @Getter
    UserUsageStatisticService usageService;
    
    @Inject
    @Getter
    ColumnCardinalityService columnCardinalityService;

    @Inject
    @Getter
    private QAPerformanceTuningService performanceTuningService;

    @Inject
    @Getter
    private QARecommendationService recommendationService;
    
    @Inject
    @Getter
    private ProfilerChartService chartService; 
    
    @Inject
    @Getter
    private LicenseDataService licenseDataService; 
    
    @Inject
    @Getter
    private LicenseDataUsageService licenseDataUsageService;
      
    @Inject
    @Getter
    private ProfilerDataManager profilerService;
    
    @Inject
    @Getter
    private ProfilerDataManager secondProfilerService;
    
    @Inject
    @Getter
    private DeadlockService deadlockService;
    
    @Inject
    @Getter
    private ProfilerSettingsService settingsService;
    
    @Inject
    @Getter
    private DBDebuggerProfilerService debuggerProfilerService;
    
    private final BooleanProperty forceBusy = new SimpleBooleanProperty(false);

    @Getter
    private volatile List<ModuleController> modules;

    private List<ModuleCallback> postConnectionHooks;

    private List<Callback> changeConnectionHooks;

    private ExportTableController exportTableController;
    
    private ExportTableAsXlsController exportTableAsXlsController;
    
    private ExportTableAsPdfController exportTableAsPdfController;

    private CopyResultController copyResultController;
    
    private ResultTabController resultTabController;

    @Inject
    @Getter()
    private SmartBaseControllerProvider provider;

    @Inject
    private SessionsSavePointService ssps;
    
    @Getter
    private ConnectionSession lastOpenedConnectionSession;
    
    @FXML
    private AnchorPane mainPane;

    @FXML
    private SplitPane splitPane;

    @FXML
    private TabPane tabPane;

    @FXML
    private MenuBar menuBar;

    @FXML
    private StackPane UiHolder;

    @FXML
    private ToolBar additionalButtonContainer;
    
    @FXML
    private ToolBar rightAdditionalButtonContainer;

    @FXML
    private HBox toolbarButtonsContainer;

    @FXML
    private HBox toolBarContainer;

    @FXML
    private Toggle emptyTab;

    private ToggleGroup toolBarButtonGroup;

    @FXML
    private AnchorPane toolbarParentContainer;

    @FXML
    private HBox footer;

    @FXML
    private AnchorPane updateFrontLine;

    /**
     * Format TOTAL SEC: 00
     */
    @FXML
    private Label secondsCounter;

    /**
     * Format LIN: 00, COL: 00
     */
    @FXML
    private Label lineColumLabel;

    @FXML
    private Label queriesCounter;
    
    @FXML
    private Label rowsCounter;

    /**
     * FOrmat CONNECTIONS: 00
     */
    @FXML
    private Label connectionsLabel;

    @FXML
    private HBox notificationBox;
    
    @FXML
    private HBox sessionBox;

    @FXML
    private Button showHideButton;

    @FXML
    private TextBanner textBanner;

    @FXML
    private ImageView companyImage;

    @FXML
    private VBox aboutContainer;

    @FXML
    private VBox appCenterPane;
    
    @FXML
    private ImageView newConnectionImageView;
    @FXML
    private Tab mainNew;

    private SimpleBooleanProperty currentLeftPaneVisibilityProperty = new SimpleBooleanProperty(true);
    SimpleObjectProperty<Boolean> currentSessionSavePoint = new SimpleObjectProperty(false);

    @Getter
    @Setter
    private List<String> customKeywords;

    private LoadingScreenController loaderScreen;

//    private final Object lock = new Object();
    private SimpleObjectProperty<ConnectionSession> selectedSession = new SimpleObjectProperty<ConnectionSession>() {
        @Override
        protected void invalidated() {
            super.invalidated();
            ConnectionSession cs = get();
            if (cs != null) {
                if (!isApplicationRunningModulationRestoration()) {
                    ConnectionParams cp = cs.getConnectionParam();
                    if (cp != null) {
                        RsSyntaxCompletionHelper.getInstance().setCompletions(cp, cp.getSelectedDatabase() != null ? cp.getSelectedDatabase().getName() : null);
                        return;
                    }
                    // this will clean completions from provider
                    RsSyntaxCompletionHelper.getInstance().setCompletions(null, null);
                }
            }
        }

    };

    private final Map<Integer, ConnectionSession> connectionSessions = new LinkedHashMap<>();

    public Map<Integer, ConnectionSession> getSessionsMap() {
        return connectionSessions;
    }

    private List<Consumer<MenuCategory>> categoryListeners = new ArrayList<>();

    private final Insets padding = new Insets(0, 2, 0, 2);

    private Map<String, Map<MenuCategory, List<Node>>> menus = new HashMap<>();

    private final EventHandler<Event> returnToDefaultTopMenuHandler = (Event event) -> {
        if (toolBarContainer.getChildren().size() > 0) {
            toolBarButtonGroup.selectToggle(emptyTab);
        }
    };

    @Getter
    public ComboBox<Database> databaseComboBox = null;

    ContextMenu newConnectionContexMenu = new ContextMenu();

    public final String os_name;
    @Getter
    private PersistenceAppSaver persistAppSaver;
    private volatile int activeTabControllerWorkProcCounter;
    private SmartPropertiesData smartPropertiesData;

    public MainController() {
        postConnectionHooks = new ArrayList<>();
        changeConnectionHooks = new ArrayList<>();
        os_name = System.getProperty("os.name").toLowerCase();
        toolBarButtonGroup = new ToggleGroup();
        log.debug("Main controller created");

        /**
         * i have stoped using this because of disabled sessions, but nothing
         * has been changed when uncommented it will work
         *
         * SessionContainerController.whosSelected.addListener( new
         * ChangeListener<SessionContainerController>() {
         *
         * @Override public void
         * changed(ObservableValue<? extends SessionContainerController>
         * observable, SessionContainerController oldValue,
         * SessionContainerController newValue) { while
         * (getSelectedConnectionSession().get() != null) {
         * disconnect(UIController.DisconnectPermission.NONE); }
         * currentSessionSavePoint.set((newValue == null) ? null :
         * newValue.getSessionsSavePoint()); } });
         *
         * currentSessionSavePoint.addListener(new ChangeListener<SessionKey>()
         * {
         * @Override public void changed(ObservableValue<? extends SessionKey>
         * observable, SessionKey oldValue, SessionKey newValue) {
         * persistAppSaver.setCurrentSession(newValue); } });
         */
    }

    public BooleanProperty forceBusy() {
        return forceBusy;
    }

    public void forceBusyChanged(Boolean newValue) {
        forceBusy.set(newValue);
    }

    private void processMenuItemsForReturningToDefaultTab(List<MenuItem> items) {
        if (items != null) {
            for (MenuItem mi : items) {
                if (mi instanceof Menu) {
                    processMenuItemsForReturningToDefaultTab(((Menu) mi).getItems());
                } else {
                    mi.addEventHandler(EventType.ROOT, returnToDefaultTopMenuHandler);
                }
            }
        }
    }

    public void clearMenuItems(Class dependence, MenuCategory category) {
        Map<MenuCategory, List<Node>> categories = menus.get(dependence != null ? dependence.getName() : null);
        if (categories != null && categories.containsKey(category)) {

            List<Node> nodes = categories.get(category);
            if (nodes != null) {
                nodes.clear();
            }
        }
    }

    //This is for menuButton animation related code
    private ScaleTransition scaleTrans;

    public synchronized void addMenuItems(Class dependence, MenuCategory category, MenuItem[] items) {

        // No need make this in BACKGROUND - its instalation of Module, so we must wait while doing this all
        // Also we have some dynamic items update and it must be done in current thread
        Map<MenuCategory, List<Node>> categories = menus.get(dependence != null ? dependence.getName() : (String) null);
        if (categories == null) {
            categories = new HashMap<>();
            menus.put(dependence != null ? dependence.getName() : null, categories);
        }

        if (!categories.containsKey(category)) {
            categories.put(category, new ArrayList<>());
        }

        processMenuItemsForReturningToDefaultTab(Arrays.asList(items));
        for (MenuItem mi : items) {
            Labeled node;
            if (mi instanceof Menu) {
                node = new MenuButton();
                node.setMaxWidth(Label.USE_COMPUTED_SIZE);
                if (category != null) {
                    node.textProperty().bind(mi.textProperty());
                }

                node.setId("tollbar-menu-button");
                if (mi instanceof ToolbarDynamicMenu) {
                    ((MenuButton) node).showingProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                        if (newValue != null && newValue) {
                            ((ToolbarDynamicMenu) mi).onShowing((MenuButton) node, returnToDefaultTopMenuHandler);
                        }
                    });
                }else{
                    ((MenuButton)node).showingProperty().addListener(new ChangeListener<Boolean>() {
                        @Override
                        public void changed(ObservableValue<? extends Boolean> observable,
                                Boolean oldValue, Boolean newValue) {
                            EventHandler<Event> evh = null;
                            if(newValue){ evh = ((Menu)mi).getOnShowing(); }
                            else{ evh = ((Menu)mi).getOnHiding(); }
                            if(evh != null){ evh.handle(null); }
                        }
                    });
                }

                Menu subMenu = (Menu) mi;
                ((MenuButton) node).getItems().addAll(subMenu.getItems());
                if (mi.getText().equals("Connection")) {
                    newConnectionContexMenu.getItems().addAll(subMenu.getItems());
                }

            } else if (mi instanceof CustomMenuItem) {
                CustomMenuItem cmi = (CustomMenuItem) mi;
                databaseComboBox = (ComboBox) cmi.getContent();
                databaseComboBox.setMaxWidth(180);
                databaseComboBox.disableProperty().bind(cmi.disableProperty());
                categories.get(category).add(databaseComboBox);
                continue;
            } else if (mi instanceof DynamicMenuItem) {
                categories.get(category).add(((DynamicMenuItem) mi).createNode());
                continue;
            } else {

                node = new Button();
                node.setId("tollbarlabel");
                node.setWrapText(true);
                //node.setMinHeight(30);
                node.setPrefHeight(15);
                node.setMaxHeight(25);
                node.setUserData(mi.getAccelerator());
                if (!(mi instanceof IconedMenuItem)) {
                    node.textProperty().bind(mi.textProperty());
                }

                node.setOnMouseClicked((MouseEvent event) -> {
                    if (mi.getOnAction() != null) {
                        mi.fire();
                        // TODO Not sure that this need
                        // If this still need that need work on 
                        // minimum width items, because if window
                        // is small - it shwoing with ...
                        // at now they wrapped with Group node
                        // but this node didnt work with animation
//                        if (category != MenuCategory.RIGHT_MENU) {
//                            PreferenceData pd = preferenceService.getPreference();
//                            if (pd.isQbEnableAnimation()) {
//                                if (scaleTrans == null) {
//                                    scaleTrans = new ScaleTransition(Duration.millis(200));
//                                    scaleTrans.setAutoReverse(true);
//                                    scaleTrans.setCycleCount(2);
//                                }
//                                scaleTrans.setByX(pd.isQbFlipAnimation() ? -1.2 : 0.5f);
//                                node.setScaleX(1.0);//Always reset to default - Sir Andry detected bug :)
//                                scaleTrans.setOnFinished((ActionEvent event1) -> {
//                                    Platform.runLater(() -> {
//                                        mi.fire();
//                                    });
//                                });
//                                scaleTrans.setNode(node);
//                                scaleTrans.playFromStart();
//                            } else {
//                                mi.fire();
//                            }
//                        } else {
//                            mi.fire();
//                        }
                    }
                });
            }
            //sir Sudheer requirement - 25/03/2018
            node.setContentDisplay(ContentDisplay.LEFT);
            node.setGraphicTextGap(5);
            node.setMaxWidth(Double.MAX_VALUE);

            node.disableProperty().bind(mi.disableProperty());
            node.visibleProperty().bind(mi.visibleProperty());
            node.setFocusTraversable(false);

            mi.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (mi.getUserData() == null) {
                    String toolTip = newValue != null ? newValue : "";
                    if (mi.getAccelerator() != null) {
                        toolTip += " (" + mi.getAccelerator().getDisplayText() + ")";
                    }
                    Tooltip tt = new Tooltip(toolTip);
                    node.setTooltip(tt);
                }
            });

            String toolTip = mi.getUserData() != null ? mi.getUserData().toString() : mi.getText();
            if (mi.getAccelerator() != null) {
                toolTip += " (" + mi.getAccelerator().getDisplayText() + ")";
            }
            Tooltip tt = new Tooltip(toolTip);
            node.setTooltip(tt);

            hackTooltipStartTiming(node.getTooltip());

            node.setCursor(Cursor.HAND);

            if ((mi instanceof TextMenuItem) || (mi instanceof TextMenu)) {
                node.setContentDisplay(ContentDisplay.RIGHT);
            }

            if (mi.getGraphic() != null) {

                ImageView im0 = (ImageView) mi.getGraphic();
                ImageView nodeGraphic = new ImageView(im0.getImage());
                nodeGraphic.setFitHeight(im0.getFitHeight());
                nodeGraphic.setFitWidth(im0.getFitWidth());
                node.setGraphic(nodeGraphic);

                mi.graphicProperty().addListener((ObservableValue<? extends Node> observable, Node oldValue, Node newValue) -> {
                    if (newValue instanceof ImageView) {
                        ImageView imageview = new ImageView(((ImageView) newValue).getImage());
                        imageview.setFitHeight(((ImageView) newValue).getFitHeight());
                        imageview.setFitWidth(((ImageView) newValue).getFitWidth());
                        node.setGraphic(imageview);
                    }
                });

                //it ends here
                if (category == null) {
                    node.textProperty().unbind();
                    node.setText("");
                }
            }

            node.setWrapText(false);
            if (mi.getStyleClass().contains("time") || (mi instanceof TextMenu)) {
                node.setMaxSize(Double.MAX_VALUE, 30);
                node.setGraphicTextGap(0);
            }

            node.setTextAlignment(TextAlignment.CENTER);
            node.setPadding(new Insets(0, 5, 0, 5));

            HBox.setMargin(node, padding);
            categories.get(category).add(new Group(node));
        }
    }

    private void hackTooltipStartTiming(Tooltip tooltip) {
        try {
            Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
            fieldBehavior.setAccessible(true);
            Object objBehavior = fieldBehavior.get(tooltip);

            Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
            fieldTimer.setAccessible(true);
            Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

            objTimer.getKeyFrames().clear();
            objTimer.getKeyFrames().add(new KeyFrame(new Duration(10)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public Tab getCurrentConnectionParentTab() {
        return tabPane.getSelectionModel().getSelectedItem();
    }

    public void addTab(Tab tab, ConnectionSession session) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            tabController.addTab(tab);
        }

    }

    public void selectTab(int index) {
        int size = tabPane.getTabs().size();
        if (index < size - 1) {
            tabPane.getSelectionModel().select(index);
        }
    }

    public void selectLastTab() {
        int size = tabPane.getTabs().size();
        if (size > 1) {
            tabPane.getSelectionModel().select(size - 2);
        }
    }

    public void nextTab() {
        int size = tabPane.getTabs().size();
        if (size > 2) {
            int index = tabPane.getSelectionModel().getSelectedIndex();
            if (index == size - 2) {
                index = 0;
            } else {
                index++;
            }

            tabPane.getSelectionModel().select(index);
        }
    }

    public void prevTab() {
        int size = tabPane.getTabs().size();
        if (size > 2) {
            int index = tabPane.getSelectionModel().getSelectedIndex();
            if (index == 0) {
                index = size - 2;
            } else {
                index--;
            }

            tabPane.getSelectionModel().select(index);
        }
    }

    public ReadOnlyObjectProperty<Tab> getSelectedConnectionTab(ConnectionSession session) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            return tabController.getSelectedTab();
        }

        return null;
    }

    public void showTab(Tab tab, ConnectionSession session) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            tabController.showTab(tab);
        }
    }

    public Tab getTab(String id, String text, ConnectionSession session) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            return tabController.getTab(id, text);
        }

        return null;
    }

    public void addTabToBottomPane(ConnectionSession session, Tab... tab) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            if (tabController.getBottamTabs() != null) {
                tabController.addTabToBottomPane(tab);
            }
        }
    }

    public void removeTabFromBottomPane(ConnectionSession session, Tab... tab) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            if (tabController.getBottamTabs() != null) {
                tabController.removeTabFromBottomPane(tab);
            }
        }
    }

    /**
     * Creates new Tab and open the connections
     * <p>
     * @param dbService
     * @param param
     * @param runOnDiffThread
     */
    public void openConnection(DBService dbService, ConnectionParams cp, boolean runOnDiffThread, Consumer<ConnectionSession> connectionOpenedCallback) {

        connectionId++;
        int staticConnection = connectionId;

        log.info("BEFORE " + staticConnection + " - " + cp.getName());

        WorkProc openConnectionProcedure = new WorkProc() {

            final int wpConnectionId = staticConnection;
            ConnectionSession session = null;

            Tab tab;
            Pane p;
            final BorderPane pane = new BorderPane();
            volatile boolean news = true;
            ModuleController selectedModuleController;
            SQLException ex = null;

            WorkProc<Boolean> tabControllerConnectionOpener;

            @Override
            public void updateUI() {
                if (ex != null) {
                    showError("Error", ex.getMessage(), ex);
                    return;
                }

                tabPane.toFront();
                tabPane.getSelectionModel().select(tab);

                if (tabControllerConnectionOpener != null) {
                    tabControllerConnectionOpener.setAdditionalData(Boolean.TRUE);
                    activeTabControllerWorkProcCounter++;
                    addWorkToBackground(tabControllerConnectionOpener);
                }
            }

            @Override
            public void run() {

                ConnectionParams param = cp.copyOnlyParams();
                param.setId(cp.getId());

                callUpdate = true;
                selectedModuleController = getModuleByClassName(param.getSelectedModuleClassName());

                // if we didnt select any module
                // need get ConnectionModule by default
                if (selectedModuleController == null) {
                    selectedModuleController = getModuleByClassName(ConnectionModule.class.getName());
                }

                if (selectedModuleController != null && !selectedModuleController.canOpenConnection(param) && !(selectedModuleController instanceof ConnectionModule)) {
                    Platform.runLater(() -> {
                        showInfo("Module cannot be opened", "Module '" + selectedModuleController.getClass().getSimpleName() + "' can be opened only one time", null);
                    });
                    return;
                }

                TabContentController tabController;

                if (((ConnectionModuleInterface) selectedModuleController).isNeedOpenInNewTab()) {

                    session = new ConnectionSession();
                    session.setId(wpConnectionId);
                    session.setConnectionParam(param);
                    session.setService(dbService);

                    for (ConnectionSession cs : connectionSessions.values()) {
                        if (cs.isOpen() && cs.getConnectionParam().getId().equals(param.getId())) {
                            session.setOpen(false);
                            break;
                        }
                    }

                    tab = selectedModuleController == null || selectedModuleController instanceof ConnectionModule
                            ? new SqlBrowserTab(session) : new Tab() {
                        @Override
                        public boolean equals(Object tab) {
                            Label tabLabel = (Label) getGraphic();
                            return tab instanceof Tab && tabLabel != null && tabLabel.getText() != null
                                    && ((Tab) tab).getGraphic() instanceof Label && tabLabel.getText().equals(
                                            ((Label) ((Tab) tab).getGraphic()).getText());
                            // return tab instanceof Tab && getText() != null && getText().equals(((Tab) tab).getText());
                        }

                        @Override
                        public int hashCode() {
                            int hash = 7;
                            return hash;
                        }
                    };

                    tabController = getTypedTabContentBaseController(selectedModuleController, session);
                    p = (Pane) tabController.getUI();

                    tab.setClosable(true);
//                    tab.setOnCloseRequest((Event event) -> {
//                        disconnect(UIController.DisconnectPermission.NONE);
//                    });
                    tab.setGraphic(new Label(tabController.getTabName()));
                    news = true;
                    // change on close
                    tab.setOnCloseRequest((Event event) -> {
                        disconnect(selectedSession.get(), DisconnectPermission.NONE);
                        if (tabControllerConnectionOpener.getAddData()) {
                            tabControllerConnectionOpener.setAdditionalData(Boolean.FALSE);
                        }
                        checkIfNoConnectionTabs();
                    });

                    //if (tabController.isNeedOpenInNewTab()) {
                    ((Label) tab.getGraphic()).setText(tabController.getTabName()
                            + " (" + wpConnectionId + ")");
                    //}
                    tab.setId("tab" + wpConnectionId);
                    tab.setUserData(tabController);

                    pane.setCenter(p);
                    p.prefWidthProperty().bind(pane.widthProperty());
                    p.prefHeightProperty().bind(pane.heightProperty());
                    pane.setStyle("-fx-padding: 2 0 0 0; -fx-background-color: white; -fx-border-width: 0px;");
                    tab.setContent(pane);
                    tabController.makeBusy(true, 15000, "Collecting Objects details from Metadata. It is a one-time job. Usually, it is a little bit slow if there are many tables.");

                    connectionSessions.put(wpConnectionId, session);
                    lastOpenedConnectionSession = session;

                    Platform.runLater(() -> {
                        tabPane.getTabs().add(tabPane.getTabs().size() - 1, tab);
                        synchronized (this) {
                            this.notify();
                        }
                    });

                    synchronized (this) {
                        try {
                            this.wait();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    manualSelectConnectionSession(session);

                    executePostConnectionHook(tabController, session, tab, selectedModuleController);
                    if (connectionOpenedCallback != null) {
                        connectionOpenedCallback.accept(session);
                    }

                } else {
                    //we init old tab and play with it
                    news = false;
                    int lastSessionId = lastOpenedConnectionSession.getId();
                    String tableId = "tab" + lastSessionId;
                    for (Tab t : tabPane.getTabs()) {
                        if (tableId.equals(t.getId())) {
                            tab = t;
                            break;
                        }
                    }
                    tabController = (TabContentController) tab.getUserData();
                    session = connectionSessions.get(lastSessionId);

                    manualSelectConnectionSession(session);

                    // todo need refactor this
                    if (tabController instanceof SSHTabContentController) {
                        ((SSHTabContentController) tabController).addTabWithParams(param);
                    }
                }

                if (!textBanner.isInited()) {
//                    try {
                    log.error("Banner text server: " + getSmartData().getBannerText());
                    if (getSmartData().getBannerText() != null && !getSmartData().getBannerText().isEmpty()) {
                        textBanner.init(getSmartData().getBannerText());
                        textBanner.start(300000);
                    }
//                        else if (isH2ForData){
//                            // just for DEV, then need remove
//                            log.error("Banned text local: " + Resources.toString(Resources.getResource("SmartSQL_banner"), Charsets.UTF_8));
//                            textBanner.init(Resources.toString(Resources.getResource("SmartSQL_banner"), Charsets.UTF_8));
//                            textBanner.start(3000);
//                        }
//                    } catch (IOException ex) {
//                        log.error("Error", ex);
//                    }
                }

//                QA demo link <a href="http://www.smartmysql.com">http://www.smartmysql.com/</a>
//                <!------!>
//                QA <span style="color: red">demo</span> message
                tabControllerConnectionOpener = new WorkProc<Boolean>() {

                    Exception error;

                    @Override
                    public void updateUI() {
                        if (error != null) {
                            //currently i use the run on diff thread to detect whether it is
                            //a session restore call or not
//                                if (runOnDiffThread) {

                            showError("Error", error.getMessage(), error);

                            TabPaneBehavior behavior = ((TabPaneSkin) tabPane.getSkin()).getBehavior();
                            behavior.closeTab(tab);

                            connectionSessions.remove(session.getId());
                            ((ConnectionModuleInterface) selectedModuleController).connectionFailedOpening(error.getMessage());

                            checkIfNoConnectionTabs();
                        }

                        ((MysqlDBService) dbService).setServiceBusy(false);
                        getUI().setCursor(Cursor.DEFAULT);
                        setAdditionalData(Boolean.FALSE);

                        /**
                         * this is very important.
                         *
                         * The logic here is app restoration runs in multi
                         * threaded state, hence the
                         * isApplicationRunningModulationRestoration() from the
                         * controller tells us whether its in restoration and is
                         * still being processed individually by the parent
                         * module.
                         *
                         * That is for example if we have 3 connections and
                         * these connections are still being processed,
                         * isApplicationRunningModulationRestoration() will be
                         * true.
                         *
                         * when isApplicationRunningModulationRestoration() is
                         * true, Flags.restorationTime need be false if it
                         * false, then Flag.restorationTime should be true, here
                         * since this is the last code point of the restoration.
                         */
                        activeTabControllerWorkProcCounter--;
                        if (!isApplicationRunningModulationRestoration() && activeTabControllerWorkProcCounter <= 0) {
                            log.info("technically restoration should be done ");
                            Flags.changeRestorationState(false);
                        }
                    }

                    @Override
                    public void run() {
                        callUpdate = true;

                        if (getAddData()) {
                            try {
                                //log.error("000000000000000000000000000");
                                tabController.openConnection(param);
                                //log.error("111111111111111111111111111");
                                ((ConnectionModuleInterface) selectedModuleController).connectionOpened(param);
                                //log.error("222222222222222222222222222");
                                if (param.isNeedReinit()) {
                                    //log.error("33333333333333333333333");
                                    selectedModuleController.reinitConnection(param);
                                    //log.error("44444444444444444444444");
                                }
                                tabController.makeBusy(false);
                            } catch (SQLException exx) {
                                error = exx;
                            }
                        }
                    }
                };

                /////
                log.info(session.getId() + " - " + session.getConnectionParam().getName());
            }
        };

        if (runOnDiffThread) {
            addWorkToBackground(openConnectionProcedure);
        } else {
            openConnectionProcedure.run();
            if (openConnectionProcedure.callUpdate) {
                Platform.runLater(() -> {
                    openConnectionProcedure.updateUI();
                });
            }
        }

    }

    private void checkIfNoConnectionTabs() {
        // Its only "+" tab for adding new - then we show file UI
        if (tabPane.getTabs().size() == 1) {
            //TODO need do something if no connection selected
        }
    }

    public ModuleController getModuleByClassName(String className) {
        if (className == null) {
            return null;
        }
        for (ModuleController module : modules) {
            if (module.getClass().getName().equals(className)) {
                return module;
            }
        }
        return null;
    }

    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem(int sessionId) {

        for (Tab t : tabPane.getTabs()) {
            if (t.getId().equals("tab" + sessionId)) {
                TabContentController controller = (TabContentController) t.getUserData();
                return controller.getSelectedTreeItem();
            }
        }
        return new SimpleObjectProperty();
    }

    public ReadOnlyObjectProperty<ConnectionSession> getSelectedConnectionSession() {
        return selectedSession;
    }

    public void refreshTree(int sessionId, boolean alertDatabaseCount) {
        for (Tab t : tabPane.getTabs()) {
            if (t.getId().equals("tab" + sessionId)) {
                TabContentController controller = (TabContentController) t.getUserData();
                controller.refreshTree(alertDatabaseCount);
                break;
            }
        }
    }

    public void performShowLeftPane() {
        for (Tab t : tabPane.getTabs()) {
            if (t.getId().equals("tab" + selectedSession.get().getId())) {
                TabContentController controller = (TabContentController) t.getUserData();
                controller.showLeftPane();
                break;
            }
        }
    }

    public void performHideLeftPane() {
        for (Tab t : tabPane.getTabs()) {
            if (t.getId().equals("tab" + selectedSession.get().getId())) {
                TabContentController controller = (TabContentController) t.getUserData();
                controller.hideLeftPane();
                break;
            }
        }
    }

    public void toggleLeftPane() {
        for (Tab t : tabPane.getTabs()) {
            if (t.getId().equals("tab" + selectedSession.get().getId())) {
                TabContentController controller = (TabContentController) t.getUserData();
                if (controller != null && controller.getLeftPaneVisibiltyProperty() != null && controller.getLeftPaneVisibiltyProperty().getValue() != null) {
                    if (controller.getLeftPaneVisibiltyProperty().get()) {
                        controller.hideLeftPane();
                    } else {
                        controller.showLeftPane();
                    }
                }
                break;
            }
        }
    }

    public void hideResultPane(ConnectionSession session, boolean visibility) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            if (tabController.getBottamTabs() != null && tabController instanceof ConnectionTabContentController) {
                ((ConnectionTabContentController) tabController).setBottomPaneVisibility(visibility);
            }
        }
    }

    public TabContentController getTabControllerBySession(ConnectionSession session) {
        if (session != null) {
            for (Tab t : tabPane.getTabs()) {
                if (t.getId().equals("tab" + session.getId())) {
                    TabContentController controller = (TabContentController) t.getUserData();
                    return controller;
                }
            }
        }
        return null;
    }

    public Tab getTabBySession(ConnectionSession session) {
        if (session != null) {
            for (Tab t : tabPane.getTabs()) {
                if (t.getId().equals("tab" + session.getId())) {
                    return t;
                }
            }
        }
        return null;
    }

    private void executePostConnectionHook(final TabContentController tabController, final ConnectionSession session,
            final Tab tab, final ModuleController selectedModuleController) {
        //Default Connection
        ModuleController defaultModuleController = selectedModuleController == null ? getModuleByClassName(
                ConnectionModule.class.getName()) : selectedModuleController;
        for (ModuleCallback hook : postConnectionHooks) {

            if ((hook.isSystemModule() && hook.getDependencyModuleClass() == null) || (defaultModuleController != null && Objects.equals(
                    hook.getDependencyModuleClass(), defaultModuleController.getClass()))) {
                hook.call(session, tab);
                Map<HookKey, Hookable> menuItemHooks = hook.getHooks(session);
                if (menuItemHooks != null) {
                    for (HookKey key : menuItemHooks.keySet()) {

                        Platform.runLater(() -> changeMenuItemsSizes(key.getItems()));

                        boolean exists = false;
                        // if exists then just add items to exists
                        for (HookKey existsKey : tabController.getHooks().keySet()) {
                            if (existsKey.getGroup() != null && existsKey.getGroup().equals(key.getGroup())) {
                                if (existsKey.getItems() != null) {
                                    existsKey.getItems().addAll(key.getItems());
                                } else {
                                    existsKey.setItems(key.getItems());
                                }
                            }
                        }
                        if (!exists) {
                            tabController.getHooks().put(key, menuItemHooks.get(key));
                        }
                    }
                }

            }

        }
    }

    private void changeMenuItemsSizes(List<MenuItem> items) {
        if (items != null) {
            // change image size to standart sizes
            for (MenuItem mi : items) {
                ImageView iv = (ImageView) mi.getGraphic();
                if (iv != null) {
                    iv.setFitHeight(12);
                    iv.setFitWidth(12);
                }

                if (mi instanceof Menu) {
                    changeMenuItemsSizes(((Menu) mi).getItems());
                }
            }
        }
    }

    public void registerModule(ModuleController module) {
        modules.add(module);
    }

    public void registerPostConnectionHook(ModuleCallback callback) {
        // At now just sort after add
        postConnectionHooks.add(callback);
        Collections.sort(postConnectionHooks);
    }

    public void registerChangeConnection(Callback callback) {
        changeConnectionHooks.add(callback);
    }

    private Map<KeyCombination, List<Object>> keyCombinations = new HashMap<>();
    private final Map<KeyCombination, Consumer> customKeyCombinations = new HashMap<>();

    @Override
    public void addCustomKeyCombinations(KeyCombination key, Consumer action) {
        if (customKeyCombinations.put(key, action) != null) {
            log.warn("Diplicate custom key combination: " + key);
        }
    }

    // qb
    public void usedQueryBuilder() {
        try {
            usageService.savePUsage(PUsageFeatures.QB);
        } catch (Throwable th) {
            log.error("Error on usedQueryBuilder", th);
        }
    }

    //  qa
    public void usedQueryAnalyzer() {
        try {
            usageService.savePUsage(PUsageFeatures.QA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzer", th);
        }
    }

    // sqa
    public void usedSlowQueryAnalyzer() {
        try {
            usageService.savePUsage(PUsageFeatures.SQA);
        } catch (Throwable th) {
            log.error("Error on usedSlowQueryAnalyzer", th);
        }
    }

    // sqaqa
    public void usedQueryAnalyzerFromSlowQueryAnalyzer() {
        try {
            usageService.savePUsage(PUsageFeatures.SQAQA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzerFromSlowQueryAnalyzer", th);
        }
    }

    // debugger
    public void usedDebugger() {
        try {
            usageService.savePUsage(PUsageFeatures.DEBUGGER);
        } catch (Throwable th) {
            log.error("Error on usedDebugger", th);
        }
    }

    // dqa
    public void usedQueryAnalyzerFromDebugger() {
        try {
            usageService.savePUsage(PUsageFeatures.DQA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzerFromDebugger", th);
        }
    }

    // profiler
    public void usedProfiler() {
        try {
            usageService.savePUsage(PUsageFeatures.PROFILER);
        } catch (Throwable th) {
            log.error("Error on usedProfiler", th);
        }
    }

    // pqa
    public void usedQueryAnalyzerFromProfiler() {
        try {
            usageService.savePUsage(PUsageFeatures.PQA);
        } catch (Throwable th) {
            log.error("Error on usedQueryAnalyzerFromProfiler", th);
        }
    }

    // reports
    public void usedReports() {
        try {
            usageService.savePUsage(PUsageFeatures.REPORTS);
        } catch (Throwable th) {
            log.error("Error on usedReports", th);
        }
    }

    // spwc
    public void usedStoredProcedureWithCursor() {
        try {
            usageService.savePUsage(PUsageFeatures.SPWC);
        } catch (Throwable th) {
            log.error("Error on usedStoredProcedureWithCursor", th);
        }
    }

    // sd
    public void usedSchemaDesigner() {
        try {
            usageService.savePUsage(PUsageFeatures.SD);
        } catch (Throwable th) {
            log.error("Error on usedSchemaDesigner", th);
        }
    }

    // psu
    public void usedPublicSchema() {
        try {
            usageService.savePUsage(PUsageFeatures.PSU);
        } catch (Throwable th) {
            log.error("Error on usedPublicSchema", th);
        }
    }

    // mst
    public void usedMySqlTuner() {
        try {
            usageService.savePUsage(PUsageFeatures.MST);
        } catch (Throwable th) {
            log.error("Error on usedMySqlTuner", th);
        }
    }

    // qf
    public void usedQueryFormat() {
        try {
            usageService.savePUsage(PUsageFeatures.QF);
        } catch (Throwable th) {
            log.error("Error on usedQueryFormat", th);
        }
    }

    // lfe
    public void usedLockExecution() {
        try {
            usageService.savePUsage(PUsageFeatures.LFE);
        } catch (Throwable th) {
            log.error("Error on usedLockExecution", th);
        }
    }

    // show_bq
    public void usedWarnIcon(int count) {
        try {
            usageService.savePUsage(PUsageFeatures.SBQ);
        } catch (Throwable th) {
            log.error("Error on usedWarnIcon", th);
        }
    }

    // click_bq
    public void usedClickWarnIcon() {
        try {
            usageService.savePUsage(PUsageFeatures.CBQ);
        } catch (Throwable th) {
            log.error("Error on usedClickWarnIcon", th);
        }
    }

    // total_connections
    public void usedTotalDBConnections(Long size) {
        try {
            usageService.savePUsage(PUsageFeatures.TC);
        } catch (Throwable th) {
            log.error("Error on usedTotalDBConnections", th);
        }
    }

    /**
     * Initializes the controller class.
     * <p>
     * @param url
     * @param rb
     */
    @Override
    public void initialize(final URL url, final ResourceBundle rb) {

        footer.backgroundProperty().bind(toolbarParentContainer.backgroundProperty());
        footer.disableProperty().bind(selectedSession.isNull());
        prepareFrontPage();
       
        Color c = Color.LIGHTGREY.deriveColor(1.0, 1.0, 1.0 / 0.4, 1.0);
        toolbarParentContainer.setBackground(new Background(new BackgroundFill(c, CornerRadii.EMPTY, Insets.EMPTY)));

        lineColumLabel.setUserData(new Integer[]{0, 0});
//        leftPane.getChildren().add(provider.getUI("LeftPane"));
//        botomPane.getChildren().add(provider.getUI("QueryBrowser"));
        modules = new ArrayList<>();
        //addMenus();
        //fileTab.setUserData(MenuCategory.FILE);
        tabPane.getSelectionModel().select(-1);
        //tabPane.setTabMinWidth(154);
        //tabPane.setTabMaxWidth(154);

        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {

            Tab tab = tabPane.getTabs().get(tabPane.getTabs().size() - 1);
            Node tabNode = null;

            { 
                tab.setContextMenu(newConnectionContexMenu);
            }

            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                Label queriesLabel = (Label)getFooter(Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_QUERIES);
            
                connectionsLabel.setText("Connections: " + String.valueOf(tabPane.getTabs().size() - 1));
                if (newValue != null) {
                    if (newValue == tab) {
                        if (tabNode == null) {
                            tabNode = tabPane.lookup("#mainNew");
                        }
//                        if (getSelectedConnectionSession().get() != null) {
                            Point2D point = Utils.pointRelativeTo(tabNode,
                                    tabNode.getBoundsInLocal().getWidth(),
                                    tabNode.getBoundsInLocal().getHeight(),
                                    HPos.RIGHT, VPos.CENTER, tabNode.getBoundsInLocal().getMinX(),
                                    tabNode.getBoundsInLocal().getMinY(), true);

                            newConnectionContexMenu.show(tabNode, point.getX(), point.getY());
//                        }
                        if (tabPane.getTabs().size() > 1) {
                            tabPane.getSelectionModel().select(oldValue);
                        } else {
                            tabPane.getSelectionModel().clearSelection();
                        }
                        Platform.runLater(() -> {
                            if (getSelectedConnectionSession().get() == null) {
                                newConnectionContexMenu.hide();
                            }
                        });
                        return;
                    }
                    
                    boolean instanceOfConnectionTabContentController = false;

                    TabContentController tcc = (TabContentController) newValue.getUserData();
                    if (tcc != null) {
                        tcc.isFocusedNow();
                        if (tcc instanceof ConnectionTabContentController) {
                            currentLeftPaneVisibilityProperty.unbind();
                            currentLeftPaneVisibilityProperty.bind(((ConnectionTabContentController) tcc).getLeftPaneVisibiltyProperty());
                            instanceOfConnectionTabContentController = true;
                        }
                    }

                    getFooter(Flags.FOOTER_ELEMENT_CONNECTION_SESSIONS_SECTION).setVisible(instanceOfConnectionTabContentController);
                    getFooter(Flags.FOOTER_ELEMENT_NAV_BUTTON).setVisible(instanceOfConnectionTabContentController);

                    try {
                        final Integer id = Integer.valueOf(newValue.getId().replace("tab", ""));
                        manualSelectConnectionSession(connectionSessions.get(id));
//                        log.error("11111111111111111");
                        addWorkToBackground(new WorkProc() {
                            public void updateUI() {
                            }

                            public void run() {
//                                log.error("333333333333333333");
                                try {
                                    setMysqlVersion(connectionSessions.get(id).getService());
                                } catch (Exception exx) {
                                }
//                                log.error("44444444444444444444");
                            }
                        });
//                        log.error("22222222222222");
                    } catch (Throwable th) {
                        showError("Error", th.getMessage(), th);
                    }
                    
                    if (queriesLabel.textProperty().isBound()) {
                        queriesLabel.textProperty().unbind();
                    }

                    QueryTabModule queryTabModule = (QueryTabModule) getModuleByClassName(QueryTabModule.class.getName());
                    SimpleStringProperty prop = queryTabModule.sessionLastRunnedQueryCountProperty(tcc.getSelectedTab().get());
                    
                    if (prop != null) {
                        queriesLabel.textProperty().bind(prop);
                    }
                } else {
                    manualSelectConnectionSession(null);
                }
                /** WE NOT NEED THIS AGAIN
                if (oldValue != null && oldValue != tab) {
                    oldValue.setStyle("-fx-background-color:white;");
                    if (oldValue.getGraphic() != null) {
                        oldValue.getGraphic().setStyle("-fx-background-color: white;");
                    }
                }
                */
            }
        });
        
//        Node node = tabPane.lookup("#newConnectionImageView");
        tabPane.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getPickResult().getIntersectedNode() instanceof Label) {
                    for (Node n: ((Label)event.getPickResult().getIntersectedNode()).getChildrenUnmodifiable()) {
                        if (n == newConnectionImageView) {
                            Node tabNode = tabPane.lookup("#mainNew");
                            Point2D point = Utils.pointRelativeTo(tabNode,
                                    tabNode.getBoundsInLocal().getWidth(),
                                    tabNode.getBoundsInLocal().getHeight(),
                                    HPos.RIGHT, VPos.CENTER, tabNode.getBoundsInLocal().getMinX(),
                                    tabNode.getBoundsInLocal().getMinY(), true);

                            newConnectionContexMenu.show(tabNode, point.getX(), point.getY());
                            
                            event.consume();
                        }
                    }
                }
            }
        });

        ChangeListener<Boolean> serviceListener = (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {

            boolean busy = forceBusy.get() || selectedSession.get() != null && ((AbstractDBService) selectedSession.get().getService()).isServiceBusy();
            toolBarContainer.setCursor(busy ? Cursor.WAIT : Cursor.DEFAULT);
            additionalButtonContainer.setCursor(busy ? Cursor.WAIT : Cursor.DEFAULT);
            rightAdditionalButtonContainer.setCursor(busy ? Cursor.WAIT : Cursor.DEFAULT);
        };

        selectedSession.addListener((ObservableValue<? extends ConnectionSession> observable,
                ConnectionSession oldValue, ConnectionSession newValue) -> {

            ((QueryTabModule)getModuleByClassName(QueryTabModule.class.getName())).hideFindReplaceDialog();
            
            if (oldValue != null && newValue != null && Objects.equals(oldValue.getConnectionParam().getSelectedModuleClassName(), newValue.getConnectionParam().getSelectedModuleClassName())) {
                writeStageTitle(newValue);
                // No need to update
                return;
            }

            if (Platform.isFxApplicationThread()) {
                sessionSwitchCleanUp(newValue);
            } else {
                Platform.runLater(() -> sessionSwitchCleanUp(newValue));
            }

            if (newValue != null) {
                keyCombinations.clear();

                String moduleClass = getCurrentModuleClass(newValue.getConnectionParam());

                Map<MenuCategory, List<Node>> menuCategories = menus.get(moduleClass);

                log.info("Menu gategory size: " + menuCategories.size());

                toolBarButtonGroup.getToggles().clear();
                if (menuCategories != null) {

                    // Dont understand why it need, so commented
//                    List<Node> rightMenus = null;
                    for (MenuCategory mc : MenuCategory.values()) {
                        if (mc != null && menuCategories.keySet().contains(mc)) {

                            if (mc == MenuCategory.RIGHT_MENU) {
//                                rightMenus = menuCategories.get(mc);
                            } else {
                                ToggleButton tog = new ToggleButton(mc.getDisplayValue());
                                tog.setUserData(mc);
                                tog.setToggleGroup(toolBarButtonGroup);
                                tog.getStyleClass().add("top-toolbar-toggle");
                                Platform.runLater(() -> {
                                    toolBarContainer.getChildren().add(tog);
                                });
                                List<Node> nodes = menuCategories.get(mc);
                                for (Node n0: nodes) {
                                    
                                    Node n;
                                    if (n0 instanceof Group) {
                                        n = ((Group)n0).getChildren().get(0);
                                    } else {
                                        n = n0;
                                    }
                                    
                                    if (n instanceof MenuButton) {
                                        for (MenuItem mi : ((MenuButton) n).getItems()) {
                                            if (mi.getAccelerator() != null) {
                                                List<Object> list = keyCombinations.get(mi.getAccelerator());
                                                if (list == null) {
                                                    list = new ArrayList<>();
                                                    keyCombinations.put(mi.getAccelerator(), list);
                                                }
                                                list.add(mi);
                                            }
                                        }
                                    } else if (n.getUserData() instanceof KeyCombination) {
                                        List<Object> list = keyCombinations.get((KeyCombination) n.getUserData());
                                        if (list == null) {
                                            list = new ArrayList<>();
                                            keyCombinations.put((KeyCombination) n.getUserData(), list);
                                        }
                                        list.add(n);
                                    }
                                }
                            }
                        }
                    }
                }

                // add with moduleClass NULL - its for all
                menuCategories = menus.get(null);
                if (menuCategories != null) {
                    for (MenuCategory mc : MenuCategory.values()) {
                        if (mc != null && menuCategories.keySet().contains(mc)) {
                            Platform.runLater(() -> {
                                ((ToggleButton) emptyTab).setText(mc.getDisplayValue());
                                ((ToggleButton) emptyTab).setUserData(mc);
                                ((ToggleButton) emptyTab).setToggleGroup(toolBarButtonGroup);
                            });
                        }
                    }
                }
            }

            if (Platform.isFxApplicationThread()) {
                invalidateTopItemsState();
            } else {
                Platform.runLater(() -> {
                    invalidateTopItemsState();
                });
            }

            serviceListener.changed(null, false, true);

            if (oldValue != null) {
                ((AbstractDBService) oldValue.getService()).busy().removeListener(serviceListener);
                forceBusy.removeListener(serviceListener);
            }

            if (newValue != null) {
                ((AbstractDBService) newValue.getService()).busy().addListener(serviceListener);
                forceBusy.addListener(serviceListener);

                boolean busy = ((AbstractDBService) newValue.getService()).isServiceBusy();
                serviceListener.changed(null, !busy, busy);
//                Platform.runLater(()-> deActiveFrontPage() );
            }
        });

        toolBarButtonGroup.selectedToggleProperty().addListener(tabChanges);

        ReadOnlyObjectProperty<Scene> sceneProperty = getUI().sceneProperty();
        Scene scene = sceneProperty.get();
        if (scene == null) {
            sceneProperty.addListener((ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) -> {
                if (newValue != null) {
                    addSceneListeners(newValue);
                }
            });
        } else {
            addSceneListeners(scene);
        }

        final Image hideImage = ((ImageView) showHideButton.getGraphic()).getImage();

        showHideButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                toggleLeftPane();
            }
        });

        currentLeftPaneVisibilityProperty.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                ImageView showHideButtonImageGraphicContent = ((ImageView) showHideButton.getGraphic());
                if (newValue) {
                    showHideButtonImageGraphicContent.setImage(hideImage);
                } else {
                    showHideButtonImageGraphicContent.setImage(SmartImageProvider.getInstance().getShowOption_Image());
                }
            }
        });

        emptyTab.setToggleGroup(toolBarButtonGroup);

        log.debug("Main Controller initialized");
    }

    private void sessionSwitchCleanUp(ConnectionSession newValue) {
        writeStageTitle(newValue);
        toolBarButtonGroup.selectedToggleProperty().removeListener(tabChanges);
        additionalButtonContainer.getItems().clear();
        rightAdditionalButtonContainer.getItems().clear();
        toolBarContainer.getChildren().clear();
        toolBarContainer.getChildren().add((ToggleButton) emptyTab);
        if (toolBarButtonGroup.getToggles().size() > 0) {
            toolBarButtonGroup.getToggles().clear();
        }
    }

    public ConnectionTabContentController getConnectionTabContent() {
        TabContentController tcc = (TabContentController) tabPane.getSelectionModel().getSelectedItem().getUserData();
        if (tcc instanceof ConnectionTabContentController) {
            return (ConnectionTabContentController) tcc;
        }

        return null;
    }

    public TreeView getLeftTreeView() {
        TabContentController tcc = (TabContentController) tabPane.getSelectionModel().getSelectedItem().getUserData();
        if (tcc instanceof ConnectionTabContentController) {
            return ((ConnectionTabContentController) tcc).getTreeView();
        }

        return null;
    }

    public void addToolbarContainerTabListener(Consumer<MenuCategory> listener) {
        if (!categoryListeners.contains(listener)) {
            categoryListeners.add(listener);
        }
    }

    public void removeToolbarContainerTabListener(Consumer<MenuCategory> listener) {
        categoryListeners.remove(listener);
    }

    private void addSceneListeners(Scene scene) {
        scene.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {

            boolean f0 = hasCoordinate(toolBarContainer, event.getX(), event.getY());
            boolean f1 = hasCoordinate(toolbarButtonsContainer, event.getX(), event.getY());
            if (!f0 && !f1) {
                returnToDefaultTopMenuHandler.handle(event);
            }
        });

        scene.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {

//            log.error("KEY_PRESSED: " + event);
            for (KeyCombination keyCom : customKeyCombinations.keySet()) {
                if (keyCom.match(event)) {

                    Consumer mi = customKeyCombinations.get(keyCom);
                    mi.accept(event);
                    // If need consume event - then do this in consumer
                    // that is why must be commented consume in this point
//                    event.consume();

                    break;
                }
            }

            // TODO 
            // This is a hack, tableview handling "F2" key inside and this KeyCombinaton not working
            // for now just add exclusion for it            
            Event e = null;
            if (FunctionHelper.checkExclusion(event)) {
                e = event;
            } else {
                try {
                    e = ((Node) event.getTarget()).getEventDispatcher().dispatchEvent(event, new EventDispatchChainImpl());
                } catch (Exception exception) {
                }
            }

            if (e != null) {
                for (KeyCombination keyCom : keyCombinations.keySet()) {
                    if (!event.isConsumed() && keyCom.match(event)) {
                        List<Object> list = keyCombinations.get(keyCom);
                        for (Object node : list) {
                            if (node instanceof MenuItem) {
                                if (!((MenuItem) node).isDisable()) {
                                    ((MenuItem) node).fire();
                                    event.consume();
                                    break;
                                }
                            } else if (!((Button) node).isDisable()) {
                                ((Button) node).getOnMouseClicked().handle(null);
                                event.consume();
                                break;
                            }
                        }
                    }
                }
            } else {
                event.consume();
            }
        });
    }

    private boolean hasCoordinate(Parent parent, double x, double y) {
        Bounds parentBounds = parent.localToScene(parent.getBoundsInLocal());
        return parentBounds.getMinX() <= x && parentBounds.getMaxX() >= x && parentBounds.getMinY() <= y && parentBounds.getMaxY() >= y;
    }

    @FXML
    void newBttnAction(ActionEvent event) {

    }

    @FXML
    void closeAction(ActionEvent event) {
        // TODO
        getStage().setOnHidden(null); //to prevent update check in production mode
        shutDown();
        Platform.exit();
    }

    @Override
    public Stage getStage() {
        return getUI() != null && getUI().getScene() != null ? (Stage) getUI().getScene().getWindow() : null;
    }

    public void closeTab(TabContentController tcc) {

        for (final Tab t : tabPane.getTabs()) {

            final TabContentController controller = (TabContentController) t.getUserData();
            if (controller == tcc) {

                controller.destroyController(DisconnectPermission.NONE);
                tabPane.getTabs().remove(t);
                return;
            }
        }
    }

    public void closeAllTabs() {
        List<Tab> tabsForRemove = new ArrayList<>();

        DisconnectPermission permission = DisconnectPermission.NONE;
        for (final Tab t : tabPane.getTabs()) {
            final TabContentController controller = (TabContentController) t.getUserData();
            if (controller != null) {
                permission = controller.destroyController(permission);
                tabsForRemove.add(t);
            }
        }
        tabPane.getTabs().removeAll(tabsForRemove);
    }

//    @Override
    public Parent getUI() {
        return mainPane;
    }

    public DialogResponce showInfo(String titleBar, String infoMessage, String detailMessage) {
        return DialogHelper.showInfo(this, titleBar, infoMessage, detailMessage, getStage());
    }
    
    public DialogResponce showInfo(String titleBar, String infoMessage, String detailMessage, Window owner) {
        return DialogHelper.showInfo(this, titleBar, infoMessage, detailMessage, owner);
    }

    public DialogResponce showConfirm(String titleBar, String infoMessage, Window owner, boolean hasMultiButtons) {
        return DialogHelper.showConfirm(this, titleBar, infoMessage, owner, hasMultiButtons);
    }
    
    public DialogResponce showConfirm(String titleBar, String infoMessage) {
        return DialogHelper.showConfirm(this, titleBar, infoMessage, getStage(), false);
    }
    
    public String showInput(String titleBar, String message, String initialValue) {
        return DialogHelper.showInput(this, titleBar, message, "", initialValue, getStage());
    }
    
    public String showInput(String titleBar, String message, String label, String initialValue, Window owner) {
        return DialogHelper.showInput(this, titleBar, message, label, initialValue, owner);
    }

    public String showPassword(String titleBar, String message, String label, String initialValue, Window owner) {
        return DialogHelper.showPassword(this, titleBar, message, label, initialValue, owner);        
    }

    public DialogResponce showError(String titleBar, String infoMessage, Throwable throwable, Window owner) {
        if (throwable != null) {
            log.error(titleBar, throwable);
        } else {
            log.error(infoMessage);
        }
        ConnectionParams params = selectedSession.get() != null ? selectedSession.get().getConnectionParam() : null;
        
        return DialogHelper.showError(this, titleBar, infoMessage, throwable, params, owner);
    }
    
    public DialogResponce showError(String titleBar, String infoMessage, Throwable throwable) {
        return showError(titleBar, infoMessage, throwable, getStage());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    private BooleanBinding centerTabForQueryTabBooleanProperty;
    private BooleanBinding centerTabForOnlyQueryTabBooleanProperty;

    /**
     * Could be refractored better in conjuction with
     * connectiontabmodulecontroller,from the centertabpane selection listener.
     * since it already check for query tab use that if statement to alter this
     * boolean binding. so save a lot of computation time.
     */
    public BooleanBinding centerTabForQueryTabBooleanProperty() {
        if (centerTabForQueryTabBooleanProperty == null) {
            centerTabForQueryTabBooleanProperty = new BooleanBinding() {
                ReadOnlyObjectProperty<Tab> active = null;

                {
                    super.bind(selectedSession);
                }

                @Override
                protected boolean computeValue() {
                    if (selectedSession.isNull().get()) {
                        if (active != null) {
                            super.unbind(active);
                            active = null;
                        }
                    } else if (active != getSelectedConnectionTab(selectedSession.get())) {
                        if (active != null) {
                            super.unbind(active);
                        }
                        active = getSelectedConnectionTab(selectedSession.get());
                        if (active != null) {
                            super.bind(active);
                        }
                    }
                    return active == null || active.get() == null || active.get().getId() == null
                            || !(active.get().getId().endsWith("Tab"));
                }
            };
        }
        return centerTabForQueryTabBooleanProperty;
    }

    public BooleanBinding centerTabForOnlyQueryTabBooleanProperty() {
        if (centerTabForOnlyQueryTabBooleanProperty == null) {
            centerTabForOnlyQueryTabBooleanProperty = new BooleanBinding() {
                ReadOnlyObjectProperty<Tab> active = null;

                {
                    super.bind(selectedSession);
                }

                @Override
                protected boolean computeValue() {
                    if (selectedSession.isNull().get()) {
                        if (active != null) {
                            super.unbind(active);
                            active = null;
                        }
                    } else if (active != getSelectedConnectionTab(selectedSession.get())) {
                        if (active != null) {
                            super.unbind(active);
                        }
                        active = getSelectedConnectionTab(selectedSession.get());
                        if (active != null) {
                            super.bind(active);
                        }
                    }
                    return active == null || active.get() == null || active.get().getId() == null
                            || !(active.get().getId().equals(QueryTabModule.TAB_USER_DATA__QUERY_TAB));
                }
            };
        }
        return centerTabForOnlyQueryTabBooleanProperty;
    }

    private ObjectBinding selectTabProperty;

    public ObjectBinding selectedTabProperty() {
        if (selectTabProperty == null) {
            selectTabProperty = new ObjectBinding() {
                ReadOnlyObjectProperty<Tab> active = null;

                {
                    super.bind(selectedSession);
                }

                @Override
                protected Object computeValue() {
                    if (selectedSession.isNull().get()) {
                        if (active != null) {
                            super.unbind(active);
                            active = null;
                        }
                    } else if (active != getSelectedConnectionTab(selectedSession.get())) {
                        if (active != null) {
                            super.unbind(active);
                        }
                        active = getSelectedConnectionTab(selectedSession.get());
                        if (active != null) {
                            super.bind(active);
                        }
                    }
                    return active != null ? active.get() : null;
                }
            };
        }
        return selectTabProperty;
    }

    private boolean shutdown = false;

    public void shutDown() {
        if (shutdown) {
            return;
        }
        
        if (getSmartData().getEmailId() != null && getSmartData().getOnlineServer() != null) {
            usageService.savePCDetailsAfterClosing(getSmartData().getEmailId(), getSmartData().getOnlineServer());
        }
        
        shutdown = true;
        saveSessions();
        persistAppSaver.close();
        closeAllTabs();
        for (ModuleController mc : modules) {
            try {
                mc.uninstall();
            } catch (UnsupportedOperationException e) {
                //for those who haven not implemented it yet
            }
        }
        Recycler.dispose();
    }

    private final ChangeListener<Toggle> tabChanges = new ChangeListener<Toggle>() {

        @Override
        public void changed(ObservableValue<? extends Toggle> ov, Toggle unSelected, Toggle selected) {
            if (selected != null && selectedSession.get() != null) {
                handleTopMenuChanges(selected);
            }
        }
    };

    public synchronized void handleTopMenuChanges(Toggle selected) {
        additionalButtonContainer.getItems().clear();
        rightAdditionalButtonContainer.getItems().clear();

        Map<MenuCategory, List<Node>> menuCategories = menus.get(
                getCurrentModuleClass(selectedSession.get().getConnectionParam()));

        for (Consumer<MenuCategory> listener : categoryListeners) {
            listener.accept((MenuCategory) selected.getUserData());
        }

        MenuCategory mc = (MenuCategory) selected.getUserData();
        if (menuCategories != null) {
            List<Node> list = menuCategories.get(mc);
            if (list != null) {
                for (Node n : list) {
                    if (n.visibleProperty().get()) {
                        additionalButtonContainer.getItems().add(n);
                    }
                }
            }
        }

        // add to right
        if (mc == null) {
            List<Node> list = menuCategories.get(MenuCategory.RIGHT_MENU);
            if (list != null) {
                for (Node n : list) {
                    if (n.visibleProperty().get()) {
                        rightAdditionalButtonContainer.getItems().add(new Group(n));
                    }
                }
            }
        }

        // do the same for null
        menuCategories = menus.get(null);
        if (menuCategories != null) {
            List<Node> list = menuCategories.get(mc);
            if (list != null) {
                for (Node n : list) {
                    if (n.visibleProperty().get()) {
                        additionalButtonContainer.getItems().add(n);
                    }
                }
            }
        }
    }

    private String getCurrentModuleClass(ConnectionParams params) {
        String moduleClass = params.getSelectedModuleClassName();
        if (moduleClass == null
                || (!moduleClass.equals(ConnectionModule.class.getName())
                && !moduleClass.equals(SSHModule.class.getName())
                && !moduleClass.equals(SmartProfilerModule.class.getName())
                && !moduleClass.equals(SchemaDesignerModule.class.getName()))) {

            moduleClass = ConnectionModule.class.getName();
        }

        return moduleClass;
    }

    public void addWorkToBackground(WorkProc backgroundWork) {
        Recycler.addWorkProc(backgroundWork);
    }

    public void reachHistoryTab() {
        try {
            for (ModuleController mod : modules) {
                if (mod instanceof HistoryTabModule) {
                    Tab t = ((HistoryTabModule) mod).getTab(selectedSession.get());
                    if (t == null) {
                        mod.postConnection(selectedSession.get(), null);
                    } else {
                        t.getTabPane().getSelectionModel().select(t);
                    }
                    break;
                }
            }
        } catch (Exception ex) {
        }
    }

    public void reachFeqTab() {
        Tab t = (Tab) ((ReadOnlyObjectProperty) getSelectedConnectionTab(selectedSession.get())).get();
        if (t != null) {
            TabPane tPain = t.getTabPane();

            for (Tab tab : tPain.getTabs()) {
                if (FrequencyExecutedQueriesModule.FEQ_History.equals(tab.getUserData())) { //its history
                    tPain.getSelectionModel().select(tab);
                    return;
                }
            }
            //was not found
            try {
                for (ModuleController mod : modules) {
                    if (mod instanceof FrequencyExecutedQueriesModule) {
                        mod.postConnection(selectedSession.get(), null);
                        reachFeqTab();
                        break;
                    }
                }
            } catch (Exception ex) {
            }
        }
    }

    ///////////////////////////////////database work////////////////////    
    private final Object smartDataLocker = new Object();

    public Object getSmartDataLocker() {
        return smartDataLocker;
    }

    public SimpleBooleanProperty mysqlStatusProperty = new SimpleBooleanProperty(false) {
    };
    private Gson gson = new Gson();
    
    public SmartsqlSavedData getSmartData() {
        return StandardApplicationManager.getInstance().getSmartsqlSavedData();
    }

    @Override
    public SmartPropertiesData getSmartProperties() {
        return smartPropertiesData;
    }

    private void setMysqlVersion(DBService service) throws Exception {
        synchronized (smartDataLocker) {
            getSmartData().setMysqlVersion(service.getDbSQLVersion());
        }
    }

    private void checkMysqlServerStatus(boolean addAlertOnFailure) {
        if (mysqlStatusProperty.getValue()) {
            return;
        }
        Process proc = null;

        try {
            synchronized (smartDataLocker) {
                proc = Runtime.getRuntime().exec(Flags.checkIfServiceExistQuery(os_name));
            }
            try {
                proc.waitFor(3, TimeUnit.SECONDS);
            } catch (InterruptedException ie) {
            }
            InputStream in = null;
            StringWriter sw = new StringWriter(proc.getErrorStream().available());
            if (proc.getErrorStream().available() > 0) {
                in = proc.getErrorStream();
                int i = -1;
                while ((i = in.read()) != -1) {
                    sw.write(i);
                }
            }

            in = proc.getInputStream();
            BufferedReader buffReader = new BufferedReader(new InputStreamReader(in));
            mysqlStatusProperty.setValue(buffReader.ready() && Integer.valueOf(buffReader.readLine().trim()) > 0);

            //mysqlStatusProperty.set(new String(chars).trim().charAt(0) == '1');  
            if (!mysqlStatusProperty.getValue()) {
                System.out.println(sw.toString());
                if (addAlertOnFailure) {
                    Platform.runLater(() -> {
                        showInfo("ELEVATED PRIVILEGES REQUIREMENT",
                                "HELLO " + System.getProperty("user.name") + " :)"
                                + "\n"
                                + "\t Elevated privileges is required for system\n"
                                + "restoration to work perfectly  in  developing mode.\n"
                                + "Production mode is already sorted for, leaving\n"
                                + "complaint reductions.\n"
                                + "Click \"SHOW DETAILS\" for how to get elevated\n"
                                + "privileges in development mode.",
                                "Close NETBEANS application and re-open with\n"
                                + "administrator privileges, by right clicking on\n"
                                + "Netbeans.exe and runing as administrator. This\n"
                                + "will fix elevated issues.", Stage.impl_getWindows().next());

                    });
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean autoStartMysqlServer(boolean alertOnError) {

        /**
         * Windows typical output
         *
         * ON SUCCESS OUTPUT ---------------- SERVICE_NAME: MYSQL57 TYPE : 10
         * WIN32_OWN_PROCESS STATE : 2 START_PENDING (NOT_STOPPABLE,
         * NOT_PAUSABLE, IGNORES_SHUTDOWN) WIN32_EXIT_CODE : 0 (0x0)
         * SERVICE_EXIT_CODE : 0 (0x0) CHECKPOINT : 0x3 WAIT_HINT : 0x3a98 PID :
         * 3180 FLAGS : *
         *
         * ON ERROR OUTPUT -------------- [SC] StartService: OpenService FAILED
         * 5:
         *
         * Access is denied.
         *
         * currently for windows
         *
         *
         */
        if (!PlatformUtil.isWindows()) {
            return true;
        }

        try {
            checkMysqlServerStatus(alertOnError);

            if (mysqlStatusProperty.getValue()) {
                System.out.println("MYSQL SERVICE ALREADY RUNNING");
            } else {
                Process proc = Runtime.getRuntime().
                        exec(Flags.startServiceQuery(os_name, getMysqlServiceNameOnWindowsName()));
                try {
                    proc.waitFor(2, TimeUnit.SECONDS);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                InputStream in = proc.getInputStream();
                StringBuilder sb = new StringBuilder(proc.getInputStream().available());
                int i;
                while ((i = in.read()) != -1) {
                    sb.append((char) i);
                }
                mysqlStatusProperty.setValue(sb.toString().split(System.lineSeparator()).length > 10);
                System.out.println(sb.toString());//will be removed in production code
                if (!mysqlStatusProperty.getValue()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    checkMysqlServerStatus(alertOnError);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return/** mysqlStatusProperty.getValue()*/ true;
    }

    public void saveSessions() {
        persistAppSaver.saveOpenedSessions();
    }

    public void saveSession(ConnectionSession session, String connection, String name) {
        persistAppSaver.saveUserSession(session, connection, name);
    }

    SessionContainerController getSessionController() {
        SessionContainerController scc
                = (SessionContainerController) provider.get("SessionContainer");
        return scc;
    }

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyy-MM-dd");
    public void syncUserUsageStatistic() {
        if (FunctionHelper.hasConnectivity()) {
            boolean valid = false;

            File f = StandardApplicationManager.getInstance().getSmartDataDirectory(np.com.ngopal.smart.sql.structure.utils.Flags.VALID_FILE);            
            try {
                valid = usageService.syncWithPublic(
                    1, // SmartMySQL Browser
                    getSmartData().getEmailId(),
                    getSmartData().getLKey(),
                    getSmartData().getOnlineServer()
                );
                
                if (valid) {
                    // save that user is valid
                    if (f.exists()) {
                        f.delete();
                    }
                    
                    try {
                        Files.write(f.toPath(), DATE_FORMAT.format(new Date()).getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                    } catch (Throwable ex) {
                        log.error("Error", ex);
                    }
                }
            } catch (PublicDataException ex) {
                // Application API all type of errors application need to 
                // functional for one week time. For ex: if API and and internal 
                // DB access has some issue and it give some error then we need 
                // save one simple indidation file and allow user to use tool for 1 week. 
                if (ex.isCriticalError() && f.exists()) {
                    try {
                        String res = new String(Files.readAllBytes(f.toPath()), "UTF-8");
                        Date date = DATE_FORMAT.parse(res);

                        Calendar c0 = Calendar.getInstance();
                        c0.setTime(new Date());
                        c0.add(Calendar.DAY_OF_YEAR, 7);

                        if (c0.getTime().compareTo(date) > 0) {
                            valid = true;
                        }

                    } catch (Throwable th) {
                        log.error("Error", th);
                    }
                }
            }
            
            
            if (!valid) {
                LicenseController licenseController = StandardBaseControllerProvider.getController(this, "License");
                Platform.runLater(() -> {
                    licenseController.prepareLicenseScreen(usageService, true);
                    if (!licenseController.isRegistrationValid()) {
                        System.exit(0);
                    }
                });
            }
        }
    }
    
    public void initLicenseManager() {
        LicenseManager.getInstance().init(getLicenseDataService(), getLicenseDataUsageService(), getSmartData().getEmailId(), getSmartData().getLKey());
    }

    private boolean isH2ForData = false;

    public void initPersistAppSaver(Consumer callback) {
        persistAppSaver = new PersistenceAppSaver(this);
        if (preferenceService.getPreference().isMiscRestoreSession()) {
            Platform.runLater(() -> {
                persistAppSaver.restoreSession(callback);
            });
        } else {
            callback.accept(null);
        }
    }
    
    public void initConditionHelper() {
        //THIS CODE WAS IN MAINUI.CLASS AND I BROUGHT IT HERE
        List<ConnectionParams> paramsList = db.getAll();
        usedTotalDBConnections(Long.valueOf(paramsList != null ? paramsList.size() : 0));

        Reflections reflections = new Reflections("np.com.ngopal.smart.sql.modules",
                "np.com.ngopal.smart.sql.core.modules");        
        Set<Class<? extends ProfilerModule>> cls = reflections.getSubTypesOf(ProfilerModule.class);
        Set<Class<? extends ModuleController>> reflectedModules = reflections.getSubTypesOf(ModuleController.class);
        reflectedModules.addAll(cls);
        List<ModuleController> moduleIntances = new ArrayList<>();
        for (Class<? extends ModuleController> module : reflectedModules) {
            if (!Modifier.isAbstract(module.getModifiers())) {
                ModuleController mc = injector.getInstance(module);
                if (mc instanceof AbstractStandardModule || mc instanceof ProfilerModule){
                    mc.setController(controller);
                }
                moduleIntances.add(mc);
            }
        }
        
        cls.clear();
        reflectedModules.clear();
        
        // FIRST INSITIALIZATION OF HELPER
        RsSyntaxCompletionHelper.getInstance(injector.getInstance(FavouritesService.class), injector.getInstance(HistoryQueryService.class),
                injector.getInstance(HistoryTemplateService.class), preferenceService);        
        RsSyntaxCompletionHelper.getInstance().updateProviders();
        RsSyntaxCompletionHelper.getInstance().getProvider().setDataProvider(new CustomCompletionProvider.DataProvider() {
            @Override
            public DBTable getTable(String tableName) {
                ConnectionParams cp = getSelectedConnectionSession().getValue().getConnectionParam();
                return DatabaseCache.getInstance().getTable(cp.cacheKey(), cp.getSelectedDatabase() != null ? cp.getSelectedDatabase().getName() : null, tableName);
            }

            @Override
            public List<DBTable> getTables() {
                ConnectionParams cp = getSelectedConnectionSession().getValue().getConnectionParam();
                return DatabaseCache.getInstance().getTables(cp.cacheKey(), cp.getSelectedDatabase() != null ? cp.getSelectedDatabase().getName() : null);
            }
        });
        
        // sort and install on order priority
        Collections.sort(moduleIntances, (ModuleController o1, ModuleController o2) -> o1.getOrder() - o2.getOrder());
        for (ModuleController m : moduleIntances) {
            try { m.install(); } catch (Throwable th) { log.error("Error", th); }
            registerModule(((ModuleController) m));
            registerChangeConnection((Callback<ConnectionParams, ConnectionParams>) (ConnectionParams param) -> {
                try { return ((ModuleController) m).connectionChanged(param); }
                catch (Exception ex) {
                    Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
                    return param;
                }
            });
            registerPostConnectionHook(new ModuleCallback() {

                @Override
                public void call(ConnectionSession session, Tab tab) {
                    try {
                        ((ModuleController) m).postConnection((ConnectionSession) session, tab);
                    } catch (Exception ex) { Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex); }

                }

                @Override
                public boolean isSystemModule() {
                    return m.getClass().getPackage().getName().contains("core");
                }

                @Override
                public Class getModuleClass() {
                    return ((ModuleController) m).getModuleClass();
                }

                @Override
                public Map<HookKey, Hookable> getHooks(ConnectionSession param) {
                    return ((ModuleController) m).getHooks(param);
                }

                @Override
                public Class getDependencyModuleClass() {
                    return ((ModuleController) m).getDependencyModuleClass();
                }

                @Override
                public int order() {
                    return ((ModuleController) m).getOrder();
                }
            });
        }        
    }

    public void callCalculateUI() {
        getStage().setMaximized(getSmartData().isRunAppInFullScreen());
    }
    ///////////////////////////////////////////////////////////

    public void resetConnectionId(int reset) {
        connectionId = reset;
    }

    public void hideContentPane(ConnectionSession session, boolean visibility) {
        TabContentController tabController = getTabControllerBySession(session);
        if (tabController != null) {
            if (tabController.getBottamTabs() != null && tabController instanceof ConnectionTabContentController) {
                ((ConnectionTabContentController) tabController).setContentPaneVisibility(visibility);
            }
        }
    }

    /**
     *
     * @param bool if true it means it goes to the right if not stays on top
     */
    @Override
    public void togleTabPaneLayerToRight(boolean bool) {
        tabPane.setSide(bool ? Side.RIGHT : Side.TOP);
        tabPane.requestLayout();
    }

    /**
     * for writing stage's title
     */
    public void writeStageTitle(ConnectionSession newValue) {
        try {
            if (newValue != null && newValue.getConnectionParam() != null) {
                Platform.runLater(() -> {
                    getStage().setTitle("Smart MYSQL " + (getSmartData().isPaid() ? "Pro " : " ") + "-[ "
                            + newValue.getConnectionParam().getName()
                            + "/" + (databaseComboBox.getSelectionModel().getSelectedItem() == null ? ""
                                    : databaseComboBox.getSelectionModel().getSelectedItem().getName())
                            + " - "
                            + newValue.getConnectionParam().getUsername()
                            + "@"
                            + newValue.getConnectionParam().getAddress() + " ]");
                });
            }
        } catch (NullPointerException npe) {
            getStage().setTitle("Smart MYSQL ");
        }
    }

    public final static String desspec = "e34e7514-5738-11e5-885d-feff819cdc9f";

    /**
     * THis method calls every module's selectedTreeItemValueChanged
     *
     */
    public void notifyAllModulesOnTreeItem(TreeItem o) {
        notifyModuleWorkProc.setAdditionalData(o);
        addWorkToBackground(notifyModuleWorkProc);
    }
    /**
     * Since the number of modules expand as the app gets bigger running this on
     * the main thread might make it slow.
     */
    private final WorkProc notifyModuleWorkProc = new WorkProc() {
        @Override
        public void run() {
            for (ModuleController mc : modules) {
                try { mc.selectedTreeItemChanged((TreeItem) getAddData()); } catch (Exception e) { }
            }
            setAdditionalData(null);
        }
    };

    public Node getFooter(int flag) {
        switch (flag) {
            case Flags.FOOTER_ELEMENT_PARENT:
                return footer;
            case Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_LINE_PARA:
                return lineColumLabel;//((HBox)footer.getChildren().get(2)).getChildren().get(2);
            case Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_N0_OF_CON:
                return connectionsLabel;//((HBox)footer.getChildren().get(2)).getChildren().get(3);
            case Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_ROWS:
                return rowsCounter;//((HBox)footer.getChildren().get(2)).getChildren().get(1);
            case Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_QUERIES:
                return queriesCounter;
            case Flags.FOOTER_ELEMENT_CONNECTIONS_SESSIONS_TOTAL_TIME:
                return secondsCounter;//((HBox)footer.getChildren().get(2)).getChildren().get(0);
            case Flags.FOOTER_ELEMENT_CONNECTION_SESSIONS_SECTION:
                return sessionBox;//footer.getChildren().get(2);
            case Flags.FOOTER_ELEMENT_NAV_BUTTON:
                return showHideButton;//footer.getChildren().get(0);
            case Flags.FOOTER_ELEMENT_NOTIFICATION_SECTION:
                return notificationBox;//footer.getChildren().get(1);
            case Flags.FOOTER_ELEMENT_NOTIFICATION_UPDATE_FRONTLINE:
                return updateFrontLine;
            default:
                return footer;
        }
    }

    public void showApplicationFooter() {
        AnchorPane.setBottomAnchor(UiHolder, 23.0);
        footer.setVisible(true);
    }

    public void hideApplicationFooter() {
        AnchorPane.setBottomAnchor(UiHolder, 0.0);
        footer.setVisible(false);
    }

    public void manualSelectConnectionSession(ConnectionSession session) {
        selectedSession.set(session);
    }

    /**
     * This flag indicates whether connection is being restored by the automated
     * restoration persitence class.
     *
     * @return
     */
    public boolean isApplicationRunningModulationRestoration() {
        return persistAppSaver.isRestoring();
    }

    public SimpleBooleanProperty getApplicationRestoration_PersistingOperationState() {
        return persistAppSaver.getOperationState();
    }

    public void sessionSaveAs(ConnectionSession session, String connection) {
        String name = showInput("Save as", "Enter new name", "Name:", session.getName() != null ? session.getName() : "", getStage());
        if (name != null) {
            // check name
            List<Map<String, String>> userSessions = persistAppSaver.getAllUserSessions();
            for (Map<String, String> us : userSessions) {
                if (name.equalsIgnoreCase(us.get("name"))) {
                    showError("Error", "Session name already exist, please save with new name", null);
                    return;
                }
            }
            session.setName(name);
            session.setOpen(false);
            saveSession(session, connection, name);
        }
    }

    public void endSession(ConnectionSession session, String connection, String name) {
        if (session.isOpen()) {
            return;
        }

        persistAppSaver.saveUserSession(session, connection, name);
        //closeTab(getConnectionTabContent());

        disconnect(session, DisconnectPermission.NONE);
        checkIfNoConnectionTabs();
    }

    public void saveSessionTab(String connection, String name, String tab) {
        persistAppSaver.saveQueryTab(connection, name, tab);
    }

    public void openSessions() {

        SessionPointDialogController spdc = getTypedBaseController("SessionPointDialog");
        spdc.init(persistAppSaver.getAllUserSessions());

        Parent p = spdc.getUI();
        Scene scene = new Scene(p);
        scene.getStylesheets().add(QueryTabModule.class.getResource("css/dialog-style.css").toExternalForm());
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());

        dialog.setTitle("Select saved session");
        dialog.showAndWait();

        if (spdc.getSelectedSession() != null) {

            if (showConfirm("Close all opened sessions", "All opened sessions with same conenction - will be closed", getStage(), false) == DialogResponce.OK_YES) {

                // need close old opened the same OPENED session
                ConnectionSession[] lsit = connectionSessions.values().toArray(new ConnectionSession[0]);
                for (ConnectionSession cs : lsit) {
                    if (spdc.getSelectedSession().getConnection().equals(cs.getConnectionParam().getId().toString())) {
                        //closeTab(getTabControllerBySession(cs));        
                        disconnect(cs, DisconnectPermission.NONE);
                    }
                }

                persistAppSaver.openSession(spdc.getSelectedSession().getConnection(), spdc.getSelectedSession().getName());
            }
        }

    }

    public SimpleBooleanProperty getCurrentLeftPaneVisibilityProperty() {
        return currentLeftPaneVisibilityProperty;
    }

    public void invalidateTopItemsState() {
        toolBarButtonGroup.selectToggle(null);
        toolBarButtonGroup.selectedToggleProperty().addListener(tabChanges);
        // workaround - tabpane always have selected tab, so we make notvisible one
        returnToDefaultTopMenuHandler.handle(null);
    }

    public void forceRefreshSession() {
        ConnectionSession session = selectedSession.get();
        manualSelectConnectionSession(null);
        manualSelectConnectionSession(session);
    }

    private Callable<String> getMysqlServiceNameOnWindowsName() {
        return new Callable<String>() {
            @Override
            public String call() throws Exception {
                String serviceName = getSmartData().getMysqlServiceName();
                if (serviceName == null || serviceName.isEmpty()) {
                    Process process = Runtime.getRuntime().exec(
                            new String[]{
                                "cmd.exe", "/c", "sc", "queryex", "type=service", "|", "find", "i", "mysql"
                            });
                    process.waitFor(5, TimeUnit.SECONDS);
                    BufferedReader buffReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    if (buffReader.ready()) {
                        String line = buffReader.readLine().trim();
                        int colonIndex = line.indexOf(':');
                        if (colonIndex > -1) {
                            serviceName = line.split(" ")[1];
                            getSmartData().setMysqlServiceName(serviceName);
                        }
                    }
                    buffReader.close();
                    process.destroy();
                }
                return serviceName;
            }
        };
    }

    public void changeObjectFont(String fontsObjectBrowser) {
        for (ConnectionSession cs : connectionSessions.values()) {
            TabContentController tcc = getTabControllerBySession(cs);
            if (tcc != null) {
                tcc.changeFont(fontsObjectBrowser);
            }
        }
    }

    public void rebuildTags() {
        for (ConnectionSession cs : connectionSessions.values()) {
            QueryTabModule queryTabModule = (QueryTabModule) getModuleByClassName(QueryTabModule.class.getName());
            Collection<QueryAreaWrapper> list = queryTabModule.getAllCodeAreas();
            RsSyntaxCompletionHelper.getInstance().rebuildTags(cs.getConnectionParam(), list);
            RsSyntaxCompletionHelper.getInstance().updateCompletionsListForFunctions(cs.getConnectionParam(), cs.getConnectionParam().getSelectedDatabase());
            RsSyntaxCompletionHelper.getInstance().updateCompletionsListForViews(cs.getConnectionParam(), cs.getConnectionParam().getSelectedDatabase());
            RsSyntaxCompletionHelper.getInstance().updateCompletionsListForTriggers(cs.getConnectionParam(), cs.getConnectionParam().getSelectedDatabase());
            RsSyntaxCompletionHelper.getInstance().updateCompletionsListForProcedures(cs.getConnectionParam(), cs.getConnectionParam().getSelectedDatabase());
        }
    }

    public LoadingScreenController getLoaderScreen() {
        return loaderScreen;
    }

    public void showYoutubeDemoIfNeed() {
        
        String youtubeLinkText = controller.getSmartData().getYoutubeText();
        File youtubeFile = StandardApplicationManager.getInstance().getSmartDataDirectory(np.com.ngopal.smart.sql.structure.utils.Flags.YOUTUBE_LINK_FILE);
        if (youtubeLinkText != null && !youtubeLinkText.trim().isEmpty() && !youtubeFile.exists()) {
            
            if (youtubeFile.exists()) {
                youtubeFile.delete();
            }

            try {
                Files.write(youtubeFile.toPath(), youtubeLinkText.getBytes("UTF-8"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }

            Platform.runLater(()-> {
                writeStageTitle(null);

                YoutubeDemoController contr = getTypedBaseController("YoutubeDemo");
                contr.init(youtubeLinkText);

                Stage dialog = new Stage();
                dialog.setResizable(false);
                dialog.initStyle(StageStyle.UNDECORATED);
                Scene scene = new Scene(contr.getUI(), getStage().getWidth() * 0.8, getStage().getHeight() * 0.8);
                dialog.setScene(scene);
                dialog.initModality(Modality.WINDOW_MODAL);
                dialog.initOwner(getStage());
                dialog.setAlwaysOnTop(true);
                dialog.setTitle("SmartMySQL Query Browser Quick Demo (3m)");
                scene.getStylesheets().addAll(getStage().getScene().getStylesheets());

                dialog.setOnCloseRequest((WindowEvent event) -> {
                    contr.stop();
                });
                dialog.showAndWait();
            });
        }
    }
    
    private void prepareFrontPage() {
        tabPane.getTabs().addListener(new InvalidationListener() {
            @Override
            public void invalidated(javafx.beans.Observable observable) {
                Platform.runLater(() -> {
                    if(tabPane.getTabs().size() > 1){
//                        deActiveFrontPage();
                    }else if (selectedSession.get() == null && !shutdown) {
                        resetConnectionId(0);
                        Platform.runLater(()-> activateFrontPage() );
                    }
                });
            }
        });
        
//        ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(5), companyImage);
//        scaleTransition.setAutoReverse(true);
//        scaleTransition.setCycleCount(ScaleTransition.INDEFINITE);
//        scaleTransition.setByX(0.2);
//        scaleTransition.setByY(0.2);
//        
//        aboutContainer.getProperties().put(Flags.SPLASH_PAGE_ANIMATOR_FLAG, scaleTransition);
    }

    public void activateFrontPage() {
        //writeStageTitle(null);
        //no good reason why i put it run later. you can remove if want - it not change anything
//            aboutContainer.toFront();
//            Object o = aboutContainer.getProperties().get(Flags.SPLASH_PAGE_ANIMATOR_FLAG);
//            if(o != null){
//                ((ScaleTransition)o).playFromStart();
//            }
        //getUI().setOpacity(0.08);

        triggerHomeAction(false);
    }

//    public void deActiveFrontPage() {
//        Platform.runLater(() -> {
////            Object o = aboutContainer.getProperties().get(Flags.SPLASH_PAGE_ANIMATOR_FLAG);
////            if(o != null){
////                ((ScaleTransition)o).stop();
////            }  
////            aboutContainer.toBack();
//            //getUI().setOpacity(1);
//            if(mainPane.getChildren().size() > 2){
//                mainPane.getChildren().remove(mainPane.getChildren().size() - 1);
//            }
//        });
//    }

    @FXML
    private void onNewConnectionAction() {
        triggerHomeAction(false);
    }
    
    public void triggerHomeAction(boolean closable) {
        NewConnectionController connControl = getTypedBaseController("NewConnection");
        connControl.setCancelable(true);
        connControl.selectModule(ConnectionModule.class);
        connControl.setHasChanges(false);

//        if (isCancelable) {
            Stage dialog = new Stage();
            dialog.setResizable(false);
            dialog.initStyle(StageStyle.UTILITY);
            Scene scene = new Scene(connControl.getUI());
            dialog.setScene(scene);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.initOwner(getStage());
            dialog.setTitle("New Connection");
            scene.getStylesheets().addAll(getStage().getScene().getStylesheets());
            dialog.showAndWait();
            
            // Check this...
            if (!closable && !connControl.isConnected()) {
                Platform.exit();
            }
//        } else {
//            AnchorPane.setTopAnchor(connControl.getUI(), 0.0);
//            AnchorPane.setLeftAnchor(connControl.getUI(), 0.0);
//            AnchorPane.setRightAnchor(connControl.getUI(), 0.0);
//            AnchorPane.setBottomAnchor(connControl.getUI(), 0.0);
//
//            mainPane.getChildren().add(connControl.getUI());
//        }

    }

    @Override
    public void selectLast() {
        selectLastTab();
    }

    @Override
    public ReadOnlyObjectProperty<TreeItem> getSelectedTreeItem(ConnectionSession session) {
        return getSelectedTreeItem(session.getId());
    }

    @Override
    public void refreshTree(ConnectionSession session, boolean alertDatabaseCount) {
        refreshTree(session.getId(), alertDatabaseCount);
    }

    @Override
    public void addTabInConnection(Tab tab, int i) {
        ConnectionModule mc = (ConnectionModule) controller.getModuleByClassName(
                "np.com.ngopal.smart.sql.core.modules.ConnectionModule");
    }

    @Override
    public DisconnectPermission disconnect(ConnectionSession session, DisconnectPermission permission) {
        if (session != null) {
            int i = session.getId();
            connectionSessions.remove(i);

            Tab t = getTabBySession(session);
            if (t != null) {
                permission = ((TabContentController) t.getUserData()).destroyController(permission);

                tabPane.getTabs().remove(t);

                EventHandler h = t.getOnClosed();
                if (h != null) {
                    h.handle(null);
                }
            }
        }

        if (selectedSession.get() == null) {
            //TODO need add code here
        }

        return permission;
    }

    @Override
    public void saveSession(ConnectionSession session) {
        saveSession(session, String.valueOf(session.getConnectionParam().getId()), session.getName());
    }

    @Override
    public void saveSessionAs(ConnectionSession session) {
        sessionSaveAs(session, String.valueOf(session.getConnectionParam().getId()));
    }

    @Override
    public void saveSessionTab(ConnectionSession session, String tab) {
        saveSessionTab(String.valueOf(session.getConnectionParam().getId()), session.getName(), tab);
    }

    @Override
    public void openSessionPoint() {
        openSessions();
    }

    @Override
    public void endSession(ConnectionSession session) {
        endSession(session, String.valueOf(session.getConnectionParam().getId()), session.getName());
    }

    @Override
    public void aboutPage() {
        AboutController aboutController = getTypedBaseController("About");
        aboutController.init();
    }

    @Override
    public void addBackgroundWork(WorkProc wp) {
        Recycler.addWorkProc(wp);
    }

    @Override
    public HBox getApplicationFooter() {
        return (HBox) getFooter(Flags.FOOTER_ELEMENT_CONNECTION_SESSIONS_SECTION);
    }
    
    public <T extends BaseController> T getTypedBaseController(String input){
        return (T) provider.get(input);
    }
    
    public <T extends TabContentController> T getTypedTabContentBaseController(ModuleController module, ConnectionSession session){
        
        if (module != null && (Objects.equals("SSHModule", module.getClass().getSimpleName()) ||
                Objects.equals("SmartProfilerModule", module.getClass().getSimpleName()) ||
                Objects.equals("SchemaDesignerModule", module.getClass().getSimpleName()) ||
                Objects.equals("SchemaSynchronizerModule", module.getClass().getSimpleName()))) {
            provider.setInput(module.getClass().getSimpleName() + "TabContent");
        } else {
            provider.setInput("ConnectionModuleTabContent");
        }
        
        T controller = (T) provider.get();
        controller.setSession(session);
        controller.setDbService(session.getService());
        
        return controller;
    }
    
    public void checkAppMiscSettings() {        
        loaderScreen = getTypedBaseController("LoaderScreen");
        smartPropertiesData = new SmartPropertiesData(getSmartData());
        textBanner.prepareUiState();        
        
        initPersistAppSaver((v) -> {
            Recycler.addWorkProc(new WorkProc() {
                @Override
                public void run() {
                    
                    try {
                        PublicDataAccess.init(getSmartData().getEmailId(), getSmartData().getLKey(), getSmartData().getOnlineServer());
                    } catch (PublicDataException ex) {
                        log.error(ex.getMessage(), ex);
                    }
                    
                    showYoutubeDemoIfNeed();
                    
                    syncUserUsageStatistic();
                    initLicenseManager();
                    checkImportConnections();
                    
                    StandardApplicationManager.getInstance().installCommunicationProtocol(new CommunicationProtocol() {
                        @Override
                        public void onConnected() { }

                        @Override
                        public void parseContext(String context) {
                            processFileOpeningsForApp(context.trim());
                        }

                        @Override
                        public void onComplete() {
                            Platform.runLater(() -> {
                                if ("2".equals(System.getProperty(np.com.ngopal.smart.sql.structure.utils.Flags.SMARTSQL_BUILD_TYPE, "2"))) {
                                    getStage().setIconified(true);
                                }
                                getStage().setIconified(false);
                                getStage().toFront();
                            });
                        }
                    });
                    processFileOpeningsForApp(StandardApplicationManager.getInstance().getCurrentApplicationArguments());
                }
            });
        });
                
        autoStartCheckUpdateRoutine();
    }
    
    private void processFileOpeningsForApp(String... args){
        if(args == null){
            return;
        }
        
        QueryTabModule queryTabModule = (QueryTabModule) getModuleByClassName(QueryTabModule.class.getName());        
        for(String arg : args){
            File argFile = new File(arg);
            if(argFile.exists() && argFile.isFile()){
                queryTabModule.openFile(argFile);
            }
        }
    }
    
    private void autoStartCheckUpdateRoutine() {
        CheckForUpdatesController cuc = StandardBaseControllerProvider.getController(this, "CheckForUpdates");
        Scene scene = new Scene(cuc.getUI());
        scene.getStylesheets().add(QueryTabModule.class.getResource("css/dialog-style.css").toExternalForm());
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setScene(scene);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(getStage());
        dialog.setTitle("Check For Updates");
        cuc.init();
    }
    
    private void checkImportConnections() {
        // try autotriggering import from yog        
        File f = StandardApplicationManager.getInstance().getApplicationPublicFilesAndStorageDirectory(AUTOIMPORT_FILE);        
        // if not exist need try import
        if (!f.exists()) {
            String userName = System.getProperty("user.name");
            File importFile = new File(String.format(YOG_LOCATION, userName));
            if (importFile.exists()) {
                ImportConnectionDetails controllerImport = (ImportConnectionDetails) provider.get("ImportConnectionDetails");
                controllerImport.init(getStage(), importFile);
            }
            try {
                f.mkdirs();
                Files.createFile(f.toPath());
            } catch (IOException ex) {
                org.slf4j.LoggerFactory.getLogger(getClass()).error("Error saving autoimport file", ex);
            }
        }
    }
}
