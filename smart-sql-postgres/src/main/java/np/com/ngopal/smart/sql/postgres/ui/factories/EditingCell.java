/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.ngopal.smart.sql.ui.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import np.com.ngopal.smart.sql.ConnectionSession;
import np.com.ngopal.smart.sql.structure.controller.SlowQueryTemplateCellController;
import np.com.ngopal.smart.sql.structure.provider.StandardBaseControllerProvider;
import np.com.ngopal.smart.sql.structure.utils.NumberFormater;
import np.com.ngopal.smart.sql.ui.MainUI;
import np.com.ngopal.smart.sql.ui.controller.ResultTabController;

/**
 *
 * @author Narayan &lt; me@ngopal.com.np &gt;
 */
public class EditingCell<T, S> extends TableCell<T, S> {
    protected TextField textField;
    protected HBox textBox;
    
    private static final int DEFAULT_MAX_LENGTH = 30;
    
    private int length;
    
    private BiFunction<Integer, String, Boolean> checkAcceptedValue;
    private ConnectionSession session;
    
    private boolean cellEditing = false;
    
    private boolean canceled = false;
    
    public EditingCell() {
        this(DEFAULT_MAX_LENGTH);
    }
    
    public EditingCell(int length) {
        this(null, null, length);
    }
    
    public EditingCell(BiFunction<Integer, String, Boolean> checkAcceptedValue, ConnectionSession session) {
        this(checkAcceptedValue, session, DEFAULT_MAX_LENGTH);
    }
    
    public EditingCell(BiFunction<Integer, String, Boolean> checkAcceptedValue, ConnectionSession session, int length) {
        this.checkAcceptedValue = checkAcceptedValue;
        this.session = session;
        this.length = length;        
        setStyle("-fx-padding: 0 0 0 0; ");
    }

    @Override
    public void startEdit() {
        super.startEdit();
        
        canceled = false;
        
        if (textField == null) {
            createTextField();
        }
        
        setGraphic(textBox);
        
        String v = getString();
        if (!ResultTabController.NULL.equals(v)) {
            textField.setText(v);
        } else {
            textField.setText("");
        }

        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        
        Platform.runLater(() -> {
            textField.requestFocus();
            textField.selectAll();
        });
    }
    
    private String bColor,fColor;
    
    private void prepColors(){
        if (session != null) {
            bColor = session.getConnectionParam().getBackgroundColor();
            fColor = session.getConnectionParam().getForegroundColor();
            if ((bColor == null || bColor.isEmpty())) {
                bColor = "#e1ebf9";
            }
            if ((fColor == null || fColor.isEmpty())) {
                fColor = "black";
            }
        }
    }

    @Override
    public void updateSelected(boolean bln) {
        super.updateSelected(bln);
        prepColors();
//        if(bln){
//            setStyle("-fx-padding: 0 0 0 0; "
//                + "-fx-background-color: "+bColor + ";-fx-text-fill:"+fColor+";");
//        }else{
//            setStyle("-fx-padding: 0 0 0 0; "
//                + "-fx-background-color: #e1ebf9; -fx-text-fill : black;");
//        }
    }

    @Override
    public void cancelEdit() {
        
        if (cellEditing) {
            cellEditing = false;
            if (checkAcceptedValue == null || checkAcceptedValue.apply(getIndex(), textField.getText())) {
                commitEdit(convertFromString(textField.getText()));
            } else {
                super.cancelEdit();
            }
        } else {
            super.cancelEdit();
        }
        
        setText(getItem() == null ? "" : String.valueOf(getItem()));
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void commitEdit(S newValue) {
        super.commitEdit(newValue); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    @Override
    public void updateItem(S item, boolean empty) {
        super.updateItem(item, empty);
            
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }

                setGraphic(textBox);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                
                String itemStr = getString();
                if (itemStr != null && itemStr.length() >= length) {
                                        
                    Label l = new Label(itemStr.replaceAll("\r", "").replaceAll("\n", ""));
                    l.setMaxWidth(Double.MAX_VALUE);
                    
                    Button b = new Button(NumberFormater.compactByteLength(getString().getBytes().length));
                    b.setPadding(new Insets(1));
                    b.setFocusTraversable(false);
                    b.setMinWidth(40);
                    b.setPrefWidth(40);
                    b.setMaxWidth(40);
                    b.setOnAction((ActionEvent event) -> {
                        final Stage dialog = new Stage();
                        
                        SlowQueryTemplateCellController cellController = StandardBaseControllerProvider.getController(MainUI.getMainController(), "SlowQueryTemplateCell");
                        cellController.setPreferenceService(MainUI.getMainController().getPreferenceService());
                        cellController.init(getString(), false);

                        final Parent p = cellController.getUI();
                        dialog.initStyle(StageStyle.UTILITY);

                        final Scene scene = new Scene(p);
                        dialog.setScene(scene);
                        dialog.initModality(Modality.WINDOW_MODAL);
                        dialog.initOwner(MainUI.getMainController().getStage());
                        dialog.setTitle(getTableColumn().getText());

                        dialog.showAndWait();
                    });
                    
                    HBox box = new HBox();
                    box.setPadding(new Insets(5, 0, 5, 0));
                    box.setAlignment(Pos.CENTER_LEFT);
                    box.getChildren().addAll(l, b);
                    
                    HBox.setHgrow(l, Priority.ALWAYS);
                    
                    setGraphic(box);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }
    }
    
    protected S convertFromString(String item) {
        return (S)item;
    }
    
    protected String convertToString(S item) {
        return item != null ? item.toString() : "";
    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                cellEditing = true;
                if (null != t.getCode()) switch (t.getCode()) {
                    case ENTER:
                        cellEditing = false;
                        if (checkAcceptedValue == null || checkAcceptedValue.apply(getIndex(), textField.getText())) {
                            commitEdit(convertFromString(textField.getText()));
                        } else {
                            canceled = true;
                            cancelEdit();
                        }
                        break;
                    case ESCAPE:
                        canceled = true;
                        cellEditing = false;
                        cancelEdit();
                        break;
                    case TAB:
                        cellEditing = false;
                        if (checkAcceptedValue == null || checkAcceptedValue.apply(getIndex(), textField.getText())) {
                            commitEdit(convertFromString(textField.getText()));
                        } else {
                            canceled = true;
                            cancelEdit();
                        }
                        
                        TableColumn nextColumn = getNextColumn(!t.isShiftDown());
                        if (nextColumn != null) {
                            Platform.runLater(() -> {
                                getTableView().getSelectionModel().select(getTableRow().getIndex(), nextColumn); 
                                getTableView().edit(getTableRow().getIndex(), nextColumn); 
                            });                            
                        }   break;
                    default:
                        break;
                }
            }
        });
        
        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!canceled && !newValue && textField != null) {
                    if (checkAcceptedValue == null || checkAcceptedValue.apply(getIndex(), textField.getText())) {
                        
                        setItem(convertFromString(textField.getText()));
                        try {
                            commitEdit(convertFromString(textField.getText()));
                        } catch (Throwable th) {                            
                        }
                    } 
                }
            }
        });
        
        textBox = new HBox(textField);
        HBox.setHgrow(textField, Priority.ALWAYS);
        textBox.setFillHeight(true);
    }

    private String getString() {
        return convertToString(getItem());
    }

    /**
     *
     * @param forward true gets the column to the right, false the column to the left of the current column
     * @return
     */
    private TableColumn<T, ?> getNextColumn(boolean forward) {
        List<TableColumn<T, ?>> columns = new ArrayList<>();
        for (TableColumn<T, ?> column : getTableView().getColumns()) {
            columns.addAll(getLeaves(column));
        }
        //There is no other column that supports editing.
        if (columns.size() < 2) {
            return null;
        }
        int currentIndex = columns.indexOf(getTableColumn());
        int nextIndex = currentIndex;
        if (forward) {
            nextIndex++;
            if (nextIndex > columns.size() - 1) {
                nextIndex = 0;
            }
        } else {
            nextIndex--;
            if (nextIndex < 0) {
                nextIndex = columns.size() - 1;
            }
        }
        return columns.get(nextIndex);
    }

    private List<TableColumn<T, ?>> getLeaves(TableColumn<T, ?> root) {
        List<TableColumn<T, ?>> columns = new ArrayList<>();
        if (root.getColumns().isEmpty()) {
            //We only want the leaves that are editable.
            if (root.isEditable()) {
                columns.add(root);
            }
            return columns;
        } else {
            for (TableColumn<T, ?> column : root.getColumns()) {
                columns.addAll(getLeaves(column));
            }
            return columns;
        }
    }
}
