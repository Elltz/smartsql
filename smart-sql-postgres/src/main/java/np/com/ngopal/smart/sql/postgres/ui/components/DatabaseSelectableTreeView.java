/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package np.com.ngopal.smart.sql.ui.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import np.com.ngopal.smart.sql.db.DBService;
import np.com.ngopal.smart.sql.model.Database;
import np.com.ngopal.smart.sql.model.DatabaseChildren;
import np.com.ngopal.smart.sql.model.Event;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController;
import np.com.ngopal.smart.sql.ui.controller.ConnectionTabContentController.DatabaseChildrenType;
import np.com.ngopal.smart.sql.ui.provider.SmartImageProvider;

/**
 * @author Andrii Tsepushel (a.tsepushel@gmail.com)
 * @modify Terah Laweh (rumorsapp@gmail.com)
 */
@Slf4j
public class DatabaseSelectableTreeView extends TreeView<Object>
{
    private DBService dbService;
    
    private final Image folderImage = SmartImageProvider.getInstance().getFolder_image();
    private final Image tableImage = SmartImageProvider.getInstance().getTableTab_image();
    private final Image viewImage = SmartImageProvider.getInstance().getViewTab_image();
    private final Image functionImage = SmartImageProvider.getInstance().getFunctionTab_image();
    private final Image storedProcImage = SmartImageProvider.getInstance().getStoredProcTab_image();
    private final Image triggerImage = SmartImageProvider.getInstance().getTriggerTab_image();
    private final Image eventImage = SmartImageProvider.getInstance().getEventTab_image();
    
    
    
    public void loadData(DBService dbService, Database database) {
        loadData(dbService, database, true);
    }
    
    public void loadData(DBService dbService, Database database, boolean selected) {
        loadData(dbService, database, selected, null);
    }
    
    /**
    * selectedDataAsString: key - type.name(), value: joined with ":" names
    */
    public void loadData(DBService dbService, Database database, boolean selected, Map<String, String> selectedDataAsString) {
        this.dbService = dbService;
        
        TreeItem root = new TreeItem(database);
        
        for (ConnectionTabContentController.DatabaseChildrenType children : ConnectionTabContentController.DatabaseChildrenType.values()) {
            CheckBoxTreeItem treeItem = new CheckBoxTreeItem(children, new ImageView(folderImage), selectedDataAsString == null ? selected : selectedDataAsString.containsKey(children.name()));
            
            if (database != null) {
                loadChildrenTree(database, treeItem, children, selected, selectedDataAsString != null ? selectedDataAsString.get(children.name()) : null);
            }
            
            root.getChildren().add(treeItem);
            root.setExpanded(false);
        }
        
        root.setExpanded(true);
        setRoot(root);
        setShowRoot(false);
        
        // set the cell factory
        setCellFactory(CheckBoxTreeCell.forTreeView());
    }
    
    
    public Map<DatabaseChildrenType, List> getSelectedData() {
        Map<DatabaseChildrenType, List> data = new HashMap<>();
        
        TreeItem root = getRoot();
        if (root != null) {
            
            for (Object ti: root.getChildren()) {
                
                List typeList = new ArrayList();
                TreeItem children = (TreeItem)ti;
                
                // check all sub children for selected
                for (Object subItem: children.getChildren()) {
                    if (((CheckBoxTreeItem)subItem).isSelected()) {
                        typeList.add(((CheckBoxTreeItem)subItem).getValue());
                    }
                }
                
                if (!typeList.isEmpty()) {
                    data.put((DatabaseChildrenType) children.getValue(), typeList);
                }
            }
        }
        
        return data;
    }
    
    public Map<String, String> getSelectedDataAsString() {
        Map<String, String> data = new HashMap<>();
        
        TreeItem root = getRoot();
        if (root != null) {
            
            for (Object ti: root.getChildren()) {
                
                String dataString = "";
                TreeItem children = (TreeItem)ti;
                
                // check all sub children for selected
                for (Object subItem: children.getChildren()) {
                    if (((CheckBoxTreeItem)subItem).isSelected()) {
                        dataString += (((CheckBoxTreeItem)subItem).getValue()) + ":";
                    }
                }
                
                if (!dataString.isEmpty()) {
                    data.put(((DatabaseChildrenType) children.getValue()).name(), dataString.substring(0, dataString.length() - 1));
                }
            }
        }
        
        return data;
    }
    
    public Map<DatabaseChildrenType, List> getData() {
        Map<DatabaseChildrenType, List> data = new HashMap<>();
        
        TreeItem root = getRoot();
        if (root != null) {
            
            for (Object ti: root.getChildren()) {
                
                List typeList = new ArrayList();
                TreeItem children = (TreeItem)ti;
                
                // check all sub children for selected
                for (Object subItem: children.getChildren()) {
                    typeList.add(((CheckBoxTreeItem)subItem).getValue());
                }
                
                data.put((DatabaseChildrenType) children.getValue(), typeList);
            }
        }
        
        return data;
    }
    
    public Map<String, String> getDataAsString() {
        Map<String, String> data = new HashMap<>();
        
        TreeItem root = getRoot();
        if (root != null) {
            
            for (Object ti: root.getChildren()) {
                
                String dataString = "";
                TreeItem children = (TreeItem)ti;
                
                // check all sub children for selected
                for (Object subItem: children.getChildren()) {
                    dataString += (((CheckBoxTreeItem)subItem).getValue()) + ":";
                }
                
                if (!dataString.isEmpty()) {
                    data.put(((DatabaseChildrenType) children.getValue()).name(), dataString.substring(0, dataString.length() - 1));
                }
            }
        }
        
        return data;
    }
    
    private void loadChildrenTree(Database database, TreeItem item, ConnectionTabContentController.DatabaseChildrenType children, boolean selected, String selectedDataAsString) {
        
        switch (children) {
            case TABLES: 
//                try {
                    loadItems(item, database.getTables(), tableImage, selected, selectedDataAsString);
//                } catch (SQLException ex) {
//                    log.error("Error loading tables in tree", ex);
//                }
                break;            
            
            case VIEWS:
//                try {
                    loadItems(item, database.getViews(), viewImage, selected, selectedDataAsString);
//                } catch (SQLException ex) {
//                    log.error("Error loading views in tree", ex);
//                }
                break;
                
            case FUNCTIONS:
//                try {
                    loadItems(item, database.getFunctions(), functionImage, selected, selectedDataAsString);
//                } catch (SQLException ex) {
//                    log.error("Error loading functions in tree", ex);
//                }
                break;
                
            case TRIGGERS:
//                try {
                    loadItems(item, database.getTriggers(), triggerImage, selected, selectedDataAsString);
//                } catch (SQLException ex) {
//                    log.error("Error loading triggers in tree", ex);
//                }
                break;

            case STORED_PROCS:
//                try {
                    loadItems(item, database.getStoredProc(), storedProcImage, selected, selectedDataAsString);
//                } catch (SQLException ex) {
//                    log.error("Error loading stored procedures in tree", ex);
//                }
                break;
                
            case EVENTS:
//                try {
                    loadItems(item, database.getEvents(), eventImage, selected, selectedDataAsString);
//                } catch (SQLException ex) {
//                    log.error("Error loading events in tree", ex);
//                }
                break;

        }
    }
    
    
    private void loadItems(TreeItem item, List items, Image image, boolean selected, String selectedDataAsString) {        
        if (items != null) {
            
            
            String[] selectedData = null;
            if (selectedDataAsString != null) {
                if ("*".equals(selectedDataAsString)) {
                    selected = true;
                } else {
                    selectedData = selectedDataAsString.split(":");
                }
            }
            
            for (Object obj: items) {
                boolean forceSelected = selected;
                if (selectedData != null) {
                    for (String s: selectedData) {
                        if (obj instanceof Event) {
                            if (s.equals(obj.toString())) {
                                forceSelected = true;
                                break;
                            }
                        } else if (obj instanceof DatabaseChildren){
                            if (s.equals(((DatabaseChildren)obj).getName())) {
                                forceSelected = true;
                                break;
                            }
                        }
                    }
                }
                
                CheckBoxTreeItem childrenItem = new CheckBoxTreeItem<>(obj, new ImageView(image), forceSelected);
                item.getChildren().add(childrenItem);
            }
        }
    }
    
    public boolean hasSelection() {
        TreeItem root = getRoot();
        if (root != null) {

            for (Object ti : root.getChildren()) {                
                CheckBoxTreeItem children = (CheckBoxTreeItem) ti;
                if(children.isSelected()){
                    return true;
                }
                // check all sub children for selected
                for (Object subItem: children.getChildren()) {
                    if (((CheckBoxTreeItem)subItem).isSelected()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public boolean hasObjects() {
        TreeItem root = getRoot();
        if (root != null) {

            for (Object ti : root.getChildren()) {                
                CheckBoxTreeItem children = (CheckBoxTreeItem) ti;
                if(children.isSelected()){
                    // at least one must be selected
                    for (Object subItem: children.getChildren()) {
                        if (((CheckBoxTreeItem)subItem).isSelected()) {
                            return true;
                        }
                    }
                }
                // check all sub children for selected
                for (Object subItem: children.getChildren()) {
                    if (((CheckBoxTreeItem)subItem).isSelected()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
